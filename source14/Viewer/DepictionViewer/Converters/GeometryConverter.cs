﻿using System.Windows.Media;
using DepictionSilverlight.UI;
using DepictionSilverlight.View;
using SharpMap.Geometries;

namespace DepictionSilverlight.Converters
{
    /// <summary>
    /// Converts from spatial geometry (IGeometry) to WPF PathGeometry
    /// </summary>
    public static class GeometryConverter
    {
        public static PathGeometry MultiLineStringGeometryToPathGeometry(MultiLineString geometry)
        {
            var pathGeometry = new PathGeometry();

            if (geometry.LineStrings != null)
            {
                foreach (var lineString in geometry.LineStrings)
                {
                    var lineStringGeometry = LineStringGeometryToPathGeometry(lineString);
                    var figures = lineStringGeometry.Figures;
                    lineStringGeometry.Figures.Clear();
                    foreach (var figure in figures)
                        pathGeometry.Figures.Add(figure);
                }
            }
            return pathGeometry;
        }

        public static PathGeometry MultiPolygonGeometryToPathGeometry(MultiPolygon geometry)
        {
            var pathGeometry = new PathGeometry();

            if (geometry.Polygons != null)
            {
                foreach (var polygon in geometry.Polygons)
                {
                    var polygonGeometry = PolygonGeometryToPathGeometry(polygon);
                    var figures = polygonGeometry.Figures;
                    polygonGeometry.Figures.Clear();
                    foreach (var figure in figures)
                    {
                        pathGeometry.Figures.Add(figure);
                    }
                }
            }
            return pathGeometry;
        }

        public static PathGeometry LineStringGeometryToPathGeometry(LineString geometry)
        {
            var pathGeometry = new PathGeometry();
            var pathFigure = new PathFigure
                                 {
                                     StartPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(geometry.StartPoint.ToLatLon()),
                                     IsFilled = false
                                 };

            var polyLineSegment = new PolyLineSegment();

            foreach (var point in geometry.Vertices)
            {
                polyLineSegment.Points.Add(SilverlightAccess.PixelConverter.WorldToGeoCanvas(point.ToLatLon()));
            }

            pathFigure.Segments.Add(polyLineSegment);
            pathGeometry.Figures.Add(pathFigure);
            return pathGeometry;
        }

        public static PathGeometry PolygonGeometryToPathGeometry(SharpMap.Geometries.Polygon geometry)
        {
            var pathGeometry = new PathGeometry();
            var outlineFigure = new PathFigure
                                    {
                                        StartPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(geometry.ExteriorRing.StartPoint.ToLatLon()),
                                        IsFilled = true

                                    };

            var outline = new PolyLineSegment();

            foreach (var outlineLatLon in geometry.ExteriorRing.Vertices)
            {
                outline.Points.Add(SilverlightAccess.PixelConverter.WorldToGeoCanvas(outlineLatLon.ToLatLon()));
            }

            outlineFigure.Segments.Add(outline);
            pathGeometry.Figures.Add(outlineFigure);

            if (geometry.InteriorRings != null)
            {
                foreach (var hole in geometry.InteriorRings)
                {
                    var holeFigure = new PathFigure
                                         {
                                             StartPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(hole.StartPoint.ToLatLon()),
                                             IsClosed = true,
                                             //IsClosed = (hole[0].Latitude == hole[hole.Length - 1].Latitude && hole[0].Longitude == hole[hole.Length - 1].Longitude)
                                         };

                    var holePoly = new PolyLineSegment();

                    foreach (var point in hole.Vertices)
                    {
                        holePoly.Points.Add(SilverlightAccess.PixelConverter.WorldToGeoCanvas(point.ToLatLon()));
                    }

                    holeFigure.Segments.Add(holePoly);
                    pathGeometry.Figures.Add(holeFigure);
                }
            }
            return pathGeometry;
        }
    }
}
