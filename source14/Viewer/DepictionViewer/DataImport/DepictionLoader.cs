﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using DepictionSilverlight.Services;
using DepictionSilverlight.UI;
using DepictionSilverlight.Model;
//using GeoAPI.Geometries;
using Ionic.Zip;
using SharpMap.Converters.WellKnownText;

namespace DepictionSilverlight.Data
{
    public static class DepictionLoader
    {
        #region fields

        private static Dictionary<string, BitmapImage> bitmapsLoaded = new Dictionary<string, BitmapImage>();
        private static readonly string imagesFolder = Application.Current.Host.Source.ToString().Replace("DepictionSilverlight.xap", null) + "Images/";
        
        #endregion

        #region methods
        static Uri uri = new Uri(Application.Current.Host.Source.ToString().Replace("DepictionSilverlight.xap", null) + "depictionForWeb.zip");
        public static void ParseDepiction()
        {
            var wc = new WebClient();
            wc.OpenReadCompleted += WcOpenReadCompleted;

            //var uri = new Uri((String)Application.Current.Resources["depictionXMLURI"]);
            
            wc.OpenReadAsync(uri);
        }

        private static void WcOpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                popup(string.Format("Error occurred while loading {0} : {1}", uri, e.Error.Message));
                //Popup popup = new Popup();
                //popup.Child = new TextBlock {Text = e.Error.Message};
                //popup.IsOpen = true;
                return;
            }
            using (e.Result)
            {

                using (ZipFile zipFile = ZipFile.Read(e.Result))
                {

                    var zipEntry = zipFile["depictionForWeb.xml"];
                    
                    ReadDepictionDataFromStream(zipEntry.OpenReader());
                }
            }
        }

        static void popup(string message)
        {
            Popup myPopup = new Popup();
            Border border = new Border();
            border.BorderBrush = new SolidColorBrush(Colors.Black);
            border.Background = new SolidColorBrush(Colors.White);
            StackPanel myStackPanel = new StackPanel();

            var tb = new TextBox();
            tb.IsReadOnly = true;
            tb.Text = message;
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.VerticalAlignment = VerticalAlignment.Center;

            myStackPanel.Children.Add(tb);

            border.Child = myStackPanel;

            myPopup.Child = border;

            myPopup.HorizontalAlignment = HorizontalAlignment.Center;
            myPopup.VerticalAlignment = VerticalAlignment.Center;
            myPopup.IsOpen = true;
        }


        private static void ReadDepictionDataFromStream(Stream readStream)
        {
            var elements = new List<DepictionElement>();
            var revealers = new List<RevealerMetadata>();
            var annotations = new List<Annotation>();

            XmlReader xmlReader = XmlReader.Create(readStream);

            while (xmlReader.Read())
            {
                switch (xmlReader.Name)
                {
                    case "depictionData":
                        DepictionData.Instance.RegionTopLeft = new LatitudeLongitude(xmlReader.GetAttribute("topLeft"));
                        DepictionData.Instance.RegionBottomRight = new LatitudeLongitude(xmlReader.GetAttribute("bottomRight"));
                        DepictionData.Instance.ViewportTopLeft = new LatitudeLongitude(xmlReader.GetAttribute("viewportTopLeft"));
                        DepictionData.Instance.ViewportBottomRight = new LatitudeLongitude(xmlReader.GetAttribute("viewportBottomRight"));
                        DepictionData.LatitudeLongitudeFormat = (LatitudeLongitudeFormat) Enum.Parse(typeof (LatitudeLongitudeFormat), xmlReader.GetAttribute("latitudeLongitudeFormat"), true);
                        DepictionData.LatitudeLongitudeSeparator = (LatitudeLongitudeSeparator) Enum.Parse(typeof (LatitudeLongitudeSeparator), xmlReader.GetAttribute("latitudeLongitudeSeparator"), true);
                        DepictionData.Instance.Title = xmlReader.GetAttribute("title");
                        DepictionData.Instance.Description = xmlReader.GetAttribute("description");
                        DepictionData.Instance.LegendType = (LegendType) Enum.Parse(typeof(LegendType), xmlReader.GetAttribute("legendType"), true);

                        DepictionData.Instance.BackgroundBlackoutModel = new BackgroundBlackoutModel { Visibility = Boolean.Parse(xmlReader.GetAttribute("hideBackground")) ? Visibility.Visible : Visibility.Collapsed };

                        break;
                    case "annotationElement":
                        if (xmlReader.NodeType == XmlNodeType.EndElement) continue;
                        Annotation annotation = ReadAnnotation(xmlReader);
                        annotations.Add(annotation);
                        break;
                    case "depictionElement":
                        if (xmlReader.NodeType == XmlNodeType.EndElement || (xmlReader.GetAttribute("HoverText") != null && xmlReader.GetAttribute("HoverText").StartsWith("Road Network"))) continue;
                        var element = ReadElement(xmlReader);
                        if (!element.InMainCanvas && element.RevealerIds == null)
                            continue;
                        elements.Add(element);
                        break;
                    case "revealer":
                        // The main canvas elements should all be loaded prior to getting here
                        // Pass them in so the revealers contain them and do not obscure them
                        revealers.Add(ReadRevealer(xmlReader));
                        break;
                }
            }

            DepictionData.Instance.Elements = elements;
            DepictionData.Instance.Revealers = revealers;
            DepictionData.Instance.Annotations = annotations;

            if (DepictionLoaded != null)
                DepictionLoaded();
        }

        private static Annotation ReadAnnotation(XmlReader reader)
        {
            var annotation = new Annotation();


            annotation.TopLeft = new LatitudeLongitude(reader.GetAttribute("TopLeft"));
            annotation.OriginalIconWidth = 20;
            
            
            var visibility = reader.GetAttribute("Visibility");
            if (Boolean.Parse(visibility))
                annotation.Visibility = Visibility.Visible;
            else
                annotation.Visibility = Visibility.Collapsed;

            annotation.BackgroundColor = new SolidColorBrush(ColorConverter.ConvertFromString(reader.GetAttribute("backgroundColor")));
            annotation.ForegroundColor = new SolidColorBrush(ColorConverter.ConvertFromString(reader.GetAttribute("foregroundColor")));
            annotation.Width = Double.Parse(reader.GetAttribute("Width"));
            annotation.Height = Double.Parse(reader.GetAttribute("Height"));
            annotation.ContentLocationX = Double.Parse(reader.GetAttribute("contentLocationX"));
            annotation.ContentLocationY = Double.Parse(reader.GetAttribute("contentLocationY"));
            if (reader.IsEmptyElement) return annotation;

            reader.Read();
            annotation.AnnotationText = reader.Value;
            return annotation;
        }

        private static RevealerMetadata ReadRevealer(XmlReader reader)
        {
            var revealer = new RevealerMetadata();

            revealer.TopLeft = new LatitudeLongitude(reader.GetAttribute("topLeft"));
            revealer.BottomRight = new LatitudeLongitude(reader.GetAttribute("bottomRight"));
            revealer.RevealerId = reader.GetAttribute("revealerId");
            revealer.RevealerName = reader.GetAttribute("revealerName");
            revealer.Opacity = Convert.ToDouble(reader.GetAttribute("opacity"));
            revealer.RevealerShape = (RevealerShapeType)Enum.Parse(typeof(RevealerShapeType), reader.GetAttribute("revealerShape"), true);
            
            revealer.Visibility = Boolean.Parse(reader.GetAttribute("revealerVisible")) ? Visibility.Visible : Visibility.Collapsed;
            revealer.BorderVisible = Boolean.Parse(reader.GetAttribute("revealerBorderVisible")) ? Visibility.Visible : Visibility.Collapsed;
            revealer.Moveable = Boolean.Parse(reader.GetAttribute("revealerMoveable"));
            return revealer;
        }

        private static DepictionElement ReadElement(XmlReader reader)
        {
            var element = new DepictionElement();
            element.HoverText = reader.GetAttribute("HoverText");
            element.PermaHoverTextOn = Boolean.Parse(reader.GetAttribute("PermaHoverTextOn")) ? Visibility.Visible : Visibility.Collapsed;
            element.PermaTextX = Double.Parse(reader.GetAttribute("PermaTextX"));
            element.PermaTextY = Double.Parse(reader.GetAttribute("PermaTextY"));
            element.IconSize = Convert.ToDouble(reader.GetAttribute("IconSize"));
            if (!reader.GetAttribute("Revealers").Equals(string.Empty))
                element.RevealerIds = reader.GetAttribute("Revealers").Split(',');
            element.InMainCanvas = Convert.ToBoolean(reader.GetAttribute("InMainCanvas"));
            element.BorderColor = new SolidColorBrush(ColorConverter.ConvertFromString(reader.GetAttribute("IconBorderColor")));
            DepictionIconBorderShape borderShape;
            Enum.TryParse(reader.GetAttribute("IconBorderShape"), true, out borderShape);
            element.BorderShape = borderShape;
            var transparent = new Color {A = 0, B = 0, G = 0, R = 0};
            if (element.BorderColor.Color.Equals(transparent))
            {
                element.BorderColor = null;
            }
            element.TypeDisplayName = reader.GetAttribute("TypeDisplayName");
            bool active = Convert.ToBoolean(reader.GetAttribute("Active"));
            element.Active = active ? Visibility.Collapsed : Visibility.Visible;


            var source = ReadIcon(reader);
            element.IconSource = source;
            element.IconVisibility = Convert.ToBoolean(reader.GetAttribute("IconVisible"))?Visibility.Visible:Visibility.Collapsed;
            

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case "ZoneOfInfluence":
                                //if (reader.IsEmptyElement) continue;
                                element.ZoneOfInfluence = ReadZoneOfInfluence(reader);
                                break;
                            case "Location":
                                if (reader.IsEmptyElement) continue;
                                reader.Read();
                                element.Location = new LatitudeLongitude(reader.Value);
                                break;
                            case "FillColor":
                                if (reader.IsEmptyElement) continue;
                                reader.Read();
                                element.FillColor = reader.Value;
                                break;
                            case "StrokeColor":
                                if (reader.IsEmptyElement) continue;
                                reader.Read();
                                element.StrokeColor = reader.Value;
                                break;
                            case "ImageMetadata":
                                element.ImageMetadata = new ImageMetadata {
                                    TopLeftLatitudeLongitude = new LatitudeLongitude(reader.GetAttribute("TopLeft")),
                                    BottomRightLatitudeLongitude = new LatitudeLongitude(reader.GetAttribute("BottomRight")),
                                    Rotation = Convert.ToDouble(reader.GetAttribute("Rotation"))
                                };
                                break;
                            case "ElementWaypoint":
                                var waypoint = new ElementWaypoint{
                                    IconSize = Convert.ToDouble(reader.GetAttribute("IconSize")),
                                    IconVisibility = Convert.ToBoolean(reader.GetAttribute("IconVisible"))?Visibility.Visible:Visibility.Collapsed,
                                    Location = new LatitudeLongitude(reader.GetAttribute("Location")),
                                    IconSource = ReadIcon(reader),
                                };
                                if (element.ElementWaypoints == null)
                                    element.ElementWaypoints = new List<ElementWaypoint>();
                                element.ElementWaypoints.Add(waypoint);
                                break;
                        }
                        break;
                    case XmlNodeType.EndElement:
                        if (reader.Name == "depictionElement")
                        {
                            if (element.ZoneOfInfluence == null)
                            {
                                element.ZoneOfInfluence = new ZoneOfInfluence
                                                              {
                                                                  Geometry = null
                                                              };
                            }
                            return element;
                        }
                        break;
                }
            }
            return element;
        }

        private static ImageSource ReadIcon(XmlReader reader)
        {
            string iconPath = reader.GetAttribute("Icon");
            ImageSource source = null;
            if (iconPath != null)
            {
                string uri = imagesFolder + iconPath;

                if (bitmapsLoaded.ContainsKey(uri))
                {
                    source = bitmapsLoaded[uri];
                }
                else
                {
                    var bitmap = new BitmapImage(new Uri(uri, UriKind.Absolute));
                    bitmapsLoaded.Add(uri, bitmap);
                    source = bitmap;
                }
            }
            return source;
        }

        private static ZoneOfInfluence ReadZoneOfInfluence(XmlReader reader)
        {
            var zoi = new ZoneOfInfluence();

            string wkt = reader.GetAttribute("ZOIGeometry");
            var geometry = GeometryFromWKT.Parse(wkt);
            zoi.Geometry = geometry;

            return zoi;
        }
        
        #endregion

        #region events

        public static event Action DepictionLoaded;

        #endregion
    }
}