﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using DepictionSilverlight.Model;
using DepictionSilverlight.Tilers;

namespace DepictionSilverlight.ViewModel
{
    public class TileDisplayerViewModel : ViewModelBase
    {

        public TileDisplayerViewModel()
        {
            Tiler = new OpenStreetMapTiler();
        }

        private IList<TileViewModel> tiles = new ObservableCollection<TileViewModel>();
        private const int MaximumTilesOnCanvas = 100;

        public IList<TileViewModel> Tiles { get { return tiles; } }

        private ITiler tiler;
        public ITiler Tiler
        {
            get { return tiler; }
            set
            {
                tiler = value;
                tiles.Clear();
                if (fetcher != null)
                {
                    fetcher.AbortFetch();
                }
                fetcher = new TileFetcher(tiler);
                fetcher.DataChanged += FetcherOnDataChanged;

            }
        }

        private void FetcherOnDataChanged(TileModel tileModel)
        {
            SilverlightAccess.Dispatcher.BeginInvoke(new Action<TileModel>(OnDataChanged), tileModel);
        }

        private void OnDataChanged(TileModel tileModel)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.SetSource(new MemoryStream(tileModel.Image));
            var tileViewModel = new TileViewModel(tileModel);
            tileViewModel.TileSource = bitmapImage;
            if (!tiles.Contains(tileViewModel))
            {
                tiles.Add(tileViewModel);
            }
        }


        private int lastZoomLevel;
        private TileFetcher fetcher;

        public void TileVisibleArea(BoundingBox currentExtent)
        {
            var currentZoomLevel = Tiler.GetZoomLevel(currentExtent, 
                                        Convert.ToInt32(Application.Current.Host.Content.ActualWidth / 512), 18);
            if (lastZoomLevel != currentZoomLevel)
            {
                ZoomLevelChange(currentZoomLevel);
                lastZoomLevel = currentZoomLevel;
            }
            fetcher.ViewChanged(currentExtent);
            //if (currentExtent == null || Tiler == null)
            //    return;
            
            if (tiles.Count > MaximumTilesOnCanvas)
            {
                for (int i = 0; i < tiles.Count - MaximumTilesOnCanvas; i++)
                    tiles.RemoveAt(0);
            }
        }

        private void ZoomLevelChange(int currentZoomLevel)
        {
            List<TileViewModel> tilesToRefresh = new List<TileViewModel>();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].ImageOpened && tiles[i].ZoomLevel == currentZoomLevel)
                {
                    tilesToRefresh.Add(tiles[i]);
                    tiles.RemoveAt(i);
                    i--;
                }
            }
            foreach (var tile in tilesToRefresh)
            {
                tiles.Add(tile);
            }
        }
        
    }

}
