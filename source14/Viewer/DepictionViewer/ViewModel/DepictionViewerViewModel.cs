﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using DepictionSilverlight.Converters;
using DepictionSilverlight.Data;
using DepictionSilverlight.Model;
using DepictionSilverlight.Tilers;
using DepictionSilverlight.UI;
using SharpMap.Geometries;
using Point = System.Windows.Point;
//using Polygon = System.Windows.Shapes.Polygon;

namespace DepictionSilverlight.ViewModel
{
    public class DepictionViewerViewModel : ViewModelBase
    {

        public RegionMetadata RegionMetadata { get; set; }
        private readonly ViewportExtent worldExtent =
            new ViewportExtent(new BoundingBox(new LatitudeLongitude(85, -180), new LatitudeLongitude(-85, 180)),
                               SilverlightConstants.MapSize, SilverlightConstants.MapSize);

        private TransformGroup transformGroup;
        public TransformGroup TransformGroup { get { return transformGroup; } set { transformGroup = value; OnNotifyPropertyChanged("TransformGroup"); } }
        private ScaleTransform Scale { get; set; }
        private TranslateTransform Translate { get; set; }

        public IList<string> AllAvailableTileProviders { get { return new List<string> { OpenStreetMapTiler.Name, MapQuestTiler.Name }; } }
        public string CurrentTileProvider { get; set; }
        private TileDisplayerViewModel tileDisplayerViewModel;
        public TileDisplayerViewModel TileDisplayerViewModel
        {
            get { return tileDisplayerViewModel; }
            set { tileDisplayerViewModel = value; OnNotifyPropertyChanged("TileDisplayerViewModel"); }
        }

        private string lastMousePosition;
        public string LastMousePosition
        {
            get { return lastMousePosition; }
            set
            {
                lastMousePosition = value;
                OnNotifyPropertyChanged("LastMousePosition");
            }
        }
        //private IEnumerable<DepictionElementMapViewModel> iconElements;
        //public IEnumerable<DepictionElementMapViewModel> IconElements
        //{
        //    get { return iconElements; }
        //    set 
        //    { 
        //        iconElements = value;
        //        OnNotifyPropertyChanged("IconElements");
        //    }
        //}

        //private IEnumerable<DepictionElementMapViewModel> imageElements;
        //public IEnumerable<DepictionElementMapViewModel> ImageElements
        //{
        //    get { return imageElements; }
        //}

        //private IEnumerable<DepictionElementMapViewModel> zoiElements;
        //public IEnumerable<DepictionElementMapViewModel> ZOIElements
        //{
        //    get { return zoiElements; }
        //}

        private List<DepictionElementMapViewModel> elementViewModels = new List<DepictionElementMapViewModel>();
        public List<DepictionElementMapViewModel> ElementViewModels { get { return elementViewModels; } }
        private BackgroundBlackoutViewModel backgroundBlackoutViewModel;
        public BackgroundBlackoutViewModel BackgroundBlackoutViewModel
        {
            get { return backgroundBlackoutViewModel; }
            set { backgroundBlackoutViewModel = value; OnNotifyPropertyChanged("BackgroundBlackoutViewModel"); }
        }


        public DepictionViewerViewModel()
        {

            SilverlightAccess.PixelConverter = new MercatorCoordinateConverter(worldExtent, 0, 0);

            SilverlightAccess.ApplicationWidth = Application.Current.Host.Content.ActualWidth;
            SilverlightAccess.ApplicationHeight = Application.Current.Host.Content.ActualHeight;

            SilverlightAccess.VisualAccess = new VisualAccess();
            CurrentTileProvider = OpenStreetMapTiler.Name;
            DepictionLoader.ParseDepiction();
            DepictionLoader.DepictionLoaded += DepictionLoaderDepictionLoaded;
            
            RegionMetadata = new RegionMetadata();
            tileDisplayerViewModel = new TileDisplayerViewModel();

            Application.Current.Host.Content.FullScreenChanged += Content_FullScreenChanged;
        }

        private void DepictionLoaderDepictionLoaded()
        {

            Point viewportTopLeftOnCanvas = SilverlightAccess.PixelConverter.WorldToGeoCanvas(DepictionData.Instance.RegionTopLeft);
            BuildViewportAtTopLeft(viewportTopLeftOnCanvas);
            RecalculateRegionExtentInScreenPixels();
            RecalculateRegionBox();

            viewportTopLeftOnCanvas = new Point(0, 0);
            Point viewportBottomRightOnCanvas = SilverlightAccess.PixelConverter.WorldToGeoCanvas(DepictionData.Instance.RegionBottomRight);
            //Point adjustedTopLeft = new Point { X = viewportTopLeftOnCanvas.X, Y = viewportTopLeftOnCanvas.Y }, adjustedBottomRight = new Point { X = viewportBottomRightOnCanvas.X, Y = viewportBottomRightOnCanvas.Y };
            //adjustedTopLeft.Y -= (viewportTopLeftOnCanvas.Y - viewportBottomRightOnCanvas.Y) * .05;
            //adjustedBottomRight.Y -= (viewportTopLeftOnCanvas.Y - viewportBottomRightOnCanvas.Y) * .05;
            //adjustedTopLeft.X -= (viewportBottomRightOnCanvas.X - viewportTopLeftOnCanvas.X) * .05;
            //adjustedBottomRight.X += (viewportBottomRightOnCanvas.X - viewportTopLeftOnCanvas.X) * .05;
            //viewportTopLeftOnCanvas = adjustedTopLeft;
            //viewportBottomRightOnCanvas = adjustedBottomRight;

            var canvasPixels = viewportBottomRightOnCanvas.X - viewportTopLeftOnCanvas.X;

            var scale = SilverlightAccess.ApplicationWidth / canvasPixels;
            //var scale = worldCanvasHolder.ActualWidth / canvasPixels;
            SilverlightAccess.VisualAccess.OnePixelWidthOnWorldCanvas = 1 / scale;

            TransformGroup = new TransformGroup();
            Translate = new TranslateTransform { X = 0, Y = 0 };
            Scale = new ScaleTransform { ScaleX = scale, ScaleY = scale };


            //RenderTransformOrigin = new Point(0, 0);
            Scale.CenterX = 0;
            Scale.CenterY = 0;
            TransformGroup.Children.Add(Scale);
            TransformGroup.Children.Add(Translate);
            //worldCanvas.RenderTransform = TransGroup;
            Zoom(.9, true, new Point());


            BackgroundBlackoutViewModel = new BackgroundBlackoutViewModel(DepictionData.Instance.BackgroundBlackoutModel);
            //backgroundBlackout.DataContext = backgroundBlackoutVM;

            SilverlightAccess.Dispatcher.BeginInvoke(SetRegionBlackOutExtent);

            Application.Current.Host.Content.Resized += Content_FullScreenChanged;
        }


        private void RecalculateRegionBox()
        {
            RegionMetadata.WindowHeight = Application.Current.Host.Content.ActualHeight;
            //RegionMetadata.InfoBarTop = RegionMetadata.WindowHeight - infoBar.ActualHeight;
            RegionMetadata.WindowWidth = Application.Current.Host.Content.ActualWidth;
        }

        private void RecalculateRegionExtentInScreenPixels()
        {
            RegionMetadata.RegionExtent = new BoundingBox(DepictionData.Instance.RegionTopLeft, DepictionData.Instance.RegionBottomRight);
            Point topLeft = SilverlightAccess.PixelConverter.WorldToGeoCanvas(DepictionData.Instance.RegionTopLeft);
            Point bottomRight = SilverlightAccess.PixelConverter.WorldToGeoCanvas(DepictionData.Instance.RegionBottomRight);

            RegionMetadata.Top = topLeft.Y;
            RegionMetadata.Left = topLeft.X;
            RegionMetadata.Right = bottomRight.X;
            RegionMetadata.Bottom = bottomRight.Y;
            RegionMetadata.Width = Math.Abs(bottomRight.X - topLeft.X);
            RegionMetadata.Height = Math.Abs(topLeft.Y - bottomRight.Y);
        }

        private void BuildViewportAtTopLeft(Point topLeft)
        {

            SilverlightAccess.PixelConverter = new MercatorCoordinateConverter(worldExtent, topLeft.X, topLeft.Y);
            foreach (Annotation annotation in DepictionData.Instance.Annotations)
                annotation.GeoConversionsViewportChanged();
            foreach (DepictionElement depictionElement in DepictionData.Instance.Elements)
                elementViewModels.Add(GenerateViewModel(depictionElement));
            foreach (RevealerMetadata revealer in DepictionData.Instance.Revealers)
                revealer.GeoConversionsViewportChanged();
        }


        public DepictionElementMapViewModel GenerateViewModel(DepictionElement depictionElement)
        {
            var viewModel = new DepictionElementMapViewModel(depictionElement);
            if (depictionElement.ImageMetadata != null)
            {
                depictionElement.ImageMetadata.TopLeftPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(depictionElement.ImageMetadata.TopLeftLatitudeLongitude);
                Point bottomRightPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(depictionElement.ImageMetadata.BottomRightLatitudeLongitude);
                depictionElement.ImageMetadata.Width = Math.Abs(depictionElement.ImageMetadata.TopLeftPoint.X - bottomRightPoint.X);
                depictionElement.ImageMetadata.Height = Math.Abs(depictionElement.ImageMetadata.TopLeftPoint.Y - bottomRightPoint.Y);
                viewModel.ImageMetadata = depictionElement.ImageMetadata;
            }
            else if (depictionElement.Location != null)
            {
                var iconData = new IconData();
                iconData.Height = iconData.Width = depictionElement.IconSize;
                Point locationPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(depictionElement.Location);
                iconData.PixelLocation = locationPoint;
                viewModel.IconData = iconData;
            }

            //NotifyPropertyChanged("IconVisibility");
            //NotifyPropertyChanged("ImageVisibility");

            //if (depictionElement.ZoneOfInfluence == null) return;
            //if (depictionElement.ZoneOfInfluence.GeometryType == GeometryType.Point)
            //    return;
            //if (depictionElement.ZoneOfInfluence.Geometry == null)
            //    return;
            if (depictionElement.ZoneOfInfluence != null && depictionElement.ZoneOfInfluence.GeometryType != GeometryType.Point && depictionElement.ZoneOfInfluence.Geometry != null)
            {
                PathGeometry pathGeometry = null;
                //var zoiGeometry = depictionElement.ZoneOfInfluence.Geometry;
                switch (depictionElement.ZoneOfInfluence.GeometryType)
                {
                    case GeometryType.Polygon:
                        pathGeometry = GeometryConverter.PolygonGeometryToPathGeometry((Polygon)depictionElement.ZoneOfInfluence.Geometry);
                        break;
                    case GeometryType.MultiPolygon:
                        pathGeometry = GeometryConverter.MultiPolygonGeometryToPathGeometry((MultiPolygon)depictionElement.ZoneOfInfluence.Geometry);
                        break;
                    case GeometryType.LineString:
                        pathGeometry = GeometryConverter.LineStringGeometryToPathGeometry((LineString)depictionElement.ZoneOfInfluence.Geometry);
                        break;
                    case GeometryType.MultiLineString:
                        pathGeometry = GeometryConverter.MultiLineStringGeometryToPathGeometry((MultiLineString)depictionElement.ZoneOfInfluence.Geometry);
                        break;
                }
                viewModel.PathGeometry = pathGeometry;
            }
            viewModel.UpdateRenderTransform(SilverlightAccess.VisualAccess.OnePixelWidthOnWorldCanvas);
            //NotifyPropertyChanged("DepictionElementMapViewModel");
            return viewModel;
        }


        private bool update;
        
        private void SetRegionBlackOutExtent()
        {
            Point topLeft = new Point(RegionMetadata.Left - (RegionMetadata.Right - RegionMetadata.Left) * .1,
                                        RegionMetadata.Top + (RegionMetadata.Top - RegionMetadata.Bottom) * .1);
            Point bottomRight = new Point(RegionMetadata.Right + (RegionMetadata.Right - RegionMetadata.Left) * .1,
                                        RegionMetadata.Bottom - (RegionMetadata.Top - RegionMetadata.Bottom) * .1);
            var screenPt = TransformGroup.Transform(topLeft);
            var screenPtBr = TransformGroup.Transform(bottomRight);
            var screenWidth = screenPtBr.X - screenPt.X;
            var screenHeight = screenPtBr.Y - screenPt.Y;
            if (!update)
            {
                GeometryGroup geometryGroup = new GeometryGroup();
                geometryGroup.Children.Add(new RectangleGeometry { Rect = new Rect(0, 0, 2000, 2000) });
                Rect regionBlackoutRect = new Rect(screenPt.X, screenPt.Y, screenWidth, screenHeight);
                var regionBlackoutRectGeom = new RectangleGeometry { Rect = regionBlackoutRect };
                regionBlackoutRectGeom.Rect = regionBlackoutRect;
                geometryGroup.Children.Add(regionBlackoutRectGeom);
                BackgroundBlackoutViewModel.Clip = geometryGroup;
                update = true;
            }
            else
            {
                //a little premature optimization, might not be any faster than recreating the entire clip though
                ((RectangleGeometry)BackgroundBlackoutViewModel.Clip.Children[1]).Rect = new Rect(screenPt.X, screenPt.Y, screenWidth, screenHeight);
            }
        }


        void Content_FullScreenChanged(object sender, EventArgs e)
        {
            SilverlightAccess.ApplicationWidth = Application.Current.Host.Content.ActualWidth;
            RecalculateRegionBox();
            CenterOnPoint(RegionMetadata.RegionExtent.Center);
        }


        private BoundingBox CurrentExtent
        {
            get
            {
                if (TransformGroup == null)
                    return null;
                Point topLeftPt = TransformGroup.Inverse.Transform(new Point(0, 0));
                Point bottomRightPt =
                    TransformGroup.Inverse.Transform(new Point(Application.Current.Host.Content.ActualWidth, Application.Current.Host.Content.ActualHeight));

                LatitudeLongitude topLeft = SilverlightAccess.PixelConverter.GeoCanvasToWorld(topLeftPt);
                LatitudeLongitude bottomRight = SilverlightAccess.PixelConverter.GeoCanvasToWorld(bottomRightPt);
                return new BoundingBox(topLeft, bottomRight);
            }
        }

        public void CenterOnPoint(LatitudeLongitude centerCoord)
        {
            var untransformedWorldCanvasPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(centerCoord);
            var transformedWorldCanvasPoint = Scale.Transform(untransformedWorldCanvasPoint);

            //var holderRect = new Rect(0, 0, Application.Current.Host.Content.ActualWidth,
            //    Application.Current.Host.Content.ActualHeight);
            var midX = Application.Current.Host.Content.ActualWidth / 2;
            var midY = Application.Current.Host.Content.ActualHeight / 2;
            var currentCenter = new Point(midX, midY);

            var x = currentCenter.X - Translate.X;
            var y = currentCenter.Y - Translate.Y;
            x -= transformedWorldCanvasPoint.X;
            y -= transformedWorldCanvasPoint.Y;

            Translate.X += x;
            Translate.Y += y;
            SilverlightAccess.Dispatcher.BeginInvoke(TileVisibleArea);
            SilverlightAccess.Dispatcher.BeginInvoke(SetRegionBlackOutExtent);
        }

        public void Zoom(double zoomFactor, bool zoomAboutCenter, Point position)
        {
            if (zoomAboutCenter)
            {
                Point center = new Point(((-Translate.X / Scale.ScaleX) + ((-Translate.X / Scale.ScaleX) + SilverlightAccess.ApplicationWidth / Scale.ScaleX)) / 2,
                         ((-Translate.Y / Scale.ScaleY) + ((-Translate.Y / Scale.ScaleY) + SilverlightAccess.ApplicationHeight / Scale.ScaleY)) / 2);
                ZoomToPoint(zoomFactor, center);
            }
            else
                ZoomToPoint(zoomFactor, position);
            SilverlightAccess.Dispatcher.BeginInvoke(TileVisibleArea);
        }

        /// <summary>
        /// Moves the viewport by a fixed step in the specified direction.
        /// </summary>
        /// <param name="direction">The direction to move the content within the viewport</param>
        public void Pan(NavigationControlPanDirection direction)
        {
            const double stepFactor = 0.1D;
            double stepX = SilverlightAccess.ApplicationWidth * stepFactor;
            double stepY = SilverlightAccess.ApplicationWidth * stepFactor;
            switch (direction)
            {
                case NavigationControlPanDirection.Left:
                    Translate.X = Translate.X + stepX;
                    break;
                case NavigationControlPanDirection.Up:
                    Translate.Y = Translate.Y + stepY;
                    break;
                case NavigationControlPanDirection.Right:
                    Translate.X = Translate.X - stepX;
                    break;
                case NavigationControlPanDirection.Down:
                    Translate.Y = Translate.Y - stepY;
                    break;
            }

            //Dispatcher.BeginInvoke(TileVisibleArea);
            SilverlightAccess.Dispatcher.BeginInvoke(SetRegionBlackOutExtent);
        }

        public void Pan(double leftDelta, double topDelta)
        {
            Translate.X += leftDelta;
            Translate.Y += topDelta;
            SilverlightAccess.Dispatcher.BeginInvoke(TileVisibleArea);
            SilverlightAccess.Dispatcher.BeginInvoke(SetRegionBlackOutExtent);
        }

        private void ZoomToPoint(double zoom, Point pt)
        {
            //Point worldCanvasPt = TransGroup.Inverse.Transform(pt);
            //Debug.WriteLine("world Canvas Pt is {0}", worldCanvasPt);
            var x = (1 - zoom) * pt.X;
            var y = (1 - zoom) * pt.Y;
            var translationX = x * TransformGroup.Value.M11 + y * TransformGroup.Value.M12;
            var translationY = y * TransformGroup.Value.M22 + y * TransformGroup.Value.M21;

            //set some semi arbitrary limits to the scaling
            if (Scale.ScaleX < SilverlightConstants.MinZoomInFactor && zoom < 1) return;
            if (Scale.ScaleX > SilverlightConstants.MaxZoomInFactor && zoom > 1) return;
            var oldX = Translate.X;
            var oldY = Translate.Y;
            Translate.X += translationX;
            Translate.Y += translationY;

            Scale.ScaleX *= zoom;
            Scale.ScaleY *= zoom;
            SilverlightAccess.VisualAccess.OnePixelWidthOnWorldCanvas = 1 / Scale.ScaleX;

            SilverlightAccess.Dispatcher.BeginInvoke(SetRegionBlackOutExtent);
        }


        internal void TileVisibleArea()
        {
            if (CurrentExtent != null)
            {
                tileDisplayerViewModel.TileVisibleArea(CurrentExtent);
            }
        }
    }
}
