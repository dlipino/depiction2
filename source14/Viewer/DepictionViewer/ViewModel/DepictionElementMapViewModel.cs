﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DepictionSilverlight.Data;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.ViewModel
{
    public class DepictionElementMapViewModel : ViewModelBase 
    {
        private DepictionElement Element { get; set; }
        

        public DepictionElementMapViewModel(DepictionElement element)
        {
            Element = element;
            Element.PropertyChanged += Element_PropertyChanged;
            SilverlightAccess.VisualAccess.PropertyChanged += VisualAccess_PropertyChanged;
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IconVisibility"))
            {
                OnNotifyPropertyChanged("IconVisibility");
            }
        }

        void VisualAccess_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("OnePixelWidthOnWorldCanvas") && IconData != null)
            {
                OnNotifyPropertyChanged("IconData");
                UpdateRenderTransform(((VisualAccess)sender).OnePixelWidthOnWorldCanvas);
            }
            if (e.PropertyName.Equals("OnePixelWidthOnWorldCanvas"))
                StrokeThickness = 2 * ((VisualAccess) sender).OnePixelWidthOnWorldCanvas;
        }

        //pass throughs, seems kind of nasty
        public Visibility ImageVisibility
        {
            get { return Element.ImageVisibility; }
        }
        public Visibility IconVisibility
        {
            get { return Element.IconVisibility; }
        }
        public bool InMainCanvas
        {
            get { return Element.InMainCanvas; }
        }
        public Visibility Active
        {
            get { return Element.Active; }
        }

        //this one should probably be moved to the viewmodel
        public ImageSource IconSource
        {
            get { return Element.IconSource; }
        }

        public ImageMetadata ImageMetadata { get; set; }
        public PathGeometry PathGeometry { get; set; }
        public string HoverText { get { return Element.HoverText; } }
        //public string HoverText { get; set; }
        public string StrokeColor { get { return Element.StrokeColor; } }
        public string FillColor { get { return Element.FillColor; } }
        public Visibility PermaHoverTextOn { get { return Element.PermaHoverTextOn; } set { Element.PermaHoverTextOn = value; OnNotifyPropertyChanged("PermaHoverTextOn");}
        }

        public IconData IconData { get; set; }
        public string[] RevealerIds { get { return Element.RevealerIds; } }
        public double PermaTextX { get { return Element.PermaTextX; } set { Element.PermaTextX = value; OnNotifyPropertyChanged("PermaTextX"); } }
        public double PermaTextY { get { return Element.PermaTextY; } set { Element.PermaTextY = value; OnNotifyPropertyChanged("PermaTextY"); } }
        public double PermaTextCenterX { get { return Element.PermaTextCenterX; } set { Element.PermaTextCenterX = value; OnNotifyPropertyChanged("PermaTextCenterX"); } }
        public double PermaTextCenterY { get { return Element.PermaTextCenterY; } set { Element.PermaTextCenterY = value; OnNotifyPropertyChanged("PermaTextCenterY"); } }
        public SolidColorBrush IconBorderColor { get { return Element.BorderColor; } }
        public Geometry BorderGeometry 
        { 
            get
            {

                if (Element.BorderShape.Equals(DepictionIconBorderShape.Circle))
                {

                    var ellipseGeometry = new EllipseGeometry();
                    ellipseGeometry.Center = new Point(Element.IconSize / 2, Element.IconSize/2);
                    ellipseGeometry.RadiusX = ellipseGeometry.RadiusY = Element.IconSize / 2 + 2;
                    return ellipseGeometry;
                }
                if (Element.BorderShape.Equals(DepictionIconBorderShape.Square))
                {
                    var rectGeometry = new RectangleGeometry();
                    rectGeometry.Rect = new Rect(0, 0, Element.IconSize, Element.IconSize);
                    return rectGeometry;
                }
                if (Element.BorderShape.Equals(DepictionIconBorderShape.Triangle))
                {
                    var geometry = new PathGeometry();
                    var figure = new PathFigure();
                    double offset = 5;
                    figure.StartPoint = new Point(-offset, Element.IconSize);
                    var segment = new PolyLineSegment();
                    segment.Points.Add(new Point(Element.IconSize + offset, Element.IconSize));
                    segment.Points.Add(new Point((Element.IconSize) / 2, -(offset*2)));
                    segment.Points.Add(new Point(-offset, Element.IconSize));
                    figure.Segments.Add(segment);
                    geometry.Figures.Add(figure);
                    return geometry;
                }
                return null;
            }
        }
        //DepictionIconBorderShape
        private double strokeThickness;
        public double StrokeThickness
        {
            get { return strokeThickness; }
            set
            {
                strokeThickness = value;
                OnNotifyPropertyChanged("StrokeThickness");
            }
        }

        //private void NotifyPropertyChanged(string property)
        //{
        //    if (PropertyChanged != null)
        //        PropertyChanged(this, new PropertyChangedEventArgs(property));
        //}
        //public event PropertyChangedEventHandler PropertyChanged;

        public TransformGroup InverseTransform { get; set; }
        protected TranslateTransform TranslateTransform1 { get; set; }
        public ScaleTransform ScaleTransform { get; set; }
        public ScaleTransform InverseScaleTransform { get; set; }
        protected TranslateTransform TranslateTransform2 { get; set; }

        internal void UpdateRenderTransform(double inverseScale)
        {
            if (IconData == null) return;
            if (InverseTransform == null)
            {
                InverseTransform = new TransformGroup();
                TranslateTransform1 = new TranslateTransform();
                ScaleTransform = new ScaleTransform();
                InverseScaleTransform = new ScaleTransform();
                TranslateTransform2 = new TranslateTransform();
                InverseTransform.Children.Add(TranslateTransform1);
                InverseTransform.Children.Add(ScaleTransform);
                InverseTransform.Children.Add(TranslateTransform2);
            }
            TranslateTransform1.X = IconData.PixelLocation.X;
            TranslateTransform1.Y = IconData.PixelLocation.Y;
            ScaleTransform.ScaleX = inverseScale;
            ScaleTransform.ScaleY = inverseScale;
            InverseScaleTransform.ScaleX = inverseScale;
            InverseScaleTransform.ScaleY = inverseScale;
            ScaleTransform.CenterX = IconData.PixelLocation.X;
            ScaleTransform.CenterY = IconData.PixelLocation.Y;
            TranslateTransform2.X = Element.IconSize / -2;
            TranslateTransform2.Y = Element.IconSize / -2;
        }

    }
}