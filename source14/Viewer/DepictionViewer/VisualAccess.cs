﻿using System.ComponentModel;
using System.Windows;
using DepictionSilverlight.UI;

namespace DepictionSilverlight
{
    public class VisualAccess : INotifyPropertyChanged
    {

        private double onePixelWidthOnWorldCanvas;
        public double OnePixelWidthOnWorldCanvas
        {
            get { return onePixelWidthOnWorldCanvas; }
            set
            {
                onePixelWidthOnWorldCanvas = value;
                NotifyPropertyChanged("OnePixelWidthOnWorldCanvas");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
