﻿using System;
using System.Globalization;
using DepictionSilverlight.Data;

namespace DepictionSilverlight
{
    public class LatitudeLongitude
    {
        public const double INVALID_VALUE = 999;

        #region constructors

        public LatitudeLongitude() {}

        public LatitudeLongitude(string stringToParse)
        {
            string[] strings = stringToParse.Split(new [] { ' ', ',' });

            if (strings.Length != 2)
                throw new Exception("Wrong number of Lat/Lon Params");

            Longitude = Convert.ToDouble(strings[0]);
            Latitude = Convert.ToDouble(strings[1]);
        }

        public LatitudeLongitude(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public LatitudeLongitude(string latitude, string longitude)
        {
            double lat;
            double lon;

            if (!(double.TryParse(latitude, NumberStyles.Number, CultureInfo.InvariantCulture, out lat) && double.TryParse(longitude, NumberStyles.Number, CultureInfo.InvariantCulture, out lon)))
            {
                Latitude = INVALID_VALUE;
                Longitude = INVALID_VALUE;
            }
            else
            {
                Latitude = lat;
                Longitude = lon;
            }
        }

        #endregion

        #region properties

        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #endregion

        #region ToString

        private string EW
        {
            get { return (Longitude < 0) ? "W" : "E"; }
        }

        private string NS
        {
            get { return (Latitude < 0) ? "S" : "N"; }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            string separator = LatitudeLongitudeSeparatorString();
            switch (DepictionData.LatitudeLongitudeFormat)
            {
                case LatitudeLongitudeFormat.ColonFractionalSeconds:
                    return DoColonFractionalSeconds(separator);
                case LatitudeLongitudeFormat.ColonIntegerSeconds:
                    return DoColonIntegerSeconds(separator);
                case LatitudeLongitudeFormat.Decimal:
                    return DoDecimal(separator);
                case LatitudeLongitudeFormat.DegreesFractionalMinutes:
                    return DoDegreesFractionalMinutes(separator);
                case LatitudeLongitudeFormat.DegreesMinutesSeconds:
                    return DoDegreesMinutesSeconds(separator, "°");
                case LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds:
                    return DoDegreesMinutesSeconds(separator, "d");
                case LatitudeLongitudeFormat.SignedDecimal:
                    return DoSignedDecimal(separator);
                case LatitudeLongitudeFormat.SignedDegreesFractionalMinutes:
                    return DoSignedDegreesFractionalMinutes(separator);
                default:
                    throw new Exception("Unexpected value for LatitudeLongitudeFormat");
            }
        }

        private string DoSignedDegreesFractionalMinutes(string separator)
        {
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(Latitude, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(Longitude, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3:00}° {4:00.00000}", latDeg, latMin, separator, longDeg, longMin);
        }

        private string DoDegreesMinutesSeconds(string separator, string degreeSign)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}{1}{2:00}'{3:00}\"{4}{5}{6:00}{7}{8:00}'{9:00}\"{10}", latDeg, degreeSign, latMin, latSec, NS, separator, longDeg, degreeSign, longMin, longSec, EW);
        }

        private string DoDegreesFractionalMinutes(string separator)
        {
            //40° 26.77170N, 79° 55.93172W
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(Latitude, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(Longitude, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3}{4:00}° {5:00.00000}{6}", Math.Abs(latDeg), latMin, NS, separator, Math.Abs(longDeg), longMin, EW);
        }

        private string DoColonIntegerSeconds(string separator)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}:{1:00}:{2:00}{3}{4}{5:00}:{6:00}:{7:00}{8}", latDeg, latMin, latSec, NS, separator, longDeg, longMin, longSec, EW);
        }

        private string DoColonFractionalSeconds(string separator)
        {
            int latDeg;
            int latMin;
            double latSec;
            int longDeg;
            int longMin;
            double longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec, 3);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec, 3);
            return string.Format("{0:00}:{1:00}:{2:00.000}{3}{4}{5:00}:{6:00}:{7:00.000}{8}", latDeg, latMin, latSec, NS, separator, longDeg, longMin, longSec, EW);
        }

        private string DoSignedDecimal(string separator)
        {
            return string.Format("{0:F5}{1}{2:F5}", Latitude, separator, Longitude);
        }

        private string DoDecimal(string separator)
        {
            string northSouth = (Latitude < 0) ? "S" : "N";
            string eastWest = (Longitude < 0) ? "W" : "E";
            return string.Format("{0:00.00000}{1}{2}{3:00.00000}{4}", Math.Abs(Latitude), northSouth, separator, Math.Abs(Longitude), eastWest);
        }

        /// <summary>
        /// Degrees will have same sign as value.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutes(double value, out int deg, out double min, int decimals)
        {
            deg = (int) Math.Floor(value);
            double fraction = Math.Abs(value) - Math.Floor(Math.Abs(value));
            min = fraction*60;
            if (Math.Round(min, decimals) >= 60)
            {
                min = 0;
                deg += 1*Math.Sign(deg);
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out int sec)
        {
            double dSec;
            ExtractDegreesMinutesSeconds(value, out deg, out min, out dSec, 0);
            sec = (int) Math.Round(dSec);
            if (sec == 60)
            {
                min += 1;
                sec = 0;
            }
            if (min == 60)
            {
                deg += 1;
                min = 0;
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out double sec, int decimals)
        {
            deg = (int) Math.Floor(Math.Abs(value));
            double temp;
            if (deg != 0)
                temp = 60*(Math.Abs(value)%deg);
            else
                temp = 60*Math.Abs(value);
            min = (int) Math.Floor(temp);
            sec = (temp - min)*60;
            if (Math.Round(sec, decimals) >= 60)
            {
                sec = 0;
                min += 1;
            }
            if (min >= 60)
            {
                min = 0;
                deg += 1;
            }
        }

        private static string LatitudeLongitudeSeparatorString()
        {
            switch (DepictionData.LatitudeLongitudeSeparator)
            {
                case LatitudeLongitudeSeparator.CommaAndSpace:
                    return ", ";
                case LatitudeLongitudeSeparator.Comma:
                    return ",";
                case LatitudeLongitudeSeparator.Space:
                    return " ";
                default:
                    throw new Exception("Unexpected value for LatitudeLongitudeSeparator");
            }
        }

        #endregion
    }
}