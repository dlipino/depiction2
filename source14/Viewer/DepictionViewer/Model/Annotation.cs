using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace DepictionSilverlight.Data
{
    public class Annotation : INotifyPropertyChanged
    {
        #region fields
        private double contentLocationX = 20;
        private double contentLocationY = 25;
        private Visibility visibility;

        #endregion

        #region constructor

        public Annotation()
        {
            visibility = Visibility.Visible;
            Width = -1;
            SilverlightAccess.VisualAccess.PropertyChanged += VisualAccess_PropertyChanged;

        }
        
        #endregion

        void VisualAccess_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("OnePixelWidthOnWorldCanvas"))
            {
                BorderThickness = new Thickness(((VisualAccess) sender).OnePixelWidthOnWorldCanvas);
                SixStrokeThickness = ((VisualAccess)sender).OnePixelWidthOnWorldCanvas * 6;
                InverseScale = new ScaleTransform
                {
                    ScaleX = ((VisualAccess)sender).OnePixelWidthOnWorldCanvas,
                    ScaleY = ((VisualAccess)sender).OnePixelWidthOnWorldCanvas
                };
                
                NotifyPropertyChanged("IconData");
            }
        }



        #region properties
        
        private ScaleTransform inverseScale;
        public ScaleTransform InverseScale
        {
            get { return inverseScale; }
            set
            {
                inverseScale = value;
                NotifyPropertyChanged("InverseScale");
            }
        }

        private Point annotationCenter;
        public Point AnnotationCenter
        {
            get
            {
                return annotationCenter;
            }
            set
            {
                annotationCenter = value;
                NotifyPropertyChanged("AnnotationCenter");
            }
        }

        public double ContentLocationX
        {
            get { return contentLocationX; }
            set
            {
                contentLocationX = value;
                NotifyPropertyChanged("ContentLocationX");
            }
        }
        public double ContentLocationY
        {
            get { return contentLocationY; }
            set
            {
                contentLocationY = value;
                NotifyPropertyChanged("ContentLocationY");
            }
        }

        public Visibility Visibility
        {
            get { return visibility; }
            set
            {
                visibility = value;
                NotifyPropertyChanged("Visibility");
            }
        }

        public string AnnotationText { get; set; }
        public SolidColorBrush BackgroundColor { get; set; }
        public SolidColorBrush ForegroundColor { get; set; }
        public Point PixelLocation { get; set; }
        public double IconWidth { get; private set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public LatitudeLongitude TopLeft { get; set; }
        public bool AllowDragging { get; set; }
        public int ZIndex { get; set; }

        public LatitudeLongitude BottomRight { get; set; }
        public double OriginalFontSize { get; set; }
        public double OriginalPixelHeightLon { get; set; }
        public double FontSize { get; set; }
        public double OriginalIconWidth { get; set; }
        private Thickness borderThickness;
        public Thickness BorderThickness
        {
            get { return borderThickness; }
            set
            {
                borderThickness = value;
                NotifyPropertyChanged("StrokeThickness");
            }
        }
        private double sixStrokeThickness;
        public double SixStrokeThickness
        {
            get { return sixStrokeThickness; }
            set
            {
                sixStrokeThickness = value;
                NotifyPropertyChanged("SixStrokeThickness");
            }
        }
        #endregion

        #region methods

        private void ChangeExtents()
        {
            PixelLocation = SilverlightAccess.PixelConverter.WorldToGeoCanvas(TopLeft);
            //Point bottomRightPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(BottomRight);
            var leftPosPlusOnePx = new Point(PixelLocation.X, PixelLocation.Y + 1);
            LatitudeLongitude lpPlusOne = SilverlightAccess.PixelConverter.GeoCanvasToWorld(leftPosPlusOnePx);
            double scale = OriginalPixelHeightLon / (TopLeft.Latitude - lpPlusOne.Latitude);
            FontSize = OriginalFontSize * scale;
            IconWidth = OriginalIconWidth;
            AnnotationCenter = new Point(Width / 2 + 10, Height / 2 + 10);

            NotifyPropertyChanged("PixelLocation");
            NotifyPropertyChanged("FontSize");
            NotifyPropertyChanged("Width");
            NotifyPropertyChanged("Height");
            NotifyPropertyChanged("IconWidth");
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void GeoConversionsViewportChanged()
        {
            ChangeExtents();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}