﻿namespace DepictionSilverlight.Model
{
    public enum DepictionIconBorderShape
    {
        None, Circle, Triangle, Square
    }
}
