﻿using System.Windows;
using System.Windows.Media;

namespace DepictionSilverlight.Data
{
    public class ImageMetadata
    {
        public LatitudeLongitude TopLeftLatitudeLongitude { get; set; }
        public LatitudeLongitude BottomRightLatitudeLongitude { get; set; }
        public Point TopLeftPoint { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Rotation { get; set; }
        public RotateTransform Transform { get { return new RotateTransform { Angle = Rotation }; } }
    }
}