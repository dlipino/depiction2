﻿using System;
using System.Windows.Media;
using DepictionSilverlight.UI;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.Data
{
    public class LegendItem : ViewModelBase
    {
        #region properties

        public ImageSource IconSource { get; set; }
        public string IconRepresents { get; set; }
        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                OnNotifyPropertyChanged("IsChecked");
            }
        }

        public LegendItemType LegendItemType { get; set; }

        #endregion
    }
}