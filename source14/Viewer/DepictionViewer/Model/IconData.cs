﻿using System.Windows;

namespace DepictionSilverlight.Data
{
    public class IconData
    {
        public Point PixelLocation { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}