﻿using DepictionSilverlight.Data;
using SharpMap.Geometries;

namespace DepictionSilverlight.Model
{
    public class ZoneOfInfluence
    {
        public Geometry Geometry { get; set; }

        public GeometryType GeometryType
        {
            get
            {
                if (Geometry is LineString)
                    return GeometryType.LineString;
                if (Geometry is Point)
                    return GeometryType.Point;
                if (Geometry is MultiLineString)
                    return GeometryType.MultiLineString;
                if (Geometry is Polygon)
                    return GeometryType.Polygon;
                if (Geometry is MultiPolygon)
                    return GeometryType.MultiPolygon;
                return GeometryType.Point;
            }

        }
    }
}