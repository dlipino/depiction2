﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading;
using DepictionSilverlight.Tilers;

//using DepictionSilverlight.Tilers;

namespace DepictionSilverlight.Model
{
    class TileFetcher
    {
        #region Fields

        //private readonly MemoryCache<MemoryStream> memoryCache;
        private readonly ITiler tileSource;
        private BoundingBox extent;
        //private double resolution;
        private readonly IList<TileModel> tilesInProgress = new List<TileModel>();
        private IList<TileModel> missingTiles = new List<TileModel>();
        private readonly IDictionary<TileModel, int> retries = new Dictionary<TileModel, int>();
        private const int ThreadMax = 6;
        private int threadCount;
        private readonly AutoResetEvent waitHandle = new AutoResetEvent(true);
        //private readonly IFetchStrategy strategy = new FetchStrategy();
        private const int MaxRetries = 2;
        private Thread loopThread;
        private volatile bool isThreadRunning;
        private volatile bool isViewChanged;

        #endregion

        #region EventHandlers

        
        public event Action<TileModel> DataChanged;

        #endregion

        #region Constructors Destructors

        public TileFetcher(ITiler tileSource)
        {
            if (tileSource == null) throw new ArgumentException("TileProvider can not be null");
            this.tileSource = tileSource;

            //if (memoryCache == null) throw new ArgumentException("MemoryCache can not be null");
            //this.memoryCache = memoryCache;
        }

        #endregion

        #region Public Methods

        public void ViewChanged(BoundingBox newExtent)
        {
            extent = newExtent;
            //resolution = newResolution;
            isViewChanged = true;
            waitHandle.Set();
            if (!isThreadRunning) { StartThread(); }
        }

        private void StartThread()
        {
            isThreadRunning = true;
            loopThread = new Thread(TileFetchLoop);
            loopThread.IsBackground = true;
            loopThread.Name = "LoopThread";
            loopThread.Start();
        }

        public void AbortFetch()
        {
            isThreadRunning = false;
            waitHandle.Set();
        }

        #endregion

        #region Private Methods

        private void TileFetchLoop()
        {
            try
            {
#if !SILVERLIGHT //In Silverlight you can not specify a thread priority
                Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
#endif
                while (isThreadRunning)
                {
                    if (tileSource == null) waitHandle.Reset();

                    waitHandle.WaitOne();

                    if (isViewChanged && (tileSource != null))
                    {
                        missingTiles = tileSource.GetTiles(extent, Convert.ToInt32(SilverlightAccess.ApplicationWidth / 512), 18);
                        //missingTiles = strategy.GetTilesWanted(tileSource.Schema, extent.ToExtent(), level);
                        retries.Clear();
                        isViewChanged = false;
                    }

                    missingTiles = GetTilesMissing(missingTiles, retries);

                    FetchTiles();

                    if (missingTiles.Count == 0) { waitHandle.Reset(); }

                    if (threadCount >= ThreadMax) { waitHandle.Reset(); }
                }
            }
            finally
            {
                isThreadRunning = false;
            }
        }

        private static IList<TileModel> GetTilesMissing(IEnumerable<TileModel> infosIn, IDictionary<TileModel, int> retries)
        {
            IList<TileModel> tilesOut = new List<TileModel>();
            foreach (TileModel info in infosIn)
            {
                if ((!retries.Keys.Contains(info) || retries[info] < MaxRetries))

                    tilesOut.Add(info);
            }
            return tilesOut;
        }

        private void FetchTiles()
        {
            foreach (TileModel info in missingTiles)
            {
                if (threadCount >= ThreadMax) return;
                FetchTile(info);
            }
        }

        private void FetchTile(TileModel info)
        {
            //first a number of checks
            if (tilesInProgress.Contains(info)) return;
            if (retries.Keys.Contains(info) && retries[info] >= MaxRetries) return;
            //if (memoryCache.Find(info.Index) != null) return;

            //now we can go for the request.
            lock (tilesInProgress) { tilesInProgress.Add(info); }
            if (!retries.Keys.Contains(info)) retries.Add(info, 0);
            else retries[info]++;
            threadCount++;
            StartFetchOnThread(info);
        }

        private void StartFetchOnThread(TileModel info)
        {
            //var fetchOnThread = new FetchOnThread(tileSource.Provider, info, LocalFetchCompleted);
            //var thread = new Thread(fetchOnThread.FetchTile);
            var worker = new BackgroundWorker();
            worker.DoWork += WorkerOnDoWork;
            worker.RunWorkerAsync(info);
            worker.RunWorkerCompleted += WorkerOnRunWorkerCompleted;

//#if !SILVERLIGHT
//            //In Wpf we use Wpf's own feature rendering which can only be done on a STA thread.
//            thread.SetApartmentState(ApartmentState.STA);
//#endif
//            thread.Name = "Tile Fetcher";
//            thread.Start();
        }


        private void WorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            var tileModel = (TileModel) doWorkEventArgs.Argument;
            doWorkEventArgs.Result = tileModel;
            try
            {
                var image = RequestHelper.FetchImage(tileModel.TileUri);
                tileModel.Image = image; 
            }
            catch (WebException e)
            {
                
                
            }
            //var tileViewModel = new TileViewModel(tileModel);
            //var bitmapImage = new BitmapImage();
            //bitmapImage.SetSource(new MemoryStream(image));
            
            //tileViewModel.TileSource = bitmapImage;

            
        }

        private void WorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
            var tileModel = (TileModel) e.Result;
            //todo remove object sender
            //try
            //{
            //    if (e.Error == null && e.Cancelled == false && isThreadRunning && e.Result != null)
            //        memoryCache.Add(e.TileInfo.Index, new MemoryStream(e.Image));
            //}
            //catch (Exception ex)
            //{
            //    e.Error = ex;
            //}
            //finally
            //{
                threadCount--;
                lock (tilesInProgress)
                {
                    if (tilesInProgress.Contains(tileModel))
                        tilesInProgress.Remove(tileModel);
                }
                waitHandle.Set();
            //}

            if (tileModel.Image != null && DataChanged != null)
                DataChanged(tileModel);
        }

        #endregion
    }
}
