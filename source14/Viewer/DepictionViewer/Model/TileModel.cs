﻿using System;

namespace DepictionSilverlight.Model
{
    public class TileModel
    {
        /// <summary>
        /// Create a reference to one tile.
        /// </summary>
        /// <param name="tileZoomLevel">The zoom level at which the style exists, as determined by Depiction.</param>
        /// <param name="tileSource">The source of the tile.</param>
        /// <param name="topLeft">The latitude and longitude of the top left corner of this tile.</param>
        /// <param name="bottomRight">The latitude and longitude of the bottom right corner of this tile.</param>
        public TileModel(int tileZoomLevel, Uri tileUri, LatitudeLongitude topLeft, LatitudeLongitude bottomRight)
        {
            TileZoomLevel = tileZoomLevel;
            TileUri = tileUri;

            //TileSource = tileSource;
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        /// <summary>
        /// The zoom level at which the tile exists.
        /// </summary>
        public int TileZoomLevel { get; private set; }

        public Uri TileUri { get; set; }

        //public BitmapSource TileSource { get; set; }

        /// <summary>
        /// The file name containing this tile.
        /// </summary>
        public string TileKey { get; private set; }

        /// <summary>
        /// The full path to the file containing this tile, including the file name.
        /// </summary>
        public string TilePath { get; private set; }

        /// <summary>
        /// The latitude and longitude of the top left corner of this tile.
        /// </summary>
        public LatitudeLongitude TopLeft { get; private set; }

        /// <summary>
        /// The latitude and longitude of the bottom right corner of this tile.
        /// </summary>
        public LatitudeLongitude BottomRight { get; private set; }

        public byte[] Image { get; set; }

        public bool Equals(TileModel other)
        {
            return TopLeft.Equals(other.TopLeft) && TileZoomLevel.Equals(other.TileZoomLevel);
        }
    }
}