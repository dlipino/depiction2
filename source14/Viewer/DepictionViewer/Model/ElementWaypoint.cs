﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DepictionSilverlight.Data;

namespace DepictionSilverlight.Model
{
    public class ElementWaypoint : INotifyPropertyChanged
    {
        public LatitudeLongitude Location { get; set; }
        public ImageSource IconSource { get; set; }
        public double IconSize { get; set; }
        private Visibility iconVisibility;
        public Visibility IconVisibility
        {
            get
            {
                return iconVisibility;
            }
            set
            {
                iconVisibility = value;
                NotifyPropertyChanged("IconVisibility");
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
