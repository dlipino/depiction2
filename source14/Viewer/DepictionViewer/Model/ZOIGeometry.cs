﻿using System.Collections.Generic;
using DepictionSilverlight.Model;
using SharpMap.Geometries;

namespace DepictionSilverlight.Data
{
    //public class Polygon : IZOIGeometry
    //{
    //    public IList<LatitudeLongitude> Shell { get; set; }
    //    public IList<LatitudeLongitude[]> Holes { get; set; }
    //}

    //public class MultiPolygon : IZOIGeometry
    //{
    //    public IList<Polygon> Polygons { get; set; }
    //}

    //public class LineString : IZOIGeometry
    //{
    //    public IList<LatitudeLongitude> Points { get; set; }
    //}

    //public class MultiLineString : IZOIGeometry
    //{
    //    public IList<LineString> LineStrings;
    //}

    //public class Point : IZOIGeometry
    //{
    //    public LatitudeLongitude Coordinate;
    //}
    
    internal class ZOIGeometry
    {
        internal ZOIGeometry(Geometry geometry, GeometryType geometryType)
        {
            Geometry = geometry;
            GeometryType = geometryType;
        }

        internal Geometry Geometry { get; set; }
        internal GeometryType GeometryType { get; set; }
    }
}