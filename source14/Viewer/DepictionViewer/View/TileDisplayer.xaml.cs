﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.View
{
    public partial class TileDisplayer : ItemsControl
    {
        public TileDisplayer()
        {
            InitializeComponent();
        }
        
        private static void Animate(DependencyObject target, string property, double from, double to, int duration)
        {
            var animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new TimeSpan(0, 0, 0, 0, duration);
            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, new PropertyPath(property));

            var storyBoard = new Storyboard();
            storyBoard.Children.Add(animation);
            //storyBoard.Completed += completed;
            storyBoard.Begin();
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            var image = sender as Image;
            if (image == null) return;
            image.Opacity = 0;
            Animate(image, "Opacity", 0, 1, 600);
            var tileViewModel = image.DataContext as TileViewModel;
            if (tileViewModel == null) return;
            tileViewModel.ImageOpened = true;
        }


    }
}
