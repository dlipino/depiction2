﻿using System.Windows;
using System.Windows.Controls;

namespace DepictionSilverlight.UI
{
    public class DepictionContentControl : ContentControl
    {
        public new DependencyObject GetTemplateChild(string name)
        {
            return base.GetTemplateChild(name);
        }
    }
}