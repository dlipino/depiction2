﻿using System.Windows.Controls.Primitives;
using DepictionSilverlight.Data;
using System.Windows;

namespace DepictionSilverlight.UI
{
    public partial class AnnotationDisplayer
    {
        public AnnotationDisplayer()
        {
            InitializeComponent();
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var annotation = ((FrameworkElement) sender).DataContext as Annotation;

            if (annotation == null) return;
            annotation.Visibility = annotation.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Annotation_Drag(object sender, DragDeltaEventArgs e)
        {
            var t = sender as Thumb;
            if (t == null) return;
            var dc = t.DataContext as Annotation;
            if (dc == null) return;
            dc.ContentLocationX += e.HorizontalChange;
            dc.ContentLocationY += e.VerticalChange;
            dc.AnnotationCenter = new Point(dc.AnnotationCenter.X + e.HorizontalChange, dc.AnnotationCenter.Y + e.VerticalChange);
        }

        private void holdingGrid_Loaded(object sender, RoutedEventArgs e)
        {
            var t = sender as FrameworkElement;
            if (t == null) return;
            var dc = t.DataContext as Annotation;
            if (dc == null) return;
            dc.AnnotationCenter = new Point(dc.ContentLocationX + t.ActualWidth / 2, dc.ContentLocationY + t.ActualHeight / 2);
        }
    }
}