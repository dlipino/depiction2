﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DepictionSilverlight.Data;

namespace DepictionSilverlight.UI
{
    public partial class Legend
    {
        #region fields
        private readonly ImageSource InvisibleImageSource;
        private readonly ImageSource VisibleImageSource;

        private bool groupNodesNeedUpdating = true;
        #endregion

        #region constructor
        public Legend()
        {
            InitializeComponent();
            VisibleImageSource = new BitmapImage(new Uri("../Icons/Visible.png", UriKind.Relative));
            InvisibleImageSource = new BitmapImage(new Uri("../Icons/HideIcon2.png", UriKind.Relative));
            LegendButtonImageSource = VisibleImageSource;
            legendView.LayoutUpdated += ItemsPresenter_LayoutUpdated;
        }

        private void ItemsPresenter_LayoutUpdated(object sender, EventArgs e)
        {
            if (groupNodesNeedUpdating && legendView.Visibility.Equals(Visibility.Visible))
            {
                UpdateAllGroupParentCheckBoxes();
            }
        }

        #endregion constructor

        #region methods
        public void BuildLegend()
        {
            
            List<LegendItem> legendItems = new List<LegendItem>();
            if (DepictionData.Instance.LegendType.Equals(LegendType.Elements) || DepictionData.Instance.LegendType.Equals(LegendType.RevealersAndElements))
            {
                legendItems =
                    DepictionData.Instance.Elements.Where(e => e.ImageMetadata == null && e.IconSource != null).Select(
                        e => new LegendItem
                                 {
                                     IconRepresents = e.TypeDisplayName,
                                     IconSource = e.IconSource,
                                     IsChecked = true,
                                     LegendItemType = LegendItemType.Element,
                                 }).ToList();
            }
            if (DepictionData.Instance.LegendType.Equals(LegendType.Revealers) || DepictionData.Instance.LegendType.Equals(LegendType.RevealersAndElements))
            {
                var bitmapImage = new BitmapImage(new Uri("../Icons/HideIcon2.png", UriKind.Relative));
                legendItems.AddRange(DepictionData.Instance.Revealers.Select(
                    e => new LegendItem
                             {
                                 IconRepresents = e.RevealerName,
                                 IconSource = bitmapImage,
                                 IsChecked = e.Visibility == Visibility.Visible,
                                 LegendItemType = LegendItemType.Revealer,
                             }));
            }
            legendItems = legendItems.Distinct(new LegendItemEqualityComparer()).ToList();
            CollectionViewSource viewSource = new CollectionViewSource {Source = legendItems};
            viewSource.SortDescriptions.Add(new SortDescription("LegendItemType", ListSortDirection.Ascending));
            viewSource.SortDescriptions.Add(new SortDescription("IconRepresents", ListSortDirection.Ascending));
            viewSource.GroupDescriptions.Add(new PropertyGroupDescription("LegendItemType"));
            LegendVisibility = DepictionData.Instance.LegendType.Equals(LegendType.NoLegend) ? Visibility.Collapsed : Visibility.Visible;
            ItemsInLegend = viewSource.View;
        }


        private void showHideLegendClick(object sender, RoutedEventArgs e)
        {
            if (LegendContentVisibility.Equals(Visibility.Visible))
                LegendContentVisibility = Visibility.Collapsed;
            else
                LegendContentVisibility = Visibility.Visible;

            LegendButtonImageSource = LegendVisibility == Visibility.Visible ? InvisibleImageSource : VisibleImageSource;
        }

        private void Border_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.8;
        }

        private void Border_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.5;
        }
        #endregion

        #region properties
        public ICollectionView ItemsInLegend
        {
            get { return (ICollectionView)GetValue(LegendTypesProperty); }
            set { SetValue(LegendTypesProperty, value); }
        }

        public static readonly DependencyProperty LegendTypesProperty =
            DependencyProperty.Register("ItemsInLegend", typeof(ICollectionView), typeof(Legend), new PropertyMetadata(null));

        public ImageSource LegendButtonImageSource
        {
            get { return (ImageSource)GetValue(LegendButtonImageSourceProperty); }
            set { SetValue(LegendButtonImageSourceProperty, value); }
        }

        public static readonly DependencyProperty LegendButtonImageSourceProperty =
            DependencyProperty.Register("LegendButtonImageSource", typeof(ImageSource), typeof(Legend), new PropertyMetadata(null));

        public Visibility LegendContentVisibility
        {
            get { return (Visibility)GetValue(LegendContentVisibilityProperty); }
            set { SetValue(LegendContentVisibilityProperty, value); }
        }

        public static readonly DependencyProperty LegendContentVisibilityProperty =
            DependencyProperty.Register("LegendContentVisibility", typeof(Visibility), typeof(Legend), new PropertyMetadata(Visibility.Collapsed));

        public static readonly DependencyProperty LegendVisibilityProperty =
            DependencyProperty.Register("LegendVisibility", typeof (Visibility), typeof (Legend), new PropertyMetadata(Visibility.Collapsed));

        public Visibility LegendVisibility
        {
            get { return (Visibility) GetValue(LegendVisibilityProperty); }
            set { SetValue(LegendVisibilityProperty, value); }
        }


        #endregion

        #region Nested type: LegendItemEqualityComparer

        private class LegendItemEqualityComparer : IEqualityComparer<LegendItem>
        {
            #region IEqualityComparer<LegendItem> Members

            public bool Equals(LegendItem a, LegendItem b)
            {
                if (ReferenceEquals(null, b)) return false;
                if (ReferenceEquals(a, b)) return true;
                return Equals(a.IconSource, b.IconSource) && Equals(a.IconRepresents, b.IconRepresents);
            }

            public int GetHashCode(LegendItem legendItem)
            {
                return (legendItem.IconSource != null ? legendItem.IconSource.GetHashCode() : 0) + (legendItem.IconRepresents != null ? legendItem.IconRepresents.GetHashCode() : 0);
            }

            #endregion
        }

        #endregion

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var fe = sender as FrameworkElement;
            if (fe == null) return;

            var legendItem = fe.DataContext as LegendItem;
            if (legendItem == null) return;

            ChangeVisibility(legendItem, Visibility.Visible);
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var fe = sender as FrameworkElement;
            if (fe == null) return;

            var legendItem = fe.DataContext as LegendItem;
            if (legendItem == null) return;

            ChangeVisibility(legendItem, Visibility.Collapsed);
        }

        private void ChangeVisibility(LegendItem legendItem, Visibility visibility)
        {
            if (legendItem.LegendItemType.Equals(LegendItemType.Revealer))
            {
                var revealer = DepictionData.Instance.Revealers.FirstOrDefault(t => t.RevealerName.Equals(legendItem.IconRepresents));
                if (revealer != null)
                    revealer.Visibility = visibility;
            }
            else
            {
                var elements =
                    DepictionData.Instance.Elements.Where(t => t.TypeDisplayName.Equals(legendItem.IconRepresents));
                foreach (var element in elements)
                {
                    element.IconVisibility = visibility;
                }
            }
        }

        private void CheckNodeState(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (checkBox == null) return;
            var dataContext = checkBox.DataContext;
            var isChecked = (bool)checkBox.IsChecked;


            bool selected = false;
            var elementChanges = new HashSet<LegendItem>();
            if (dataContext is CollectionViewGroup)
            {
                var group = dataContext as CollectionViewGroup;

                foreach (LegendItem item in group.Items)
                {
                    if (item != null)
                    {
                        selected = (bool)isChecked;
                        item.IsChecked = selected;
                        elementChanges.Add(item);
                    }
                }
                UpdateAllGroupParentCheckBoxes();
            }
            else if (dataContext is LegendItem)
            {
                var elemVM = (LegendItem)dataContext;
                elemVM.IsChecked = (bool)isChecked;
                selected = elemVM.IsChecked;
                elementChanges.Add(elemVM);
                UpdateAllGroupParentCheckBoxes();
            }
            foreach (var item in elementChanges)
            {
                ChangeVisibility(item, isChecked ? Visibility.Visible : Visibility.Collapsed);
            }

            //var app = Application.Current as DepictionApplication;
            //if (app == null) return;
            //var currentDepiction = app.CurrentDepiction;
            //if (currentDepiction == null) return;
            //var mainDC = DataContext as ElementViewSelectionListViewModel;
            //if (mainDC == null) return;
            //var displayerToControl = mainDC.DisplayerToControl;
            //if (displayerToControl == null) return;
            //var elementModels = currentDepiction.GetElementsWithIds(elementChanges.ToList());
            //if (selected)
            //{
            //    displayerToControl.AddElementListToModel(elementModels);
            //}
            //else
            //{
            //    displayerToControl.RemoveElementListFromModel(elementModels);
            //}
        }
        public void UpdateAllGroupParentCheckBoxes()
        {
            groupNodesNeedUpdating = true;
            foreach (var singleItem in legendView.Items)
            {
                var item = singleItem as CollectionViewGroup;
                if (item == null) continue;
                CheckAndSetStateOfCollectionViewGroup(item);
            }
            groupNodesNeedUpdating = false;
            //var dc = DataContext as LegendItem;
            //if (dc == null) return;
            //dc.UpdateSelectedCount();
        }

        private void CheckAndSetStateOfCollectionViewGroup(CollectionViewGroup group)
        {
            TreeViewItem topNode = legendView.ItemContainerGenerator.ContainerFromItem(group) as TreeViewItem;
            if (group == null || topNode == null) return;

            bool? value = false;
            var firstChildSelection = false;
            //A linq call might work here var selectionTypes = group.Items.Distinct();

            for (int idx = 0; idx < group.Items.Count; ++idx)
            {
                var isChildSelected = ((LegendItem)group.Items[idx]).IsChecked;

                if (idx == 0)
                {
                    firstChildSelection = isChildSelected;
                    value = isChildSelected;
                }
                else if (firstChildSelection != isChildSelected)
                {
                    value = null;
                    break;
                }
            }
            topNode.Tag = value;
            groupNodesNeedUpdating = false;

        }
    }
    public enum LegendItemType
    {
        Revealer, Element
    }

    public enum LegendType
    {
        NoLegend, Revealers, Elements, RevealersAndElements
    }

}