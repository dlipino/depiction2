﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.View
{
    public partial class ElementIconItemsControlView
    {
        public ElementIconItemsControlView()
        {
            InitializeComponent();
            Canvas.SetZIndex(this, ViewModelZIndexs.BackdropIconDisplayerZ);
        }



        private DateTime? lastClick;
        private void element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var clickTime = DateTime.Now;
            if (lastClick != null)
            {
                var span = clickTime - (DateTime)lastClick;
                if (span.TotalMilliseconds < 300)
                {
                    var t = sender as FrameworkElement;
                    if (t == null) return;
                    var dc = t.DataContext as DepictionElementMapViewModel;
                    if (dc == null) return;

                    dc.PermaHoverTextOn = dc.PermaHoverTextOn == Visibility.Visible
                                              ? Visibility.Collapsed
                                              : Visibility.Visible;
                    lastClick = null;
                }
                lastClick = clickTime;
            }
            else
            {
                lastClick = clickTime;
            }

            

        }
    }
}
