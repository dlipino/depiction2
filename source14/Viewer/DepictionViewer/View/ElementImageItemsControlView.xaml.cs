﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using DepictionSilverlight.Data;

namespace DepictionSilverlight.UI
{
    public partial class ElementImageItemsControlView
    {
        public ElementImageItemsControlView()
        {
            InitializeComponent();
            Canvas.SetZIndex(this, ViewModelZIndexs.BackdropImageDisplayerZ);
        }

        //protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        //{
        //    base.PrepareContainerForItemOverride(element, item);
        //    var depictionElement = item as DepictionElement;
        //    var elementPresenter = element as ContentPresenter;

        //    if (depictionElement == null || elementPresenter == null) return;

        //    var binding = new Binding("ZIndex");
        //    elementPresenter.SetBinding(Canvas.ZIndexProperty, binding);

        //    if (depictionElement.ImageMetadata != null)
        //    {
        //        elementPresenter.ContentTemplate = (DataTemplate)Resources["ImageElement"];
        //    }
        //    else
        //    {
        //        if (item != null && element != null)
        //        {
        //            elementPresenter.ContentTemplate = (DataTemplate)Resources["IconAndZOIElement"];
        //        }
        //    }
        //}
    }
}
