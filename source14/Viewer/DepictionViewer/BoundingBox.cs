﻿using System.Windows;

namespace DepictionSilverlight
{
    public class BoundingBox
    {
        public BoundingBox(LatitudeLongitude topLeft, LatitudeLongitude bottomRight)
        {
            Left = topLeft.Longitude;
            Top = topLeft.Latitude;
            Right = bottomRight.Longitude;
            Bottom = bottomRight.Latitude;
        }

        public double Left { get; set; }
        public double Right { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }

        public LatitudeLongitude TopLeft
        {
            get
            {
                return new LatitudeLongitude
                {
                    Latitude = Top,
                    Longitude = Left
                };
            }
        }

        public LatitudeLongitude TopRight
        {
            get
            {
                return new LatitudeLongitude {
                    Latitude = Top,
                    Longitude = Right
                };
            }
        }

        /// <summary>
        /// Center of the bounding box; calculated with Lat = (Top + Bottom / 2), Lon = (Right + Left) /2
        /// </summary>
        public LatitudeLongitude Center
        {
            get { return new LatitudeLongitude(((Bottom + Top) / 2), ((Right + Left) / 2)); }
        }

        public LatitudeLongitude BottomLeft
        {
            get
            {
                return new LatitudeLongitude {
                    Latitude = Bottom,
                    Longitude = Left
                };
            }
        }

        public LatitudeLongitude BottomRight
        {
            get
            {
                return new LatitudeLongitude
                {
                    Latitude = Bottom,
                    Longitude = Right
                };
            }
        }

        private Size mapImageSize;
        public Size MapImageSize
        {
            get { return mapImageSize; }
            set { mapImageSize = value; }
        }

    }
}