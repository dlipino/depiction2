using System;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.Core.Entity//Grrr needed because of list serialization
//namespace Depiction.DPNPorting.SerializationObjects12
{
    public class AnnotationElement : IAnnotationElement12, IXmlSerializable
    {
        //        public event Action DialogRequested;
        #region fields

        private double onePixelWidthAtCreation;
        private string annotationText = "Enter annotation text here.";
        //        private SolidColorBrush backgroundColor;
        //        private Visibility collapsed = Visibility.Visible;
        //        private double currentOnePixelInWorldCanvasCoordinates;
        //        private SolidColorBrush foregroundColor;
        private Point pixelLocation;
        private double width = 200;
        private double height = 150;

        private const double BaseWidth = 200;
        private const double BaseHeight = 40;
        private const double BaseFontSize = 14;
        private const double BaseBorderThickness = 1;
        private const double BasePadding = 4;
        private const double BaseIconSize = 20;

        private double contentLocationX = 20;
        private double contentLocationY = 25;
        #endregion

        #region Properties

        public ILatitudeLongitude MapLocation { get; private set; }

        public double ContentLocationX
        {
            get { return contentLocationX; }
            set
            {
                contentLocationX = value;
            }
        }
        public double ContentLocationY
        {
            get { return contentLocationY; }
            set
            {
                contentLocationY = value;
            }
        }

        public string BackgroundColor{ get; set;}

        public string ForegroundColor { get; set; }

        public bool AllowDragging { get; set; }
        //        public void SetPixelLocationWithoutChangingZOI(Point newPixelLocation)
        //        {
        //            pixelLocation = newPixelLocation;
        //            NotifyPropertyChanged("PixelLocation");
        //        }
        public double PixelX { get { return pixelLocation.X; } }
        public double PixelY { get { return pixelLocation.Y; } }
        public Point PixelLocation
        {
            get { return pixelLocation; }
            set
            {
                pixelLocation = value;
            }
        }
        public int ZIndex { get; private set; }


        //        public Visibility Visibility { get; private set; }

        //        public Visibility Collapsed
        //        {
        //            get { return collapsed; }
        //            set
        //            {
        //                collapsed = value;
        //                NotifyPropertyChanged("Collapsed");
        //            }
        //        }

        public string AnnotationText
        {
            get { return annotationText; }
            set
            {
                annotationText = value;
            }
        }
        public double Width { get { return width; } }
        public double Height { get { return height; } }

        public double FontSize
        {
            get { return BaseFontSize; }// / PixelWidthAtCreation; }
        }

        public double Padding
        {
            get { return BasePadding; }// / PixelWidthAtCreation; }
        }
        public double IconSize
        {
            get { return BaseIconSize; }// / PixelWidthAtCreation; }
        }

        public double BorderThickness
        {
            get { return BaseBorderThickness; }// / currentOnePixelInWorldCanvasCoordinates; }
        }

        //        public double CurrentOnePixelInWorldCanvasCoordinates
        //        {
        //            get { return currentOnePixelInWorldCanvasCoordinates; }
        //            set
        //            {
        //                currentOnePixelInWorldCanvasCoordinates = value;
        //                if (currentOnePixelInWorldCanvasCoordinates > PixelWidthAtCreation * zoomInMax
        //                    || currentOnePixelInWorldCanvasCoordinates < (PixelWidthAtCreation / zoomOutMax))
        //                    Visibility = Visibility.Hidden;
        //                else
        //                    Visibility = Visibility.Visible;
        //
        //                NotifyPropertyChanged("Visibility");
        //                NotifyPropertyChanged("BorderThickness");
        //            }
        //        }

        //        public ImageSource IconSource
        //        {
        //            get
        //            {
        //                var d = Application.Current.Resources["Depiction.Notes"] as BitmapImage;
        //                return d;
        //            }
        //        }

        //        public SolidColorBrush BackgroundColor
        //        {
        //            get { return backgroundColor; }
        //            set
        //            {
        //                backgroundColor = value;
        //                backgroundColor.Freeze();
        //                NotifyPropertyChanged("BackgroundColor");
        //            }
        //        }

        //        public SolidColorBrush ForegroundColor
        //        {
        //            get { return foregroundColor; }
        //            set
        //            {
        //                foregroundColor = value;
        //                foregroundColor.Freeze();
        //                NotifyPropertyChanged("ForegroundColor");
        //            }
        //        }

        //        private bool annotationDialogVisible;
        //        public bool AnnotationDialogVisible
        //        {
        //            get { return annotationDialogVisible; }
        //            set
        //            {
        //                annotationDialogVisible = value;
        //
        //                if (DialogRequested != null)
        //                {
        //                    DialogRequested.Invoke();
        //                }
        //            }
        //        }

        public double PixelWidthAtCreation
        {
            get { return onePixelWidthAtCreation; }
        }

        public bool IsCollapsed { get; private set; }

        public string TypeDisplayName { get { return "Annotation"; } }
        #endregion

        #region constructors

        /// <summary>
        /// Parameterless construction for XML serialization.
        /// </summary>
        public AnnotationElement()
        {
            //            BackgroundColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f8eb8f"));
            //            ForegroundColor = Brushes.Black;
            //            ZIndex = ZIndexConstants.annotationsDisplayerZ;
            width = BaseWidth;
            height = BaseHeight;
            ContentLocationY = contentLocationY;
            ContentLocationX = contentLocationX;
            AllowDragging = true;
        }

        //        public AnnotationElement(double scaleAtCreation, int ZIndex)
        //            : this()
        //        {
        //            this.ZIndex = ZIndex;
        //            onePixelWidthAtCreation = scaleAtCreation;
        //            CurrentOnePixelInWorldCanvasCoordinates = scaleAtCreation;
        //        }
        //        public AnnotationElement(double scaleAtCreation)
        //            : this(scaleAtCreation, ZIndexConstants.annotationsDisplayerZ) { }

        #endregion

        #region methods

        public void AdjustWidth(double WorldCanvasPixelsToMove)
        {
            var oldWidth = width;
            width -= WorldCanvasPixelsToMove;
            if (width <= 15)
            {
                width = oldWidth;
            }
            //            NotifyPropertyChanged("Width");
        }
        public void AdjustHeight(double WorldCanvasPixelsToMove)
        {
            var oldHeight = height;
            height -= WorldCanvasPixelsToMove;

            if (height <= 15) height = oldHeight;
            if (height >= 300) height = oldHeight;
            //            NotifyPropertyChanged("Height");
        }

        //        public IAnnotationElement Copy(double newPixelAtCreation)
        //        {
        //            AnnotationElement newAnnot;
        //            if (newPixelAtCreation.Equals(double.NaN))
        //                newAnnot = new AnnotationElement(PixelWidthAtCreation);
        //            else
        //                newAnnot = new AnnotationElement(newPixelAtCreation);
        //
        //            newAnnot.BackgroundColor = BackgroundColor;
        //            newAnnot.ForegroundColor = ForegroundColor;
        //            newAnnot.PixelLocation = PixelLocation;
        //            newAnnot.AnnotationText = AnnotationText;
        //            newAnnot.AllowDragging = AllowDragging;
        //            newAnnot.ZIndex = ZIndex;
        //            newAnnot.Visibility = Visibility;
        //            newAnnot.Collapsed = Collapsed;
        //            newAnnot.width = Width;
        //            newAnnot.height = Height;
        //            return newAnnot;
        //        }

        //        public override bool Equals(object obj)
        //        {
        //            var aElement = obj as AnnotationElement;
        //            if (aElement == null) return false;
        //
        //            if (!AreBrushesEqual(BackgroundColor, aElement.BackgroundColor)) return false;
        //            if (!AreBrushesEqual(ForegroundColor, aElement.ForegroundColor)) return false;
        //
        //            return PixelWidthAtCreation == aElement.PixelWidthAtCreation
        //                && pixelLocation.Equals(aElement.pixelLocation)
        //                && annotationText == aElement.annotationText
        //                && AllowDragging == aElement.AllowDragging
        //                && width == aElement.width
        //                && height == aElement.height
        //                && currentOnePixelInWorldCanvasCoordinates == aElement.currentOnePixelInWorldCanvasCoordinates
        //                && ZIndex == aElement.ZIndex
        //                && Visibility == aElement.Visibility
        //                && collapsed == aElement.collapsed;
        //        }

        //        private static bool AreBrushesEqual(SolidColorBrush brush1, SolidColorBrush brush2)
        //        {
        //            if (brush1 == null)
        //            {
        //                if (brush2 != null) return false;
        //            }
        //            else
        //            {
        //                if (brush2 == null) return false;
        //                if (brush1.Color != brush2.Color) return false;
        //            }
        //            return true;
        //        }
        #endregion

        #region serialization

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            var latAttrib = reader.GetAttribute("Latitude");
            var longAttrib = reader.GetAttribute("Longitude");
            MapLocation = new LatitudeLongitude(latAttrib,longAttrib);// Helper12DeserializationClasses.PixelToGeoconverters.GeoMapToCanvasMapConverter.GeoCanvasToWorld(pixelLocation);

            reader.ReadStartElement();
            var mapDiv = Deserializers12.MapSizeMulti;

            onePixelWidthAtCreation = reader.ReadElementContentAsDouble("onePixelWidthAtCreation", ns);
            //Was inverse scale in the old one
            onePixelWidthAtCreation = 1 / (onePixelWidthAtCreation * mapDiv);
            //onePixelWidthAtCreation *= 100;
            pixelLocation = Point.Parse(reader.ReadElementContentAsString("pixelLocation", ns));
            var newPixelLocation = new Point(pixelLocation.X * mapDiv, pixelLocation.Y * mapDiv);
            pixelLocation = newPixelLocation;



            annotationText = reader.ReadElementContentAsString("annotationText", ns);
            AllowDragging = Boolean.Parse(reader.ReadElementContentAsString("allowDragging", ns));
            width = reader.ReadElementContentAsDouble("width", ns);
            //width /= onePixelWidthAtCreation;
            reader.ReadElementContentAsDouble("currentOnePixelInWorldCanvasCoordinates", ns);
            ZIndex = reader.ReadElementContentAsInt("ZIndex", ns);
            var visibility = (Visibility)Enum.Parse(typeof(Visibility), reader.ReadElementContentAsString("Visibility", ns));
            var collapsed = (Visibility)Enum.Parse(typeof(Visibility), reader.ReadElementContentAsString("collapsed", ns));
            IsCollapsed = false;
            if(collapsed.Equals(Visibility.Visible))
            {
                IsCollapsed = true;
            }
            BackgroundColor = reader.ReadElementContentAsString("backgroundColor", ns);
            ForegroundColor = reader.ReadElementContentAsString("foregroundColor", ns);

            //            CurrentOnePixelInWorldCanvasCoordinates = reader.ReadElementContentAsDouble("currentOnePixelInWorldCanvasCoordinates", ns);
            //            ZIndex = reader.ReadElementContentAsInt("ZIndex", ns);
            //            Visibility = (Visibility)Enum.Parse(typeof(Visibility), reader.ReadElementContentAsString("Visibility", ns));
            //            collapsed = (Visibility)Enum.Parse(typeof(Visibility), reader.ReadElementContentAsString("collapsed", ns));
            //            BackgroundColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString("backgroundColor", ns)));
            //            ForegroundColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString(reader.ReadElementContentAsString("foregroundColor", ns)));

            if (reader.Name.Equals("ContentLocationX"))
            {
                ContentLocationX = double.Parse(reader.ReadElementContentAsString("ContentLocationX", ns));
            }
            if (reader.Name.Equals("ContentLocationY"))
            {
                ContentLocationY = double.Parse(reader.ReadElementContentAsString("ContentLocationY", ns));
            }
            height = BaseHeight;
            if (reader.Name.Equals("height"))
            {
                height = reader.ReadElementContentAsDouble("height", ns);
                //height /= onePixelWidthAtCreation;
            }

            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            //            var ns = Constants.DepictionXmlNameSpace;
            //            var mapDiv = UIAccess.MapSizeMulti;
            //            var saveOnePix = 1 / (onePixelWidthAtCreation * mapDiv);
            //            writer.WriteElementString("onePixelWidthAtCreation", ns, saveOnePix.ToString());
            //
            //            var savePixLocation = new Point((pixelLocation.X + DepictionAccess.PixelConverter.Offsetx) / mapDiv,
            //                    (pixelLocation.Y + DepictionAccess.PixelConverter.Offsety) / mapDiv);
            //
            //
            //            writer.WriteElementString("pixelLocation", ns, savePixLocation.ToString());
            //            writer.WriteElementString("annotationText", ns, annotationText);
            //            writer.WriteElementString("allowDragging", ns, AllowDragging.ToString());
            //            writer.WriteElementString("width", ns, width.ToString());
            //            writer.WriteElementString("currentOnePixelInWorldCanvasCoordinates", ns, currentOnePixelInWorldCanvasCoordinates.ToString());
            //            writer.WriteElementString("ZIndex", ns, ZIndex.ToString());
            //            writer.WriteElementString("Visibility", ns, Visibility.ToString());
            //            writer.WriteElementString("collapsed", ns, collapsed.ToString());
            //            writer.WriteElementString("backgroundColor", ns, BackgroundColor.ToString());
            //            writer.WriteElementString("foregroundColor", ns, ForegroundColor.ToString());
            //            writer.WriteElementString("ContentLocationX", ns, ContentLocationX.ToString());
            //            writer.WriteElementString("ContentLocationY", ns, ContentLocationY.ToString());
            //
            //            writer.WriteElementString("height", ns, height.ToString());
        }

        #endregion
        public void CorrectPixelLocationForOffset()
        {
            //            if (DepictionAccess.PixelConverter != null)
            //                pixelLocation.Offset(-DepictionAccess.PixelConverter.Offsetx, -DepictionAccess.PixelConverter.Offsety);
        }
    }
}