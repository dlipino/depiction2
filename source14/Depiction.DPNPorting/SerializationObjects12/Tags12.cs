using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class Tags : ITags12,IXmlSerializable
    {
        private List<KeyValuePair<string, string>> restoredTags = new List<KeyValuePair<string, string>>();

        //private readonly Dictionary<string, List<IElement12>> tags = new Dictionary<string, List<IElement12>>();
//        private readonly Dictionary<string, List<string>> tagsWithIdsForPortingTo13 = new Dictionary<string, List<string>>();

        public HashSet<string> GetTagsForElementID(string id)
        {
            var tagList = new HashSet<string>();

            foreach (var keyValPair in restoredTags)
            {
                if(keyValPair.Key.Equals(id))
                {
                    tagList.Add(keyValPair.Value);
                }
            }
            return tagList;
        }

//        public void TagElement(IElement12 element, string tag)
//        {
//            List<string> tagList;
//
//            if (tagsWithIdsForPortingTo13.TryGetValue(tag, out tagList))
//            {
//                if (!tagList.Contains(element.ElementKey))
//                {
//                    tagList.Add(element.ElementKey);
//                }
//            }
//            else
//            {
//                tagList = new List<string> { element.ElementKey };
//                tagsWithIdsForPortingTo13.Add(tag, tagList);
//            }
//        }
        
        
//        public void RestoreElements(IEnumerable<IElementInternal12> elements)
//        {
//            var elementLookup = new Dictionary<string, IElementInternal12>();
//            var localRestoredTags = restoredTags;
//            restoredTags = null;
//            foreach (var element in elements)
//            {
//                elementLookup.Add(element.ElementKey, element);
//            }
//            tagsWithIdsForPortingTo13.Clear();
//            foreach (var elementKVPair in localRestoredTags)
//            {
//                IElementInternal12 element;
//                if (elementLookup.TryGetValue(elementKVPair.Key, out element))
//                {
//                    TagElement(element, elementKVPair.Value);
//                }
//            }
//        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            restoredTags = Deserializers12.Deserialize<List<KeyValuePair<string, string>>>("restoredTags", reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }


        
    }
}