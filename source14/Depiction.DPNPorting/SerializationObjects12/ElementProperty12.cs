using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.ExceptionHandling;
using Depiction.API.OldValidationRules;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.DPNPorting.Depiction12Deserializers.Helpers;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class Property : IElementProperty12, IXmlSerializable
    {
        public object Value { get; private set; }
        public object DefaultValue { get; private set; }
        public bool Deletable { get; private set; }

        public bool Editable { get; set; }
        public bool Visible { get;  set; }
        public Type PropertyType { get; private set; }
        public string Name { get; private set; }
        public string DisplayName { get; private set; }
        public int PropertyIndex { get; set; }
        public IElement12 Element { get; set; }
        public bool IsHoverText { get; set; }

        /// <summary>
        /// Gets the validation rules.
        /// </summary>
        /// <value>The validation rules.</value>
        public IValidationRule[] ValidationRules { get; private set;}

        public string StringValue { get; private set; }

        /// <summary>
        /// Gets the post set actions.
        /// </summary>
        /// <value>The post set actions.</value>
        public SerializableDictionary<string, string[]> PostSetActions{ get; private set;}


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Property"/> class.
        /// </summary>
        public Property()
        {
            Visible = true;
            Deletable = true;
          //  DeleteIfEditZoi = false;
            //PropertySource = ElementPropertySource.DML;//This needs to be changed when the element is created
            PropertyIndex = -1;//This needs to be changed when the property is added to the element
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Property"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="value">The value.</param>
        /// <param name="validationRules">The validation rules.</param>
        /// <param name="postSetActions">The post set actions.</param>
        /// <param name="deletable">Whether or not the user can delete this property</param>
        public Property(string name, string displayName, object value, IValidationRule[] validationRules, SerializableDictionary<string, string[]> postSetActions, bool deletable)
            : this()
        {
//            if (value == null)
//                throw new Exception(StringConstants.AttemptedToCreatePropertyWithNullValue);

            Visible = true;
            Editable = true;

            if (name == null)
                throw new Exception("Attempted to create a Property with a null name argument");
            if (name.Equals(string.Empty))
                throw new Exception("Attempted to create a Property with an empty name argument");
            Name = BaseElement.ValidInternalPropertyName(name);
            DisplayName = displayName;
            Deletable = deletable;
            PropertyType = value.GetType();
           ValidationRules = validationRules;
            PostSetActions = postSetActions;
            //Set(value, false, SetWeight.Add);
        }

        #endregion


        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;

            reader.ReadStartElement();
            Name = reader.ReadElementContentAsString("name", ns);
            DisplayName = reader.ReadElementContentAsString("DisplayName", ns);
            Deletable = reader.ReadElementContentAsBoolean("Deletable", ns);
            if (reader.Name.Equals("IsHoverText"))
                IsHoverText = reader.ReadElementContentAsBoolean("IsHoverText", ns);
            var DeleteIfEditZoi = reader.ReadElementContentAsBoolean("deleteIfEditZoi", ns);
            Visible = reader.ReadElementContentAsBoolean("Visible", ns);
            Editable = reader.ReadElementContentAsBoolean("Editable", ns);
//            SetWeight = (SetWeight)Enum.Parse(typeof(SetWeight), reader.ReadElementContentAsString("SetWeight", ns));
             reader.ReadElementContentAsString("SetWeight", ns);
            var typeName = reader.ReadElementContentAsString("propertyType", ns);
            PropertyType = Type.GetType(typeName);
            if(PropertyType == null) PropertyType = Deserializers12.FindTypeByString(typeName);
            
            if(PropertyType == null)
            {
                while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("Property"))
                {
                    reader.Read();
                }
                reader.ReadEndElement();
                throw new Exception(string.Format(
                        "Error Deserializing value for property: {0}. Cannot be null type", Name));
            }
            if(PropertyType == null) return;
            if (reader.ReadElementContentAsBoolean("postSetActions_Exist", ns))
                PostSetActions = Deserializers12.Deserialize<SerializableDictionary<string, string[]>>("postSetActions", reader);
            if (reader.ReadElementContentAsBoolean("validationRules_Exist", ns))
            {
                var validationRuleArray = Deserializers12.DeserializeHeterogeneousItemList<IValidationRule>("validationRules", "rule", reader);
                var validationRuleList = new List<IValidationRule>(validationRuleArray);
                ValidationRules = validationRuleList.ToArray();
            }
            if (!reader.ReadElementContentAsBoolean("value_IsNull", ns))
            {
                if (PropertyType != null && ((typeof(IXmlSerializable)).IsAssignableFrom(PropertyType) || PropertyType.IsSerializable))
                {
                    Value = Deserializers12.DeserializeObject("value", PropertyType, reader);
                    if (reader.ReadElementContentAsBoolean("defaultValue_IsSameAsValue", ns))
                        DefaultValue = Value;
                    else
                        DefaultValue = Deserializers12.DeserializeObject("defaultValue", PropertyType, reader);
                }
                else
                {
                    Value = reader.ReadElementContentAsString("value", ns);
                    Value = DepictionTypeConverter12.ChangeType(Value, PropertyType);
                    DefaultValue = reader.ReadElementContentAsString("defaultValue", ns);
                    DefaultValue = DepictionTypeConverter12.ChangeType(DefaultValue, PropertyType);
                }
                if (Value == null)
                {
                    throw new Exception(string.Format(
                                               "Error Deserializing value for property: {0}. Cannot change type", Name));
                   
                    
                }
            }
            if (reader.Name.Equals("PropertySource"))
            {
                reader.ReadElementContentAsString("PropertySource", ns);
//                PropertySource = (ElementPropertySource)Enum.Parse(typeof(ElementPropertySource), reader.ReadElementContentAsString("PropertySource", ns));
            }
            if (!PropertyType.ToString().Equals(Value.ToString()))//A better way to do this is to see if a propertype converter (to string) exists.
            {
                StringValue = DepictionTypeConverter12.ChangeType(Value, typeof (string)).ToString();
            }
            
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}