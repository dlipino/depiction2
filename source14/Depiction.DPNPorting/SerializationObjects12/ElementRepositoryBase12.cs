using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;


namespace Depiction.DPNPorting.SerializationObjects12
{
    [KnownType(typeof(ElementRepository))]
    [KnownType(typeof(UnregisteredElementRepository))]
    public abstract class ElementRepositoryBase :  IElementRepository12, IXmlSerializable
    {
        protected List<IElementInternal12> observableElementsInRepository = new List<IElementInternal12>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ElementRepositoryBase"/> class.
        /// </summary>
        protected ElementRepositoryBase()
        {
//            observableElementsInRepository.CollectionChanged += observableElementsInRepository_CollectionChanged;
            Tags = new Tags();
        }

        #region IElementRepository Members

        /// <summary>
        /// A readonly collection of the Objects in the repository.
        /// This is the preferred method of getting the objects for two reasons:  
        ///     It is more performant than using the ToArray implementation ( the ElementsInRepository property)
        ///     It also is dynamic, meaning you can save a reference to this collection and as new objects are added later, the new objects are available;
        /// </summary>
        public ReadOnlyCollection<IElementInternal12> AllElementsInternal
        {
            get
            {
                return new ReadOnlyCollection<IElementInternal12>(observableElementsInRepository);
            }
        }

        public ReadOnlyCollection<IElement12> AllElements
        {
            get
            {
                return new ReadOnlyCollection<IElement12>(observableElementsInRepository.OfType<IElement12>().ToList());
            }
        }

        public ITags12 Tags { get; protected set; }

//        public void AddElementList(List<IElement12> elements, string sourceName, SourceType sourceType, bool autoAdd)
//        {
//            try
//            {
//                DisableNotifications();
//                foreach (var element in elements)
//                {
//                    Add(element, sourceName, sourceType, autoAdd);
//                    Thread.Sleep(1);
//                }
//            }
//            finally
//            {
//                EnableNotifications();
//            }
//            // Update interactions after all elements are in the repository.
//            foreach (var element in elements)
//            {
//                if (element.TypeDisplayName.ToLower().Contains("route"))
//                {
//                    var displayName = "DisplayName";
//                    if (element.HasProperty(displayName))
//                    {
//                        var prop = element.GetProperty(displayName) as Property;
//                        if (prop != null)
//                        {
//                            prop.Set(prop.Value, true, SetWeight.User, false);
//                        }
//                    }
//                }
//                element.UpdateInteractions();
//            }
//        }
//
//        public void Add(IElement element, string sourceName, SourceType sourceType)
//        {
//            Add(element, sourceName, sourceType, true);
//        }

//        public abstract void Add(IElement12 element, string sourceName, SourceType sourceType, bool autoAdd);

        public void Clear()
        {
            observableElementsInRepository.Clear();
        }

//        public void FireCollectionChangedEvent(NotifyCollectionChangedEventArgs args)
//        {
//            // Make sure this is on the foreground thread because groups get updated here, etc.
//            if (DepictionAccess.ForegroundThreadDispatcher != null && !DepictionAccess.ForegroundThreadDispatcher.CheckAccess())
//            {
//                DepictionAccess.ForegroundThreadDispatcher.Invoke(DispatcherPriority.Normal, new Action<NotifyCollectionChangedEventArgs>(FireCollectionChangedEvent), args);
//                return;
//            }
//            if (CollectionChanged != null) CollectionChanged(observableElementsInRepository, args);
//        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

//        protected void observableElementsInRepository_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            if (CollectionChanged != null)
//            {
//                if (CommonData.Story != null)
//                    CommonData.Story.NeedSaving = true;
//                CollectionChanged(observableElementsInRepository, e);
//            }
//        }
//
//        protected void WireupEvents(IElementInternal element)
//        {
//            element.OnDeleted += Element_OnElementDeleted;
//        }
//
//        protected void Element_OnElementDeleted(IDeleteable iElement)
//        {
//            var element = iElement as IElementInternal;
//
//            observableElementsInRepository.Remove(element);
//            if (element != null) element.UpdateInteractions();
//            if (CommonData.Story != null)
//                CommonData.Story.NeedSaving = true;
//        }
//
//        public static string CreateSourceTag(string sourceName, SourceType sourceType)
//        {
//            return string.Format("source [{0}]: {1}", sourceType.ToString().ToLower(), sourceName);
//        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var element in observableElementsInRepository)
            {
                sb.Append(element.ElementType + Environment.NewLine);
            }

            return sb.ToString();
        }

//        public abstract event DisplayNewElementDelegate DisplayNewElementEvent;
        public XmlSchema GetSchema()
        {
            return null;
        }

//        public List<IElement> GetAllElementsAndChildren()
//        {
//            var elementList = new List<IElement>();
//            foreach (var element in observableElementsInRepository)
//            {
//                RecursivelyAddElementAndChildrenToList(elementList, element);
//            }
//            return elementList;
//        }
//
//        public void DisableNotifications()
//        {
//            observableElementsInRepository.DisableNotifications();
//        }
//
//        public void EnableNotifications()
//        {
//            observableElementsInRepository.EnableNotifications();
//        }

//        private static void RecursivelyAddElementAndChildrenToList(ICollection<IElement> elementList, IElement element)
//        {
//            foreach (IElementInternal child in element.Children)
//                RecursivelyAddElementAndChildrenToList(elementList, child);
//            elementList.Add(element);
//        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            observableElementsInRepository.Clear();

            // Check for, and discard duplicate elements that somehow got saved to the depiction.
            var keyElementPairs = new Dictionary<string, IElementInternal12>();
            var duplicateElements = new HashSet<IElementInternal12>();
           //var elementList = Deserializers12.DeserializeItemList<IElementInternal12>("ElementList", typeof(BaseElement), reader);
            var ns = Deserializers12.DepictionXmlNameSpace;
            var elementList = new List<BaseElement>();

            //Return empty list if the node type is empty (davidl). would it be better to return a null?
//            if (reader.NodeType.Equals(XmlNodeType.EndElement)) return elementList;
            try
            {
                bool listIsEmpty = reader.IsEmptyElement;
                reader.ReadStartElement("ElementList", ns);
                if (!listIsEmpty)
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        var ret = new BaseElement();
                        ((IXmlSerializable)ret).ReadXml(reader);
                        elementList.Add(ret);
                        reader.MoveToContent();
                    }
                    reader.ReadEndElement();
                }
            }
            catch {  }


            foreach (var element in elementList)
            {
                if (keyElementPairs.ContainsKey(element.ElementKey))
                    duplicateElements.Add(element);
                else
                    keyElementPairs.Add(element.ElementKey, element);
            }
            if (duplicateElements.Count > 0)
            {
                var builder = new StringBuilder();
                builder.AppendLine(string.Format("The following {0} elements are duplicates in the {1} file and have been dropped:",
                    duplicateElements.Count, StoryPersistence12.OldDepictionName));
                foreach (var element in duplicateElements)
                {
                    builder.AppendLine(string.Format("{0} with ElementKey={1}", element.ElementType, element.ElementKey));
                }
                //DepictionAccess.NotificationService.WriteToLog(builder.ToString(), new Exception(string.Format("Duplicate elements found in {0} file", UIAccess._productInformation.StoryName)));
#if (DEBUG)
                throw new Exception(string.Format("Duplicate elements found in {0} file", StoryPersistence12.OldDepictionName));
#endif
            }
            foreach (var keyValuePair in keyElementPairs)
            {
                observableElementsInRepository.Add(keyValuePair.Value);
//                WireupEvents(keyValuePair.Value);
            }
            if (reader.Name.Equals("Tags"))
            {
                Tags = Deserializers12.Deserialize<Tags>("Tags", reader);
            }
//            Tags.RestoreElements(observableElementsInRepository);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            // TODO: remove duplicate elements
//            SerializationService.SerializeItemList("ElementList", typeof(BaseElement), observableElementsInRepository, writer);
//            SerializationService.Serialize("Tags", Tags, writer);

        }
    }
}