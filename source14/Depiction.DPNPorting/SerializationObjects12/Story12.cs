using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Core.Entity;
using Depiction.DPNPorting.Interfaces122;
using Depiction.DPNPorting.SerializationObjects12.DataTypes12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    /// <summary>
    /// This is the main container for the story
    /// </summary>
    public sealed class Story :  IStory12, IXmlSerializable
    {
        #region Variables

        private static List<string> regionCodes = new List<string>();
        private IElementRepository12 elementRepository;
        private IElementRepository12 unregisteredElementRepository;

        #endregion

        #region Properties

        public IDepictionMetadata12 Metadata { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Story"/> class.
        /// </summary>
        public Story()
        {
            Metadata = new DepictionMetadata
                           {
                               Author = "User",
                               Description = "No description.",
                               SaveDateTime = DateTime.Now,
                               Title = "Default"
                           };

            Annotations = new List<IAnnotationElement12>();
//            BitmapResources = new RasterImageResourceDictionary();
//            VectorResources = new ResourceDictionary();
//            PrototypeRepository = new PrototypeRepository();
//            LoadVectorResources();

//            elementRepository = new ElementRepository();

//            unregisteredElementRepository = new UnregisteredElementRepository();

//            InteractionRuleRepository = new InteractionRuleRepository();
        }

        #endregion

        #region Properties

        private IBoundingBox12 regionExtent;

        public string[] RegionCodes
        {
            get { return regionCodes.Distinct().ToArray(); }
        }

        public string CurrentStoryName { get; set; }

        /// <summary>
        /// Gets the element repository.
        /// </summary>
        public IElementRepository12 ElementRepository
        {
            get { return elementRepository; }
            set { elementRepository = value; }
        }

        /// <summary>
        /// Gets the unregistered element repository.
        /// </summary>
        public IElementRepository12 UnregisteredElementRepository
        {
            get { return unregisteredElementRepository; }
            set { unregisteredElementRepository = value; }
        }

        public List<IAnnotationElement12> Annotations { get; private set; }
//        public RasterImageResourceDictionary bitmapResources;
//        public ResourceDictionary BitmapResources
//        {
//            get { return bitmapResources; }
//            set
//            {
//                if ((value != null && value is RasterImageResourceDictionary) || value == null)
//                    bitmapResources = value as RasterImageResourceDictionary;
//                else
//                    throw new Exception("BitmapResources must be of type RasterImageResourceDictionary");
//            }
//        }
//
//        public ResourceDictionary VectorResources { get; private set; }
        public bool NeedSaving { get; set; }

        /// <summary>
        /// Gets or sets the region extent.
        /// </summary>
        /// <value>The region extent.</value>
        public IBoundingBox12 RegionExtent
        {
            get { return regionExtent; }
            set
            {
                regionExtent = value;
//                NotifyPropertyChanged("HasRegion");
            }
        }

        public bool HasRegion
        {
            get { return null != RegionExtent; }
        }

        public ResourceDictionary BitmapResources { get; set; }

        /// <summary>
        /// Delegates to the Tags object of the element repository.
        /// </summary>
        public ITags12 Tags
        {
            get { return ElementRepository.Tags; }
        }

//        public IInteractionRuleRepository InteractionRuleRepository { get; set; }


        #endregion

//        #region events handlers
//
//        private void elementRepository_PropertyChanged(object sender, PropertyChangedEventArgs e)
//        {
//            NeedSaving = true;
//        }
//
//        private void story_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
//        {
//            NeedSaving = true;
//        }
//
//        #endregion

        #region methods

//        public void LoadVectorResources()
//        {
//            const string dictionaryName = "VectorElementIcons";
//            const string assemblyName = "Depiction.Resources";
//            const string resourcePath = assemblyName + ";Component/ElementIcons/" + dictionaryName + ".xaml";
//
//            var uri = new Uri(resourcePath, UriKind.Relative);
//            var vectorResourceDictionary = Application.LoadComponent(uri) as ResourceDictionary;
//            if (vectorResourceDictionary == null) return;
//
//            VectorResources.Clear();
//
//            foreach (DictionaryEntry de in vectorResourceDictionary)
//            {
//                VectorResources.Add(de.Key, de.Value);
//            }
//            //Easiest way of doing this without having to mess up the whole save load thing. davidl
//            uri = new Uri("Depiction.Resources;Component/ElementIcons/BitmapElementIcons.xaml", UriKind.Relative);
//            var bitmaps = Application.LoadComponent(uri) as ResourceDictionary;
//            if (bitmaps == null) return;
//            foreach (DictionaryEntry entry in bitmaps)
//            {
//                var bitmapSource = (ImageSource)entry.Value;
//                VectorResources.Add(entry.Key, bitmapSource);
//            }
//        }

        public void ClearStory()
        {
            regionCodes.Clear();
        }

//        #region Adding Elements
//
//        public void AddAndMergeElementsToRepositories(ImportedElements importedElements, string sourceName, SourceType sourceType, bool autoAdd)
//        {
//            var markUpdated = false;
//            if (sourceType.ToString().ToLower().Equals("email"))
//            {
//                markUpdated = true;
//            }
//            importedElements.GeocodedElements = ElementUpdater.UpdateEIDElements(this, importedElements.GeocodedElements, importedElements.ExtraElementDataList, markUpdated);
//            importedElements.UngeocodedElements = ElementUpdater.UpdateEIDElements(this, importedElements.UngeocodedElements, importedElements.ExtraElementDataList);
//            AddElementsToRepositories(importedElements, sourceName, sourceType, autoAdd);
//        }
//
//        private void AddElementsToRepositories(ImportedElements importedElements, string sourceName, SourceType sourceType, bool autoAdd)
//        {
//            ElementRepository.AddElementList(importedElements.GeocodedElements, sourceName, sourceType, autoAdd);
//            UnregisteredElementRepository.AddElementList(importedElements.UngeocodedElements, sourceName, sourceType, autoAdd);
//            if (importedElements.UngeocodedElements.Count > 0)
//                DepictionAccess.NotificationService.SendNotification(
//                    String.Format("Could not geocode {0} of {1} elements imported from {2}. To geo-align these, go to the \"elements to geo-align\" tab of Manage Content.",
//                                  importedElements.UngeocodedElements.Count, importedElements.Count, sourceName),
//                    30);
//        }
//
//        #endregion

        public static void AddRegionCodes(string[] regionCodesToAdd)
        {
            foreach (var regionCode in regionCodesToAdd)
            {
                if (!regionCodes.Contains(regionCode))
                    regionCodes.Add(regionCode);
            }
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            //The metadata is currently saved in StoryPersistence to keep it out of the Story file. (davidl)
            // only save to xml the non-repository stuff.  Repositories are saved to their own subtrees.
            reader.ReadStartElement();
            regionCodes = new List<string>(Deserializers12.Deserialize<string[]>("RegionCodes", reader));
            var annotationsList = Deserializers12.Deserialize<List<AnnotationElement>>("annotations", reader);
            Annotations = annotationsList.OfType<IAnnotationElement12>().ToList();//new List<IAnnotationElement12>(annotationsList);
            RegionExtent = Deserializers12.Deserialize<BoundingBox>("regionExtent", reader);
//            if (RegionExtent == null)
//                throw new DepictionDeserializationException(string.Format("{0} area was not saved", UIAccess._productInformation.StoryName));

            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            Deserializers12.Serialize("RegionCodes", RegionCodes, writer);
            Deserializers12.Serialize("annotations", Annotations.ToList(), writer);
            Deserializers12.Serialize("regionExtent", RegionExtent, writer);
        }

//        #region IDepiction methods
//
//        public IElement GetFirstElementByTag(string tag)
//        {
//            return Tags.GetFirstElementByTag(tag);
//        }
//
//        public IElement[] GetElementsByType(string elementType)
//        {
//            return ElementRepository.AllElements.Where(e => e.ElementType == elementType).ToArray();
//        }
//
//        #endregion
//
//        /// <summary>
//        /// Gets or sets the PrototypeRepository.
//        /// </summary>
//        /// <value>The PrototypeRepository.</value>
//        public IPrototypeRepository PrototypeRepository { get; set; }
    }
}