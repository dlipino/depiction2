namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Volume : BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.Imperial:
                case MeasurementSystemAndScale.ImperialLarge:
                    return "gallons";
                case MeasurementSystemAndScale.Metric:
                case MeasurementSystemAndScale.MetricLarge:
                    return "liters";
                case MeasurementSystemAndScale.ImperialSmall:
                    return "ounces";
                case MeasurementSystemAndScale.MetricSmall:
                    return "milliliters";
                default:
                    return "";
            }
        }
    }
}