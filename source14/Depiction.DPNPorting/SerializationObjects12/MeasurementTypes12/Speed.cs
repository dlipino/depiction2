namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Speed:BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.Imperial:
                case MeasurementSystemAndScale.ImperialLarge:
                    return "mph";
                case MeasurementSystemAndScale.Metric:
                case MeasurementSystemAndScale.MetricLarge:
                    return "kph";
                case MeasurementSystemAndScale.ImperialSmall:
                    return "ft/s";
                case MeasurementSystemAndScale.MetricSmall:
                    return "m/s";
                default:
                    return "";
            }
        }
        
    }
}