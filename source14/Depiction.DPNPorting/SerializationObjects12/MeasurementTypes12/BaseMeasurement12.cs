using System;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public enum MeasurementSystemAndScale
    {
        /// <summary>
        /// Medium-sized values in the Imperial measurement system.
        /// </summary>
        Imperial = 0,

        /// <summary>
        /// Small-sized values in the Imperial measurement system.
        /// </summary>
        ImperialSmall = 1,

        /// <summary>
        /// Medium-sized values in the Metric measurement system.
        /// </summary>
        Metric = 2,

        /// <summary>
        /// Small-sized values in the Metric measurement system.
        /// </summary>
        MetricSmall = 3,

        /// <summary>
        /// Large-sized values in the Imperial measurement system.
        /// </summary>
        ImperialLarge = 4,

        /// <summary>
        /// Large-sized values in the Metric measurement system.
        /// </summary>
        MetricLarge = 5
    }

    /// <summary>
    /// The base class from which to create new types for Depiction element properties.
    /// Each type represents a measurement system (e.g. Imperial or Metric), and a size for
    /// the range of values, and a unit (e.g. mph or kph).
    /// </summary>
    public abstract class BaseMeasurement : IXmlSerializable// IFormattable//IMeasurement, 
    {
//        protected abstract double[,] ConversionFactors { get; }
//        protected abstract double[,] OffsetFactors { get; }
        protected MeasurementSystemAndScale internalSystemAndScale;
        protected double value;
        protected bool useInternalSystemAndScale;

        public double GetValue()
        {
            return value;
        }
        #region Serialization
        XmlSchema IXmlSerializable.GetSchema()
        {
            throw new NotImplementedException();
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            internalSystemAndScale = (MeasurementSystemAndScale) Enum.Parse(typeof (MeasurementSystemAndScale), reader.ReadElementContentAsString("internalSystem", ns));
            value = reader.ReadElementContentAsDouble("value", ns);
            reader.ReadEndElement();
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            writer.WriteElementString("internalSystem", ns, internalSystemAndScale.ToString());
            writer.WriteElementString("value", ns, value.ToString());
        }
        #endregion
        #region Methods

        public abstract string GetUnits(MeasurementSystemAndScale systemAndScale);
        protected MeasurementSystemAndScale GetSystem(MeasurementSystemAndScale systemAndScale)
        {
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.Imperial:
                    switch (internalSystemAndScale)
                    {
                        case MeasurementSystemAndScale.Imperial:
                        case MeasurementSystemAndScale.Metric:
                            return MeasurementSystemAndScale.Imperial;
                        case MeasurementSystemAndScale.ImperialSmall:
                        case MeasurementSystemAndScale.MetricSmall:
                            return MeasurementSystemAndScale.ImperialSmall;
                        case MeasurementSystemAndScale.ImperialLarge:
                        case MeasurementSystemAndScale.MetricLarge:
                            return MeasurementSystemAndScale.ImperialLarge;
                    }
                    break;
                case MeasurementSystemAndScale.Metric:
                    switch (internalSystemAndScale)
                    {
                        case MeasurementSystemAndScale.Imperial:
                        case MeasurementSystemAndScale.Metric:
                            return MeasurementSystemAndScale.Metric;
                        case MeasurementSystemAndScale.ImperialSmall:
                        case MeasurementSystemAndScale.MetricSmall:
                            return MeasurementSystemAndScale.MetricSmall;
                        case MeasurementSystemAndScale.ImperialLarge:
                        case MeasurementSystemAndScale.MetricLarge:
                            return MeasurementSystemAndScale.MetricLarge;
                    }
                    break;
            }
            return systemAndScale;
        }
        #endregion
        public override string ToString()
        {
            var systemAndScale = internalSystemAndScale;
            return string.Format("{0} {1}", GetValue(), GetUnits(systemAndScale));

        }
    }
}