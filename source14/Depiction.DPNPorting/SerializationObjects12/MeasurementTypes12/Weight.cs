namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Weight : BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.ImperialLarge:
                    return "tons";
                case MeasurementSystemAndScale.MetricLarge:
                    return "metric tons";
                case MeasurementSystemAndScale.Imperial:
                case MeasurementSystemAndScale.ImperialSmall:
                    return "pounds";
                case MeasurementSystemAndScale.Metric:
                case MeasurementSystemAndScale.MetricSmall:
                    return "kg";
                default:
                    return "";
            }
        }
    }
}