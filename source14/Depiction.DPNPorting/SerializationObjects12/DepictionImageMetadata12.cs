using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    /// <summary>
    /// ImageObjectMetadata class
    /// </summary>
    public class ImageObjectMetadata : IImageObjectMetadata12,IXmlSerializable
    {
        #region fields

        private string imageFilename = String.Empty;

        #endregion

        #region properties

        /// <summary>
        /// Gets or sets the full path.
        /// </summary>
        /// <value>The full path.</value>
        public string ImageFilename
        {
            get { return imageFilename; }
            set { imageFilename = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is geo referenced.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is geo referenced; otherwise, <c>false</c>.
        /// </value>
        public bool IsGeoReferenced { get; set; }


        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>The rotation.</value>
        public double Rotation { get; set; }

        /// <summary>
        /// Gets or sets the retrieved from.
        /// </summary>
        /// <value>The retrieved from.</value>
        public string RetrievedFrom { get; set; }

        /// <summary>
        /// Gets or sets the top left latitude.
        /// </summary>
        /// <value>The top left latitude.</value>
        public double TopLeftLatitude { get; set; }

        /// <summary>
        /// Gets or sets the top left longitude.
        /// </summary>
        /// <value>The top left longitude.</value>
        public double TopLeftLongitude { get; set; }

        /// Gets or sets the bottom right latitude.
        /// </summary>
        /// <value>The bottom right latitude.</value>
        public double BottomRightLatitude { get; set; }

        /// <summary>
        /// Gets or sets the bottom right longitude.
        /// </summary>
        /// <value>The bottom right longitude.</value>
        public double BottomRightLongitude { get; set; }

        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageObjectMetadata"/> class.
        /// </summary>
        public ImageObjectMetadata()
        {
            RetrievedFrom = String.Empty;
            imageFilename = String.Empty;
        }

        /// <summary>
        /// Internally sets the Catagory of the Imageinfo passed on retrieved from.  Mappoint == map anything else gets satellite
        /// </summary>
//        public ImageObjectMetadata(string path, LatitudeLongitude topLeftPos, LatitudeLongitude bottomRightPos, string retrievedFrom)
//        {
//            imageFilename = path;
//            IsGeoReferenced = true;
//            TopLeftLatitude = topLeftPos.Latitude;
//            TopLeftLongitude = topLeftPos.Longitude;
//            BottomRightLatitude = bottomRightPos.Latitude;
//            BottomRightLongitude = bottomRightPos.Longitude;
//            RetrievedFrom = retrievedFrom;
//        }

        #endregion

//        #region methods
//
//        /// <summary>
//        /// Copies the image object.
//        /// </summary>
//        /// <param name="inValue">The in value.</param>
//        /// <returns></returns>
//        public static DepictionImageMetadata12 Clone(DepictionImageMetadata12 inValue)
//        {
//            var copy = new ImageObjectMetadata();
//
//            copy.IsGeoReferenced = inValue.IsGeoReferenced;
//            copy.TopLeftLatitude = inValue.TopLeftLatitude;
//            copy.TopLeftLongitude = inValue.TopLeftLongitude;
//            copy.BottomRightLatitude = inValue.BottomRightLatitude;
//            copy.BottomRightLongitude = inValue.BottomRightLongitude;
//            copy.RetrievedFrom = inValue.RetrievedFrom;
//            copy.Rotation = inValue.Rotation;
//            copy.ImageFilename = inValue.ImageFilename;
//            return copy;
//        }
//
//        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            IsGeoReferenced = reader.ReadElementContentAsBoolean("IsGeoReferenced", ns);//ok so this wasn't always stored correctly
            TopLeftLatitude = reader.ReadElementContentAsDouble("TopLeftLatitude", ns);
            TopLeftLongitude = reader.ReadElementContentAsDouble("TopLeftLongitude", ns);
            BottomRightLatitude = reader.ReadElementContentAsDouble("BottomRightLatitude", ns);
            BottomRightLongitude = reader.ReadElementContentAsDouble("BottomRightLongitude", ns);
            Rotation = reader.ReadElementContentAsDouble("Rotation", ns);
            RetrievedFrom = reader.ReadElementContentAsString("RetrievedFrom", ns);
            ImageFilename = reader.ReadElementContentAsString("ImageFilename", ns);
            reader.ReadEndElement();
            if(TopLeftLongitude==BottomRightLatitude)
            {
                //Assume not geo alightned
                TopLeftLatitude = double.NaN;
                TopLeftLongitude = double.NaN;
                BottomRightLatitude = double.NaN;
                BottomRightLongitude = double.NaN;
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            writer.WriteElementString("IsGeoReferenced", ns, IsGeoReferenced.ToString().ToLower());
            writer.WriteElementString("TopLeftLatitude", ns, TopLeftLatitude.ToString());
            writer.WriteElementString("TopLeftLongitude", ns, TopLeftLongitude.ToString());
            writer.WriteElementString("BottomRightLatitude", ns, BottomRightLatitude.ToString());
            writer.WriteElementString("BottomRightLongitude", ns, BottomRightLongitude.ToString());
            writer.WriteElementString("Rotation", ns, Rotation.ToString());
            writer.WriteElementString("RetrievedFrom", ns, RetrievedFrom);
            writer.WriteElementString("ImageFilename", ns, ImageFilename);
        }
    }
}