using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class StorySubtreeInfo : IXmlSerializable
    {

        private string schemaResourcePath;
        private string dataPath;
        private string xmlFileName;
        private string loaderTypeName;
        public StorySubtreeInfo(string dataPath, string loaderTypeName, string schemaResourcePath, string xmlFileName)
        {
            this.schemaResourcePath = schemaResourcePath;
            this.dataPath = dataPath;
            this.xmlFileName = xmlFileName;
            this.loaderTypeName = loaderTypeName;
        }

        public StorySubtreeInfo()
        { }

        public string LoaderTypeName
        {
            get { return loaderTypeName; }
        }

        public string XmlFileName
        {
            get { return xmlFileName; }
        }

        public string DataPath
        {
            get { return dataPath; }
        }

        public string SchemaResourcePath
        {
            get { return schemaResourcePath; }
        }


        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            schemaResourcePath = reader.ReadElementContentAsString("schemaResourcePath", ns);
            dataPath = reader.ReadElementContentAsString("dataPath", ns);
            xmlFileName = reader.ReadElementContentAsString("xmlFileName", ns);
            loaderTypeName = reader.ReadElementContentAsString("loaderTypeName", ns);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            writer.WriteElementString("schemaResourcePath", ns, schemaResourcePath);
            writer.WriteElementString("dataPath", ns, dataPath);
            writer.WriteElementString("xmlFileName", ns, xmlFileName);
            writer.WriteElementString("loaderTypeName", ns, loaderTypeName);
        }
    }
}