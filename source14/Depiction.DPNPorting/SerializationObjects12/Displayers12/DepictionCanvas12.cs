using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.Displayers12
{
    public class DepictionCanvas : IElementDisplayer12, IXmlSerializable
    {
        protected List<string> restoredElements;
        protected List<string> elementsThatNeedLabelsShownOnLoad;
        public string FriendlyName { get; set; }
        public List<string> ElementIds { get { return restoredElements; } }
        public void AddElements(IEnumerable<IElement12> elements)
        {
            throw new NotImplementedException();
        }

        public void RemoveElements(IEnumerable<IElement12> elements)
        {
            throw new NotImplementedException();
        }

        public void RemoveManuallyAddedElements()
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<IElement12> GetElements()
        {
            throw new NotImplementedException();
        }

        public void RestoreElements(IEnumerable<IElement12> elements)
        {
            throw new NotImplementedException();
        }
        protected virtual void InternalReadXml(XmlReader reader) { }
        protected virtual void InternalWriteXml(XmlWriter writer) { }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        virtual public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            InternalReadXml(reader);
            FriendlyName = reader.ReadElementContentAsString("FriendlyName", ns);
            var elements = Deserializers12.DeserializeItemList<string>("manuallyAddedElements", typeof(string), reader);
            restoredElements = new List<string>(elements);

            var displayedLabels = Deserializers12.DeserializeItemList<string>("displayedLabels", typeof(string), reader);
            elementsThatNeedLabelsShownOnLoad = new List<string>(displayedLabels);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}