using System.Collections.Generic;
using System.Windows;
using System.Xml;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.Displayers12
{
    public class DepictionRevealerCanvas : DepictionCanvas, IRevealer12
    {
        public IMapCoordinateBounds RevealerGeoBounds { get; set; }
        public bool RectShape { get; set; }
        public bool Moveable { get; set; }
        public bool RevealerVisible { get; set; }
        public bool BorderVisible { get; set; }
        public bool ButtonsAndInfoVisible { get; set; }
        public double Opacity { get; set; }
        public Point Center { get; private set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Units { get; private set; }
        public double MinContentLocationX { get; set; }
        public double MinContentLocationY { get; set; }
        public bool PermaTextVisible { get; set; }

        override public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            var boundStringForm = reader.GetAttribute("GeoBounds");
            if (!string.IsNullOrEmpty(boundStringForm))
            {
                var split = boundStringForm.Split(' ');
                if (split.Length == 4)
                {
                    var geoTopLeft = new LatitudeLongitude(split[0], split[1]);
                    var geoBottomRight = new LatitudeLongitude(split[2], split[3]);
                    RevealerGeoBounds = new MapCoordinateBounds(geoTopLeft, geoBottomRight);
                }
            }
            reader.ReadStartElement();
            InternalReadXml(reader);
            FriendlyName = reader.ReadElementContentAsString("FriendlyName", ns);
            var elements = Deserializers12.DeserializeItemList<string>("manuallyAddedElements", typeof(string), reader);
            restoredElements = new List<string>(elements);

            var displayedLabels = Deserializers12.DeserializeItemList<string>("displayedLabels", typeof(string), reader);
            elementsThatNeedLabelsShownOnLoad = new List<string>(displayedLabels);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        protected override void InternalReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
           
            Opacity = reader.ReadElementContentAsDouble("revealerOpacity", ns);
            var instanceNumber = reader.ReadElementContentAsInt("instanceNumber", ns);
            var revealerId = reader.ReadElementContentAsString("revealerId", ns);
            Moveable = reader.ReadElementContentAsBoolean("moveable", ns);
            BorderVisible = reader.ReadElementContentAsBoolean("borderVisible", ns);
           var  borderHighlighted = reader.ReadElementContentAsBoolean("borderHighlighted", ns);
           RevealerVisible = reader.ReadElementContentAsBoolean("revealerVisible", ns);

            var revealerExtent = Rect.Parse(reader.ReadElementContentAsString("revealerExtent", ns));
            //Nasty stuff to make old depiction revealers load right with the new depiction canvas size.
            var mapDiv = Deserializers12.MapSizeMulti;
            var topLeft = new Point(revealerExtent.TopLeft.X * mapDiv, revealerExtent.TopLeft.Y * mapDiv);
            var newSize = new Size(revealerExtent.Width * mapDiv, revealerExtent.Height * mapDiv);
            var extent = new Rect(topLeft, newSize);
            //The pixel extent and loction, needs to be converted to latlong system
            revealerExtent = extent;

            RectShape = reader.ReadElementContentAsBoolean("rectShape", ns);
            ButtonsAndInfoVisible = reader.ReadElementContentAsBoolean("buttonsVisible", ns);
//            if (nextRevealerNumber <= instanceNumber)
//                nextRevealerNumber = instanceNumber + 1;
            if (reader.Name.Equals("MinContentLocationX"))
            {
                MinContentLocationX = double.Parse(reader.ReadElementContentAsString("MinContentLocationX", ns));
            }
            if (reader.Name.Equals("MinContentLocationY"))
            {
                MinContentLocationY = double.Parse(reader.ReadElementContentAsString("MinContentLocationY", ns));
            }
            if (reader.Name.Equals("PermaTextVisible"))
            {
                PermaTextVisible = bool.Parse(reader.ReadElementContentAsString("PermaTextVisible", ns));
            }
//            WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(revealer.ClipController.Rect.TopLeft);

        }

    }
}