using System;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.DataTypes12
{
    public class LatitudeLongitude : IXmlSerializable
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public override string ToString()
        {
            return Latitude.ToString("R", CultureInfo.InvariantCulture) + "," +
                   Longitude.ToString("R", CultureInfo.InvariantCulture);
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var nameSpace = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();

            Latitude = reader.ReadElementContentAsDouble("Latitude", nameSpace);
            Longitude = reader.ReadElementContentAsDouble("Longitude", nameSpace);

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var nameSpace = Deserializers12.DepictionXmlNameSpace;
            writer.WriteElementString("Latitude", nameSpace, Latitude.ToString("R"));
            writer.WriteElementString("Longitude", nameSpace, Longitude.ToString("R"));
        }
    }
}