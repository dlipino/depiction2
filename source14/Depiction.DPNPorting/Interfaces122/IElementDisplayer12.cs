using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace Depiction.DPNPorting.Interfaces122
{
    public interface IElementDisplayer12 
    {
        string FriendlyName { get; set; }
        List<string> ElementIds { get; }
        // void AddElementVisuals(IEnumerable<IElement> elements);//Add to canvas but no interactions
        //void RemoveElementVisuals(IEnumerable<IElement> elements);
        void AddElements(IEnumerable<IElement12> elements);
        void RemoveElements(IEnumerable<IElement12> elements);
        void RemoveManuallyAddedElements();
        ReadOnlyCollection<IElement12> GetElements();
//        bool ContainsElement(IElement element);
        void RestoreElements(IEnumerable<IElement12> elements);
//        void AttachToElementRepository();
//        void DetachFromElementRepository();
//        void AddAnnotation(IAnnotationElement annotation);
//        void DeleteAnnotation(IAnnotationElement annotation);
    }
}