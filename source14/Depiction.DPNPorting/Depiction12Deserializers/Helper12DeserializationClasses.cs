using System.Collections.Generic;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.DPNPorting.Interfaces122;
using Depiction.DPNPorting.SerializationObjects12.DataTypes12;
using LatitudeLongitude=Depiction.API.ValueTypes.LatitudeLongitude;
//using LatitudeLongitude12=Depiction.DPNPorting.SerializationObjects12.DataTypes12.LatitudeLongitude;

namespace Depiction.DPNPorting.Depiction12Deserializers
{
    public class Helper12DeserializationClasses
    {
        public interface ISubtreeLoader
        {
            void LoadSubtree(PersistenceMemento12 memento, SubtreeInfo12 subtreeInfo);
            //void SaveSubtree(PersistenceMemento12 memento, SubtreeInfo12 subtreeInfo);
        }
//        public class PixelToGeoconverters
//        {
//            static public MapCoordinateBounds WorldLatLongBoundingBox = new MapCoordinateBounds(new LatitudeLongitude(85, -180), new LatitudeLongitude(-85, 180));
//            public static MercatorCoordinateConverterFor122Conversion GeoMapToCanvasMapConverter = null;
//            public PixelToGeoconverters(ILatitudeLongitude startLocation)
//            {
//                var worldBounds = WorldLatLongBoundingBox;
//                var viewExtent = new WindowViewportExtent(worldBounds, Deserializers12.MapSize, Deserializers12.MapSize);
//
//                var geoConverter = new MercatorCoordinateConverterFor122Conversion(viewExtent, 0, 0);
//                var pixCenter = geoConverter.WorldToGeoCanvas(startLocation);
//                //Not sure if the converter should be in the model
//                GeoMapToCanvasMapConverter = new MercatorCoordinateConverterFor122Conversion(viewExtent, pixCenter.X, pixCenter.Y);
//               
//            }
//        }
        public class SubtreeInfo12
        {
            public string SchemaResourcePath { get; set; }
            public string SchemaResourceAssemblyName { get; set; }
            public string DataPath { get; set; }
            public string XMLFileName { get; set; }
        }
        public class PersistenceMemento12
        {
            public IVisualData VisualData { get; set; }
            public IStory12 Story { get; set; }
            public IElementPrototypeLibrary InherentPrototypes { get; set; }
//            public ElementTupleList ElementRulePairs { get; set; }
        }
        public interface IVisualData
        {
            List<IRevealer12> RevealerCollection { get; set; }
            IElementDisplayer12 PrimaryDisplayer { get; set; }
//            double ScaleX { get; set; }
//            double ScaleY { get; set; }
//            double TransX { get; set; }
//            double TransY { get; set; }
//            double OnePixelWidthInWorldCanvas { get; set; }
//            double ZoomFactor { get; set; }
            IGeoExtent ViewportExtent { get; set; }
        }
        public interface IGeoExtent
        {
//            ILatitudeLongitude NorthWest { get; }
//            ILatitudeLongitude SouthWest { get; }
//            ILatitudeLongitude SouthEast { get; }
//            ILatitudeLongitude NorthEast { get; }
            IMapCoordinateBounds GeoBounds { get; }
//            LatitudeLongitude Center { get; }
//            /// <summary>
//            /// Gets the width.
//            /// </summary>
//            /// <value>The width.</value>
//            double Width { get; }
//            double Height { get; }
//
//            double WidthInPixels { get; set; }
//            double HeightInPixels { get; set; }
        }
    }
}