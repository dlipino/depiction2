using System;
using System.IO;
using System.Reflection;
using Depiction.API.InteractionEngine;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public abstract class BaseSubtreeLoader : Helper12DeserializationClasses.ISubtreeLoader
    {

        public void LoadSubtree(Helper12DeserializationClasses.PersistenceMemento12 memento, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            var fullFilename = Path.Combine(subtreeInfo.DataPath, subtreeInfo.XMLFileName);
            if (!File.Exists(fullFilename)) return;
            using (var xmlStream = new FileStream(fullFilename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                if (!string.IsNullOrEmpty(subtreeInfo.SchemaResourcePath))
                {
                    const string assyName = "Depiction.DPNPorting";
                    var assy = Assembly.Load(new AssemblyName(assyName));
                    using (Stream schemaStream = assy.GetManifestResourceStream(subtreeInfo.SchemaResourcePath))
                    {
                        if (null != schemaStream)
                        {
                            var result = SchemaValidator.ValidateXmlStream(xmlStream, schemaStream);
                            if (!result.IsValid)
                                throw new Exception(string.Format("Schema validation error: {0}", result.EventArgs.Message));
                        }
                    }
                }

                xmlStream.Position = 0;
                Load(memento, xmlStream, subtreeInfo);
            }
        }

        protected abstract void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo);

    }
}