using System.IO;
using Depiction.DPNPorting.SerializationObjects12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public class RasterImagesSubtreeLoader : BaseSubtreeLoader
    {
        #region ISubtreeLoader Members

        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            ((Story)memento.Story).BitmapResources = Deserializers12.DeserializeFromFolder<RasterImageResourceDictionary>("RasterImages", stream, subtreeInfo.DataPath);
        }

        #endregion
    }
}