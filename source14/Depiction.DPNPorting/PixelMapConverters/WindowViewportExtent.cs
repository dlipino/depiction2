using System;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.Interfaces.MapVisualConverter;
using Depiction.APINew.ValueTypes;

namespace Depiction.DPNPorting.PixelMapConverters
{
    public class WindowViewportExtent : IGeoAndPixelExtent
    {
        private IMapCoordinateBounds viewPortBoundingBox;
        private double heightInPixels;
        private double widthInPixels;

        public WindowViewportExtent(){}

        public WindowViewportExtent(IMapCoordinateBounds rect, double widthInPixels, double heightInPixels)
        {
            SetExtent(rect, widthInPixels, heightInPixels);
        }

        public WindowViewportExtent(ILatitudeLongitude center, double widthLon, double heightLat, double widthInPixels, double heightInPixels)
        {
            var topLeft = new LatitudeLongitude(center.Latitude + heightLat / 2,
                                                center.Longitude - widthLon/2);
            var bottomRight = new LatitudeLongitude(topLeft.Latitude - heightLat,
                                                    topLeft.Longitude + widthLon);
            var rect = new MapCoordinateBounds(topLeft, bottomRight);
            SetExtent(rect, widthInPixels, heightInPixels);
        }

        #region IGeoExtent Members

        public double WidthInPixels
        {
            get { return widthInPixels; }
            set { widthInPixels = value; }
        }

        public double HeightInPixels
        {
            get { return heightInPixels; }
            set { heightInPixels = value; }
        }

        public ILatitudeLongitude Center
        {
            get { return new LatitudeLongitude(viewPortBoundingBox.Center.Latitude, viewPortBoundingBox.Center.Longitude); }
        }

        public ILatitudeLongitude NorthWest
        {
            get { return viewPortBoundingBox.TopLeft as LatitudeLongitude; }
        }

        public ILatitudeLongitude SouthWest
        {
            get { return viewPortBoundingBox.BottomLeft; }
        }

        public ILatitudeLongitude SouthEast
        {
            get { return viewPortBoundingBox.BottomRight as LatitudeLongitude; }
        }

        public ILatitudeLongitude NorthEast
        {
            get { return viewPortBoundingBox.TopRight; }
        }

        public double Height
        {
            get { return Math.Abs(viewPortBoundingBox.Top - viewPortBoundingBox.Bottom); }
        }

        public double Width
        {
            get { return Math.Abs(viewPortBoundingBox.Right - viewPortBoundingBox.Left); }
            
        }

        #endregion

        private void SetExtent(IMapCoordinateBounds rect, double newWidthInPixels, double newHeightInPixels)
        {
            if (newWidthInPixels == 0)
                throw new Exception("Attempt to create WindowViewportExtent with zero width");
            if (newHeightInPixels == 0)
                throw new Exception("Attempt to create WindowViewportExtent with zero height");
            viewPortBoundingBox = rect;
            heightInPixels = newHeightInPixels;
            widthInPixels = newWidthInPixels;
        }
    }
}