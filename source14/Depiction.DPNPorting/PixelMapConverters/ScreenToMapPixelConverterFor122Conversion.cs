using System.Windows;
using System.Windows.Media;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.ValueTypes;

namespace Depiction.DPNPorting.PixelMapConverters
{
    public class ScreenToMapPixelConverterFor122Conversion : IGeoConverter
    {
        private readonly IGeoConverter pixelConverter;
        private readonly Transform transform;

        public ScreenToMapPixelConverterFor122Conversion(IGeoConverter pConverter, Transform tForm)
        {
            transform = tForm;
            pixelConverter = pConverter;
        }

        public Point WorldToGeoCanvas(ILatitudeLongitude latLon)
        {
            Point pnt = pixelConverter.WorldToGeoCanvas(latLon);
            return transform.Transform(pnt);
        }

        public Rect WorldToGeoCanvas(IMapCoordinateBounds mapBounds)
        {
            var topLeftGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Top, mapBounds.Left));
            var bottomRightGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Bottom, mapBounds.Right));
            return new Rect(topLeftGeo, bottomRightGeo);
        }

        public ILatitudeLongitude GeoCanvasToWorld(Point p)
        {
            Point trPnt = transform.Inverse.Transform(p);
            return pixelConverter.GeoCanvasToWorld(trPnt);
        }

        public IMapCoordinateBounds GeoCanvasToWorld(Rect rect)
        {
            var topLeftGeo = GeoCanvasToWorld(rect.TopLeft);
            var bottomRightGeo = GeoCanvasToWorld(rect.BottomRight);
            return new MapCoordinateBounds(topLeftGeo, bottomRightGeo);
        }

        public double Offsetx
        {
            get { return pixelConverter.Offsetx; }
        }

        public double Offsety
        {
            get { return pixelConverter.Offsety; }
        }
    }
}