using System.Windows;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.Interfaces.MapVisualConverter;
using Depiction.APINew.ValueTypes;
using ProjNet.Converters.WellKnownText;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;


namespace Depiction.DPNPorting.PixelMapConverters
{
    public class MercatorCoordinateConverterFor122Conversion : IGeoConverter
    {
        private double hMetersPerPixel;
        // These get recalculated only when the extent changes
        private Point topLeftCartesian;
        private double vMetersPerPixel;
        private readonly ICoordinateTransformation transformer;

        public MercatorCoordinateConverterFor122Conversion(IGeoAndPixelExtent extent, double offsetX, double offsetY)
        {
            const string utmWKT = "PROJCS[\"WGS 84 / UTM zone 32N\",GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]],PROJECTION[\"Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"32632\"]]";
            IProjectedCoordinateSystem utmCS = CoordinateSystemWktReader.Parse(utmWKT) as IProjectedCoordinateSystem;

            const string geowkt = "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geowkt) as IGeographicCoordinateSystem;

            CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();

            //// MORE THAN MEETS THE EYE!!
            transformer = ctfac.CreateFromCoordinateSystems(geoCS, utmCS);

            InitializeExtent(extent);

            Offsetx = offsetX;
            Offsety = offsetY;
        }

        #region IGeoConverter Members

        public Point WorldToGeoCanvas(ILatitudeLongitude latLon)
        {
            double[] pointCartesian = transformer.MathTransform.Transform(new [] { latLon.Longitude, latLon.Latitude});
            double x = (pointCartesian[0] - topLeftCartesian.X)/hMetersPerPixel;
            double y = (pointCartesian[1] - topLeftCartesian.Y)/vMetersPerPixel;
            return new Point(x - Offsetx, y - Offsety);
        }

        public Rect WorldToGeoCanvas(IMapCoordinateBounds mapBounds)
        {
            var topLeftGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Top,mapBounds.Left));
            var bottomRightGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Bottom, mapBounds.Right));
            return new Rect(topLeftGeo, bottomRightGeo);
        }

        public ILatitudeLongitude GeoCanvasToWorld(Point p)
        {
            double px = p.X + Offsetx;
            double py = p.Y + Offsety;
            double x = (px*hMetersPerPixel) + topLeftCartesian.X;
            double y = (py*vMetersPerPixel) + topLeftCartesian.Y;
            double[] pointWorld = transformer.MathTransform.Inverse().Transform(new[] {x, y});
            return new LatitudeLongitude(pointWorld[1], pointWorld[0]);
        }

        public IMapCoordinateBounds GeoCanvasToWorld(Rect rect)
        {
            var topLeftGeo = GeoCanvasToWorld(rect.TopLeft);
            var bottomRightGeo = GeoCanvasToWorld(rect.BottomRight);
            return new MapCoordinateBounds(topLeftGeo, bottomRightGeo);
        }

        #endregion

        public double Offsetx { get; private set; }
        public double Offsety { get; private set; }

        private void InitializeExtent(IGeoAndPixelExtent extent)
        {
            var tl = transformer.MathTransform.Transform(new[] { extent.NorthWest.Longitude, extent.NorthWest.Latitude });
            topLeftCartesian = new Point(tl[0], tl[1]);
            var br = transformer.MathTransform.Transform(new[] { extent.SouthEast.Longitude, extent.SouthEast.Latitude });
            vMetersPerPixel = (br[1] - topLeftCartesian.Y) / extent.HeightInPixels;
            hMetersPerPixel = (br[0] - topLeftCartesian.X) / extent.WidthInPixels;
        }
    }
}