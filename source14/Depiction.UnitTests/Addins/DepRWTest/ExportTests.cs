﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.Serialization;
//using GeoAPI.CoordinateSystems;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OSR;
using ProjNet.Converters.WellKnownText;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;

namespace Depiction.UnitTests.Addins.DepRWTest
{
    [TestFixture]
    public class ExportTests
    {

        private TempFolderService temp;
        [SetUp]
        public void SetupTest()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            temp = new TempFolderService(true);
            GdalEnvironment.SetupEnvironment();
        }
        [TearDown]
        public void Teardown()
        {
            DepictionAccess.ElementLibrary = null;
            temp.Close();
        }

        [Ignore("Just for quick import/export turn around time")]
        [Test]
        public void CanGeoTiffImageBeLoaded()
        {
            string fileStart = "Depiction.UnitTests.Addins.DepRWTest.";
            string assemblyName = "Depiction.UnitTests";
            var tempDir = temp.FolderName;

            var baseName = "Randomimage";
            var imageName = baseName + ".jpg";
            var tempImageName = Path.Combine(tempDir, imageName);

            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
            Assert.IsTrue(File.Exists(tempImageName));
            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
            var dsOriginal = Gdal.Open(tempImageName, Access.GA_ReadOnly);
            var origWidth = 1920;
            var origHeight = 1200;
            var newWidth = 1920;
            var newHeight = 1200;
            var outImage = baseName + ".tif";
            var tempOutImage = Path.Combine(tempDir, outImage);
//            var saveOptions = new string[] { "TFW=YES" }; 
            var saveOptions = new string[] { };
            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);

            var adfGeoTransform = new double[] { 444720, 30, 0, 3751320, 0, -30 };
            geoTiff.SetGeoTransform(adfGeoTransform);
            var projection = geoTiff.GetProjection();
            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            string wtkString = string.Empty;
            srs.ExportToWkt(out wtkString);
            geoTiff.SetProjection(wtkString);
            var outTrans = new double[6];
            geoTiff.GetGeoTransform(outTrans);
            var projectionRef = geoTiff.GetProjectionRef();

            geoTiff.FlushCache();
            geoTiff.Dispose();
        }
        [Ignore("Just for quick import/export turn around time keeps the output in a not deleted dir")]
        [Test]
        public void IsGDALTFWImageExportRoundTrippable()
        {
            var tempDir = temp.FolderName;
            var baseName = "tfwImage";
            var imageName = baseName + ".tif";
            var tempImageName = Path.Combine(tempDir, imageName);
            string fileStart = "Depiction.UnitTests.TestImages.WorldFile.";
            string assemblyName = "Depiction.UnitTests";
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
            var dsOriginal = Gdal.Open(tempImageName, Access.GA_ReadOnly);
            var tempOut = "C:\\dlp\\depTest";
            var outName = "outtiff.tif";
            var tempOutImage = Path.Combine(tempOut, outName);
//            var saveOptions = new string[] { "TFW=YES" };
            var saveOptions = new string[] { };
            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
           
            var topLeftLatLong = new LatitudeLongitude(47.9320413898329, -122.374351441618);
            var tlUTM = topLeftLatLong.ConvertToUTM();
            var botLatLong = new LatitudeLongitude(47.5776691792137, -121.954224837974);
            var brUTM = botLatLong.ConvertToUTM();
             var imagePixWidth = 3141.0;//pixWidth of sample image
            var imagePixHeight = 3843.0;//pixheight of sample image
            var widthDis = tlUTM.EastingMeters - brUTM.EastingMeters;
            var heightDis = tlUTM.NorthingMeters - brUTM.NorthingMeters;
            // top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
            double pixelWidth = widthDis/imagePixWidth;
            double pixelHeight = heightDis/imagePixHeight;
            //Alright so there are oddities with the saved TFW files and the ones that depiction uses as the truth. I believe the gdal since it seems more consistent
            //with what sources say about the tfw files. ie we shouldn't use the  + (pixelWidth / 2) and  - (pixelHeight / 2)
            //The removal of half pixel height is because spatial reference is expecting the mid point (real world length) of the topleft coordinate pixel
            var wgs84Transform = new double[] { tlUTM.EastingMeters + (pixelWidth / 2), pixelHeight, 0, tlUTM.NorthingMeters - (pixelHeight / 2), 0, -pixelWidth };
            geoTiff.SetGeoTransform(wgs84Transform);
            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            srs.SetUTM(tlUTM.UTMZoneNumber, 1);
            string wtkString = string.Empty;
            srs.ExportToWkt(out wtkString);
            geoTiff.SetProjection(wtkString);
            geoTiff.FlushCache();
            geoTiff.Dispose();

            var reloadTiff = Gdal.Open(tempImageName, Access.GA_ReadOnly);
            var outTrans = new double[6];
            reloadTiff.GetGeoTransform(outTrans);
        }
        [Ignore("This sucks")]
        [Test]
        public void ActualExportTest()
        {
            var tempDir = temp.FolderName;
            var baseName = "tfwImage";
            var imageName = baseName + ".tif";
            var tempImageName = Path.Combine(tempDir, imageName);
            string fileStart = "Depiction.UnitTests.TestImages.WorldFile.";
            string assemblyName = "Depiction.UnitTests";
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
            var dsOriginal = Gdal.Open(tempImageName, Access.GA_ReadOnly);
            var tempOut = "C:\\dlp\\depTest";
            var outName = "outtiffRoundTrip.tif";
            var tempOutImage = Path.Combine(tempOut, outName);
            //            var saveOptions = new string[] { "TFW=YES" };
            var saveOptions = new string[] { };
            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
            const string popularVisualisationWKT = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
                                                           "GEOGCS[\"Popular Visualisation CRS\",  " +
                                                               "DATUM[\"WGS84\",    " +
                                                                   "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
                                                               "AUTHORITY[\"EPSG\",\"6055\"]], " +
                                                          "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
                                                          "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
                                                          "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
                                                      "PROJECTION[\"Mercator\"]," +
                                                      "PARAMETER[\"semi_minor\",6378137]," +
                                                      "PARAMETER[\"False_Easting\", 0]," +
                                                      "PARAMETER[\"False_Northing\", 0]," +
                                                      "PARAMETER[\"Central_Meridian\", 0]," +
                                                      "PARAMETER[\"Latitude_of_origin\", 0]," +
                                                      "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
                                                      "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
                                                      "AUTHORITY[\"EPSG\",\"3785\"]]";
            IProjectedCoordinateSystem popularVisualizationCS = CoordinateSystemWktReader.Parse(popularVisualisationWKT) as IProjectedCoordinateSystem;
            //Cant get an inverse out of this one
//            const string geowkt = "GEOGCS[\"WGS 84\"," +
//                                  "DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
//                                  "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
//                                  "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
//            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geowkt) as IGeographicCoordinateSystem;

            const string geoWKT =
               "GEOGCS[\"GCS_WGS_1984\"," +
                   "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                   "PRIMEM[\"Greenwich\",0]," +
                   "UNIT[\"Degree\",0.0174532925199433]" +
               "]";
            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geoWKT) as IGeographicCoordinateSystem;


            CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();

            //// MORE THAN MEETS THE EYE!!
            var transformer = ctfac.CreateFromCoordinateSystems(geoCS, popularVisualizationCS);

            var topLeftLatLong = new LatitudeLongitude(47.9320413898329, -122.374351441618);
            var botLatLong = new LatitudeLongitude(47.5776691792137, -121.954224837974);

            var topleft = WorldToGeoCanvas(topLeftLatLong, transformer);
            var bottomRight = WorldToGeoCanvas(botLatLong, transformer);
            var imagePixWidth = dsOriginal.RasterXSize;//pixWidth of baseimage
            var imagePixHeight = dsOriginal.RasterYSize;//pixheight of baseimage

            var widthDis = bottomRight.X - topleft.X;
            var heightDis = bottomRight.Y - topleft.Y;
            // top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
            double pixelWidth = Math.Abs(widthDis / imagePixWidth);
            double pixelHeight = Math.Abs(heightDis / imagePixHeight);
            //Alright so there are oddities with the saved TFW files and the ones that depiction uses as the truth. I believe the gdal since it seems more consistent
            //with what sources say about the tfw files. ie we shouldn't use the  + (pixelWidth / 2) and  - (pixelHeight / 2)
            //The removal of half pixel height is because spatial reference is expecting the mid point (real world length) of the topleft coordinate pixel
            //                var wgs84Transform = new double[] { tlUTM.EastingMeters + (pixelWidth / 2), pixelWidth, 0, tlUTM.NorthingMeters - (pixelHeight / 2), 0, -pixelHeight };
            var wgs84Transform = new double[] { topleft.X, pixelWidth, 0, topleft.Y, 0, -pixelHeight };
            geoTiff.SetGeoTransform(wgs84Transform);
            geoTiff.SetProjection(popularVisualisationWKT);
            geoTiff.FlushCache();
            geoTiff.Dispose();

            var reloadTiff = Gdal.Open(tempOutImage, Access.GA_ReadOnly);
            var outTrans = new double[6];
            reloadTiff.GetGeoTransform(outTrans);
            var proj = reloadTiff.GetProjection();
            var projRef = reloadTiff.GetProjectionRef();
            var topLeftPoint = new System.Windows.Point(outTrans[0], outTrans[3]);
            var topLeftCart = GeoCanvasToWorld(topLeftPoint, transformer);
        }
        private System.Windows.Point WorldToGeoCanvas(ILatitudeLongitude latLon, ICoordinateTransformation transformer)
        {
            double[] pointCartesian = transformer.MathTransform.Transform(new[] { latLon.Longitude, latLon.Latitude });
            return new System.Windows.Point(pointCartesian[0], pointCartesian[1]);
        }
        private ILatitudeLongitude GeoCanvasToWorld(System.Windows.Point pointCart, ICoordinateTransformation transformer)
        {
            var pointLatLong = transformer.MathTransform.Inverse().Transform(new[] { pointCart.X, pointCart.Y });
            return new LatitudeLongitude(pointLatLong[1], pointLatLong[0]);
        }

        [Ignore("Just for quick import/export turn around time")]
        [Test]
        public void CanTFWImageBeLoaded()
        {
            var dmlsToLoad = new List<string> { "Image.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, temp, false);
            string fileStart = "Depiction.UnitTests.TestImages.WorldFile.";
            string assemblyName = "Depiction.UnitTests";
            var tempDir = temp.FolderName;

            var baseName = "tfwImage";
            var imageName = baseName + ".tif";
            var fileName = baseName + ".tfw";
            var tempImageName = Path.Combine(tempDir, imageName);
            var tempFileName = Path.Combine(tempDir, fileName);
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + fileName, tempFileName);
            Assert.IsTrue(File.Exists(tempImageName));
            Assert.IsTrue(File.Exists(tempFileName));
            var width = 3141.0;//pixWidth of sample image
            var height = 3843.0;//pixheight of sample image

            var tfwParam = DepictionImageElementAddingService.ReadTFWFileParams(tempFileName);
            Assert.IsNotNull(tfwParam);
            Assert.AreEqual(6, tfwParam.Length);
            var center = new LatitudeLongitude(47.602468967170395, -122.32896000000001);
            var imageMetadata =
                DepictionImageElementAddingService.CreateImageMetaDataFromCenterAndWorldFileParams(string.Empty, width,
                                                                                                   height, tfwParam,
                                                                                                   center);
            Assert.IsNotNull(imageMetadata);

            //GeoBounds = {TopLeft: 47.9320413898329,-122.374351441618 BottomRight: 47.5776691792137,-121.954224837974}  
            var expectedGeoBounds = new MapCoordinateBounds(new LatitudeLongitude(47.9320413898329, -122.374351441618),
                                                            new LatitudeLongitude(47.5776691792137, -121.954224837974));
            Assert.AreEqual(expectedGeoBounds, imageMetadata.GeoBounds);

        }

        [Ignore]
        [Test]
        public void ExportingToShapeFileTest()
        {
            var tempOut = "C:\\dlp\\depTest";
//            Driver drv = Ogr.GetDriverByName("ESRI Shapefile");
//            if (drv == null) return;
//            DataSource ds = drv.CreateDataSource(fileName, null);
//            var layer = ds.CreateLayer("lineString", null, wkbGeometryType.wkbLineString, null);
//            //            var fdefn = new FieldDefn("ElementType", FieldType.OFTString);
//            //            fdefn.SetWidth(32);
//            //            layer.CreateField(fdefn, 0);
//            var elementsWithCorrectGeomType = GetElementsWithMatchingGeometry(DepictionGeometryType.LineString, elementList);
//            IDepictionElement element = null;
//            if (elementsWithCorrectGeomType.Any())
//            {
//                element = elementsWithCorrectGeomType.First();
//            }
//            if (element == null) return;
//            Feature feature = new Feature(layer.GetLayerDefn());
//            //            feature.SetField("ElementType", "thing");
//            var geom = GeometryToOgrConverter.ZOIToOgrGeometry(new ZoneOfInfluence(new DepictionGeometry()));
//            feature.SetGeometry(geom);
//            layer.CreateFeature(feature);
//
//            ds.Dispose();
        }

    }
}
//            ColorTable colorTable = dsOriginal.GetRasterBand(1).GetColorTable(); // Get the color table from the first band. If it is null then it is a greyscale image.
//            bool grayscale = colorTable == null; // If the colorTable is null then Greyscale will be null.
//            int[][] lookupTable = {
//                                  new int[256],
//                                  new int[256],
//                                  new int[256],
//                                };
//
//            if (!grayscale)
//            {
//                for (int i = 0; i < colorTable.GetCount(); i++)
//                {
//                    ColorEntry entry = colorTable.GetColorEntry(i);
//                    for (int x = 0; x < 3; x++)
//                    {
//                        short value = 0;
//                        switch (x)
//                        {
//                            case 0:
//                                value = entry.c1;
//                                break;
//                            case 1:
//                                value = entry.c2;
//                                break;
//                            case 2:
//                                value = entry.c3;
//                                break;
//                        };
//                        lookupTable[x][i] = value;
//                    }
//                }
//            }
//            int newLineIndex = 0;
//            for (int line = 0; line < origHeight - 2; line++)
//            {
//                for (int bandNumber = 0; bandNumber < 3; bandNumber++)
//                {
//                    int bandIndex = dsOriginal.RasterCount > 1 ? bandNumber + 1 : 1;
//                    //Read in the original band
//                    Band band = dsOriginal.GetRasterBand(bandIndex);
//                    Band newBand = geoTiff.GetRasterBand(bandNumber + 1);
//                    band.ReadRaster(0, line, origWidth, 1, oldbuffer, origWidth, 1, 1, 1);
//
//                    if (grayscale)
//                    {
//                        newBand.WriteRaster(0, newLineIndex, newWidth, 1, oldbuffer, newWidth, 1, 1, 1);
//                    }
//                    else
//                    {
//
//                        int[] bandTable = lookupTable[bandNumber];
//                        int newPixelIndex = 0;
//                        for (int pixel = TopBeginningPixel; pixel < BotEndingPixel - 1; pixel++)
//                        {
//                            newbuffer[newPixelIndex] = (Byte)bandTable[(short)oldbuffer[pixel]];
//                            newPixelIndex++;
//                        }
//                        newBand.WriteRaster(0, newLineIndex, newWidth, 1, newbuffer, newWidth, 1, 1, 1);
//                    }
//                }
//
//                newLineIndex++;
//                //If you are using a progress bar then update the progress here.
//            }
