using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.LiveReports.Models;
using Depiction.Serialization;
using NUnit.Framework;
namespace Depiction.UnitTests.Addins.EmailTests
{
    [TestFixture]
    public class EMailParsingTests
    {
        private string positionKey = "position";
        private string displayNameKey = "displayname";
        private TempFolderService tempFolder;
        //These tests should be in the addin that creates them. For now mash everything into the main depiction app.
        #region Setup/Teardown
        [SetUp]
        protected void SetUp()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            var prototype = new ElementPrototype("MockType", "Mock element");
            DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(prototype);
            tempFolder = new TempFolderService(true);

            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;
        }

        [TearDown]
        protected void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();

            DepictionAccess.ProductInformation = null;
        }

        #endregion

        //This test doesn't entirely belong here, but what ever
        [Test]
        public void IsCorrectElementPrototypeCreated()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            
            Assert.AreEqual(2, DepictionAccess.ElementLibrary.UserPrototypes.Count);

            var subject = "Person: me,20,40";
            var props = EmailParser.ParseEmailSubjectIntoProperties(subject);
            var keyName = "elementtype";
            Assert.IsTrue(props.ContainsKey(keyName));

            Assert.AreEqual("Person", props[keyName].ToString());
            var prototype = ElementFactory.CreateRawPrototypeOfType(props[keyName].ToString());
            var element = ElementFactory.CreateElementFromPrototype(prototype);
            Assert.IsNotNull(element);
            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
        }
        [Test]
        public void LiveReportsWith122TypesGetCorrectPrototypeFromSubject()
        {
            var convertedType = "Depiction.Plugin.UserDrawnPolygon";
            var subjectText = "Depiction.Plugin.ShapeUserDrawn: ,35.71973, -78.58180";
            
            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subjectText, string.Empty);
            Assert.IsNotNull(prototype);
            Assert.AreEqual(convertedType,prototype.ElementType);
        }
        [Test]
        public void LiveReportsWith122TypesGetCorrectPrototypeFromEmailBody()
        {
            var convertedType = "Depiction.Plugin.UserDrawnPolygon";
            var bodyString = "ElementType : Depiction.Plugin.ShapeUserDrawn";
            var updatedPrototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty,bodyString);
            Assert.IsNotNull(updatedPrototype);
            Assert.AreEqual(convertedType, updatedPrototype.ElementType);
        }
        [Test]
        public void ZoneOfInfluenceInEmailBodyFrom122IsReadCorretly()
        {
            var dmlsToLoad = new List<string> { "LoadedShapes.dml" };//, "AutoDetect.dml" 
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false,true);

            Assert.AreEqual(3, DepictionAccess.ElementLibrary.UserPrototypes.Count);//Why was it 3 before, ok i guess there is a mock element in here somewhere
            var typeName = "Depiction.Plugin.LoadedPolygon";
            var zoiString =
                "POLYGON((-78.5783839670058 35.7165146698669,-78.5795345351876 35.7234187393739,-78.5856119285 35.7208737096267,-78.5856119285 35.7181266205267,-78.5783839670058 35.7165146698669))";
            var bodyString = "ZoneOfInfluence: "+zoiString;

            var updatedPrototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty,bodyString);
            Assert.IsNotNull(updatedPrototype);
            Assert.AreEqual(typeName,updatedPrototype.ElementType);

            var element = ElementFactory.CreateElementFromPrototype(updatedPrototype);
            Assert.IsNotNull(element);
            var modernZOI = new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString));
            Assert.AreEqual(modernZOI.ToString(), element.ZoneOfInfluence.ToString());
        }

        [Test]
        public void LatLongWithCommaSubject()
        {
            var subject = "hello,20,40";
            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);

            object displayName;
            var hasDisplayName = dictionary.TryGetValue(displayNameKey, out displayName);
            Assert.IsTrue(hasDisplayName);
            Assert.AreEqual("hello", displayName);

            object position;
            var hasPosition = dictionary.TryGetValue(positionKey, out position);
            Assert.IsTrue(hasPosition);
            Assert.IsNotNull(position);
            Assert.AreEqual(new LatitudeLongitude(20, 40), position);

            //            Assert.IsNull(record.ElementType);
        }
        [Test]
        public void LatLongWithNoCommaSubject()
        {
            var subject = "hello,20 40";
            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);

            object displayName;
            var hasDisplayName = dictionary.TryGetValue(displayNameKey, out displayName);
            Assert.IsTrue(hasDisplayName);
            Assert.AreEqual("hello", displayName);

            object position;
            var hasPosition = dictionary.TryGetValue(positionKey, out position);
            Assert.IsTrue(hasPosition);
            Assert.IsNotNull(position);
            Assert.AreEqual(new LatitudeLongitude(20, 40), position);

            //            Assert.IsNull(record.ElementType);
        }
        [Test]
        public void SubjectMissingLocation()
        {
            var subject = "a label without location";
            var prototype = EmailParser.ParseEmailSubjectIntoProperties(subject);

            object displayName;
            var hasDisplayName = prototype.TryGetValue(displayNameKey, out displayName);
            Assert.IsTrue(hasDisplayName);
            Assert.AreEqual("a label without location", displayName);
            object position;
            var hasPosition = prototype.TryGetValue(positionKey, out position);
            Assert.IsFalse(hasPosition);
        }

        [Test]
        public void SubjectWithBadLocation()
        {
            var subject = "label,...............";
            var prototype = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(prototype);
            object position;
            var hasPosition = prototype.TryGetValue(positionKey, out position);
            Assert.IsFalse(hasPosition);
        }
        [Test]
        public void ElementTypeGuessedFromSubject()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            Assert.AreEqual(2, DepictionAccess.ElementLibrary.UserPrototypes.Count);

            var subject = "car";
            var dictionary = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subject,string.Empty);

            var element = ElementFactory.CreateElementFromPrototype(dictionary);
            Assert.IsNotNull(element);

            Assert.AreEqual("Depiction.Plugin.Car", element.ElementType);
        }

        

        [Test]
        public void TestZeroOrOnePairInEmailBody()
        {
            CheckEmailBodyHasNProperties(" p1 : ", 1);
            CheckEmailBodyHasNProperties(" 42 : v2 ", 0);
            CheckEmailBodyHasNProperties(" 42p : v2 ", 0);
            CheckEmailBodyHasNProperties(" : v1", 0);

            CheckEmailBodyHasNProperties("prop : value", 1);
            CheckEmailBodyHasNProperties(" a b : v2 ", 1);
            CheckEmailBodyHasNProperties("p1 : v1\n\np2 v2", 1);
        }
        private IElementPrototype CheckEmailBodyHasNProperties(string bodyString, int expectedNumberOfPropertiesFromBody)
        {
            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty,bodyString);
            Assert.AreEqual(expectedNumberOfPropertiesFromBody, prototype.OrderedCustomProperties.Length);
            return prototype;
        }

        [Test]
        public void TestMultiplePairsInBody()
        {
            var prototype = CheckEmailBodyHasNProperties("p1 : v1\n\n \t p2 : v2 ", 2);
            string p1;
            var hasP1 = prototype.GetPropertyValue("p1", out p1);
            Assert.IsTrue(hasP1);
            Assert.AreEqual("v1", p1);
            string p2;
            var hasP2 = prototype.GetPropertyValue("p2", out p2);
            Assert.IsTrue(hasP2);
            Assert.AreEqual("v2", p2);
        }

        [Test]
        public void HandleDuplicateProperties()
        {
            var bodyString = "p1 : v1\n\n p1 : v2";
            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty,bodyString);
            Assert.AreEqual(1, prototype.OrderedCustomProperties.Length);
            //Drops the first value, for now
            string p1;
            var hasP1 = prototype.GetPropertyValue("p1", out p1);
            Assert.IsTrue(hasP1);
            Assert.AreEqual("v2", p1);
            //            Assert.AreEqual(1, DepictionAccess.NotificationService.Messages.Count);
        }
        [Test]
        public void HandleDuplicatePropertiesIsCaseInsensitive()
        {
            var bodyString = "p1 : v1\n\n P1 : v2"; // Adds "Email error" property, too. Not really but it might be useful

            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(1, prototype.OrderedCustomProperties.Length);
            //Drops the last value, for now
            string p1;
            var hasP1 = prototype.GetPropertyValue("p1", out p1);
            Assert.IsTrue(hasP1);
            Assert.AreEqual("v2", p1);
            //            Assert.AreEqual(1, DepictionAccess.NotificationService.Messages.Count);
        }
        [Test]
        public void HandlePropertiesWithNoValue()
        {
            var bodyString = "p1 : \n\n P2 : v2"; // Adds "Email error" property, too. Not really but it might be useful
            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(2, prototype.OrderedCustomProperties.Length);
            //Drops the last value, for now
            string p1;
            var hasP1 = prototype.GetPropertyValue("p1", out p1);
            Assert.IsTrue(hasP1);
            Assert.AreEqual("", p1);
        }

        [Test]
        public void ParseBodyMayIgnoreSpecifiedProperties()
        {
            var bodyString = "NaMe1 : v1";
            var dict = EmailParser.ParseEmailBodyStringIntoProperties(bodyString,  new List<string> { "name1" });
            Assert.AreEqual(0, dict.Keys.Count);
        }
        [Test]
        public void ParseBodyOnlyIgnoresSpecifiedProperties()
        {
            var bodyString = "p1 : v1";
            var dict = EmailParser.ParseEmailBodyStringIntoProperties(bodyString, new List<string> { "not a match" });
            Assert.AreEqual(1, dict.Keys.Count);
        }

        [Test]
        public void LatLonInSubjectTakesPriorityOverBody()
        {
            var subjectString = "Person: me, 10, 11";
            var bodyString = "latitude: 20\nlongitude: 21";

            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subjectString,bodyString);

            LatitudeLongitude position;
            var hasPosition = prototype.GetPropertyValue(positionKey, out position);
            Assert.IsTrue(hasPosition);
            Assert.AreEqual(new LatitudeLongitude(10, 11), position);
        }

        [Test]
        public void LatLonTakenFromBody()
        {
            var subjectString = "Person: me";
            var bodyString = "latitude: 20\nlongitude: 21";

            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subjectString,bodyString);

            LatitudeLongitude position;
            var hasPosition = prototype.GetPropertyValue(positionKey, out position);
            Assert.IsTrue(hasPosition);
            Assert.AreEqual(new LatitudeLongitude(20, 21), position);
        }

        [Test]
        public void PositionTakenFromBody()
        {
            var subjectString = "Person: me";
            var bodyString = "Position: 20, 21";

            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subjectString, bodyString);

            LatitudeLongitude position;
            var hasPosition = prototype.GetPropertyValue(positionKey, out position);
            Assert.IsTrue(hasPosition);
            Assert.AreEqual(new LatitudeLongitude(20, 21), position);
        }
        [Test]
        public void HandlesRepliesOrForwardedEmailSubjectLines()
        {
            string[] subjects ={
                    "RE: FW: Person: me, 10 11",
                    "FW: RE: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "Re: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "FW: Person: me, 10 11",};

            foreach (var subject in subjects)
            {
                var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);

                object position;
                var hasPosition = dictionary.TryGetValue(positionKey, out position);
                Assert.IsTrue(hasPosition);
                Assert.AreEqual(new LatitudeLongitude(10, 11), position);

                object displayName;
                var hasDisplayName = dictionary.TryGetValue(displayNameKey, out displayName);
                Assert.IsTrue(hasDisplayName);
                Assert.AreEqual("me", displayName);
            }
        }
        [Test]
        public void YesNoWorksForBoolean()
        {
            var subject = "label,__notAnAddress__";
            var body = "Latitude: 10\nLongitude: 20\nActive: yes";
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,string.Empty,string.Empty,string.Empty,string.Empty);
            UnitTestHelperMethods.PropertyTester("Active",true,prototype);


            subject = "label,__notAnAddress__";
            body = "Latitude: 10\nLongitude: 20\nActive: no";
            prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester("Active", false, prototype);
        }

        [Test]
        public void TrueFalseWorksForBoolean()
        {
            var subject = "label,__notAnAddress__";
            var body = "Latitude: 10\nLongitude: 20\nActive: true";
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester("Active", true, prototype);


            subject = "label,__notAnAddress__";
            body = "Latitude: 10\nLongitude: 20\nActive: false";
            prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester("Active", false, prototype);
        }
        /*Depiction.Plugin.Explosion: ,35.72019, -78.58027
         * Author: Depiction, Inc.
Description:
Name: Explosion
Type:
Blast radius: [Distance] 200 feet
Additional notes:
Fill color: [Color] #AA990000
Outline color: [Color] #AA990000
Active: [Bool] True
Allow dragging: [Bool] True
Xaml resource name: embeddedresource:Depiction.Explosion
IconSize: [Double] 24
Perimeter: [Distance] 0.577129649193396 miles
Area: [Area] 0.0183528158115479 square km
EID: 20101116092511.212.1
ZoneOfInfluence: POLYGON((-78.5795996948292 35.7210540477097,-78.5788665541668 35.7209593811982,-78.5785229012377 35.7209196555066,-78.5785898732418 35.7207216929778,-78.5788779956542 35.7207322155103,-78.5792368931667 35.7207281233105,-78.5796113971833 35.7207660327037,-78.5799041622707 35.7207629608197,-78.5801020432291 35.7206796882082,-78.5801060591041 35.7204355408037,-78.5801106611 35.7201666771394,-78.5802747064833 35.7198267408938,-78.5806211861708 35.7184636929089,-78.5808453671833 35.7184667648823,-78.5808655201458 35.7191705613523,-78.5808243438875 35.719545504485,-78.5808266993208 35.7199892717758,-78.5808250872499 35.721030908487,-78.5801842366625 35.7210799051925,-78.5795996948292 35.7210540477097))

         * */
        /*Depiction.Plugin.ShapeUserDrawn: ,35.71973, -78.58180
         * From: Depiction Exercise
Sent: Tue 11/16/2010 8:15 AM
To: Reports THREE
Subject: Depiction.Plugin.ShapeUserDrawn: ,35.71973, -78.58180

Author: Depiction, Inc.
Description: To change the shape from a basic rectangle, right-click after placing and choose Edit Shape to move/add vertices. Hold the Shift key while clicking (Shift+click) on a line to add vertices.
Name: Shape - user drawn
Length: [Distance] 1000 feet
Width: [Distance] 500 feet
Orientation in degrees: [Double] 0
Active: [Bool] True
Allow dragging: [Bool] True
Outline color: [Color] #FFCC3300
Fill color: [Color] #FF000000
Additional notes:
Xaml resource name: embeddedresource:Depiction.Rectangle
IconSize: [Double] 24
Perimeter: [Distance] 1.47516660479147 miles
Area: [Area] 0.325706793418266 square km
EID: 20101116081554.839.2
ZoneOfInfluence: POLYGON((-78.5783839670058 35.7165146698669,-78.5795345351876 35.7234187393739,-78.5856119285 35.7208737096267,-78.5856119285 35.7181266205267,-78.5783839670058 35.7165146698669))
         * */

    }
}