using System.Collections.Generic;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.LiveReports.Models;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.EmailTests
{
    [TestFixture]
    public class EmailAndLiveReportComboTests
    {
        private TempFolderService tempFolder;
        #region Setup/Teardown
        [SetUp]
        protected void SetUp()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);
        }

        [TearDown]
        protected void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
        }

        #endregion

        [Test]
        public void DoElementsFromLiveReportsGetHovertext()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var geocoder = new MockGeocoder();
            var eidVal = "testing123";
            //the email that gets sent
            var subject = "Car: Test, " + MockGeocoder.FakeAddress1;
            var body = "EID:" + eidVal + "\n";
            var emailReadService = new DepictionEmailReaderBackgroundService();
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                        string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });
            var depictionStory = new DepictionStory();
            emailReadService.AddReadEmailsToDepictionStory(depictionStory,new List<IElementPrototype> { prototype });
            var elements = depictionStory.CompleteElementRepository.AllElements;
            Assert.AreEqual(1,elements.Count);
            foreach(var element in elements)
            {
                var prop = element.GetPropertyByInternalName("displayName");
                Assert.IsNotNull(prop);
                Assert.IsTrue(prop.IsHoverText);
            }
        }
        [Test]
        public void IsSimpleEmailElementReadableByLiveReports()
        {
            var from = "";
            var to = "";
            var prop1Name = "EID";
            var prop2Name = "weight";
            var prop3Name = "randomNumber";
            var prop1Value = "firstID";
            var prop2Value = new Weight(MeasurementSystem.Metric, MeasurementScale.Normal, 15);
            var prop3Value = 4;
            var position = new LatitudeLongitude(30, 40);

            var element = new DepictionElementParent();
            element.Position = position;
            var prop1 = new DepictionElementProperty(prop1Name, prop1Value);
            element.AddPropertyOrReplaceValueAndAttributes(prop1);
            var prop2 = new DepictionElementProperty(prop2Name, prop2Value);
            element.AddPropertyOrReplaceValueAndAttributes(prop2);
            var prop3 = new DepictionElementProperty(prop3Name, prop3Value);
            element.AddPropertyOrReplaceValueAndAttributes(prop3);
            var dragProp = new DepictionElementProperty("draggable", false);
            element.AddPropertyOrReplaceValueAndAttributes(dragProp);

            var emailElement = new ElementEmail(element);
            var body = emailElement.Body;
            var subject = emailElement.Subject;

            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, from, to, "", "");
            var otherElement = new DepictionElementParent();

            ElementFactory.UpdateElementWithPrototypeProperties(otherElement, readPrototype, false,true);
            Assert.IsTrue(otherElement.HasPropertyByInternalName(prop1Name));
            Assert.IsTrue(otherElement.HasPropertyByInternalName(prop2Name));
            Assert.IsTrue(otherElement.HasPropertyByInternalName(prop3Name));

            Assert.AreEqual(position, otherElement.Position);

            //Make sure all the base properties are the same
            foreach (var prop in element.OrderedCustomProperties)
            {
                UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, otherElement);
            }
        }
        [Test]
        public void IsElementWithWaypointsSentAndRecievedCorrectly()
        {
            var location1 = new LatitudeLongitude(1, 2);
            var location2 = new LatitudeLongitude(3, 4);
            var location3 = new LatitudeLongitude(5, 6);

            var name1 = "First";
            var name2 = "Second";
            var name3 = "Third";
            var elementToSend = new DepictionElementParent();

            var way1 = new DepictionElementWaypoint { Name = name1, Location = location1 };
            var way2 = new DepictionElementWaypoint { Name = name2, Location = location2 };
            var way3 = new DepictionElementWaypoint { Name = name3, Location = location3 };
            elementToSend.AddWaypoint(way1);
            elementToSend.AddWaypoint(way2);
            elementToSend.AddWaypoint(way3);
            var emailElement = new ElementEmail(elementToSend);
            var body = emailElement.Body;
            var subject = emailElement.Subject;

            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, "me", "them", "", "");
            Assert.IsNotNull(readPrototype);
            Assert.AreEqual(3, readPrototype.Waypoints.Length);

        }

        [Test]
        public void ZOIFillAndBorderColorEmailRoundTripTest()
        {
            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var elementToSend = ElementFactory.CreateElementFromTypeString("PointOfInterest");

            var color = Colors.Orange;
            elementToSend.SetPropertyValue("ZOIFill", color);
            var eidPropIntVal = "123456789983213423";
            var eidProp = new DepictionElementProperty("eid", eidPropIntVal);
            elementToSend.AddPropertyOrReplaceValueAndAttributes(eidProp, false);
            object expectedColorValue;
            elementToSend.GetPropertyValue("ZOIFill", out expectedColorValue);
            var emailElement = new ElementEmail(elementToSend);
            var body = emailElement.Body;
            var subject = emailElement.Subject;

            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, "me", "them", "", "");
            var depictionStory = new DepictionStory();
            
            var elementToCompare = ElementFactory.CreateElementFromTypeString("PointOfInterest");
            var eidProp1 = new DepictionElementProperty("eid", eidPropIntVal);
            elementToCompare.AddPropertyOrReplaceValueAndAttributes(eidProp1, false);

            depictionStory.AddElementToDepictionElementList(elementToCompare,false);

            Assert.AreEqual(1,depictionStory.CompleteElementRepository.AllElements.Count);
            var elementResult = depictionStory.CompleteElementRepository.AllElements[0];
            Assert.AreEqual(eidPropIntVal, elementResult.ElementUserID);
            depictionStory.CreateOrUpdateDepictionElementListFromPrototypeList(new List<IElementPrototype> { readPrototype }, false, false);
            Assert.AreEqual(1, depictionStory.CompleteElementRepository.AllElements.Count);
            elementResult = depictionStory.CompleteElementRepository.AllElements[0];
            object finalColorValue;
            elementResult.GetPropertyValue("ZOIFill", out finalColorValue);
            Assert.AreEqual(expectedColorValue,finalColorValue);

        }
        [Test]
        public void DoEmailsWithPropertyDisplayNamesUpdateTheIntendedProperty()
        {
            var eidPropVal = "1234";
            var propToUpdateName = "thing";
            var propToUpdateDisplayName = "Thing to add";
            var propUpdateValue = 1;
           
            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var elementToSend = ElementFactory.CreateElementFromTypeString("PointOfInterest");

            var eidProp = new DepictionElementProperty("eid", eidPropVal);
            elementToSend.AddPropertyOrReplaceValueAndAttributes(eidProp, false);
            var randomProp = new DepictionElementProperty(propToUpdateName, propToUpdateDisplayName, 1);
            elementToSend.AddPropertyOrReplaceValueAndAttributes(randomProp, false);

            var emailElement = new ElementEmail(elementToSend);

            var body = "EID:" + eidPropVal +"\n" + propToUpdateDisplayName+":"+ propUpdateValue;
            var subject = emailElement.Subject;

            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, "me", "them", "", "");
            
            var depictionStory = new DepictionStory();

            var baseElement = ElementFactory.CreateElementFromTypeString("PointOfInterest");
            var eidProp1 = new DepictionElementProperty("eid", eidPropVal);
            baseElement.AddPropertyOrReplaceValueAndAttributes(eidProp1, false);
            var randomProp1 = new DepictionElementProperty(propToUpdateName, propToUpdateDisplayName, 4);
            baseElement.AddPropertyOrReplaceValueAndAttributes(randomProp1, false);

            depictionStory.AddElementToDepictionElementList(baseElement, false);

            Assert.AreEqual(1, depictionStory.CompleteElementRepository.AllElements.Count);
            var elementResult = depictionStory.CompleteElementRepository.AllElements[0];
            Assert.AreEqual(eidPropVal, elementResult.ElementUserID);
            depictionStory.CreateOrUpdateDepictionElementListFromPrototypeList(new List<IElementPrototype> { readPrototype }, false, false);
            Assert.AreEqual(1, depictionStory.CompleteElementRepository.AllElements.Count);
            elementResult = depictionStory.CompleteElementRepository.AllElements[0];
            object updatedPropvalue;
            elementResult.GetPropertyValue(propToUpdateName, out updatedPropvalue);
            Assert.AreEqual(propUpdateValue,updatedPropvalue);


        }
        [Test]
        public void IsRoute122RecievedCorrectly()
        {
            var dmlsToLoad = new List<string> { "RouteUserDrawn.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var long1 = "-122.369212241474";
            var lat1 = "47.7925238614053";
            var expectedZOI = "LINESTRING(" + long1 + " " + lat1 + ",-122.337268841705 47.803546688162399)";//47.8035466881624 was original value

            var latLong1 = new LatitudeLongitude(lat1, long1);
            var subject = "Depiction.Plugin.RouteUserDrawn: ,47.79252, -122.36921";
            var body = "Author: Depiction, Inc.\n" +
                       "Description: Drag the start and end points to create a land, air or sea route. With your cursor over the route, hold the Shift key while clicking (Shift+click) to add waypoints.\n" +
                       "Name: Evacuate to Hospital\n" +
                       "Additional notes:\n" +
                       "Icon path: embeddedresource:Depiction.RouteUserDrawn\n" +
                       "Color: [Color] #FF0000FF\n" +
                       "IconSize: [Double] 24\n" +
                       "EID: 20100203153810.529.21\n" +
                       "ZoneOfInfluence: " + expectedZOI + "\n" +
                       "ElementChild:\n" +
                       "Author: Depiction, Inc.\n" +
                       "Name: Evacuate non-ambulatory residents via paratransit vans\n" +
                       "Allow dragging: [Bool] True\n" +
                       "Icon path: xaml:Depiction.RouteStart\n" +
                       "IconSize: [Double] 24\n" +
                       "ZoneOfInfluence: POINT(" + long1 + " " + lat1 + ")\n" +
                       "ElementChild:\n" +
                       "Author: Depiction, Inc.\n" +
                       "Name: Route end\n" +
                       "Allow dragging: [Bool] True\n" +
                       "Icon path: xaml:Depiction.RouteEnd\n" +
                       "IconSize: [Double] 24\n" +
                       "ZoneOfInfluence: POINT(-122.337268841705 47.803546688162399)";
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, "me", "them", "", "");
            Assert.IsNotNull(readPrototype);
            Assert.IsNotNull(readPrototype.ZoneOfInfluence);
            //There is a round trip issue
            var modernZOI = new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(expectedZOI));
            Assert.AreEqual(modernZOI.ToString(), readPrototype.ZoneOfInfluence.ToString());
            Assert.AreEqual(2, readPrototype.Waypoints.Length);
            Assert.AreEqual(latLong1, readPrototype.Waypoints[0].Location);
            var element = ElementFactory.CreateElementFromPrototype(readPrototype, null);

            Assert.IsNotNull(element);
            Assert.AreEqual(2, element.Waypoints.Length);
            Assert.AreEqual(latLong1, element.Waypoints[0].Location);
            Assert.IsNotNull(element.ClickActions);
        }

        [Test(Description = "Ensures that location properties are used for geocoding (an mock geocoder is used since the result isn't what is tested)")]
        public void DoesElementWithLocationInformationGeocode()
        {
            var dmlsToLoad = new List<string> { "EmergencyOperationsCenter.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var subject =
                "Depiction.Plugin.EmergencyOperationsCenter: Message # 39 184 Raleigh Wilson Road - Depiction Demo Ops. Period #1, 391 Raleigh Wilson Road, Vilas, North Carolina, 28692";
            var body = "EID: 8B408DF833790A658525780800707B23\n" +
                       "Name: Emergency operations center (EOC)\n" +
                       "Author: David Hancock - Carolina County, North Carolina\n" +
                       "Description: \n" +
                       "Street address: 391 Raleigh Wilson Road\n" +
                       "City: Vilas\n" +
                       "State: North Carolina\n" +
                       "Zip code: 28692\n" +
                       "Active: [Bool] False\n" +
                       "Fill color: [Color] #00000000\n" +
                       "Outline color: [Color] #FFCC3300\n" +
                       "IconSize: [Double] 24\n" +
                       "Worker Completing Form: David Hancock\n" +
                       "Color: [Color] #FF808080";
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                                                                                                           "me",
                                                                                                           "them", "",
                                                                                                           "", new[] { new MockGeocoder() });
            Assert.IsNotNull(readPrototype);
            LatitudeLongitude position;
            var foundPosition = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(foundPosition);
            Assert.AreEqual(MockGeocoder.Address1Result, position);
            DepictionAccess.GeoCodingService = null;
        }
        
        [Test]
        public void DoesElementWithLocationInformationInSubjectGeocode()
        {
            var dmlsToLoad = new List<string> { "EmergencyOperationsCenter.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var subject =
                "Depiction.Plugin.EmergencyOperationsCenter: Message # 39 184 Raleigh Wilson Road - Depiction Demo Ops. Period #1, " + MockGeocoder.RawRealAddress;
            var body = string.Empty;
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                                                "me","them", "","", new[] { new MockGeocoder() });
            Assert.IsNotNull(readPrototype);
            LatitudeLongitude position;
            var foundPosition = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(foundPosition);
            Assert.AreEqual(MockGeocoder.Address1Result, position);
            DepictionAccess.GeoCodingService = null;
        }
        
        [Test]//This test is more an integration tests and be split up later
            //Since it tests depictions ability to update elements. Better 
            //yet a test should be made to ensure that a depiction story updates
            //elements by their eids properly.
        public void DoElementsWithZOITriggersGetCreatedFromEmail()
        {
            var dmlsToLoad = new List<string> { "CircleShape.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var testProduct = new TestProductInformation();
            DepictionAccess.PathService = new ApplicationPathService(testProduct.DirectoryNameForCompany);//DepictionAccess._productInformation);
            AddinRepository.Compose();//It would be cool to have a smaller version of this for tests
            var behaviours = AddinRepository.Instance.GetBehaviors();
            Assert.AreNotEqual(0,behaviours.Count);
            var subject = "Circle: EID Test, 47, -122";
            var body = "EID: 123";
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                           "me","them", "","");
            Assert.IsNotNull(readPrototype);
            LatitudeLongitude position;
            var hasPropValue = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(hasPropValue);
            Assert.AreEqual(new LatitudeLongitude(47, -122), position);
            //IDepictionElement createdElement;

            //var repo = new ElementRepository();
            //var updatedId = repo.AddOrUpdateRepositoryFromPrototype(readPrototype, out createdElement, false);
            //Assert.IsFalse(string.IsNullOrEmpty(updatedId));

            var story = new DepictionStory();
            Assert.AreEqual(0,story.CompleteElementRepository.AllElements.Count);
            var emailService = new DepictionEmailReaderBackgroundService();
            emailService.AddReadEmailsToDepictionStory(story,new List<IElementPrototype>{readPrototype});
//            story.CreateOrUpdateDepictionElementListFromPrototypeList(new[] {readPrototype}, false, false);
            Assert.AreEqual(1, story.CompleteElementRepository.AllElements.Count);
            var element = story.CompleteElementRepository.AllElements[0];
            Assert.AreEqual(DepictionGeometryType.Polygon,element.ZoneOfInfluence.DepictionGeometryType);
            
            AddinRepository.Decompose();
            DepictionAccess.PathService = null;
            DepictionAccess.GeoCodingService = null;
        }

        [Test]
        public void AnotherTest()
        {
            var dmlsToLoad = new List<string> { "Bus.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var subject = "Depiction.Plugin.Bus: ,47.96616, -122.206";
            var body = "Author: Depiction, Inc.\n" +
                        "Description: General purpose bus\n" +
                        "Name: Bus\n" +
                        "Type:\n" +
                        "Route description:\n" +
                        "Street address:\n" +
                        "City:\n" +
                        "State:\n" +
                        "Zip code:\n" +
                        "Owner/Operator:\n" +
                        "Phone number:\n" +
                        "Maximum passenger capacity: [Number] 0\n" +
                        "Average speed: [Speed] 0 mph\n" +
                        "Gross vehicle weight: [Weight] 0 pounds\n" +
                        "Additional notes:\n" +
                        "Active: [Bool] True\n" +
                        "Allow dragging: [Bool] True\n" +
                        "Xaml resource name: embeddedresource:Depiction.Bus\n" +
                        "IconSize: [Double] 24\n" +
                        "EID: 20110125115706.914.1\n" +
                        "ZoneOfInfluence: POINT(-122.206814498127 47.9661582585405)";
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                                                                                                           "me",
                                                                                                           "them", "",
                                                                                                           "");
            Assert.IsNotNull(readPrototype);
            LatitudeLongitude position;
            var hasPropValue = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(hasPropValue);
            Assert.AreEqual(new LatitudeLongitude(47.96616, -122.206), position);
            DepictionAccess.GeoCodingService = null;
        }

        [Test]
        public void PlumeElementZOIGetsLoadedIfItExistsInEmail()
        {
            var dmlsToLoad = new List<string> { "Plume.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var subject = "Depiction.Plugin.Plume: ,47.60815, -122.30128";
            var body = "Author: Depiction, Inc.\n" +
                       "Description: Create the extent of a basic plume based on wind direction and other variables\n" +
                       "Name: Gas Smell\n" +
                       "Material type:\n" +
                       "Wind speed: [Speed] 5 mph\n" +
                       "Direction wind is from (degrees): [Double] 45\n" +
                       "Amount of release: [Double] 100\n" +
                       "Time in hours: [Double] 0.5\n" +
                       "Additional notes:\n" +
                       "Fill color: [Color] #AAFF8C00\n" +
                       "Outline color: [Color] #FF808080\n" +
                       "Allow dragging: [Bool] True\n" +
                       "Icon path: embeddedresource:Depiction.Plume\n" +
                       "IconSize: [Double] 24\n" +
                       "Email sent: Thu, 17 Feb 2011 10:06:08 -0800\n" +
                       "EID: <3E34DF2D43A0AA47828FCCC0AC9C579B6E17B6@simioserver.Simio.local>\n" +
                       "Perimeter: [Distance] 1.05564123295494 miles\n" +
                       "Area: [Area] 0.193039997348948 square km\n" +
                       "ZoneOfInfluence:";
            var zoiString = "POLYGON((-122.307412464469 47.6040042555394,-122.307769203669 47.6044637264394,-122.307768266369 47.6050964942394," +
         "-122.307409744269 47.6058406190394,-122.306728732069 47.6066232576394,-122.305791891869 47.6073677949394," +
         "-122.304690928269 47.6080013455394,-122.303533611169 47.6084618899394,-122.302433226769 47.6087043470394," +
         "-122.301497488469 47.6087049864394,-122.300817992869 47.6084637507394,-122.300461253669 47.6080042584394," +
         "-122.300462190969 47.6073714910394,-122.300820713069 47.6066273882394,-122.301501725269 47.6058447847394," +
         "-122.302438565469 47.6051002821394,-122.303539529069 47.6044667529394,-122.304696846169 47.6040062081394," +
         "-122.305797230569 47.6037637290394,-122.306732968769 47.6037630545394,-122.307412464469 47.6040042555394))\n";
            var zoi = new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString));
            body += zoi.ToString();
            var readPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                                                                                                          "me",
                                                                                                          "them", "",
                                                                                                          "");
            Assert.IsNotNull(readPrototype);
            LatitudeLongitude position;
            var hasPropValue = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(hasPropValue);
            Assert.AreEqual(new LatitudeLongitude(47.60815, -122.30128), position);
            Assert.AreEqual(zoi, readPrototype.ZoneOfInfluence);
            //Color?
            hasPropValue = readPrototype.GetPropertyValue("Position", out position);
            Assert.IsTrue(hasPropValue);
        }
        /**Depiction.Plugin.RouteUserDrawn: ,47.79252, -122.36921
 * Author: Depiction, Inc.
Description: Drag the start and end points to create a land, air or sea route. With your cursor over the route, hold the Shift key while clicking (Shift+click) to add waypoints.
Name: Evacuate to Hospital
Additional notes:
Icon path: embeddedresource:Depiction.RouteUserDrawn
Color: [Color] #FF0000FF
IconSize: [Double] 24
EID: 20100203153810.529.21
ZoneOfInfluence: LINESTRING(-122.369212241474 47.7925238614053,-122.337268841705 47.8035466881624)
ElementChild:
Author: Depiction, Inc.
Name: Evacuate non-ambulatory residents via paratransit vans
Allow dragging: [Bool] True
Icon path: xaml:Depiction.RouteStart
IconSize: [Double] 24
ZoneOfInfluence: POINT(-122.369212241474 47.7925238614053)
ElementChild:
Author: Depiction, Inc.
Name: Route end
Allow dragging: [Bool] True
Icon path: xaml:Depiction.RouteEnd
IconSize: [Double] 24
ZoneOfInfluence: POINT(-122.337268841705 47.8035466881624)
 * */
    }
    class MockGeocoder : IDepictionGeocoder
    {
        public const string RawRealAddress = "391 Raleigh Wilson Road, Vilas, North Carolina, 28692";
        public const string FakeAddress1 = "First place";
        public const string FakeAddress2 = "Second place";
        public static ILatitudeLongitude DefaultResult = new LatitudeLongitude(1, 2);
        public static ILatitudeLongitude Address1Result = new LatitudeLongitude(3, 4);
        public static ILatitudeLongitude FakeResult1 = new LatitudeLongitude(10, 30);
        public static ILatitudeLongitude FakeResult2 = new LatitudeLongitude(20, 25);

        public bool StoreResultsInDepiction
        {
            get { return false; }
        }

        public int PerUseGeocodeLimit
        {
            get { return -1; }
            set { }
        }

        public string GeocoderName
        {
            get { return "TestingGeocoder"; }
        }

        public bool IsAddressGeocoder
        {
            get { return true; }
        }

        public bool IsLatLongGeocoder
        {
            get { return true; }
        }

        public void ReceiveParameters(Dictionary<string, string> parameters)
        {
        }

        public GeocodeResults GeocodeAddress(string addressString)
        {
            switch (addressString)
            {
                case RawRealAddress:
                    return new GeocodeResults(Address1Result, true);
                case FakeAddress1:
                    return new GeocodeResults(FakeResult1, true);
                case FakeAddress2:
                    return new GeocodeResults(FakeResult2, true);

            }
            return new GeocodeResults(DefaultResult, true);
        }

        public GeocodeResults GeocodeAddress(string addressString, bool isLatLong)
        {
            return GeocodeAddress(addressString, true);
        }

        public GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country)
        {
            return new GeocodeResults(DefaultResult, true);
        }
    }
}