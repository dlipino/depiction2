using Depiction.FrameworkElements.Helpers.HtmlXamlConverter;
using NUnit.Framework;

namespace Depiction.UnitTests.View.Dialogs.Converters
{
    [TestFixture]
    public class ConverterTests
    {
        [Test(Description="This is not actually a resut test, more of a does it finish test")]
        public void DoesBadLinkWork()
        {
            var badHref = "<a href=\"http//www.depiction.com\">test</a>";
            var result = true;
            try
            {
                HtmlToXamlConverter.ConvertHtmlToXaml(badHref, true).ToString();
            }catch
            {
                result = false;
            }
            Assert.IsTrue(result);
        }
    }
}