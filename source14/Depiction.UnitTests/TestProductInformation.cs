﻿
using Depiction.API.AbstractObjects;

namespace Depiction.UnitTests
{
    public class TestProductInformation : ProductInformationBase
    {

        public override string ResourceAssemblyName { get { return "Resources.Product.Depiction"; } }
        
        override public string DirectoryNameForCompany { get { return "Depiction_Testing"; } }
    }
}
