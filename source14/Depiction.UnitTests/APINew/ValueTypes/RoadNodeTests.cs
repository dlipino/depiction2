using System.IO;
using Depiction.API.ValueTypes;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ValueTypes
{
    [TestFixture]
    public class RoadNodeTests
    {
        [Test]
        public void DoesRoadNodeSaveLoad()
        {
            var temp = new TempFolderService(true);

            var toSave = new RoadNode();
            toSave.NodeID = 1;
            toSave.Vertex = new LatitudeLongitude(1,2);

            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<RoadNode>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}