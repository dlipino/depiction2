﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ValueTypes
{
    [TestFixture]
    public class InteractionRuleTests
    {
        #region Temp testing stuff to better understand xml serialization Not actually ever used
        //        class Thing : IXmlSerializable
        //        {
        //            #region Implementation of IXmlSerializable
        //
        //            public XmlSchema GetSchema()
        //            {
        //                throw new System.NotImplementedException();
        //            }
        //
        //            public void ReadXml(XmlReader reader)
        //            {
        //                throw new System.NotImplementedException();
        //            }
        //
        //            public void WriteXml(XmlWriter writer)
        //            {
        //                writer.WriteStartElement("InteractionRule");
        //                writer.WriteStartElement("Publisher");
        //                writer.WriteAttributeString("Type", "Element");
        //                writer.WriteStartElement("TriggeredProperties");
        //                writer.WriteStartElement("Property");
        //                writer.WriteFullEndElement();
        //            }
        //
        //            #endregion
        //        }
        #endregion
        #region setup tear down
        private TempFolderService tempFolder;
        [SetUp]
        public void Setup()
        {
//            DepictionAccess.ElementLibrary = null;
//            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);

            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;

        }
        [TearDown]
        public void TearDown()
        {
//            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();

            DepictionAccess.ProductInformation = null;
        }
        #endregion
        #region helpers

        protected List<IInteractionRule> InteractionRuleCopyHelper(TempFolderService tempFolderLocal)
        {
            string assemblyName = "Resources.Product.Depiction";
            string desiredAssemblyLocation = "Resources.Product.Depiction.Interactions";
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(assemblyName, "xml", true, tempFolderLocal.FolderName);
            var files = Directory.GetFiles(tempFolderLocal.FolderName);
            var interactionRules = new List<IInteractionRule>();
            foreach (var file in files)
            {
                if (Path.GetFileName(file).StartsWith(desiredAssemblyLocation, StringComparison.InvariantCultureIgnoreCase))
                {
                    var readInteractions = InteractionRule.LoadRulesFromXmlFile(file);
                    interactionRules.AddRange(readInteractions);
                }
            }
            return interactionRules;
        }
        #endregion
        [Test]
        public void DoesDepictionInteractionRuleSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
//            var temp = new TempFolderService(true);

            var thingToSave = new InteractionRule();
            thingToSave.Author = "Me";
            thingToSave.Publisher = "SomeType";
            thingToSave.PublisherTriggerPropertyNames.Add("PropName");
            thingToSave.Subscribers.Add("DifferentType");
            thingToSave.Conditions.Add("RandomCondition", new HashSet<string> { "diffProp" });
            thingToSave.SubscriberTriggerPropertyNames.Add("subscriberProp");
            //Dang this doesn't have a behavior :(
            var file = tempFolder.GetATempFilenameInDir("xml");
            var localName = "SingleType";
            SerializationService.SaveToXmlFile(thingToSave, file, localName);

            var loadedThing = SerializationService.LoadFromXmlFile<InteractionRule>(file, localName);

            Assert.IsTrue(thingToSave.FunctionallyEquals(loadedThing));

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            //This should delete everything in the temp directory
//            temp.Close();
        }

        [Test]
        public void DoDefaultXMLInteractionRulesLoad()
        {
            var expectedRuleCount = (1 + 1 + 2 +1 + 4 + 1 + 1 + 2 + 1 );

//            var temp = new TempFolderService(true);
            var interactionRules = InteractionRuleCopyHelper(tempFolder);

            Assert.AreEqual(expectedRuleCount, interactionRules.Count);
//            temp.Close();
        }
        [Test]
        public void DoesInteractionDeepCloneWork()
        {
//            var temp = new TempFolderService(true);
            var interactionRules = InteractionRuleCopyHelper(tempFolder);

            foreach (var rule in interactionRules)
            {
                var deepClone = rule.DeepClone();
                Assert.AreNotSame(rule, deepClone);
            }
//            temp.Close();
        }
        [Test]
        public void Do12InteractionsLoadInto13()
        {
//            var tempFolder = new TempFolderService(true);
            string assemblyName = "Depiction.UnitTests";
            string desiredAssemblyLocation = "Depiction.UnitTests.APINew.ValueTypes";
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(assemblyName, "xml", true, tempFolder.FolderName);
            var files = Directory.GetFiles(tempFolder.FolderName);
            var interactionRules = new List<IInteractionRule>();
            foreach (var file in files)
            {
                if (Path.GetFileName(file).StartsWith(desiredAssemblyLocation, StringComparison.InvariantCultureIgnoreCase))
                {
                    var readInteractions = InteractionRule.LoadRulesFromXmlFile(file);
                    interactionRules.AddRange(readInteractions);
                }
            }
//            tempFolder.Close();
        }
    }

}
