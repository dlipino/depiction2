﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ValueTypes
{
    [TestFixture]
    public class LatitudeLongitudeTests
    {
        [Test]
        public void DoesDepictionLatitudeLongitudeSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new LatitudeLongitude(13,15);
            Assert.IsFalse(Equals(toSave, null));
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<LatitudeLongitude>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
       
        [Test]
        public void ToStringConvertsToLocalCulture()
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("et-EE"); //Estonian culture uses , as decimal symbol
            var latlong = new LatitudeLongitude(48.002232265238668, 10);
            Settings.Default.LatitudeLongitudeFormat = LatitudeLongitudeFormat.ColonFractionalSeconds;
            Assert.AreEqual("48:00:08,036N, 10:00:00,000E", latlong.ToString());
            Settings.Default.LatitudeLongitudeFormat = LatitudeLongitudeFormat.ColonIntegerSeconds;
            Assert.AreEqual("48:00:08N, 10:00:00E", latlong.ToString());
            Thread.CurrentThread.CurrentCulture = cultureInfo;
        }

        [Test]
        public void LatLongParseUtilityWorksForAllCultures()
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            var cultureWithPeriod = new CultureInfo("en-US");
            var cultureWithCommas = new CultureInfo("et-EE");
            var numberFormat = cultureWithPeriod.NumberFormat;
            //numberFormat.NumberDecimalSeparator
            Thread.CurrentThread.CurrentCulture = cultureWithPeriod;//Estonian culture uses , as decimal symbol
            var expectedLatLong = new LatitudeLongitude(1.2, 3.4);
            var latLongStringPeriod = "1.2,3.4";
            var latLongStringPeriodSpace = "1.2 3.4";
            var latLongStringCommas = "1,2,3,4";
            var latLongStringCommasSpace = "1,2 3,4";
            double latVal, longVal;
            string message;
            var parseEvaluation = LatLongParserUtility.ParseForLatLong(latLongStringPeriod, out latVal, out longVal,
                                                                       out message);
            Assert.AreEqual(true,parseEvaluation);
            var stringLatLong = new LatitudeLongitude(latVal, longVal);
            Assert.AreEqual(expectedLatLong, stringLatLong);

            parseEvaluation = LatLongParserUtility.ParseForLatLong(latLongStringPeriodSpace, out latVal, out longVal,
                                                                       out message);
            Assert.AreEqual(true, parseEvaluation);
             stringLatLong = new LatitudeLongitude(latVal, longVal);
            Assert.AreEqual(expectedLatLong, stringLatLong);

            Thread.CurrentThread.CurrentCulture = cultureWithCommas;

            parseEvaluation = LatLongParserUtility.ParseForLatLong(latLongStringCommas, out latVal, out longVal,
                                                                       out message);
            Assert.AreEqual(true, parseEvaluation);
            stringLatLong = new LatitudeLongitude(latVal, longVal);
            Assert.AreEqual(expectedLatLong, stringLatLong);

            parseEvaluation = LatLongParserUtility.ParseForLatLong(latLongStringCommasSpace, out latVal, out longVal,
                                                                      out message);
            Assert.AreEqual(true, parseEvaluation);
            stringLatLong = new LatitudeLongitude(latVal, longVal);
            Assert.AreEqual(expectedLatLong, stringLatLong);
            Thread.CurrentThread.CurrentCulture = cultureInfo;
        }

        [Test]
        public void LatLongTypeConverterWorksForAllCultures()
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            var cultureWithPeriod = new CultureInfo("en-US");
            var cultureWithCommas = new CultureInfo("et-EE");//Estonian culture uses , as decimal symbol
            //var numberFormat = cultureWithPeriod.NumberFormat;
            //numberFormat.NumberDecimalSeparator
            Thread.CurrentThread.CurrentCulture = cultureWithPeriod;
            var expectedLatLong = new LatitudeLongitude(1.2, 3.4);
            var latLongStringPeriod = "1.2,3.4";
            var latLongStringCommas = "1,2,3,4";

            var stringLatLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(latLongStringPeriod);
            Assert.AreEqual(expectedLatLong, stringLatLong);

            Thread.CurrentThread.CurrentCulture = cultureWithCommas;

            stringLatLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(latLongStringCommas);
            Assert.AreEqual(expectedLatLong, stringLatLong);

            Thread.CurrentThread.CurrentCulture = cultureInfo;
            
        }
         [Test]
        public void LatLongTypeConverterWorksForUTM()
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            var cultureWithPeriod = new CultureInfo("en-US");
            var cultureWithCommas = new CultureInfo("et-EE");

            Thread.CurrentThread.CurrentCulture = cultureWithPeriod;
             var expectedLatLongString = "43.6423069, -79.3869514";
             var expectedLatLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(expectedLatLongString);

             Thread.CurrentThread.CurrentCulture = cultureWithPeriod;
             var utmInputString = "17N 630100.0 4833410.0";
             var convertedUtmLatLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(utmInputString);
             Assert.AreEqual(expectedLatLong, convertedUtmLatLong);

             Thread.CurrentThread.CurrentCulture = cultureWithCommas;
             utmInputString = "17N 630100,0 4833410,0";
             convertedUtmLatLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(utmInputString);
             Assert.AreEqual(expectedLatLong, convertedUtmLatLong);
             Thread.CurrentThread.CurrentCulture = cultureInfo;
         }

     
       [Test]
       public void TestToStringFormatWithNW()
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
           var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                           {
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonFractionalSeconds, "40:26:46.302N, 79:56:55.903W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonIntegerSeconds, "40:26:46N, 79:56:56W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal, "40.44620N, 79.94886W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 26.77170N, 79° 56.93172W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°26'46\"N, 79°56'56\"W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d26'46\"N, 79d56'56\"W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDecimal, "40.44620, -79.94886"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "40° 26.77170, -79° 56.93172"),
                           };
           foreach (var pair in pairs)
           {
               var latlong = new LatitudeLongitude(40.446195, -79.948862);
               Settings.Default.LatitudeLongitudeFormat = pair.Key;
               Assert.AreEqual(pair.Value, latlong.ToString());
           }
           Thread.CurrentThread.CurrentCulture = cultureInfo;
       }

       [Test]
       public void TestToStringFormatWithSE()
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
           var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                           {
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonFractionalSeconds, "40:26:46.302S, 79:56:55.903E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonIntegerSeconds, "40:26:46S, 79:56:56E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal, "40.44620S, 79.94886E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 26.77170S, 79° 56.93172E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°26'46\"S, 79°56'56\"E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d26'46\"S, 79d56'56\"E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDecimal, "-40.44620, 79.94886"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "-40° 26.77170, 79° 56.93172"),
                           };
           foreach (var pair in pairs)
           {
               var latlong = new LatitudeLongitude(-40.446195, 79.948862);
               Settings.Default.LatitudeLongitudeFormat = pair.Key;
               Assert.AreEqual(pair.Value, latlong.ToString());
           }
           Thread.CurrentThread.CurrentCulture = cultureInfo;
       }

       [Test]
       public void TestToStringFormatWhenLittleGreaterOrLessThanWholeLatLongNW()
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
           var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                           {
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.ColonFractionalSeconds, "40:00:00.036N, 79:59:59.964W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.ColonIntegerSeconds, "40:00:00N, 80:00:00W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal,
                                                                                 "40.00001N, 79.99999W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 00.00060N, 79° 59.99940W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°00'00\"N, 80°00'00\"W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d00'00\"N, 80d00'00\"W"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.SignedDecimal, "40.00001, -79.99999"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(
                                   LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "40° 00.00060, -79° 59.99940"),
                           };
           foreach (var pair in pairs)
           {
               var latlong = new LatitudeLongitude(40.00001, -79.99999);
               Settings.Default.LatitudeLongitudeFormat = pair.Key;
               Assert.AreEqual(pair.Value, latlong.ToString());
           }
           Thread.CurrentThread.CurrentCulture = cultureInfo;
       }

       [Test]
       public void TestToStringFormatWhenLittleGreaterOrLessThanWholeLatLongSE()
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
           var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                           {
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonFractionalSeconds, "40:00:00.036S, 79:59:59.964E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonIntegerSeconds, "40:00:00S, 80:00:00E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal, "40.00001S, 79.99999E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 00.00060S, 79° 59.99940E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°00'00\"S, 80°00'00\"E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d00'00\"S, 80d00'00\"E"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDecimal, "-40.00001, 79.99999"),
                               new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "-40° 00.00060, 79° 59.99940"),
                           };
           foreach (var pair in pairs)
           {
               var latlong = new LatitudeLongitude(-40.00001, 79.99999);
               Settings.Default.LatitudeLongitudeFormat = pair.Key;
               Assert.AreEqual(pair.Value, latlong.ToString());
           }
           Thread.CurrentThread.CurrentCulture = cultureInfo;
       }
        /*
    [Test]
    public void TestToStringFormatWhenJustBarelyGreaterOrLessThanWholeLatLongNW()
    {
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                        {
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.ColonFractionalSeconds, "40:00:00.000N, 80:00:00.000W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.ColonIntegerSeconds, "40:00:00N, 80:00:00W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal,
                                                                              "40.00000N, 80.00000W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 00.00000N, 80° 00.00000W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°00'00\"N, 80°00'00\"W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d00'00\"N, 80d00'00\"W"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.SignedDecimal, "40.00000, -80.00000"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(
                                LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "40° 00.00000, -80° 00.00000"),
                        };
        foreach (var pair in pairs)
        {
            var latlong = new LatitudeLongitude(40.00000001, -79.99999999);
            DepictionAccess.UserConfigurableProperties.LatitudeLongitudeFormat = pair.Key;
            Assert.AreEqual(pair.Value, latlong.ToString());
        }
        Thread.CurrentThread.CurrentCulture = cultureInfo;
    }

    [Test]
    public void TestToStringFormatWhenJustBarelyGreaterOrLessThanWholeLatLongSE()
    {
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        var pairs = new List<KeyValuePair<LatitudeLongitudeFormat, string>>
                        {
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonFractionalSeconds, "40:00:00.000S, 80:00:00.000E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.ColonIntegerSeconds, "40:00:00S, 80:00:00E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.Decimal, "40.00000S, 80.00000E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 00.00000S, 80° 00.00000E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°00'00\"S, 80°00'00\"E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d00'00\"S, 80d00'00\"E"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDecimal, "-40.00000, 80.00000"),
                            new KeyValuePair<LatitudeLongitudeFormat, string>(LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "-40° 00.00000, 80° 00.00000"),
                        };
        foreach (var pair in pairs)
        {
            var latlong = new LatitudeLongitude(-40.00000001, 79.99999999);
            DepictionAccess.UserConfigurableProperties.LatitudeLongitudeFormat = pair.Key;
            Assert.AreEqual(pair.Value, latlong.ToString());
        }
        Thread.CurrentThread.CurrentCulture = cultureInfo;
    }
     **/
    }
}
