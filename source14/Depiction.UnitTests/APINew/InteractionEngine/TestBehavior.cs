using System.Collections.Generic;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.TypeConverter;


namespace Depiction.UnitTests.APINew.InteractionEngine
{
    public class TestBehavior : BaseBehavior
    {
        public override ParameterInfo[] Parameters
        {
            get
            {
                return new[] {new ParameterInfo("PropertyName", typeof (string))
                                  {
                                      ParameterName = "Property To Set", ParameterDescription = "Indicates the property whos value will be changed"
                                  }, 
                              new ParameterInfo("Value", typeof (object))
                                  {
                                      ParameterName = "New Value", ParameterDescription = "The property to be changed will have its value set to this value"
                                  }};
            }
        }

        public string FriendlyName
        {
            get { return "Set property value"; }
        }

        public string Description
        {
            get { return "Set a property value of a chosen element property"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (!parameterBag.ContainsKey("PropertyName")) return new BehaviorResult(); 
            var propName = parameterBag["PropertyName"].ToString();
            var val = parameterBag["Value"];
            var prop = subscriber.GetPropertyByInternalName(propName);
            if (prop == null) return new BehaviorResult() { SubscriberHasChanged = false };
            var convertedVal = DepictionTypeConverter.ChangeType(val, prop.ValueType);
            if (subscriber.SetPropertyValue(propName, convertedVal) == false) return new BehaviorResult() { SubscriberHasChanged = false };
            // ((IProperty) parameterBag["ToSet"]).Set(parameterBag["Value"], SetWeight.High);

            return new BehaviorResult();
        }
    }
}