﻿using System.Collections.Generic;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionStoryInterfaces;

namespace Depiction.UnitTests.APINew.InteractionEngine
{
    class ForegroundThreadRouter : Router
    {
        public ForegroundThreadRouter(IElementRepository elementRepository, IInteractionRuleRepository interactionsRepository)
            : base(elementRepository, interactionsRepository)
        {
            interactionGraph = new InteractionGraph(elementRepository, interactionsRepository.InteractionRules);
            foreach (var element in elementRepository.AllElements)
            {
                RegisterElementForInteractions(element);
            }
            ConnectRouterToElementRepositoryChanges();
        }

        public void Begin()
        {
            IList<DepictionInteractionMessage> messages;
            while ((messages = messageQueue.PopAll()) != null && messages.Count > 0)
            {
                Processing = true;
                interactionGraph.ExecuteInteractions(messages);
            }
            Processing = false;
        }

    }
}
