using System.Threading;
using Depiction.APINew.AccessLeftOvers;
using Depiction.APINew.InteractionEngine;
using Depiction.APINew.Interfaces;
using Depiction.APINew.Interfaces.ElementInterfaces;
using Depiction.APINew.StaticAccessors;
using Depiction.APINew.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.UnitTests.APINew.InteractionEngine;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.Test.Depiction.Interaction
{
    [TestFixture]
    public class RouterTests 
    {
        private IInteractionRuleRepository interactionRuleRepository;

        protected override void SetUp()
        {
            CreateNewEmptyStory();
            interactionRuleRepository = ProductAndFolderService.CurrentDepiction.InteractionRuleRepository;
            CommonData.depictionAccessInternalHandle.SpatialOps = new SpatialOps();

        }

        protected override void TearDown()
        {
        }

        private static void setZOI(IDepictionElementBase element, LatitudeLongitude ZOI)
        {
            element.ClearZoneOfInfluence();
            element.SetZoneOfInfluence(new ZoneOfInfluence(), false);
            var point = new Point(ZOI.Longitude, ZOI.Latitude);
            element.ZoneOfInfluence.SetGeometry(point);
        }

        private static InteractionRule CreateNewInteractionRule(string triggerElementType, string affectedElementType,
                                                                string propertyName, string NewValue)
        {
            var rule = new InteractionRule {Publisher = triggerElementType, Name = triggerElementType + affectedElementType + propertyName};
            rule.Subscribers.Add(affectedElementType);
            rule.Conditions.Add("IntersectionCondition", null);

            rule.Behaviors.Add("SetProperty",
                               new[]
                                   {
                                       new ElementFileParameter
                                           {Type = ElementFileParameterType.Subscriber, ElementQuery = "." + propertyName},
                                       new ElementFileParameter
                                           {Type = ElementFileParameterType.Value, ElementQuery = NewValue}
                                   });
            return rule;
        }

        private static IDepictionElementBase CreateMockElement(string elementType, LatitudeLongitude initialZOI)
        {
            var triggerElement = new BaseElement();
            setZOI(triggerElement, initialZOI);
            triggerElement.ElementType = elementType;
            return triggerElement;
        }

        private static IDepictionElementBase CreateMockElement(string elementType)
        {
            var triggerElement = new BaseElement {ElementType = elementType};
            return triggerElement;
        }

        [Test]
        public void DoesBigHammerProblemExist()
        {
            // With cascading changes from trigger->affected->affected2
            // Make sure that after the changes have cascaded through, the changes are still intact after setting affected2 to dirty.

            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            // behaviors used in this test
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new PropCondition(), "PropertyCondition");

            // Add an interaction rule
            var rule2 = new InteractionRule { Publisher = affectedElementType, Name = affectedElementType };
            rule2.Subscribers.Add("affected2");
            rule2.Behaviors.Add("SetProperty",
                                new[]
                                    {
                                        new ElementFileParameter
                                            {Type = ElementFileParameterType.Subscriber, ElementQuery = ".TestProp"},
                                        new ElementFileParameter
                                            {Type = ElementFileParameterType.Value, ElementQuery = "changed"}
                                    });
            //rule2.Conditions.Add("PropertyCondition", null);
            interactionRuleRepository.AddInteractionRule(rule2);

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType);
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType);
            IDepictionElementBase affectedElement2 = CreateMockElement("affected2");
            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));
            affectedElement2.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement2, string.Empty, SourceType.Web);

            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(affectedElement);
            router.RegisterInteraction(affectedElement2);
            router.RegisterInteraction(triggerElement);
            triggerElement.UpdateInteractions();
            // no cascading changes
            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName("TestProp").Value);
            Assert.AreEqual("originalValue", affectedElement2.GetReadOnlyPropertyByInternalName("TestProp").Value);

            var rule = new InteractionRule {Publisher = triggerElementType, Name = triggerElementType};
            rule.Subscribers.Add(affectedElementType);
            rule.Behaviors.Add("SetProperty",
                               new[]
                                   {
                                       new ElementFileParameter
                                           {Type = ElementFileParameterType.Subscriber, ElementQuery = ".TestProp"},
                                       new ElementFileParameter
                                           {Type = ElementFileParameterType.Value, ElementQuery = "trigger"}
                                   });
            interactionRuleRepository.AddInteractionRule(rule, false);
            triggerElement.UpdateInteractions();
            // check that cascading changes worked
            Assert.AreEqual("trigger", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            Assert.AreEqual("changed", affectedElement2.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            // now check again when affectedElement2 is dirty

            triggerElement.UpdateInteractions();
            Assert.AreEqual("trigger", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            Assert.AreEqual("changed", affectedElement2.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        public void DoesChangingAffectedElementTriggerRule()
        {
            // behaviors used in this test
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");

            // conditions used in this test
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");

            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "Test", "changed"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));

            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "vv", null, null));

            Assert.AreEqual("vv", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement, string.Empty, SourceType.Web);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(affectedElement);
            affectedElement.UpdateInteractions();
            Thread.Sleep(357);
            Assert.AreEqual("changed", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);
        }




        [Test]
        public void DoesChangingElementOnlyRestoreRelatedRules()
        {
            //Given 2 pairs of objects A,B and C,D with rules between them, 
            // When A changes, B is affected.  When C changes, D is affected.
            // First A changes and affects B.
            // Make sure that when D is dirty, the changes from A->B are still intact

            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule("AType", "BType", "Test", "changed"));
            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule("CType", "DType", "Test", "changed"));

            IDepictionElementBase AElement = CreateMockElement("AType", new LatitudeLongitude(4, 3));
            IDepictionElementBase BElement = CreateMockElement("BType", new LatitudeLongitude(4, 3));
            IDepictionElementBase CElement = CreateMockElement("CType", new LatitudeLongitude(8, 9));
            IDepictionElementBase DElement = CreateMockElement("DType", new LatitudeLongitude(8, 9));

            AElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "original", null,null));
            BElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "original", null, null));
            CElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "original", null, null));
            DElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "original", null, null));

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(AElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(BElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(CElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(DElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(AElement);
            router.RegisterInteraction(DElement);
            
            AElement.UpdateInteractions();
            Assert.AreEqual("changed", BElement.GetReadOnlyPropertyByInternalName(".Test").Value);
            Assert.AreEqual("original", DElement.GetReadOnlyPropertyByInternalName(".Test").Value);
            
            DElement.UpdateInteractions();
            Assert.AreEqual("changed", BElement.GetReadOnlyPropertyByInternalName(".Test").Value);
            Assert.AreEqual("changed", DElement.GetReadOnlyPropertyByInternalName(".Test").Value);
        }

        [Test]
        public void DoesChangingTriggerElementTriggerRule()
        {
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "Test", "changed"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));

            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "vv", null, null));

            Assert.AreEqual("vv", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(triggerElement);
            triggerElement.UpdateInteractions();
            Assert.AreEqual("changed", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);
        }

        [Test]
        public void DoesChangingZOIOfTriggerToNoLongerIntersectAffectedElementRestoreOriginalProperty()
        {
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "TestProp",
                                                                                  "changedValue"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));
            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(triggerElement);
            router.RegisterInteraction(affectedElement);
            affectedElement.UpdateInteractions();

            Assert.AreEqual("changedValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);

            // now move trigger object out of zone of influence of affected
            setZOI(triggerElement, new LatitudeLongitude(7, 8));
            triggerElement.UpdateInteractions();
            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        public void DoesDeletingATriggerElementRestoreOriginalProperty()
        {
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "TestProp",
                                                                                  "changedValue"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));

            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));
            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(affectedElement);
            router.RegisterInteraction(triggerElement);
            affectedElement.UpdateInteractions();

            Assert.AreEqual("changedValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            router.HandleElementActionAndWait(triggerElement.Delete);
            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        public void DoesMovingAffectedElementOutOfTriggerZoneOfInfluenceRestoreOriginalProperty()
        {
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");
            // test for case A within router.ElementChangedEventHandler
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "TestProp",
                                                                                  "changedValue"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));

            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));
            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(affectedElement);

            affectedElement.UpdateInteractions();

            Assert.AreEqual("changedValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            // now move affected object out of zone of influence of trigger
            setZOI(affectedElement, new LatitudeLongitude(7, 8));

            affectedElement.UpdateInteractions();
            Assert.AreEqual("originalValue", affectedElement.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        [Ignore ("Had to bail and did not have time to finish this test")]
        public void DoCascadingChangesUndoAfterUndoingTheOriginalCause_ATestForBug1338()
        {
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //conditions.Add(new IntersectionCondition(), "IntersectionCondition");
            // test for case A within router.ElementChangedEventHandler
            const string t1Type = "t1";
            const string a1Type = "a1_t2";

            const string t2Type = a1Type;
            const string a2Type = "a2_t3";

            const string t3Type = a2Type;
            const string a3Type = "a3";


            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(t1Type, a1Type, "TestProp", "changedValue1"));
            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(t2Type, a2Type, "TestProp", "changedValue2"));
            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(t3Type, a3Type, "TestProp", "changedValue3"));

            IDepictionElementBase t1 = CreateMockElement(t1Type, new LatitudeLongitude(4, 3));

            IDepictionElementBase a1_t2 = CreateMockElement("a1_t2", new LatitudeLongitude(4, 3));
            IDepictionElementBase a2_t3 = CreateMockElement("a2_t3", new LatitudeLongitude(4, 3));
            IDepictionElementBase a3 = CreateMockElement("a3", new LatitudeLongitude(4, 3));
            IDepictionElementBase unaffected = CreateMockElement("unaffected", new LatitudeLongitude(4, 3));

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(t1, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(a1_t2, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(a2_t3, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(a3, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(unaffected, string.Empty, SourceType.Web);

            a1_t2.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));
            a2_t3.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));
            a3.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));
            unaffected.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            Assert.AreEqual("originalValue", a1_t2.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(t1);
            router.RegisterInteraction(a1_t2);
            router.RegisterInteraction(a2_t3);
            router.RegisterInteraction(a3);
            router.RegisterInteraction(unaffected);

            t1.UpdateInteractions();
            
            Assert.AreEqual("changedValue3", a3.GetReadOnlyPropertyByInternalName(".TestProp").);
            Assert.AreEqual("originalValue", unaffected.GetReadOnlyPropertyByInternalName(".TestProp").Value);
            //// now move trigger object so that it does not interact with a1_t2
            setZOI(t1, new LatitudeLongitude(7, 8));

            t1.UpdateInteractions();
            Assert.AreEqual("originalValue", a3.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        public void DoesPropertyChangeTriggerRule()
        {
            //This behavior is currently the way it works.  Whether it should work this way is an open question at this point (brian, jason 4/9/08)
            //We now DO fire interactions when a property is set.  Whether it should work this way is an open question at this point (jason 11/25/08)
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";
            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));
            //triggerElement.InitMock();
            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(triggerElementType,
                                                                                  affectedElementType, "Test", "changed"));

            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("Test", "Test", "vv", null, null));
            triggerElement.AddPropertyOrReplaceValue(new DepictionElementProperty("PropertyToChange", string.Empty, true, null, null));

            Assert.AreEqual("vv", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(triggerElement);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.AddElement(affectedElement);
            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(triggerElement);
            IProperty dragProp = triggerElement.GetProperty("PropertyToChange");
            dragProp.Set(false, SetWeight.High);
            Thread.Sleep(500);
            Assert.AreEqual("changed", affectedElement.GetReadOnlyPropertyByInternalName(".Test").Value);
        }

        [Test]
        public void DoesRuleWithBehaviorThatGrowsZOIIntoOtherObjectCascade()
        {
            const string triggerElementType = "trigger";
            const string affectedElementType = "affected";

            // behaviors used in this test
            //behaviors.Add(new SetPropertyBehavior(), "SetProperty");
            //behaviors.Add(new SetZOIBehavior(), "SetZOI");

            // Add an interaction rule
            interactionRuleRepository.AddInteractionRule(CreateNewInteractionRule(affectedElementType, "affected2", "TestProp", "changedValue"));

            IDepictionElementBase triggerElement = CreateMockElement(triggerElementType, new LatitudeLongitude(4, 3));
            IDepictionElementBase affectedElement = CreateMockElement(affectedElementType, new LatitudeLongitude(4, 3));

            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));
            var newZOI = new ZoneOfInfluence();
            var coordinates = new []
                                  {
                                      new Coordinate(0, 0), new Coordinate(10, 0), new Coordinate(10, 10),
                                      new Coordinate(0, 10), new Coordinate(0, 0)
                                  };
            GeometryFactory geomFact = new GeometryFactory();
            newZOI.SetGeometry(geomFact.CreatePolygon(geomFact.CreateLinearRing(coordinates), null));
            affectedElement.AddPropertyOrReplaceValue(new DepictionElementProperty("NewZOI", "NewZOI", newZOI, null, null));
            IDepictionElementBase affectedElement2 = CreateMockElement("affected2", new LatitudeLongitude(7, 7));
            affectedElement2.AddPropertyOrReplaceValue(new DepictionElementProperty("TestProp", "TestProp", "originalValue", null, null));

            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.Add(triggerElement, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.Add(affectedElement, string.Empty, SourceType.Web);
            ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded.Add(affectedElement2, string.Empty, SourceType.Web);

            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            router.RegisterInteraction(affectedElement);
            router.RegisterInteraction(affectedElement2);
            router.RegisterInteraction(triggerElement);
            triggerElement.UpdateInteractions();
            Assert.AreEqual("originalValue", affectedElement2.GetProperty("TestProp").Value);

            var rule = new InteractionRule {Publisher = triggerElementType};
            rule.Subscribers.Add(affectedElementType);
            rule.Conditions.Add("IntersectionCondition", null);
            rule.Behaviors.Add("SetZOI", new ElementFileParameter[0]);

            interactionRuleRepository.AddInteractionRule(rule);
            Assert.AreEqual(2, interactionRuleRepository.Count);
            triggerElement.UpdateInteractions();
            Assert.AreEqual("changedValue", affectedElement2.GetReadOnlyPropertyByInternalName(".TestProp").Value);
        }

        [Test]
        public void TestDoesNotThrowAnError()
        {
            const string elementFileString =
                @"<depictionElement displayName='Flood'  elementType='Depiction.Plugin.Flood'>
                	<properties>
                		<property name='displayName' displayName='Display Name' value='Flood' typeName='System.String' editable='True' visible='True'>
                 			<validationRules>
                 				<validationRule type='Depiction.API.Rule.DataTypeValidationRule, Depiction.API'>   
                 					<parameters>     						<parameter value='System.String' type='System.RuntimeType' />   
                 						<parameter value='Display Name must be a string.' type='System.String' />     					</parameters>   
                 				</validationRule>     			</validationRules>     		</property>   
                 		<property name='ShowElementPropertiesOnCreate' displayName='' value='True' typeName='System.Boolean' editable='False' visible='False'/>   
                 		<property name='InteractionRulesConfigurationFile' displayName='' value='Depiction.DomainModel.Resources.SeaLevelChangeEventInteractionRules.xml' typeName='System.String' editable='False' visible='False'/>   
                 		<property name='IconPath' displayName='' value='Depiction.DomainModel.Images.SeaLevelChange.png' typeName='System.String' editable='False' visible='False'/>   
                 		<property name='FillColor' displayName='' value='#98E9B0' typeName='System.Windows.Media.Color' editable='False' visible='False'/>   
                 		<property name='ElementClassification' displayName='' value='1' typeName='Depiction.API.Enums.ElementClassification' editable='False' visible='False'/>   
                 		<property name='Draggable' displayName='Allow Dragging' value='False' typeName='System.Boolean' editable='False' visible='False'/>   
                 		<property name='position' displayName='position' value='0,0' typeName='Depiction.API.ValueObject.LatitudeLongitude, Depiction.API' editable='False' visible='False'/>   
                 		<property name='MetersAboveSeaLevel' displayName='Height in meters above terrain:' value='0' typeName='System.Double' editable='True' visible='True'>   
                  			<postSetActions>     				<action name='.Flood'>     					<parameters>   
                 						<parameter elementQuery='.MetersAboveSeaLevel' />     					</parameters>   
                 				</action>     			</postSetActions>     			<validationRules>   
                 				<validationRule type='Depiction.API.Rule.RangeValidationRule, Depiction.API '>   
                 					<parameters>     						<parameter value='0' type='System.Double' />   
                 						<parameter value='5000' type='System.Double' />     					</parameters>     				</validationRule>   
                 			</validationRules>     		</property>     	</properties>     </depictionElement>";

            IDepictionElementBase setFloodEvent = PrototypePersistenceToDml.CreateElementFromXmlString(elementFileString);

            var simObj = new BaseElement();
            setZOI(simObj, new LatitudeLongitude(4, 3));

            var router = new TestableRouter(ProductAndFolderService.CurrentDepiction.ElementRepositoryGeocoded, ProductAndFolderService.CurrentDepiction.InteractionRuleRepository) { ActivityTimeout = 1 };
            setFloodEvent.DoInteraction += router.ElementChangedEventHandler;
        }
    }
}