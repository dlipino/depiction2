﻿using System.IO;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.HelperObjects
{
    [TestFixture]
    public class DepictionImageMetadataTest
    {
        [Test]
        public void DoesDepictionImageMetadataSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionImageMetadata();
            var file = temp.GetATempFilenameInDir();
            var localName = "ImageMetadata";
            SerializationService.SaveToXmlFile(toSave, file, localName);

            var loaded = SerializationService.LoadFromXmlFile<DepictionImageMetadata>(file, localName);

            Assert.AreEqual(toSave,loaded);

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            //This should delete everything in the temp directory
            temp.Close();
        }
    }
}