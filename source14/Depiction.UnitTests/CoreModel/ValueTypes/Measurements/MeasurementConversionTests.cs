﻿using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Properties;
using Depiction.CoreModel.ValueTypes.Measurements;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ValueTypes.Measurements
{
    [TestFixture]
    public class MeasurementConversionTests
    {
        [Test]
        public void DistanceConversionTests()
        {
            var distance = new Distance(MeasurementSystem.Imperial, MeasurementScale.Normal, 1);
            var val = distance.GetValue(MeasurementSystem.Imperial, MeasurementScale.Normal);
            Assert.AreEqual(1,val);
            //inches
            val = distance.GetValue(MeasurementSystem.Imperial, MeasurementScale.Small);
            Assert.AreEqual(Distance.feetToInch,val);
        }
        [Test]
        public void AreaConversionTests()
        {
            var area = new Area(MeasurementSystem.Metric, MeasurementScale.Large, 1);
            var val = area.GetValue(MeasurementSystem.Imperial, MeasurementScale.Normal);
            Assert.AreEqual(Area.km2Toft2, val);
            //inches
            val = area.GetValue(MeasurementSystem.Imperial, MeasurementScale.Small);
            Assert.AreEqual(Area.km2Toin2, val);
        }
        [Test]
        public void DistanceUsesDefaultValueCorrectlyTests()
        {
            Settings.Default.MeasurementSystem = MeasurementSystem.Imperial;
            Settings.Default.MeasurementScale = MeasurementScale.Small;
            var distance = new Distance(MeasurementSystem.Imperial, MeasurementScale.Normal, 1);
            distance.UseDefaultScale = true;
            var stringVal = distance.Units;
            Assert.AreEqual(1,distance.NumericValue);
            Assert.IsTrue(stringVal.Contains("feet"));

            distance.SetValue(Distance.feetToInch,MeasurementSystem.Imperial, MeasurementScale.Small);
            stringVal = distance.Units;
            Assert.AreEqual(1,distance.NumericValue);
            Assert.IsTrue(stringVal.Contains("feet"));
            Assert.AreEqual(Distance.feetToInch, distance.GetValue(MeasurementSystem.Imperial, MeasurementScale.Small));
        }

        [Test]
        public void WeightConvertingTests()
        {

            var weight = new Weight(MeasurementSystem.Metric, MeasurementScale.Normal, 1);
            var val = weight.GetValue(MeasurementSystem.Imperial, MeasurementScale.Normal);
            Assert.AreEqual(Weight.kgToLb,val);
            var weight2 = new Weight(MeasurementSystem.Imperial, MeasurementScale.Normal, val);
            Assert.AreEqual(weight,weight2);
        }
    }
}
