using System;
using System.Collections.Generic;
using Depiction.API;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes;
using Depiction.ExporterExtensions;
using Depiction.Serialization;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.Service
{
    [TestFixture]
    public class OgrFileReaderToDepictionElementsTests
    {
        private TempFolderService temp;

        [SetUp]
        protected void Setup()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            temp = new TempFolderService(true);

            var prototypeNames = new List<string> { "Person.dml", "PointOfInterest.dml", "LoadedShapes.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(prototypeNames, temp, false);
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            temp.Close();
        }

        [Test]
        public void SimpleElementToGMLFilesAreRoundtripable()
        {
            var latLong = new LatitudeLongitude(1, 2);
            var file = temp.GetATempFilenameInDir() +".gml";
            var writer = new DepictionGMLFileExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Person");
            Assert.IsNotNull(elemParent);
            elemParent.Position = latLong;
            writer.WriteElements(file, new[] { elemParent },false);
            var ogrParser = new OsGeoFileTypeReaderToDepictionElements();
            var elements = ogrParser.GetElementsFromShapeFile(file, string.Empty, null, "Shape file", "File", true);
            Assert.AreEqual(1,elements.Count);
            foreach(var element in elements)
            {
                Assert.AreEqual(elemParent.ElementType, element.ElementType);
                foreach (var prop in elemParent.OrderedCustomProperties)
                {
                    
                    if(prop.InternalName.Equals("draggable",StringComparison.InvariantCultureIgnoreCase))
                    {
                        UnitTestHelperMethods.PropertyTester(prop.InternalName, false, element);
                    }else
                    {
                        UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, element);
                    }
                }
                Assert.AreEqual(elemParent.ZoneOfInfluence, element.ZoneOfInfluence);
            }
        }

        [Test]
        public void PolygonElementToGMLFilesAreRoundtripable()
        {
            var file = temp.GetATempFilenameInDir() + ".gml";
            var writer = new DepictionGMLFileExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Polygon");
            Assert.IsNotNull(elemParent);
            var linearRing = UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(0, 0), new Coordinate(0, 10), new Coordinate(10, 10), new Coordinate(10, 0),
				         		new Coordinate(0, 0)
				         	});

            ZoneOfInfluence zone = new ZoneOfInfluence(new DepictionGeometry(linearRing));
            elemParent.SetInitialPositionAndZOI(null, zone);
            writer.WriteElements(file, new[] { elemParent }, false);

            var ogrParser = new OsGeoFileTypeReaderToDepictionElements();
            var elements = ogrParser.GetElementsFromShapeFile(file, string.Empty, null, "Shape file", "File", true);
            Assert.AreEqual(1, elements.Count);
            foreach (var element in elements)
            {
                Assert.AreEqual(elemParent.ElementType, element.ElementType);
                foreach (var prop in elemParent.OrderedCustomProperties)
                {
                    UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, element);
                }
                Assert.AreEqual(elemParent.ZoneOfInfluence, element.ZoneOfInfluence);
            }
        }
        
    }
}