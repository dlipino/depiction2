﻿using Depiction.API;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.Service;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.Service
{
    [TestFixture]
    public class DepictionGeocodingServiceTests
    {
        [Test]
        public void DoesDepictionGeocodingServiceSaveLoad()
        {
            var testProduct = new TestProductInformation();
            DepictionAccess.PathService = new ApplicationPathService(testProduct.DirectoryNameForCompany);//DepictionAccess._productInformation);


            var geocodeService = new DepictionGeocodingService();
            var inLatLong = new LatitudeLongitude(1, 2);
            var inLatLong2 = new LatitudeLongitude(4.32342, 34.43539845);

            geocodeService.AddLocationPairToGeneralCache("home", inLatLong);
            geocodeService.AddLocationPairToGeneralCache("home1", inLatLong2);
            geocodeService.AddLocationPairToGeneralCache("home2", new LatitudeLongitude(4, 5));
            geocodeService.SaveGeneralGeoCodeCache();
            //create a new service with no cache
            geocodeService = new DepictionGeocodingService();
            geocodeService.LoadGeneralGeoCodeCache();
            var result = geocodeService.GeoCodeRawStringAddress("home").Position;
            Assert.IsNotNull(result);
            Assert.AreEqual(inLatLong, result);
             result = geocodeService.GeoCodeRawStringAddress("home1").Position;
            Assert.IsNotNull(result);
            Assert.AreEqual(inLatLong2, result);
            DepictionAccess.PathService.RemoveInstanceTempFolder();
            DepictionAccess.PathService = null;

        }
    }
}