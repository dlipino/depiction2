﻿using System.IO;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionPermaTextTests
    {
        [Test]
        public void DoesDepictionPermaTextSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var permaText = new DepictionPermaText();
            permaText.IsEnhancedPermaText = false;

            var file = temp.GetATempFilenameInDir();
            var localName = "SinglePermaText";
            SerializationService.SaveToXmlFile(permaText, file, localName);

            var loadedPermaText = SerializationService.LoadFromXmlFile<DepictionPermaText>(file, localName);

            //For some reasoin the areEqual call does not give the expected result
            //Assert.AreEqual(permaText,loadedPermaText);
            Assert.IsTrue(permaText.Equals(loadedPermaText));

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            temp.Close();
        }
    }
}
