﻿using System.IO;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionElementNodeTests
    {
        [Test]
        public void DoesDepictionElementWaypointSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            var toSave = new DepictionElementWaypoint();
            toSave.Location = new LatitudeLongitude(1, 2);
            Assert.IsFalse(toSave.Equals(null));//A quick null check, should probably be a different test

            var file = temp.GetATempFilenameInDir()+".txt";
            var localName = "ElementWaypoint";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loadedThing = SerializationService.LoadFromXmlFile<DepictionElementWaypoint>(file, localName);

                Assert.IsTrue(Equals(toSave, loadedThing));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
