﻿using System;
using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.Service;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionStoryTests
    {
        [Test]
        public void DoesDepictionStorySerializeAndDeserialize()
        {
            //This is more of a sanity check than an actual unit test.
            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;
            DepictionAccess.PathService = new ApplicationPathService(testProduct.DirectoryNameForCompany);//DepictionAccess._productInformation);
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory("Depiction.UnitTests", "dml", true, DepictionAccess.PathService.DepictionElementPath);
            var temp = new TempFolderService(true);
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();

            DepictionFileFormatInfo fileFormat = new DepictionFileFormatInfo();
            DepictionStory toSave = new DepictionStory();

            toSave.DepictionGeographyInfo = new DepictionGeographicInfo() { DepictionStartLocation = new LatitudeLongitude(45, 23) };
            var elementList = new List<IDepictionElement>();
            for (int i = 0; i < 2; i++)
            {

                var elemParent = new DepictionElementParent();
                elemParent.Tags.Add("Fist tag");
                elemParent.UsePermaText = true;
                elemParent.AddWaypoint(new DepictionElementWaypoint());
                if (i == 0)
                {
                    elemParent.SetInitialPositionAndZOI(new LatitudeLongitude(3, 4), null);
                }
                elementList.Add(elemParent);
            }
            toSave.AddElementGroupToDepictionElementList(elementList);
            for (int i = 0; i < 2; i++)
            {
                var revealer = new DepictionRevealer("Revealer :" + i);

                toSave.AddRevealer(revealer);
            }
            toSave.AddAnnotation(new DepictionAnnotation { MapLocation = new LatitudeLongitude(1d, 2d) });

            Assert.AreEqual(1, toSave.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, toSave.CompleteElementRepository.ElementsUngeoLocated.Count);

            Assert.IsFalse(Equals(toSave, null));
            var file = temp.GetATempFilenameInDir();
            Console.WriteLine(temp.FolderName);
            DepictionStory.SerializeADepiction(temp.FolderName, toSave, fileFormat.DepictionSubtrees);
            var newDepiction = DepictionStory.DeserializeADepiction(temp.FolderName, fileFormat.DepictionSubtrees);

            Assert.AreEqual(toSave, newDepiction);
            temp.Close();
        }

        [Test]
        public void RawPrototypesThatAreAddedWithNoElementLibraryDoesNotCrash()
        {
            var rawPrototypes = new[] { ElementFactory.CreateRawPrototype() };
            var currentDepiction = new DepictionStory();
            currentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(rawPrototypes, false,false);
            Assert.AreEqual(0, currentDepiction.CompleteElementRepository.AllElements.Count);
        }

        [Test]
        public void RawPrototypesWithNoPositionAreAddedAsElementsWithNoPosition()
        {
//            string assemblyName = "Depiction.DefaultResources";
//            string dmlStart = "Depiction.DefaultResources.DefaultElementDefinitions.";
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            var temp = new TempFolderService(true);

            var prototypeNames = new List<string> { "Person.dml", "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(prototypeNames, temp, false);
//            var tempFileName = temp.GetATempFilenameInDir();
//            int count = 0;
//            foreach (var name in prototypeNames)
//            {
//                DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + name, tempFileName);
//                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
//                if (loadedPrototype != null)
//                {
//                    DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
//                    count++;
//                }
//            }
//            Assert.AreEqual(prototypeNames.Count,count);

            var rawPrototypes = new[] { ElementFactory.CreateRawPrototype() };
            var currentDepiction = new DepictionStory();
            currentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(rawPrototypes, false,false);
            Assert.AreEqual(0, currentDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, currentDepiction.CompleteElementRepository.ElementsUngeoLocated.Count);
            DepictionAccess.ElementLibrary = null;
            temp.Close();
        }

        [Test]
        public void RawPrototypesWithEIDoverwriteMatchingEIDs()
        {
//            string assemblyName = "Depiction.DefaultResources";
//            string dmlStart = "Depiction.DefaultResources.DefaultElementDefinitions.";
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            var temp = new TempFolderService(true);

            var prototypeNames = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(prototypeNames,temp,false);
//            var tempFileName = temp.GetATempFilenameInDir();
//            int count = 0;
//            foreach (var name in prototypeNames)
//            {
//                DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + name, tempFileName);
//                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
//                if (loadedPrototype != null)
//                {
//                    DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
//                    count++;
//                }
//            }
//            Assert.AreEqual(prototypeNames.Count, count);
            var proto = ElementFactory.CreateRawPrototype();
            proto.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("eid", "single"));
            var rawPrototypes = new[] { proto };
            var currentDepiction = new DepictionStory();
            currentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(rawPrototypes, false,true);
            Assert.AreEqual(0, currentDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, currentDepiction.CompleteElementRepository.ElementsUngeoLocated.Count);

            var otherProto = ElementFactory.CreateRawPrototype();
            otherProto.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("eid", "single"));
            otherProto.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RandomPRoperty", 1));
            rawPrototypes = new[] { otherProto };
            currentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(rawPrototypes, false,true);
            Assert.AreEqual(0, currentDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, currentDepiction.CompleteElementRepository.ElementsUngeoLocated.Count);
            DepictionAccess.ElementLibrary = null;
            temp.Close();
        }
    }
}
