﻿using System.IO;
using Depiction.CoreModel.DepictionObjects;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionSpecificsTests
    {
        [Test]
        public void DoesDepictionSpecificsSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionGeographicInfo();
            Assert.IsFalse(Equals(toSave, null));
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionGeographicInfo>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
