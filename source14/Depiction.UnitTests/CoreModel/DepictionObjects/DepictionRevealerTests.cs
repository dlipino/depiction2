﻿using System.Collections.Generic;
using System.IO;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionRevealerTests
    {
        [Test]
        public void DoesDepictionRevealerSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            var toSave = new DepictionRevealer();
            toSave.DisplayerName = "Revealer 1";
            toSave.AddProperty("TestProp",15d);
            Assert.IsTrue(toSave.SetPropertyValue("Anchored", false));
            Assert.IsFalse(toSave.SetPropertyValue("random", "thing"));
            var elementList = new List<IDepictionElement>();
            for (int i = 0; i < 2; i++)
            {
                var elemParent = new DepictionElementParent();
                elemParent.UsePermaText = true;
                elemParent.AddWaypoint(new DepictionElementWaypoint());
                elementList.Add(elemParent);
                
            }
            toSave.AddElementList(elementList);
            Assert.IsFalse(Equals(toSave,null));

            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionRevealer>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
