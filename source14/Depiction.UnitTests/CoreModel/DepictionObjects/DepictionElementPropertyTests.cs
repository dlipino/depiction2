﻿using System.IO;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionElementPropertyTests
    {
        [Test]
        public void DoesDepictionElementPropertySaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);
//            DepictionTypeInformationSerialization.SimpleTypeNameToFullNameDictionary.Clear();
            var value = new LatitudeLongitude(double.NaN, double.NaN);
            var property = new DepictionElementProperty("TestProperty",value);
            //property.PropertData.SetInitialValue(value);
            var file = temp.GetATempFilenameInDir();
            var localName = "SingleProperty";

            SerializationService.SaveToXmlFile(property, file, localName);
           
            var loadedProperty = SerializationService.LoadFromXmlFile<DepictionElementProperty>(file, localName);
            //Assert.IsTrue(Equals(property.PropertData,loadedProperty.PropertData));
            Assert.IsTrue(Equals(property,loadedProperty));
           // Assert.AreEqual(property.ValueType, property.PropertData.PropertyType);
            Assert.AreEqual(loadedProperty.ValueType, loadedProperty.ValueType);
            //Assert.AreEqual(property.InitialValue,loadedProperty.InitialValue);

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            temp.Close();
        }
        [Test]
        public void DoesElementPropertyTypeGetSetCorrectly()
        {
            var val = new LatitudeLongitude(double.NaN, double.NaN);
            var property = new DepictionElementProperty("TestProperty",val );

            Assert.AreEqual(val.GetType(),property.ValueType);
        }
        //So these tests might be all over the place :(
        [Test]
        public void DoesElementPropertytReplacingIgnoreCase()
        {
            var origVal = new LatitudeLongitude();
            var modVal = new LatitudeLongitude(1, 2);
            var propName = "TestProperty";
            var element = new DepictionElementParent();
            var origPropCount = element.OrderedCustomProperties.Length;
            var property = new DepictionElementProperty(propName, origVal);
            var property1 = new DepictionElementProperty("TESTPROPERTY", modVal);
            element.AddPropertyOrReplaceValueAndAttributes(property);
            Assert.AreEqual(origPropCount+1,element.OrderedCustomProperties.Length);
            Assert.AreEqual(element.GetPropertyByInternalName(propName, true).Value,origVal);

            element.AddPropertyOrReplaceValueAndAttributes(property1);
            Assert.AreEqual(origPropCount + 1, element.OrderedCustomProperties.Length);
            Assert.AreEqual(element.GetPropertyByInternalName(propName, true).Value, modVal);
        }
        
        [Test]
        public void DoesElementPropertytReplacingIgnoreCaseAndUseDisplayNameWhenAsked()
        {
            //TransferAllPropertiesToDifferentPrototype
            var origVal = "Something from gml";
            var difVal = "Differ";
            var propName = "DisplayName";
            var propDisplayName = "Name";
            var elementPrototype = ElementFactory.CreateRawPrototype();
            var property = new DepictionElementProperty(propName,propDisplayName, origVal);
            elementPrototype.AddPropertyOrReplaceValueAndAttributes(property);
            Assert.AreEqual( origVal,elementPrototype.GetPropertyByInternalName(propName).Value);

            var completeProp = new DepictionElementProperty(propDisplayName, propDisplayName, difVal);
            elementPrototype.AddPropertyOrReplaceValueAndAttributes(completeProp, false, false);
            Assert.AreEqual(2,elementPrototype.OrderedCustomProperties.Length);//Make sure there are two
            //reset back to the orig
            elementPrototype.RemovePropertyIfNameAndTypeMatch(completeProp);
            Assert.AreEqual(1, elementPrototype.OrderedCustomProperties.Length);
            elementPrototype.AddPropertyOrReplaceValueAndAttributes(completeProp, true, false);
            Assert.AreEqual(1, elementPrototype.OrderedCustomProperties.Length);
            Assert.AreEqual( difVal,elementPrototype.GetPropertyByInternalName(propName).Value);

        }
    }
}
