﻿using System.IO;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;
using Depiction.Serialization;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class ZoneOfInfluenceTests
    {
        [Test]
        public void ZOIToStringTest()
        {
            var empy = new ZoneOfInfluence();
            var zoiPoly = GetPoly(new Point(0, 0));
            var zoneOfInfluence = new ZoneOfInfluence(new DepictionGeometry(zoiPoly));
            GeometryToOgrConverter.ZOIGeometryToWkt(zoneOfInfluence);
        }
        [Test]
        public void DoesZoneOfInfluenceSaveLoad()
        {
            var zoiPoly = GetPoly(new Point(0,0));
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);
            var element = new DepictionElementParent();
            var zoneOfInfluence = new ZoneOfInfluence(new DepictionGeometry(zoiPoly));
            element.SetInitialPositionAndZOI(new LatitudeLongitude(),zoneOfInfluence);
            
            Assert.IsFalse(zoneOfInfluence.Equals(null));

            var file = temp.GetATempFilenameInDir();
            var localName = "SingleZOI";
            SerializationService.SaveToXmlFile(zoneOfInfluence, file, localName);

            var loadedZoneOfInfluence = SerializationService.LoadFromXmlFile<ZoneOfInfluence>(file, localName);

            Assert.IsTrue(zoneOfInfluence.Equals(loadedZoneOfInfluence));

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            //This should delete everything in the temp directory
            temp.Close();
        }

        [Test]
        public void ZOIShiftPointTest()
        {
            
            var depGeom = new DepictionGeometry(new LatitudeLongitude(1,1));
            var depZOI = new ZoneOfInfluence(depGeom);
            var expectedGeom = new DepictionGeometry(new LatitudeLongitude(1, 11));
            var shiftLatLong = new LatitudeLongitude(0, 10);
            var shifted = depGeom.TranslateGeometry(shiftLatLong);
            Assert.AreEqual(expectedGeom,shifted);
            var expectedZOI = new ZoneOfInfluence(expectedGeom);
            depZOI.ShiftZoneOfInfluence(shiftLatLong);
            Assert.AreEqual(expectedZOI, depZOI);
        }
        [Test]
        public void ZOIShiftPolygonTest()
        {
            var empy = new ZoneOfInfluence();
            var lati = 0d;
            var longi = 10d;
            var shiftLatLong = new LatitudeLongitude(lati, longi);
            var depGeom = new DepictionGeometry(GetPoly(new Point(0,0)));
            var expGeom = new DepictionGeometry(GetPoly(new Point(longi,lati)));
            Assert.AreEqual(expGeom,depGeom.TranslateGeometry(shiftLatLong));
            var depZOI = new ZoneOfInfluence(depGeom);
            var expZOI = new ZoneOfInfluence(expGeom);
            depZOI.ShiftZoneOfInfluence(shiftLatLong);
            Assert.AreEqual(expZOI, depZOI);
          

        }

        #region Test helpers
        public static IPolygon GetPoly(Point offset)
        {
            GeometryFactory geoFact = new GeometryFactory();
            var x = offset.X;
            var y = offset.Y;
            var coordinates = new CoordinateList
    		                  	{
    		                  		new Coordinate(5+x, 0+y),
    		                  		new Coordinate(5+x, 5+y),
    		                  		new Coordinate(10+x, 5+y),
    		                  		new Coordinate(10+x, 0+y),
    		                  		new Coordinate(5+x, 0+y)
    		                  	};
            return geoFact.CreatePolygon(geoFact.CreateLinearRing(coordinates.ToArray()), null);
        }
        #endregion
    }
}
