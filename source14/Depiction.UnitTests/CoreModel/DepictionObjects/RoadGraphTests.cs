using System.Collections.Generic;
using System.IO;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class RoadGraphTests
    {
        protected RoadSegment CreateEdge(int a, int b)
        {
            var node = new RoadNode();
            node.NodeID = a;
            node.Vertex = new LatitudeLongitude(a, 0);

            var node1 = new RoadNode();
            node1.NodeID = b;
            node1.Vertex = new LatitudeLongitude(0, b);
            var tEdge = new RoadSegment(node, node1);
            tEdge.Name = "road";
            return tEdge;
        }

        [Test]
        public void DoesRoadGraphSaveLoad()
        {
            var temp = new TempFolderService(true);

            var toSave = new RoadGraph();
            var allEdges = new List<RoadSegment>();

            allEdges.Add(CreateEdge(1, 2));
            var disabledEdge = CreateEdge(3, 4);
            allEdges.Add(disabledEdge);
            
            foreach (var ls in allEdges)
            {
                toSave.AddEdge(ls);
            }
            toSave.DisableEdge(disabledEdge);
           
            Assert.IsFalse(Equals(toSave, null));
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<RoadGraph>(file, localName);

                Assert.IsTrue(RoadGraphEquals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }

        public bool RoadGraphEquals(RoadGraph current, object obj)
        {//USed in Equals(obj,obj)
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as RoadGraph;
            if (other == null) return false;
            if (current.DisabledEdges.Count != other.DisabledEdges.Count) return false;
            foreach (var edge in current.DisabledEdges)
            {
                if (!other.DisabledEdges.Contains(edge))
                    return false;
            }
            var edgeCount = current.Graph.EdgeCount;
            if (edgeCount != other.Graph.EdgeCount) return false;
            //if (!Equals(Graph, other.Graph)) return false;

            return true;
        }
    }
}