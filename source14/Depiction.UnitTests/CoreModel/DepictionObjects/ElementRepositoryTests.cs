﻿using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class ElementRepositoryTests
    {
        private TempFolderService tempFolder;
        [SetUp]
        public void Setup()
        {
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);

        }

        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
        }

        [Test]
        public void DoesElementRepositorySaveLoad()
        {
            var temp = new TempFolderService(true);
            var toSave = new ElementRepository();

            var file = temp.GetATempFilenameInDir();
            ElementRepository.SerializeElementRepositoryIntoParts(temp.FolderName,toSave);
            var loaded = ElementRepository.DeserializeElementRepositoryFromParts(temp.FolderName);
//            var localName = "ElementRepository";
//            SerializationService.SaveToXmlFile(toSave, file, localName);
//            var loaded = SerializationService.LoadFromXmlFile<ElementRepository>(file, localName);

            Assert.IsTrue(toSave.Equals(loaded));
            if (File.Exists(file))
            {
                File.Delete(file);
            }

            temp.Close();
        }

        [Test]
        public void DoesAddOrUpdateWorkWithOneElement()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var eidValue = "MyID";
            var changePropName = "ChangeProp";
            var initChangeVal = "One thing";
            var modChangeVal = "Two thing";
            var propDict = new Dictionary<string, object> { { "Eid", eidValue }, { changePropName, initChangeVal } };

            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);
            Assert.IsTrue(prototypeWithEid.HasPropertyByInternalName("eid"));

            var repo = new ElementRepository();

            Assert.AreEqual(0,repo.AllElements.Count);

            IDepictionElement createdElement;
            var updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNotNull(createdElement);
            Assert.IsTrue(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);

            prototypeWithEid.SetPropertyValue(changePropName, modChangeVal);

            updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.AreEqual(1, repo.AllElements.Count);
            Assert.IsNull(createdElement);
            Assert.IsFalse(string.IsNullOrEmpty(updatedKey));
        }

        [Test]
        public void DoesEIDUpateWorkWhenElementTypesDontMatch()
        {
            var dmlsToLoad = new List<string> { "Person.dml","PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var eidValue = "MyID";
            var changePropName = "ChangeProp";
            var initChangeVal = "One thing";
            var modChangeVal = "Two thing";
            var propDict = new Dictionary<string, object> { { "Eid", eidValue }, { changePropName, initChangeVal } };

            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);
            Assert.IsTrue(prototypeWithEid.HasPropertyByInternalName("eid"));

            var repo = new ElementRepository();

            Assert.AreEqual(0, repo.AllElements.Count);

            IDepictionElement createdElement;
            var updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNotNull(createdElement);
            Assert.IsTrue(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);

            var otherPrototype = UnitTestHelperMethods.CreateRawPrototypeWithProps("PointOfInterest", propDict);
            otherPrototype.SetPropertyValue(changePropName, modChangeVal);

            updatedKey = repo.AddOrUpdateRepositoryFromPrototype(otherPrototype, out createdElement, false);
            Assert.AreEqual(1, repo.AllElements.Count);
            Assert.IsNull(createdElement);
            Assert.IsFalse(string.IsNullOrEmpty(updatedKey));

            string propVal;
            var elem = repo.AllElements[0];

            Assert.IsTrue(elem.GetPropertyValue(changePropName,out propVal));
            Assert.AreEqual(modChangeVal,propVal);
        }

        [Test]
        public void DoesIntEIDUpdatedByStringEIDWork()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var eidIntValue = 1234;
            var changePropName = "ChangeProp";
            var initChangeVal = "One thing";
            var modChangeVal = "Two thing";
            var propDict = new Dictionary<string, object> { { "Eid", eidIntValue }, { changePropName, initChangeVal } };

            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);
            Assert.IsTrue(prototypeWithEid.HasPropertyByInternalName("eid"));

            var repo = new ElementRepository();

            Assert.AreEqual(0, repo.AllElements.Count);

            IDepictionElement createdElement;
            var updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNotNull(createdElement);
            Assert.IsTrue(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);

            var eidStringValue = "1234";
            propDict = new Dictionary<string, object> { { "Eid", eidStringValue }, { changePropName, initChangeVal } };

            prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);

            prototypeWithEid.SetPropertyValue(changePropName, modChangeVal);

            updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNull(createdElement);
            Assert.IsFalse(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);
        }
        [Test]
        public void DoesStringEIDElementUpdatedByIntEIDWork()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var eidIntValue = "1234";
            var changePropName = "ChangeProp";
            var initChangeVal = "One thing";
            var modChangeVal = "Two thing";
            var propDict = new Dictionary<string, object> { { "Eid", eidIntValue }, { changePropName, initChangeVal } };

            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);
            Assert.IsTrue(prototypeWithEid.HasPropertyByInternalName("eid"));

            var repo = new ElementRepository();

            Assert.AreEqual(0, repo.AllElements.Count);

            IDepictionElement createdElement;
            var updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNotNull(createdElement);
            Assert.IsTrue(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);

            var eidStringValue = 1234;
            propDict = new Dictionary<string, object> { { "Eid", eidStringValue }, { changePropName, initChangeVal } };

            prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);

            prototypeWithEid.SetPropertyValue(changePropName, modChangeVal);

            updatedKey = repo.AddOrUpdateRepositoryFromPrototype(prototypeWithEid, out createdElement, false);
            Assert.IsNull(createdElement);
            Assert.IsFalse(string.IsNullOrEmpty(updatedKey));
            Assert.AreEqual(1, repo.AllElements.Count);
        }
        [Test]
        public void DoesAddOrUpdateWorkWithManyElements()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var eidValue = "MyID";
            var changePropName = "ChangeProp";
            var initChangeVal = "One thing";
            var modChangeVal = "Two thing";
            var propDictEid = new Dictionary<string, object> { { "Eid", eidValue }, { changePropName, initChangeVal } };
            var propDict = new Dictionary<string, object> { { changePropName, initChangeVal } };
            
            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDictEid);
            var protypeWithOutEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Person", propDict);

            var protoList = new List<IElementPrototype> {prototypeWithEid, protypeWithOutEid};
            var repo = new ElementRepository();

            Assert.AreEqual(0, repo.AllElements.Count);

            List<IDepictionElement> createdElement;
            var updatedKey = repo.AddOrUpdateRepositoryFromPrototypes(protoList, out createdElement, false);
            Assert.AreEqual(2, repo.AllElements.Count);
            Assert.AreEqual(0, updatedKey.Count);
            Assert.AreEqual(2, createdElement.Count);

            prototypeWithEid.SetPropertyValue(changePropName, modChangeVal);
            protypeWithOutEid.SetPropertyValue(changePropName, modChangeVal);

            updatedKey = repo.AddOrUpdateRepositoryFromPrototypes(protoList, out createdElement, false);
            Assert.AreEqual(3, repo.AllElements.Count);
            Assert.AreEqual(1, updatedKey.Count);
            Assert.AreEqual(1, createdElement.Count);
        }

    }
}
