﻿using System.Collections.Generic;
using System.IO;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionPersistence
{
    [TestFixture]
    public class DepictionFileFormatInfoTests
    {
        [Test]
        public void DoesDepictionFileFormatInfoSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionFileFormatInfo(new List<DepictionSubtreeInfoBase> { new DepictionSubtreeInfoBase() });
            //toSave.DepictionFileVersion = 3;
            Assert.IsFalse(toSave.Equals(null));

            var file = temp.GetATempFilenameInDir();
            var localName = "ToSave";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionFileFormatInfo>(file, localName);

                Assert.IsTrue(toSave.Equals(loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
        [Test]
        public void DoesDefaultDepictionFileFormatInfoSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionFileFormatInfo();
            //toSave.DepictionFileVersion = 3;
            Assert.IsFalse(toSave.Equals(null));

            var file = temp.GetATempFilenameInDir();
            var localName = "ToSave";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionFileFormatInfo>(file, localName);

                Assert.IsTrue(toSave.Equals(loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
