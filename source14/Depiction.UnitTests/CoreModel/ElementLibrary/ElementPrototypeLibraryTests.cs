﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ElementLibrary
{
    [TestFixture]
    public class ElementPrototypeLibraryTests
    {
        private TempFolderService tempFolder;
        [SetUp]
        public void Setup()
        {
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);

            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;
            
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();

            DepictionAccess.ProductInformation = null;
        }
        [Test]
        public void DoesElementPrototypeLibrarySaveLoad()
        {
            //This is more of a sanity check than an actual unit test.

            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory("Depiction.UnitTests", "dml", true, tempFolder.FolderName);
            //Since the element prototype library is a single ton, it will get
            //treated as such in the tests
            var toSave = new ElementPrototypeLibrary();
            toSave.SetDefaultPrototypesFromPath(tempFolder.FolderName,false);

            Assert.IsFalse(Equals(toSave, null));

            toSave.SaveElementPrototypeLibraryToPath(tempFolder.FolderName,false);
            var toLoad = new ElementPrototypeLibrary();
            toLoad.SetDefaultPrototypesFromPath(tempFolder.FolderName,false);

            toLoad.UpdateElementPrototypeLibraryFromTempLoadedDpnLocation(tempFolder.FolderName);

            Assert.AreEqual(toSave,toLoad);
        }

        [Test]
        public void PrototypesTakeFromElementLibraryCannotBeChanged()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad,tempFolder,false);
            var prototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("Person");
            Assert.IsNotNull(prototype);

            var propertyCount = prototype.OrderedCustomProperties.Length;
            var nodeCount = prototype.Waypoints.Length;
            var tagCount = prototype.Tags.Count;
            prototype.Tags.Add("Something");

            prototype.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RandomProperty", 1));
            var freshPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("Person");

            Assert.AreEqual(propertyCount,freshPrototype.OrderedCustomProperties.Length);
            Assert.AreEqual(tagCount, freshPrototype.Tags.Count);
            Assert.AreEqual(nodeCount, freshPrototype.Waypoints.Length);
            DepictionAccess.ElementLibrary = null;
        }

        //So these tests might be all over the place :(
        [Test]//This test is semi duplicate of the tranfer properties test, NM tranfer properties just got hit by a sword and will never be the same
        public void IsRawPrototypePropertyReplacementCaseInsensitive()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var origVal = "Something from gml";
            var propName = "NAME";
            var elementPrototype = ElementFactory.CreateRawPrototype();
            var origPropCount = elementPrototype.OrderedCustomProperties.Length;
            var property = new DepictionElementProperty(propName, origVal);
            elementPrototype.AddPropertyOrReplaceValueAndAttributes(property,false);
            elementPrototype.UpdateTypeIfRaw("Person");
            Assert.AreEqual(origPropCount + 1, elementPrototype.OrderedCustomProperties.Length);
            Assert.AreEqual(elementPrototype.GetPropertyByInternalName(propName).Value, origVal);

            var element = ElementFactory.CreateElementFromPrototype(elementPrototype);
            Assert.IsNotNull(element);
            Assert.IsNotNull(element.GetPropertyByInternalName(propName));
            Assert.AreEqual(element.GetPropertyByInternalName(propName).Value, origVal);
        }

        [Ignore("This test seems like a test for a hack fix, so it doesn't seem so useful")]
        [Test]//This test makes sure that raw prototype with a NAME property gets placed in the 
        //displayName property of a real element
        public void IsRawPrototypePropertyNAMETurnedIntoDisplayName()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var origVal = "Something from gml";
            var propName = "NAME";
            var elementPrototype = ElementFactory.CreateRawPrototype();
            elementPrototype.UpdateTypeIfRaw("Person");

            var origElement = ElementFactory.CreateElementFromPrototype(elementPrototype);
            var origPropCount = origElement.OrderedCustomProperties.Length;
            var property = new DepictionElementProperty(propName, origVal);
            elementPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);

            var element = ElementFactory.CreateElementFromPrototype(elementPrototype);
            Assert.AreEqual(origPropCount, element.OrderedCustomProperties.Length);
            Assert.AreEqual(elementPrototype.GetPropertyByInternalName(propName, true).Value, origVal);

            Assert.IsNotNull(element);
            Assert.IsNotNull(element.GetPropertyByInternalName(propName, true));
            Assert.AreEqual(element.GetPropertyByInternalName(propName, true).Value, origVal);
        }
    }
}
