using System.IO;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.ValueTypes;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ElementLibrary
{
    [TestFixture]
    public class DMLReadingTests
    {
        private TempFolderService temp; 
        string assemblyName = "Depiction.UnitTests";
        [SetUp]
        public void Setup()
        {
            temp = new TempFolderService(true);
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(assemblyName, "dml", true, temp.FolderName);
        }
        [TearDown]
        public void TearDown()
        {
            temp.Close();
        }
        [Test]
        public void PrototypesCreatedFromFilesAreNotRawPrototypes()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.ValidTestElement.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);
            Assert.IsNotNull(createdPrototype);
            Assert.IsFalse(createdPrototype.IsRawPrototype);
        }

        [Test]
        public void DoesDMLWithNodesLoad()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.ElementWithNodes.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);
            Assert.IsNotNull(createdPrototype);
            Assert.AreEqual(2,createdPrototype.Waypoints.Length);
        }

        [Test]
        public void DoesAngleLoad()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.ElementWithAngle.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);
            Assert.IsNotNull(createdPrototype);
            UnitTestHelperMethods.PropertyTester("Angle", new Angle { Value = 20 }, createdPrototype);
            //            ZOIShapeType shapeType;            
        }


        [Test]
        public void DoesNewerDMLLoad()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.UserDrawnPolygon.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);
            Assert.IsNotNull(createdPrototype);
            UnitTestHelperMethods.PropertyTester("ZOIShapeType", ZOIShapeType.UserPolygon, createdPrototype);
            //            ZOIShapeType shapeType;
            //            createdPrototype.GetPropertyValue("ZOIShapeType", out shapeType);
            //            Assert.AreEqual(ZOIShapeType.Polygon,shapeType);
        }
        [Test]
        public void DoesSimpleDMLLoadCreateAValidPrototype()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.ValidTestElement.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);

            Assert.AreEqual(6, createdPrototype.OrderedCustomProperties.Length);
            Assert.AreEqual(1, createdPrototype.CreateActions.Keys.Count);
            //Hack for a quick test
            var elem = ElementFactory.CreateElementFromPrototype(createdPrototype);
            Assert.AreEqual(1, elem.CreateActions.Keys.Count);
            Assert.AreEqual(1, elem.GenerateZoiActions.Keys.Count);
            Assert.AreEqual(1, elem.ClickActions.Keys.Count);
        }
        [Test]
        public void DMLWithBadValidationRuleDoesNotBreakXMLRead()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.DMLWithIncompleteValidationRule.dml");
            Assert.IsTrue(File.Exists(fileName));
            var createdPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);

            Assert.AreEqual(1, createdPrototype.OrderedCustomProperties.Length);
        }
    }
}