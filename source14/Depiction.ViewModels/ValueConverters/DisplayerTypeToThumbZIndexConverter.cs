﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ValueConverters
{
    public class DisplayerTypeToThumbZIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var zIndex = 1;
            var param = parameter.ToString();

           if(value is DepictionDisplayerType)
           {
               var type = (DepictionDisplayerType) value;
               switch(type)
               {
                   case DepictionDisplayerType.MainMap:
                       break;
                   case DepictionDisplayerType.Revealer:
                       if (param.Equals("border"))
                       {
                           zIndex = ViewModelZIndexs.RevealerBorderThumbZ;
                       }
                       if(param.Equals("main"))
                       {
                           zIndex = ViewModelZIndexs.RevealerMainThumbZ;
                       }
                       break;
                   case DepictionDisplayerType.TopRevealer:
                       if (param.Equals("border"))
                       {
                           zIndex = ViewModelZIndexs.TopRevealerBorderThumbZ;
                       }
                       if (param.Equals("main"))
                       {
                           zIndex = ViewModelZIndexs.TopRevealerMainThumbZ;
                       }
                       break;
               }

           }
            return zIndex;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
