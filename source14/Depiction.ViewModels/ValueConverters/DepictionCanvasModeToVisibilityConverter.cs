﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ValueConverters
{
    public class DepictionCanvasModeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DepictionCanvasMode)
            {
                var types =parameter.ToString().Split(' ');
                
                var screen = ((DepictionCanvasMode)value).ToString().ToLower();
                foreach (var type in types)
                {
                    if (type.ToLower().Equals(screen))
                    {
                        return Visibility.Visible;
                    }
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CurrentAppDepictionScreen.Welcome;
        }
    }
}
