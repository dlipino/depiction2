﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MenuElementPropertyViewModel : ViewModelBase, IDataErrorInfo
    {
        internal DepictionElementBase OwningElementModel { get; set; }
        internal IElementProperty PropertyModel { get; set; }
        internal bool RepresentsMany { get; set; }
        private object propertyValue;
        private string displayName;
        private bool delete;
        private bool valueChanged;
        private bool isPreview;
        private bool deletable;
        private bool editable;
        private bool isVisibleToUser;
        private bool hovertextChanged;
        private bool isHoverText;

        private DelegateCommand restorePropertyValueCommand;
        private bool IsActiveElementModel
        {
            get
            {
                if (OwningElementModel is IDepictionElement) return true;
                if (RepresentsMany) return true;
                return false;
            }
        }
        #region Properties

        public string InternalName { get { return PropertyModel.InternalName; } }
        public Type FullPropertyType { get { return PropertyModel.ValueType; } }
        public string PropertyTypeString { get { return PropertyModel.ValueType.Name; } }
        public bool ValueChanged { get { return valueChanged; } set { valueChanged = value; NotifyPropertyChanged("ValueChanged"); } }

        public bool IsPreview { get { return isPreview; } set { isPreview = value; NotifyPropertyChanged("IsPreview"); } }
        public string DisplayName
        {
            get { return displayName; }
            set
            {
                ValueChanged = !Equals(PropertyModel, value);
                displayName = value; NotifyPropertyChanged("DisplayName");
            }
        }
        public bool HoverTextChanged { get { return hovertextChanged; } set { hovertextChanged = value; NotifyPropertyChanged("HoverTextChanged"); } }

        public bool IsHoverText
        {
            get { return isHoverText; }
            set
            {
                isHoverText = value; 
                var hoverChange = !Equals(PropertyModel.IsHoverText, value);
                if (hoverChange)
                {
                    HoverTextChanged = true;
                    NotifyPropertyChanged("IsHoverText");
                }
            }
        }
        public int Rank { get { return PropertyModel.Rank; } }
        public object PropertyValue
        {
            get { return propertyValue; }
            set
            {//Hack double to measure on the set because of the converter not knowing what type of measumrent the propertyvalue
                //can be
                if (FullPropertyType.IsSubclassOf(typeof(BaseMeasurement)) && value is double)
                {
                    var measure = propertyValue as BaseMeasurement;
                    if (measure != null)
                    {
                        var numVal = (double)value;
                        ValueChanged = (measure.NumericValue != numVal);

                        measure.NumericValue = numVal;
                    }

                }
                else if (FullPropertyType.Equals(typeof(Angle)) && value is double)
                {
                    var angle = propertyValue as Angle;
                    if (angle != null)
                    {
                        var angleVal = (double)value;
                        ValueChanged = (angle.Value != angleVal);
                        angle.Value = angleVal;
                    }
                }
                else
                {
                    ValueChanged = !Equals(PropertyModel.Value, value);
                    propertyValue = value;
                }
                NotifyPropertyChanged("PropertyValue");
            }
        }

        public bool Delete { get { return delete; } set { delete = value; NotifyPropertyChanged("Delete"); } }

        public bool IsPropertyReadOnly
        {
            get
            {
                if (IsActiveElementModel) return !editable;
                return false;
            }
            set { Editable = !value; NotifyPropertyChanged("IsPropertyReadOnly"); }
        }
        public bool Editable { get { return editable; } set { editable = value; ValueChanged = !Equals(PropertyModel.Editable, value); } }
        public bool IsVisibleToUser { get { return isVisibleToUser; } set { isVisibleToUser = value; ValueChanged = !Equals(PropertyModel.VisibleToUser, value); } }

        public bool Deletable { get { return deletable; } set { deletable = value; ValueChanged = !Equals(PropertyModel.Deletable, value); } }
        public bool IsPropertyRemovable
        {
            get
            {
                if (IsActiveElementModel) return deletable;
                return true;
            }
            set { Deletable = value; NotifyPropertyChanged("IsPropertyRemovable"); }
        }
        #endregion
        #region command and associated methdos
        #region Restor property value command

        public ICommand RestorePropertyValueCommand
        {
            get
            {
                if (restorePropertyValueCommand == null)
                {
                    restorePropertyValueCommand = new DelegateCommand(RestorePropertyValue, HasPropertyValueChanged);
                    restorePropertyValueCommand.Text = "Restore original property values";
                }
                return restorePropertyValueCommand;
            }
        }
        bool HasPropertyValueChanged()
        {
            return ValueChanged;
        }

        void RestorePropertyValue()
        {
            propertyValue = GetDeepCopyOfPropertyValue(PropertyModel.Value);
            displayName = PropertyModel.DisplayName;
            deletable = PropertyModel.Deletable;
            editable = PropertyModel.Editable;
            isHoverText = PropertyModel.IsHoverText;
            isVisibleToUser = PropertyModel.VisibleToUser;
            ValueChanged = false;
            NotifyPropertyChanged("PropertyValue");
            NotifyPropertyChanged("Deletable");
            NotifyPropertyChanged("IsHoverText");
            NotifyPropertyChanged("IsVisibleToUser");
            NotifyPropertyChanged("DisplayName");
        }
        #endregion
        #endregion
        #region Constructor

        public MenuElementPropertyViewModel(IElementProperty inPropertyModel, DepictionElementBase owningElement)
        {
            OwningElementModel = owningElement;
            PropertyModel = inPropertyModel;
            propertyValue = GetDeepCopyOfPropertyValue(PropertyModel.Value);
            displayName = PropertyModel.DisplayName;
            deletable = PropertyModel.Deletable;
            editable = PropertyModel.Editable;
            isHoverText = PropertyModel.IsHoverText;
            isVisibleToUser = PropertyModel.VisibleToUser;
            PropertyModel.PropertyChanged += PropertyModel_PropertyChanged;
        }

        #endregion
        #region Destruction
        protected override void OnDispose()
        {
            PropertyModel.PropertyChanged -= PropertyModel_PropertyChanged;
            OwningElementModel = null;
            PropertyModel = null;
            base.OnDispose();
        }
        #endregion

        #region Event connecting methods

        void PropertyModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (propName.Equals("Value"))
            {
                propertyValue = GetDeepCopyOfPropertyValue(PropertyModel.Value);
                NotifyPropertyChanged("PropertyValue");
            }
            else if (propName.Equals("DisplayName"))//bad this should be 
            {
                DisplayName = PropertyModel.DisplayName;
            }
        }

        #endregion

        #region Methods
        protected object GetDeepCopyOfPropertyValue(object value)
        {
            if (value is IDeepCloneable)
            {
                return ((IDeepCloneable)(value)).DeepClone();
            }
            return value;
        }

        public void ApplyVMChangesToModel()
        {
            if (!ValueChanged && !IsPreview) return;
            if (OwningElementModel != null)
            {
                //Deal with images This gets duplicated 
                if (FullPropertyType.Equals(typeof(DepictionIconPath)))
                {
                    var app = Application.Current as IDepictionApplication;
                    if (app != null)
                    {
                        var dict = Application.Current.Resources;
                        var iconPath = (DepictionIconPath)(PropertyValue);
                        var smallPath = Path.GetFileName(iconPath.Path);
                        if (!dict.Contains(smallPath))
                        {
                            string keyValue;
                            BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(iconPath.Path, out keyValue);
                            iconPath.Path = smallPath;
                        }
                    }
                }
                if (IsPreview)
                {
                    if (OwningElementModel.AddPropertyOrReplaceValueAndAttributes(PropertyModel) != false)
                    {
                        ValueChanged = false;
                        IsPreview = false;
                    }
                }

                //Hack for position
                if (PropertyModel.InternalName.Equals("Position", StringComparison.InvariantCultureIgnoreCase) && OwningElementModel is DepictionElementParent)
                {
                    var elementParent = OwningElementModel as DepictionElementParent;
                    var origPos = elementParent.Position;
                    var modPos = PropertyValue;
                    if (!Equals(origPos, modPos))
                    {
                        elementParent.Position = GetDeepCopyOfPropertyValue(PropertyValue) as ILatitudeLongitude;
                        ValueChanged = false;
                    }
                }
                else
                {
                    //Send the value to the model but ignore model changes
                    PropertyModel.PropertyChanged -= PropertyModel_PropertyChanged;
                    if (OwningElementModel.SetPropertyValue(PropertyModel.InternalName, GetDeepCopyOfPropertyValue(PropertyValue)) != false)
                    {
                        ValueChanged = false;
                    }

                    var propData = PropertyModel;
                    if (propData != null) propData.DisplayName = displayName;
                    PropertyModel.Deletable = deletable;
                    PropertyModel.Editable = editable;
                    PropertyModel.IsHoverText = isHoverText;
                    PropertyModel.VisibleToUser = isVisibleToUser;

                    PropertyModel.PropertyChanged += PropertyModel_PropertyChanged;
                }

            }
        }
        #endregion

        #region Implementation of IDataErrorInfo

        private string errorString = string.Empty;
        public string this[string columnName]
        {
            get
            {
                errorString = string.Empty;
                if (PropertyValue == null)
                {
                    errorString = "Could not convert to type.";
                    return errorString;
                }

                if (PropertyModel.ValidationRules == null) return null;
                foreach (var rule in PropertyModel.ValidationRules)
                {
                    var result = rule.Validate(PropertyValue);
                    if (result.ValidationOutput == ValidationResultValues.InValid)
                    {
                        errorString = result.Message;
                        return errorString;
                    }
                }
                return null;
            }
        }

        public string Error
        {
            get { return errorString; }
        }

        #endregion
    }
}
