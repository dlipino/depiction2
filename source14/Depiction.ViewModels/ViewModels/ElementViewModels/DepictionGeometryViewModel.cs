﻿using System.Collections.Generic;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class DepictionGeometryViewModel : ViewModelBase
    {
        List<EnhancedPointListWithChildren> wpfUsablePolygon = new List<EnhancedPointListWithChildren>();

        private Geometry outlineGeometry;
        private Brush outlineColor = new SolidColorBrush(Colors.Olive);
        private double strokeSize = 1;
        #region Properties

        public Geometry OutlineGeometry { get { return outlineGeometry; } private set { outlineGeometry = value; } }

        public Brush OutlineColor
        {
            get { return outlineColor; }
            set
            {
                outlineColor = value;
                if (outlineColor.CanFreeze) { outlineColor.Freeze(); }
            }
        }
        public double StrokeSize
        {
            get
            {
                return strokeSize;
            }
            set
            {
                strokeSize = value;
                NotifyPropertyChanged("StrokeSize");
            }
        }

        #endregion
        #region Constructor
        public DepictionGeometryViewModel(DepictionGeometry geometryModel, IGeoConverter realWorldToPixelWorldConverter)
        {
            wpfUsablePolygon =
                DepictionGeometryToEnhancedPointListWithChildrenConverter.IGeometryToEnhancedPointListWithChildren(
                    geometryModel, realWorldToPixelWorldConverter);

            OutlineColor = new SolidColorBrush(Colors.Olive);
            OutlineGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(wpfUsablePolygon);
        }
        #endregion
    }
}