﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MapZoneOfInfluenceViewModel : ViewModelBase, IMapZoneOfInfluenceViewModel
    {
        public static double ViewModelScaleHack = 1d;
        private  double defaultLineThickness = 2d;
        private IDepictionElement ElementModel { get; set; }
        private IZoneOfInfluence ZOIModel { get; set; }
        private Brush fillBrush;
        private Brush borderBrush;
        private List<EnhancedPointListWithChildren> zoiToDraw;
        private Geometry zoiDrawGeometry;
        private double lineThickness;

        #region Properties
        public bool ViewModified { get; set; }
        //Also sets the ZOIDrawGeometry, kind of like update
        public List<EnhancedPointListWithChildren> ZOIToDraw
        {
            get { return zoiToDraw; }
            set
            {
                zoiToDraw = value;
//                if (value != null)
//                {
                    
                    ZOIDrawGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(zoiToDraw);
                    ViewModified = true;
//                }
            }
        }
        public Geometry ZOIDrawGeometry
        {
            get { return zoiDrawGeometry; }
            private set { zoiDrawGeometry = value; NotifyPropertyChanged("ZOIDrawGeometry"); }
        }

        public Brush FillBrush
        {
            get { return fillBrush; }
            set
            {
                fillBrush = value;
                if (fillBrush.CanFreeze) fillBrush.Freeze();
                NotifyPropertyChanged("FillBrush");
            }
        }
        public Brush BorderBrush
        {
            get { return borderBrush; }
            set
            {
                borderBrush = value; if (borderBrush.CanFreeze) borderBrush.Freeze();
                NotifyPropertyChanged("BorderBrush");
            }
        }

        public double LineThickness
        {
            get
            {
                return lineThickness;
            }
            set
            {
                defaultLineThickness = value;
                UpdateLineThickness(ViewModelScaleHack);
            }
        }

        #endregion 

        #region Temp hacks for line thickness


        public DepictionGeometryType ZOIGeometryType
        {
            get { return ZOIModel.DepictionGeometryType; }
        }

        public ZOIShapeType BaseShapeType
        {
            get
            {
                ZOIShapeType zoiType;
                if (ElementModel.GetPropertyValue("ZOIShapeType", out zoiType))
                {
                    return zoiType;
                }
                return ZOIShapeType.Point;
            }
        }

        public void UpdateLineThickness(double multiplier)
        {
            ViewModelScaleHack = multiplier;
            lineThickness = defaultLineThickness/multiplier;
            NotifyPropertyChanged("LineThickness");
        }
        #endregion

        #region Constructor
        public MapZoneOfInfluenceViewModel(IDepictionElement element)//List<EnhancedPointListWithChildren> zoiPoints)
        {
            ElementModel = element;
            ZOIModel = element.ZoneOfInfluence;
            Color color;
            element.GetPropertyValue("ZOIBorder", out color);
            BorderBrush = new SolidColorBrush(color);//brushConverter.ConvertFromString(color) as Brush);
            element.GetPropertyValue("ZOIFill", out color);
            FillBrush = new SolidColorBrush(color);//brushConverter.ConvertFromString(color) as Brush);
            double val;
            if (!ElementModel.GetPropertyValue("ZOILineThickness", out val))
            {
                val = defaultLineThickness;
            }else
            {
                defaultLineThickness = val;
            }
            lineThickness = val / ViewModelScaleHack;
            UpdateZOIViewFromModel();
        }
        #endregion
        
        #region Destructro

        protected override void OnDispose()
        {
            ElementModel = null;
            ZOIModel = null;
            if(ZOIToDraw != null) ZOIToDraw.Clear();
            ZOIToDraw = null;
            zoiDrawGeometry = null;
            base.OnDispose();
        }

        #endregion
        
        #region Methods
        //This method no longer makes sense to me (davidl)

        public void UpdateModelFromViewModel()
        {
            if (ViewModified)
            {
                var geo =
                    DepictionGeometryToEnhancedPointListWithChildrenConverter.
                        EnhancedPointListWithChildrenListToIGeometry(ZOIToDraw,
                                                                     ZOIModel.DepictionGeometryType.ToString(),
                                                                      DepictionAccess.GeoCanvasToPixelCanvasConverter);
                ElementModel.SetZOIGeometryAndUpdatePosition(geo);
            }
            ViewModified = false;
        }
        //This method is also a mystery (davidl)
        public void UpdateZOIViewFromModel()
        {
            ZOIToDraw = DepictionGeometryToEnhancedPointListWithChildrenConverter.
                IGeometryToEnhancedPointListWithChildren(ZOIModel.Geometry as DepictionGeometry, DepictionAccess.GeoCanvasToPixelCanvasConverter);
            NotifyPropertyChanged("LineThickness");
        }
        public void CloseAndFillZOIToDraw()
        {
            foreach (var enhancedList in ZOIToDraw)
            {
                enhancedList.IsClosed = true;
                enhancedList.IsFilled = true;
            }
            ZOIToDraw = ZOIToDraw;
            UpdateModelFromViewModel();
            //ZOIDrawGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(ZOIToDraw);
        }
        public void ShiftZOI(Point offset)
        {
            foreach (var enhancedList in ZOIToDraw)
            {
                enhancedList.ShiftPoints(offset);
            }
            ZOIDrawGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(ZOIToDraw);
            //NotifyPropertyChanged("ZOIDrawGeometry");
        }

        
        //The boolean is a hack (hopefully temp) to draw the full zoi as the user adds it. It should only be set to true
        //when you want a visual of what the zoi will look like.
        public bool AddPointToEnd(Point pointToAdd)
        {
            if (ZOIToDraw == null) return false;
            if (ZOIToDraw.Count != 1) return false;
            var mainOutLine = zoiToDraw[0];
            if (mainOutLine.IsClosed) return false;
           
            mainOutLine.Outline.Add(pointToAdd);
            ZOIToDraw = ZOIToDraw;
            return true;
        }
        public bool AddVisualPointToEnd(Point pointToAdd, bool drawClosed)
        {
            if (ZOIToDraw == null) return false;
            if (ZOIToDraw.Count != 1) return false;
            var mainOutLine = zoiToDraw[0];
            if (mainOutLine.IsClosed) return false;
                var listCopy = new List<EnhancedPointListWithChildren>();
                var pointListCopy = new EnhancedPointListWithChildren();

                listCopy.Add(pointListCopy);
                foreach (var point in mainOutLine.Outline)
                {
                    pointListCopy.Outline.Add(point);
                }
                pointListCopy.Outline.Add(pointToAdd);
                if (drawClosed)
                {
                    pointListCopy.IsClosed = true;
                    pointListCopy.IsFilled = true;
                }
                ZOIDrawGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(listCopy);
                return true;
            
        }
        //Used when Zoi is not complete
        public bool RemoveEndPoint()
        {
            if (ZOIToDraw == null) return false;
            if (ZOIToDraw.Count != 1) return false;
            var mainOutLine = zoiToDraw[0];
            if (mainOutLine.IsClosed) return false;
            var pointCount = mainOutLine.Outline.Count;
            if (pointCount < 2) return false;
            mainOutLine.Outline.RemoveAt(pointCount - 1);
            ZOIToDraw = ZOIToDraw;
            return true;
        }
        //Used when the zoi is closedand complete
        public bool RemovePoint(Point pointToRemove)
        {
            //Hopefully there are not duplicates
            var minPointCount = 3;
                
            if (ElementModel != null)
            {
                var zoiType = ZOIShapeType.Point;
                if (ElementModel.GetPropertyValue("ZOIShapeType", out zoiType))
                {
                    if (zoiType.Equals(ZOIShapeType.Line) || zoiType.Equals(ZOIShapeType.UserLine))
                    {
                        minPointCount = 2;
                    }
                }
            }
            for (int i = 0; i < ZOIToDraw.Count; i++)
            {
                var pointList = ZOIToDraw[i];

                if (pointList.Outline.Contains(pointToRemove) && pointList.Outline.Count > minPointCount)
                {
                    pointList.Outline.Remove(pointToRemove);
                    ZOIToDraw = ZOIToDraw;
                    return true;
                }
                //Not messign with holes yets
            }
            return false;
        }

        public bool InsertPointBefore(Point pointToInsertBefore, Point newPoint)
        {
            //Hopefully there are not duplicates
            for (int i = 0; i < ZOIToDraw.Count; i++)
            {
                var pointList = ZOIToDraw[i];
                var index = pointList.Outline.IndexOf(pointToInsertBefore);
                if (index != -1)
                {
                    if (index == 0) pointList.Outline.Add(newPoint);
                    else
                    {
                        pointList.Outline.Insert(index, newPoint);
                    }

                    ZOIToDraw = ZOIToDraw;
                    return true;
                }
                //Not messign with holes yets
            }
            return false;
        }
        #endregion
    }
}