﻿using System.Windows;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MapAnnotationViewModel : ViewModelBase, IMapDraggable
    {
        public IDepictionAnnotation annotationModel;
        private IGeoConverter pixelToMapConverter;//Temp. When there is a global depiction story converter, remove this
        private const int zoomInMax = 3;
        private const int zoomOutMax = 2;
        private const double BaseFontSize = 14;
        private const double BaseBorderThickness = 1;
        private const double BasePadding = 4;
        private const double BaseIconSize = 20;

        #region Variables

        private double scaleWhenCreated;
        private Brush backgroundColor = Brushes.OldLace;
        private Brush foregroundColor = Brushes.NavajoWhite;

        private Visibility collapsed = Visibility.Visible;
        private string annotationText = "This is default annotation text.";
        private double width = 200;
        private double height = 50;
        private double contentLocationX = 20;
        private double contentLocationY = 25;

        TranslateTransform elementImagePosition = new TranslateTransform();
        #endregion

        #region Properties
        public string AnnotationID { get { return annotationModel.AnnotationID; } }
        public ImageSource IconSource
        {
            get
            {
                var iconPath = annotationModel.IconPath;
                if (iconPath.IsValid)
                {
                    switch (iconPath.Source)
                    {
                        case IconPathSource.EmbeddedResource:
                            object rawImage = Application.Current.Resources[iconPath.Path];
                            if (rawImage is Drawing)
                                return new DrawingImage(rawImage as Drawing);
                            if (rawImage is ImageSource)
                                return rawImage as ImageSource;
                            break;
                    }
                }

                return Application.Current.Resources["Depiction.Default"] as ImageSource;
            }
        }
        public Brush BackgroundColor//SolidColorBrush?
        {
            get { return backgroundColor; }
            set
            {
                backgroundColor = value;
                if (backgroundColor.CanFreeze) backgroundColor.Freeze();
                annotationModel.BackgroundColor = backgroundColor.ToString();

                NotifyPropertyChanged("BackgroundColor");
            }
        }
        public Brush ForegroundColor
        {
            get { return foregroundColor; }
            set
            {
                foregroundColor = value;
                if (foregroundColor.CanFreeze) foregroundColor.Freeze();
                annotationModel.ForegroundColor = foregroundColor.ToString();//Go one way for now
                NotifyPropertyChanged("ForegroundColor");
            }
        }
        public double ScaleWhenCreated
        {
            get { return scaleWhenCreated; }
        }
        public Visibility AnnotationVisibility { get; private set; }

        public Visibility Collapsed
        {
            get { return collapsed; }
            set
            {
                collapsed = value;
                if(annotationModel != null) annotationModel.IsTextCollapsed = (collapsed == Visibility.Collapsed) ? true : false;
                NotifyPropertyChanged("Collapsed");
            }
        }

        public string AnnotationText
        {
            get { return annotationText; }
            set
            {
                annotationText = value;
                if (annotationModel != null) annotationModel.AnnotationText = value;
                NotifyPropertyChanged("AnnotationText");
            }
        }
        public double FontSize
        {
            get { return BaseFontSize; }
        }
        public double Padding
        {
            get { return BasePadding; }
        }
        public double IconSize
        {
            get { return BaseIconSize; }
        }

        public double BorderThickness
        {
            get { return BaseBorderThickness; }
        }
        public double Width
        {
            get { return width; }
            set
            {
                var oldWidth = width;
                width = value;
                if (width <= 15)
                {
                    width = oldWidth;
                } 
                if (annotationModel != null)
                {
                    annotationModel.PixelWidth = width;
                }
                NotifyPropertyChanged("Width");

            }
        }
        public double Height
        {
            get { return height; }
            set
            {
                var oldHeight = height;
                height = value;
                if (height <= 15) height = oldHeight;
                if (height >= 300) height = oldHeight;
                if (annotationModel != null)
                {
                    annotationModel.PixelHeight = height;
                }
                NotifyPropertyChanged("Height");
            }
        }
        public double ContentLocationX
        {
            get { return contentLocationX; }
            set
            {
                contentLocationX = value;
                if (annotationModel != null)
                {
                    annotationModel.ContentLocationX= contentLocationX;
                }
                NotifyPropertyChanged("ContentLocationX");
            }
        }
        public double ContentLocationY
        {
            get { return contentLocationY; }
            set
            {
                contentLocationY = value;
                if(annotationModel != null)
                {
                    annotationModel.ContentLocationY = contentLocationY;
                }
                NotifyPropertyChanged("ContentLocationY");
            }
        }
        public bool IsElementIconDraggable { get { return true; } }
        public bool IsElementZOIDraggable { get { return true; } }

        public TranslateTransform ElementImagePosition
        {
            get { return elementImagePosition; }
        }
        public TranslateTransform IconCenterTransform { get; private set; }
        public bool EditingProperties{ get; set;}//Indicates if there is a dialog that is editing the properties
        #endregion

        #region COnstructor

        public MapAnnotationViewModel():this(null,1,null){}
        public MapAnnotationViewModel(double currentScale) : this(null, currentScale,null) { }
        public MapAnnotationViewModel(IDepictionAnnotation annotation,double currentScale, IGeoConverter pixelToGeoConverter)
        {
            AnnotationVisibility = Visibility.Visible;
            scaleWhenCreated = currentScale;
            pixelToMapConverter = pixelToGeoConverter;
            IconCenterTransform = new TranslateTransform(-IconSize / 2, -IconSize / 2);
            if (annotation != null)
            {
                annotationModel = annotation;
                if(annotation.ScaleWhenCreated.Equals(double.NaN))
                {
                    annotation.ScaleWhenCreated = currentScale;
                }
                scaleWhenCreated = annotation.ScaleWhenCreated;
                //            BackgroundColor = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f8eb8f"));//As reference
                var brushConverter = new BrushConverter();
                
                if (!string.IsNullOrEmpty(annotationModel.ForegroundColor))
                {
                    foregroundColor = brushConverter.ConvertFromString(annotationModel.ForegroundColor) as Brush;
                }else
                {
                    annotationModel.ForegroundColor = foregroundColor.ToString();
                }
                if(!string.IsNullOrEmpty(annotationModel.BackgroundColor))
                {
                    backgroundColor = brushConverter.ConvertFromString(annotationModel.BackgroundColor) as Brush;
                }else
                {
                    annotationModel.BackgroundColor = backgroundColor.ToString();
                }
                collapsed = annotationModel.IsTextCollapsed ? Visibility.Collapsed : Visibility.Visible;
                annotationText = annotationModel.AnnotationText;
                contentLocationX = annotationModel.ContentLocationX;
                contentLocationY = annotationModel.ContentLocationY;
                width = annotationModel.PixelWidth;
                height = annotationModel.PixelHeight;

                if(pixelToMapConverter != null)
                {
                    SetElementPosition(pixelToMapConverter.WorldToGeoCanvas(annotationModel.MapLocation));
                }
            }
        }
        #endregion

        #region Helper methods
        public void SetElementPosition(Point position)
        {
            elementImagePosition.X = position.X;
            elementImagePosition.Y = position.Y;
            if(annotationModel != null && pixelToMapConverter != null)
            {
                annotationModel.MapLocation = pixelToMapConverter.GeoCanvasToWorld(position);
            }
        }

        public void AdjustVisibilityBasedOnVisibleScale(double scale)
        {
            if (scale > ScaleWhenCreated * zoomInMax
                || scale < (ScaleWhenCreated / zoomOutMax))
                AnnotationVisibility = Visibility.Hidden;
            else
                AnnotationVisibility = Visibility.Visible;

            NotifyPropertyChanged("AnnotationVisibility");
//            NotifyPropertyChanged("BorderThickness");
        }
        public void UpdateModelValuesFromViewModel()
        {
            
        }
        #endregion
    }
}