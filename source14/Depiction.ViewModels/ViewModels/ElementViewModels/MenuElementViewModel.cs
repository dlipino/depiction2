﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypeConverters;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MenuElementViewModel : ViewModelBase
    {
        IDepictionElement ElementModel { get; set; }
        private string normalDisplayName;
        private string hoverText;
        private string enhancedHoverText;

        private bool isSelected;

        #region Properties
        public string ElementID { get { return ElementModel.ElementKey; } }

        public string TypeDisplayName
        {
            get
            {//TODO what is going on here, why does this get called on dispose
                if (ElementModel != null)
                    return ElementModel.TypeDisplayName;
                return string.Empty;
            }
        }
        public string DisplayName
        {
            get
            {
                if (normalDisplayName == null)
                {
                    normalDisplayName = ElementModel.GetToolTipText(", ", ElementModel.UsePropertyNameInHoverText,true);
                }
                return normalDisplayName;
            }
        }

        public string HoverText
        {
            get
            {
                if (hoverText == null)
                {
                    hoverText = ElementModel.GetToolTipText(Environment.NewLine, ElementModel.UsePropertyNameInHoverText,false);
                }
                return hoverText;
            }
        }
        public string EnhancedHoverText
        {
            get
            {
                if (enhancedHoverText == null)
                {
                    enhancedHoverText = ElementModel.GetToolTipText(Environment.NewLine, ElementModel.UsePropertyNameInHoverText,true);
                }
                return enhancedHoverText;
            }
        }

        public ImageSource IconSource
        {//This should be different from FullImageSource in the sense that this should be thumb of a real image
            get
            {
                var iconPath = ElementModel.IconPath;
                if (!string.IsNullOrEmpty(ElementModel.ImageMetadata.ImageFilename))
                {
                    iconPath = ElementModel.ImageMetadata.DepictionImageName;
                }
                return DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath);
            }
        }

        public List<string> Tags
        {
            get { return ElementModel.Tags.ToList(); }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value; NotifyPropertyChanged("IsSelected");
            }
        }

        #endregion

        #region Constructor

        public MenuElementViewModel(IDepictionElement elementModel)
        {
            ElementModel = elementModel;
            ElementModel.PropertyChanged += ElementModel_PropertyChanged;
        }

        #endregion

        #region Destruction

        protected override void OnDispose()
        {
            if (ElementModel != null)
            {
                ElementModel.PropertyChanged -= ElementModel_PropertyChanged;
            }
            ElementModel = null;
            base.OnDispose();
        }

        #endregion

        #region Methods
       

        void ElementModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("HoverText", StringComparison.OrdinalIgnoreCase)||
                e.PropertyName.Equals("UseEnhancedPermaText", StringComparison.OrdinalIgnoreCase))
            {
                normalDisplayName = ElementModel.GetToolTipText(", ", ElementModel.UsePropertyNameInHoverText,false);
                NotifyPropertyChanged("DisplayName");
                hoverText = ElementModel.GetToolTipText(Environment.NewLine, ElementModel.UsePropertyNameInHoverText,false);
                enhancedHoverText = ElementModel.GetToolTipText(Environment.NewLine, ElementModel.UsePropertyNameInHoverText, true);
                NotifyPropertyChanged("HoverText");
                if(ElementModel.UseEnhancedPermaText)
                {
                    NotifyPropertyChanged("UseEnhancedPermaText");
                }
            }else if(e.PropertyName.Equals("iconpath",StringComparison.InvariantCultureIgnoreCase))
            {
                NotifyPropertyChanged("IconSource");
            }
        }

        #endregion

    }
}
