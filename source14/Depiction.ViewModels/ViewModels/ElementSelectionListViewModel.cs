﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Data;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels
{
    //Manage content view model,or maybe just half of it
    public class ElementSelectionListViewModel : ViewModelBase
    {
        #region Variables
        private ListByMode groupingMode = ListByMode.NotSet;
        private string selectionFilter = string.Empty;

        private CollectionViewSource ManageElementsCollection { get; set; }
        #endregion

        #region Properties
        public bool ClearSelectionsOnFilter { get; set; }
        public string SelectionFilter
        {
            get { return selectionFilter; }
            set
            {
                if(selectionFilter == value) return;
                selectionFilter = value;
                ManageElementsCollection.Filter -= CollectionViewSource_Filter;
                if (!string.IsNullOrEmpty(selectionFilter))
                {
                    ManageElementsCollection.Filter += CollectionViewSource_Filter;
                }
                if(ClearSelectionsOnFilter)
                {
                    foreach (var element in originalElementList)
                    {
                        element.IsSelected = false;
                    } 
                }
                NotifyPropertyChanged("SelectionFilter");
            }
        }
        public int SelectedCount { get { return SelectedElementIDs.Count; } }
        public int TotalElementCount { get { return originalElementList.Count; } }
        public Dictionary<ListByMode, string> GroupingSourceList { get; set; }
        public ListByMode GroupingMode
        {
            get { return groupingMode; }
            set
            {

                SetElementGrouping(groupingMode, value);
                groupingMode = value;
                NotifyPropertyChanged("GroupingMode");
            }
        }
        public object ElementBindList { get; private set; }
        protected Dictionary<string, MenuElementViewModel> originalElementDictionary = new Dictionary<string, MenuElementViewModel>();
        protected ObservableCollection<MenuElementViewModel> originalElementList = new ObservableCollection<MenuElementViewModel>();

        public List<string> SelectedElementIDs
        {
            get
            {
                var result = originalElementList.Where(t => t.IsSelected).Select(t => t.ElementID).ToList();
                return result;
            }
        }
        #endregion

        #region Constructor

        public ElementSelectionListViewModel(IEnumerable<IDepictionElement> initialElements)
        {
            var vmList = new List<MenuElementViewModel>();
            foreach(var element in initialElements)
            {
                vmList.Add(new MenuElementViewModel(element));
            }
            originalElementDictionary = vmList.ToDictionary(t => t.ElementID);
            originalElementList = new ObservableCollection<MenuElementViewModel>(originalElementDictionary.Values);
            GroupingSourceList = new Dictionary<ListByMode, string>
                             {
                                 {ListByMode.All, "Show all"},
                                 {ListByMode.Types, "List by types"},
                                 {ListByMode.Tags, "List by tags"}
                             };
            ManageElementsCollection = new CollectionViewSource();
            ManageElementsCollection.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
            ManageElementsCollection.Source = originalElementList;
            GroupingMode = ListByMode.Types;
        }

        #endregion

        #region destrocutr

        protected override void OnDispose()
        {
            ManageElementsCollection.Filter -= CollectionViewSource_Filter;
            foreach(var elem in originalElementDictionary.Values)
            {
                elem.Dispose();
            }
            foreach(var elem in originalElementList)
            {
                elem.Dispose();
            }
            originalElementDictionary.Clear();
            originalElementList.Clear();

            ManageElementsCollection = null;
            originalElementList = null;
            originalElementDictionary = null;
            base.OnDispose();
        }
        #endregion

        #region public helpers

        public void SelectElementsById(IEnumerable<string> elementIdList, bool addToExisting)
        {
            var statusChanged = false;
            if(originalElementList == null)
            {
               // UpdateSelectedCount();
                return;
            }
            if (!addToExisting)
            {
                foreach (var element in originalElementList)
                {
                    element.IsSelected = false;
                }
                statusChanged = true;
            }
            foreach(var id in elementIdList)
            {
                if(originalElementDictionary.ContainsKey(id))
                {
                    if (originalElementDictionary[id].IsSelected != true)
                    {
                        if(!statusChanged) statusChanged = true;
                        originalElementDictionary[id].IsSelected = true;
                    }
                }
            }
            //This is actually kind of strange, the count gets updated automatically, but the selected change does
            //not trigger which causes issues with other commands that use it as a paramter
            if(statusChanged)
            {
                UpdateSelectedCount();
            }

        }
        public void DeselectElementsById(IEnumerable<string> elementIdList)
        {

            foreach (var id in elementIdList)
            {
                if (originalElementDictionary.ContainsKey(id))
                {
                    originalElementDictionary[id].IsSelected = false;
                }
            }
            //This is actually kind of strange, the count gets updated automatically, but the selected change does
            //not trigger which causes issues with other commands that use it as a paramter
            UpdateSelectedCount();
        }


        public void UpdateElementTagList()//Hoepuflly
        {
            if (GroupingMode.Equals(ListByMode.Tags))
            {
                SetElementGrouping(ListByMode.NotSet, ListByMode.Tags);
            }
        }

        public void AddElementVMList(IEnumerable<MenuElementViewModel> elementVMs)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<MenuElementViewModel>>(AddElementVMList),
                    elementVMs);
                return;
            }
            if (elementVMs == null) return;
            if (elementVMs.Count() < 40)
            {
                foreach (var elemVM in elementVMs)
                {
                    elemVM.IsSelected = false;
                    if (!originalElementDictionary.ContainsKey(elemVM.ElementID))
                    {
                        originalElementList.Add(elemVM);
                        originalElementDictionary.Add(elemVM.ElementID, elemVM);
                    }
                }
            }
            else
            {
                //Dispose call?
                originalElementDictionary = originalElementList.Concat(elementVMs).ToDictionary(t => t.ElementID);
                originalElementList = new ObservableCollection<MenuElementViewModel>(originalElementDictionary.Values);
                ManageElementsCollection.Source = originalElementList;
                SetElementGrouping(ListByMode.NotSet, GroupingMode);
            }
        }
        //This should remove by element key only
        public void RemoveElementByKeyVMList(List<string> elementVMKeys)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<string>>(RemoveElementByKeyVMList),
                    elementVMKeys);
                return;
            }
            if (elementVMKeys == null) return;
            if (elementVMKeys.Count < 40)
            {
                foreach (var elemVMKey in elementVMKeys)
                {
                    
                    if (originalElementDictionary.ContainsKey(elemVMKey))//Is try get value faster?
                    {
                        var toRemove = originalElementDictionary[elemVMKey];
                        originalElementList.Remove(toRemove);
                        originalElementDictionary.Remove(elemVMKey);
                    }
                }
            }
            else
            {
                //Dispose call?
                foreach (var key in elementVMKeys)
                {
                    originalElementDictionary.Remove(key);
                }
                originalElementList = new ObservableCollection<MenuElementViewModel>(originalElementDictionary.Values);
                ManageElementsCollection.Source = originalElementList;
                SetElementGrouping(ListByMode.NotSet, GroupingMode);
            }
        }

        public void UpdateSelectedCount()
        {
            NotifyPropertyChanged("SelectedCount");
            NotifyPropertyChanged("SelectedElementIDs");
            NotifyPropertyChanged("ElementBindList");
        }

        #endregion
        
        #region private helper methods

        void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            var elementViewModel = e.Item as MenuElementViewModel;
            if (elementViewModel == null) return;
            e.Accepted = Regex.IsMatch(elementViewModel.DisplayName, selectionFilter, RegexOptions.IgnoreCase) && !elementViewModel.TypeDisplayName.Equals("Road network");
        }

        private void SetElementGrouping(ListByMode oldMode, ListByMode groupMode)
        {
            if (Equals(oldMode, groupMode)) return;
            ManageElementsCollection.GroupDescriptions.Clear();
            switch (groupMode)
            {
                case ListByMode.All:
                    ElementBindList = ManageElementsCollection.View;
                    break;
                case ListByMode.Types:
                    ManageElementsCollection.GroupDescriptions.Add(new PropertyGroupDescription("TypeDisplayName"));
                    ElementBindList = ManageElementsCollection.View.Groups;
                    break;
                case ListByMode.Tags:
                    ManageElementsCollection.GroupDescriptions.Add(new PropertyGroupDescription("Tags"));
                    ElementBindList = ManageElementsCollection.View.Groups;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            NotifyPropertyChanged("ElementBindList");
        }
        #endregion
    }
    public class ElementViewModelKeyEqualityComparer<T> : IEqualityComparer<T> where T : MenuElementViewModel
    {
        #region Implementation of IEqualityComparer<MapElementViewModel>

        public bool Equals(T x, T y)
        {
            return Equals(x.ElementID, y.ElementID);
        }

        public int GetHashCode(T obj)
        {
            return obj.ElementID.GetHashCode();
        }
        #endregion
    }
    public enum ListByMode
    {
        All,
        Types,
        Tags,
        NotSet
    }
}
