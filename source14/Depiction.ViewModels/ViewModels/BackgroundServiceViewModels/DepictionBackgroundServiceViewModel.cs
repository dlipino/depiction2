﻿using System;
using System.Windows.Input;
using Depiction.API.Interfaces.MessagingInterface;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.BackgroundServiceViewModels
{
    public class DepictionBackgroundServiceViewModel : ViewModelBase
    {
        private IDepictionBackgroundService model;
        private DelegateCommand cancelService;
        private DelegateCommand refreshService;

        public Guid ServiceID { get { return model.ServiceID; } }
        public string ServiceName { get { return model.ServiceName; } }
        public string ProgressReport { get; set; }
        

        public ICommand CancelServiceCommand
        {
            get
            {
                if (cancelService == null)
                {
                    cancelService = new DelegateCommand(CancelService);
                }
                return cancelService;
            }
        }
        public ICommand RefreshServiceCommand
        {
            get
            {
                if (refreshService == null)
                {
                    refreshService = new DelegateCommand(RefreshService,IsRefreshable);
                }
                return refreshService;
            }
        }


        #region Constructor

        public DepictionBackgroundServiceViewModel(IDepictionBackgroundService service)
        {
            model = service;
            model.BackgroundServiceStatusReportUpdate += model_BackgroundServiceStatusReportUpdate;

        }

        #endregion
        public void StartService(object parameters)
        {
            model.StartBackgroundService(parameters);
        }

        void model_BackgroundServiceStatusReportUpdate(string obj)
        {
            ProgressReport = obj;
            NotifyPropertyChanged("ProgressReport");
        }
        private void CancelService()
        {
            model.StopBackgroundService();
        }
        private void RefreshService()
        {
            model.RefreshResult();
        }
        private bool IsRefreshable()
        {
            return model.IsServiceRefreshable;
        }

    }
}