﻿using System;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.QuickstartViewModels
{
    public class QuickstartViewModel : ViewModelBase
    {
        private DelegateCommand quickstartInfoCommand;

        public IQuickstartItem QuickstartModel { get; set; }
        public string Name { get { return QuickstartModel.Name; } }

        public ICommand QuickstartInfoCommand
        {
            get
            {
                if (quickstartInfoCommand == null)
                {
                    quickstartInfoCommand = new DelegateCommand(DisplayQuickStartInfo);
                }
                return quickstartInfoCommand;
            }
        }

        private void DisplayQuickStartInfo()
        {
            var dialogMessage = QuickstartModel.Description;
            var title = string.Format("{0} information",QuickstartModel.Name);
            DepictionAccess.NotificationService.SendNotificationDialog(null, dialogMessage, title);
        }
    }
}