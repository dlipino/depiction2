﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.DepictionComparers;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.CoreModel;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class ElementPropertyDialogContentVM : ViewModelBase
    {
        private DepictionIconPath unifiedIconPath;
        private ElementSettingsModality settingsModality = ElementSettingsModality.Properties;
        private ObservableCollection<MenuTagViewModel> TagList { get; set; }
        private CollectionViewSource TagCollectionSource { get; set; }
        //        private CollectionViewSource PropertyViewModelCollectionSource { get; set; }
        //        private CollectionViewSource PropertyHoverTextViewModelCollectionSource { get; set; }

        public ReadOnlyCollection<IDepictionElement> OriginalElementList { get; private set; }

        public ElementSettingsModality ElementSettingsModality
        {
            get { return settingsModality; }
            set
            {
                if (settingsModality != value)
                {
                    settingsModality = value;
                    NotifyPropertyChanged("ElementSettingsModality");
                }
            }
        }
        public string ElementType { get; private set; }
        public string ElementDisplayName { get; private set; }
        public bool ShowingSingleElement { get; set; }

        public List<string> OriginalElementIds
        {
            get
            {
                var list = OriginalElementList.Select(t => t.ElementKey).ToList();
                return list;
            }
        }
        public ReadOnlyCollection<string> OriginalTags { get; private set; }
        public string ContentKey { get; set; }
        public string Title { get; set; }
        public MapElementViewModel PreviewElement { get; private set; }
        public ObservableCollection<MenuElementPropertyViewModel> PropertyViewModels { get; set; }
        public ObservableCollection<MenuElementPropertyViewModel> HovertextPropertyViewModels { get; set; }

        public object PropertyViewModelCollection { get; private set; }
        public object PropertyHoverTextViewModelCollection { get; private set; }
        public object TagCollection { get; private set; }
        //Stuff dealing with hovertext
        public bool? UsePermaText { get; set; }
        public bool? UseEnhancedPermaText { get; set; }
        public bool? UsePropertyNameInHoverText { get; set; }
        public bool ModifyHoverText { get; set; }
        public DepictionIconPath UnifiedIconPath
        {
            get { return unifiedIconPath; }
            set { unifiedIconPath = value; NotifyPropertyChanged("UnifiedIconPath"); }
        }

        #region thematic mapping variable and property region

        private MenuElementPropertyViewModel selectedPropertyForMapping;

        private Color thematicMapStartColor;
        private Color thematicMapEndColor;
        private ElementColorMode colorMode = ElementColorMode.NoChange;
        public bool SelectedPropertyIsNumber { get; private set; }
        public MenuElementPropertyViewModel SelectedPropertyForMapping
        {
            get { return selectedPropertyForMapping; }
            set
            {
                selectedPropertyForMapping = value;
                if (value != null)
                {
                    SelectedPropertyIsNumber = ViewModelHelperMethods.IsTypeNumeric(value.FullPropertyType, value.PropertyValue);
                    NotifyPropertyChanged("SelectedPropertyIsNumber");
                }
            }
        }
        public bool ManuallySelectThematicMapRange { get; set; }
        public Color ThematicMapStartColor
        {
            get { return thematicMapStartColor; }
            set { thematicMapStartColor = value; NotifyPropertyChanged("ThematicMapStartColor"); }
        }
        public double ThematicMapStartValue { get; set; }
        public Color ThematicMapEndColor
        {
            get { return thematicMapEndColor; }
            set { thematicMapEndColor = value; NotifyPropertyChanged("ThematicMapEndColor"); }
        }
        public double ThematicMapEndValue { get; set; }
        public ElementColorMode ColorMode
        {
            get { return colorMode; }
            set
            {
                if (!value.Equals(ElementColorMode.NotAValidMode))
                {
                    colorMode = value;
                }
            }
        }

        #endregion

        #region tags

        private string newTag = string.Empty;
        private DelegateCommand<string> addTagPreviewCommand;
        public ICommand AddTagPreviewCommand
        {
            get
            {
                if (addTagPreviewCommand == null) { addTagPreviewCommand = new DelegateCommand<string>(AddTagPreview, TagIsValid); }
                return addTagPreviewCommand;
            }
        }

        private bool TagIsValid(string arg) { return !string.IsNullOrEmpty(arg); }

        private void AddTagPreview(string tagName)
        {
            var list = TagList.Where(t => t.DisplayName.Equals(tagName));
            if (list.Count() != 0) return;
            var tagVM = new MenuTagViewModel(tagName) { IsPreview = true };
            TagList.Add(tagVM);
            NewTag = string.Empty;
        }

        public string NewTag
        {
            get { return newTag; }
            set { newTag = value; NotifyPropertyChanged("NewTag"); }
        }

        #endregion

        #region Property adding thigns

        public IEnumerable<KeyValuePair<string, object>> PropertyTypes
        {
            get
            {
                var props = ViewModelHelperMethods.GetSimpleDepictionTypes();
                return props;
            }
        }
        private string propDisplayName = string.Empty;
        public string PropDisplayName
        {
            get { return propDisplayName; }
            set { propDisplayName = value; NotifyPropertyChanged("NewPropDisplayName"); }
        }
        public object SelectedType { get; set; }
        private DelegateCommand addPropertyPreviewCommand;
        public ICommand AddPropertyPreviewCommand
        {
            get
            {
                if (addPropertyPreviewCommand == null) { addPropertyPreviewCommand = new DelegateCommand(AddPropertyPreview, PropertyIsValid); }
                return addPropertyPreviewCommand;
            }
        }

        private bool PropertyIsValid()
        {
            if (string.IsNullOrEmpty(PropDisplayName)) return false;
            var internalName = ViewModelHelperMethods.GetInternalNameFromString(PropDisplayName);
            if (PropertyViewModels == null) return false;
            var list = PropertyViewModels.Where(t => t.InternalName.Equals(internalName));
            if (list.Count() != 0) return false;
            if (SelectedType == null) return false;

            return true;
        }

        private void AddPropertyPreview()
        {
            var internalName = ViewModelHelperMethods.GetInternalNameFromString(PropDisplayName);
            var propData = new DepictionElementProperty(internalName, PropDisplayName, SelectedType);
            var propVM = new MenuElementPropertyViewModel(propData, null);
            propVM.IsPreview = true;
            PropertyViewModels.Add(propVM);
        }

        #endregion

        #region Constructor

        public ElementPropertyDialogContentVM()
        {
            SetThematicColors();
        }

        public ElementPropertyDialogContentVM(List<IDepictionElement> elementList)
            : this()
        {
            if (elementList == null) return;

            OriginalElementList = elementList.AsReadOnly();
            if (PropertyViewModels == null) PropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
            if (TagList == null) TagList = new ObservableCollection<MenuTagViewModel>();
            HovertextPropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
            UnifiedIconPath = null;
            if (elementList.Count == 1)
            {
                var element = elementList[0];
                Title = element.TypeDisplayName + " Properties";
                ElementType = element.ElementType;
                ElementDisplayName = element.TypeDisplayName;
                ShowingSingleElement = true;
            }
            else
            {
                ElementType = "";
                Title = string.Format("Properties for {0} elements (showing values for {1})", elementList.Count, elementList[0].TypeDisplayName);
                ShowingSingleElement = false;
            }


            //find all matching properties
            var propertyIntersection = new List<IElementProperty>();
            var propertyUnion = new List<IElementProperty>();
            var tagIntersection = new List<string>();
            var comparer = new PropertyComparers.PropertyNameAndTypeComparer<IElementProperty>();
            UsePermaText = null;
            UseEnhancedPermaText = null;
            long contentKeyHash = 0;
            foreach (var element in elementList)
            {
                contentKeyHash += (long)element.ElementKey.GetHashCode();
                var elementParent = element as DepictionElementParent;
                if (elementParent != null)
                {
                    elementParent.ElementUpdated = false;
                }
                //Check for use of permatext in the group
                if (UsePermaText != element.UsePermaText)
                {
                    UsePermaText = null;
                }
                if (UseEnhancedPermaText != element.UseEnhancedPermaText)
                {
                    UseEnhancedPermaText = null;
                }
                if (UsePropertyNameInHoverText != element.UsePropertyNameInHoverText)
                {
                    UsePropertyNameInHoverText = null;
                }
                if (propertyIntersection.Count == 0)
                {
                    UseEnhancedPermaText = element.UseEnhancedPermaText;
                    UsePermaText = element.UsePermaText;
                    UsePropertyNameInHoverText = element.UsePropertyNameInHoverText;
                    propertyIntersection = element.OrderedCustomProperties.ToList();
                    propertyUnion = element.OrderedCustomProperties.ToList();
                }
                else
                {
                    propertyIntersection = propertyIntersection.Intersect(element.OrderedCustomProperties, comparer).ToList();
                    propertyUnion = propertyUnion.Union(element.OrderedCustomProperties, comparer).ToList();
                }
                if (tagIntersection.Count == 0)
                {
                    tagIntersection = element.Tags.ToList();
                }
                else
                {
                    tagIntersection = tagIntersection.Intersect(element.Tags).ToList();
                }
            }
            //            foreach (var prop in propertyUnion)
            //            {
            //                if (prop.VisibleToUser) HovertextPropertyViewModels.Add(new MenuPropertyHovertextElementViewModel(prop, null));
            //            }
            foreach (var prop in propertyIntersection)
            {
                if (prop.VisibleToUser && AddProp(prop.InternalName, ElementType))
                {
                    var model = new MenuElementPropertyViewModel(prop, null) { RepresentsMany = true };
                    switch (DepictionAccess.ProductInformation.ProductType)
                    {
                        case ProductInformationBase.Prep:
                            if (prop.InternalName.Equals("Position", StringComparison.InvariantCultureIgnoreCase))
                            {
                                model.IsPropertyReadOnly = true;
                            }
                            break;
                    }
//#if PREP
//                    if(prop.InternalName.Equals("Position",StringComparison.InvariantCultureIgnoreCase))
//                    {
//                        model.IsPropertyReadOnly = true;
//                    }
//#endif
                    PropertyViewModels.Add(model);
                    HovertextPropertyViewModels.Add(model);
                }
            }
            var leftOver = propertyUnion.Except(propertyIntersection, comparer);
            foreach (var prop in leftOver)
            {
                if (prop.VisibleToUser)
                {
                    var model = new MenuElementPropertyViewModel(prop, null) { RepresentsMany = true };
                    HovertextPropertyViewModels.Add(model);
                }
            }
            foreach (var tag in tagIntersection)
            {
                TagList.Add(new MenuTagViewModel(tag));
            }
            ContentKey = contentKeyHash.ToString();
            SetCollectionViewSources();
        }
        public ElementPropertyDialogContentVM(MapElementViewModel singleElement)
            : this(singleElement.ElementModel)
        {
            PreviewElement = singleElement;
        }

        public ElementPropertyDialogContentVM(IDepictionElement singleElement)
            : this()
        {
            OriginalElementList = new ReadOnlyCollection<IDepictionElement>(new List<IDepictionElement> { singleElement });
            Title = singleElement.TypeDisplayName + " Properties";
            ElementType = singleElement.ElementType;
            ElementDisplayName = singleElement.TypeDisplayName;
            ContentKey = singleElement.ElementKey;
            ShowingSingleElement = true;
            PropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
            HovertextPropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
            TagList = new ObservableCollection<MenuTagViewModel>();
            UsePermaText = singleElement.UsePermaText;
            UseEnhancedPermaText = singleElement.UseEnhancedPermaText;
            UsePropertyNameInHoverText = singleElement.UsePropertyNameInHoverText;
            var elementParent = singleElement as DepictionElementParent;
            if (elementParent != null)
            {
                elementParent.ElementUpdated = false;
            }
            foreach (var prop in singleElement.OrderedCustomProperties)
            {
                if (prop.VisibleToUser && AddProp(prop.InternalName, ElementType))
                {
                    var vmodel = new MenuElementPropertyViewModel(prop, singleElement as DepictionElementBase);
                    switch (DepictionAccess.ProductInformation.ProductType)
                    {
                        case ProductInformationBase.Prep:
                            if (prop.InternalName.Equals("Position", StringComparison.InvariantCultureIgnoreCase))
                            {
                                vmodel.IsPropertyReadOnly = true;
                            }
                            break;
                    }
//#if PREP
//                    if(prop.InternalName.Equals("Position",StringComparison.InvariantCultureIgnoreCase))
//                    {
//                        vmodel.IsPropertyReadOnly = true;
//                    }
//#endif
                    PropertyViewModels.Add(vmodel);
                    HovertextPropertyViewModels.Add(vmodel);
                }
                else
                {
                    Debug.WriteLine(prop.DisplayName + " not visible");
                }
            }
            foreach (var tag in singleElement.Tags)
            {
                TagList.Add(new MenuTagViewModel(tag));
            }
            DepictionIconPath iconPath;
            singleElement.GetPropertyValue("IconPath", out iconPath);
            UnifiedIconPath = iconPath;
            SetCollectionViewSources();
        }

        #endregion

        #region Destruction

        protected override void OnDispose()
        {
            foreach (var vm in PropertyViewModels)
            {
                vm.Dispose();
            }
            foreach (var vm in TagList)
            {
                vm.Dispose();
            }
            PreviewElement = null;//.Dispose();//Yeah we don't want to do this
            TagList.Clear();
            TagList = null;
            PropertyViewModels.Clear();
            PropertyViewModels = null;
            base.OnDispose();
        }

        #endregion
        #region private helper methods
        private List<string> PrepPropsToRemove = new List<string> { "IconBorderColor", "IconBorderShape", "IconPath", "Area", "Perimeter" };
        private bool AddProp(string propInternalName, string elementType)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep: if (PrepPropsToRemove.Contains(propInternalName))
                    {
                        if (!string.IsNullOrEmpty(elementType) && (elementType.Contains("UserDrawnLine") || elementType.Contains("UserDrawnPolygon")))
                        {
                            if (propInternalName.Equals("Area") || propInternalName.Equals("Perimeter"))
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                    break;
            }
//#if PREP
//            if (PrepPropsToRemove.Contains(propInternalName))
//            {
//                if (!string.IsNullOrEmpty(elementType) && (elementType.Contains("UserDrawnLine") || elementType.Contains("UserDrawnPolygon")))
//                {
//                    if(propInternalName.Equals("Area") || propInternalName.Equals("Perimeter"))
//                    {
//                        return true;
//                    }
//                }
//                return false;
//            }
//#endif
            return true;
        }
        private void SetThematicColors()
        {
            SelectedPropertyForMapping = null;
            ManuallySelectThematicMapRange = false;
            ThematicMapStartColor = Colors.White;
            ThematicMapStartValue = 0;
            ThematicMapEndColor = Colors.Red;
            ThematicMapEndValue = 100;
            ColorMode = ElementColorMode.NoChange;
        }

        private void SetCollectionViewSources()
        {
            if (TagList == null) TagList = new ObservableCollection<MenuTagViewModel>();
            TagCollectionSource = new CollectionViewSource();
            TagCollectionSource.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
            TagCollectionSource.Source = TagList;
            TagCollection = TagCollectionSource.View;
            //Group descriptor is no longer getting used
            if (PropertyViewModels == null) PropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();

            //            var PropertyViewModelCollectionSource = new CollectionViewSource();
            //            PropertyViewModelCollectionSource.SortDescriptions.Add(new SortDescription("Rank", ListSortDirection.Descending));
            //            PropertyViewModelCollectionSource.GroupDescriptions.Add(new PropertyGroupDescription("PropertyTypeString"));
            //            PropertyViewModelCollectionSource.Source = PropertyViewModels;
            //            PropertyViewModelCollection = PropertyViewModelCollectionSource.View;
            //            NotifyPropertyChanged("PropertyViewModelCollection");

            NotifyPropertyChanged("OriginalElementIds");
            if (HovertextPropertyViewModels == null) HovertextPropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
            //            PropertyHoverTextViewModelCollectionSource = new CollectionViewSource();
            //            PropertyHoverTextViewModelCollectionSource.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
            //            PropertyHoverTextViewModelCollectionSource.GroupDescriptions.Add(new PropertyGroupDescription("PropertyTypeString"));
            //            PropertyHoverTextViewModelCollectionSource.Source = HovertextPropertyViewModels;
            //            PropertyHoverTextViewModelCollection = PropertyHoverTextViewModelCollectionSource.View.Groups;
        }

        private void ApplyColorChanges()
        {
            switch (ColorMode)
            {
                case ElementColorMode.RemoveColor:
                    foreach (var element in OriginalElementList)
                    {
                        element.SetPropertyValue("IconBorderColor", Colors.Transparent);
                    }
                    return;
                case ElementColorMode.ThematicMapping:
                    if (SelectedPropertyForMapping == null) return;
                    var min = Double.MaxValue;
                    var max = Double.MinValue;
                    if (ManuallySelectThematicMapRange)
                    {
                        if (ThematicMapStartValue >= ThematicMapEndValue) return;
                        min = ThematicMapStartValue;
                        max = ThematicMapEndValue;
                    }
                    else
                    {
                        foreach (var element in OriginalElementList)
                        {
                            object val;
                            if (element.GetPropertyValue(SelectedPropertyForMapping.InternalName, out val))
                            {
                                double dblVal;
                                var success = ColorInterpolator.TryConvertToDouble(val, out dblVal);
                                if (!success) break;
                                if (double.NaN.Equals(min) || dblVal < min) min = dblVal;
                                if (double.NaN.Equals(max) || dblVal > max) max = dblVal;
                            }
                        }
                    }
                    var colorCreator = new ColorInterpolator(new List<object> { min, max },
                                                                new List<Color> { ThematicMapStartColor, ThematicMapEndColor });
                    ViewModelHelperMethods.SetThematicMapping(OriginalElementList, SelectedPropertyForMapping.InternalName, colorCreator);
                    return;
                default:
                    break;
            }
        }
        private void ApplyTagChanges()
        {
            NotifyPropertyChanged("OriginalElementIds");
            var currentTagList = TagList.ToList();
            var currentDepictionApp = Application.Current as DepictionApplication;
            IDepictionStory depiction = null;
            if (currentDepictionApp != null)
            {
                depiction = currentDepictionApp.CurrentDepiction;
            }
            //This just doesn't seem right at all
            foreach (var tag in currentTagList)
            {
                if (tag.IsPreview && !tag.Delete)
                {
                    tag.IsPreview = false;
                    if (depiction != null)
                    {
                        foreach (var element in OriginalElementList)
                        {
                            depiction.CompleteElementRepository.TagElement(element, tag.DisplayName);
                        }
                    }
                }
                if (tag.Delete)
                {
                    TagList.Remove(tag);
                    if (depiction != null && !tag.IsPreview)
                    {
                        foreach (var element in OriginalElementList)
                        {
                            depiction.CompleteElementRepository.UntagElement(element, tag.DisplayName);
                        }
                    }
                }
            }
        }
        private void ApplyHovertextSettingsChanges()
        {
            var changed = new List<MenuElementPropertyViewModel>();
            foreach (var hoverProp in HovertextPropertyViewModels)
            {
                if (hoverProp.HoverTextChanged)
                {
                    changed.Add(hoverProp);
                }
                hoverProp.HoverTextChanged = false;
            }
            bool elemChanged = false;
            foreach (var element in OriginalElementList)
            {
                if (UsePermaText != null)
                {
                    element.UsePermaText = (bool)UsePermaText;
                }
                if (UseEnhancedPermaText != null)
                {
                    element.UseEnhancedPermaText = (bool)UseEnhancedPermaText;
                }
                if (UsePropertyNameInHoverText != null && element.UsePropertyNameInHoverText != UsePropertyNameInHoverText)
                {
                    element.UsePropertyNameInHoverText = (bool)UsePropertyNameInHoverText;
                    elemChanged = true;
                }
                foreach (var hoverProp in changed)
                {
                    var hoverTextChange = element.GetPropertyByInternalName(hoverProp.PropertyModel.InternalName);
                    if (hoverTextChange != null)
                    {
                        hoverTextChange.IsHoverText = hoverProp.IsHoverText;
                        elemChanged = true;
                    }
                }
                if (elemChanged)
                {
                    element.UpdateToolTip();
                }
            }
        }

        private void ApplyPropertyValueChanges()
        {
            var origList = PropertyViewModels.ToList();

            foreach (var propVM in origList)
            {
                if (propVM.Delete)
                {
                    PropertyViewModels.Remove(propVM);
                    if (!propVM.IsPreview)
                    {
                        foreach (var elem in OriginalElementList)
                        {
                            elem.RemovePropertyIfNameAndTypeMatch(propVM.PropertyModel);
                        }
                    }
                }
                else
                {
                    DepictionIconPath iconPath = null;

                    if (propVM.ValueChanged || propVM.IsPreview)
                    {
                        if (propVM.FullPropertyType.Equals(typeof(DepictionIconPath)))
                        {
                            var app = Application.Current as IDepictionApplication;
                            if (app != null)
                            {
                                var dict = Application.Current.Resources;
                                iconPath = (DepictionIconPath)(propVM.PropertyValue);
                                var smallPath = Path.GetFileName(iconPath.Path);
                                if (!dict.Contains(smallPath))
                                {
                                    string keyValue;
                                    BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(iconPath.Path, out keyValue);
                                    iconPath.Path = smallPath;
                                }
                            }
                        }
                        if (OriginalElementList.Count == 1)
                        {
                            if (propVM.OwningElementModel == null)//Should also be a preview
                                propVM.OwningElementModel = OriginalElementList[0] as DepictionElementBase;
                            if (string.IsNullOrEmpty(propVM.Error))
                                propVM.ApplyVMChangesToModel();
                        }
                        else
                        {
                            var valueChanged = propVM.ValueChanged || propVM.IsPreview;
                            //propVM.IsPreview
                            bool addComplete = true;
                            foreach (var element in OriginalElementList)
                            {
                                if (valueChanged)
                                {
                                    //clone the original property data,but not the vm value
                                    var clone = propVM.PropertyModel.DeepClone();
                                    //Make it a full element property and set the value
                                    if (string.IsNullOrEmpty(propVM.Error))
                                    {//TODO get hack for position change

                                        clone.SetPropertyValue(propVM.PropertyValue, element);

                                        if (element.AddPropertyOrReplaceValueAndAttributes(clone) == false)
                                        {//If it gets here something is wrong, and im not really sure 
                                            //why it does what it does.
                                            addComplete = false;//This only changes one property
                                            valueChanged = false;
                                        }
                                    }
                                }
                            }
                            if (addComplete)
                            {
                                propVM.IsPreview = false;
                                propVM.ValueChanged = false;
                            }
                        }
                    }
                    if (iconPath != null)
                    {
                        UnifiedIconPath = iconPath;
                    }
                }
            }
        }

        #endregion

        #region Public methods

        public void ApplyChangesVMChangesToModels()
        {
            ApplyPropertyValueChanges();
            ApplyHovertextSettingsChanges();

            //Part for color changning
            ApplyColorChanges();
            ApplyTagChanges();
        }

        #endregion

    }
    public enum ElementSettingsModality
    {
        Properties,
        ThematicMapping,
        Hovertext,
        Tags
    }

}
