﻿using System.Reflection;
using System.Windows;
using System.Windows.Documents;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.MVVM;
using Depiction.API.Properties;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class TipControlDialogVM : DialogViewModelBase
    {
        #region Variables
        private FlowDocument tipDocument;
        #endregion

        #region Properties

        public TipControlTypeEnum TipControlType{ get; set; }
        public bool HideControlOnStartup
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        if (TipControlType.Equals(TipControlTypeEnum.StartTipControl))
                        {
                            return !Settings.Default.ShowTipAtMenuStart;
                        }
                        return !Settings.Default.ShowTipsAtStart;
                    default:
                        return !Settings.Default.ShowTipsAtStart;
                }
            }
            set
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        if (TipControlType.Equals(TipControlTypeEnum.StartTipControl))
                        {
                            Settings.Default.ShowTipAtMenuStart = !value;
                            return;
                        }
                        Settings.Default.ShowTipsAtStart = !value;
                        return;
                    default: 
                        Settings.Default.ShowTipsAtStart = !value;
                        return;

                }
            }
        }

        public FlowDocument TipDocument
        {
            get { return tipDocument; }
        }

//        public bool UseDefaultTipText
//        {//Used in xaml
//            get
//            {
//                switch (DepictionAccess.ProductInformation.ProductType)
//                {
//                    case ProductInformationBase.Prep:
//                    case ProductInformationBase.DepictionRW:
//                        return false;
//                    default:
//                        return true;
//                }
//                return true;
//            }
//        }

        #endregion

        #region constructor
        public TipControlDialogVM()
        {

            DialogDisplayName = "Depiction Tips";
            tipDocument = CreateGeneralTipDocument();
        }
        #endregion
        
        public FlowDocument CreateFlowDocumentFromEmbeddedResourceFile(string fileName)
        {
            FlowDocument flowDocument = new FlowDocument();
            var resourceName = DepictionAccess.ProductInformation.ResourceAssemblyName;
            string prepResourceStart = string.Format("{0}.Docs",resourceName);
            var tipFileName = "DepictionPrepInitialTipText.rtf";
            var embeddedResourceName = string.Format("{0}.{1}",prepResourceStart, tipFileName);
            if(!string.IsNullOrEmpty(fileName))
            {
                embeddedResourceName = string.Format("{0}.{1}", prepResourceStart, fileName);
            }
            Assembly resourceAssy = Assembly.Load(new AssemblyName(resourceName));

            var files = resourceAssy.GetManifestResourceNames();
            using (var rtfStream = resourceAssy.GetManifestResourceStream(embeddedResourceName))
            {
                if (rtfStream != null)
                {
                    TextRange textRange = new TextRange(flowDocument.ContentStart, flowDocument.ContentEnd);

                    textRange.Load(rtfStream, DataFormats.Rtf);
                }
            }
            tipDocument = flowDocument;
            tipDocument.PagePadding = new Thickness(10,15,10,15);
            tipDocument.LineHeight = 12;
            tipDocument.FontSize = 12;
            tipDocument.LineStackingStrategy = LineStackingStrategy.BlockLineHeight;
            NotifyPropertyChanged("TipDocument");
            return flowDocument;
        }

        protected FlowDocument CreateGeneralTipDocument()
        {
            var flowDoc = new FlowDocument();
            flowDoc.FontSize = 12;
            var startPara = new Paragraph();
            var run1 = new Run("Your Quickstart maps and images are being loaded into sizable, moveable Revealers");

            return flowDoc;
        }
    }
    public enum TipControlTypeEnum
    {
        MapTopControl,//Used for after region selection
        StartTipControl//used at start of app
    }
}