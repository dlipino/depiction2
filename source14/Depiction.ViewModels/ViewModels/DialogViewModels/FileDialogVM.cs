﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class FileDialogVM : DialogViewModelBase
    {
        public FileDialogVM()
        {
            DialogDisplayName = "File menu";
        }
    }
}