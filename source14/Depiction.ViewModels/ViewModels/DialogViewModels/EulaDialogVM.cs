﻿using System.Windows.Documents;
using Depiction.API;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class EulaDialogVM : DialogViewModelBase
    {
        private FixedDocumentSequence aboutDocument;

        public FixedDocumentSequence EulaDocument
        {
            get { return aboutDocument; }
            set
            {
                aboutDocument = value;
                NotifyPropertyChanged("EulaDocument");
            }
        }

        public EulaDialogVM()
        {
            var eulafileName = DepictionAccess.ProductInformation.EulaFileAssemblyLocation;
            var assembly = DepictionAccess.ProductInformation.ResourceAssemblyName;

            var xpsDoc = AboutDialogVM.LoadDocFromAssemblyResource(assembly, eulafileName);
            if (xpsDoc == null) { EulaDocument = new FixedDocumentSequence(); }
            else
            {
                EulaDocument = xpsDoc.GetFixedDocumentSequence();
            }
            DialogDisplayName = "EULA";
        }
    }
}