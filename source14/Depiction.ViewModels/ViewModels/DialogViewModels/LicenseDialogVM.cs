﻿using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class LicenseDialogVM : DialogViewModelBase
    {
        #region Varialbles

        private DelegateCommand deactivateCommand;
        private ILicenseService currentLicenseService;
        private string license = string.Empty;

        #endregion

        #region Properteis

        public string LicenseButtonText
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return "Purchase Depiction";
                    default:
                        return "Deactivate Depiction";
                }
//#if IS_READER
//                return "Purchase Depiction";
//#endif
//                return "Deactivate Depiction";
            }
        }

        public ICommand DeactivateDepictionCommand
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return DepictionAppViewModel.PurchaseDepictionCommand;
                }
//#if IS_READER
//                return DepictionAppViewModel.PurchaseDepictionCommand;
//#endif
                if(deactivateCommand == null)
                {
                    deactivateCommand = new DelegateCommand(DeactivateLicense);
                    deactivateCommand.Text = "Deactivate Depiction";
                }
                return deactivateCommand;
            }
        }

       
        public string LicenseKey
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return "Depiction Reader";
                }
//#if IS_READER
//                return "Depiction Reader";
//#endif
                if (currentLicenseService == null) return "No License";
                if(string.IsNullOrEmpty(license))
                {
                    license = currentLicenseService.SerialNumber;
                }
                return license;
            }
        }

        #endregion

        #region Constructor
        public LicenseDialogVM(ILicenseService licenseService)
        {
            DialogDisplayName = "Depiction license";
            currentLicenseService = licenseService;
        }
        #endregion

        private void DeactivateLicense()
        {
            if (currentLicenseService == null) return;
            currentLicenseService.DeactivateLicense();
        }
    }
}