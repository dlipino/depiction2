﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class DepictionAppHelpDialogVM : DialogViewModelBase
    {
        public DepictionAppHelpDialogVM()
        {
            DialogDisplayName = "Depiction help";
        }
    }
}