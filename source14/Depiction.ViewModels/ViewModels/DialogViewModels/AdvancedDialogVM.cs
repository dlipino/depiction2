﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class AdvancedDialogVM : DialogViewModelBase
    {
        public AdvancedDialogVM()
        {
            DialogDisplayName = "Advanced menu";
        }
    }
}