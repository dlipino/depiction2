﻿using System;
using System.Windows.Input;
using Depiction.API.ExceptionHandling;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class BugSubmitterDialogVM : DialogViewModelBase
    {
        private FogBugzPoller bugPoller;
        private string errorMessage;

        #region Properties
 
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; NotifyPropertyChanged("ErrorMessage"); }
        }

        public string UserEmail { get; set; }
        public string UserDescription { get; set; }
        #endregion

        #region Commands

        public ICommand SendReportCommand
        {
            get
            {
                var sendReportCommand = new DelegateCommand(SendReport,CanSendReport);
                return sendReportCommand;
            }
        }

        private bool CanSendReport()
        {
            return true;
        }

        private void SendReport()
        {
            bugPoller.SubmitBug(ErrorMessage,UserEmail,UserDescription);
            IsDialogVisible = false;
        }

        #endregion
        #region Constructor

        public BugSubmitterDialogVM(FogBugzPoller poller)
        {
            SetBugPoller(poller);
        }

        #endregion

        protected void SetBugPoller(FogBugzPoller poller)
        {
            bugPoller = poller;
            if(bugPoller != null)  bugPoller.MessageAddedToStack += bugPoller_MessageAddedToStack;
        }

        void bugPoller_MessageAddedToStack()
        {
            ErrorMessage = (string)bugPoller.ExceptionMessageStack.Pop();
            IsDialogVisible = true;
            
        }
        protected override void OnDispose()
        {
            if (bugPoller != null)
            {
                bugPoller.MessageAddedToStack -= bugPoller_MessageAddedToStack;
            }
            base.OnDispose();
        }
    }
}