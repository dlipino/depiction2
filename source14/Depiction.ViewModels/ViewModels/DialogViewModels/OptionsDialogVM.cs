﻿using System.Collections.Generic;
using System.Net;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.MVVM;
using Depiction.API.Properties;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class OptionsDialogVM : DialogViewModelBase
    {
        #region Variables

        private bool zoomAboutCenter;
        #endregion
        #region Properties

        public Dictionary<MeasurementSystem, string> MeasurementSystemsList { get; set; }
        public Dictionary<MeasurementScale, string> MeasurementScalesList { get; set; }
        public Dictionary<LatitudeLongitudeFormat, string> LatitudeLongitudeFormatList { get; set; }
        public LatitudeLongitudeFormat SelectedLatitudeLongitudeFormat { get; set; }

        public IList<int> PrecisionList { get; set; }

        public bool ZoomAboutCenter {
            get { return zoomAboutCenter; }
            set { zoomAboutCenter = value; NotifyPropertyChanged("ZoomAboutCenter"); }
        }
        public int SelectedPrecision { get; set; }
        public MeasurementScale SelectedScale { get; set; }
        public MeasurementSystem SelectedSystem { get; set; }
        public LatitudeLongitudeFormat SelectedLatLongFormat { get; set; }
        public bool OverwriteCache { get; set; }
        public bool DisplayWorldOutline { get; set; }

        public string ProxyUserName { get; set; }
        public string ProxyPassword { get; set; }

        public string UserDirectory{get{return DepictionAccess.PathService.AppDataDirectoryPath;}}

        #endregion

        #region Constructor

        public OptionsDialogVM()
        {

            DialogDisplayName = "Depiction settings options";
            SetLists();
            SelectedLatLongFormat = Settings.Default.LatitudeLongitudeFormat;
            SelectedScale = Settings.Default.MeasurementScale;
            SelectedSystem = Settings.Default.MeasurementSystem;
            ZoomAboutCenter = Settings.Default.ZoomAboutCenter;
            SelectedPrecision = Settings.Default.Precision;
            OverwriteCache = Settings.Default.ReplaceCachedFiles;
            ProxyPassword = Settings.Default.ProxyPassword;
            ProxyUserName = Settings.Default.ProxyUserName;
            DisplayWorldOutline = Settings.Default.DisplayWorldOutline;
        }

        #endregion

        private void SetLists()
        {
            MeasurementSystemsList = new Dictionary<MeasurementSystem, string>
                                         {
                                             {MeasurementSystem.Imperial, "Imperial"},
                                             {MeasurementSystem.Metric, "Metric"}
                                         };
            MeasurementScalesList = new Dictionary<MeasurementScale, string>
                                         {
                                             {MeasurementScale.Small, "Small"},
                                             {MeasurementScale.Normal, "Normal"},
                                             {MeasurementScale.Large, "Large"},
                                         };
            LatitudeLongitudeFormatList = new Dictionary<LatitudeLongitudeFormat, string>
                                              {
                                                  {LatitudeLongitudeFormat.SignedDecimal, "40.44620, -79.94886"},
                                                  {LatitudeLongitudeFormat.Decimal, "40.44620N, 79.94886W"},
                                                  
                                                  {LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "40° 26.77170, -79° 56.93172"},
                                                   {LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 26.77170N, 79° 56.93172W"},
                                                   {LatitudeLongitudeFormat.UTM,"17T 630084 4833438"},
                                                  {LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°26'46\"N, 79°56'55\"W"},
                                                  {LatitudeLongitudeFormat.ColonFractionalSeconds, "40:26:46.302N, 79:56:55.903W"},
                                                  {LatitudeLongitudeFormat.ColonIntegerSeconds, "40:26:46N, 79:56:55W"},
                                                  {LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d26'46\"N, 79d56'55\" W"},
                                              };
            PrecisionList = new List<int> { 0, 1, 2, 3, 4, 5 };
        }
        #region Command

        public ICommand ApplyOptionsChangesCommand
        {
            get
            {
                var applyOptionsChangesCommand = new DelegateCommand(ApplyOptions, OptionsHaveChanged);
                return applyOptionsChangesCommand;
            }
        }
        public ICommand AcceptOptionsChangesCommand
        {
            get
            {
                var applyOptionsChangesCommand = new DelegateCommand(AcceptOptions);
                return applyOptionsChangesCommand;
            }
        }
//        public ICommand CancelOptionsChangesCommand
//        {
//            get
//            {
//                var cancelOptionsChangesCommand = new DelegateCommand(CancelOptions);
//                return cancelOptionsChangesCommand;
//            }
//        }
//
//        private void CancelOptions()
//        {
//            Settings.Default.Reload();
//            IsDialogVisible = false;
//        }


        private bool OptionsHaveChanged()
        {
            if (!Settings.Default.LatitudeLongitudeFormat.Equals(SelectedLatLongFormat)) return true;
            if (!Settings.Default.MeasurementScale.Equals(SelectedScale)) return true;
            if (!Settings.Default.MeasurementSystem.Equals(SelectedSystem)) return true;
            if (!Settings.Default.ZoomAboutCenter.Equals(ZoomAboutCenter)) return true;
            if (!Settings.Default.Precision.Equals(SelectedPrecision)) return true;
            if (!Settings.Default.ReplaceCachedFiles.Equals(OverwriteCache)) return true;
            if (!Settings.Default.ProxyUserName.Equals(ProxyUserName)) return true;
            if (!Settings.Default.ProxyPassword.Equals(ProxyPassword)) return true;
            if (!Settings.Default.DisplayWorldOutline.Equals(DisplayWorldOutline)) return true;
            return false;
        }

        private void ApplyOptions()
        {
            Settings.Default.Precision = SelectedPrecision;//This has to be before selectedScale because
            //there is no event change assocatiated with this.
            Settings.Default.LatitudeLongitudeFormat = SelectedLatLongFormat;
            Settings.Default.MeasurementScale = SelectedScale;
            Settings.Default.MeasurementSystem = SelectedSystem;
            Settings.Default.ZoomAboutCenter = ZoomAboutCenter;
            
            //A different way, the view is using direct access to the settings properties
            OverwriteCache = Settings.Default.ReplaceCachedFiles;
//            Settings.Default.ReplaceCachedFiles = OverwriteCache;
            ProxyUserName = ProxyUserName ?? string.Empty;
            ProxyPassword = ProxyPassword ?? string.Empty;

            Settings.Default.ProxyUserName = ProxyUserName;
            Settings.Default.ProxyPassword = ProxyPassword;
            if (WebRequest.DefaultWebProxy != null)
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(ProxyUserName, ProxyPassword);
            Settings.Default.DisplayWorldOutline = DisplayWorldOutline;
            Settings.Default.Save();
        }
        private void AcceptOptions()
        {
            if(OptionsHaveChanged()) ApplyOptions();
            IsDialogVisible = false;
        }

        #endregion
    }
}