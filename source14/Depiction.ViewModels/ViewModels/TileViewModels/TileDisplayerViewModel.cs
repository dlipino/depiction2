﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.CoreModel;

namespace Depiction.ViewModels.ViewModels.TileViewModels
{
    public class TileDisplayerViewModel : ViewModelBase
    {
        #region Variables
        private ObservableCollection<TileViewModel> tiles = new ObservableCollection<TileViewModel>();
        private const int MaximumTilesOnCanvas = 100;
        private readonly List<ITiler> allTileProvidersStorage = new List<ITiler>();
//        private TileImageTypes selectableTileProviderType = TileImageTypes.Unknown;
        private int lastZoomLevel;
        private TileFetcher fetcher;
        private ITiler tiler;
//        private ObservableCollection<ITiler> selectableTileProviders = new ObservableCollection<ITiler>();
        private TileImageTypes tileProviderType;
        private ITiler selectedTileProvider;
        private HashSet<TileImageTypes> availableTilerTypes = new HashSet<TileImageTypes>();

        #endregion

        #region properties
        public IList<TileViewModel> Tiles { get { return tiles; } }
        public HashSet<TileImageTypes> AvailableTilerTypes { get { return availableTilerTypes; } }
        //The tiler that is getting selected, can probably be reduced to just a name
        //Not sure why these overlap,i think to keep from starting a tiler unnecessarily 
        public ITiler SelectedTileProvider
        {
            get { return selectedTileProvider; }
            set
            {
                NotifyPropertyChanged("SelectedTileProvider");//hack for ui change
                if (value == null) return;
                if (selectedTileProvider == null)
                {
                    selectedTileProvider = value;
                    NotifyPropertyChanged("SelectedTileProvider");
                    Tiler = value;
                    return;
                }
                if (!selectedTileProvider.Equals(value))
                {
                    selectedTileProvider = value;
                    NotifyPropertyChanged("SelectedTileProvider");
                    Tiler = value;
                }
            }
        }
        //The active tiler
        public ITiler Tiler
        {
            get { return tiler; }
            set
            {
//                if (tiler.Equals(value)) return;
                tiler = value;
                tiles.Clear();
                if (fetcher != null)
                {
                    fetcher.AbortFetch();
                    fetcher = null;

                }
                if (tiler == null)
                    return;
                if (!(tiler is EmptyTiler))
                {
                    fetcher = new TileFetcher(tiler);

                    fetcher.DataChanged += FetcherOnDataChanged;
                    if (DepictionAccess.CurrentDepiction != null)
                        TileVisibleArea(DepictionAccess.CurrentDepiction.ViewportBounds);
                }
                Settings.Default.TilingSourceName = tiler.DisplayName;
                Settings.Default.Save();
                NotifyPropertyChanged("Tiler");
            }
        }
        public TileImageTypes TileProviderType
        {
            get { return tileProviderType; }
            set
            {
                if (tileProviderType == value) return;
                tileProviderType = value;
                NotifyPropertyChanged("SelectableTileProviders");
                NotifyPropertyChanged("SelectedTileProvider");
                NotifyPropertyChanged("TileProviderType");
            }
        }

        public List<ITiler> SelectableTileProviders
        {
            get { return allTileProvidersStorage.Where(t => t.TileImageType.Equals(tileProviderType)).ToList(); }
        }
       
//        public TileImageTypes SelectableTileProviderType
//        {
//            get { return selectableTileProviderType; }
//            set
//            {
//                if (selectableTileProviderType == value) return;
//
//                selectableTileProviderType = value;
//
//                UpdateSelectableTileProviders(selectableTileProviderType);
//
//                NotifyPropertyChanged("SelectableTileProviderType");
//                if (Tiler != null && Tiler.TileImageType.Equals(selectableTileProviderType)) return;
//
//                if (SelectableTileProviders != null && SelectableTileProviders.Any())
//                {
//                    Tiler = SelectableTileProviders.First();
//                }
//            }
//        }
//        public ObservableCollection<ITiler> SelectableTileProviders { get { return selectableTileProviders; } }
        #endregion
        #region Constructor
        public TileDisplayerViewModel()
        {
            DepictionApplication depictionApp = (DepictionApplication)Application.Current;
            depictionApp.DepictionChanged += depictionApp_DepictionChanged;
            var empty = new EmptyTiler();
            allTileProvidersStorage.Add(empty);
            availableTilerTypes.Add(TileImageTypes.None);
        }
        #endregion
        public void AddToAvailableTilers(ITiler[] tilers)
        {
            if (tilers != null)
            {
                foreach (var tiler in tilers)
                {
                    SelectableTileProviders.Add(tiler);
                    allTileProvidersStorage.Add(tiler);
                    AvailableTilerTypes.Add(tiler.TileImageType);
                }
            }
            SelectLastOrDefaultTiler();
        }

        void depictionApp_DepictionChanged(IDepictionStory oldDepiction, IDepictionStory newDepiction)
        {
            Tiles.Clear();
            Debug.WriteLine("Tile background service starting for a depiction change.");
            if (oldDepiction != null)
            {
                oldDepiction.DepictionViewportChange -= OnViewPortChanged;
            }
            if (newDepiction != null)
            {
                newDepiction.DepictionViewportChange += OnViewPortChanged;
                if (newDepiction.ViewportBounds.IsValid)
                {
                    OnViewPortChanged(null, newDepiction.ViewportBounds);
                }
            }
        }

        public void OnViewPortChanged(IMapCoordinateBounds oldBounds, IMapCoordinateBounds bounds)
        {
            Debug.WriteLine(bounds.TopLeft + " " + bounds.BottomRight);
            TileVisibleArea(bounds);
        }

        protected void SelectLastOrDefaultTiler()
        {
            //Overall semi hackish because of legacy code and settings
            var tilerToLoadName = Settings.Default.TilingSourceName;
            ITiler selectedTiler = null;
            //Ugly stuff, really ugly stuff, and have not intention of making pretty yet
            if (string.IsNullOrEmpty(tilerToLoadName))
            {
                selectedTiler = SelectFirstAvailableTilerAndUpdateTileService();
            }
            else
            {
                var matchingTilers = allTileProvidersStorage.Where(t => t.DisplayName.Equals(tilerToLoadName)).ToList();
                if (!matchingTilers.Any())
                {
                    selectedTiler = SelectFirstAvailableTilerAndUpdateTileService();
                }
                else
                {
                    var firstMatch = matchingTilers.First();

                    var tilerType = firstMatch.TileImageType;
                    TileProviderType = tilerType;
                    Tiler = firstMatch;
                    selectedTiler = firstMatch;
                }
            }
            if (selectedTiler != null)
            {
                UpdateSelectableTileProviders(selectedTiler.TileImageType);
                Settings.Default.TilingSourceName = selectedTiler.DisplayName;
            }

//            NotifyPropertyChanged("SelectableTileProviders"); NotifyPropertyChanged("SelectedTileProvider");
        }

        private ITiler SelectFirstAvailableTilerAndUpdateTileService()
        {
            //There should always be something in thislist.
            var firstTiler = allTileProvidersStorage.First();

            var tilerType = firstTiler.TileImageType;
            TileProviderType = tilerType;
            Tiler = firstTiler;
            return firstTiler;
        }

        private void UpdateSelectableTileProviders(TileImageTypes tilerImageType)
        {
            //If type does not match, select the first one that does.
            //kind of a hack
            var matchingTilers =
                allTileProvidersStorage.Where(t => t.TileImageType.Equals(tilerImageType));
            //Hopefully this doesnt cause a race condition, or something bad like that
            SelectableTileProviders.Clear();
            foreach (var matchingTiler in matchingTilers)
            {
                SelectableTileProviders.Add(matchingTiler);
            }
        }

        private void FetcherOnDataChanged(TileModel tileModel)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action<TileModel>(OnDataChanged), tileModel);
        }

        private void OnDataChanged(TileModel tileModel)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(tileModel.Image);
            bitmapImage.EndInit();
            bitmapImage.Freeze();
            //bitmapImage.SetSource(new MemoryStream(tileModel.Image));
            var tileViewModel = new TileViewModel(tileModel);
            tileViewModel.TileSource = bitmapImage;
            if (!tiles.Contains(tileViewModel))
            {
                tiles.Add(tileViewModel);
            }
        }

        public void TileVisibleArea(IMapCoordinateBounds currentExtent)
        {
            if (tiler == null) return;
            if (tiler.PixelWidth == 0) return;
            var tilesAcross = Convert.ToInt32(Application.Current.MainWindow.ActualWidth/(tiler.PixelWidth*2));
            var currentZoomLevel = Tiler.GetZoomLevel(currentExtent,
                                       tilesAcross, 18);
            if (lastZoomLevel != currentZoomLevel)
            {
                ZoomLevelChange(currentZoomLevel);
                lastZoomLevel = currentZoomLevel;
            }
            EnforceMaximumTiles();
            if (fetcher != null)
                fetcher.ViewChanged(currentExtent, tilesAcross);
        }

        private void EnforceMaximumTiles()
        {
            if (tiles.Count > MaximumTilesOnCanvas)
                for (int i = 0; i < tiles.Count - MaximumTilesOnCanvas; i++)
                    tiles.RemoveAt(0);
        }

        private void ZoomLevelChange(int currentZoomLevel)
        {
            List<TileViewModel> tilesToRefresh = new List<TileViewModel>();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].ImageOpened && tiles[i].ZoomLevel == currentZoomLevel)
                {
                    tilesToRefresh.Add(tiles[i]);
                    tiles.RemoveAt(i);
                    i--;
                }
            }
            foreach (var tile in tilesToRefresh)
            {
                tiles.Add(tile);
            }
        }
    }
}
