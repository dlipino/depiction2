﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.API.TileServiceHelpers;
using Depiction.ViewModels.MapConverters;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.ViewModels.ViewModels.TileViewModels
{
    public class TileViewModel : IEquatable<TileViewModel>
    {
        private readonly TileModel tileModel;

        public TileViewModel(TileModel tileModel)
        {
            this.tileModel = tileModel;
            Point topLeftPos = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tileModel.TopLeft);
            Point bottomRight = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tileModel.BottomRight);
            Top = topLeftPos.Y;
            Left = topLeftPos.X;
            Width = Math.Abs(bottomRight.X - topLeftPos.X);
            Height = Math.Abs(topLeftPos.Y - bottomRight.Y);
        }

        public TileModel TileModel { get { return tileModel; } }

        public double Width { get; set; }
        public double Height { get; set; }
        public double Top { get; set; }
        public double Left { get; set; }

        public BitmapSource TileSource { get; set; }

        public bool ImageOpened { get; set; }

        public int ZoomLevel { get { return tileModel.TileZoomLevel; } }
        public bool Equals(TileViewModel other)
        {
            return Top.Equals(other.Top) && Left.Equals(other.Left) && tileModel.TileZoomLevel.Equals(other.tileModel.TileZoomLevel);
        }
    }

    //public class TileViewModel : ViewModelBase, IEquatable<TileViewModel>
    //{
    //    private TileModel tileModel;
    //    private int zIndex = 0;
    //    #region Properties

    //    public Point TopLeft { get; private set; }
    //    public double Width { get; private set; }
    //    public double Height { get; private set; }
    //    public ImageSource TileImageSource { get; private set; }
    //    public int ZIndex { get { return zIndex; } set { zIndex = value; NotifyPropertyChanged("ZIndex"); } }
    //    #endregion

    //    #region Constructor

    //    public TileViewModel(TileModel tileModel)
    //    {
    //        this.tileModel = tileModel;
    //        TileImageSource = BitmapSaveLoadService.LoadImageFromURLAndCreateThumb(tileModel.TilePath,100,100);
    //        TileImageSource = new BitmapImage(new Uri(tileModel.TilePath));
    //        TileImageSource.Freeze();

    //        TopLeft = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tileModel.TopLeft);//pixelConverter.WorldToGeoCanvas(tileModel.TopLeft);
    //        Point bottomRightPoint = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tileModel.BottomRight);
    //        Height = Math.Abs(bottomRightPoint.Y - TopLeft.Y);
    //      //  Console.WriteLine(Height + " th ehid ");
    //        Width = Math.Abs(bottomRightPoint.X - TopLeft.X);
    //    }

    //    #endregion

    //    override protected void OnDispose()
    //    {
    //        TileImageSource = null;
    //        tileModel = null;
    //        base.OnDispose();
    //    }

    //    public bool Equals(TileViewModel other)
    //    {
    //        if (other.tileModel.TileKey.Equals(tileModel.TileKey)) return true;
    //        return false;
    //    }

    //    public override int GetHashCode()
    //    {
    //        if (tileModel == null || string.IsNullOrEmpty(tileModel.TileKey)) return 0;
    //        return tileModel.TileKey.GetHashCode();
    //    }
    //}
}
