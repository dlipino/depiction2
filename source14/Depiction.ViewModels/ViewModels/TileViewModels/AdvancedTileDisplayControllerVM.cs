﻿using System.Collections.Generic;
using System.Linq;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.TileViewModels
{
    public class AdvancedTileDisplayControllerVM : ViewModelBase
    {
        private TileImageTypes tileProviderType;
        private readonly List<ITiler> allTileProvidersStorage = new List<ITiler>();
        private ITiler selectedTileProvider;

        public TileImageTypes TileProviderType
        {
            get { return tileProviderType; }
            set
            {
                if (tileProviderType == value) return;
                tileProviderType = value;
                NotifyPropertyChanged("TileProviderType");
                NotifyPropertyChanged("SelectableTileProviders");
            }
        }

        public List<ITiler> SelectableTileProviders
        {
            get { return allTileProvidersStorage.Where(t => t.TileImageType.Equals(tileProviderType)).ToList(); }
        }
        public ITiler SelectedTileProvider
        {
            get { return selectedTileProvider; }
            set
            {
                if (!selectedTileProvider.Equals(value))
                {
                    selectedTileProvider = value;
                    NotifyPropertyChanged("SelectedTileProvider");
                }
            }
        }

    }
}