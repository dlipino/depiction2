﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;
using Depiction.CoreModel.DepictionObjects;
using Depiction.ViewModels.MapConverters;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class BaseMapViewModel : ViewModelBase
    {
        public IDepictionStory WorldModel { get; set; }//Rename this
        protected WindowViewportExtent viewExtent;
        #region test for commands and contextmenu

        public ObservableCollection<ICommand> MapCommands { get; set; }
        public List<ICommand> GenericElementCommands { get; set; }
        private DelegateCommand<Point> copyRightClickLocationToClipboard;
        #endregion

        #region constructor

        public BaseMapViewModel(IDepictionStory depiction)
        {
            MapCommands = new ObservableCollection<ICommand>();
            MapCommands.Add(CopyRightClickToClipboard);
            GenericElementCommands = new List<ICommand>();
            if(depiction != null)
            {
                WorldModel = depiction;
                AddEventsToModel();
                var worldBounds = DepictionGeographicInfo.WorldLatLongBoundingBox;
                //Where should this really go?
                viewExtent = new WindowViewportExtent(worldBounds, ViewModelConstants.MapSize, ViewModelConstants.MapSize);

                var geoConverter = new MercatorCoordinateConverter(viewExtent, 0, 0);
                var pixCenter = geoConverter.WorldToGeoCanvas(depiction.DepictionGeographyInfo.DepictionStartLocation);
                //Not sure if the converter should be in the model
                DepictionAccess.GeoCanvasToPixelCanvasConverter = new MercatorCoordinateConverter(viewExtent, pixCenter.X, pixCenter.Y);
            }
        }

        #endregion

        #region destruction

        protected override void OnDispose()
        {
            RemoveEvents();
            base.OnDispose();
        }
        #endregion

        #region constructor and destructor helpers

        private void RemoveEvents()
        {
            if (WorldModel == null) return;
            modelEventsAttached = false;
            DetachEventsFromModel();
        }
        private void AddEventsToModel()
        {
            if (WorldModel == null) return;
            if (!modelEventsAttached) { AttachEventsToModel(); }
        }
        virtual protected void AttachEventsToModel()
        {
        }

        virtual protected void DetachEventsFromModel()
        {
        }
        #endregion

        #region Commands and matching methods

        #region Copy right click location to clip board

        public ICommand CopyRightClickToClipboard
        {
            get
            {
                if (copyRightClickLocationToClipboard == null)
                {
                    copyRightClickLocationToClipboard = new DelegateCommand<Point>(CopyLocation);
                    copyRightClickLocationToClipboard.Text = "Copy location to clipboard";
                }
                return copyRightClickLocationToClipboard;
            }
        }
        protected void CopyLocation(Point location)
        {
            var geoLocation = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location);
            Clipboard.SetText(geoLocation.ToString());
        }

        #endregion

        #endregion
    }
}
