﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.ViewModels.MapConverters;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class DepictionBasicMapViewModel : WorldMapViewModel
    {//This does not deal with any elements, only with revealers and regions
        public static IElementRevealerDisplayerViewModel topRevealer;

        public event Func<MapAnnotationViewModel, bool> RequestShowAnnotationProperty;
        public event Action<DepictionCanvasMode> CanvasVMModeChanged;//Hmm is the new or old?
        #region Variables

        private Point currentMainWindowPoint = new Point(double.NaN, double.NaN);
        private RectangleGeometry depictionRegionPixelBounds;
        private double inverseScale = 1;
        private DepictionCanvasMode worldCanvasMode = DepictionCanvasMode.Uninitialized;
        private Rect regionSelectionBounds;

        private DelegateCommand startRegionSelection;
        private DelegateCommand<Rect> endRegionSelection;
        private DelegateCommand addRevealer;
        private DelegateCommand<IElementRevealerDisplayerViewModel> removeRevealer;

        private DelegateCommand<MapAnnotationViewModel> deleteAnnotationCommand;
        private DelegateCommand<MapAnnotationViewModel> showAnnotationPropertyInfo;

        #endregion

        #region Properties
        public string MapLocationInformation { get; protected set; }

        public Point CurrentMainWindowPoint
        {
            get { return currentMainWindowPoint; }
            set
            {
                currentMainWindowPoint = value;
                GetDataForPoint(currentMainWindowPoint);
                NotifyPropertyChanged("MapLocationInformation");
            }
        }
        //Might not be needed
        public double InverseScale { get { return inverseScale; } set { inverseScale = value; NotifyPropertyChanged("InverseScale"); } }

        public bool RegionExists { get { return WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid; } }
        public RectangleGeometry DepictionRegionPixelBounds
        {
            get { return depictionRegionPixelBounds; }
            set { depictionRegionPixelBounds = value; NotifyPropertyChanged("DepictionRegionPixelBounds"); }
        }
        public Rect RegionSelectionBounds
        {
            get { return regionSelectionBounds; }
            set { regionSelectionBounds = value; NotifyPropertyChanged("RegionSelectionBounds"); }
        }

        public DepictionCanvasMode WorldCanvasModeVM //TODO rename this
        {
            get { return worldCanvasMode; }
            set
            {
                worldCanvasMode = value;
                NotifyPropertyChanged("WorldCanvasModeVM");
                if (CanvasVMModeChanged != null)
                {
                    CanvasVMModeChanged(worldCanvasMode);
                }
            }
        }
        #endregion
        #region Observable collections

        private ObservableCollection<MapAnnotationViewModel> annotations = new ObservableCollection<MapAnnotationViewModel>();
        private ElementDisplayerViewModel<DepictionElementBackdrop> mainMapDisplayerViewModel;
        private ObservableCollection<IElementRevealerDisplayerViewModel> revealers = new ObservableCollection<IElementRevealerDisplayerViewModel>();

        public ObservableCollection<MapAnnotationViewModel> Annotations { get { return annotations; } }

        public ElementDisplayerViewModel<DepictionElementBackdrop> MainMapDisplayerViewModel { get { return mainMapDisplayerViewModel; } }
        public ObservableCollection<IElementRevealerDisplayerViewModel> Revealers { get { return revealers; } }

        #endregion

        #region Constructor

        public DepictionBasicMapViewModel(IDepictionStory depiction)
            : base(depiction)
        {//The automatically attaches DepictionStory events
            WorldCanvasModeVM = DepictionCanvasMode.Uninitialized;
            revealerCount = 0;
            mainMapDisplayerViewModel =
                new ElementDisplayerViewModel<DepictionElementBackdrop>(depiction.MainMap as DepictionElementBackdrop,
                                                                        depiction,
                                                                        DepictionAccess.GeoCanvasToPixelCanvasConverter);
            foreach (var annotation in depiction.DepictionAnnotations)
            {
                Annotations.Add(new MapAnnotationViewModel(annotation, Scale, DepictionAccess.GeoCanvasToPixelCanvasConverter));
            }
            foreach (var revealer in depiction.Revealers)
            {
                var rVM = new RevealerDisplayerViewModel<IDepictionRevealer>(revealer, depiction,
                                                                             DepictionAccess.
                                                                                 GeoCanvasToPixelCanvasConverter);
                rVM.DisplayerTypeChange += rVM_DisplayerTypeChange;
                Revealers.Add(rVM);
            }

            if (WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid)
            {
                WorldCanvasModeVM = DepictionCanvasMode.Normal;
                var geoBounds = WorldModel.DepictionGeographyInfo.DepictionRegionBounds;
                var pixelBounds = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(geoBounds);
                DepictionRegionPixelBounds = new RectangleGeometry(pixelBounds);
            }
            else
            {
                StartRegionSelection();
            }

            UpdateCenterOnRegionCommand();//TODO find a good way to connect this stuff
        }

        #endregion

        #region Destruction

        protected override void OnDispose()
        {
            WorldCanvasModeVM = DepictionCanvasMode.Uninitialized;
            MainMapDisplayerViewModel.Dispose();
            foreach (var revealer in Revealers)
            {
                revealer.DisplayerTypeChange -= rVM_DisplayerTypeChange;
                revealer.Dispose();
            }
            Revealers.Clear();
            Annotations.Clear();
            DetachEventsFromModel();

            base.OnDispose();
        }

        #endregion
        #region Event attach/detach methods

        override protected void AttachEventsToModel()
        {
            base.AttachEventsToModel();
            WorldModel.DepictionRegionChange += DepictionWorldModel_DepictionRegionChange;
            WorldModel.AnnotationListChange += DepictionWorldModel_AnnotationListChange;
            WorldModel.RevealerListChange += DepictionWorldModel_RevealerListChange;
        }

        override protected void DetachEventsFromModel()
        {
            base.DetachEventsFromModel();
            WorldModel.DepictionRegionChange -= DepictionWorldModel_DepictionRegionChange;
            WorldModel.AnnotationListChange -= DepictionWorldModel_AnnotationListChange;
            WorldModel.RevealerListChange -= DepictionWorldModel_RevealerListChange;
        }

        void DepictionWorldModel_DepictionRegionChange(IMapCoordinateBounds oldRegion, IMapCoordinateBounds newRegion)
        {
            var mapPixRegion = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(newRegion);
            DepictionRegionPixelBounds = new RectangleGeometry(mapPixRegion);
            UpdateCenterOnRegionCommand();
        }

        void DepictionWorldModel_AnnotationListChange(object sender, NotifyCollectionChangedEventArgs changeDetails)// IDepictionAnnotation modifiedAnnotation, AddRemoveEnum annotationResult)
        {
            switch (changeDetails.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in changeDetails.NewItems)
                    {
                        var modifiedAnnotation = item as IDepictionAnnotation;
                        if (modifiedAnnotation == null) continue;
                        var annotation = new MapAnnotationViewModel(modifiedAnnotation, Scale, DepictionAccess.GeoCanvasToPixelCanvasConverter);
                        Annotations.Add(annotation);
                        //  RequestShowAnnotationProperty(annotation);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in changeDetails.OldItems)
                    {
                        var modifiedAnnotation = item as IDepictionAnnotation;
                        if (modifiedAnnotation == null) continue;
                        var matching =
                            Annotations.Where(t => t.AnnotationID.Equals(modifiedAnnotation.AnnotationID)).ToList();
                        foreach (var annotToRemove in matching)
                        {
                            Annotations.Remove(annotToRemove);
                        }
                    }
                    break;
            }
        }

        void DepictionWorldModel_RevealerListChange(object sender, NotifyCollectionChangedEventArgs changeDetails)//(IDepictionRevealer revealer, AddRemoveEnum addOrRemove)
        {
            Debug.WriteLine("a revealer change was made threading could be an issue");

            switch (changeDetails.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in changeDetails.NewItems)
                    {
                        var revealer = item as IDepictionRevealer;
                        if (revealer == null) continue;
                        var rVM = new RevealerDisplayerViewModel<IDepictionRevealer>(revealer, null,
                                                                                     DepictionAccess.GeoCanvasToPixelCanvasConverter);
                        rVM.DisplayerTypeChange += rVM_DisplayerTypeChange;
                        Revealers.Add(rVM);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in changeDetails.OldItems)
                    {
                        var revealer = item as IDepictionRevealer;
                        if (revealer == null) continue;
                        var matching = Revealers.Where(t => t.ModelID.Equals(revealer.DisplayerID)).ToList();
                        foreach (var toRemove in matching)
                        {
                            Revealers.Remove(toRemove);
                            toRemove.Dispose();
                        }
                    }
                    GC.Collect();
                    break;
            }
        }

        #endregion

        #region Commands and matching methods

        #region Center on region or start point

        override protected void CenterOriginMapLocation()
        {
            if (CanCenterOnOriginLocation() && !CanCenterOnRegion()) base.CenterOriginMapLocation();

            if (!WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid) return;
            var latLongCenter = WorldModel.DepictionGeographyInfo.DepictionRegionBounds.Center;
            CenterOnLatLongLocation(latLongCenter);
            //            //Order is important because the shift chagnes the StaticMainWindowToGeoCanvasConverterViewModel results.
            var geoBounds = WorldModel.DepictionGeographyInfo.DepictionRegionBounds;
            var pixelBounds = StaticMainWindowToGeoCanvasConverterViewModel.WorldToGeoCanvas(geoBounds);
            RegionSelectionBounds = pixelBounds;//minor hack to make sure the region bounds (if in selection mode) stay with the center command
        }

        protected bool CanCenterOnRegion()
        {
            if (WorldModel == null) return false;
            if (WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid) return true;
            return false;
        }
        #endregion

        #region start region selection command
        public ICommand StartRegionSelectionCommand
        {
            get
            {
                if (startRegionSelection == null)
                {
                    startRegionSelection = new DelegateCommand(StartRegionSelection);
                }
                return startRegionSelection;
            }
        }

        protected void StartRegionSelection()
        {
            if (WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid)
            {
                var geoBounds = WorldModel.DepictionGeographyInfo.DepictionRegionBounds;
                var pixelBounds = StaticMainWindowToGeoCanvasConverterViewModel.WorldToGeoCanvas(geoBounds);
                RegionSelectionBounds = pixelBounds;//minor hack

                DepictionRegionPixelBounds = null;// Make the bounds disappear.new RectangleGeometry(pixelBounds);
                UpdateCenterOnRegionCommand();
            }
            WorldCanvasModeVM = DepictionCanvasMode.RegionSelection;
        }
        #endregion

        #region end region selection command

        public ICommand EndRegionSelectionCommand
        {
            get
            {
                if (endRegionSelection == null)
                {
                    endRegionSelection = new DelegateCommand<Rect>(BruteForceEndRegion);
                }
                return endRegionSelection;
            }
        }
        private void BruteForceEndRegion(Rect pixelRegion)
        {
            if (EndRegionSelection(pixelRegion))
            {
                Debug.WriteLine("Region selection failed");
            }
        }
        virtual protected bool EndRegionSelection(Rect pixelRegion)
        {
            if (pixelRegion.IsEmpty || (pixelRegion.X == 0 && pixelRegion.Y == 0 && pixelRegion.Width == 0 && pixelRegion.Height == 0))
            {
                var oldBounds = WorldModel.DepictionGeographyInfo.DepictionRegionBounds;
                if (!oldBounds.IsValid)
                {
                    ApplicationCommands.New.Execute(null, Application.Current.MainWindow);
                    return false;
                }
                var oldPixelBounds = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(oldBounds);
                DepictionRegionPixelBounds = new RectangleGeometry(oldPixelBounds);
                return true;//Not sure about this return
            }

            var geoBounds = StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(pixelRegion);

            if (!geoBounds.IsValid)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Invalid depiction region.", 4);
                return false;
            }
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    if (!DepictionGeographicInfo.VerifyRegionSizeForPrep(geoBounds))
                    {
                        return false;
                    }
                    break;
                default:
                    if (!DepictionGeographicInfo.VerifyLargeRegionArea(geoBounds))
                    {
                        //DepictionAccess.NotificationService.DisplayMessageString("Region too large.", 4);
                        return false;
                    }
                    break;
            }

            //Reset the center location
            var geoConverter = new MercatorCoordinateConverter(viewExtent, 0, 0);
            var pixCenter = geoConverter.WorldToGeoCanvas(geoBounds.Center);
            //Not sure if the converter should be in the model
            DepictionAccess.GeoCanvasToPixelCanvasConverter = new MercatorCoordinateConverter(viewExtent, pixCenter.X, pixCenter.Y);
            ((ScreenToMapPixelConverter)StaticMainWindowToGeoCanvasConverterViewModel).UpdatePixelConverter(DepictionAccess.GeoCanvasToPixelCanvasConverter);
            SetWorldCanvasOffset(DepictionAccess.GeoCanvasToPixelCanvasConverter.Offsetx, DepictionAccess.GeoCanvasToPixelCanvasConverter.Offsety);
            //End the madness
            WorldCanvasModeVM = DepictionCanvasMode.Normal;
            WorldModel.RegionBounds = geoBounds;
            var mapPixRegion = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(geoBounds);
            DepictionRegionPixelBounds = new RectangleGeometry(mapPixRegion);
            var interiorRegion = MapCoordinateBounds.GetGeoRectThatIsPercentage(geoBounds, 1.2, null, -1);
            WorldModel.RequestVisibleViewportBounds(interiorRegion);
            UpdateCenterOnRegionCommand();
            return true;
        }

        #endregion
        #region add revealer command
        public ICommand AddRevealerCommand
        {
            get
            {
                if (addRevealer == null)
                {
                    switch (DepictionAccess.ProductInformation.ProductType)
                    {
                        case ProductInformationBase.Prep:
                            addRevealer = new DelegateCommand(CreateAndAddRevealer, CanAddRevealer);
                            break;
                        default:
                            addRevealer = new DelegateCommand(CreateAndAddRevealer);
                            break;
                    }
//#if PREP
//                    addRevealer = new DelegateCommand(CreateAndAddRevealer,CanAddRevealer);
//#else
//                    addRevealer = new DelegateCommand(CreateAndAddRevealer);
//#endif

                }
                return addRevealer;
            }
        }

        private int MaxRevealersForPrep = 5;
        private static int revealerCount = 0;
        private bool HasMaxRevealerCountBeenReached(int maxCount)
        {
            var revealerCount = WorldModel.Revealers.Count;

            if (revealerCount >= maxCount)
            {
                return false;
            }
            return true;
        }
        private bool CanAddRevealer()
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    return HasMaxRevealerCountBeenReached(MaxRevealersForPrep);
            }
//#if PREP
//            return HasMaxRevealerCountBeenReached(MaxRevealersForPrep);
//#endif
            return true;
        }
        
        private void CreateAndAddRevealer()
        {
            var geoBounds = GetGeoRectInteriorToViewport(pixelViewport);
            revealerCount++;
            var revealer = new DepictionRevealer("Enter Name Here - Revealer" + revealerCount, geoBounds);

            WorldModel.AddRevealer(revealer);
        }
        #endregion

        #region delete revealer command

        public ICommand RemoveRevealerCommand
        {
            get
            {
                if (removeRevealer == null)
                {
                    removeRevealer = new DelegateCommand<IElementRevealerDisplayerViewModel>(RemoveRevealer);
                }
                return removeRevealer;
            }
        }

        private void RemoveRevealer(IElementRevealerDisplayerViewModel revealerToRemove)
        {
            var revealer = revealerToRemove as RevealerDisplayerViewModel<IDepictionRevealer>;
            if (revealer == null) return;
            var result =
            DepictionAccess.NotificationService.DisplayInquiryDialog(
                string.Format("Do you want to delete {0}?", revealerToRemove.DisplayerName), "Delete Revealer",
                MessageBoxButton.YesNo);

            if (result.Equals(MessageBoxResult.No)) return;
            //            if (Equals(revealer,topRevealer))
            //            {
            //                topRevealer = null;
            //            }
            WorldModel.DeleteRevealer(revealer.DisplayerModel);
        }
        #endregion
        #region Annotation stuff
        //TODO grr
        public ICommand ShowAnnotationPropertyInfoCommand
        {
            get
            {
                if (showAnnotationPropertyInfo == null)
                {
                    showAnnotationPropertyInfo = new DelegateCommand<MapAnnotationViewModel>(ShowAnnotationPropertyDialog);
                    showAnnotationPropertyInfo.Text = "Edit annotation";
                }
                return showAnnotationPropertyInfo;
            }
        }

        private void ShowAnnotationPropertyDialog(MapAnnotationViewModel annotationVM)
        {
            if (RequestShowAnnotationProperty != null)
            {
                RequestShowAnnotationProperty(annotationVM);
            }
        }

        public ICommand AddAnnotationCommand
        {
            get
            {
                var command = new DelegateCommand<Point>(AddAnnotationAtLocation);
                command.Text = "Add annotation";
                return command;
            }
        }
        protected void AddAnnotationAtLocation(Point location)
        {
            var annotationModel = new DepictionAnnotation();
            annotationModel.MapLocation = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location);
            WorldModel.AddAnnotation(annotationModel);
        }
        public ICommand DeleteAnnotationCommand
        {
            get
            {
                if (deleteAnnotationCommand == null)
                {
                    deleteAnnotationCommand = new DelegateCommand<MapAnnotationViewModel>(DeleteAnnotation);
                    deleteAnnotationCommand.Text = "Delete annotation";
                }
                return deleteAnnotationCommand;
            }
        }

        private void DeleteAnnotation(MapAnnotationViewModel annotationVM)
        {
            WorldModel.RemoveAnnotation(annotationVM.annotationModel);
        }

        #endregion

        #endregion

        #region Methods, mostly used from commands
        virtual public void GetDataForPoint(Point location)
        {
            if (StaticMainWindowToGeoCanvasConverterViewModel != null)
            {
                var mapLocation = StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(location);
                if (!DepictionGeographicInfo.WorldLatLongBoundingBox.Contains(mapLocation))
                {
                    mapLocation = new LatitudeLongitude();
                }
                MapLocationInformation = mapLocation.ToString();
            }
        }

        public void CenterOnLatLongLocation(ILatitudeLongitude newCenter)
        {
            pixelViewport = Rect.Empty;//Silently reset the viewport since the center normally comes from the center//hack
            CurrentCenter = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(newCenter);
        }

        static public IMapCoordinateBounds GetGeoRectInteriorToViewport(Rect viewport)
        {
            var aW = viewport.Width;
            var aH = viewport.Height;

            var w = aW * .6;
            var h = aH * .6;
            var t = aH * .2 + viewport.Top;
            var l = aW * .2 + viewport.Left;
            var pixelSize = new Rect(l, t, w, h);
            return DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(pixelSize);
        }

        #endregion
        #region Action stuff

        virtual public void rVM_DisplayerTypeChange(IElementDisplayerViewModel caller, DepictionDisplayerType obj)
        {
            //this is used further down the line to control the CurrentDisplayer getting viewed in display content
        }

        #endregion
        //        #region temp helper methods for testing
        //
        //        public IDepictionElementBase AddElementWithZOI(Point startPoint)
        //        {
        //            var elementModel = new DepictionElementParent();
        //            
        //            elementModel.Position = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(startPoint);
        //            var pixelZOI = RandomHelpers.CreateAZOI(startPoint);
        //            var geoZOI =
        //                DepictionGeometryToEnhancedPointListWithChildrenConverter.EnhancedPointListWithChildrenListToIGeometry(
        //                    pixelZOI, "Polygon", DepictionAccess.GeoCanvasToPixelCanvasConverter);
        //            var zoi = new ZoneOfInfluence(geoZOI);
        //            elementModel.SetInitialPositionAndZOI(new LatitudeLongitude(), zoi);
        //            return elementModel;
        //        }
        //
        //        #endregion
    }
}
