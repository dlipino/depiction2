﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class RevealerDisplayerViewModel<T> : ElementDisplayerViewModel<T>, IDataErrorInfo, IElementRevealerDisplayerViewModel where T : IDepictionRevealer
    {
        #region Constants
        private const double OPACITY_MIN = .0;
        private const double OPACITY_MAX = 1;//hmm, this hasn't worked out as planned, this was supposed to be the hard stop, but the view made it impractical
        private const double DEFAULTOPACITY = .6;

        private const string MainThumbTemplate = "RevealerMainThumbTemplate";
        private const string RectangleTemplate = "RevealerRectangleTemplate";
        private const string CircleTemplate = "RevealerCircleTemplate";
        private const string MinimizeRevealerStyleName = "MinimizedRevealerStyle";
        private const string NormalRevealerStyleName = "RevealerFrameStyle";
        #endregion

        #region Variables
        private string revealerTemplateName = RectangleTemplate;
        private string revealerStyleName = NormalRevealerStyleName;
        private RevealerShapeType revealerShape = RevealerShapeType.Rectangle;
        private double revealerOpacity = DEFAULTOPACITY;
        private double scale = 1;
        private bool permaTextVisible;
        private double minContentLocationX = DepictionRevealer.MINCONTENT_X;

        private double minContentLocationY = DepictionRevealer.MINCONTENT_Y;
        private bool isRevealerMaximized;
        private bool isRevealerBorderVisible;
        private bool isRevealerAnchored;
        private bool revealerBottomVisible = true;
        private bool revealerSideVisible;
        private bool revealerTopVisible = true;
        private Rect clipBounds;
        private TranslateTransform topLeftTransform = new TranslateTransform(0, 0);
        private double actualWidth = 0;
        private double actualHeight = 0;
        private bool mouseByPassEnabled;
        #endregion

        #region Constructor
        
        public RevealerDisplayerViewModel(T model, IDepictionStory mainDepiction, IGeoConverter geoVsPixelConverter)
            : base(model, mainDepiction, geoVsPixelConverter)
        {
            ClipBounds = DepictionPixelToMapConverter.WorldToGeoCanvas(model.RevealerBounds);
            actualWidth = model.RevealerBounds.GetMidLineHorizontalDistance(Settings.Default.MeasurementSystem,
                                                                          MeasurementScale.Large);
            actualHeight = model.RevealerBounds.GetMidLineVerticalDistance(Settings.Default.MeasurementSystem, MeasurementScale.Large);
            Settings.Default.PropertyChanged += Default_PropertyChanged;

            DisplayerModel.GetPropertyValue("Maximized", out isRevealerMaximized);
            RevealerMaximized = isRevealerMaximized;
            DisplayerModel.GetPropertyValue("BorderVisible", out isRevealerBorderVisible);
            DisplayerModel.GetPropertyValue("Anchored", out isRevealerAnchored);
            DisplayerModel.GetPropertyValue("RevealerShape", out revealerShape);
            switch (revealerShape)
            {
                case RevealerShapeType.Circle:
                    revealerTemplateName = CircleTemplate;
                    break;
                default:
                    revealerTemplateName = RectangleTemplate;
                    break;
            }
            DisplayerModel.GetPropertyValue("Opacity", out revealerOpacity);
            DisplayerModel.GetPropertyValue("BottomMenu", out  revealerBottomVisible);
            DisplayerModel.GetPropertyValue("TopMenu", out  revealerTopVisible);
            DisplayerModel.GetPropertyValue("SideMenu", out  revealerSideVisible);
            DisplayerModel.GetPropertyValue("PermaTextVisible", out  permaTextVisible);
            DisplayerModel.GetPropertyValue("PermaTextX", out  minContentLocationX);
            DisplayerModel.GetPropertyValue("PermaTextY", out  minContentLocationY);
            DisplayerType = DepictionDisplayerType.Revealer;
        }

        #endregion
        #region Disposing
        protected override void OnDispose()
        {
            Settings.Default.PropertyChanged -= Default_PropertyChanged;
            base.OnDispose();
        }
        #endregion
        #region Properties
        public bool MouseByPassEnabled { get { return mouseByPassEnabled; } set { mouseByPassEnabled = value; NotifyPropertyChanged("MainThumbDraggable"); } }
        new public DepictionDisplayerType DisplayerType
        {
            get { return DisplayerModel.DisplayerType; }
            set
            {
                DisplayerModel.DisplayerType = value;
                NotifyPropertyChanged("DisplayerType");
                NotifyDisplayerTypeChange();
            }
        }
        public bool PermaTextVisible
        {
            get { return permaTextVisible; }
            set
            {
                permaTextVisible = value;
                DisplayerModel.SetPropertyValue("PermaTextVisible", permaTextVisible);
                NotifyPropertyChanged("PermaTextVisible");
            }
        }

        public double MinContentLocationX
        {
            get { return minContentLocationX; }
            set
            {
                minContentLocationX = value;
                DisplayerModel.SetPropertyValue("PermaTextX", minContentLocationX);
                NotifyPropertyChanged("MinContentLocationX");
            }
        }

        public double MinContentLocationY
        {
            get { return minContentLocationY; }
            set
            {
                minContentLocationY = value;
                DisplayerModel.SetPropertyValue("PermaTextY", minContentLocationY);
                NotifyPropertyChanged("MinContentLocationY");
            }
        }

        public double Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        public Geometry ClipArea
        {
            get
            {
                //Kind of a cheap trick, whenever the clip area is requested (usually just on an update) set the model
                DisplayerModel.RevealerBounds = DepictionPixelToMapConverter.GeoCanvasToWorld(clipBounds);

                ClearElementCount();

                actualWidth = DisplayerModel.RevealerBounds.GetMidLineHorizontalDistance(Settings.Default.MeasurementSystem, MeasurementScale.Large);
                actualHeight = DisplayerModel.RevealerBounds.GetMidLineVerticalDistance(Settings.Default.MeasurementSystem, MeasurementScale.Large);
                NotifyPropertyChanged("ActualWidth");
                NotifyPropertyChanged("ActualHeight");
                switch (RevealerShape)
                {
                    case RevealerShapeType.Rectangle:
                        return new RectangleGeometry(ClipBounds);
                    case RevealerShapeType.Circle:
                        return new EllipseGeometry(ClipBounds);
                }

                return new RectangleGeometry(ClipBounds);
            }
        }

        public Rect ClipBounds
        {
            get { return clipBounds; }
            set
            {
                clipBounds = value;
                topLeftTransform.X = clipBounds.X;
                topLeftTransform.Y = clipBounds.Y;

                NotifyPropertyChanged("ClipArea");
                NotifyPropertyChanged("PixelWidth");
                NotifyPropertyChanged("PixelHeight");
            }
        }
        public RevealerShapeType RevealerShape
        {
            get { return revealerShape; }
            set
            {
                if (revealerShape == value) return;
                revealerShape = value;
                DisplayerModel.SetPropertyValue("RevealerShape", revealerShape);
                switch (revealerShape)
                {
                    case RevealerShapeType.Circle:
                        PixelHeight = PixelWidth;
                        revealerTemplateName = CircleTemplate;

                        break;
                    //case RevealerShapeType.Rectangle://For reference
                    default:
                        revealerTemplateName = RectangleTemplate;
                        break;
                }

                NotifyPropertyChanged("RevealerShape");
                NotifyPropertyChanged("ClipArea");
                NotifyPropertyChanged("EdgeDraggerTemplate");
            }
        }
        public string ActualWidth
        {
            get
            {
                var displayWidth = actualWidth;
                return displayWidth.ToString(string.Format("N{0}", Settings.Default.Precision));
            }
            set
            {
                var displayWidth = double.Parse(value);
               
                actualWidth = displayWidth;
                var midLat = (DisplayerModel.RevealerBounds.Top + DisplayerModel.RevealerBounds.Bottom) / 2;
                var lon = DisplayerModel.RevealerBounds.Left;
                var midLatLong = new LatitudeLongitudeBase(midLat, lon);

                var otherLatLong = MapCoordinateBounds.GetLatLongAtDistanceFromOrigin(midLatLong, actualWidth,
                                                                                      Settings.Default.MeasurementSystem,
                                                                                      MeasurementScale.Large, 90);
                var endPixelLoc = DepictionPixelToMapConverter.WorldToGeoCanvas(otherLatLong);
                PixelWidth = endPixelLoc.X - clipBounds.Left;

            }
        }

        public string ActualHeight
        {
            get
            {
                var displayHeight = actualHeight;
                switch (revealerShape)
                {
                    case RevealerShapeType.Circle:
                        displayHeight = displayHeight / 2;
                        break;
                }
                return displayHeight.ToString("N" + Settings.Default.Precision);
            }
            set
            {
                var displayHeight = double.Parse(value);
                switch (revealerShape)
                {
                    case RevealerShapeType.Circle:
                        displayHeight = displayHeight * 2;
                        break;
                }
                actualHeight = displayHeight;
                var midLon = (DisplayerModel.RevealerBounds.Left + DisplayerModel.RevealerBounds.Right) / 2;
                var lat = DisplayerModel.RevealerBounds.Top;
                var midLatLong = new LatitudeLongitudeBase(lat, midLon);

                var otherLatLong = MapCoordinateBounds.GetLatLongAtDistanceFromOrigin(midLatLong, actualHeight,
                                                                                      Settings.Default.MeasurementSystem,
                                                                                      MeasurementScale.Large, 180);
                var endPixelLoc = DepictionPixelToMapConverter.WorldToGeoCanvas(otherLatLong);
                PixelHeight = endPixelLoc.Y - clipBounds.Top;
            }
        }
        public string Units { get { return new Distance().GetUnits(Settings.Default.MeasurementSystem, MeasurementScale.Large); } }
        public double PixelWidth
        {
            get { return clipBounds.Width; }
            set
            {
                clipBounds.Width = value;
                NotifyPropertyChanged("PixelWidth");
                NotifyPropertyChanged("ClipArea");
            }
        }
        public double PixelHeight
        {
            get { return clipBounds.Height; }
            set
            {
                clipBounds.Height = value;
                if (RevealerShape.Equals(RevealerShapeType.Circle))
                {
                    clipBounds.Width = value;
                    NotifyPropertyChanged("PixelWidth");
                }
                NotifyPropertyChanged("PixelHeight");
                NotifyPropertyChanged("ClipArea");
            }
        }

        public TranslateTransform TopLeftTransform
        {
            get { return topLeftTransform; }
        }

        public bool RevealerMaximized
        {
            get { return isRevealerMaximized; }
            set
            {
                isRevealerMaximized = value;
                DisplayerModel.SetPropertyValue("Maximized", isRevealerMaximized);
                if (isRevealerMaximized)
                {
                    revealerStyleName = NormalRevealerStyleName;
                }
                else
                {
                    PermaTextVisible = true;
                    revealerStyleName = MinimizeRevealerStyleName;
                }
                NotifyPropertyChanged("RevealerMaximized");
                NotifyPropertyChanged("RevealerFrameStyle");
                NotifyPropertyChanged("MainThumbDraggable");
                NotifyPropertyChanged("EdgeThumbDraggable");
            }
        }
        public bool RevealerBorderVisible
        {
            get { return isRevealerBorderVisible; }
            set
            {
                isRevealerBorderVisible = value;
                DisplayerModel.SetPropertyValue("BorderVisible", isRevealerBorderVisible);
                NotifyPropertyChanged("RevealerBorderVisible");
                NotifyPropertyChanged("MainThumbDraggable");
                NotifyPropertyChanged("EdgeThumbDraggable");
            }
        }
        public bool RevealerAnchored
        {
            get { return isRevealerAnchored; }
            set
            {
                isRevealerAnchored = value;
                DisplayerModel.SetPropertyValue("Anchored", isRevealerAnchored);
                NotifyPropertyChanged("RevealerAnchored");
                NotifyPropertyChanged("MainThumbDraggable");
                NotifyPropertyChanged("EdgeThumbDraggable");
            }
        }

        public double RevealerOpacity
        {
            get { return revealerOpacity; }
            set
            {
                revealerOpacity = value;
                if (revealerOpacity > OPACITY_MAX) revealerOpacity = OPACITY_MAX;
                if (revealerOpacity < OPACITY_MIN) revealerOpacity = OPACITY_MIN;
                DisplayerModel.SetPropertyValue("Opacity", revealerOpacity);
                NotifyPropertyChanged("RevealerOpacity");
            }
        }

        public bool RevealerBottomVisible
        {
            get { return revealerBottomVisible; }
            set
            {
                revealerBottomVisible = value;
                DisplayerModel.SetPropertyValue("BottomMenu", revealerBottomVisible);
                NotifyPropertyChanged("RevealerBottomVisible");
            }
        }
        public bool RevealerTopVisible
        {
            get { return revealerTopVisible; }
            set
            {
                revealerTopVisible = value;
                DisplayerModel.SetPropertyValue("TopMenu", revealerTopVisible);
                NotifyPropertyChanged("RevealerTopVisible");
            }
        }
        public bool RevealerSideVisible
        {
            get { return revealerSideVisible; }
            set
            {
                revealerSideVisible = value;
                DisplayerModel.SetPropertyValue("SideMenu", revealerSideVisible);
                NotifyPropertyChanged("RevealerSideVisible");
            }
        }

        public bool MainThumbDraggable
        {
            get
            {
                if (RevealerAnchored) return false;
                if (!RevealerMaximized) return false;
                if (MouseByPassEnabled) return false;
                return true;
            }
        }
        public bool EdgeThumbDraggable
        {
            get { return !(RevealerAnchored && RevealerMaximized && RevealerBorderVisible); }
        }
        #endregion

        #region even handling

        void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("MeasurementSystem") || e.PropertyName.Equals("Precision"))
            {
                actualWidth =
                    DisplayerModel.RevealerBounds.GetMidLineHorizontalDistance(Settings.Default.MeasurementSystem,
                                                                               MeasurementScale.Large);
                actualHeight =
                    DisplayerModel.RevealerBounds.GetMidLineVerticalDistance(Settings.Default.MeasurementSystem,
                                                                             MeasurementScale.Large);
                NotifyPropertyChanged("ActualWidth");
                NotifyPropertyChanged("ActualHeight");
                NotifyPropertyChanged("Units");
            }
        }

        #endregion

        #region Methods
        public void SetTopLeft(double top, double left)
        {
            clipBounds.X = left;
            clipBounds.Y = top;
            topLeftTransform.X = left;
            topLeftTransform.Y = top;
            NotifyPropertyChanged("ClipArea");
        }
        public void SetClipBounds(Rect newBounds)
        {
            bool changeHeight = !PixelHeight.Equals(newBounds.Height);
            bool changeWidth = !PixelWidth.Equals(newBounds.Width);
            if (changeHeight)
            {
                clipBounds.Height = newBounds.Height;
                if (RevealerShape.Equals(RevealerShapeType.Circle))
                {
                    clipBounds.Width = newBounds.Height;
                    NotifyPropertyChanged("PixelWidth");
                }
                NotifyPropertyChanged("PixelHeight");
            }
            if (changeWidth)
            {
                PixelWidth = newBounds.Width;
                if (RevealerShape.Equals(RevealerShapeType.Circle))
                {
                    clipBounds.Height = newBounds.Width;
                    NotifyPropertyChanged("PixelHeight");
                }
                NotifyPropertyChanged("PixelWidth");
            }
            SetTopLeft(newBounds.Top, newBounds.Left);
        }
        #endregion
        #region Implementation of IDataErrorInfo

        private string errorString = string.Empty;
        public string this[string columnName]
        {
            get
            {
                errorString = string.Empty;
                if (columnName.Equals("ActualWidth") || columnName.Equals("ActualHeight"))
                {
                    if (actualWidth < 0) return "Must be greater than zero;";
                    if (actualHeight < 0) return "Must be greater than zero;";
                    return errorString;
                }
                return null;
            }
        }

        public string Error
        {
            get { return errorString; }
        }

        #endregion

        #region Visual templates setters

        public ControlTemplate MainDraggerTemplate
        {
            get
            {
                ControlTemplate template = null;
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    if (Application.Current.Resources.Contains(MainThumbTemplate))
                    {
                        template = Application.Current.Resources[MainThumbTemplate] as ControlTemplate;
                    }
                }
                return template;
            }
        }
        public ControlTemplate EdgeDraggerTemplate
        {
            get
            {
                ControlTemplate template = null;

                if (Application.Current != null && Application.Current.Resources != null)
                {
                    if (Application.Current.Resources.Contains(revealerTemplateName))
                    {
                        template = Application.Current.Resources[revealerTemplateName] as ControlTemplate;
                    }
                }
                return template;
            }
        }
        public Style RevealerFrameStyle
        {
            get
            {
                Style frameStyle = null;
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    if (Application.Current.Resources.Contains(revealerStyleName))
                    {
                        frameStyle = Application.Current.Resources[revealerStyleName] as Style;
                    }
                }
                return frameStyle;
            }
        }

        #endregion
    }
}
