﻿using System;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypeConverters;
using Depiction.CoreModel.DepictionObjects;

namespace Depiction.ViewModels.ViewModels.PrototypeViewModels
{
    public class ElementPrototypeViewModel : ViewModelBase
    {
        public IBaseDepictionMapType Model { get; private set; }

        #region Variables

        private string author = string.Empty;
        private string description = string.Empty;
        private bool isCurrentAddable = true;
        private string elementType = "";

        #endregion
        #region Properties for View
        public string ElementType { get { return Model.ElementType; } }
        public string DisplayName { get { return Model.TypeDisplayName; } }

        public string DefinitionGroup { get; private set; }
        public string DefinitionSource { get; private set; }
       // public string PrototypeOriginDescription { get; private set; }
        public string Author { get { return author; } }
        public string Description { get { return description; } }
        //omghackz (again)
        public DepictionSortPriority SortPriority { get; set; }

        public ImageSource IconSource
        {
            get
            {
                var iconPath = Model.IconPath;
                return DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath);
            }
        }
        public bool IsCurrentlyAddable
        {
            get { return isCurrentAddable; }
            set{ isCurrentAddable = value; NotifyPropertyChanged("IsCurrentlyAddable");}
        }
        public bool IsPlaceableByMouse
        {
            get
            {
                var temp = Model as IElementPrototype;
                if (temp == null) return true;
                return temp.IsMouseAddable;
            }
        }
        public bool IsAddableByFile
        {
            get
            {
                var temp = Model as IElementPrototype;
                if (temp == null) return false;
                return temp.IsFileAddable;
            }
        }
        public bool AddByZOI
        {
            get
            {
                var shapeType = ZOIShape();
                if (shapeType.Equals(ZOIShapeType.UserLine) || shapeType.Equals(ZOIShapeType.UserPolygon)) return true;
                return false;
            }
        }
        #endregion

        #region Constructor
        public ElementPrototypeViewModel(IBaseDepictionMapType model)
        {
            if(model == null)
            {
                throw new Exception("Element model cannot be null");
            }
            Model = model;
            elementType = model.ElementType;
            SortPriority = DepictionSortPriority.Normal;
            var propHolder = model as IElementPrototype;
            if(propHolder != null)
            {
                var hasAuthor = propHolder.GetPropertyValue("Author", out author);
                if(!hasAuthor)
                {
                    author = "No author";
                }
                var hasDescription = propHolder.GetPropertyValue("Description", out description);
                if (!hasDescription)
                {
                    description = "No description";
                }

                DefinitionSource = propHolder.PrototypeOrigin;
                DefinitionGroup = propHolder.DefinitionGroup;
            }
        }
        #endregion
        #region destruchtino

        protected override void OnDispose()
        {
            Model = null;
            base.OnDispose();
        }
        #endregion

        ZOIShapeType zoiType = ZOIShapeType.Unknown;
        public ZOIShapeType ZOIShape()
        {
           if(zoiType.Equals(ZOIShapeType.Unknown))
           {
               var propHolder = Model as IElementPropertyHolder;
               if (propHolder == null)
               {
                   if (Model is DepictionAnnotation)
                   {
                       zoiType = ZOIShapeType.Point;
                   }
                   return zoiType;
               }
               if (!propHolder.GetPropertyValue("ZOIShapeType", out zoiType))
               {
                   zoiType = ZOIShapeType.Point;
                   return zoiType;
               }
           }
            return zoiType;
        }

    }
}
