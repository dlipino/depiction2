﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels
{
    public class ElementViewSelectionListViewModel : ElementSelectionListViewModel
    {
        public IElementDisplayerViewModel DisplayerToControl { get; private set; }

        #region constructor

        public ElementViewSelectionListViewModel(IEnumerable<IDepictionElement> allElements, IElementDisplayerViewModel displayerToControl)
            : base(allElements)
        {
            SetDisplayerToControl(displayerToControl);
        }

        #endregion
        #region destructino
        protected override void OnDispose()
        {
            if (DisplayerToControl != null && DisplayerToControl.ViewModelManager != null)
            {
                DisplayerToControl.ViewModelManager.CollectionChanged -= DisplayerToControl_CollectionChanged;
            }
            DisplayerToControl = null;
            base.OnDispose();
        }
        #endregion
        public void SetDisplayerToControl(IElementDisplayerViewModel displayer)
        {
            if (originalElementList != null)
            {
                foreach (var element in originalElementList)
                {
                    element.IsSelected = false;
                }
            }
            if (DisplayerToControl != null)
            {
                DisplayerToControl.ViewModelManager.CollectionChanged -= DisplayerToControl_CollectionChanged;
            }
            DisplayerToControl = displayer;

            if (displayer != null && displayer.ViewModelManager != null)
            {
                DisplayerToControl.ViewModelManager.CollectionChanged += DisplayerToControl_CollectionChanged;
                var matching = originalElementList.ToDictionary(t => t.ElementID);
                foreach (var key in displayer.ElementIDsInDisplayer)
                {
                    if (matching.ContainsKey(key))
                    {
                        matching[key].IsSelected = true;
                    }
                }
            }
            NotifyPropertyChanged("DisplayerToControl");
        }

        void DisplayerToControl_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add) && DisplayerToControl != null)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is MapElementViewModel)
                    {
                        var key = ((MapElementViewModel)item).ElementKey;
                        if (originalElementDictionary.ContainsKey(key))
                        {
                            originalElementDictionary[key].IsSelected = true;
                        }
                    }
                }
            }
        }
    }
}
