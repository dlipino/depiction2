using System.Collections.Generic;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels.MapControlDialogViewModels
{
    public class ManageContentDialogVM : DialogViewModelBase
    {//Shares many element view modle manipulation methods with displaycontentDialogVM

        private DelegateCommand<List<string>> showSelectedInMainMapCommand;
        private DelegateCommand<List<string>> hideSelectedInMainMapCommand;
        private ManageContentModality manageModality = ManageContentModality.GeoAligned;
        #region Properties
        
        public int GeoLocatedElementCount
        {
            get
            {
                if (GeoLocatedElementSelectionVM == null) return 0;
                return GeoLocatedElementSelectionVM.TotalElementCount;
            }
        }
        public ElementSelectionListViewModel GeoLocatedElementSelectionVM { get; private set; }

        public int UnGeoLocatedElementCount
        {
            get
            {
                if (UnGeoLocatedElementSelectionVM == null) return 0;
                return UnGeoLocatedElementSelectionVM.TotalElementCount;
            }
        }
        public ElementSelectionListViewModel UnGeoLocatedElementSelectionVM { get; private set; }

        public ManageContentModality ManageModality
        {
            get { return manageModality; }
            set
            {
                if (!manageModality.Equals(value))
                {
                    manageModality = value;
                    NotifyPropertyChanged("ManageModality");
                }
            }
        }
        #region Commands

        public ICommand ShowSelectedInMainMapCommand
        {
            get
            {
                if (showSelectedInMainMapCommand == null)
                {
                    showSelectedInMainMapCommand=new DelegateCommand<List<string>>(AddElementsWithIdsToBackdrop);
                    showSelectedInMainMapCommand.Text = "Show selected elements in main canvas";
                }
                return showSelectedInMainMapCommand;
            }
        }
        public ICommand HideSelectedInMainMapCommand
        {
            get
            {
                if (hideSelectedInMainMapCommand == null)
                {
                    hideSelectedInMainMapCommand = new DelegateCommand<List<string>>(RemoveElementsWithIdsFromBackdrop);
                    hideSelectedInMainMapCommand.Text = "Remove selected elements from main canvas";
                }
                return hideSelectedInMainMapCommand;
            }
        }

        #endregion
        #endregion

        #region Constructor

        public ManageContentDialogVM(IElementRepository allElements)
        {
            GeoLocatedElementSelectionVM = new ElementSelectionListViewModel(allElements.ElementsGeoLocated)
                                               {ClearSelectionsOnFilter = true};
            UnGeoLocatedElementSelectionVM = new ElementSelectionListViewModel(allElements.ElementsUngeoLocated) { ClearSelectionsOnFilter = true };
        }

        #endregion

        #region Destruction (yes this is basically depricated will be fixed in 1.4 hopefully)

        protected override void OnDispose()
        {
            GeoLocatedElementSelectionVM.Dispose();
            UnGeoLocatedElementSelectionVM.Dispose();
            base.OnDispose();
        }
        #endregion

        protected override void ToggleDialogVisibility(bool? obj)
        {
            if (obj != true)
            {
                GeoLocatedElementSelectionVM.SelectElementsById(new List<string>(), false);
                UnGeoLocatedElementSelectionVM.SelectElementsById(new List<string>(), false);
            }
            base.ToggleDialogVisibility(obj);
        }
        public void UpdateAllElementTags()
        {
            GeoLocatedElementSelectionVM.UpdateElementTagList();
            UnGeoLocatedElementSelectionVM.UpdateElementTagList();
        }

        public void CreateVMFromModelsAndAdd(IEnumerable<IDepictionElement> elements)
        {
            var geoVMs = new List<MenuElementViewModel>();
            var ungeoVMs = new List<MenuElementViewModel>();
            foreach (var element in elements)
            {
                if (element.IsGeolocated)
                {
                    geoVMs.Add(new MenuElementViewModel(element));
                }
                else
                {
                    ungeoVMs.Add(new MenuElementViewModel(element));
                }
            }

            if (geoVMs.Count > 0)
            {
                GeoLocatedElementSelectionVM.AddElementVMList(geoVMs);
                NotifyPropertyChanged("GeoLocatedElementCount");
            }
            if (ungeoVMs.Count > 0)
            {
                UnGeoLocatedElementSelectionVM.AddElementVMList(ungeoVMs);
                NotifyPropertyChanged("UnGeoLocatedElementCount");
            }
        }

        public void RemoveElementVMsThatMatch(IEnumerable<IDepictionElement> elements)
        {//For now this has to be big hammer
            var allVMKeys = new List<string>();
            foreach (var element in elements)
            {
                allVMKeys.Add(element.ElementKey);
            }
            if(allVMKeys.Count >0)
            {
                GeoLocatedElementSelectionVM.RemoveElementByKeyVMList(allVMKeys);
                UnGeoLocatedElementSelectionVM.RemoveElementByKeyVMList(allVMKeys);
                NotifyPropertyChanged("GeoLocatedElementCount");
                NotifyPropertyChanged("UnGeoLocatedElementCount");
            }
        }
        public void SelectDesiredGeoLocatedElements(IEnumerable<string> elementIdList,bool addToExisting)
        {
            var oldClear = GeoLocatedElementSelectionVM.ClearSelectionsOnFilter;
            GeoLocatedElementSelectionVM.ClearSelectionsOnFilter = false;
            GeoLocatedElementSelectionVM.SelectionFilter = "";
            GeoLocatedElementSelectionVM.SelectElementsById(elementIdList,addToExisting);
            GeoLocatedElementSelectionVM.ClearSelectionsOnFilter = oldClear;
        }

        private void AddElementsWithIdsToBackdrop(IEnumerable<string> idList)
        {
            var story = DepictionAccess.CurrentDepiction;
            if(story == null) return;
            var elements = story.CompleteElementRepository.GetElementsFromIds(idList);
            story.AddElementListToMainMap(elements);
        }
        private void RemoveElementsWithIdsFromBackdrop(IEnumerable<string> idList)
        {
            var story = DepictionAccess.CurrentDepiction;
            if (story == null) return;
            var elements = story.CompleteElementRepository.GetElementsFromIds(idList);
            story.RemoveElementListFromMainMap(elements);
        }
    }
    public enum ManageContentModality
    {
        GeoAligned,
        NonGeoAligned
    }
}