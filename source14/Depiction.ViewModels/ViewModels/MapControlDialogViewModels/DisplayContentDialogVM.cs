﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.ViewModels.ViewModels.MapControlDialogViewModels
{
    public class DisplayContentDialogVM : DialogViewModelBase
    {//Shares many element view modle manipulation methods with MnageContentDialogVM
        #region variables
        private IElementDisplayerViewModel currentActiveDisplayer;
        private DepictionMapAndMenuViewModel viewModelOwner;
        private DepictionDisplayerType currentActiveDisplayerType = DepictionDisplayerType.MainMap;

        #endregion

        #region Properties
        //Create an independent variable to control tab selection and current selected revealer
        public ElementViewSelectionListViewModel ElementsForMapPlacementVM { get; set; }
        //Controls what tab the view is on
        public DepictionDisplayerType CurrentActiveDisplayerType
        {
            get { return currentActiveDisplayerType; }
            set
            {
               
                if (viewModelOwner == null) return;
                currentActiveDisplayerType = value;
                if (currentActiveDisplayerType.Equals(DepictionDisplayerType.MainMap))
                {
                    CurrentActiveDisplayer = viewModelOwner.MainMapDisplayerViewModel;
                }
                else if (currentActiveDisplayerType.Equals(DepictionDisplayerType.Revealer) ||
                   currentActiveDisplayerType.Equals(DepictionDisplayerType.TopRevealer))
                {
                    CurrentActiveRevealer = DepictionBasicMapViewModel.topRevealer;
                }
                NotifyPropertyChanged("CurrentActiveDisplayerType");
            }
        }
        public IElementRevealerDisplayerViewModel CurrentActiveRevealer
        {
            get { return DepictionBasicMapViewModel.topRevealer; }
            set
            {
                var revealer = value;
                if (revealer != null)
                {
                    if (DepictionBasicMapViewModel.topRevealer != null)
                    {
                        DepictionBasicMapViewModel.topRevealer.DisplayerType = DepictionDisplayerType.Revealer;
                    }
                    revealer.DisplayerType = DepictionDisplayerType.TopRevealer;
                    DepictionBasicMapViewModel.topRevealer = revealer;
                    
                }
                CurrentActiveDisplayer = value;
                NotifyPropertyChanged("CurrentActiveRevealer");
            }
        }

        public IElementDisplayerViewModel CurrentActiveDisplayer
        {
            get { return currentActiveDisplayer; }
            set
            {
                if (viewModelOwner == null) return;

                //Stops an infinite loop
                if (Equals(value, currentActiveDisplayer)) return;
                if(currentActiveDisplayer != null)
                {
                    currentActiveDisplayer.BaseDisplayerModel.VisibleElementsChange -= BaseDisplayerModel_VisibleElementsChange;
                }
                currentActiveDisplayer = value;
//                if (currentActiveDisplayer != null)
//                {
//                    currentActiveDisplayer.BaseDisplayerModel.VisibleElementsChange += BaseDisplayerModel_VisibleElementsChange;
//                }
                if (ElementsForMapPlacementVM != null)
                {
                    ElementsForMapPlacementVM.SetDisplayerToControl(currentActiveDisplayer);
                }
                NotifyPropertyChanged("CurrentActiveDisplayer");
            }
        }

        #endregion
        #region Command for display with elements

        private DelegateCommand<IElementDisplayerViewModel> showDisplayContentForCanvasCommand;
        public ICommand ShowDisplayContentForCanvasCommand
        {
            get
            {
                if (showDisplayContentForCanvasCommand == null)
                {
                    showDisplayContentForCanvasCommand = new DelegateCommand<IElementDisplayerViewModel>(ShowDisplayContentForCanvas);
                }
                return showDisplayContentForCanvasCommand;
            }
        }

        private void ShowDisplayContentForCanvas(IElementDisplayerViewModel obj)
        {
            ToggleDialogVisibility(true);
            var revealer = obj as IElementRevealerDisplayerViewModel;
            if(revealer == null) return;
            CurrentActiveRevealer = revealer;
            CurrentActiveDisplayerType = DepictionDisplayerType.Revealer;
        }

        #endregion
        #region Constructor
        public DisplayContentDialogVM(IEnumerable<IDepictionElement> allElements, DepictionMapAndMenuViewModel mainViewModel)//IElementDisplayerViewModel currentDisplayer)
        {
            DepictionBasicMapViewModel.topRevealer = null;
            viewModelOwner = mainViewModel;
            var mainMapViewModel = mainViewModel.MainMapDisplayerViewModel;
            currentActiveDisplayer = mainMapViewModel;
            //TODO clean up
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    ElementsForMapPlacementVM = new ElementViewSelectionListViewModel(allElements, mainMapViewModel);
                    
                    break;
                default:
                    ElementsForMapPlacementVM = new ElementViewSelectionListViewModel(allElements.Where(t => t.ElementType != "Depiction.Plugin.RoadNetwork"), mainMapViewModel);
                    break;
            }
            if (viewModelOwner != null)
            {
                viewModelOwner.Revealers.CollectionChanged += WorldModelViewModel_RevealerListChange;
            }
        }

        #endregion

        #region destruction

        protected override void OnDispose()
        {
            if (viewModelOwner != null)
            {
                viewModelOwner.Revealers.CollectionChanged -= WorldModelViewModel_RevealerListChange;
            }
            DepictionBasicMapViewModel.topRevealer = null;
            currentActiveDisplayerType = DepictionDisplayerType.MainMap;
            ElementsForMapPlacementVM.Dispose();
            if (currentActiveDisplayer != null)
            {
                currentActiveDisplayer.BaseDisplayerModel.VisibleElementsChange -= BaseDisplayerModel_VisibleElementsChange;
            }
            currentActiveDisplayer = null;
            base.OnDispose();
        }

        #endregion

        #region world event connectors

        void BaseDisplayerModel_VisibleElementsChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NotifyCollectionChangedEventArgs>(BaseDisplayerModel_VisibleElementsChange),
                    sender, e);
                return;
            }
            switch (e.Action)
            {
                #region Add
                case NotifyCollectionChangedAction.Add:
                    try
                    {
                        var elementsNew = new List<IDepictionElement>();
                        foreach (var item in e.NewItems)
                        {

                            var elements = item as IEnumerable<IDepictionElement>;
                            if (elements != null)
                            {
                                elementsNew.AddRange(elements);
                            }
                            else
                            {
                                var element = item as IDepictionElement;
                                if (element != null)
                                {
                                    elementsNew.Add(element);
                                }
                            }
                        }
                        SelectElementsFromList(elementsNew);
                    }
                    catch { }

                    break;
                #endregion

                #region Remove
                case NotifyCollectionChangedAction.Remove:
                    try
                    {
                        var elementsOld = new List<IDepictionElement>();
                        foreach (var item in e.OldItems)
                        {
                            var elements = item as IEnumerable<IDepictionElement>;
                            if (elements != null)
                            {
                                elementsOld.AddRange(elements);
                            }else
                            {
                                var element = item as IDepictionElement;
                                if(element != null)
                                {
                                    elementsOld.Add(element);
                                }
                            }
                        }
                        DeselectElementsFromList(elementsOld);
                    }
                    catch { }
                    break;
                #endregion
            }
        }

        void WorldModelViewModel_RevealerListChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                if (CurrentActiveDisplayerType.Equals(DepictionDisplayerType.Revealer))
                {
                    var newItems = e.NewItems;
                    if (newItems.Count > 0)
                    {
                        var first = newItems[0] as IElementRevealerDisplayerViewModel;
                        if (first != null) CurrentActiveRevealer = first;
                    }
                }
            }else if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                if (currentActiveDisplayer != null)
                {
                    if (viewModelOwner.Revealers.Count > 0 &&
                        (currentActiveDisplayer.DisplayerType.Equals(DepictionDisplayerType.Revealer) ||
                        currentActiveDisplayer.DisplayerType.Equals(DepictionDisplayerType.TopRevealer)))
                    {
                        CurrentActiveRevealer = viewModelOwner.Revealers[0];
                    }
                    else
                    {
                        DepictionBasicMapViewModel.topRevealer = null;
                    }
                }
            }
        }

        #endregion

        #region public methods

        #region by element manipulations
        protected void SelectElementsFromList(IEnumerable<IDepictionElement> elements)
        {
            //An odd loop is created when the user changes thigns using display content checkboxes.
            var ids = elements.Select(t => t.ElementKey);
            ElementsForMapPlacementVM.SelectElementsById(ids, true);
            ids = null;
        }
        protected void DeselectElementsFromList(IEnumerable<IDepictionElement> elements)
        {
            var ids = elements.Select(e => e.ElementKey);
            ElementsForMapPlacementVM.DeselectElementsById(ids);
            ids = null;
        }

        public void CreateVMFromElementListAndAdd(IEnumerable<IDepictionElement> elements)
        {
            var mapVMs = new List<MenuElementViewModel>();
            foreach (var element in elements)
            {
                if (element.IsGeolocated)
                {
                    mapVMs.Add(new MenuElementViewModel(element));
                }
            }

            if (mapVMs.Count > 0)
            {
                AddToElementVMList(mapVMs);
            }
        }

        public void RemoveElementVMsThatMatch(IEnumerable<IDepictionElement> elements)
        {
            var geoVMKeys = new List<string>();
            foreach (var element in elements)
            {
                geoVMKeys.Add(element.ElementKey);

            }

            if (geoVMKeys.Count > 0)
            {
                RemoveMatchingKeysFromElementVMList(geoVMKeys);
            }
        }

        #endregion

        #region by element key manipulations
      
        public void AddToElementVMList(List<MenuElementViewModel> elementVMs)
        {
            //Not sure if this call is needed because it is doen again in asddelemnevmlist
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<MenuElementViewModel>>(AddToElementVMList),
                                                                elementVMs);
                return;
            }
            ElementsForMapPlacementVM.AddElementVMList(elementVMs.Where(t => t.TypeDisplayName != "Road network"));
        }

        public void RemoveMatchingKeysFromElementVMList(List<string> elementVMKeys)
        {//Not sure if this call is needed because it is doen again in asddelemnevmlist
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<string>>(RemoveMatchingKeysFromElementVMList),
                                                                elementVMKeys);
                return;
            }
            ElementsForMapPlacementVM.RemoveElementByKeyVMList(elementVMKeys);
        }
        #endregion
        #endregion
    }
}