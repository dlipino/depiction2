using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.APIUnmanaged.Service;
using Depiction.CoreModel;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.Service.CoreBackgroundServices;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModels.PrototypeViewModels;
using Depiction.ViewModels.ViewModels.QuickstartViewModels;

namespace Depiction.ViewModels.ViewModels.MapControlDialogViewModels
{
    public class AddContentDialogVM : DialogViewModelBase
    {
        private DepictionEnhancedMapViewModel mapControlVM;
        private DelegateCommand loadPrototypeFromFileToLibraryCommand;
        //Is it time to create the thread safe observable?
        //private ObservableCollection<ElementPrototypeViewModel> allElementPrototypes = new ObservableCollection<ElementPrototypeViewModel>();
        private List<ElementPrototypeViewModel> prototypeList = new List<ElementPrototypeViewModel>();

        private AddContentMode _addMode = AddContentMode.Mouse;
        private bool doMouseSingleAdd = true;
        private string fileNameWithElements;
        private string locationToPlaceElementAt;
        private bool isLocationInvalid;
        private KeyValuePair<string, IDepictionImporterBase> selectedFileNameAndLoader;

        private DelegateCommand<ElementPrototypeViewModel> addElementTypeBySelectedMethod;
        private DelegateCommand<string> getFileName;

        #region Properties
        public bool ShowAdvancedOptions { get { return Settings.Default.DisplayAdvancedOptions; } }
        public CollectionViewSource PrototypeNamesViewSource { get; set; }
        public CollectionViewSource QuickStartItems { get; set; }

        public ElementPrototypeViewModel AutoDetectPrototypeViewModel { get; set; }
        public ElementPrototypeViewModel SelectedPrototypeViewModel { get; set; }//Not all that useful, because it doesn't control the view

        public bool DoMouseSingleAdd
        {
            get { return doMouseSingleAdd; }
            set
            {
                doMouseSingleAdd = value;
                NotifyPropertyChanged("DoMouseSingleAdd");
            }
        }

        public AddContentMode AddMode
        {
            get { return _addMode; }
            set
            {
                if (!_addMode.Equals(value))
                {
                    _addMode = value;
                    UpdateElementDefinitionAddableState();
                    NotifyPropertyChanged("AddMode");
                }
            }
        }

        public string FileNameWithElementsToAdd
        {
            get { return fileNameWithElements; }
            set { fileNameWithElements = value; NotifyPropertyChanged("FileNameWithElementsToAdd"); }
        }
        public string FileLoadInformationText
        {
            get
            {
                var s = new StringBuilder();
                s.AppendLine(string.Format("{0} can add elements from the following file formats:",
                                           DepictionAccess.ProductInformation.ProductName));
                s.AppendLine();
                //The proper way of doing this is to check the importers and add the descriptions as available
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        s.AppendLine("Spreadsheet (CSV)");
                        s.AppendLine("Image (jpg, tif, gif, png, bmp)");
                        break;
                    case ProductInformationBase.DepictionRW:
                        s.AppendLine("Elevation (dem, tiff, gtiff, bt, adf, hgt)");
                        s.AppendLine("ESRI Shapefile");
                        s.AppendLine("GML (geography markup)");
                        s.AppendLine("Image (jpg, tif, gif, png, bmp)");
                        break;
                    default:
                        s.AppendLine("Elevation (dem, tiff, gtiff, bt, adf, hgt)");
                        s.AppendLine("ESRI Shapefile");
                        s.AppendLine("GML (geography markup)");
                        s.AppendLine("GPX file (GPS devices)");
                        s.AppendLine("Spreadsheet (CSV)");
                        s.AppendLine("Image (jpg, tif, gif, png, bmp)");
                        break;
                }

                return s.ToString();
            }
        }

        public string LocationToPlaceElementAt
        {
            get { return locationToPlaceElementAt; }
            set { locationToPlaceElementAt = value; NotifyPropertyChanged("LocationToPlaceElementAt"); }
        }
        public bool CenterOnNewLocation { get; set; }
        public bool ConfineDataToRegion { get; set; }
        public bool IsLocationInvalid
        {
            get { return isLocationInvalid; }
            set { isLocationInvalid = value; NotifyPropertyChanged("IsLocationInvalid"); }
        }
        public DepictionWxsToElementService WebService { get; set; }
        #endregion
        #region Stuff picked up from addins, live reports for now

        public IDepictionDefaultImporter LiveReportAddin { get; set; }

        #endregion

        #region Commands

        #region Element file loading
        public ICommand LoadPrototypeFromFileToLibraryCommand
        {
            get
            {
                if (loadPrototypeFromFileToLibraryCommand == null)
                {
                    loadPrototypeFromFileToLibraryCommand = new DelegateCommand(FindLoadPrototypeFileToLibrary) { Text = "Load dml file into library" };
                }
                return loadPrototypeFromFileToLibraryCommand;
            }
        }
        public void FindLoadPrototypeFileToLibrary()
        {
            var prototypeFileName = DepictionAppViewModel.GetFileNameToLoad(null, FileFilter.DMLTypeFilter);
            if (string.IsNullOrEmpty(prototypeFileName)) return;
            var prototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(prototypeFileName);
            if (prototype == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                        string.Format("Could not create prototoype : {0}", Path.GetFileName(prototypeFileName)));
                return;
            }
            prototype.PrototypeOrigin = ElementPrototypeLibrary.LoadedPrototypeLocation;

            var library = DepictionAccess.ElementLibrary;
            if (library != null)
            {
                if (library.UserPrototypes.Contains(prototype))
                {
                    DepictionAccess.NotificationService.DisplayMessageString(
                        string.Format("Prototype {0} already exists in library", prototype.ElementType));
                    return;
                }
                var addSuccess = library.AddElementPrototypeToLibrary(prototype, false);
                if (!addSuccess)
                {
                    DepictionAccess.NotificationService.DisplayMessageString(
                        string.Format("Could not add prototoype : {0}", prototype.ElementType));
                }
                else
                {
                    library.NotifyPrototypeLibraryChange();
                }
            }
        }

        #endregion

        #region the add non quick start element command and methods

        public ICommand AddElementTypeBySelectedMethod
        {
            get
            {
                if (addElementTypeBySelectedMethod == null)
                {
                    addElementTypeBySelectedMethod = new DelegateCommand<ElementPrototypeViewModel>(AddTypeByMethod, CanAddElementType);
                    addElementTypeBySelectedMethod.Text = "Add element type using selected method";
                }
                return addElementTypeBySelectedMethod;
            }
        }

        private bool CanAddElementType(ElementPrototypeViewModel elementTypeToAdd)
        {
            if (elementTypeToAdd == null)
            {
                addElementTypeBySelectedMethod.Text = string.Format("Select an element type to add.");
                return false;
            }
            if (AddMode.Equals(AddContentMode.Mouse) && !elementTypeToAdd.IsPlaceableByMouse)
            {
                addElementTypeBySelectedMethod.Text = string.Format("Element types of {0} cannot be added by mouse.", elementTypeToAdd.DisplayName);
                return false;
            }
            if (AddMode.Equals(AddContentMode.Address))
            {
                if (elementTypeToAdd.IsPlaceableByMouse)
                {
                    if (string.IsNullOrEmpty(LocationToPlaceElementAt))
                    {
                        addElementTypeBySelectedMethod.Text =
                            string.Format("Empty address cannot be used for geolocating");
                        return false;

                    }
                }
                else
                {
                    addElementTypeBySelectedMethod.Text =
                              string.Format("Element types of {0} cannot be added by address.",
                                            elementTypeToAdd.DisplayName);
                    return false;
                }
                return true;
            }

            if (AddMode.Equals(AddContentMode.File))
            {
                if (!elementTypeToAdd.IsAddableByFile)//I guess this would only happen with auto add
                {
                    addElementTypeBySelectedMethod.Text = string.Format("Element types of {0} cannot be added by file.", elementTypeToAdd.DisplayName);
                    return false;
                }
                if (string.IsNullOrEmpty(FileNameWithElementsToAdd))
                {
                    addElementTypeBySelectedMethod.Text = string.Format("Please type in a file name.");
                    return false;
                }
            }

            addElementTypeBySelectedMethod.Text = string.Format("Add {0} to depiction", elementTypeToAdd.DisplayName);
            return true;
        }

        private void AddTypeByMethod(ElementPrototypeViewModel elementTypeToAdd)
        {
            if (elementTypeToAdd == null)
            {
                elementTypeToAdd = SelectedPrototypeViewModel;
            }
            if (elementTypeToAdd == null) elementTypeToAdd = AutoDetectPrototypeViewModel;
            var elementTypeString = elementTypeToAdd.Model.ElementType;
            switch (AddMode)
            {
                case AddContentMode.Mouse:
                    if (mapControlVM != null)
                    {
                        mapControlVM.StartElementMouseAdd(elementTypeToAdd, !DoMouseSingleAdd);
                    }
                    break;
                case AddContentMode.File:
                    if (selectedFileNameAndLoader.Key == null || !selectedFileNameAndLoader.Key.Equals(FileNameWithElementsToAdd))
                    {
                        selectedFileNameAndLoader = new KeyValuePair<string, IDepictionImporterBase>(FileNameWithElementsToAdd, null);
                    }

                    if (LoadFileWithElementInformation(selectedFileNameAndLoader, elementTypeString))
                    {
                    }
                    else
                    {
                        var message = string.Format("Could not load {0} as {1}", Path.GetFileName(selectedFileNameAndLoader.Key), elementTypeToAdd.Model.TypeDisplayName);
                        DepictionAccess.NotificationService.DisplayMessageString(message, 4);
                    }
                    break;
                case AddContentMode.Address:
                    var centerController = mapControlVM;
                    GeocodeSingleElement(elementTypeString, LocationToPlaceElementAt, null, centerController, new HashSet<string> { "source [location]" }, CenterOnNewLocation);

                    break;
                case AddContentMode.LiveReport:

                    if (LiveReportAddin != null)
                    {
                        LiveReportAddin.ImportElements(null, elementTypeString, null, null);
                    }
                    break;
                case AddContentMode.Web:
                    var app = Application.Current as DepictionApplication;
                    if (app == null) return;
                    var depictionRegion = app.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
                    if (!ConfineDataToRegion) depictionRegion = null;

                    var parameters = new Dictionary<string, object>();
                    parameters.Add("Area", depictionRegion);
                    parameters.Add("elementType", elementTypeString);
                    parameters.Add("format", WebService.ImageFormat);
                    parameters.Add("url", WebService.ServiceURL);
                    parameters.Add("layerName", WebService.LayerNames);
                    parameters.Add("style", WebService.Style);
                    if (WebService.WebServiceType == "WMS")
                    {

                        var operationThread = new WmsElementProviderBackground();
                        var name = string.Format("WMS: {0}", WebService.FirstLayerTitle);
                        parameters.Add("ServiceType", "WMS");

                        parameters.Add("name", name);
                        DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
                        operationThread.UpdateStatusReport(name);
                        operationThread.StartBackgroundService(parameters);
                    }
                    else
                    {
                        var operationThread = new WfsElementProviderBackground();
                        parameters.Add("ServiceType", "WFS");
                        var name = string.Format("WFS: {0}", WebService.FirstLayerTitle);
                        parameters.Add("name", name);
                        DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
                        operationThread.UpdateStatusReport(name);
                        operationThread.StartBackgroundService(parameters);
                    }

                    break;
            }

        }
        #endregion
        #region Geocoding one element

        //This needs to be put into a unit test
        static public BackgroundWorker GeocodeSingleElement(string elementTypeString, string stringLocation, IDepictionElementBase elementToGeoCode, DepictionEnhancedMapViewModel mapViewModel,
                HashSet<string> tags, bool centerOnElement)
        {
            if (string.IsNullOrEmpty(elementTypeString) && elementToGeoCode == null) return null;
            //This feels so wrong, and there also might be a small race condition
            var thread = new BackgroundWorker();
            thread.DoWork += delegate(object sender, DoWorkEventArgs e)
                            {
                                e.Result = false;
                                if (e.Argument == null) return;
                                var addressString = e.Argument.ToString();

                                var geoCoder = DepictionAccess.GeoCodingService as DepictionGeocodingService; ;
                                if (geoCoder == null)
                                {
                                    DepictionAccess.NotificationService.DisplayMessageString("Could not find a geocoding service.", 3);
                                    return;
                                }
                                if (string.IsNullOrEmpty(stringLocation))
                                {
                                    DepictionAccess.NotificationService.DisplayMessageString("Cannot geocode an empty string.", 3);
                                    return;
                                }
                                var addressStruct = new FullAddressParts { rawAddress = addressString };

                                e.Result = geoCoder.GeoCodeFromAddressParts(addressStruct);
                            };

            thread.RunWorkerCompleted += delegate(object sender, RunWorkerCompletedEventArgs e)
                                             {
                                                 if (e.Result is bool)
                                                 {
                                                     var boolResult = (bool)e.Result;
                                                     if (!boolResult)
                                                     {
                                                         return;
                                                     }
                                                 }

                                                 var result = e.Result as GeocodeResults;

                                                 if (result == null || !result.IsValid)
                                                 {
                                                     DepictionAccess.NotificationService.DisplayMessageString(string.Format("Could not geocode {0}", stringLocation), 3);
                                                     return;
                                                 }
                                                 var element = elementToGeoCode as IDepictionElement;
                                                 if (element == null) element = ElementFactory.CreateElementFromTypeString(elementTypeString);
                                                 if (tags != null)
                                                 {
                                                     foreach (var tag in tags)
                                                     {
                                                         element.Tags.Add(tag);
                                                     }
                                                 }
                                                 var position = result.Position;
                                                 element.Position = position;
                                                 element.SetPropertyValue("Draggable", false);
                                                 element.ElementUpdated = true;

                                                 bool showPreview;
                                                 if (!element.GetPropertyValue("ShowElementPropertiesOnCreate", out showPreview))
                                                 {
                                                     showPreview = false;
                                                 }

                                                 if (DepictionAccess.CurrentDepiction != null)
                                                 {
                                                     if (showPreview)
                                                     {
                                                         mapViewModel.AddTempElementWithPropertyDialogForElement(element);
                                                         if (mapViewModel != null && centerOnElement)
                                                         {
                                                             mapViewModel.CenterOnLatLongLocation(position);
                                                         }
                                                         return;
                                                     }
                                                     DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new[] { element }, true);
                                                     if (mapViewModel != null && centerOnElement)
                                                     {
                                                         mapViewModel.CenterOnLatLongLocation(position);
                                                     }
                                                 }
                                             };

            thread.RunWorkerAsync(stringLocation);
            return thread;
        }
        #endregion
        #region getting a file name

        public ICommand GetFileToLoadNameCommand
        {
            get
            {
                if (getFileName == null)
                {
                    getFileName = new DelegateCommand<string>(GetFileNameForAdd);
                }
                return getFileName;
            }
        }

        private void GetFileNameForAdd(string startDir)
        {
            selectedFileNameAndLoader = DepictionAppViewModel.GetFileToLoadAndImporter(startDir);
            FileNameWithElementsToAdd = selectedFileNameAndLoader.Key;
        }

        #endregion

        #region Adding the selected quickstart, taken from quickstartdatadialogVM

        private DelegateCommand<IList> acceptQuickstartData;
        public ICommand AcceptQuickstartDataCommand
        {
            get
            {
                if (acceptQuickstartData == null)
                {
                    acceptQuickstartData = new DelegateCommand<IList>(LoadQSData);
                }
                return acceptQuickstartData;
            }
        }

        private void LoadQSData(IList obj)
        {
            QuickstartDataDialogVM.LoadSelectedQuickstartSources(obj);
        }

        #endregion

        #endregion

        #region Constructor

        public AddContentDialogVM(DepictionEnhancedMapViewModel controllingVM)
        {
            PrototypeNamesViewSource = new CollectionViewSource();
            QuickStartItems = new CollectionViewSource();
            WebService = new DepictionWxsToElementService();
            NotifyPropertyChanged("WebService");
            SetElementPrototypeSorting();
            ConfineDataToRegion = true;
            mapControlVM = controllingVM;
            var importers = AddinRepository.Instance.DefaultImporters;

            foreach (var importer in importers)
            {
                if (importer.Key.Name.Contains("LiveReportImporter"))
                {
                    LiveReportAddin = importer.Value;
                    break;
                }
            }
            if (DepictionAccess.ElementLibrary != null)
            {
                DepictionAccess.ElementLibrary.PrototypesUpdated += ElementLibrary_PrototypesUpdated;
                DepictionAccess.ElementLibrary.NotifyPrototypeLibraryChange();
            }
        }

        #endregion
        #region overrides
        protected override void ToggleDialogVisibility(bool? obj)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.DepictionRW:
                    AddMode = AddContentMode.QuickStart;
                    break;
                default:
                    AddMode = AddContentMode.Mouse;
                    break;
            }
            UpdateElementDefinitionAddableState();
            if (obj == false && WebService != null)
            {
                WebService.CancelCababiliesSearch();
            }
            base.ToggleDialogVisibility(obj);
        }
        #endregion
        #region Desctuction
        protected override void OnDispose()
        {
            IsDialogVisible = false;
            ClearPrototypes();
            DepictionAccess.ElementLibrary.PrototypesUpdated -= ElementLibrary_PrototypesUpdated;
            if (PrototypeNamesViewSource != null) PrototypeNamesViewSource.Source = null;
            PrototypeNamesViewSource = null;
            //QuickStartItems = null;
            WebService = null;
            base.OnDispose();
        }
        #endregion

        #region Helper methods

        void ElementLibrary_PrototypesUpdated()
        {
            UpdatePrototypes();
            UpdateElementDefinitionAddableState();
        }
        protected void UpdateElementDefinitionAddableState()
        {
            foreach (var prototypeViewModel in prototypeList)
            {
                if (_addMode.Equals(AddContentMode.Mouse))
                {
                    if (!prototypeViewModel.IsPlaceableByMouse)
                    {
                        if (prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = false;
                        }
                    }
                    else
                    {
                        if (!prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = true;
                        }
                    }
                }
                else if (_addMode.Equals(AddContentMode.Address))
                {
                    if (!prototypeViewModel.IsPlaceableByMouse || prototypeViewModel.AddByZOI)
                    {
                        if (prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = false;
                        }
                    }
                    else
                    {
                        if (!prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = true;
                        }
                    }
                }
                else if (_addMode.Equals(AddContentMode.LiveReport))
                {
                    //This is a stop gap measure, we need to figure out a more solid way
                    //to figure out when an element is addable by a certain method
                    if (prototypeViewModel.ElementType.Equals("Depiction.Plugin.Image") ||
                        prototypeViewModel.ElementType.Equals("Depiction.Plugin.RoadNetwork") ||
                        prototypeViewModel.ElementType.Equals("Depiction.Plugin.Elevation"))
                    {
                        if (prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = false;
                        }
                    }
                    else
                    {
                        if (!prototypeViewModel.IsCurrentlyAddable)
                        {
                            prototypeViewModel.IsCurrentlyAddable = true;
                        }
                    }
                }
                else
                {
                    if (!prototypeViewModel.IsCurrentlyAddable)
                    {
                        prototypeViewModel.IsCurrentlyAddable = true;
                    }
                }
            }
        }
        protected void ClearPrototypes()
        {
            //Don't like this part
            foreach (var proto in prototypeList)
            {
                proto.Dispose();
            }
            prototypeList.Clear();
        }
        public void UpdatePrototypes()
        {
            if (DepictionAccess.ElementLibrary == null) return;
            ClearPrototypes();
            //Hack for not displaying certain prep elements.
            var hackNoShowList = new List<string>();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    hackNoShowList = new List<string>
                                     {
                                         "Depiction.Plugin.WaterBody",
                                         "Depiction.Plugin.Elevation",
                                         "Depiction.Plugin.RoadNetwork",
                                         "Depiction.Plugin.USNationalWeatherService24HourForecast",
                                         "Depiction.Plugin.AnimalControlVehicle",
                                         "Depiction.Plugin.HurricaneTrack",
                                         "Depiction.Plugin.PetTransportVehicle",
                                         "Depiction.Plugin.TornadoTrack",
                                         "Depiction.Plugin.CommunicationsVehicle",
                                         "Depiction.Plugin.LoadedLines",
                                         "Depiction.Plugin.LoadedPolygon"
                                     };
                    break;
                default: hackNoShowList = new List<string>
                                     {
                                         "Depiction.Plugin.USNationalWeatherService24HourForecast",
                                     };
                    break;
            }
            var defaultPrototypes = DepictionAccess.ElementLibrary.DefaultPrototypes;
            bool showInAddContent;
            foreach (var prototype in defaultPrototypes)
            {
                if (prototype != null)
                {

                    if (!prototype.GetPropertyValue("DisplayInAddContent", out showInAddContent))
                    {
                        showInAddContent = true;
                    }
                    if (hackNoShowList.Contains(prototype.ElementType))
                    {
                        showInAddContent = false;
                    }
//                    switch (DepictionAccess.ProductInformation.ProductType)
//                    {
//                        case ProductInformationBase.Prep:
//                            if (hackNoShowList.Contains(prototype.ElementType))
//                            {
//                                showInAddContent = false;
//                            }
//                            break;
//                    }
                    if (showInAddContent)
                    {
                        var dProto = prototype as ElementPrototype;
                        var visible = true;
                        if (dProto != null && dProto.Deprecated)
                        {
                            visible = false;
                        }
                        if (visible) prototypeList.Add(new ElementPrototypeViewModel(prototype));
                    }
                }
            }
            var userPrototypes = DepictionAccess.ElementLibrary.UserPrototypes;
            foreach (var prototype in userPrototypes)
            {
                if (prototype != null)
                {
                    if (!prototype.GetPropertyValue("DisplayInAddContent", out showInAddContent))
                    {
                        showInAddContent = true;
                    }
                    if (showInAddContent)
                    {
                        var dProto = prototype as ElementPrototype;
                        var visible = true;
                        if (dProto != null && dProto.Deprecated)
                        {
                            visible = false;
                        }
                        if (visible) prototypeList.Add(new ElementPrototypeViewModel(prototype));
                    }
                }
            }
            var loadedPrototypes = DepictionAccess.ElementLibrary.LoadedDepictionPrototypes;
            foreach (var prototype in loadedPrototypes)
            {
                if (prototype != null)
                {
                    if (!prototype.GetPropertyValue("DisplayInAddContent", out showInAddContent))
                    {
                        showInAddContent = true;
                    }
                    if (showInAddContent)
                    {
                        var dProto = prototype as ElementPrototype;
                        var visible = true;
                        if (dProto != null && dProto.Deprecated)
                        {
                            visible = false;
                        }
                        if (visible) prototypeList.Add(new ElementPrototypeViewModel(prototype));
                    }
                }
            }

            //omghackz (this completes it, getting auto-detect to the top
            if (prototypeList.Count > 0)
            {
                try
                {
                    AutoDetectPrototypeViewModel = prototypeList.First(t => t.ElementType.Equals(DepictionStringService.AutoDetectElementString));
                    AutoDetectPrototypeViewModel.SortPriority = DepictionSortPriority.High;
                }
                catch { }

            }
            SetPrototypeSourceThreadSafe(prototypeList);
        }

        protected void SetPrototypeSourceThreadSafe(List<ElementPrototypeViewModel> prototypes)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<ElementPrototypeViewModel>>(SetPrototypeSourceThreadSafe), prototypes);
                return;
            }
            PrototypeNamesViewSource.Source = null;
            PrototypeNamesViewSource.SortDescriptions.Clear();
            PrototypeNamesViewSource.SortDescriptions.Clear();
            PrototypeNamesViewSource.GroupDescriptions.Clear();
            SetElementPrototypeSorting();
            PrototypeNamesViewSource.Source = prototypeList;
            NotifyPropertyChanged("PrototypeNamesViewSource");

        }

        public void SetQuickStartListVMs(QuickstartDataDialogVM quickstartItems)
        {
            QuickStartItems = quickstartItems.QuickstartViewSource;
            QuickStartItems.View.Refresh();
            NotifyPropertyChanged("QuickStartItems");
        }

        public void SetElementPrototypeSorting()
        {
            //Element prototypes
            PrototypeNamesViewSource = new CollectionViewSource();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    var groupSort = new SortDescription();
                    groupSort.Direction = ListSortDirection.Ascending;
                    groupSort.PropertyName = "DefinitionGroup";
                    PrototypeNamesViewSource.SortDescriptions.Add(groupSort);
                    break;
            }
            //#if PREP
            //
            //            var groupSort = new SortDescription();
            //            groupSort.Direction = ListSortDirection.Ascending;
            //            groupSort.PropertyName = "DefinitionGroup";
            //            PrototypeNamesViewSource.SortDescriptions.Add(groupSort);
            //#endif

            var depictionSort = new SortDescription();
            depictionSort.Direction = ListSortDirection.Descending;
            depictionSort.PropertyName = "SortPriority";

            PrototypeNamesViewSource.SortDescriptions.Add(depictionSort);

            var sortDescription = new SortDescription();
            sortDescription.Direction = ListSortDirection.Ascending;
            sortDescription.PropertyName = "DisplayName";
            PrototypeNamesViewSource.SortDescriptions.Add(sortDescription);
            PropertyGroupDescription groupDescription = null;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    groupDescription = new PropertyGroupDescription("DefinitionGroup");
                    PrototypeNamesViewSource.GroupDescriptions.Add(groupDescription);
                    break;
                default:
                    groupDescription = new PropertyGroupDescription("DefinitionSource");
                    PrototypeNamesViewSource.GroupDescriptions.Add(groupDescription);
                    break;
            }
            //#if PREP
            //            var groupDescription = new PropertyGroupDescription("DefinitionGroup");
            //            PrototypeNamesViewSource.GroupDescriptions.Add(groupDescription);
            //#else
            //            var groupDescription = new PropertyGroupDescription("DefinitionSource");
            //            PrototypeNamesViewSource.GroupDescriptions.Add(groupDescription);
            //#endif

        }

        public bool LoadFileWithElementInformation(KeyValuePair<string, IDepictionImporterBase> fileNameAndLoader, string elementTypeString)
        {
            var fileName = fileNameAndLoader.Key;
            if (!File.Exists(fileName)) return false;

            var reader = fileNameAndLoader.Value;
            if (reader == null)
            {
                reader = FindAddinToReadFile(fileName, elementTypeString);
            }
            if (reader != null)
            {
                var app = Application.Current as DepictionApplication;
                if (app == null) return false;
                var depictionRegion = app.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
                if (!ConfineDataToRegion) depictionRegion = null;
                reader.ImportElements(fileName, elementTypeString, depictionRegion, null);

                return true;
            }
            return false;
        }

        private IDepictionImporterBase FindAddinToReadFile(string fileName, string elementTypeString)
        {
            var inExtenstion = Path.GetExtension(fileName).ToLowerInvariant();
            var importers = AddinRepository.Instance.DefaultImporters;
            var supportedImporter = new Dictionary<IDepictionExtensionIOMetadata, IDepictionImporterBase>();

            foreach (var importer in importers)
            {
                var metadata = importer.Key;
                var typeList = new List<string>(metadata.ElementTypesSupported);
                var unsupportedList = new List<string>(metadata.ElementTypesNotSupported);
                var extensionList = new List<string>(metadata.ExtensionsSupported);

                var supported = true;
                //TODO this can use a lot of clean up
                if (typeList.Count != 0)
                {
                    supported = typeList.Contains(elementTypeString);
                }
                supported = !unsupportedList.Contains(elementTypeString);

                if (!supported) continue;
                foreach (var extension in extensionList)
                {
                    if (inExtenstion.Equals(extension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        supportedImporter.Add(importer.Key, importer.Value);
                        //return importer.Value;
                    }
                }
            }
            //Hack to make sure image importer is picked over elevation importer
            var count = supportedImporter.Count();
            if (count > 0)
            {
                if (count == 1)
                {
                    return supportedImporter.ElementAt(0).Value;
                }
                foreach (var importer in supportedImporter)
                {
                    if (importer.Key.ElementTypesSupported.Contains("Depiction.Plugin.Image"))
                    {
                        return importer.Value;
                    }
                }
                //Couldn't find an image one so just return the first one
                return supportedImporter.ElementAt(0).Value;
            }
            var nonDefaultImporters = AddinRepository.Instance.NonDefaultImporters;
            foreach (var importer in nonDefaultImporters)
            {
                var metadata = importer.Key;
                var typeList = new List<string>(metadata.ElementTypesSupported);
                var unsupportedList = new List<string>(metadata.ElementTypesNotSupported);
                var extensionList = new List<string>(metadata.ExtensionsSupported);

                var supported = true;
                //TODO this can use a lot of clean up
                if (typeList.Count != 0)
                {
                    supported = typeList.Contains(elementTypeString);
                }
                supported = !unsupportedList.Contains(elementTypeString);

                if (!supported) continue;
                foreach (var extension in extensionList)
                {
                    if (inExtenstion.Equals(extension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return importer.Value;
                    }
                }
            }
            return null;

        }

        #endregion

    }

    public enum MouseAddMode
    {
        Single, Multiple
    }

    public enum AddContentMode
    {
        Unknown,
        Mouse,
        Address,
        File,
        LiveReport,
        Web,
        QuickStart
    }
}