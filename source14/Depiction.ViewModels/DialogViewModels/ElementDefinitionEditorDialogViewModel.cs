using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.DialogViewModels
{
    public class ElementDefinitionEditorDialogViewModel : DialogViewModelBase
    {
        public const string NewDefinitinoName = "NewDefinition";

        private string _newTypeDefinitionDescription = "New Definition";
        private string newTypeDefinitionName = "NewDefinition";

        private DelegateCommand loadEditPrototypeFromFileCommand;

        private DelegateCommand applyCurrentChangesCommand;
        private DelegateCommand addPropertyPreviewCommand;
        private DelegateCommand<List<string>> loadPrototypeElementKeyCommand;//Hackish

        private DelegateCommand<IElementPrototype> loadPrototypeFromLibraryCommand;
        private DelegateCommand<IElementPrototype> savePrototypeToUserLibrary;
        private DelegateCommand<IElementPrototype> savePrototypeToUserLibraryAndExit;
        private ElementPrototype _definitionToCreate = null;
        private string _newNewPropDisplayName = "";
        private CollectionViewSource PropertyViewModelCollectionSource { get; set; }
        #region Properties

        public ObservableCollection<MenuElementPropertyViewModel> PropertyViewModels { get; set; }
        public object PropertyViewModelCollection { get; private set; }

        public IEnumerable<KeyValuePair<string, object>> PropertyTypes
        {
            get
            {
                var props = ViewModelHelperMethods.GetSimpleDepictionTypes();
                return props;
            }
        }
        public ElementPrototype DefinitionToCreate
        {
            get { return _definitionToCreate; }
            set { _definitionToCreate = value; NotifyPropertyChanged("DefinitionToCreate"); }
        }
        public string NewTypeDefinitionName
        {
            get { return _definitionToCreate.ElementType; }
            set { _definitionToCreate.ElementType = value; NotifyPropertyChanged("NewTypeDefinitionName"); }
        }
        public string NewTypeDefinitionDescription
        {
            get { return _definitionToCreate.TypeDisplayName; }
            set
            {
                _definitionToCreate.TypeDisplayName = value;
                NotifyPropertyChanged("NewTypeDefinitionDescription");
            }
        }
        public string NewPropDisplayName
        {
            get { return _newNewPropDisplayName; }
            set { _newNewPropDisplayName = value; NotifyPropertyChanged("NewPropDisplayName"); }
        }
        public object SelectedType { get; set; }

        #region Command Properties
        public ICommand LoadEditPrototypeFromFileCommand
        {
            get
            {
                if (loadEditPrototypeFromFileCommand == null)
                {
                    loadEditPrototypeFromFileCommand = new DelegateCommand(FindPrototypeFileToLoad) { Text = "Load dml file as definition" };
                }
                return loadEditPrototypeFromFileCommand;
            }
        }

        //USed by map right click context menu
        public ICommand LoadPrototypeElementKeyCommand
        {
            get
            {
                if (loadPrototypeElementKeyCommand == null)
                {
                    loadPrototypeElementKeyCommand = new DelegateCommand<List<string>>(LoadPrototypeFromElementKey) { Text = "Edit definition" };
                }
                return loadPrototypeElementKeyCommand;
            }
        }

        public ICommand LoadPrototypeFromLibraryCommand
        {
            get
            {
                if (loadPrototypeFromLibraryCommand == null)
                {
                    loadPrototypeFromLibraryCommand = new DelegateCommand<IElementPrototype>(LoadPrototypeFromLibrary) { Text = "Load definition from depiction library" };
                }
                return loadPrototypeFromLibraryCommand;
            }
        }
        public ICommand SaveDefinitionToFileCommand
        {
            get
            {
                return new DelegateCommand<IElementPrototype>(SaveDefinitinoToFileSimple) { Text = "Save definition to file" };
            }
        }
        public ICommand ResetElementDefinitionCommand
        {
            get
            {
                return new DelegateCommand(ResetElementDefinition) { Text = "Create empty element definition" };
            }
        }
        public ICommand SavePrototypeToUserLibraryCommand
        {
            get
            {
                if (savePrototypeToUserLibrary == null)
                {
                    savePrototypeToUserLibrary = new DelegateCommand<IElementPrototype>(SavePrototypeToUserLibrary, CanSaveDefinition) { Text = "Save definition to Depiction and create a new definition" };
                }
                return savePrototypeToUserLibrary;
            }
        }
        public ICommand SavePrototypeToUserLibraryAndExitCommand
        {
            get
            {
                if (savePrototypeToUserLibraryAndExit == null)
                {
                    savePrototypeToUserLibraryAndExit = new DelegateCommand<IElementPrototype>(SavePrototypeToUserLibraryAndExit, CanSaveDefinition) { Text = string.Format("Save definition to Depiction and exit editor.") };
                }
                return savePrototypeToUserLibraryAndExit;
            }
        }
        public ICommand AddPropertyPreviewCommand
        {
            get
            {
                if (addPropertyPreviewCommand == null) { addPropertyPreviewCommand = new DelegateCommand(AddPropertyPreview, PropertyIsValid) { Text = "Add property" }; }
                return addPropertyPreviewCommand;
            }
        }
        public ICommand ApplyCurrentChangesCommand
        {
            get
            {
                if (applyCurrentChangesCommand == null)
                {
                    applyCurrentChangesCommand = new DelegateCommand(ApplyCurrentChanges) { Text = "Apply modifications current definition" };
                }
                return applyCurrentChangesCommand;
            }
        }
        #endregion
        #endregion

        #region Constructor

        public ElementDefinitionEditorDialogViewModel()
        {
            DialogDisplayName = "Depiction prototype creator";
            ResetElementDefinition();
            //Hackish
            Settings.Default.PropertyChanged += Default_PropertyChanged;
        }

        #endregion
        #region destruction

        protected override void OnDispose()
        {
            Settings.Default.PropertyChanged -= Default_PropertyChanged;
            base.OnDispose();
        }
        #endregion


        #region static helpers
        //Sets user definition origin to user and copies it to the user defintion directory, also copies the icon.
        static public bool SaveDefinitionToFile(IElementPrototype elementPrototype, bool useDefaultLocation)
        {
            var userElementPath = "";
            if (DepictionAccess.PathService != null)
            {
                userElementPath = DepictionAccess.PathService.UserElementsDirectoryPath;
            }
            if (useDefaultLocation && !Directory.Exists(userElementPath))
            {
                var message = string.Format("Could not auto-save {0}.", elementPrototype.TypeDisplayName);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return false;
            }
            var prototypeFileName = Path.Combine(userElementPath, elementPrototype.ElementType + ".dml");
            if (!useDefaultLocation)
            {
                prototypeFileName = DepictionAppViewModel.GetFileToSaveName(userElementPath, FileFilter.DMLTypeFilter);
            }
            if (string.IsNullOrEmpty(prototypeFileName)) return false;
            var extension = ".dml";
            if (!Path.GetExtension(prototypeFileName).Equals(extension, StringComparison.InvariantCultureIgnoreCase))
            {
                prototypeFileName = prototypeFileName.Trim() + extension;
            }

            elementPrototype.PrototypeOrigin = ElementPrototypeLibrary.UserPrototypeDescription;
            elementPrototype.DMLFileName = prototypeFileName;
            //There is a little ambiguity as to how to deal with icons, currently the icon dialog copies anything that is a file
            //to the userelementiconpath

            var clone = elementPrototype.DeepClone();
            DepictionIconPath iconPath;
            if (clone.GetPropertyValue("IconPath", out iconPath))
            {
                // if (iconPath.Source.Equals(IconPathSource.File))
                // {
                var imageName = Path.GetFileName(iconPath.Path);
                if (DepictionAccess.PathService != null)
                {
                    object image = null;

                    if (DepictionAccess.CurrentDepiction.ImageResources.Contains(imageName))
                    {
                        image = DepictionAccess.CurrentDepiction.ImageResources[imageName];
                    }
                    else if (Application.Current.Resources.Contains(imageName))
                    {
                        image = Application.Current.Resources[imageName];
                    }
                    //If there is no extension things could get messy
                    if (!BitmapSaveLoadService.HasValidImageExtension(imageName))
                    {
                        imageName = string.Concat(imageName, ".png");
                        clone.SetPropertyValue("IconPath", new DepictionIconPath(IconPathSource.File, imageName));
                    }
                    var fullPath = Path.Combine(DepictionAccess.PathService.UserElementIconDirectoryPath, imageName);
                    
                    BitmapSaveLoadService.SaveBitmap(image as BitmapSource, fullPath);
                }
                //}
            }
            UnifiedDepictionElementWriter.WriteIDepictionElementBaseToFile(clone, prototypeFileName, true);
           
            return true;
        }
        #endregion
        #region Private helper event connections

        private void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (e.PropertyName.Equals("measurementScale", StringComparison.InvariantCultureIgnoreCase) ||
                 e.PropertyName.Equals("measurementSystem", StringComparison.InvariantCultureIgnoreCase))
            {
                NotifyPropertyChanged("PropertyTypes");
                if (_definitionToCreate != null)
                {
                    foreach (var prop in _definitionToCreate.OrderedCustomProperties)
                    {
                        prop.RefreshValue();
                    }
                }
            }
            if (propName.Equals("LatitudeLongitudeFormat", StringComparison.InvariantCultureIgnoreCase))
            {
                if (_definitionToCreate != null)
                {
                    foreach (var prop in _definitionToCreate.OrderedCustomProperties)
                    {
                        prop.RefreshValue();
                    }
                }
            }
        }

        #endregion

        #region Private helpers for commands

        private bool CanSaveDefinition(IElementPrototype arg)
        {
            if (arg == null)
            {
                savePrototypeToUserLibrary.Text = "Definition cannot be empty";
                savePrototypeToUserLibraryAndExit.Text = "Definition can not be empty";
                return false;
            }
            var library = DepictionAccess.ElementLibrary;
            if (library != null)
            {
                if (library.GetPrototypeByFullTypeName(arg.ElementType) != null)
                {
                    savePrototypeToUserLibrary.Text = string.Format("{0} already exists.", arg.ElementType);
                    savePrototypeToUserLibraryAndExit.Text = string.Format("{0} already exists.", arg.ElementType);
                    return false;
                }
            }
            savePrototypeToUserLibrary.Text = string.Format("Save {0} to Depiction and create a new definition", arg.ElementType);
            savePrototypeToUserLibraryAndExit.Text = string.Format("Save {0} to Depiction and exit editor.", arg.ElementType);
            return true;
        }

        private void LoadPrototypeFromElementKey(List<string> elementIds)
        {
            var possibleModels = DepictionAccess.CurrentDepiction.GetElementsWithIds(elementIds);
            if (possibleModels.Count != 1) return;
            if (LoadPrototypeFromPreExisting(possibleModels[0] as DepictionElementBase))
            {
                IsDialogVisible = true;
            }
        }

        private void FindPrototypeFileToLoad()
        {
            var userElementPath = "";
            if (DepictionAccess.PathService != null)
            {
                userElementPath = DepictionAccess.PathService.UserElementsDirectoryPath;
            }
            var prototypeFileName = DepictionAppViewModel.GetFileNameToLoad(userElementPath,FileFilter.DMLTypeFilter);
            LoadEditPrototypeFromFile(prototypeFileName);
        }
        private void LoadPrototypeFromLibrary(IElementPrototype prototypeToCopy)
        {

            SetPrototypeToUse(prototypeToCopy as ElementPrototype);
            IsDialogVisible = true;
        }
        private void SaveDefinitinoToFileSimple(IElementPrototype elementPrototype)
        {
            PrivateSaveDefinitionToFile(elementPrototype, false);
        }
        
        private bool PrivateSaveDefinitionToFile(IElementPrototype elementPrototype, bool useDefaultLocation)
        {
            ApplyCurrentChanges();
            return SaveDefinitionToFile(elementPrototype, useDefaultLocation);
        }
        private void SavePrototypeToUserLibrary(IElementPrototype elementPrototype)
        {
            elementPrototype.PrototypeOrigin = ElementPrototypeLibrary.UserPrototypeDescription;
            PrivateSaveDefinitionToFile(elementPrototype, true);

            var library = DepictionAccess.ElementLibrary;
            if (library != null)
            {
                var addSuccess = library.AddElementPrototypeToLibrary(elementPrototype, true);
                if (!addSuccess)
                {
                    DepictionAccess.NotificationService.DisplayMessageString(
                        string.Format("Could not add definition : {0}", elementPrototype.ElementType));
                }
                else
                {
                    library.NotifyPrototypeLibraryChange();
                }
            }
            ResetElementDefinition();
        }
        private void ResetElementDefinition()
        {
            var newDef = ElementFactory.CreateRawPrototypeOfType(NewDefinitinoName, true) as ElementPrototype;
            if (newDef != null)
            {
                newDef.ElementType = newTypeDefinitionName;
                newDef.TypeDisplayName = _newTypeDefinitionDescription;
            }
            SetPrototypeToUse(newDef);
        }
        private void SavePrototypeToUserLibraryAndExit(IElementPrototype elementPrototype)
        {
            SavePrototypeToUserLibrary(elementPrototype);
            IsDialogVisible = false;
        }
        private void AddPropertyPreview()
        {
            var internalName = ViewModelHelperMethods.GetInternalNameFromString(NewPropDisplayName);
            var propData = new DepictionElementProperty(internalName, NewPropDisplayName, SelectedType);
            var propVM = new MenuElementPropertyViewModel(propData, _definitionToCreate);
            propVM.IsPreview = true;
            PropertyViewModels.Add(propVM);
        }
        private bool PropertyIsValid()
        {
            if (string.IsNullOrEmpty(NewPropDisplayName)) return false;
            var internalName = ViewModelHelperMethods.GetInternalNameFromString(NewPropDisplayName);
            if (PropertyViewModels == null) return false;
            var list = PropertyViewModels.Where(t => t.InternalName.Equals(internalName));
            if (list.Count() != 0) return false;
            if (SelectedType == null) return false;

            return true;
        }

        #endregion

        #region Public methods

        public void ApplyCurrentChanges()
        {
            if (PropertyViewModels != null)
            {
                foreach (var propertyViewModel in PropertyViewModels)
                {

                    if (propertyViewModel.Delete)
                    {
                        var removed = _definitionToCreate.RemovePropertyIfNameAndTypeMatch(propertyViewModel.PropertyModel);
                    }
                    else
                    {
                        if (propertyViewModel.ValueChanged || propertyViewModel.IsPreview)
                        {
                            propertyViewModel.ApplyVMChangesToModel();
                        }
                        if(propertyViewModel.HoverTextChanged)
                        {
                            propertyViewModel.PropertyModel.IsHoverText = propertyViewModel.IsHoverText;
                        }
                    }
                }
            }
            SetPrototypeToUse(_definitionToCreate);
        }


        public bool LoadEditPrototypeFromFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return false;
            if (!File.Exists(fileName)) return false;
            var prototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);
            if (prototype == null) return false;
            SetPrototypeToUse(prototype as ElementPrototype);
            return true;
        }

        public bool LoadPrototypeFromPreExisting(DepictionElementBase elementBase)
        {
            if (elementBase == null) return false;
            var prototype = ElementFactory.CreateDesiredTypeFromElementBase<ElementPrototype>(elementBase);
            if (prototype == null) return false;
            SetPrototypeToUse(prototype);
            return true;
        }

        #endregion
        protected void SetPrototypeToUse(ElementPrototype prototype)
        {
            DefinitionToCreate = null;
            //Clear old things
            if (PropertyViewModels != null)
            {
                foreach (var propVM in PropertyViewModels)
                {
                    propVM.Dispose();
                }
                PropertyViewModels.Clear();
            }

            if (prototype != null)
            {
                if (PropertyViewModels == null)
                    PropertyViewModels = new ObservableCollection<MenuElementPropertyViewModel>();
                foreach (var prop in prototype.OrderedCustomProperties)
                {
                    PropertyViewModels.Add(new MenuElementPropertyViewModel(prop, prototype));
                }

                PropertyViewModelCollectionSource = new CollectionViewSource();
                PropertyViewModelCollectionSource.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
                PropertyViewModelCollectionSource.GroupDescriptions.Add(new PropertyGroupDescription("PropertyTypeString"));
                PropertyViewModelCollectionSource.Source = PropertyViewModels;
                PropertyViewModelCollection = PropertyViewModelCollectionSource.View.Groups;
            }
            else
            {
                PropertyViewModels = null;
                PropertyViewModelCollection = null;
            }
            NotifyPropertyChanged("PropertyViewModels");
            NotifyPropertyChanged("PropertyViewModelCollection");

            DefinitionToCreate = prototype;
            NotifyPropertyChanged("NewTypeDefinitionDescription");
            NotifyPropertyChanged("NewTypeDefinitionName");
        }
    }
}