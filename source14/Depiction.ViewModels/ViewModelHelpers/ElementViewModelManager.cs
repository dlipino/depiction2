using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModelHelpers
{
    public class ElementViewModelManager
    {
        private const int MAX_ICON_ELEMENTS = 5000;
        private const int pixelDiameter = 20;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public ObservableCollection<MapElementViewModel> ZOIElements { get; private set; }
        public ObservableCollection<MapElementViewModel> PTextElements { get; private set; }
        public ObservableCollection<MapElementViewModel> ImageElements { get; private set; }
        public ObservableCollection<MapElementViewModel> IconElements { get; private set; }
        public ObservableCollection<MapElementViewModel> IconElementsFiltered { get; private set; }


        Dictionary<string, MapElementViewModel> ElementIdsAndViewModels { get; set; }
        private QuadTree spatialIndex;
        private IDepictionStory currentDepiction;

        
        public ElementViewModelManager()
        {
            ZOIElements = new ObservableCollection<MapElementViewModel>();
            PTextElements = new ObservableCollection<MapElementViewModel>();
            ImageElements = new ObservableCollection<MapElementViewModel>();
            IconElements = new ObservableCollection<MapElementViewModel>();
            IconElementsFiltered = new ObservableCollection<MapElementViewModel>();
            ElementIdsAndViewModels = new Dictionary<string, MapElementViewModel>();
            //DepictionAccess.CurrentDepiction.DepictionViewportChange += CurrentDepictionOnDepictionViewportChange;
        }

        private void CurrentDepictionOnDepictionViewportChange(IMapCoordinateBounds mapCoordinateBounds, IMapCoordinateBounds coordinateBounds)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var elements = spatialIndex.Search(coordinateBounds);

            stopwatch.Stop();
            Debug.WriteLine(string.Format("search took {0} milliseconds", stopwatch.ElapsedMilliseconds));
            stopwatch.Start();

            if (elements.Count > 1000)
            {
                IconElementsFiltered.Clear();
                var app = Application.Current as DepictionApplication;
                var columns = app.MainWindow.ActualWidth / 200;
                var columnWidth = coordinateBounds.Width / columns;
                var rows = app.MainWindow.ActualHeight / 200;
                var rowHeight = coordinateBounds.Height / rows;
                for (int i = 0; i < columns; i++)
                {
                    for (int j = 0; j < rows; j++)
                    {
                        var realBounds =
                            new MapCoordinateBounds(coordinateBounds.BottomLeft.Add(new LatitudeLongitude((j + 1)*rowHeight,
                                                                                          i*columnWidth)),
                                                    coordinateBounds.BottomLeft.Add(new LatitudeLongitude((j)*rowHeight,
                                                                                          (i + 1)*columnWidth)));

                            
                        //var bounds = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(
                        //        new Rect(new Point {X = i*pixelDiameter, Y = j*pixelDiameter}, new Point {X = (i + 1)*pixelDiameter, Y = (j + 1)*pixelDiameter}));
                        var elementsInTile = spatialIndex.Search(realBounds);
                        if (elementsInTile.Count > 0)
                        {
                            var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.People");
                            prototype.SetInitialPositionAndZOI(realBounds.Center, null);
                            //prototype.UsePermaText = true;
                            prototype.AddPropertyIfItDoesNotExist(new DepictionElementProperty("elementsHere", elementsInTile.Count) { IsHoverText = true }, false, false);

                            var element = ElementFactory.CreateElementFromPrototype(prototype);

                            //var element = new DepictionElementParent();

                            IconElementsFiltered.Add(new MapElementViewModel(element));
                        }
                    }
                }

                stopwatch.Stop();
                Debug.WriteLine(string.Format("LOD add took {0} milliseconds", stopwatch.ElapsedMilliseconds));
                Debug.WriteLine(string.Format("total {0} columns {1} rows {2}", IconElementsFiltered.Count, columns, rows));
            }
            else
            {
                var elementsToRemove = new List<MapElementViewModel>();
                foreach (var element in IconElementsFiltered)
                {
                    if (element.ElementModel == null || !elements.Contains(Guid.Parse(element.ElementKey)))
                        elementsToRemove.Add(element);
                }
                foreach (var element in elementsToRemove)
                {
                    IconElementsFiltered.Remove(element);
                }

                int added = 0;
                foreach (var elementGuid in elements)
                {
                    MapElementViewModel element;
                    //element = ElementIdsAndViewModels[elementGuid.ToString()];
                    var success = ElementIdsAndViewModels.TryGetValue(elementGuid.ToString(), out element);
                    if (success)
                    {

                        if (!IconElementsFiltered.Contains(element))
                        {
                            added++;
                            IconElementsFiltered.Add(element);
                        }
                    }
                }
                stopwatch.Stop();
                Debug.WriteLine(string.Format("add took {0} milliseconds", stopwatch.ElapsedMilliseconds));
                Debug.WriteLine(string.Format("total {0} added {1} removed {2}", IconElementsFiltered.Count, added, elementsToRemove.Count));
            }
        }

        #region Public add/remove methods
        public void ClearElementVMSet()
        {
            if (CollectionChanged != null)
            {
                CollectionChanged.Invoke(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
            ZOIElements.Clear();
            PTextElements.Clear();
            ImageElements.Clear();
            IconElements.Clear();
            var elements = ElementIdsAndViewModels.Values.ToArray();
            for (int i = 0; i < elements.Length; i++)
            {
                elements[i].PropertyChanged -= elementVM_PropertyChanged;
                elements[i].Dispose();
                elements[i] = null;
            }

            ElementIdsAndViewModels.Clear();

            ElementIdsAndViewModels = null;
        }
        public void AddElementVMSet(IEnumerable<MapElementViewModel> elementVMs)
        {

            if (DepictionAccess.CurrentDepiction != null && currentDepiction != DepictionAccess.CurrentDepiction)//hackhack
            {
                DepictionAccess.CurrentDepiction.DepictionViewportChange += CurrentDepictionOnDepictionViewportChange;
                currentDepiction = DepictionAccess.CurrentDepiction;
            }

            var vmsAdded = new List<MapElementViewModel>();
            Mouse.OverrideCursor = Cursors.Wait;

            foreach (var elementVM in elementVMs)
            {
                if (ElementIdsAndViewModels.ContainsKey(elementVM.ElementKey)) continue;
                vmsAdded.Add(elementVM);
                ElementIdsAndViewModels.Add(elementVM.ElementKey, elementVM);

                if (elementVM.UsePermaText)
                {
                    PTextElements.Add(elementVM);
                }
                if (elementVM.IconVisibility == Visibility.Visible || elementVM.ElementModel.Waypoints.Length >0)
                {
                    IconElements.Add(elementVM);
                    
                }
                if (elementVM.FullImageSource != null)
                {
                    ImageElements.Add(elementVM);
                }
                if (!elementVM.ZOI.ZOIGeometryType.Equals(DepictionGeometryType.Point) ||
                    !elementVM.ZOI.BaseShapeType.Equals(ZOIShapeType.Point))
                {
                    ZOIElements.Add(elementVM);
                }
                //Not sure if this is a hack, but we do need to make sure the proper things
                //get added to the PTextElement list if the permatext is turned on
                elementVM.PropertyChanged += elementVM_PropertyChanged;
                
            }


            spatialIndex = CreateIndex(IconElements);

            if (CollectionChanged != null)
            {
                CollectionChanged.Invoke(this,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, vmsAdded));
            }
        }

        private QuadTree CreateIndex(ObservableCollection<MapElementViewModel> iconElements)
        {
            QuadTree qt = SpatialIndexing.CreateSpatialIndexRecursive(iconElements);
            return qt;
        }

        public void AddElementVM(MapElementViewModel elementVM)
        {
            AddElementVMSet(new List<MapElementViewModel> { elementVM });
        }
        public void RemoveElementWithKey(string key)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            if (ElementIdsAndViewModels.ContainsKey(key))
            {
                var elemVM = ElementIdsAndViewModels[key];

                if (elemVM != null)
                {
                    if (CollectionChanged != null)
                    {
                        CollectionChanged.Invoke(this,
                            new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, elemVM));
                    }
                    ZOIElements.Remove(elemVM);
                    PTextElements.Remove(elemVM);
                    ImageElements.Remove(elemVM);
                    IconElements.Remove(elemVM);
                    elemVM.PropertyChanged -= elementVM_PropertyChanged;
                    if (!elemVM.IsElementPreview)
                    {
                        ElementIdsAndViewModels[key].Dispose();
                        ElementIdsAndViewModels[key] = null;
                    }
                    ElementIdsAndViewModels.Remove(key);
                }

                //spatialIndex = CreateIndex(IconElements);
            }
            
        }
        public void RemoveElementVM(MapElementViewModel elementVM)
        {
            RemoveElementWithKey(elementVM.ElementKey);

        }
        #endregion
        #region Private helpers

        void elementVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals("UsePermaText"))
            {
                var elemVM = sender as MapElementViewModel;
                if (elemVM == null) return;
                if(!ElementIdsAndViewModels.ContainsKey(elemVM.ElementKey))
                {
                    Debug.WriteLine("An element that has not been added is trying to get added");
                }
                if(elemVM.UsePermaText)
                {
                    if(!PTextElements.Contains(elemVM))
                    {
                        PTextElements.Add(elemVM);
                    }
                }else
                {
                    PTextElements.Remove(elemVM);
                }
            }else if(e.PropertyName.Equals("ZoneOfInfluence",StringComparison.InvariantCultureIgnoreCase))
            {
                var elemVM = sender as MapElementViewModel;
                if (elemVM == null) return;
                var zoiVM = elemVM.ZOI.ZOIGeometryType;
                if(zoiVM.Equals(DepictionGeometryType.Point))
                {
                    ZOIElements.Remove(elemVM);
                    
                }else
                {
                    if (!ZOIElements.Contains(elemVM))
                    {
                        ZOIElements.Add(elemVM);
                    }
                }
            }
        }
        #endregion

        public static class SpatialIndexing
        {
            /// <summary>
            /// Generates a spatial index for a specified shape file.
            /// </summary>
            /// <param name="filename">The filename</param>
            public static QuadTree CreateSpatialIndexRecursive(ObservableCollection<MapElementViewModel> elements)
            {
                var sw = new Stopwatch();
                sw.Start();

                var objList = new List<QuadTree.BoxObjects>();
                //Convert all the geometries to boundingboxes 
                uint i = 0;
                foreach (var box in GetAllFeatureBoundingBoxes(elements))
                {

                    var g = new QuadTree.BoxObjects { Box = box, ID = Guid.Parse(elements[(int)i].ElementKey) };
                    objList.Add(g);
                    i++;
                }

                Heuristic heur;
                heur.maxdepth = (int)Math.Ceiling(Math.Log(elements.Count, 2));
                heur.minerror = 10;
                heur.tartricnt = 5;
                heur.mintricnt = 2;
                var root = new QuadTree(objList, 0, heur);

                sw.Stop();
                Debug.WriteLine("Linear creation of QuadTree took {0}ms", sw.ElapsedMilliseconds);

                //if (_fileBasedIndexWanted && !String.IsNullOrEmpty(filename))
                //    root.SaveIndex(filename + ".sidx");

                return root;
            }

            private static IEnumerable<MapCoordinateBounds> GetAllFeatureBoundingBoxes(ObservableCollection<MapElementViewModel> elements)
            {
                foreach (var element in elements)
                {
                    if (element.ElementModel.ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point))
                    {
                        yield return new MapCoordinateBounds(element.ElementModel.Position, element.ElementModel.Position);
                    }

                }
                //if (_shapeType == ShapeType.Point)
                //{
                //    for (int a = 0; a < _featureCount; ++a)
                //    {
                //        //if (recDel((uint)a)) continue;

                //        _fsShapeFile.Seek(_offsetOfRecord[a] + 8, 0); //skip record number and content length
                //        if ((ShapeType)_brShapeFile.ReadInt32() != ShapeType.Null)
                //        {
                //            yield return new Envelope(new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()));
                //        }
                //    }
                //}
                //else
                //{
                //    for (int a = 0; a < _featureCount; ++a)
                //    {
                //        //if (recDel((uint)a)) continue;
                //        _fsShapeFile.Seek(_offsetOfRecord[a] + 8, 0); //skip record number and content length
                //        if ((ShapeType)_brShapeFile.ReadInt32() != ShapeType.Null)
                //            yield return new Envelope(new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()),
                //                                      new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()));
                //        //boxes.Add(new BoundingBox(brShapeFile.ReadDouble(), brShapeFile.ReadDouble(),
                //        //                          brShapeFile.ReadDouble(), brShapeFile.ReadDouble()));
                //    }
                //}            
            }
        }
    }
}