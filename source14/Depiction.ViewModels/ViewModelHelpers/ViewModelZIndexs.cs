﻿namespace Depiction.ViewModels.ViewModelHelpers
{
    static public class ViewModelZIndexs
    {
        //Backdrop Z indexs
        public const int BackdropLabelDisplayerZ = 40;
        public const int BackdropIconDisplayerZ = 35;
        public const int BackdropNodeDisplayerZ = 30;
        public const int BackdropZOIDisplayerZ = 25;
        public const int BackdropImageDisplayerZ = 20;

        //ZIndexs for world
        public const int BottomWorldBackGroundRectangleZ = -6;
        public const int TilingControlZ = -5;
        public const int WorldOutlineZ = -4;
        public const int RegionBoxZIndex = 26;
        public const int AnnotationsControlZ = 320;
        public const int TopWorldBackGroundRectangleZ = 300;

        //Normal Revealer ZIndexes
        public const int RevealerLabelDisplayerZ = 41;
        public const int RevealerIconDisplayerZ = 36;
        public const int RevealerNodeDisplayerZ = 31;
        public const int RevealerZOIDisplayerZ = 26;
        public const int RevealerImageDisplayerZ = 21;

        //top level Revealer ZIndexes
        public const int TopRevealerLabelDisplayerZ = 42;
        public const int TopRevealerIconDisplayerZ = 37;
        public const int TopRevealerNodeDisplayerZ = 32;
        public const int TopRevealerZOIDisplayerZ = 27;
        public const int TopRevealerImageDisplayerZ = 22;

        //Revealer dragger and border z indexes.
        
        public const int RevealerMainThumbZ = 5;
        public const int TopRevealerMainThumbZ = 6;
        public const int RevealerBorderThumbZ = 300;
        public const int TopRevealerBorderThumbZ = 301;

        //Element z indexes
        public const int CoverageElementZIndex = 2;
        public const int HiResCoverageElementZIndex = 3;
        public const int UserImageryElementZIndex = 4;
        public const int ElementCoverageZIndex = 6;
        public const int ElementUsableCoverageZIndex = 7;
        public const int WorldCanvasPolygonElementZIndex = 8;//Elements with polygons are below other normal elements
        public const int WorldCanvasEditablePolygonElementZIndex = 10;
        //the difference of two is important to make sure lines always stay above polygon zois when ToFront is called
        public const int WorldCanvasElementZIndex = 12;
       
    }
}