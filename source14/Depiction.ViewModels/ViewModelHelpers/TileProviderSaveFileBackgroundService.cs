﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.ExtensionMethods;

namespace Depiction.ViewModels.ViewModelHelpers
{
    public class TileProviderSaveFileBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        private readonly TiledElementImporter provider;
        private readonly IMapCoordinateBounds region;
        private List<string> tagList = new List<string>();
        private bool addToMainMap;

        public TileProviderSaveFileBackgroundService(TiledElementImporter provider, IMapCoordinateBounds region)
        {
            this.provider = provider;
            this.region = region;
        }

        public override void StopBackgroundService()
        {
            provider.CancelRequest();
            base.StopBackgroundService();
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            if (args.ContainsKey("Tag"))
            {
                var tag = args["Tag"].ToString();
                if (tag.ToLower().Contains("highres"))
                {
                    addToMainMap = true;
                    tag += provider.DisplayName;//This part is a temp hack because we can't get then name before hand (yet)
                }
                tagList.Add(tag);
            }
            if (args.ContainsKey("Tags"))
            {
                var allTags = args["Tags"].ToString();
                var split = allTags.Split('|');
            }
            provider.TileServiceInformation += provider_TileServiceInformation;
            UpdateStatusReport(string.Format("Starting {0} importer", provider.DisplayName));
            return true;
        }

        void provider_TileServiceInformation(string information)
        {
            UpdateStatusReport(string.Format("{0} for {1}", information, provider.DisplayName));
        }

        protected override object ServiceAction(object args)
        {
            UpdateStatusReport(string.Format("Retrieving {0}", provider.DisplayName));

            var fileName = provider.CreateImageFromTilesInBounds(region, 1000 / provider.PixelWidth, 18);
            if (string.IsNullOrEmpty(fileName)) return null;
            if (!File.Exists(fileName)) return null;

            try
            {
                UpdateStatusReport(string.Format("Loading image into depiction library"));
                string partialName;
                var imageWasAdded = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fileName, out partialName);
                if (imageWasAdded)
                {
                    return partialName;
                    //                    UpdateStatusReport(string.Format("Creating image element"));
                    //                    var elementType = "Depiction.Plugin.Image";
                    //                    var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(elementType);
                    //                    var elementModel = ElementFactory.CreateElementFromPrototype(prototype);
                    //                    
                    //                    if (string.IsNullOrEmpty(partialName))
                    //                    {
                    //                        var message = new DepictionMessage(string.Format("Image was not created."), 6);
                    //                        DepictionAccess.NotificationService.DisplayMessage(message);
                    //                        return null;
                    //                    }
                    //                    var metadata = new DepictionImageMetadata(partialName, "File");
                    //                    if (region != null)
                    //                    {
                    //                        metadata = new DepictionImageMetadata(partialName, region.TopLeft, region.BottomRight, "File") { CanBeManuallyGeoAligned = false }; ;
                    //                    }
                    //                    elementModel.SetPropertyValue("DisplayName", provider.DisplayName);
                    //                    elementModel.SetImageMetadata(metadata);
                    //
                    //                    return elementModel;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            //provider.TileServiceInformation -= provider_TileServiceInformation;
            UpdateStatusReport(string.Format("Creating image element"));
            var elementType = "Depiction.Plugin.Image";
            var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(elementType);
            var elementModel = ElementFactory.CreateElementFromPrototype(prototype);
            var partialName = args as string;
            if (partialName == null)
            {
                var message = new DepictionMessage(string.Format("Image was not created."), 6);
                DepictionAccess.NotificationService.DisplayMessage(message);
                return;
            }
            var metadata = new DepictionImageMetadata(partialName, "File");
            if (region != null)
            {
                metadata = new DepictionImageMetadata(partialName, region.TopLeft, region.BottomRight, "File") { CanBeManuallyGeoAligned = false }; ;
            }
            elementModel.SetPropertyValue("DisplayName", provider.DisplayName);
            elementModel.SetImageMetadata(metadata);

            foreach (var tag in tagList)
            {

                elementModel.Tags.Add(tag);
            }


            if (DepictionAccess.CurrentDepiction != null)
            {
               
                //Order is important, for now
                var elements = new List<IDepictionElement> { elementModel };
                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, addToMainMap);
                if (!addToMainMap)
                {
                    DepictionAccess.CurrentDepiction.CreateAndAddRevealer(elements, string.Format("{0}", provider.DisplayName),null);

//                    var geoBounds = MapCoordinateBounds.GetGeoRectThatIsPercentage(
//                      DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds,
//                      .8, null);
//                    var revealer = new DepictionRevealer(string.Format("{0}", provider.DisplayName), geoBounds);
//                    DepictionAccess.CurrentDepiction.AddRevealer(revealer);
//                    revealer.AddElementList(elements);
                }
            }

            //           like DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new List<IDepictionElement> { elementModel }, true);
        }
    }
}