﻿namespace Depiction.ViewModels.ViewModelHelpers
{
    public class ViewModelConstants
    {
        #region Map size and min/max zoom

        public const double MapSize = DefaultMapSize * MapSizeMulti;
        private const double DefaultMapSize = 300000;
        public const double MapSizeMulti = 100;
        public const double MaxZoomFactor = 4500.0 / MapSizeMulti;//4500 is the max, there are actually some miss hits when editing zoi at that zoom
        public const double MinZoomFactor = .001 / MapSizeMulti;
        #endregion

        #region Zoom factors

        protected const double zoomFineToCoarseRatio = 9.2D / 11.0D; // Ditto.
        public const double ZoomRatioInCoarse = 11.0 / 9.0; // "Because it looks good".
        public const double ZoomRatioInFine = ZoomRatioInCoarse * zoomFineToCoarseRatio;
        public const double ZoomRatioOutCoarse = 1D / ZoomRatioInCoarse;
        public const double ZoomRatioOutFine = ZoomRatioOutCoarse / zoomFineToCoarseRatio;
        #endregion
    }

    public enum PanDirection
    {
        Left=0,
        Right=1,
        Up=2,
        Down=3,
        Mouse=4
    }

    public enum ZoomDirection
    {
        In=0, Out=1
    }

    public enum DepictionCanvasMode
    {
        Uninitialized,
        RegionSelection,
        Normal,
        SuspendedAdd,
        ElementAdd,
        AnnotationAdd,
        ZOIEdit,
        ZOIAdd,
        PointElementRegistration,
        ImageElementRegistration,
        AreaSelection,
        ArbitraryAreaSelection
    }

    public enum ZOIEditThumbType
    {
        Edge,
        Vertex
    }

    public enum GeneralThumbType
    {
        None,
        Main,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        TopMiddle,
        BottomMiddle,
        LeftMiddle,
        RightMiddle,
        ScaleUp,
        ScaleDown
    }

    public enum CurrentAppDepictionScreen
    {
        Welcome, Samples, World, Location
    }

    public enum ServerType
    {
        WFS,
        WMS
        //WCS goes bye bye for now 
        //Mar 6 2009
    }
}