﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Properties;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.ViewModels.ViewModelHelpers
{
    public class ViewModelHelperMethods
    {
        static public void AttachMouseToDragger(MouseButtonEventArgs e, Thumb randomDragger)
        {//This does the virtual grap on the canvas
//            randomDragger.CaptureMouse()
            if (Mouse.Capture(randomDragger, CaptureMode.Element))
            {
                var ea = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton);
                ea.RoutedEvent = UIElement.MouseDownEvent;
                ea.Handled = false;
                if(!randomDragger.IsDragging) Mouse.DirectlyOver.RaiseEvent(ea);
            }
        }
        static public bool IsTypeNumeric(Type val, object value)
        {
            double conv = 0;
            if (double.TryParse(value.ToString(), out conv))
            {
                return true;
            }
            if (val == null) return false;
            if (val == typeof(int) || val == typeof(double) || val == typeof(float) || val == typeof(short))
            {
                return true;
            }
            if (val.BaseType.Equals(typeof(BaseMeasurement)))
            {
                return true;
            }
            return false;
        }
        static public string GetInternalNameFromString(string inString)
        {
            var spaceLess = inString.Replace(" ", String.Empty);
            var internalName = new string(spaceLess.ToCharArray().Where(Char.IsLetterOrDigit).ToArray());
            return internalName;
        }
        #region Depiction types. oh man not another one of these
        public static IEnumerable<KeyValuePair<string, object>> GetSimpleDepictionTypes()
        {
            var list = new List<KeyValuePair<string, object>>();
            var currentSystem = Settings.Default.MeasurementSystem;

            list.Add(new KeyValuePair<string, object>("Number", 0d));
            list.Add(new KeyValuePair<string, object>("Yes/No", false));
            list.Add(new KeyValuePair<string, object>("Text", ""));
            list.Add(new KeyValuePair<string, object>("Color", Colors.White));
            list.Add(new KeyValuePair<string, object>("Angle", new Angle()));

            IMeasurement measurement = new Area(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Area", measurement));

            measurement = new Distance(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Distance", measurement));

            measurement = new Speed(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Speed", measurement));

            measurement = new Volume(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Volume", measurement));

            measurement = new Weight(currentSystem, MeasurementScale.Normal, 0);
            list.Add(new KeyValuePair<string, object>("Weight", measurement));

            measurement = new Temperature(currentSystem, MeasurementScale.Normal, 0);
            list.Add(new KeyValuePair<string, object>("Temperature", measurement));

            return list;
        }


        public static IEnumerable<KeyValuePair<string, object>> GetDepictionTypes()
        {
            var list = new List<KeyValuePair<string, object>>();
            var currentSystem = Settings.Default.MeasurementSystem;

            list.Add(new KeyValuePair<string, object>("Number", 0d));
            list.Add(new KeyValuePair<string, object>("Yes/No", false));
            list.Add(new KeyValuePair<string, object>("Text", ""));
            list.Add(new KeyValuePair<string, object>("Color", Colors.White));
            list.Add(new KeyValuePair<string, object>("Angle", new Angle()));

            IMeasurement measurement = new Area(currentSystem,MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Area (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Area (" + measurement.GetUnits(currentSystem, MeasurementScale.Normal) + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Area (" + measurement.GetUnits(currentSystem, MeasurementScale.Large) + ")", measurement));

            measurement = new Distance(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Distance (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Distance (" + measurement.GetUnits(currentSystem, MeasurementScale.Normal) + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Distance (" + measurement.GetUnits(currentSystem, MeasurementScale.Large) + ")", measurement));

            measurement = new Speed(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Speed (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Speed (" + measurement.GetUnits(currentSystem, MeasurementScale.Large) + ")", measurement));

            measurement = new Volume(currentSystem, MeasurementScale.Small, 0);
            list.Add(new KeyValuePair<string, object>("Volume (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Volume (" + measurement.GetUnits(currentSystem, MeasurementScale.Large) + ")", measurement));

            measurement = new Weight(currentSystem, MeasurementScale.Normal, 0);
            list.Add(new KeyValuePair<string, object>("Weight (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));
            list.Add(new KeyValuePair<string, object>("Weight (" + measurement.GetUnits(currentSystem, MeasurementScale.Large) + ")", measurement));

            measurement = new Temperature(currentSystem, MeasurementScale.Normal, 0);
            list.Add(new KeyValuePair<string, object>("Temperature (" + measurement.GetCurrentSystemDefaultScaleUnits() + ")", measurement));

            return list;
        }
        #endregion

        #region drawable geometry stuff

        static public StreamGeometry CreateGeometryFromEnhancedPointListWithChildren(List<EnhancedPointListWithChildren> zoiPixelPointCollectionList)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (zoiPixelPointCollectionList == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            foreach (var pl in zoiPixelPointCollectionList)
            {
                bool start = false;
                var isClosed = pl.IsClosed;
                var isFilled = pl.IsFilled;

                foreach (var p in pl.Outline)
                {
                    if (!start)
                    {
                        context.BeginFigure(p, isFilled, isClosed);
                        start = true;
                    }
                    else
                    {
                        context.LineTo(p, true, true);
                    }
                }
                var holes = pl.Children;
                foreach (var hole in holes)
                {
                    bool startHole = false;
                    var isHoleClosed = hole.IsClosed;
                    var isHoleFilled = hole.IsFilled;

                    foreach (var p in hole.Outline)
                    {
                        if (!startHole)
                        {
                            context.BeginFigure(p, isHoleFilled, isHoleClosed);
                            startHole = true;
                        }
                        else
                        {
                            context.LineTo(p, true, true);
                        }
                    }
                }
            }

            context.Close();
            streamGeometry.Freeze();

            return streamGeometry;
        }
        #endregion

        
        #region Element coloring

        public static void SetThematicMapping(IEnumerable<IDepictionElement> elements, string mappingPropName, ColorInterpolator map)
        {
            foreach (var element in elements)
            {
                if (string.IsNullOrEmpty(element.ImageMetadata.ImageFilename))
                {
                    if (string.IsNullOrEmpty(mappingPropName))//????? when would this happen
                    {
                        var value = 0;
                        MapColorForValue(element, map, value);
                    }
                    else
                    {
                        object val;
                        if (element.GetPropertyValue(mappingPropName, out val))
                        {
                            MapColorForValue(element, map, val);
                        }
                    }
                }
            }
        }
        private static void MapColorForValue(IDepictionElement element, ColorInterpolator map, object value)
        {
            Color color = map.GetColorForValue(value);
            switch (element.ZoneOfInfluence.DepictionGeometryType)
            {
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    element.SetPropertyValue("ZOIFill", color);

                    break;
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                    element.SetPropertyValue("ZOIBorder", color);
                    break;
                case DepictionGeometryType.Point:
                    element.SetPropertyValue("IconBorderColor", color);
                    DepictionIconBorderShape border;
                    if(element.GetPropertyValue("IconBorderShape",out border))
                    {
                        if(border.Equals(DepictionIconBorderShape.None))
                        {
                            element.SetPropertyValue("IconBorderShape", DepictionIconBorderShape.Square);
                        }
                    }
                    break;

            }
        }
        #endregion

    }
}