﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IMapElementLabelViewModel : INotifyPropertyChanged
    {
        ICommand HideLabelCommand { get; }
        ICommand ShowLabelParentsInfoCommand { get; }

        double LabelCenterX { get; }
        double LabelCenterY{ get;}

        double MinWidth { get; }
        double MinHeight { get; }
        double ScreenPixelWidth { get; }
        double ScreenPixelHeight { get; }
        
        Point TopLeft { get; }
        void SetTopLeft(double top, double left);
        void SetWidthHeight(double width, double height);
        void SetInverseScale(double inverseScale);
        TransformGroup LabelTransform { get;  }
        Visibility LabelVisibility { get; set; }
        string ParentElementID { get; }

    }
}