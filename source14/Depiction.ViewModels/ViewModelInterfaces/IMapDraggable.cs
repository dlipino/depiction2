﻿using System.Windows;
using System.Windows.Media;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IMapDraggable : IBaseDepictionMapViewModel
    {
        TranslateTransform ElementImagePosition { get; }
        bool IsElementZOIDraggable { get;  }
        bool IsElementIconDraggable { get; }

        void SetElementPosition(Point position);
       
    }
}