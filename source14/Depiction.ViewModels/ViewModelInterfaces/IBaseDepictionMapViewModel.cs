﻿using System.Windows.Media;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IBaseDepictionMapViewModel
    {
        ImageSource IconSource { get; }
    }
}
