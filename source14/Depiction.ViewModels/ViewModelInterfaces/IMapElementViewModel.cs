﻿using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IMapElementViewModel : INotifyPropertyChanged, IMapDraggable
    {
        TransformGroup ElementIconTransform { get; }
        TransformGroup ElementTopLeftTransform { get; }

        ICommand ReregisterImagePassCommand { get; set; }

        IMapZoneOfInfluenceViewModel ElementZOI { get; }

        double IconSize { get; }
        string ElementToolTip { get; }
        Image ElementImage { get; }
        object ElementIcon { get; }
        void UpdateIconTransform();
        void SetIconScale(double newScale);
    }
}