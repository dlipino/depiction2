// This is the main DLL file.

#include "stdafx.h"

#include "Coverage.h"


#include "vtdata/ElevationShader.h"
#include "vtdata/HeightField.h"

#define PI	3.14159265358979323846264338f
#define SMALLDOUBLE 0.000001

using System::String;
using System::Collections::ArrayList;

//always use the new grid's spacing when resampling two grids
#define USENEWGRIDSPACING true
namespace DepictionCoverage
{

	//
	//create a new grid with the specified geographic corners
	//

	bool Coverage::Create(double south, double north, double east, double west, int width, int height, bool floatgrid, float defaultValue,
						  System::String^ projFilename, int wktLength)

	{


		char* wktArray = new char[wktLength];
		if (wktLength > 1)
		{
			char* projFilenameArray = new char[projFilename->Length + 1];

			sprintf(projFilenameArray, "%s", projFilename);
			FILE *fp = vtFileOpen(projFilenameArray, "r");

			fgets(wktArray, wktLength, fp);

			fclose(fp);
		}

		//
		//default projection is mercator
		//
		//
		vtProjection* newProj = new vtProjection();

		if (wktLength > 1)
		{
			newProj->SetTextDescription("wkt", wktArray);
		}
		else 
			newProj->SetTextDescription("wkt", "PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");
			

		DRECT corners;
		

		corners.top = north;
		corners.bottom = south;
		corners.left = west;
		corners.right = east;

		//north,south,east,west are all in geog coord system, i.e., latlon
		//convert to mercator
		DPoint2 topLeftInMerc, bottomRightInMerc, bottomLeftInMerc, topRightInMerc;
		topLeftInMerc.x = west;
		topLeftInMerc.y = north;
		bottomRightInMerc.x = east;
		bottomRightInMerc.y = south;
		bottomLeftInMerc.x = west;
		bottomLeftInMerc.y = south;
		topRightInMerc.x = east;
		topRightInMerc.y = north;

		m_pGrid->TransformPoint(m_projGeo, newProj, &topLeftInMerc);
		m_pGrid->TransformPoint(m_projGeo, newProj, &bottomRightInMerc);
		m_pGrid->TransformPoint(m_projGeo, newProj, &bottomLeftInMerc);
		m_pGrid->TransformPoint(m_projGeo, newProj, &topRightInMerc);

		corners.top = topLeftInMerc.y;
		corners.bottom = bottomRightInMerc.y;
		corners.left = topLeftInMerc.x;
		corners.right = bottomRightInMerc.x;

		vtElevationGrid* tempGrid = new vtElevationGrid(corners, width, height, floatgrid, *newProj);
		if (!tempGrid->IsValidGrid()) return false;

		//set initial values of zero
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				tempGrid->SetValue(i, j, defaultValue);
			}
		}

		m_imageWidth = width;
		m_imageHeight = height;

		//OVERWRITE m_pGrid
		//m_pGrid->CleanUp();
		m_pGrid->FreeData();
		m_pGrid = tempGrid;

		needRefreshing = true;

		
		return true;
	}


	bool Coverage::SaveToGeoTIFF(System::String^ fileName)
	{

		char* fnameCharArray = new char[fileName->Length+5];
		sprintf(fnameCharArray,"%s",fileName);
		return m_pGrid->SaveToGeoTIFF(fnameCharArray);

	}

	bool Coverage::GenerateQuikGridFromVTPGrid(vtElevationGrid* grid, bool useGrid)
	{
		if (!grid->HasData())
		{
			return false;
		}

	  if (m_surfaceGrid!=NULL) delete m_surfaceGrid;

	  //get the current corner values
	  GetCurrentCornersAndHeightExtents();

	  m_surfaceGrid = new SurfaceGrid(m_imageWidth, m_imageHeight);         // Define the grid to be generated.

	  if (m_surfaceGrid==NULL)return false;

	  if (m_thresholdedGrid!=NULL) delete m_thresholdedGrid;
	  m_thresholdedGrid = new SurfaceGrid(m_imageWidth + 2, m_imageHeight + 2);	//to be used for thresholding purposes

	  if (m_thresholdedGrid==NULL) return false;

	   // Initialize the grid x and y AND Z coordinates
	  for(int i = 0; i < m_imageWidth; i++)
	  {
		  m_surfaceGrid->xset( i, i );
		  m_thresholdedGrid->xset( i, i );
	  }
	  for(int i = 0; i < m_imageHeight; i++)
	  {
		  m_surfaceGrid->yset(i, i);
		  m_thresholdedGrid->yset(i, i);
	  }
	  for(int i = m_imageWidth; i < m_imageWidth + 2; i++)
		  m_thresholdedGrid->xset(i, i);
	  for(int i = m_imageHeight; i < m_imageHeight + 2; i++)
		  m_thresholdedGrid->yset(i, i);

	  DPoint2 point;
	  float elevValue;
	  int i, j;
	  for(i = 0; i < m_imageWidth; i++)
	   for (j = 0;j < m_imageHeight; j++)
	   {
		   if (!useGrid)
		   {
			   point = GetWorldCoordinates(i, j, m_imageWidth, m_imageHeight, true, SW);
			   elevValue = GetInterpolatedElevationValue(point.x, point.y, true);
			   m_surfaceGrid->zset(i, j, elevValue);
			   m_thresholdedGrid->zset(i + 1, j + 1, elevValue);
		   }
		   else
		   {
			   //does surface grid row increase from top to bottom?
			   //my guess is yes
			   m_surfaceGrid->zset(i, j, GetElevationFromGridCoordinates(i, m_imageHeight - 1 - j));
			   m_thresholdedGrid->zset(i + 1, j + 1, GetElevationFromGridCoordinates(i, m_imageHeight - 1 - j));
		   }
	   }

	   
		//fill top, left, right, and bottom lines with DEFAULT ELEVATION
		for(int i = 0; i < m_imageWidth + 2; i++)
		{
			m_thresholdedGrid->zset(i, 0, (float)DEFAULTELEVATION);
			m_thresholdedGrid->zset(i, m_imageHeight + 1, (float)DEFAULTELEVATION);
		}
		for(int j = 0; j < m_imageHeight + 2; j++)
		{
			m_thresholdedGrid->zset(0, j, (float)DEFAULTELEVATION);
			m_thresholdedGrid->zset(m_imageWidth + 1, j, (float)DEFAULTELEVATION);
		}

	   return true;
	}


	void Coverage::ConvertRasterToPolygon(double threshold, System::Collections::ArrayList^ cList)
	{

		if (!m_pGrid->HasData())
		{
			return;
		}
		//get the current corner values
		GetCurrentCornersAndHeightExtents();
		GenerateQuikGridFromVTPGrid(m_pGrid, true);

		float gridValue;

		for(int i = 1; i < m_imageWidth + 1; i++)
		{
			for(int j = 1; j < m_imageHeight + 1; j++)
			{
				gridValue = m_surfaceGrid->z(i - 1, j - 1);
				
				if (gridValue > threshold)
				{
					m_thresholdedGrid->zset(i, j, (float)1);
				}
				else
				{
					m_thresholdedGrid->zset(i, j, (float)0);
				}
			}
		}

		Contour(*m_thresholdedGrid, 0, cList);

		//
		//convert image/grid coordinates to latlon
		//
		ConvertImageToWorldCoordinates(cList, 1);
	}


	bool Coverage::LineOfSightContours(double centerX, double centerY, double heightOffset, double range, double heightTolerance,
		System::Collections::ArrayList^ cList, double startAngle, double stopAngle)
	{
		
		if (!m_pGrid->HasData())
		{
			return false;
		}
		//
		//int* centerCoords = GetImageCoordinatesUnsafe(centerX,centerY,*m_imageWidth,*m_imageHeight);
		//int startCol = centerCoords[0] - range;
		//int startRow = centerCoords[1] - range;
		//int endRow = centerCoords[1] + range;
		//int endCol = centerCoords[0] + range;

		//double* latlon = GetWorldCoordinates(startCol,startRow,*m_imageWidth,*m_imageHeight,true,SW);
		//double left = latlon[0];
		//double bottom = latlon[1];
		//latlon = GetWorldCoordinates(endCol,endRow,*m_imageWidth,*m_imageHeight,true,SW);
		//double right = latlon[0];
		//double top = latlon[1];
		//DRECT rect;
		//rect.top = top; rect.bottom = bottom; rect.left = left; rect.right = right;
		////CROP the grid to just the subgrid defined by the center point and range of the line of sight object
		//int newWidth;
		//int newHeight;
		//vtElevationGrid* tempGrid = MyCropElevationGrid(m_pGrid,left,top,right,bottom,&newWidth,&newHeight,true);

		//m_pGrid->FreeData();
		//m_pGrid = tempGrid;
		////
		////set the bounding GEO rectangle
		////
		//DLine2 line;
		//m_pGrid->GetCorners(line,true);
		//DPoint2 pointBottomLeft	= line.GetAt(0);
		//DPoint2 pointTopLeft	= line.GetAt(1);
		//DPoint2 pointTopRight	= line.GetAt(2);
		//DPoint2 pointBottomRight= line.GetAt(3);

		//rect.top = pointTopLeft.y;
		//rect.bottom = pointBottomRight.y;
		//rect.left = pointTopLeft.x;
		//rect.right = pointTopRight.x;
		//m_boundingRectGeo = new DRECT(rect.left,rect.top,rect.right,rect.bottom);


		//*m_imageWidth = newWidth;
		//*m_imageHeight = newHeight;
		//m_pGrid->SaveToGeoTIFF("C:\\downloads\\test.tif");

		//get the current corner values
		GetCurrentCornersAndHeightExtents();
		//quikgrid is used for contouring the line of sight grid
		GenerateQuikGridFromVTPGrid(m_pGrid,false);

		FPoint3 center;
		center.x = centerX;
		center.y = centerY;
		center.z = heightOffset;
		//
		//get the center point's row and column
		//
		int width;
		int height;

		m_pGrid->GetDimensions(width, height);

		IPoint2 coords = GetImageCoordinatesUnsafe(centerX, centerY, width, height);
		float elevvalue = m_pGrid->GetElevation(coords.x, coords.y, true);
		if (elevvalue == (float)SHRT_MIN) elevvalue = 0;
		center.z += elevvalue;

		if (!LineOfSightGrid(center, range, heightTolerance, cList, startAngle, stopAngle))
		return false;

		Contour(*m_thresholdedGrid,0, cList);
		//
		//convert image/grid coordinates to latlon
		//
		ConvertImageToLatLonCoordinates(cList, 1);
	}
	
	bool Coverage::LineOfSightGrid(FPoint3 center, double pixelRadius, double heightTolerance,System::Collections::ArrayList^ cList, double startAngle, double stopAngle)
	{

			DLine2 line ;
			m_pGrid->GetCorners(line,false);
			int size = line.GetSize();

			DRECT rect;
			DPoint2 pointBottomLeft	= line.GetAt(0);
			DPoint2 pointTopLeft	= line.GetAt(1);
			DPoint2 pointTopRight	= line.GetAt(2);
			DPoint2 pointBottomRight= line.GetAt(3);

			DRECT corners;
			corners.top =pointTopLeft.y;
			corners.bottom = pointBottomLeft.y;
			corners.left = pointTopLeft.x;
			corners.right = pointTopRight.x;

			int width;
			int height;

			m_pGrid->GetDimensions(width, height);

			DPoint2 spacing = m_pGrid->GetSpacingD();
			IPoint2 centerCoords = GetImageCoordinatesUnsafe(center.x, center.y, width, height);

			bool floatMode = m_pGrid->IsFloatMode();
			//set initial values of zero
			for (int i = 0; i < width; i++)
			{
				for (int j = 0;j < height; j++)
				{
					if (i == centerCoords.x && j == centerCoords.y)
					{
						
						m_thresholdedGrid->zset(i+1,(j+1),1);
					}
					else
					{
						m_thresholdedGrid->zset(i+1,(j+1),0);
						
					}
				}
			}
				
			//find line of sight only within the bounding box that encloses the range distance
			int startRow, endRow, startCol, endCol;
			startRow = centerCoords.y - pixelRadius;
			startCol = centerCoords.x - pixelRadius;
			
			endRow = centerCoords.y + pixelRadius;
			endCol = centerCoords.x + pixelRadius;

			//center point is in latlon coordinates
			//convert it to local (mercator)
			DPoint2 centerPoint ;
			centerPoint.x = center.x;
			centerPoint.y = center.y;
			m_pGrid->TransformPoint(m_projGeo,m_proj,&centerPoint);
			center.x = centerPoint.x;
			center.y = centerPoint.y;
			//center.z remains unchanged since it is the actual elevation value
		
			
			if (heightTolerance<=0)heightTolerance=0.1;//this is needed since LOS algorithm
													  //produces choppy polygons when height is zero
			FPoint3 threedpoint;
			pixelRadius *= pixelRadius;
			int i,j;
			for(i=startCol;i<endCol;i++)
			{
				if (i<0 || i>width-1)continue;

				for(j=startRow;j<endRow;j++)
				{
					if (j<0 || j>height-1)continue;
						if (PixelDistanceSquared(i, j, centerCoords.x, centerCoords.y) < pixelRadius)
							if (IsInsideArc(centerCoords.x, centerCoords.y, i, j, startAngle, stopAngle))
							{
								//construct threedpoint of target...source is the center point
								ThreeDPoint(i,j,width,height,&threedpoint);
								if ( m_pGrid->LineOfSight(center,threedpoint,spacing, heightTolerance))
								{
									m_thresholdedGrid->zset(i+1,(j+1),1);
								}
							}
				}
			}
		
			return true;
	}
	bool Coverage::IsInsideArc(int centerCol, int centerRow, int col, int row, double startAngle, double stopAngle )
	{
		double diffAngle = fabs(startAngle-stopAngle);
		if (diffAngle==360||diffAngle==0)return true;

		double a,b,c,d,z;
		double angle;
		//if any of the four corners of the cell defined by (col,row) is within the angle spanned
		//by startAngle and stopAngle, then return true

		//since the representative grid cell location is SOUTHWEST corner,
		//we will look at four cells
		//(col,row), (col,row+1), (col+1,row+1), and (col+1,row)


		double mag;
		a = 1;
		b = 0;
		
		int rowOffset, colOffset;

		double maxAngle,minAngle;
		maxAngle=-500;minAngle=500;

		double pos,neg;
		pos=neg=false;
	
		for(rowOffset=0;rowOffset<=1;rowOffset++)
			for(colOffset=0;colOffset<=1;colOffset++)
			{
				c = (col+colOffset) - centerCol;
				d = (row+rowOffset) - centerRow;
				
				mag = sqrt(c*c+d*d);
				if (mag<=SMALLDOUBLE) return false;
				z = (a*c) / mag;
				angle = acos(z); //this is the angle subtended by the X axis and the line joining
										//the center coordinate and the point under consideration
										//the angle is in RADIANS
				angle *= 180/PI;

				if (d<0)
				{
					angle = 360-angle;
					neg=true;
				}
				else pos=true;
				
				if (angle>maxAngle)maxAngle=angle;
				if (angle<minAngle)minAngle=angle;

			}
			if (c>0&&pos&&neg)//corners of the pixel stride the 0 degree, horizontal X axis
			{
				//angles could be from 330 to 30...
				//this doesn't mean it goes counterclockwise from 30 to 330 for an arc of 300 degrees
				//so, how do we check if startAngle and stopAngle are inside this smaller arc going
				//from 330, 320, 310...0,20,30?
				//
				double tempDouble;
				tempDouble = maxAngle;
				maxAngle=minAngle;
				minAngle=tempDouble;

				if (startAngle>=minAngle)return true;
				if (startAngle>=0&&startAngle<=maxAngle)return true;

				if (stopAngle>=minAngle)return true;
				if (stopAngle>=0&&stopAngle<=maxAngle)return true;

				return false;
			}


				if (startAngle<=stopAngle)
				{
					//stop and start angles encompass the grid cell
					if (startAngle<=minAngle&&stopAngle>=maxAngle)return true;
					//stop angle passes thro cell, not start
					if (startAngle<=minAngle&&stopAngle<=maxAngle&&stopAngle>=minAngle)return true;
					//start goes thro cell, not stop angle
					if (startAngle>=minAngle&&startAngle<=maxAngle&&stopAngle>=maxAngle)return true;
					if (startAngle>=minAngle&&stopAngle<=maxAngle)return true;//
				}
				else
				{	//the arc could be, say, from 350 to 190 degrees, going counterclockwise!

					//check 1: for angles from startAngle to 360

					//stop and start angles encompass the grid cell
					if (startAngle<=minAngle&&360>=maxAngle)return true;
					//stop angle passes thro cell, not start
					if (startAngle<=minAngle&&360<=maxAngle&&360>=minAngle)return true;
					//start goes thro cell, not stop angle
					if (startAngle>=minAngle&&startAngle<=maxAngle&&360>=maxAngle)return true;
					if (startAngle>=minAngle&&360<=maxAngle)return true;//

					//check 2: for angles from zero to stopAngle

					//stop and start angles encompass the grid cell
					if (0<=minAngle&&stopAngle>=maxAngle)return true;
					//stop angle passes thro cell, not start
					if (0<=minAngle&&stopAngle<=maxAngle&&stopAngle>=minAngle)return true;
					//start goes thro cell, not stop angle
					if (0>=minAngle&&0<=maxAngle&&stopAngle>=maxAngle)return true;
					if (0>=minAngle&&stopAngle<=maxAngle)return true;//
				}
			
		return false;

	}

	double Coverage::PixelDistance(int x1, int y1, int x2, int y2)
	{
		return sqrt((double)( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
	}

	double Coverage::PixelDistanceSquared(int x1, int y1, int x2, int y2)
	{
		return (( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
	}

	void Coverage::ThreeDPoint(int col, int row, int gridWidth, int gridHeight, FPoint3* threedpoint)
	{
		DPoint2 coords = GetWorldCoordinates(col,row,gridWidth,gridHeight,true,SW);
		threedpoint->x = coords.x;
		threedpoint->y = coords.y;
		threedpoint->z = m_pGrid->GetElevation(col, row, false);
	}

	void Coverage::FloodContours(double startX, double startY, double offset, double horizontalResolution, System::Collections::ArrayList^ cList)
	{

		if (!m_pGrid->HasData())
		{
			return ;
		}

		//get the current corner values
		GetCurrentCornersAndHeightExtents();
		if (!m_pGrid->HasData())
		{
			return ;
		}

		if (m_surfaceGrid==NULL || needRefreshing)
		{
			GenerateQuikGridFromVTPGrid(m_pGrid,true);
			needRefreshing=false;
		}

		
		//Contour algorithm in contour.cpp requires a one-pixel-wide padding around the terrain grid
		//to return meaningful results
		//So, m_thresholdedGrid has a dimension of *m_imageWidth+2, *m_imageHeight+2
		//these boundary row/cols are filled with the DEFAULTELEVATION value
		//
		ThresholdQuikGrid(offset, horizontalResolution);

		Contour(*m_thresholdedGrid,0, cList);

		//
		//convert image/grid coordinates to latlon
		//
		ConvertImageToWorldCoordinates(cList, 1);
	}

	void Coverage::Dilate(System::Collections::ArrayList^ se)
	{
		float gridValue, newGridValue;
		int x, y;

		if (!InitializeBackupGrid(m_thresholdedGrid)) return;
		for (int i = 1; i < m_imageWidth + 1; i++)
		{
			for (int j = 1; j < m_imageHeight + 1; j++)
			{
				gridValue = m_thresholdedGrid->z(i, j);
				if (gridValue >= 1)
				{
					for (int elem = 0; elem < se->Count; elem++)
					{
						x = ((System::Windows::Point^)se[elem])->X;
						y = ((System::Windows::Point^)se[elem])->Y;
						if ((x + i) >=1 && (x + i) < (m_imageWidth + 1) && (y + j) >= 1 && (y + j) < (m_imageHeight + 1))
						{
							newGridValue = m_thresholdedGrid->z(x + i, y + j);
							if (newGridValue <1)
								m_backupGrid->zset(x + i, y + j, (float)1);

						}
					}
				}
			}
		}
		//restore backupgrid to thresholdedGrid
		for(int i = 0; i < m_imageWidth + 2; i++)
		{
			for(int j = 0; j < m_imageHeight + 2; j++)
			{
				m_thresholdedGrid->zset(i, j, m_backupGrid->z(i, j));
			}
		}
		
	}

	void Coverage::Erode(System::Collections::ArrayList^ se)
	{
		float gridValue, newGridValue;
		int x, y;

		if (!InitializeBackupGrid(m_thresholdedGrid)) return;
		for (int i = 1; i < m_imageWidth + 1; i++)
		{
			for (int j = 1; j < m_imageHeight + 1; j++)
			{
				gridValue = m_thresholdedGrid->z(i, j);
				if (gridValue >= 1)
				{
					for (int elem = 0; elem < se->Count; elem++)
					{
						x = ((System::Windows::Point^)se[elem])->X ;
						y = ((System::Windows::Point^)se[elem])->Y ;
						if ((x + i) >=1 && (x + i) < (m_imageWidth + 1) && (y + j) >= 1 && (y + j) < (m_imageHeight + 1))
						{
							newGridValue = m_thresholdedGrid->z(x + i,y + j);
							if (newGridValue < (float)1)
							{
								m_backupGrid->zset(i, j, (float)0);
								elem = se->Count;
							}
						}
					}
				}
			}
		}
		
		//restore backupgrid to thresholdedGrid
		for (int i = 0; i < m_imageWidth + 2; i++)
		{
			for (int j = 0; j < m_imageHeight + 2; j++)
			{
				m_thresholdedGrid->zset(i, j, m_backupGrid->z(i, j));
			}
		}
	}
	
	bool Coverage::InitializeBackupGrid(SurfaceGrid* grid)
	{

		if (m_backupGrid != NULL)
		{
		  //reinitialize the values to zero
			for (int i = 0; i < m_imageWidth + 2; i++)
			{
				for (int j = 0; j < m_imageHeight + 2; j++)
				{
					m_backupGrid->zset(i, j, grid->z(i, j));
				}
			}
			return true;
		}
		m_backupGrid = new SurfaceGrid(m_imageWidth + 2, m_imageHeight + 2);
		if (m_backupGrid == NULL) return false;
	  
	  

	    // Initialize the grid x and y AND Z coordinates
	    for (int i = 0; i < m_imageWidth + 2; i++)
	    {
		    m_backupGrid->xset(i, i);
	    }
	    for (int i = 0; i < m_imageHeight + 2; i++)
	    {
		    m_backupGrid->yset(i, i);
	    }
		for(int i = 0; i < m_imageWidth + 2; i++)
			for(int j = 0; j < m_imageHeight + 2; j++)
				m_backupGrid->zset(i, j, grid->z(i, j));

		return true;
	}

	void Coverage::ThresholdQuikGrid(double offset, double resolution)
	{
		float localOffset = (float)offset;
		float gridValue;

		for (int i = 1; i < m_imageWidth + 1; i++)
		{
			for (int j = 1; j < m_imageHeight + 1; j++)
			{
				gridValue = m_surfaceGrid->z(i - 1, j - 1);
				if (gridValue == UNKNOWN)
					gridValue = DEFAULTELEVATION;
				else if (gridValue == NODATA)
					gridValue = NODATA;
				if (gridValue <= localOffset)
				{
					m_thresholdedGrid->zset(i, j, (float)1);
				}
				else
				{
					m_thresholdedGrid->zset(i, j, (float)0);
				}
			}
		}
		//fill top, left, right, and bottom lines with DEFAULTELEVATION
		for (int i = 0; i < m_imageWidth + 2; i++)
		{
			m_thresholdedGrid->zset(i, 0, (float)DEFAULTELEVATION);
			m_thresholdedGrid->zset(i, m_imageHeight + 1, (float)DEFAULTELEVATION);
		}
		for (int j = 0; j < m_imageHeight + 2; j++)
		{
			m_thresholdedGrid->zset(0, j, (float)DEFAULTELEVATION);
			m_thresholdedGrid->zset(m_imageWidth + 1, j, (float)DEFAULTELEVATION);
		}

		//
		//REMOVE LONG STRETCHES OF HORIZONTAL AND VERTICAL LEAKS
		//
		//DO a morphological OPENING, which is an erosion followed by dilation
		//by the same structuring element
		//round desired resolution of the SE to the nearest integer
		int pixelResolution = (int)(resolution + 0.5);
		if (resolution >= 3)
		{
			System::Collections::ArrayList^ se = gcnew System::Collections::ArrayList();
			for(int i = 0; i < pixelResolution; i++)
				for(int j = 0; j < pixelResolution; j++)
				{
					System::Windows::Point^ pt = gcnew System::Windows::Point(i - (int)(resolution / 2), j - (int)(resolution / 2));
					se->Add(pt);
				}
			Erode(se);
			Dilate(se);
		}

	}

	void Coverage::ConvertImageToWorldCoordinates(System::Collections::ArrayList^ cList, int offset)
	{
		float x, y;
		float currentX = -10000, currentY = -10000;


		for(int i = 0; i < cList->Count; i++)
		{
			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			for(int j = 0; j < aList->Count; j++)
			{
				x = ((System::Windows::Point^)aList[j])->X - offset;
				y = ((System::Windows::Point^)aList[j])->Y - offset;
				//X (after the offset subtraction) can range from -1 (with reference to the main grid, remember that thresholdedGrid has row/col 
				//padding at the outer boundary of the original terrain grid) to m_imageWidth
				//X value of -1 is outside the LEFT boundary of the original terrain grid. We want that snapped to the left edge
				//Same logic for Y
				//
				//The resultant contour will be entirely inside the Grid
				if (x < 0) x=0;
				if (x > m_imageWidth - 1) x = m_imageWidth - 1;
				if (y < 0) y = 0;
				if (y > m_imageHeight - 1) y = m_imageHeight - 1;

				if (x != currentX || y != currentY)
				{

					currentX = x; currentY = y;
					//
					//Contour algorithm returns the row,col coordinates of pixels that are on the OUTSIDE boundary of the
					//shape we are interested in drawing
					//
					//The lat,lon coordinates of a row,col pixel, by our convention returns the SW corner of the pixel
					//Depending on the location of the contour vertex, this coordinate may be OFF by up to ONE-PIXEL spacing
					//So, to split the difference, we can use the CENTER point of the pixel as the representative coordinate
					//returned by GetWorldCoordinates
					//
					DPoint2& worldCoords = GetWorldCoordinates(x, (m_imageHeight - 1 - y), m_imageWidth, m_imageHeight, false, CENTER);
					//the above logic is because
					//a) this method is only called with reference to xy coords in a SurfaceGrid object
					//b) SurfaceGrid object has rows going from top to bottom
					//c) while TerrainGrid has rows going from bottom to top
					//capiche?
					aList->Insert(j, gcnew System::Windows::Point(worldCoords.x, worldCoords.y));
					aList->RemoveAt(j + 1);
				}
				else
				{
					aList->RemoveAt(j);
					j--;
				}
			}
		}
		bool notClosed = false;
		for (int i = 0; i < cList->Count; i++)
		{

			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			int count = aList->Count;
			//see if the polygons are closed
			if ( ((System::Windows::Point^)aList[0])->X ==
				((System::Windows::Point^)aList[count-1])->X &&
				((System::Windows::Point^)aList[0])->Y ==
				((System::Windows::Point^)aList[count-1])->Y
				)
			{
			}
			else
			{
				notClosed = true;
			}

		}

		//m_pGrid->SaveToPNG16("C:\\downloads\\contour.png");
		//SaveTo24BitRGB("C:\\downloads\\colormap.jpg",90);


	}

	void Coverage::ConvertImageToLatLonCoordinates(System::Collections::ArrayList^ cList, int offset)
	{
		DPoint2 worldCoords;
		float x, y;
		float currentX = -10000, currentY = -10000;


		for (int i = 0; i < cList->Count; i++)
		{
			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			for (int j = 0; j < aList->Count; j++)
			{
				x = ((System::Windows::Point^)aList[j])->X - offset;
				y = ((System::Windows::Point^)aList[j])->Y - offset;
				if (x != currentX || y != currentY)
				{
					currentX = x; currentY = y;
					worldCoords = GetWorldCoordinates(x, y, m_imageWidth, m_imageHeight, false, SW);
					//the above logic is because
					//a) this method is only called with reference to xy coords in a SurfaceGrid object
					//b) SurfaceGrid object has rows going from top to bottom
					//c) while TerrainGrid has rows going from bottom to top
					//capiche?
					aList->Insert(j, gcnew System::Windows::Point(worldCoords.x, worldCoords.y));
					aList->RemoveAt(j + 1);
				}
				else
				{
					aList->RemoveAt(j);
					j--;
				}
			}
		}
		bool notClosed = false;
		for(int i = 0; i < cList->Count; i++)
		{

			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			int count = aList->Count;
			//see if the polygons are closed
			if ( ((System::Windows::Point^)aList[0])->X ==
				((System::Windows::Point^)aList[count-1])->X &&
				((System::Windows::Point^)aList[0])->Y ==
				((System::Windows::Point^)aList[count-1])->Y
				)
			{
			}
			else
			{
				notClosed=true;
			}

		}

		//m_pGrid->SaveToPNG16("C:\\downloads\\contour.png");
		//SaveTo24BitRGB("C:\\downloads\\colormap.jpg",90);


	}

	IPoint2 Coverage::GetImageCoordinates(double x, double y, int gridWidth, int gridHeight)
	{
		DLine2 line ;
		m_pGrid->GetCorners(line,true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		DPoint2 spacing = m_pGrid->GetSpacingD();

		int row, col;

		col = (int)((x-rect.left)/( fabs(rect.left-rect.right)) * (gridWidth-1)+0.001);
		row = (int)((y-rect.bottom)/ ( fabs(rect.top-rect.bottom)) * (gridHeight-1)+0.001);

		if (col<0)col=0;
		if (col>gridWidth-1)col=gridWidth-1;

		if (row<0)row=0;
		if (row>gridHeight-1)row=gridHeight-1;

		return IPoint2(col, row);
	}


	//Get image coordinates even if x,y is outside the grid
	//don't crop it to a value inside the grid
	IPoint2 Coverage::GetImageCoordinatesUnsafe(double x, double y, int gridWidth, int gridHeight)
	{
		DLine2 line ;
		m_pGrid->GetCorners(line,true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		DPoint2 spacing=  m_pGrid->GetSpacingD();

		int row, col;

		col = (int)((x-rect.left)/( fabs(rect.left-rect.right)) * (gridWidth-1)+0.001);
		row = (int)((y-rect.bottom)/ ( fabs(rect.top-rect.bottom)) * (gridHeight-1)+0.001);

		return IPoint2(col, row);
	}

	//
	//Given X, Y in lon/lat, get the image coordinates in the resampled terrain grid
	//
	IPoint2 Coverage::GetImageCoordinates(double x, double y)
	{

		DLine2 line ;
		m_pGrid->GetCorners(line, true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		DPoint2 spacing = m_pGrid->GetSpacingD();

		int row, col;

		col = (int)((x-rect.left)/( fabs(rect.left-rect.right)) * (m_imageWidth-1)+0.001);
		row = (int)((y-rect.bottom)/ ( fabs(rect.top-rect.bottom)) * (m_imageHeight-1)+0.001);

		if (col<0)col=0;
		if (col>m_imageWidth-1)col=m_imageWidth-1;

		if (row<0)row=0;
		if (row>m_imageHeight-1)row=m_imageHeight-1;

		return IPoint2(col, row);
	}

	double Coverage::GetLatitude(int col,int row, int gridWidth, int gridHeight)
	{
		return GetWorldCoordinates(col,row,gridWidth,gridHeight,false,SW).y;
	}

	double Coverage::GetLongitude(int col,int row, int gridWidth, int gridHeight)
	{
		return GetWorldCoordinates(col,row,gridWidth,gridHeight,false,SW).x;
	}
	//
	//Given image coordinates in the terrain grid, obtain the world coordinates in lon/lat
	//
	DPoint2 Coverage::GetWorldCoordinates(int col, int row, bool localCS, ORIGIN desiredOrigin)
	{
	
		DLine2 line;
		DRECT rect;
		if (!localCS && m_boundingRectGeo==NULL)
		{
			m_pGrid->GetCorners(line,true);
			DPoint2 pointBottomLeft	= line.GetAt(0);
			DPoint2 pointTopLeft	= line.GetAt(1);
			DPoint2 pointTopRight	= line.GetAt(2);
			DPoint2 pointBottomRight= line.GetAt(3);

			rect.top = pointTopLeft.y;
			rect.bottom = pointBottomRight.y;
			rect.left = pointTopLeft.x;
			rect.right = pointTopRight.x;
			m_boundingRectGeo = new DRECT(rect.left,rect.top,rect.right,rect.bottom);
		}
		if (localCS && m_boundingRect==NULL)
		{
			m_pGrid->GetCorners(line,false);
			DRECT rect;
			DPoint2 pointBottomLeft	= line.GetAt(0);
			DPoint2 pointTopLeft	= line.GetAt(1);
			DPoint2 pointTopRight	= line.GetAt(2);
			DPoint2 pointBottomRight= line.GetAt(3);

			rect.top = pointTopLeft.y;
			rect.bottom = pointBottomRight.y;
			rect.left = pointTopLeft.x;
			rect.right = pointTopRight.x;
			m_boundingRect = new DRECT(rect.left,rect.top,rect.right,rect.bottom);

		}
		double lon, lat;

		if (!localCS)
		{
			lon = (double)(col) / ((double)m_imageWidth - 1) * fabs(m_boundingRectGeo->left - m_boundingRectGeo->right) + m_boundingRectGeo->left;
			lat = m_boundingRectGeo->bottom + ((double)row) / ((double)m_imageHeight - 1) * fabs(m_boundingRectGeo->top - m_boundingRectGeo->bottom);
		}
		else
		{
			lon = (double)(col) / ((double)m_imageWidth - 1) * fabs(m_boundingRect->left - m_boundingRect->right) + m_boundingRect->left;
			lat = m_boundingRect->bottom + ((double)row) / ((double)m_imageHeight - 1) * fabs(m_boundingRect->top - m_boundingRect->bottom);
		
		}

		//
		switch(desiredOrigin)
		{
		case CENTER:
			break;
		case NW:
			break;
		default:
			//default is SouthWest corner of the grid pixel
			break;
		}
		
		return DPoint2(lon, lat);
	}

	//
	//Given image coordinates in the terrain grid, obtain the world coordinates in lon/lat
	//
	DPoint2 Coverage::GetWorldCoordinates(int col, int row, int gridWidth, int gridHeight, bool localCS, ORIGIN desiredOrigin)
	{

	
		DLine2 line ;
		DRECT rect;
		if (!localCS && m_boundingRectGeo==NULL)
		{
			m_pGrid->GetCorners(line,true);
			DPoint2 pointBottomLeft	= line.GetAt(0);
			DPoint2 pointTopLeft	= line.GetAt(1);
			DPoint2 pointTopRight	= line.GetAt(2);
			DPoint2 pointBottomRight= line.GetAt(3);

			rect.top = pointTopLeft.y;
			rect.bottom = pointBottomRight.y;
			rect.left = pointTopLeft.x;
			rect.right = pointTopRight.x;
			m_boundingRectGeo = new DRECT(rect.left,rect.top,rect.right,rect.bottom);
		}
		if (localCS && m_boundingRect==NULL)
		{
			m_pGrid->GetCorners(line,false);
			DRECT rect;
			DPoint2 pointBottomLeft	= line.GetAt(0);
			DPoint2 pointTopLeft	= line.GetAt(1);
			DPoint2 pointTopRight	= line.GetAt(2);
			DPoint2 pointBottomRight= line.GetAt(3);

			rect.top = pointTopLeft.y;
			rect.bottom = pointBottomRight.y;
			rect.left = pointTopLeft.x;
			rect.right = pointTopRight.x;
			m_boundingRect = new DRECT(rect.left,rect.top,rect.right,rect.bottom);

		}
		double lon, lat;
		double spacingx, spacingy;
		if (!localCS)
		{
			spacingx = fabs(m_boundingRectGeo->left-m_boundingRectGeo->right) / ((double)gridWidth-1);
			spacingy = fabs(m_boundingRectGeo->top-m_boundingRectGeo->bottom) / ((double)gridHeight-1);
			lon = (double)(col) * spacingx  + m_boundingRectGeo->left;
			lat = m_boundingRectGeo->bottom + ((double)row)*spacingy;
		}
		else
		{
			spacingx = fabs(m_boundingRect->left-m_boundingRect->right) / ((double)gridWidth-1);
			spacingy = fabs(m_boundingRect->top-m_boundingRect->bottom) / ((double)gridHeight-1);
			lon = (double)(col) * spacingx  + m_boundingRect->left;
			lat = m_boundingRect->bottom + ((double)row) * spacingy ;
		
		}
		switch(desiredOrigin)
		{
		case CENTER:
			//move the lat, lon coordinate half a pixel width north and east
			lat += spacingy/2;
			lon += spacingx/2;
			break;
		case NW:
			//move the lat, lon coordinate ONE pixel width north
			lat += spacingy;
			break;
		default:
			//default is SouthWest corner of the grid pixel
			break;
		}

		return DPoint2(lon, lat);
	}


	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns ZERO to enable flood calc
	//
	float Coverage::GetInterpolatedElevationValue(double x, double y, bool localCS)
		{

			//use GetFilteredValue on elevation grid at a specific point
			DPoint2 point ;
			point.x = x;
			point.y = y;
			if (!localCS) m_pGrid->TransformPoint(m_projGeo,m_proj,&point);
			float elevValue = m_pGrid->GetFilteredValue(point);
			if (elevValue==(float)UNKNOWN)
				elevValue= DEFAULTELEVATION;
			else if (elevValue==NODATA)
				elevValue=SHRT_MIN;

			return elevValue;

		}
	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns ZERO to enable flood calc
	//
	float Coverage::GetClosestElevationValue(double x, double y)
		{
			//use GetFilteredValue on elevation grid at a specific point
			DPoint2 point ;
			point.x = x;
			point.y = y;
			m_pGrid->TransformPoint(m_projGeo,m_proj,&point);
			float elevValue = m_pGrid->GetClosestValue(point);
			if (elevValue==(float)UNKNOWN)elevValue= System::Double::NaN;

			return elevValue;

		}

	//Get the elevation value from row/col grid coordinate

	float Coverage::GetElevationFromGridCoordinates(int col, int row)
	{

		float elevValue = m_pGrid->GetElevation(col,row,false);

		return elevValue;
	}
	float Coverage::GetConvolvedValue(int col, int row, int kernel)
	{
		int width = m_imageWidth;
		int height = m_imageHeight;

		int numNeighbors = 0;
		float avgValue=0;
		if (kernel<2) return m_pGrid->GetElevation(col,row);
		int offset = kernel-1 / 2;
        for (int i = -offset; i <= offset; i++)
            for (int j = -offset; j <= offset; j++)
            {
                if (col + i >= 0 && col + i < width && row + j >= 0 &&
                    row + j < height)
                {
                    if (i == 0 && j == 0) continue;
					float val = m_pGrid->GetElevation(col+i,row+j);
					if (val != UNKNOWN)
					{
						avgValue += val;
						numNeighbors ++;
					}
				}
			}
		if (numNeighbors != 0)
			return avgValue/numNeighbors;
		else return UNKNOWN;
	}
	//
	//Given a lon/lat value, set the elevation grid value at that point
	//
	bool Coverage::SetElevationValue(double lon, double lat, float elevValue)
	{
//from the lon/lat value, get the image grid coordinate
		//

		if (m_pGrid==NULL) return false;

		DLine2 line ;
		if ( !(m_pGrid->GetCorners(line,true)) )
		{
			return false;
		}

		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		int row, col;
		if (m_imageWidth <= 0 || m_imageHeight <= 0) return false;
		
		double doubleCol, doubleRow;
		doubleCol = ((lon - rect.left) / (fabs(rect.left - rect.right)) * (m_imageWidth - 1)) + 0.0000001;
		doubleRow = ((lat - rect.bottom)/ (fabs(rect.top - rect.bottom)) * (m_imageHeight - 1)) + 0.0000001;
		col = (int)doubleCol;
		row = (int)doubleRow;

		if (col < 0) col = 0;
		if (col > m_imageWidth - 1) col = m_imageWidth - 1;

		if (row < 0) row = 0;
		if (row > m_imageHeight - 1) row = m_imageHeight - 1;


		m_pGrid->SetFValue(col, row, elevValue);

		return true;
	}

	bool Coverage::SetElevationValue(int col, int row, float elevValue)
	{

		m_pGrid->SetFValue(col,row,elevValue);
		return true;
	}

		//
	//Given a lon/lat value, Add to the elevation grid value at that point
	//
	bool Coverage::AddElevationValue(double lon, double lat, float elevValue)
	{
//from the lon/lat value, get the image grid coordinate

		if (m_pGrid==NULL) return false;

		DLine2 line ;
		if ( !(m_pGrid->GetCorners(line,true)) )
		{
			return false;
		}

		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		int row, col;
		if (m_imageWidth <= 0 || m_imageHeight <= 0) return false;


		col = (int)((lon - rect.left) / (fabs(rect.left - rect.right)) * (m_imageWidth - 1) + 0.0000001);
		row = (int)((lat - rect.bottom) / (fabs(rect.top - rect.bottom)) * (m_imageHeight - 1) + 0.0000001);

		if (col < 0) col = 0;
		if (col > m_imageWidth - 1) col = m_imageWidth - 1;

		if (row < 0) row = 0;
		if (row > m_imageHeight - 1) row = m_imageHeight - 1;

		float heightValue = GetElevationFromGridCoordinates(col, row);
		if (heightValue!=NODATA&&heightValue!=SHRT_MIN)
		{
			m_pGrid->SetFValue(col,row,elevValue+heightValue);
		}

		return true;
	}
	bool Coverage::AddElevationValue(int col, int row, float elevValue)
	{
		
		float heightValue=GetElevationFromGridCoordinates(col,row);
		if (heightValue!=NODATA&&heightValue!=SHRT_MIN)
		{
			m_pGrid->SetFValue(col,row,elevValue+heightValue);
		}

		return true;
	}

	bool Coverage::HasElevation(double x, double y)
	{
			DPoint2 point ;
			point.x = x;
			point.y = y;
			float elevValue = m_pGrid->GetFilteredValue(point);
			return (elevValue!=(float)UNKNOWN);
	}

	bool Coverage::HasNoData(double x, double y)
	{
			DPoint2 point ;
			point.x = x;
			point.y = y;
			float elevValue = m_pGrid->GetFilteredValue(point);
			return (elevValue==(float)NODATA);
	}

	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns double.Nan
	//
	float Coverage::GetInterpolatedElevationValueInternal(double x, double y, bool localCS)
		{


			//use GetFilteredValue on elevation grid at a specific point
			
			DPoint2 point;
			point.x = x;
			point.y = y;
			if (!localCS) m_pGrid->TransformPoint(m_projGeo,m_proj,&(point));
			float elevValue = m_pGrid->GetFilteredValue(point);


			if (elevValue==NODATA)
				elevValue = m_maxHeight+1;
			if (elevValue==(float)UNKNOWN)
				elevValue=DEFAULTELEVATION;
			return elevValue;

		}

	void Coverage::GetCurrentCornersAndHeightExtents()
    {
		

		//get min max value of the coverage dataset
		m_pGrid->ComputeHeightExtents();

		m_minHeight = SHRT_MAX;
		m_maxHeight = SHRT_MIN;
		float heightValue;
		for (int col = 0; col < m_imageWidth; col++)
		   for (int row = 0; row < m_imageHeight; row++)
		   {
		       heightValue = GetElevationFromGridCoordinates(col, row);
			   if (heightValue != NODATA && heightValue != UNKNOWN)
			   {
			       if (heightValue > m_maxHeight) m_maxHeight = heightValue;
				   else if (heightValue < m_minHeight) m_minHeight = heightValue;
			   }
		   }
    }


	int Coverage::GetWidthInPixels()
	{
		return m_imageWidth;
	}

	int Coverage::GetHeightInPixels()
	{
		return m_imageHeight;
	}

	///<summary>
	//Get the corners of the elevation grid into an array list
	//[0] - top left lon
	//[1] - top left lat
	//[2] - bottom right lon
	//[3] - bottom right lat
	///</summary>
	void Coverage::GetCorners(System::Collections::ArrayList^ cornerPts, bool geo)
	{
		DLine2 line ;
		m_pGrid->GetCorners(line,geo);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		//top left
		cornerPts->Add( (System::Double) rect.left);
		cornerPts->Add( (System::Double) rect.top);
		//bottom right
		cornerPts->Add( (System::Double) rect.right);
		cornerPts->Add( (System::Double) rect.bottom);
	}
	
	vtElevationGrid* Coverage::MyCropElevationGrid(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight, bool useNewSpacing)

														{


	DPoint2 spacing;
		
	DRECT rect;
	DLine2 line;
	DPoint2 pointBottomLeft;
	DPoint2 pointTopLeft;
	DPoint2 pointTopRight;
	DPoint2 pointBottomRight;
	
	//calculate current spacing
	spacing = m_pGrid->GetSpacingD();
	DPoint2 newSpacing = newGrid->GetSpacingD();
	
	//set the new corners to be the passed extent values
	rect.top = topLeftY; 
	rect.bottom = bottomRightY; 
	rect.left = topLeftX; 
	rect.right = bottomRightX;

	
	pointBottomLeft.x	= rect.left;
	pointBottomLeft.y   = rect.bottom;
	pointTopLeft.x	= rect.left;
	pointTopLeft.y  = rect.top;
	pointTopRight.x = rect.right;
	pointTopRight.y = rect.top;
	pointBottomRight.x = rect.right;
	pointBottomRight.y = rect.bottom;


	line.SetAt(0,pointBottomLeft);
	line.SetAt(1,pointTopLeft);
	line.SetAt(2,pointTopRight);
	line.SetAt(3,pointBottomRight);

	int numCols, numRows;
	
	numCols = 1+ 0.01+ fabs(rect.right-rect.left)/ ((useNewSpacing)?newSpacing.x:spacing.x);
	numRows = 1+ 0.01+ fabs(rect.top-rect.bottom)/ ((useNewSpacing)?newSpacing.y:spacing.y);

	*gridWidth=numCols;
	*gridHeight=numRows;
	
	float xCoord, yCoord;
	DPoint2 pointCoord;
	float elevValue;

	
	
	vtElevationGrid* tempGrid = new vtElevationGrid(rect,numCols,numRows,true,newGrid->GetProjection());
	if (!tempGrid->IsValidGrid())return NULL;
	



	for(int i=0;i<numCols;i++)
	{
		for(int j=0;j<numRows;j++)
		{
			
			pointCoord.x = rect.left + i * (newSpacing.x);
			pointCoord.y = rect.bottom + j * (newSpacing.y);
			//
			//if pointCoord is outside the bounds of newGrid
			//
			if (pointCoord.x<pointTopLeft.x||pointCoord.x>pointBottomRight.x)elevValue=NODATA;
			if (pointCoord.y>pointTopLeft.y||pointCoord.y<pointBottomRight.y) elevValue=NODATA;
			else
			elevValue = newGrid->GetClosestValue(pointCoord);

			if (elevValue==UNKNOWN||elevValue==NODATA)//see if the original grid had value at this point
			{
				elevValue = m_pGrid->GetClosestValue(pointCoord);
				//if it is still NODATA, do nothing...SimioGeo has to handle it there
				//Bharath March 17 2008
				//if (elevValue==NODATA)
				//elevValue=0;
			}
			tempGrid->SetFValue(i,j,elevValue);
		}
	}


	
		return tempGrid;


}

	
	//
	//Crops the current elevation grid to within the bounds specified
	//
	//replaces the existing grid with this resampled grid
	//
	//pass in the topleft and bottom right coordinates in lat lon
	//
	void Coverage::CropElevationGrid(double topLeftX, double topLeftY, double bottomRightX, double bottomRightY)
	{
		int width, height;
		if (m_pGrid==NULL) return;

		//vtElevationGrid* tempGrid = m_pGrid->ResampleElevationGrid(m_pGrid,topLeftX,topLeftY,bottomRightX,bottomRightY,&width,&height);
		vtElevationGrid* tempGrid = TransformAndCrop(m_pGrid,topLeftX,topLeftY,bottomRightX,bottomRightY,&width,&height);
		if (tempGrid==NULL) return;

		// TODO: BHARATH - null reference exception on the next line if no elevation.
		m_imageWidth = width;
		m_imageHeight = height;

		m_pGrid->FreeData();
		m_pGrid = tempGrid;
	}
	vtElevationGrid* Coverage::MyResampleElevationGrid(vtElevationGrid* newGrid, int* gridWidth, int* gridHeight, bool useNewSpacing)
{
	//RESAMPLE newGrid into m_pGrid

	DLine2 line;
	DRECT rect;
	
	DLine2 oldCorners;
	m_pGrid->GetCorners(oldCorners,false);

	DPoint2 pointBottomLeft	= oldCorners.GetAt(0);
	DPoint2 pointTopLeft	= oldCorners.GetAt(1);
	DPoint2 pointTopRight	= oldCorners.GetAt(2);
	DPoint2 pointBottomRight= oldCorners.GetAt(3);

	rect.top = pointTopLeft.y;
	rect.bottom = pointBottomRight.y;
	rect.left = pointTopLeft.x;
	rect.right = pointTopRight.x;

	vtProjection proj = m_pGrid->GetProjection();
	vtProjection newGridProjection = newGrid->GetProjection();

	vtProjection *projGeo;
	projGeo = new vtProjection();
	projGeo->SetGeogCSFromDatum(NAD83);//NAD83

	DPoint2 spacing;
		
	
	//Existing SPACING of the original grid

	
	int widthRef;
	int heightRef;

	m_pGrid->GetDimensions(widthRef,heightRef);
	//calculate current spacing

	spacing.x = fabs(rect.right - rect.left) / (widthRef - 1);
	spacing.y = fabs(rect.top - rect.bottom) / (heightRef - 1);


	DLine2 newCorners;
	newGrid->GetCorners(newCorners, false);
	DRECT newBoundsRect;
	DPoint2 pointBottomLeft1	= newCorners.GetAt(0);
	DPoint2 pointTopLeft1	= newCorners.GetAt(1);
	DPoint2 pointTopRight1	= newCorners.GetAt(2);
	DPoint2 pointBottomRight1= newCorners.GetAt(3);

	//convert the corners from newgrid's coord system to m_pGrid's (mercator)
	//
	m_pGrid->TransformPoint(&newGridProjection,&proj,&pointBottomLeft1);
	m_pGrid->TransformPoint(&newGridProjection,&proj,&pointTopLeft1);
	m_pGrid->TransformPoint(&newGridProjection,&proj,&pointTopRight1);
	m_pGrid->TransformPoint(&newGridProjection,&proj,&pointBottomRight1);

	DPoint2 newSpacing;
	
	int newWidthRef;
	int newHeightRef;
	newGrid->GetDimensions(newWidthRef,newHeightRef);

	newSpacing.x = fabs(pointBottomRight1.x - pointTopLeft1.x) / (newWidthRef - 1);
	newSpacing.y = fabs(pointTopLeft1.y - pointBottomRight1.y) / (newHeightRef - 1);



	//set new X and Y dimensions
	int numCols, numRows;

	//instead of determining which grid's spacing to use based on resolution
	//use the passed boolean useNewSpacing to determine that

	//Bharath Apr 8 2008
	bool useOldGrid= !(useNewSpacing); //(spacing.x<newSpacing.x)?true:false;

	//choose the resolution based on the grid with the higher resolution
	numCols = 1+0.001+((float)fabs(rect.right-rect.left)/(float)((useOldGrid)?spacing.x:newSpacing.x));
	numRows = 1+0.001+((float)fabs(rect.top-rect.bottom)/(float)((useOldGrid)?spacing.y:newSpacing.y));

	*gridWidth = numCols;
	*gridHeight=numRows;

		vtElevationGrid* tempGrid = new vtElevationGrid(rect,numCols,numRows,true,proj);

		if (!tempGrid->IsValidGrid())return NULL;


	float xCoord, yCoord;
	DPoint2 pointCoord;
	float elevValue;
	

	double workingSpacingX, workingSpacingY;
	workingSpacingX=(useOldGrid)?spacing.x:newSpacing.x;
	workingSpacingY=(useOldGrid)?spacing.y:newSpacing.y;

	//proj is the existing grid's projection
	//newGridProjection is newGrid's projection
	//projGeo is geographic projection

	for(int i=0;i<numCols;i++)
	{
		for(int j=0;j<numRows;j++)
		{
			
			//pointCoord is in geo coord system
			pointCoord.x = rect.left + i * workingSpacingX;
			pointCoord.y = rect.bottom + j * workingSpacingY;

			elevValue = (useOldGrid)?m_pGrid->GetFilteredValue(pointCoord):newGrid->GetFilteredValue(pointCoord);
			
			if (elevValue==UNKNOWN || elevValue==NODATA)
			{
				
				elevValue = (useOldGrid)?newGrid->GetFilteredValue(pointCoord):m_pGrid->GetFilteredValue(pointCoord);
				
			}
			tempGrid->SetFValue(i,j,elevValue);
		}
	}


	
		return tempGrid;



}
	

	double Coverage::ResampleElevationGrid(double samplingIntervalInMeters)
	{
		int numRows = GetHeightInPixels();
		int numCols = GetWidthInPixels();
		//current spacing
		DPoint2 spacing = m_pGrid->GetSpacingInDegrees ();

		double currentResolutionInDegrees = spacing.x;
		DPoint2 location = GetWorldCoordinates(numCols/2,numRows/2,numCols,numRows,false,SW);

		double conversion = EstimateDegreesToMeters(location.y);
		double currentResolutionInMeters = currentResolutionInDegrees * conversion;

		double scaleFactor = currentResolutionInMeters/samplingIntervalInMeters;
		if (samplingIntervalInMeters>currentResolutionInMeters)
		numCols = (int)( (double)(numCols*scaleFactor) )  ;
		
		if (samplingIntervalInMeters>currentResolutionInMeters)
		numRows = (int)( (double)(numRows*scaleFactor) )  ;

		//number of rows and columns in the new grid should at least be 3x3 to 
		//have the semblance of a grid
		//
		if (numCols<32)
		{
			numCols=32;
			scaleFactor= (double)32/(double)GetWidthInPixels();
			numRows = (int)( (double)(GetHeightInPixels()*scaleFactor) )  ;
		}
		else if (numRows<32)
		{
			numRows=32;
			scaleFactor= (double)32/(double)GetHeightInPixels();
			numCols = (int)( (double)(GetWidthInPixels()*scaleFactor) )  ;
		}

		ResampleElevationGrid(numRows,numCols);
		if (scaleFactor>1)scaleFactor=1;
		return scaleFactor;
	}

	//
	// Earth's diameter: 12756 km
	// approximate circumference: 40074 km
	// each degree of latitude: 111.3 km
	//
	#define EARTH_RADIUS		6378000.0f	// in meters
	#define METERS_PER_LATITUDE	111317.1f
	#define PId	3.14159265358979323846264338

	/**
	 * Determine an approximate conversion from degrees of longitude to meters,
	 * given a latitude in degrees.
	 */
	double Coverage::EstimateDegreesToMeters(double latitude)
	{
		// estimate meters per degree of longitude, using the terrain origin
		double r0 = EARTH_RADIUS * cos(latitude / 180.0 * PId);
		double circ = 2.0 * r0 * PId;
		return circ / 360.0f;
	}

	void Coverage::ResampleElevationGrid( int numRows, int numCols)
	{
		DLine2 line ;
		m_pGrid->GetCorners(line,false);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		//preserve aspect ratio?
		float ratio = (float)numCols / (float)m_imageWidth;
		numRows = ratio * m_imageHeight;

		vtElevationGrid* tempGrid = new vtElevationGrid(rect, numCols, numRows, true, *m_proj);
		if (!tempGrid->IsValidGrid())return;

		

		float xCoord, yCoord;
		DPoint2 pointCoord;
		float elevValue;

		int row, col;
		for(int j=0;j<numRows;j++)
		{
			for(int i=0;i<numCols;i++)
			{
				col = (int)((double)i/ratio);
				row = (int)((double)j/ratio);
				elevValue = GetElevationFromGridCoordinates(col,row);
				//Leave the elevation value as is when it has NO DATA
				//if (elevValue==NODATA)elevValue=0;
				tempGrid->SetFValue(i,(numRows-j-1),elevValue);
			}
		}

		//replace the existing grid
		m_pGrid->FreeData();
		m_pGrid = tempGrid;

		m_imageWidth = numCols;
		m_imageHeight = numRows;
	}

	void Coverage::ResampleElevationGrid( vtElevationGrid* newGrid)
	{
		int width, height;
		vtElevationGrid* tempGrid = /*m_pGrid->*/MyResampleElevationGrid(newGrid,&width,&height,USENEWGRIDSPACING);//MyResample(newGrid,&width,&height); //
		m_imageWidth = width;
		m_imageHeight = height;

		if (tempGrid!=NULL)
		{
			m_pGrid->FreeData();
			m_pGrid = tempGrid;
		}
	}

	vtElevationGrid* Coverage::TransformAndCrop(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight)
{
			vtProjection oldProj = m_pGrid->GetProjection();
			vtProjection newProj = newGrid->GetProjection();

			vtProjection *projGeo;
			projGeo = new vtProjection();
			projGeo->SetGeogCSFromDatum(NAD83);//NAD83

			DPoint2 newTopLeft;
			//transform topleft corner of the cropping rectangle
			//from geog coord system to whatever the new grid is in
			newTopLeft.x = topLeftX; newTopLeft.y = topLeftY;

			newGrid->TransformPoint(projGeo,&newProj,&newTopLeft);

			//transform bottom right
			DPoint2 newBottomRight;
			newBottomRight.x = bottomRightX; newBottomRight.y = bottomRightY;
			newGrid->TransformPoint(projGeo,&newProj,&newBottomRight);

			int newWidth, newHeight;
			//crop the new grid to the extents of the existing grid
			//this is so that LARGE files can be read in and transformed
			//to the GEOG COORD SYSTEM relatively quickly

			//last passed parameter says to use the spacing of the new grid
			vtElevationGrid* tempGrid= newGrid->CropElevationGrid(newGrid,newTopLeft.x,newTopLeft.y,newBottomRight.x,newBottomRight.y,&newWidth,&newHeight, USENEWGRIDSPACING);
			
			*gridWidth = newWidth;
			*gridHeight = newHeight;
			return tempGrid;



}

	

	void Coverage::SetCorners(vtElevationGrid* newGrid, DLine2* corners)
	{
		DLine2 line ;
		newGrid->GetCorners(line,true);
		int size = line.GetSize();

		corners->SetAt(0,line.GetAt(0));
		corners->SetAt(1,line.GetAt(1));
		corners->SetAt(2,line.GetAt(2));
		corners->SetAt(3,line.GetAt(3));
	}

	///<summary>
	///Actually reproject the new grid to the coordinate system of the present grid
	///</summary>
	bool Coverage::TransformCoords(vtElevationGrid** newGrid)
{

	vtProjection proj_new = (*newGrid)->GetProjection();
	vtProjection origProj = *m_proj;

			char *str1, *str2;
			origProj.exportToProj4(&str1);
			proj_new.exportToProj4(&str2);


	if (origProj == proj_new)
		return true; // No conversion necessary

	bool success = false;
	if (newGrid)
	{
		// Check to see if the projections differ *only* by datum
		vtProjection test = *m_proj;
		test.SetDatum(proj_new.GetDatum());
		if (test == proj_new)
		{
			success = (*newGrid)->ReprojectExtents(proj_new);
		}
		else
		{
			bool bUpgradeToFloat = true;

			if (!(*newGrid)->IsFloatMode())
			{

						bUpgradeToFloat = true;
			}

			// actually re-project the grid elements
			vtElevationGrid *grid_new = new vtElevationGrid;

			success = grid_new->ConvertProjection(*newGrid, origProj,
				bUpgradeToFloat, NULL/*progress_callback*/);

			if (success)
			{
				delete *newGrid;
				*newGrid = grid_new;
			}
			else
			{
				//ERROR
				delete grid_new;
			}
		}
	}

	return success;
}



}
