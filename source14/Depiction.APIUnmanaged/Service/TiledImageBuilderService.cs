﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Depiction.API;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using PixelFormat=System.Drawing.Imaging.PixelFormat;
using Point = System.Windows.Point;
using Size=System.Windows.Size;

namespace Depiction.APIUnmanaged.Service
{
    /// <summary>
    /// Class to build an image from a set of tiles. 
    /// </summary>
    public static class TiledImageBuilderService
    {
        public static string GetTiledImage(IMapCoordinateBounds boundingBox, List<TileModel> tiles, Size totalSize,string requestedFileName)
        {
            var bitmap = new Bitmap((int)totalSize.Width, (int)totalSize.Height, PixelFormat.Format32bppRgb);

            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                var bbTl = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(boundingBox.TopLeft);
                var bbBr = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(boundingBox.BottomRight);
                Point bbWH = new Point(bbBr.X - bbTl.X, bbBr.Y - bbTl.Y);
                var scaleX = totalSize.Width / bbWH.X;
                var scaleY = totalSize.Height / bbWH.Y;
                foreach (var tile in tiles)
                {
                    Point tlPx = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tile.TopLeft);
                    Point diffTl = new Point(tlPx.X - bbTl.X, tlPx.Y - bbTl.Y);

                    Point brPx = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(tile.BottomRight);
                    Point diffBr = new Point(brPx.X - bbTl.X, brPx.Y - bbTl.Y);
                    // need integer pixel boundaries for things to work right
                    // ie.  not show black bands between some tile boundaries.
                    diffTl.X = Math.Floor(diffTl.X * (float)scaleX);
                    diffTl.Y = Math.Floor(diffTl.Y * (float)scaleY);
                    diffBr.X = Math.Ceiling(diffBr.X * (float)scaleX);
                    diffBr.Y = Math.Ceiling(diffBr.Y * (float)scaleY);

                    var twidth = (float)(diffBr.X - diffTl.X);
                    var theight = (float)(diffBr.Y - diffTl.Y);

                    using (var memoryStream = new MemoryStream(tile.Image))
                    {
                        using (var tileImage = new Bitmap(memoryStream))
                        {
                            graphics.DrawImage(tileImage, (float) diffTl.X, (float) diffTl.Y, twidth, theight);
                        }
                    }
                }
            }
            var filePath = Path.Combine(DepictionAccess.PathService.TempFolderRootPath,"TempImages");
            var filename = Path.Combine(filePath, requestedFileName);
            if(File.Exists(filename))
            {
                File.Delete(filename);
            }
            ImageSaveLoadServiceNonWPF.SaveBitmapImageToFileName(bitmap, filename);
            //var bitmapImage = new BitmapImage(new Uri(filename));
            //File.Delete(filename);
            return filename;

        }
    }
}
