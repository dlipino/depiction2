using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.WebDataInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.ExtensionMethods;

namespace Depiction.APIUnmanaged.Service
{
    public class WmsElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        private string title = "WMS Source";
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            if (args.ContainsKey("name"))
            {
                title = args["name"].ToString();
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var area = parameters["Area"] as IMapCoordinateBounds;
            if (area == null) return null;
            var topLeft = area.TopLeft;
            var bottomRight = area.BottomRight;
            string requestName = "WMS source";
            if (parameters.ContainsKey("name"))
                requestName = parameters["name"].ToString();
            string layerTitle = "";
            if (parameters.ContainsKey("name"))
                layerTitle = parameters["name"].ToString();

            string url = "";
            if (parameters.ContainsKey("url"))
                url = parameters["url"].ToString();

            string layerName = "";
            if (parameters.ContainsKey("layerName"))
                layerName = parameters["layerName"].ToString() ?? "";

            string imageFormat = "image/png";
            if (parameters.ContainsKey("format"))
            {
                imageFormat = parameters["format"].ToString();
                if (string.IsNullOrEmpty(imageFormat))
                {
                    imageFormat = "image/png";
                }
            }

            string imageStyle = "";
            if (parameters.ContainsKey("style"))
                imageStyle = parameters["style"].ToString() ?? "";

            var elementType = "Depiction.Plugin.Image";
            if (parameters.ContainsKey("elementType"))
            {
                elementType = parameters["elementType"].ToString() ?? "Depiction.Plugin.Image";
                if (elementType.Contains("AutoDetect"))
                {
                    elementType = "Depiction.Plugin.Image";
                }
            }

            string thumbnailFilename = "";
            //            Image thumbnailImage = null;
            //            try
            //            {
            //                thumbnailImage = ReadBitmap(response.Data.DataFilename);
            //            }
            //            catch (Exception)
            //            {
            //
            //            }
            //            Image thumbnailImage = ReadBitmap(thumbnailFilename);
            //TODO: have to get thumbnail image, if available from a url

            var dataProvider = new WmsDataProvider(url, layerName, area, imageStyle, imageFormat, layerTitle,
                                                   elementType, thumbnailFilename);
            Response data = dataProvider.GetData();
            if (data.IsRequestSuccessful)
            {
                //data.ResponseFile contains the image retrieved
                var fileName = data.ResponseFile;
                UpdateStatusReport(string.Format("Getting data from downloaded file:{0}.", Path.GetFileName(fileName)));

                var element = CreateImageElement(fileName, elementType, topLeft, bottomRight);
                if (element != null)
                {
                    if (parameters.ContainsKey("Tag"))
                    {
                        var tag = parameters["Tag"].ToString();
                        element.Tags.Add(tag);
                    }else
                    {
                        element.Tags.Add(title);
                    }
                    return new List<IDepictionElement> { element };
                }
            }
            return string.Format("Could not find data for {0}", requestName);
        }

        protected override void ServiceComplete(object args)
        {
            var message = args as string;
            if (message != null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(message, 4);
            }

            var elements = args as List<IDepictionElement>;
            if (elements == null)
            {

                return;
            }

            if (elements.Count > 0 && DepictionAccess.CurrentDepiction != null)
            {
                //                UpdateStatusReport("Adding elements to depiction");
                DepictionAccess.CurrentDepiction.CreateAndAddRevealer(elements, title, null);
                //                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, true);
            }
        }

        #endregion

        private IDepictionElement CreateImageElement(string fileName, string elementType,
                                                         ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(elementType);
            var elementModel = ElementFactory.CreateElementFromPrototype(prototype);
            var element = elementModel;
            string partialName;

            var success = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fileName, out partialName);
            if (success)
            {
                var metadata = new DepictionImageMetadata(partialName, topLeft, bottomRight, "Web");
                metadata.CanBeManuallyGeoAligned = false;//For now anything coming from quickstart can not have its position changed.
                element.SetImageMetadata(metadata);
                element.SetPropertyValue("DisplayName", title);
            }
            return element;
        }
    }
}