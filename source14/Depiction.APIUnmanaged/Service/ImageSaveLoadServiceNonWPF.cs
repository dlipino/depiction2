using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using Depiction.API.ExceptionHandling;

namespace Depiction.APIUnmanaged.Service
{
    public class ImageSaveLoadServiceNonWPF
    {
        static public bool DownloadAndSaveImage(string url, string finalImageName, int imageWidth, int imageHeight)
        {
            var image = DownloadImage(url, imageWidth, imageHeight);
            if (image == null) return false;
            return SaveBitmapImageToFileName(image, finalImageName);
        }

        static public bool SaveBitmapImageToFileName(Bitmap bitmapImage, string fullImageName)
        {
            var dir = Path.GetDirectoryName(fullImageName);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            try
            {
                if (File.Exists(fullImageName))
                {
                    return true;
                }
                var ext = Path.GetExtension(fullImageName);
                if(ext.ToLowerInvariant().Contains("png"))
                {
                    bitmapImage.Save(fullImageName, ImageFormat.Png);
                }else
                {
                    bitmapImage.Save(fullImageName, ImageFormat.Jpeg); 
                }
            }
            catch (ExternalException ex)
            {
                // Log it and forget it ... this is an odd GDI+ error
                DepictionExceptionHandler.HandleException(ex, false,true);
                return false;
            }
            catch
            {
                return false;
            }
            return true;
        }
        public static Image ReadBitmapFile(string filename)
        {
            if (string.IsNullOrEmpty(filename)) return null;
            Image bitmap = Image.FromFile(filename);
            return bitmap;
        }
        static public Bitmap DownloadImage(string url, int imageWidth, int imageHeight)
        {
            var bmpCrop = new Bitmap(imageWidth, imageHeight);
            var r = new Rectangle(0, 0, imageWidth, imageHeight);

            var wb = new WebClient();
            byte[] baImage = wb.DownloadData(url);

            //if (wb.ResponseHeaders["Content-Type"] == "text/html" || (baImage.Length == 8321 && layers.Length > (currentLayer + 1)))
            //    throw new InvalidZoomException();

            using (var ms = new MemoryStream(baImage))
            {
                Image tile = Image.FromStream(ms, true, true);

                using (Graphics graphics = Graphics.FromImage(bmpCrop))
                {
                    graphics.DrawImage(tile, r, r, GraphicsUnit.Pixel);
                }
            }

            return bmpCrop;
        }

    }
}