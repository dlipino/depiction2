﻿using System;
using System.IO;
using OSGeo.GDAL;
using OSGeo.OGR;

namespace Depiction.APIUnmanaged
{
    /// <summary>
    /// Borrowed from https://bitbucket.org/bjartn/gdalgonewild/overview.  Sets up GDAL environment so we can use it in C#.
    /// </summary>
    public class GdalEnvironment
    {
        public static void SetupEnvironment()
        {
            SetEnvironmentVariables();
            Gdal.AllRegister();
            Ogr.RegisterAll();
        }
        //Option to unregister? for testing?
        private static void SetEnvironmentVariables()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            
            Gdal.SetConfigOption("GDAL_DATA", Path.Combine(path, @"gdal-data"));
            Gdal.SetConfigOption("GDAL_DRIVER_PATH", Path.Combine(path, @"gdal-plugins"));
            Gdal.SetConfigOption("PROJ_LIB", Path.Combine(path, @"proj\SHARE"));
            Gdal.SetConfigOption("GEOTIFF_CSV", Path.Combine(path, @"gdal-data"));

            SetValueNewVariable("GEOTIFF_CSV", Path.Combine(path, @"gdal-data"));
            SetValueNewVariable("GDAL_DRIVER_PATH", Path.Combine(path, @"gdal-plugins"));
            //Is this one even used by GDAL, it was not in the build for a while and things still seemed to work
            //I kind of wonder how these files are used
            SetValueNewVariable("PROJ_LIB", Path.Combine(path, @"proj\SHARE"));
        }

        private static void SetValueNewVariable(string name, string value)
        {
            //if (Environment.GetEnvironmentVariable(name) == null)
                Environment.SetEnvironmentVariable(name, value);
        }
    }
}
