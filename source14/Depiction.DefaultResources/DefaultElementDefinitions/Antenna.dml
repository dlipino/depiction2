﻿<depictionElement elementType="Depiction.Plugin.Antenna" displayName="Antenna" >
  <properties>
    <property name="Author" displayName="Author" value="Depiction, Inc." typeName="System.String" editable="false" deletable="false" />
    <property name="Description" displayName="Description" value="This element calculates an arc or circle of what is visible from a given antenna height above ground level." typeName="System.String" editable="False" deletable="False" />
    <property name="Transmit" displayName="Transmit" value="False" typeName="System.Boolean" />
    <property name="Receive" displayName="Receive" value="False" typeName="System.Boolean" />
    <property name="Frequency" displayName="Frequency (MHz)" value="0" typeName="System.Double" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Frequency must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Wattage" displayName="Power (watts)" value="0" typeName="System.Double" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Power must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Range" displayName="Maximum distance" value="2 miles" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType"/>
            <parameter value="This value must be a distance." type="System.String"/>
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 miles" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Height" displayName="Height above ground level" value="30 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType"/>
            <parameter value="Height must be a distance." type="System.String"/>
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Orientation" displayName="Centerline direction in degrees (0=north)" value="0" deleteIfEditZoi="true" typeName="System.Double" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Orientation angle must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
            <parameter value="360" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Width" displayName="Field of view in degrees" value="360" deleteIfEditZoi="true" typeName="System.Double" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Field of view must be between 1 and 360, inclusive." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="1" type="System.Double" />
            <parameter value="360" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Resolution" displayName="Horizontal sampling (larger => faster)" value="250 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType"/>
            <parameter value="Horizontal sampling must be a distance." type="System.String"/>
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="HeightTolerance" displayName="Vertical accuracy" value="0 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API" visible="False" >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType"/>
            <parameter value="Vertical accuracy must be a distance." type="System.String"/>
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Notes" displayName="Additional notes" typeName="System.String" value="" />
    <property name="ZOIFill" displayName="Fill color" value="#AA9900CC" typeName="System.Windows.Media.Color" deletable="false"  />
    <property name="ZOIBorder" displayName="Outline color" value="#00000000" typeName="System.Windows.Media.Color" deletable="false" />
    <property name="ZOIShapeType" displayName="ZOI shape type" typeName="ZOIShapeType" value="Polygon" editable="False" deletable="False" visible="False" />
    <property name="IconPath" displayName="Icon path" value="embeddedresource:Depiction.Tower" typeName="System.String" deletable="False"/>
    <property name="ShowElementPropertiesOnCreate"  value="True" typeName="System.Boolean" editable="False" visible="False" />
    <property name="Dependencies" displayName="Dependencies" value="Elevation" typeName="System.String" editable="False" visible="False" />
    <property name="Draggable" displayName="Allow dragging" value="True" typeName="System.Boolean" deletable="false" />
    <property name="PlaceableWithMouse"  value="true" editable="false" typeName="System.Boolean" visible="false" />
  </properties>
</depictionElement>