﻿<depictionElement elementType="Depiction.Plugin.SurveillanceCamera" displayName="Surveillance camera" >
	<properties>
    <property name="Author" displayName="Author" value="Depiction, Inc." typeName="System.String" editable="false" deletable="false" />
		<property name="Description" displayName="Description" value="Show the coverage of a camera by setting its range, orientation and field of view." typeName="System.String" editable="False" deletable="False" />
		<property name="Radius" displayName="Nominal range" value="1000 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API"  >
			<postSetActions>
				<action name="CreateArc">
					<parameters>
						<parameter elementQuery=".Radius" />
						<parameter elementQuery=".Orientation"/>
						<parameter elementQuery=".Width"/>
					</parameters>
				</action>
			</postSetActions>
			<validationRules>
				<validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
					<parameters>
						<parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType"/>
						<parameter value="Radius must be a distance." type="System.String"/>
					</parameters>
				</validationRule>
				<validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
					<parameters>
						<parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
						<parameter value="1000000 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
					</parameters>
				</validationRule>
			</validationRules>
		</property>
		<property name="Orientation" displayName="Centerline orientation in degrees"  value="270 degrees" typeName="Angle"   >
			<postSetActions>
				<action name="CreateArc">
					<parameters>
						<parameter elementQuery=".Radius" />
						<parameter elementQuery=".Orientation"/>
						<parameter elementQuery=".Width"/>
					</parameters>
				</action>
			</postSetActions>
			<validationRules>
				<validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
					<parameters>
						<parameter value="System.Double" type="System.RuntimeType" />
						<parameter value="Orientation angle must be a number." type="System.String" />
					</parameters>
				</validationRule>
				<validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
					<parameters>
						<parameter value="0" type="System.Double" />
						<parameter value="360" type="System.Double" />
					</parameters>
				</validationRule>
			</validationRules>
		</property>
		<property name="Width" displayName="Field of view in degrees" value="120" deleteIfEditZoi="true" typeName="System.Double"  >
			<postSetActions>
				<action name="CreateArc">
					<parameters>
						<parameter elementQuery=".Radius" />
						<parameter elementQuery=".Orientation"/>
						<parameter elementQuery=".Width"/>
					</parameters>
				</action>
			</postSetActions>
			<validationRules>
				<validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
					<parameters>
						<parameter value="System.Double" type="System.RuntimeType" />
						<parameter value="Stop angle must be a number." type="System.String" />
					</parameters>
				</validationRule>
				<validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
					<parameters>
						<parameter value="0" type="System.Double" />
						<parameter value="360" type="System.Double" />
					</parameters>
				</validationRule>
			</validationRules>
		</property>
		<property name="Notes" displayName="Additional notes" typeName="System.String" value="" />
		<property name="Draggable" displayName="Allow dragging" value="True" typeName="System.Boolean" deletable="false" />
		<property name="ZOIFill" displayName="Fill color" value="#AAFF8C00" typeName="System.Windows.Media.Color" deletable="false" />
    		<property name="ZOIBorder" displayName="Outline color" value="#00000000" typeName="System.Windows.Media.Color" deletable="false" />		
		<property name="IconPath" displayName="Icon path" value ="embeddedresource:Depiction.SurveillanceCamera" typeName="System.String" deletable="False"/>
		<property name="ShowElementPropertiesOnCreate"  value="True" typeName="System.Boolean" editable="False" visible="False" />
		<property name="PlaceableWithMouse"  value="true" editable="false" typeName="System.Boolean" visible="false" />
	</properties>
  <generateZoi>
    <actions>
      <action name="CreateArc">
        <parameters>
          <parameter elementQuery=".Radius" />
          <parameter elementQuery=".Orientation"/>
          <parameter elementQuery=".Width"/>
        </parameters>
      </action>
    </actions>
  </generateZoi>
</depictionElement>