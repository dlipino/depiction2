﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Authentication;
using System.ServiceModel;
using System.Text;

namespace Depiction.WebServices
{
    // NOTE: If you change the interface name "IDepictionWebServiceMVVM" here, you must also update the reference to "IDepictionWebServiceMVVM" in Web.config.
    [ServiceContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public interface IDepictionWebServiceMVVM
    {
        [OperationContract]
        string[] GetRegionCodesForRegion(BoundingBox parameters);

        [OperationContract]
        QuickstartData[] GetQuickstartSourcesByRegionCodes(string depictionVersion, string[] regionCodes);

        [OperationContract]
        bool IsProductSNValid(string serialNumber, int productId);

        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        Int32 BeginPublishToWeb(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract]
        bool FinalizePublishToWeb(Int32 webDepictionId);

        [OperationContract]
        void PublishWebDepictionChunk(Int32 webDepictionId, byte[] fileChunk);

        [OperationContract]
        void PublishDPNChunk(Int32 webDepictionId, byte[] fileChunk);
    }
}
