﻿using System.ServiceModel;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    // NOTE: If you change the interface name "IDepictionWebServiceV1M1" here, you must also update the reference to "IDepictionWebServiceV1M1" in Web.config.
    [ServiceContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public interface IDepictionWebServiceV1M3
    {
        [OperationContract(Name = "GetRegionCodesForRegion")]
        string[] GetRegionCodesForRegion(BoundingBox region);

        [OperationContract(Name = "GetQuickstartSourcesByRegionCodes")]
        QuickstartDataV1M0[] GetQuickstartSourcesByRegionCodes(string[] regionCodes, string[] gatherersInstalled);

        [OperationContract(Name = "GeocodeLocation")]
        LatitudeLongitude GeocodeLocation(string locationToGeocode);

        [OperationContract(Name = "SearchRemoteRepositoryForAddinNotFoundLocally")]
        AddinInformationV1M0 SearchRemoteRepositoryForAddinNotFoundLocally(string addinName, AddinType addinType);
    }
}