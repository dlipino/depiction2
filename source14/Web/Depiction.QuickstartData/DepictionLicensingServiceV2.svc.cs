﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Model;
using NHibernate.Linq;

namespace Depiction.WebServices
{
    public class DepictionLicensingServiceV2 : IDepictionLicensingServiceV2
    {
        #region IDepictionLicensingService Members

        public TrialLicenseResult GetTrialLicense(string activationCode)
        {
            var licenseResult = new TrialLicenseResult { Valid = false, ActivationCode = activationCode };

            try
            {
                TrialRegistration registration;

                using (var session = DBFactory.GetSession())
                {
                    registration = Enumerable.SingleOrDefault(session.Linq<TrialRegistration>(), r => r.ActivationCode == activationCode.ToUpper());

                    if (registration == null)
                        throw new Exception("There were no registrations found matching that activation code.");
                    if (registration.ActivationCount > 2)
                        throw new Exception("That activation code has already been used to activate the trial version three times. Please register for the trial to obtain a new registration code.");

                    licenseResult.LicenseKey = getHash(registration.ActivationCode);
                    licenseResult.Valid = true;

                    registration.Activated = true;
                    registration.LastActivationDate = DateTime.Now;

                    if (registration.ActivationCount == 0)
                        registration.FirstActivationDate = DateTime.Now;

                    registration.ActivationCount++;

                    using (var transaction = session.BeginTransaction())
                    {
                        session.Update(registration);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                licenseResult.Valid = false;
                licenseResult.ErrorMessage = ex.Message;
            }

            return licenseResult;
        }

        #endregion

        /// <summary>
        /// This method needs to be the same as the web service
        /// </summary>
        private static byte[] getHash(string trialKey)
        {
            var encoding = new ASCIIEncoding();
            byte[] keyBits = encoding.GetBytes(trialKey);

            SHA512 sha512 = new SHA512Managed();
            return sha512.ComputeHash(keyBits);
        }
    }
}