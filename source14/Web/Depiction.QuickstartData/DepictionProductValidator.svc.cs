﻿using Depiction.WebServices.CheckpointService;

namespace Depiction.WebServices
{
    // NOTE: If you change the class name "DepictionProductValidator" here, you must also update the reference to "DepictionProductValidator" in Web.config.
    public class DepictionProductValidator : IDepictionProductValidator
    {
        #region IDepictionProductValidator Members

        public bool IsProductSNValid(string serialNumber, int productID)
        {
            var checkpointService = new CheckPointServiceClient();
            var record = new TCheckPointSNSerialRecord();
            TRecordResponse response = checkpointService.GetSerialNoRecordBySN(serialNumber, productID, ref record);
            return response.Success;
        }

        #endregion
    }
}