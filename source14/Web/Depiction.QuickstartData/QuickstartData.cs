﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Depiction.WebServices
{
    [DataContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public class QuickstartData
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DataFacade { get; set; }
        [DataMember]
        public string Description { get; set; }
        //[DataMember]
        //public string URL { get; set; }
        [DataMember]
        public Dictionary<string, string> Parameters { get; set; }
        [DataMember]
        public string ElementType { get; set; }
    }
}