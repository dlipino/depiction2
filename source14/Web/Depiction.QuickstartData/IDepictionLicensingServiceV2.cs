﻿using System.ServiceModel;

namespace Depiction.WebServices
{
    [ServiceContract(Namespace = "http://request.depiction.info/DepictionLicensingService")]
    public interface IDepictionLicensingServiceV2
    {
        [OperationContract(Name = "GetTrialLicense")]
        TrialLicenseResult GetTrialLicense(string activationCode);
    }
}