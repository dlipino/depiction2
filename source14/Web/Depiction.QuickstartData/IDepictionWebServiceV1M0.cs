﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    [ServiceContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public interface IDepictionWebServiceV1M0
    {
        [OperationContract(Name = "GetRegionCodesForRegion")]
        string[] GetRegionCodesForRegion(BoundingBox region);

        [OperationContract(Name = "GetQuickstartDataByRegionCodes")]
        QuickstartDataV1M0[] GetQuickstartDataByRegionCodes(string[] regionCodes);

        [OperationContract(Name = "GeocodeLocation")]
        LatitudeLongitude GeocodeLocation(string locationToGeocode);

        [OperationContract(Name = "SearchAddinRepository")]
        AddinInformationV1M0 SearchAddinRepository(string addinName, AddinType addinType);
    }

    [DataContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public class QuickstartDataV1M0
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DataFacade { get; set; }

        [DataMember]
        public Dictionary<string, string> Parameters { get; set; }

        [DataMember]
        public string ElementType { get; set; }
    }

    [DataContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public class AddinInformationV1M0
    {
        [DataMember]
        public string FriendlyName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string AddinDownloadLocation { get; set; }

        [DataMember]
        public string[] DependencyLocations { get; set; }

        [DataMember]
        public AddinType AddinType { get; set;}
    }
}