﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Depiction.WebServices
{
    // NOTE: If you change the interface name "IDepictionProductValidator" here, you must also update the reference to "IDepictionProductValidator" in Web.config.
    [ServiceContract]
    public interface IDepictionProductValidator
    {
        [OperationContract]
        bool IsProductSNValid(string serialNumber, int productID);
    }
}
