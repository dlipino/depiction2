﻿namespace Depiction.Marketing.DomainModel.Model
{
    public class Role
    {
        public virtual int Id { get; set; }
        public virtual string RoleName { get; set; }
    }   
}