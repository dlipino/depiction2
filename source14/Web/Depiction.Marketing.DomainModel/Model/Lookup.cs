﻿namespace Depiction.Marketing.DomainModel.Model
{
    public class Lookup
    {
        public virtual int Id { get; set; }
        public virtual LookupCategory LookupCategory { get; set; }
        public virtual string Value { get; set; }
        public virtual int Status { get; set; }
    }
}