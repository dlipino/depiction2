﻿using System;
using System.Collections.Generic;

namespace Depiction.Marketing.DomainModel.Model
{
    public class WebDepiction
    {
        public WebDepiction()
        {
            Tags = new List<DepictionTag>();
            Comments = new List<WebDepictionComment>();
        }

        public virtual Int32 Id { get; private set; }
        public virtual User User { get; set; }
        public virtual String DepictionTitle { get; set; }
        public virtual String Description { get; set; }
        public virtual IList<DepictionTag> Tags { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual IList<WebDepictionComment> Comments { get; set; }
        public virtual String Filename { get; set; }
    }
}