﻿using System;
using System.Collections.Generic;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.Marketing.DomainModel.Model
{
    public class AddinInformation
    {
        public virtual Int32 Id { get; private set; }
        public virtual string FriendlyName { get; set; }
        public virtual string Name { get; set; }
        public virtual string AddinDownloadLocation { get; set; }
        public virtual AddinType AddinType { get; set; }
        public virtual IList<DepictionVersion> ValidWithVersions { get; set; }
        public virtual IList<RegionDefinition> Regions { get; set; }
        public virtual IList<QuickstartData> RelatedQuickstartItems { get; set; }
    }
}