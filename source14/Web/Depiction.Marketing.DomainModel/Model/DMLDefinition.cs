﻿using System.Collections.Generic;

namespace Depiction.Marketing.DomainModel.Model
{
    public class DMLDefinition
    {
        public virtual int Id { get; private set; }
        public virtual string DMLDisplayName { get; set; }
        public virtual string DMLTypeName { get; set; }
        public virtual string DMLDownloadPath { get; set; }
        public virtual IList<DMLProperty> Properties { get; set; }
    }
}