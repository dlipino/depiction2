﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class QuickstartParameter
    {
        public virtual Int32 Id { get; set; }
        public virtual string ParameterName { get; set; }
        public virtual string ParameterValue { get; set; }
    }
}