﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class UsageMetric
    {
        public virtual int Id { get; set; }
        public virtual Person Person { get; set; }
        public virtual DateTime DateLogged { get; set; }
        public virtual Lookup Metric { get; set; }
        public virtual string Details { get; set; }
    }
}