﻿namespace Depiction.Marketing.DomainModel.Model
{
    public class Person
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
    }
}