﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class DepictionVersion
    {
        public virtual Int32 Id { get; set; }
        public virtual string FriendlyName { get; set; }
        public virtual string VersionNumber { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (DepictionVersion)) return false;
            return ((DepictionVersion) obj).Id == Id;
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}