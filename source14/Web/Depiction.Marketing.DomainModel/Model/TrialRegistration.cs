﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class TrialRegistration
    {
        public virtual int Id { get; set; }
        public virtual Person Registrant { get; set; }
        public virtual string Organization { get; set; }
        public virtual Lookup MarketSegment { get; set; }
        public virtual string AdditionalInformation { get; set; }
        public virtual string ActivationCode { get; set; }
        public virtual bool Activated { get; set; }
        public virtual DateTime RegistrationDate { get; set; }
        public virtual DateTime FirstActivationDate { get; set; }
        public virtual DateTime LastActivationDate { get; set; }
        public virtual int ActivationCount { get; set; }
        public virtual bool Purchased { get; set; }
    }
}