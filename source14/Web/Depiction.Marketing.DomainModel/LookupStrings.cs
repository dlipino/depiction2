﻿using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.Marketing.DomainModel
{
    public static class LookupStrings
    {
        public static string GetMetricTypeString(MetricType metricType)
        {
            switch (metricType)
            {
                case MetricType.ExampleOpened:
                    return "Example Opened";
                case MetricType.Launched:
                    return "Depiction Launched";
                case MetricType.MenuClicked:
                    return "Menu Clicked";
                case MetricType.MinutesRan:
                    return "Minutes Ran";
                case MetricType.Saved:
                    return "Depiction Saved";
                case MetricType.OperatingSystem:
                    return "Operating System";
                case MetricType.FileExtension:
                    return "File Extension";
                case MetricType.SpecificAction:
                    return "Specific Action";
            }

            return string.Empty;
        }
    }
}