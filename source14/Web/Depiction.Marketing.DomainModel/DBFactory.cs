﻿using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.Model;
using FluentNHibernate;
using FluentNHibernate.AutoMap;
using FluentNHibernate.Cfg;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace Depiction.Marketing.DomainModel
{
    public static class DBFactory
    {
        private const string server = "localhost";
        private const string database = "depictionmarketing";
        private const string uid = "dmarket";
        private const string password = "3305Tango";

        private static readonly string dpconnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};", server, database,uid, password);

        private static ISessionFactory sessionFactory;

        public static ISession GetSession()
        {
            if (sessionFactory == null)
            {
                Configuration configuration = GetConfiguration();
                sessionFactory = configuration.BuildSessionFactory();
            }

            return sessionFactory.OpenSession();
        }

        public static void UpdateDatabase()
        {
            Configuration configuration = GetConfiguration();
            var updater = new SchemaUpdate(configuration);
            updater.Execute(false, true);
            sessionFactory = null;
        }

        private static Configuration GetConfiguration()
        {
            var configuration = new Configuration();
            MySQLConfiguration.Standard.ConnectionString.Is(dpconnectionString).ConfigureProperties(configuration);
            configuration.AddAutoMappings(DBModel);
            return configuration;
        }

        public static AutoPersistenceModel DBModel
        {
            get
            {
                return AutoPersistenceModel.MapEntitiesFromAssemblyOf<QuickstartParameter>()
                    .Where(t => t.Namespace == "Depiction.Marketing.DomainModel.Model")
                    .ForTypesThatDeriveFrom<Person>(map =>
                    {
                        map.Map(p => p.Email).WithLengthOf(500);
                        map.Map(p => p.Title).WithLengthOf(30);
                    })
                    .ForTypesThatDeriveFrom<User>(map =>
                    {
                        map.HasManyToMany<Role>(u => u.Roles).Cascade.All();
                        //map.HasMany<WebDepiction>(u => u.WebDepictions);
                        map.References(u => u.Person).Cascade.All();
                    })
                    .ForTypesThatDeriveFrom<AddinInformation>(map =>
                    {
                        map.HasManyToMany<DepictionVersion>(a => a.ValidWithVersions).Cascade.All();
                        map.HasManyToMany<RegionDefinition>(a => a.Regions).Cascade.All();
                        map.Map(a => a.AddinType).CustomTypeIs(typeof (AddinType));
                    })
                    .ForTypesThatDeriveFrom<QuickstartData>(map =>
                    {
                        map.HasManyToMany<QuickstartParameter>(d => d.Parameters).Cascade.All();
                    })
                    .ForTypesThatDeriveFrom<DMLProperty>(map =>
                    {
                        map.References(p => p.DMLDefinition);
                    })
                    .ForTypesThatDeriveFrom<DMLDefinition>(map =>
                    {
                        map.HasMany<DMLProperty>(d => d.Properties).Cascade.All();
                    })
                    .ForTypesThatDeriveFrom<WebDepictionComment>(map =>
                    {
                        map.References(p => p.WebDepiction);
                    })
                    .ForTypesThatDeriveFrom<WebDepiction>(map =>
                    {
                        map.References(w => w.User);
                        map.HasManyToMany<DepictionTag>(webDepiction => webDepiction.Tags);
                        map.HasMany<WebDepictionComment>(d => d.Comments).Cascade.All();
                    });
            }
        }
    }
}