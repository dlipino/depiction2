﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Depiction.Marketing.DomainModel.ExtensionMethods
{
    public static class StringExtensionMethods
    {
        public static string PasswordEncode(this string st)
        {
            return EncodePassword(st);
        }

        private static string EncodePassword(string originalPassword)
        {
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(originalPassword);
            byte[] encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes);
        }
    }
}
