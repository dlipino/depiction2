﻿namespace Depiction.Marketing.DomainModel.Enum
{
    public enum MetricType
    {
        Saved,
        Launched,
        MinutesRan,
        ExampleOpened,
        MenuClicked,
        OperatingSystem,
        FileExtension,
        SpecificAction
    }
}