﻿namespace Depiction.Marketing.DomainModel.Enum
{
    public enum AddinType
    {
        ElementGatherer,
        Behavior,
        Condition,
        Geocoder,
        TileProvider
    }
}