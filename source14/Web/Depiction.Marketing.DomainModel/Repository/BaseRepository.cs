﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;

namespace Depiction.Marketing.DomainModel.Repository
{
    public class BaseRepository : IRepository, IDisposable
    {
        private readonly ISession session;

        public BaseRepository(ISession session)
        {
            this.session = session;
        }

        public void Delete<T>(T objectToDelete)
        {
            session.Delete(objectToDelete);
        }

        public void Save<T>(T objectToSave)
        {
            session.SaveOrUpdate(objectToSave);
        }

        public T GetOne<T>(int id)
        {
            return session.Get<T>(id);
        }

        public T[] GetAll<T>()
        {
            return session.Linq<T>().ToArray();
        }

        public T[] Search<T>(Expression<Func<T, bool>> expression)
        {
            return session.Linq<T>().Where(expression).ToArray();
        }

        public IQueryable<T> Queryable<T>()
        {
            return session.Linq<T>();
        }

        public ITransaction BeginTransaction()
        {
            return session.BeginTransaction();
        }

        public ISession RepositorySession
        {
            get { return session; }
        }

        public T GetById<T>(int Id)
        {
            return session.Get<T>(Id);
        }

        public void Dispose()
        {
            if (session == null) return;
            session.Close();
            session.Dispose();
        }
    }
}