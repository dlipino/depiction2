﻿using System.Reflection;

// Contains common stuff for every Depiction project.
// Include this (as a link) in every Depiction project.
//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyCompany("Depiction Inc.")]
[assembly: AssemblyProduct("Depiction")]
[assembly: AssemblyCopyright("© 2008-2012 Depiction, Inc. All rights reserved.")]
[assembly: AssemblyTrademark("Depiction is a trademark of Depiction, Inc.")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
//AssemblyInformationalVersion|("1.0.0")]//The product version when you get dll properties major.minor.revision.build
//AssemblyVersion|("1.0.0")]//Controls folder name of user.config, should be major.minor
//AssemblyFileVersion|("1.0.0")]//File version as seen from dll properties major.minor.revision
//THere is some connection to
//\\build-pc\c on build-pc\Program Files\Macrovision\IS2008\Redist\Language Independent\i386\ISP\Setup.exe
//But i don't know what it is exactly

[assembly: AssemblyInformationalVersion("1.0.0")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
