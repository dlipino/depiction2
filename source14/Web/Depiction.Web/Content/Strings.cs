﻿namespace Depiction.Web.Content
{
    public static class Strings
    {
        public static string LastName = "*Last Name";
        public static string Email = "*Email";
        public static string MarketSegment = "*I'm a";
        public static string FirstName = "*First Name";
        public static string Organization = "*Organization";
        public static string AdditionInformation = "Additional<br />Information";
        public static string Title = "Title";
        public static string SelectOne = "-- Select One --";
        public static string Tutorials = "Tutorials";
        public static string Forum = "Forum";
        public static string Webinars = "Webinars";
        public static string Blog = "Blog";
        public static string Samples = "Samples";
        public static string FAQ = "FAQ";
    }
}