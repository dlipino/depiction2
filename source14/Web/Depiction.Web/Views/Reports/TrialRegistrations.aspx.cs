﻿using System;
using System.Web.Mvc;
using Depiction.Web.Helpers;

namespace Depiction.Web.Views.Reports
{
    public class TrialRegistrations : ViewPage<object>
    {
        protected override void OnLoad(EventArgs e)
        {
            ExcelExport.Export(Response, Model, "TrialRegistrations.xls");
        }
    }
}