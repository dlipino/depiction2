﻿using System.Web.Mvc;
using Depiction.Web.Helpers;

namespace Depiction.Web.Views.Reports
{
    public class UsageStatistics : ViewPage<object>
    {
        protected override void OnLoad(System.EventArgs e)
        {
            ExcelExport.Export(Response, Model, "UsageStatistics.xls");
        }
    }
}