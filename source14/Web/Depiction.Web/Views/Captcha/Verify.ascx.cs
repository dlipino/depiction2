﻿using System.Web.Mvc;

namespace Depiction.Web.Views.Captcha
{
    public class Verify : ViewUserControl<VerifyParams> { }
    public class VerifyParams
    {
        public bool Result { get; set; }
    }
}