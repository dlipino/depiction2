﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Depiction.Web.Views.Admin.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="<%= ResolveClientUrl("~/Content/mootools-1.1.js")%>" type="text/javascript"></script>
<script src="<%= ResolveClientUrl("~/Content/sortableTable.js")%>" type="text/javascript"></script>
<script type="text/javascript">
    window.addEvent('domready', function() {
        myTable = new sortableTable('usersTable', { overCls: 'over', onClick: function() { window.location = "/User/Edit/" + this.id } });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Manage users</h1>
<p>Select a user to manage their account settings.</p>
<table id="usersTable" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th axis="string">Name</th>
            <th axis="string">Email</th>
        </tr>
    </thead>
    <tbody>
<%  foreach (var user in ViewData.Model) { %>
        <tr id="<%= user.Id %>">
            <td><%= string.Format("{0}, {1}", user.Person.LastName, user.Person.FirstName) %></td>
            <td><%= string.Format("{0}", user.Person.Email) %></td>
        </tr>
<%  } %>
    </tbody>
</table>
</asp:Content>