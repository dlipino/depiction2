<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>The purchases have been indicated</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>The purchases have been indicated</h2>
    <p>The trial registrants that match the email addresses you entered will no longer receive trial-related emails.</p>
</asp:Content>