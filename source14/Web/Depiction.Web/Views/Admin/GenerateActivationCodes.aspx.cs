﻿using System;
using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Admin
{
    public class GenerateActivationCodes : ViewPage
    {
        protected string form;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new IFormItem[] 
                {
                    new DataFormItem("How many?", new TextHtmlControl("howMany", string.Empty, new ValidationInformation {Validations = new IValidation[] {new NumericValidation(), new RequiredValidation()}})),
                    new ButtonsFormItem(new[] {new SubmitButton("Generate codes")})
                };

            form = FormBuilder.BuildForm<AdminController>(a => a.GeneratedCodes(), "generateForm", formItems);

            base.OnLoad(e);
        }
    }
}