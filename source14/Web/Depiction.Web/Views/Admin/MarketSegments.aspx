﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="MarketSegments.aspx.cs" Inherits="Depiction.Web.Views.Admin.MarketSegments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Manage market segments</h1>
<table cellspacing="0">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>Market segment</th>
            <th>Usages</th>
        </tr>
    </thead>
<%
    foreach (var lookup in Model)
    {
        %>
    <tr>
        <td><a href="<%= ExpressionHelper.GetActionLink<MarketSegmentController>(m => m.Edit(lookup.Id)) %>">edit</a></td>
<%      if (lookup.Status == 0)
        { %>
        <td><a href="<%= ExpressionHelper.GetActionLink<MarketSegmentController>(m => m.Disable(lookup.Id)) %>">disable</a></td>
<%
        }
        else
        { %>
        <td><a href="<%= ExpressionHelper.GetActionLink<MarketSegmentController>(m => m.Enable(lookup.Id)) %>">enable</a></td>
<%      }%>
        <td><%= lookup.MarketSegment %></td>
        <td><%= lookup.UsageCount %></td>
    </tr>
        <%
    } %>
    <tfoot>
        <tr>
            <td colspan="4"><%= (new LinkButtonHtmlControl<MarketSegmentController>(m => m.Edit(0), "Create market segment")).Button %></td>
        </tr>
    </tfoot>
</table>
</asp:Content>