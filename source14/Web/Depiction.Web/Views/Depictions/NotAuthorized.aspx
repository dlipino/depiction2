﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" CodeBehind="NotAuthorized.aspx.cs" Inherits="Depiction.Web.Views.Depictions.NotAuthorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Not Authorized</h1>
<p>You are not authorized to make changes to that Depiction. To make changes to a Depiction, you must be logged in as the user who published it or as an administrator.</p>
</asp:Content>
