﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.Depictions.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Web Depictions</h1>
<%
    foreach (var webDepiction in Model)
    {
        %>
        <p>
        <% if (User.Identity.IsAuthenticated)
           {
               if (User.IsInRole("admin") || Convert.ToInt32(User.Identity.Name) == webDepiction.User.Id)
               {%> 
            <a href="<%= ResolveUrl("~/Depictions/Delete/" + webDepiction.Id) %>">delete</a>
            <a href="<%= ResolveUrl("~/Depictions/Edit/" + webDepiction.Id) %>">edit</a>
        <%     }
           } %>
            <a href="<%= ResolveUrl("~/Depictions/Show/" + webDepiction.Id) %>"><%= webDepiction.DepictionTitle %></a>
        </p>
        <%
    } %>
</asp:Content>