﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Depictions
{
    public class Edit : ViewPage<WebDepiction>
    {
        protected string form;

        protected override void OnLoad(EventArgs e)
        {
            string tags = string.Empty;

            foreach (DepictionTag depictionTag in Model.Tags)
            {
                tags += depictionTag.Tag + ", ";
            }

            var formItems = new List<IFormItem> {
                new HeaderFormItem("Edit web depiction"),
                new DataFormItem("Title", new TextHtmlControl("DepictionTitle", Model.DepictionTitle, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DataFormItem("Tags", new TextHtmlControl("Tags", tags.Trim(new[] {' ', ','}), new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DataFormItem("Description", new MultiLineTextHtmlControl("Description", Model.Description, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Save Changes")})
            };

            form = FormBuilder.BuildForm<DepictionsController>(d => d.DoEdit(Model.Id), "EditWebDepiction", formItems.ToArray());

            base.OnLoad(e);
        }
    }
}