﻿using System.Web.Mvc;

namespace Depiction.Web.Views.Home
{
    public class Dashboard : ViewPage<DashboardStats> {}

    public class DashboardStats
    {
        public int NotActivatedUserCount { get; set; }
        public int ActivatedUserCount { get; set; }
        public int TrialRegistrations { get; set; }
        public int CurrentlyActiveTrials { get; set; }
        public int AverageTimeBetweenSignUpAndActivation { get; set; }

        public int GeneratedKeysActivated { get; set; }

        public int GeneratedKeysNotActivated { get; set; }

        public int GeneratedKeyCount { get; set; }
    }
}