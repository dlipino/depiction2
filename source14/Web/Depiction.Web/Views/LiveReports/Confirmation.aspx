﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Depiction.Web.Views.LiveReports.Confirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Thank you for sending a Live Report!</h1>
    <p>Your live report has been submitted.</p>
</asp:Content>
