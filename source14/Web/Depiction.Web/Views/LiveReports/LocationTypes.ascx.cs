﻿using System.Web.Mvc;

namespace Depiction.Web.Views.LiveReports
{
    public class LocationTypes : ViewUserControl<LocationTypeData> {}

    public class LocationTypeData
    {
        public string LocationType { get; set; }
    }
}