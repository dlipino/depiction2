﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Submit.aspx.cs" Inherits="Depiction.Web.Views.LiveReports.Submit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

	<script type="text/javascript">
	    window.addEvent('domready', function() {
	        $('SubmitLiveReport').reset();
	        $('LocationType').value = '';
	        $('ElementType').value = '';
	        
	        var exValidatorA = new fValidator("SubmitLiveReport");

	        $('ElementType').addEvent('change', function(e) {
	            e = new Event(e).stop();

	            var elementId = $('ElementType').value;

	            if (elementId == "")
	                elementId = "0/0";

	            var url = "/LiveReports/DMLPropertyOptions/" + elementId;

	            /**
	            * The simple way for an Ajax request, use onRequest/onComplete/onFailure
	            * to do add your own Ajax depended code.
	            */
	            new Ajax(url, {
	                method: 'get',
	                update: $('ElementProperties'),
	                onComplete: function() {
	                    exValidatorA.Scrape();
	                }
	            }).request();
	        });

	        $('LocationType').addEvent('change', function(e) {
	            e = new Event(e).stop();

	            var locationType = $('LocationType').value;

	            if (locationType == "")
	                locationType = "none";

	            var url = "/LiveReports/LocationTypes/" + locationType;

	            /**
	            * The simple way for an Ajax request, use onRequest/onComplete/onFailure
	            * to do add your own Ajax depended code.
	            */
	            new Ajax(url, {
	                method: 'get',
	                update: $('LocationSpec'),
	                onComplete: function() {
	                    exValidatorA.Scrape();
	                }
	            }).request();
	        });

	        $('SubmitLiveReport').addEvent('submit', function(e) {
	            if (exValidatorA.IsValid()) {
	                e = new Event(e).stop();

	                new XHR({
	                    method: 'get',
	                    onSuccess: function(req) {
	                        if (Boolean(req.toString())) {
	                            $('SubmitLiveReport').submit();
	                        }
	                        else {
	                            var owner = $('recaptcha_widget_div');

	                            if (!Boolean($(owner.getProperty("id") + "captcha" + "_msg"))) {
	                                new Element("div", { "id": owner.getProperty("id") + "captcha" + "_msg", "class": "fValidator-msg" })
				                    .setHTML("Are you human?")
				                    .setStyle("opacity", 0)
				                    .injectAfter(owner)
				                    .effect("opacity", {
				                        duration: 500,
				                        transition: Fx.Transitions.linear
				                    }).start(0, 1);
	                            }

	                            Recaptcha.reload();
	                        }
	                    }
	                }).send('/Captcha/Verify', 'recaptcha_challenge_field=' + $('recaptcha_challenge_field').value + '&recaptcha_response_field=' + $('recaptcha_response_field').value);
	            }
	        });
	    }); 
	</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%= form %>
</asp:Content>
