﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.LiveReports
{
    public class CreateSession : ViewPage<LiveReportSession>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Create a Live Reporting Session: "),
                new DataFormItem("Send reports to", new TextHtmlControl("SendReportsToEmail", Model.SendReportsToEmail, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation(), new EmailValidation()}
                })),
                new DataFormItem("Available elements", new DisplayTextHtmlControl(BuildProperties(Model.Properties))),
                new ButtonsFormItem(new IButton[] {new SubmitButton("AddElement", "Add an element")}),
                new ButtonsFormItem(new IButton[] {new SubmitButton("CreateSession", "Create Session")})
            };

            form = FormBuilder.BuildForm<LiveReportsController>(lr => lr.DoCreateSession(), "CreateLiveReportSession", formItems.ToArray());

            base.OnLoad(e);
        }

        private string BuildProperties(IList<SessionProperty> properties)
        {
            if (properties == null || properties.Count == 0)
                return "No element types selected";

            var returnString = "<ul>";

            var elementDefinitions = properties.Select(p => new { displayName = p.Property.DMLDefinition.DMLDisplayName, id = p.Property.DMLDefinition.Id }).Distinct();

            foreach (var elementDefinition in elementDefinitions)
            {
                returnString += "<li>" + elementDefinition.displayName + "</li>";

                returnString += "<ul>";

                foreach (var property in properties.Where(p => p.Property.DMLDefinition.Id == elementDefinition.id))
                {
                    returnString += string.Format("<li>{0}</li>", property.Property.DisplayName);
                }

                returnString += "</ul>";
            }

            return returnString + "</ul>";
        }
    }
}