﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="AddElement.aspx.cs" Inherits="Depiction.Web.Views.LiveReports.AddElement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">

    window.addEvent("domready", function() {
        $('AddElementForm').reset();
        $('ElementType').value = '';

        var exValidatorA = new fValidator("AddElementForm");

        $('ElementType').addEvent('change', function(e) {
            e = new Event(e).stop();

            var elementId = $('ElementType').value;

            if (elementId == "")
                elementId = "0";

            var url = "/LiveReports/DMLPropertySelector/" + elementId;

            /**
            * The simple way for an Ajax request, use onRequest/onComplete/onFailure
            * to do add your own Ajax depended code.
            */
            new Ajax(url, {
                method: 'get',
                update: $('ElementProperties')
            }).request();
        });
    });

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%= form %>
</asp:Content>
