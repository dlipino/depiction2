﻿using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.MarketSegment
{
    public class Edit : ViewPage<Lookup>
    {
        protected string form;

        protected override void OnLoad(System.EventArgs e)
        {
            var formItems = new IFormItem[] {
                new HeaderFormItem("Edit market segment"),
                new DataFormItem("Market segment", new TextHtmlControl("ParameterValue", Model.Value, new ValidationInformation { Validations = new IValidation[] {new RequiredValidation(), new AlphaNumericValidation()} })),
                new ButtonsFormItem(new IButton [] {new LinkButtonHtmlControl<AdminController>(a => a.MarketSegments(), "Cancel"), new SubmitButton("Save")}),
            };

            form = FormBuilder.BuildForm<MarketSegmentController>(t => t.Save(Model.Id), "EditMarketSegmentForm", formItems);

            base.OnLoad(e);
        }
    }
}