﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Addin
{
    public class Edit : ViewPage<EditAddinInformationData>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem(Model.AddinToEdit.Id > 0 ? "Edit addin definition: " : "Create an addin definition:"),
                new DataFormItem("Friendly name", new TextHtmlControl("FriendlyName", Model.AddinToEdit.FriendlyName, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DataFormItem("Addin name", new TextHtmlControl("Name", Model.AddinToEdit.Name, new ValidationInformation {
                    Validations = new[] {new RequiredValidation()}
                })),
                new DataFormItem("Download location", new TextHtmlControl("AddinDownloadLocation", Model.AddinToEdit.AddinDownloadLocation, new ValidationInformation {
                    Validations = new[] {new RequiredValidation()}
                })),
                new DataFormItem("Addin type", new DropDownListHtmlControl("AddinType", Enum.GetNames(typeof (AddinType)).Select(at => new SelectOption {
                    Label = at,
                    Selected = at == Enum.GetName(typeof (AddinType), Model.AddinToEdit.AddinType)
                }).ToArray())),
                new DataFormItem("Valid Depiction versions", new ListBoxHtmlControl("DepictionVersions", Model.DepictionVersions.Select(v => new SelectOption {
                    Label = v.FriendlyName,
                    Value = v.Id.ToString(),
                    Selected = Model.AddinToEdit.ValidWithVersions.Contains(v)
                }).ToArray())),
                new DataFormItem("Regions", new ListBoxHtmlControl("Regions", Model.RegionDefinitions.Select(r => new SelectOption {
                    Label = r.RegionCode + " - " + r.RegionName,
                    Value = r.Id.ToString(),
                    Selected = Model.AddinToEdit.Regions.Contains(r)
                }).ToArray())),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Save")})
            };

            form = FormBuilder.BuildForm<AddinController>(a => a.Save(Model.AddinToEdit.Id), "EditAddinForm", formItems.ToArray());
            base.OnLoad(e);
        }
    }

    public class EditAddinInformationData
    {
        public AddinInformation AddinToEdit { get; set; }
        public DepictionVersion[] DepictionVersions { get; set; }
        public RegionDefinition[] RegionDefinitions { get; set; }
    }
}