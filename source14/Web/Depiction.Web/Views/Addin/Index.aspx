﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.Addin.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Addin Definitions</h1>
    <table>
        <thead>
            <td>Friendly name</td>
            <td>Addin information</td>
        </thead>
    <%
        foreach (var addinInformation in Model)
        {
            string versions = string.Empty;
            
            foreach (var depictionVersion in addinInformation.ValidWithVersions)
            {
                versions += depictionVersion.FriendlyName + ", ";
            }

            string regions = string.Empty;

            if (addinInformation.Regions.Count == 0) regions = "All regions";

            foreach (var regionDefinition in addinInformation.Regions)
            {
                regions += regionDefinition.RegionCode + " (" + regionDefinition.RegionName + "), ";
            }

            string quickstartItems = "<ul>";

            foreach (var quickstartItem in addinInformation.RelatedQuickstartItems)
            {
                quickstartItems += "<li>" + quickstartItem.QuickstartName
                    + " " + (new LinkButtonHtmlControl<AddinController>(a => a.EditQuickstart(quickstartItem.Id), "edit")).Link
                    + " " + (new LinkButtonHtmlControl<AddinController>(a => a.DeleteQuickstartItem(quickstartItem.Id), "delete")).Link + "</li>";
            }

            quickstartItems += "</ul>";
            
            %>
        <tr>
            <td>
                <p><%= addinInformation.FriendlyName %></p>
                <%= (new LinkButtonHtmlControl<AddinController>(a => a.Edit(addinInformation.Id), "edit")).Link %>&nbsp;<%= (new LinkButtonHtmlControl<AddinController>(a => a.Delete(addinInformation.Id), "delete")).Link %>
            </td>
            <td>
                <p><strong>Addin name: </strong><%= addinInformation.Name %></p>
                <p><strong>Download Location: </strong><%= addinInformation.AddinDownloadLocation %></p>
                <p>
                    <strong>Depiction versions:</strong> <%= versions.Trim(", ".ToCharArray())%></p>
                <p>
                    <strong>Regions:</strong> <%= regions.Trim(", ".ToCharArray()) %>
                </p>
                <p>
                    <strong>Associated quickstart items:</strong> <br />
                    <%=quickstartItems.Trim(", ".ToCharArray())%>
                </p>
                <p><%= (new LinkButtonHtmlControl<AddinController>(a => a.AddQuickstart(addinInformation.Id), "add a quickstart association")).Link %></p>
            </td>
        </tr>
            <%
        } %>
    </table>
    <%= (new LinkButtonHtmlControl<AddinController>(a => a.New(), "Define a new addin")).Button %>
</asp:Content>