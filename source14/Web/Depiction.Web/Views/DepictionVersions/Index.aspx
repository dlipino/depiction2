﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.DepictionVersions.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Depiction version mappings</h1>
    <table>
        <thead>
            <td>&nbsp;</td>
            <td>Friendly name</td>
            <td>Hard coded value</td>
        </thead>
    <%
        foreach (var depictionVersion in Model)
        {
            %>
        <tr>
            <td><%= (new LinkButtonHtmlControl<DepictionVersionsController>(d => d.Edit(depictionVersion.Id), "edit")).Link %>&nbsp;<%= (new LinkButtonHtmlControl<DepictionVersionsController>(d => d.Delete(depictionVersion.Id), "delete")).Link %></td>
            <td><p><%= depictionVersion.FriendlyName %></p></td>
            <td><p><%= depictionVersion.VersionNumber %></p></td>
        </tr>
            <%
        } %>
    </table>
    <%= (new LinkButtonHtmlControl<DepictionVersionsController>(d => d.New(), "Define a new version mapping")).Button %>
</asp:Content>
