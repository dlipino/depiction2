﻿using System;
using System.Web.Mvc;
using Depiction.Web.Content;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.SignUp
{
    public class Index : ViewPage<RegisterViewData>
    {
        protected string form { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new IFormItem[] {
                new HeaderFormItem("Registration"),
                new DataFormItem(Strings.Title, new DropDownListHtmlControl("Title", ViewData.Model.Titles)),
                new DataFormItem(Strings.FirstName, new TextHtmlControl("FirstName", string.Empty,new ValidationInformation { Validations = new IValidation[] {new RequiredValidation(), new AlphaNumericValidation()} })), 
                new DataFormItem(Strings.LastName, new TextHtmlControl("LastName", string.Empty, new ValidationInformation { Validations = new IValidation[] {new RequiredValidation(), new AlphaNumericValidation()} })),
                new DataFormItem(Strings.Email, new TextHtmlControl("Email", string.Empty, new ValidationInformation { Validations = new IValidation[] {new EmailValidation(), new RequiredValidation()} })),
                new DataFormItem(Strings.Organization, new TextHtmlControl("Organization", string.Empty, new ValidationInformation { Validations = new IValidation[] {new RequiredValidation(), new AlphaNumericValidation()} })),
                new DataFormItem(Strings.MarketSegment, new DropDownListHtmlControl("MarketSegment", ViewData.Model.MarketSegments, new ValidationInformation { Validations = new[] {new RequiredValidation()} })),
                new DataFormItem(Strings.AdditionInformation, new MultiLineTextHtmlControl("Organization")), new ButtonsFormItem(new IButton [] {new SubmitButton("Register")}),
                new ReferenceFormItem("*required information")
            };

            form = FormBuilder.BuildForm<SignUpController>(s => s.Register(), "RegistrationForm", formItems);

            base.OnLoad(e);
        }
    }

    public class RegisterViewData
    {
        public SelectOption[] MarketSegments { get; set; }
        public SelectOption[] Titles { get; set; }
    }
}