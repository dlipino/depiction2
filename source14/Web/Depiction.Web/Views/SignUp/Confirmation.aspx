﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="Depiction.Web.Views.SignUp.Confirmation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Thank you for registering.</h1>
<p>You will receive an email from Depiction shortly containing your activation code. We want your test drive of Depiction to be both valuable and enjoyable. For that reason, we provide the following resources:</p>
<div class="helpButtons">
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/tutorials", Strings.Tutorials)%>
        <h3>Step-by-step videos about Depiction.</h3>
    </div>
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/forum", Strings.Forum)%>
        <h3>Get online support from Depiction staff and other users. Also share ideas, tips and tricks with people who have common interests.</h3>
    </div>
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/webinars", Strings.Webinars)%>
        <h3>Depiction 101: The Basics. This webinar (online seminar) is intended for the novice Depictior.</h3>
    </div>
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/blog", Strings.Blog)%>
        <h3>The latest posts from Depiction staff about how Depiction is being used, new features, reviews, etc.</h3>
    </div>
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/sample-whatif-scenarios", Strings.Samples)%>
        <h3>Sample files of what if scenarios.</h3>
    </div>
    <div>
        <%= Html.ClickHereButton("http://www.depiction.com/faqs", Strings.FAQ)%>
        <h3>Read answers to Frequently Asked Questions like &ldquo;What exactly is a Depiction?&rdquo;</h3>
    </div>
    <p>You will find additional resources within the help section of the application, including step-by-step written instructions with screen captures and a glossary.</p>
</div>
</asp:Content>