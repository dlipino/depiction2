﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.DML.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>DML Definitions</h1>
<p><%= (new LinkButtonHtmlControl<DMLController>(d => d.Define(), "Add a DML file")).Button %></p>

    <table>
        <thead>
            <td>Name</td>
            <td>Type</td>
            <td>Download path</td>
            <td>Properties</td>
        </thead>
    <%
        foreach (var dmlDefinition in Model)
        {
            %>
            
        <tr>
            <td><%= dmlDefinition.DMLDisplayName %></td>
            <td><%= dmlDefinition.DMLTypeName %></td>
            <td><%= dmlDefinition.DMLDownloadPath %></td>
            <td>
                <%
            foreach (var dmlProperty in dmlDefinition.Properties)
            {
                %>
                <p><%= dmlProperty.DisplayName %></p>
                <%
            } %>
            </td>
        </tr>
            <%
        } %>
    </table>

</asp:Content>
