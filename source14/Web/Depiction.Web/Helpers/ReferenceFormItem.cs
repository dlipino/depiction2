﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Depiction.Web.Helpers
{
    public class ReferenceFormItem : IFormItem
    {
        private readonly string referenceText;

        public ReferenceFormItem(string referenceText)
        {
            this.referenceText = referenceText;
        }

        public string FormItem
        {
            get { return string.Format("<p class=\"ReferenceLine\">{0}</p>", referenceText); }
        }
    }
}
