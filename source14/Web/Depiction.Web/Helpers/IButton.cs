﻿namespace Depiction.Web.Helpers
{
    public interface IButton
    {
        string Button { get; }
    }
}