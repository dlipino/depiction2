﻿using System;

namespace Depiction.Web.Helpers
{
    public class SelectOption : IComparable<SelectOption>
    {
        public bool Disabled { get; set; }
        public string Label { get; set; }
        public bool Selected { get; set; }
        public string Value { get; set; }
        public int CompareTo(SelectOption other)
        {
            if (other == null) return 0;
            return Label.CompareTo(other.Label);
        }
    }
}