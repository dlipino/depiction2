﻿namespace Depiction.Web.Helpers.Interfaces
{
    public interface IAuthorizer
    {
        void DoAuthorize(string user, bool authorized);
    }
}