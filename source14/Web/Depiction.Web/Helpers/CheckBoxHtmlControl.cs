﻿namespace Depiction.Web.Helpers
{
    public class CheckBoxHtmlControl : IHtmlControl
    {
        private readonly string name;
        private readonly string value;
        private readonly string text;
        private readonly bool isChecked;

        public CheckBoxHtmlControl(string name, string value, string text, bool isChecked)
        {
            this.name = name;
            this.value = value;
            this.text = text;
            this.isChecked = isChecked;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                if(isChecked)
                    return string.Format("<span class=\"checkbox\"><input type=\"checkbox\" name=\"{0}\" value=\"{1}\" checked />{2}</span>", name, value, text);

                return string.Format("<span class=\"checkbox\"><input type=\"checkbox\" name=\"{0}\" value=\"{1}\" />{2}</span>", name, value, text);
            }
        }

        #endregion
    }
}