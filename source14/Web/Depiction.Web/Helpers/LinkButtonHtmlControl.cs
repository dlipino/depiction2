﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Depiction.Web.Helpers
{
    public class LinkButtonHtmlControl<T> : IButton, IHtmlControl where T : Controller
    {
        private readonly Expression<Action<T>> action;
        private readonly string buttonText;

        public LinkButtonHtmlControl(Expression<Action<T>> action, string buttonText)
        {
            this.action = action;
            this.buttonText = buttonText;
        }

        #region IButton Members

        public string Button
        {
            get { return string.Format("<a class=\"button\" href=\"{0}\"><span>{1}</span></a>", buildActionFromExpression(action), buttonText); }
        }

        public string Link
        {
            get { return string.Format("<a href=\"{0}\">{1}</a>", buildActionFromExpression(action), buttonText); }
        }

        #endregion

        private static string buildActionFromExpression<T>(Expression<Action<T>> action) where T : Controller
        {
            RouteValueDictionary routeInformation = ExpressionHelper.GetRouteValuesFromExpression(action);
            string returnValue = string.Empty;

            foreach (var routeItem in routeInformation)
            {
                returnValue += "/" + routeItem.Value;
            }

            return returnValue;
        }

        public string HtmlControl
        {
            get { return Link; }
        }
    }
}