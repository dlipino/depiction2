﻿using System.Web.Mvc;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Helpers
{
    public class MultiLineTextHtmlControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly string value;
        private readonly ValidationInformation validation;

        public MultiLineTextHtmlControl(string controlName) : this(controlName, string.Empty)
        {
        }

        public MultiLineTextHtmlControl(string controlName, string value) : this(controlName, value, null)
        {
        }

        public MultiLineTextHtmlControl(string controlName, string value, ValidationInformation validation)
        {
            this.controlName = controlName;
            this.value = value;
            this.validation = validation;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                string cssClass = string.Empty;

                if (validation != null)
                    cssClass += string.Format("fValidate[{0}]", validation.BuildValidationString());

                var tagBuilder = new TagBuilder("textarea");

                tagBuilder.Attributes.Add("id", controlName);
                tagBuilder.Attributes.Add("name", controlName);
                tagBuilder.Attributes.Add("rows", "3");

                if (!string.IsNullOrEmpty(cssClass))
                    tagBuilder.Attributes.Add("class", cssClass.Trim());

                if (!string.IsNullOrEmpty(value))
                    tagBuilder.InnerHtml = value;

                return tagBuilder.ToString(TagRenderMode.Normal);

                //return string.Format("<textarea id=\"{0}\" name=\"{0}\" rows=\"3\">{1}</textarea>", controlName, value);
            }
        }

        #endregion
    }
}