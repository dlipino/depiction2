﻿namespace Depiction.Web.Helpers
{
    public class DisplayTextHtmlControl : IHtmlControl
    {
        private readonly string textTODisplay;

        public DisplayTextHtmlControl(string textToDisplay)
        {
            textTODisplay = textToDisplay;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get { return string.Format("<div class=\"DisplayText\">{0}</div>", textTODisplay); }
        }

        #endregion
    }
}