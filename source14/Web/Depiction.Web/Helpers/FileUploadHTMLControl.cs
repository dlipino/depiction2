﻿namespace Depiction.Web.Helpers
{
    public class FileUploadHTMLControl : IHtmlControl
    {
        private readonly string name;

        public FileUploadHTMLControl(string name)
        {
            this.name = name;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get { return string.Format("<input type=\"file\" name=\"{0}\" />", name); }
        }

        #endregion
    }
}