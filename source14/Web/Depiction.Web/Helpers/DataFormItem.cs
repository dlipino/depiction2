﻿namespace Depiction.Web.Helpers
{
    public class DataFormItem : IFormItem
    {
        private readonly string displayText;
        private readonly IHtmlControl htmlControl;

        public DataFormItem(string displayText, IHtmlControl htmlControl)
        {
            this.displayText = displayText;
            this.htmlControl = htmlControl;
        }

        #region IFormItem Members

        public string FormItem
        {
            get { return string.Format("<div class=\"DataFormItem\"><span>{0}</span>{1}</div>", displayText, htmlControl.HtmlControl); }
        }

        #endregion
    }
}