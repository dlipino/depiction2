﻿namespace Depiction.Web.Helpers
{
    public class DistanceHTMLControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly string value;
        private readonly string unit;

        public DistanceHTMLControl(string controlName, string value, string unit)
        {
            this.controlName = controlName;
            this.value = value;
            this.unit = unit;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get { return string.Format(
                "<input value=\"{0}\" id=\"{1}\" name=\"{1}\" style=\"width: 100px;\" class=\"fValidate['required','real']\" />&nbsp;" +
                "<select id=\"{1}::Unit\" name=\"{1}::Unit\">" +
                "<option value=\"feet\" label=\"feet\"" + (unit == "feet" ? " selected" : string.Empty) + "\">feet</option>" +
                "<option value=\"meters\" label=\"meters\"" + (unit == "meters" ? " selected" : string.Empty) + "\">meters</option>" +
                "<option value=\"inches\" label=\"inches\"" + (unit == "inches" ? " selected" : string.Empty) + "\">inches</option>" +
                "<option value=\"centimeters\" label=\"centimeters\"" + (unit == "centimeters" ? " selected" : string.Empty) + "\">centimeters</option>" +
                "<option value=\"miles\" label=\"miles\"" + (unit == "miles" ? " selected" : string.Empty) + "\">miles</option>" +
                "<option value=\"kilometers\" label=\"kilometers\"" + (unit == "kilometers" ? " selected" : string.Empty) + "\">kilometers</option>" +
                "</select>", value, controlName);
            }
        }

        #endregion
    }
}