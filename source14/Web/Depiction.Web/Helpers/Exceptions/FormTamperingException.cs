﻿using System;

namespace Depiction.Web.Helpers.Exceptions
{
    public class FormTamperingException : Exception
    {
        public FormTamperingException() : base("The form appears to have been tampered with!") {}
    }
}