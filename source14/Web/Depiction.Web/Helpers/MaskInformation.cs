﻿namespace Depiction.Web.Helpers
{
    public class MaskInformation
    {
        public string Mask { get; set; }
        public bool StripMask { get; set; }
    }
}