﻿namespace Depiction.Web.Helpers
{
    public class DivFormItem : IFormItem
    {
        private readonly string divContents;
        private readonly string divName;

        public DivFormItem(string divName, string divContents)
        {
            this.divName = divName;
            this.divContents = divContents;
        }

        #region IFormItem Members

        public string FormItem
        {
            get { return string.Format("<div id=\"{0}\" name=\"{0}\">{1}</div>", divName, divContents); }
        }

        #endregion
    }
}