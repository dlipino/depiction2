﻿using System.Web.Mvc;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Helpers
{
    public class ListBoxHtmlControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly SelectOption[] options;
        private readonly ValidationInformation validation;
        private readonly bool allowMultiSelect;

        public ListBoxHtmlControl(string controlName, SelectOption[] options) : this(controlName, options, null, true)
        {
        }

        public ListBoxHtmlControl(string controlName, SelectOption[] options, ValidationInformation validation, bool allowMultiSelect)
        {
            this.controlName = controlName;
            this.options = options;
            this.validation = validation;
            this.allowMultiSelect = allowMultiSelect;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                string cssClass = string.Empty;

                if (validation != null)
                    cssClass += string.Format("fValidate[{0}]", validation.BuildValidationString());

                var tagBuilder = new TagBuilder("select");
                tagBuilder.Attributes.Add("id", controlName);
                tagBuilder.Attributes.Add("name", controlName);
                tagBuilder.Attributes.Add("size", "5");

                if(allowMultiSelect)
                    tagBuilder.Attributes.Add("multiple", "multiple");

                if (!string.IsNullOrEmpty(cssClass))
                    tagBuilder.Attributes.Add("class", cssClass.Trim());

                tagBuilder.InnerHtml = buildOptions();

                return tagBuilder.ToString();
            }
        }

        #endregion

        private string buildOptions()
        {
            if (options == null) return string.Empty;
            string returnValue = string.Empty;

            foreach (SelectOption option in options)
            {
                if (option.Value == null)
                    option.Value = option.Label;

                var tagBuilder = new TagBuilder("option");

                if (option.Disabled)
                    tagBuilder.Attributes.Add("disabled", "disabled");

                if (option.Selected)
                    tagBuilder.Attributes.Add("selected", "selected");

                tagBuilder.Attributes.Add("value", option.Value);
                tagBuilder.Attributes.Add("label", option.Label);
                tagBuilder.InnerHtml = option.Label;

                returnValue += tagBuilder.ToString();
            }

            return returnValue;
        }
    }
}