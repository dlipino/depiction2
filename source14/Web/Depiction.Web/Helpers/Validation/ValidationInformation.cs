﻿namespace Depiction.Web.Helpers.Validation
{
    public class ValidationInformation
    {
        public IValidation[] Validations { get; set; }

        public string BuildValidationString()
        {
            string validations = string.Empty;

            foreach (IValidation validationItem in Validations)
                validations += string.Format("'{0}',", validationItem.ValidationText);

            return validations.Trim(',');
        }
    }
}