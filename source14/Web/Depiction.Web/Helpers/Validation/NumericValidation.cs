﻿namespace Depiction.Web.Helpers.Validation
{
    public class NumericValidation : IValidation
    {
        #region IValidation Members

        public string ValidationText
        {
            get { return "integer"; }
        }

        #endregion
    }
}