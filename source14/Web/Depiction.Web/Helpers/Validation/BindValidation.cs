﻿namespace Depiction.Web.Helpers.Validation
{
    public class BindValidation : IValidation
    {
        private readonly string bindsToFieldName;

        public BindValidation(string bindsToFieldName)
        {
            this.bindsToFieldName = bindsToFieldName;
        }

        #region IValidation Members

        public string ValidationText
        {
            get { return string.Format("bind={0}", bindsToFieldName); }
        }

        #endregion
    }
}
