﻿namespace Depiction.Web.Helpers.Validation
{
    public interface IValidation
    {
        string ValidationText { get; }
    }
}