﻿using System;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Autofac.Builder;
using Autofac.Integration.Web;
using Autofac.Integration.Web.Mvc;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Interfaces;
using NHibernate;

namespace Depiction.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication, IContainerProviderAccessor
    {
        private static IContainerProvider _containerProvider;

        #region IContainerProviderAccessor Members

        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }

        #endregion

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", // Route name
                            "{controller}/{action}/{id}", // URL with parameters
                            new {
                                    controller = "SignUp",
                                    action = "Index",
                                    id = ""
                                } // Parameter defaults
                );

            routes.MapRoute("AJAX", // Route name
                            "{controller}/{action}/{liveReportSessionId}/{dmlDefinitionId}"
                );
        }

        protected void Application_Start()
        {
            // Set up Autofac ...
            var builder = new ContainerBuilder();
            builder.Register(c => DBFactory.GetSession()).As<ISession>().HttpRequestScoped();
            builder.Register<BaseRepository>().As<IRepository>().HttpRequestScoped();
            builder.Register<FormsAuthorizer>().As<IAuthorizer>().HttpRequestScoped();
            builder.RegisterModule(new AutofacControllerModule(Assembly.GetExecutingAssembly()));

            _containerProvider = new ContainerProvider(builder.Build());

            // notify ASP.NET MVC that it should use Autofacs own controller factory to instantiate controllers,
            // so that Autofac can resolve any dependencies the controllers need
            ControllerBuilder.Current.SetControllerFactory(new AutofacControllerFactory(_containerProvider));

            RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_OnAuthenticateRequest(Object sender, EventArgs e)
        {
            if (Context.User == null) return;
            if (!Context.User.Identity.IsAuthenticated) return;

            var repository = _containerProvider.RequestContainer.Resolve<IRepository>();
            var user = repository.GetOne<User>(Convert.ToInt32(Context.User.Identity.Name));

            if (user == null || user.Id < 1)
            {
                FormsAuthentication.SignOut();
                Thread.CurrentPrincipal = null;
                return;
            }

            var principal = new GenericPrincipal(new GenericIdentity(user.Id.ToString()), user.Roles.Select(r => r.RoleName).ToArray());

            Thread.CurrentPrincipal = Context.User = principal;
            return;
        }
    }
}