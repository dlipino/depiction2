﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Depiction.Web.Views.Captcha;

namespace Depiction.Web.Controllers
{
    public class CaptchaController : Controller
    {
        public ActionResult Verify()
        {
            var recaptchaChallenge = Request.QueryString["recaptcha_challenge_field"];
            var recaptchaResponse = Request.QueryString["recaptcha_response_field"];

            string publicKey = "6Lfe1gYAAAAAAPCx_m8wOqp2saxZUynDiMx14LKY";
            string privateKey = "6Lfe1gYAAAAAAHsIYP5gnqwMH6FS3eKhbkiJhyyS";
            string remoteIP = Request.UserHostAddress;

            bool captchaValid;

            var webRequest = (HttpWebRequest)WebRequest.Create("http://api-verify.recaptcha.net/verify");

            // Set values for the request back
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            string strNewValue = String.Format("privatekey={0}&remoteip={1}&challenge={2}&response={3}", privateKey, remoteIP, recaptchaChallenge, recaptchaResponse);
            webRequest.ContentLength = strNewValue.Length;

            // Write the request
            var streamWriter = new StreamWriter(webRequest.GetRequestStream(), Encoding.ASCII);
            streamWriter.Write(strNewValue);
            streamWriter.Close();

            // Do the request to get the response
            using (var streamReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                var response = streamReader.ReadToEnd();
                var lines = response.Split('\n');
                captchaValid = Convert.ToBoolean(lines[0]);
            }

            return View(new VerifyParams { Result = captchaValid });
        }
    }
}