using System.Web.Mvc;
using Depiction.Marketing.DomainModel;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class DBController : Controller
    {
        public ActionResult UpdateDB()
        {
            DBFactory.UpdateDatabase();
            return View();
        }
    }
}