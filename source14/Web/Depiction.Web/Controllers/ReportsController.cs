using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin,stats")]
    public class ReportsController : Controller
    {
        private readonly IRepository repository;

        public ReportsController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TrialRegistrations()
        {
            return View(repository.Queryable<TrialRegistration>().Select(t => new {
                RegistrationId = t.Id,
                PersonId = t.Registrant.Id,
                t.Organization,
                t.AdditionalInformation,
                t.ActivationCode,
                t.Activated,
                t.RegistrationDate,
                t.FirstActivationDate,
                t.LastActivationDate,
                t.ActivationCount,
                t.Registrant.LastName,
                t.Registrant.FirstName,
                t.Registrant.Email,
                t.Registrant.Title
            }).ToArray());
        }

        public ActionResult UsageStatistics()
        {
            return View(repository.Queryable<UsageMetric>().Select(u => new {
                u.DateLogged,
                Metric = u.Metric.Value,
                PersonId = u.Person.Id,
                u.Details
            }));
        }
    }
}