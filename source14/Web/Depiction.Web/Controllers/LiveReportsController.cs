using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers;
using Depiction.Web.Views.LiveReports;
using NHibernate;

namespace Depiction.Web.Controllers
{
    public class LiveReportsController : Controller
    {
        private const string DefaultSendToEmailAddress = "exercise@depiction.com";
        private readonly IRepository repository;
        private LiveReportSession CurrentLiveReportSession
        {
            get
            {
                if (Session["CurrentLiveReportSession"] == null)
                    CurrentLiveReportSession = new LiveReportSession
                    {
                        SendReportsToEmail = DefaultSendToEmailAddress
                    };

                return (LiveReportSession)Session["CurrentLiveReportSession"];
            }
            set { Session.Add("CurrentLiveReportSession", value); }
        }

        public LiveReportsController(IRepository repository)
        {
            this.repository = repository;
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public ActionResult CreateSession()
        {
            if (Request.QueryString["ElementAdded"] == null)
                CurrentLiveReportSession = new LiveReportSession {
                    SendReportsToEmail = DefaultSendToEmailAddress
                };

            // This is a hack to attach the transient properties to a session
            foreach (SessionProperty sessionProperty in CurrentLiveReportSession.Properties)
            {
                sessionProperty.Property = repository.GetOne<DMLProperty>(sessionProperty.Property.Id);
            }

            return View(CurrentLiveReportSession);
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public ActionResult Sessions()
        {
            return View(repository.GetAll<LiveReportSession>());
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public void DoCreateSession()
        {
            CurrentLiveReportSession.SendReportsToEmail = Request.Form["SendReportsToEmail"];
            if (Equals(Request.Form["AddElement"], "AddElement"))
                Response.Redirect("~/LiveReports/AddElement/");
            using (ITransaction transaction = repository.BeginTransaction())
            {
                foreach (SessionProperty sessionProperty in CurrentLiveReportSession.Properties)
                {
                    repository.Save(sessionProperty);
                }

                repository.Save(CurrentLiveReportSession);
                transaction.Commit();
            }

            Response.Redirect("~/LiveReports/Submit/" + CurrentLiveReportSession.Id);
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public ActionResult DMLPropertySelector(int id)
        {
            return View(repository.GetAll<DMLProperty>().Where(d => d.DMLDefinition.Id == id).ToArray());
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public ActionResult AddElement()
        {
            List<SelectOption> options = repository.GetAll<DMLDefinition>().Select(d => new SelectOption {
                Label = d.DMLDisplayName,
                Value = d.Id.ToString()
            }).ToList();
            options.Sort();
            options.Insert(0, new SelectOption {
                Label = "Choose element type ...",
                Value = string.Empty
            });

            return View(options.ToArray());
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public void DoAddElement()
        {
            foreach (string dmlPropertyId in Request.Form["dmlProperties"].Split(','))
            {
                CurrentLiveReportSession.Properties.Add(new SessionProperty {
                    Property = repository.GetOne<DMLProperty>(Convert.ToInt32(dmlPropertyId))
                });
            }

            Response.Redirect("~/LiveReports/CreateSession?ElementAdded=True");
        }

        [Authorize(Roles = "admin,stats,livereports")]
        public ActionResult Index()
        {
            return View();
        }

        #region everybody gets to see these methods ...

        public ActionResult Submit(int id)
        {
            return View(repository.GetOne<LiveReportSession>(id));
        }

        public void DoSubmit(int id)
        {
            var liveReportSession = repository.GetOne<LiveReportSession>(id);
            int dmlDefinitionId = Convert.ToInt32(Request.Form["ElementType"].Replace(liveReportSession.Id + "/", string.Empty));

            // Send out a confirmation email ...
            var liveReportMessage = new MailMessage(Request.Form["From"], liveReportSession.SendReportsToEmail)
            {
                //"jereme@depiction.com") {
                Subject = repository.GetOne<DMLDefinition>(dmlDefinitionId).DMLTypeName + ":",
                BodyEncoding = Encoding.ASCII
            };


            var properties = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
            foreach (SessionProperty sessionProperty in liveReportSession.Properties.Where(p => p.Property.DMLDefinition.Id == dmlDefinitionId))
            {

                string value = Request.Form["Property::" + sessionProperty.Property.PropertyName];

                if (sessionProperty.Property.ValueType == "Depiction.API.ValueObject.Distance, Depiction.API")
                    value += " " + Request.Form["Property::" + sessionProperty.Property.PropertyName + "::Unit"];

                if (Request.Form["Property::" + sessionProperty.Property.PropertyName] != null && Request.Form["Property::" + sessionProperty.Property.PropertyName].Trim() != string.Empty)
                    properties[sessionProperty.Property.PropertyName] = value;
            }

            switch (Request.Form["LocationType"])
            {
                case "LatLon":
                    properties["Latitude"] = Request.Form["Location::Latitude"];
                    properties["Longitude"] = Request.Form["Location::Longitude"];
                    break;
                case "Address":
                    if (Request.Form["Location::Address"].Trim() != string.Empty)
                        properties["Street address"] = Request.Form["Location::Address"];
                    if (Request.Form["Location::City"].Trim() != string.Empty)
                        properties["City"] = Request.Form["Location::City"];
                    if (Request.Form["Location::State"].Trim() != string.Empty)
                        properties["State"] = Request.Form["Location::State"];
                    if (Request.Form["Location::ZipCode"].Trim() != string.Empty)
                        properties["Zipcode"] = Request.Form["Location::ZipCode"];
                    break;
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var property in properties)
                stringBuilder.Append(property.Key + ":" + property.Value + "\n");
            string emailBody = stringBuilder.ToString();
            
            // Our email reader does not know how to handle quoted-printable transfer-encoding (which is the default, and which we really should support)
            // but because of that limitation, send in 7bit encoding.
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(emailBody, new ContentType("text/plain"));
            plainView.TransferEncoding = TransferEncoding.SevenBit;

            liveReportMessage.AlternateViews.Add(plainView);

            var smtp = new SmtpClient("mail.depiction.com")
            {
                Credentials = new NetworkCredential("info", "900_spg")
            };

            smtp.Send(liveReportMessage);

            Response.Redirect("~/LiveReports/Confirmation");
        }

        public ActionResult DMLPropertyOptions(int liveReportSessionId, int dmlDefinitionId)
        {
            var liveReportSession = repository.GetOne<LiveReportSession>(liveReportSessionId);
            SessionProperty[] properties = liveReportSession == null ? null : liveReportSession.Properties.Where(p => p.Property.DMLDefinition.Id == dmlDefinitionId).ToArray();
            return View(properties);
        }

        public ActionResult LocationTypes(string id)
        {
            return View(new LocationTypeData
            {
                LocationType = id
            });
        }

        public ActionResult Confirmation()
        {
            return View();
        }

        #endregion
    }
}