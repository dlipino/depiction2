﻿using System;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Views.Home;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin,stats")]
    public class HomeController : Controller
    {
        private readonly IRepository repository;

        public HomeController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View("Dashboard", getDashboardStats());
        }

        public ActionResult Dashboard()
        {

            return View("Dashboard", getDashboardStats());
        }

        private DashboardStats getDashboardStats()
        {
            int activatedUserCount = repository.Queryable<TrialRegistration>().Where(t => !t.Registrant.Email.EndsWith("depiction.com")).Count(t => t.Activated);
            int notActivatedUserCount = repository.Queryable<TrialRegistration>().Where(t => !t.Registrant.Email.EndsWith("depiction.com")).Count(t => !t.Activated);
            int trialRegistrations = repository.Queryable<TrialRegistration>().Where(t => !t.Registrant.Email.EndsWith("depiction.com")).Count();
            int currentlyActiveTrials = repository.Queryable<TrialRegistration>().Count(t => t.FirstActivationDate > DateTime.Now.AddDays(-30));
            var activationDates = repository.Queryable<TrialRegistration>().Where(t => t.FirstActivationDate > new DateTime(2000, 1, 1) && !t.Registrant.Email.EndsWith("depiction.com")).Select(t => t.FirstActivationDate).ToArray();
            var registrationDates = repository.Queryable<TrialRegistration>().Where(t => t.FirstActivationDate > new DateTime(2000, 1, 1) && !t.Registrant.Email.EndsWith("depiction.com")).Select(t => t.RegistrationDate).ToArray();

            int generatedKeysActivated = repository.Queryable<TrialRegistration>().Where(t => t.Registrant.Email == "trialuser@depiction.com").Count(t => t.Activated);
            int generatedKeysNotActivated = repository.Queryable<TrialRegistration>().Where(t => t.Registrant.Email == "trialuser@depiction.com").Count(t => !t.Activated);
            int generatedKeyCount = repository.Queryable<TrialRegistration>().Where(t => t.Registrant.Email == "trialuser@depiction.com").Count();

            var averageDaysToActivation = activationDates.Average(r => (r.Ticks / TimeSpan.TicksPerDay)) - registrationDates.Average(a => (a.Ticks / TimeSpan.TicksPerDay));

            return new DashboardStats
            {
                ActivatedUserCount = activatedUserCount,
                NotActivatedUserCount = notActivatedUserCount,
                TrialRegistrations = trialRegistrations,
                CurrentlyActiveTrials = currentlyActiveTrials,
                AverageTimeBetweenSignUpAndActivation = Convert.ToInt32(averageDaysToActivation),
                GeneratedKeysActivated = generatedKeysActivated,
                GeneratedKeysNotActivated = generatedKeysNotActivated,
                GeneratedKeyCount = generatedKeyCount
            };
        }
    }
}