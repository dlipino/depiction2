using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using NHibernate;

namespace Depiction.Web.Controllers
{
    [HandleError(ExceptionType = typeof(NotDepictionOwnerException), View = "NotAuthorized")]
    public class DepictionsController : Controller
    {
        private readonly IRepository repository;

        public DepictionsController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.GetAll<WebDepiction>().ToArray());
        }

        public ActionResult Show(int id)
        {
            return View(repository.GetOne<WebDepiction>(id));
        }

        public ActionResult Embeddable(int id)
        {
            return View("Embeddable",repository.GetOne<WebDepiction>(id));
        }

        public ActionResult NotAuthorized()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            var webDepiction = repository.GetOne<WebDepiction>(id);
            AuthorizeDepictionPermissions(webDepiction);

            return View(webDepiction);
        }

        [Authorize(Roles = "admin,stats,livereports, publishtoweb")]
        public void DoComment(int id)
        {
            var comment = Request.Form["Post Comment"];
            var person = repository.GetOne<User>(Convert.ToInt32(User.Identity.Name)).Person;
            WebDepictionComment webDepictionComment = new WebDepictionComment();
            webDepictionComment.Comment = comment;
            webDepictionComment.CreationDate = DateTime.Now;
            webDepictionComment.WebDepiction = repository.GetOne<WebDepiction>(id);
            webDepictionComment.Person = person;

            using (ITransaction transaction = repository.BeginTransaction())
            {
                repository.Save(webDepictionComment);
                transaction.Commit();
            }
            Response.Redirect("~/Depictions/Show/" + id);
        }

        public void DoEdit(int id)
        {
            var webDepiction = repository.GetOne<WebDepiction>(id);
            AuthorizeDepictionPermissions(webDepiction);

            webDepiction.Tags.Clear();

            webDepiction.Description = Request.Form["Description"];
            webDepiction.DepictionTitle = Request.Form["DepictionTitle"];
            string[] tags = Request.Form["Tags"].Split(new[] {',', ' '});

            foreach (string tag in tags)
            {
                if (tag.Trim() == string.Empty) continue;

                DepictionTag[] depictionTag = repository.Search<DepictionTag>(dt => dt.Tag == tag.Trim().ToLower());

                if (depictionTag.Length > 0)
                {
                    webDepiction.Tags.Add(depictionTag[0]);
                }
                else
                {
                    webDepiction.Tags.Add(new DepictionTag {
                        Tag = tag.Trim().ToLower()
                    });
                }
            }

            using (ITransaction transaction = repository.BeginTransaction())
            {
                foreach (DepictionTag tag in webDepiction.Tags)
                {
                    repository.Save(tag);
                }

                repository.Save(webDepiction);

                transaction.Commit();
            }

            Response.Redirect("~/Depictions/Show/" + id);
        }

        public void Delete(int id)
        {
            var webDepiction = repository.GetOne<WebDepiction>(id);
            AuthorizeDepictionPermissions(webDepiction);

            using (ITransaction transaction = repository.BeginTransaction())
            {
                repository.Delete(webDepiction);
                transaction.Commit();
            }

            Response.Redirect("~/Depictions");
        }

        private void AuthorizeDepictionPermissions(WebDepiction depictionToAuthorize)
        {
            if(User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("admin") || Convert.ToInt32(User.Identity.Name) == depictionToAuthorize.User.Id)
                    return;
            }

            throw new NotDepictionOwnerException();
        }

        #region Nested type: NotDepictionOwnerException

        private class NotDepictionOwnerException : Exception
        {
            public NotDepictionOwnerException() : base("User does not have permission to edit the depiction") {}
        }

        #endregion


    }
}