using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Views.Addin;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class AddinController : Controller
    {
        private readonly IRepository repository;

        public AddinController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.GetAll<AddinInformation>());
        }

        public ActionResult New()
        {
            return View("Edit", new EditAddinInformationData { AddinToEdit = new AddinInformation { ValidWithVersions = new List<DepictionVersion>(), Regions = new List<RegionDefinition>(), RelatedQuickstartItems = new List<QuickstartData>()}, DepictionVersions = repository.GetAll<DepictionVersion>(), RegionDefinitions = repository.GetAll<RegionDefinition>()});
        }

        public void Save(int id)
        {
            using(var transaction = repository.BeginTransaction())
            {
                var addinToSave = repository.GetOne<AddinInformation>(id) ?? new AddinInformation();

                addinToSave.FriendlyName = Request.Form["FriendlyName"];
                addinToSave.AddinType = (AddinType)Enum.Parse(typeof(AddinType), Request.Form["AddinType"]);
                addinToSave.AddinDownloadLocation = Request.Form["AddinDownloadLocation"];
                addinToSave.Name = Request.Form["Name"];

                addinToSave.ValidWithVersions = new List<DepictionVersion>();

                if (Request.Form["DepictionVersions"] != null)
                {
                    var versions = Request.Form["DepictionVersions"].Split(',');

                    foreach (var version in versions)
                    {
                        addinToSave.ValidWithVersions.Add(repository.GetOne<DepictionVersion>(Convert.ToInt32(version)));
                    }
                }

                addinToSave.Regions = new List<RegionDefinition>();

                if(Request.Form["Regions"] != null)
                {
                    var regions = Request.Form["Regions"].Split(',');

                    foreach (var region in regions)
                    {
                        addinToSave.Regions.Add(repository.GetOne<RegionDefinition>(Convert.ToInt32(region)));
                    }
                }

                repository.Save(addinToSave);

                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }

        public ActionResult Edit(int id)
        {
            return View(new EditAddinInformationData { AddinToEdit = repository.GetOne<AddinInformation>(id), DepictionVersions = repository.GetAll<DepictionVersion>(), RegionDefinitions = repository.GetAll<RegionDefinition>() });
        }

        public void Delete(int id)
        {
            using(var transaction = repository.BeginTransaction())
            {
                repository.Delete(repository.GetOne<AddinInformation>(id));
                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }

        public ActionResult AddQuickstart(int id)
        {
            return View(repository.GetOne<AddinInformation>(id));
        }

        public ActionResult EditQuickstart(int id)
        {
            return View(repository.GetOne<QuickstartData>(id));
        }

        public void SaveQuickstartAddition(int id)
        {
            var addinToSave = repository.GetOne<AddinInformation>(id);
            var quickStartItemToSave = new QuickstartData {
                QuickstartName = Request.Form["QuickstartName"],
                Description = Request.Form["Description"],
                //URL = Request.Form["URL"],
            };

            var parameters = Request.Form["Parameters"].Replace("][", "|").Split('|');
            quickStartItemToSave.Parameters = new List<QuickstartParameter>();

            foreach (var parameter in parameters)
            {
                var stringValue = parameter.Trim(new[] {'[', ']'}).Split(',');
                if (stringValue.Length != 2) continue;
                quickStartItemToSave.Parameters.Add(new QuickstartParameter {
                    ParameterName = stringValue[0],
                    ParameterValue = stringValue[1]
                });
            }

            addinToSave.RelatedQuickstartItems.Add(quickStartItemToSave);

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(quickStartItemToSave);
                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }


        //did a little much copy paste, this and the add method should probably share code(jason)
        [ValidateInput(false)]
        public void SaveQuickstartEdit(int id)
        {
            var quickstartData = repository.GetOne<QuickstartData>(id);

            quickstartData.QuickstartName = Request.Form["QuickstartName"];
            quickstartData.Description = Request.Form["Description"];
            //quickstartData.URL = Request.Form["URL"];
            var parameters = Request.Form["Parameters"].Replace("\r\n","").Replace("][", "|").Split('|');
            quickstartData.Parameters = new List<QuickstartParameter>();

            foreach (var parameter in parameters)
            {
                var stringValue = parameter.Trim(new[] { '[', ']' }).Split(',');
                if (stringValue.Length != 2) continue;
                quickstartData.Parameters.Add(new QuickstartParameter
                {
                    ParameterName = stringValue[0],
                    ParameterValue = stringValue[1]
                });
            }

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(quickstartData);
                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }

        public void RemoveQuickstartItem(int id)
        {
            using (var transaction = repository.BeginTransaction())
            {
                repository.Delete(repository.GetOne<QuickstartData>(id));
                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }

        public void DeleteQuickstartItem(int id)
        {
            using (var transaction = repository.BeginTransaction())
            {
                repository.Delete(repository.GetOne<QuickstartData>(id));
                transaction.Commit();
            }

            Response.Redirect("~/Addin");
        }


    }
}