using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers;
using Depiction.Web.Views.SignUp;
using Depiction.Web.Content;
using NHibernate;

namespace Depiction.Web.Controllers
{
    public class SignUpController : Controller
    {
        private readonly IRepository repository;

        public SignUpController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            var registrationViewData = new RegisterViewData();

            Lookup[] lookups = repository.Search<Lookup>(l => l.LookupCategory.Category == "Market Segment" && l.Status == 0).ToArray();

            var marketSegments = new List<SelectOption> {
                new SelectOption {
                    Label = Strings.SelectOne,
                    Value = string.Empty,
                    Selected = true
                }
            };

            foreach (Lookup lookup in lookups)
                marketSegments.Add(new SelectOption {
                    Value = lookup.Id.ToString(),
                    Label = lookup.Value
                });

            registrationViewData.MarketSegments = marketSegments.ToArray();

            registrationViewData.Titles = new[] {new SelectOption {
                Label = "Mrs."
            }, new SelectOption {
                Label = "Ms."
            }, new SelectOption {
                Label = "Mr.",
                Selected = true
            }, new SelectOption {
                Label = "Dr."
            }};

            return View(registrationViewData);
        }

        public void Register()
        {
            var registration = new TrialRegistration {
                RegistrationDate = DateTime.Now,
                Registrant = new Person()
            };

            UpdateModel(registration, new[] {"AdditionalInformation", "Organization"});
            UpdateModel(registration.Registrant, new[] {"FirstName", "LastName", "Email", "Title"});
            registration.MarketSegment = repository.GetOne<Lookup>(Convert.ToInt32(Request.Form["MarketSegment"]));

            bool activationCodeIsUnique = false;

            while (!activationCodeIsUnique)
            {
                registration.ActivationCode = GenerateActivationCode();
                activationCodeIsUnique = !repository.Queryable<TrialRegistration>().Any(r => r.ActivationCode == registration.ActivationCode);
            }

            using (ITransaction transaction = repository.BeginTransaction())
            {
                repository.Save(registration.Registrant);
                repository.Save(registration);
                transaction.Commit();
            }

            // Send out a confirmation email ...
            var confirmationMessage = new MailMessage("info@depiction.com", registration.Registrant.Email);
            confirmationMessage.Subject = "Thank you for signing up for the trial version of Depiction!";

            string nonHtml = string.Format(@"
Thank you for signing up for the trial version of Depiction!\r\n\r\n
Your trial activation code is:\r\n
{0}\r\n\r\n
The trial version of Depiction can be downloaded at:\r\n
http://www.depiction.com/trial/download\r\n\r\n
We want your experience with Depiction ""What if?"" mapping software to be successful and enjoyable. We've provided resources to help you get up to speed quickly. In the community page at http://www.depiction.com/community you'll find tutorial videos, a forum for users and staff, FAQ, our blog, and samples. We also have weekly webinars (http://www.depiction.com/webinars) with varying topics, starting with Depiction 101: The Basics. Additional resources are available in the Help section of the application, including step-by-step written instructions with screen captures and a glossary.\r\n\r\n
Thanks,\r\n
The Depiction Team 
", registration.ActivationCode);

            confirmationMessage.Body = nonHtml;

            string html = string.Format(@"
<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd"">
<html>
    <head>
		<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">
		<title>Thank you for signing up for the trial version of Depiction!</title>
		<style type=""text/css"">
			body
			{{
				font-family: Tahoma, ""Arial Black"", sans-serif;
				font-size: medium;
			}}
			
			h1
			{{
				font-size: 1.5em;
				color: #FF6700;
			}}
			
            h2
            {{
            	clear: both;
            	border: 1px solid #777;
            	display: block;
				font-size: 1.6em;
                font-weight: normal;
				float: left;
				padding: 6px;
				margin: 0 0 10px;
				color: #2852DB;
            }}
			
			h3
			{{
				font-size: .9em;
				font-weight: normal;
				color: #2852DB;
				clear: both;
			}}
			
			p
			{{
				font-size: .8em;
				clear: both;
				line-height: 2em;
			}}
			
			a
			{{
				color: #2852DB;
				text-decoration: none;
			}}
			
			a:hover
			{{
				text-decoration: underline;
			}}

            a.clickHereButton
            {{
                background-color: #FDD108;
                color: Black;
                display: block;
                float: left;
                clear: both;
                text-decoration: none;
                border: 3px solid black;
                font-weight: bold;
                font-size: .8em;
                padding: 0;
                margin: 0 0 20px 0;
                cursor: pointer;
            }}

            a.clickHereButton span.action
            {{
                float: left;
                padding: 4px;
                font-size: 1.5em;
            }}

            a.clickHereButton span.callOut
            {{
                float: left;
                display: block;
                background-color: Black;
                color: #FDD108;
                width: 4em;
                text-align: center;
                font-size: .9em;
                padding: 2px;
                margin: 0 0 0 4px;
            }}
        </style>
    </head>
    <body>
    	<img src=""http://www.depiction.com/files/DepictionWebLogo_v8.jpg"" alt=""Depiction is more than mapping"">
        <h1>Thank you for signing up for the trial version of Depiction!</h1>
        <h3>Your trial activation code is:</h3>
        <h2>{0}</h2>
		<a class=""clickHereButton"" href=""http://www.depiction.com/trial/download""><span class=""action"">Download the Trial</span><span class=""callOut"">Click Here</span></a>
		<p>
			We want your experience with Depiction ""What if?"" mapping software to be successful and enjoyable.
			We've provided resources to help you get up to speed quickly.
			In the <a href=""http://www.depiction.com/community"">community page at depiction.com</a> you'll find tutorial videos, a forum for users and staff, FAQ, our blog, and samples.
			We also have weekly <a href=""http://www.depiction.com/webinars"">webinars</a> with varying topics, starting with <span style=""font-style: bold;"">Depiction 101:</span> <span style=""font-style: italic;"">The Basics</span>.
			Additional resources are available in the Help section of the application, including step-by-step written instructions with screen captures and a glossary.
		</p>
		<p>
			Thanks,<br />
			The Depiction Team
		</p>
    </body>
</html>
", registration.ActivationCode);

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
            confirmationMessage.AlternateViews.Add(htmlView);

            var smtp = new SmtpClient("mail.depiction.com");
            smtp.Credentials = new NetworkCredential("info", "900_spg");
            smtp.Send(confirmationMessage);

            // Redirect to the confirmation so people can't refresh the
            // page to generate unlimited keys ...
            Response.Redirect("/SignUp/Confirmation");
        }

        public ActionResult Confirmation()
        {
            return View();
        }

        public static string GenerateActivationCode()
        {
            // The first part of our activation code correlates to the minute
            // they registered
            TimeSpan timeSpan = DateTime.Today - new DateTime(2000, 1, 1);
            string activationCode = Convert.ToInt32(timeSpan.TotalMinutes).ToString("X");

            // The second part of our activation code is randomized from a guid
            string[] guidParts = Guid.NewGuid().ToString().Split('-');

            foreach (string part in guidParts)
                activationCode += string.Format("-{0}", part.Substring(0, 4).ToUpper());

            return activationCode.ToUpper();
        }
    }
}