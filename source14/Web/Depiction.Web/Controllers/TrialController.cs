using System.Web.Mvc;

namespace Depiction.Web.Controllers
{
    public class TrialController : Controller
    {
        public void Index()
        {
            Response.Redirect("~/SignUp");
        }

        public void Confirmation()
        {
            Response.Redirect("~/SignUp");
        }
    }
}