using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class MarketSegmentController : Controller
    {
        private readonly IRepository repository;

        public MarketSegmentController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Edit(int id)
        {
            var lookup = repository.GetOne<Lookup>(id) ?? new Lookup();
            return View(lookup);
        }

        public void Save(int id)
        {
            Request.Form.Validate();

            var segmentToSave = repository.GetOne<Lookup>(id) ?? new Lookup {
                LookupCategory = Enumerable.SingleOrDefault(repository.Queryable<LookupCategory>().Where(l => l.Category == "Market Segment"))
            };

            segmentToSave.Value = Request.Form["ParameterValue"];

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(segmentToSave);
                transaction.Commit();
            }

            Response.Redirect("~/Admin/MarketSegments");
        }

        public void Disable(int id)
        {
            var segmentToSave = repository.GetOne<Lookup>(id);

            segmentToSave.Status = 1;

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(segmentToSave);
                transaction.Commit();
            }

            Response.Redirect("~/Admin/MarketSegments");
        }

        public void Enable(int id)
        {
            var segmentToSave = repository.GetOne<Lookup>(id);

            segmentToSave.Status = 0;

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(segmentToSave);
                transaction.Commit();
            }

            Response.Redirect("~/Admin/MarketSegments");
        }
    }
}