using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Xml;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;

namespace Depiction.Web.Controllers
{
    public class DMLController : Controller
    {
        private readonly IRepository repository;

        public DMLController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.GetAll<DMLDefinition>());
        }

        public ActionResult Define()
        {
            return View();
        }

        public void SubmitDML()
        {
            if(Request.Files.Count == 1)
            {
                var file = Request.Files["DMLFile"];

                if(file.ContentLength > 0)
                {
                    if (!Directory.Exists(HttpContext.Server.MapPath("/UploadedDMLFiles")))
                        Directory.CreateDirectory(HttpContext.Server.MapPath("/UploadedDMLFiles"));

                    var filePath = Path.Combine(HttpContext.Server.MapPath("/UploadedDMLFiles"), file.FileName);
                    file.SaveAs(filePath);

                    using (var ms = System.IO.File.OpenRead(filePath))
                    {
                        var document = new XmlDocument();
                        document.Load(ms);

                        XmlNode elementNode = document.SelectSingleNode("/depictionElement");

                        var element = ParseDMLFile(elementNode);
                        element.DMLDownloadPath = HttpContext.Request.Url.Scheme + "//" + HttpContext.Request.Url.Authority + "/UploadedDMLFiles/" + file.FileName;

                        using(var transaction = repository.BeginTransaction())
                        {
                            foreach (var dmlProperty in element.Properties)
                            {
                                repository.Save(dmlProperty);
                            }

                            repository.Save(element);
                            transaction.Commit();
                        }
                    }
                }
            }

            Response.Redirect("~/DML");
        }

        private static DMLDefinition ParseDMLFile(XmlNode elementNode)
        {
            var dmlDefinition = new DMLDefinition();
            var properties = new List<DMLProperty>();

            dmlDefinition.DMLTypeName = elementNode.Attributes["elementType"].Value;
            dmlDefinition.DMLDisplayName = elementNode.Attributes["displayName"].Value;

            XmlNodeList propertyNodes = elementNode.SelectNodes("./properties/property");

            if (propertyNodes != null)
            {
                foreach (XmlElement node in propertyNodes)
                {
                    var name = node.Attributes["name"].Value;
                    var displayName = String.Empty;
                    if (node.Attributes["displayName"] != null)
                        displayName = node.Attributes["displayName"].Value;

                    string outType = node.Attributes["typeName"].Value;
                    string nodeStringValue = node.Attributes["value"].Value;

                    bool visible = true;
                    bool editable = true;

                    if (node.Attributes["visible"] != null)
                        visible = Convert.ToBoolean(node.Attributes["visible"].Value);
                    if (node.Attributes["editable"] != null)
                        editable = Convert.ToBoolean(node.Attributes["editable"].Value);

                    if(visible && editable)
                        properties.Add(new DMLProperty { DisplayName = displayName, PropertyName = name, ValueType = outType, DefaultValue = nodeStringValue });
                }
            }

            dmlDefinition.Properties = properties;

            return dmlDefinition;
        }
    }
}