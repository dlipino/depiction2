using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Views.Admin;
using NHibernate;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IRepository repository;

        public AdminController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View(repository.GetAll<User>());
        }

        public ActionResult MarketSegments()
        {
            var segmentCounts = repository.Queryable<TrialRegistration>().GroupBy(tr => tr.MarketSegment.Id).Select(g => new {
                lookupid = g.Key,
                usages = g.Count()
            }).ToArray();

            var segments = repository.Queryable<Lookup>().Where(l => l.LookupCategory.Category == "Market Segment").ToArray();

            MarketSegmentInfo[] marketSegments = segments.GroupJoin(segmentCounts, l => l.Id, a => a.lookupid, (l, a) => new
            {
                LookupId = l.Id,
                MarketSegment = l.Value,
                Count = a.Count() == 1 ? a.First().usages : 0,
                LookupStatus = l.Status
            }).Select(l => new MarketSegmentInfo {
                Id = l.LookupId,
                MarketSegment = l.MarketSegment,
                Status = l.LookupStatus,
                UsageCount = l.Count
            }).ToArray();

            return View(marketSegments);
        }

        public ActionResult GenerateActivationCodes()
        {
            return View();
        }

        public ActionResult GeneratedCodes()
        {
            var codes = new List<string>();
            var howMany = Convert.ToInt32(Request.Form["howMany"]);

            for (int i = 0; i < howMany; i++)
            {
                var registration = new TrialRegistration
                {
                    RegistrationDate = DateTime.Now,
                    Registrant = new Person()
                };

                registration.AdditionalInformation = string.Empty;
                registration.Organization = string.Empty;
                registration.MarketSegment = repository.Queryable<Lookup>().First(l => l.Value == "Other");

                registration.Registrant.FirstName= "trial cd";
                registration.Registrant.LastName = "trial cd";
                registration.Registrant.Email = "trialuser@depiction.com";

                bool activationCodeIsUnique = false;

                while (!activationCodeIsUnique)
                {
                    registration.ActivationCode = SignUpController.GenerateActivationCode();
                    activationCodeIsUnique = !repository.Queryable<TrialRegistration>().Any(r => r.ActivationCode == registration.ActivationCode);
                }

                using (var transaction = repository.BeginTransaction())
                {
                    repository.Save(registration.Registrant);
                    repository.Save(registration);
                    transaction.Commit();
                }

                codes.Add(registration.ActivationCode);
            }

            return View(codes.ToArray());
        }

        public ActionResult IndicatePurchases()
        {
            return View();
        }

        public ActionResult PurchasesIndicated()
        {
            var commaDelimitedEmailAddresses = Request.Form["emailAddresses"];

            var emailAddresses = commaDelimitedEmailAddresses.Split(',');

            using (ITransaction transaction = repository.BeginTransaction())
            {
                foreach (var emailAddress in emailAddresses)
                {
                    var registrations = repository.Queryable<TrialRegistration>().Where(t => t.Registrant.Email == emailAddress.Trim());

                    foreach (var registration in registrations)
                    {
                        if (registration == null) continue;
                        registration.Purchased = true;
                        repository.Save(registration);
                    }
                }

                transaction.Commit();
            }

            return View();
        }
    }
}