﻿using Depiction.Web.Helpers.Interfaces;

namespace Depiction.Web.Tests.MockObjects
{
    internal class MockAuthorizer : IAuthorizer
    {
        #region IAuthorizer Members

        public void DoAuthorize(string user, bool authorized) {}

        #endregion
    }
}