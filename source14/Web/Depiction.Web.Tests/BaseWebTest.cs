﻿using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac.Builder;
using Autofac.Integration.Web;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers.Interfaces;
using Depiction.Web.Tests.MockObjects;
using FluentNHibernate;
using FluentNHibernate.Cfg;
using Moq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace Depiction.Web.Tests
{
    public abstract class BaseWebTest
    {
        protected ContainerProvider containerProvider;

        #region Setup/Teardown

        [SetUp]
        public void Setup()
        {
            var configuration = new Configuration();
            new SQLiteConfiguration().InMemory().ConfigureProperties(configuration);
            configuration.AddAutoMappings(DBFactory.DBModel);

            var builder = new ContainerBuilder();
            builder.Register(c => configuration.BuildSessionFactory().OpenSession()).As<ISession>().ContainerScoped();
            builder.Register<BaseRepository>().As<IRepository>().ContainerScoped();
            builder.Register<MockAuthorizer>().As<IAuthorizer>().ContainerScoped();

            containerProvider = new ContainerProvider(builder.Build());

            var exporter = new SchemaExport(configuration);
            exporter.Execute(false, true, false, true, containerProvider.ApplicationContainer.Resolve<ISession>().Connection, null);

            initializeDatabase();
        }

        #endregion

        protected static ControllerContext getMockContext(NameValueCollection formData)
        {
            var mockContext = new Mock<ControllerContext>();
            mockContext.Expect(c => c.HttpContext.Request.Form).Returns(formData);
            mockContext.Expect(c => c.HttpContext.Request.QueryString).Returns(new NameValueCollection());
            mockContext.Expect(c => c.RouteData).Returns(new RouteData());
            mockContext.Expect(c => c.HttpContext.Response).Returns(new MockResponse());

            return mockContext.Object;
        }

        private void initializeDatabase()
        {
            var marketSegment = new LookupCategory {
                Category = "Market Segment"
            };

            var roles = new[] {new Role {
                RoleName = "admin"
            }, new Role {
                RoleName = "stats"
            }};

            var lookups = new[] {new Lookup {
                LookupCategory = marketSegment,
                Value = "Emergency Managers"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "First Responders"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Security and Military"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Planners"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Volunteers"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Map Users"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Student"
            }, new Lookup {
                LookupCategory = marketSegment,
                Value = "Other"
            },};

            var session = containerProvider.ApplicationContainer.Resolve<ISession>();
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.SaveOrUpdate(marketSegment);

                foreach (Lookup lookup in lookups)
                {
                    session.SaveOrUpdate(lookup);
                }

                foreach (Role role in roles)
                    session.SaveOrUpdate(role);

                transaction.Commit();
            }
        }
    }

    public class MockResponse : HttpResponseBase
    {
        public string RedirectedTo { get; private set; }

        public override void Redirect(string url)
        {
            RedirectedTo = url;
        }
    }
}