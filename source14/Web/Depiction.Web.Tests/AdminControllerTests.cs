﻿using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Controllers;
using NHibernate;
using NUnit.Framework;

namespace Depiction.Web.Tests
{
    [TestFixture]
    public class AdminControllerTests : BaseWebTest
    {
        [Test]
        public void IndexWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var adminController = new AdminController(repository);
            ActionResult result = adminController.Index();
            Assert.IsNotNull(result);
        }

        [Test]
        public void UsersWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());

            repository.Save(new User {
                Password = "blah",
                Person = new Person {
                    FirstName = "Test",
                    LastName = "Test",
                    Email = "test@test.com"
                }
            });

            var adminController = new AdminController(repository);
            ActionResult result = adminController.Users();
            Assert.IsNotNull(result);

            var registerViewData = (User[]) ((ViewResult) result).ViewData.Model;

            Assert.AreEqual(1, registerViewData.Length);
        }
    }
}