﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;

namespace AutomaticEmailService
{
    public partial class EmailService : ServiceBase
    {
        private BackgroundWorker mailThread;
        private bool serviceRunning;

        #region seven day notice email bodies

        private const string sevenDayNoticePlainText = "Your trial version of Depiction will expire soon!\r\n\r\n" +
                                                       "Depiction is a leading-edge geospatial tool that lets users mash up maps and data to create dynamic simulations. Analyze critical data at a glance, or simulate the impact of changes in the landscape. At a lower price-point than traditional geographic mapping applications, Depiction provides heavy-duty simulations in a lightweight package.\r\n\r\n" +
                                                       "Depiction is more than mapping\r\n\r\n" +
                                                       "Depiction has been used by the Salvation Army and the Red Cross, and it has been implemented to help save 15,000 animals during Hurricane Gustav (http://www.wired.com/software/coolapps/news/2009/02/disaster_modeling) \r\n\r\n" +
                                                       "Depiction is not just for emergency management. Use it to track any geo-specific data.\r\n" +
                                                       "   * Plan alternate routes to work by mapping local construction sites\r\n" +
                                                       "   * Map out road race-courses by route length, elevation gain and loss, and race checkpoints\r\n" +
                                                       "   * Chart real estate data in your neighborhood\r\n\r\n" +
                                                       "What our users are saying:\r\n\r\n" +
                                                       "   Depiction is so fast and easy to learn. Within moments I realized that I can use the software for so many purposes: vulnerability analysis, disaster assessment, scenario testing, planning, and education.\r\n" +
                                                       "   Carol Dunn\r\n" +
                                                       "   CDE Program Manager\r\n" +
                                                       "   American Red Cross\r\n\r\n" +
                                                       "   The big agencies think they are prepared, but smaller cities are just scrambling to come up with op plans, and don?t have a lot of money. For them Depiction is just perfect.\r\n" +
                                                       "   David F.<\r\n" +
                                                       "   Pierce County Animal Rescue Team\r\n\r\n" +
                                                       "Like what you see?\r\n" +
                                                       "For just $89, get the full version of Depiction so you can create and share dynamic scenario-based maps.\r\n" +
                                                       "Purchase Depiction! (http://www.depiction.com/purchase)\r\n\r\n" +
                                                       "Got questions about Depiction?\r\n" +
                                                       "Contact us at info@depiction.com! We'd love to hear from you.\r\n\r\n" +
                                                       "Depictin, Inc.\r\n" +
                                                       "More than mapping\r\n" +
                                                       "www.depiction.com\r\n" +
                                                       "To view our Privacy Policy go to http://www.depiction.com/depiction-privacy-policy";

        private readonly string sevenDayNoticeHTML;

        #endregion

        public EmailService()
        {
            var sevenDayHtmlStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("AutomaticEmailService.7DaysToTrialExpiresEmail.htm");

            if (sevenDayHtmlStream != null)
                using (var reader = new StreamReader(sevenDayHtmlStream))
                {
                    sevenDayNoticeHTML = reader.ReadToEnd();
                }

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serviceRunning = true;
            mailThread = new BackgroundWorker();
            mailThread.DoWork += mailThread_DoWork;
            mailThread.RunWorkerAsync();
        }

        void mailThread_DoWork(object sender, DoWorkEventArgs e)
        {
            while(serviceRunning)
            {
                var currentTime = DateTime.Now;

                // Run this every weekday morning between 8 and 8:15
                if(currentTime < DateTime.Today.AddHours(8))
                    Thread.Sleep(DateTime.Today.AddHours(8) - currentTime);
                if(currentTime > DateTime.Today.AddHours(8).AddMinutes(15))
                    Thread.Sleep(DateTime.Today.AddDays(1).AddHours(8) - currentTime);

                // Don't run on Saturday or Sunday
                while(DateTime.Today.DayOfWeek == DayOfWeek.Saturday || DateTime.Today.DayOfWeek == DayOfWeek.Sunday)
                    Thread.Sleep(new TimeSpan(1, 0, 0, 0));

                using(var session = DBFactory.GetSession())
                {
                    using(var repository = new BaseRepository(session))
                    {
                        var peopleToSendSevenDaysLeftEmailTo = repository.Queryable<TrialRegistration>().Where(t => !t.Purchased && !t.Registrant.Email.EndsWith("depiction.com") && t.FirstActivationDate > DateTime.Now.AddDays(-30) && t.FirstActivationDate < DateTime.Now.AddDays(-23));
                        var sevenDayEmailsSent = new List<TrialRegistration>();
                        const string sevenDayEmailLookupCategory = "SevenDaysLeftEmailSent";

                        foreach (var registration in peopleToSendSevenDaysLeftEmailTo)
                        {
                            if (repository.Queryable<Lookup>().Any(lc => lc.LookupCategory.Category == sevenDayEmailLookupCategory && lc.Value == registration.Id.ToString())) continue;
                            sendSevenDaysLeftEmail(registration.Registrant.Email);
                            sevenDayEmailsSent.Add(registration);
                        }

                        using(var transaction = session.BeginTransaction())
                        {
                            var sevenDayCategory = repository.Queryable<LookupCategory>().FirstOrDefault(lc => lc.Category == sevenDayEmailLookupCategory);

                            if(sevenDayCategory == null)
                            {
                                sevenDayCategory = new LookupCategory { Category = sevenDayEmailLookupCategory };
                                session.Save(sevenDayCategory);
                            }

                            if (sevenDayEmailsSent.Count > 0)
                            {
                                string emailBody = "The following people were sent \"Trial Expiration\" emails:\r\n";

                                foreach (var registration in sevenDayEmailsSent)
                                {
                                    var record = new Lookup {
                                        LookupCategory = sevenDayCategory,
                                        Value = registration.Id.ToString()
                                    };
                                    session.SaveOrUpdate(record);
                                    emailBody += "\r\n" + registration.Registrant.FirstName + " " + registration.Registrant.LastName + " (" + registration.Registrant.Email + ")";
                                }

                                // Send out a confirmation email ...
                                var sevenDayNotice = new MailMessage("info@depiction.com", "biz@depiction.com");
                                sevenDayNotice.Subject = "\"Trial Expiration\" emails sent";

                                sevenDayNotice.Body = emailBody;

                                var smtp = new SmtpClient("mail.depiction.com") {
                                    Credentials = new NetworkCredential("info", "900_spg")
                                };
                                smtp.Send(sevenDayNotice);
                            }
                            else
                            {
                                var nothingSentMessage = new MailMessage("info@depiction.com", "biz@depiction.com");
                                nothingSentMessage.Subject = "No \"Trial Expiration\" emails sent";

                                nothingSentMessage.Body = "The \"Trial Expiration\" email service just ran but there were no users who needed to receive the message today!";

                                var smtp = new SmtpClient("mail.depiction.com")
                                {
                                    Credentials = new NetworkCredential("info", "900_spg")
                                };
                                smtp.Send(nothingSentMessage);
                            }

                            transaction.Commit();
                        }
                    }
                }

                // Sleep 8 hours here to make sure we're moving to the next day with our sleeps above ...
                Thread.Sleep(new TimeSpan(8, 0, 0));
            }
        }

        protected override void OnStop()
        {
            serviceRunning = false;
            mailThread.Dispose();
            mailThread = null;
        }

        private void sendSevenDaysLeftEmail(string emailAddress)
        {
            // Send out a confirmation email ...
            var sevenDayNotice = new MailMessage("info@depiction.com", emailAddress);
            sevenDayNotice.Subject = "Oh no! Your Depiction trial is about to expire!";

            sevenDayNotice.Body = sevenDayNoticePlainText;

            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(sevenDayNoticeHTML, new ContentType("text/html"));
            sevenDayNotice.AlternateViews.Add(htmlView);

            var smtp = new SmtpClient("mail.depiction.com");
            smtp.Credentials = new NetworkCredential("info", "900_spg");
            smtp.Send(sevenDayNotice);
        }
    }
}