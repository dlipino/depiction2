﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace AutomaticEmailService
{
    [RunInstaller(true)]
    public class DepictionServiceInstaller : Installer
    {
        public DepictionServiceInstaller()
        {
            var processInstaller = new ServiceProcessInstaller();
            var serviceInstaller = new ServiceInstaller();

            //set the privileges
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller.DisplayName = "Depiction Marketing Email Service";
            serviceInstaller.StartType = ServiceStartMode.Manual;

            //must be the same as what was set in Program's constructor
            serviceInstaller.ServiceName = "Depiction Marketing Email Service";

            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}