﻿using System.Windows.Controls;
using Depiction.API.DialogBases.TextConverters;
using Depiction.Converters.MultiValueConverters;
using Depiction.Converters.ValueConverters;

namespace Depiction.Converters
{
    public static class DepictionConverters
    {
        public static readonly StringToPointConverter StringToPoint = new StringToPointConverter();
        public static readonly StringToLatitudeLongitudeConverter StringToLatitudeLongitude = new StringToLatitudeLongitudeConverter();
        public static readonly ShiftGeometryToOriginConverter ShiftGeometryToOrigin = new ShiftGeometryToOriginConverter();
        public static readonly ShapeTypeToVisibilityConverter ShapeTypeToVisibility = new ShapeTypeToVisibilityConverter();
        public static readonly ShapeTypeToBooleanConverter ShapeTypeToBoolean = new ShapeTypeToBooleanConverter();
        public static readonly NumberTimesParameterConverter NumberTimesParameter = new NumberTimesParameterConverter();
        public static readonly LatLongToMapPixelCoordinatesConverter LatLongToMapPixelCoordinates = new LatLongToMapPixelCoordinatesConverter();
        public static readonly InverseOfVariableTimesParameterConverter InverseOfVariableTimesParameter = new InverseOfVariableTimesParameterConverter();
        public static readonly ElementColorModeToBoolConverter ElementColorModeToBool = new ElementColorModeToBoolConverter();
        public static readonly DepictionDisplayerTypeToBoolConverter DepictionDisplayerTypeToBool = new DepictionDisplayerTypeToBoolConverter();
        public static readonly DepictionIntConverter DepictionInt = new DepictionIntConverter();
        public static readonly DepictionIMeasurementToDoubleStringConverter DepictionIMeasurementToDoubleString = new DepictionIMeasurementToDoubleStringConverter();
        public static readonly DepictionIconPathToImageSourceConverter DepictionIconPathToImageSource = new DepictionIconPathToImageSourceConverter();
        public static readonly DepictionDoubleConverter DepictionDouble = new DepictionDoubleConverter();
        public static readonly DepictionAngleToDoubleConverter DepictionAngleToDouble = new DepictionAngleToDoubleConverter();
        public static readonly BooleanToOpacityConverter BooleanToOpacity = new BooleanToOpacityConverter(false);
        public static readonly BooleanInverterConverter BooleanInverter = new BooleanInverterConverter();
        public static readonly BooleanToVisibilityConverter BooleanToVisibility = new BooleanToVisibilityConverter();

        public static readonly PlainTextToFlowDocumentConverter PlainTextToFlowDocument = new PlainTextToFlowDocumentConverter();
        public static readonly InputStringToFlowDocumentConverter InputStringToFlowDocument = new InputStringToFlowDocumentConverter();
        public static readonly HtmlToFlowDocumentConverter HtmlToFlowDocument = new HtmlToFlowDocumentConverter();

        //Multi convertesr
        public static readonly MultiBooleanToVisibilityConverter MultiBooleanToVisibilityUsingOrLogic = new MultiBooleanToVisibilityConverter(false);
        public static readonly MultiEqualsConverter MultiEquals = new MultiEqualsConverter();
        public static readonly NumberPlusParameterDivTwoConverter NumberPlusParameterDivTwo = new NumberPlusParameterDivTwoConverter();
    }
}
