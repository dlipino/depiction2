using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Depiction.Converters.MultiValueConverters
{
    /// <summary>
    /// Returns true if every input element is the same as the others.
    /// </summary>
    public class MultiEqualsConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return  values.Distinct().Count() <= 1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}