﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction.Converters.MultiValueConverters
{
    public class NumberPlusParameterDivTwoConverter : IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double inValue = 0;
            double inWidth = 0;
            var paramString = "0";
            if (values.Length == 2)
            {
                if (values[1] != null) paramString = values[1].ToString();
                if (!double.TryParse(values[0].ToString(), NumberStyles.Number, culture, out inValue) ||
                    !double.TryParse(paramString, NumberStyles.Number, culture, out inWidth))
                    return inValue;
            }
            return inValue + inWidth / 2d;

        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            return new object[2];
        }
    }
}