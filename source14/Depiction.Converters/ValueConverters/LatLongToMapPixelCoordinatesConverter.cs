using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction.API;
using Depiction.API.ValueTypes;

namespace Depiction.Converters.ValueConverters
{
    public class LatLongToMapPixelCoordinatesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var latLong = value as LatitudeLongitude;
            if (latLong != null)
            {
                return DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(latLong);
            }
            return new Point();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is Point)
            {
                return DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld((Point)value); 
            }
            return new LatitudeLongitude();
        }
    }
}