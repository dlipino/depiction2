using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction.Converters.ValueConverters
{
    public class InverseOfVariableTimesParameterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double inValue;
            double inParam = 1;
            if(parameter != null)
            {
                if(!double.TryParse(parameter.ToString(), NumberStyles.Number, culture, out inParam))
                {
                    inParam = 1;
                }
            }
            if (!double.TryParse(value.ToString(), NumberStyles.Number, culture, out inValue))
                return value;
            return (1d/inValue) * inParam;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}