using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) return value.ToString();
            int intValue = 0;
            if (value != null) intValue = (int)value;
            return string.Format("{0:f0}", intValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int result;
            if (int.TryParse((string)value, NumberStyles.Number, culture, out result))
                return result;
//            double dResult;
//            if (double.TryParse((string)value, NumberStyles.Number, culture, out dResult))
//                return (int)dResult;
            return value;
        }
    }
}