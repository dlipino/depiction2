﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Depiction.Converters.ValueConverters
{
    public class StringToPointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {//Hmm this might not work with european numbers
            var stringLoc = value.ToString();
            var worldStart = new Point();
            var parsed = stringLoc.Split(',');
            if (parsed.Length == 2)
            {
                double x;
                double.TryParse(parsed[0], out x);
                double y;
                double.TryParse(parsed[1], out y);
                worldStart = new Point(x,y);
            }
            return worldStart;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }
    }
}