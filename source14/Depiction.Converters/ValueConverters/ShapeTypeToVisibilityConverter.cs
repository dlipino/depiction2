﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.Converters.ValueConverters
{
    public class ShapeTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is RevealerShapeType)
            {
                var shape = (RevealerShapeType)value;
                if (shape.Equals(RevealerShapeType.Circle))
                {
                    return Visibility.Collapsed;
                }
                if (shape.Equals(RevealerShapeType.Rectangle))
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
            {
                var shape = (Visibility)value;
                if (shape.Equals(Visibility.Visible))
                {
                    return RevealerShapeType.Rectangle;
                    
                }
                return  RevealerShapeType.Circle;
            }
            return RevealerShapeType.Rectangle;
        }
    }
}
