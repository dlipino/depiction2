using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.Properties;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is double)) return value.ToString();
            double doubleValue = (double) value;
            var precision = Settings.Default.Precision;
            if (doubleValue % 1 == 0) precision = 0;
            return string.Format(string.Format("{{0:f{0}}}", precision), doubleValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double result;
            if (double.TryParse((string)value, NumberStyles.Number, culture, out result))
                return result;
            return value;
        }
    }
}