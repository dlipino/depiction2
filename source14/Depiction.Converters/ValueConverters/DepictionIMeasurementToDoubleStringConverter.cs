using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.Properties;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionIMeasurementToDoubleStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var measure = value as IMeasurement;
            if (measure == null) return string.Empty;
            var doubleValue = measure.NumericValue;
            return string.Format(string.Format("{{0:N{0}}}", Settings.Default.Precision), doubleValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double result;
            if (double.TryParse((string)value, NumberStyles.Number, culture, out result))
                return result;
            return double.NaN;
        }
    }
}