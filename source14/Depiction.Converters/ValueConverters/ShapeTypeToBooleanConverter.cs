﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.Converters.ValueConverters
{
    public class ShapeTypeToBooleanConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is RevealerShapeType)
            {
                var shape = (RevealerShapeType)value;
                if (shape.Equals(RevealerShapeType.Circle))
                {
                    return false;
                }
                if (shape.Equals(RevealerShapeType.Rectangle))
                {
                    return true;
                }
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                var shape = (bool)value;
                if (shape)
                {
                    return RevealerShapeType.Rectangle;
                    
                }
                return  RevealerShapeType.Circle;
            }
            return RevealerShapeType.Rectangle;
        }
    }
}
