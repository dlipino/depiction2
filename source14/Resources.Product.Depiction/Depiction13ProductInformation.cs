﻿namespace Resources.Product.Depiction
{
    public class Depiction13ProductInformation : DepictionProductInformation
    {
        override public string DirectoryNameForCompany { get { return "Depiction_13"; } }
        public override string ProcessName { get { return "Depiction_13"; } }
    }
}