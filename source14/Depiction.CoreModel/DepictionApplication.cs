﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using Depiction.API;
using Depiction.API.DepictionConfiguration;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.Properties;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.Service;

namespace Depiction.CoreModel
{
    public class DepictionApplication : Application, IDepictionApplication
    {
        public event Action<IDepictionStory, IDepictionStory> DepictionChanged;
        private BackgroundWorker prototypeLibraryLoader;
        private WorldOutlineModel worldOutlines;

        RasterImageResourceDictionary appImageResources = new RasterImageResourceDictionary();
        private IDepictionStory currentDepiction;

        //public TileBackgroundService TilingService { get; private set; }
        public ITiler CurrentTileProvider { get; set; }
        public IElementPrototypeLibrary prototypeLibrary;
        public AddinRepository addinRepository;
        public DepictionSaveLoadManager saveLoadManager = new DepictionSaveLoadManager();

        //Interactions need to go somewhere
        #region Properties

        public RasterImageResourceDictionary ImageResources
        {
            get { return appImageResources; }
        }
        public WorldOutlineModel WorldOutlines { get { return worldOutlines; } }

        public IAddinRepository Repository
        {
            get { return AddinRepository.Instance; }
        }
        public DepictionSaveLoadManager DepictionSaveLoadManager
        {
            get { return saveLoadManager; }
        }
        public IApplicationPathService PathService { get; set; }
        public bool ContinueApplicationRequest { get; set; }//Used with modal dialogs
        public IDepictionStory CurrentDepiction { get { return currentDepiction; } }
        #endregion

     //Hack for updating hover text measurement string and the element property box
        void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (currentDepiction == null) return;
            //Apparently the save notifies all the property changes, even if they don't actually change
            if (e.PropertyName.Equals("measurementScale", StringComparison.InvariantCultureIgnoreCase) ||
                e.PropertyName.Equals("measurementSystem", StringComparison.InvariantCultureIgnoreCase))
            {
                foreach (var element in currentDepiction.CompleteElementRepository.AllElements)
                {
                    element.UpdateToolTip();
                    foreach (var prop in element.OrderedCustomProperties)
                    {
                        prop.RefreshValue();
                    }
                }
            }
        }

        protected void StartDepictionAppServices()
        {
            //TilingService = new TileBackgroundService(this);
            PathService = new ApplicationPathService();

            DepictionAccess.PathService = PathService;//This cleans up itself
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            DepictionAccess.GeoCodingService.LoadGeneralGeoCodeCache();
            //The config has to be set up after the addons because it currently uses one of the default addons
            //Later version should clean up the App config, please
            DepictionUserConfigurationManager.CreateDepictionUserConfigFile();//this has issues with addons

            Current.Resources.MergedDictionaries.Add(appImageResources);

            saveLoadManager.DepictionLoadCompleted += saveLoadManager_DepictionLoadCompleted;
            saveLoadManager.DepictionSaveCompleted += saveLoadManager_DepictionSaveCompleted;

            ContinueApplicationRequest = true;
            
            //SetPrototypeLibraryFromBackground();
            worldOutlines = new WorldOutlineModel();

            //Hackish
            Settings.Default.PropertyChanged += Default_PropertyChanged;
            //Epic timing hack. This should be the last Exit event called since this
            //calls the ultimate process kill.
            Exit += UltimateDepictionApplication_Exit;
        }

        void UltimateDepictionApplication_Exit(object sender, ExitEventArgs e)
        {
            if (DepictionAccess.GeoCodingService != null)
            {
                DepictionAccess.GeoCodingService.SaveGeneralGeoCodeCache();
            }
            Settings.Default.PropertyChanged -= Default_PropertyChanged;

            saveLoadManager.DepictionLoadCompleted -= saveLoadManager_DepictionLoadCompleted;
            saveLoadManager.DepictionSaveCompleted -= saveLoadManager_DepictionSaveCompleted;
            //Sad, very very sad
            Process.GetCurrentProcess().Kill();
        }
        public void SetNewDepictionStory(DepictionStory newDepiction)
        {
            Debug.WriteLine("Canceling interaciton runner etc");
            if (currentDepiction != null)
            {
                // DepictionAccess.CurrentDepiction.ElementListChange -= CurrentDepiction_ElementListChange;
                DepictionAccess.InteractionsRunner.CancelAsync();

                Current.Resources.MergedDictionaries.Remove(currentDepiction.ImageResources);
            }
            Debug.WriteLine("Merging resources");
            if (newDepiction != null)
            {
                Current.Resources.MergedDictionaries.Add(newDepiction.ImageResources);
            }
            if (newDepiction != null)
            {

                Debug.WriteLine("Starting new interaction runner");
                //race condition between interaction runner and the interaction library, for now we need all the interactions present
                //before the router is created because of the interactiongraph and its setup.
                newDepiction.InteractionRuleRepository.LoadRules(DepictionAccess.InteractionsLibrary.MergedInteractions);
                //set up the interaction engine
                DepictionAccess.InteractionsRunner = new Router(newDepiction.CompleteElementRepository,newDepiction.InteractionRuleRepository);
                DepictionAccess.InteractionsRunner.BeginAsync();
            }
            Debug.WriteLine("Invoking change in depiction");
            if (DepictionChanged != null)
            {
                DepictionChanged(currentDepiction, newDepiction);
            }
            Debug.WriteLine("Setting new depiction");
            currentDepiction = newDepiction;
            if (currentDepiction != null) currentDepiction.DepictionNeedsSaving = false;
        }
        
        public void SetPrototypeLibraryFromBackground()
        {
            prototypeLibraryLoader = new BackgroundWorker();
            prototypeLibraryLoader.DoWork += prototypeLibraryLoader_DoWork;
            prototypeLibraryLoader.RunWorkerCompleted+=prototypeLibraryLoader_RunWorkerCompleted;
            prototypeLibraryLoader.RunWorkerAsync();
        }

        void prototypeLibraryLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (DepictionAccess.ElementLibrary == null) return;
            DepictionAccess.ElementLibrary.NotifyPrototypeLibraryChange();
        }

        protected void ComposeDepictionAddonsAndSetupTileServices()
        {
            AddinRepository.Compose();
            //TilingService.AddToAvailableTilers(AddinRepository.Instance.BackgroundTileProviders);
        }
        public void LoadElementDefinitions(string productResourceAssembly)
        {
            prototypeLibrary = DepictionAccess.ElementLibrary;//Not sure why this is here, probably so things don't get overwritten.
            //Load autodetect prototype
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory("Depiction.CoreModel", "dml", true, PathService.DepictionElementPath);
            //Load the elements for the product type
            if(string.IsNullOrEmpty(productResourceAssembly))
            {
                productResourceAssembly = "Resources.Product.Depiction";
            }
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(productResourceAssembly, "dml", true, PathService.DepictionElementPath);

            prototypeLibrary.SetDefaultPrototypesFromPath(PathService.DepictionElementPath, false);
            prototypeLibrary.SetUserPrototypesFromPath(PathService.UserElementsDirectoryPath, false);
            prototypeLibrary.SetAddinPrototypesAndIcons(false);
            
        }
        void prototypeLibraryLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadElementDefinitions(DepictionAccess.ProductInformation.ResourceAssemblyName);
        }
        #region Save/Load complete things
        public void SaveCurrentDepictionWithFileName(string fileName)
        {
            saveLoadManager.SaveDepiction(fileName, currentDepiction);
        }
        public bool CanSaveCurrentDepiction()
        {
            if (currentDepiction != null) return true;
            return false;
        }
        public void LoadADepictionFromFileName(string fileName)
        {
            saveLoadManager.LoadDepiction(fileName);
        }
        void saveLoadManager_DepictionLoadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Load Cancelled ");
            }
            else if (e.Error != null)
            {
                Console.WriteLine("Load error " + e.Error.Message);
            }
            else
            {
                try
                {
                    var loadedStory = e.Result as DepictionStory;
                    if (loadedStory != null)
                    {
                        var story = loadedStory;
                        SetNewDepictionStory(story);
                    }
                }
                catch (Exception ex) { }
            }
        }
        void saveLoadManager_DepictionSaveCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                Console.WriteLine("Save Cancelled ");
            }
            else if (e.Error != null)
            {
                Console.WriteLine("Save error " + e.Error.Message);
            }
        }

        #endregion
    }
}