using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows;
using Depiction.CoreModel.DepictionObjects;

namespace Depiction.VisualModel.Entity
{
    public class StoryPersistence : IDepictionPersistence
    {
        // THESE TWO VARIABLES MUST BE INCREMENTED TOGETHER! basically this is the 8th file format change.
        public const int CurrentDepictionFileVersion = 9;
        // The Application version in which this DepictionFileVersion was introduced.
        private const string MinimumAppVersionForDepictionFileVersion = "1.2.1";
        //Variables that should set road network/elevation changes to the state they were before the
        //save since versions less than 5 did not save the change.
        public static int OriginalVersionOfCurrentLoadedDepiction { get; private set; }
        public const int VersionsThatMustRunInteractionsAfterLoading = 5;//Don't change this one
        public const int VersionThatGavePositionToShapes = 9;//8, there was a bug somewhere which made this not work;//Don't change this one
        public const int DepictionRoadgraphWithOneWay = 9; //Or this one

        static public int PercentOfOperationCompleted { get; set; }
        #region Human readable names for subtrees
        private static List<StorySubtreeInfo> GetStorySubtrees()
        {
            var subtrees = new List<StorySubtreeInfo>
                                {
                                    new StorySubtreeInfo("Story", "Depiction.VisualModel.Loaders.StorySubtreeLoader", "",
                                                         "Story.xml"),
                                    new StorySubtreeInfo("VisualData",
                                                         "Depiction.VisualModel.Loaders.VisualDataSubtreeLoader", "",
                                                         "VisualData.xml"),
                                    new StorySubtreeInfo("InteractionRules",
                                                         "Depiction.VisualModel.Loaders.InteractionRulesSubtreeLoader",
                                                         "", "InteractionRules.xml"),
                                    new StorySubtreeInfo("Images",
                                                         "Depiction.VisualModel.Loaders.RasterImagesSubtreeLoader",
                                                         "Depiction.DomainModel.Resources.RasterImagesMemento.xsd",
                                                         "Images.xml"),
                                    new StorySubtreeInfo("ElementTypes",
                                                         "Depiction.VisualModel.Loaders.ElementTypesSubtreeLoader", "",
                                                         ""),
                                    new StorySubtreeInfo("Elements", "Depiction.VisualModel.Loaders.ElementSubtreeLoader",
                                                         "", "Elements.xml"),
                                    new StorySubtreeInfo("UnregisteredElements",
                                                         "Depiction.VisualModel.Loaders.UnregisteredElementSubtreeLoader",
                                                         "", "UnregisteredElements.xml"),
                                    new StorySubtreeInfo("InteractionGraphElements",
                                                         "Depiction.VisualModel.Loaders.InteractionGraphElementsSubtreeLoader",
                                                         "", "InteractionGraphElements.xml")

                                };

            return subtrees;
        }
        #endregion

        private static ISubtreeLoader FindSubtreeLoader(string typeName)
        {
            Type type = Type.GetType(typeName);
            if (type != null)
            {
                var types = new Type[0];
                ConstructorInfo constructorInfo = type.GetConstructor(types);
                if (constructorInfo != null)
                    return constructorInfo.Invoke(null) as ISubtreeLoader;
            }
            return null;
        }

        private static string CreateArchiveDirectory()
        {
            string archiveDir = Path.Combine(DepictionAccess.ApplicationPathService.TempFolderPath, StringConstants.TempSaveDirectory);
            if (Directory.Exists(archiveDir))
                Directory.Delete(archiveDir, true);
            Directory.CreateDirectory(archiveDir);
            return archiveDir;
        }

        /// <summary>
        /// Returns story loaded from file, and sets DomainAccess.VisualData.
        /// 
        /// If fails, returns null;
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static IStory LoadStoryFromFile(string fileName, BackgroundWorker worker, DoWorkEventArgs e)
        {
            var archiveDir = Path.Combine(DepictionAccess.ApplicationPathService.TempFolderPath, StringConstants.TempSaveDirectory);
            //On every load, assume the loaded depictions comes from the current version (in case of errors)
            OriginalVersionOfCurrentLoadedDepiction = CurrentDepictionFileVersion;

            ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Decompressing file...", e);
            try
            {
                UnzipFileToDirectory(fileName, archiveDir);
            }
            catch (ZipException ex)
            {
                //DepictionAccess.NotificationService.SendNotificationDialog should not be used because this is generally used in background which does not
                //have access to main window. if we really need these error messages, they should be sent up the chain of command and used when
                //background worker is done. Lies, you can't even send it to notification
                if (ex.Message.Equals("Invalid password"))
                {
                    throw new Exception(String.Format(
                            "File \"{0}\" is not a valid {1} file.\n\nPlease select a valid {1} file.",
                            fileName, UIAccess.Product.StoryName));
                }

                throw new Exception(
                    String.Format(
                        "File \"{0}\" is not a valid {2} file.\n\nPlease select a valid {2} file.\n\nInternal error was: {1}.",
                        fileName, ex.Message, UIAccess.Product.StoryName));

            }
            var persistenceMemento = new PersistenceMemento();
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 3, string.Format("Retrieving {0} information...", UIAccess.Product.StoryName), e)) return null;
            var storyPersistenceInfo = GetStoryPersistenceInfo(archiveDir);
            if (storyPersistenceInfo == null)
            {
                throw new Exception(String.Format(
                        "File \"{0}\" is not a valid {1} file.\n\nPlease select a valid {1} file.",
                        fileName, UIAccess.Product.StoryName));
            }

            //Remember the version of the depiction that is currently getting loaded
            OriginalVersionOfCurrentLoadedDepiction = storyPersistenceInfo.DepictionFileVersion;

            if (!string.IsNullOrEmpty(storyPersistenceInfo.Author) && storyPersistenceInfo.Author == "Depiction, Inc.")
                WebServiceFacade.LogUsageMetric(MetricType.ExampleOpened, fileName.Substring(fileName.LastIndexOf('\\') + 1).MD5Hash());

            // See if we need to import from one file version to the current
            if (CurrentDepictionFileVersion < OriginalVersionOfCurrentLoadedDepiction)
            {
                // Need newer version of Depiction.
                throw new Exception(string.Format("Cannot open {2} file {0}\n\nThis file was created by a more recent version of {3}. To open it, please upgrade your {3} to at least version {1}. See Help for how to upgrade.",
                    fileName,
                    storyPersistenceInfo.MinimumAppVersionForDepictionFileVersion, UIAccess.Product.StoryName, UIAccess.Product.ProductName));
            }
            if (CurrentDepictionFileVersion > OriginalVersionOfCurrentLoadedDepiction)
            {
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 3, string.Format("Updating {0} to latest version...", UIAccess.Product.StoryName), e)) return null;
                // Update the DPN file.
                try
                {
                    archiveDir = ImportToLatestDpnVersion(archiveDir, OriginalVersionOfCurrentLoadedDepiction);
                    DepictionAccess.NotificationService.SendNotification(string.Format("This {0} file was created by an older version of {1}. When saved, it will not load on older versions.", UIAccess.Product.StoryName, UIAccess.Product.ProductName), 20);
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Could not convert {2} file \"{0}\" to current file format.\n\nInternal error: {1}",
                                      fileName, ex.Message, UIAccess.Product.StoryName));
                }
            }

            try
            {
                LoadStorySubtrees(persistenceMemento, storyPersistenceInfo.Subtrees, archiveDir, worker, e);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error opening {2} file \"{0}\"\n\nInternal error: {1}",
                                  fileName, ex.Message, UIAccess.Product.StoryName));
            }
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Retrieving visual data...", e)) return null;
            DomainAccess.VisualData = persistenceMemento.VisualData;
            DomainAccess.Router = new Router(persistenceMemento.Story.ElementRepository, persistenceMemento.Story.InteractionRuleRepository);
            DomainAccess.Router.BeginAsync();
            DomainAccess.Router.ElementRulePairs = persistenceMemento.ElementRulePairs;
            if (DomainAccess.VisualData != null)
            {

                var elements = persistenceMemento.Story.ElementRepository.GetAllElementsAndChildren();
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting backdrop...", e)) return null;
                if (DomainAccess.VisualData.PrimaryDisplayer != null)
                    DomainAccess.VisualData.PrimaryDisplayer.RestoreElements(elements);
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting revealers...", e)) return null;
                foreach (var displayer in DomainAccess.VisualData.RevealerCollection)
                {
                    displayer.RestoreElements(elements);
                }
            }
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting up visuals...", e)) return null;
            Directory.Delete(archiveDir, true);
            return persistenceMemento.Story;
        }

        private static string ImportToLatestDpnVersion(string archiveDir, int depictionFileVersion)
        {
            var importFilter =
                DepictionImportFilter.CreateImportFilterFromVersionXToCurrentVersion(depictionFileVersion, CurrentDepictionFileVersion);
            string homeDir = archiveDir;

            //Only do filter actions if there is more than one thing to do.  Sometimes we just want to give
            //a good message when a user tries to open a newer version in an older version.
            if (importFilter != null)
            {
                //Do nothing if there are no actions
                if (importFilter.actions.Count == 0) return homeDir;

                var importedArchiveDir = Path.Combine(DepictionAccess.ApplicationPathService.TempFolderPath, "importDirectory");
                //Copy stuff over
                FileSystemHelper.CopyDirectory(homeDir, importedArchiveDir, true);
                //Do the import stuff, usually implies taking stuff from the homeDir, changing them then passing them to
                //the importedArchiveDir (this might explain duplicate elements sometimes)
                importFilter.DoImport(homeDir, importedArchiveDir);
                return importedArchiveDir;
            }

            throw new Exception("Import filter not found");
        }

        /// <summary>
        /// Returns the story persistent info.
        /// Returns null if "StoryInfo" directory does not exist.
        /// </summary>
        /// <param name="archiveDir"></param>
        /// <returns></returns>
        private static StoryPersistenceInfo GetStoryPersistenceInfo(string archiveDir)
        {
            string filePath = Path.Combine(archiveDir, StringConstants.StoryFilename);
            if (!File.Exists(filePath)) return null;
            return SerializationService.LoadFromXmlFile<StoryPersistenceInfo>(filePath, "StoryInfo");
        }

        public static void UnzipFileToDirectory(string fileName, string archiveDir)
        {
            if (Directory.Exists(archiveDir))
                Directory.Delete(archiveDir, true);

            string tempName = fileName;
            bool deleteTemp = false;
            var fileExtension = Path.GetExtension(fileName);
            var needToUnEncrypt = true;
            if (fileExtension.Equals(new DepictionProduct().FileExtension) || string.IsNullOrEmpty(UIAccess.Product.FilePassword))
                needToUnEncrypt = false;
            if (needToUnEncrypt)
            {
                tempName = GetZipFromInAZip(fileName, UIAccess.Product.FilePassword);
                //Since i don't know exactly what happens if the GetZipFromInAZip goes bad, and we need
                //to delete the temp file. We must make sure that the temp file is not the original.
                if (!string.IsNullOrEmpty(tempName) && !tempName.Equals(fileName))
                {
                    deleteTemp = true;
                }
                else
                {
                    throw new Exception(String.Format(
                                            "File \"{0}\" is not a valid {1} file.\n\nPlease select a valid {1} file.",
                                            fileName, UIAccess.Product.StoryName));
                }
            }
            var decompressor = new FastZip { CreateEmptyDirectories = true, RestoreDateTimeOnExtract = true, RestoreAttributesOnExtract = true };
            if (needToUnEncrypt)
            {
                decompressor.Password = UIAccess.Product.FilePassword;
            }
            decompressor.ExtractZip(tempName, archiveDir, "");
            if (deleteTemp && File.Exists(tempName))
            {
                File.Delete(tempName);
            }
        }
        #region Stuff for removing images taht are not used

        private static ResourceDictionary RemoveUnusedImagesFromResources()
        {
            var imageNames = new List<string>();
            foreach (var elem in CommonData.Story.UnregisteredElementRepository.AllElements)
            {
                var name = elem.ImagePath;
                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
                {
                    imageNames.Add(name);
                }
            }
            foreach (var elem in CommonData.Story.ElementRepository.AllElements)
            {
                var name = elem.ImagePath;
                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
                {
                    imageNames.Add(name);
                }
            }
            foreach (var elem in CommonData.Story.PrototypeRepository.AvailableElementPrototypes)
            {
                var name = elem.ImagePath;
                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
                {
                    imageNames.Add(name);
                }
            }

            var matchingRD = new RasterImageResourceDictionary();
            foreach (object keyObject in CommonData.Story.BitmapResources.Keys)
            {
                var keyString = keyObject as string;

                if (!string.IsNullOrEmpty(keyString) && imageNames.Contains(keyString))
                {
                    matchingRD.Add(keyObject, CommonData.Story.BitmapResources[keyObject]);
                }
            }

            //            var usedImages = CommonData.Story.BitmapResources.Where(images => (imageNames.Contains(images.Key)));
            return matchingRD;

        }
        #endregion

        static public bool ReportProgressCheckForCancel(BackgroundWorker worker, int progress, string report, DoWorkEventArgs e)
        {
            if (worker != null)
            {
                if (e != null)
                {
                    if (worker.CancellationPending)
                    {
                        e.Cancel = true;
                        return true;
                    }
                }
                if (report != null)
                    worker.ReportProgress(progress, report);
                else
                    worker.ReportProgress(progress);

            }
            return false;
        }

        public void SaveDepictionToFile(IVisualData visualData, IStory story, ElementTupleList elementRulePairs, string fileName, BackgroundWorker worker, DoWorkEventArgs e)
        {
            SaveStoryToFile(visualData, story, elementRulePairs, fileName, worker, e);
        }

        private static void SaveStoryToFile(IVisualData visualData, IStory story, ElementTupleList elementRulePairs, string fileName, BackgroundWorker worker, DoWorkEventArgs e)
        {
            string archiveDir = CreateArchiveDirectory();
            var persistenceMemento = new PersistenceMemento { Story = story, ElementRulePairs = elementRulePairs };

            PercentOfOperationCompleted = 1;
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Removing unused images...", e)) return;

            //Remove all unused images before saving (davidl)
            ((Story)persistenceMemento.Story).BitmapResources = RemoveUnusedImagesFromResources();
            persistenceMemento.VisualData = visualData;
            PercentOfOperationCompleted = 5;
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Starting save...", e)) return;

            SaveStoryToArchive(persistenceMemento, archiveDir, worker, e);

            story.NeedSaving = false;

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
                PercentOfOperationCompleted += 1;
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Deleting old file...", e)) return;
            }

            PercentOfOperationCompleted += 1;
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Compressing file...", e)) return;


            var fastZip = new FastZip
                              {
                                  CreateEmptyDirectories = true,
                                  RestoreDateTimeOnExtract = true,
                                  RestoreAttributesOnExtract = true
                              };
            if (!string.IsNullOrEmpty(UIAccess.Product.FilePassword))
            {
                fastZip.Password = UIAccess.Product.FilePassword;
            }
            fastZip.CreateZip(fileName, archiveDir, true, "");
            if (!string.IsNullOrEmpty(UIAccess.Product.FilePassword))
            {
                PutZipIntoAZip(fileName, UIAccess.Product.FilePassword);
            }

            WebServiceFacade.LogUsageMetric(MetricType.Saved, fileName.Substring(fileName.LastIndexOf('\\') + 1).MD5Hash());

            if (ReportProgressCheckForCancel(worker, 100, "Save complete.", e)) return;
        }



        #region Save Helper Methods

        private static void SaveStoryToArchive(PersistenceMemento dataToSave, string archiveDir, BackgroundWorker worker, DoWorkEventArgs e)
        {
            SaveDepictionPersistenceMemento(dataToSave, archiveDir, worker, e);
            var storyPersistenceInfo = new StoryPersistenceInfo
            {
                Author = Settings.Default.Author,
                SaveDateTime = DateTime.Now,
                DepictionFileVersion = CurrentDepictionFileVersion,
                MinimumAppVersionForDepictionFileVersion = MinimumAppVersionForDepictionFileVersion,
                AppVersion = VersionInfo.AppVersion,
                Subtrees = GetStorySubtrees()
            };

            string storyInfoFilename = Path.Combine(archiveDir, StringConstants.StoryFilename);
            PercentOfOperationCompleted += 1;
            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Saving version info...", e)) return;
            SerializationService.SaveToXmlFile(storyPersistenceInfo, storyInfoFilename, "StoryInfo");


        }

        /// <summary>
        /// Provide a user-friendly name for the subtree, instead of the internal name we use in the .dpn file.
        /// </summary>
        /// <param name="subtreeName"></param>
        /// <returns></returns>
        private static string FriendlyNameForSubtree(string subtreeName)
        {
            var friendlyNamesForSubtrees =
                new Dictionary<string, string>
                    {
                        {"ElementTypes", "element definitions"},
                        {"VisualData", "revealers"},
                        {"InteractionRules", "interactions"},
                        {"Images", "images"},
                        {"Elements", "elements"},
                        {"UnregisteredElements", "un-geo-aligned elements"},
                        {"Story", UIAccess.Product.StoryName},
                        {"InteractionGraphElements", "modified elements"},
                    };
#if (DEBUG)
            if (!friendlyNamesForSubtrees.ContainsKey(subtreeName))
                throw new Exception(string.Format("No friendly name for \"{0}\"", subtreeName));
#endif
            return friendlyNamesForSubtrees[subtreeName] ?? subtreeName;
        }

        private static void SaveDepictionPersistenceMemento(PersistenceMemento dataToSave, string archiveDir, BackgroundWorker worker, DoWorkEventArgs e)
        {
            var subTrees = GetStorySubtrees();
            const int totalSaveToComplete = 85;
            int subTreeCount = subTrees.Count;
            if (subTreeCount == 0) subTreeCount = 1;
            int increment = totalSaveToComplete / subTreeCount;

            foreach (var subtree in subTrees)
            {
                var report = string.Format("Saving {0}...", FriendlyNameForSubtree(subtree.DataPath));
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, report, e)) return;

                ISubtreeLoader loader = FindSubtreeLoader(subtree.LoaderTypeName);
                if (loader != null)
                {
                    string fullDataPath = Path.Combine(archiveDir, subtree.DataPath);
                    if (!Directory.Exists(fullDataPath))
                        Directory.CreateDirectory(fullDataPath);
                    loader.SaveSubtree(dataToSave,
                                       new SubtreeInfo
                                           {
                                               DataPath = fullDataPath,
                                               XMLFileName = subtree.XmlFileName,
                                               SchemaResourcePath = subtree.SchemaResourcePath
                                           });
                }
                PercentOfOperationCompleted += increment;
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, null, e)) return;
            }

            string depictionMetadataFilename = Path.Combine(archiveDir, StringConstants.DepictionInfoFilename);
            SerializationService.SaveToXmlFile(dataToSave.Story.Metadata, depictionMetadataFilename, "DepictionInfo");
        }

        #endregion

        #region Load Helper Methods

        /// <summary>
        /// Loads all of the subtrees / folders in this .dpn file.
        /// Caller must catch any exceptions.
        /// </summary>
        /// <param name="dataToLoad"></param>
        /// <param name="subtreeList"></param>
        /// <param name="archiveDir"></param>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        private static void LoadStorySubtrees(PersistenceMemento dataToLoad, ICollection<StorySubtreeInfo> subtreeList, string archiveDir, BackgroundWorker worker, DoWorkEventArgs e)
        {

            const int totalLoadComplete = 85;
            int subTreeCount = subtreeList.Count;
            if (subTreeCount == 0) subTreeCount = 1;
            int increment = totalLoadComplete / subTreeCount;
            foreach (var subtree in subtreeList)
            {
                var report = string.Format("Loading {0}...", FriendlyNameForSubtree(subtree.DataPath));
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, report, e)) return;
                ISubtreeLoader loader = FindSubtreeLoader(subtree.LoaderTypeName);
                if (loader != null)
                {
                    string fullDataPath = Path.Combine(archiveDir, subtree.DataPath);
                    var subtreeInfo = new SubtreeInfo
                    {
                        DataPath = fullDataPath,
                        XMLFileName = subtree.XmlFileName,
                        SchemaResourcePath = subtree.SchemaResourcePath
                    };

                    loader.LoadSubtree(dataToLoad, subtreeInfo);
                }
                PercentOfOperationCompleted += increment;
                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, null, e)) return;
            }
            string filePath = Path.Combine(archiveDir, StringConstants.DepictionInfoFilename);

            if (File.Exists(filePath))
            {
                dataToLoad.Story.Metadata = SerializationService.LoadFromXmlFile<DepictionMetadata>(filePath, "DepictionInfo");
            }
        }

        #endregion
    }
}
