﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.CoreModel.ValueTypes
{
    /// <summary>
    /// A container for a piece of spatial data with a set of common methods for operating on that geometry.
    /// Delegates many of these methods to the internal IGeometry object.
    /// </summary>
    public class DepictionGeometry : IDepictionGeometry
    {
        /// <summary>
        /// Will never be null.
        /// </summary>
        private IGeometry geometry;
        private static readonly IGeometryFactory geometryFactory = new GeometryFactory();

        #region Properties

        /// <summary>
        ///  Cannot return the wrapped geometry.
        /// </summary>
        public IGeometry Geometry
        {
            get { return geometry; }
        }
        public IDepictionGeometry BoundingBox
        {
            get { return new DepictionGeometry(geometry.Envelope); }
        }
        public string GeometryType
        {
            get { return geometry.GeometryType; }
        }
        public bool IsValid
        {
            get { return geometry.IsValid; }
        }
        public bool IsEmpty
        {
            get { return geometry.IsEmpty; }
        }
        /// <summary>
        /// Delegate to IGeometry.IsSimple.
        /// </summary>
        public bool IsSimple
        {
            get { return geometry.IsSimple; }
        }
        /// <summary>
        /// Delegate to IGeometry.Area.
        /// </summary>
        public double Area
        {
            get { return geometry.Area; }
        }

        /// <summary>
        /// Delegate to IGeometry.Length.
        /// </summary>
        public double Length
        {
            get { return geometry.Length; }
        }
        ///<summary>
        /// Get the centroid for this geometry if it is not a line. If it is a linestring it returns first point
        ///</summary>
        public ILatitudeLongitude Centroid//Hacked
        {
            get
            { //Get centroid of the geometry
                var point = geometry.Centroid;
                if(geometry.GeometryType.Equals(DepictionGeometryType.LineString.ToString()))
                {
                    return new LatitudeLongitude(geometry.Coordinate.Y, geometry.Coordinate.X);
                }
                if(point != null) return new LatitudeLongitude(point.Y, point.X);
                return new LatitudeLongitude();
            }
        }
        /// <summary>
        /// Delegate to IGeometry.NumGeometries.
        /// </summary>
        public int NumGeometries
        {
            get { return geometry.NumGeometries; }
        }

        /// <summary>
        /// Delegate to IGeometry.NumPoints.
        /// </summary>
        public int NumPoints
        {
            get { return geometry.NumPoints; }
        }

        /// <summary>
        /// Delegate to IGeometry.Boundary.
        /// </summary>
        public IGeometry Boundary
        {
            get { return geometry.Boundary; }
        }

        /// <summary>
        /// Delegate to IGeometry.Coordinate.
        /// </summary>
        public ICoordinate Coordinate
        {
            get { return geometry.Coordinate; }
        }

        /// <summary>
        /// Delegate to IGeometry.Coordinates.
        /// </summary>
        public ICoordinate[] Coordinates
        {
            get { return geometry.Coordinates; }
        }

        /// <summary>
        /// Delegate to IGeometry.Envelope.
        /// </summary>
        public IGeometry Envelope
        {
            get { return geometry.Envelope; }
        }

        /// <summary>
        /// Delegate to IGeometry.EnvelopeInternal.
        /// </summary>
        public IEnvelope EnvelopeInternal
        {
            get { return geometry.EnvelopeInternal; }
        }
        
        #endregion

        #region Constructors
        ///<summary>
        /// Construct a depiction geometry as a semi empty point geometry
        ///</summary>
        public DepictionGeometry()
        {
            geometry = geometryFactory.CreatePoint((ICoordinate)null);
        }
        ///<summary>
        /// Construct a point geometry
        ///</summary>
        ///<param name="point"></param>
        public DepictionGeometry(ILatitudeLongitude point)
        {
            var p = geometryFactory.CreatePoint(new Coordinate(point.Longitude, point.Latitude));
            InitializeFromGeometry(p);
        }
        /// <summary>
        /// Construct a rectangle with this top left corner and bottom right corner.
        /// </summary>
        /// <param name="topLeft"></param>
        /// <param name="bottomRight"></param>
        public DepictionGeometry(IMapCoordinateBounds mapCoordinateBounds)//ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            var topLeft = mapCoordinateBounds.TopLeft;
            var bottomRight = mapCoordinateBounds.BottomRight;
            var linearRing = geometryFactory.CreateLinearRing(new[]
                                                                  {
                                                                      new Coordinate(topLeft.Longitude, topLeft.Latitude),
                                                                      new Coordinate(bottomRight.Longitude, topLeft.Latitude),
                                                                      new Coordinate(bottomRight.Longitude, bottomRight.Latitude),
                                                                      new Coordinate(topLeft.Longitude, bottomRight.Latitude),
                                                                      new Coordinate(topLeft.Longitude, topLeft.Latitude)
                                                                  });
            InitializeFromGeometry(geometryFactory.CreatePolygon(linearRing, null));
        }
        public DepictionGeometry(List<ILatitudeLongitude> latLongPoints)
        {
            if (latLongPoints == null || latLongPoints.Count == 0)
            {
                geometry = geometryFactory.CreatePoint((ICoordinate)null);
                return;
            }

            if (latLongPoints.Count == 1)
            {
                geometry =
                    geometryFactory.CreatePoint(new Coordinate(latLongPoints[0].Longitude, latLongPoints[0].Latitude));
                return;
            }
            if (latLongPoints.Count == 2)
            {
                geometry = geometryFactory.CreateLineString(new[]
                                                                {
                                                                    new Coordinate(latLongPoints[0].Longitude,
                                                                                   latLongPoints[0].Latitude),
                                                                    new Coordinate(latLongPoints[1].Longitude,
                                                                                   latLongPoints[1].Latitude)
                                                                });
                return;
            }
            if (latLongPoints.Count > 2)
            {
                var coords = new List<ICoordinate>();
                foreach (var latLong in latLongPoints)
                {
                    coords.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
                }
                //Make sure it is closed
                if(!latLongPoints[0].Equals(latLongPoints[latLongPoints.Count-1]))
                {
                    coords.Add(new Coordinate(latLongPoints[0].Longitude, latLongPoints[0].Latitude));
                }
                var linearRing = geometryFactory.CreateLinearRing(coords.ToArray());
                InitializeFromGeometry(geometryFactory.CreatePolygon(linearRing, null));
                return;
            }
            geometry = geometryFactory.CreatePoint((ICoordinate)null);
        }
        /// <summary>
        /// Wrap this geometry in a DepictionGeometry.
        /// </summary>
        /// <param name="geometry"></param>
        public DepictionGeometry(IGeometry geometry)
        {
            InitializeFromGeometry(geometry);
        }
        #endregion

        #region Private helper methods

        private void InitializeFromGeometry(IGeometry aGeometry)
        {
            if (aGeometry != null)
                geometry = aGeometry;
            else
            {
                // never allow internal geometry to be null;
                var geomFactory = new GeometryFactory();
                geometry = geomFactory.CreatePoint((ICoordinate) null);
            }
        }

        #endregion

        #region Static public methods
        
        static public bool FindInsertPointIntoClosestLineSegment(List<DepictionGeometry> lineSegments, Point coordToAdd, out int closestLineSegment)
        {
            closestLineSegment = 0;

            var minDistance = double.MaxValue;
            int count = 0;
            foreach (var segment in lineSegments)
            {
                double tempDistance;

                if ((tempDistance = segment.Distance(coordToAdd)) < minDistance)
                {
                    minDistance = tempDistance;
                    closestLineSegment = count;
                }
                count++;
            }
            if (minDistance == -10000) return false;
            return true;
        }
        #endregion

        #region Static Private helper methods
        private static ILinearRing TranslateLinearRing(ICoordinate[] oldCoordinates, ILatitudeLongitude moveDelta)
        {

            var coords = oldCoordinates;
            if(moveDelta.IsValid)
            {
                coords = MoveCoordinates(oldCoordinates, moveDelta);
            }else
            {
                Debug.WriteLine("Move delta was invalid, what happened?!");
            }
            return geometryFactory.CreateLinearRing(coords);
        }

        private static ICoordinate[] MoveCoordinates(ICoordinate[] coordinates, ILatitudeLongitude MoveDelta)
        {
            var newCoordinates = new List<ICoordinate>();
            foreach (ICoordinate coordinate in coordinates)
            {
                var position = new LatitudeLongitude(coordinate.Y, coordinate.X);
                position = position + MoveDelta ;
                newCoordinates.Add(new Coordinate(position.Longitude, position.Latitude));
            }
            return newCoordinates.ToArray();
        }

        private static IDepictionGeometry TranslateGeometry(IDepictionGeometry geom, ILatitudeLongitude moveDelta)
        {
            if (geom.IsEmpty) return geom;

            if (geom.Geometry is IPolygon)
            {
                var polygon = geom.Geometry as IPolygon;
                if (polygon != null)
                {
                    ILinearRing exteriorRing = TranslateLinearRing(polygon.ExteriorRing.Coordinates, moveDelta);
                    var holes = new List<ILinearRing>();
                    foreach (ILineString linearRing in polygon.InteriorRings)
                        holes.Add(TranslateLinearRing(linearRing.Coordinates, moveDelta));
                    return new DepictionGeometry(geom.Geometry.Factory.CreatePolygon(exteriorRing, holes.ToArray()));
                }
            }else if(geom.Geometry is IMultiPolygon)
            {
                var multiPoly = geom.Geometry as IMultiPolygon;
                if(multiPoly != null)
                {
                    var polys = new List<IPolygon>();
                    foreach(var polyObject in multiPoly)
                    {
                        var polygon = polyObject as IPolygon;
                        if (polygon != null)
                        {
                            ILinearRing exteriorRing = TranslateLinearRing(polygon.ExteriorRing.Coordinates, moveDelta);
                            var holes = new List<ILinearRing>();
                            foreach (ILineString linearRing in polygon.InteriorRings)
                                holes.Add(TranslateLinearRing(linearRing.Coordinates, moveDelta));
                            polys.Add(geometryFactory.CreatePolygon(exteriorRing,holes.ToArray()));
                        }
                    }
                    return new DepictionGeometry(geom.Geometry.Factory.CreateMultiPolygon(polys.ToArray()));
                }
            }
            else if (geom.Geometry is IPoint)
            {
                var position = new LatitudeLongitude(geom.Geometry.Coordinate.Y, geom.Geometry.Coordinate.X);
                position = position + moveDelta;
                return new DepictionGeometry(new Point(position.Longitude, position.Latitude));
            }
            else if (geom.Geometry is ILineString)
            {
                var lineString = geom.Geometry as ILineString;
                if (lineString != null)
                {
                    ICoordinate[] coords = MoveCoordinates(lineString.Coordinates, moveDelta);
                    return new DepictionGeometry(geom.Geometry.Factory.CreateLineString(coords));
                }
            }
            else
                throw new Exception(String.Format("Cannot translate. Unsupported geometry type {0}", geom.GeometryType));
            return null;
        }
        #endregion

        #region Helper methods
        
        /// <summary>
        /// Delegate to IGeometry.TranslateGeometry.
        /// </summary>
        public IDepictionGeometry TranslateGeometry(ILatitudeLongitude moveDelta)
        {
            return TranslateGeometry(this, moveDelta);
        }

        public bool Intersects(IDepictionSpatialData g)
        {
            return SpatialOps.Intersects(this, g);
        }

        /// <summary>
        /// Do these two geometries intersect?
        /// </summary>
        /// <param name="other">The other geometry to check for intersection.</param>
        /// <returns></returns>
        public IGeometry Intersection(DepictionGeometry other)
        {
            return geometry.Intersection(other.Geometry);
        }

        /// <summary>
        /// Wrapper for IGeometry.EqualsExact.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool EqualsExact(IGeometry other)
        {
            return geometry.EqualsExact(other);
        }

        /// <summary>
        /// Delegate to IGeometry.Within.
        /// </summary>
        public bool Within(DepictionGeometry g)
        {
            return geometry.Within(g.Geometry);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Contains.
        /// </summary>
        public bool Contains(DepictionGeometry g)
        {
            return geometry.Contains(g.Geometry);
        }

        /// <summary>
        /// Delegate to IGeometry.Contains.
        /// </summary>
        public bool Covers(IGeometry g)
        {
            return geometry.Covers(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Crosses.
        /// </summary>
        public bool Crosses(IGeometry g)
        {
            return geometry.Crosses(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Overlaps.
        /// </summary>
        public bool Overlaps(IGeometry g)
        {
            return geometry.Overlaps(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Touches.
        /// </summary>
        public bool Touches(IGeometry g)
        {
            return geometry.Touches(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Disjoint.
        /// </summary>
        public bool Disjoint(IGeometry g)
        {
            return geometry.Disjoint(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Distance.
        /// </summary>
        public double Distance(IGeometry g)
        {
            return geometry.Distance(g);
        }
        
        /// <summary>
        /// Delegate to IGeometry.Distance.
        /// </summary>
        public double Distance(DepictionGeometry g)
        {
            return geometry.Distance(g.Geometry);
        }
        #endregion


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            var other = (DepictionGeometry) obj;
            if (ReferenceEquals(null, Geometry)) return false;
            if (ReferenceEquals(null, other.Geometry)) return false;
            if (Geometry != null && other.Geometry != null)
                return Geometry.Equals(other.Geometry);
            
            return true;
        }
        
        public override int GetHashCode()
        {
            return (geometry != null ? geometry.GetHashCode() : 0);
        }
    }
}