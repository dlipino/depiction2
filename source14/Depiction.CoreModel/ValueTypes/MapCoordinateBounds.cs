﻿using Depiction.APINew.CoreEnumAndStructs;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using GeoAPI.Geometries;
using GeoFramework;
using GisSharpBlog.NetTopologySuite.Geometries;
using Distance=GeoFramework.Distance;

namespace Depiction.CoreModel.ValueTypes
{
    public class MapCoordinateBounds : IMapCoordinateBounds
    {
        #region Properties

        public double Top { get; set; }
        public double Bottom { get; set; }
        public double Right { get; set; }
        public double Left { get; set; }

        /// <summary>
        /// Top left (northwest) corner of the box
        /// </summary>
        public ILatitudeLongitude TopLeft
        {
            get { return new LatitudeLongitude(Top, Left); }
        }

        /// <summary>
        /// Bottom right (southeast) corner of the box
        /// </summary>
        public ILatitudeLongitude BottomRight
        {
            get { return new LatitudeLongitude(Bottom, Right); }
        }

        /// <summary>
        /// Top right (northeast) corner of the box
        /// </summary>
        public ILatitudeLongitude TopRight
        {
            get { return new LatitudeLongitude(Top, Right); }
        }

        /// <summary>
        /// Bottom left (southwest) corner of the box
        /// </summary>
        public ILatitudeLongitude BottomLeft
        {
            get { return new LatitudeLongitude(Bottom, Left); }
        }
        /// <summary>
        /// Center of the bounding box; calculated with Lat = (Top + Bottom / 2), Lon = (Right + Left) /2
        /// </summary>
        public ILatitudeLongitude Center
        {
            get { return new LatitudeLongitude(((Bottom + Top) / 2), ((Right + Left) / 2)); }
        }

        public bool Contains(ILatitudeLongitude location)
        {
            var lat = location.Latitude;
            if (!location.IsValid) return false;
            if (lat < Bottom || lat > Top) return false;
            var lon = location.Longitude;
            if (lon < Left || lon > Right) return false;
            return true;
        }

        public bool IsValid
        {
            get
            {
                if (Top.Equals(double.NaN) || Left.Equals(double.NaN) || Bottom.Equals(double.NaN) || Right.Equals(double.NaN))
                    return false;
                return true;
            }
        }
        #endregion

        public MapCoordinateBounds()
        {
            Top = Left = Bottom = Right = double.NaN;
        }
        ///<summary>
        /// Initializes a new instance of the <see cref="MapCoordinateBounds"/> class.
        ///</summary>
        ///<param name="topLeft">The top left coordinate of the bounding box.</param>
        ///<param name="bottomRight">The bottom right coordinate of the bounding box.</param>
        public MapCoordinateBounds(ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            Top = topLeft.Latitude;
            Left = topLeft.Longitude;
            Bottom = bottomRight.Latitude;
            Right = bottomRight.Longitude;
        }
        public double GetMidLineHorizontalDistance(MeasurementSystem system, MeasurementScale scale)
        {
            var start = new LatitudeLongitude((Top + Bottom) / 2, Left);
            var end = new LatitudeLongitude((Top + Bottom) / 2, Right);
            return GetDistanceFromLocationToOtherLocation(start, end, system, scale);
        }
        public double GetMidLineVerticalDistance(MeasurementSystem system, MeasurementScale scale)
        {
            var start = new LatitudeLongitude(Top, (Left + Right) / 2);
            var end = new LatitudeLongitude(Bottom, (Left + Right) / 2);
            return GetDistanceFromLocationToOtherLocation(start, end, system, scale);
        }

        static public ILatitudeLongitude GetLatLongAtDistanceFromOrigin(ILatitudeLongitude start,double distance, MeasurementSystem system, MeasurementScale scale,double bearing)
        {
            var startGeoPos = new Position(new Latitude(start.Latitude), new Longitude(start.Longitude));
            var geoDistanceUnit = DistanceUnit.Kilometers;
            var systemAndScale = (int)system + (int)scale;
            switch (systemAndScale)
            {
                case BaseMeasurement.ImperialSmall:
                    geoDistanceUnit = DistanceUnit.Inches;
                    break;
                case BaseMeasurement.ImperialNormal:
                   geoDistanceUnit = DistanceUnit.Feet;
                    break;
                case BaseMeasurement.ImperialLarge:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case BaseMeasurement.MetricSmall:
                    geoDistanceUnit = DistanceUnit.Centimeters;
                    break;
                case BaseMeasurement.MetricNormal:
                    geoDistanceUnit = DistanceUnit.Meters;
                    break;
                case BaseMeasurement.MetricLarge:
                   geoDistanceUnit = DistanceUnit.Kilometers;
                    break;
                default:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
            }
            var geoDistance = new Distance(distance, geoDistanceUnit);
            var endGeoPos = startGeoPos.TranslateTo(bearing, geoDistance);
            return new LatitudeLongitude(endGeoPos.Latitude, endGeoPos.Longitude);
        }
        static public double GetDistanceFromLocationToOtherLocation(ILatitudeLongitude start, ILatitudeLongitude end, MeasurementSystem system, MeasurementScale scale)
        {
            var startGeoPos = new Position(new Latitude(start.Latitude), new Longitude(start.Longitude));
            var endGeoPos = new Position(new Latitude(end.Latitude), new Longitude(end.Longitude));

            var distance = startGeoPos.DistanceTo(endGeoPos);

            var systemAndScale = (int)system + (int)scale;
            switch (systemAndScale)
            {
                case BaseMeasurement.ImperialSmall:
                    return distance.ToInches().Value;
                case BaseMeasurement.ImperialNormal:
                    return distance.ToFeet().Value;
                case BaseMeasurement.ImperialLarge:
                    return distance.ToStatuteMiles().Value;
                case BaseMeasurement.MetricSmall:
                    return distance.ToCentimeters().Value;
                case BaseMeasurement.MetricNormal:
                    return distance.ToMeters().Value;
                case BaseMeasurement.MetricLarge:
                    return distance.ToKilometers().Value;
                default:
                    return distance.ToStatuteMiles().Value;
            }
        }


        #region ToString
        public override string ToString()
        {
            var info = string.Format("TopLeft: {0},{1}", Top, Left);
            info += "\n";
            info += string.Format("BottomRight: {0},{1}", Bottom, Right);
            return info;
        }
        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (MapCoordinateBounds)obj;
            if (!Equals(Top, other.Top)) return false;
            if (!Equals(Left, other.Left)) return false;
            if (!Equals(Bottom, other.Bottom)) return false;
            if (!Equals(Right, other.Right)) return false;

            return true;
        }
        #endregion
    }
}