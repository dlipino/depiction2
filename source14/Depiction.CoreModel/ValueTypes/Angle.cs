﻿using System.ComponentModel;
using Depiction.API.Interfaces;

namespace Depiction.CoreModel.ValueTypes
{
    [TypeConverter("Depiction.CoreModel.TypeConverter.AngleConverter")]
    public class Angle : IDeepCloneable<Angle>
    {
        public double Value { get; set; }
        public Angle DeepClone()
        {
            var newAngle = new Angle();
            newAngle.Value = Value;
            return newAngle;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var angle = obj as Angle;
            if (angle == null) return false;
            return Value.Equals(((Angle)obj).Value);
        }

        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
    }
}