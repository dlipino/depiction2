﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using System.Xml;
using System.Xml.Schema;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ElementPrototype;
using Depiction.API.ExtensionMethods;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.AbstractDepictionObjects
{
    public abstract class DepictionElementBase : DepictionPropertyHolderBase, IDepictionElementBase
    {
        #region Variables

        protected string elementType;
        private string typeDisplayName;

        protected IZoneOfInfluence zoneOfInfluence;
        protected IDepictionImageMetadata imageMetadata;//bazinga
        protected IPermaText permaText = new DepictionPermaText();
        private bool usePermaText;
        private bool usePropertyNameInHoverText;

        protected List<IDepictionElementWaypoint> waypoints = new List<IDepictionElementWaypoint>();
        private HashSet<string> tags = new HashSet<string>();
        private SerializableDictionary<string, string[]> clickActions;
        private SerializableDictionary<string, string[]> createActions;
        private SerializableDictionary<string, string[]> deleteActions;
        private SerializableDictionary<string, string[]> generateZoiActions;

        #endregion

        #region ElementProperties

        public string ElementType
        {
            get { return elementType; }
            set { elementType = value; }
        }

        public DepictionIconPath IconPath
        {
            get
            {
                DepictionIconPath iconPathUri;
                var hasPath = GetPropertyValue("IconPath", out iconPathUri);
                if (!hasPath) return new DepictionIconPath();
                return iconPathUri;
            }
        }
        //Human readable version of element type, not to be saved
        public string TypeDisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(typeDisplayName))
                {
                    var allNames = elementType.Split('.');
                    if (allNames.Length > 0)
                        typeDisplayName = allNames[allNames.Length - 1];
                }
                return typeDisplayName;
            }
            set { typeDisplayName = value; NotifyModelPropertyChanged("TypeDisplayName"); }
        }

        public HashSet<string> Tags { get { return tags; } }

        override public bool UsePropertyNameInHoverText
        {
            get { return usePropertyNameInHoverText; }
            set { usePropertyNameInHoverText = value; NotifyModelPropertyChanged("UsePropertyNameInHoverText"); }
        }

        override public bool UseEnhancedPermaText
        {
            get { return PermaText.IsEnhancedPermaText; }
            set { PermaText.IsEnhancedPermaText = value; NotifyModelPropertyChanged("UseEnhancedPermaText"); }
        }

        public string DefinitionGroup { get; set; }

        public bool UsePermaText
        {
            get { return usePermaText; }
            set { usePermaText = value; NotifyModelPropertyChanged("UsePermaText"); }
        }

        public IPermaText PermaText
        {
            get { return permaText; }
            set { permaText = value; }
        }

        public IZoneOfInfluence ZoneOfInfluence
        {
            get { return zoneOfInfluence; }
            protected set { zoneOfInfluence = value; }
        }

        public IDepictionImageMetadata ImageMetadata
        {
            get { return imageMetadata; }
        }
        override public SerializableDictionary<string, string[]> ClickActions
        {
            get { return clickActions; }
            set { clickActions = value; }
        }

        override public SerializableDictionary<string, string[]> CreateActions
        {
            get { return createActions; }
            set { createActions = value; }
        }

        override public SerializableDictionary<string, string[]> DeleteActions
        {
            get { return deleteActions; }
            set { deleteActions = value; }
        }

        override public SerializableDictionary<string, string[]> GenerateZoiActions
        {
            get { return generateZoiActions; }
            set { generateZoiActions = value; }
        }

        #endregion

        #region constructor

        protected DepictionElementBase()
        {
            elementType = "UnknownType";
            permaText = new DepictionPermaText();

            //I don't like doubling up DisplayName, but it is needed because of hovertext (for now)
            var prop = CreateProperty("DisplayName", "Name", TypeDisplayName);
            prop.Deletable = false;
            prop.IsHoverText = true;
            prop.Rank = -1;

            prop = CreateProperty("Position", "Position", new LatitudeLongitude());
            prop.Editable = false;
            prop.Rank = 1000;

            CreateProperty("IconBorderColor", "Icon border color", Colors.Red);
            CreateProperty("IconBorderShape", "Icon border shape", DepictionIconBorderShape.None);

            CreateProperty("IconPath", "Icon path", new DepictionIconPath("embeddedresource:Depiction.Default"));
            CreateProperty("IconSize", "Icon size", ProductAndFolderService.AvailableIconSizes[DepictionIconSize.Medium]);


            //For now the elmeent view modle will be able to modify these, later
            //the zoi view model should be able to modify them, perhaps the zoi will become a property

            prop = CreateProperty("ZOIShapeType", "ZOI shape type", ZOIShapeType.Point);
            prop.VisibleToUser = false;
            prop = CreateProperty("PlaceableWithMouse", "Placeable by mouse", true);
            prop.VisibleToUser = false;
            CreateProperty("ZOIFill", "ZOI fill color", Colors.Gray);
            CreateProperty("ZOIBorder", "ZOI border color", Colors.Red);
            CreateProperty("Draggable", "Draggable", true);
            prop = CreateProperty("ShowIcon", "Display icon", true);
            prop.VisibleToUser = false;

            zoneOfInfluence = new ZoneOfInfluence(this);

            //Hack for the Active
            var prop1 = new DepictionElementProperty("Active", "Active status", true, true, null, null);
            prop1.VisibleToUser = false;
            AddPropertyOrReplaceValueAndAttributes(prop1, false);
        }

        #endregion

        #region Node stuff

        public IDepictionElementWaypoint[] Waypoints
        {
            get { return waypoints.ToArray(); }
        }

        #endregion

        #region Methods
        #region public methods for adjust initial element ie no notification
        //This method needs help
        public void SetInitialPositionAndZOI(ILatitudeLongitude initialPosition, IZoneOfInfluence initialZOI)
        {
            bool ignoreInitalZOI = false;
            var proto = this as IElementPrototype;

            if (initialZOI != null && !initialZOI.Geometry.IsEmpty &&
                initialZOI.DepictionGeometryType.Equals(DepictionGeometryType.Point) &&
                initialPosition != null && initialPosition.IsValid)
            {
                ignoreInitalZOI = true;
            }
            if (initialZOI != null && !initialZOI.Geometry.IsEmpty && !ignoreInitalZOI)
            {
#if DEBUG
                if (!initialZOI.Geometry.IsValid)
                {
                    var message = string.Format("Initial ZOI for {0} is considered complex", typeDisplayName);
                    DepictionAccess.NotificationService.DisplayMessageString(message, 4);
                }
#endif
                zoneOfInfluence = initialZOI;
                var finalPosition = initialPosition;
                //This is bad, because this is used for email loading things are getting difficult
                //when the email is do we generate a new zoi if one does not exist or do we use the old
                //zoi and use the initial position. How often de we actually want the position to be set to the centriod?
                if (initialPosition == null)
                {
                    finalPosition = zoneOfInfluence.Geometry.Centroid;
                }
                var setWorked = SetPropertyValue("Position", finalPosition, true, false);
                if ((setWorked == false) && finalPosition != null)
                {
                    CreateProperty("Position", "Position", finalPosition);
                }

                AdjustElementZOIPropertiesForZOIType(zoneOfInfluence);

            }
            else
            {
                if (initialPosition != null && initialPosition.IsValid)
                {
                    var setWorked = SetPropertyValue("Position", initialPosition, true, false);
                    if (setWorked == false)
                    {
                        CreateProperty("Position", "Position", initialPosition);
                    }

                    if (proto != null && proto.IsRawPrototype && initialZOI != null && !initialZOI.IsEmpty)
                    {
                        zoneOfInfluence = initialZOI;

                    }
                    else
                    {
                        zoneOfInfluence = new ZoneOfInfluence(new DepictionGeometry(initialPosition));
                        AdjustElementZOIPropertiesForZOIType(zoneOfInfluence);
                    }
                }
            }
        }

        public abstract void ReplaceWaypointsWithoutNotification(IDepictionElementWaypoint[] newWayPoints);
        public abstract void SetImageMetadataWithoutNotification(IDepictionImageMetadata imageMetadata);
        //        public IElementPropertyData SetOrCreatePropertyValue<TPt>(string internalName, TPt value)
        //        {
        //            if(!SetPropertyValue(internalName,value))
        //            {
        //                var prop = new DepictionElementProperty(internalName, value);
        //                prop.Deletable = false;
        //                AddPropertyOrReplaceValueAndAttributes(prop, false);
        //                return prop.PropertData;
        //            }
        //            return GetPropertyByInternalName(internalName);
        //        }

        #endregion
        #region protected methods
        protected DepictionElementProperty CreateProperty<TPt>(string internalName, string displayName, TPt value)
        {
            var prop = new DepictionElementProperty(internalName, displayName, value);
            prop.Deletable = false;
            prop.Rank = 10;
            AddPropertyOrReplaceValueAndAttributes(prop, false);
            return prop;
        }
        
        public void FixIconPathType()
        {
            var prop = GetPropertyByInternalName("IconPath") as DepictionElementProperty;
            if (prop == null) return;
            if (!prop.ValueType.Equals(typeof(string))) return;
            prop.SetPropertyValue(new DepictionIconPath(prop.Value.ToString()),null,true);

        }
        #endregion

        /// <summary>
        /// Generally used when getting elements from files
        /// </summary>
        /// <param name="zoi"></param>
        protected void AdjustElementZOIPropertiesForZOIType(IZoneOfInfluence zoi)
        {
            var expectedZOIType = this.GetElementZOIDepictionGeometryType();
            if (expectedZOIType.Contains(DepictionGeometryType.LineString))
            {
                var prop = GetPropertyByInternalName("ZOIFill");
                if (prop != null) prop.VisibleToUser = false;
                prop = GetPropertyByInternalName("ZOIBorder");
                if (prop != null) prop.VisibleToUser = true;
            }
            else if (expectedZOIType.Contains(DepictionGeometryType.Polygon))
            {
                var prop = GetPropertyByInternalName("ZOIFill");
                if (prop != null) prop.VisibleToUser = true;
                prop = GetPropertyByInternalName("ZOIBorder");
                if (prop != null) prop.VisibleToUser = true;
            }
            else
            {
                if (zoi == null || zoi.Geometry.GeometryType.Equals("Point"))
                {
                    var prop = GetPropertyByInternalName("ZOIFill");
                    if (prop != null) prop.VisibleToUser = false;
                    prop = GetPropertyByInternalName("ZOIBorder");
                    if (prop != null) prop.VisibleToUser = false;
                }
                else
                {
                    var prop = GetPropertyByInternalName("ZOIFill");
                    if (prop != null) prop.VisibleToUser = true;
                    prop = GetPropertyByInternalName("ZOIBorder");
                    if (prop != null) prop.VisibleToUser = true;
                }
            }
        }

        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {//USed in Equals(obj,obj)
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionElementBase;
            if (other == null) return false;
            //Ok so this equals is not very fast, actually its really really really slow.
            if (!base.Equals(other)) return false;
            if (Waypoints.Length != other.Waypoints.Length) return false;
            for (int i = 0; i < Waypoints.Length; i++)
            {
                if (!Equals(Waypoints[i], other.Waypoints[i])) return false;
            }
            if (!Equals(ElementType, other.ElementType)) return false;
            if (!Equals(ZoneOfInfluence, other.ZoneOfInfluence)) return false;
            if (!Equals(ImageMetadata, other.ImageMetadata)) return false;
            if (!Equals(PermaText, other.PermaText)) return false;

            return true;
        }
        #endregion

        #region Implementation of IXmlSerializable

        virtual protected void ReadInitialAttributes(XmlReader reader)
        {
            elementType = reader.GetAttribute("elementType");
            typeDisplayName = reader.GetAttribute("displayName");
            //elementGuid = reader.GetAttribute("elementKey");
            var usePermaAttrib = reader.GetAttribute("usingPermaText");
            usePermaText = false;
            if (usePermaAttrib != null)
            {
                usePermaText = bool.Parse(usePermaAttrib);
            }
            var usePropNameInHoverTextAttrib = reader.GetAttribute("usePropertyNameInHoverText");
            usePropertyNameInHoverText = false;
            if (usePropNameInHoverTextAttrib != null)
            {
                usePropertyNameInHoverText = bool.Parse(usePropNameInHoverTextAttrib);
            }
            var defCategory = reader.GetAttribute("definitionGroup");
            if (string.IsNullOrEmpty(defCategory))
            {
                defCategory = "Default";
            }
            DefinitionGroup = defCategory;
        }

        public XmlSchema GetSchema() { throw new NotImplementedException(); }

        virtual public void ReadXml(XmlReader reader)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (!reader.Name.Equals("DepictionElement", StringComparison.InvariantCultureIgnoreCase)) return;

            ReadInitialAttributes(reader);
            reader.Read();
            string nodeName;
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (reader.Name.Equals("properties", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                var properties = SerializationService.DeserializeItemList<IElementProperty>(nodeName, typeof(DepictionElementProperty), reader) as List<IElementProperty>;
                propertyDictionary.Clear();
                if (properties != null)
                {
                    foreach (var prop in properties)
                    {
                        var key = prop.InternalName.ToLowerInvariant();
                        IElementProperty updatedProp = null;
                        #region Element prototype porting from 1.2
                        //1.2.2 porting
                        if (key.Equals("StrokeColor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            updatedProp = new DepictionElementProperty("ZOIBorder", "ZOI border color", prop.Value);
                            key = updatedProp.InternalName.ToLowerInvariant();
                        }
                        else if (key.Equals("FillColor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            updatedProp = new DepictionElementProperty("ZOIFill", "ZOI fill color", prop.Value);
                            key = updatedProp.InternalName.ToLowerInvariant();
                        }
                        else if (key.Equals("BorderColor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            updatedProp = new DepictionElementProperty("IconBorderColor", "Icon border color", prop.Value);
                            key = updatedProp.InternalName.ToLowerInvariant();
                        }
                        #endregion
                        if (!propertyDictionary.ContainsKey(key))
                        {
                            if (updatedProp != null)
                            {
                                updatedProp.Deletable = false;
                                prop.Rank = 10;

                                propertyDictionary.Add(key, updatedProp);
                            }
                            else
                            {
                                propertyDictionary.Add(key, prop);
                            }
                        }
                    }
                }
                //Brutal, this would normally replace all existing preset properties,so alas it is not a straight reset of the property list.
                //HOpefullyt that doesn't make badthings happen later.
                //                var newProps = SerializationService.DeserializeItemList<IElementProperty>(nodeName, typeof(DepictionElementProperty), reader) as List<IElementProperty>;
                //                foreach(var prop in newProps)
                //                {
                //                    AddPropertyOrReplaceValueAndAttributes(prop, false);
                //                }
            }
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            //elementWaypoints
            if (reader.Name.Equals("elementWaypoints"))
            {
                waypoints = SerializationService.DeserializeItemList<IDepictionElementWaypoint>("elementWaypoints", typeof(DepictionElementWaypoint), reader).ToList();
            }
            if (SerializationService.IsSaveLoadCancelled() == true) return;

            permaText = new DepictionPermaText();
            if (reader.Name.Equals("PermaText", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                permaText = SerializationService.DeserializeObject<DepictionPermaText>(nodeName, reader);
            }

            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (reader.Name.Equals("Tags", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                var tagList = SerializationService.DeserializeItemList<string>(nodeName, typeof(string), reader);
                foreach (var tag in tagList) { tags.Add(tag); }
            }
            if (reader.Name.Equals("ZoneOfInfluence", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                var loadedZOI = SerializationService.DeserializeObject<ZoneOfInfluence>(nodeName, reader);
                if (loadedZOI.IsEmpty && ElementType.Equals("Depiction.Plugin.RoadNetwork"))
                {
                    var prop = GetPropertyByInternalName("Roadgraph");
                    if (prop != null)
                    {
                        var roadGraph = prop.Value as RoadGraph;
                        if (roadGraph != null)
                        {
                            loadedZOI = new ZoneOfInfluence(roadGraph.ConvertToGeometry());
                        }
                    }
                }
                else if (loadedZOI.IsEmpty)
                {
                    var prop = GetPropertyByInternalName("Position");
                    if (prop != null)
                    {
                        var latitudeLongitude = prop.Value as LatitudeLongitude;
                        if (latitudeLongitude != null)
                        {
                            loadedZOI = new ZoneOfInfluence(new DepictionGeometry(latitudeLongitude));
                        }
                    }
                }
                loadedZOI.Owner = this;
                zoneOfInfluence = loadedZOI;
            }
            imageMetadata = new DepictionImageMetadata();
            if (reader.Name.Equals("ImageMetadata", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                imageMetadata = SerializationService.DeserializeObject<DepictionImageMetadata>(nodeName, reader);
            }

            if (reader.Name.Equals("onClick", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                clickActions = UnifiedDepictionElementReader.ReadElementActions(reader, nodeName);
            }
            if (reader.Name.Equals("onCreate", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                createActions = UnifiedDepictionElementReader.ReadElementActions(reader, nodeName);
            }

            if (reader.Name.Equals("generateZoi", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                generateZoiActions = UnifiedDepictionElementReader.ReadElementActions(reader, nodeName);
            }
            if (reader.Name.Equals("onDelete", StringComparison.InvariantCultureIgnoreCase))
            {
                nodeName = reader.Name;
                deleteActions = UnifiedDepictionElementReader.ReadElementActions(reader, nodeName);
            }
            //Need to get a good way of ending
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("DepictionElement", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra");
                reader.Read();

            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            UnifiedDepictionElementWriter.WriteDepictionElementBase(writer, this, true);
        }

        #endregion

    }
}