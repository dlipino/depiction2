using System.Collections.Generic;
using System.Diagnostics;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.CoreModel.Service.CoreBackgroundServices
{
    public class WfsElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            var name = "Wfs Element Importer";
            if (args.ContainsKey("name"))
            {
                name = args["name"].ToString();
            }
            UpdateStatusReport(name);
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var area = parameters["Area"] as IMapCoordinateBounds;
            if (area == null) return null;

            if (!parameters.ContainsKey("ServiceType"))
            {
                parameters.Add("ServiceType", "WFS");
            }

            var prototypes = DepictionWxsToElementService.GetDataFromWxSUsingParameter(parameters, area, this);
            if (prototypes == null) return null;

            foreach (var prototype in prototypes)
            {
//                if (parameters.ContainsKey("Tag"))
//                {
//                    var tag = parameters["Tag"] as string;
//                    if (!string.IsNullOrEmpty(tag))
//                    {
//                        prototype.Tags.Add(tag);
//                    }
//                }

                if (parameters.ContainsKey("hoverText"))
                {
                    var hoverKeys = parameters["hoverText"].ToString();
                    var allHoverKeys = hoverKeys.Split('|');
                    foreach (var hoverKey in allHoverKeys)
                    {
                        var prop = prototype.GetPropertyByInternalName(hoverKey);
                        if (prop != null) prop.IsHoverText = true;
                        else
                        {
                            Debug.WriteLine("error with an element");
                        }
                    }
                }
            }
            return prototypes;

            //            var service = new WxsDataService(parameters, area);
            //            Response response;
            //            string filePath = "";
            //            filePath = service.RequestFileData(filePath, out response);
            //
            //            var elementType = parameters["elementType"].ToString();
            //
            //
            //            if (File.Exists(filePath))
            //            {
            //                UpdateStatusReport("Getting data from downloaded file.");
            //                var ogrParser = new OsGeoFileTypeReaderToDepictionElements();
            //
            //                var elements = ogrParser.GetElementsFromShapeFile(filePath, elementType, area, parameters["description"].ToString(),
            //                                                            parameters["name"].ToString(), true /*use zoi color from DML*/, this);
            //                if (parameters.ContainsKey("Tag"))
            //                {
            //                    var tag = parameters["Tag"] as string;
            //                    if (!string.IsNullOrEmpty(tag))
            //                    {
            //                        foreach (var element in elements)
            //                        {
            //                            element.Tags.Add(tag);
            //                        }
            //                    }
            //                }
            //
            //                if (parameters.ContainsKey("hoverText"))
            //                {
            //                    foreach (var element in elements)
            //                    {
            //                        string hoverKey = parameters["hoverText"].ToString();
            //                        var prop = element.GetPropertyByInternalName(hoverKey);
            //                        if (prop != null) prop.IsHoverText = true;
            //                        else
            //                        {
            //                            Debug.WriteLine("error with an element");
            //                        }
            //
            //                    }
            //                }
            //                return elements;
            //            }
            //            return null;
        }


        protected override void ServiceComplete(object args)
        {
            var elements = args as List<IElementPrototype>;
            if (elements == null) return;

            if (elements.Count > 0 && DepictionAccess.CurrentDepiction != null)
            {
                UpdateStatusReport("Adding elements to depiction");
                DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(elements, true,false);
            }
            //            var elements = args as List<IDepictionElement>;
            //            if (elements == null) return;
            //
            //            if (elements.Count > 0 && DepictionAccess.CurrentDepiction != null)
            //            {
            //                UpdateStatusReport("Adding elements to depiction");
            //                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, true);
            //            }
        }

        #endregion
    }
}