using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.Service.FileReading;
using Ionic.Zip;

namespace Depiction.CoreModel.Service.CoreBackgroundServices
{
    public class KMLKMZBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var fileName = parameters["FileName"].ToString();
            var elementType = parameters["ElementType"].ToString();
            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (File.Exists(fileName))
            {
                var elements = ProcessKMZKMLFileIntoRawPrototypes(fileName, elementType, regionBounds);
                var tag = Tags.DefaultFileTag + Path.GetFileNameWithoutExtension(fileName);
                if(parameters.ContainsKey("Tag"))
                {
                    tag = parameters["Tag"].ToString();
                }
                foreach (var thing in elements)
                {
                    thing.Tags.Add(tag);
                }
                UpdateStatusReport(string.Format("Finished reading {0} elements,preparing to display.", elements.Count));
                return elements;
                //                var list = ProcessKMZKMLFileIntoElements(fileName, elementType, regionBounds);
                //                if (list != null && list.Count > 0 && DepictionAccess.CurrentDepiction != null)
                //                    DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(list, true);

            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
//            var list = args as List<IDepictionElement>;
//            if (list != null && list.Count > 0 && DepictionAccess.CurrentDepiction != null)
//            {
//                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(list, true);
//
//                DepictionAccess.CurrentDepiction.RequestElementsWithIdsViewing(list.Select(t => t.ElementKey), new[] { DepictionDialogType.PropertyEditor });
//            }
            var elements = args as List<IElementPrototype>;
            if (elements == null) return;
            //Reading the CSV file happens too fast for the UI thread.
            UpdateStatusReport(string.Format("Loading {0} elements into the depiction", elements.Count));
            if (DepictionAccess.CurrentDepiction != null)
            {
                List<IDepictionElement> updatedElements;
                List<IDepictionElement> createdElements;
                DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(elements, out createdElements, out updatedElements, true, false);
                foreach (var element in updatedElements)
                {
                    element.ElementUpdated = true;
                }
                var createdElementIds = from ce in createdElements
                                        select ce.ElementKey;
                var updatedElementIds = from ce in updatedElements
                                        select ce.ElementKey;
                var elementIds = createdElementIds.Concat(updatedElementIds).ToList();

                DepictionAccess.CurrentDepiction.RequestElementsWithIdsViewing(elementIds, new[] { DepictionDialogType.PropertyEditor });
            }
        }

        #endregion

        #region private helpers
        static public List<IElementPrototype> ProcessKMZKMLFileIntoRawPrototypes(string filePath, string elementType, IMapCoordinateBounds depictionRegion)
        {
            string fileName = filePath;
            var fileType = ProductAndFolderService.GetFileType(filePath);
            if (fileType == DepictionReadableFileTypes.KMZ)
            {
                fileName = ExtractKmzFolder(filePath);
            }
            var tag = Tags.DefaultFileTag + Path.GetFileNameWithoutExtension(filePath);
            var processor = new KmlKmzProcessor(fileName);
            var prototypes = processor.ProcessKmlKmzAndGetPrototypes(depictionRegion, elementType, true);
            foreach (var element in prototypes)
            {
                if (element == null) continue;
                element.Tags.Add(tag);
                element.UseEnhancedPermaText = true;

            }
            return prototypes;
        }

//        static public List<IDepictionElement> ProcessKMZKMLFileIntoElements(string filePath, string elementType, IMapCoordinateBounds depictionRegion)
//        {
//            string fileName = filePath;
//            var fileType = ProductAndFolderService.GetFileType(filePath);
//            if (fileType == DepictionReadableFileTypes.KMZ)
//            {
//                fileName = ExtractKmzFolder(filePath);
//            }
//            var tag = Tags.DefaultFileTag + Path.GetFileNameWithoutExtension(filePath);
//            var processor = new KmlKmzProcessor(fileName);
//            var elementBaseTypes = processor.ProcessKmlKmzAndBaseElementTypes(depictionRegion, elementType, true,false);
//            var elements = new List<IDepictionElement>();
//            foreach (IDepictionElement element in elementBaseTypes)
//            {
//                if (element == null) continue;
//                element.Tags.Add(tag);
//                element.UseEnhancedPermaText = true;
//
//            }
//            return elements;
//        }

        /// <summary>
        /// Return the first file name in this directory structure that has this file extension.
        /// Uses a breadth-first recursive serarch.
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        static private string RecursivelyFindFirstFileWithExtension(string folder, string fileExtension)
        {
            foreach (string filename in Directory.GetFiles(folder))
            {
                if (Path.GetExtension(filename).ToLower() == fileExtension)
                    return filename;
            }
            foreach (string dir in Directory.GetDirectories(folder))
            {
                string filename = RecursivelyFindFirstFileWithExtension(dir, fileExtension);
                if (filename != null)
                    return filename;
            }
            return null;
        }

        static private string ExtractKmzFolder(string path)
        {
            var fileInfo = new FileInfo(path);

            string tempFolder = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid().ToString("N"));
            Directory.CreateDirectory(tempFolder);
            using (var zip = new ZipFile(fileInfo.FullName))
            {
                zip.ExtractAll(tempFolder);
            }
            string kmlFilename = RecursivelyFindFirstFileWithExtension(tempFolder, ".kml");
            return kmlFilename;
        }
        #endregion
    }
}