using System.Collections.Generic;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.WebDataInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.OGCServices;
using Depiction.CoreModel.DepictionObjects.Repositories;

namespace Depiction.CoreModel.Service.CoreBackgroundServices
{
    public class WcsElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        private IMapCoordinateBounds depictionRegion;
        string elevationUnit = "m";
        private string tags = "";
        private WcsDataProvider dataProvider = null;

        #region Overrides of BaseDepictionBackgroundThreadOperation

        public override void StopBackgroundService()
        {
            if (dataProvider != null)
            {
                dataProvider.Cancel();
            }
            base.StopBackgroundService();
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            if (args.ContainsKey("Tag"))
            {
                tags = args["Tag"].ToString();
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            if (!parameters.ContainsKey("Area")) return null;
            var area = parameters["Area"] as IMapCoordinateBounds;
            depictionRegion = area;
            return GetRequestedFileName(area, parameters);
        }
        
        protected override void ServiceComplete(object args)
        {
            var fileName = args as string;
            if (fileName == null) return;
            var importers = AddinRepository.Instance.DefaultImporters;
            IDepictionDefaultImporter importerToUse = null;
            foreach (var importer in importers)
            {
                var metadata = importer.Key;
                var typeList = new List<string>(metadata.ElementTypesSupported);
                var extensionList = new List<string>(metadata.ExtensionsSupported);

                if (typeList.Contains("Depiction.Plugin.Elevation")
                    & extensionList.Contains(".tiff"))
                {
                    importerToUse = importer.Value;
                    break;
                }
            }
            if (importerToUse == null) return;
            var parameters = new Dictionary<string, string>();
            parameters.Add("units", elevationUnit);
            UpdateStatusReport("Retrieved elevation file");
            //TODO make tags sendable as a list
            if (string.IsNullOrEmpty(tags))
            {
                //parameters.Add("Tag", Tags.DefaultQuickStartTag);
            }
            else
            {
                parameters.Add("Tag", tags);
            }
            importerToUse.ImportElements(fileName, "Depiction.Plugin.Elevation", depictionRegion, parameters);
        }

        #endregion

        private string GetRequestedFileName(IMapCoordinateBounds area, Dictionary<string, object> parameters)
        {
            depictionRegion = area;
            string url = "";
            if (parameters.ContainsKey("url"))
                url = parameters["url"].ToString();

            if (parameters.ContainsKey("units"))
                elevationUnit = parameters["units"].ToString() ?? "m";
            string layerName = "";
            if (parameters.ContainsKey("layerName"))
                layerName = parameters["layerName"].ToString() ?? "";

            string imageFormat = "GeoTIFF";
            if (parameters.ContainsKey("format"))
                imageFormat = parameters["format"].ToString();

            string request = "";
            if (parameters.ContainsKey("request"))
                request = parameters["request"].ToString() ?? "";

            var descriptor = "";
            if (parameters.ContainsKey("name"))
                descriptor = parameters["name"].ToString() ?? "";

            dataProvider = new WcsDataProvider(area, url, layerName, imageFormat, request, descriptor);
            Response data;
            dataProvider.RequestFileData("", out data);

            if (data != null && data.IsRequestSuccessful)
            {
                return data.ResponseFile;
            }
            return null;
        }
    }
}