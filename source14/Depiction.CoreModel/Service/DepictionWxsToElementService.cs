using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using CarbonTools.Content.OGC;
using CarbonTools.Content.OGC.Capabilities;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.MessagingInterface;
using Depiction.API.Interfaces.WebDataInterfaces;
using Depiction.API.OGCServices;

namespace Depiction.CoreModel.Service
{ //URL used to verify this: http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
    //http://preview.grid.unep.ch:8080/geoserver/ows? //layers
    public class DepictionWxsToElementService : IDepictionWxsToElementService
    {
        public const string elementTypeParamString = "elementType";

        public string WebServiceType { get; set; }
        public string ServiceURL { get; set; }
        public string LayerNames { get; set; }
        public string FirstLayerTitle { get; set; }
        public string Style { get; set; }
        public string ImageFormat { get; set; }
        HandlerOGCCapabilities OgcCapabiliesHandler { get; set; }
        private EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

        public void ClearAdditionalInfo()
        {
            Style = string.Empty;
            ImageFormat = string.Empty;
            LayerNames = string.Empty;
            FirstLayerTitle = string.Empty;
        }
        #region static thing
        //This sets tags if they are available in the filterParameters "Tag"
        static public List<IElementPrototype> GetDataFromWxSUsingParameter(Dictionary<string, object> filterParameters, IMapCoordinateBounds regionExtent,
            IDepictionBackgroundService backgroundService)
        {
            if (regionExtent == null) return null;

            if (!filterParameters.ContainsKey("ServiceType"))
            {
                filterParameters.Add("ServiceType", "WFS");
            }

            var service = new WxsDataService(filterParameters, regionExtent);
            Response response;
            string filePath = "";
            filePath = service.RequestFileData(filePath, out response);

            var elementType = string.Empty;
            if (filterParameters.ContainsKey(elementTypeParamString))
            {
                elementType = filterParameters[elementTypeParamString].ToString();
            }
            

            if (File.Exists(filePath))
            {
                if(backgroundService != null)
                {
                    backgroundService.UpdateStatusReport("Getting data from downloaded file.");
                }
                var ogrParser = new OsGeoFileTypeReaderToDepictionElements();

                object description;
                object name;
                filterParameters.TryGetValue("description", out description);
                if (description == null) description = string.Empty;
                filterParameters.TryGetValue("name", out name);
                if (name == null) name = string.Empty;
                var prototypes = ogrParser.GetElementPrototypesFromShapeFile(filePath, elementType, regionExtent, description.ToString(),
                                                            name.ToString(), true /*use zoi color from DML*/, backgroundService);
                
//                var tag = Tags.DefaultFileTag + Path.GetFileName(fullName);
                if (filterParameters.ContainsKey("Tag"))
                {
                    var tag =  filterParameters["Tag"].ToString();
                    if (!string.IsNullOrEmpty(tag))
                    {
                        foreach (var prototype in prototypes)
                        {
                            prototype.Tags.Add(tag);
                        }
                    }
                }

                return prototypes;
            }
            return null;
        }
        #endregion


        private SourceOGCCapabilities CreateOGCSource(string serverUrl, string serverType)
        {
            if (serverUrl == null) return null;
            Uri uri;
            try
            {
                uri = new Uri(serverUrl);
            }
            catch (UriFormatException)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Invalid url: {0}\nPlease enter valid url.", serverUrl));
                return null;
            }
            var ogcCapSource = new SourceOGCCapabilities { Address = uri };
            if (serverType == (WxSInformationType.WFS).ToString())
            {
                ogcCapSource.ServiceType = OGCServiceTypes.WFS;
                WebServiceType = serverType;
            }
            else if (serverType == (WxSInformationType.WMS).ToString())
            {
                ogcCapSource.ServiceType = OGCServiceTypes.WMS;
                WebServiceType = serverType;
            }
            //else if (serverType == (ServerType.WCS).ToString())
            //    ogcCapSource.ServiceType = OGCServiceTypes.WCS;
            //include the above else when WCS is functioning
            //uncomment the WCS line in ServerTypeEnum.cs too
            ServiceURL = serverUrl;

            return ogcCapSource;
        }

        private DataOGCCapabilities ogcData = null;
        
        public void ResetOGCData()
        {
            ogcData = null;
        }
        /// <summary>
        /// Obtains GetCapability of the serverUrl passed
        /// Also sets the serverOnlineAddress string to be the OnlineResource
        /// address contained in the GetCapabilities document
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="serverType"></param>
        /// <returns>list of layer names contained in the server</returns>
        public Dictionary<string,string> GetLayerNamesFromServer(string serverUrl, string serverType)
        {
            Dictionary<string, string> layerNames;

            var ogcCapSource = CreateOGCSource(serverUrl, serverType);

            //
            //Upon Carbontools support's advice, we are using the handlers in asynch mode
            //
            //URL used to verify this: http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
            //Feb 27 2010
//            var ogcHandler = new HandlerOGCCapabilities(ogcCapSource) { Synchronous = false };
//            ogcHandler.OperationDone += ogcCapabilitiesHandler_OperationDone;
//            ogcHandler.GetCapabilities();
//            waitHandle.WaitOne();
//            waitHandle.Reset();
            var ogcHandler = GetOGCCapabilities(ogcCapSource);
             if (ogcHandler == null )
             {
                 return new Dictionary<string, string>(); 
             }
            if ( ogcHandler.Data != null && ((DataOGCCapabilities)ogcHandler.Data).LayerItems != null && ((DataOGCCapabilities)ogcHandler.Data).LayerItems.Count == 0 && ogcCapSource.ServiceType == OGCServiceTypes.WFS)
            {
                //try a different version of WFS
                ogcCapSource.Version = "1.0.0";
                ogcHandler = GetOGCCapabilities(ogcCapSource);
            }

            //
            //Get all layer names
            //
             ogcData = ogcHandler.Data as DataOGCCapabilities;
            if (ogcData != null && ogcData.LayerItems != null)
            {
                layerNames = new Dictionary<string, string>();
                foreach (LayerItem item in ogcData.LayerItems)
                {
                    if (!string.IsNullOrEmpty(item.Name))
                    {
                        layerNames.Add(item.Name, item.Title + " (" + item.Name + ")");
                    }
                }
            }
            else
            {
                //layer items was empty or null
                layerNames = new Dictionary<string, string>();
            }

            //Find the service address using the request items in capabilities
            if (ogcData != null)
            {
                RequestItem requestItem = null;
                if (serverType == "WFS") requestItem = ogcData.RequestItems.Find("GetFeature");
                else if (serverType == "WMS") requestItem = ogcData.RequestItems.Find("GetMap");

                //Set the WFS Address
                string address = null;
                if (requestItem != null && serverType == "WFS") address = requestItem.GetDCPOnlineResource("HTTP", "POST");
                else if (requestItem != null && serverType == "WMS") address = requestItem.GetDCPOnlineResource("HTTP", "GET");
                //if (address != null) serverOnlineAddress = address;
            }

            return layerNames;
        }

        public List<string> GetMapStylesFromLayer(string layerName)
        {
            if (ogcData == null) return null;
            var liWms = GetLayerItemFromOGCData(layerName, ogcData);
            //Get map styles from layer item
            if (liWms == null) return null;
            List<MapStyle> styles = liWms.GetStyles();

            var mapStyles = new List<string>();
            if (styles != null)
            {
                foreach (var style in styles)
                {
                    mapStyles.Add(style.Name);
                }
            }

            return mapStyles;
        }


        ///
        public List<string> GetFormatsFromLayer(string layerName)
        {
            if (ogcData == null) return null;
            var liWms = GetLayerItemFromOGCData(layerName, ogcData);
            //Get map styles from layer item
            if (liWms == null) return null;

            var requestItem = ogcData.RequestItems.Find("GetMap");
            if(requestItem == null) return new List<string>();
            var mapFormats = new List<string>(requestItem.Formats);

            return mapFormats;
        }
        #region static OGC capabilies for WxS stuff
        private LayerItemWMS GetLayerItemFromOGCData(string layerName, DataOGCCapabilities ogcCapData)
        {
            if (ogcCapData.LayerItems != null)
            {
                foreach (LayerItem item in ogcCapData.LayerItems)
                {
                    if (item.Name == layerName)
                        return item as LayerItemWMS;
                }
            }
            return null;
        }
        public void CancelCababiliesSearch()
        {
            if (OgcCapabiliesHandler != null && OgcCapabiliesHandler.IsWorking)
            {
                OgcCapabiliesHandler.Abort();
                waitHandle.Set();
            }
        }
        
        public HandlerOGCCapabilities GetOGCCapabilities(SourceOGCCapabilities ogcCapSource)
        {
            if (ogcCapSource == null) return null;
            //
            //Upon Carbontools support's advice, we are using the handlers in asynch mode
            //
            //URL used to verify this: http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
            //http://preview.grid.unep.ch:8080/geoserver/ows? //layers
            //Feb 27 2010
            OgcCapabiliesHandler = new HandlerOGCCapabilities(ogcCapSource) { Synchronous = false };
            OgcCapabiliesHandler.OperationDone += ogcCapabilitiesHandler_OperationDone;
            OgcCapabiliesHandler.OperationError += ogcCapabilitiesHandler_OperationDone;

            OgcCapabiliesHandler.GetCapabilities();
            waitHandle.WaitOne();
            waitHandle.Reset();
            return OgcCapabiliesHandler;
        }
        void ogcCapabilitiesHandler_OperationDone(object sender, EventArgs e)
        {
            try { }
            finally
            {
                waitHandle.Set();
            }
        }
        #endregion
    }
}