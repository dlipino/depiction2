﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using Depiction.API;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.DepictionTypeInterfaces;

namespace Depiction.CoreModel.Service
{
    /// <summary>
    /// Service that gets the directory paths requested by the user
    /// </summary>
    public class ApplicationPathService : IApplicationPathService//SHould be renamed to depictionApplicationPathService
    {
        private string folderInTempFolderRootPath;
        private string topLevelAppDirName = string.Empty;

        #region COnstructor
        public ApplicationPathService(string baseDir)
        {
            if(string.IsNullOrEmpty(baseDir))
            {
                topLevelAppDirName = DepictionAccess.ProductInformation.DirectoryNameForCompany;
            }else
            {
                topLevelAppDirName = baseDir;
            }
            if (Application.Current != null)
            {
                Application.Current.Startup += TempPathCleanup_Current_Startup;
                Application.Current.Exit += TempPathCleanup_Current_Exit;
            }
        }

        public ApplicationPathService():this(string.Empty){}

        #endregion
        #region Constructor events
        private void TempPathCleanup_Current_Startup(object sender, StartupEventArgs e)
        {
            Directory.CreateDirectory(UserElementsDirectoryPath);
            Directory.CreateDirectory(UserElementIconDirectoryPath);
            Directory.CreateDirectory(UserInteractionRulesDirectoryPath);
            Directory.CreateDirectory(UserInstalledAddinsDirectoryPath);
        }

        void TempPathCleanup_Current_Exit(object sender, ExitEventArgs e)
        {
            RemoveInstanceTempFolder();
        }

        #endregion

        #region Default directory properties

        #region default depiction directories

        public string AppDataDirectoryPath
        {
            get
            {
                return GetUserDataPath();
                //return EnsurePath(Path.Combine(GetUserDataPath(), productInfo.DirectoryNameForCompany));
            }
        }
        public string DepictionElementPath
        {
            get
            {
                return EnsurePath(Path.Combine(FolderInTempFolderRootPath, "DepictionElements"));
            }
        }
        public string DepictionAddinPath
        {
            get
            {
                return EnsurePath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Default Add-ons"));
            }
        }
        public string TempFolderRootPath
        {
            get
            {
                return EnsurePath(Path.Combine(Path.GetTempPath(), topLevelAppDirName));
            }
        }
        public string FolderInTempFolderRootPath
        {
            get
            {
                if (String.IsNullOrEmpty(folderInTempFolderRootPath))
                {
                    try
                    {
                        folderInTempFolderRootPath = Path.Combine(TempFolderRootPath, Guid.NewGuid().ToString());
                    }
                    catch (Exception ex)
                    {
                        //                        DepictionAccess.NotificationService.WriteToLog("Unable to get Temporary folder path", ex);
                        return null;
                    }
                }
                // Make sure this directory is there on each request, just in case it was deleted by somebody, somewhere, sometime.
                if (!Directory.Exists(folderInTempFolderRootPath))
                {
                    Directory.CreateDirectory(folderInTempFolderRootPath);
                }
                return folderInTempFolderRootPath;
            }
        }
        #endregion

        #region directory for user added things
        public string UserElementsDirectoryPath
        {
            get
            {
                return EnsurePath(Path.Combine(GetUserDataPath(), "UserElements"));
            }
        }

        public string UserInstalledAddinsDirectoryPath
        {
            get
            {
                return EnsurePath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Add-ons"));
            }
        }

        public string UserElementIconDirectoryPath
        {
            get
            {
                return EnsurePath(Path.Combine(GetUserDataPath(), "UserElementIcons"));
            }
        }

        public string UserInteractionRulesDirectoryPath
        {
            get
            {
                return EnsurePath(Path.Combine(GetUserDataPath(), "UserInteractionRules"));
            }
        }

//        public string UserInteractionRulesFileName
//        {
//            get
//            {
//                return "UserInteractions.xml";
//            }
//        }
        #endregion
        #endregion

        #region private helpers
        /// <summary>
        ///  If this directory does not exist, create it.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string EnsurePath(string path)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }catch(Exception ex)
                {
                    
                }
            }
            return path;
        }
        private void DoCleanup(string folderToClean)
        {
            try
            {
                if (Directory.Exists(folderToClean))
                {
                    Directory.Delete(folderToClean, true);
                }
            }
            catch (Exception ex)
            {
                //                DepictionExceptionHandler.HandleException("Unable to read temporary folder", ex, false);
            }
        }
        #endregion

        #region public helpers
        public string GetUserDataPath()
        {
            return EnsurePath(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), topLevelAppDirName));
        }
        /// <summary>
        /// Cleans up the temporary files created by Depiction.
        /// </summary>
        public void RemoveInstanceTempFolder()
        {
            DoCleanup(FolderInTempFolderRootPath);
        }

        #endregion
        #region older stuff that should be updated
        public void CacheTerrainFile(string filename, string tileKey)
        {

            string tilePath = PathToTileFile(tileKey);
            try
            {
                if (File.Exists(filename))
                {
                    File.Copy(filename, tilePath);
                }
            }
            catch (ExternalException ex)
            {
                // Log it and forget it ... this is an odd GDI+ error
                //DepictionExceptionHandler.HandleException(ex, false);
            }
            catch
            {
                //NotificationService.SendNotification(string.Format("{0} has recovered from an error that occurred while saving a cached tile.", UIAccess._productInformation.ProductName));
            }
        }
        private string PathToTileFile(string fileName)
        {
            return Path.Combine(DepictionCacheDirectory, fileName);
        }

        public string CacheFile(Stream buffer, string fileName)
        {
            if (buffer == null)
                return null;
            string fileSavePath = PathToCacheFile(fileName);
            using (var fileStream = new FileStream(fileSavePath, FileMode.Create))
            {
                buffer.CopyTo(fileStream);
            }
            return fileSavePath;
        }

        private string PathToCacheFile(string fileName)
        {
            return Path.Combine(DepictionCacheDirectory, fileName);
        }
        #endregion

        #region older things that may no longer be needed

        public string ReadOnlyAddInElementsPath//These are the files that are saved with the depictoin
        {
            get
            {
                //return Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), productInfo.DirectoryNameForCompany), "UserElements");
                return Path.Combine(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles), topLevelAppDirName), "Elements");
            }
        }

        public string DepictionCacheDirectory
        {
            get
            {
                return EnsurePath(Path.Combine(GetUserDataPath(), "Cache"));
            }
        }
        public string RetrieveCachedFilePathIfCached(string fileName)
        {
            var tilePath = PathToCacheFile(fileName);
            return File.Exists(tilePath) ? tilePath : null;
        }

        public void DeleteCachedFile(string fileName)
        {
            var filePath = PathToCacheFile(fileName);
            if (File.Exists(filePath))
                File.Delete(filePath);
        }
        /// <summary>
        /// Will try to create this directory, if it doesn't exist.
        /// Returns the directory if found or could create it.
        /// Returns null if cannot find or create it.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public string TryThisDirectory(string directory)
        {
            if (String.IsNullOrEmpty(directory))
                return null;
            if (!Directory.Exists(directory))
            {
                try
                {
                    Directory.CreateDirectory(directory);
                }
                catch
                {
                    return null;
                }
            }
            return directory;
        }
        #endregion 
    }
}