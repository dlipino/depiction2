﻿using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.ValueTypes;
using GeoFramework;

namespace Depiction.CoreModel.ExtensionMethods
{
    public static class DepictionLatLongExtensionMethods
    {
//        public static ILatitudeLongitude UTMToLatLong(this UTMLocation utm)
//        {
//            var position = new Position(utm.UTMZoneLetter, utm.UTMZoneNumber, new Distance(utm.EastingMeters, DistanceUnit.Meters),
//                                        new Distance(utm.NorthingMeters, DistanceUnit.Meters));
//            var x = position.Longitude.DecimalDegrees;
//            var y = position.Latitude.DecimalDegrees;
//
//            return new LatitudeLongitudeBase(y, x);
//        }

        /// <summary>
        /// Returns the distance between two points in the current measurement system and scale.
        /// </summary>
        /// <param name="originLatLon"></param>
        /// <param name="destinationLatLon"></param>
        /// <returns></returns>
        public static double DistanceTo(this ILatitudeLongitude originLatLon, ILatitudeLongitude destinationLatLon)
        {
            return ConvertDistanceToDisplayable(originLatLon.GetGeoPosition().DistanceTo(destinationLatLon.GetGeoPosition()));
        }

        //Minor hack so that geo frameworks doesn't need to be referenced in DepictionCanvas.
        static private string milesString = "miles";
        static private string kmString = "km";
        public static LatitudeLongitude TranslateTo(this ILatitudeLongitude latLon, double bearing, double distance, string unit)
        {
            Distance gfDistance;
            //Default miles
            gfDistance = new Distance(distance, DistanceUnit.StatuteMiles);
            if (unit.Equals(milesString))
                gfDistance = new Distance(distance, DistanceUnit.StatuteMiles);
            if (unit.Equals(kmString))
                gfDistance = new Distance(distance, DistanceUnit.Kilometers);

            var newPosition = latLon.GetGeoPosition().TranslateTo(bearing, gfDistance);
            return new LatitudeLongitude(newPosition.Latitude.DecimalDegrees, newPosition.Longitude.DecimalDegrees);
        }
        public static LatitudeLongitude TranslateTo(this ILatitudeLongitude latLon, double bearing, double distance, MeasurementSystem measurementSystem)
        {
            Distance gfDistance;

            switch (measurementSystem)
            {
                case MeasurementSystem.Imperial:
                    gfDistance = new Distance(distance, DistanceUnit.Feet);
                    break;
                case MeasurementSystem.Metric:
                    gfDistance = new Distance(distance, DistanceUnit.Meters);
                    break;
                default:
                    gfDistance = new Distance(distance, DistanceUnit.Meters);
                    break;
            }

            var newPosition = latLon.GetGeoPosition().TranslateTo(bearing, gfDistance);
            return new LatitudeLongitude(newPosition.Latitude.DecimalDegrees, newPosition.Longitude.DecimalDegrees);
        }
        public static ILatitudeLongitude TranslatePosition(this ILatitudeLongitude latLon, double easting, double northing, MeasurementSystem translateUnits)
        {
            return latLon.TranslateTo(Azimuth.East, easting, translateUnits).TranslateTo(Azimuth.North, northing, translateUnits);
        }
        private static double ConvertDistanceToDisplayable(Distance distance)
        {
            return Settings.Default.MeasurementSystem.Equals(MeasurementSystem.Imperial) ? distance.ToStatuteMiles().Value : distance.ToKilometers().Value;
        }

    }
}
