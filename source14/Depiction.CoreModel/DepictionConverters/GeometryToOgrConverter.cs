﻿using System;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using GisSharpBlog.NetTopologySuite.IO;
using OSGeo.OGR;
using Geometry = OSGeo.OGR.Geometry;


namespace Depiction.CoreModel.DepictionConverters
{
    public class GeometryToOgrConverter 
    {
        private static WKTReader wktReader;
        private static GeometryFactory geometryFactory;

        static public DepictionGeometry WktToZoiGeometry(string geometryWkt)
        {
            if (string.IsNullOrEmpty(geometryWkt)) return new DepictionGeometry();
            if (geometryFactory == null)
                geometryFactory = new GeometryFactory();
            if (wktReader == null)
                wktReader = new WKTReader(geometryFactory);
            //POINT (-1.#IND -1.#IND) some how this was getting saved, still don't know how
            try
            {
                var geom = wktReader.Read(geometryWkt);
                return new DepictionGeometry(geom);
            }catch(Exception ex)
            {
                //This should return an empty zoi
                return new DepictionGeometry();
            }
            
//            //i don't like this but
//            try
//            {
//                var zoi = new ZoneOfInfluence(new DepictionGeometry(geom));
//            }catch(Exception ex)
//            {
//                if (!geom.IsEmpty) geom = geom.Buffer(0.000001);
//            }

           
        }

        static public string ZOIGeometryToWkt(IZoneOfInfluence zoi)
        {
            var ogrGeom = ZOIToOgrGeometry(zoi);
            if (ogrGeom == null) return string.Empty;

            string wellKnownText;
            ogrGeom.ExportToWkt(out wellKnownText);
            return wellKnownText;
        }
        public static Geometry DepictionGeometryToOgrGeometry(IDepictionGeometry inGeo)
        {
            var geoType = ZoneOfInfluence.GeometryTypeFromIGeometry(inGeo);
            switch (geoType)
            {
                case DepictionGeometryType.Point:
                    return ZoiPointToOgrGeometry(inGeo.Geometry);
                case DepictionGeometryType.MultiPoint:
                    return ZoiMultiPointToOgrGeometry((IMultiPoint)inGeo.Geometry);
                case DepictionGeometryType.LineString:
                    return ZoiLineStringToOgrGeometry(inGeo.Geometry);
                case DepictionGeometryType.MultiLineString:
                    return ZoiMultiLineStringToOgrGeometry((IMultiLineString)inGeo.Geometry);
                case DepictionGeometryType.Polygon:
                    return ZoiPolygonToOgrGeometry((IPolygon)inGeo.Geometry);
                case DepictionGeometryType.MultiPolygon:
                    return ZoiMultiPolygonToOgrGeometry((IMultiPolygon)inGeo.Geometry);
            }
            throw new Exception(String.Format("Gml Exporter cannot export geometries of type: {0}", geoType));
        }
        public static Geometry ZOIToOgrGeometry(IZoneOfInfluence zoi)
        {
            if (zoi.IsEmpty) return null;
            if (!(zoi.Geometry is DepictionGeometry)) return null;
            var geom = (DepictionGeometry)zoi.Geometry;
            return DepictionGeometryToOgrGeometry(geom);
        }

        #region Private convertersion helpers

        private static Geometry ZoiPointToOgrGeometry(IGeometry pointGeom)
        {
            Geometry geom = new Geometry(wkbGeometryType.wkbPoint);
            geom.AddPoint_2D(pointGeom.Coordinate.X, pointGeom.Coordinate.Y);
            return geom;
        }

        private static Geometry ZoiMultiPointToOgrGeometry(IMultiPoint multiPoint)
        {
            var geom = new Geometry(wkbGeometryType.wkbMultiPoint);
            foreach (IPoint point in multiPoint.Geometries)
            {
                geom.AddGeometryDirectly(ZoiPointToOgrGeometry(point));
            }
            return geom;
        }

        private static Geometry ZoiLineStringToOgrGeometry(IGeometry zoiLineString)
        {
            var geom = new Geometry(wkbGeometryType.wkbLineString);
            foreach (var coord in zoiLineString.Coordinates)
            {
                geom.AddPoint_2D(coord.X, coord.Y);
            }
            return geom;
        }

        private static Geometry ZoiMultiLineStringToOgrGeometry(IMultiLineString multiLineStringZoiGeometry)
        {
            var geom = new Geometry(wkbGeometryType.wkbMultiLineString);
            foreach (ILineString lineStringGeom in multiLineStringZoiGeometry.Geometries)
            {
                geom.AddGeometryDirectly(ZoiLineStringToOgrGeometry(lineStringGeom));
            }
            return geom;
        }

        private static Geometry ZoiPolygonToOgrGeometry(IPolygon polyGeom)
        {
            var geom = new Geometry(wkbGeometryType.wkbPolygon);
            var linearRing = new Geometry(wkbGeometryType.wkbLinearRing);
            foreach (var coord in polyGeom.ExteriorRing.Coordinates)
                linearRing.AddPoint_2D(coord.X, coord.Y);
            geom.AddGeometryDirectly(linearRing);
            foreach (ILineString interiorRing in polyGeom.InteriorRings)
            {
                linearRing = new Geometry(wkbGeometryType.wkbLinearRing);
                foreach (var coord in interiorRing.Coordinates)
                    linearRing.AddPoint_2D(coord.X, coord.Y);
                geom.AddGeometryDirectly(linearRing);
            }
            return geom;
        }

        private static Geometry ZoiMultiPolygonToOgrGeometry(IMultiPolygon multiPolygon)
        {
            var geom = new Geometry(wkbGeometryType.wkbMultiPolygon);
            foreach (IPolygon poly in multiPolygon.Geometries)
            {
                geom.AddGeometryDirectly(ZoiPolygonToOgrGeometry(poly));
            }
            return geom;
        }
        #endregion
    }
}