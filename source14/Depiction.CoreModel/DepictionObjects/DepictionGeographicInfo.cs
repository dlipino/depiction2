﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects
{
    public class DepictionGeographicInfo : IDepictionGeographicInfo, IXmlSerializable
    {
        public event Action DepictionSpecificsChange;
        #region Variables

        protected static List<string> regionCodes = new List<string>();//What the heck is this for?

        public LatitudeLongitude bottomRightOfWorld = new LatitudeLongitude(-85, 180);
        public LatitudeLongitude topLeftOfWorld = new LatitudeLongitude(85, -180);
        static public MapCoordinateBounds WorldLatLongBoundingBox = new MapCoordinateBounds(new LatitudeLongitude(85, -180), new LatitudeLongitude(-85, 180));

        private ILatitudeLongitude startLocation;
        private IMapCoordinateBounds mapWindow;
        private IMapCoordinateBounds regionBounds;

        #endregion
        #region ElementProperties

        public ILatitudeLongitude DepictionStartLocation
        {
            get { return startLocation; }
            set
            {
                startLocation = value;
                NotifyChange();
            }
        }
        public IMapCoordinateBounds DepictionMapWindow { get { return mapWindow; } set { mapWindow = value; NotifyChange(); } }
        public IMapCoordinateBounds DepictionRegionBounds { get { return regionBounds; } set { regionBounds = value; NotifyChange(); } }
        public string[] RegionCodes
        {
            get { return regionCodes.Distinct().ToArray(); }
        }

        #endregion
        #region Constructor
        public DepictionGeographicInfo()
        {
            DepictionStartLocation = new LatitudeLongitude(47.6505, -122.3215);//Seattle
            DepictionMapWindow = new MapCoordinateBounds();
            DepictionRegionBounds = new MapCoordinateBounds();
        }
        public DepictionGeographicInfo(IEnumerable<string> inRegionCodes)
        {
            DepictionStartLocation = new LatitudeLongitude(47.6505, -122.3215);//Seattle
            DepictionMapWindow = new MapCoordinateBounds();
            DepictionRegionBounds = new MapCoordinateBounds();
            regionCodes.Clear();
            foreach (var region in inRegionCodes)
            {
                regionCodes.Add(region);
            }
        }

        #endregion
        #region Event helper
        public void NotifyChange()
        {
            if (DepictionSpecificsChange != null)
            {
                DepictionSpecificsChange.Invoke();
            }
        }
        #endregion
        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {//USed in Equals(obj,obj)
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (DepictionGeographicInfo)obj;

            if (RegionCodes.Length != other.RegionCodes.Length) return false;
            for (int i = 0; i < regionCodes.Count; i++)
            {
                if (!RegionCodes[i].Equals(other.RegionCodes[i])) return false;
            }

            if (!Equals(DepictionMapWindow, other.DepictionMapWindow)) return false;
            if (!Equals(DepictionRegionBounds, other.DepictionRegionBounds)) return false;
            if (!Equals(DepictionStartLocation, other.DepictionStartLocation)) return false;

            return true;
        }
        public static bool VerifyRegionSizeForPrep(IMapCoordinateBounds box)
        {
            var bigArea = 100d; /* square miles */
            var sqMeters = bigArea * 1609d * 1609d;

            var topLeft = box.TopLeft;
            var topRight = box.TopRight;
            var bottomLeft = box.BottomLeft;

            var currentWidth = topLeft.DistanceTo(topRight, MeasurementSystem.Metric, MeasurementScale.Normal);
            var currentHeight = topLeft.DistanceTo(bottomLeft, MeasurementSystem.Metric, MeasurementScale.Normal);
            double originalArea = currentWidth * currentHeight;

           if (originalArea > sqMeters)
            {
                var prepErrorMessage =
                    string.Format("DepictionPrep regions cannot exceed {0} square miles. Please select a smaller region.",
                                  bigArea);

                DepictionAccess.NotificationService.DisplayInquiryDialog(prepErrorMessage
                    , "Region too large", MessageBoxButton.OK);
                return false;
            }
            return true;
        }
        public static bool VerifyLargeRegionArea(IMapCoordinateBounds box)
        {
            var bigArea = 15000d; /* square miles */
            var sqMeters = bigArea * 1609d * 1609d;

            var topLeft = box.TopLeft;
            var topRight = box.TopRight;
            var bottomLeft = box.BottomLeft;

            var currentWidth = topLeft.DistanceTo(topRight, MeasurementSystem.Metric, MeasurementScale.Normal);
            var currentHeight = topLeft.DistanceTo(bottomLeft, MeasurementSystem.Metric, MeasurementScale.Normal);
            double originalArea = currentWidth * currentHeight;

            bool returnVal = true;
            if (originalArea > sqMeters)
            {
                var largeAreaMessage = string.Format(
                    "Your {1} area exceeds {0} sq miles. Loading elevation or road network data sources for such a large area is not recommended, since it is likely to cause {2} to fail due to system memory limitations.\n\n Would you like to continue using this region?",
                    bigArea, "depiction", "Depiction");
                // return false;
                var ret = DepictionAccess.NotificationService.DisplayInquiryDialog(largeAreaMessage
                    , "Large region warning", MessageBoxButton.YesNo);
                if (ret.Equals(MessageBoxResult.No)) returnVal = false;
            }
            return returnVal;
        }
        #endregion
        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            DepictionStartLocation = new LatitudeLongitude(47.6505, -122.3215);//Seattle
            DepictionMapWindow = new MapCoordinateBounds();
            DepictionRegionBounds = new MapCoordinateBounds();
            if (reader.Name.Equals("DepictionLocationInformation"))
            {
                reader.ReadStartElement();
                if (reader.Name.Equals("WorldLatLongBoundingBox"))
                {
                    WorldLatLongBoundingBox =
                        SerializationService.DeserializeObject<MapCoordinateBounds>("WorldLatLongBoundingBox", reader);
                    bottomRightOfWorld = WorldLatLongBoundingBox.BottomRight as LatitudeLongitude;
                    topLeftOfWorld = WorldLatLongBoundingBox.TopLeft as LatitudeLongitude;
                }
                if (reader.Name.Equals("RegionCodes"))
                    regionCodes =
                        new List<string>(SerializationService.DeserializeObject<string[]>("RegionCodes", reader));
                if (reader.Name.Equals("DepictionStartLocation"))
                {
                    DepictionStartLocation =
                        SerializationService.DeserializeObject<LatitudeLongitude>("DepictionStartLocation", reader);
                }
                if (reader.Name.Equals("DepictionMapWindow"))
                    DepictionMapWindow =
                        SerializationService.DeserializeObject<MapCoordinateBounds>("DepictionMapWindow", reader);
                if (reader.Name.Equals("DepictionRegionBounds"))
                    DepictionRegionBounds =
                        SerializationService.DeserializeObject<MapCoordinateBounds>("DepictionRegionBounds", reader);
            }
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            writer.WriteStartElement("DepictionLocationInformation");
            SerializationService.SerializeObject("WorldLatLongBoundingBox", WorldLatLongBoundingBox, writer);
            SerializationService.SerializeObject("RegionCodes", RegionCodes, writer);
            SerializationService.SerializeObject("DepictionStartLocation", DepictionStartLocation as LatitudeLongitude, writer);
            SerializationService.SerializeObject("DepictionMapWindow", DepictionMapWindow as MapCoordinateBounds, writer);
            SerializationService.SerializeObject("DepictionRegionBounds", DepictionRegionBounds as MapCoordinateBounds, writer);
            writer.WriteEndElement();
        }


        #endregion
    }
}
