﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using Depiction.API;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Repositories
{
    public class ElementRepository : IElementRepository
    {
        public event Action TagsChanged;
        public event Action<IDepictionElement> ElementGeoLocationUpdated;
        public event NotifyCollectionChangedEventHandler ElementListChange;

        Dictionary<string, IDepictionElement> elements = new Dictionary<string, IDepictionElement>();

        Tags elementTags = new Tags();
        #region Properties
        public ReadOnlyCollection<IDepictionElement> AllElements { get { return elements.Values.ToList().AsReadOnly(); } }
        public ReadOnlyCollection<IDepictionElement> ElementsGeoLocated
        {
            get
            {
                return elements.Values.Where(t => t.IsGeolocated).ToList().AsReadOnly();
            }
        }
        public ReadOnlyCollection<IDepictionElement> ElementsUngeoLocated
        {
            get
            {
                return elements.Values.Where(t => !(t.IsGeolocated)).ToList().AsReadOnly();
            }
        }
        #endregion
        #region public methods

        public List<IDepictionElement> GetElementsFromIds(IEnumerable<string> elementIds)
        {
            if (elementIds == null) return null;
            var matching = new List<IDepictionElement>();

            foreach (var id in elementIds)
            {
                if (elements.ContainsKey(id))
                    matching.Add(elements[id]);
            }
            return matching;
        }

        public List<IDepictionElement> DeleteElementsWithIdsAndReturnElements(List<string> elementIds)
        {
            var matching = new List<IDepictionElement>();
            foreach (var id in elementIds)
            {
                if (elements.ContainsKey(id))
                {
                    matching.Add(elements[id]);
                    RemoveElement(elements[id]);
                }
            }
            return matching;
        }

        public List<string> AddOrUpdateRepositoryFromPrototypes(IEnumerable<IElementPrototype> elementPrototypes, out List<IDepictionElement> createdElements, bool markAsUpdated)
        {
            List<IDepictionElement> updatedElements;
            AddOrUpdateRepositoryFromPrototypes(elementPrototypes, markAsUpdated, out createdElements, out updatedElements);
            var updatedIds = from e in updatedElements
                             select e.ElementKey;
            return updatedIds.ToList();
           
        }

        public void AddOrUpdateRepositoryFromPrototypes(IEnumerable<IElementPrototype> elementPrototypes, bool markAsUpdated,
            out List<IDepictionElement> createdElementsList, out List<IDepictionElement> updatedElementsList)
        {
            createdElementsList = new List<IDepictionElement>();
            updatedElementsList = new List<IDepictionElement>();
            var allElements = elements.Values.ToList();

            //Find existing matches with element list
            var result = from e in allElements
                         from p in elementPrototypes
                         where !string.IsNullOrEmpty(e.ElementUserID) && !string.IsNullOrEmpty(p.ElementUserID) && e.ElementUserID.Equals(p.ElementUserID)
                         select new { p, e };
            var protosUsedForUpdate = new List<IElementPrototype>();
            foreach (var prototypeElementPair in result)
            {
                var elem = prototypeElementPair.e;
                var proto = prototypeElementPair.p;
                ElementFactory.UpdateElementWithPrototypeProperties(elem, proto, true, markAsUpdated, false,true);
                if (!proto.ZoneOfInfluence.IsEmpty)
                {
                    elem.SetZOIGeometry(proto.ZoneOfInfluence.Geometry);
                }
                updatedElementsList.Add(elem);
                //If it was used store it in order to discard later
                protosUsedForUpdate.Add(proto);
            }
            
            //Get all the unused prototypes, ok this process takes a long time, at some point this should be made more efficient.
            //Not optimised by any stretch of the imagination
//            var unusedProtos = new List<IElementPrototype>();
//            foreach(var proto in protosUsedForUpdate)
//            {
//                if(!elementPrototypes.Contains(proto))
//                {
//                    unusedProtos.Add(proto);
//                }
//            }
            var unusedProtos = elementPrototypes;
            if (protosUsedForUpdate.Count() != 0)
            {
                unusedProtos = elementPrototypes.Except(protosUsedForUpdate).ToArray();//This caueses problems, not sure why, but it does cause problems with richards lummie example
            }

            var uniqueProtoSet = new Dictionary<string, IElementPrototype>();
            var finalProtos = new List<IElementPrototype>();
            //If the unused prototype has no id, store it no fuss. If it does have an idea, make sure that element gets
            //updated if the id appears again (put only store it the first time ie not on the update pass)
            foreach (var proto in unusedProtos)
            {
                var id = proto.ElementUserID;
                if (string.IsNullOrEmpty(id))
                {
                    finalProtos.Add(proto);
                }
                else
                {
                    if (uniqueProtoSet.ContainsKey(id))
                    {
                        ElementFactory.UpdateElementWithPrototypeProperties(uniqueProtoSet[id], proto, false, false, false,true);
                    }
                    else
                    {
                        uniqueProtoSet.Add(id, proto);
                        finalProtos.Add(proto);
                    }
                }
            }
            //Create the elements from the prototypes
            foreach (var proto in finalProtos)
            {
                var elem = ElementFactory.CreateElementFromPrototype(proto);
                //the createElementFromPrototype method does not run 
                //behaviours
                if (elem != null)
                {
                    if(markAsUpdated) elem.ElementUpdated = true;
                    createdElementsList.Add(elem);
                }
            }
            if (createdElementsList.Count() > 0)
            {
                AddElements(createdElementsList);
            }
        }
        //GRR this is ugly,really ugly
        public string AddOrUpdateRepositoryFromPrototype(IElementPrototype elementPrototype, out IDepictionElement createdElement, bool markAsUpdated)
        {
            createdElement = null;
            List<IDepictionElement> createdElements;
            var updated = AddOrUpdateRepositoryFromPrototypes(new List<IElementPrototype> { elementPrototype }, out createdElements, markAsUpdated);

            if (createdElements.Count > 0)
            {
                createdElement = createdElements[0];
                return string.Empty;
            }
            if (updated.Count > 0)
            {
                return updated[0];
            }
            return null;
        }

        public bool AddElements(List<IDepictionElement> elementsToAdd)
        {
            foreach (var element in elementsToAdd)
            {
                var elementExists = elements.ContainsKey(element.ElementKey);
                if (elementExists) return false;
                elements.Add(element.ElementKey, element);
                foreach (var tag in element.Tags)
                {
                    elementTags.TagElement(element, tag);
                }
                element.PropertyChanged += element_PropertyChanged;
            }

            NotifyElementCollectionChanged(elementsToAdd, NotifyCollectionChangedAction.Add);
            return true;
        }
        public bool AddElement(IDepictionElement element)
        {

            return AddElements(new List<IDepictionElement> { element });
        }

        void element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsGeolocated"))
            {
                NotifyElementGeoLocationUpdated(sender as IDepictionElement);
            }
        }

        public bool RemoveElement(IDepictionElement element)
        {
            var removeGood = elements.ContainsKey(element.ElementKey);
            if (!removeGood) return false;
            element.PropertyChanged -= element_PropertyChanged;
            elements.Remove(element.ElementKey);
            elementTags.RemoveElementFromTagGroup(element);
            NotifyElementCollectionChanged(new List<IDepictionElement> { element }, NotifyCollectionChangedAction.Remove);
            return removeGood;
        }

        public void TagElement(IDepictionElement element, string tag)
        {
            AddTagsToElement(element, new List<string> { tag });
        }

        public void AddTagsToElement(IDepictionElement element, List<string> tags)
        {
            //hmmm what happens if we want to tag a new element before it gets added
            //how can we make sure that new elements can be tagged and that 
            //random elements can't be tagged from outside the repository
            if (!elements.ContainsKey(element.ElementKey)) return;
            foreach (var tag in tags)
            {
                element.Tags.Add(tag);
                elementTags.TagElement(element, tag);
            }
            NotifyTagsChanged();
        }

        public void UntagElement(IDepictionElement element, string tag)
        {
            RemoveTagsFromElement(element, new List<string> { tag });
        }

        public void RemoveTagsFromElement(IDepictionElement element, List<string> tags)
        {
            if (!elements.ContainsKey(element.ElementKey)) return;
            foreach (var tag in tags)
            {
                element.Tags.Remove(tag);
                elementTags.UntagElement(element, tag);
            }
            NotifyTagsChanged();
        }

        public IEnumerable<IDepictionElement> GetElementsByTag(string tag)
        {
            return elementTags.GetElementsByTag(tag);
        }

        public IDepictionElement GetFirstElementByTag(string tag)
        {
            return elementTags.GetFirstElementByTag(tag);
        }

        #endregion

        #region proteced helpers
        protected void NotifyTagsChanged()
        {
            if (TagsChanged != null)
            {
                TagsChanged.Invoke();
            }
        }
        protected void NotifyElementGeoLocationUpdated(IDepictionElement element)
        {
            if (ElementGeoLocationUpdated != null)
            {
                ElementGeoLocationUpdated.Invoke(element);
            }
        }

        protected void NotifyElementCollectionChanged<T>(IEnumerable<T> items, NotifyCollectionChangedAction action)
        {
            //This can be changed to make connecting easier. Using IDepictionElementBase makes the enuberals auto case
            if (typeof(T).Equals(typeof(IDepictionElement)) && ElementListChange != null)
            {
                ElementListChange.Invoke(this, new NotifyCollectionChangedEventArgs(action, items));
            }
            //            if(ElementListChange != null)
            //            {
            //                ElementListChange.Invoke(this,new NotifyCollectionChangedEventArgs(action,elements));
            //            }
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as ElementRepository;
            if (other == null) return false;
            if (elements.Count != other.elements.Count) return false;
            var current = new HashSet<string>(elements.Keys);
            var compari = new HashSet<string>(other.elements.Keys);
            if (!current.SetEquals(compari)) return false;
            if (!Equals(elementTags, other.elementTags)) return false;
            return true;
        }

        #endregion

        #region Serialization helpers

        static public IElementRepository DeserializeElementRepositoryFromParts(string folderLocation)
        {
            var newRepo = new ElementRepository();
            var geoList = DeserializeElementListFromFile(Path.Combine(folderLocation, "ElementsGeolocated.xml"));
            foreach (var element in geoList)
            {
                newRepo.AddElement(element);
            }
            geoList = DeserializeElementListFromFile(Path.Combine(folderLocation, "ElementsNonGeolocated.xml"));
            foreach (var element in geoList)
            {
                newRepo.AddElement(element);
            }
            return newRepo;

        }

        static public void SerializeElementRepositoryIntoParts(string folderLocation, IElementRepository elementRepository)
        {
            if (elementRepository == null) return;
            SerializeElementListToFile(Path.Combine(folderLocation, "ElementsGeolocated.xml"), elementRepository.ElementsGeoLocated);
            SerializeElementListToFile(Path.Combine(folderLocation, "ElementsNonGeolocated.xml"), elementRepository.ElementsUngeoLocated);
        }

        static protected IEnumerable<IDepictionElement> DeserializeElementListFromFile(string fileName)
        {
            var elementList = new List<IDepictionElement>();
            if (!File.Exists(fileName)) return elementList;
            var errorMessage = "No error";
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = SerializationService.GetXmlReader(stream))
                    {
                        reader.MoveToContent();//Not sure why this has to be here

                        double totalCount = 6000;
                        if (!reader.Name.Equals("ElementList")) return elementList;
                        var count = reader.GetAttribute("elementCount");
                        if (count != null) totalCount = int.Parse(count);
                        reader.ReadStartElement("ElementList");
                        double readCount = 0;
                        //Return empty list if the node type is empty (davidl). would it be better to return a null?
                        if (reader.Name.Equals("Elements"))
                            try
                            {
                                
                                bool listIsEmpty = reader.IsEmptyElement;
                                reader.ReadStartElement("Elements");
                                if (!listIsEmpty)
                                {
                                   
                                    while (reader.NodeType != XmlNodeType.EndElement)
                                    {

                                        var element = SerializationService.DeserializeObject<DepictionElementParent>(reader);
                                        elementList.Add(element);
                                        readCount++;
                                        if (SerializationService.UpdateProgressReport((readCount / totalCount) * 100d,
                                                    string.Format("Loading element {0} of {1}", readCount, totalCount)) == false)
                                        {
                                            return elementList = new List<IDepictionElement>();
                                        }
                                        reader.MoveToContent();
                                    }
                                    reader.ReadEndElement();
                                }
                            }
                            catch (Exception ex)
                            {
                                errorMessage =
                                    string.Format(
                                        "There is a load error at element list location {0} of {1}. No additional elements could be read.",
                                        readCount, totalCount);
                                DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                                //elementList = new List<IDepictionElement>();
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    string.Format("There was a problem while loading all elements,only {0} elements will load.",elementList.Count));
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
            return elementList;
        }
        static protected void SerializeElementListToFile(string fileName, IEnumerable<IDepictionElement> elements)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;//Weird
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    using (var writer = SerializationService.GetXmlWriter(stream))
                    {
                        double totalCount = elements.Count();
                        writer.WriteStartElement("ElementList", ns);
                        writer.WriteAttributeString("elementCount", totalCount.ToString());

                        writer.WriteStartElement("Elements");
                        double writeCount = 1;
                        foreach (var element in elements)
                        {
                            if (SerializationService.UpdateProgressReport((writeCount / totalCount) * 100d,
                                                string.Format("Writing element {0} of {1}", writeCount, totalCount)) == false) break;
                            SerializationService.SerializeObject(element, element.GetType(), writer);
                            writeCount++;
                        }
                        writer.WriteEndElement();
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine("From serialize :" + ex.Message); }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }


        #endregion
    }
}