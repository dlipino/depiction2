﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Repositories
{
    /// <summary>
    /// A collection of Tags.
    /// </summary>
    public class Tags : IXmlSerializable
    {
        public const string DefaultFileTag = "source [file]: ";//file name
        public const string DefaultMouseTag = "source [mouse]";
        public const string DefaultQuickStartTag = "source [quickstart]: ";//quickstart name
        public const string DefaultEmailTag = "source [email]: ";//email account
        public const string DefaultHighResTag= "source [highrestile]: ";//highrestile
        //        public delegate void TagEvent(string tag, IDepictionElementBase element);
        //        public event TagEvent ElementTagged;
        //        public event TagEvent ElementUntagged;
        private List<KeyValuePair<string, string>> restoredTags;
        private readonly Dictionary<string, HashSet<IDepictionElement>> tags = new Dictionary<string, HashSet<IDepictionElement>>();

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count
        {
            get { return tags.Count; }
        }

        public IList<KeyValuePair<string, HashSet<IDepictionElement>>> AllTags
        {
            get { AssertIsReady(); return tags.ToList(); }
        }

        /// <summary>
        /// Adds the given tag to the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="tag">The tag.</param>
        public void TagElement(IDepictionElement element, string tag)
        {
            AssertIsReady();
            HashSet<IDepictionElement> tagList;

            if (tags.TryGetValue(tag, out tagList))
            {
                if (!tagList.Contains(element))
                {
                    tagList.Add(element);
                    //                    RaiseElementTagged(tag, element);
                    //                    element.OnDeleted += element_OnDeleted;

                }
            }
            else
            {
                tagList = new HashSet<IDepictionElement> { element };
                tags.Add(tag, tagList);
                //                RaiseElementTagged(tag, element);
                //                element.OnDeleted += element_OnDeleted;
            }
        }
        public void RemoveElementFromTagGroup(IDepictionElement element)
        {
            IEnumerable<string> tagsToBeDeleted = tags.Where(t => t.Value.Contains(element)).Select(t => t.Key);
//            IEnumerable<string> tagsToBeDeleted = from t in tags where t.Value.Contains(element) select t.Key;
            foreach (string tag in tagsToBeDeleted.ToArray())
            {
                UntagElement(element, tag);
            }
        }
        //        private void element_OnDeleted(IDeleteable deletedElement)
        //        {
        //            var element = deletedElement as IElementInternal;
        //            if (element == null) return;
        //
        //            element.OnDeleted -= element_OnDeleted;
        //
        //            IEnumerable<string> tagsToBeDeleted = from t in tags where t.Value.Contains(element) select t.Key;
        //            foreach (string tag in tagsToBeDeleted.ToArray())
        //            {
        //                UntagElement(element, tag);
        //            }
        //        }

        /// <summary>
        /// Finds the elements by tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public ReadOnlyCollection<IDepictionElement> GetElementsByTag(string tag)
        {
            AssertIsReady();
            HashSet<IDepictionElement> tagList;
            return tags.TryGetValue(tag, out tagList) ? tagList.ToList().AsReadOnly() : new List<IDepictionElement>().AsReadOnly();
        }

        /// <summary>
        /// Untags the element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="tag">The tag.</param>
        public void UntagElement(IDepictionElement element, string tag)
        {
            AssertIsReady();
            HashSet<IDepictionElement> tagList;
            if (!tags.TryGetValue(tag, out tagList)) return;
            tagList.Remove(element);
            if (tagList.Count == 0) tags.Remove(tag);

            //            RaiseElementUntagged(tag, element);
        }

        /// <summary>
        /// Returns the first element found with this tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public IDepictionElement GetFirstElementByTag(string tag)
        {
            AssertIsReady();
            HashSet<IDepictionElement> tagList;
            if (tags.TryGetValue(tag, out tagList) && tagList.Count > 0)
            {
                return tagList.First();
            }
            return null;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            tags.Clear();
        }

        private void AssertIsReady()
        {
            if (restoredTags != null)
                throw new Exception("Cannot access tags after deserialization without first calling method: RestoreElements");

        }

        public void RestoreElements(IEnumerable<IDepictionElement> elements)
        {
            var elementLookup = new Dictionary<string, IDepictionElement>();
            var localRestoredTags = restoredTags;
            restoredTags = null;
            foreach (var element in elements)
            {
                elementLookup.Add(element.ElementKey, element);
            }
            tags.Clear();
            foreach (var elementKVPair in localRestoredTags)
            {
                IDepictionElement element;
                if (elementLookup.TryGetValue(elementKVPair.Key, out element))
                {
                    TagElement(element, elementKVPair.Value);
                }
            }
        }

        public IList<string> GetTagsByElement(IDepictionElement element)
        {
            AssertIsReady();
            var tagList = new List<string>();
            foreach (var kvp in tags)
            {
                if (kvp.Value.Contains(element))
                {
                    tagList.Add(kvp.Key);
                }
            }
            return tagList;
        }
        #region Event stuff that might not be needed for model
        //        private void RaiseElementTagged(string tag, IElement element)
        //        {
        //            if (DepictionAccess.ForegroundThreadDispatcher != null && !DepictionAccess.ForegroundThreadDispatcher.CheckAccess())
        //            {
        //                DepictionAccess.ForegroundThreadDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string, IElement>(RaiseElementTagged), tag, element);
        //                return;
        //            }
        //            if (ElementTagged != null)
        //                ElementTagged(tag, element);
        //        }

        //        private void RaiseElementUntagged(string tag, IElement element)
        //        {
        //            if (DepictionAccess.ForegroundThreadDispatcher != null && !DepictionAccess.ForegroundThreadDispatcher.CheckAccess())
        //            {
        //                DepictionAccess.ForegroundThreadDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string, IElement>(RaiseElementUntagged), tag, element);
        //                return;
        //            }
        //            if (ElementUntagged != null)
        //                ElementUntagged(tag, element);
        //        }
        #endregion
        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var other = obj as Tags;
            if (other == null) return false;
            if (tags.Count != other.tags.Count) return false;
            foreach(var key in tags.Keys)
            {
                if (!other.tags.ContainsKey(key)) return false;
                var mainSet = tags[key];
                var otherSet = other.tags[key];
                if(mainSet.Count != otherSet.Count) return false;
                for (int i = 0; i < mainSet.Count;i++ )
                {
                    if(!Equals(mainSet.ElementAt(i),otherSet.ElementAt(i))) return false;
                }
                //if (!tags[key].SetEquals(other.tags[key])) return false;//No clue why this stopped working
            }
            return true;
        }

        #endregion

        #region Serialization

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            restoredTags = SerializationService.DeserializeObject<List<KeyValuePair<string, string>>>("restoredTags", reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var tempTags = new List<KeyValuePair<string, string>>();
            foreach (var tag in tags.Keys)
            {
                foreach (var element in tags[tag])
                {
                    tempTags.Add(new KeyValuePair<string, string>(element.ElementKey, tag));
                }
            }

            SerializationService.SerializeObject("restoredTags", tempTags, writer);
        }
        #endregion

    }
}