﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Displayers
{
    public class DepictionRevealer : DepictionDisplayerBase, IDepictionRevealer
    {
        public IDepictionStory CurrentDepiction { get; internal set; }
        List<IRevealerProperty> revealerProperties = new List<IRevealerProperty>();
        public const double MINCONTENT_X = -25;
        public const double MINCONTENT_Y = -25;
        private IMapCoordinateBounds revealerBounds;
        #region ElementProperties

        public IMapCoordinateBounds RevealerBounds
        {
            get { return revealerBounds; }
            set
            {
                if (CurrentDepiction != null)
                {
                    CurrentDepiction.DepictionNeedsSaving = true;
                }
                revealerBounds = value;
            }
        }
        public ReadOnlyCollection<IRevealerProperty> RevealerProperties { get { return revealerProperties.AsReadOnly(); } }

        #endregion

        #region Constructor

        public DepictionRevealer() : this("Default Revealer") { }

        public DepictionRevealer(string name) : this(name, new MapCoordinateBounds()) { }
        public DepictionRevealer(string name, IMapCoordinateBounds bounds)
        {
            DisplayerName = name;
            RevealerBounds = bounds;
            AddProperty("Maximized", true);
            AddProperty("BorderVisible", true);
            AddProperty("Anchored", false);
            AddProperty("Opacity", .5);
            AddProperty("RevealerShape", RevealerShapeType.Rectangle);
            AddProperty("TopMenu", true);
            AddProperty("BottomMenu", true);
            AddProperty("SideMenu", false);
            AddProperty("PermaTextVisible", true);
            AddProperty("PermaTextX", MINCONTENT_X);
            AddProperty("PermaTextY", MINCONTENT_Y);
        }
        #endregion

        public List<IDepictionElement> GetDisplayedElements()
        {
            var current = DepictionAccess.CurrentDepiction;
            if (current == null) return null;
            var elements = current.CompleteElementRepository.GetElementsFromIds(elementIDsInDisplayer);
            var revealerArea = new DepictionGeometry(revealerBounds);//revealerBounds.TopLeft, revealerBounds.BottomRight);
            var elementsDisplayedByRevealer = new List<IDepictionElement>();
            bool maximized;
            if (!GetPropertyValue("Maximized", out maximized))
            {
                return elementsDisplayedByRevealer;
            }
            if(!maximized)
            {
                return elementsDisplayedByRevealer;
            }
            foreach (var element in elements)
            {
                if (revealerArea.Intersects(element.ZoneOfInfluence.Geometry))
                {
                    elementsDisplayedByRevealer.Add(element);
                }
            }
            return elementsDisplayedByRevealer;
        }
        public override string VisibleElementCount()
        {
            var elements = GetDisplayedElements();
            if(elements == null) return "-";
            return elements.Count().ToString();
        }

        #region Methods
        public bool GetPropertyValue<T>(string propName, out T value)
        {
            //There is probably a better way of doing this, but for now this is faster and works
            var count = RevealerProperties.Count(t => t.PropertyName.Equals(propName, StringComparison.OrdinalIgnoreCase));
            value = default(T);
            if (count != 1)
            {
                return false;
            }

            var rawProp = RevealerProperties.Single(t => t.PropertyName.Equals(propName, StringComparison.OrdinalIgnoreCase));
            var prop = rawProp as RevealerProperty;
            if (prop == null) return false;
            if (typeof(T).Equals(typeof(object)))
            {
                value = (T)prop.Value;
                return true;
            }
            if (prop.PropertyType.Equals(typeof(T)))
            {
                value = (T)prop.Value;
                return true;
            }
            return false;

        }
        public void AddProperty<T>(string propertyName, T val)
        {
            var prop = new RevealerProperty { PropertyName = propertyName, Value = val };
            revealerProperties.Add(prop);
            if (CurrentDepiction != null)
            {
                CurrentDepiction.DepictionNeedsSaving = true;
            }
        }
        public bool SetPropertyValue(string propertyName, object value)
        {
            return SetPropertyValue(propertyName, value, true);
        }
        public bool SetPropertyValue(string propertyName, object value, bool overwriteType)
        {
            var res = revealerProperties.Where(t => t.PropertyName.Equals(propertyName, StringComparison.InvariantCultureIgnoreCase));
            if (res.Count() != 1) return false;//Todo check for none or too many of the propertyName
            var prop = res.First();
            if (CurrentDepiction != null)
            {
                CurrentDepiction.DepictionNeedsSaving = true;
            }
            return prop == null || prop.SetValue(value, overwriteType);
        }

        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (!base.Equals(obj)) return false;

            // safe because of the GetType check
            var other = (DepictionRevealer)obj;

            if (!Equals(RevealerBounds, other.RevealerBounds)) return false;
            if (RevealerProperties.Count != other.RevealerProperties.Count) return false;
            foreach (var prop in RevealerProperties)
            {
                object value;
                if (!other.GetPropertyValue(prop.PropertyName, out value))
                {
                    return false;
                }
                if (!Equals(prop.PropertyType, value.GetType())) return false;
                if (!Equals(prop.Value, value)) return false;

            }
            return true;
        }
        #endregion

        #region Implementation of IXmlSerializable
        override public void DisplayerReader(XmlReader reader)
        {
            if (reader.Name.Equals("RevealerBounds"))
            {
                RevealerBounds = SerializationService.DeserializeObject<MapCoordinateBounds>("RevealerBounds", reader);
            }
            revealerProperties.Clear();
            if (reader.Name.Equals("RevealerProperties"))
            {
                reader.ReadStartElement("RevealerProperties");
                while (!reader.NodeType.Equals(XmlNodeType.EndElement))
                {

                    var prop = new RevealerProperty();
                    prop.ReadXml(reader);
                    revealerProperties.Add(prop);
                    //Assume the readxml move to the next important element
                }
                reader.ReadEndElement();
            }

        }
        public override void DisplayerWrite(XmlWriter writer)
        {
            SerializationService.SerializeObject("RevealerBounds", RevealerBounds as MapCoordinateBounds, writer);
            writer.WriteStartElement("RevealerProperties");
            foreach (var prop in revealerProperties)
            {
                prop.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        #endregion
    }
}