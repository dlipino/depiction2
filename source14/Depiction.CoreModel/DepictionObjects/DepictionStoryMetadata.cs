﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects
{
    public class DepictionStoryMetadata : IDepictionStoryMetadata,IXmlSerializable
    {
        public event Action DepictionStoryMetadataChange;

        #region ElementProperties

        public string Title { get;  set; }
        public string Description { get;  set; }
        public string Author { get;  set; }
        public DateTime SaveDateTime { get; set; }
        public string Tags { get; set; }
        public Guid DepictionStoryID { get; private set; }
        
        #endregion

        #region Constructor

        public DepictionStoryMetadata()
        {
            Title = string.Empty;
            Author = string.Empty;
            SaveDateTime = DateTime.Now;
            Description = "No description was given.";
            DepictionStoryID = Guid.NewGuid();
        }
        #endregion

        #region public helper methods
        public void SetMetadata(string title,string description,string author)
        {
            Title = title;
            Description = description;
            Author = author;
            NotifyChange();
        }

        #endregion
        #region Event helper
        public void NotifyChange()
        {
            if (DepictionStoryMetadataChange != null)
            {
                DepictionStoryMetadataChange.Invoke();
            }
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {//USed in Equals(obj,obj)
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (DepictionStoryMetadata)obj;
            if (!Title.Equals(other.Title)) return false;
            if (!Author.Equals(other.Author)) return false;
            if (!Description.Equals(other.Description)) return false;
            if (!DepictionStoryID.Equals(other.DepictionStoryID)) return false;
            if (!Equals(SaveDateTime.ToString(),other.SaveDateTime.ToString())) return false;

            return true;
        }

        #endregion

        #region XML serialization

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            Title = string.Empty;
            Author = string.Empty;
            SaveDateTime = DateTime.Now;
            Description = "No description was given.";
            DepictionStoryID = Guid.NewGuid();

            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (reader.Name.Equals("DepictionMetadata"))
            {
                reader.ReadStartElement();

                if (reader.Name.Equals("Title"))
                    Title = reader.ReadElementContentAsString("Title", ns);
                if (reader.Name.Equals("Author"))
                    Author = reader.ReadElementContentAsString("Author", ns);

                if (reader.Name.Equals("SaveDateTime"))
                {
                    SaveDateTime = reader.ReadElementContentAsDateTime("SaveDateTime", ns);
                }
                if (reader.Name.Equals("Description"))
                    Description = reader.ReadElementContentAsString("Description", ns);

                if (reader.Name.Equals("DepictionStoryID"))
                    DepictionStoryID = new Guid(reader.ReadElementContentAsString("DepictionStoryID", ns));
                if (reader.Name.Equals("Tags"))
                    Tags = reader.ReadElementContentAsString("Tags", ns);
            }
            //in case there is extra junk, just keep reading (davidl)
//            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("DepictionElement", StringComparison.InvariantCultureIgnoreCase))
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("DepictionMetadata"))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            writer.WriteStartElement("DepictionMetadata",ns);
            writer.WriteElementString("Title", ns, Title);
            writer.WriteElementString("Author", ns, Author);
            writer.WriteElementString("SaveDateTime", ns, SaveDateTime.ToString("s"));
            writer.WriteElementString("Description", ns, Description);
            writer.WriteElementString("DepictionStoryID", ns, DepictionStoryID.ToString());
            writer.WriteElementString("Tags", ns, Tags);
            writer.WriteEndElement();
        }
        #endregion
        /// <summary>
        /// Overrides of the equals.  This is kind of a bad use of this since it is not an exact equals.
        /// It is more of a sort of equal. Used for this.Equals()
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(DepictionStoryMetadata other)
        {
            return other.Title.Equals(Title) &&
                   other.Author == Author &&
                   other.Description == Description &&
                   other.DepictionStoryID.Equals(DepictionStoryID);
        }
    }
}