﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExceptionHandling;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.Serialization;
using GeoAPI.Geometries;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class ZoneOfInfluence : IZoneOfInfluence
    {
        #region Variables

        private IDepictionGeometry geometry = new DepictionGeometry();
        private IDepictionElementBase owner;

        #endregion

        #region ElementProperties

        public IDepictionElementBase Owner//This is hardly, if at all used, i can't even remember what this is for
        {
            get { return owner; }
            internal set { owner = value; }
        }

        public bool IsEmpty
        {
            get { return Geometry.IsEmpty; }
        }

        public DepictionGeometryType DepictionGeometryType
        {
            get { return GeometryTypeFromIGeometry(Geometry); }
        }

        public IDepictionGeometry Geometry
        {
            get { return geometry; }
            internal set
            {
                if (value == null) geometry = new DepictionGeometry();
                else geometry = value;
            }
        }

        #endregion

        #region Constructor

        public ZoneOfInfluence():this(new DepictionGeometry()){}

        public ZoneOfInfluence(IDepictionElementBase elementOwner)
        {
            Owner = elementOwner;
            Geometry = new DepictionGeometry();
        }
        public ZoneOfInfluence(IDepictionGeometry inGeometry)
        {
            Owner = null;
            Geometry = inGeometry;
        }

        #endregion

        #region Helper Methods
        public static DepictionGeometryType GeometryTypeFromIGeometryTypeString(string geomType)
        {
            var lowerGeomType = geomType.ToLowerInvariant();
            switch (lowerGeomType)
            {
                case "polygon":
                    return DepictionGeometryType.Polygon;
                case "multipolygon":
                    return DepictionGeometryType.MultiPolygon;
                case "linestring":
                    return DepictionGeometryType.LineString;
                case "multilinestring":
                    return DepictionGeometryType.MultiLineString;
                case "point":
                    return DepictionGeometryType.Point;
                case "multipoint":
                    return DepictionGeometryType.MultiPoint;
            }
            throw new Exception(string.Format("Unsupported geometry type {0}", geomType));
        }

        public static DepictionGeometryType GeometryTypeFromIGeometry(IDepictionSpatialData geom)
        {
            if (geom == null) return DepictionGeometryType.Point;
            if (geom is IGridSpatialData) return DepictionGeometryType.Coverage;
            return GeometryTypeFromIGeometryTypeString(geom.GeometryType);
        }

        public ILatitudeLongitude[] GetVertices()
        {
            var latLongList = new List<LatitudeLongitude>();
            var zoiCoords = Geometry.Geometry.Coordinates;
            foreach(var coord in zoiCoords)
            {
                latLongList.Add(new LatitudeLongitude(coord.Y,coord.X));
            }
            return latLongList.ToArray();
        }

        public void ShiftZoneOfInfluence(ILatitudeLongitude shiftDistance)
        {
            try
            {
                Geometry = geometry.TranslateGeometry(shiftDistance);
            }
            catch (Exception ex)
            {//TODO we need to bring the logging mechanism back
                var message = "Could not shift zone of influence geometry.";
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                Geometry = new DepictionGeometry();
            }
        }

        public Distance GetLengthDistance()
        {
            double lengthMiles = -1d;
            // GeometryType == GeometryType.MultiPolygon ||GeometryType == GeometryType.MultiLineString ||
            if (!IsEmpty &&
                //Only get length for things that are connected ?
                (DepictionGeometryType.Equals(DepictionGeometryType.LineString) || DepictionGeometryType.Equals(DepictionGeometryType.MultiLineString) ||
                 DepictionGeometryType.Equals(DepictionGeometryType.Polygon) || DepictionGeometryType.Equals(DepictionGeometryType.MultiPolygon)))
            {
                var geom = (DepictionGeometry)Geometry;
                var startLocation = new LatitudeLongitude(geom.Coordinate.Y, geom.Coordinate.X);
                lengthMiles = 0d;
                bool first = true;
                foreach (ICoordinate coord in geom.Coordinates)
                {
                    if (first)
                    {
                        first = false;
                        continue;
                    }

                    var tempEndLocation = new LatitudeLongitude(coord.Y, coord.X);
                    var dis = startLocation.DistanceTo(tempEndLocation,
                                                            MeasurementSystem.Imperial,MeasurementScale.Large);
                    lengthMiles += dis;
                    startLocation = tempEndLocation;
                }

            }
            var returnValue = new Distance(MeasurementSystem.Imperial, MeasurementScale.Large,lengthMiles);
            var val = returnValue.GetValue(MeasurementSystem.Metric, MeasurementScale.Large);
            returnValue = new Distance(MeasurementSystem.Metric, MeasurementScale.Large, val);
            //Ensures that the initial scale does not change with the apps scale
            //returnValue.UseDefaultScale = true;
            return returnValue;
        }
        #region what the heck?
        private const double MILESTOMETERS = 1609.344;
        private const double METERSPERDEGLATITUDE = 111325.0;
        private static double MetersPerDegreeLong(double baseLat)
        {
            return MILESTOMETERS * MilesPerDegreeLong(baseLat);
        }
        private static double MilesPerDegreeLong(double baseLat)
        {

            return 69.172 /*111.325 km*/ * LongToLatRatio(baseLat);
        }
        private static double LongToLatRatio(double baseLat)
        {
            return (Math.Cos(baseLat * Math.PI / 180));
        }
        #endregion
        public Area GetArea()
        {
            double area = -1d;
            DepictionGeometry geom;

            if (!IsEmpty && DepictionGeometryType.Equals(DepictionGeometryType.Polygon))// || GeometryType == GeometryType.MultiPolygon))
            {
                geom = (DepictionGeometry)Geometry;
                area = geom.Geometry.Area;
                //area in meters
                area *= MetersPerDegreeLong(geom.Geometry.Centroid.Y) * METERSPERDEGLATITUDE;
                //convert to square miles
                // area *= 0.000621371 * 0.000621371;
            }
            var meterArea = new Area(MeasurementSystem.Metric, MeasurementScale.Normal, area);
            var returnValue = new Area(MeasurementSystem.Metric, MeasurementScale.Large, meterArea.GetValue(MeasurementSystem.Metric, MeasurementScale.Large));
            var val = returnValue.GetValue(MeasurementSystem.Metric, MeasurementScale.Large);
            returnValue = new Area(MeasurementSystem.Metric, MeasurementScale.Large, val);
            returnValue.UseDefaultScale = false;//use the depiction defined scale
            return returnValue;
        }

        #endregion

        public override string ToString()
        {
            return GeometryToOgrConverter.ZOIGeometryToWkt(this);
        }
        #region Implementation of IXmlSerializable


        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            string ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            Geometry = GeometryToOgrConverter.WktToZoiGeometry(reader.ReadElementContentAsString("geometryWkt", ns));
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            string ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("ElementZOI");
            string geoString = null;
//            if(Geometry.IsValid)//Don't actually know what being valid means
//            {
            try
            {
                geoString = GeometryToOgrConverter.ZOIGeometryToWkt(this);
            }catch(Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex, false, true);
                geoString = "";
            }
//            }
            if(string.IsNullOrEmpty(geoString)) geoString = string.Empty;
            writer.WriteElementString("geometryWkt", ns,geoString);
            writer.WriteEndElement();
        }

        #endregion

        #region Implementation of IEquatable<IZoneOfInfluence>
        public override int GetHashCode()
        {
            unchecked
            {
                return ((geometry != null ? geometry.GetHashCode() : 0) * 397) ^ (owner != null ? owner.GetHashCode() : 0);
            }
        }
        public override bool Equals(object obj)
        {
            var other = obj as ZoneOfInfluence;
            if (other == null) return false;
            if (!Geometry.Equals(other.Geometry)) return false;
            return true;
        }
        #endregion


    }
}