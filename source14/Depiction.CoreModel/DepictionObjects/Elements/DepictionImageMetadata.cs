﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    /// <summary>
    /// DepictionImageMetadata class
    /// </summary>
    public class DepictionImageMetadata : IDepictionImageMetadata, IXmlSerializable
    {
        #region fields

        private string imageFilename = String.Empty;

        #endregion

        #region properties

        public DepictionIconPath DepictionImageName
        {
            get { return new DepictionIconPath(IconPathSource.File,imageFilename); }
        }

        /// <summary>
        /// Gets or sets the full path.
        /// </summary>
        /// <value>The full path.</value>
        public string ImageFilename
        {
            get { return imageFilename; }
            set { imageFilename = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is geo referenced.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is geo referenced; otherwise, <c>false</c>.
        /// </value>
        public bool IsGeoReferenced
        {
            get
            {
                return !(BottomRightLatitude.Equals(double.NaN) || BottomRightLongitude.Equals(double.NaN) || TopLeftLatitude.Equals(double.NaN) || TopLeftLongitude.Equals(double.NaN));
            }
        }

        public bool CanBeManuallyGeoAligned { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>The rotation.</value>
        public double RotationDegrees { get; set; }

        /// <summary>
        /// Gets or sets the retrieved from.
        /// </summary>
        /// <value>The retrieved from.</value>
        public string RetrievedFrom { get; set; }

        /// <summary>
        /// Gets or sets the top left latitude.
        /// </summary>
        /// <value>The top left latitude.</value>
        public double TopLeftLatitude { get; set; }

        /// <summary>
        /// Gets or sets the top left longitude.
        /// </summary>
        /// <value>The top left longitude.</value>
        public double TopLeftLongitude { get; set; }

        /// Gets or sets the bottom right latitude.
        /// </summary>
        /// <value>The bottom right latitude.</value>
        public double BottomRightLatitude { get; set; }

        /// <summary>
        /// Gets or sets the bottom right longitude.
        /// </summary>
        /// <value>The bottom right longitude.</value>
        public double BottomRightLongitude { get; set; }

        public IMapCoordinateBounds GeoBounds
        {
            get
            {
                return new MapCoordinateBounds(new LatitudeLongitude(TopLeftLatitude, TopLeftLongitude),
                                               new LatitudeLongitude(BottomRightLatitude, BottomRightLongitude));  
            }
        }
        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DepictionImageMetadata"/> class.
        /// </summary>
        public DepictionImageMetadata()
        {
            RetrievedFrom = String.Empty;
            imageFilename = String.Empty;
            BottomRightLatitude = BottomRightLongitude = TopLeftLatitude = TopLeftLongitude = double.NaN;
            RotationDegrees = 0;
            CanBeManuallyGeoAligned = true;
        }

        /// <summary>
        /// Internally sets the Catagory of the Imageinfo passed on retrieved from.  Mappoint == map anything else gets satellite
        /// </summary>
        public DepictionImageMetadata(string path, ILatitudeLongitude topLeftPos, ILatitudeLongitude bottomRightPos, string retrievedFrom)
        {
            imageFilename = path;
            TopLeftLatitude = topLeftPos.Latitude;
            TopLeftLongitude = topLeftPos.Longitude;
            BottomRightLatitude = bottomRightPos.Latitude;
            BottomRightLongitude = bottomRightPos.Longitude;
            RetrievedFrom = retrievedFrom;
            CanBeManuallyGeoAligned = true;
        }
        public DepictionImageMetadata(string path,string retrievedFrom):this(path,new LatitudeLongitude(),new LatitudeLongitude(),retrievedFrom)
        {
            
        }
        #endregion

        #region methods
        public void ResetImage()
        {
            BottomRightLatitude = BottomRightLongitude = TopLeftLatitude = TopLeftLongitude = double.NaN;
        }
        /// <summary>
        /// Copies the image object.
        /// </summary>
        /// <param name="inValue">The in value.</param>
        /// <returns></returns>
        public static DepictionImageMetadata Clone(DepictionImageMetadata inValue)
        {
            var copy = new DepictionImageMetadata();

            //            copy.IsGeoReferenced = inValue.IsGeoReferenced;
            copy.TopLeftLatitude = inValue.TopLeftLatitude;
            copy.TopLeftLongitude = inValue.TopLeftLongitude;
            copy.BottomRightLatitude = inValue.BottomRightLatitude;
            copy.BottomRightLongitude = inValue.BottomRightLongitude;
            copy.RetrievedFrom = inValue.RetrievedFrom;
            copy.RotationDegrees = inValue.RotationDegrees;
            copy.ImageFilename = inValue.ImageFilename;
            return copy;
        }

        #endregion
        #region Deep cloning
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        public IDepictionImageMetadata DeepClone()
        {
            var newMetadata = new DepictionImageMetadata(imageFilename, RetrievedFrom);
            newMetadata.TopLeftLatitude = TopLeftLatitude;
            newMetadata.TopLeftLongitude = TopLeftLongitude;
            newMetadata.BottomRightLatitude = BottomRightLatitude;
            newMetadata.BottomRightLongitude = BottomRightLongitude;
            newMetadata.CanBeManuallyGeoAligned = CanBeManuallyGeoAligned;
            return newMetadata;
        }
        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionImageMetadata;
            if (other == null) return false;

            if (!ImageFilename.Equals(other.ImageFilename)) return false;
            if (!RetrievedFrom.Equals(other.RetrievedFrom)) return false;
            //            if (!IsGeoReferenced.Equals(other.IsGeoReferenced)) return false;
            if (!RotationDegrees.Equals(other.RotationDegrees)) return false;
            if (!TopLeftLatitude.Equals(other.TopLeftLatitude)) return false;
            if (!TopLeftLongitude.Equals(other.TopLeftLongitude)) return false;
            if (!BottomRightLatitude.Equals(other.BottomRightLatitude)) return false;
            if (!BottomRightLongitude.Equals(other.BottomRightLongitude)) return false;
            return true;
        }
        #endregion

        #region Serialization region
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            //            IsGeoReferenced = reader.ReadElementContentAsBoolean("IsGeoReferenced", ns);
            TopLeftLatitude = reader.ReadElementContentAsDouble("TopLeftLatitude", ns);
            TopLeftLongitude = reader.ReadElementContentAsDouble("TopLeftLongitude", ns);
            BottomRightLatitude = reader.ReadElementContentAsDouble("BottomRightLatitude", ns);
            BottomRightLongitude = reader.ReadElementContentAsDouble("BottomRightLongitude", ns);
            RotationDegrees = reader.ReadElementContentAsDouble("Rotation", ns);
            RetrievedFrom = reader.ReadElementContentAsString("RetrievedFrom", ns);
            ImageFilename = reader.ReadElementContentAsString("ImageFilename", ns);
            if (reader.Name.Equals("CanManuallyGeoAlign"))
            {
                CanBeManuallyGeoAligned = reader.ReadElementContentAsBoolean("CanManuallyGeoAlign", ns);
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("DepictionImageMetadata");
            //            writer.WriteElementString("IsGeoReferenced", ns, IsGeoReferenced.ToString().ToLower());
            writer.WriteElementString("TopLeftLatitude", ns, TopLeftLatitude.ToString("R"));
            writer.WriteElementString("TopLeftLongitude", ns, TopLeftLongitude.ToString("R"));
            writer.WriteElementString("BottomRightLatitude", ns, BottomRightLatitude.ToString("R"));
            writer.WriteElementString("BottomRightLongitude", ns, BottomRightLongitude.ToString("R"));
            writer.WriteElementString("Rotation", ns, RotationDegrees.ToString("R"));
            writer.WriteElementString("RetrievedFrom", ns, RetrievedFrom);
            writer.WriteElementString("ImageFilename", ns, ImageFilename);
            writer.WriteElementString("CanManuallyGeoAlign", ns, CanBeManuallyGeoAligned.ToString().ToLowerInvariant());
            writer.WriteEndElement();
        }
        #endregion
    }
}