﻿using System;
using System.Collections.Generic;
using System.Xml;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.ElementLibrary;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class ElementPrototype : DepictionElementBase, IElementPrototype
    {
        #region Variables

        private string elementUserID = null;

        private bool isRawPrototype = true;
        #endregion

        #region Properties

        public string PrototypeOrigin { get; set; }
        public string DMLFileName { get; set; }

        public string ElementUserID
        {
            get
            {
                if (string.IsNullOrEmpty(elementUserID))
                {
                    object id;
                    if (GetPropertyValue("eid", out id))
                    {
                        elementUserID = id.ToString();
                    }
                }
                return elementUserID;
            }
        }
        public bool IsRawPrototype { get { return isRawPrototype; } set { isRawPrototype = value; } }
        public bool IsFileAddable { get { return true; } }
        public bool IsMouseAddable
        {
            get
            {
                bool value;
                var hasProp = GetPropertyValue("PlaceableWithMouse", out value);
                if (!hasProp) return false;
                return value;
            }
        }
        //Hack for now, so that fluid flow does not appear in the add content dialog
        public bool Deprecated { get; set; }

        #endregion

        #region constructor
        public ElementPrototype()
            : this(ElementPrototypeLibrary.RawPrototypeName, ElementPrototypeLibrary.RawPrototypeName, false)
        {
        }
        public ElementPrototype(string type, string typeDisplayName, bool isRaw)
            : this(type, typeDisplayName, isRaw, false)
        {

        }
        public ElementPrototype(string type, string typeDisplayName)
            : this(type, typeDisplayName, false)
        { }
        public ElementPrototype(string type, string typeDisplayName, bool isRaw, bool addDefaultProperties)
        {
            TypeDisplayName = typeDisplayName;
            ElementType = DepictionStringService.ConvertStringToValidInternalName(type);
            isRawPrototype = isRaw;
            //This will probalby have to change
            PrototypeOrigin = ElementPrototypeLibrary.DefaultPrototypeDescription;
            if (!addDefaultProperties)
            {
                propertyDictionary.Clear();
            }
        }
        #endregion

        #region Waypoint, different from elemenparent waypoints because they have no events
        public void RemoveAllWaypoints()
        {
            foreach (var node in waypoints)
            {
                RemoveWaypoint(node);
            }
        }

        public bool AddWaypoint(IDepictionElementWaypoint waypoint)
        {
            waypoints.Add(waypoint);
            return true;
        }

        public bool RemoveWaypoint(IDepictionElementWaypoint waypoint)
        {
            if (!waypoint.IsDeletable) return false;
            ((DepictionElementWaypoint)waypoint).NodeOwnerKey = string.Empty;
            return waypoints.Remove(waypoint);
        }

        public bool InsertWaypointBeforeLast(IDepictionElementWaypoint waypoint)
        {
            // ((DepictionElementWaypoint)node).NodeOwnerKey = ElementKey;
            var waypointCount = waypoints.Count;
            if (waypointCount == 0) { waypoints.Add(waypoint); }
            else if (waypointCount >= 1) { waypoints.Insert(waypointCount - 1, waypoint); }
            else
            {
                //This should never happen
                return false;
            }
            return true;
        }

        #endregion

        #region helper methods
        public override void ReplaceWaypointsWithoutNotification(IDepictionElementWaypoint[] newWayPoints)
        {
            waypoints = new List<IDepictionElementWaypoint>();
            foreach (var waypoint in newWayPoints)
            {
                waypoints.Add(((IDeepCloneable<IDepictionElementWaypoint>)waypoint).DeepClone());
            }
        }

        public override void SetImageMetadataWithoutNotification(IDepictionImageMetadata newImageMetadata)
        {
            imageMetadata = newImageMetadata.DeepClone();
        }

        public void UpdateTypeIfRaw(string type)
        {
            if (!IsRawPrototype) return;
            ElementType = type;
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        public void TransferNonPropertyAndNonActionFromPrototype(IElementPrototype prototypeToCopy)
        {
            imageMetadata = prototypeToCopy.ImageMetadata;
            UsePermaText = prototypeToCopy.UsePermaText;
            permaText = prototypeToCopy.PermaText.DeepClone();
            if (prototypeToCopy.Waypoints != null)
            {
                waypoints.Clear();
                foreach (var node in prototypeToCopy.Waypoints)
                {
                    AddWaypoint(node);
                }
            }
            if (prototypeToCopy.Tags != null)
            {
                Tags.Clear();
                foreach (var tag in prototypeToCopy.Tags)
                {
                    Tags.Add(tag);
                }
            }
            DMLFileName = prototypeToCopy.DMLFileName;
            PrototypeOrigin = prototypeToCopy.PrototypeOrigin;
            var geom = GeometryToOgrConverter.WktToZoiGeometry(GeometryToOgrConverter.ZOIGeometryToWkt(prototypeToCopy.ZoneOfInfluence));
            ZoneOfInfluence = new ZoneOfInfluence(geom);
        }
        //This method is goign to get copied a lot
        public IElementPrototype DeepClone()
        {
            var newPrototype = new ElementPrototype(ElementType, TypeDisplayName, IsRawPrototype, false);

            if (CreateActions != null) newPrototype.CreateActions = CreateActions.DeepClone();
            if (DeleteActions != null) newPrototype.DeleteActions = DeleteActions.DeepClone();
            if (ClickActions != null) newPrototype.ClickActions = ClickActions.DeepClone();
            if (GenerateZoiActions != null) newPrototype.GenerateZoiActions = GenerateZoiActions.DeepClone();

            foreach (var prop in propertyDictionary.Values)
            {
                newPrototype.AddPropertyOrReplaceValueAndAttributes(prop.DeepClone(), false);
            }
            newPrototype.imageMetadata = imageMetadata;
            newPrototype.UsePermaText = UsePermaText;
            newPrototype.permaText = PermaText.DeepClone();
            if (Waypoints != null)
            {
                newPrototype.RemoveAllWaypoints();
                foreach (var node in Waypoints)
                {
                    newPrototype.AddWaypoint(node);
                }
            }
            if (Tags != null)
            {
                newPrototype.Tags.Clear();
                foreach (var tag in Tags)
                {
                    newPrototype.Tags.Add(tag);
                }
            }
            newPrototype.DMLFileName = DMLFileName;
            newPrototype.PrototypeOrigin = PrototypeOrigin;
            var geom = GeometryToOgrConverter.WktToZoiGeometry(GeometryToOgrConverter.ZOIGeometryToWkt(ZoneOfInfluence));
            newPrototype.ZoneOfInfluence = new ZoneOfInfluence(geom);
            return newPrototype;
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            //This doesn seem right
            return ElementType.GetHashCode();
        }


        public override bool Equals(object obj)
        {//TODO this needs to be fixed up, because it is duplicating depictionelementbase stuff
            if (!base.Equals(obj)) return false;
            var other = obj as ElementPrototype;
            if (other == null) return false;
            if (!ElementType.Equals(other.ElementType)) return false;
            return true;
        }
        #endregion

        #region IXmlSerialize
        protected override void ReadInitialAttributes(XmlReader reader)
        {
            base.ReadInitialAttributes(reader);
            var defSource = reader.GetAttribute("definitionSource");
            if (string.IsNullOrEmpty(defSource))
            {
                defSource = "Default";
            }
            PrototypeOrigin = defSource;
            
        }
        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            isRawPrototype = false;
        }
        #endregion
    }

    public class ElementTypeEqualityComparer<T> : IEqualityComparer<T> where T : IBaseDepictionMapType
    {
        #region Implementation of IEqualityComparer<IElementPrototype>

        public bool Equals(T x, T y)
        {
            if (x.ElementType.Equals(y.ElementType, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
        }

        public int GetHashCode(T obj)
        {
            return obj.ElementType.GetHashCode();
        }

        #endregion
    }

}