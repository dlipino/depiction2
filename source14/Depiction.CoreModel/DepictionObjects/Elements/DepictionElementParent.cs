﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Xml;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.ValueTypes;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class DepictionElementParent : DepictionElementBase, IDepictionElement
    {
        #region Hack element command region, this will change, hopefully. What gets placed here is a one time thing and is gone once session ends
        internal List<ICommand> elementCommandList = new List<ICommand>();
        public ReadOnlyCollection<ICommand> ElementCommands { get { return new ReadOnlyCollection<ICommand>(elementCommandList); } }
        #endregion

        private string elementUserID;
        private string elementGuid = Guid.NewGuid().ToString();
        private bool hideImage;
        //These are for pulsating icons, this will hopefully be temp.
        private bool elementUpdated;

        public string ElementID { set { elementGuid = value; } }//This is just for 1.2.2 conversions
        public bool ElementUpdated
        {
            get { return elementUpdated; }
            set
            {
                if (elementUpdated == value) return;
                elementUpdated = value; NotifyModelPropertyChanged("ElementUpdated");
            }
        }
        public bool HideImage
        {
            get { return hideImage; }
            set
            {
                if (hideImage == value) return;
                hideImage = value; NotifyModelPropertyChanged("HideImage");
            }
        }

        //Hack to avoid circular interactions. This is a dummy property
        public bool WaypointUpdate { get; set; }
        #region Properties
        public string ElementKey
        {
            get { return elementGuid; }
        }

        //Man its tempting to create a general post set action for the position property, this shoudl also 
        //probable get moved into a method.
        public ILatitudeLongitude Position
        {
            get { return GetPropertyByInternalName("Position").Value as LatitudeLongitude; }
            set
            {
                bool validOldPosition = Position.IsValid;
                if (value == null)
                {
                    value = new LatitudeLongitude();
                }
                bool validNewPosition = value.IsValid;
                //must be careful when setting this the first time

                if (validOldPosition && validNewPosition)
                {
                    var shift = (value as LatitudeLongitude) - Position;
                    //Order is important because of events associated with the position change.
                    zoneOfInfluence.ShiftZoneOfInfluence(shift); //Must go before setting property value
                    NotifyModelPropertyChanged("ZoneOfInfluence");
                }
                if (!validNewPosition && validOldPosition)
                {
                    if (PrivateSetZOIGeometry(null))
                    {
                        NotifyModelPropertyChanged("ZoneOfInfluence");
                    }
                }
                //only if the zoi is empty and a point set the value to current position
                if (zoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point) && validNewPosition && ZoneOfInfluence.IsEmpty)
                {
                    if (PrivateSetZOIGeometry(new DepictionGeometry(value)))
                    {
                        NotifyModelPropertyChanged("ZoneOfInfluence");
                    }
                }
                //This will happen unless something really bad has happened(hopefully)
                var prop = GetPropertyByInternalName("Position");
                prop.SetPropertyValue(value, this);
                if (prop.IsHoverText) UpdateToolTip();
                NotifyModelPropertyChanged("Position");
                //SetPropertyValue("Position", value);
                if (validNewPosition != validOldPosition)
                {
                    NotifyModelPropertyChanged("IsGeolocated");
                }
            }
        }
        public bool IsGeolocated
        {
            get { return Position.IsValid; }
        }
        public string ElementUserID
        {
            get
            {
                if (string.IsNullOrEmpty(elementUserID))
                {
                    object id;
                    if (GetPropertyValue("eid", out id))
                    {
                        elementUserID = id.ToString();
                    }
                }
                return elementUserID;
            }
        }

        #endregion

        #region Constructor
        //base constructor is good enough
        #endregion

        #region Public helper methods

        public List<string> GetAllWaypointKeys()
        {
            var keys = new List<string>();
            keys.Add(ElementKey);
            foreach (var child in Waypoints)
            {
                keys.Add(child.Key);
            }
            return keys;
        }

        public void UpdateToolTip()
        {
            NotifyModelPropertyChanged("HoverText");//THe changing names is kind of annoying
        }
        public void SetZOIGeometryAndUpdatePosition(IDepictionGeometry geometry)
        {
            if (geometry.IsEmpty)
            {
                if (Position.IsValid)
                {
                    PrivateSetZOIGeometry(new DepictionGeometry(Position));
                }
                return;
            }
            if (!PrivateSetZOIGeometry(geometry)) return;
            //This is not used because the ZOI property change is called when
            //the position is set (it would be redundent)
            //NotifyModelPropertyChanged("ZoneOfInfluence");

            var iconPosition = ZoneOfInfluence.Geometry.Centroid;
            if (ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.LineString) ||
               ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.MultiLineString) ||
                ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point))
            {
                iconPosition = ZoneOfInfluence.GetVertices().First();
            }

            SetPropertyValue("Position", iconPosition);
            SetElementAreaAndLengthProperty();
            NotifyModelPropertyChanged("ZoneOfInfluence");
        }
        public void SetZOIGeometry(IDepictionGeometry geometry)
        {
            if (!PrivateSetZOIGeometry(geometry)) return;
            //This is not used because the ZOI property change is called when
            //the position is set (it would be redundent)
            SetElementAreaAndLengthProperty();
            NotifyModelPropertyChanged("ZoneOfInfluence");
        }
        public void SetElementAreaAndLengthProperty()
        {//ZOIShapeType

            if (ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point)) return;
            var routeLength = ((ZoneOfInfluence)ZoneOfInfluence).GetLengthDistance();
            //The sets it to miles since we know the get distance calc sets it to large scale
            if (ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") || ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                routeLength.UseDefaultScale = true;
            }
            var distanceName = "Perimeter";
            if (ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.LineString) ||
                ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.MultiLineString))
            {
                ZOIShapeType shape;
                if (GetPropertyValue("ZOIShapeType", out shape))
                {
                    if (shape.Equals(ZOIShapeType.Line) || shape.Equals(ZOIShapeType.UserLine)) distanceName = "Length";
                }
            }

            //LENGTH
            var prop = GetPropertyByInternalName(distanceName);
            if (prop == null)
            {
                prop = new DepictionElementProperty(distanceName, routeLength);
                prop.VisibleToUser = true;
                prop.Editable = false;
                AddPropertyOrReplaceValueAndAttributes(prop);
            }
            else
            {

                var changeResult = prop.SetPropertyValue(routeLength);
                if (!changeResult.ValidationOutput.Equals(ValidationResultValues.InValid) && prop.IsHoverText)
                {
                    NotifyModelPropertyChanged("HoverText");
                }
            }

            if (ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.LineString) ||
                ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.MultiLineString))
            {
                return;
            }
            //AREA
            var area = ((ZoneOfInfluence)ZoneOfInfluence).GetArea();
            if (area.GetCurrentSystemDefaultScaleValue() >= 0d)
            {
                prop = GetPropertyByInternalName("Area");
                if (prop == null)
                {
                    prop = new DepictionElementProperty("Area", area);
                    prop.VisibleToUser = true;
                    prop.Editable = false;
                    AddPropertyOrReplaceValueAndAttributes(prop);
                }
                else
                {
                    var changeResult = prop.SetPropertyValue(area);
                    if (!changeResult.ValidationOutput.Equals(ValidationResultValues.InValid) && prop.IsHoverText)
                    {
                        NotifyModelPropertyChanged("HoverText");
                    }
                }
            }
        }


        public override void SetImageMetadataWithoutNotification(IDepictionImageMetadata newImageMetadata)
        {
            SetImageMetadata(newImageMetadata, false);
        }
        public void SetImageMetadata(IDepictionImageMetadata newImageMetadata, bool notifypropertyChange)
        {
            //The actual image is not guarenteed to be in the image resource dictionary at this point. . . 
            //not really sure what to do about that
            imageMetadata = newImageMetadata;

            //TODO combine with image registration
            if (imageMetadata.IsGeoReferenced)
            {
                SetInitialPositionAndZOI(imageMetadata.GeoBounds.TopLeft, null);
            }
            else
            {
                SetInitialPositionAndZOI(new LatitudeLongitude(), null);
            }
            if (notifypropertyChange) NotifyModelPropertyChanged("ImageMetadata");
        }
        public void SetImageMetadata(IDepictionImageMetadata newImageMetadata)
        {
            SetImageMetadata(newImageMetadata, true);
        }
        public void UpdateImageMetadata(IMapCoordinateBounds bounds, double rotation)
        {
            if (ImageMetadata == null) return;
            ImageMetadata.TopLeftLatitude = bounds.TopLeft.Latitude;
            ImageMetadata.TopLeftLongitude = bounds.TopLeft.Longitude;
            ImageMetadata.BottomRightLatitude = bounds.BottomRight.Latitude;
            ImageMetadata.BottomRightLongitude = bounds.BottomRight.Longitude;
            ImageMetadata.RotationDegrees = rotation;
            NotifyModelPropertyChanged("ImageMetadata");

        }
        public int NumberIncomingEdgesFired { get; set; }

        public void Restore(bool notifyPropertyChange)
        {
            lock (OrderedCustomProperties)
            {
                foreach (var prop in OrderedCustomProperties)
                {
                    if (prop.Value is IRestorable)
                    {
                        ((IRestorable)prop.Value).Restore(notifyPropertyChange);
                    }
                    else
                    {//This might cause problems because of referencing, should be phased out, or something
                        if (prop.RestoreValue != null)
                        {
                            var val = prop.RestoreValue;
                            //TODO man this seems very fishy, timing wise
                            SetPropertyValue(prop.InternalName, val, notifyPropertyChange);
                        }
                    }
                }
            }
        }

        #endregion

        #region Wayponts
        public IDepictionElementWaypoint GetWaypointWithName(string name)
        {
            try
            {
                return waypoints.First(t => t.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            }
            catch { return null; }
        }

        public override void ReplaceWaypointsWithoutNotification(IDepictionElementWaypoint[] newWayPoints)//This is not really safe
        {
            foreach (var node in waypoints)
            {
                RemoveWaypointWithNotification(node, false);
            }
            foreach (var waypoint in newWayPoints)
            {
                AddWaypointWithNotification(waypoint.DeepClone(), false);
            }
        }

        public bool AddWaypointWithNotification(IDepictionElementWaypoint waypoint, bool notify)
        {
            //((DepictionElementWaypoint)child).NodeOwnerKey = ElementKey;
            waypoint.PropertyChanged += waypoint_PropertyChanged;
            waypoints.Add(waypoint);
            if (notify) NotifyOfWaypointChanges();
            return true;
        }
        public bool AddWaypoint(IDepictionElementWaypoint waypoint)
        {
            return AddWaypointWithNotification(waypoint, true);
        }
        public bool RemoveWaypointWithNotification(IDepictionElementWaypoint waypoint, bool notify)
        {
            if (!waypoint.IsDeletable) return false;

            ((DepictionElementWaypoint)waypoint).NodeOwnerKey = string.Empty;
            waypoint.PropertyChanged -= waypoint_PropertyChanged;

            var removeSuccess = waypoints.Remove(waypoint);
            if (removeSuccess && notify)
            {
                NotifyOfWaypointChanges();
            }
            return removeSuccess;
        }
        public bool RemoveWaypoint(IDepictionElementWaypoint waypoint)
        {
            return RemoveWaypointWithNotification(waypoint, true);
        }
        public bool InsertWaypoint(int index, IDepictionElementWaypoint waypoint)
        {
            waypoints.Insert(index, waypoint);
            waypoint.PropertyChanged += waypoint_PropertyChanged;
            NotifyOfWaypointChanges();
            return true;
        }
        public bool InsertWaypointBeforeLast(IDepictionElementWaypoint waypoint)
        {
            // ((DepictionElementWaypoint)node).NodeOwnerKey = ElementKey;
            var nodeCount = waypoints.Count;
            if (nodeCount == 0) { waypoints.Add(waypoint); }
            else if (nodeCount >= 1) { waypoints.Insert(nodeCount - 1, waypoint); }
            else
            {
                //This should never happen
                return false;
            }
            waypoint.PropertyChanged += waypoint_PropertyChanged;
            NotifyOfWaypointChanges();
            return true;
        }

        protected void NotifyOfWaypointChanges()
        {
            //Epic hack hack hack needs to be run before interactesion get triggered and only for routes
            //i can't remember why this needs to run before notifying of waypoints. oh wait, this is so that the
            //routes redraw if there is no roadnetwork in the depiction since the interactions won't trigger if there is no
            //road network
            bool roadNetworkPresent = false;
            if (ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") || ElementType.Equals("Depiction.Plugin.RouteUserDrawn") || ElementType.Equals("Depiction.Plugin.DriveTimeRoute") || ElementType.Equals("Depiction.Plugin.RouteWaypoint"))
            {
                if (DepictionAccess.CurrentDepiction == null) return;
                //Don't overlap the interaction that will get triggered (interaction only gets triggered
                //kind of a hack to have interactions for route free form, but this area of depiction
                //is still a bit confusing to me (davidl)
                var count = DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements.Where(
                        t => t.ElementType.Equals("Depiction.Plugin.RoadNetwork")).Count();

                if (count == 0)
                {
                    var addins = AddinRepository.Instance.Behaviors.Where(t => t.Key.BehaviorName.Equals("FindRoute"));
                    if (addins.Count() != 0)
                    {
                        var behave = addins.First();
                        behave.Value.DoBehavior(this, new object[] { null });
                    }
                }
                else
                {
                    roadNetworkPresent = true;
                }
            }
            //End epic hack
            //The intent of this is to avoid doubling up the interacation call, it might nto be possible though
            if (roadNetworkPresent)
            {
                //waypointupdate is used in the interaction rule
                NotifyModelPropertyChanged("WaypointUpdate");
            }
            //This updates the visual
            NotifyModelPropertyChanged("Waypoints");

        }
        void waypoint_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Location"))
            {
                NotifyOfWaypointChanges();
            }
            else if (e.PropertyName.Equals("LocationNoBehavior"))
            {
                NotifyModelPropertyChanged("Waypoints");
            }
        }
        #endregion

        #region Private helpers

        private bool PrivateSetZOIGeometry(IDepictionGeometry geometry)
        {
            if (ZoneOfInfluence == null)
            {
                ZoneOfInfluence = new ZoneOfInfluence(this);
            }
            else
            {
                try
                {
                    if (ZoneOfInfluence.Geometry.Equals(geometry))
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            ((ZoneOfInfluence)ZoneOfInfluence).Geometry = geometry;
            AdjustElementZOIPropertiesForZOIType(ZoneOfInfluence);
            SetElementAreaAndLengthProperty();
            return true;
        }

        #endregion

        #region overrides from DepictionElementBase
        protected override void ReadInitialAttributes(XmlReader reader)
        {
            base.ReadInitialAttributes(reader);
            elementGuid = reader.GetAttribute("elementKey");
            if (elementGuid == null) elementGuid = Guid.NewGuid().ToString();
        }
        //Small hack for road networks
        public override void ReadXml(XmlReader reader)
        {//TODO Add a better test
            base.ReadXml(reader);
            var allNodes = Waypoints;
            waypoints.Clear();
            foreach (var node in allNodes)
            {//Drat a normal test won't catch the bug
                //AddWaypoint(node);
                AddWaypointWithNotification(node, false);
            }
            if (Position != null && Position.IsValid && zoneOfInfluence.IsEmpty)
            {
                zoneOfInfluence = new ZoneOfInfluence(new DepictionGeometry(Position));
                AdjustElementZOIPropertiesForZOIType(zoneOfInfluence);
            }
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {//USed in Equals(obj,obj)
            var other = obj as DepictionElementParent;
            if (other == null) return false;

            if (!Equals(ElementKey, other.ElementKey)) return false;
            return base.Equals(obj);
        }

        #endregion

    }
}