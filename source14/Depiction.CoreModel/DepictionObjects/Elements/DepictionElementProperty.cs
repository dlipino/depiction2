﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.API.OldValidationRules;
using Depiction.API.Service;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    [Flags]
    public enum PropertyFlags
    {
        VisibleToUser = 1,
        Editable = 2,
        Deletable = 4,
        IsHoverText = 8,

    }

    public class DepictionElementProperty : DepictionModelBase, IElementProperty
    {
        public const bool visibleToUserDefault = true;
        public const bool editableDefault = true;
        public const bool deletableDefault = true;
        public const bool isHoverTextDefault = false;

        #region Variables for properties

        private int rank;
        private string internalName;
        private string displayName;
        private object value;
        private object restoreValue;
        private Type propertyType;

        private PropertyFlags PropertyFlags;

        //private bool visibleToUser = visibleToUserDefault;
        //private bool editable = editableDefault;
        //private bool deletable = deletableDefault;
        //private bool isHoverText = isHoverTextDefault;

        private PropertySource propertySource;//is this used?

        private SerializableDictionary<string, string[]> postSetActions;
        private IValidationRule[] validationRules = new IValidationRule[0];

        #endregion

        public int Rank { get { return rank; } set { rank = value; } }


        #region ElementProperties

        public string InternalName { get { return internalName; } }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(displayName))
                {
                    return InternalName;
                }
                return displayName;
            }
            set { displayName = value; NotifyModelPropertyChanged("DisplayName"); }
        }

        public object Value
        {
            get { return value; }
            private set { this.value = value; NotifyModelPropertyChanged("Value"); }
        }

        public object RestoreValue { set { restoreValue = value; } get { return restoreValue; } }


        public Type ValueType
        {
            get { return value.GetType(); }
        }

        public bool CanCloneValue
        {
            get { return true; }
        }

        public bool VisibleToUser
        {
            get
            {
                return PropertyFlags.HasFlag(PropertyFlags.VisibleToUser);
            }
            set
            {
                if (value)
                    PropertyFlags = PropertyFlags | PropertyFlags.VisibleToUser;
                else
                    PropertyFlags = PropertyFlags ^ PropertyFlags.VisibleToUser;
            }
        }

        public bool Editable
        {
            get
            {
                return PropertyFlags.HasFlag(PropertyFlags.Editable);
            }
            set
            {
                if (value)
                    PropertyFlags = PropertyFlags | PropertyFlags.Editable;
                else
                    PropertyFlags = PropertyFlags ^ PropertyFlags.Editable;
            }
        }

        public bool Deletable
        {
            get
            {
                return PropertyFlags.HasFlag(PropertyFlags.Deletable);
            }
            set
            {
                if (value)
                    PropertyFlags = PropertyFlags | PropertyFlags.Deletable;
                else
                    PropertyFlags = PropertyFlags ^ PropertyFlags.Deletable;
            }
        }

        public bool IsHoverText
        {
            get
            {
                return PropertyFlags.HasFlag(PropertyFlags.Deletable);
            }
            set
            {
                if (value)
                    PropertyFlags = PropertyFlags | PropertyFlags.Deletable;
                else
                    PropertyFlags = PropertyFlags ^ PropertyFlags.Deletable;
                NotifyModelPropertyChanged("IsHoverText");
            }
        }

        //public bool Deletable { get { return deletable; } set { deletable = value; } }

        //public bool IsHoverText
        //{
        //    get { return isHoverText; }
        //    set
        //    {
        //        isHoverText = value; NotifyModelPropertyChanged("IsHoverText");
        //    }
        //}

        public DateTime LastModified { get; set; }

        public PropertySource PropertySource { get; set; }

        public IValidationRule[] ValidationRules
        {
            get { return validationRules; }
        }
        public SerializableDictionary<string, string[]> PostSetActions
        {
            get { return postSetActions; }
        }
        #endregion

        #region Constructor

        public DepictionElementProperty() : this("DefaultName", "Name", "string", null, null) { }
        public DepictionElementProperty(string propInterName, object val) : this(propInterName, propInterName, val, null, null) { }

        public DepictionElementProperty(string propInterName, string displayName, object val) : this(propInterName, displayName, val, null, null) { }

        public DepictionElementProperty(string propertyInternalName, string displayName, object inValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs)
            : this(propertyInternalName, displayName, inValue, null, valRules, postSetActs, null)
        {
        }
        public DepictionElementProperty(string propertyInternalName, string displayName, object inValue, object initialValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs)
            : this(propertyInternalName, displayName, inValue, initialValue, valRules, postSetActs, null)
        {
        }
        public DepictionElementProperty(string propertyInternalName, string displayName, object inValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs, IElementProperty inValueChanger)
            : this(propertyInternalName, displayName, inValue, null, valRules, postSetActs, inValueChanger)
        {
        }
        public DepictionElementProperty(string propertyInternalName, string displayName, object inValue, object initValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs, IElementProperty inValueChanger)
        {
            internalName = DepictionStringService.ConvertStringToValidInternalName(propertyInternalName);
            this.displayName = displayName;

            propertyType = inValue.GetType();
            value = inValue;
            restoreValue = initValue;
            VisibleToUser = true;
            Editable = true;
            Deletable = true;
            IsHoverText = false;
            propertySource = PropertySource.DML;
            validationRules = valRules;
            postSetActions = postSetActs;
        }

        #endregion

        #region Helper methods

        //Hack
        public object GetValueClone()
        {
            if (value is IDeepCloneable)
            {
                var cloneable = value as IDeepCloneable;
                return cloneable.DeepClone();
            }
            return value;
        }

        public DepictionValidationResult SetValueAndUpdateType(object newValue, IDepictionElement elementForPostSetActions)
        {
            return SetPropertyValue(newValue, elementForPostSetActions, true);
        }

        public void RefreshValue()
        {
            NotifyModelPropertyChanged("Value");
        }
        #endregion

        #region Implementation of IElementProperty
        //Why is this even using a validationresult, i think it is generally reserved for viewmodel/view?

        public DepictionValidationResult SetPropertyValue(object newValue)
        {
            return SetPropertyValue(newValue, null);
        }

        public DepictionValidationResult SetPropertyValue(object newValue, IDepictionElement elementForPostSetActions)
        {
            return SetPropertyValue(newValue, elementForPostSetActions, false);
        }

        public DepictionValidationResult SetPropertyValue(object newValue, IDepictionElement elementForPostSetActions, bool updateType)
        {

            if (!ValueType.Equals(newValue.GetType()) && !updateType)
            {
                return new DepictionValidationResult(ValidationResultValues.InValid, "Current property type does not match new value type.");
            }

            //Hack to stop never ending loop in interactions, sort of hack
            if (Value.Equals(newValue))
            {
                return new DepictionValidationResult(ValidationResultValues.NoChange, "Values are equal");
            }
            Value = newValue;

            //NotifyModelPropertyChanged("Value");//Im not 100% sure who listens to this, but i think it is the property editor view/viewmodel
            //                        lastModified = DateTime.Now;
            //                        NotifyModelPropertyChanged("LastModified");

            //TODO do the post set actions here. experimental, doesnt really work
            if (PostSetActions != null && AddinRepository.Instance != null && elementForPostSetActions != null)
            {
                var behaviorDict = AddinRepository.Instance.GetBehaviors();
                if (behaviorDict != null) //the null only happens during tests
                {
                    foreach (var action in PostSetActions)
                    {
                        var key = action.Key;
                        var behaviorList = behaviorDict.Where(t => t.Key.BehaviorName.Equals(key)).ToList();
                        if (behaviorList.Count == 1)
                        {
                            var behavior = behaviorList[0].Value;
                            var behaviorParams = InteractionRule.BuildParametersForPostSetActions(action.Value);
                            behavior.DoBehavior(elementForPostSetActions, behaviorParams);
                        }
                    }
                }
            }
            return new DepictionValidationResult(ValidationResultValues.Valid, string.Empty);
        }

        #endregion


        #region Cloning
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        public IElementProperty DeepClone()
        {//Using the serialize trick is more complete, but im slightly worried about read/write speeds
            var rulesCopy = validationRules;
            if (validationRules != null)
            {
                var ruleList = new List<IValidationRule>();
                foreach (var rule in validationRules)
                {
                    ruleList.Add(rule);
                }
                rulesCopy = ruleList.ToArray();
            }

            var postSetActionsCopy = postSetActions;
            if (postSetActions != null)
            {
                postSetActionsCopy = postSetActions.DeepClone();
            }

            var newProp = new DepictionElementProperty(InternalName, DisplayName, Value, rulesCopy, postSetActionsCopy);

            newProp.displayName = displayName;

            newProp.VisibleToUser = VisibleToUser;
            newProp.Editable = Editable;
            newProp.Deletable = Deletable;
            newProp.IsHoverText = IsHoverText;
            newProp.propertySource = PropertySource;

            return newProp;
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        override public bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionElementProperty;
            if (other == null) return false;
            //Ok so im getting lazy and there are a few properties that are not currently getting checked
            if (!Equals(Value, other.Value)) return false;
            if (InternalName.Equals(other.InternalName) && ValueType.Equals(other.ValueType) &&
                 IsHoverText.Equals(other.IsHoverText) && Deletable.Equals(other.Deletable) &&
                VisibleToUser.Equals(other.VisibleToUser) && Editable.Equals(other.Editable) &&
                PropertySource.Equals(other.PropertySource))
            {
                return true;
            }
            return false;
        }

        #endregion
        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            //This didn't work at all, it basically caused an infinite loop because the reader
            //would never get out of the property tag. I think it means one more element will be completely
            //read, then the cancel should work
            //            if (SerializationService.IsSaveLoadCancelled() == true)
            //            {
            //                reader.Skip();
            //                return;
            //            }
            var propertyTypeString = string.Empty;
            if (reader.Name.Equals("property"))
            {
                propertyTypeString = reader.GetAttribute("typeName");

                propertyType = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(propertyTypeString);
                if (propertyType == null)
                {
                    //try older types
                    propertyType = DepictionCoreTypes.FindType(propertyTypeString);
                    if (propertyType == null)
                    {//Man this is bad if it gets here

                        propertyType = DepictionTypeConverter.Get12ObjectTypeFromStringValue(propertyTypeString);
                    }
                }

                var valueString = reader.GetAttribute("value");
                if (valueString != null)
                {
                    if (propertyType != null)
                    {
                        try
                        {
                            value = DepictionTypeConverter.ChangeType(valueString, propertyType);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("type change problem");
                        }
                    }
                    else
                    {
                        reader.ReadStartElement();//I guess this moves to the next node, i think this will cause bugs though :(
                        //reader.ReadEndElement();
                        throw new Exception("Property cannot be null " + propertyTypeString);
                    }
                }
                valueString = reader.GetAttribute("restoreValue");
                if (valueString != null)
                {
                    if (propertyType != null)
                    {
                        try
                        {
                            restoreValue = DepictionTypeConverter.ChangeType(valueString, propertyType);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("type change problem");
                        }
                    }
                    else
                    {
                        reader.ReadStartElement();//I guess this moves to the next node, i think this will cause bugs though :(
                        //reader.ReadEndElement();
                        throw new Exception("Property cannot be null " + propertyTypeString);
                    }
                }


                internalName = reader.GetAttribute("name");
                displayName = reader.GetAttribute("displayName");

                VisibleToUser = reader.GetAttribute("visible").ToBool(visibleToUserDefault);
                Editable = reader.GetAttribute("editable").ToBool(editableDefault);
                Deletable = reader.GetAttribute("deletable").ToBool(deletableDefault);
                IsHoverText = reader.GetAttribute("isHoverText").ToBool(isHoverTextDefault);
            }
            if (!reader.IsEmptyElement)
            {
                reader.Read();
                if (reader.Name.Equals("value"))//INcomplete
                {
                    reader.ReadStartElement("value");
                    value = SerializationService.DeserializeObject(propertyType, reader);
                    reader.ReadEndElement();
                }
                //1.3.3
                if (reader.Name.Equals("initialValue"))//INcomplete
                {
                    reader.ReadStartElement("initialValue");
                    restoreValue = SerializationService.DeserializeObject(propertyType, reader);
                    reader.ReadEndElement();
                }
                //1.4
                if (reader.Name.Equals("restoreValue"))//INcomplete
                {
                    reader.ReadStartElement("restoreValue");
                    restoreValue = SerializationService.DeserializeObject(propertyType, reader);
                    reader.ReadEndElement();
                }

                postSetActions = new SerializableDictionary<string, string[]>();
                if (reader.Name.Equals("postSetActions"))
                {
                    postSetActions = UnifiedDepictionElementReader.ReadPostSetActions(reader);
                }
                validationRules = new IValidationRule[0];
                if (reader.Name.Equals("validationRules"))
                {
                    validationRules = UnifiedDepictionElementReader.ReadValidationRules(reader, propertyTypeString).ToArray();
                }
            }
            reader.Read();//Why not read end?
        }

        public void WriteXml(XmlWriter writer)
        {
            UnifiedDepictionElementWriter.WriteProperty(writer, this, false);
            //if (SerializationService.IsSaveLoadCancelled() == true) return;
            //SerializationService.SerializeObject(propertyData, propertyData.PropertyType, writer);
        }

        #endregion

    }
}