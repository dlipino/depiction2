﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Schema;
using Depiction.APINew.CoreEnumAndStructs;
using Depiction.APINew.ExtensionMethods;
using Depiction.APINew.Interfaces;
using Depiction.APINew.Interfaces.ElementInterfaces;
using Depiction.APINew.MVVM;
using Depiction.APINew.OldValidationRules;
using Depiction.APINew.Service;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class DepictionElementPropertyData : DepictionModelBase,IElementPropertyData
    {
        public const bool visibleToUserDefault = true;
        public const bool editableDefault = true;
        public const bool deletableDefault = true;
        public const bool isHoverTextDefault = false;
        public const bool isInUseDefault = true;

        #region Variables for properties

        private int rank = 0;
        private string internalName;
        private string displayName;
        private object value;
        private object defaultValue;
        private object initialValue;
        private Type propertyType;
        private MeasurementScale measurementScale;

        private bool visibleToUser = visibleToUserDefault;
        private bool editable = editableDefault;
        private bool deletable = deletableDefault;
        private bool isHoverText = isHoverTextDefault;
        private bool isInUse = isInUseDefault;

        private DateTime lastModified;
        private PropertySource propertySource;

        private SerializableDictionary<string, string[]> postSetActions = new SerializableDictionary<string, string[]>();
        private IValidationRule[] validationRules = new IValidationRule[0];

        #endregion
        #region for connecting to value changer

        private IElementProperty valueChanger;
        internal IElementProperty ValueChanger
        {
            get { return valueChanger; }
            set
            {
                if (value == null)//A hack disconnect 
                {
                    if (valueChanger != null)
                    {
                        valueChanger.ValueChanged -= ValueChanger_ValueChanged;
                    }
                    valueChanger = value;
                    return;
                }
                if (valueChanger == null)
                {
                    valueChanger = value;

                    valueChanger.ValueChanged += ValueChanger_ValueChanged;
                }

            }
        }
        #endregion

        #region ElementProperties
        public int Rank { get { return rank; } set { rank = value; } }
        public string InternalName
        {
            get { return internalName; }
            private set { internalName = value; }
        }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(displayName))
                {
                    return InternalName;
                }
                return displayName;
            }
            set { displayName = value; NotifyModelPropertyChanged("DisplayName"); }//TODO fix hackThis should be internal
        }

        public object Value
        {
            get
            {//TODO Should this get deep cloned?
                if (ValueChanger != null)
                {
                    value = ValueChanger.Value;
                }
                return value;
            }
            private set { this.value = value; }
        }

        //Man this was a bad idea to have default value and initial value
        public object DefaultValue
        {
            get { return defaultValue; }
            set { defaultValue = value; }
        }
        public object InitialValue
        {
            get { return initialValue; }
            private set { initialValue = value; }
        }

        public Type PropertyType
        {
            get
            {
                if (ValueChanger != null)
                {
                    propertyType = ValueChanger.ValueType;
                }
                return propertyType;
            }
            private set { propertyType = value; }
        }

        public MeasurementScale MeasurementScale
        {
            get { return measurementScale; }
            set { measurementScale = value; NotifyModelPropertyChanged("MeasurementScale"); }
        }

        public bool VisibleToUser
        {
            get { return visibleToUser; }
            set { visibleToUser = value; }
        }

        public bool Editable
        {
            get { return editable; }
            set { editable = value; NotifyModelPropertyChanged("Editable"); }
        }

        public bool Deletable
        {
            get { return deletable; }
            set { deletable = value; NotifyModelPropertyChanged("Deletable"); }
        }

        public bool IsHoverText
        {
            get { return isHoverText; }
            set { isHoverText = value; NotifyModelPropertyChanged("IsHoverText"); }
        }

        public bool IsInUse
        {
            get { return isInUse; }
            internal set { isInUse = value; NotifyModelPropertyChanged("IsInUse"); }
        }

        public DateTime LastModified
        {
            get { return lastModified; }
            internal set { lastModified = value; NotifyModelPropertyChanged("LastModified"); }

        }

        public PropertySource PropertySource
        {
            get { return propertySource; }
            set { propertySource = value; }
        }
        /// <summary>
        /// Gets the validation rules.
        /// </summary>
        /// <value>The validation rules.</value>
        public IValidationRule[] ValidationRules
        {
            get { return validationRules; }
        }
        /// <summary>
        /// Gets the post set actions.
        /// </summary>
        /// <value>The post set actions.</value>
        public SerializableDictionary<string, string[]> PostSetActions
        {
            get { return postSetActions; }
        }

        #endregion
        #region Constructor
        public DepictionElementPropertyData() : this("DefaultName", "Name", "string", null, null) { }
        public DepictionElementPropertyData(string propInterName, string displayName, object val) : this(propInterName, displayName, val, null, null) { }

        public DepictionElementPropertyData(string propertyInternalName, string displayName, object inValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs)
            : this(propertyInternalName, displayName, inValue, null,valRules, postSetActs, null)
        {
        }
        public DepictionElementPropertyData(string propertyInternalName, string displayName, object inValue,object initialValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs)
            : this(propertyInternalName, displayName, inValue, initialValue, valRules, postSetActs, null)
        {
        }
        public DepictionElementPropertyData(string propertyInternalName, string displayName, object inValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs, IElementProperty inValueChanger)
            : this(propertyInternalName, displayName, inValue, null, valRules, postSetActs, inValueChanger)
        {
        }
        public DepictionElementPropertyData(string propertyInternalName, string displayName, object inValue,object initValue,
                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs, IElementProperty inValueChanger)
        {
            internalName = DepictionStringService.ConvertStringToValidInternalName(propertyInternalName);
            this.displayName = displayName;
            if (inValueChanger != null)
            {
                valueChanger = inValueChanger;
            }
            else
            {
                propertyType = inValue.GetType();
                value = inValue;
            }
            initialValue = initValue;
            defaultValue = inValue;
            
            visibleToUser = true;
            editable = true;
            deletable = true;
            isHoverText = false;
            isInUse = true;
            propertySource = PropertySource.DML;
            validationRules = valRules;
            postSetActions = postSetActs;
        }

        void ValueChanger_ValueChanged(object obj)
        {
            NotifyModelPropertyChanged("Value");
        }

        #endregion

        #region Methods

        public void RefreshValue()
        {
            NotifyModelPropertyChanged("Value");
        }

        #endregion

        #region Cloning
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        //This should probably be a random method and not a deep clone since it loses the connection
        //to the valuechanger TODO
        public IElementPropertyData DeepClone()
        {//Using the serialize trick is more complete, but im slightly worried about read/write speeds
            //This does not creat the connection to the data changer 

            var rulesCopy = validationRules;
            if (validationRules != null)
            {
                var ruleList = new List<IValidationRule>();
                foreach (var rule in validationRules)
                {
                    ruleList.Add(rule);
                }
                rulesCopy = ruleList.ToArray();
            }

            var postSetActionsCopy = postSetActions;
            if (postSetActions != null)
            {
                postSetActionsCopy = postSetActions.DeepClone();
            }

            var newProp = new DepictionElementPropertyData(InternalName, DisplayName, Value, rulesCopy, postSetActionsCopy);

            newProp.displayName = displayName;

            newProp.defaultValue = DefaultValue;
            newProp.initialValue = InitialValue;
            newProp.visibleToUser = VisibleToUser;
            newProp.editable = Editable;
            newProp.deletable = Deletable;
            newProp.isHoverText = IsHoverText;
            newProp.isInUse = IsInUse;
            newProp.propertySource = PropertySource;

            return newProp;
        }
        public void CopyValuesFrom(IElementPropertyData other)
        {//Using the serialize trick is more complete, but im slightly worried about read/write speeds
            //This does not creat the connection to the data changer 

            var rulesCopy = other.ValidationRules;
            if (validationRules != null)
            {
                var ruleList = new List<IValidationRule>();
                foreach (var rule in validationRules)
                {
                    ruleList.Add(rule);
                }
                rulesCopy = ruleList.ToArray();
            }

            var postSetActionsCopy = other.PostSetActions;
            if (postSetActions != null)
            {
                postSetActionsCopy = postSetActions.DeepClone();
            }

            //            var newProp = new DepictionElementPropertyData(InternalName, DisplayName, Value, rulesCopy, postSetActionsCopy);
            propertyType = other.Value.GetType();
            value = other.Value;

            internalName = other.InternalName;

            displayName = other.DisplayName;

            defaultValue = other.DefaultValue;
            visibleToUser = other.VisibleToUser;
            editable = other.Editable;
            deletable = other.Deletable;
            isHoverText = other.IsHoverText;
            isInUse = other.IsInUse;
            propertySource = other.PropertySource;
            validationRules = rulesCopy;
            postSetActions = postSetActionsCopy;

            return;
        }
        public void SetInitialValue(object restoreValue)
        {
            initialValue = restoreValue;
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        override public bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionElementPropertyData;
            if (other == null) return false;
            //Ok so im getting lazy and there are a few properties that are not currently getting checked
            if (!Equals(Value, other.Value)) return false;
            if (!Equals(InitialValue, other.InitialValue)) return false;
            if (InternalName.Equals(other.InternalName) && PropertyType.Equals(other.PropertyType) &&
                 IsHoverText.Equals(other.IsHoverText) && Deletable.Equals(other.Deletable) &&
                VisibleToUser.Equals(other.VisibleToUser) && Editable.Equals(other.Editable) &&
                PropertySource.Equals(other.PropertySource))
            {
                return true;
            }
            return false;
        }

        #endregion
        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        public void ReadXml(XmlReader reader)
        {
            var propertyTypeString = string.Empty;
            if (reader.Name.Equals("property"))
            {
                propertyTypeString = reader.GetAttribute("typeName");

                propertyType = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(propertyTypeString);
                if(propertyType == null)
                {
                    //try older types
                    propertyType = DepictionCoreTypes.FindType(propertyTypeString);
                    if(propertyType == null)
                    {//Man this is bad if it gets here
                      
                       propertyType = DepictionTypeConverter.Get12ObjectTypeFromStringValue(propertyTypeString);
                    }
                }

                var valueString = reader.GetAttribute("value");
                var defaultValueString = reader.GetAttribute("defaultValue");
                var intialValueString = reader.GetAttribute("initialValue");
                if (valueString != null)
                {
                    if (propertyType != null)
                    {
                        try
                        {
                            value = DepictionTypeConverter.ChangeType(valueString, propertyType);
                            if (defaultValueString != null)
                            {
                                defaultValue = DepictionTypeConverter.ChangeType(defaultValueString, propertyType);
                            }
                            if (intialValueString != null)
                            {
                                initialValue = DepictionTypeConverter.ChangeType(intialValueString, propertyType);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("type change problem");
                        }
                    }else
                    {
                        reader.ReadStartElement();//I guess this moves to the next node, i think this will cause bugs though :(
                            //reader.ReadEndElement();
                        throw new Exception("Property cannot be null " + propertyTypeString);
                    }
                }

                internalName = reader.GetAttribute("name");
                displayName = reader.GetAttribute("displayName");

                visibleToUser = reader.GetAttribute("visible").ToBool(visibleToUserDefault);
                editable = reader.GetAttribute("editable").ToBool(editableDefault);
                deletable = reader.GetAttribute("deletable").ToBool(deletableDefault);
                isHoverText = reader.GetAttribute("isHoverText").ToBool(isHoverTextDefault);
                isInUse = reader.GetAttribute("isInUse").ToBool(isInUseDefault);
                
            }
            if (!reader.IsEmptyElement)
            {
                reader.Read();
                if(reader.Name.Equals("value"))//INcomplete
                {
                    reader.ReadStartElement("value");
                    value = SerializationService.DeserializeObject(propertyType, reader);
                    reader.ReadEndElement();
                }
                if (reader.Name.Equals("initialValue"))//INcomplete
                {
                    reader.ReadStartElement("initialValue");
                    initialValue = SerializationService.DeserializeObject(propertyType, reader);
                    reader.ReadEndElement();
                }
               
                postSetActions = new SerializableDictionary<string, string[]>();
                if (reader.Name.Equals("postSetActions"))
                {
                    postSetActions = UnifiedDepictionElementReader.ReadPostSetActions(reader);
                }
                validationRules = new IValidationRule[0];
                if (reader.Name.Equals("validationRules"))
                {
                    validationRules = UnifiedDepictionElementReader.ReadValidationRules(reader, propertyTypeString).ToArray();
                }
            }
            reader.Read();//Why not read end?
        }

        public void WriteXml(XmlWriter writer)
        {
            UnifiedDepictionElementWriter.WriteProperty(writer, this, false);
        }

        #endregion

    }
}