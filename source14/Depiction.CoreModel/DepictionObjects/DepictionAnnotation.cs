﻿using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects
{
    public class DepictionAnnotation : IDepictionAnnotation
    {
        #region Variables

        private string annotationID = Guid.NewGuid().ToString();
        private ILatitudeLongitude mapLocation;
        private double pixelWidth = 200;
        private double pixelHeight = 50;
        private double contentLocationX = 20;
        private double contentLocationY = 25;
        private double scaleWhenCreated = double.NaN;
        private double iconSize = 20;
        private bool isAnnotationVisible = true;
        private bool isTextCollapsed;
        private string backgroundColor = string.Empty;
        private string foregroundColor = "Black";
        private string annotationText = "Enter annotation text here.";
        
        #endregion

        #region Implementation of IDepictionAnnotation
        public string ElementType { get { return "Depiction.Annotation"; } }
        public string TypeDisplayName
        {
            get { return "Annotation"; }
        }

        public DepictionIconPath IconPath
        {
            get
            {
                var iconPathUri = "embeddedresource:Depiction.Notes";
                return DepictionIconPath.Parse(iconPathUri);
            }
        }
        public ILatitudeLongitude MapLocation
        {
            get { return mapLocation; }
            set { mapLocation = value; }
        }
        public string AnnotationID { get { return annotationID; } }
        public string AnnotationText
        {
            get { return annotationText; }
            set { annotationText = value; }
        }

        public double ContentLocationX
        {
            get { return contentLocationX; }
            set { contentLocationX = value; }
        }

        public double ContentLocationY
        {
            get { return contentLocationY; }
            set { contentLocationY = value; }
        }

        public double PixelWidth
        {
            get { return pixelWidth; }
            set { pixelWidth = value; }
        }

        public double PixelHeight
        {
            get { return pixelHeight; }
            set { pixelHeight = value; }
        }

        public double IconSize
        {
            get { return iconSize; }
        }

        public bool IsAnnotationVisible
        {
            get { return isAnnotationVisible; }
        }
        public double ScaleWhenCreated
        {
            get { return scaleWhenCreated; }
            set { scaleWhenCreated = value; }
        }
        public bool IsTextCollapsed
        {
            get { return isTextCollapsed; }
            set { isTextCollapsed = value; }
        }

        public string BackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; }
        }

        public string ForegroundColor
        {
            get { return foregroundColor; }
            set { foregroundColor = value; }
        }

        #endregion

        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            if (reader.Name.Equals("MapLocation"))
            {
                MapLocation = SerializationService.DeserializeObject<LatitudeLongitude>("MapLocation", reader);
            }
            if (reader.Name.Equals("AnnotationText"))
            {
                AnnotationText = reader.ReadElementContentAsString("AnnotationText", ns);
            }
            if (reader.Name.Equals("ContentLocationX"))
            {
                ContentLocationX = reader.ReadElementContentAsDouble("ContentLocationX", ns);
            }
            if (reader.Name.Equals("ContentLocationY"))
            {
                ContentLocationY = reader.ReadElementContentAsDouble("ContentLocationY", ns);
            }
            if (reader.Name.Equals("PixelWidth"))
            {
                PixelWidth = reader.ReadElementContentAsDouble("PixelWidth", ns);
            }
            if (reader.Name.Equals("PixelHeight"))
            {
                PixelHeight = reader.ReadElementContentAsDouble("PixelHeight", ns);
            }
            if(reader.Name.Equals("ScaleWhenCreated"))
            {
                ScaleWhenCreated = reader.ReadElementContentAsDouble("ScaleWhenCreated", ns);
            }
            if (reader.Name.Equals("IsTextCollapsed"))
            {
                IsTextCollapsed = reader.ReadElementContentAsBoolean("IsTextCollapsed", ns);
            }
            if (reader.Name.Equals("BackgroundColor"))
            {
                BackgroundColor = reader.ReadElementContentAsString("BackgroundColor", ns);
            }
            if (reader.Name.Equals("ForegroundColor"))
            {
                ForegroundColor = reader.ReadElementContentAsString("ForegroundColor", ns);
            }

            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("DepictionAnnotation");
            SerializationService.SerializeObject("MapLocation", MapLocation as LatitudeLongitude, writer);
            writer.WriteElementString("AnnotationText", ns, AnnotationText);
            writer.WriteElementString("ContentLocationX", ns, ContentLocationX.ToString());
            writer.WriteElementString("ContentLocationY", ns, ContentLocationY.ToString());
            writer.WriteElementString("PixelWidth", ns, PixelWidth.ToString());
            writer.WriteElementString("PixelHeight", ns, PixelHeight.ToString());
            writer.WriteElementString("ScaleWhenCreated",ns,ScaleWhenCreated.ToString());
            //The to lower is needed for it to be round trippable, for now
            writer.WriteElementString("IsTextCollapsed", ns, IsTextCollapsed.ToString().ToLower());
            writer.WriteElementString("BackgroundColor", ns, BackgroundColor);
            writer.WriteElementString("ForegroundColor", ns, ForegroundColor);
            writer.WriteEndElement();
        }

        #endregion

        #region Equals override 

        override public bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionAnnotation;
            if (other == null) return false;
            if (!Equals(MapLocation,other.MapLocation) || !AnnotationText.Equals(other.AnnotationText))
            {
                return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
    }
}
