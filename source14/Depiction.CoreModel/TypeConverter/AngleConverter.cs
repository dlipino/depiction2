﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.CoreModel.ValueTypes;

namespace Depiction.CoreModel.TypeConverter
{
    public class AngleConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var angle = (Angle)value;
                return string.Format(culture, "{0} degrees", angle.Value);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                if (((string)value).EndsWith("degrees"))
                {
                    double angleValue;
                    if (double.TryParse(((string)value).Replace("degrees", ""), NumberStyles.Any, info, out angleValue))
                        return new Angle {Value = angleValue};
                }
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}