﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    public class VolumeConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var measurement = (IMeasurement)value;
                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Volume", out systemString, null);
                if (valueString == null) return null;

                var scale = MeasurementScale.Normal;
                var system = MeasurementSystem.Metric;

                switch (systemString.ToLower())
                {
                    case "liters":
                    case "liter":
                    case "litres":
                    case "litre":
                    case "l":
                    case "metric":
                        system = MeasurementSystem.Metric;
                        break;
                    case "gallons":
                    case "gallon":
                    case "gal":
                    case "imperial":
                        system = MeasurementSystem.Imperial;
                        break;
                    case "ounces":
                    case "ounce":
                    case "oz":
                    case "imperialsmall":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Small;
                        break;
                    case "milliliters":
                    case "milliliter":
                    case "millilitres":
                    case "millilitre":
                    case "ml":
                    case "metricsmall":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Small;
                        break;
                    default:
                        throw new Exception(string.Format("Volume cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Volume(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}