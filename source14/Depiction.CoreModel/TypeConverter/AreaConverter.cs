﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    public class AreaConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var measurement = (IMeasurement)value;
                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Area", out systemString, null);
                if (valueString == null) return null;

                var scale = MeasurementScale.Normal;
                var system = MeasurementSystem.Metric;

                switch (systemString.ToLower())
                {
                    case "square meters":
                    case "square metres":
                    case "square meter":
                    case "square metre":
                    case "square m":
                    case "sq meters":
                    case "sq metres":
                    case "sq meter":
                    case "sq metre":
                    case "sq m":
                    case "metric":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Normal;
                        break;
                    case "square feet":
                    case "square ft":
                    case "sq feet":
                    case "sq ft":
                    case "imperial":
                        system = MeasurementSystem.Imperial;
                        break;
                    case "square inches":
                    case "square inch":
                    case "square in":
                    case "sq inches":
                    case "sq inch":
                    case "sq in":
                    case "imperialsmall":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Small;
                        break;
                    case "square centimeters":
                    case "square centimetres":
                    case "square centimeter":
                    case "square centimetre":
                    case "square cm":
                    case "sq centimeters":
                    case "sq centimetres":
                    case "sq centimeter":
                    case "sq centimetre":
                    case "sq cm":
                    case "metricsmall":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Small;
                        break;
                    case "square km":
                    case "square kms"://Thanks to dlps error with abbrevations and not know how the metric system works
                    case "metriclarge":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Large;
                        break;
                    case "square miles":
                    case "imperiallarge":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Large;
                        break;
                    default:
                        throw new Exception(string.Format("Area cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Area(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}