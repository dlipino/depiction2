using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    /// <summary>
    /// TODO Converts one type to another, type conversion is all over the place, fix after element adding gets completed
    /// DepictionCoreTypes does similar things
    /// </summary>
    public static class DepictionTypeConverter
    {
        public static object ChangeTypeByGuessing(object value)
        {
            //The measurement types aren't used for guess so anything that looks like a measurement type goes on as if
            //it were a string
            if (value == null) return null;

            try
            {
                var newVal = EnhancedBooleanConverter.Parse(value.ToString());
                if (newVal != null) return newVal;
            }
            catch
            {
            }

            double number;
            if( double.TryParse(value.ToString(),out number))
            {
                return number;
            }

            return value;
        }
        /// <summary>
        /// Changes the type.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="conversionType">Type of the conversion.</param>
        /// <returns></returns>
        public static object ChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
                return null;
            if (value.GetType().Equals(conversionType))
                return value;
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                var nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            if (value is string && conversionType == typeof(LatitudeLongitude))
            {
                var latLongConverter = new LatitudeLongitudeTypeConverter();
                return latLongConverter.ConvertFrom(value);
            }
            if (value is string && conversionType == typeof(Color))
            {
                try
                {
                    return (Color)ColorConverter.ConvertFromString((string)value);
                }
                catch
                {
                    throw new Exception(string.Format("Value \"{0}\" is not a valid color.", value));
                }
            }
            if(value is Color && conversionType.Equals(typeof(string)))
            {
                return new SolidColorBrush((Color)value).ToString();
            }
            if (value is string && conversionType.IsEnum)
            {
                try
                {
                    return Enum.Parse(conversionType, (string)value);
                }
                catch (ArgumentException)
                {
                    throw new Exception(string.Format("Value \"{0}\" is not defined for enum \"{1}\"", value,
                                                                  conversionType.FullName));
                    //                    throw new InvalidElementFileEnumException(string.Format(
                    //                                                                  "Value \"{0}\" is not defined for enum \"{1}\"", value,
                    //                                                                  conversionType.FullName));
                }
            }
            if (value is string && conversionType == typeof(bool))
            {
                try
                {
                    return EnhancedBooleanConverter.Parse((string)value);
                }
                catch
                {
                    throw new Exception(string.Format("Value \"{0}\" is not a valid boolean.", value));
                }
            }

            //.NET's Int32Converter doesn't allow thousands separators, so we special case and use int.Parse
            if (value is string && conversionType == typeof(int))
            {
                return int.Parse((string)value, NumberStyles.AllowThousands, CultureInfo.CurrentCulture);
            }
            //find destination type converter
            var converter = TypeDescriptor.GetConverter(conversionType);

            if (converter != null && value is string && converter.CanConvertFrom(typeof(string)))
            {

                return converter.ConvertFromInvariantString((string)value);
            }
            if (converter != null && converter.CanConvertFrom(value.GetType()))
            {
                return converter.ConvertFrom(value);
            }

            //find source type converter
            var sourceTypeConverter = TypeDescriptor.GetConverter(value.GetType());
            if (sourceTypeConverter != null && conversionType.Equals(typeof(string)) && sourceTypeConverter.CanConvertTo(typeof(string)))
            {
                return sourceTypeConverter.ConvertToInvariantString(value);
            }
            if (sourceTypeConverter != null && sourceTypeConverter.CanConvertTo(conversionType))
            {
                return sourceTypeConverter.ConvertTo(value, conversionType);
            }
            return Convert.ChangeType(value, conversionType);
        }

        /// <summary>
        /// Extracts the value and measurement system from the given value, which must be a string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="typeString"></param>
        /// <param name="theSystem"></param>
        /// <param name="defaultSystem"></param>
        /// <returns></returns>
        public static string GetTheValue(object value, string typeString, out string theSystem, string defaultSystem)
        {
            var valString = value as string;
            if (valString == null)
            {
                throw new Exception(string.Format("Cannot have null value for {0}", typeString));
            }
            valString = valString.Trim();
            var options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            var pattern = @"^(?<value>[^ \t]+)[ \t]+(?<system>.+)$";
            if (!Regex.IsMatch(valString, pattern, options))
            {
                theSystem = null;
                return null;
            }
            var matches = Regex.Matches(valString, pattern, options);
            var match = matches[0];
            var theValue = match.Groups["value"].Value;
            theSystem = match.Groups["system"].Value;
            return theValue;
        }
        #region Refactor converters
        static public Type GetRuleTypeFromStringValue(string inType)
        {
            if (inType.Contains("MinValueValidationRule"))
            {
                return typeof(MinValueValidationRule);
            }
            if (inType.Contains("DataTypeValidationRule"))
            {
                return typeof(DataTypeValidationRule);
            }
            if (inType.Contains("RangeValidationRule"))
            {
                return typeof(RangeValidationRule);
            }
            if (inType.Contains("TerrainExistsValidationRule"))
            {
                return typeof(TerrainExistsValidationRule);
            }

            return null;
        }

        //TODO This seems to work very well for 1.2 style elements competes with DepictionCoreTypes
        static public Type Get12ObjectTypeFromStringValue(string inTypeString)
        {
            //            Console.WriteLine("Converting " + inTypeString);
            if (inTypeString.Equals((typeof(Color)).ToString()))
            {
                return typeof(Color);
            }
            if (inTypeString.Contains("MinValueValidationRule"))
            {
                return typeof(MinValueValidationRule);
            }
            if (inTypeString.Contains("DataTypeValidationRule"))
            {
                return typeof(DataTypeValidationRule);
            }
            if (inTypeString.Contains("RangeValidationRule"))
            {
                return typeof(RangeValidationRule);
            }
            if (inTypeString.Contains("TerrainExistsValidationRule"))
            {
                return typeof(TerrainExistsValidationRule);
            }

            var parsed = inTypeString.Split(',');
            if (parsed.Length == 2)
            {//Probably a depiction type
                if (parsed[1].Trim().Equals("Depiction.API"))
                {
                    if (parsed[0].Equals("Depiction.API.Enums.VisualType"))
                    {
                        //This puppy got depicrated for ever!!
                        return null;
                    }
                    var depictionTypes = parsed[0].Split('.');
                    var type = depictionTypes[depictionTypes.Length - 1];
                    if (type.Equals("DataCategory"))
                    {//This needs to go on teh chopping block
                        return null;
                        //                        return typeof(DataCategory);
                    }
                    if (type.Equals("ElementClassification"))//Need to change this to be depiction specific, maybe, i guess i don't know proper procedure for this
                    {
                        return typeof(ElementClassification);
                    }
                    if (type.Equals("Temperature"))
                    {
                        return typeof(Temperature);
                    }
                    if (type.Equals("Volume"))
                    {
                        return typeof(Volume);
                    }
                    if (type.Equals("Speed"))
                    {
                        return typeof(Speed);
                    }
                    if (type.Equals("Weight"))
                    {
                        return typeof(Weight);
                    }
                    if (type.Equals("Area"))
                    {
                        return typeof(Area);
                    }
                    if (type.Equals("Distance"))
                    {
                        return typeof(Distance);
                    }
                    if (type.Equals("Angle"))
                    {
                        return typeof(Angle);
                    }
                    if (type.Equals("LatitudeLongitude"))
                    {
                        return typeof(LatitudeLongitude);
                    }
                }
            }

            return null;
        }

        #endregion
    }
}