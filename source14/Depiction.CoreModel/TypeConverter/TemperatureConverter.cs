﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    public class TemperatureConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var measurement = (IMeasurement)value;
                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Temperature", out systemString, null);
                if (valueString == null) return null;

                var system = MeasurementSystem.Metric;
                var scale = MeasurementScale.Normal;

                switch (systemString.ToLower())
                {
                    case "celsius":
                    case "centigrade":
                    case "c":
                    case "°celsius":
                    case "°centigrade":
                    case "°c":
                    case "degrees celsius":
                    case "degrees centigrade":
                    case "degrees c":
                    case "metric":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Normal;
                        break;
                    case "fahrenheit":
                    case "f":
                    case "°fahrenheit":
                    case "°f":
                    case "degrees fahrenheit":
                    case "degrees f":
                    case "imperial":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Normal;
                        break;
                    default:
                        throw new Exception(string.Format("Temperature cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Temperature(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}