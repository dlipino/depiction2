using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.HelperClasses
{
    public class UnifiedDepictionElementWriter
    {
        static public bool WriteIDepictionElementBaseToFile(IDepictionElementBase propertyHolder, string fullFileName)
        {
            return WriteIDepictionElementBaseToFile(propertyHolder, fullFileName, false);
        }

        static public bool WriteIDepictionElementBaseToFile(IDepictionElementBase propertyHolder, string fullFileName, bool saveTypes)
        {
            try
            {
                var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, CheckCharacters = false, Encoding = Encoding.UTF8 };
                var writer = XmlWriter.Create(fullFileName, settings);
                using (writer)
                {
                    WriteDepictionElementBase(writer, propertyHolder, saveTypes);
                    return true;
                }
            }
            catch (PathTooLongException)
            {
                return false;
            }
        }

        public static void WriteDepictionElementBase(XmlWriter writer, IDepictionElementBase elementToSave, bool saveTypes)
        {
            var elementParent = elementToSave as IDepictionElement;
            var elementPrototype = elementToSave as IElementPrototype;
            if (SerializationService.IsSaveLoadCancelled() == true) return;

            writer.WriteStartElement("DepictionElement");
            writer.WriteAttributeString("elementType", elementToSave.ElementType);
            writer.WriteAttributeString("displayName", elementToSave.TypeDisplayName);
            if (elementParent != null)
            {
                writer.WriteAttributeString("elementKey", elementParent.ElementKey);
            }
            if (elementPrototype != null)
            {
                writer.WriteAttributeString("definitionSource", elementPrototype.PrototypeOrigin);
            }
            if (elementToSave.UsePermaText)
            {
                writer.WriteAttributeString("usingPermaText", elementToSave.UsePermaText.ToString().ToLower());
            }
            if (elementToSave.UsePropertyNameInHoverText)
            {
                writer.WriteAttributeString("usePropertyNameInHoverText", elementToSave.UsePropertyNameInHoverText.ToString().ToLower());
            }
            writer.WriteAttributeString("definitionGroup", elementToSave.DefinitionGroup);
            if (SerializationService.IsSaveLoadCancelled() == true) return;

            //Properties
            writer.WriteStartElement("Properties");
            var propSet = new HashSet<string>();
            foreach (var property in elementToSave.OrderedCustomProperties)//Grrrrr
            {
                propSet.Add(property.ValueType.Name);
                property.WriteXml(writer);
            }
            writer.WriteEndElement();

            //elementWaypoints
            if (elementToSave.Waypoints.Length > 0)
            {
                SerializationService.SerializeItemList("elementWaypoints", elementToSave.Waypoints, writer);
            }

            //Permatext
            if (SerializationService.IsSaveLoadCancelled() == true) return;

            if (!elementToSave.PermaText.IsDefault)
                SerializationService.SerializeObject("PermaText", (DepictionPermaText)elementToSave.PermaText, writer);

            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (elementParent != null)//For now don't save tags/image/zoi for prototypes
            {
                if (elementToSave.Tags.Count > 0)
                {
                    SerializationService.SerializeItemList("Tags", elementToSave.Tags, writer);
                }

                //This is not liking the IZone if it is invalid
                if (elementToSave.ZoneOfInfluence != null && !elementToSave.ZoneOfInfluence.Geometry.IsEmpty)
                {
                    if (elementToSave.ElementType.Equals("Depiction.Plugin.RoadNetwork"))
                    {
                        SerializationService.SerializeObject("ZoneOfInfluence", new ZoneOfInfluence(), writer);
                    }
                    else
                    {
                        SerializationService.SerializeObject("ZoneOfInfluence",
                                                             (ZoneOfInfluence)elementToSave.ZoneOfInfluence, writer);
                    }
                }

                if (!string.IsNullOrEmpty(elementToSave.ImageMetadata.ImageFilename))
                {
                    SerializationService.SerializeObject("ImageMetadata",
                                                         (DepictionImageMetadata)elementToSave.ImageMetadata, writer);
                }
            }

            if (elementToSave.ClickActions != null && elementToSave.ClickActions.Count > 0)
            {
                WriteElementActions(writer, elementToSave.ClickActions, "onClick");
            }
            if (elementToSave.CreateActions != null && elementToSave.CreateActions.Count > 0)
            {
                WriteElementActions(writer, elementToSave.CreateActions, "onCreate");
            }
            if (elementToSave.GenerateZoiActions != null && elementToSave.GenerateZoiActions.Count > 0)
            {
                WriteElementActions(writer, elementToSave.GenerateZoiActions, "generateZoi");
            }
            if (elementToSave.DeleteActions != null && elementToSave.DeleteActions.Count > 0)
            {
                WriteElementActions(writer, elementToSave.DeleteActions, "onDelete");
            }
            //This is for know what name belongs to what c# type
            //ONly do this for prototypes
            if (elementParent == null && saveTypes)
            {
                writer.WriteStartElement("elementValueTypes");
                //                writer.WriteStartElement("actions");
                foreach (var simpleName in propSet)
                {
                    var fullName =
                        DepictionTypeInformationSerialization.GetFullTypeStringFromSimpleTypeString(simpleName);
                    if (!string.IsNullOrEmpty(fullName))
                    {
                        writer.WriteStartElement("typePair");
                        writer.WriteAttributeString("key", simpleName);
                        writer.WriteAttributeString("value", fullName);
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement();
            }

            //End of depictionElement 
            writer.WriteEndElement();
        }
        //Still not used :(
        public static void WriteProperty(XmlWriter writer, IElementProperty property, bool useValueAsDefault)
        {
            if (property.Value is IElementPrototype) return;//Why is this here?

            writer.WriteStartElement("property");
            writer.WriteAttributeString("name", property.InternalName);
            if (property.DisplayName != String.Empty)
                writer.WriteAttributeString("displayName", property.DisplayName);
            //Saving the value as a string won't always work, hacked in some way to tell if it would work
            bool writePropAsString = true;
            //An intersting hack
            if (property.ValueType.ToString().Equals(property.Value.ToString()))//A better way to do this is to see if a propertype converter (to string) exists.
            {
                writePropAsString = false;
            }

            //if (!(property.Value is IXmlSerializable) && !(property.Value is IXmlSerializable) && !(property.Value is IXmlSerializable))
            if (writePropAsString)
            {
                writer.WriteAttributeString("value", (string)DepictionTypeConverter.ChangeType(property.Value, typeof(string)));
                if(property.RestoreValue != null)
                    writer.WriteAttributeString("restoreValue", (string)DepictionTypeConverter.ChangeType(property.RestoreValue, typeof(string)));

            }
            var typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(property.Value.GetType());

            writer.WriteAttributeString("typeName", typeName);


            if (property.Editable != DepictionElementProperty.editableDefault)
                writer.WriteAttributeString("editable", property.Editable.ToString());

            if (property.VisibleToUser != DepictionElementProperty.visibleToUserDefault)
                writer.WriteAttributeString("visible", property.VisibleToUser.ToString());

            if (property.Deletable != DepictionElementProperty.deletableDefault)
                writer.WriteAttributeString("deletable", property.Deletable.ToString());

            if (property.IsHoverText != DepictionElementProperty.isHoverTextDefault)
                writer.WriteAttributeString("isHoverText", property.IsHoverText.ToString());


            if (!writePropAsString)//INcompolete
            {
                writer.WriteStartElement("value");
                SerializationService.SerializeObject(property.Value, property.ValueType, writer);
                writer.WriteEndElement();

                if (property.RestoreValue != null)
                {
                    writer.WriteStartElement("restoreValue");
                    SerializationService.SerializeObject(property.RestoreValue, property.ValueType, writer);
                    writer.WriteEndElement();
                }
            }

            if (property.PostSetActions != null && property.PostSetActions.Count > 0)
            {
                writePostSetActions(writer, property);
            }

            if (property.ValidationRules != null && property.ValidationRules.Length > 0)
            {
                WriteValidationRules(writer, property);
            }

            writer.WriteEndElement();
        }

        private static void writePostSetActions(XmlWriter writer, IElementProperty property)
        {
            writer.WriteStartElement("postSetActions");

            foreach (var postSetAction in property.PostSetActions)
            {
                writer.WriteStartElement("action");
                writer.WriteAttributeString("name", postSetAction.Key);
                writer.WriteStartElement("parameters");

                foreach (string parameter in postSetAction.Value)
                {
                    writer.WriteStartElement("parameter");
                    var split = parameter.Split(':');
                    if (split.Length == 2)
                    {
                        writer.WriteAttributeString("elementQuery", split[0]);
                        writer.WriteAttributeString("value", split[1]);
                    }
                    else
                    {
                        writer.WriteAttributeString("elementQuery", parameter);
                    }
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteValidationRules(XmlWriter writer, IElementProperty property)
        {
            writer.WriteStartElement("validationRules");

            foreach (IValidationRule rule in property.ValidationRules)
            {
                writer.WriteStartElement("validationRule");
                var typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(rule.GetType());
                writer.WriteAttributeString("type", typeName);
                writer.WriteStartElement("parameters");

                foreach (ValidationRuleParameter parameter in rule.Parameters)
                {
                    writer.WriteStartElement("parameter");
                    writer.WriteAttributeString("value", parameter.Value);
                    typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(parameter.Type);
                    writer.WriteAttributeString("type", typeName);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static void WriteElementActions(XmlWriter writer, Dictionary<string, string[]> actions, string actionElementName)
        {
            if (actions != null && actions.Keys.Count > 0)
            {
                writer.WriteStartElement(actionElementName);
                writer.WriteStartElement("actions");
                foreach (var action in actions)
                {
                    WriteActionKeyValuePair(writer, action);
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        private static void WriteActionKeyValuePair(XmlWriter writer, KeyValuePair<string, string[]> action)
        {
            writer.WriteStartElement("action");
            writer.WriteAttributeString("name", action.Key);
            if (action.Value != null && action.Value.Length > 0)
            {
                writer.WriteStartElement("parameters");
                foreach (string parameter in action.Value)
                {
                    writer.WriteStartElement("parameter");
                    writer.WriteAttributeString("elementQuery", parameter);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
    }
}