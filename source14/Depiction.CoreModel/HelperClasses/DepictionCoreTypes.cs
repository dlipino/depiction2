using System;
using System.Collections.Generic;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.Serialization;
using Area12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Area;
using Distance12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Distance;
using Speed12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Speed;
using Volume12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Volume;
using Weight12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Weight;
using LatitudeLongitude12 = Depiction.DPNPorting.SerializationObjects12.DataTypes12.LatitudeLongitude;
using Temperature12=Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12.Temperature;

namespace Depiction.CoreModel.HelperClasses
{
    //Ok this is a hassle since all the legal types have to manully inputted
    public class DepictionCoreTypes
    {
        //TODO Competes with DepictionTypeConverter, sort of
//        private static ReadOnlyCollection<KeyValuePair<string, Type>> typesCollection;
        private static Dictionary<string, Type> allowedElementPropertyTypeDictionary;
        private static Dictionary<string, Type> stringToTypeForConversion;
        private static List<Type> depictionCoreTypes = new List<Type>();
        static public Dictionary<string, Type> StringToTypeDictionaryFor122To13Conversion { get { return stringToTypeForConversion; } }
        static DepictionCoreTypes()
        {
            allowedElementPropertyTypeDictionary = new Dictionary<string, Type>();
            allowedElementPropertyTypeDictionary.Add("DepictionIconBorderShape", typeof(DepictionIconBorderShape));
            allowedElementPropertyTypeDictionary.Add("DepictionIconPath", typeof(DepictionIconPath));
            //            allowedElementPropertyTypeDictionary.Add("DepictionIconSize", typeof(DepictionIconSize));
            allowedElementPropertyTypeDictionary.Add("ZOIShapeType", typeof(ZOIShapeType));
            allowedElementPropertyTypeDictionary.Add("MinValueValidationRule", typeof(MinValueValidationRule));
            allowedElementPropertyTypeDictionary.Add("DataTypeValidationRule", typeof(DataTypeValidationRule));
            allowedElementPropertyTypeDictionary.Add("RangeValidationRule", typeof(RangeValidationRule));
            allowedElementPropertyTypeDictionary.Add("TerrainExistsValidationRule", typeof(TerrainExistsValidationRule));//i hate this one

            allowedElementPropertyTypeDictionary.Add("Area", typeof(Area));
            allowedElementPropertyTypeDictionary.Add("Distance", typeof(Distance));
            allowedElementPropertyTypeDictionary.Add("Speed", typeof(Speed));
            allowedElementPropertyTypeDictionary.Add("Temperature", typeof(Temperature));
            allowedElementPropertyTypeDictionary.Add("Volume", typeof(Volume));
            allowedElementPropertyTypeDictionary.Add("Weight", typeof(Weight));
            allowedElementPropertyTypeDictionary.Add("Color", typeof(Color));
            allowedElementPropertyTypeDictionary.Add("Bool", typeof(bool));
            allowedElementPropertyTypeDictionary.Add("Boolean", typeof(bool));
            allowedElementPropertyTypeDictionary.Add("Number", typeof(double));
            allowedElementPropertyTypeDictionary.Add("Integer", typeof(int));
            allowedElementPropertyTypeDictionary.Add("Double", typeof(double));
            allowedElementPropertyTypeDictionary.Add("Float", typeof(double));
            allowedElementPropertyTypeDictionary.Add("String", typeof(string));
            allowedElementPropertyTypeDictionary.Add("Text", typeof(string));
            allowedElementPropertyTypeDictionary.Add("Angle", typeof(Angle));
            allowedElementPropertyTypeDictionary.Add("RoadGraph", typeof (RoadGraph));
            //            allowedElementPropertyTypeDictionary.Add("Terrain", typeof(Terrain));//is this even needed, kind of just hacked in
            #region the base depiction types, hopefully
            depictionCoreTypes.Add(typeof(Area));
            depictionCoreTypes.Add(typeof(Distance));
            depictionCoreTypes.Add(typeof(Temperature));
            depictionCoreTypes.Add(typeof(Volume));
            depictionCoreTypes.Add(typeof(Weight));
            depictionCoreTypes.Add(typeof(Speed));

            depictionCoreTypes.Add(typeof(int));
            depictionCoreTypes.Add(typeof(double));
            depictionCoreTypes.Add(typeof(string));
            depictionCoreTypes.Add(typeof(bool));
            depictionCoreTypes.Add(typeof(Color));
            //depictionCoreTypes.Add(typeof(RuntimeType));//WHy does this one not work

            depictionCoreTypes.Add(typeof(RoadGraph));
            depictionCoreTypes.Add(typeof(MapCoordinateBounds));
            depictionCoreTypes.Add(typeof(LatitudeLongitude));
            depictionCoreTypes.Add(typeof(ZOIShapeType));
            depictionCoreTypes.Add(typeof(DepictionIconPath));
            depictionCoreTypes.Add(typeof(DepictionIconBorderShape));

            depictionCoreTypes.Add(typeof(DataTypeValidationRule));
            depictionCoreTypes.Add(typeof(TerrainExistsValidationRule));
            depictionCoreTypes.Add(typeof(MinValueValidationRule));
            depictionCoreTypes.Add(typeof(RangeValidationRule));
            #endregion

            CreateConversionTypes();

        }
        static public void DoFullTypeUpdateFromFileAndDefaults(string fileName)
        {
            DepictionTypeInformationSerialization.UpdateSerializationServiceTypeDictionaryWithFile(fileName);
            UpdateTypeSerializationDictionaryWithDefaultTypes();
        }
        static public void UpdateTypeSerializationDictionaryWithDefaultTypes()
        {
//            if (DepictionTypeInformationSerialization.TypeFileExists) return;
//            var existingDict = DepictionTypeInformationSerialization.SimpleTypeNameToFullNameDictionary;
//            //it should never be null
//            if(existingDict == null)
//            {
//                existingDict = DepictionTypeInformationSerialization.SimpleTypeNameToFullNameDictionary =
//                               new SerializableDictionary<string, string>();
//            }
            foreach (var type in depictionCoreTypes)
            {
//                var simpleName = type.Name;
                DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(type);
//                if(existingDict.ContainsKey(simpleName))
//                {
//                    continue;
//                }
//                var fullName = type.FullName;
//                existingDict.Add(simpleName,fullName);
            }
        }
        static private void CreateConversionTypes()
        {
            stringToTypeForConversion = new Dictionary<string, Type>();
            //Longer names first to hack use contains
            stringToTypeForConversion.Add("DepictionIconBorderShape", typeof(DepictionIconBorderShape));
            stringToTypeForConversion.Add("DepictionIconPath", typeof(DepictionIconPath));
            //            allowedElementPropertyTypeDictionary.Add("DepictionIconSize", typeof(DepictionIconSize));
            stringToTypeForConversion.Add("ZOIShapeType", typeof(ZOIShapeType));
            stringToTypeForConversion.Add("MinValueValidationRule", typeof(MinValueValidationRule));
            stringToTypeForConversion.Add("DataTypeValidationRule", typeof(DataTypeValidationRule));
            stringToTypeForConversion.Add("RangeValidationRule", typeof(RangeValidationRule));
            stringToTypeForConversion.Add("TerrainExistsValidationRule", typeof(TerrainExistsValidationRule));//i hate this one

            stringToTypeForConversion.Add("Area", typeof(Area12));
            stringToTypeForConversion.Add("Distance", typeof(Distance12));
            stringToTypeForConversion.Add("Speed", typeof(Speed12));
            stringToTypeForConversion.Add("Temperature", typeof(Temperature12));
            stringToTypeForConversion.Add("Volume", typeof(Volume12));
            stringToTypeForConversion.Add("LatitudeLongitude", typeof(LatitudeLongitude12));
            stringToTypeForConversion.Add("Weight", typeof(Weight12));
            stringToTypeForConversion.Add("Color", typeof(Color));
            stringToTypeForConversion.Add("Bool", typeof(bool));
            stringToTypeForConversion.Add("Boolean", typeof(bool));
            stringToTypeForConversion.Add("Number", typeof(double));
            stringToTypeForConversion.Add("Integer", typeof(int));
            stringToTypeForConversion.Add("Double", typeof(double));
            stringToTypeForConversion.Add("Float", typeof(double));
            stringToTypeForConversion.Add("String", typeof(string));
            stringToTypeForConversion.Add("Text", typeof(string));
            stringToTypeForConversion.Add("Angle", typeof(Angle));
            stringToTypeForConversion.Add("Terrain", typeof(Terrain));
            stringToTypeForConversion.Add("RoadGraph", typeof(RoadGraph));


        }

        public static Type FindType(string friendlyName)
        {
            if (allowedElementPropertyTypeDictionary.ContainsKey(friendlyName))
            {
                return allowedElementPropertyTypeDictionary[friendlyName];
            }
            return null;
        }

        public static string FriendlyName(Type requestedType)
        {
            foreach(var keyValuePair in allowedElementPropertyTypeDictionary)
            {
                if (requestedType.Equals(keyValuePair.Value))
                {
                    return keyValuePair.Key;
                }
            }
            return requestedType.Name;
        }
    }
}