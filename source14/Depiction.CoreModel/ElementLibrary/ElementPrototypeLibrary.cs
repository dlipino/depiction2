﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace Depiction.CoreModel.ElementLibrary
{
    //Needs to be renamed to ElementDefinitionLibrary
    public class ElementPrototypeLibrary : IElementPrototypeLibrary
    {        //This will need updating, more importantly, why should there be 3 different prototype lists? This thing really needs help

        public event Action PrototypesUpdated;

        public static Dictionary<string, string> TypeConversionDictionary122To13 = new Dictionary<string, string>
                                                                                       {
                                                                                           { "Depiction.Plugin.ShapeUserDrawn","Depiction.Plugin.UserDrawnPolygon" },
                                                                                           {"Depiction.Plugin.Lines","Depiction.Plugin.LoadedLines"},
                                                                                           {"Depiction.Plugin.Shapes","Depiction.Plugin.LoadedPolygon"}
                                                                                       };

        public static List<string> DeprecatedList = new List<string> { "Depiction.Plugin.FluidFlow" };

        public const string RawPrototypeName = "RawDefinition";
        public const string ElementFileExtension = "dml";
        //Ug what is the difference for the name scheme?
        public const string DefaultPrototypeLocation = "DefaultDefinitions";
        public const string UserPrototypeLocation = "UserDefinitions";
        public const string LoadedPrototypeLocation = "LoadedDefinitions";

        public const string DefaultPrototypeDescription = "Default Definitions";
        public const string UserPrototypeDescription = "User Definitions";
        public const string LoadedPrototypeDescription = "Loaded Definitions";

        List<IElementPrototype> defaultPrototypes = new List<IElementPrototype>();
        List<IElementPrototype> userPrototypes = new List<IElementPrototype>();
        List<IElementPrototype> loadedPrototypes = new List<IElementPrototype>();//Stragglers picked up from a loaded depiction
        #region Properties

        public ReadOnlyCollection<IElementPrototype> DefaultPrototypes
        {
            get { return defaultPrototypes.AsReadOnly(); }
        }

        public ReadOnlyCollection<IElementPrototype> UserPrototypes
        {
            get { return userPrototypes.AsReadOnly(); }
        }

        public ReadOnlyCollection<IElementPrototype> LoadedDepictionPrototypes
        {
            get { return loadedPrototypes.AsReadOnly(); }
        }

        public ReadOnlyCollection<IElementPrototype> AllDepictionDefinitions
        {
            get
            {
                var newList = new List<IElementPrototype>(defaultPrototypes);
                newList.AddRange(userPrototypes);
                newList.AddRange(loadedPrototypes);
                return newList.AsReadOnly();
            }
        }


        public int PrototypeCount
        {
            get { return defaultPrototypes.Count + userPrototypes.Count + loadedPrototypes.Count; }
        }

        public bool DefaultsSet { get; private set; }
        public bool UsersSet { get; private set; }
        public bool LoadedSet { get; private set; }

        #endregion
        #region Methods

        public void SetDefaultPrototypesFromPath(string path, bool notifyChange)
        {
            if (string.IsNullOrEmpty(path)) return;
            if (!Directory.Exists(path)) return;

            defaultPrototypes = ElementFactory.GetAllPrototypesFromDirectory(path, null);

            DefaultsSet = true;
            if (notifyChange)
            {
                NotifyPrototypeLibraryChange();
            }
        }

        public void SetAddinPrototypesAndIcons(bool notifyChange)
        {
            var addinPath = DepictionAccess.PathService.UserInstalledAddinsDirectoryPath;
            if (!Directory.Exists(addinPath)) return;
            var addinFolders = Directory.GetDirectories(addinPath);
            //TODO make sure prep only picks up preparedness addon
            foreach (var addinFolder in addinFolders)
            {
                var elementFolder = Path.Combine(addinFolder, "Elements");
                if (!Directory.Exists(elementFolder)) continue;
                var iconFolder = Path.Combine(elementFolder, "Icons");
                if (!Directory.Exists(iconFolder))
                {
                    iconFolder = null;
                }
                var addinPrototypes = ElementFactory.GetAllPrototypesFromDirectory(elementFolder, iconFolder);
                if (addinPrototypes != null)
                {
                    foreach (var prototype in addinPrototypes)
                    {
                        if (prototype.PrototypeOrigin.Equals("default", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var parsed = addinFolder.Split('\\');

                            var origin = "Addin";
                            if (parsed.Length > 0)
                            {
                                origin = parsed[parsed.Length - 1];
                            }
                            prototype.PrototypeOrigin = origin;
                        }
                    }
                    defaultPrototypes.AddRange(addinPrototypes);
                }
                if (notifyChange)
                {
                    NotifyPrototypeLibraryChange();
                }
            }
        }

        public void SetUserPrototypesFromPath(string path, bool notifyChange)
        {
            if (string.IsNullOrEmpty(path)) return;
            if (!Directory.Exists(path)) return;
            string userIconImagePath = null;
            if (DepictionAccess.PathService != null)
            {
                userIconImagePath = DepictionAccess.PathService.UserElementIconDirectoryPath;
            }
            userPrototypes = ElementFactory.GetAllPrototypesFromDirectory(path, userIconImagePath);
            foreach (var iProto in userPrototypes)
            {
                var proto = iProto as ElementPrototype;
                if (proto != null)
                {
                    proto.PrototypeOrigin = UserPrototypeDescription;
                }
            }
            UsersSet = true;
            if (notifyChange)
            {
                NotifyPrototypeLibraryChange();
            }
        }
        public void SetLoadedPrototypesFromPath(string path, bool notifyChange)
        {//TODO make sure there is no over lap with default elements
            if (string.IsNullOrEmpty(path)) return;
            if (!Directory.Exists(path)) return;
            var typeComparer = new ElementTypeEqualityComparer<IElementPrototype>();
            var initialLoadedPrototypes = ElementFactory.GetAllPrototypesFromDirectory(path, null);
            loadedPrototypes = initialLoadedPrototypes.Except(defaultPrototypes, typeComparer).ToList();
            foreach (var iProto in userPrototypes)
            {
                var proto = iProto as ElementPrototype;
                if (proto != null)
                {
                    proto.PrototypeOrigin = LoadedPrototypeDescription;
                }
            }
            LoadedSet = true;
            if (notifyChange)
            {
                NotifyPrototypeLibraryChange();
            }
        }

        public bool CanGuessPrototypeFromString(string name)
        {
            return GuessPrototypeFromString(name) != null;
        }

        public IElementPrototype GuessPrototypeFromString(string name)
        {
            if (string.IsNullOrEmpty(name)) return null;
            var typeName = name;
            //this is needed because many of the quickstart do not have updated element types
            if (TypeConversionDictionary122To13.ContainsKey(name))
            {
                typeName = TypeConversionDictionary122To13[name];
            }
            var prototype = GetPrototypeByFullTypeName(typeName);
            if (prototype == null)
            {
                prototype = GetPrototypeByPartOfFullTypeName(typeName);
            }
            if (prototype == null)
            {
                prototype = GetPrototypeByTypeDisplayName(typeName);
            }
            return prototype;
        }
        //Hack method. Well, more of a clever method
        protected IElementPrototype GetPrototypeByPartOfFullTypeName(string type)
        {
            var defaults = DefaultPrototypes.Where(t => t.ElementType.ToLower().Contains(type.ToLower()));
            if (defaults.Count() == 1)
            {
                return defaults.First().DeepClone();
            }
            var users = UserPrototypes.Where(t => t.ElementType.ToLower().Contains(type.ToLower()));
            if (users.Count() == 1)
            {
                return users.First().DeepClone();
            }
            var loaded = LoadedDepictionPrototypes.Where(t => t.ElementType.ToLower().Contains(type.ToLower()));
            if (loaded.Count() == 1)
            {
                return loaded.First().DeepClone();
            }
            return null;
        }
        public IElementPrototype GetPrototypeByFullTypeName(string type)
        {
            var defaults = DefaultPrototypes.Where(t => t.ElementType.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (defaults.Count() == 1)
            {
                return defaults.First().DeepClone();
            }
            var users = UserPrototypes.Where(t => t.ElementType.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (users.Count() == 1)
            {
                return users.First().DeepClone();
            }
            var loaded = LoadedDepictionPrototypes.Where(t => t.ElementType.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (loaded.Count() == 1)
            {
                return loaded.First().DeepClone();
            }
            return null;
        }
        public IElementPrototype GetPrototypeByTypeDisplayName(string type)
        {
            var defaults = DefaultPrototypes.Where(t => t.TypeDisplayName.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (defaults.Count() == 1)
            {
                return defaults.First().DeepClone();
            }
            var users = UserPrototypes.Where(t => t.TypeDisplayName.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (users.Count() == 1)
            {
                return users.First().DeepClone();
            }
            var loaded = LoadedDepictionPrototypes.Where(t => t.TypeDisplayName.Equals(type, StringComparison.InvariantCultureIgnoreCase));
            if (loaded.Count() == 1)
            {
                return loaded.First().DeepClone();
            }
            return null;
        }

        public void SaveElementPrototypeLibraryToPath(string path, bool saveOnlyElementsInCurrentDepiction)
        {
            if (!saveOnlyElementsInCurrentDepiction)
            {
                var defaultPrototypePath = Path.Combine(path, DefaultPrototypeLocation);
                if (!Directory.Exists(defaultPrototypePath))
                {
                    Directory.CreateDirectory(defaultPrototypePath);
                }
                ElementFactory.SavePrototypesToDirectory(defaultPrototypes, defaultPrototypePath);
                var userPrototypePath = Path.Combine(path, UserPrototypeLocation);
                if (!Directory.Exists(userPrototypePath))
                {
                    Directory.CreateDirectory(userPrototypePath);
                }
                ElementFactory.SavePrototypesToDirectory(userPrototypes, userPrototypePath);
                //Not sure if these should even get save . . .
                var loadedPrototypePath = Path.Combine(path, LoadedPrototypeLocation);
                if (!Directory.Exists(loadedPrototypePath))
                {
                    Directory.CreateDirectory(loadedPrototypePath);
                }
                ElementFactory.SavePrototypesToDirectory(loadedPrototypes, loadedPrototypePath);
                return;
            }
            var currentDepiction = DepictionAccess.CurrentDepiction;
            if (currentDepiction == null) return;
            var typeComparer = new ElementTypeEqualityComparer<IDepictionElement>();
            var usedElements = currentDepiction.CompleteElementRepository.AllElements.Distinct(typeComparer).Cast<IBaseDepictionMapType>();
            var allPrototypes = AllDepictionDefinitions.Cast<IBaseDepictionMapType>();


            var usedPrototypes = allPrototypes.Intersect(usedElements, new ElementTypeEqualityComparer<IBaseDepictionMapType>()).Cast<IElementPrototype>();

            //Not sure if these should even get save . . .
            var allDefinitionPath = Path.Combine(path, "");
            if (!Directory.Exists(allDefinitionPath))
            {
                Directory.CreateDirectory(allDefinitionPath);
            }
            ElementFactory.SavePrototypesToDirectory(usedPrototypes, allDefinitionPath);

            var allDefinitionIconPath = Path.Combine(path, "icons");
            if (!Directory.Exists(allDefinitionIconPath))
            {
                Directory.CreateDirectory(allDefinitionIconPath);
            }
            foreach (var proto in usedPrototypes)
            {
                var iconPath = proto.IconPath;
                var image = DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath) as BitmapSource;
                var libName = iconPath.Path;

                if (image != null) BitmapSaveLoadService.SaveBitmap(image, Path.Combine(allDefinitionIconPath, libName));
            }
        }

        public void UpdateElementPrototypeLibraryFromTempLoadedDpnLocation(string path)
        {//USed when loading. Especially wen loading an old file, if the defaults are differetn things shoudl happen
            var typeComparer = new ElementTypeEqualityComparer<IElementPrototype>();

            var defaultPrototypePath = Path.Combine(path, DefaultPrototypeLocation);
            var loadedPrototypePath = Path.Combine(path, LoadedPrototypeLocation);
            var userPrototypePath = Path.Combine(path, UserPrototypeLocation);
            //A change form 1.3.1 to 1.3.2. With 1.3.2 only the elements that are used in the depiction are saved and
            //their icons are stored in the same path.
            var allSavedPrototypes = Path.Combine(path, "");
            var allSavedPrototypesIcons = Path.Combine(path, "icons");

            var allLoadedPrototypes = ElementFactory.GetAllPrototypesFromDirectory(allSavedPrototypes, allSavedPrototypesIcons);
            if (allLoadedPrototypes.Count != 0)
            {
                var allPrototypes = AllDepictionDefinitions;
                var elementNotInLibrary = allLoadedPrototypes.Except(allPrototypes, typeComparer).ToList();
                foreach (var proto in elementNotInLibrary)
                {
                    proto.PrototypeOrigin = LoadedPrototypeDescription;
                }
                loadedPrototypes = elementNotInLibrary;
                return;
            }
            var loadedDefaultPrototypes = ElementFactory.GetAllPrototypesFromDirectory(defaultPrototypePath, null);
#if PREP
           //OK this is bad. If it is prep it will not get the elements unless the depiction was
            //saved with 1.3.2, actually its not as bad as i thought, loaded elements aren't supposed
            //to appear in prep.
#endif
            //User prototypes
            var userIconImagePath = string.Empty;
            if (DepictionAccess.PathService != null)
            {
                userIconImagePath = DepictionAccess.PathService.UserElementIconDirectoryPath;
            }
            var loadedLoadedPrototypes = ElementFactory.GetAllPrototypesFromDirectory(loadedPrototypePath, null);
            //var differentDefaults = loadedDefaultPrototypes.Except(defaultPrototypes, typeComparer);

            var loadedUserPrototypes = ElementFactory.GetAllPrototypesFromDirectory(userPrototypePath, userIconImagePath);
            var userDefinitinsNotInCurrentUserDir = loadedUserPrototypes.Except(userPrototypes, typeComparer);
            var userDefinitionsInCurrentUserDir = loadedUserPrototypes.Intersect(userPrototypes, typeComparer);
            //combine all the user elements, wait, we might not need to since they would already be in
            userPrototypes = userPrototypes.Union(userDefinitionsInCurrentUserDir, typeComparer).ToList();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    break;
                default:
                    var allLoadedDefinitions = userDefinitinsNotInCurrentUserDir.Union(loadedLoadedPrototypes, typeComparer);

                    var differentLoaded = allLoadedDefinitions.Except(userPrototypes, typeComparer).ToList();
                    differentLoaded = differentLoaded.Except(defaultPrototypes, typeComparer).ToList();

                    foreach (var proto in differentLoaded)
                    {
                        proto.PrototypeOrigin = LoadedPrototypeDescription;
                    }
                    loadedPrototypes = differentLoaded;
                    break;
            }
            //#if !PREP
            //            var allLoadedDefinitions = userDefinitinsNotInCurrentUserDir.Union(loadedLoadedPrototypes, typeComparer);
            //
            //            var differentLoaded = allLoadedDefinitions.Except(userPrototypes, typeComparer).ToList();
            //            differentLoaded = differentLoaded.Except(defaultPrototypes, typeComparer).ToList();
            //
            //            foreach (var proto in differentLoaded)
            //            {
            //                proto.PrototypeOrigin = LoadedPrototypeDescription;
            //            }
            //            loadedPrototypes = differentLoaded;
            //#endif
        }

        public bool DeleteElementPrototypeFromLibrary(IElementPrototype prototypeToDelete)
        {
            if (prototypeToDelete.PrototypeOrigin.Equals(DefaultPrototypeDescription)) return false;

            var fileName = prototypeToDelete.DMLFileName;
            if (string.IsNullOrEmpty(fileName))
            {
                userPrototypes.Remove(prototypeToDelete);
            }
            else
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                    userPrototypes.Remove(prototypeToDelete);
                }
                else
                {
                    //userPrototypes.Remove(prototypeToDelete);
                    //the file might not exist, so it was loaded from story it should get removed if there are not
                    //instances of the element in the depiction.
                }

            }
            return false;
        }
        public bool AddElementPrototypeToLibrary(IElementPrototype prototype)
        {
            return AddElementPrototypeToLibrary(prototype, false);
        }
        public bool AddElementPrototypeToLibrary(IElementPrototype prototypeToAdd, bool replaceIfExists)
        {//TODO this can be enhanced by giving more control to where the prototype gets added
            if (prototypeToAdd == null) return false;
            var listToUse = userPrototypes;
            //This part is bad, and should be fixed
            if (prototypeToAdd.PrototypeOrigin.Equals(LoadedPrototypeDescription))
            {
                listToUse = loadedPrototypes;
            }
            if (listToUse.Contains(prototypeToAdd))
            {
                if (replaceIfExists)
                {
                    var removed = listToUse.Remove(prototypeToAdd);
                    if (!removed) return false;
                    listToUse.Add(prototypeToAdd);
                    return true;
                }
                return false;
            }

            listToUse.Add(prototypeToAdd);
            return true;
        }

        public void NotifyPrototypeLibraryChange()
        {
            if (PrototypesUpdated != null)
            {
                PrototypesUpdated.Invoke();
            }
        }

        #endregion

        public IElementPrototype GetPrototypeFromAutoDetect(string elementType, string shapeType)
        {
            if (string.IsNullOrEmpty(elementType)) return null;
            var autoDetect = DepictionStringService.AutoDetectElementString;
            var originalPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(elementType);
            if (originalPrototype == null) return null;//This usually only happens in tests because of an incomplete library
            if (elementType.Equals(autoDetect))
            {
                var geoType = ZoneOfInfluence.GeometryTypeFromIGeometryTypeString(shapeType);

                var autoTypes = "none";
                if (originalPrototype.GetPropertyValue("GeometryTypeMapping", out autoTypes))
                {
                    string[] mappingArray = autoTypes.Split(';');
                    foreach (string map in mappingArray)
                    {
                        string[] geometryToElementMap = map.Split(':');
                        var geometryType =
                            (DepictionGeometryType)Enum.Parse(typeof(DepictionGeometryType), geometryToElementMap[0]);
                        if (geometryType == geoType)
                        {
                            var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(geometryToElementMap[1]);
                            if (prototype != null)
                            {
                                var prop = new DepictionElementProperty("DisplayName", "Name", prototype.TypeDisplayName);
                                prop.Deletable = false;
                                prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);
                            }
                            return prototype;
                        }
                    }
                }
                throw new Exception(String.Format("Did not find expected property: GeometryTypeMapping for element {0}", elementType));

            }
            var displayProp = new DepictionElementProperty("DisplayName", "Name", originalPrototype.TypeDisplayName);
            displayProp.Deletable = false;
            originalPrototype.AddPropertyOrReplaceValueAndAttributes(displayProp, false);

            return originalPrototype;
        }

        #region Equals override for testing
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as ElementPrototypeLibrary;
            if (other == null) return false;
            if (defaultPrototypes.Count != other.defaultPrototypes.Count) return false;
            for (int i = 0; i < defaultPrototypes.Count; i++)
            {
                if (!Equals(defaultPrototypes[i], other.defaultPrototypes[i])) return false;
            }
            if (defaultPrototypes.Count != other.defaultPrototypes.Count) return false;
            for (int i = 0; i < defaultPrototypes.Count; i++)
            {
                if (!Equals(defaultPrototypes[i], other.defaultPrototypes[i])) return false;
            }
            if (userPrototypes.Count != other.userPrototypes.Count) return false;
            for (int i = 0; i < userPrototypes.Count; i++)
            {
                if (!Equals(userPrototypes[i], other.userPrototypes[i])) return false;
            }
            if (loadedPrototypes.Count != other.loadedPrototypes.Count) return false;
            for (int i = 0; i < loadedPrototypes.Count; i++)
            {
                if (!Equals(loadedPrototypes[i], other.loadedPrototypes[i])) return false;
            }
            return true;
        }
        #endregion

    }
}