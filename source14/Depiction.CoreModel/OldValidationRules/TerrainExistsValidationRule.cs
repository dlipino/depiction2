using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.OldValidationRules;

namespace Depiction.CoreModel.OldValidationRules
{
    ///<summary>
    /// A validation rule to check for the existence of elevation in this depiction.
    /// Ick this one should get killed off, but it is needed for dpn porting
    ///</summary>
    public class TerrainExistsValidationRule : IValidationRule
    {
        #region IValidationRule Members

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
        }

        public DepictionValidationResult Validate(object valueToValidate)
        {
            var requiredElement = "Elevation";
            var terrainExists = DepictionAccess.CurrentDepiction != null && DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag(requiredElement) != null;
            if (terrainExists)
                return new DepictionValidationResult { ValidationOutput = ValidationResultValues.Valid };

            var message = string.Format("Must add \"{0}\" data before changing flood height. (see \"Elevation data\" in help).",
                                        requiredElement);
            return new DepictionValidationResult { ValidationOutput = ValidationResultValues.InValid, Message = message };
        }

        public bool IsRuleValid
        {
            get { return true; }
        }

        public ValidationRuleParameter[] Parameters
        {
            get { return new ValidationRuleParameter[0]; }
        }

        #endregion
    }
}