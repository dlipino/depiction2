using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.OldValidationRules
{
    /// <summary>
    /// Validates whether a variable is of a particular type.
    /// </summary>
    public class DataTypeValidationRule : IValidationRule
    {
        #region Variables

        private string errorMessage = string.Empty;
        private Type validType = null;// typeof(int);

        #endregion

        #region Constructor

        ///<summary>
        /// Create a new instance of this class. Needed for saving
        ///</summary>
        public DataTypeValidationRule()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTypeValidationRule"/> class.
        /// </summary>
        /// <param name="TypeToValidate">The type to validate.</param>
        /// <param name="FailErrorMessage">The fail error message.</param>
        public DataTypeValidationRule(Type TypeToValidate, string FailErrorMessage)
        {
            validType = TypeToValidate;
            errorMessage = FailErrorMessage;
        }

        #endregion

        #region Properties
        public bool IsRuleValid
        {
            get
            {
                return validType != null;
            }
        }
        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public ValidationRuleParameter[] Parameters
        {
            get { return new[] { new ValidationRuleParameter(GetSerializableTypeName(validType), validType.GetType()), new ValidationRuleParameter(errorMessage, errorMessage.GetType()) }; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the specified value to validate.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult();

            try
            {
                object convertedValue = DepictionTypeConverter.ChangeType(valueToValidate, validType);
                if (convertedValue is IMeasurement)
                    convertedValue = ((IMeasurement)convertedValue).GetCurrentSystemDefaultScaleValue();
                if (convertedValue.Equals(double.NaN) || convertedValue.Equals(double.PositiveInfinity) || convertedValue.Equals(double.NegativeInfinity) || convertedValue.Equals(float.NaN) || convertedValue.Equals(float.PositiveInfinity) || convertedValue.Equals(float.NegativeInfinity))
                    throw new Exception(String.Format("{0} is not a valid numeric input", convertedValue));
                result.ValidationOutput = ValidationResultValues.Valid;
            }
            catch
            {
                result.ValidationOutput = ValidationResultValues.InValid;
                result.Message = errorMessage;
            }

            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();

            var typeName = reader.ReadElementContentAsString("validTypeName", ns);
            validType = Type.GetType(typeName);
            errorMessage = reader.ReadElementContentAsString("errorMessage", ns);

            reader.ReadEndElement();
        }

        private static string GetSerializableTypeName(Type type)
        {
            // A copy of code in SerializationService
            var assemblyName = type.Assembly.FullName.Split(new[] { ',' });
            var nonSpecificTypeName = string.Format("{0},{1}", type.FullName, assemblyName[0]);

            if (Type.GetType(nonSpecificTypeName) != null)
                return nonSpecificTypeName;
            return type.AssemblyQualifiedName;
        }


        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteElementString("validTypeName", ns, GetSerializableTypeName(validType));
            writer.WriteElementString("errorMessage", ns, errorMessage);
        }
    }
}