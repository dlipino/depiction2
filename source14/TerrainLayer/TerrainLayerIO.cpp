#include "TerrainLayer.h"


namespace DepictionTerrainLayer
{
	//Initialize TerrainLayer from a file
	//
	//Currently supported formats
	//DEM, NED, ADF, GeoTIFF
	//
	bool TerrainLayer::ImportFromFile(System::String^ fnameString, System::String^ coordinateSystemString, System::String^ fileExt, bool useExistingGridSpacing, bool cropGridBeforeProjection)
	{
		
		//Set m_proj somewhere here in this method

		//have to convert fnameString to char*

		char* fnameCharArray = new char[fnameString->Length + 5];
		sprintf(fnameCharArray, "%s", fnameString);

		char* coordinateSystemArray = NULL;
		coordinateSystemArray = new char[coordinateSystemString->Length + 1];
		sprintf(coordinateSystemArray, "%s", coordinateSystemString);

		int fileNameLength = strlen(fnameCharArray);

		char* fileExtArray = new char[fileExt->Length + 1];
		sprintf(fileExtArray, "%s", fileExt);

		bool success = false;

		if (!m_pGrid->HasData())//FIRST TIME LOADING OF AN ELEVATION FILE
		{
			m_pGrid->FreeData();
			delete m_pGrid;
			m_pGrid = new vtElevationGrid();

			if(m_pGrid)
			{
				success = ImportFromVariousFormats(m_pGrid, fnameCharArray, fileExtArray);
				m_pGrid->SetupConversion();
				m_pGrid->CleanUp();
				if(!success)
				{
					delete [] fnameCharArray;
					delete [] coordinateSystemArray;
					delete [] fileExtArray;
					return false;
				}
			}

			int width, height;
			m_pGrid->GetDimensions(width,height);
			m_imageWidth = width;
			m_imageHeight = height;

			m_proj = new vtProjection();
			vtProjection proj = m_pGrid->GetProjection();
			*m_proj = proj;

			//if(!m_proj->IsGeographic())
			{//this is a projection coord system
				//need to reproject it to geographic
				vtElevationGrid* newGrid = m_pGrid;
				vtProjection newProj = newGrid->GetProjection();
				//m_proj->SetGeogCSFromDatum(6326);//WGS84
				m_proj->SetTextDescription("wkt","PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");

				//Add Layer after checking its coordinate system for compatibility
				AddLayerWithCheck(&newGrid);
				m_pGrid = newGrid;

			}
			needRefreshing = true;
			//will have to construct Surface Grid data

			//get the current corner values 
			GetCurrentCornersAndHeightExtents();
		}
		else
		{
			//m_pGrid exists...new elevation data being loaded
			//resample and merge automatically
			vtElevationGrid* m_newGrid = new vtElevationGrid();

			if (m_newGrid)
			{
				success = ImportFromVariousFormats(m_newGrid, fnameCharArray, fileExtArray);
				m_newGrid->SetupConversion();
				m_newGrid->CleanUp();
				if (!success)
				{
					delete [] fnameCharArray;
					delete [] coordinateSystemArray;
					delete [] fileExtArray;				
					return false;
				}
			}

			

			int newWidth, newHeight;
			System::Collections::ArrayList^ cornerPts = gcnew System::Collections::ArrayList();
			GetCorners(cornerPts, false);
			vtProjection oldProj = m_pGrid->GetProjection();
			vtProjection newProj = m_newGrid->GetProjection();
			char *strOld, *strNew;
			oldProj.exportToWkt(&strOld);
			newProj.exportToWkt(&strNew);

			DPoint2 newPt;
			DPoint2 originalPt;
			//transform topleft corner of the cropping rectangle
			//from geog coord system to whatever the new grid is in
			newPt.x = (double)cornerPts[0];
			newPt.y = (double)cornerPts[1];
			originalPt.x = newPt.x;
			originalPt.y = newPt.y;

			if(oldProj != newProj)
				m_newGrid->TransformPoint(&oldProj, &newProj, &newPt);

			cornerPts[0] = newPt.x;
			cornerPts[1] = newPt.y;
			//transform bottom right
			newPt.x = (double)cornerPts[2]; 
			newPt.y = (double)cornerPts[3];
			
			if (oldProj != newProj)
				m_newGrid->TransformPoint(&oldProj, &newProj, &newPt);

			cornerPts[2] = newPt.x;
			cornerPts[3] = newPt.y;

			
			//NOTE: the original grid is in MERCATOR COORDINATE SYSTEM
			

			//Crop the new grid to the extents of the existing grid
			//this is so that LARGE files can be read in and transformed
			//to the GEOG COORD SYSTEM relatively quickly

			//last passed parameter says to use the spacing of the new grid
			if (cropGridBeforeProjection)
			{
				float width = fabs((double)cornerPts[2] - (double)cornerPts[0]);
				float height = fabs((double)cornerPts[3] - (double)cornerPts[1]);
				float tenPercentHeight = .10 * height;
				float tenPercentWidth = .10 * width;
				double topLeftX = (double)cornerPts[0] - tenPercentWidth;
				double topLeftY = (double)cornerPts[1] + tenPercentHeight;
				double bottomRightX = (double)cornerPts[2] + tenPercentWidth;
				double bottomRightY = (double)cornerPts[3] - tenPercentHeight;
				vtElevationGrid* tempGrid= MyCropElevationGrid(m_newGrid,topLeftX, topLeftY, bottomRightX, bottomRightY, &newWidth, &newHeight, USENEWGRIDSPACING);
				if(tempGrid!=NULL)
				{
					m_newGrid->FreeData();
					delete m_newGrid;
					m_newGrid = tempGrid;
				}
			}
			//Add Layer after checking its coordinate system for compatibility
			//this step will also convert the new grid, if necessary, to Mercator

			AddLayerWithCheck(&m_newGrid);

			//resampleelevationgrid might downsample m_newGrid if it's too big
			ResampleElevationGrid(m_newGrid, useExistingGridSpacing);
			needRefreshing = true;

			m_newGrid->FreeData();
			delete m_newGrid;
			//get the current corner values 
			GetCurrentCornersAndHeightExtents();
		}

		//TODO: set m_proj to the projection of the just read elevation data
		delete [] fnameCharArray;
		delete [] coordinateSystemArray;
		delete [] fileExtArray;
		return success;
	}

	bool TerrainLayer::ImportFromFileWithoutExtents(System::String^ fnameString,double north, double south, double east, double west)
	{		
		char* fnameCharArray = new char[fnameString->Length + 5];
		sprintf(fnameCharArray, "%s", fnameString);

		vtElevationGrid* newGrid = new vtElevationGrid();
		bool success = newGrid->LoadWithGDAL(fnameCharArray, NULL);

		//set the corners
		DLine2* corners = new DLine2(4);
		DPoint2 bottomLeft;
		bottomLeft.x = west; 
		bottomLeft.y = south;
		DPoint2 topLeft;
		topLeft.x = west; 
		topLeft.y = north;
		DPoint2 topRight;
		topRight.x = east;
		topRight.y = north;
		DPoint2 bottomRight;
		bottomRight.x = east;
		bottomRight.y = south;

		corners->SetAt(0, bottomLeft);
		corners->SetAt(1, topLeft);
		corners->SetAt(2, topRight);
		corners->SetAt(3, bottomRight);

		newGrid->SetCorners ((const DLine2)*corners);

		DRECT rect;

		rect.top = north;
		rect.bottom = south;
		rect.left = west;
		rect.right = east;
		newGrid->SetEarthExtents(rect);

		vtProjection* proj = new vtProjection();
		proj->SetGeogCSFromDatum(NAD83);//NAD83
		newGrid->SetProjectionOnly(*proj);
		success=newGrid->SaveToGeoTIFF(fnameCharArray);

		delete newGrid;

		return success;
	}

	bool TerrainLayer::ImportFromVariousFormats(vtElevationGrid* m_newGrid, char* fnameCharArray, char* fileExtension)
	{
		bool success = false;

		if (!strcmp(fileExtension, "dem"))
			success = m_newGrid->LoadFromDEM(fnameCharArray, NULL);
		else if (!strcmp(fileExtension, "hgt"))
			success = m_newGrid->LoadFromHGT(fnameCharArray, NULL);
		else if (!strcmp(fileExtension, "bt"))
			success = m_newGrid->LoadFromBT(fnameCharArray, NULL);
		else
			success = m_newGrid->LoadWithGDAL(fnameCharArray, NULL);

		//check to see if the grid has geographic earth extents
		if(success && m_newGrid->GetEarthExtents().IsEmpty())
			success=false;

		return success;
	}

	

	bool TerrainLayer::SaveToGeoTIFF(System::String^ fileName)
	{		
		char* fnameCharArray = new char[fileName->Length+5];
		sprintf(fnameCharArray,"%s",fileName);
		return m_pGrid->SaveToGeoTIFF(fnameCharArray);
	}

	bool TerrainLayer::SaveToBT(System::String^ fileName)
	{		
		char* fnameCharArray = new char[fileName->Length + 5];
		sprintf(fnameCharArray, "%s", fileName);
		return m_pGrid->SaveToBT(fnameCharArray);
	}

	bool TerrainLayer::SaveAsPNG16(System::String^ fileName)
	{
		char* fnameCharArray = new char[fileName->Length + 5];
		sprintf(fnameCharArray, "%s", fileName);
		return m_pGrid->SaveToPNG16(fnameCharArray);
	}

	//
	//Save coverage data to 24bit RGB
	//
	//This colormapped image would represent the raster visualization of any coverage data
	//
	bool TerrainLayer::SaveTo24BitRGB(System::String^ fileName, int quality, System::String^ colormapFileName)
	{
		bool status = true;

		char* fnameCharArray = new char[fileName->Length + 5];
		sprintf(fnameCharArray, "%s", fileName);
		TerrainColorMap *mycmap = new TerrainColorMap();

		char* cnameCharArray = new char[colormapFileName->Length + 5];
		sprintf(cnameCharArray, "%s", colormapFileName);

		if(!mycmap->Load(cnameCharArray))//"C:\\simio\\VTP\\TerrainApps\\Data\\GeoTypical\\default_relative.cmt"))
			mycmap->SetupDefaultColors();

		// Rather than look through the color map for each pixel, pre-build
		//  a color lookup table once - should be faster in nearly all cases.
		std::vector<RGBi> table;
		float fMin, fMax;

		//get min max value of the coverage dataset
		GetCurrentCornersAndHeightExtents();
		fMin = m_minHeight; 
		fMax = m_maxHeight;

		float fRange = fMax - fMin;
		bool bFlat = (fRange < 0.0001f);
		if (bFlat)
		{
			// avoid numeric trouble with flat terrains by growing range
			fMin -= 1;
			fMax += 1;
			fRange = fMax - fMin;
		}

		int iGranularity = 8000;
		mycmap->GenerateColors(table, iGranularity, fMin, fMax);

		vtDIB dib;
		bool success = dib.Create(m_imageWidth, m_imageHeight, 24, false);
		if (!success)
		{//try a smaller sized bitmap for the terrain visualization
			success = dib.Create(m_imageWidth / 2, m_imageHeight / 2, 24, false);
			if (!success)
			{
				success = dib.Create(m_imageWidth / 4, m_imageHeight / 4, 24, false);
			}
			if (!success)
				return false;
		}
		ColorDIBFromTable(&dib, table, fMin, fMax, NULL);
		//if(dib.GetWidth()<3000)//avoid costly shading if image is LARGE
		ShadeQuick(&dib, SHADING_BIAS, true, NULL/*progress_callback*/);

		return dib.WriteJPEG(fnameCharArray, quality, NULL);
	}

}