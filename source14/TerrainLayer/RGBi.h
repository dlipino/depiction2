
/**
 * An RGB class for handling color operations.
 *
 * This one handles colors with integer components (0-255)
 */
class RGBi
{
public:
	RGBi() {}
	RGBi(short _r, short _g, short _b) { r = _r; g = _g; b = _b; }
	RGBi(const class RGBf &v) { *this = v; }

	void Set(short _r, short _g, short _b) { r = _r; g = _g; b = _b; }
	RGBi operator +(const RGBi &v) const { return RGBi(r+v.r, g+v.g, b+v.b); }
	RGBi operator +(const class RGBAi &v) const;
	RGBi operator -(const RGBi &v) const { return RGBi(r-v.r, g-v.g, b-v.b); }
	RGBi operator *(float s) const { return RGBi((short)(r*s), (short)(g*s), (short)(b*s)); }
	RGBi operator /(float s) const { return RGBi((short)(r/s), (short)(g/s), (short)(b/s)); }
	void operator *=(float s) { r=(short)(r*s); g=(short)(g*s); b=(short)(b*s); }
	void operator /=(float s) { r=(short)(r/s); g=(short)(g/s); b=(short)(b/s); }

	void operator +=(const RGBi &v) { r=r+v.r; g=g+v.g; b=b+v.b; }
	void operator +=(const class RGBAi &v);

	void Crop();

	// assignment
	RGBi &operator=(const RGBi &v) { r = v.r; g = v.g; b = v.b; return *this; }
	RGBi &operator=(const class RGBf &v);

	bool operator==(const RGBi &v) const { return (r == v.r && g == v.g && b == v.b); }
	bool operator!=(const RGBi &v) const { return (r != v.r || g != v.g || b != v.b); }

	short r, g, b;
};