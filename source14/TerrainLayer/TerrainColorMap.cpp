#include "stdafx.h"
#include "stdlib.h"
#include "TerrainColorMap.h"




//
// Class implementation: TerrainColorMap
//
TerrainColorMap::TerrainColorMap()
{
	m_bBlend = true;
	m_bRelative = true;
}

/**
 * Add a color entry, keeping the elevation values sorted.
 */
void TerrainColorMap::Add(float elev, const RGBi &color)
{
	float next, previous = -1E9;
	int size = m_elev.size();
	for (int i = 0; i < size+1; i++)
	{
		if (i < size)
			next = m_elev[i];
		else
			next = 1E9;
		if (previous <= elev && elev <= next)
		{
			m_elev.insert(m_elev.begin() + i, elev);
			m_color->insert(m_color->begin() + i, color);
			return;
		}
	}
}

void TerrainColorMap::RemoveAt(int num)
{
	m_elev->erase(m_elev.begin()+num);
	m_color->erase(m_color.begin()+num);
}

int TerrainColorMap::Num() const
{
	return m_elev.size();
}

/**
 * Generate an array of interpolated colors from this TerrainColorMap.
 *
 * \param table An empty table ready to fill with the interpolated colors.
 * \param iTableSize The desired number of elements in the resulting table.
 * \param fMin, fMax The elevation range to interpolate over.
 */
void TerrainColorMap::GenerateColors(std::vector<RGBi> &table, int iTableSize,
							  float fMin, float fMax) const
{
	if (m_color->size() < 2)
		return;

	float fRange = fMax - fMin;
	float step = fRange/iTableSize;

	int current = 0;
	int num = Num();
	RGBi c1, c2;
	float base=0, next, bracket_size=0, fraction;

	if (m_bRelative == true)
	{
		bracket_size = fRange / (num - 1);
		current = -1;
	}

	RGBi c3;
	for (int i = 0; i < iTableSize; i++)
	{
		float elev = fMin + (step * i);
		if (m_bRelative)
		{
			// use regular divisions
			int bracket = (int) ((elev-fMin) / fRange * (num-1));
			if (bracket != current)
			{
				current = bracket;
				base = fMin + bracket * bracket_size;
				c1 = m_color[current];
				c2 = m_color[current+1];
			}
		}
		else
		{
			// use absolute elevations
			while (current < num-1 && elev >= m_elev[current])
			{
				c1 = m_color[current];
				c2 = m_color[current+1];
				base = m_elev[current];
				next = m_elev[current+1];
				bracket_size = next - base;
				current++;
			}
		}
		if (m_bBlend)
		{
			fraction = (elev - base) / bracket_size;
			c3 = c1 * (1-fraction) + c2 * fraction;
		}
		else
			c3 = c1;
		table.push_back(c3);
	}

	// Add one to catch top data
	c3 = table[iTableSize-1];
	table.push_back(c3);
}

