#include "TerrainLayer.h"

namespace DepictionTerrainLayer
{
	int TerrainLayer::GetRow(double lat)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, true);
		DPoint2 pointBottomLeft	= line.GetAt(0);
		IPoint2 point = GetImageCoordinates(pointBottomLeft.x, lat);
		return point.y;
	}

	int TerrainLayer::GetColumn(double lon)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, true);
		DPoint2 pointBottomLeft	= line.GetAt(0);
		IPoint2 point = GetImageCoordinates(lon, pointBottomLeft.y);
		return point.x;
	}

	//
	//Given X, Y in lon/lat, get the image coordinates in the resampled terrain grid
	//
	IPoint2 TerrainLayer::GetImageCoordinates(double x, double y)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		DPoint2 spacing = m_pGrid->GetSpacingD();

		int row, col;

		col = (int)((x - rect.left) / (fabs(rect.left-rect.right)) * (m_imageWidth - 1) + 0.001);
		row = (int)((y - rect.bottom) / (fabs(rect.top-rect.bottom)) * (m_imageHeight - 1) + 0.001);

		if (col < 0) col = 0;
		if (col > m_imageWidth - 1) col = m_imageWidth - 1;

		if (row < 0) row = 0;
		if (row > m_imageHeight - 1) row = m_imageHeight - 1;

		IPoint2 point(col, row);
		return point;
	}

	double TerrainLayer::GetLatitude(int row)
	{
		return GetWorldCoordinates(0, row).y;
	}

	double TerrainLayer::GetLongitude(int col)
	{
		return GetWorldCoordinates(col, 0).x;
	}
	
	///<summary>
	//Given image coordinates in the terrain grid, obtain the world coordinates in lon/lat
	///</summary>
	DPoint2 TerrainLayer::GetWorldCoordinates(int col, int row)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		double lon = (double)(col) / ((double)m_imageWidth - 1) * fabs(rect.left - rect.right) + rect.left;
		double lat = rect.bottom + ((double)row) / ((double)m_imageHeight - 1) * fabs(rect.top - rect.bottom);

		return DPoint2(lon, lat);
	}

	///<summary>
	//Given image coordinates in the terrain grid, obtain the world coordinates in lon/lat
	///</summary>
	DPoint2 TerrainLayer::GetWorldCoordinates(int col, int row, int gridWidth, int gridHeight)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, true);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		double lon = (double)(col) / ((double)gridWidth - 1) * fabs(rect.left - rect.right) + rect.left;
		double lat = rect.bottom + ((double)row) / ((double)gridHeight - 1) * fabs(rect.top - rect.bottom);

		return DPoint2(lon, lat);
	}
	
	void TerrainLayer::ConvertImageToWorldCoordinates(System::Collections::ArrayList^ cList, int offset)
	{
		DPoint2 worldCoords;
		float x, y;
		float currentX = -10000, currentY = -10000;

		//remove

		//for(int i=0;i<cList->Count;i++)
		//{
		//	System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
		//	for(int j=0;j<aList->Count;j++)
		//	{
		//		x = ((System::Drawing::PointF^)aList[j])->X - offset;
		//		y = ((System::Drawing::PointF^)aList[j])->Y - offset;
		//		m_resampledGrid->SetFValue(x,y,200);
		//	}
		//}
		//m_resampledGrid->SaveToBMP("C:\\downloads\\contours.bmp");

		for(int i = 0; i < cList->Count; i++)
		{
			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			for(int j = 0; j < aList->Count; j++)
			{
				x = ((System::Windows::Point^)aList[j])->X - offset;
				y = ((System::Windows::Point^)aList[j])->Y - offset;
				if( x != currentX || y != currentY)
				{
					currentX = x; 
					currentY = y;
					worldCoords = GetWorldCoordinates(x, y, m_imageWidth, m_imageHeight);
					//m_pGrid->SetFValue(x+1,(*m_imageHeight-1)-y+1,200);
					aList->Insert(j, gcnew System::Windows::Point(worldCoords.x, worldCoords.y));
					aList->RemoveAt(j + 1);
				}
				else
				{
					aList->RemoveAt(j);
					j--;
				}
			}
		}
		bool notClosed=false;
		for(int i = 0; i < cList->Count; i++)
		{
			System::Collections::ArrayList^ aList = (System::Collections::ArrayList^)cList[i];
			int count = aList->Count;
			//see if the polygons are closed
			if( ((System::Windows::Point^)aList[0])->X == 
				((System::Windows::Point^)aList[count-1])->X &&
				((System::Windows::Point^)aList[0])->Y == 
				((System::Windows::Point^)aList[count-1])->Y
				)
			{
			}
			else
			{
				notClosed = true;
			}
		}

		//m_pGrid->SaveToPNG16("C:\\downloads\\contour.png");
		//SaveTo24BitRGB("C:\\downloads\\colormap.jpg",90);
	}

	///<summary>
	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns ZERO to enable flood calc
	///</summary>
	float TerrainLayer::GetInterpolatedElevationValue(double x, double y)
	{
		//use GetFilteredValue on elevation grid at a specific point
		DPoint2 point;
		point.x = x;
		point.y = y;
		m_pGrid->TransformPoint(m_projGeo ,m_proj, &point);
		float elevValue = m_pGrid->GetFilteredValue(point);
		if (elevValue == (float)UNKNOWN)
			elevValue = DEFAULTELEVATION;
		else if (elevValue == NODATA)
			elevValue = SHRT_MIN;

		return elevValue;
	}

	///<summary>
	//Within the elevation data bounds, this method returns the non-iterpolated
	//elevation value at a point.
	//if there's no data at a point, it returns the value as NaN
	///</summary>
	float TerrainLayer::GetElevationValue(double x, double y)
	{
		//use GetFilteredValue on elevation grid at a specific point
		DPoint2 point;
		point.x = x;
		point.y = y;
		//use actual grid value
		IPoint2 gridCoords = GetImageCoordinates(x, y);
		float elevValue = m_pGrid->GetElevation(gridCoords.x, gridCoords.y, true);
		//float elevValue = m_pGrid->GetFilteredValue(point);
		if(elevValue == (float)UNKNOWN || elevValue == (float)NODATA)
			elevValue = System::Double::NaN;

		return elevValue;
	}

	///<summary>
	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns ZERO to enable flood calc
	///</summary>
	float TerrainLayer::GetClosestElevationValue(double x, double y)
	{
		//use GetFilteredValue on elevation grid at a specific point
		DPoint2 point;
		point.x = x;
		point.y = y;
		m_pGrid->TransformPoint(m_projGeo , m_proj, &point);
		float elevValue = m_pGrid->GetClosestValue(point);
		//double left, bottom;
		//left = m_pGrid->GetEarthExtentsLeft();
		//bottom = m_pGrid->GetEarthExtentsBottom();
		//double width = m_pGrid->GetEarthExtentsWidth();
		//double height = m_pGrid->GetEarthExtentsHeight();
		//int gridWidth = m_pGrid->GetWidth();
		//int gridHeight = m_pGrid->GetHeight();
		////
		//if(point.x<left || point.y < bottom) elevValue=UNKNOWN;
		//else
		//{
		//	double ixDouble = ((point.x - left) /width * (gridWidth-1)+0.01);
		//	double iyDouble = ((point.y - bottom) / height* (gridHeight-1)+0.01);
		//int ix = (int)ixDouble;
		//int iy = (int)iyDouble;
		//if (ix >= 0 && ix < m_pGrid->GetWidth() && iy >= 0 && iy < m_pGrid->GetHeight())
		//	elevValue = m_pGrid->GetFValue(ix, iy);
		//}

		//
		
		//if(elevValue==(float)UNKNOWN)elevValue= System::Double.NaN;

		return elevValue;
	}

	///<summary>
	//Get the elevation value from row/col grid coordinate
	///</summary>
	float TerrainLayer::GetElevationFromGridCoordinates(int col, int row)
	{
		float elevValue = m_pGrid->GetElevation(col, row, false);
		return elevValue;
	}
	
	///<summary>
	///Return averaged value based on a square kernel
	///</summary>
	float TerrainLayer::GetConvolvedValue(int col, int row, int kernel)
	{
		int width = m_imageWidth;
		int height = m_imageHeight;

		int numNeighbors = 0;
		float avgValue = 0;
		if(kernel < 2) return m_pGrid->GetElevation(col, row);
		float fval = (float)(kernel - 1) / 2;
		int offset = (int)fval;
        for (int i = -offset; i <= offset; i++)
            for (int j = -offset; j <= offset; j++)
            {
                if (col + i >= 0 && col + i < width && row + j >= 0 &&
                    row + j < height)
                {
                    if (i == 0 && j == 0) continue;
					float val = m_pGrid->GetElevation(col + i, row + j);
					if (val != UNKNOWN)
					{
						avgValue += val;
						numNeighbors++;
					}
				}
			}
		if (numNeighbors != 0)
			return avgValue / numNeighbors;
		else 
			return UNKNOWN;
	}
	///<summary>
	//Given a lon/lat value, set the elevation grid value at that point
	///</summary>
	bool TerrainLayer::SetElevationValue(double lon, double lat, float elevValue)
	{
		//from the lon/lat value, get the image grid coordinate
		//

		if (m_pGrid == NULL) return false;

		DLine2 line;
		if(!(m_pGrid->GetCorners(line, true)))
		{
			return false;
		}

		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		int row, col;
		if (m_imageWidth <= 0 || m_imageHeight <= 0) return false;
		
		double doubleCol, doubleRow;
		doubleCol = ((lon - rect.left)/ (fabs(rect.left - rect.right)) * (m_imageWidth - 1)) + 0.0000001;
		doubleRow = ((lat - rect.bottom)/ (fabs(rect.top - rect.bottom)) * (m_imageHeight - 1)) + 0.0000001;
		col = (int)doubleCol;
		row = (int)doubleRow;

		if (col < 0) col = 0;
		if (col > m_imageWidth - 1) col = m_imageWidth-1;

		if (row < 0) row = 0;
		if (row > m_imageHeight - 1) row = m_imageHeight-1;


		m_pGrid->SetFValue(col, row, elevValue);

		return true;
	}
	bool TerrainLayer::SetElevationValue(int col, int row, float elevValue)
	{
		m_pGrid->SetFValue(col, row, elevValue);
		return true;
	}

	///<summary>
	//Given a lon/lat value, Add to the elevation grid value at that point.
	///</summary>
	bool TerrainLayer::AddElevationValue(double lon, double lat, float elevValue)
	{
		//from the lon/lat value, get the image grid coordinate

		if (m_pGrid==NULL) return false;

		DLine2 line;
		if (!(m_pGrid->GetCorners(line, true)))
		{
			return false;
		}

		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		int row, col;
		if (m_imageWidth <= 0 || m_imageHeight <= 0) return false;


		col = (int)((lon-rect.left) / (fabs(rect.left - rect.right)) * (m_imageWidth - 1) + 0.0000001);
		row = (int)((lat-rect.bottom) / (fabs(rect.top - rect.bottom)) * (m_imageHeight - 1) + 0.0000001);

		if (col < 0) col = 0;
		if (col > m_imageWidth - 1) col = m_imageWidth - 1;

		if (row < 0) row=0;
		if (row > m_imageHeight - 1) row = m_imageHeight - 1;

		float heightValue = GetElevationFromGridCoordinates(col, row);
		if (heightValue != NODATA && heightValue != SHRT_MIN)
		{
			m_pGrid->SetFValue(col, row, elevValue + heightValue);
		}

		return true;
	}

	bool TerrainLayer::AddElevationValue(int col, int row, float elevValue)
	{
		float heightValue = GetElevationFromGridCoordinates(col, row);
		if (heightValue != NODATA && heightValue != SHRT_MIN)
		{
			m_pGrid->SetFValue(col, row, elevValue + heightValue);
		}

		return true;
	}
	
	bool TerrainLayer::HasElevation(double x, double y)
	{
		if (m_pGrid == NULL)
			return false;

		if (m_projGeo == NULL) return false;

		DPoint2 point;
		point.x = x;
		point.y = y;
		float elevValue = GetClosestElevationValue(point.x, point.y);
		return (elevValue != (float)UNKNOWN);
	}

	bool TerrainLayer::HasNoData(double x, double y)
	{
		DPoint2 point;
		point.x = x;
		point.y = y;
		float elevValue = GetClosestElevationValue(point.x, point.y);
		return (elevValue == (float)NODATA);
	}

	//Within the elevation data bounds, this method returns the elevation value
	//at a point (interpolated)
	//if there's no data at a point, it returns double.Nan
	//
	float TerrainLayer::GetInterpolatedElevationValueInternal(double x, double y)
	{
		//use GetFilteredValue on elevation grid at a specific point
		
		DPoint2 point;
		point.x = x;
		point.y = y;
		m_pGrid->TransformPoint(m_projGeo, m_proj, &point);
		float elevValue = m_pGrid->GetFilteredValue(point);
		if (elevValue == NODATA)
			elevValue = m_maxHeight + 1;
		if (elevValue == (float)UNKNOWN)
			elevValue = DEFAULTELEVATION;
		return elevValue;
	}

	void TerrainLayer::GetCurrentCornersAndHeightExtents()
	{
		//now, for the height value extents

		//get min max value of the coverage dataset
		m_pGrid->ComputeHeightExtents();

		m_minHeight = SHRT_MAX;
		m_maxHeight = SHRT_MIN;
		float heightValue;
		for (int col = 0; col < m_imageWidth; col++)
		{
			for (int row = 0; row < m_imageHeight; row++)
			{
				heightValue = GetElevationFromGridCoordinates(col, row);
				if (heightValue != NODATA && heightValue != SHRT_MIN)
				{
					//SHRT MAX is 32767
					//SHRT MIN is -32768
					//Some elevation data files, however, have -32767 for no data
					//hence the above check
					if (heightValue > m_maxHeight) m_maxHeight = heightValue;
					if (heightValue < m_minHeight) m_minHeight = heightValue;
				}
			}
		}
	}

	int TerrainLayer::GetWidthInPixels()
	{
		return m_imageWidth;
	}

	int TerrainLayer::GetHeightInPixels()
	{
		return m_imageHeight;
	}

		///<summary>
	//Get the corners of the elevation grid into an array list
	//[0] - top left lon
	//[1] - top left lat
	//[2] - bottom right lon
	//[3] - bottom right lat
	///</summary>
	void TerrainLayer::GetCorners(System::Collections::ArrayList^ cornerPts, bool isGeographic)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, isGeographic);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		//top left
		cornerPts->Add( (System::Double) rect.left);
		cornerPts->Add( (System::Double) rect.top);
		//bottom right
		cornerPts->Add( (System::Double) rect.right);
		cornerPts->Add( (System::Double) rect.bottom);
	}

	void TerrainLayer::SetCorners(vtElevationGrid* newGrid, DLine2* corners)
	{
		DLine2 line;
		newGrid->GetCorners(line, true);
		int size = line.GetSize();

		corners->SetAt(0, line.GetAt(0));
		corners->SetAt(1, line.GetAt(1));
		corners->SetAt(2, line.GetAt(2));
		corners->SetAt(3, line.GetAt(3));
	}

	//
	//Scale height values
	//
	void TerrainLayer::Scale(float scale)
	{
		if (m_pGrid != NULL)
			m_pGrid->Scale(scale, true);
	}

}