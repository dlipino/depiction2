﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.CSVExtension.Models;

namespace Depiction.CSVExtension.ViewModels
{
    public class ImportFromCsvViewModel : DialogViewModelBase
    {
//        public event Action RequestClose;

        public event Action<ImportFromCsvViewModel> RequestAndAddElements;
        #region Variables

        private IEnumerable<string> properties;
        private CSVLocationModeType locationStringMode = CSVLocationModeType.None;
        private CSVElementTypeSelection userSelectedType;
        private bool showEIDOption = false;
        private string fullElementTypeName = DepictionStringService.AutoDetectElementString;

        private static readonly string[] CommonLatitudeAliases = new[] { "lat", "latitude" };
        private static readonly string[] CommonLongitudeAliases = new[] { "lon", "long", "longitude" };
        private static readonly string[] CommonLatitudeLongitudeAliases = new[] { "latlon", "latlong", "latitudelongitude", "position" };
        private static readonly string[] CommonStreetAddressAliases = new[] { "street address", "street" };
        private static readonly string[] CommonCityAliases = new[] { "city" };
        private static readonly string[] CommonStateAliases = new[] { "state" };
        private static readonly string[] CommonZipCodeAliases = new[] { "zipcode", "zip code", "statecode", "zip", "Postal Code", "PostalCode", "Post code", "Postcode" };
        private static readonly string[] CommonCountryAliases = new[] { "country" };
        private static readonly string[] CommonAddressSingleFieldAliases = new[] { "address", "location" };

        #endregion

        #region Properties

        public bool ShowEIDOption
        {
            get { return showEIDOption; }
            set { showEIDOption = value; NotifyPropertyChanged("ShowEIDOption"); }
        }

        public CSVLocationModeType LocationStringMode
        {
            get { return locationStringMode; }
            set { locationStringMode = value; NotifyPropertyChanged("LocationStringMode"); }
        }

        public CSVElementTypeSelection UserTypeSelection
        {
            get { return userSelectedType; }
            set
            {
                userSelectedType = value;
                NotifyPropertyChanged("UserTypeSelection");
            }
        }

        public GeoLocationInfoToPropertyHolder LocationPropertyHolder { get; set; }
        public string Latitude { get { return LocationPropertyHolder.Latitude; } set { LocationPropertyHolder.Latitude = value; } }
        public string Longitude { get { return LocationPropertyHolder.Longitude; } set { LocationPropertyHolder.Longitude = value; } }
        public string LatitudeLongitude { get { return LocationPropertyHolder.LatitudeLongitude; } set { LocationPropertyHolder.LatitudeLongitude = value; } }
        public string StreetAddress { get { return LocationPropertyHolder.StreetAddress; } set { LocationPropertyHolder.StreetAddress = value; } }
        public string City { get { return LocationPropertyHolder.City; } set { LocationPropertyHolder.City = value; } }
        public string State { get { return LocationPropertyHolder.State; } set { LocationPropertyHolder.State = value; } }
        public string ZipCode { get { return LocationPropertyHolder.ZipCode; } set { LocationPropertyHolder.ZipCode = value; } }
        public string Country { get { return LocationPropertyHolder.Country; } set { LocationPropertyHolder.Country = value; } }
        public string AddressSingleField { get { return LocationPropertyHolder.AddressSingleField; } set { LocationPropertyHolder.AddressSingleField = value; } }

        public DataTable DataTable { get; set; }
        public bool CropDataToRegion { get; set; }

        public FileSystemInfo FileInfo { get; set; }
        private IElementPrototype prototype;
        public string FullElementTypeName
        {
            get
            {
                if (userSelectedType.Equals(CSVElementTypeSelection.CSVDefined)) return DepictionStringService.AutoDetectElementString; ;
                return fullElementTypeName;
            }
            private set { fullElementTypeName = value; }
        }

        public IElementPrototype Prototype
        {
            get { return prototype; }
            set
            {
                prototype = value;
                FullElementTypeName = prototype.ElementType;
                NotifyPropertyChanged("Prototype");
            }
        }
        public List<IElementPrototype> AvailableElements { get; private set; }

        public IEnumerable<string> Properties
        {
            get { return properties; }
            private set
            {
                properties = value;
                SetFieldsFromProperties();
                NotifyPropertyChanged("Properties");
            }
        }

        #endregion

        private void SetFieldsFromProperties()
        {
            foreach (var property in Properties)
            {
                if (PropertyIsOneOfThese(property, CommonLatitudeAliases))
                    Latitude = property;
                if (PropertyIsOneOfThese(property, CommonLongitudeAliases))
                    Longitude = property;
                if (PropertyIsOneOfThese(property, CommonLatitudeLongitudeAliases))
                    LatitudeLongitude = property;
                if (PropertyIsOneOfThese(property, CommonStreetAddressAliases))
                    StreetAddress = property;
                if (PropertyIsOneOfThese(property, CommonCityAliases))
                    City = property;
                if (PropertyIsOneOfThese(property, CommonStateAliases))
                    State = property;
                if (PropertyIsOneOfThese(property, CommonZipCodeAliases))
                    ZipCode = property;
                if (PropertyIsOneOfThese(property, CommonCountryAliases))
                    Country = property;
                if (PropertyIsOneOfThese(property, CommonAddressSingleFieldAliases))
                    AddressSingleField = property;
            }
        }

        private static bool PropertyIsOneOfThese(string property, IEnumerable<string> aliases)
        {
            foreach (var alias in aliases)
            {
                if (alias.Equals(property, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        #region COmmands

        private DelegateCommand importCommand;

        public ICommand ImportCommand
        {
            get
            {
                if (importCommand == null)
                {
                    importCommand = new DelegateCommand(ImportFromCsv, ImportFromCsvCanExecute);
                }
                return importCommand;
            }
        }

        private void ImportFromCsv()
        {
//            if (!CropDataToRegion)
//            {
//                var result = DepictionMessageService.FileImportWithUnlimitedAreaWarning();
//                if (result.Equals(MessageBoxResult.No))
//                {
//                    return;
//                }
//            }
            if (RequestAndAddElements != null)
                RequestAndAddElements(this);
//            if (RequestClose != null)
//                RequestClose();
            IsDialogVisible = false;
        }

        private bool ImportFromCsvCanExecute()
        {
            //if (LocationStringMode.Equals(CSVLocationModeType.None)) return false;
            return true;
            //return ((LocationMode & LocationModeType.LatitudeLongitudeSingleField).Equals(LocationModeType.LatitudeLongitudeSingleField) && !string.IsNullOrEmpty(LatitudeLongitude))
            //    || ((LocationMode & LocationModeType.LatitudeLongitudeSeparateFields).Equals(LocationModeType.LatitudeLongitudeSeparateFields) && !string.IsNullOrEmpty(Latitude) && !string.IsNullOrEmpty(Longitude))
            //    || ((LocationMode & LocationModeType.AddressSingleField).Equals(LocationModeType.AddressSingleField) && !string.IsNullOrEmpty(AddressSingleField))
            //    || ((LocationMode & LocationModeType.AddressMultipleFields).Equals(LocationModeType.AddressMultipleFields) && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(State));
        }
        #endregion


        #region constructor

        public ImportFromCsvViewModel(string elementType)
        {
            if (DepictionAccess.ElementLibrary != null)
            {
                LocationPropertyHolder = new GeoLocationInfoToPropertyHolder();
                FullElementTypeName = elementType;
                var list = new List<IElementPrototype>();
                var auto = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(DepictionStringService.AutoDetectElementString);
                list.Add(auto);
                foreach (IElementPrototype elem in DepictionAccess.ElementLibrary.UserPrototypes)
                {
                    if (elem != null) list.Add(elem);
                }
                foreach (IElementPrototype elem in DepictionAccess.ElementLibrary.DefaultPrototypes)
                {
                    if (elem != null) list.Add(elem);
                }
                foreach (IElementPrototype elem in DepictionAccess.ElementLibrary.LoadedDepictionPrototypes)
                {
                    if (elem != null) list.Add(elem);
                }
                AvailableElements = list;
            }
        }

        #endregion

        #region public methods

        public void SetFirstFiveRows(string[] headers, List<string[]> rows)
        {
            var headerWithEmpty = headers.ToList();
            headerWithEmpty.Add("");
            Properties = headerWithEmpty;

            DataTable table = new DataTable();
            foreach (var columnName in headers)
            {
                var dataColumn = new DataColumn(columnName);
                dataColumn.DataType = typeof(string);
                table.Columns.Add(dataColumn);
                if(columnName.Equals("eid",StringComparison.InvariantCultureIgnoreCase))
                {
                    showEIDOption = true;
                }
            }

            foreach (var record in rows)
            {
                var row = table.NewRow();
                for (int i = 0; i < record.Length; i++)
                {
                    row[i] = record[i];
                }
                //foreach (var columnName in Properties)
                //{
                //    object value;
                //    if (record.GetPropertyValue(columnName, out value))
                //        row[columnName] = value;
                //}
                table.Rows.Add(row);
            }

            DataTable = table;
            NotifyPropertyChanged("DataTable");
        }

        #endregion
    }

    public enum CSVLocationModeType
    {
        None = 0x0,
        LatitudeLongitudeSingleField = 0x1,
        LatitudeLongitudeSeparateFields = 0x2,
        AddressSingleField = 0x4,
        AddressMultipleFields = 0x8
    }

    public enum CSVElementTypeSelection
    {
        CSVDefined,
        UserDefined
    }
}