﻿using System;

namespace Depiction.CSVExtension.Exceptions
{
    public class CSVMissingHeaderException : Exception
    {
        public const string CsvFileMissingHeader = "Your file is either missing a header row or is not a CSV file: {0}";

        public CSVMissingHeaderException(string FileName)
            : base(string.Format(CsvFileMissingHeader, FileName))
        {
        }
    }
}