﻿using System;

namespace Depiction.CSVExtension.Exceptions
{
    public class CSVMissingDataException : Exception
    {
        public const string CsvFileMissingData = "Could not find any data in the CSV file: {0}\rDoes this file have a header containing information to geocode the records?";
        
        public CSVMissingDataException(string FileName) : base(string.Format(CsvFileMissingData, FileName)) { }
    }
}