﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.CSVExtension.Exceptions;
using Depiction.CSVExtension.ViewModels;
using Depiction.CSVExtension.Views;
using Depiction.Externals.Csv;

namespace Depiction.CSVExtension.Models
{
    [DepictionDefaultImporterMetadata("CSVImporter", new[] { ".csv" },
        FileTypeInformation = "CSV File reader", DisplayName = "CSV File Importer")]
    public class CSVFileElementImporter : AbstractDepictionDefaultImporter
    {//This works as longs a the reader add in is "recreatd" every time it gets called. If it uses the same one
        #region Implementation of IDepictionDefaultImporter

        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            if (elementLocation == null) return;
            var fileName = elementLocation.ToString();
            ImportFromCsvViewModel importFromCsvViewModel = new ImportFromCsvViewModel(defaultElementType);
            //importFromCsvViewModel.Properties = FindHeaders(csvFile);
            
            string[] headers;
            var rows = FindFirstFiveRowsAndHeaders(fileName, out headers);
            importFromCsvViewModel.SetFirstFiveRows(headers, rows);
            importFromCsvViewModel.FileInfo = new FileInfo(fileName);
            //hack 
            if (depictionRegion == null)
            {
                importFromCsvViewModel.CropDataToRegion = false;
            }
            else
            {
                importFromCsvViewModel.CropDataToRegion = true;
            }

            if (headers.Contains("Position"))
            {
                importFromCsvViewModel.LocationStringMode = CSVLocationModeType.LatitudeLongitudeSingleField;
            }
            else if (headers.Contains("Latitude") && headers.Contains("Longitude"))
            {
                importFromCsvViewModel.LocationStringMode = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            }

            // var prototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(defaultElementType, "Point");

            var prototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(defaultElementType);
            importFromCsvViewModel.Prototype = prototype;
            if (defaultElementType.Equals(DepictionStringService.AutoDetectElementString))
            {
                importFromCsvViewModel.UserTypeSelection = CSVElementTypeSelection.CSVDefined;
            }
            else
            {
                importFromCsvViewModel.UserTypeSelection = CSVElementTypeSelection.UserDefined;
            }

            ImportFromCsvView view = new ImportFromCsvView();

            view.DataContext = importFromCsvViewModel;
            importFromCsvViewModel.RequestAndAddElements += importFromCsvViewModel_RequestAndAddElements;
            var result = view.ShowDialog();
            //until the conversion from customcontrolframe is complete
//            importFromCsvViewModel_RequestAndAddElements(importFromCsvViewModel);
//            var window = (Application.Current.MainWindow as IDepictionMainWindow);
//            if (window == null) return;
//            window.AddContentControlToMapCanvas(view);
//            importFromCsvViewModel.IsDialogVisible = true;
//            view.CenterWidthAndGivenTop(100);
        }

        #endregion


        void importFromCsvViewModel_RequestAndAddElements(ImportFromCsvViewModel csvImporterViewModel)
        {
            var name = string.Format("CSV file reader. File : {0}", csvImporterViewModel.FileInfo.Name);
            var csvReadingService = new CSVFileReadingService();

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(csvReadingService);
            csvReadingService.UpdateStatusReport(name);
            var parameters = new Dictionary<string, object>();
            if (DepictionAccess.CurrentDepiction != null)
            {
                parameters.Add("Area", DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds);
            }
            parameters.Add("ViewModel", csvImporterViewModel);
            csvReadingService.StartBackgroundService(parameters);
        }

        public List<string[]> FindFirstFiveRowsAndHeaders(string csvFile, out string[] headers)
        {
            List<string[]> firstFiveRows = new List<string[]>();
           
            try
            {
                using (var streamReader = new StreamReader(new FileStream(csvFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    using (var reader = new CsvReader(streamReader, true){MissingFieldAction = MissingFieldAction.ReplaceByEmpty})
                    {
                        headers = reader.GetFieldHeaders();
                        if (headers == null || headers.Length == 0)
                            throw new CSVMissingHeaderException(csvFile);

                        MissingFieldCsvException missingFieldCsvException = null;
                        int missingFieldsCount = 0;
                        int i = 0;
                        while (reader.ReadNextRecord() && i < 5)
                        {
                            try
                            {
                                string[] row = new string[headers.Length];
                                for (int j = 0; j < headers.Length; j++)
                                {
                                    row[j] = reader[j].Trim();
                                }
                                firstFiveRows.Add(row);
                            }
                            catch (MissingFieldCsvException ex)
                            {
                                missingFieldCsvException = ex;
                                missingFieldsCount++;
                            }
                            catch (MalformedCsvException ex)
                            {
                                DepictionAccess.NotificationService.DisplayMessageString(
                                    string.Format("Problem reading a line from csv file \"{0}\":\n{1}", csvFile,
                                                  ex.Message));
                            }
                            i++;
                        }
                        if (missingFieldCsvException != null)
                        {
                            DepictionAccess.NotificationService.DisplayMessageString(
                                string.Format("{0} records in the file \"{1}\" are missing fields:\n{2}",
                                              missingFieldsCount, csvFile, missingFieldCsvException.Message));
                        }
                    }
                }
            }catch(Exception ex)
            {
                headers = null;
                DepictionAccess.NotificationService.DisplayMessageString(
                                string.Format("File error : {0}", ex.Message));
                return null;
            }

            return firstFiveRows;
        }
    }
}