using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.DepictionComparers;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.MVVM;

namespace Depiction.CSVExtension.Models.Exporters
{
    [DepictionElementExporterMetadata("DepictionCSVWriter", new[] { ".csv" }, 
        FileTypeInformation = "CSV file", DisplayName = "Depiction CSV writer")]
    public class DepictionCSVExporter : IDepictionElementExporter
    {
        #region Implementation of IDepictionElementFileWriter

        public void WriteElements(string fileName, IEnumerable<IDepictionElement> elementList, bool useBackgroundThread)
        {
            var csvWriter = new DepictionCSVWriterService();
            if(useBackgroundThread)
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("FileName", fileName);
                parameters.Add("ElementList", elementList);
                var name = string.Format("Writing CSV file : {0}", Path.GetFileName(fileName));
                
                //Export with zoi's?
                var askForZOI = MessageBoxResult.No;
                foreach(var element in elementList)
                {
                    if(!element.ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point))
                    {
                        askForZOI = DepictionAccess.NotificationService.DisplayInquiryDialog("Include Zone of Influence in export?\n" +
                        "(Note: Complex ZOIs in a CSV file can cause problems when loaded into spreadsheet software.)",
                                                                        "Export with ZOI", MessageBoxButton.YesNo);
                        break;
                    }
                }

                if (askForZOI.Equals(MessageBoxResult.No))
                {
                    parameters.Add("ExportZOI", false);
                }
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(csvWriter);

                csvWriter.UpdateStatusReport(name);
                csvWriter.StartBackgroundService(parameters);
                
            }else
            {
                csvWriter.DoWriteElements(fileName, elementList,true);
            }
        }

        #endregion

        public object AddonMainView
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }

        public string AddonConfigViewText { get { return null; } }

        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }

        public void ExportElements(object location, IEnumerable<IDepictionElement> elements)
        {
            if (location == null) return;
            var fileName = location.ToString();
            WriteElements(fileName, elements, true);
        }
    }

    public class DepictionCSVWriterService : BaseDepictionBackgroundThreadOperation
    {
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var fileName = parameters["FileName"].ToString();
            var elementList = parameters["ElementList"] as IEnumerable<IDepictionElement>;
            var exportZOIString = "ExportZOI";
            var exportZOI = true;
            if(parameters.ContainsKey(exportZOIString))
            {
                exportZOI = (bool) parameters["ExportZOI"];
            }
            DoWriteElements(fileName, elementList,exportZOI);
            return null;
        }

        protected override void ServiceComplete(object args)
        {

        }

        #endregion

        #region stuff that actually does the writing

        public void DoWriteElements(string fileName, IEnumerable<IDepictionElement> elementList,bool exportZOI)
        {
            if (elementList == null) return;
            if (GetExportableElementCount(elementList) <= 0) return;

            try
            {
                using (var writer = File.CreateText(fileName))
                {
                    var builder = new StringBuilder();
                    var properties = AllProperties(elementList);

                    //Prop names to ignore
                    //Author Description DataCategory VisualType IconPath
                    var propToNotExport = new List<string> { "Author", "Description", "DataCategory", "VisualType", "IconPath" };
                    //write the header
                    foreach (var property in properties)
                    {
                        if (!propToNotExport.Contains(property.InternalName))
                        {
                            builder.Append(property.InternalName);
                            builder.Append(",");
                        }
                    }
                    builder.Append("ElementType");
                    if(exportZOI) builder.Append(",ZOI");
                    builder.AppendLine();

                    var comparer = new PropertyComparers.InternalPropertyNameComparer<IElementProperty>();
                    //write the elements
                    int count = 0;
                    var total = elementList.Count();
                    foreach (var element in elementList)
                    {
                        count++;
                        if (ServiceStopRequested) break;
                        UpdateStatusReport(string.Format("Writing element {0} of {1}", count, total));
                        foreach (var property in properties)
                        {
                            if (!propToNotExport.Contains(property.InternalName))
                            {
                                if (element.OrderedCustomProperties.Contains(property, comparer))
                                {
                                    object objectVal;
                                    if (element.GetPropertyValue(property.InternalName, out objectVal))
                                    {
                                        var val = objectVal.ToString();
                                        if (val.Contains(","))
                                            val = "\"" + val + "\"";
                                        builder.Append(val);
                                    }
                                }
                                builder.Append(",");
                            }
                        }
                        builder.Append(element.ElementType);
                        if(exportZOI) builder.Append(",\"" + element.ZoneOfInfluence + "\"");
                        builder.AppendLine();
                    }
                    if (!ServiceStopRequested)
                    {
                        writer.Write(builder.ToString());
                        writer.Flush();
                    }
                    else
                    {
                        writer.Close();//Does this close automatically?
                    }
                }
                if (ServiceStopRequested)
                {
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (IOException e)
            {
                var message = new DepictionMessage(string.Format("Could not export CSV elements to {0} : {1}", fileName, e));
                DepictionAccess.NotificationService.DisplayMessage(message);
            }
        }
        #endregion

        #region private helpers, also older stuff

        private static bool CanExportType(DepictionGeometryType geomType)
        {
            switch (geomType)
            {
                case DepictionGeometryType.Point:
                case DepictionGeometryType.MultiPoint:
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    return true;
            }
            return false;
        }
        private static IList<IElementProperty> AllProperties(IEnumerable<IDepictionElement> elements)
        {
            var propComparer = new PropertyComparers.PropertyNameAndTypeComparer<IElementProperty>();

            List<IElementProperty> properties = null;
            foreach (var element in elements)
            {
                var elementProperties = element.OrderedCustomProperties.Where(p => p != null).ToList();

                List<IElementProperty> properties1 = properties;
                if (properties == null)
                    properties = elementProperties;
                else
                    properties = properties1.Union(elementProperties, propComparer).ToList();
            }
            return properties;
        }

        private static int GetExportableElementCount(IEnumerable<IDepictionElement> elementList)
        {
            var cnt = 0;
            foreach (var elem in elementList)
            {
                if (elem.ZoneOfInfluence != null && CanExportType(elem.ZoneOfInfluence.DepictionGeometryType))
                    cnt++;
            }
            return cnt;
        }
        #endregion
    }
}