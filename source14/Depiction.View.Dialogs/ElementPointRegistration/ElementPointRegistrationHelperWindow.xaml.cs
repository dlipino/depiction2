﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModels.PrototypeViewModels;
using Depiction.ViewModels.ViewModels.MapControlDialogViewModels;

namespace Depiction.View.Dialogs.ElementPointRegistration
{
    /// <summary>
    /// Interaction logic for ElementPointRegistrationHelperWindow.xaml
    /// </summary>
    public partial class ElementPointRegistrationHelperWindow
    {
        public static RoutedUICommand GeoLocateElementWithString = new RoutedUICommand("Geo-locate element", "GeoLocateWithString", typeof(ElementPointRegistrationHelperWindow));
        #region dep props
        public bool CenterOnElementAfterGeoLocate
        {
            get { return (bool)GetValue(CenterOnElementAfterGeoLocateProperty); }
            set { SetValue(CenterOnElementAfterGeoLocateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CenterOnElementAfterGeoLocate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CenterOnElementAfterGeoLocateProperty =
            DependencyProperty.Register("CenterOnElementAfterGeoLocate", typeof(bool), typeof(ElementPointRegistrationHelperWindow), new UIPropertyMetadata(false));



        public string LocationTextToGeolocate
        {
            get { return (string)GetValue(LocationTextStringProperty); }
            set { SetValue(LocationTextStringProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LocationTextString.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LocationTextStringProperty =
            DependencyProperty.Register("LocationTextString", typeof(string), typeof(ElementPointRegistrationHelperWindow), new UIPropertyMetadata(string.Empty));
        #endregion

        public ElementPointRegistrationHelperWindow()
        {
            InitializeComponent();
            var bindings = CommandBindings;
            var commandBinding = new CommandBinding(GeoLocateElementWithString, GeolocateUsingDataContext);
            bindings.Add(commandBinding);
        }
        private void GeolocateUsingDataContext(object sender, ExecutedRoutedEventArgs e)
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            //if (dc == null) return;
            //Console.WriteLine(LocationTextToGeolocate);
            var elementVM = Content as ElementPrototypeViewModel;
            var element = elementVM.Model as IDepictionElement;
            var geocoder = AddContentDialogVM.GeocodeSingleElement(null, LocationTextToGeolocate, element, dc, null, true);
            //            var geocoder = AddContentDialogVM.GeocodeSingleElement(null, LocationTextToGeolocate, element, dc, null, CenterOnElementAfterGeoLocate);
            if (geocoder == null)
            {
                if (dc != null)
                {
                    dc.EndGeoLocateElementCommand.Execute(null);
                }
                return;
            }
            geocoder.RunWorkerCompleted += delegate(object threadSender, RunWorkerCompletedEventArgs eThread)
            {
                if (eThread.Result is bool)
                {
                    var boolResult = (bool)eThread.Result;
                    if (!boolResult)
                    {
                        return;
                    }
                }
                if (eThread.Result != null)
                {
                    if (dc != null)
                    {
                        dc.EndGeoLocateElementCommand.Execute(null);
                    }
                }
            };
        }

        protected override void SoftCloseWindow()
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc != null) { dc.EndGeoLocateElementCommand.Execute(null); }
            base.SoftCloseWindow();
        }
    }
}
