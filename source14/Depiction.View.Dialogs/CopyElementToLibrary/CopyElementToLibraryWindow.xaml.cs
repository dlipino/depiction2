﻿using System.Globalization;
using System.IO;
using System.Windows.Controls;

namespace Depiction.View.Dialogs.CopyElementToLibrary
{
    /// <summary>
    /// Interaction logic for CopyElementToLibraryWindow.xaml
    /// </summary>
    public partial class CopyElementToLibraryWindow 
    {
        public CopyElementToLibraryWindow()
        {
            InitializeComponent();
        }
    }
    public class ElementNameValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var stringValue = (string)value;
            char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
            var found = stringValue.IndexOfAny(invalidFileNameChars);
            if (found > -1)
            {
                return new ValidationResult(false, string.Concat("Element name may not contain any of the characters ", @"\ / : * ? ", "\" ", @"< > |"));
            }

            return new ValidationResult(true, null);
        }
    }
}
