﻿using System.Windows;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for DisplayContentControlFrame.xaml
    /// </summary>
    public partial class DisplayContentDialog 
    {
        public DisplayContentDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            AssociatedHelpFile = "displayContent.html#top";
            SetLocationToStoredValue();
        }
        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.DisplayContentDialogRestoreBounds = RestoreBounds;
            }
        }

        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.DisplayContentDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.DisplayContentDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                Width = bounds.Width;
                Height = bounds.Height;
            }
            base.SetLocationToStoredValue();
        }
    }
}