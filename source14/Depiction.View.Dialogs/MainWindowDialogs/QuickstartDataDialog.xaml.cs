﻿namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for QuickstartDataDialog.xaml
    /// </summary>
    public partial class QuickstartDataDialog
    {
        public QuickstartDataDialog()
        {
            InitializeComponent();
            AssociatedHelpFile = "getting-started.html#quickstartData";
        }
    }
}
