﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.DialogBases;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class UpdateDialog
    {
        public bool doUpdate;
        public UpdateDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            AssociatedHelpFile = "toolsMenu.html#updating";
        }

        public bool ConfirmDepictionUpdate()
        {
            var title = "Continue update?";
            var message = string.Format("Starting an update will exit {0}. Continue?",
                                        DepictionAccess.ProductInformation.ProductName);
            var result = DepictionMessageBox.ShowDialog(message, title, MessageBoxButton.YesNo);
            if (result.Equals(MessageBoxResult.Yes)) return true;
            return false;
        }

        public bool RunDepictionUpdaterClient()
        {
            bool bReturn = false;//Since update is not a modal dialog give the user one chance to change mind before leaving program.

            try
            {
                Process UpdateProcess = new Process();
                UpdateProcess.StartInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("{0}Updater.exe", DepictionAccess.ProductInformation.ProductName));
                UpdateProcess.StartInfo.CreateNoWindow = false;
                UpdateProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                //The Start method returns true on success and
                //false on failure
                bReturn = UpdateProcess.Start();
            }
            catch (Win32Exception)
            {
                var appWin = Application.Current.MainWindow;
                var message = string.Format("Could not find {0} update program.", DepictionAccess.ProductInformation.ProductName);
                var title = string.Format("Error finding {0} Updater", DepictionAccess.ProductInformation.ProductName);
                if(appWin == null)
                {
                    DepictionMessageBox.ShowDialog(message, title);
                }else
                {
                    DepictionMessageBox.ShowDialog(appWin, message, title);
                }
                //An error has occurred
                bReturn = false;
            }
            return bReturn;
        }

        private void RunUpdate_Handler(object sender, RoutedEventArgs e)
        {
            if (!ConfirmDepictionUpdate()) return;
            if(RunDepictionUpdaterClient())
            {
                Application.Current.MainWindow.Close();
            }
        }
    }
}