﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class AboutDialog
    {
        public AboutDialog()
        {
            InitializeComponent();
            IsVisibleChanged += AboutDialog_IsVisibleChanged;
        }

        private void AboutDialog_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(true))
            {
                aboutDocument.FitToWidth();
            }
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }
    }
}