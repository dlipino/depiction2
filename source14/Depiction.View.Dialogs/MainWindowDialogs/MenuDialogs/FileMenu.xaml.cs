using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenu.xaml
    /// </summary>
    public partial class FileMenu
    {
        public FileMenu()
        {
            InitializeComponent();
            SetLocationToStoredValue();
            //Sort of hacky to connect to some of the application commands from the main window
            newButton.CommandTarget = Application.Current.MainWindow;
            openButton.CommandTarget = Application.Current.MainWindow;
            saveAsButton.CommandTarget = Application.Current.MainWindow;
            saveButton.CommandTarget = Application.Current.MainWindow;
            printButton.CommandTarget = Application.Current.MainWindow;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    buttonPanel.Children.Remove(showFileInfoButton);
                    break;
                case ProductInformationBase.Reader:
                    buttonPanel.Children.Remove(newButton);
                    buttonPanel.Children.Remove(saveAsButton);
                    buttonPanel.Children.Remove(saveButton);
                    buttonPanel.Children.Remove(printButton);
                    break;
                case ProductInformationBase.DepictionRW:
                    buttonPanel.Children.Remove(printButton);
                    break;
            }
        }

        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.FileDialogRestoreBounds = RestoreBounds;
            }
            base.LocationOrSizeChanged_Updated();
        }
        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.FileDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.FileDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                //                Width = bounds.Width;
                //                Height = bounds.Height;
            }
        }
    }
}