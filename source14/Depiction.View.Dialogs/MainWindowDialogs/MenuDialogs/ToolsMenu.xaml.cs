using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenu.xaml
    /// </summary>
    public partial class ToolsMenu
    {
        public ToolsMenu()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    ButtonPanel.Children.Remove(ElementCreateButton);
                    ButtonPanel.Children.Remove(AddonButton);
                    ButtonPanel.Children.Remove(InteractionsButton);
                    break;
            }
            SetLocationToStoredValue();
        }
        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.ToolsDialogRestoreBounds = RestoreBounds;
            }
            base.LocationOrSizeChanged_Updated();
        }
        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.ToolsDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                var bounds = Settings.Default.ToolsDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                //                Width = bounds.Width;
                //                Height = bounds.Height;

            }
            base.SetLocationToStoredValue();
        }
    }
}