using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs
{
    /// <summary>
    /// Interaction logic for ElementInfo.xaml
    /// </summary>
    public partial class DepictionHelperMenuDialog
    {
        public DepictionHelperMenuDialog()
        {
            InitializeComponent();
            SetLocationToStoredValue();
            buttonsStackPanel.Children.Remove(updateButton);
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    buttonsStackPanel.Children.Remove(tutorialsButton);
                    break;
                case ProductInformationBase.Reader:
                    buttonsStackPanel.Children.Remove(tutorialsButton);
                    buttonsStackPanel.Children.Remove(licensingButton);
                    buttonsStackPanel.Children.Remove(updateButton);
                    break;
                case ProductInformationBase.DepictionRW:
                     buttonsStackPanel.Children.Remove(tutorialsButton);
                    buttonsStackPanel.Children.Remove(licensingButton);
                    break;
                default:
//                    buttonsStackPanel.Children.Remove(updateButton);
                    break;
            }
        }
        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.HelpDialogRestoreBounds = RestoreBounds;
            }
            base.LocationOrSizeChanged_Updated();
        }
        public override void SetLocationToStoredValue()
        {

            if (!Settings.Default.HelpDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                var bounds = Settings.Default.HelpDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                //                Width = bounds.Width;
                //                Height = bounds.Height;
            }
            base.SetLocationToStoredValue();
        }
    }
}