using Depiction.ViewModels.ViewModels.DialogViewModels;

namespace Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs
{
    public partial class OptionsDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsDialog"/> class.
        /// </summary>
        public OptionsDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            AssociatedHelpFile = "toolsMenu.html#settings";
            DataContextChanged += OptionsDialog_DataContextChanged;
        }

        void OptionsDialog_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            var newDC = e.NewValue as OptionsDialogVM;
            if (newDC == null) return;
            proxyPassword.Password = newDC.ProxyPassword;
        }

        void proxyPassword_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            var dc = DataContext as OptionsDialogVM;
            if (dc == null) return;
            dc.ProxyPassword = proxyPassword.Password;
        }
    }
}