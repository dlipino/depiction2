﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.HelperClasses;
using Depiction.View.Dialogs.HelperClasses.ElementPropertyStyles;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ElementPropertiesDialog.xaml
    /// </summary>
    public partial class ElementPropertiesDialog
    {
        static public RoutedUICommand DeleteElementsCommand = new RoutedUICommand("Delete all", "DeleteElementsCommand", typeof(ElementPropertiesDialog));
        private GridViewColumnHeader _CurSortCol = null;
        private SortAdorner _CurAdorner = null;
        private GridViewColumnHeader _HoverCurSortCol = null;
        private SortAdorner _HoverCurAdorner = null;

        public ElementPropertiesDialog()
        {
            InitializeComponent();
            // This transform group is required for the storyboard to work right
            var transformGroup = new TransformGroup();
            transformGroup.Children.Add(new ScaleTransform { ScaleX = 1, ScaleY = 1, CenterX = .5, CenterY = .5 });
            transformGroup.Children.Add(new SkewTransform { AngleX = 0, AngleY = 0 });
            transformGroup.Children.Add(new RotateTransform { Angle = 0 });
            transformGroup.Children.Add(new TranslateTransform { X = 0, Y = 0 });
            RenderTransform = transformGroup;
            //End transformgroup
            ShowInfoButton = true;
            AssociatedHelpFile = "elementInformation.html";
            var commandBinding =
                new CommandBinding(ElementPropertiesControlResourceLibrary.ChangeButtonBackgroundCommand,
                                   ElementPropertiesControlResourceLibrary.ChangeButtonBackground);
            CommandBindings.Add(commandBinding);
            commandBinding = new CommandBinding(DeleteElementsCommand, DeleteElements, CanDeleteElements);
            CommandBindings.Add(commandBinding);
            DataContextChanged += ElementPropertiesControl_DataContextChanged;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    PropertyMainPanel.Children.Remove(PropertyAddPanel);
                    btnStackPanel.Children.Remove(btnExportFile);
                    btnStackPanel.Children.Remove(btnSendToEmail);
                    //ElementPropDockPanel.Children.Remove(ElementNameStackPanel);
                    tabControl.Items.Remove(colorizeTab);
                    tabControl.Items.Remove(setTagTab);
                    HoverTextPropertiesPanel.Children.Remove(useEnhancedLabelCheckbox);
                    ElementPropDockPanel.Children.Remove(elementPropertiesListView);
                    break;
                case ProductInformationBase.DepictionRW:
                    PropertyMainPanel.Children.Remove(PropertyAddPanel);
                    btnStackPanel.Children.Remove(btnExportFile);
                    btnStackPanel.Children.Remove(btnSendToEmail);
                    //ElementPropDockPanel.Children.Remove(ElementNameStackPanel);
                    tabControl.Items.Remove(colorizeTab);
                    tabControl.Items.Remove(setTagTab);
                    HoverTextPropertiesPanel.Children.Remove(useEnhancedLabelCheckbox);
                    ElementPropDockPanel.Children.Remove(elementPropertiesListView);
                    break;
                default:
                    commandBinding = new CommandBinding(ElementPropertiesControlResourceLibrary.SelectElementIconCommand,
                                                ElementPropertiesControlResourceLibrary.SelectElementIcon);
                    CommandBindings.Add(commandBinding);
                    ElementPropDockPanel.Children.Remove(elementPropertiesSimpleListView);
                    break;
            }
        }

        //Doesn't work well enough yet
        //        private Storyboard burpStoryboard;
        //        public void DoFocusVisualStoryboard()
        //        {
        //            if (burpStoryboard == null)
        //            {
        //                if(string.IsNullOrEmpty(Name))
        //                {
        //                    Name = "elementPropertyDialog";
        //                }
        //                RegisterName(Name, this);
        //                var duration = new Duration(TimeSpan.FromMilliseconds(75));
        //                var myDoubleAnimationX = new DoubleAnimation
        //                {
        //                    From = 1.0,
        //                    To = 1.05,
        //                    Duration = duration,
        //                    AutoReverse = true
        //                };
        //                var myDoubleAnimationY = new DoubleAnimation
        //                {
        //                    From = 1.0,
        //                    To = 1.05,
        //                    Duration = duration,
        //                    AutoReverse = true
        //                };
        //
        //                // Create the storyboard.
        //                burpStoryboard = new Storyboard();
        //                burpStoryboard.Children.Add(myDoubleAnimationX);
        //                burpStoryboard.Children.Add(myDoubleAnimationY);
        //                Storyboard.SetTargetName(myDoubleAnimationX, Name);
        //                Storyboard.SetTargetName(myDoubleAnimationY, Name);
        //
        //                var myPropertyPathX =
        //                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)");
        //                Storyboard.SetTargetProperty(myDoubleAnimationX, myPropertyPathX);
        //                var myPropertyPathY =
        //                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)");
        //                Storyboard.SetTargetProperty(myDoubleAnimationY, myPropertyPathY);
        //            }
        //            burpStoryboard.Begin(this, false);
        //        }

        #region event/command handlers

        override protected void SoftCloseWindow()
        {

            var mapVM = Tag as DepictionEnhancedMapViewModel;
            var dataContext = DataContext as ElementPropertyDialogContentVM;
            if (dataContext != null)
            {
                if (mapVM != null)
                {
                    var previewElement = dataContext.PreviewElement;
                    if (previewElement != null)
                    {
                        if (previewElement.IsElementPreview)
                            mapVM.RemoveElementPreviewVM(previewElement);
                    }
                }
            }
            DataContext = null;
            Tag = null;
            DataContextChanged -= ElementPropertiesControl_DataContextChanged;
            Close();
        }

        #endregion
        void ElementPropertiesControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dcOld = e.OldValue as ElementPropertyDialogContentVM;

            var dc = e.NewValue as ElementPropertyDialogContentVM;
            if (dcOld != null)
            {
                dcOld.Dispose();
            }
            if (dc == null) return;
            if (dc.OriginalElementIds.Count == 1)
            {
                DeleteElementsCommand.Text = "Delete";
                multipeElementText.Visibility = Visibility.Collapsed;
            }
            else
            {
                DeleteElementsCommand.Text = "Delete all";
            }
        }

        #region Delete commands
        private void DeleteElements(object sender, ExecutedRoutedEventArgs e)
        {
            var parentDC = Tag as DepictionMapAndMenuViewModel;
            if (parentDC == null) return;
            parentDC.DeleteElementCommand.Execute(e.Parameter);
            var dialog = sender as ElementPropertiesDialog;
            if (dialog != null)
            {
                dialog.Close();
            }
        }

        private void CanDeleteElements(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            var dc = DataContext as ElementPropertyDialogContentVM;
            if (dc == null) return;
            e.CanExecute = true;
        }

        #endregion
        #region private helpers

        private void ApplyChanges(ElementPropertyDialogContentVM dataContext, DepictionEnhancedMapViewModel mapVM)
        {
            if (dataContext != null)
            {
                if (mapVM != null)
                {
                    var previewElement = dataContext.PreviewElement;
                    if (previewElement != null)
                    {
                        if (previewElement.IsElementPreview)
                            mapVM.AddElementVMToWorldModel(dataContext.PreviewElement);
                    }
                    dataContext.ApplyChangesVMChangesToModels();
                }
            }
        }
        #endregion
        #region Sort click events
        //Duplicate of prototypeeditordialogwindow
        private void PropertySettingSortClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            if (column == null) return;
            String field = column.Tag.ToString();// as String;
            var propertySelectorListView = elementPropertiesListView;

            if (propertySelectorListView == null) return;
            if (_CurSortCol != null)
            {
                if (_CurAdorner != null)
                {
                    AdornerLayer.GetAdornerLayer(_CurSortCol).Remove(_CurAdorner);
                }
                propertySelectorListView.Items.SortDescriptions.Clear();
            }

            if (field.Equals("Reset"))
            {
                return;
            }
            ListSortDirection newDir = ListSortDirection.Ascending;
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Descending))
            {
                _CurAdorner = null;
                return;
            }
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Ascending))
            {
                newDir = ListSortDirection.Descending;
            }

            //            if (_CurSortCol == column && _CurAdorner.Direction == newDir)
            //                newDir = ListSortDirection.Descending;

            _CurSortCol = column;
            _CurAdorner = new SortAdorner(_CurSortCol, newDir);
            AdornerLayer.GetAdornerLayer(_CurSortCol).Add(_CurAdorner);
            propertySelectorListView.Items.SortDescriptions.Add(new SortDescription(field, newDir));
        }

        //Duplicate of prototypeeditordialogwindow
        private void HoverTextSettingSortClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            if (column == null) return;
            String field = column.Tag.ToString();// as String;
            var propertySelectorListView = hoverTextPropertiesListView;

            if (propertySelectorListView == null) return;
            if (_HoverCurSortCol != null)
            {
                if (_HoverCurAdorner != null)
                {
                    AdornerLayer.GetAdornerLayer(_HoverCurSortCol).Remove(_HoverCurAdorner);
                }
                propertySelectorListView.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (_HoverCurSortCol == column && _HoverCurAdorner != null && _HoverCurAdorner.Direction.Equals(ListSortDirection.Descending))
            {
                _CurAdorner = null;
                return;
            }
            if (_HoverCurSortCol == column && _HoverCurAdorner != null && _HoverCurAdorner.Direction.Equals(ListSortDirection.Ascending))
            {
                newDir = ListSortDirection.Descending;
            }

            _HoverCurSortCol = column;
            _HoverCurAdorner = new SortAdorner(_HoverCurSortCol, newDir);
            AdornerLayer.GetAdornerLayer(_HoverCurSortCol).Add(_HoverCurAdorner);
            propertySelectorListView.Items.SortDescriptions.Add(new SortDescription(field, newDir));
        }
        #endregion

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {
            ApplyChanges(DataContext as ElementPropertyDialogContentVM, Tag as DepictionEnhancedMapViewModel);
            Close();
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            ApplyChanges(DataContext as ElementPropertyDialogContentVM, Tag as DepictionEnhancedMapViewModel);
        }

        private void btnCloseDialog_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
