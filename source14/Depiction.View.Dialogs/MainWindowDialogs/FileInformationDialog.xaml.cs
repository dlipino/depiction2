﻿using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class FileInformationDialog
    {
        public FileInformationDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    titlePanel.IsEnabled = false;
                    authorPanel.IsEnabled = false;
                    descriptionPanel.IsEnabled = false;
                    break;
            }
        }
    }
}