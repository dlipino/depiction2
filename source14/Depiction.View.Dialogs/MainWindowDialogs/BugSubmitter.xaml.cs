﻿using System;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for BugSubmitter.xaml
    /// </summary>
    public partial class BugSubmitter
    {
        public BugSubmitter()
        {
            InitializeComponent();
        }

        override protected void MainThreadShow(bool show)
        {
            if (Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<bool>(MainThreadShow), show);
                return;
            }
            if (show)
            {
                txtWhatWereYouDoing.Text = string.Empty;
#if DEBUG
                MessageTabControl.SelectedIndex = 1; 
#else
                MessageTabControl.SelectedIndex = 0; 
#endif
                Show();
            }
            else
            {
                Hide(); 
                if (Owner != null)
                {
                    Owner.Focus();
                }
            }
        }
    }
}