﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Depiction.APINew.Interfaces.ElementInterfaces;
using Depiction.View.Dialogs.LookLessElementAndRevealerDialogs;
using Depiction.ViewModelNew.ViewModels.MapControlDialogViewModels;
using Depiction.ViewModelNew.ViewModels.MapViewModels;
using Depiction.ViewModelNew.ViewModels.PrototypeViewModels;

namespace Depiction.View.Dialogs.HelperDialogs
{
    public class ElementPointRegistrationHelperControl : CommonControlFrame2
    {
        private  RoutedUICommand GeoLocateElementWithString = new RoutedUICommand("Geo-locate element", "GeoLocateWithString", typeof(ElementPointRegistrationHelperControl));
        
        static ElementPointRegistrationHelperControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementPointRegistrationHelperControl), new FrameworkPropertyMetadata(typeof(ElementPointRegistrationHelperControl)));
        }

        #region dep props
        public bool CenterOnElementAfterGeoLocate
        {
            get { return (bool)GetValue(CenterOnElementAfterGeoLocateProperty); }
            set { SetValue(CenterOnElementAfterGeoLocateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CenterOnElementAfterGeoLocate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CenterOnElementAfterGeoLocateProperty =
            DependencyProperty.Register("CenterOnElementAfterGeoLocate", typeof(bool), typeof(ElementPointRegistrationHelperControl), new UIPropertyMetadata(false));



        public string LocationTextToGeolocate
        {
            get { return (string)GetValue(LocationTextStringProperty); }
            set { SetValue(LocationTextStringProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LocationTextString.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LocationTextStringProperty =
            DependencyProperty.Register("LocationTextString", typeof(string), typeof(ElementPointRegistrationHelperControl), new UIPropertyMetadata(string.Empty));
        #endregion

        #region properties

        public ICommand GeoLocatFromTextCommand { get { return GeoLocateElementWithString; } }

        #endregion
        public ElementPointRegistrationHelperControl()
        {
            var bindings = CommandBindings;
            var commandBinding = new CommandBinding(GeoLocateElementWithString,GeolocateUsingDataContext);
            bindings.Add(commandBinding);
        }

        private void GeolocateUsingDataContext(object sender, ExecutedRoutedEventArgs e)
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            //if (dc == null) return;
            //Console.WriteLine(LocationTextToGeolocate);
            var elementVM = Content as ElementPrototypeViewModel;
            var element = elementVM.Model as IDepictionElement;
            var geocoder = AddContentDialogVM.GeocodeSingleElement(null, LocationTextToGeolocate, element, dc, null, CenterOnElementAfterGeoLocate);
            if(geocoder == null)
            {
                if (dc != null)
                {
                    dc.EndGeoLocateElementCommand.Execute(null);
                }
                return;
            }
            geocoder.RunWorkerCompleted += delegate(object threadSender, RunWorkerCompletedEventArgs eThread)
                                               {
                                                   if (eThread.Result is bool)
                                                   {
                                                       var boolResult = (bool)eThread.Result;
                                                       if (!boolResult)
                                                       {
                                                           return;
                                                       }
                                                   }
                                                   if (eThread.Result != null)
                                                   {
                                                       if (dc != null)
                                                       {
                                                           dc.EndGeoLocateElementCommand.Execute(null);
                                                       }
                                                   }
                                               };
            
            //notify currentAddprototype never gets called because it start addmode, hence this content is null.
//            dc.AddPrototypeToLocation(dc.CurrentAddPrototype,new Point(),CenterOnElementAfterGeoLocate );
//            dc.AddPrototypeToLocation(Content as ElementPrototypeViewModel, new Point(), CenterOnElementAfterGeoLocate);
        }

        protected override void Show()
        {
            BringToFront();
            Visibility = Visibility.Visible;
            Focus();
            Center();
        }

        protected override void DefaultCloseCancelOrders()
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc == null) return;
            dc.EndGeoLocateElementCommand.Execute(null);
        }
    }
}