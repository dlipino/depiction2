﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Depiction.View.Dialogs.LookLessElementAndRevealerDialogs;
using Depiction.ViewModelNew.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.HelperDialogs
{
    public class AddElementsHelpingDialog : CommonControlFrame2
    {
        static AddElementsHelpingDialog()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AddElementsHelpingDialog), new FrameworkPropertyMetadata(typeof(AddElementsHelpingDialog)));
        }

        private const string normalAdd =
            "To stop adding, click the \"Done\" button, press \"Esc\", or click the right mouse button.\r\n\r\nTo drag the world or an element, hold \"Shift\" while dragging with the left mouse button.";
        //        For lines & shapes: 
        private const string linesAndShapes =
            "Click to add vertices or 'Ctrl'+click to remove last.\r\nTo complete your element, click the \"Done\" button, press \"Esc\", or click the right mouse button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";

        //For user-drawn routes: 
        private const string userDrawnRoute =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route, click the \"Done\" button, press \"Esc\", or click the right mouse button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";

        //For road network routes: 
        private const string roadNetworkRoute =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route and snap to the road network, click the \"Done\"button, press “Esc”, or click the right mouse button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple lines & shapes: 
        private const string linesAndShapesMulti =
            "Click to add vertices or 'Ctrl'+click to remove last.\r\nTo complete your element, press \"Esc\", or click the right mouse button. To stop adding elements, click the \"Done\" button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple user-drawn routes: 
        private const string userDrawnRouteMulti =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route, press \"Esc\", or click the right mouse button. To stop adding routes, click the \"Done\" button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple road network routes: 
        private const string roadNetworkRouteMulti =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route and snap to the road network, press \"Esc\", or click the right mouse button.\r\nTo stop adding routes, click the \"Done\" button.\r\nTo drag the background or an element, hold \"Shift\" while dragging with the left mouse button.";


        public string PlaceElementHelperText
        {
            get { return (string)GetValue(PlaceElementHelperTextProperty); }
            set { SetValue(PlaceElementHelperTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlaceElementHelperText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceElementHelperTextProperty =
            DependencyProperty.Register("PlaceElementHelperText", typeof(string), typeof(AddElementsHelpingDialog), new UIPropertyMetadata(normalAdd));

        public int TopOffset { get; set; }
        #region Constructor
        public AddElementsHelpingDialog()
        {
            TopOffset = 50;

            Loaded += stopDialog_Loaded;
            DataContextChanged += AddElementsHelpingDialog_DataContextChanged;
            Closed += new System.EventHandler(AddElementsHelpingDialog_Closed);

        }

       
        #endregion


        void AddElementsHelpingDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldValue = e.OldValue as INotifyPropertyChanged;
            var newValue = e.NewValue as INotifyPropertyChanged;
            if (newValue != null)
            {
                newValue.PropertyChanged += DataContext_PropertyChanged;
            }
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= DataContext_PropertyChanged;
            }
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var castSender = sender as DepictionEnhancedMapViewModel;
            if (castSender == null) return;
            if (e.PropertyName.Equals("CurrentAddPrototype"))
            {
                var proto = castSender.CurrentAddPrototype;
                if (proto == null)
                {//Annotations
                    PlaceElementHelperText = normalAdd;
                    return;
                }
                Title = string.Format("Adding {0}", proto.DisplayName);
                if (proto.AddByZOI)
                {
                    var multiAdd = castSender.doMultiAdd;
                    if (proto.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork"))
                    {
                        PlaceElementHelperText = multiAdd ? roadNetworkRouteMulti : roadNetworkRoute;
                    }
                    else if (proto.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
                    {
                        PlaceElementHelperText = multiAdd ? userDrawnRouteMulti : userDrawnRoute;
                    }
                    else
                    {
                        PlaceElementHelperText = multiAdd ? linesAndShapesMulti : linesAndShapes;
                    }
                }else
                {
                    PlaceElementHelperText = normalAdd;
                }
            }
        }
        void Parent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsVisible) return;
            if (Parent is FrameworkElement)
            {
//                var parentRect = new RectangleGeometry
//                {
//                    Rect = LayoutInformation.GetLayoutSlot(((FrameworkElement)Parent))
//                };

                //var myRect = new RectangleGeometry(new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), ActualWidth, ActualHeight), 0, 0, RenderTransform);
                //if (!parentRect.Bounds.Contains(myRect.Bounds))
                //{
                    CenterWidthAndGivenTop(TopOffset);
                //}
            }
        }
        protected override void DefaultCloseCancelOrders()
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc == null) return;
            dc.EndElementAddCommand.Execute(null);
        }
        void AddElementsHelpingDialog_Closed(object sender, System.EventArgs e)
        {
//            var dc = DataContext as DepictionEnhancedMapViewModel;
//            if (dc == null) return;
//            dc.EndElementAddCommand.Execute(null);
//            //The double close is to add box when the user deletes elements from manage content
//            //the double is designed to get ride of the semi created shape.
//            dc.EndElementAddCommand.Execute(null);
        }

        void stopDialog_Loaded(object sender, RoutedEventArgs e)
        {
            var dialog = sender as AddElementsHelpingDialog;
            if (dialog == null) return;
            dialog.CenterWidthAndGivenTop(TopOffset);
            var parent = Parent as FrameworkElement;
            if(parent != null)
            {
                parent.SizeChanged += Parent_SizeChanged;
            }
        }
        protected override void Show()
        {
            BringToFront();
            Visibility = Visibility.Visible;
            Focus();
            var c = Parent as Canvas;
            if (Parent is Canvas && c != null)
            {

                var rect = LayoutInformation.GetLayoutSlot(c); //This sometimes returnins an empty (0 size) rect
                if (rect.Width == 0 && rect.Height == 0)
                {
                    rect = new Rect(0, 0, c.ActualWidth, c.ActualHeight);
                }

                var parentRect = new RectangleGeometry
                {
                    Rect = rect
                };

                var myRect = new RectangleGeometry(new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), ActualWidth, ActualHeight), 0, 0, RenderTransform);
                if (!parentRect.Bounds.Contains(myRect.Bounds))
                {
                    CenterWidthAndGivenTop(TopOffset);
                }
            }
        }

    }
}
