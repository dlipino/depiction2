using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Media;

namespace Depiction.View.Dialogs.ColorComb
{
    public partial class ColorPickerDialog
    {
        private bool notUserInitiated;
        private DrawingAttributes selectedDrawingAttributes;
        private DrawingAttributes origColor;
        private float brightness, transparency;

        public DrawingAttributes SelectedDrawingAttributes
        {
            get { return selectedDrawingAttributes; }
            set
            {
                selectedDrawingAttributes = new DrawingAttributes();
                origColor = value;
                Color noAlpha = origColor.Color;
                if (origColor.Color.A == 0)
                {
                    noAlpha.A = 255;
                    selectedDrawingAttributes.Color = noAlpha;
                    brightnessSlider.Value = brightnessSlider.Maximum;
                }
                else
                {
                    selectedDrawingAttributes = origColor;
                }

                UpdateControlValues();
                UpdateControlVisuals();
            }
        }

        //
        // Initialization
        #region constructor
        public ColorPickerDialog()
        {
            InitializeComponent();
        }
        #endregion

        #region override methods

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            selectedDrawingAttributes = new DrawingAttributes();
            UpdateControlValues();
            UpdateControlVisuals();

            colorComb.ColorSelected += colorComb_ColorSelected;
            brightnessSlider.ValueChanged += brightnessSlider_ValueChanged;
            transparencySlider.ValueChanged += transparencySlider_ValueChanged;
            acceptButton.Click += acceptButton_Click;
            noColorButton.Click += noColorButton_Click;
            cancelButton.Click += cancelButton_Click;
        }

        #endregion
        #region static usage

        static public SolidColorBrush PickColorModal(SolidColorBrush currentColor)
        {
            var color = new DrawingAttributes { Color = currentColor.Color };
            var mainWindow = Application.Current.MainWindow;

            var picker = new ColorPickerDialog { Owner = mainWindow, SelectedDrawingAttributes = color };

            bool? dialogResult = picker.ShowDialog();

// ReSharper disable PossibleInvalidOperationException
            if ((bool)dialogResult)
// ReSharper restore PossibleInvalidOperationException
            {
                var newColor = new SolidColorBrush(picker.SelectedDrawingAttributes.Color);
                return newColor;
            }
            return null;
        }
        #endregion
        // Completes initialization after all XAML member vars have been initialized.

        //
        // Interface



        void noColorButton_Click(object sender, RoutedEventArgs e)
        {
            selectedDrawingAttributes = new DrawingAttributes();
            Color noAlpha = origColor.Color;
            noAlpha.A = 0;
            selectedDrawingAttributes.Color = noAlpha;
            DialogResult = true;
        }

        private void transparencySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (notUserInitiated) return;

            Color nc = colorComb.SelectedColor;
            transparency = (float)e.NewValue;

            float r = brightness * nc.ScR;
            float g = brightness * nc.ScG;
            float b = brightness * nc.ScB;

            selectedDrawingAttributes.Color = Color.FromScRgb(transparency, r, g, b);
            UpdateControlVisuals();
        }

        //
        // Implementation

        // Updates values of controls when new DA is set (or upon initialization).
        private void UpdateControlValues()
        {
            notUserInitiated = true;
            try
            {
                // Set nominal color on comb.
                Color nc = selectedDrawingAttributes.Color;
                brightness = Math.Max(Math.Max(nc.ScR, nc.ScG), nc.ScB);
                transparency = nc.ScA;
                if (brightness < 0.001f) // black
                    nc = Color.FromScRgb(1f, 1f, 1f, 1f);
                else
                    nc = Color.FromScRgb(1f, nc.ScR / brightness, nc.ScG / brightness, nc.ScB / brightness);
                colorComb.SelectedColor = nc;

                // Set brightness
                brightnessSlider.Value = brightness;
                transparencySlider.Value = transparency;
            }
            finally
            {
                notUserInitiated = false;
            }
        }

        // Updates visual properties of all controls, in response to any change.
        private void UpdateControlVisuals()
        {
            // Update LGB for brightnessSlider
            var sb1 = brightnessSlider.Parent as Border;
            if (sb1 != null)
            {
                var lgb1 = sb1.Background as LinearGradientBrush;
                if (lgb1 != null)
                {
                    var startColor = Colors.Black;
                    var endColor = colorComb.SelectedColor;
                    startColor.ScA = endColor.ScA = transparency;
                    lgb1.GradientStops[0].Color = startColor;
                    lgb1.GradientStops[1].Color = endColor;
                }
            }

            // Update LGB for transparencySlider
            var sb2 = transparencySlider.Parent as Border;
            if (sb2 != null)
            {
                var lgb2 = sb2.Background as LinearGradientBrush;
                if (lgb2 != null)
                {
                    var startColor = Colors.Transparent;
                    var endColor = colorComb.SelectedColor;
                    startColor.ScR = endColor.ScR = colorComb.SelectedColor.ScR * brightness;
                    startColor.ScG = endColor.ScG = colorComb.SelectedColor.ScG * brightness;
                    startColor.ScB = endColor.ScB = colorComb.SelectedColor.ScB * brightness;
                    lgb2.GradientStops[0].Color = startColor;
                    lgb2.GradientStops[1].Color = endColor;
                }
            }

            // Update thickness
            selectedDrawingAttributes.Width = Math.Round(selectedDrawingAttributes.Width, 2);

            previewPresenter.Fill = new SolidColorBrush(selectedDrawingAttributes.Color);
        }

        //
        // Event Handlers
        private void colorComb_ColorSelected(object sender, ColorEventArgs e)
        {
            if (notUserInitiated) return;

            float f, r, g, b, transparency;
            f = (float)brightnessSlider.Value;
            transparency = (float)transparencySlider.Value;

            Color nc = e.Color;
            r = f * nc.ScR;
            g = f * nc.ScG;
            b = f * nc.ScB;

            selectedDrawingAttributes.Color = Color.FromScRgb(transparency, r, g, b);
            UpdateControlVisuals();
        }

        private void brightnessSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (notUserInitiated) return;

            Color nc = colorComb.SelectedColor;
            brightness = (float)e.NewValue;

            float r = brightness * nc.ScR;
            float g = brightness * nc.ScG;
            float b = brightness * nc.ScB;

            selectedDrawingAttributes.Color = Color.FromScRgb(transparency, r, g, b);
            UpdateControlVisuals();
        }

        private void acceptButton_Click(object sender, RoutedEventArgs e)
        {
            // Setting this property closes the dialog, when shown modally.
            DialogResult = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            selectedDrawingAttributes = origColor;
            // Setting this property closes the dialog, when shown modally.
            DialogResult = false;
        }
    }
}