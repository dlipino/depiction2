﻿using System;
using System.Windows;
using System.Windows.Controls;
using Depiction.ViewModelNew.ViewModels;

namespace Depiction.View.Dialogs.LicenseDialogs
{
    /// <summary>
    /// Interaction logic for ActivateDepictionWindow.xaml
    /// </summary>
    public partial class ActivateDepictionWindow
    {
        #region Events
        public event EventHandler ActivateDepiction;
        public event EventHandler RequestManualActivationCode;
        public event EventHandler ActivateManually;
        public event EventHandler Purchase;
        #endregion

        public ActivateDepictionWindow()
        {
            InitializeComponent();
            serialNumberText.TextChanged += serialNumberText_TextChanged;
            manualActivationText.TextChanged += serialNumberText_TextChanged;
        }

        public string SerialNumber
        {
            get { return (string)GetValue(SerialNumberProperty); }
            set { SetValue(SerialNumberProperty, value); }
        }

        //{Binding Source={x:Static Core:UIAccess.Product}, Path=ProductName, StringFormat='Purchase {0}'}
        public static readonly string PurchaseProductText = string.Format("Purchase {0}", DepictionAppViewModel.ProductType.ProductName); 

        public string ManualActivationCode
        {
            get { return (string)GetValue(ManualActivationCodeProperty); }
            set { SetValue(ManualActivationCodeProperty, value); }
        }

        public static readonly DependencyProperty ManualActivationCodeProperty =
            DependencyProperty.Register("ManualActivationCode", typeof(string), typeof(ActivateDepictionWindow), new UIPropertyMetadata(string.Empty));


        public static readonly DependencyProperty SerialNumberProperty =
            DependencyProperty.Register("SerialNumber", typeof(string), typeof(ActivateDepictionWindow), new UIPropertyMetadata(string.Empty));

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(SerialNumber);
            if (ActivateDepiction != null)
                ActivateDepiction(sender, e);
        }

        private void buyButton_Click(object sender, RoutedEventArgs e)
        {
            if (Purchase != null)
                Purchase(sender, e);
        }

        private void RequestManualActivationButton_Click(object sender, RoutedEventArgs e)
        {
            if (RequestManualActivationCode != null)
                RequestManualActivationCode(sender, e);
        }

        private static string CleanUpSerialNumber(string serialNumber)
        {
            return serialNumber.ToUpper().Replace('I', '1').Replace('O', '0').Replace('S', '5').Replace('Z', '2');
        }

        public static void serialNumberText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = ((TextBox) sender);
            int index = textBox.CaretIndex;
            textBox.Text = CleanUpSerialNumber(textBox.Text);
            textBox.CaretIndex = index;
        }

        private void btnManuallyActivate_Click(object sender, RoutedEventArgs e)
        {
            if (ActivateManually != null)
                ActivateManually(sender, e);

        }
    }
}