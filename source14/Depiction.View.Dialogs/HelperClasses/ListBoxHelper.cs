﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Depiction.View.Dialogs.HelperClasses
{
    public static class ListBoxHelper
    {
        public static void SelectItems(this ListBox listBox, IEnumerable<object> itemsToSelect)
        {
            var itemsList = listBox.Items.Cast<object>();
            //            var matchingItems = from inItems in itemsToSelect
            //                                from displayedItems in itemsList
            //                                where displayedItems.Equals(inItems)
            //                                select displayedItems;
            //           
            //            foreach (object item in matchingItems)
            //            {
            //                var currentContainer = listBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;
            //
            //                if (currentContainer != null)
            //                {
            //                    currentContainer.IsSelected = true;
            //                    //                    currentContainer.BringIntoView();
            //                    //                    currentContainer.Focus();
            //
            //                }
            //            }
            foreach (object item in itemsList)
            {
                var currentContainer = listBox.ItemContainerGenerator.ContainerFromItem(item) as ListBoxItem;

                if (currentContainer != null )
                {
                    if( itemsToSelect.Contains(item))
                    {
                        currentContainer.IsSelected = true;
                    }
                    else
                    {
                        currentContainer.IsSelected = false;
                    }
                    //                    currentContainer.BringIntoView();
                    //                    currentContainer.Focus();

                }
            }
        }
    }
}