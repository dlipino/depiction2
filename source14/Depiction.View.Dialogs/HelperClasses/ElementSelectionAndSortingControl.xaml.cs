﻿using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.HelperClasses
{
    /// <summary>
    /// Interaction logic for ElementSelectionAndSortingControl.xaml
    /// </summary>
    public partial class ElementSelectionAndSortingControl
    {
        public bool UseExpandedTreeView
        {
            get { return (bool)GetValue(UseExpandedTreeViewProperty); }
            set { SetValue(UseExpandedTreeViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UseExpandedTreeView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseExpandedTreeViewProperty =
            DependencyProperty.Register("UseExpandedTreeView", typeof(bool), typeof(ElementSelectionAndSortingControl), new UIPropertyMetadata(true));
        
        public ElementSelectionAndSortingControl()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    mainPanel.Children.Remove(groupingComboBox);
                    break;
                default:
                    break;
            }
//#if PREP
//            mainPanel.Children.Remove(groupingComboBox);
//#endif
        }
    }
}
