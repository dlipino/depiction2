﻿using System.Collections.Specialized;
using Depiction.API.AbstractObjects;

namespace Resources.Product.Prep
{
    public class DepictionPrepInformation : ProductInformationBase
    {

        public override string ProductType { get { return Prep; } }
        public override string ProductName
        {
            get { return "Depiction: Prep"; }
        } 
        public override string ProductNameInternalUsage { get { return "DepictionPrep"; } }
        public override string ResourceAssemblyName { get { return "Resources.Product.Prep"; } }
        public override string SplashScreenPath { get { return "Images/PrepSplashScreen.png"; } }
//        public override string AppIconResourceName { get { return "Images/DepictioPrep.ico"; } }
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",ProductVersion, DepictionCopyright);
            }
        }
        public override string LicenseFileName { get { return "depictionprep.lic"; } }
        public override int ProductID { get { return 19; } }

        override public StringCollection ProductQuickAddElements
        {
            get
            {
                return new StringCollection
                               {
                                   "My home",
                                   "Workplace",
                                   "School",
                                   "Emergency kit",
                                   "Shut-off: electricity",
                                   "Shut-off: gas",
                                   "Shut-off: water",
                                   "Meeting point: home",
                                   "Meeting point: neighborhood",
                                   "Meeting point: out of area",
                                   "Neighbor"
                               };
            }
        }

        public override string EulaFileAssemblyLocation
        {
            get { return "Docs.DepictionPrepEULA.xps"; }
        }
    }
}