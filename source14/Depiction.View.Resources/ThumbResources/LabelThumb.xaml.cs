﻿using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.Resources.ThumbResources
{
    /// <summary>
    /// Interaction logic for LabelThumb.xaml
    /// </summary>
    public partial class LabelThumb
    {

        public GeneralThumbType ThumbType
        {
            get { return (GeneralThumbType)GetValue(ThumbTypeProperty); }
            set { SetValue(ThumbTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThumbType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThumbTypeProperty =
            DependencyProperty.Register("ThumbType", typeof(GeneralThumbType), typeof(LabelThumb), new UIPropertyMetadata(GeneralThumbType.None,SetThumbType));

        #region Dep prop events
        private static void SetThumbType(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thumb = d as LabelThumb;
            if (thumb == null) return;
            var newValue = (GeneralThumbType)e.NewValue;
            switch(newValue)
            {
                case GeneralThumbType.Main:
                    thumb.Cursor = Cursors.SizeAll;
                    break;
                case GeneralThumbType.Top:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    break;
                case GeneralThumbType.Bottom:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
                case GeneralThumbType.Right:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                case GeneralThumbType.Left:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case GeneralThumbType.TopRight:
                    thumb.Cursor = Cursors.SizeNESW;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    break;
                case GeneralThumbType.TopLeft:
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    thumb.Cursor = Cursors.SizeNWSE;
                    break;
                case GeneralThumbType.BottomRight:
                    thumb.Cursor = Cursors.SizeNWSE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
                case GeneralThumbType.BottomLeft:
                    thumb.Cursor = Cursors.SizeNESW;
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
            }
        }

        #endregion

        public LabelThumb()
        {
            InitializeComponent();
        }

        private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = sender as LabelThumb;
            if(thumb == null) return;
            var context = thumb.DataContext as MapElementViewModel;
            if (context == null) return;

            var horzChange = e.HorizontalChange;
            var vertChange = e.VerticalChange;
            var left = context.PermaTextX;
            var top = context.PermaTextY;
            var minW = context.PermaText.MinPixelWidth;
            var minH = context.PermaText.MinPixelHeight;
            var w = context.PermaText.PixelWidth;
            var h = context.PermaText.PixelHeight;
            double finalW = w;// double.NaN;
            double finalH = h;// double.NaN;

            var thumbTypeString = thumb.ThumbType.ToString().ToLower();
            switch(thumb.ThumbType)
            {
                case GeneralThumbType.Main:
                    left += horzChange;
                    top += vertChange;
                    break;
                default://Ick?, yes definetly ick
                    if(thumbTypeString.Contains("right"))
                    {
                        w += horzChange;
                        if (w > minW) finalW = w;
                        
                    }else if(thumbTypeString.Contains("left"))
                    {
                        w -= horzChange;
                        if (w >= minW)
                        {
                            left += horzChange;
                            finalW = w;
                        }
                    }

                    if(thumbTypeString.Contains("top"))
                    {
                        h -= vertChange;
                        if (h > minH)
                        {
                            finalH = h;
                            top += vertChange;
                        }
                        
                    }else if(thumbTypeString.Contains("bottom"))
                    {
                        h += vertChange;
                        if (h > minH) finalH = h;
                        
                    }
                    
                    break;
            }
            //Not the most efficient way of doing this, but it is clearer than being clever
             context.PermaTextX = left;
            context.PermaTextY = top;
            context.PermaTextWidth = finalW;
            context.PermaTextHeight = finalH;
            context.PermaTextCenterX = left + finalW / 2;
            context.PermaTextCenterY = top + finalH / 2;

        }
    }
}
