﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.Resources.ThumbResources
{
    public class AnnotionResizeThumb : Thumb
    {
        private Style thumbStyle;
        private string styleName = "AnnotationResizeThumbStyle";

        public Style AnnotionResizeThumbStyle
        {
            get
            {
                if (Application.Current != null && Application.Current.Resources != null && thumbStyle == null)
                {
                    if (Application.Current.Resources.Contains(styleName))
                    {
                        thumbStyle = Application.Current.Resources[styleName] as Style;
                    }
                }
                return thumbStyle;
            }
        }

        public AnnotionResizeThumb()
        {
            DragDelta += AnnotionResizeThumb_DragDelta;
            Style = AnnotionResizeThumbStyle;
        }

        void AnnotionResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var f = sender as FrameworkElement;
            if (f == null) return;
            var tp = f.TemplatedParent as ContentPresenter;
            if (tp == null) return;
            var annotContext = tp.DataContext as IMapAnnotationViewModel;
            if (annotContext == null) return;

            annotContext.AdjustWidth(-e.HorizontalChange);
            e.Handled = true;
        }
    }
}
