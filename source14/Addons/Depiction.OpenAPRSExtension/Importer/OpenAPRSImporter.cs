using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;


namespace Depiction.OpenAPRSExtension.Importer
{
    [DepictionNonDefaultImporterMetadata("DepictionOpenAPRSImporter", DisplayName = "APRS Stations (OpenAPRS)",
        ImporterSources = new[] { InformationSource.Web }, AddonPackage = "Open APRS")]
    public class OpenAPRSImporter : NondefaultDepictionImporterBase
    {
        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new OpenAPRSImporterBackgroundService();
            var name = string.Format("APRS Stations (OpenAPRS)");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }
}