﻿using System.IO;
using System.Windows;

namespace APRSLiveConfiguration.View
{
    /// <summary>
    /// Interaction logic for APRSAddinConfigView.xaml
    /// </summary>
    public partial class APRSAddinConfigView
    {
        public APRSAddinConfigView()
        {
            InitializeComponent();
        }

        public string RunPath = string.Empty;

        private void btnOK_Click_1(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window != null)
            {
                if (System.Windows.Interop.ComponentDispatcher.IsThreadModal)
                    window.DialogResult = true;
                else
                    window.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window != null)
            {
                if (System.Windows.Interop.ComponentDispatcher.IsThreadModal)
                    window.DialogResult = false;
                else
                    window.Close();
            }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            var helpPath = Path.Combine(RunPath, "help\\mainHelp.html");
            try
            {
                System.Diagnostics.Process.Start(helpPath);
            }
            catch
            {
                // do nothing if no help exists.
            }
        }

        private void btnShowLog_Click(object sender, RoutedEventArgs e)
        {
            var logpath = Path.Combine(RunPath, "AprsLive_log.txt");
            try
            {
                System.Diagnostics.Process.Start(logpath);
            }
            catch 
            {
                // do nothing if no log exists.
            }
        }
    }
}