using APRS;
using APRSLiveConfiguration.Model;
using Depiction.API.MVVM;

namespace APRSLiveConfiguration.ViewModel
{
    public class TCPConfigViewModel : ViewModelBase
    {
        public TCPConfigModel Model { get; set; }
        public TCPConfigViewModel()
        {
            Model = new TCPConfigModel();
        }

        public TCPConfigViewModel(TCPConfigModel model)
        {
            Model = model;
        }

        public string Password { get { return Model.Password; } set { Model.Password = value; NotifyPropertyChanged("Password"); } }
        public string Host { get { return Model.Host; } set { Model.Host = value; NotifyPropertyChanged("Host"); } }
        public int Port { get { return Model.Port; } set { Model.Port = value; NotifyPropertyChanged("Port"); } }
        public bool UseCustomFilter { get { return Model.UseCustomFilter; } set { Model.UseCustomFilter = value; NotifyPropertyChanged("UseCustomFilter"); } }
        public string CustomFilter { get { return Model.CustomFilter; } set { Model.CustomFilter = value; NotifyPropertyChanged("CustomFilter"); } }


        public string TypeName
        {
            get { return Model.TypeName; }
        }
        public APRSInputType ConfigType
        {
            get { return APRSInputType.TCP; }
        }

        public override string ToString()
        {
            return TypeName;
        }
    }
}