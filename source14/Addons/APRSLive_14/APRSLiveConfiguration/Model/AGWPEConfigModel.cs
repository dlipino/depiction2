


namespace APRSLiveConfiguration.Model
{
    public class AGWPEConfigModel : IAPRSInputConfiguration
    {
        public string Host { get; set; }
        public int TCPPort { get; set; }
        public byte RadioPort { get; set; }

        public string TypeName
        {
            get { return "AGWPE"; }
        }
        public AGWPEConfigModel()
        {
            Host = "localhost";
            TCPPort = 8000;
            RadioPort = 0;
        }
        
        
    }
}