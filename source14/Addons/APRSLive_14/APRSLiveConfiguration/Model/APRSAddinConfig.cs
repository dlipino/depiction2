using System;
using APRS;
using APRSLiveConfiguration.Properties;


namespace APRSLiveConfiguration.Model
{
    public class APRSAddinConfig
    {
        public event EventHandler ConfigChangesApplied;
        public SerialPortConfigModel serialPortModel = new SerialPortConfigModel();
        public TCPConfigModel tcpConfigModel = new TCPConfigModel();
        public AGWPEConfigModel agwpeConfigModel = new AGWPEConfigModel();
        public APRSInputType InputType = APRSInputType.TCP;

//        private ICommand okCommand;
//        private ICommand cancelCommand;
        #region Properties

        public int TimeWindowMinutes { get; set; }
        public bool UserHasSavedConfig { get; set; }
        public bool CropToRegion { get; set; }

//        public ICommand CancelCommand
//        {
//            get
//            {
//                if (cancelCommand == null)
//                    cancelCommand = new DelegateCommand(ReloadConfig);
//                return cancelCommand;
//            }
//        }
//
//        public ICommand OKCommand
//        {
//            get
//            {
//                if (okCommand == null)
//                    okCommand = new DelegateCommand(SaveConfig);
//                return okCommand;
//            }
//        }

        public string CallSign { get; set; }

        //        public string ConfigPath { get { return APRSLiveConfigPersistence.GetConfigPath(); } }
        #endregion
        #region constructor
        public APRSAddinConfig()
        {
            CallSign = String.Empty;
            LoadConfig();//this);
        }
        #endregion

//        private void ReloadConfig()
//        {
//            LoadConfig();
//        }
        private  void LoadConfig()//APRSAddinConfig config)
        {
                serialPortModel.PortName = Settings.Default.SerialPort;
                serialPortModel.Baud = Settings.Default.BaudRate;
                serialPortModel.Handshake = Settings.Default.SerialHandshake;
                serialPortModel.Parity = Settings.Default.SerialParity;
                InputType = Settings.Default.InputType;
                tcpConfigModel.Host = Settings.Default.TCPHost;
                tcpConfigModel.Port = Settings.Default.TCPPort;
                tcpConfigModel.Password = Settings.Default.TCPPassword;
                tcpConfigModel.UseCustomFilter = Settings.Default.TCPUseCustomFilter;
                tcpConfigModel.CustomFilter = Settings.Default.TCPCustomFilter;
                agwpeConfigModel.Host = Settings.Default.AgwpeTCPHost;
                agwpeConfigModel.RadioPort = Settings.Default.AgwpeRadioPort;
                agwpeConfigModel.TCPPort = Settings.Default.AgwpeTCPPort;
                CallSign = Settings.Default.CallSign;
                CropToRegion = Settings.Default.CropToRegion;
                TimeWindowMinutes = Settings.Default.TimeWindowMinutes;
                UserHasSavedConfig = Settings.Default.UserHasSavedConfig;
            
        }
        public void SaveConfig()
        {
//            var config = APRSLiveConfigPersistence.GetConfig();

            var section = Settings.Default;// APRSLiveConfigPersistence.GetConfigSection(config);
//            if (section == null)
//            {
//                section = new APRSLiveConfigPersistence();
//                config.Sections.Add("AprsLiveConfig", section);
//            }
//            section.SectionInformation.ForceSave = true;
            section.SerialPort = serialPortModel.PortName;
            section.BaudRate = serialPortModel.Baud;
            section.SerialHandshake = serialPortModel.Handshake;
            section.SerialParity = serialPortModel.Parity;
            section.InputType = InputType;
            section.TCPHost = tcpConfigModel.Host;
            section.TCPPort = tcpConfigModel.Port;
            section.TCPPassword = tcpConfigModel.Password;
            section.CropToRegion = CropToRegion;

            section.TCPUseCustomFilter = tcpConfigModel.UseCustomFilter;
            section.TCPCustomFilter = tcpConfigModel.CustomFilter;

            section.CallSign = CallSign;
            section.TimeWindowMinutes = TimeWindowMinutes;

            section.AgwpeTCPHost = agwpeConfigModel.Host;
            section.AgwpeRadioPort = agwpeConfigModel.RadioPort;
            section.AgwpeTCPPort = agwpeConfigModel.TCPPort;
            section.UserHasSavedConfig = true;
            UserHasSavedConfig = true;
            section.Save();
//            ConfigurationManager.RefreshSection("AprsLiveConfig");

            if (ConfigChangesApplied != null)
                ConfigChangesApplied(this, EventArgs.Empty);
        }
    }
}