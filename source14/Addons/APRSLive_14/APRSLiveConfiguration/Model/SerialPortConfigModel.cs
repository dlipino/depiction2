﻿using System.IO.Ports;

namespace APRSLiveConfiguration.Model
{
    public enum FlowControlType { Software, Hardware, None };
    
    public class SerialPortConfigModel: IAPRSInputConfiguration
    {
        public string PortName { get; set; }
        private int baud;
        public int Baud
        {
            get
            {
                if (baud == 0)
                    baud = 2400;
                return baud;
            }
            set { baud = value; }
        }

        public Handshake Handshake { get; set; }
        public Parity Parity{ get; set;}
           

        public string TypeName
        {
            get { return "Serial Port"; }
        }
    }

    public interface IAPRSInputConfiguration
    {
        string TypeName { get; }
    }
}