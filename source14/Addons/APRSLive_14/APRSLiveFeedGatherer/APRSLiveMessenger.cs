﻿using System;
using System.Collections.Generic;
using APRSLiveFeedGatherer.Model;
using APRSLiveFeedGatherer.View;
using APRSLiveFeedGatherer.ViewModel;
using APRSPacketIO;

namespace APRSLiveFeedGatherer
{
    public class APRSLiveMessenger
    {
        public event EventHandler<APRSMessageEventArgs> MessageReceived;
        private PacketIO packetIO;
        //Conversations that associated with each call sign.
        //Seems like every aprs station has a call sign and thus each station has a conversation associated with it
        //not sure how far back the history goes.
        private readonly Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();
        private string userCallSign;

        #region properties
        public string UserCallSign
        {
            get { return userCallSign; }
        }

        #endregion
        #region constructor
        public APRSLiveMessenger(string inCallSign)
        {
            userCallSign = inCallSign;
        }
        #endregion

        //How is the send message related to recieving
        public void SendMessage(APRSMessage msg)
        {
            var conv = GetConversation(msg.DestinationCallSign);
            conv.AddMessage(msg);
            packetIO.Send(msg.Message, msg.DestinationCallSign);
        }

        public void DisplayMessagingDialog(string destCallSign)
        {
            var message = new APRSMessage
            {
                DestinationCallSign = destCallSign,
                FromCallSign = userCallSign
            };
            Conversation conv = GetConversation(destCallSign);
            var view = new MessageWithConversationView();

            var mwconvModel = new MessageWithConversationModel(message, conv);
            var mwconvVM = new MessageWithConversationViewModel(mwconvModel);//, this);
            mwconvVM.DestinationCallsignChanged += mwconv_DestinationCallsignChanged;
            view.DataContext = mwconvVM;
            view.Show();
        }

        public void ProcessMessage(string message, string fromCallSign, string messageId)
        {
            if (message == "ack") return;
            var msg = new APRSMessage
            {
                DestinationCallSign = userCallSign,
                FromCallSign = fromCallSign,
                Message = message,
                MessageID = messageId,
                Timestamp = DateTime.Now
            };
            //MOved to backgroundservice
//            APRSStationInfo stationInfo;
//            //What the heck are the collected elements?
//            if (collectedElements.TryGetValue(fromCallSign, out stationInfo))
//            {
//                stationInfo.ReceivedMessageFromStation(msg);
//            }

            var conversation = GetConversation(fromCallSign);
            conversation.AddMessage(msg);
            if (MessageReceived != null)
            {
                MessageReceived(this, new APRSMessageEventArgs(msg));
            }
            packetIO.SendAck(fromCallSign, msg.MessageID);
        }

        public Conversation GetConversation(string destCallSign)
        {
            Conversation conv;
            if (!conversations.TryGetValue(destCallSign, out conv))
            {
                conv = new Conversation();
                conversations.Add(destCallSign, conv);
            }
            return conv;
        }

        private void mwconv_DestinationCallsignChanged(object sender, EventArgs e)
        {
            var mwconvViewModel = sender as MessageWithConversationViewModel;
            if (mwconvViewModel != null)
            {
                var conv = GetConversation(mwconvViewModel.MessageViewModel.DestinationCallSign);
                if (conv != mwconvViewModel.Model.Conversation)
                {
                    mwconvViewModel.Model.Conversation = conv;
                    mwconvViewModel.ConversationViewModel = new ConversationViewModel(conv);//, this);
                }
            }
        }
    }
}
