﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace APRSLiveFeedGatherer.Model
{
    public class Conversation: INotifyCollectionChanged
    {
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        private readonly List<APRSMessage> messages = new List<APRSMessage>();
       
        public string PartyOneCallSign;
        public string PartyTwoCallSign;

        public List<APRSMessage> Messages
        {
            get { return messages; }
        }


        public void AddMessage(APRSMessage message)
        {
            if (PartyOneCallSign == null)
            {
                PartyOneCallSign = message.DestinationCallSign;
                PartyTwoCallSign = message.FromCallSign;
            }
            Messages.Add(message);
            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, message));

        }

    }
}