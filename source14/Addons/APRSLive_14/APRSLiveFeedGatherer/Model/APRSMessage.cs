﻿using System;

namespace APRSLiveFeedGatherer.Model
{
    public class APRSMessage
    {
        public string FromCallSign { get; set; }
        public string DestinationCallSign { get; set; }
        public string Message { get; set; }
        public string MessageID { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class APRSMessageEventArgs : EventArgs
    {
        public APRSMessageEventArgs(APRSMessage msg)
        {
            Message = msg;
        }

        public APRSMessage Message { get; private set; }
    }
}
