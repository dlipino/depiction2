﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using APRS;
using APRSLiveFeedGatherer.Model;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;

namespace APRSLiveFeedGatherer
{
    public class APRSStationInfo
    {
//        private readonly APRSLiveFeedGatherer gatherer;
        private int timeWindowFromConfig;
        public IDepictionElement Element { get; set; }
        public SortedList<DateTime, TimestampedPosition> PositionHistory = new SortedList<DateTime, TimestampedPosition>();
        static readonly BitmapSource primarySymbols;
        static readonly BitmapSource secondarySymbols;
        public PacketInfo lastPacket;
        public string lastMessage;
        public DateTime lastMessageReceivedTime;
        public DateTime lastUpdatedAt;
        
        #region Properties
        
        public BitmapSource Icon { get; set; }

        public string IconKey { get; set; }
        
        #endregion

        #region Constructor
        
        public APRSStationInfo(int timeWindowMinutes)//APRSLiveFeedGatherer gatherer)
        {
           // this.gatherer = gatherer;
            timeWindowFromConfig = timeWindowMinutes;
        }
        
        #endregion

        public static APRSStationInfo CreateNewFromPacket(int timeWindowMinutes,PacketInfo packetInfo)//APRSLiveFeedGatherer gather1er,
        {
            if (packetInfo.Latitude == null || packetInfo.Longitude == null) return null;

            var currentTime = DateTime.Now;
            var elem = ElementFactory.CreateElementFromTypeString("Depiction.Plugin.PointOfInterest");
            var elembase = elem as DepictionElementBase;
            if (elembase == null)
                return null;
            elembase.ElementType = "Depiction.Plugin.APRSLiveFeed";
            elembase.TypeDisplayName = "APRS Station";
            SetPropertyValue(elem, "DisplayName", "Display Name", "APRS Station");
            elem.SetPropertyValue("Draggable", false, false);
            IElementProperty prop = new DepictionElementProperty("CallSign", "Call Sign", packetInfo.Callsign);

            prop.IsHoverText = true;
            elem.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prop = new DepictionElementProperty("LastUpdated", "Last Updated", currentTime.ToString());
            //prop.IsHoverText = true;
            elem.AddPropertyOrReplaceValueAndAttributes(prop, false);
            elem.PermaText.IsEnhancedPermaText = true;

            var stationInfo = new APRSStationInfo(timeWindowMinutes) { Element = elem };
            stationInfo.Icon = GetAPRSIcon(packetInfo.SymbolTable, packetInfo.RawSymbol - 32);
            stationInfo.IconKey = Guid.NewGuid().ToString();
            UpdateStation(stationInfo, packetInfo, currentTime);

            return stationInfo;
        }

        private static void UpdateStation(APRSStationInfo stationInfo, PacketInfo packetInfo, DateTime currentTime)
        {
            IElementProperty prop;
            stationInfo.lastUpdatedAt = currentTime;
            if (packetInfo.Latitude != null && packetInfo.Longitude != null)
            {
                var currentPos = new LatitudeLongitude(packetInfo.Latitude.Value, packetInfo.Longitude.Value);
                stationInfo.UpdatePositionHistory(currentPos, currentTime);
                stationInfo.Element.Position = currentPos;
            }
            stationInfo.lastPacket = packetInfo;
            if (packetInfo.Altitude != null)
            {
                prop = new DepictionElementProperty("Altitude", "Altitude", packetInfo.Altitude.ToString());
                stationInfo.Element.AddPropertyOrReplaceValueAndAttributes(prop, false);
            }
            if (packetInfo.Speed != null)
            {
                prop = new DepictionElementProperty("Speed", "Speed", packetInfo.Speed.ToString());
                stationInfo.Element.AddPropertyOrReplaceValueAndAttributes(prop, false);
            }

            if (packetInfo.Direction != null)
            {
                prop = new DepictionElementProperty("Direction", "Direction", packetInfo.Direction.ToString());
                stationInfo.Element.AddPropertyOrReplaceValueAndAttributes(prop, false);
            }
            if (packetInfo.WeatherInfo != null)
                UpdateWeatherInfo(stationInfo, packetInfo.WeatherInfo);
            int lines;
            var labelString = GetHtmlLabelStringFromStationInfo(stationInfo, out lines);
            UpdateStationLabel(stationInfo.Element, labelString, lines);
        }

        private static void UpdateStationFromPacket(APRSStationInfo stationInfo, PacketInfo packetInfo)
        {
            UpdateStation(stationInfo, packetInfo, DateTime.Now);
            SetPropertyValue(stationInfo.Element, "LastUpdated", "Last Updated", stationInfo.lastUpdatedAt.ToString());
        }

        private static void UpdateWeatherInfo(APRSStationInfo stationInfo, WeatherInfo wi)
        {
            if (wi.WindDirection != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "WindDirection", "Wind Direction", wi.WindDirection.ToString());
            if (wi.WindSpeed != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "WindSpeed", "Wind Speed (mph)", wi.WindSpeed.ToString());
            if (wi.WindGust != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "WindGust", "Wind Gust (mph)", wi.WindGust.ToString());
            if (wi.Temperature != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "Temperature", "Temperature (F)", wi.Temperature.ToString());
            if (wi.RainLastHour != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "RainLastHour", "Rain Last Hour (in)", (wi.RainLastHour / 100).ToString());
            if (wi.RainLast24Hours != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "RainLast24Hours", "Rain Last 24 Hours (in)", (wi.RainLast24Hours / 100).ToString());
            if (wi.RainSinceMidnight != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "RainSinceMidnight", "Rain Since Midnight (in)", (wi.RainSinceMidnight / 100).ToString());
            if (wi.BarometricPressure != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "BarometricPressure", "Barometric Pressure (mb)", (wi.BarometricPressure / 10).ToString());
            if (wi.Humidity != Int32.MaxValue)
                SetPropertyValue(stationInfo.Element, "Humidity", "Humidity (%)", wi.Humidity.ToString());
        }

        private static void UpdateStationLabel(IDepictionElement elem, string labelText, int lines)
        {
            var prop = new DepictionElementProperty("HTMLLabel", "Label", labelText);
            prop.IsHoverText = true;
            elem.PermaText.PixelWidth = 250;
            elem.PermaText.PixelHeight = 70 + lines * 14;
            elem.AddPropertyOrReplaceValueAndAttributes(prop, true);
        }

        private static string GetHtmlLabelStringFromStationInfo(APRSStationInfo stationInfo, out int lines)
        {
            lines = 1;

            //var labelText = string.Format("<b>{0}</b><br/>Updated At: {1}", stationInfo.lastPacket.Callsign, stationInfo.lastUpdatedAt);
            var labelText = string.Format("<br/>Updated At: {0}", stationInfo.lastUpdatedAt);
            var packetInfo = stationInfo.lastPacket;
            if (stationInfo.lastMessage != null)
                AppendLabelText("{0}", string.Format("Last Message ({0}): <b>:{1}</b>", stationInfo.lastMessageReceivedTime.ToShortTimeString(), stationInfo.lastMessage), ref labelText, ref lines);

            if (packetInfo.Altitude != null)
                AppendLabelText("<br/>Altitude : {0}", packetInfo.Altitude, ref labelText, ref lines);

            if (packetInfo.Speed != null)
                AppendLabelText("<br/>Speed : {0}", packetInfo.Speed, ref labelText, ref lines);

            if (packetInfo.Direction != null)
                AppendLabelText("<br/>Direction : {0}", packetInfo.Direction.ToDegrees() + " deg", ref labelText, ref lines);

            var wi = stationInfo.lastPacket.WeatherInfo;
            if (wi != null)
            {
                if (wi.WindDirection != Int32.MaxValue)
                    AppendLabelText("<br/>Wind Direction : {0}", wi.WindDirection, ref labelText, ref lines);
                if (wi.WindSpeed != Int32.MaxValue)
                    AppendLabelText("<br/>Wind Speed (mph) : {0}", wi.WindSpeed, ref labelText, ref lines);
                if (wi.WindGust != Int32.MaxValue)
                    AppendLabelText("<br/>Wind Gust (mph) : {0}", wi.WindGust, ref labelText, ref lines);
                if (wi.Temperature != Int32.MaxValue)
                    AppendLabelText("<br/>Temperature (F) : {0}", wi.Temperature, ref labelText, ref lines);
                if (wi.RainLastHour != Int32.MaxValue)
                    AppendLabelText("<br/>Rain Last Hour (in) : {0}", wi.RainLastHour * 0.01, ref labelText, ref lines);
                if (wi.RainLast24Hours != Int32.MaxValue)
                    AppendLabelText("<br/>Rain Last 24 Hours (in) : {0}", wi.RainLast24Hours * 0.01, ref labelText, ref lines);
                if (wi.RainSinceMidnight != Int32.MaxValue)
                    AppendLabelText("<br/>Rain Since Midnight (in) : {0}", wi.RainSinceMidnight * 0.01, ref labelText, ref lines);
                if (wi.BarometricPressure != Int32.MaxValue)
                    AppendLabelText("<br/>Barometric Pressure (mb) : {0}", wi.BarometricPressure * 0.1, ref labelText, ref lines);
                if (wi.Humidity != Int32.MaxValue)
                    AppendLabelText("<br/>Humidity (%) : {0}", wi.Humidity, ref labelText, ref lines);
            }
            labelText = labelText + "</br>";
            return labelText;
        }

        private static void AppendLabelText(string label, object alt, ref string labelText, ref int lines)
        {
            labelText += string.Format(label, alt);
            lines++;
        }
        
        private static void SetPropertyValue(IDepictionElement elem, string propName, string propDisplayName, string propValue)
        {
            var prop = new DepictionElementProperty(propName, propDisplayName, propValue);
            elem.AddPropertyOrReplaceValueAndAttributes(prop, false);
        }

        public void UpdateStationFromPacket(PacketInfo packetInfo)
        {
            UpdateStationFromPacket(this, packetInfo);
        }

        public void ReceivedMessageFromStation(APRSMessage message)
        {
            lastMessage = message.Message;
            lastMessageReceivedTime = message.Timestamp;
            SetPropertyValue(Element, "LastMessage", "Last Message", message.Message);
            int lines;
            var labelString = GetHtmlLabelStringFromStationInfo(this, out lines);
            UpdateStationLabel(Element, labelString, lines);
            Element.ElementUpdated = true;
        }

        public TimestampedPosition LastPosition
        {
            get
            {
                return PositionHistory.Count == 0 ? null : PositionHistory.Values[PositionHistory.Count - 1];
            }
        }

        public void UpdatePositionHistory(LatitudeLongitude position, DateTime timestamp)
        {
            var posHist = new TimestampedPosition { Position = position, Time = timestamp };
            if (PositionHistory.Count > 0)
            {
                var lastPos = PositionHistory.Values[PositionHistory.Count - 1];
                if (lastPos.Position.Equals(posHist.Position))
                {
                    lastPos.Time = timestamp;
                }
                else
                {
                    //PositionHistory.Add(posHist.Time, posHist);
                }
            }
            else
            {
                PositionHistory.Add(posHist.Time, posHist);
            }
        }

        public void UpdateHistory(DateTime time)
        {
            DateTime startOfWindow = time - TimeSpan.FromMinutes(timeWindowFromConfig);//gatherer.TimeWindowMinutes);
            int delCount = 0;
            for (var i = 0; i < PositionHistory.Count; i++)
            {
                if (PositionHistory.Keys[i] > startOfWindow)
                {
                    delCount = i;
                    break;
                }
            }
            for (var i = 0; i < delCount; i++)
            {
                PositionHistory.RemoveAt(i);
            }
        }

        static APRSStationInfo()
        {
            if (primarySymbols == null)
            {
                var uri = new Uri("/APRSLiveFeedGatherer;component/ResourceDictionary.xaml", UriKind.Relative);
                var resourceDictionary = new ResourceDictionary { Source = uri };
                primarySymbols = (BitmapImage)resourceDictionary["APRSLive.PrimarySymbols"];
                primarySymbols.Freeze();
                secondarySymbols = (BitmapImage)resourceDictionary["APRSLive.SecondarySymbols"];
                secondarySymbols.Freeze();
            }
        }

        public static BitmapSource GetAPRSIcon(SymbolTable symbolTable, int index)
        {
            if (symbolTable == SymbolTable.Primary)
                return getCroppedBitmap(primarySymbols, index, 16, 16);
            return getCroppedBitmap(secondarySymbols, index, 16, 16);
        }

        private static BitmapSource getCroppedBitmap(BitmapSource multiBitmap, int index, int width, int height)
        {
            int x = 0;
            int y = index * height;
            while (y > (multiBitmap.PixelHeight - height))
            {
                x += width;
                y -= multiBitmap.PixelHeight;
            }
            var slice = new CroppedBitmap(multiBitmap, new Int32Rect(x, y, width, height));
            slice.Freeze();
            //return slice;
            var bmi = BitmapSourceToBitmapImage(slice);   // need to do this because stuff saved in dpn file as bitmapimage,  not bitmapsource
            bmi.Freeze();
            return bmi;
        }

        public static BitmapImage BitmapSourceToBitmapImage(BitmapSource bitmapSource)
        {
            var encoder = new PngBitmapEncoder();
            var memoryStream = new MemoryStream();
            var bImg = new BitmapImage();

            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            encoder.Save(memoryStream);

            bImg.BeginInit();
            bImg.StreamSource = new MemoryStream(memoryStream.ToArray());
            bImg.EndInit();

            memoryStream.Close();

            return bImg;
        }
    }

    public class TimestampedPosition
    {
        public DateTime Time;
        public ILatitudeLongitude Position;
    }
}
