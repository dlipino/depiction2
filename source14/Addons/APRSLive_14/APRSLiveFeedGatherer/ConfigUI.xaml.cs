﻿using System.IO.Ports;
using System.Windows;
using System.Windows.Controls;

namespace APRSLiveFeedGatherer
{
    /// <summary>
    /// Interaction logic for ConfigUI.xaml
    /// </summary>
    public partial class ConfigUI : UserControl
    {
        public ConfigUI()
        {
            InitializeComponent();
            UpdateUIState();
            portsCombo.ItemsSource = SerialPort.GetPortNames();
        }

        private void UpdateUIState()
        {
            SerialPortSetup.Visibility = Visibility.Visible;
            TCPSetup.Visibility = Visibility.Visible;
            if (tcpConnectionRB.IsChecked == true)
                SerialPortSetup.Visibility = Visibility.Collapsed;
            else
            {
                TCPSetup.Visibility = Visibility.Collapsed;
            }
        }

        private void connectionTypeRadio_Click(object sender, RoutedEventArgs e)
        {
            UpdateUIState();

        }
    }
}
