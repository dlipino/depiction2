﻿using System;
using System.Windows.Input;
using APRSLiveFeedGatherer.Model;
using Depiction.API.MVVM;

namespace APRSLiveFeedGatherer.ViewModel
{
    public class APRSMessageVM : ViewModelBase
    {
        public event EventHandler DestinationCallsignChanged;
        public event EventHandler RequestClose;
//        private readonly APRSLiveFeedGatherer gatherer;
        private APRSLiveMessenger messenger;
        private APRSMessage message;
        private string destinationCallSign;


        private ICommand sendCommand;

        #region Properties

        public ICommand SendCommand
        {
            get
            {
                if (sendCommand == null)
                    sendCommand = new DelegateCommand(SendMessage);
                return sendCommand;
            }
        }
        public string DestinationCallSign
        {
            get { return message.DestinationCallSign; }
            set
            {
                message.DestinationCallSign = value;
                destinationCallSign = value;
                NotifyPropertyChanged("DestinationCallSign");
                if (DestinationCallsignChanged != null)
                    DestinationCallsignChanged(this, new EventArgs());
            }
        }
        public string NewMessage
        {
            get { return message.Message; }
            set
            {
                message.Message = value;
                NotifyPropertyChanged("NewMessage");
            }
        }
        public string MessageID
        {
            get { return message.MessageID; }
            set
            {
                message.MessageID = value;
                NotifyPropertyChanged("MessageID");
            }
        }

        #endregion
        #region Constructor

        public APRSMessageVM(APRSMessage message)//, APRSLiveFeedGatherer gatherer)
        {
            this.message = message;
            destinationCallSign = message.DestinationCallSign;
//            this.gatherer = gatherer;
        }
        #endregion

       
        public override string ToString()
        {
         //   if (message.IsOutgoing)
         //       return string.Format("{0}:{1}", message.FromCallSign, message.Message);
            return string.Format("{0:T}  {1} :{2}", message.Timestamp, message.FromCallSign, message.Message);
        }

        private void SendMessage()
        {
            message.Timestamp = DateTime.Now;
            messenger.SendMessage(message);
            message = new APRSMessage { DestinationCallSign = DestinationCallSign, FromCallSign = messenger.UserCallSign };
            NotifyPropertyChanged("NewMessage");
        }
    }
}
