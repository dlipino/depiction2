﻿using System;
using System.Windows.Input;
using APRSLiveFeedGatherer.Model;
using MvvmFoundation.Wpf;

namespace APRSLiveFeedGatherer.ViewModel
{
    public class InputConfigViewModel: ObservableObject

    {
        //private APRSAddinConfigSaveCommand _saveCommand;
        private IAPRSInputConfiguration inputConfig;

        public InputConfigViewModel(IAPRSInputConfiguration inputConfig)
        {
            this.inputConfig = inputConfig;
            if (inputConfig is SerialPortConfigModel)
                InnerViewModel = new SerialPortConfigViewModel(inputConfig as SerialPortConfigModel);
        }

        private ObservableObject InnerViewModel { get; set; }

        public string Name { get { return inputConfig.TypeName; } }
        public override string ToString()
        {
            return Name;
        }
        public ICommand SaveCommand
        {
            get
            {
                return null;
            }    
        }
        
    }
}
