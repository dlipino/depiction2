﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace APRSLiveFeedGatherer.View
{
    /// <summary>
    /// Interaction logic for APRSAddinConfigView.xaml
    /// </summary>
    [Export(typeof(UserControl))]
    public partial class AboutAPRSLiveView
    {
        private readonly APRSLiveFeedGatherer addon;

        public AboutAPRSLiveView()
        {
            InitializeComponent();
        }

        public AboutAPRSLiveView(APRSLiveFeedGatherer addon):this()
        {
            this.addon = addon;
        }

        public string RunPath = string.Empty;

       

        private void btnOpenConfigDialog_Click(object sender, RoutedEventArgs e)
        {
            addon.ConfigDialogShowModal();
        }
        
        

       
        
        
    }
}