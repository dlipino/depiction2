﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using APRS;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer.Model;
using APRSLiveFeedGatherer.View;
using APRSLiveFeedGatherer.ViewModel;
using APRSPacketIO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.ExceptionHandling;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.ValueTypes;

[assembly: InternalsVisibleTo("APRSTest")]
namespace APRSLiveFeedGatherer
{
    /***
     * this service is basically in charge of the PacketIO, which is what connects to the HAM radio input of choice.
     * The parameters of the PacketIO class are supposed to be changable on the fly.
     * Stopping the backgroundservice should stop the packetIO. The packetIO gets started and stopped withing the backgroundservice
     * in order to reset its properties.
     */
    public class APRSLiveBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        #region variables

        private APRSLiveMessenger _messengerService;
        private PacketIO packetIO;
        private IPacketListener listener;
        private EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
        private readonly Dictionary<string, APRSStationInfo> collectedElements = new Dictionary<string, APRSStationInfo>();
        private APRSAddinConfig config;
        private IMapCoordinateBounds regionBounds;
        private int TimeWindowMinutes = 15;//what is this?
        private bool isConnectedToElementRepository ;
        public const string serviceNameConst = "APRSLiveService";

        #endregion

        #region properties
        internal APRSLiveMessenger MessengerSerice { get { return _messengerService; } }
        public bool IsRunning { get; private set; }//im pretty sure im duplicating something 
        public override bool IsServiceRefreshable
        {
            get { return false; }
        }

        public override string ServiceName { get { return serviceNameConst; } }

        #endregion

        #region constructor

        public APRSLiveBackgroundService(IMapCoordinateBounds regionOfInterest, APRSAddinConfig aprsConfig)//PacketIO packetIO)
        {
            IsRunning = false;
            regionBounds = regionOfInterest;
            config = aprsConfig;
        }

        void MessengerServiceMessageReceived(object sender, APRSMessageEventArgs e)
        {
            var message = e.Message;
            APRSStationInfo stationInfo;
            //What the heck are the collected elements?
            if (collectedElements.TryGetValue(message.FromCallSign, out stationInfo))
            {
                stationInfo.ReceivedMessageFromStation(message);
            }
        }

        #endregion

        #region Public helpers
        public PacketIO CreatePacketIOFromConfig(APRSAddinConfig aprsConfig)
        {
            PacketEncoder encoder;

            if (aprsConfig.InputType == APRSInputType.Serial)
            {
                listener = new SerialPacketListener(aprsConfig.serialPortModel.PortName, aprsConfig.serialPortModel.Baud, aprsConfig.serialPortModel.Parity,
                    8, StopBits.One, aprsConfig.serialPortModel.Handshake);
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }
            else if (aprsConfig.InputType == APRSInputType.TCP)
            {
                var tcpListener = new TCPPacketListener(aprsConfig.tcpConfigModel.Host, aprsConfig.tcpConfigModel.Port, aprsConfig.CallSign, aprsConfig.tcpConfigModel.Password);
                tcpListener.SoftwareName = "APRSLive-Depiction";
                tcpListener.SoftwareVersion = "0.95";
                tcpListener.UseCustomFilter = aprsConfig.tcpConfigModel.UseCustomFilter;
                tcpListener.CustomFilter = aprsConfig.tcpConfigModel.CustomFilter;
                if (regionBounds != null)
                    tcpListener.SetCenter(regionBounds.Center.Latitude, regionBounds.Center.Longitude);
                listener = tcpListener;
                encoder = new TcpPacketEncoder(aprsConfig.CallSign);
            }
            else if (aprsConfig.InputType == APRSInputType.AGWPE)
            {
                var agwpeListener = new AGWPEListener(aprsConfig.agwpeConfigModel.RadioPort, aprsConfig.agwpeConfigModel.Host, aprsConfig.agwpeConfigModel.TCPPort, aprsConfig.CallSign);
                listener = agwpeListener;
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }
            else
            {
                listener = new LoopbackPacketListener();
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }

            packetIO = new PacketIO(listener, encoder, aprsConfig.CallSign);
            return packetIO;
        }
#endregion

        #region Overrides
        public override void RefreshResult() { }
        #region setup
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            waitHandle.Reset();
            _messengerService = new APRSLiveMessenger(config.CallSign);
            _messengerService.MessageReceived += MessengerServiceMessageReceived;
            packetIO = CreatePacketIOFromConfig(config);
            return true;
        }
        #endregion
        #region stuff for starting
        protected override object ServiceAction(object args)
        {
            if (!StartPackeService()) return false;

            IsRunning = true;
            waitHandle.WaitOne();
            return null;
        }
        internal bool StartPackeService()
        {
            if (packetIO == null)
            {
                return false;
            }
            try
            {
                packetIO.Start();
                if (listener is TCPPacketListener && regionBounds != null)
                {
                    (listener as TCPPacketListener).SetCenter(regionBounds.Center.Latitude, regionBounds.Center.Longitude);
                }
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(ex.Message, 10);
                return false;
            }
            packetIO.PacketReceived += _PacketListener_PacketReceived;

            var elements = DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements.Where(e => e.ElementType == "Depiction.Plugin.APRSLiveFeed");
            if(!isConnectedToElementRepository)
            {
                isConnectedToElementRepository = true;
                DepictionAccess.CurrentDepiction.CompleteElementRepository.ElementListChange += CompleteElementRepository_ElementListChange;
            }
            try
            {
                lock (collectedElements)
                {
                    foreach (var element in elements)
                    {
                        var sInfo = new APRSStationInfo(TimeWindowMinutes) { Element = element };
                        string callSign;
                        if (element.GetPropertyValue("CallSign", out callSign))
                        {
                            collectedElements.Add(callSign, sInfo);
                        }
                    }
                }
            }
            catch
            {

            }

            return true;
        }
        #endregion
        #region Ending hep methods
        public override void StopBackgroundService()
        {
            StopService();
            ServiceStopRequested = true;
            waitHandle.Set();
            UpdateStatusReport("Cancelling");
        }
        internal void StopService()
        {
            if (packetIO != null)
            {
                packetIO.Stop();
                packetIO.PacketReceived -= _PacketListener_PacketReceived;
                
                DepictionAccess.CurrentDepiction.CompleteElementRepository.ElementListChange -= CompleteElementRepository_ElementListChange;
                isConnectedToElementRepository = false;
                packetIO = null;
            }
        }
        protected override void ServiceComplete(object args)
        {
            ServiceStopRequested = false;
            IsRunning = false;
            Debug.WriteLine("done with service");
        }
        #endregion
        #endregion
        #region message dialog stuff (what ever that is)
        //What is this supposed to do,and how is it used?
        public void OpenSendMessageDialog(object commandData)
        {
            var element = commandData as IDepictionElement;

            if (element != null)
            {
                string callSign;
                if (element.GetPropertyValue("CallSign", out callSign))
                {
                    SendMessageDialogShowModal(callSign);
                }
            }
        }
        //So confused as to what this is supposed to do and why it cares about the change in call sign
        public void SendMessageDialogShowModal(string destCallSign)
        {
            var message = new APRSMessage
            {
                DestinationCallSign = destCallSign,
                FromCallSign = config.CallSign
            };
            Conversation conv = _messengerService.GetConversation(destCallSign);
            var view = new MessageWithConversationView();

            var mwconvModel = new MessageWithConversationModel(message, conv);
            var mwconvVM = new MessageWithConversationViewModel(mwconvModel);//, this);
            mwconvVM.DestinationCallsignChanged += mwconv_DestinationCallsignChanged;
            view.DataContext = mwconvVM;
            view.Show();
        }
        private void mwconv_DestinationCallsignChanged(object sender, EventArgs e)
        {
            var mwconvViewModel = sender as MessageWithConversationViewModel;
            if (mwconvViewModel != null)
            {
                var conv = _messengerService.GetConversation(mwconvViewModel.MessageViewModel.DestinationCallSign);
                if (conv != mwconvViewModel.Model.Conversation)
                {
                    mwconvViewModel.Model.Conversation = conv;
                    mwconvViewModel.ConversationViewModel = new ConversationViewModel(conv);//, this);
                }
            }
        }
        #endregion
        #region Event connectors
        void CompleteElementRepository_ElementListChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            //What is this supposed to be doing?
            if (e.Action != NotifyCollectionChangedAction.Remove)
                return;
            foreach (var obj in e.OldItems)
            {
                if (obj.GetType() == typeof(List<IDepictionElement>))
                {
                    var elements = obj as List<IDepictionElement>;
                    if (elements != null)
                    {
                        foreach (var element in elements)
                        {
                            if (element.ElementType != "Depiction.Plugin.APRSLiveFeed")
                                continue;
                            string callSign;
                            if (element.GetPropertyValue("CallSign", out callSign))
                            {
                                if (collectedElements.ContainsKey(callSign))
                                    collectedElements.Remove(callSign);
                            }
                        }
                    }
                }
            }
        }

        /***
        * Seems to be the general packed listener. This is not controlled by the background service at all. Instead
        * the background service needs to stay running.
        * The packet listener appears to always be on and thus seems to not need a timer to recheck the status of what it is connectect to.
        * There appear to be 2 types of packets, the message type and the non message type
        * */
        private void _PacketListener_PacketReceived(object sender, PacketInfoEventArgs e)
        {
            var fromCallSign = e.PacketInfo.Callsign;
            APRSStationInfo stationInfo;
            if (e.PacketInfo.DataType == DataType.Message)
            {
                if (e.PacketInfo.MessageRecipient == config.CallSign)
                {
                    _messengerService.ProcessMessage(e.PacketInfo.Message, e.PacketInfo.Callsign, e.PacketInfo.MessageID);
                }
            }
            else
            {
                lock (collectedElements)
                {
                    if (collectedElements.TryGetValue(fromCallSign, out stationInfo))
                    {
                        stationInfo.UpdateStationFromPacket(e.PacketInfo);
                    }
                    else
                    {
                        stationInfo = APRSStationInfo.CreateNewFromPacket(10, e.PacketInfo);//TimeWindowMinutes, e.PacketInfo);
                        if (stationInfo != null)
                        {
                            var element = stationInfo.Element;
                            var story = DepictionAccess.CurrentDepiction;
                            if (story == null)
                                return;
                            if (config.CropToRegion)
                            {
                                var region = story.RegionBounds;
                                var pos = new DepictionGeometry(stationInfo.LastPosition.Position);
                                var regionGeom = new DepictionGeometry(region);
                                if (!pos.Within(regionGeom))
                                    return;
                            }
                            // TODO: figure out how to implement line below
                            //DepictionAccess.Librarian.AddBitmapToElement(element, stationInfo.Icon, stationInfo.IconKey);

                            if (BitmapSaveLoadService.AddImageWithKeyInAppResources(stationInfo.Icon, stationInfo.IconKey, true))
                            {
                                var depIconPath = new DepictionIconPath(IconPathSource.File, stationInfo.IconKey);
                                var property = new DepictionElementProperty("IconPath", "Icon path", depIconPath);
                                property.Deletable = false;
                                element.AddPropertyOrReplaceValueAndAttributes(property, false);
                            }

                            // TODO: reenable add command
                            if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                            {
                                //    element.AddCommand("Send Message", OpenSendMessageDialog, element);
                                var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                                cmd.Text = "Send Message";
                                ((DepictionElementParent)element).AddCommandToElement(cmd);

                            }
                            story.AddElementToDepictionElementList(element, true);
                            collectedElements.Add(fromCallSign, stationInfo);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
