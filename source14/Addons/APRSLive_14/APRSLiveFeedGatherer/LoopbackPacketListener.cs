using System;
using System.IO;
using System.Text;
using APRS;

namespace APRSLiveFeedGatherer
{
    internal class LoopbackPacketListener : PacketListenerBase
    {
        private const string  filename = @"c:\tmp\loopbackPackets";
        private readonly FileStream readStream;
        private readonly FileStream writeStream;

        #region Constructor
        public LoopbackPacketListener()
        {
           if (File.Exists(filename))
                  File.Delete(filename);
            readStream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
            //readStream.ReadTimeout = 100;
            writeStream  = new FileStream(filename, FileMode.Open, FileAccess.Write, FileShare.ReadWrite);
            
        }
        #endregion

        public override Stream GetReadStream()
        {
            return readStream;
        }

        public override Stream GetWriteStream()
        {
            return writeStream;
        }

        

    }
}