﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Windows;
using APRS;
using APRSLive.Core;
using APRSLiveConfiguration;
using APRSLiveConfiguration.Model;
using APRSLiveConfiguration.Properties;
using APRSLiveConfiguration.View;
using APRSLiveConfiguration.ViewModel;
using APRSLiveFeedGatherer.Model;
using APRSPacketIO;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.ValueTypes;

namespace APRSLiveFeedGatherer
{
//        [DepictionNonDefaultImporterMetadata("DepictionAPRSLive", DisplayName = "APRS Live Stations",ImporterSources = new[] { InformationSource.Web })]
    public class APRSLiveAddon : NondefaultDepictionImporterBase

//    [DepictionAddonBaseMetadata(AddonPackage = "APRSLive", DisplayName = "APRS Live Stations",
//        Company = "Smucker Data Solutions",
//        Description = "The APRS Live data feed allows your APRS-enabled radio to feed APRS position data into Depiction. F")]
//    public class APRSLiveAddon : IDepictionAddonBase
    {
        #region Stuff for base addon

            public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
            {
            }

//            public object AddonMainView
//        {
//            get { return null; }
//        }

       override public string AddonConfigViewText
        {
            get { return "Configure APRSLive"; }
        }

        override public object AddonConfigView
        {
            get
            {
                return GetNewConfigWin();
            }
        }

//        public object AddonActivationView
//        {
//            get { return null; }
//        }
//
//        public ViewModelBase AddinViewModel
//        {
//            get { return null; }
//        }
        #endregion
        #region Config stuff
        private APRSAddinConfigView GetNewConfigWin()
        {
            if(config == null)
            {
                try
                {
                    config = new APRSAddinConfig();
                }catch
                {
                    return null;
                }
            }

            var configViewModel = new APRSAddinConfigViewModel(config)
            {
                SerialPortTestConnectionCommand = new DelegateCommand(OpenSerialPortTestConsole),
                OpenAboutBoxCommand = new DelegateCommand(OpenAboutBox)
            };
            var view = new APRSAddinConfigView
            {
                DataContext = configViewModel,
                RunPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            //            var cfgWin = new ConfigDialogWindow { WindowStartupLocation = WindowStartupLocation.CenterOwner };
            //            cfgWin.MainGrid.Children.Add(view);

            return view;
        }
        private void OpenSerialPortTestConsole()
        {
            OpenSerialPortConsoleForConfig(config);
        }
        public static void OpenSerialPortConsoleForConfig(APRSAddinConfig cfg)
        {
            var window = new TestConsoleWindow();
            var view = new SerialPortTestConsole
            {
                Config =
                    new APRSLive.Core.SerialPortConfigModel(cfg.serialPortModel.PortName,
                                                             cfg.serialPortModel.Baud,
                                                             cfg.serialPortModel.Handshake,
                                                             cfg.serialPortModel.Parity)
            };
            window.MGrid.Children.Add(view);
            window.ShowDialog();
        }
        public static void OpenAboutBox()
        {
            //            var window = new AboutBoxWindow();
            var view = new AboutBox { VersionString = APRSLiveVersion.GetVersionStr() };
            //            window.MGrid.Children.Add(view);
            view.ShowDialog();
        }
        #endregion
        //These just cause issues
        //        #region Constructor
//        public APRSLiveAddon()
//            : this(new APRSAddinConfig())
//        {
//
//        }
//
//        public APRSLiveAddon(APRSAddinConfig config)
//        {
//            try
//            {
//                Config = config;
//            }
//            catch
//            {
//
//            }
//
//        }
//        #endregion

        #region Older stuff

        #region config stuff
        private APRSAddinConfig config;
        public APRSAddinConfig Config
        {
            get { return config; }
            set
            {
                if (config != null)
                    config.ConfigChangesApplied -= config_ConfigChangesApplied;
                config = value;
                config.ConfigChangesApplied += config_ConfigChangesApplied;
                ApplyConfigChanges();
            }
        }
        public void config_ConfigChangesApplied(object sender, EventArgs e)
        {
            ApplyConfigChanges();
        }
        private IPacketListener listener;
        private bool isStarted;
        private PacketIO packetIO;
        private readonly Dictionary<string, APRSStationInfo> collectedElements = new Dictionary<string, APRSStationInfo>();
        private IMapCoordinateBounds regionBounds;

        private void ApplyConfigChanges()
        {
            var wasStarted = isStarted;

            if (isStarted)
            {
                packetIO.Stop();
                packetIO.PacketReceived -= _PacketListener_PacketReceived;
                packetIO = null;
            }
            PacketEncoder encoder;

            if (config.InputType == APRSInputType.Serial)
            {
                listener = new SerialPacketListener(config.serialPortModel.PortName, config.serialPortModel.Baud, config.serialPortModel.Parity,
                    8, StopBits.One, config.serialPortModel.Handshake);
                encoder = new PacketEncoder(config.CallSign);
            }
            else if (config.InputType == APRSInputType.TCP)
            {
                var tcpListener = new TCPPacketListener(config.tcpConfigModel.Host, config.tcpConfigModel.Port, config.CallSign, config.tcpConfigModel.Password);
                tcpListener.SoftwareName = "APRSLive-Depiction";
                tcpListener.SoftwareVersion = "0.95";
                tcpListener.UseCustomFilter = config.tcpConfigModel.UseCustomFilter;
                tcpListener.CustomFilter = config.tcpConfigModel.CustomFilter;
                if (regionBounds != null)
                    tcpListener.SetCenter(regionBounds.Center.Latitude, regionBounds.Center.Longitude);
                listener = tcpListener;
                encoder = new TcpPacketEncoder(config.CallSign);
            }
            else if (config.InputType == APRSInputType.AGWPE)
            {
                var agwpeListener = new AGWPEListener(config.agwpeConfigModel.RadioPort, config.agwpeConfigModel.Host, config.agwpeConfigModel.TCPPort, config.CallSign);
                listener = agwpeListener;
                encoder = new PacketEncoder(config.CallSign);
            }
            else
            {
                listener = new LoopbackPacketListener();
                encoder = new PacketEncoder(config.CallSign);
            }

            packetIO = new PacketIO(listener, encoder, config.CallSign);
            packetIO.PacketReceived += _PacketListener_PacketReceived;
            if (wasStarted)
                packetIO.Start();
        }

        private void _PacketListener_PacketReceived(object sender, PacketInfoEventArgs e)
        {
            var fromCallSign = e.PacketInfo.Callsign;
            APRSStationInfo stationInfo;
            if (e.PacketInfo.DataType == DataType.Message)
            {
                if (e.PacketInfo.MessageRecipient == config.CallSign)
                {
                    ReceivedMessage(e.PacketInfo.Message, e.PacketInfo.Callsign, e.PacketInfo.MessageID);
                }
            }
            else
            {
                lock (collectedElements)
                {
                    if (collectedElements.TryGetValue(fromCallSign, out stationInfo))
                    {
                        stationInfo.UpdateStationFromPacket(e.PacketInfo);
                    }
                    else
                    {
                        stationInfo = APRSStationInfo.CreateNewFromPacket(config.TimeWindowMinutes, e.PacketInfo);
                        if (stationInfo != null)
                        {
                            var element = stationInfo.Element;
                            var story = DepictionAccess.CurrentDepiction;
                            if (story == null)
                                return;
                            if (config.CropToRegion)
                            {
                                var region = story.RegionBounds;
                                var pos = new DepictionGeometry(stationInfo.LastPosition.Position);
                                var regionGeom = new DepictionGeometry(region);
                                if (!pos.Within(regionGeom))
                                    return;
                            }
                            // TODO: figure out how to implement line below
                            //DepictionAccess.Librarian.AddBitmapToElement(element, stationInfo.Icon, stationInfo.IconKey);

                            if (BitmapSaveLoadService.AddImageWithKeyInAppResources(stationInfo.Icon, stationInfo.IconKey, true))
                            {
                                var depIconPath = new DepictionIconPath(IconPathSource.File, stationInfo.IconKey);
                                var property = new DepictionElementProperty("IconPath", "Icon path", depIconPath);
                                property.Deletable = false;
                                element.AddPropertyOrReplaceValueAndAttributes(property, false);
                            }

                            // TODO: reenable add command
                            if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                            {
                                //    element.AddCommand("Send Message", OpenSendMessageDialog, element);
                                var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                                cmd.Text = "Send Message";
                                ((DepictionElementParent)element).AddCommandToElement(cmd);

                            }
                            story.AddElementToDepictionElementList(element, true);
                            collectedElements.Add(fromCallSign, stationInfo);
                        }
                    }
                }
            }
        }
        public void OpenSendMessageDialog(object commandData)
        {
            var element = commandData as IDepictionElement;

            if (element != null)
            {
                string callSign;
                if (element.GetPropertyValue("CallSign", out callSign))
                    SendMessageDialogShowModal(callSign);
            }
        }
        public void SendMessageDialogShowModal(string destCallSign)
        {
            //            var message = new APRSMessage
            //            {
            //                DestinationCallSign = destCallSign,
            //                FromCallSign = config.CallSign
            //            };
            //            Conversation conv = GetConversation(destCallSign);
            //            var view = new MessageWithConversationView();
            //
            //            var mwconvModel = new MessageWithConversationModel(message, conv);
            //            var mwconvVM = new MessageWithConversationViewModel(mwconvModel, this);
            //            mwconvVM.DestinationCallsignChanged += mwconv_DestinationCallsignChanged;
            //            view.DataContext = mwconvVM;
            //            var dlg = new MessageWindow();
            //            dlg.MainGrid.Children.Add(view);
            //            dlg.Show();
        }
        public event EventHandler<APRSMessageEventArgs> MessageReceived;
        private void ReceivedMessage(string message, string fromCallSign, string messageId)
        {
            if (message == "ack") return;
            APRSStationInfo stationInfo;
            var msg = new APRSMessage
            {
                DestinationCallSign = config.CallSign,
                FromCallSign = fromCallSign,
                Message = message,
                MessageID = messageId,
                Timestamp = DateTime.Now
            };
            if (collectedElements.TryGetValue(fromCallSign, out stationInfo))
            {
                stationInfo.ReceivedMessageFromStation(msg);
            }

            var conversation = GetConversation(fromCallSign);
            conversation.AddMessage(msg);
            if (MessageReceived != null)
                MessageReceived(this, new APRSMessageEventArgs(msg));
            packetIO.SendAck(fromCallSign, msg.MessageID);
        }
        private readonly Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();
        public Conversation GetConversation(string destCallSign)
        {
            Conversation conv;
            if (!conversations.TryGetValue(destCallSign, out conv))
            {
                conv = new Conversation();
                conversations.Add(destCallSign, conv);
            }
            return conv;
        }
        #endregion
        #endregion
    }
}