﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace APRSLiveAddinTestJig
{
    public class LElement: DepictionElementParent
    {
        private LatitudeLongitude position;
//        private Dictionary<string, IProperty> properties = new Dictionary<string, IProperty>();
//        public LElement()
//        {
//            ElementKey = Guid.NewGuid().ToString();
//        }

//        public void Delete()
//        {
//            
//        }

//        public event DeletedEvent OnDeleted;
//        public event PropertyChangedEventHandler PropertyChanged;
//        public IElement[] Children
//        {
//            get { return null; }
//        }

        public bool Updated
        {
            get { throw new NotImplementedException(); }
            set {  }
        }

//        public IElement Parent
//        {
//            get { return null; }
//            set { }
//        }



//        public void NotifyPropertyChanged(string propertyName)
//        {
//            if (!Application.Current.CheckAccess())
//            {
//                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<string>(NotifyPropertyChanged), propertyName);
//                return;
//            }
//
//            if (PropertyChanged != null)
//            {
//                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
//            }
//        }

//        public void AddChild(IElement child)
//        {
//            
//        }
//
//        public void RemoveChild(IElement child)
//        {
//            
//        }
//
//        public bool SetImage(ImageObjectMetadata visual)
//        {
//            return false;
//        }
//
//        public void InsertChild(int index, IElement child)
//        {
//            
//        }

        public string LabelData
        {
            get { return null;}
            set { }
        }

        public void CreateLabel(string labelData, double labelWidth, double labelHeight)
        {
            
        }

        public bool Intersects(IDepictionElement otherElement)
        {
            return false;
        }

        public Visibility Visibility
        {
            get { return System.Windows.Visibility.Visible; }
            set { }
        }

        private ImageSource groupIconSource;
        public ImageSource GroupIconSource
        {
            get { return groupIconSource; }
            set 
            {
                groupIconSource = value;
//                PropertyChanged("GroupIconSource");
            }
        }


//        public void ClearZoneOfInfluence()
//        {
//            throw new System.NotImplementedException();
//        }
//
//        public bool RemoveProperty(IProperty propertyToRemove)
//        {
//            throw new System.NotImplementedException();
//        }

//        public void SetPosition(LatitudeLongitude newPosition)
//        {
//            position = newPosition;
//        }

//        private LatitudeLongitude Position
//        {
//            get { return position;}
//            set { throw new NotImplementedException(); }
//        }

//        public void UpdateInteractions()
//        {
//            
//        }

//        public IProperty GetProperty(string propertyName)
//        {
//            IProperty prop;
//            if (properties.TryGetValue(propertyName, out prop))
//                return prop;
//            return null;
//        }
//
//        public bool HasProperty(string propertyName)
//        {
//            return properties.ContainsKey(propertyName);
//        }

        public IZoneOfInfluence ZoneOfInfluence
        {
            get { throw new System.NotImplementedException(); }
        }

        public void SetElementAreaAndLengthProperty()
        {
            throw new NotImplementedException();
        }

        public string ElementType { get; set;}

        public string HoverText
        {
            get { return ElementType; }
        }

        public string HoverTextOneLine
        {
            get { return ElementType; }
        }

//        public IProperty GetPropertyByDisplayName(string displayName)
//        {
//            throw new System.NotImplementedException();
//        }
//
//        public IProperty[] Properties
//        {
//            get { return properties.Values.ToArray(); }
//        }

//        public bool IsGeoLocated
//        {
//            get { return true;; }
//        }

//        public ImageObjectMetadata ImageMetaData
//        {
//            get { throw new System.NotImplementedException(); }
//            set { throw new System.NotImplementedException(); }
//        }

//        public ImageSource IconSource
//        {
//            get { throw new System.NotImplementedException(); }
//        }
//
//        public bool AddProperty(IProperty property)
//        {
//            properties.Add(property.Name, property);
//            return true;
//        }
//        public override string ToString()
//        {
//            var prop = GetProperty("CallSign");
//            return prop.Value.ToString();
//        }
//
//        public string DisplayName 
//        {
//            get
//            {
//                var prop = GetProperty("CallSign");
//                return prop.Value.ToString();
//            }
//        }
//
//        public string ElementKey { get; set;}
//
//        public string TypeDisplayName
//        {
//            get { throw new System.NotImplementedException(); }
//            set { }
//        }

        public void AddAlarm(string alarm)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveAlarm(string alarm)
        {
            throw new System.NotImplementedException();
        }

        public void InvalidateIcon()
        {
            throw new System.NotImplementedException();
        }

//        public event Action<IElement> ChildAdded;
//        public event Action<IElement> ChildRemoved;
//        public void SetElementIconPath(IconPath iconPath)
//        {
//            throw new System.NotImplementedException();
//        }

//        public void AddCommand(string displayName, Func<object, CommandResult> executeAction, object commandData)
//        {
//            //throw new NotImplementedException();
//        }
    }
}
