﻿using System;
using System.Windows;
using Depiction.APINew.Interfaces.MessagingInterface;

namespace APRSLiveAddinTestJigWpf
{
    public class LimitedNotificationService: IDepictionNotificationService
    {
        public void SendNotification(string message)
        {
            Console.WriteLine(message);
            if (NotificationReceived != null)
                NotificationReceived(message);
        }

        public event Action<string> NotificationReceived;


        public event Action MessageAdded;
        public bool DisplayMessage(IDepictionMessage message)
        {
            throw new System.NotImplementedException();
        }

        public bool DisplayMessageString(string simpleMessage)
        {
            throw new System.NotImplementedException();
        }

        public bool DisplayMessageString(string simpleMessage, int timeSeconds)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveMessage(IDepictionMessage message)
        {
            throw new System.NotImplementedException();
        }

        public void SendNotificationDialog(string message)
        {
            throw new System.NotImplementedException();
        }

        public void SendNotificationDialog(string message, string title)
        {
            throw new System.NotImplementedException();
        }

        public void SendNotificationDialog(Window owner, string message, string title)
        {
            throw new System.NotImplementedException();
        }

        public MessageBoxResult DisplayInquiryDialog(Window owner, string message, string title, MessageBoxButton buttonOptions)
        {
            throw new System.NotImplementedException();
        }

        public MessageBoxResult DisplayInquiryDialog(string message, string title, MessageBoxButton buttonOptions)
        {
            throw new System.NotImplementedException();
        }
    }
}
