using System;
using System.Collections.Generic;
using Depiction.API.Interface;
using Depiction.APINew.OldValidationRules;

namespace APRSLiveAddinTestJigWpf
{
    public class LimitedPropertyFactory : IPropertyFactory
    {
        public IProperty CreateProperty(string name, string displayName, object value, IValidationRule[] validationRules, Dictionary<string, string[]> postSetActions, bool deletable, bool visible)
        {
            var prop = new LimitedProperty {Name = name, Value = value};
            return prop;
        }

        public IProperty CreateProperty(string name, string displayName, object value, IValidationRule[] validationRules, Dictionary<string, string[]> postSetActions, bool deletable, bool visible, bool editable)
        {
            throw new NotImplementedException();
        }
    }
}