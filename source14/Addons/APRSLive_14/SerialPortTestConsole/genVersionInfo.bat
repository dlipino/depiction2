REM Generates versionInfo.cs file.
svnversion .. | cut -d: -f2 |cut -dM -f1>tmprevnumber
set /p rev= <tmprevnumber
rm tmprevnumber

echo [assembly: System.Reflection.AssemblyVersion("1.1.0.%rev%")] >VersionInfo.cs