using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace APRS
{
    public interface IPacketListener
    {
        bool IsListening { get; }
        void Start();
        void Stop();
        event EventHandler<PacketInfoEventArgs> PacketReceived;
        event EventHandler<LineReceivedEventArgs> LineReceived;
        void SendString(string str);
        string CallSign { get; set; }

       
    }

    public class PacketListenerBase : IPacketListener
    {
        protected const int bufSize = 256;
        protected virtual char TerminationChar { get { return '\n';} }
        protected bool closed = true;
        protected readonly byte[] readBuffer = new byte[bufSize];
        public virtual bool IsListening { get { return false; } }
        public virtual void Start(){ BeginListening();}
        public virtual void Stop()
        {
            if (closed)
                return;
            closed = true;
            var stream = GetReadStream();
            if (stream != null)
                stream.Close();

        }
        
        public PacketListenerBase()
        {
            packetParser = new PacketParser();
            packetParser.LineReceived += packetParser_LineReceived;
        }

        private void packetParser_LineReceived(object sender, LineReceivedEventArgs e)
        {
            if (LineReceived != null)
                LineReceived(this, e);
        }

        public event EventHandler<LineReceivedEventArgs> LineReceived;
        public event EventHandler<PacketInfoEventArgs> PacketReceived; 
        public virtual Stream GetReadStream() {return null;}
        protected PacketParser packetParser;
        protected void BeginListening()
        {
            closed = false;
            var stream = GetReadStream();
            stream.BeginRead(readBuffer, 0, bufSize, receiveCallback, null);
        }


        public virtual void SendString(string str)
        {
            Console.WriteLine("Sending " + str);
            str += TerminationChar;
            var bytes = Encoding.ASCII.GetBytes(str);
            var stream = GetWriteStream();

            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }

        public string CallSign { get; set;}

        private void receiveCallback(IAsyncResult ar)
        {
            if (closed) return;
            var stream = GetReadStream();
            lock (this)
            {
                var bytesRead = stream.EndRead(ar);

                
                string data = null;
                if (bytesRead > 0)
                {
                    data = Encoding.ASCII.GetString(readBuffer, 0, bytesRead);
                }
                //copy data quickly out of buffer, then trigger asynchronous read again
                if (stream.CanRead)
                    stream.BeginRead(readBuffer, 0, bufSize, receiveCallback, null);

                if (data != null)
                {
                    List<PacketInfo> packets = packetParser.GetPackets(data);
                    if (PacketReceived != null)
                    {
                        foreach (var pkt in packets)
                            PacketReceived(this, new PacketInfoEventArgs(pkt));
                    }
                }
            }
            

        }

        public virtual Stream GetWriteStream()
        {
            return GetReadStream();
        }
    }
    public class LineReceivedEventArgs : EventArgs
    {
        public string Line { get; set; }
        public LineReceivedEventArgs(string line)
        {
            Line = line;
        }

    }
}