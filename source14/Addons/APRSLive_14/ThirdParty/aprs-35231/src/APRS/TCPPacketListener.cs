using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace APRS
{
    public class TCPPacketListener : PacketListenerBase
    {
        public string Uri { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Callsign { get; set; }
        public string CustomFilter { get; set; }

        public string SoftwareName { get; set; }
        public string SoftwareVersion { get; set; }
        private TcpClient tcpClient;

        public override System.IO.Stream GetReadStream()
        {
            if (tcpClient != null)
                return tcpClient.GetStream();
            return null;
        }

       
        public TCPPacketListener(string uri, int port, string callsign, string password)
        {
            Uri = uri;
            Port = port;
            Callsign = callsign;
            Password = password;
            
        }
        public void SetCenter(double lat, double lon)
        {
            latitude = lat;
            longitude = lon;
            
        }
        private double  latitude { get; set; }
        private double longitude { get; set; }
        public bool UseCustomFilter { get; set; }

        public TCPPacketListener()
        {
            var config = AprsConfig.GetConfig();
            Uri = config.URI;
            Port = config.Port;
            Callsign = config.Callsign;
            Password = config.Password;
            CustomFilter = config.Filter;
        }

        
        
        public override void  Start()

        {
            if (tcpClient != null)
                Stop();

            tcpClient = new TcpClient(Uri, Port);
            var versString = string.Empty;
            if (!string.IsNullOrEmpty(SoftwareName))
                versString = string.Format("vers {0} {1}", SoftwareName, SoftwareVersion);
            string filter;
            if (UseCustomFilter)
                filter = CustomFilter;
            else
                filter = string.Format("r/{0:F2}/{1:F2}/100", latitude, longitude);

            Thread.Sleep(500);
            var loginStr = string.Format("user {0} pass {1} {2} filter {3} /r \n", Callsign, Password, versString, filter);
            SendString(loginStr);
            BeginListening();
        }

        public override void Stop()
        {
            base.Stop();
            if (tcpClient != null && tcpClient.Connected)
                tcpClient.Close();
            tcpClient = null;
        }
        
       
    }

}