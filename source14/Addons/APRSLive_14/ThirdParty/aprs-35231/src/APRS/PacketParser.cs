﻿using System;
using System.Collections.Generic;


namespace APRS
{
    public class PacketParser
    {
        public string msgFragment;
        public List<string> unparsedLines = new List<string>();
        private PacketInfo partialPacket;
        public event EventHandler<LineReceivedEventArgs> LineReceived;
        public List<PacketInfo> GetPackets(string data)
        {
            if (msgFragment != null)
            {
                data = msgFragment + data;
                msgFragment = null;
            }
            var hasFragment = !lastLineComplete(data);

            //var lines = data.Split('\n');
            var lines = data.Split(new [] { '\r', '\n' });
            if (lines.Length > 0)
            {
                if (hasFragment)
                {
                    var lastLine = lines[lines.Length - 1];
                    msgFragment = lastLine;
                    lines[lines.Length - 1] = string.Empty;
                }

            }
            var packets = new List<PacketInfo>();
            foreach (var s in lines)
            {
                var line = s.TrimEnd('\r');
                if (line == string.Empty) continue;
                if (LineReceived != null)
                    LineReceived(this, new LineReceivedEventArgs(line));
                PacketInfo pkt;
                if (partialPacket != null)
                {
                    pkt = partialPacket;
                    partialPacket = null;
                    if ((DateTime.UtcNow - pkt.ReceivedDate).TotalSeconds < 2)  // partial packets need to disappear after 2 seconds
                    {
                        try
                        {
                            pkt.ParseFromAX25Data(line);
                        }
                        catch (Exception e)
                        {
                            //continue;
                        }
                        if (pkt.DataParsed)
                        {
                            packets.Add(pkt);
                            continue;
                        }
                    }
                }
                pkt = ParseLine(line);
                if (pkt != null)
                    if (pkt.DataParsed) 
                        packets.Add(pkt);
                    else
                        partialPacket = pkt;    
                else
                {
                    unparsedLines.Add(s);
                }
            }
            return packets;
        }

        private static bool lastLineComplete(string data)
        {
            return data.EndsWith("\n");
        }

        public static PacketInfo ParseLine(string data)
        {
            if (data.StartsWith("#"))
                return null;
            
            try
            {
                return new PacketInfo(data);
            }
            catch (Exception e)
            {
                //TODO: log/show an error
                return null;
            }


        }
    }

   
}
