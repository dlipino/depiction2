﻿This is the source code for the APRS.dll, which was derived from a project on CodePlex,
aprs.codeplex.com and released under the LGPL open source license version 2.1

See LICENSE-LGPL.txt for actual license terms.
