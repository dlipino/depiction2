using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;

namespace APRS
{
    public sealed class PacketInfo
    {
        private static readonly Regex _AltitudeRegex = new Regex(@"A\=(\d\d\d\d\d\d)", RegexOptions.Compiled);

        private static readonly Dictionary<string, MicEMessageType> _CustomMicEMessageTypeMap = new Dictionary<string, MicEMessageType>();
        private static readonly Dictionary<char, DataType> _DataTypeMap = new Dictionary<char, DataType>();
        private static readonly Dictionary<char, Symbol> _PrimarySymbolTableSymbolMap = new Dictionary<char, Symbol>();
        private static readonly Dictionary<char, Symbol> _SecondarySymbolTableSymbolMap = new Dictionary<char, Symbol>();
        private static readonly Dictionary<string, MicEMessageType> _StandardMicEMessageTypeMap = new Dictionary<string, MicEMessageType>();
        private static readonly Dictionary<char, SymbolTable> _SymbolTableMap = new Dictionary<char, SymbolTable>();

        static PacketInfo()
        {
            _DataTypeMap[(char) 0x1c] = DataType.CurrentMicERev0;
            _DataTypeMap[(char) 0x1d] = DataType.OldMicERev0;
            _DataTypeMap['!'] = DataType.PositionWithoutTimestampNoAprsMessaging;
            _DataTypeMap['#'] = DataType.PeetBrosUiiWxStation;
            _DataTypeMap['$'] = DataType.RawGpsDataOrUltimeter2000;
            _DataTypeMap['%'] = DataType.AgreloDfJrMicroFinder;
            _DataTypeMap['\''] = DataType.OldMicE;
            _DataTypeMap[')'] = DataType.Item;
            _DataTypeMap['*'] = DataType.PeetBrosUiiWxStation;
            _DataTypeMap['+'] = DataType.ShelterDataWithTime;
            _DataTypeMap[','] = DataType.InvalidOrTestData;
            _DataTypeMap['.'] = DataType.SpaceWeather;
            _DataTypeMap['/'] = DataType.PositionWithTimestampNoAprsMessaging;
            _DataTypeMap[':'] = DataType.Message;
            _DataTypeMap[';'] = DataType.Object;
            _DataTypeMap['<'] = DataType.StationCapabilities;
            _DataTypeMap['='] = DataType.PositionWithoutTimestampWithAprsMessaging;
            _DataTypeMap['>'] = DataType.Status;
            _DataTypeMap['?'] = DataType.Query;
            _DataTypeMap['@'] = DataType.PositionWithTimestampWithAprsMessaging;
            _DataTypeMap['T'] = DataType.TelemetryData;
            _DataTypeMap['['] = DataType.MaidenheadGridLocatorBeacon;
            _DataTypeMap['_'] = DataType.WeatherReportWithoutPosition;
            _DataTypeMap['`'] = DataType.CurrentMicE;
            _DataTypeMap['{'] = DataType.UserDefinedAprsPacketFormat;
            _DataTypeMap['}'] = DataType.ThirdPartyTraffic;

            _StandardMicEMessageTypeMap["111"] = MicEMessageType.OffDuty;
            _StandardMicEMessageTypeMap["110"] = MicEMessageType.EnRoute;
            _StandardMicEMessageTypeMap["101"] = MicEMessageType.InService;
            _StandardMicEMessageTypeMap["100"] = MicEMessageType.Returning;
            _StandardMicEMessageTypeMap["011"] = MicEMessageType.Committed;
            _StandardMicEMessageTypeMap["010"] = MicEMessageType.Special;
            _StandardMicEMessageTypeMap["001"] = MicEMessageType.Priority;

            _CustomMicEMessageTypeMap["111"] = MicEMessageType.Custom0;
            _CustomMicEMessageTypeMap["110"] = MicEMessageType.Custom1;
            _CustomMicEMessageTypeMap["101"] = MicEMessageType.Custom2;
            _CustomMicEMessageTypeMap["100"] = MicEMessageType.Custom3;
            _CustomMicEMessageTypeMap["011"] = MicEMessageType.Custom4;
            _CustomMicEMessageTypeMap["010"] = MicEMessageType.Custom5;
            _CustomMicEMessageTypeMap["001"] = MicEMessageType.Custom6;

            _SymbolTableMap['/'] = SymbolTable.Primary;
            _SymbolTableMap['\\'] = SymbolTable.Secondary;

            //_PrimarySymbolTableSymbolMap['\''] = Symbol.Aircraft;
            //_SecondarySymbolTableSymbolMap['`'] = Symbol.Aircraft;
        }
        public PacketInfo()
        {
            
        }

        public PacketInfo(string callsign, string stationDest, string stationRoute, string ax25Data)
        {
            Callsign = callsign;
            var list = new List<string>();
            list.Add(stationDest);
            list.AddRange(stationRoute.Split(','));
            StationRoute = new ReadOnlyCollection<string>(list);
            ParseFromAX25Data(ax25Data);
        }

        public PacketInfo(string rawData)
        {
            ReceivedDate = DateTime.UtcNow;

            //Parse out the callsign
            var match = Regex.Match(rawData, @"^([^\>]*)\>(.*)$");
            Callsign = match.Groups[1].Value;
            var yetToParse = match.Groups[2].Value;


            match = Regex.Match(yetToParse, @"^([^\:]*)\:(.*)$");
            StationRoute = new ReadOnlyCollection<string>(match.Groups[1].Value.Split(','));
            yetToParse = match.Groups[2].Value;
            // a test to filter out things like "<UI>:" which are actually ax.25 frame information
            // which Kantronics adds to packets by default. setting: MHEADER ON.  These do not get printed if MHEADER OFF.
            match = Regex.Match(yetToParse, @"^[\s ](\<.*\>[\s]*?:)(.*)$");
            if (match.Groups.Count > 1 && match.Groups[2].Success)
                yetToParse = match.Groups[2].Value;
            if (yetToParse.Length > 0)                
                ParseFromAX25Data(yetToParse);
        }

        public void ParseFromAX25Data(string ax25Data)
        {
            string yetToParse = ax25Data;
            Match match;
            DataType dataType;
            if (!_DataTypeMap.TryGetValue(yetToParse[0], out dataType))
                throw new ArgumentException("Unsupported data type in raw data", yetToParse);

            DataType = dataType;
            DataParsed = true;


            switch (DataType)
            {
                case DataType.Message:
                    match = Regex.Match(yetToParse, @"^:(.*?)\:(.*)\{(.*)$");
                    if (match.Success)
                    {
                        MessageRecipient = match.Groups[1].Value.Trim();
                        Message = match.Groups[2].Value;
                        if (match.Groups[3].Success)
                            MessageID = match.Groups[3].Value;
                    }
                    else
                    {
                        match = Regex.Match(yetToParse, @"^:(.*?)\:(.*)$");
                        if (match.Success)
                        {
                            MessageRecipient = match.Groups[1].Value.Trim();
                            Message = match.Groups[2].Value.Trim();
                        }
                    }

                    break;    
                case DataType.OldMicE:
                case DataType.CurrentMicE:
                    {
                        Latitude latitude;
                        short longitudeOffset;
                        LongitudeHemisphere longitudeHemisphere;
                        MicEMessageType micEMessageType;
                        var routeData = StationRoute[0];
                        if (routeData.Length > 6)
                            routeData = routeData.Substring(0, 6);

                        decodeMicEDestinationAddress(
                            routeData,
                            out latitude,
                            out longitudeOffset,
                            out longitudeHemisphere,
                            out micEMessageType);
                        

                        Latitude = latitude;
                        MicEMessageType = micEMessageType;

                        var longitudeDegrees = (short) (yetToParse[1] - 28 + longitudeOffset);
                        if (180 <= longitudeDegrees && longitudeDegrees <= 189)
                            longitudeDegrees -= 80;
                        else if (190 <= longitudeDegrees && longitudeDegrees <= 199)
                            longitudeDegrees -= 190;

                        Longitude = new Longitude(
                            longitudeDegrees,
                            (short) ((yetToParse[2] - 28)%60),
                            (yetToParse[3] - 28)*0.6,
                            longitudeHemisphere,
                            latitude.Ambiguity);

                        var speedCourseSharedByte = (yetToParse[5] - 28);
                        Speed = Speed.FromKnots(((yetToParse[4] - 28)*10 + (int) Math.Floor(speedCourseSharedByte/10.0))%800);
                        Direction = new Heading((short) (((speedCourseSharedByte%10*100) + (yetToParse[6] - 28))%400), 0, 0);
                        Symbol = LookupSymbol(yetToParse[8], yetToParse[7]);
                        RawSymbol = yetToParse[7];

                        if (yetToParse.Length > 12 && yetToParse[12] == '}')
                        {
                            Altitude = Altitude.FromMetersAboveBaseline(convertFromBase91(yetToParse.Substring(9, 3)));
                        }
                    }
                    break;
                case DataType.PositionWithoutTimestampWithAprsMessaging:
                case DataType.PositionWithoutTimestampNoAprsMessaging:
                    Latitude = new Latitude(Convert.ToInt16(yetToParse.Substring(1, 2)), Convert.ToDouble(yetToParse.Substring(3, 5)), yetToParse[8] == 'N' ? LatitudeHemisphere.North : LatitudeHemisphere.South);
                    
                    Longitude = new Longitude(Convert.ToInt16(yetToParse.Substring(10, 3)), Convert.ToDouble(yetToParse.Substring(13, 5)), yetToParse[18] == 'E' ? LongitudeHemisphere.East : LongitudeHemisphere.West);
                    Symbol = LookupSymbol(yetToParse[9], yetToParse[19]);
                    RawSymbol = yetToParse[19];
                    yetToParse = yetToParse.Substring(20);
                    if (Regex.IsMatch(yetToParse, @"^\d\d\d\\\d\d\d"))
                    {
                        Direction = new Heading(Convert.ToInt16(yetToParse.Substring(0, 3)), 0, 0);
                        Speed = Speed.FromKnots(Convert.ToDouble(yetToParse.Substring(4, 3)));
                        yetToParse = yetToParse.Substring(7);
                    }
                    if (_AltitudeRegex.IsMatch(yetToParse))
                        Altitude = Altitude.FromFeetAboveSeaLevel(Convert.ToInt32(_AltitudeRegex.Match(yetToParse).Groups[1].Value));
                    break;
                case DataType.PositionWithTimestampWithAprsMessaging:
                case DataType.PositionWithTimestampNoAprsMessaging:
                    if (Regex.IsMatch(yetToParse.Substring(1), @"^(\d\d\d\d\d\d)(z|h|/)"))
                    {
                        yetToParse = yetToParse.Substring(8);
                    }
                    else
                    {
                        return;
                    }
                    Latitude = new Latitude(Convert.ToInt16(yetToParse.Substring(0, 2)), Convert.ToDouble(yetToParse.Substring(2, 5)), yetToParse[7] == 'N' ? LatitudeHemisphere.North : LatitudeHemisphere.South);
                    SymbolTable = _SymbolTableMap[yetToParse[8]];
                    Symbol = LookupSymbol(yetToParse[8], yetToParse[18]);
                    RawSymbol = yetToParse[18];
                    Longitude = new Longitude(Convert.ToInt16(yetToParse.Substring(9, 3)), Convert.ToDouble(yetToParse.Substring(12, 5)), yetToParse[17] == 'E' ? LongitudeHemisphere.East : LongitudeHemisphere.West);
                    if (RawSymbol == '_')
                    {
                        WeatherInfo = WeatherInfo.ParseWith7ByteWindData(yetToParse.Substring(19));
                        break;
                    }
                    Direction = new Heading(Convert.ToInt16(yetToParse.Substring(19, 3)), 0, 0);
                    Speed = Speed.FromKnots(Convert.ToDouble(yetToParse.Substring(23, 3)));
                    yetToParse = yetToParse.Substring(26);
                    if (_AltitudeRegex.IsMatch(yetToParse))
                        Altitude = Altitude.FromFeetAboveSeaLevel(Convert.ToInt32(_AltitudeRegex.Match(yetToParse).Groups[1].Value));
                    
                    break;
                case DataType.WeatherReportWithoutPosition:
                    yetToParse = yetToParse.Substring(9);
                    WeatherInfo = WeatherInfo.ParsePositionless(yetToParse);

                    break;
                case DataType.Status:
                    StatusMessage = yetToParse;
                    break;
                case DataType.RawGpsDataOrUltimeter2000:
                    if (yetToParse.Substring(0, 5) == "$ULTW")
                    {
                        WeatherInfo = WeatherInfo.ParseUltimeter(yetToParse.Substring(5));
                        break;
                    }
                    DataParsed = false;
                    break;
                default:
                    DataParsed = false;
                    break;

            }
        }


        private static Symbol LookupSymbol(char symTableType, char symbolType)
        {
            try
            {
                var symTable = _SymbolTableMap[symTableType];

                try
                {
                    if (symTable == SymbolTable.Primary)
                        return _PrimarySymbolTableSymbolMap[symbolType];
                    else
                        return _SecondarySymbolTableSymbolMap[symbolType];
                }
                catch (Exception e)
                {
                    Console.WriteLine("Problem with symbol {0}", symbolType);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Problem decoding symbol table {0}", symTableType);
            }
            return Symbol.Notfound;
        }

        public string Callsign { get; set; }
        public ReadOnlyCollection<string> StationRoute { get; private set; }
        public DataType DataType { get; set; }
        public Latitude Latitude { get; private set; }
        public Longitude Longitude { get; private set; }
        public Altitude Altitude { get; private set; }
        public Heading Direction { get; private set; }
        public Speed Speed { get; private set; }
        public SymbolTable SymbolTable { get; private set; }
        public Symbol Symbol { get; private set; }
        public char RawSymbol { get; private set; }
        public string Message { get; set; }
        public MicEMessageType MicEMessageType { get; private set; }
        public DateTime ReceivedDate { get; private set; }
        public string MessageRecipient { get; set; }
        public string MessageID { get; set; }
        public string StatusMessage { get; set; }
        public WeatherInfo WeatherInfo { get; set; }
        public bool DataParsed { get; set; }
        
        

        private static int convertFromBase91(string data)
        {
            var returnValue = 0;

            for (var i = 0; i < data.Length; i++)
                returnValue += Convert.ToInt32(Math.Pow(91, data.Length - i - 1))*(data[i] - 33);

            return returnValue;
        }

        private static void decodeMicEDestinationAddress(
            string data,
            out Latitude latitude,
            out short longitudeOffset,
            out LongitudeHemisphere longitudeHemisphere,
            out MicEMessageType micEMessageType)
        {
            if (string.IsNullOrEmpty(data) || data.Length != 6)
                throw new ArgumentException("Data must be a six character string", "data");

            var sbLatitude = new StringBuilder();
            var sbMicEMessageCode = new StringBuilder();

            var isStandardMessage = false;
            var isCustomMessage = false;
            var latitudeHemisphere = default(LatitudeHemisphere);
            //latitude = null;
            longitudeOffset = 0;
            longitudeHemisphere = default(LongitudeHemisphere);
            //micEMessageType = MicEMessageType.Unknown;

            for (var p = 0; p < 6; p++)
            {
                var c = data[p];
                if (c >= '0' && c <= '9' || c == 'L')
                {
                    if (c == 'L')
                        sbLatitude.Append(" ");
                    else
                        sbLatitude.Append(c - '0');

                    if (p < 3)
                        sbMicEMessageCode.Append('0');
                    else
                        switch (p)
                        {
                            case 3:
                                latitudeHemisphere = LatitudeHemisphere.South;
                                break;
                            case 4:
                                longitudeOffset = 0;
                                break;
                            case 5:
                                longitudeHemisphere = LongitudeHemisphere.East;
                                break;
                        }
                }
                else if (c >= 'A' && c <= 'K')
                {
                    if (c == 'K')
                        sbLatitude.Append(" ");
                    else
                        sbLatitude.Append(c - 'A');

                    if (p < 3)
                    {
                        sbMicEMessageCode.Append(1);
                        isCustomMessage = true;
                    }
                    else
                    {
                        throw new ArgumentException("Invalid data: " + data);
                    }
                }
                else if (c >= 'P' && c <= 'Z')
                {
                    if (c == 'Z')
                        sbLatitude.Append(" ");
                    else
                        sbLatitude.Append(c - 'P');

                    if (p < 3)
                    {
                        sbMicEMessageCode.Append(1);
                        isStandardMessage = true;
                    }
                    else
                        switch (p)
                        {
                            case 3:
                                latitudeHemisphere = LatitudeHemisphere.North;
                                break;
                            case 4:
                                longitudeOffset = 100;
                                break;
                            case 5:
                                longitudeHemisphere = LongitudeHemisphere.West;
                                break;
                        }
                }
                else
                {
                    throw new ArgumentException("Invalid data: " + data);
                }
            }

            short degrees, minutes;
            double seconds;
            short latitudeAmbiguity;
            parseLatitudeValue(sbLatitude.ToString(), out degrees, out minutes, out seconds, out latitudeAmbiguity);

            latitude = new Latitude(degrees, minutes, seconds, latitudeHemisphere, latitudeAmbiguity);

            if (isStandardMessage && isCustomMessage)
            {
                micEMessageType = MicEMessageType.Unknown;
            }
            else if (isStandardMessage)
            {
                if (!_StandardMicEMessageTypeMap.TryGetValue(sbMicEMessageCode.ToString(), out micEMessageType))
                    throw new InvalidOperationException("Invalid MicE Message Code: " + sbMicEMessageCode);
            }
            else if (isCustomMessage)
            {
                if (!_CustomMicEMessageTypeMap.TryGetValue(sbMicEMessageCode.ToString(), out micEMessageType))
                    throw new InvalidOperationException("Invalid MicE Message Code: " + sbMicEMessageCode);
            }
            else
            {
                micEMessageType = MicEMessageType.Emergency;
            }
        }

        private static void parseLatitudeValue(string data, out short degrees, out short minutes, out double seconds, out short ambiguity)
        {
            ambiguity = 0;
            for (var i = data.Length - 1; i >= 0; i--)
            {
                if (data[i] != ' ')
                    break;
                ambiguity++;
            }

            degrees = Convert.ToInt16(data.Substring(0, 2).Replace(' ', '0'));
            minutes = Convert.ToInt16(data.Substring(2, 2).Replace(' ', '0'));
            seconds = Convert.ToDouble("0." + data.Substring(4).Replace(' ', '0'))*60;
        }


        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Packet Information Received {0} UTC\n", ReceivedDate);
            sb.AppendFormat("        Callsign: {0}\n", Callsign);
            //sb.AppendFormat("   Station Route: {0}\n", string.Join(", ", ));
            sb.AppendFormat("        DataType: {0}\n", DataType);
            sb.AppendFormat("        Latitude: {0}\n", Latitude);
            sb.AppendFormat("       Longitude: {0}\n", Longitude);
            sb.AppendFormat("        Altitude: {0}\n", Altitude);
            sb.AppendFormat("          Course: {0}\n", Direction);
            sb.AppendFormat("           Speed: {0}\n", Speed);
            sb.AppendFormat("    Symbol Table: {0}\n", SymbolTable);
            sb.AppendFormat("          Symbol: {0}\n", Symbol);

            if (DataType == DataType.CurrentMicE)
                sb.AppendFormat("   Mic E Message: {0}\n", MicEMessageType);


            return sb.ToString();
        }
    }
}