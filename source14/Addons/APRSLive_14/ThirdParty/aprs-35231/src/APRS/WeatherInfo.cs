using System;

namespace APRS
{
    public class WeatherInfo 
    {
        public int WindDirection { get; private set; }
        public int WindSpeed { get; private set; }
        public int WindGust { get; private set; }
        public int Temperature { get; private set; }
        public int RainLastHour { get; private set; }
        public int RainLast24Hours { get; private set; }
        public int RainSinceMidnight { get; private set; }
        public int Humidity { get; private set; }
        public int BarometricPressure { get; private set; }


        public WeatherInfo()
        {

        }

        public static WeatherInfo ParseWith7ByteWindData(string wString)
        {
            var wi = new WeatherInfo
            {
                WindDirection = Int32.MaxValue,
                WindSpeed = Int32.MaxValue,
                WindGust = Int32.MaxValue,
                Temperature = Int32.MaxValue,
                RainLastHour = Int32.MaxValue,
                RainLast24Hours = Int32.MaxValue,
                RainSinceMidnight = Int32.MaxValue,
                BarometricPressure = Int32.MaxValue,
                Humidity = Int32.MaxValue
            };
            if (wString.Length > 6)
            {
                wi.WindDirection = Convert.ToInt32(wString.Substring(0, 3));
                wi.WindSpeed = Convert.ToInt32(wString.Substring(4, 3));
            }
            ParseWeatherString(wString.Substring(7), wi);
            return wi;
        }

        public static WeatherInfo ParsePositionless(string wString)
        {
            var wi = new WeatherInfo
                         {
                             WindDirection = Int32.MaxValue,
                             WindSpeed = Int32.MaxValue,
                             WindGust = Int32.MaxValue,
                             Temperature = Int32.MaxValue,
                             RainLastHour = Int32.MaxValue,
                             RainLast24Hours = Int32.MaxValue,
                             RainSinceMidnight = Int32.MaxValue,
                             BarometricPressure = Int32.MaxValue,
                             Humidity = Int32.MaxValue
                         };
            ParseWeatherString(wString, wi);
            return wi;
            
        }

        private static void ParseWeatherString(string wString, WeatherInfo wi)
        {
            var strLen = wString.Length;
            var len = 3;
            for (var i = 0; i < strLen; i+=len + 1)
            {
                
                try
                {
                    switch (wString[i])
                    {
                        case 'c':
                            wi.WindDirection = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 's':
                            wi.WindSpeed = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'g':
                            wi.WindGust = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 't':
                            wi.Temperature = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'r':
                            wi.RainLastHour = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'p':
                            wi.RainLast24Hours = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'P':
                            wi.RainSinceMidnight = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'h':
                            len = 2;
                            wi.Humidity = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'b':
                            len = 5;
                            wi.BarometricPressure = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;

                    }
                }
                catch (Exception e)
                {
                    continue;
                }


            }
        }

        public static WeatherInfo ParseUltimeter(string s)
        {
            var wi = new WeatherInfo
                         {
                             WindDirection = Int32.MaxValue,
                             WindSpeed = Int32.MaxValue,
                             WindGust = Int32.MaxValue,
                             Temperature = Int32.MaxValue,
                             RainLastHour = Int32.MaxValue,
                             RainLast24Hours = Int32.MaxValue,
                             RainSinceMidnight = Int32.MaxValue,
                             BarometricPressure = Int32.MaxValue,
                             Humidity = Int32.MaxValue
                         };

            var pos = 0;
            wi.WindSpeed = (int) Math.Round(GetUltimeterFieldValue(s, pos) * 0.0621371192); 
            pos += 4;
            wi.WindDirection = GetUltimeterFieldValue(s, pos);
            pos += 4;
            wi.Temperature = (int) Math.Round(GetUltimeterFieldValue(s, pos)*0.1);
            pos += 4;
            pos += 4; 
            wi.BarometricPressure = GetUltimeterFieldValue(s, pos);
            pos += 4;
            pos += 4;
            pos += 4;
            pos += 4;
            wi.Humidity = (int)Math.Round(GetUltimeterFieldValue(s, pos) * 0.1);
            pos += 4;
            pos += 4;
            pos += 4;
            wi.RainSinceMidnight = GetUltimeterFieldValue(s, pos);
            
            return wi;
            
        }

        private static int GetUltimeterFieldValue(string s, int pos)
        {
            try
            {
                return Convert.ToInt32(s.Substring(pos, 4), 16);
            }
            catch (Exception e)
            {
                
            }
            return Int32.MaxValue;
        }
    }
}