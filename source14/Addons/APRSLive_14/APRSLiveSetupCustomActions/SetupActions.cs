﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Security.AccessControl;
using Microsoft.Win32;


namespace APRSLiveSetupCustomActions
{
    [RunInstaller(true)]
    public partial class SetupActions : Installer
    {
        private static string addonsInDepictionDir = "Add-ons";
        private static string helpDir = "Help";
        public SetupActions()
        {
            InitializeComponent();
        }
//        public override void Install(System.Collections.IDictionary stateSaver)
//        {
//            base.Install(stateSaver);
//            Debugger.Break();
//        }
        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
            // /APRSBaseDir=[PRODUCTNAME]  didnt seem to work

            var aprsInstallDir = Context.Parameters["ProductName"];
            var aprsAddoInDepictionDir = "APRSLive";
            var depictionDir = GetDepictionInstallDirectory();
            if(!string.IsNullOrEmpty(aprsInstallDir))
            {
                aprsAddoInDepictionDir = aprsInstallDir;
            }
            
            if (!string.IsNullOrEmpty(depictionDir))
            {
                DeleteAPRSLiveFromDepictionAddinsDirectory(depictionDir, aprsAddoInDepictionDir);
            }
        }
        //The default values attempt to delete the .99.01 directory
        private static void DeleteAPRSLiveFromDepictionAddinsDirectory(string depictionDir,string aprsInstallDir)
        {
            if(string.IsNullOrEmpty(depictionDir))
            {
                depictionDir = GetDepictionInstallDirectory();
            }
            var aprsDir = "APRSLive";
            if (!string.IsNullOrEmpty(aprsInstallDir)) aprsDir = aprsInstallDir;
            var aprsLiveDirInDepictionDir = Path.Combine(depictionDir, addonsInDepictionDir, aprsDir);
            if (Directory.Exists(aprsLiveDirInDepictionDir))
            {
                Directory.Delete(aprsLiveDirInDepictionDir, true);
            }
        }

        public override void Commit(System.Collections.IDictionary savedState)
        {
            base.Commit(savedState);
            //TargetDir="[TARGETDIR]\"//what is needed
            string installDir = Context.Parameters["TargetDir"];
            // /APRSBaseDir=[PRODUCTNAME]  didnt seem to work
            var aprsInstallDir = Context.Parameters["ProductName"];
            //Delete previous version which does nto seem to get uninstalled correctly
            DeleteAPRSLiveFromDepictionAddinsDirectory(null, null);
            if(!string.IsNullOrEmpty(installDir) )
            {
                CopyAPRSToDepictionDirAndUpdatePrivilages(installDir, aprsInstallDir);
            }
        }

        public static void CopyAPRSToDepictionDirAndUpdatePrivilages(string installDir,string aprsInstallDir)
        {
            var depictionInstallDir = GetDepictionInstallDirectory();
            var aprsDir = "APRSLive";
            var aprsInDepictionDir = aprsDir;
            if (!string.IsNullOrEmpty(aprsInstallDir)) aprsInDepictionDir = aprsInstallDir;
            if (!string.IsNullOrEmpty(depictionInstallDir))
            {
                var addonDir = Path.Combine(depictionInstallDir, addonsInDepictionDir);
                GiveUserPrivilegesToDirectory(addonDir);

                var aprsLiveAddonDir = Path.Combine(addonDir, aprsInDepictionDir);
                GiveUserPrivilegesToDirectory(aprsLiveAddonDir);

                CopyDirectoryAndFiles(Path.Combine(installDir, aprsDir), aprsLiveAddonDir);
                CopyDirectoryAndFiles(Path.Combine(installDir, helpDir), Path.Combine(aprsLiveAddonDir, helpDir));
            }
        }

        private static void GiveUserPrivilegesToDirectory(string dir)
        {
            DirectorySecurity dirSec;
            FileSystemAccessRule fsar;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            
            dirSec = Directory.GetAccessControl(dir);
            fsar = new FileSystemAccessRule("Users"
                                            , FileSystemRights.FullControl
                                            , InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit
                                            , PropagationFlags.None
                                            , AccessControlType.Allow);
            dirSec.AddAccessRule(fsar);
            Directory.SetAccessControl(dir, dirSec);
        }

        private static string GetDepictionInstallDirectory()
        {
            var depictionInstallDir = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Depiction Inc\Depiction 1.4", "Location", "");
            if (depictionInstallDir == "")
            {
                depictionInstallDir = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Wow6432Node\Depiction Inc\Depiction 1.4", "Location", "");
                if(depictionInstallDir.Equals(string.Empty)) return string.Empty; 
            }
            return depictionInstallDir;
        }


        private static void CopyDirectoryAndFiles( string srcDirectory, string destDirectory)
        {
            try
            {
                if (!Directory.Exists(destDirectory))
                {
                    Directory.CreateDirectory(destDirectory);
                }
                foreach (var originalFullFileName in Directory.GetFiles(srcDirectory))
                {
                    var originalFileName = Path.GetFileName(originalFullFileName);
                    if(!string.IsNullOrEmpty(originalFileName))
                    {
                        var destFileName = Path.Combine(destDirectory, originalFileName);
                        if(File.Exists(destFileName))
                        {
                            File.Delete(destFileName);
                        }
                        File.Copy(originalFullFileName, destFileName);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }
    }
}
