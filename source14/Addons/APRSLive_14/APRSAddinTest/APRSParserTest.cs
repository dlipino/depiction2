﻿using System;
using System.Threading;
using APRSLiveFeedGatherer;
using AgwpePort.Aprs;
using APRS;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer.Model;
using APRSPacketIO;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSParserTest
    {
        private int cnt;
        [Test]
        public void packetInfoCanHandleMultiLineStrings()
        {
            var str =

                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\n" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:" + "`2'1 -Wj/]\"54}\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\n" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\n" +
                "N9VW-7>T7RVXQ,XTAL,WIDE1,BALDI*,WIDE2-1,qAR,KD7DVD-6:`1OglqsK\"9U}HAPPY TRAILS=\"^]\n" +
                "K7BKE-9>TW4PPU,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1nfj/]\"5,}\n" +
            "KB7ZKA-9>GPSLK,SEA*,qAR,KD7DVD-6:$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\"\n";  // this data type does not
            // get parsed, data type not yet supported.

            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.AreEqual(16, packetInfos.Count);
            
        }
        [Test]
        public void packetInfoCanHandleMultiLineStringsWithCRLFCombo()
        {
            var str =

                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r\n" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\r\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:" + "`2'1 -Wj/]\"54}\r\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r\n" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\r\n" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r\n" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r\n" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r\n" +
                "N9VW-7>T7RVXQ,XTAL,WIDE1,BALDI*,WIDE2-1,qAR,KD7DVD-6:`1OglqsK\"9U}HAPPY TRAILS=\"^]\r\n" +
                "K7BKE-9>TW4PPU,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1nfj/]\"5,}\r\n" +
            "KB7ZKA-9>GPSLK,SEA*,qAR,KD7DVD-6:$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\"\r\n";  // this data type does not
            // get parsed, data type not yet supported.

            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.AreEqual(16, packetInfos.Count);

        }

        [Test]
        public void packetInfoCanHandleMultiLineStringsWithNoNewline()
        {
            var str =

                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\r" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:" + "`2'1 -Wj/]\"54}\r" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r" +
                "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1\r" +
                "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}\r" +
                "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}\r" +
                "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=\r" +
                "N9VW-7>T7RVXQ,XTAL,WIDE1,BALDI*,WIDE2-1,qAR,KD7DVD-6:`1OglqsK\"9U}HAPPY TRAILS=\"^]\r" +
                "K7BKE-9>TW4PPU,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1nfj/]\"5,}\r" +
            "KB7ZKA-9>GPSLK,SEA*,qAR,KD7DVD-6:$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\"\r";  // this data type does not
            // get parsed, data type not yet supported.

            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.AreEqual(16, packetInfos.Count);

        }

        [Test]
        public void TryPositionWithTimestampIncludingWeatherData()
        {
            var str =
            "ASTRWX>APRS,AA7OA,KOPEAK,BALDI*: <UI>:@172205z4615.77N/12353.22W_068/001g009t053r000p000P000h..b10140.U21h";
            //var parser = new PacketParser();
            var packet = PacketParser.ParseLine(str);
            Assert.IsNotNull(packet);
        }
        [Test]
        public void TryWeatherData()
        {
            var str =
                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            var packet = PacketParser.ParseLine(str);
            Assert.IsNotNull(packet);
        }

        [Test]
        public void TryTinyTrackerData()
        {
            var str= "K7SDW-15>3T1PWS-2,WIDE1-1,WIDE2-1 <UI>:`.UHl >/\"7R}CVC SAG Vehicle";
            var packet = PacketParser.ParseLine(str);
            Assert.IsNotNull(packet);
        }

        [Test]
        public void TryAlternateParser()
        {
            var str = "$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\"\n";
            var v = new AprsData(str);
        }

        [Test]
        public void DoesParseLinesWork1()
        {
            var str =
            "WA7RVV>APW285,WA7RVV-8*,WIDE2-1: <UI>:_03160551c153s004g009t060r000p000P000h00b00000wU2K\r\n\r\n";
            var parser = new PacketParser();
            var packets = parser.GetPackets(str);
            Assert.AreEqual(1, packets.Count);
        }


        [Test]
        public void DoesParseLinesWorkWithFragments()
        {
            var str =
                "7OA>RED,RED,SCOOP,WA7RVV-8*,WIDE3: <<UI>>:=4918.68N/11739.56W-Castlegar,BC \"146.520 SIMPLEX\" {UIV32N}\r\n" +
                "\r\nWA7RVV>APW285,WA7RVV-8*,WIDE2-1: <UI>:_03160551c153s004g009t060r000p000P000h00b00000wU2K\r\n";
            
            var parser = new PacketParser();
            parser.GetPackets("V");
            parser.GetPackets("A");
            var packets = parser.GetPackets(str);
            Assert.AreEqual(2, packets.Count);
            Assert.AreEqual("VA7OA", packets[0].Callsign);

            
        }


        [Test]
        public void CanHandleActualRepresentativeDataStrings()
        {
            var str = "SOMTN>APN383,BALDI,WIDE2*,qAR,KD7DVD-6:!4719.20NS12320.73W#PHG2860/W2,WAn South Mtn KB7UVC\n" +
                      "VE6VQ-7>APOT21,WIDE1-1*,WIDE2-1,qAR,KD7DVD-6:/162544h4745.69N/12207.95Wk271/026/A=000418 13C EPE007 Larry 443.325/224.78\n";
                      

            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.IsNull(parser.msgFragment);
            Assert.AreEqual(2, packetInfos.Count);

        }

        [Test]
        public void CanHandleDataString2()
        {
            var str = "ETIGER>BEACON,qAR,KD7DVD-6:>147080t103.5 ETIGER Mnt W7GLB";
            
            str = "W7JGY-14>TW3YQP,ETIGER,WIDE1*,WIDE2-1:`2/?!!lu/]\"4I}";

            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);

        }

        [Test]
        public void CanHandleActualMessageReceivedAtArlington()
        {
            
            
            var str = "KN7S-7>TW5YTS,WIDE1-1,WIDE2-1:'2&z!fk/]\"4,}444.700+/103.5";
            str = @"WE7U-12>APOT2A,KD7NM,ETIGER,WIDE2*:!/6<?;/VWSjR?G SCVSAR";
            str = "N7GME-1>T7SQQW,WW7RA*,WIDE2-2:`2Asm Xk/]\"4h}";
            str = "KN7S-7>TW5YTS,WIDE1-1,WIDE2-1:'2&z!fk/]\"4,}444.700+/103.5";
            


            str = "KC7OO-7>TX1SRP,WIDE2-2:'2)6!SZk/]\"4@}";

            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            //var parser = new hamradio.ke4pjw.APRS();
            //parser.Parse(str);
            //Assert.IsNotNull(parser);



        }

        [Test]
        public void CanHandleTextMessage()
        {
            var str = "KD7DVD-8>APY008,WIDE1-1,WIDE2-1::KD7DVD-7 :test message{79";
            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            Assert.AreEqual("test message", packetInfo.Message);
            Assert.AreEqual("KD7DVD-7", packetInfo.MessageRecipient);
            Assert.AreEqual("79", packetInfo.MessageID);

        }
        [Test]
        public void CanHandleActualReceivedTextMessage()
        {
            var str = "KD7DVD-6>APND13,WIDE2-1,qAR,KD7DVD::KF7GDY   :Use: H1, H2 H3..H8 or   use \"List\"  for all{40";
            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            Assert.AreEqual("Use: H1, H2 H3..H8 or   use \"List\"  for all", packetInfo.Message);
            Assert.AreEqual("KF7GDY", packetInfo.MessageRecipient);
            Assert.AreEqual("40", packetInfo.MessageID);

        }

        [Test]
        public void CanHandleReceivedMessageAck()
        {
            var str = "KD7DVD-6>APND13,WIDE2-1,qAR,KD7DVD::KF7GDY   :ack";
            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            Assert.AreEqual("ack", packetInfo.Message);
            Assert.AreEqual("KF7GDY", packetInfo.MessageRecipient);
            Assert.IsNull(packetInfo.MessageID);

        }

        
        

            [Test]
        public void CanReconstructSerialMessage()
            {
                var str = "C7KP>AP";
                var parser = new PacketParser();
                var packetInfos = parser.GetPackets(str);
                packetInfos = parser.GetPackets("W");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("251,BAL");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("D");
                packetInfos = parser.GetPackets("I,ETIGE");
                packetInfos = parser.GetPackets("R");
                packetInfos = parser.GetPackets(",WIDE2*");
                packetInfos = parser.GetPackets(":");
                packetInfos = parser.GetPackets("=4654.9");
                packetInfos = parser.GetPackets("1");
                packetInfos = parser.GetPackets("N/12221");
                packetInfos = parser.GetPackets(".");
                packetInfos = parser.GetPackets("40W_PHG");
                packetInfos = parser.GetPackets("0");
                packetInfos = parser.GetPackets("000/Win");
                packetInfos = parser.GetPackets("A");
                packetInfos = parser.GetPackets("PRS 2.5");
                packetInfos = parser.GetPackets(".");
                packetInfos = parser.GetPackets("1 -WAPI");
                packetInfos = parser.GetPackets("E");
                packetInfos = parser.GetPackets("EATONVI");
                packetInfos = parser.GetPackets("L");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("-251\n");
                Assert.AreEqual(1, packetInfos.Count);
                

            }

        [Test]
        public void CanReCombineMessageFragment()
        {
            var str = "SOMTN>APN383,BALDI,WIDE2*,qAR,"; //partial string
            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.AreEqual(0, packetInfos.Count);
            Assert.IsNotNull(parser.msgFragment);
            var str2 = "KD7DVD-6:!4719.20NS12320.73W#PHG2860/W2,WAn South Mtn KB7UVC\n" +
            "VE6VQ-7>APOT21,WIDE1-1*,WIDE2-1,qAR,KD7DVD-6:/162544h4745.69N/12207.95Wk271/026/A=000418 13C EPE007 Larry 443.325/224.78\n";
            packetInfos = parser.GetPackets(str2);
            Assert.AreEqual(2, packetInfos.Count);
       
        }

        

            [Test]
        public void CanParseUltimeterMessage()
        {
            var str = "IIDAR>AP3383,ETIEER,WIDE**:$ULTW004500DB021922ED28260006871F000103C7009402120002000F";
            var pi = new PacketInfo(str);
            Assert.IsNotNull(pi.WeatherInfo);
            Assert.AreEqual(4, pi.WeatherInfo.WindSpeed);
            Assert.AreEqual(219, pi.WeatherInfo.WindDirection);
            Assert.AreEqual(54, pi.WeatherInfo.Temperature);
            Assert.AreEqual(10278, pi.WeatherInfo.BarometricPressure);
            Assert.AreEqual(97, pi.WeatherInfo.Humidity);
            Assert.AreEqual(2, pi.WeatherInfo.RainSinceMidnight);
            
        }
        [Test]
        public void ParseMessageScratchpad()
        {
            var str = "KB7SQE-10>BEACON,CREST,BAKER*,WIDE3-1,qAR,VE7NEK:KB7SQE APRS VOICE 146.840 - LIBBY MT";
            var pi = new PacketInfo(str);
            Assert.IsNotNull(pi.Latitude);
        }


        [Test]
        public void CanParseMessage()
        {
            var str = "@291931z3303.70N/11634.43W_066/005g012t049r000p000P000h59b10167L441.DsVP";
            var pi = new PacketInfo("FROM", "TO", "VIA", str);
            Assert.IsNotNull(pi.Latitude);
            //var str = "_09230335c195s000g000t040r000p001P000h71b10172tU2k";  weather report without position
            
            //var str = "$GPRMC,193427,A,3347.6442,N,11805.4995,W,000.0,107.9,290110,013.4,E*67";  //rawgpsorultimeter2000
            str = "@291934z3350.98N/11752.79W_132/000g000t063r000p000P000h49b10165/PHG61604/Anaheim WX & Email {UIV32N}";
            pi = new PacketInfo("FROM", "TO", "VIA", str);
            Assert.IsNotNull(pi.Latitude);

            str = "/291937z3346.02N/11754.21W>089/016!W78!/A=000078 14.1V 23C CNT00000";
            pi = new PacketInfo("FROM", "TO", "VIA", str);
            Assert.IsNotNull(pi.Latitude);
            
            //Assert.AreEqual(2, packetInfos.Count);
        }

        [Test]
        public void CanParseMessageWithFrameInfo()
        {

            var str =
                "VE6VQ-7>APOT21,BALDI,WIDE1,BALDI*: <UI>:/035137h4730.26N/12211.82Wk349/051/A=000141 16C EPE013 Larry 443.325/224.78\n";
            var packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            Assert.IsNotNull(packetInfo.Latitude);
            str = @"K7HRT-8>TW0RYZ,ETIGER,SEDRO,WIDE2*: <<UI>>:`2MGlIZk/]""3t} 73 de PAT=\n";
            packetInfo = PacketParser.ParseLine(str);
            Assert.IsNotNull(packetInfo);
            Assert.IsNotNull(packetInfo.Latitude);


        }

        [Test]
        public void CanParseBlockOf2LineMessages()
        {
            var str = "VE6VQ-7>APOT21,BALDI,WIDE1,BALDI*: <UI>:\n" +
            "/035137h4730.26N/12211.82Wk349/051/A=000141 16C EPE013 Larry 443.325/224.78\n" +
            "K7HRT-8>TW0RYZ,BALDI*: <<UI>>:\n" +
            "`2MGl mk/]\"4&} 73 de PAT=\n" +
            "K7HRT-8>TW0RYZ,ETIGER*,WIDE2-1: <<UI>>:\n" +
            "`2MGl mk/]\"4&} 73 de PAT=\n" +
            "BALDI>APOT21,WIDE2-1: <UI>:\n" +
            "!4713.13NS12150.61W#PHG5660/W2,WAn,   Baldi N7FSP WX\n" +
            "BALDI>APOT21,WIDE2-1: <UI>:\n" +
            "!4713.13N/12150.61W_182/004g007t027p000h59b10044T2WX\n" +
            "K7HRT-8>TW0RYZ,ETIGER*,WIDE2-1: <<UI>>:\n" +
            "`2MGlIZk/]\"3t} 73 de PAT=\n" +
            "K7HRT-8>TW0RYZ,ETIGER,SEDRO,WIDE2*: <<UI>>:\n" +
            "`2MGlIZk/]\"3t} 73 de PAT=\n" +
            "W7ROC>APW251,BALDI*: <<UI>>:\n" +
            "=4802.20N/12240.71W-PHG2130/WinAPRS  -WAJEFNORDLAND-251-<530>\n";
            var parser = new PacketParser();
            var packetInfos = parser.GetPackets(str);
            Assert.AreEqual(8, packetInfos.Count);
            

            //Assert.AreEqual(2, packetInfos.Count);
        }


        [Test]
        public void CanParseWeatherReport()
        {
            
            
            var str = "_09230335c195s000g000t040r000p001P000h71b10172tU2k";  //weather report without position
            var pi = new PacketInfo("FROM", "TO", "VIA", str);

            Assert.IsNotNull(pi.WeatherInfo);
            Assert.AreEqual(195, pi.WeatherInfo.WindDirection);
            Assert.AreEqual(0, pi.WeatherInfo.WindSpeed);
            Assert.AreEqual(0, pi.WeatherInfo.WindGust);
            Assert.AreEqual(40, pi.WeatherInfo.Temperature);
            Assert.AreEqual(0, pi.WeatherInfo.RainLastHour); 
            Assert.AreEqual(1, pi.WeatherInfo.RainLast24Hours);
            Assert.AreEqual(0, pi.WeatherInfo.RainSinceMidnight);
            Assert.AreEqual(71, pi.WeatherInfo.Humidity);
            Assert.AreEqual(10172, pi.WeatherInfo.BarometricPressure);

            //Assert.AreEqual(2, packetInfos.Count);
        }


        
        [Test]
        public void CanRoundTripTextMessage()
        {
            var str = "KD7DVD-8>APY008,WIDE1-1,WIDE2-1::KD7DVD-7 :test message{79";
            var packetIn = new PacketInfo
                                 {
                                     Callsign = "XWXX",
                                     DataType = DataType.Message,
                                     MessageRecipient = "vvvvv",
                                     Message = "orchid in my hand",
                                     MessageID = "23"
                                 };

            //var pktString = PacketEncoder.EncodePacket(packetIn,"APRS,TCPIP*");
            var packetEncoder = new TcpPacketEncoder("XWXX");
            var pktString = packetEncoder.EncodePacket(packetIn);
            var packetOut = PacketParser.ParseLine(pktString);
            Assert.AreEqual(packetIn.Callsign, packetOut.Callsign);
            Assert.AreEqual(packetIn.DataType, packetOut.DataType);
            Assert.AreEqual(packetIn.MessageRecipient, packetOut.MessageRecipient);
            Assert.AreEqual(packetIn.Message, packetOut.Message);
            Assert.AreEqual(packetIn.MessageID, packetOut.MessageID);
        }
        //outgoing packet for test
        //KF7GDY>WIDE2-2:/240931z4813.04N/12210.77W_014/007g016t040P007b10105h59cwMServer

        [Test]
        public void CanReceiveMessageSent()
        {
            var config = new APRSAddinConfig();
            config.InputType = APRSInputType.Loopback;
            config.CallSign = "XXXXX";
            var aprsService = new APRSLiveBackgroundService(null, config);
            aprsService.StartPackeService();
            var msg = new APRSMessage { DestinationCallSign = "XXXXX", Message = "hello" };
            aprsService.MessengerSerice.SendMessage(msg);
            Thread.Sleep(75);
            aprsService.StopBackgroundService();
//            var aprsGatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer(config); 
//            aprsGatherer.Start();
//            var msg = new APRSMessage {DestinationCallSign = "XXXXX", Message = "hello"};
//            aprsGatherer.SendMessage(msg);
//            Thread.Sleep(75);
//            aprsGatherer.Stop();


        }






        
    }
}
