﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using APRS;
//using APRSLiveAddinTestJigWpf;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ElementLibrary;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSStationInfoTest
    {
        [TestFixtureSetUp]
        public void FSetup()
        {
            var fe = new FrameworkElement();
            //DepictionAccess.Initialize(new LimitedDepictionAccess());
        }

        [Test]
        public void APRSStationInfoDoesNotAddToHistoryIfPositionUnchanged()
        {
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:24:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);

        }

        
        [Test]
        [Ignore("Do not do history at this point")]
        public void APRSStationInfoAddsToHistoryIfPositionChanged()
        {
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:43"));
            Assert.AreEqual(2, stationInfo.PositionHistory.Count);

        }

        [Ignore("Do not do history at this point")]
        [Test]
        public void APRSStationInfoHistoryMaintainsRollingTimeWindow()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:44"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.43), DateTime.Parse("10/24/2009 11:20:43"));
            Assert.AreEqual(3, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 11:24:43"));
            Assert.AreEqual(2, stationInfo.PositionHistory.Count);
        }


        [Test]
        public void APRSStationInfoUpdateStationFromPacketGrabsWeatherData()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            
            var elem = ElementFactory.CreateElementFromTypeString("Depiction.Plugin.PointOfInterest");
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes) { Element = elem, lastUpdatedAt = DateTime.Now };

            var str =
                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            var packet = PacketParser.ParseLine(str);
            stationInfo.UpdateStationFromPacket(packet);

            object propValue;
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindDirection", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindSpeed", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindGust", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainSinceMidnight", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainLastHour", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("BarometricPressure", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("Humidity", out propValue));

        }

        [Test]
        public void APRSStationInfoCreatePacketInfoGrabsWeatherData()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            var str =
                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            var packet = PacketParser.ParseLine(str);
            var stationInfo = APRSStationInfo.CreateNewFromPacket(gatherer.TimeWindowMinutes, packet);

            object propValue;
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindDirection", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindSpeed", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindGust", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainSinceMidnight", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainLastHour", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("BarometricPressure", out propValue));
            Assert.IsTrue(stationInfo.Element.GetPropertyValue("Humidity", out propValue));

        }



        [Ignore("Do not do history at this point")]
        [Test]
        public void APRSStationInfoHistoryNeverDeletesLastPositionEvenAfterFallingOutOfTimeWindow()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:44"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.43), DateTime.Parse("10/24/2009 11:20:43"));
            Assert.AreEqual(3, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 11:24:45"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 12:24:45"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);


        }
        [Test]
        public void APRSStationInfoGetCroppedBitmapworks()
        {
            //var uri = new Uri("k://application:,,,/APRSLiveFeedGatherer;component/ResourceDictionary.xaml");
            // This application.loadcomponents is needed to load
            var uri = new Uri("/APRSLiveFeedGatherer;component/ResourceDictionary.xaml", UriKind.Relative);
            var resourceDictionary = Application.LoadComponent(uri) as ResourceDictionary;
         
  
            
            for (int i = 0; i < 96; i++)
            {
                var bmp = APRSStationInfo.GetAPRSIcon(SymbolTable.Primary, i);
                SaveBitmap(bmp, string.Format(@"c:\tmp\testbmp{0}.png", i));    
            }
            
        }


        public static bool SaveBitmap(BitmapSource bitmap, string fileName)
        {
            if (bitmap != null)
            {
                BitmapEncoder encoder = new JpegBitmapEncoder();
                ((JpegBitmapEncoder)encoder).QualityLevel = 80;

                if (Path.HasExtension(fileName))
                {
                    switch (Path.GetExtension(fileName).ToLower())
                    {
                        case ".gif":
                        case ".giff":
                            encoder = new GifBitmapEncoder();
                            break;
                        case ".tiff":
                        case ".tif":
                            encoder = new TiffBitmapEncoder();
                            break;
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                }

                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(stream);
                }

                return true;
            }

            return false;
        }
    }
}
