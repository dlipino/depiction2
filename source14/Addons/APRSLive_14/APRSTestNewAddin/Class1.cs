﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Depiction.APINew.AddinObjects.Objects;
using Depiction.APINew.CoreEnumAndStructs;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;

namespace APRSTestNewAddin
{
    [DepictionNonDefaultImporterMetadata("DepictionAPRSLiveAddin", DisplayName = "APRS Live Stations",
       ImporterSources = new[] { InformationSource.Web })]
    public class AprsLiveWorker : NondefaultDepictionImporterBase
    {
        public override void ImportElements(object elementLocation, string defaultElementType,
            IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            throw new System.NotImplementedException();
        }
    }
}
