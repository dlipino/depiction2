﻿using APRS;

namespace APRSPacketIO
{
    public class PacketEncoder
    {
        protected readonly string callSign;

        public PacketEncoder(string callSign)
        {
            this.callSign = callSign;
        }

        public virtual string EncodePacket(PacketInfo pkt)
        {
            return string.Format(":{0}:{1}{{{2}",
                                 pkt.MessageRecipient.PadRight(9),
                                 pkt.Message,
                                 pkt.MessageID);
                                 

        }

        public virtual string EncodeAck(string recipient, string msgId)
        {
            if (string.IsNullOrEmpty(msgId))
            {
                return string.Format(":{0}:ack", recipient.PadRight(9));
            }
            return string.Format(":{0}:ack{1}", recipient.PadRight(9), msgId);
        }
    }

    public class TcpPacketEncoder: PacketEncoder
    {
        public TcpPacketEncoder(string callSign): base(callSign)
        {
            
        }
        public override string EncodePacket(PacketInfo pkt)
        {
            return string.Format("{0}>APRS,TCPIP*:{1}", callSign,  base.EncodePacket(pkt));
        }

        public override string EncodeAck(string recipient, string msgId)
        {
            return string.Format("{0}>APRS,TCPIP*:{1}", callSign, base.EncodeAck(recipient, msgId));
        }
    }
}