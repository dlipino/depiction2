﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace APRSPacketIO
{
    public class Logger
    {
        private static List<LogItem> logItems = new List<LogItem>();
        private static string logFileName;
        static Logger()
        {
            var folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            logFileName = Path.Combine(folder, "AprsLive_log.txt");
            if (File.Exists(logFileName))
                File.Delete(logFileName);
        }
        public static void WriteLogItem(string message)
        {
            logItems.Add(new LogItem {Message = message, Timestamp = DateTime.Now});
            if (OnWriteLog != null)
                OnWriteLog(message);

            if (logItems.Count > 0)
                Flush();
        }

        public static event OnWriteLogEventHandler OnWriteLog;

        public static void Flush()
        {
            using (var sw = new StreamWriter(logFileName, true))
            {
                foreach (var item in logItems)
                {
                    sw.WriteLine(string.Format("{0:T} {1}", item.Timestamp, item.Message));
                }
            }
            logItems.Clear();
        }
    }

    public delegate void OnWriteLogEventHandler(string message);

    
}
