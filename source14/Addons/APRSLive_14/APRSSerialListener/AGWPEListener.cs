﻿using System;
using System.Collections.Generic;
using System.Text;
using AgwpePort;

using APRS;

namespace APRSPacketIO
{
    public class AGWPEListener : IPacketListener
    {
        private readonly byte radioPort;
        private readonly string host;
        private readonly int tcpPort;
        private readonly string callsign;
        private AgwpePort.AgwpePort port;
        public AGWPEListener(byte radioPort, string host, int tcpPort, string callsign)
        {
            this.radioPort = radioPort;
            this.host = host;
            this.tcpPort = tcpPort;
            this.callsign = callsign;
            port = new AgwpePort.AgwpePort(radioPort, host, tcpPort);
            port.FrameReceived += port_FrameReceived;
            
            
        }

        void port_FrameReceived(object sender, AgwpeEventArgs e)
        {
            var dataKind = (char)e.FrameHeader.DataKind;
            
            switch (dataKind)
            {
                case 'X':
                    break;
                case 'I': // Read AX25 Information frames and data
//                    rtWrite(((AgwpeMoniConnInfo)e.FrameData).AX25Header + '\n' + Encoding.ASCII.GetString(((AgwpeMoniConnInfo)e.FrameData).AX25Data) + "\n\n");
                    break;
                case 'U': // Read AX25 Unproto frames and data
                    var frameData = (AgwpeMoniUnproto) e.FrameData;
                    
                    //var packet = new AprsData(frameData.AX25Data);

                    try
                    {
                        var packetStr = Encoding.ASCII.GetString(frameData.AX25Data);
                        Logger.WriteLogItem("<- " + frameData.AX25Header + "\n" + packetStr + "\n");
                        var pi = new PacketInfo(frameData.AX25CallFrom, frameData.AX25CallTo, frameData.AX25Via, packetStr);
                        //Console.WriteLine(pi.ToString());
                        if (PacketReceived != null)
                            PacketReceived(this, new PacketInfoEventArgs(pi));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error parsing AGWPE packet {0}", ex.Message);
                    }

                    //Console.WriteLine(dataKind + " " + Encoding.ASCII.GetString(frameData.AX25Data) + "\n" + packet.DataTypeString + " " + packet.Latitude);
                    //(((AgwpeMoniUnproto)e.FrameData).AX25Header + '\n' + Encoding.ASCII.GetString(((AgwpeMoniUnproto)e.FrameData).AX25Data) + "\n\n");
                    break;
            }
            
        }

        public bool IsListening { get; private set; }

        public void Start()
        {
            port.Open(radioPort, host, tcpPort);
            port.RegisterCallSign(callsign);
            port.StartMonitoring();
            IsListening = true;
        }

        public void Stop()
        {
            IsListening = false;
            port.Close();
        }

        public event EventHandler<PacketInfoEventArgs> PacketReceived;
        public event EventHandler<LineReceivedEventArgs> LineReceived;

        public void SendString(string str)
        {
            throw new System.NotImplementedException();
        }

        public string CallSign { get; set;}
        
    }
}
