﻿using System;
using System.Text;
using APRS;

namespace APRSPacketIO
{
    public class PacketIO
    {
        public event EventHandler<PacketInfoEventArgs> PacketReceived;
        protected PacketParser packetParser;
        private readonly string callSign;
        protected IPacketListener listener;
        private readonly PacketEncoder encoder;

        #region Constructor

        public PacketIO(IPacketListener listener, PacketEncoder encoder, string callSign)
        {
            this.listener = listener;
            this.encoder = encoder;
            if (listener == null)
                throw new Exception("Listener Cannot be null");
            listener.PacketReceived += listener_PacketReceived;
            listener.LineReceived +=listener_LineReceived;
            listener.CallSign = callSign;
            this.callSign = callSign;
            packetParser= new PacketParser();
        }

        #endregion

        #region Event stuff
        static void listener_LineReceived(object sender, LineReceivedEventArgs e)
        {
            Logger.WriteLogItem("<- " + e.Line);
        }

        public void listener_PacketReceived(object sender, PacketInfoEventArgs e)
        {
            if (PacketReceived != null)
                PacketReceived(sender, e);
        }
        #endregion

        public void Start()
        {
            listener.Start();
        }
        public void Stop()
        {
            listener.Stop();
            Logger.Flush();
        }
        

        public void Send(string message, string destCallsign)
        {
            //if (PacketReceived != null)
            //{
            //    var apkt = new PacketInfo { Message = string.Format("Replying to: {0}", message), Callsign = destCallsign, MessageRecipient = srcCallsign };
            //    apkt.DataType = DataType.Message;
            //    PacketReceived(listener, new PacketInfoEventArgs(apkt));
            //    return;
            //}
            var pkt = new PacketInfo {Message = message, Callsign = callSign, MessageRecipient = destCallsign};
            var pktStr = encoder.EncodePacket(pkt);
            
            listener.SendString(pktStr);
            Logger.WriteLogItem("-> " + pktStr);
        }

        public void SendAck(string destCallsign, string messageId)
        {
            var pktStr = encoder.EncodeAck(destCallsign, messageId);
            listener.SendString(pktStr);
            Logger.WriteLogItem("-> " + pktStr);
        }
    }
}