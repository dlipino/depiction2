﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APRSPacketIO
{
    public class LogItem
    {
        public DateTime Timestamp { get; set; }
        public string Message { get; set; }
    }
}
