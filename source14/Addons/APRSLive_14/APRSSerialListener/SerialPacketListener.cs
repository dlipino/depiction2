﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using APRS;

namespace APRSPacketIO
{
    public class SerialPacketListener: PacketListenerBase
    {
        
        private readonly SerialPort port;
        
        protected override char TerminationChar { get { return '\r'; } }
        public override Stream GetReadStream()
        {
            return port.BaseStream;
        }

        public SerialPacketListener(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake) 
        {
            SerialPortFixer.Execute(portName);
            port = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
            port.Handshake = handshake;
            if (port.Handshake != Handshake.None)
                port.DtrEnable = true;

        }

        public override bool IsListening { get { return port.IsOpen; } }

        public override void Start()
        {
            if (port.IsOpen)
                Stop();
            port.Open();
            Console.WriteLine("Opening Serial Port");
            BeginListening();
        }


        public void EnableSendMode()
        {
            base.SendString(string.Format("{0}{0}\rCONVERS", '\x3'));
        }


        public override void SendString(string str)
        {
            EnableSendMode();
            Thread.Sleep(1000);
            base.SendString(str);
        }


        


        public override void Stop()
        {
            base.Stop();
            if (port != null && port.IsOpen)
                port.Close();

            
 
        }
    }
}