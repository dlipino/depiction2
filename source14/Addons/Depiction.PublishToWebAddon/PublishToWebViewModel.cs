﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.MVVM;
using Depiction.CoreModel;
using Depiction.CoreModel.WebService;
using Ionic.Zip;
using Microsoft.Win32;

namespace Depiction.PublishToWebAddon
{
    class PublishToWebViewModel : DialogViewModelBase
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }

        public bool HideBackground { get; set; }
        private bool publishLocally;
        public bool PublishLocally
        {
            get { return publishLocally; }
            set
            {
                publishLocally = value;
                NotifyPropertyChanged("PublishLocally");
                NotifyPropertyChanged("PublishOnServer");
            }
        }

        public bool PublishOnServer
        {
            get { return !PublishLocally; }
        }

        private string localFilename;
        public string LocalFilename
        {
            get { return localFilename; }
            set
            {
                localFilename = value;
                NotifyPropertyChanged("LocalFilename");
            }
        }

        public string Username { get; set; }
        public string Filename { get; set; }
        public string Password { get; set; }
        public LegendType LegendType { get; set; }

        private Dictionary<LegendType, string> legendTypeList;
        public IDictionary LegendTypeList
        {
            get 
            { 
                if (legendTypeList == null)
                {
                    legendTypeList = new Dictionary<LegendType, string>();
                    legendTypeList.Add(LegendType.NoLegend, "No legend");
                    legendTypeList.Add(LegendType.Revealers, "Revealers");
                    legendTypeList.Add(LegendType.Elements, "Elements");
                    legendTypeList.Add(LegendType.RevealersAndElements, "Elements and Revealers");
                }
                return legendTypeList;
            }
        }



        private DelegateCommand browseCommand;

        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                    browseCommand = new DelegateCommand(executeBrowse, canExecuteBrowse);
                return browseCommand;
            }
        }

        private bool canExecuteBrowse()
        {
            return PublishLocally;
        }

        private void executeBrowse()
        {
            var dialog = new SaveFileDialog { Filter = "Zip files (*.zip)|*.zip" };
            dialog.ShowDialog();
            LocalFilename = dialog.FileName;
            if (LocalFilename == null)
                LocalFilename = string.Empty;
        }

        private DelegateCommand okCommand;
        
        public ICommand OKCommand
        {
            get
            {
                if (okCommand == null)
                    okCommand = new DelegateCommand(submit);
                return okCommand;
            }
        }

        string webFolder;
        string tempPath;
        string saveDepictionFileName;
        private bool subscribed;
        private void submit()
        {
            //MessageBox.Show("Publish to web is not yet implemented.");
            // If this fails, it will throw and fall out to catch below.
                
            tempPath = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, Guid.NewGuid().ToString());

            webFolder = Path.Combine(tempPath, "webDepiction");
            Directory.CreateDirectory(webFolder);


            //save the dpn
            if (string.IsNullOrEmpty(DepictionAccess.CurrentDepiction.DepictionsFileName ?? "saveForWebTempStoryName.dpn"))
            {
                saveDepictionFileName = Path.Combine(tempPath, "saveForWebTempStoryName.dpn");
            }
            else
            {
                saveDepictionFileName = Path.Combine(tempPath, Path.GetFileName(DepictionAccess.CurrentDepiction.DepictionsFileName));
            }
                
            var app = Application.Current as DepictionApplication;
            if (!subscribed)
            {
                app.saveLoadManager.DepictionSaveCompleted += saveLoadManager_DepictionSaveCompleted;
                subscribed = true;
            }

            app.saveLoadManager.SaveDepiction(saveDepictionFileName, DepictionAccess.CurrentDepiction);
        }

        void saveLoadManager_DepictionSaveCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            bool succeeded = false;
            try
            {
                var app = Application.Current as DepictionApplication;
                app.saveLoadManager.DepictionSaveCompleted -= saveLoadManager_DepictionSaveCompleted;
                subscribed = false;
                //save to disk
                var saveForWebCommand = new SaveForWebCommand(webFolder, Title, Description, LegendType, HideBackground);
                saveForWebCommand.Execute();

                //now publish

                var dpnZipFilePath = Path.Combine(webFolder, "depictionForWeb.zip");
                using(var zip = new ZipFile())
                {
                    zip.AddFile(Path.Combine(webFolder, "depictionForWeb.xml"), "");
                    //zip.AddDirectory(webFolder);
                    zip.Save(dpnZipFilePath);
                }
                
//                var fastZip = new FastZip
//                {
//                    CreateEmptyDirectories = true,
//                    RestoreDateTimeOnExtract = true,
//                    RestoreAttributesOnExtract = true
//                };
//                fastZip.CreateZip(dpnZipFilePath, webFolder, false, "depictionForWeb.xml");
//                File.Delete(Path.Combine(webFolder, "depictionForWeb.xml"));
                var saveForWebZipPath = Path.Combine(tempPath, string.Concat(Path.GetFileNameWithoutExtension(DepictionAccess.CurrentDepiction.DepictionsFileName ?? "saveForWebTempStoryName"), ".swf"));
//                fastZip.CreateZip(saveForWebZipPath, webFolder, true, "");
                using (var zip = new ZipFile())
                {
                    zip.AddDirectory(webFolder);
                    zip.Save(saveForWebZipPath);
                }

                if (PublishLocally && !string.IsNullOrEmpty(LocalFilename))
                {
#if DEBUG
                    foreach (var file in Directory.GetFiles(webFolder))
                    {
                        File.Copy(file, Path.Combine(@"c:\depiction\trunk\geo\source\Viewer\SilverlightTest\ClientBin", Path.GetFileName(file)), true);
                    }
#else
#endif
                    File.Copy(saveForWebZipPath, LocalFilename, true);
                    succeeded = true;
                    DepictionAccess.NotificationService.DisplayMessageString(string.Format("Successfully published the depiction titled {0} to {1}.", Title, LocalFilename));

                }
                else if (!PublishLocally)
                {
                    try
                    {
                        var result = WebServiceFacade.DoPublishToWeb(
                            Username, Password, Title, Description, Filename, null, saveDepictionFileName, saveForWebZipPath);
                        succeeded = result.Succeeded;
                        if (result.Succeeded)
                        {
                            DepictionAccess.NotificationService.DisplayMessageString(
                                string.Format("Successfully published the depiction titled {0} to the web at http://stories.depiction.com/Depictions/Show/{1}.", Title, result.ID));
                        }
                    }
                    catch (FaultException ex)
                    {
                        DepictionAccess.NotificationService.DisplayMessageString(string.Format("Could not publish to web: {0}", ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Could not publish to web: {0}", ex.Message));
            }
            
            IsDialogVisible = !succeeded;

        }
    }

    public enum LegendType
    {
        NoLegend, Revealers, Elements, RevealersAndElements
    }


}
