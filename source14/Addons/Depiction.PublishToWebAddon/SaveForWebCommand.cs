﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Xml;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypeConverters;
using Depiction.CoreModel.DepictionConverters;
using Color=System.Windows.Media.Color;

namespace Depiction.PublishToWebAddon
{
    public class SaveForWebCommand
    {
        private string imageFolder;
        private readonly HashSet<string> imageFilesSaved = new HashSet<string>();
        private readonly Dictionary<IDepictionRevealer, string> revealerKeys = new Dictionary<IDepictionRevealer, string>();
        private readonly string saveForWebFolder;
        private readonly string title;
        private readonly string description;
        private readonly LegendType legendType;
        private readonly bool hideBackground;

        public SaveForWebCommand(string saveForWebFolder, string title, string description, LegendType legendType, bool hideBackground)
        {
            this.saveForWebFolder = saveForWebFolder;
            this.title = title;
            this.description = description;
            this.legendType = legendType;
            this.hideBackground = hideBackground;
        }

        public bool Execute()
        {
            //WebServiceFacade.DoPublishToWeb("Jereme", "blah", "Seattle Depiction", "This is Seattle, friends", new[] {"Seattle", "Flood"}, "C:\\Temp\\TestPublish\\Seattle.dpn", "C:\\Temp\\TestPublish\\Seattle.zip");
            //return new CommandResult(true);
            //TODO: This is the save for web stuff, short circuiting it to test publish to web
            imageFolder = Path.Combine(saveForWebFolder, "Images");

            if (Directory.Exists(saveForWebFolder))
                Directory.Delete(saveForWebFolder, true);

            Directory.CreateDirectory(saveForWebFolder);
            Directory.CreateDirectory(imageFolder);

            var xmlWriter = new XmlTextWriter(saveForWebFolder + "\\depictionForWeb.xml", Encoding.UTF8);
            xmlWriter.Formatting = Formatting.Indented;

            var depictionStory = DepictionAccess.CurrentDepiction;
            using (xmlWriter)
            {
                try
                {
                    xmlWriter.WriteStartElement("webDepiction");

                    xmlWriter.WriteStartElement("depictionData");

                    xmlWriter.WriteAttributeString("topLeft", depictionStory.RegionBounds.TopLeft.Longitude + "," + depictionStory.RegionBounds.TopLeft.Latitude);
                    xmlWriter.WriteAttributeString("bottomRight", depictionStory.RegionBounds.BottomRight.Longitude + "," + depictionStory.RegionBounds.BottomRight.Latitude);
                    xmlWriter.WriteAttributeString("viewportTopLeft", depictionStory.ViewportBounds.TopLeft.Longitude + "," + depictionStory.ViewportBounds.TopLeft.Latitude);
                    xmlWriter.WriteAttributeString("viewportBottomRight", depictionStory.ViewportBounds.BottomRight.Longitude + "," + depictionStory.ViewportBounds.BottomRight.Latitude);
                    xmlWriter.WriteAttributeString("latitudeLongitudeSeparator", Settings.Default.LatitudeLongitudeSeparator.ToString());
                    xmlWriter.WriteAttributeString("latitudeLongitudeFormat", Settings.Default.LatitudeLongitudeFormat.ToString());
                    xmlWriter.WriteAttributeString("title", title);
                    xmlWriter.WriteAttributeString("description", description);
                    xmlWriter.WriteAttributeString("legendType", legendType.ToString());
                    xmlWriter.WriteAttributeString("hideBackground", hideBackground.ToString(CultureInfo.InvariantCulture));
                    xmlWriter.WriteEndElement();

                    foreach (var revealer in depictionStory.Revealers)
                    {
                        xmlWriter.WriteStartElement("revealer");

                        var topLeft = revealer.RevealerBounds.TopLeft;
                        var bottomRight = revealer.RevealerBounds.BottomRight;
                        var revealerKey = Guid.NewGuid();
                        revealerKeys.Add(revealer, revealerKey.ToString());

                        xmlWriter.WriteAttributeString("topLeft", topLeft.Longitude + "," + topLeft.Latitude);
                        xmlWriter.WriteAttributeString("bottomRight", bottomRight.Longitude + "," + bottomRight.Latitude);
                        xmlWriter.WriteAttributeString("revealerId", revealerKey.ToString());
                        xmlWriter.WriteAttributeString("revealerName", revealer.DisplayerName);
                        double opacity;
                        revealer.GetPropertyValue("Opacity", out opacity);
                        xmlWriter.WriteAttributeString("opacity", opacity.ToString(CultureInfo.InvariantCulture));
                        RevealerShapeType shapeType;
                        revealer.GetPropertyValue("RevealerShape", out shapeType);
                        xmlWriter.WriteAttributeString("revealerShape", shapeType.ToString());
                        bool revealerVisible;
                        revealer.GetPropertyValue("Maximized", out revealerVisible);
                        xmlWriter.WriteAttributeString("revealerVisible", revealerVisible.ToString(CultureInfo.InvariantCulture));
                        bool revealerBorderVisible;
                        revealer.GetPropertyValue("BorderVisible", out revealerBorderVisible);
                        xmlWriter.WriteAttributeString("revealerBorderVisible", revealerBorderVisible.ToString(CultureInfo.InvariantCulture));
                        bool anchored;
                        revealer.GetPropertyValue("Anchored", out anchored);
                        xmlWriter.WriteAttributeString("revealerMoveable", (!anchored).ToString(CultureInfo.InvariantCulture));

                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteStartElement("depictionElements");

                    foreach (var element in depictionStory.CompleteElementRepository.ElementsGeoLocated)
                    {
                        if (element.ElementType == "Depiction.Plugin.RoadNetwork") continue;
                        WriteElement(element, xmlWriter);
                    }

                    // End of the depictionElements (these elements are for the main canvas)
                    xmlWriter.WriteEndElement();

                    WriteAnnotations(xmlWriter);

                    xmlWriter.WriteEndElement();

                    DefaultResourceCopier.CopySpecificResourcesToDirectory(new Dictionary<string, string> {
                        {"Depiction.PublishToWebAddon.WebViewer.Silverlight.js", "Silverlight.js"},
                        {"Depiction.PublishToWebAddon.WebViewer.DepictionViewer.html", "DepictionViewer.html"},
                        {"Depiction.PublishToWebAddon.WebViewer.DepictionSilverlight.xap", "DepictionSilverlight.xap"}
                    }, saveForWebFolder, Assembly.Load("Depiction.PublishToWebAddon"));
                }
                finally
                {
                    xmlWriter.Close();
                }
            }

            return true;
        }

        private static void WriteAnnotations(XmlTextWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("annotationElements");
            foreach (var annotation in DepictionAccess.CurrentDepiction.DepictionAnnotations)
            {
                WriteAnnotationElement(annotation, xmlWriter);
            }

            // End of the depictionElements (these elements are for the main canvas)
            xmlWriter.WriteEndElement();
        }

        private static void WriteAnnotationElement(IDepictionAnnotation annotation, XmlTextWriter writer)
        {
            writer.WriteStartElement("annotationElement");

            var leftPos = annotation.MapLocation;
            writer.WriteAttributeString("TopLeft", leftPos.Longitude + "," + leftPos.Latitude);
            writer.WriteAttributeString("Visibility", annotation.IsAnnotationVisible.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("backgroundColor", annotation.BackgroundColor.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("foregroundColor", annotation.ForegroundColor.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Width", annotation.PixelWidth.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Height", annotation.PixelHeight.ToString(CultureInfo.InvariantCulture));

            writer.WriteAttributeString("contentLocationX", annotation.ContentLocationX.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("contentLocationY", annotation.ContentLocationY.ToString(CultureInfo.InvariantCulture));
            writer.WriteString(annotation.AnnotationText);
            writer.WriteEndElement();
        }

        private void WriteElement(IDepictionElement element, XmlWriter writer)
        {
            writer.WriteStartElement("depictionElement");

            writer.WriteAttributeString("HoverText", element.GetToolTipText(Environment.NewLine, element.UsePropertyNameInHoverText, true));
            writer.WriteAttributeString("PermaHoverTextOn", element.UsePermaText.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("PermaTextX", element.PermaText.PermaTextX.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("PermaTextY", element.PermaText.PermaTextY.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("ElementName", element.ElementType);
            bool active;
            element.GetPropertyValue("Active", out active);
            writer.WriteAttributeString("Active", active.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("InMainCanvas", DepictionAccess.CurrentDepiction.MainMap.IsElementPresent(element.ElementKey).ToString(CultureInfo.InvariantCulture));
            Color borderColor;

            element.GetPropertyValue("IconBorderColor", out borderColor);
            writer.WriteAttributeString("IconBorderColor", borderColor.ToString());
            DepictionIconBorderShape borderShape;
            element.GetPropertyValue("IconBorderShape", out borderShape);
            writer.WriteAttributeString("IconBorderShape", borderShape.ToString());
            writer.WriteAttributeString("TypeDisplayName", element.TypeDisplayName);

            string revealers = string.Empty;

            foreach (var revealer in DepictionAccess.CurrentDepiction.Revealers)
            {
                if (revealer.IsElementPresent(element.ElementKey))
                    revealers += revealerKeys[revealer] + ",";
            }

            writer.WriteAttributeString("Revealers", revealers.Trim(','));

            double iconSize;
            element.GetPropertyValue("IconSize", out iconSize);
                

            writer.WriteAttributeString("IconSize", iconSize.ToString(CultureInfo.InvariantCulture));

            bool showIcon;
            element.GetPropertyValue("ShowIcon", out showIcon);
            WriteIconOrImage(writer, element.IconPath, element.ImageMetadata.ImageFilename, showIcon);

            writer.WriteStartElement("ZoneOfInfluence");
            if (element.ZoneOfInfluence.Geometry != null)
            {
                string wkt = GeometryToOgrConverter.ZOIGeometryToWkt(element.ZoneOfInfluence);
                writer.WriteAttributeString("ZOIGeometry", wkt);
            }
            // End of ZoneOfInfluence
            writer.WriteEndElement();

            var position = element.Position;

            if (position != null)
                writer.WriteElementString("Location", position.Longitude + "," + position.Latitude);

            Color zoiFill;
            element.GetPropertyValue("ZOIFill", out zoiFill);
            writer.WriteElementString("FillColor", zoiFill.ToString());
            Color zoiBorder;
            element.GetPropertyValue("ZOIBorder", out zoiBorder);
            writer.WriteElementString("StrokeColor", zoiBorder.ToString());

            if (!string.IsNullOrEmpty(element.ImageMetadata.ImageFilename))
            {
                writer.WriteStartElement("ImageMetadata");
                writer.WriteAttributeString("TopLeft", element.ImageMetadata.TopLeftLongitude + "," + element.ImageMetadata.TopLeftLatitude);
                writer.WriteAttributeString("BottomRight", element.ImageMetadata.BottomRightLongitude + "," + element.ImageMetadata.BottomRightLatitude);
                writer.WriteAttributeString("Rotation", element.ImageMetadata.RotationDegrees.ToString(CultureInfo.InvariantCulture));
                writer.WriteEndElement();
            }

            foreach (var waypoint in element.Waypoints)
            {
                WriteWaypoint(waypoint, writer);
            }


            //End of depictionElement
            writer.WriteEndElement();
        }


        private void WriteIconOrImage(XmlWriter writer, DepictionIconPath iconPath, string imageMetadataFilename, bool showIcon)
        {
            var iconSource = DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath);
            string fileName = iconPath.Path;
            if (!fileName.EndsWith(".png") && !fileName.EndsWith(".jpg") && !fileName.EndsWith(".gif"))
                fileName += ".png";
            if (!string.IsNullOrEmpty(imageMetadataFilename) && !imageFilesSaved.Contains(imageMetadataFilename))
            {
                BitmapImage rawImage = Application.Current.Resources[imageMetadataFilename] as BitmapImage;

                BitmapSaveLoadService.SaveBitmap(rawImage, imageFolder + "\\" + imageMetadataFilename);
                imageFilesSaved.Add(imageMetadataFilename);
                writer.WriteAttributeString("Icon", imageMetadataFilename);
                writer.WriteAttributeString("IconVisible", false.ToString(CultureInfo.InvariantCulture));
            }

            if (iconSource != null && showIcon)
            {
                if (!imageFilesSaved.Contains(fileName))
                {
                    BitmapImage iconBitmap = null;

                    Uri uri;
                    Uri.TryCreate(iconSource.ToString(), UriKind.Absolute, out uri);
                    if (uri != null)
                    {
                        iconBitmap = new BitmapImage();
                        iconBitmap.BeginInit();
                        iconBitmap.CacheOption = BitmapCacheOption.OnLoad;
                        iconBitmap.UriSource = uri;
                        iconBitmap.EndInit();
                        iconBitmap.Freeze();
                    }
                    else if (iconSource is BitmapImage)
                        iconBitmap = (BitmapImage)iconSource;

                    //var bitmapSaver = new BitmapSaveLoadService(iconBitmap);
                    //BitmapSaveLoadService.SaveBitmap()
                    //string extension = ".jpg";

                    //if (iconBitmap != null)
                    //{
                    //if (iconBitmap.UriSource != null && (iconBitmap.UriSource.LocalPath.EndsWith(".png") || iconBitmap.UriSource.LocalPath.EndsWith(".gif")))
                    //extension = ".png";
                    //}
                    //string imageName = Guid.NewGuid() + extension;

                    if (string.IsNullOrEmpty(imageMetadataFilename))
                    {
                        // This is not an image element, so scale down the icon here
                        // because Silverlight image scaling sucks
                        var bitmap = new Bitmap(22, 22);
                        Graphics graphics = Graphics.FromImage(bitmap);

                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        using (var stream = new MemoryStream())
                        {
                            var encoder = new PngBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(iconBitmap));
                            encoder.Save(stream);

                            graphics.DrawImage(Image.FromStream(stream), 0, 0, 22, 22);
                            bitmap.Save(imageFolder + "\\" + fileName);
                        }
                    }
                    else
                        BitmapSaveLoadService.SaveBitmap(iconBitmap, imageFolder + "\\" + fileName);

                    imageFilesSaved.Add(fileName);
                }

                writer.WriteAttributeString("Icon", fileName);
                writer.WriteAttributeString("IconVisible", true.ToString(CultureInfo.InvariantCulture));
            }
        }

        private void WriteWaypoint(IDepictionElementWaypoint waypoint, XmlWriter writer)
        {
            writer.WriteStartElement("ElementWaypoint");
            writer.WriteAttributeString("IconPath", waypoint.IconPath.ToString());
            writer.WriteAttributeString("IconSize", waypoint.IconSize.ToString(CultureInfo.InvariantCulture));
            writer.WriteAttributeString("Location", waypoint.Location.ToXmlSaveString());
            WriteIconOrImage(writer, waypoint.IconPath, null, true);

            writer.WriteEndElement();
        }
    }
}
