﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Depiction.PublishToWebAddon.Converters;

namespace Depiction.PublishToWebAddon
{
    /// <summary>
    /// Interaction logic for PublishToWebDialog.xaml
    /// </summary>
    public partial class PublishToWebDialog
    {
        public static readonly BooleanInverterConverter BooleanInverter = new BooleanInverterConverter();
        public static readonly BooleanToVisibilityConverter BooleanToVisibility = new BooleanToVisibilityConverter();
        public PublishToWebDialog()
        {
            InitializeComponent();
        }

        private void passwordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            PublishToWebViewModel viewModel = DataContext as PublishToWebViewModel;
            if (viewModel != null) viewModel.Password = passwordBox.Password;
        }
    }
}
