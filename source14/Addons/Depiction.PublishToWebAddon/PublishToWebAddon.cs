﻿using System;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.MVVM;

namespace Depiction.PublishToWebAddon
{
    [DepictionAddonBaseMetadata (AddonPackage = "Publish to web", DisplayName = "Publish to Web", Company = "Depiction, Inc.", Description = "Addon for publishing depictions to the web.")]
    public class PublishToWebAddon : IDepictionAddonBase
    {
        public object AddonMainView
        {
            get { throw new NotImplementedException(); }
        }

        public string AddonConfigViewText
        {
            get { return "Publish Your Depiction"; }
        }

        public object AddonConfigView
        {
            get { return new PublishToWebDialog {DataContext = new PublishToWebViewModel() }; }
        }

        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }
    }
}
