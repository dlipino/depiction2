using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.APIUnmanaged.Service;

namespace Depiction.PreparednessExtension.Importers
{
    [DepictionNonDefaultImporterMetadata("HistoricalWildfires2002_2009",
        Description = "Names and perimeters of recent wildfires in the U.S., provided by the Geospatial Multi-Agency Coordination Group or GeoMAC and hosted by the U.S. Geological Survey (USGS). Additional info: <a href=\"http://rmgsc.cr.usgs.gov/rmgsc/apps.shtml\">http://rmgsc.cr.usgs.gov/rmgsc/apps.shtml</a>",
        DisplayName = "Wildfires 2002-2010 (USGS)",
        ImporterSources = new[] { InformationSource.Web }, ValidRegions = new[] { "US" }, AddonPackage = "Preparedness")]
    public class HistoricalWildfireImporter : NondefaultDepictionImporterBase
    {
        #region Overrides of AbstractDepictionDefaultImporter

        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Area", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new WmsElementProviderBackground();
            var name = string.Format("Wildfires 2002-2010 (USGS)");
            var layerNames =
                "2002fires,2003fires,2004fires,2005fires,2006fires,2007fires,2008fires,2009fires,2010fires,prevmod,prevper,prevperlab,25,hms,modis,actperim";
            parameters.Add("layerName", layerNames);
            //        http://wildfire.cr.usgs.gov/wmsconnector/com.esri.wms.Esrimap/geomac_wms
            if (!parameters.ContainsKey("name"))
            {
                parameters.Add("name", name);
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }

        #endregion
    }
}