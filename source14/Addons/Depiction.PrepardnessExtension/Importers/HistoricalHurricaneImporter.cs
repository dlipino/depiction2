using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.Service;

namespace Depiction.PreparednessExtension.Importers
{
    [DepictionNonDefaultImporterMetadata("HistoricalHurricaneTrackImporter",
        Description = "These lines indicate the path, date, name and intensity of storms from 1851 through 2006, from the National Oceanic and Atmospheric Administration (NOAA) records. See <a href=\"http://csc-s-maps-q.csc.noaa.gov/hurricanes\">http://csc-s-maps-q.csc.noaa.gov/hurricanes</a> for more information. ",
        DisplayName = "Historical Hurricane Tracks (NOAA)",
        ImporterSources = new[] { InformationSource.Web }, ValidRegions = new[] { "US" }, AddonPackage = "Preparedness")]
    public class HistoricalHurricaneImporter : NondefaultDepictionImporterBase
    {
        #region Overrides of AbstractDepictionDefaultImporter

        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
           
            var operationThread = new HistoricalHurricaneImporterBackgroundService();
            var name = string.Format("Historical Hurricane Tracks (NOAA)");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }

        #endregion
    }

    public class HistoricalHurricaneImporterBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        string name = string.Format("Historical Hurricane Tracks (NOAA)");
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var area = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (area == null) return null;
            var typeKey = DepictionWxsToElementService.elementTypeParamString;
            var hurricaneType = "Depiction.Plugin.HurricaneTrack";
            if (!parameters.ContainsKey(typeKey))
            {
                parameters.Add(typeKey,hurricaneType );
            }else
            {
                parameters[typeKey] = hurricaneType; 
            }
            //            string errorMessage;
            //            var elements = DepictionAccess.WfsElementFactory.RequestElementsFromWfs(gathererName, new Dictionary<string, string> { { "LayerName", "allhurtrack_Type" } },
            //                new Uri("http://portal.depiction.com/geoserver/wfs"), "Depiction.Plugin.HurricaneTrack", SourceType.Quickstart, out errorMessage);
            //            if (elements == null)
            //            {
            //                DepictionAccess.NotificationService.SendCouldNotGetDataNotification(SourceType.Quickstart, gathererName);
            //                return new IElement[0];
            //            }

            return GatherElements(area, parameters);

        }

        protected override void ServiceComplete(object args)
        {
            if (DepictionAccess.CurrentDepiction == null) return;
            var elements = args as IEnumerable<IElementPrototype>;
            if (elements == null || elements.Count() == 0)
            {
                var message =
                    string.Format(
                        "Could not add {0} The source may be unavailable or there is no data for the requested area", name);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return;
            }

            UpdateStatusReport("Adding hurricane tracks");
            DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(elements, true,false);

        }

        #endregion

        #region From 1.2

        public IElementPrototype[] GatherElements(IMapCoordinateBounds area, Dictionary<string, object> parameters)
        {
            var prototypes = DepictionWxsToElementService.GetDataFromWxSUsingParameter(parameters, area, this);

            //combine elements
            var parentElements = CreateCombinedElements(prototypes);

            List<object> cats = new List<object>();

            foreach (var element in prototypes)
            {
                var prop = element.GetPropertyByInternalName("YEAR");
                if (prop != null)
                    prop.IsHoverText = true;
                prop = element.GetPropertyByInternalName("WIND_KTS");
                if (prop != null)
                    prop.IsHoverText = true;
                prop = element.GetPropertyByInternalName("CAT");
                if (prop != null)
                {
                    prop.IsHoverText = true;
                    cats.Add(prop.Value);
                }
                prop = element.GetPropertyByInternalName("DisplayName");
                if (prop != null)
                    prop.IsHoverText = true;
                prop = element.GetPropertyByInternalName("PRESSURE");
                if (prop != null)
                    prop.IsHoverText = true;
                element.UsePropertyNameInHoverText = true;
            }
            List<Color> colorPoints = new List<Color>();
            colorPoints.Add(Colors.DarkBlue);
            colorPoints.Add(Colors.LightBlue);

            List<object> domainPoints = new List<object>();
            cats.Sort();
            domainPoints.Add(cats.First());
            domainPoints.Add(cats.Last());
            var colorMap = new ColorInterpolator(domainPoints, colorPoints);
            var borderName = "ZOIBorder";
            foreach (var element in prototypes)
            {
                var prop = element.GetPropertyByInternalName("CAT");
                if (prop != null)
                {
                    var propVal = colorMap.GetColorForValue(prop.Value);
                    if (element.SetPropertyValue(borderName, propVal) != true)
                    {
                        var newProp = new DepictionElementProperty(borderName, "Line color", propVal);
                        newProp.Deletable = false;
                        newProp.Rank = 10;
                        element.AddPropertyOrReplaceValueAndAttributes(newProp, false);
                    }
                    //= colorMap.GetColorForValue(prop.Value).ToString();
                }
            }
            return parentElements.ToArray();
        }
        //Combine the needed zois with matching hurricane ids
        private static IEnumerable<IElementPrototype> CreateCombinedElements(IEnumerable<IElementPrototype> elements)
        {
            var elementDictionary = new Dictionary<string, List<IElementPrototype>>();
            foreach (var element in elements)
            {
                var prop = element.GetPropertyByInternalName("BTID");
                if (prop != null)
                {
                    var propValue = prop.Value.ToString().ToLowerInvariant();
                    if (elementDictionary.ContainsKey(propValue))
                    {
                        elementDictionary[propValue].Add(element);
                    }
                    else
                    {
                        var protoList = new List<IElementPrototype>();
                        protoList.Add(element);
                        elementDictionary.Add(propValue, protoList);
                    }
                }
            }
            return elements;
            //            var combinedZOIPrototypes = new List<IElementPrototype>();
            //            foreach (var kvp in elementDictionary.Values)
            //            {
            //                var prototype = ElementFactory.CreateRawPrototypeOfType("Depiction.Plugin.HurricaneTrack");
            //
            //
            //                var prop = DepictionAccess.PropertyFactory.CreateProperty("Year", "Year", kvp[0].GetProperty("YEAR").Value, null, null, false, true);
            //                prop.IsHoverText = true;
            //                newElement.AddProperty(prop);
            //
            //                prop = newElement.GetProperty("DisplayName");
            //                prop.IsHoverText = true;
            //                prop.Set(kvp[0].GetProperty("DisplayName").Value, SetWeight.User);
            //
            //                prop = DepictionAccess.PropertyFactory.CreateProperty("BTID", "BTID", kvp[0].GetProperty("BTID").Value, null, null, false, true);
            //                prop.IsHoverText = true;
            //                newElement.AddProperty(prop);
            //
            //                foreach (var element in kvp)
            //                {
            //                    newElement.AddChild(element);
            //                }
            //
            //                combinedElements.Add(newElement);
            //            }
            //            return combinedElements;
        }
        #endregion
    }
}