﻿using System.ComponentModel;
using System.Windows;
using Depiction.LicenseManager.Model;

namespace Depiction.DriveTimeAddon
{
    /// <summary>
    /// Interaction logic for LicenseInformationDialog.xaml
    /// </summary>
    public partial class LicenseInformationDialog : INotifyPropertyChanged
    {
        private string license;

        public LicenseInformationDialog(LicenseService Service)
        {
            this.Service = Service;
            InitializeComponent();
        }

        public LicenseInformationDialog()
        {
            InitializeComponent();
        }

        public LicenseService Service { get; set; }

        private void btnDeactivate_Click(object sender, RoutedEventArgs e)
        {

        }

        public string LicenseKey
        {
            get
            {
                if (Service == null) return "No License";
                if (string.IsNullOrEmpty(license))
                {
                    license = Service.SerialNumber;
                }
                return license;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
