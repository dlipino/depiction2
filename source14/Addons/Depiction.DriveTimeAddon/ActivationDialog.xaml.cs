﻿using System;
using System.Windows;
using Depiction.API;
using Depiction.LicenseManager.Model;

namespace Depiction.DriveTimeAddon
{
    /// <summary>
    /// Interaction logic for ActivationDialog.xaml
    /// </summary>
    public partial class ActivationDialog
    {
        public ActivationDialog()
        {
            InitializeComponent();
        }

        public LicenseService Service { get; set; }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            string message;
            bool activated = Service.ActivateLicense(serialNumberText.Text, false, out message);
                if (activated)
                {
                    DepictionAccess.NotificationService.SendNotificationDialog(this,
                        "Congratulations! Your copy of Depiction - Logistics Addon is now activated.",
                        "Depiction - Logistics Addon activated");
                    Close();
                }
                else
                {
                    DepictionAccess.NotificationService.SendNotificationDialog(this,
                        string.Format("Your copy of Depiction - Logistics Addon failed to activate.<br/><br/>{0}", message),
                        "Depiction - Logistics Addon not activated");
                }
            if (activated)
                Close();
        }

        private void buyButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void RequestManualActivationButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnManuallyActivate_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
