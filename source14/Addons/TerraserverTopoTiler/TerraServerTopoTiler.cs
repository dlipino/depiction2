﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged.Service;

namespace TerraserverTopoTiler
{
    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("TerraServerTopoImporter", Description = "Topographic Map (USGS)",
    //    DisplayName = "Topographic Map (USGS)")]
    public sealed class TerraServerTopoTiler : TerraServerTilerBase
    {
        public TerraServerTopoTiler()
        {
            cacheService = new TileCacheService(SourceName);
        }
        public override TileImageTypes TileImageType
        {
            get { return TileImageTypes.Topographic; }
        }

        public override string DisplayName
        {
            get { return "Topographic Map (USGS)"; }
        }

        public override string SourceName
        {
            get { return "TerraServerTopo"; }
        }

        public override string LegacyImporterName
        {
            get { return "TerraServerTopoImporter"; }
        }

        protected override LayerAndMaxResolution[] Layers
        {
            get { return new[] { new LayerAndMaxResolution { LayerID = 2, MaxZoomLevel = 11 } }; }
        }
    }

    public abstract class TerraServerTilerBase : IUTMTiler
    {

        protected int currentLayer = 0;//This is a really bad name, easily confused

        public int PixelWidth { get { return 200; } }
        //protected int PixelsPerTile
        //{
        //    get { return 200; }
        //}

        ImageWarperService warpService = new ImageWarperService();
        protected abstract LayerAndMaxResolution[] Layers { get; }
        public abstract string DisplayName { get; }
        public abstract string SourceName { get; }
        public abstract string LegacyImporterName { get; }

        public abstract TileImageTypes TileImageType { get; }
        public bool DoesOwnCaching { get { return true; } }
        public int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i;
            double widthInMeters = boundingBox.TopLeft.DistanceTo(boundingBox.TopRight, MeasurementSystem.Metric, MeasurementScale.Normal);
            var maxZoomInLevel = Layers[currentLayer].MaxZoomLevel;
            // the 1/4 and 1/2 meter resolutions seems to fail, so restrict it to 1 meter
            for (i = 19; i > maxZoomInLevel; i--)
            {
                double metersPerTile = GetMetersPerPixelAtZoomLevel(i) * PixelWidth;
                if ((widthInMeters / metersPerTile) > minTilesAcross) break;
            }

            return i;
        }

        public IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            List<TileModel> tiles = new List<TileModel>();
            //cancelled = false;
            if (boundingBox.TopLeft.ConvertToUTM().UTMZoneNumber != boundingBox.TopRight.ConvertToUTM().UTMZoneNumber)
                return tiles;
            //throw new Exception("We don't deal with crossing UTM boundaries quite yet ...");
            // This doesn't cross a UTM Zone, so we can handle it much like mercator tiles ...
            var tileScaleLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            //tileScaleLevel = 12;



            try
            {
                int startRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, tileScaleLevel);
                int startCol = LongitudeToColAtZoom(boundingBox.BottomLeft, tileScaleLevel);
                int endCol = LongitudeToColAtZoom(boundingBox.TopRight, tileScaleLevel);
                int endRow = LatitudeToRowAtZoom(boundingBox.TopRight, tileScaleLevel);

                var orderedTiles = TilesToGet(boundingBox.TopLeft.ConvertToUTM().UTMZoneNumber, startCol, endCol, startRow, endRow);

                //What a mess, the X,Y are totally random.
                foreach (var point in orderedTiles)
                {

                    var tile = GetTileModel(point, tileScaleLevel);
                    tiles.Add(tile);
                    //var tile = GetUTMTile((int)point.Y, (int)point.X, tileScaleLevel, utmZoneNumber, Layers[currentLayer].LayerID);
                    //if (tile != null)
                    //{
                    //    if (BackgroundTileReceived != null) BackgroundTileReceived(tile);
                    //}
                    //if (!cancelled) continue;
                    //return;
                }
                return tiles;
            }
            catch (Exception ex)
            {
                #region im sure it is improtant
                //                if (ex is InvalidZoomException && layers.Length > (currentLayer + 1))
                //                {
                //                    currentLayer++;
                //                    GetTiles(boundingBox, minTilesAcross, maxZoomLevel);
                //                }
                //                else
                //                    throw new Exception(ex.Message);
                #endregion
            }
            finally
            {
                currentLayer = 0;
            }
            //cancelled = false;
            return null;
        }

        protected static TileCacheService cacheService;
        private bool replaceCachedImages { get { return Settings.Default.ReplaceCachedFiles; } }
        //int tileXLeft, int tileYBottom, int tileScale, int utmZoneNumber, int layerID
        public byte[] GetUTMTile(TileModel tile)
        {
            var utmMult = GetUTMMultiplier(tile.TileZoomLevel);
            var tileXRight = tile.Column + 1; //Long=X
            var tileYTop = tile.Row + 1; //lat = y

            var topLeftUTM = new UTMLocation()
            {
                EastingMeters = tile.Column * utmMult,
                NorthingMeters = tileYTop * utmMult,
                UTMZoneLetterString = "unknown",
                UTMZoneNumber = tile.UTMZone
            };
            var bottomRightUTM = new UTMLocation()
            {
                EastingMeters = tileXRight * utmMult,
                NorthingMeters = tile.Row * utmMult,
                UTMZoneLetterString = "unknown",
                UTMZoneNumber = tile.UTMZone
            };
            var topleftLatLong = topLeftUTM.UTMToLatLong();
            var bottomRightLatLong = bottomRightUTM.UTMToLatLong();
            var extension = ".png";//Because of warping these need to be saved as png
            IMapCoordinateBounds imageBounds = new MapCoordinateBounds(topleftLatLong, bottomRightLatLong);
            //var cacheName = SourceName + "_" + tile.Row + "_" + tile.Column + "_" + tile.TileZoomLevel + ".jpg";
            var cacheName = SourceName + "_X_" + tile.Row + "_Y_" + tile.Column + "_Scale_" + tile.TileZoomLevel + "_UTM_" +
                            tile.UTMZone + "_Layer_" + Layers[0].LayerID + extension;
            var fullCacheFileName = cacheService.GetCacheFullStoragePath(cacheName);
            //var fileName = Path.GetTempFileName();
            string warpCacheName = SourceName + "_X_" + tile.Column + "_Y_" + tile.Row + "_Scale_" + tile.TileZoomLevel + "_UTM_" +
                                   tile.UTMZone + extension;
            var warpedBounds = cacheService.RetrieveCachedWarpBounds(warpCacheName);

            //TileModel tileModel = null;
            if (warpedBounds == null || !File.Exists(fullCacheFileName) || replaceCachedImages)//No warped bounds so create everything
            {
                //if (!File.Exists(warpCacheName))
                //    cacheService.CacheWarpBounds(warpCacheName, topleftLatLong, bottomRightLatLong);
                var imageLocationString = string.Format(CultureInfo.InvariantCulture,
                                                        "http://msrmaps.com/tile.ashx?T={4}&S={0}&X={1}&Y={2}&Z={3}",
                                                        tile.TileZoomLevel, tile.Column, tile.Row, tile.UTMZone, Layers[0].LayerID
                    );

                int errorThreshold = 1; //WHo knows what this is for?
                var warpSuccessfull = warpService.GetImageFromWebWarpImageGetBoundsAndSave(imageLocationString, topLeftUTM,
                                                                                           GetMetersPerPixelAtZoomLevel(tile.TileZoomLevel),
                                                                                           GetMetersPerPixelAtZoomLevel(tile.TileZoomLevel),
                                                                                           out imageBounds,
                                                                                           errorThreshold,
                                                                                           PixelWidth, PixelWidth,
                                                                                           fullCacheFileName);
                if (warpSuccessfull)
                {
                    tile.TopLeft = imageBounds.TopLeft;
                    tile.BottomRight = imageBounds.BottomRight;
                    cacheService.CacheWarpBounds(warpCacheName, imageBounds.TopLeft, imageBounds.BottomRight);
                    var bytes = File.ReadAllBytes(fullCacheFileName);
                    return bytes;

                    //tileModel = new TileModel(tileScale, cacheName, fullCacheFileName, imageBounds.TopLeft, imageBounds.BottomRight);
                }
            }
            else
            {
                tile.TopLeft = warpedBounds.TopLeft;
                tile.BottomRight = warpedBounds.BottomRight;
                var bytes = File.ReadAllBytes(fullCacheFileName);
                return bytes;
                //tileModel = new TileModel(tileScale, cacheName, fullCacheFileName, warpedBounds.TopLeft, warpedBounds.BottomRight);
            }
            //return tileModel;
            return null;
        }

        public int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            return (int)Math.Floor(latLong.ConvertToUTM().EastingMeters / GetUTMMultiplier(zoom));
        }

        public int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            return (int)Math.Floor(latLong.ConvertToUTM().NorthingMeters / GetUTMMultiplier(zoom));
        }

        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            throw new NotImplementedException();
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            throw new NotImplementedException();
        }

        public TileModel GetTileModel(TileXY tileToGet, int tileScaleLevel)
        {

            int utmZoneNumber = tileToGet.UTMZoneNumber;
            var utmTopLeft = new UTMLocation();
            utmTopLeft.EastingMeters = tileToGet.Column * GetUTMMultiplier(tileScaleLevel);
            utmTopLeft.NorthingMeters = (tileToGet.Row + 1) * GetUTMMultiplier(tileScaleLevel);
            utmTopLeft.UTMZoneNumber = utmZoneNumber;
            var utmBottomRight = new UTMLocation();
            utmBottomRight.EastingMeters = (tileToGet.Column + 1) * GetUTMMultiplier(tileScaleLevel);
            utmBottomRight.NorthingMeters = (tileToGet.Row) * GetUTMMultiplier(tileScaleLevel);
            utmBottomRight.UTMZoneNumber = utmZoneNumber;
            var tile = new TileModel(tileScaleLevel, tileToGet.Row, tileToGet.Column, null, utmTopLeft.UTMToLatLong(), utmBottomRight.UTMToLatLong());
            tile.UTMZone = utmZoneNumber;
            tile.FetchStrategy = GetUTMTile;
            return tile;
        }

        private IEnumerable<TileXY> TilesToGet(int utmZoneNumber, int startCol, int endCol, int startRow, int endRow)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = startRow + (endRow - startRow) / 2;
            int centerCol = startCol + (endCol - startCol) / 2;
            for (int j = startRow; j <= endRow; j++)
            {
                for (int i = startCol; i <= endCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j, UTMZoneNumber = utmZoneNumber }));
                }
            }
            IEnumerable<TileXY> sortedTiles =
                from pair in tilesToGet
                orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                select pair.Value;
            return sortedTiles;
        }

        private static int GetUTMMultiplier(int terraScale)
        {
            switch (terraScale)
            {
                case 8:
                    return 50;
                case 9:
                    return 100;
                case 10:
                    return 200;
                case 11:
                    return 400;
                case 12:
                    return 800;
                case 13:
                    return 1600;
                case 14:
                    return 3200;
                case 15:
                    return 6400;
                case 16:
                    return 12800;
                case 17:
                    return 25600;
                case 18:
                    return 51200;
                case 19:
                    return 102400;
            }

            return 0;
        }

        private static double GetMetersPerPixelAtZoomLevel(int zoom)
        {
            switch (zoom)
            {
                case 8:
                    return .25;
                case 9:
                    return .5;
                case 10:
                    return 1;
                case 11:
                    return 2;
                case 12:
                    return 4;
                case 13:
                    return 8;
                case 14:
                    return 16;
                case 15:
                    return 32;
                case 16:
                    return 64;
                case 17:
                    return 128;
                case 18:
                    return 256;
                case 19:
                    return 512;
            }

            return 0;
        }

        #region Nested type: LayerAndMaxResolution

        protected struct LayerAndMaxResolution
        {
            public int LayerID { get; set; }
            public int MaxZoomLevel { get; set; }
        }

        #endregion
    }
}
