﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("AddRouteWaypoint", "Add a route waypoint", "Set initial route waypoints")]
    public class AddRouteWaypointBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Location", typeof(LatitudeLongitude))
                };
        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Initiate route waypoints"; }
        }

        public string Description
        {
            get { return "Sets initial route waypoints."; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var location = parameterBag["Location"] as LatitudeLongitude;
            if (location == null) return new BehaviorResult();
            var waypoints = subscriber.Waypoints;
            var geomFact = new GeometryFactory();
            var point = new Point(location.Longitude, location.Latitude);
            var lineSegments = new List<DepictionGeometry>();
            var zoiCoords = subscriber.ZoneOfInfluence.Geometry.Geometry.Coordinates;
            var coordCount = 0;
            for (int i = 0; i < (waypoints.Length - 1); i++)
            {
                ICoordinate[] coordinates = new Coordinate[2];
                coordinates[0] =
                    new Coordinate(waypoints[i].Location.Longitude,
                                   waypoints[i].Location.Latitude);

                coordinates[1] =
                    new Coordinate(waypoints[i + 1].Location.Longitude,
                                   waypoints[i + 1].Location.Latitude);

                var routePoints = new List<ICoordinate>();
                for (; coordCount < zoiCoords.Length; coordCount++)
                {
                    routePoints.Add(zoiCoords[coordCount]);
                    if (zoiCoords[coordCount].Equals2D(coordinates[1]))
                    {
                        break;
                    }
                }

                lineSegments.Add(new DepictionGeometry(geomFact.CreateLineString(routePoints.ToArray())));

            }
            int firstIndex;
            bool foundPoint = DepictionGeometry.FindInsertPointIntoClosestLineSegment(lineSegments, point, out firstIndex);
            if (!foundPoint) return new BehaviorResult();
            var waypoint = new DepictionElementWaypoint
                               {
                                   Location = location,
                                   IconSize = 16,
                                   Name = "WayPoint"
                               };
            subscriber.InsertWaypoint(firstIndex + 1, waypoint);
            return new BehaviorResult();

            //This way seems shorter and less bloated
            //            for (int i = 0; i < subscriber.Nodes.Length - 1; i++)
            //            {
            //                var coords = new List<Coordinate>();
            //                coords.Insert(0, new Coordinate(waypoints[i].Location.Longitude, waypoints[i].Location.Latitude));
            //                coords.Add(new Coordinate(waypoints[i + 1].Location.Longitude, waypoints[i + 1].Location.Latitude));
            //                var line = geomFact.CreateLineString(coords.ToArray());
            //
            //                bool foundPoint = DepictionGeometry.FindInsertPointIntoClosestLineSegment(line, location, out firstIndex);
            //                if (line.Envelope.Contains(newGeom))
            //                {
            //                    var node = new DepictionElementWaypoint()
            //                                             {
            //                                                 Location = location,
            //                                                 IconSize = 16,
            //                                                 Name = "WayPoint"
            //                                             };
            //                    subscriber.InsertWaypoint(i + 1, node);
            //                    break;
            //                }
            //            }

        }
    }
}