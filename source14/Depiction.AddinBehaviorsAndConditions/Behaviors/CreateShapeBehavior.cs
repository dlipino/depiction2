using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using Distance=Depiction.CoreModel.ValueTypes.Measurements.Distance;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CreateShape", "Create a shape using input parameters", "Give element zoi a desired shape")]
    public class CreateShapeBehavior : BaseBehavior
    {
        List<ParameterInfo> arbitraryParams = new List<ParameterInfo>();
        #region Overrides of BaseBehavior

        public override ParameterInfo[] Parameters
        {
            get { return arbitraryParams.ToArray(); }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var radius = (Distance)parameterBag["Radius"];
            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;

            var shapeToCreate = "Circle";
            if(parameterBag.ContainsKey("Shape"))
            {
                shapeToCreate = (string)parameterBag["Shape"];
            }
            List<Point> points;
            switch (shapeToCreate)
            {
                case "Octagon":
                    points = ShapeCreatingService.CreateOctagon(radius.GetValue(measureSystem,measureScale));
                    break;
                case "Circle":
                    points = ShapeCreatingService.CreateCircle(radius.GetValue(measureSystem,measureScale));
                    break;
                default:
//                    string message = string.Format("Element \"{0}\" has a \"Shape\" property with an invalid value: \"{1}\" . This could be an error in the element's .dml file. Will use a default zone of influence for this element.",
//                        subscriber.DisplayName(), shapeToCreate);
                    points = ShapeCreatingService.CreateCircle(new Distance(MeasurementSystem.Metric, MeasurementScale.Normal,50).GetValue(measureSystem,measureScale));
//                    DepictionAccess.NotificationService.SendNotification(message);
                    break;
            }
            var pointDistances = MapCoordinateBounds.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);
            var geom =
                new DepictionGeometry(ShapeCreatingService.GeneratePolygonZOIFromPointList(subscriber.Position, points, pointDistances));

            subscriber.SetZOIGeometryAndUpdatePosition(geom);
            return new BehaviorResult { SubscriberHasChanged = true };
 
        }

        #endregion
    }
}