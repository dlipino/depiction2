﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes.Measurements;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("UpdateTerrainPlusStructureBehavior", "Change terrain values", "Change terrain")]
    public class UpdateTerrainPlusStructureBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Zone", typeof(IDepictionElementBase)), 
                    new ParameterInfo("Height", typeof(Distance))
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Update terrain data structure"; }
        }

        public string Description
        {
            get { return "Update terrain data structure"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var terrainProp = subscriber.GetPropertyByInternalName("Terrain");

            Terrain terrain;
            if (terrainProp == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Behavior {0} could not find elevation data.", FriendlyName));
                return new BehaviorResult { SubscriberHasChanged = false };
            }
            subscriber.RemovePropertyIfNameAndTypeMatch(terrainProp);

            terrain = terrainProp.Value as Terrain;
            if (terrain == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Behavior {0} could not find elevation data.", FriendlyName));
                return new BehaviorResult { SubscriberHasChanged = false };
            }

            var elemWithZOI = parameterBag["Zone"] as IDepictionElement;
            var heightProp = parameterBag["Height"] as Distance;
            if (heightProp == null || elemWithZOI == null) return new BehaviorResult();

            var terrainZOI = elemWithZOI.ZoneOfInfluence;
            var rectangleHeight = heightProp.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);


            lock (terrain)
            {
                try
                {
                    int width = terrain.GetGridWidthInPixels();

                    int height = terrain.GetGridHeightInPixels();
                    double gridSpacing = terrain.GetGridResolution();
                    var geomFactory = new GeometryFactory();

                    //find the bounding box of the rectangle and
                    //update the terrain only within the bounding box
                    var zoiGeom = terrainZOI.Geometry.BoundingBox;
                    ICoordinate[] zoiCoords = zoiGeom.Geometry.Coordinates;
                    double minLon = 1000, maxLon = -1000, minLat = 1000, maxLat = -1000;
                    for (int i = 0; i < zoiCoords.Length; i++)
                    {
                        ICoordinate coord = zoiCoords[i];
                        if (coord.X < minLon) minLon = coord.X;
                        if (coord.X > maxLon) maxLon = coord.X;
                        if (coord.Y < minLat) minLat = coord.Y;
                        if (coord.Y > maxLat) maxLat = coord.Y;
                    }
                    //find the pixel row/col of the min/max coordinates of the rectangle
                    int minRow, maxRow, minCol, maxCol;
                    //remember that the bottom most row (lowest latitude) is Row 0
                    //with Row height-1 at the top (max latitude)
                    //
                    maxRow = (maxRow = terrain.GetRow(maxLat)) < 0 ? 0 : maxRow;
                    minRow = (minRow = terrain.GetRow(minLat)) > height - 1
                                 ? height - 1
                                 : minRow;
                    minCol = (minCol = terrain.GetColumn(minLon)) < 0 ? 0 : minCol;
                    maxCol = (maxCol = terrain.GetColumn(maxLon)) > width - 1
                                 ? width - 1
                                 : maxCol;

                    //pad the bounding box row/col extent by 1, just to cover
                    //for the dreaded n-1 problem
                    minRow = (minRow - 1) >= 0 ? (minRow - 1) : 0;
                    minCol = (minCol - 1) >= 0 ? (minCol - 1) : 0;
                    maxRow = (maxRow + 1) > (height - 1) ? (height - 1) : maxRow + 1;
                    maxCol = (maxCol + 1) > (width - 1) ? (width - 1) : maxCol + 1;

                    for (int row = minRow; row <= maxRow; row++)
                        for (int col = minCol; col <= maxCol; col++)
                        {
                            double lat = terrain.GetLatitude(row);
                            double lon = terrain.GetLongitude(col);

                            var gridCellBox = GeoProcessor.CreateGridCellPolygon(geomFactory, lon, lat, gridSpacing);

                            if ( /*pt*/terrainZOI.Geometry.Intersects(gridCellBox))
                            {
                                float elevValue = terrain.GetValueAtGridCoordinate(col, row);
                                terrain.SetValueAtGridCoordinate(col, row, (float)rectangleHeight + elevValue);
                            }
                        }
                }
                catch (Exception ex)
                {
                    DepictionAccess.NotificationService.DisplayMessageString(
                        string.Format("Error modifying the terrain.\n\nInternal erorr: {0}", ex.Message));
                    return new BehaviorResult { SubscriberHasChanged = false };
                }
                terrain.ClearVisual();
                var imageMetadata = terrain.Visual;
                //this part should probalby be moved to the ImageMetadata setter
                string newFileName;

                BitmapSaveLoadService.LoadImageStoreInDepictionImageResourcesAndRemoveOldKey(
                    imageMetadata.ImageFilename, subscriber.ImageMetadata.ImageFilename, out newFileName);
                imageMetadata.ImageFilename = newFileName;
                subscriber.SetImageMetadata(imageMetadata);
                var notifyChange = true;
                subscriber.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("Terrain", terrain) { VisibleToUser = false }, notifyChange);
                //                subscriber.ImageMetadata = imageMetadata;
            }

            return new BehaviorResult { SubscriberHasChanged = true };
        }
    }
}