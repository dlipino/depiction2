﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CreateArc", "Calculate a simple arc", "Calculates a simple  arc")]
    public class CreateArcBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Radius", typeof (Distance))
                        {
                            ParameterName = "Radius",
                            ParameterDescription = "The new radius for the arc shape",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (Distance),
                                                                   "The new radius value must be a valid distance")
                                    }
                        },
                    new ParameterInfo("Orientation", typeof (double))
                        {
                            ParameterName = "Orientation",
                            ParameterDescription = "The new orientation angle for the arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new orientation value must be a number")
                                    }
                        },
                    new ParameterInfo("Width", typeof (double))
                        {
                            ParameterName = "Width",
                            ParameterDescription = "The new angle width for the arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new angle width value must be a number")
                                    }
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Create arc"; }
        }

        public string Description
        {
            get { return "Set the element's zone of influence to the specified arc"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var radius = (Distance)parameterBag["Radius"];
            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;
            var angleObject = parameterBag["Orientation"];
            var angleValue = 0d;
            if(angleObject is Angle)
            {
                angleValue = ((Angle)angleObject).Value;
            }else if(angleObject is double)
            {
                angleValue = (double) angleObject;   
            }
            double startAngle = angleValue - (double)parameterBag["Width"] / 2;
            double stopAngle = angleValue + (double)parameterBag["Width"] / 2;

            var points = ShapeCreatingService.CreateArc(radius.GetValue(measureSystem,measureScale), startAngle, stopAngle);
            
            if (points.Count < 3) return new BehaviorResult();
            var pointDistances = MapCoordinateBounds.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);

            var polygon = ShapeCreatingService.GeneratePolygonZOIFromPointList(subscriber.Position, points, pointDistances);
            subscriber.SetZOIGeometry(new DepictionGeometry(polygon));
            return new BehaviorResult { SubscriberHasChanged = true };
        }
    }
}