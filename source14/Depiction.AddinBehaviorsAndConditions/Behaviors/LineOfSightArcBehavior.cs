﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using DepictionCoverage;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("LineOfSightArc", "Calculate a simple line of sight arc", "Calculates a simple line of sight arc")]
    public class LineOfSightArc : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Terrain", typeof (Terrain))
                        {
                            ParameterDescription = "Elevation Data",
                            ParameterName = "The elevation data to use when calculating the flood"
                        },
                    new ParameterInfo("Range", typeof (Distance))
                        {
                            ParameterDescription = "Distance to cover for line of sight",
                            ParameterName = "Range distance"
                        },
                    new ParameterInfo("Height", typeof (Distance))
                        {
                            ParameterDescription = "Height from which to evaluate line of sight",
                            ParameterName = "Height"
                        },
                    new ParameterInfo("Orientation", typeof (Angle))
                        {
                            ParameterName = "Orientation",
                            ParameterDescription = "The new orientation angle for the line of sight arc"
//                            ,ValidationRules =
//                                new IValidationRule[]
//                                    {
//                                        new DataTypeValidationRule(typeof (Angle),
//                                                                   "The new orientation value must be a number")
//                                    }
                        },
                    new ParameterInfo("Width", typeof (double))
                        {
                            ParameterName = "Width",
                            ParameterDescription = "The new angle width for the line of sight arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new angle width value must be a number")
                                    }
                        },
                    new ParameterInfo("Resolution", typeof (Distance))
                        {
                            ParameterDescription = "Sampling interval at which to evaluate line of sight",
                            ParameterName = "Resolution"
                        },
                    new ParameterInfo("HeightTolerance", typeof (Distance))
                        {
                            ParameterDescription = "Height tolerance that accounts for elevation data accuracy",
                            ParameterName = "Height Tolerance"
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }

        }

        public string FriendlyName
        {
            get { return "Create line of sight arc polygon"; }
        }

        public string Description
        {
            get { return "Create line of sight arc polygon"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {

            if (!subscriber.Position.IsValid)
                return new BehaviorResult();
            //WHy are all the parameters element properties?
            var terrainPlusStructure = parameterBag["Terrain"] as Terrain;
            var radius = parameterBag["Range"] as Distance;
            var height = parameterBag["Height"] as Distance;
            var width = (double)(parameterBag["Width"]);
            var orientation = (double)(parameterBag["Orientation"]); 
            if(radius==null || height == null || terrainPlusStructure == null)
            {
                if(terrainPlusStructure == null)
                {
                    DepictionAccess.NotificationService.DisplayMessageString(
                        "Cannot run line of sight interaction without elevation", 5);
                    subscriber.SetPropertyValue("Active", false, true);
                }
                return new BehaviorResult();
            }
            bool active;
            if(subscriber.GetPropertyValue("Active", out active))
            {
                if(!active)
                {
                    subscriber.SetPropertyValue("Active", true, true);
                }
            }

            var northernOrientation = 90 - orientation;
            double startAngle = northernOrientation - width / 2;
            double stopAngle = northernOrientation + width / 2;
            if (startAngle < 0) startAngle += 360;
            if (stopAngle < 0) stopAngle += 360;
            if (stopAngle > 360) stopAngle = 360 - stopAngle;
            //this makes sure start and stop angles are always between  and 360

            var samplingInterval = parameterBag["Resolution"] as Distance;
            var heightTolerance = parameterBag["HeightTolerance"] as Distance;
            if (heightTolerance == null || samplingInterval == null) return new BehaviorResult();
            //we want everything internally represented in meters
            var radiusMeters = radius.GetValue(MeasurementSystem.Metric,MeasurementScale.Normal);
            var heightMeters = height.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            var sampingIntervalInMeters = samplingInterval.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            var heightToleranceMeters = heightTolerance.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            DepictionAccess.NotificationService.DisplayMessageString(string.Format(
                                                                     "{0} calculations for large ranges may take many minutes...", subscriber.TypeDisplayName),3);
            long memAllocatedBefore = GC.GetTotalMemory(true);
            subscriber.SetZOIGeometry(new DepictionGeometry(subscriber.Position));//reset ZOI to a point
            var elevationData = CreateCoverage(subscriber, terrainPlusStructure, sampingIntervalInMeters);

            UpdateZoneOfInfluence(subscriber, elevationData,subscriber, radiusMeters, heightMeters, heightToleranceMeters,startAngle,stopAngle);

            //delete elevationData
            elevationData.Dispose(); 
            GC.Collect();
            long memAllocatedAfter = GC.GetTotalMemory(true);
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        private static void UpdateZoneOfInfluence(IDepictionElement subscriber, Coverage elevationData, IDepictionElement LOSObject, double radius, double height, double heightTolerance, double startAngle, double stopAngle)
        {
//            LOSObject.ClearZoneOfInfluence();
            var geomFactory = new GeometryFactory();
            //
            //Get the Line Of Sight object's latlon
            //
            double y = LOSObject.Position.Latitude;
            double x = LOSObject.Position.Longitude;

            //if the location is outside the Depiction region, LOS runs into problems
            //the elevation outside the region is unknown. The tower sits on top of this elevation
            //what do you do?
            //kick em out
            double elevvalue = elevationData.GetClosestElevationValue(x, y);
            if(elevvalue.Equals(Double.NaN))
            {
                //unknown elevation or outside the Depiction region
                //notify

                DepictionAccess.NotificationService.DisplayMessageString(string.Format(
                                                                         "{0} has been placed at a location of unknown elevation value. Please place it where there is elevation and try again.", subscriber.TypeDisplayName));
                return ;
            }

            var cList = new ArrayList();
            radius = GetRadiusInPixels(elevationData, radius);
            //
            //compute line of sight
            //
            elevationData.LineOfSightContours(x, y, height, radius, heightTolerance, cList,startAngle,stopAngle);
            
            //death to arraylists, for now localize them here and keep them from infecting other things
            List<List<LatitudeLongitude>> actualList = ArrayListToActualList(cList);

            List<LatitudeLongitude>[] contourList = GeoProcessor.ConvertToPolygon(actualList, -1000, -1000, 0/*tolerance around seed point*/);
            if (contourList != null)
            {
                lock (LOSObject.ZoneOfInfluence)
                {
//                    LOSObject.ClearZoneOfInfluence();
                    ILinearRing shell = null;
                    var holes = new List<ILinearRing>();
                    for (int i = 0; i < contourList.Length; i++)
                    {
                        if (i == 0)
                        {
                            List<LatitudeLongitude> shellCoordinates = contourList[i];
                            var coordinates = new CoordinateList();

                            foreach (LatitudeLongitude latLong in shellCoordinates)
                            {
                                coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
                            }
                            shell = geomFactory.CreateLinearRing(coordinates.ToArray());
                        }
                        else
                        {
                            var coordinates = new CoordinateList();
                            foreach (LatitudeLongitude latLong in contourList[i])
                            {
                                coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
                            }
                            holes.Add(geomFactory.CreateLinearRing(coordinates.ToArray()));
                        }
                    }

                    IGeometry geometry = geomFactory.CreatePolygon(shell, holes.ToArray());

                    //buffering it to prevent intersection conflicts//alas that causes issues with polygons that are multi polygons
                    //but are not declared as such in the above loop. Basically it will turn the normal polygon into a multipolygon
                    //and then it won't get drawn correctly.
                   //if (!geometry.IsEmpty)
                   //     geometry = geometry.Buffer(0.000001);
                    LOSObject.SetZOIGeometry(new DepictionGeometry(geometry));
                }
            }
            //force garbage collection

            for (int j = 0; j < cList.Count; j++)
            {
                var contour = (ArrayList)cList[j];
                for (int k = 0; k < contour.Count; k++)
                {
                    contour[k] = null;
                }
                contour.Clear();
                cList[j] = null;
            }
            cList.Clear();
            GC.Collect();

        }

        private static List<List<LatitudeLongitude>> ArrayListToActualList(ArrayList cList)
        {
            var cListActualList = new List<List<LatitudeLongitude>>();
            foreach (var list in cList)
            {
                var actualList = new List<LatitudeLongitude>();
                var arrayList = (ArrayList)list;
                foreach (var obj in arrayList)
                {
                    var point = (System.Drawing.PointF)obj;
                    actualList.Add(new LatitudeLongitude(point.Y, point.X));
                }
                cListActualList.Add(actualList);
            }
            return cListActualList;
        }

        private static double GetRadiusInPixels(Coverage data, double radius)
        {

            var widthInPixels = data.GetWidthInPixels();
            var heightInPixels = data.GetHeightInPixels();
            var diagDistance = Math.Sqrt(widthInPixels * widthInPixels + heightInPixels * heightInPixels);
            var cornerPts = new ArrayList();
            data.GetCorners(cornerPts, true);//corners in true geographic coordinates


            var topLeft = new LatitudeLongitude((double)cornerPts[1], (double)cornerPts[0]);
            var bottomRight = new LatitudeLongitude((double)cornerPts[3], (double)cornerPts[2]);
            var diagDistanceInMeters = topLeft.DistanceTo(bottomRight, MeasurementSystem.Metric, MeasurementScale.Normal);

            double radiusInPixels = radius * diagDistance / diagDistanceInMeters;
            return radiusInPixels;
        }

        /// <summary>
        /// Returns a Coverage containing the data from this terrain object.
        /// Will resample the elevation data grid to the resolution defined by the samplingInterval parameter.
        /// </summary>
        /// <param name="subscriber">The element to change.</param>
        /// <param name="terrain">The elevation data for the depiction area.</param>
        /// <param name="samplingInterval">Resample the elevation grid every this number of meters.</param>
        /// <returns></returns>
        private static Coverage CreateCoverage(IDepictionElementBase subscriber, Terrain terrain, double samplingInterval)
        {
            var elevationData = new Coverage();
            lock (terrain)
            {
                int originalNumRows = terrain.GetGridHeightInPixels();
                int originalNumCols = terrain.GetGridWidthInPixels();
                var topLeft = terrain.GetTopLeftPosition();
                var botRight = terrain.GetBottomRightPosition();
                double south = botRight.Latitude;
                double north = topLeft.Latitude;
                double east = botRight.Longitude;
                double west = topLeft.Longitude;

                bool success = elevationData.Create(south, north, east, west, terrain.GetGridWidthInPixels(),
                                                    terrain.GetGridHeightInPixels(),
                                                    terrain.IsFloatMode(), -32768, "", 0);
                if (success)
                {
                    //resample the elevation data grid to the resolution defined by the samplingInterval (in meters) parameter.
                    double resampleFactor = elevationData.ResampleElevationGrid(samplingInterval);

                    //
                    //fill in the elevationData grid values from terrain
                    //fill in the grid with values from terrain
                    for (int col = 0; col < elevationData.GetWidthInPixels(); col++)
                        for (int row = 0; row < elevationData.GetHeightInPixels(); row++)
                        {
                            int origCol = (int)(col / resampleFactor);
                            if (origCol >= originalNumCols - 1) origCol = originalNumCols - 1;
                            int origRow = (int)(row / resampleFactor);
                            if (origRow >= originalNumRows - 1) origRow = originalNumRows - 1;
                            float val = terrain.GetValueAtGridCoordinate(origCol, origRow);
                            elevationData.SetElevationValue(col, row, val);
                        }
                }
                else
                {
                    DepictionAccess.NotificationService.DisplayMessageString(string.Format(
                                                                             "{0} failed; system might be low on memory.", subscriber.TypeDisplayName));
                }
            }
            return elevationData;
        }
    

    }
}