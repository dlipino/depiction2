using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("DestroyZone", "Destroy a road network", "Destroy a road network")]
    public class DestroyRoadNetworkZoneBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters = 
            new[]
                {
                    new ParameterInfo("Destroyer", typeof(IDepictionElementBase))
                        {
                            ParameterDescription = "Specify which element destroys the road network (this is usually the publisher)", 
                            ParameterName = "Choose the Publisher"
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Destroy road network"; }
        }

        public string Description
        {
            get { return "Makes all road segments within the Destroyer elements zone of influence unusable"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var triggerElement = parameterBag["Destroyer"] as IDepictionElement;

            var roadGraphProp = subscriber.GetPropertyByInternalName("RoadGraph");

            subscriber.RemovePropertyIfNameAndTypeMatch(roadGraphProp);
            if (roadGraphProp == null)
            {
                throw new Exception(string.Format("The '{0}' behavior expects its Subscriber to be a Road Network", FriendlyName));
            }
            var roadGraph = roadGraphProp.Value as IRoadGraph;
            if(roadGraph == null) return new BehaviorResult();
            bool notifyChange = DisableIntersectingEdges(triggerElement, roadGraph);
            subscriber.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RoadGraph", roadGraph) { VisibleToUser = false }, notifyChange);
            return new BehaviorResult { SubscriberHasChanged = true };
        }


        private static bool DisableIntersectingEdges(IDepictionElementBase triggerElement, IRoadGraph roadGraph)
        {
            if (triggerElement.ZoneOfInfluence == null || triggerElement.ZoneOfInfluence.Geometry == null) return false;
            var affectedEdges = GetIntersectingEdges(triggerElement.ZoneOfInfluence, roadGraph);
            foreach (var edge in affectedEdges)
            {
                roadGraph.DisableEdge(edge);
            }
            return affectedEdges.Count > 0;
        }

        private static IList<RoadSegment> GetIntersectingEdges(IZoneOfInfluence zoneOfInfluence, IRoadGraph roadGraph)
        {
            var precisionModel = new PrecisionModel(); //FLOATING POINT precision
            var geometryFactory = new GeometryFactory(precisionModel, 32767);
            var edgesAffected = new List<RoadSegment>();
            var graph = roadGraph.Graph;

            foreach (var edge in graph.Edges)
            {
                ICoordinate[] coordinates = new Coordinate[2];
                coordinates[0] = new Coordinate(edge.Source.Vertex.Longitude, edge.Source.Vertex.Latitude);
                coordinates[1] = new Coordinate(edge.Target.Vertex.Longitude, edge.Target.Vertex.Latitude);

                var objLineString = new DepictionGeometry(geometryFactory.CreateLineString(coordinates));
                lock (zoneOfInfluence)
                {
                    if (zoneOfInfluence.Geometry == null) continue;
                    if (objLineString.Intersects(zoneOfInfluence.Geometry))
                    {
                        edgesAffected.Add(edge);
                    }
                }
            }

            return edgesAffected;
        }
    }
}