﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("SimpleFlood", "Calculate a simple flood", "Calculates a simple flood")]
    public class FloodBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Terrain", typeof(ICoverage))
                        {
                            ParameterDescription = "Elevation Data", 
                            ParameterName = "The elevation data to use when calculating the flood"
                        }, 
                    new ParameterInfo("FloodHeight", typeof(Distance))
                        {
                            ParameterDescription = "Indicates how many meters above sea level the flood will rise", 
                            ParameterName = "Height above sea level"
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

  
        //public string FriendlyName
        //{
        //    get { return "Calculate a flood"; }
        //}

        //public string Description
        //{
        //    get { return "Calculate a flood"; }
        //}

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var floodOffset = (Distance)parameterBag["FloodHeight"];
            var floodOffsetMeters = floodOffset.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            if (subscriber.Position == null || !subscriber.Position.IsValid || floodOffsetMeters < 0)
                return new BehaviorResult();

            var terrainData = parameterBag["Terrain"] as ICoverage;


            DoFlood(subscriber, terrainData, floodOffsetMeters);
            return new BehaviorResult { SubscriberHasChanged = true };
        }


        private static void DoFlood(IDepictionElement Flood, ICoverage terrain, double metersAboveFloodOrigin)
        {
            //var elevationData = CreateCoverage(terrain);
            var pos = Flood.Position;

            float elevationAtOrigin = terrain.GetInterpolatedElevationValue(pos);
            double elevationOfFlood = elevationAtOrigin + metersAboveFloodOrigin;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;
            var floodGeometry = SimpleFloodModel.GenerateSimpleFloodGeometryFromTerrain(terrain, pos, elevationOfFlood);
            //var floodGeometry = DepictionAccess.DepictionGeomFactory.CreateFloodGeometryFromTerrain(terrain, pos, elevationOfFlood);
            if (floodGeometry == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Invalid Flood polygon generated. Please try again.");
                return;
            }

            lock (Flood.ZoneOfInfluence)
            {
                {

                    Flood.SetZOIGeometry(floodGeometry);
                }
            }
        }
    }
}
