﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Drawing;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using DepictionCoverage;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using Point=GisSharpBlog.NetTopologySuite.Geometries.Point;


namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("Runoff", "Calculate runoff", "Set element property")]

    public class SetRunoffBehavior : BaseBehavior
    {
        private Coverage flowLayer; //coverage grid that contains only flood
        private Terrain terrain; //coverage grid that contains elevation data
        private Coverage smoothTerrain;//smoothed terrain to cover for vertical inaccuracies
        private int gridHeightInPixels;
        private int gridWidthInPixels;
        private const double MCubedToGallons = 264.172052358;
        private const double NODATA = -32767;
        const float LARGEFLOAT = 100000;
        private LatitudeLongitude seedPosition;
        private int testSeedColumn, testSeedRow;
        private Volume totalSupply;
        private double totalSupplyGallons; //in gallons
        private double triggerThreshold;
        private double gridResolution;
        private double gridArea;
        private RowCol[] demPixels;
        private float heightTolerance;
        private int pitNumber;
        private int smoothingFactor;
        private bool showMaxInundation;
        private static Dictionary<int, double> quantizedAngle = new Dictionary<int, double> { { 0, 2 * Math.PI - Math.PI / 4 }, { 1, 2 * Math.PI }, { 2, Math.PI / 4 }, { 3, Math.PI / 2 }, { 4, Math.PI / 2 + Math.PI / 4 }, { 5, Math.PI }, { 6, Math.PI + Math.PI / 4 }, { 7, 1.5 * Math.PI } };
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("Terrain", typeof (Terrain))
                        {
                            ParameterDescription = "Elevation Data",
                            ParameterName = "The elevation data to use when calculating the flow"
                        },
                    new ParameterInfo("HeightTolerance", typeof (Distance))
                        {
                            ParameterDescription = "The total supply of fluid available (gallons)",
                            ParameterName = "Total Supply (gallons)"
                        },
                    new ParameterInfo("TotalSupply", typeof (Volume))
                        {
                            ParameterDescription = "The total supply of fluid available (gallons)",
                            ParameterName = "Total Supply (gallons)"
                        },
                    new ParameterInfo("SmoothingFactor", typeof (Int32))
                        {
                            ParameterDescription = "Smoothing factor for terrain data (0: no smoothing; 10: smoothing over a neighborhood of 10 pixels)",
                            ParameterName = "Smoothing factor for terrain data (number from 0 to 10"
                        },
                    new ParameterInfo("ShowMaxInundation", typeof (Boolean))
                        {
                            ParameterDescription = "Show maximum inundation zone",
                            ParameterName = "Show maximum inundation zone"
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Calculate a river flood"; }
        }

        public string Description
        {
            get { return "Calculate a river flood"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var tol = (Distance)parameterBag["HeightTolerance"];
            heightTolerance = Math.Abs((float)(tol.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal)));

            totalSupply = (Volume)parameterBag["TotalSupply"];
            totalSupplyGallons = totalSupply.GetValue(MeasurementSystem.Imperial, MeasurementScale.Normal);

            smoothingFactor = (Int32)parameterBag["SmoothingFactor"];

            showMaxInundation = (Boolean)parameterBag["ShowMaxInundation"];

            if (!subscriber.Position.IsValid)
                return new BehaviorResult();

            terrain = parameterBag["Terrain"] as Terrain;

            if (terrain == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    "Cannot compute run off behavior without elevation", 5);
                subscriber.SetPropertyValue("Active", false, true);
                return new BehaviorResult();
            }

            seedPosition = new LatitudeLongitude(subscriber.Position.Latitude, subscriber.Position.Longitude);
            InitializeVariables();

            DepictionAccess.NotificationService.DisplayMessageString(string.Format(
                                                                         "{0} calculations may take many minutes...", subscriber.TypeDisplayName),3);
            ComputeWatershed();
            bool active;
            if (subscriber.GetPropertyValue("Active", out active))
            {
                if (!active)
                {
                    subscriber.SetPropertyValue("Active", true, true);
                }
            }
            UpdateZoneOfInfluence(subscriber);
            //free up some memory
            flowLayer.Dispose();
            smoothTerrain.Dispose();

            int numPixels = gridWidthInPixels * gridHeightInPixels;
            //This was not quite being freed by the garbage collector
            //So, we force it here
            for (int i = 0; i < numPixels; i++)
            {
                demPixels[i] = null;
            }
            demPixels = null;
            GC.Collect();
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        private void UpdateZoneOfInfluence(IDepictionElement FlowElement)
        {
            if (FlowElement == null) throw new ArgumentNullException("FlowElement");

            var geomFactory = new GeometryFactory();
            var cList = new ArrayList();
            //
            //convertrastertopolygon method takes in a binary grid (0 and 1 values), find the
            //contours and
            //then returns a list of latlon coordinates of the boundary in the passed array list
            //
            flowLayer.ConvertRasterToPolygon(0/*(smallestFluidVolume / gridArea)*/, cList);

            //List<LatitudeLongitude>[] contourList = GeoProcessor.ConvertToPolygon(cList,seedLon,seedLat,gridSpacing);

            //
            //if you didn't really want to impose that the seed pixel be part of the contour, just
            //convert the xy contour list to latlong
            List<LatitudeLongitude>[] contourList = GetContourList(cList);

            if (contourList != null && contourList.Length > 0)
            {
//                FlowElement.ClearZoneOfInfluence();
                ILinearRing shell = null;
                var holes = new List<ILinearRing>();
                for (int i = 0; i < contourList.Length; i++)
                {
                    if (!GeoProcessor.IsCoordinateListClosed(contourList[i]))
                    {
                        var list = contourList[i];
                        contourList[i].Add(new LatitudeLongitude(list[contourList[i].Count - 1].Latitude, list[contourList[i].Count - 1].Longitude));
                    }
                    if (i == 0)
                    {
                        List<LatitudeLongitude> shellCoordinates = contourList[i];
                        var coordinates = new CoordinateList();


                        foreach (LatitudeLongitude latLong in shellCoordinates)
                        {
                            coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
                        }
                        shell = geomFactory.CreateLinearRing(coordinates.ToArray());
                    }
                    else
                    {
                        var coordinates = new CoordinateList();
                        foreach (LatitudeLongitude latLong in contourList[i])
                        {
                            coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
                        }
                        holes.Add(geomFactory.CreateLinearRing(coordinates.ToArray()));
                    }
                }
                FlowElement.SetZOIGeometry(new DepictionGeometry(geomFactory.CreatePolygon(shell, holes.ToArray())));
            }
            else
            {
                DepictionAccess.NotificationService.DisplayMessageString("No runoff zone was calculated. The Total Supply was perhaps too small OR the runoff start is in a pit. Try checking the \"Show maximum inundation\" box.");

                //Notify
//                DepictionAccess.NotificationService.SendNotificationDialog("No runoff zone was calculated. The Total Supply was perhaps too small OR the runoff start is in a pit. Try checking the \"Show maximum inundation\" box.", "No runoff zone calculated");
            }
        }

        private static List<LatitudeLongitude>[] GetContourList(IList cList)
        {
            var contourList = new List<List<LatitudeLongitude>>();//new List<LatitudeLongitude>[cList.Count];
            for (int i = 0; i < cList.Count; i++)
            {
                //contourList[i] = new List<LatitudeLongitude>();
                var clist1 = (ArrayList)cList[i];
                if (clist1.Count > 3 && GeoProcessor.IsPolygonSimple(clist1))
                {
                    var tempList = new List<LatitudeLongitude>();
                    for (int j = 0; j < clist1.Count; j++)
                    {
                        var pt = (PointF)clist1[j];
                        tempList.Add(new LatitudeLongitude(pt.Y, pt.X));
                    }
                    contourList.Add(tempList);
                }
            }
            return contourList.ToArray();
        }

        private void InitializeVariables()
        {
            gridWidthInPixels = terrain.GetGridWidthInPixels();
            gridHeightInPixels = terrain.GetGridHeightInPixels();
            //create and initialize the flow layer
            var topLeftPos = terrain.GetTopLeftPosition();
            var bottomRightPos = terrain.GetBottomRightPosition();
            flowLayer = new Coverage();
            flowLayer.Create(bottomRightPos.Latitude, topLeftPos.Latitude, bottomRightPos.Longitude,
                             topLeftPos.Longitude, gridWidthInPixels, gridHeightInPixels, true, 0, "", 1);

            //from the seedposition in latlon, get the row column values
            testSeedRow = terrain.GetRow(seedPosition.Latitude);
            testSeedColumn = terrain.GetColumn(seedPosition.Longitude);

            //grid resolution in meters
            //assuming square pixels

            totalSupplyGallons = ConvertGallonsToMetersCubed(totalSupplyGallons);
            //grid resolution in meters
            gridResolution = terrain.GetGridResolution();
            //assuming square pixels
            gridArea = gridResolution * gridResolution;
            var supplyInMCubed = totalSupplyGallons;
            double minHeight = 0.01; //100th of a meter
            triggerThreshold = supplyInMCubed/(gridArea*minHeight); 
        }


        /// <summary>
        /// Compute the watershed polygon starting from the seed location pixel
        /// </summary>
        private void ComputeWatershed()
        {

            var seedPoints = new ArrayList();
            GetSeedPoints(seedPoints);
            int numSeedPoints = seedPoints.Count;

            int numPixels = gridWidthInPixels * gridHeightInPixels;

            demPixels = new RowCol[numPixels];
            for (int i = 0; i < gridWidthInPixels; i++)
                for (int j = 0; j < gridHeightInPixels; j++)
                {
                    var index = j * gridWidthInPixels + i;
                    demPixels[index] = new RowCol(i, j) {InWatershed = false, Direction = (-1)};
                }

            //Smooth the terrain data
            smoothTerrain = Convolve(terrain,smoothingFactor);
            //smoothTerrain.SaveToGeoTIFF(@"c:\downloads\smooth.tif");
            for (int seedling = 0; seedling < numSeedPoints; seedling++)
            {
                int row = (int)((Point)seedPoints[seedling]).Y;
                int col = (int)((Point)seedPoints[seedling]).X;
                WaterShedFromSeed(col, row);

            }
            int pixelIndex;
            if(showMaxInundation)
            {
                triggerThreshold = Double.MaxValue;
            }
            for (int i = 0; i < gridWidthInPixels; i++)
                for (int j = 0; j < gridHeightInPixels; j++)
                {
                    pixelIndex = j*gridWidthInPixels + i;
                    if (demPixels[pixelIndex].InWatershed)
                    {
                        flowLayer.SetElevationValue(i, j, demPixels[pixelIndex].Accumulation <= triggerThreshold ? 1 : 0);
                    }
                }
            //flowLayer.SaveToGeoTIFF(@"c:\downloads\flow.tif");
        }

        private static Coverage Convolve(Terrain incomingTerrain, int kernelSize)
        {
            var elevationData = new Coverage();
            lock (incomingTerrain)
            {
                double south, north, east, west;
                var topLeft = incomingTerrain.GetTopLeftPosition();
                var botRight = incomingTerrain.GetBottomRightPosition();
                south = botRight.Latitude;
                north = topLeft.Latitude;
                east = botRight.Longitude;
                west = topLeft.Longitude;

                int height = incomingTerrain.GetGridHeightInPixels();
                int width = incomingTerrain.GetGridWidthInPixels();
                bool success = elevationData.Create(south, north, east, west, incomingTerrain.GetGridWidthInPixels(),
                                                    incomingTerrain.GetGridHeightInPixels(),
                                                    incomingTerrain.IsFloatMode(), 0f, "", 0);
                if (success)
                {
                    //
                    //fill in the elevationData grid values from incomingTerrain
                    //fill in the grid with values from incomingTerrain
                    for (int col = 0; col < width; col++)
                        for (int row = 0; row < height; row++)
                        {
                            float val = incomingTerrain.GetConvolvedValue(col, row, kernelSize);
                            elevationData.SetElevationValue(col, row, val);
                        }
                }
                else
                {
                    DepictionAccess.NotificationService.DisplayMessageString("Error...system possibly low on memory");
                }
            }
            return elevationData;

        }

        private void WaterShedFromSeed(int col, int row)
        {
            GetOutFlow(col, row);
        }

        void GetOutFlow(int startCol, int startRow)
        {
            float seedElevation = smoothTerrain.GetElevationFromGridCoordinates(startCol, startRow);
            if (seedElevation == NODATA) return;
            var outFlowList = new List<RowCol>();
            int seedPixelIndex = startRow*gridWidthInPixels + startCol;
            outFlowList.Add(demPixels[seedPixelIndex]);

            //seed pixel starts it all
            demPixels[seedPixelIndex].Accumulation = 1;
            RowCol PreviousCell = null;

            var preferredDirection = new List<int>();

            while (outFlowList.Count > 0)
            {
                var lowestElevation = (float) NODATA;
                var seed = outFlowList[outFlowList.Count - 1];
                outFlowList.RemoveAt(outFlowList.Count - 1);
                int seedCol = seed.Column;
                int seedRow = seed.Row;
                seedPixelIndex = seedRow*gridWidthInPixels + seedCol;
                //
                //seed pixel is always in the watershed
                //
                demPixels[seedPixelIndex].InWatershed = true;
                //Direction of outflow and elevationdifference with the neighbor are to be determined yet

                seedElevation = smoothTerrain.GetElevationFromGridCoordinates(seedCol, seedRow);
                int outFlowCol = -1, outFlowRow = -1;
                int direction = -1;
                var validNeighbors = FindLowestNeighbor(seedCol, seedRow, seedElevation, ref lowestElevation,
                                                        ref outFlowRow, ref direction, ref outFlowCol, preferredDirection);

                if (!validNeighbors /*no lower neighbor*/)
                {
                    //this is a pit
                    //flood the pit
                    demPixels[seedPixelIndex].Accumulation = (PreviousCell == null)
                                                                 ? 1
                                                                 : PreviousCell.Accumulation + 1;
                    RowCol outletCell = FloodThePit(seedCol, seedRow);
                    //if outlet cell is not the same as original pit cell, add it to the outFlow Stack and keep processing
                    if (!(outletCell.Column == seedCol && outletCell.Row == seedRow))
                    {
                        outFlowList.Add(demPixels[outletCell.Row*gridWidthInPixels + outletCell.Column]);
                        demPixels[outletCell.Row*gridWidthInPixels + outletCell.Column].Accumulation =
                            demPixels[seedPixelIndex].Accumulation;
                        preferredDirection.Clear();//start anew
                    }
                }
                else
                {
                    //NOT A PIT, this pixel has neighbors that are part of the runoff
                    try
                    {
                        //Outflow is to the neighbor with lowest elevation
                        //seed pixel is already in the watershed
                        //just set its outflow direction and elevation diff with the lowest neighbor
                        demPixels[seedPixelIndex].Direction = direction;
                        preferredDirection.Add(direction);
                        if (preferredDirection.Count > 5)
                            preferredDirection.RemoveAt(0);

                        demPixels[seedPixelIndex].Accumulation = (PreviousCell == null)
                                                                     ? 1
                                                                     : PreviousCell.Accumulation + 1;
                        //push the outflow neighbor into the stack so we can process it in the next round
                        var pixelIndex = outFlowRow*gridWidthInPixels + outFlowCol;
                        if (!demPixels[pixelIndex].InWatershed)
                        {
                            outFlowList.Add(demPixels[pixelIndex]);
                        }
                        if (PreviousCell != null && PreviousCell.Direction == -1)
                        {
                            //previous cell was a lake
                        }

                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Seed pixel index: " + seedPixelIndex);
                    }
                } //NOT A PIT
                PreviousCell = demPixels[seedPixelIndex];
            }
        }

        /// <summary>
        /// Starting at the seed pixel (startCol,startRow), this algorithm determines the lake that is flooded
        /// until an outlet is found
        /// Further processing is done to determine the lake with that has the lowest outlet (if more than one is present)
        /// 
        /// </summary>
        /// <param name="startCol"></param>
        /// <param name="startRow"></param>
        /// <returns></returns>
        private RowCol FloodThePit(int startCol, int startRow)
        {
            var currentPitElevation = smoothTerrain.GetElevationFromGridCoordinates(startCol, startRow);
            var startElevation = currentPitElevation;
            float elevationNextToOutlet=LARGEFLOAT;
            var pitCells = new List<RowCol>();
            var tempCellsInLake = new List<RowCol>();
            int seedIndex = gridWidthInPixels*startRow + startCol;
            float inletAccumulation = demPixels[seedIndex].Accumulation;
            pitCells.Add(demPixels[seedIndex]);
            tempCellsInLake.Add(demPixels[seedIndex]);
            RowCol lowCell = null;
            var outletCell = demPixels[seedIndex];
            bool terminate = false;
            bool outletFound = false;
            float outletLevel = LARGEFLOAT;
            float idempotentElevation = -LARGEFLOAT;
            while(!terminate)
            {
                float lowestElevation = LARGEFLOAT; //large number
                int cellCount = pitCells.Count;
                bool cellsAdded = false;
                bool cellsRemoved = false;
                bool newOutletFound = false;
                if (cellCount == 0) terminate = true;
                for (int cells = 0; cells < cellCount; cells++)
                {
                    int pixelIndex;
                    var seed = pitCells[cells];
                    var seedCol = seed.Column;
                    var seedRow = seed.Row;
                    var staleNeighbors = 0;
                    var neighbors = 0;
                    var seedElevation = smoothTerrain.GetElevationFromGridCoordinates(seedCol, seedRow);
                    float lowestNeighborElevation = LARGEFLOAT;
                    for (int i = -1; i < 2; i++)
                        for (int j = -1; j < 2; j++)
                        {
                            if (seedCol + i >= 0 && seedCol + i < gridWidthInPixels && seedRow + j >= 0 &&
                                seedRow + j < gridHeightInPixels)
                            {
                                if (i == 0 && j == 0) //seed pixel
                                    continue;
                                pixelIndex = gridWidthInPixels*(seedRow + j) + seedCol + i;
                                var elevation = smoothTerrain.GetElevationFromGridCoordinates(seedCol + i, seedRow + j);
                                if (elevation == NODATA) continue;

                                neighbors++;
                                if (demPixels[pixelIndex].InWatershed)
                                {
                                    staleNeighbors++;
                                    continue;
                                }
                                if (elevation < lowestNeighborElevation)
                                    lowestNeighborElevation = elevation;
                                if (!outletFound)
                                {
                                    if (elevation  < currentPitElevation)
                                    {
                                        //no outlet has been found
                                        //record this neighbor as an outlet
                                        //Finish the rest of the lake
                                        outletFound = true;
                                        outletCell = demPixels[pixelIndex];
                                        outletCell.Accumulation = inletAccumulation;
                                        outletLevel = elevation;
                                        elevationNextToOutlet = seedElevation;
                                    }
                                }
                                if (elevation <= lowestElevation)
                                {
                                    lowestElevation = elevation;
                                    lowCell = demPixels[pixelIndex];
                                }
                                if(!outletFound)
                                    if (demPixels[pixelIndex] != outletCell && elevation <= currentPitElevation)
                                    {
                                        //this neighbor is lower than the lake level, add it.
                                        pitCells.Add(demPixels[pixelIndex]);
                                        demPixels[pixelIndex].InWatershed = true;
                                        demPixels[pixelIndex].Accumulation = inletAccumulation;
                                        cellCount++;
                                        tempCellsInLake.Add(demPixels[pixelIndex]);
                                        cellsAdded = true;
                                    }
                                if (outletFound && elevation < outletLevel)
                                {   //at least one outlet has been found
                                    //here is a new outlet that is lower than the existing outlet
                                    outletCell = demPixels[pixelIndex];
                                    outletLevel = elevation;
                                    outletCell.Accumulation = inletAccumulation;
                                    elevationNextToOutlet = seedElevation;
                                    newOutletFound = true;
                                }

                                if (outletFound && !newOutletFound)
                                {//do we add this pixel or not?
                                    if (elevation <= currentPitElevation &&
                                        demPixels[pixelIndex] != outletCell &&
                                        elevation >= elevationNextToOutlet)
                                    {
                                        //this neighbor is lower than the lake level, add it.
                                        pitCells.Add(demPixels[pixelIndex]);
                                        demPixels[pixelIndex].InWatershed = true;
                                        demPixels[pixelIndex].Accumulation = inletAccumulation;
                                        cellCount++;
                                        tempCellsInLake.Add(demPixels[pixelIndex]);
                                        cellsAdded = true;
                                    }
                                }
                            }
                        }
                    if (staleNeighbors == neighbors || 
                        (idempotentElevation != -LARGEFLOAT && lowestNeighborElevation > idempotentElevation) 
                        || lowCell == outletCell)
                    {
                        //first condition is satisfied when a pixel has no neighbor that's outside the watershed
                        //second condition happens when the lowest neighbor of a pixel is still higher than the global
                        //lowest neighbor
                        //last condition happens when lowest elevation of a neighbor is the outlet cell
                        //in all three cases, there's no more need to look at this cell anymore while flooding the pit
                        pitCells.RemoveAt(cells);
                        cellCount--;
                        cellsRemoved = true;
                    }
                }

                if (!cellsAdded && !cellsRemoved)
                {
                    if (idempotentElevation == lowestElevation)
                    {
                        terminate = true;
                        continue;
                    }
                    idempotentElevation = lowestElevation;
                    //continue;
                }
                //among the neighbors of this pit, the lowest elevation is lowestElevation
                //raise the pit to this flood level
                if (lowestElevation != LARGEFLOAT )
                {
                    //no previous outlet to this pit has been found
                    if (!outletFound && lowestElevation > currentPitElevation)
                    {
                        if (lowestElevation <= startElevation)
                        {
                            //we're raising the level of the pit
                            //but we're requiring that the pit level not be higher than the
                            //the start elevation -- meaning, no backwash.
                            currentPitElevation = lowestElevation;
                            pitCells.Add(lowCell);
                            tempCellsInLake.Add(lowCell);
                            if (lowCell != null)
                            {
                                lowCell.InWatershed = true; //include it in the lake flood
                                lowCell.Accumulation = inletAccumulation;
                            }
                        }
                        else
                        {
                            terminate = true;
                        }
                    }
                    if (newOutletFound)
                    {//a previous outlet to this lake has already been found
                        //now, we have a new low outlet
                        if (currentPitElevation > elevationNextToOutlet /*&& 
                            elevationNextToOutlet > startElevation*/
                            )
                        {
                            outletLevel = lowestElevation;
                            currentPitElevation = (elevationNextToOutlet<startElevation)?startElevation:elevationNextToOutlet;
                            //adjust the lake to a new level of elevationNextToOutlet
                            pitCells = AdjustLake(tempCellsInLake, currentPitElevation,startElevation);
                            pitCells.CopyTo(tempCellsInLake.ToArray(), 0);
                        }
                        if (lowestElevation > currentPitElevation)
                        {
                            terminate = true; //TERMINATE!!!
                        }
                    }
                    else
                    {//there's already an outlet
                        //no new outlets found in this pass
                        if (lowestElevation > currentPitElevation)
                        {
                            terminate = true;
                        }
                    }
                }
                else
                {
                    //all neighbors were looked at
                    terminate = true;
                }
                
            }
            //tempCellsInLake -- all the cells in this pit
            //currentPitElevation -- water level in the pit
            var pitVol = ComputePitVolume(tempCellsInLake,currentPitElevation);
            if (outletCell != null)
            {
                outletCell.Accumulation = inletAccumulation + pitVol;
            }
            return outletCell;
        }


        private  float ComputePitVolume(IList<RowCol> lakeCells, float currentPitElevation)
        {
            float pitVol = 0;
            if (lakeCells == null) return 0;

            var inletAccumulation = lakeCells[0].Accumulation;

            for (int i = 0; i < lakeCells.Count;i++)
            {
                //lakeCells[i].PitNumber = pitNumber;
                var diff = smoothTerrain.GetElevationFromGridCoordinates(lakeCells[i].Column, lakeCells[i].Row) - currentPitElevation;
                pitVol += (diff)<=0? 1:diff;
            }
            for (int i = 0; i < lakeCells.Count; i++)
            {
                lakeCells[i].Accumulation = inletAccumulation + pitVol;
            }
            pitNumber++;
            return pitVol;
        }

        /// <summary>
        /// This method is used to drain a lake to a new height
        /// Typically, this is employed when a new outlet to a lake is found that is lower than a previous outlet
        /// </summary>
        /// <param name="lake"></param>
        /// <param name="elevation"></param>
        /// <param name="startElevation"></param>
        /// <returns></returns>
        private List<RowCol> AdjustLake(IList<RowCol> lake, float elevation, float startElevation)
        {
            var newLakeCells = new List<RowCol>();

            for (int i = 0; i < lake.Count;i++)
            {
                var ht = smoothTerrain.GetElevationFromGridCoordinates(lake[i].Column, lake[i].Row);
                var index = gridWidthInPixels*lake[i].Row + lake[i].Column;
                
                if (ht > elevation && ht > startElevation)
                {
                    demPixels[index].InWatershed = false;
                    demPixels[index].Accumulation = 0;
                }
                else newLakeCells.Add(demPixels[index]);
            }
            return newLakeCells;
        }

        /// <summary>
        /// Flatness test by fitting a plane to the 8-neighborhood
        /// If the angle is less than 0.1 degree, the neighborhood is deemed FLAT
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool IsTerrainFlat(int col, int row)
        {
            //USE just the 2x2 neighborhood around the seed pixel
            double z1, z2, z3, z4;
            var centerElevation = smoothTerrain.GetElevationFromGridCoordinates(col, row);

            if (col - 1 >= 0 && col - 1 < gridWidthInPixels && row - 1 >= 0 &&
                row - 1 < gridHeightInPixels)
            {
                z1 = smoothTerrain.GetElevationFromGridCoordinates(col - 1, row - 1);
            }
            else z1 = centerElevation;
            if (col - 1 >= 0 && col - 1 < gridWidthInPixels && row + 1 >= 0 &&
                row + 1 < gridHeightInPixels)
            {
                z2 = smoothTerrain.GetElevationFromGridCoordinates(col - 1, row + 1);
            }
            else z2 = centerElevation;
            if (col + 1 >= 0 && col + 1 < gridWidthInPixels && row - 1 >= 0 &&
                row - 1 < gridHeightInPixels)
            {
                z3 = smoothTerrain.GetElevationFromGridCoordinates(col + 1, row - 1);
            }
            else z3 = centerElevation;
            if (col + 1 >= 0 && col + 1 < gridWidthInPixels && row + 1 >= 0 &&
                row + 1 < gridHeightInPixels)
            {
                z4 = smoothTerrain.GetElevationFromGridCoordinates(col + 1, row + 1);
            }
            else z4 = centerElevation;

            double b = (-z1 + z2 - z3 + z4)/4;
            double c = (-z1 - z2 + z3 + z4)/4;

            double slope =  Math.Sqrt(b*b + c*c);
            double angleInDegrees = Math.Atan(slope)*180/Math.PI;
            if (angleInDegrees < 0.1) 
                return true;

            return false;
        }

        /// <summary>
        /// Given a seed pixel, look at its eight neighbors and return the lowest elevation neighbor
        /// 
        /// Do a flatness test and return false if the terrain neighborhood is flat
        /// </summary>
        /// <param name="seedCol"></param>
        /// <param name="seedRow"></param>
        /// <param name="seedElevation"></param>
        /// <param name="lowestElevation"></param>
        /// <param name="outFlowRow"></param>
        /// <param name="direction"></param>
        /// <param name="outFlowCol"></param>
        /// <param name="preferredDirection">contains the past 5 neighbors in the runoff</param>
        /// <returns></returns>

        private bool FindLowestNeighbor(int seedCol, int seedRow, float seedElevation, ref float lowestElevation, ref int outFlowRow, ref int direction, ref int outFlowCol, List<int> preferredDirection)
        {
            if(IsTerrainFlat(seedCol,seedRow))
            {
                outFlowRow = outFlowCol = -1;
                return false;
            }

            double desiredAngle = GetAverageDirection(preferredDirection);
            bool useDesiredDirection = true;
            if (desiredAngle == -1) useDesiredDirection = false;

            bool noNeighbors = true;

            if (!useDesiredDirection)
            {
                lowestElevation = GetLowestElevationNeighbor(seedCol, seedRow, seedElevation, lowestElevation, ref noNeighbors, out outFlowRow, out outFlowCol, out direction);
            }
            else
            {
                //find low neighbor that's closest to the desired angle
                lowestElevation = GetDirectionalNeighbor(desiredAngle, seedCol, seedRow, seedElevation, lowestElevation, ref noNeighbors, out outFlowRow, out outFlowCol, out direction);
            }
            return !noNeighbors;
        }
        /// <summary>
        /// Given a seed pixel, find the lowest (elevation wise) neighbor that's not already in the watershed
        /// </summary>
        /// <param name="seedCol"></param>
        /// <param name="seedRow"></param>
        /// <param name="seedElevation"></param>
        /// <param name="lowestElevation"></param>
        /// <param name="noNeighbors"></param>
        /// <param name="outFlowRow"></param>
        /// <param name="outFlowCol"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private float GetLowestElevationNeighbor(int seedCol, int seedRow, float seedElevation, float lowestElevation, ref bool noNeighbors, out int outFlowRow, out int outFlowCol, out int direction)
        {
            outFlowRow = outFlowCol = direction = -1;
            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                {
                    if (seedCol + i >= 0 && seedCol + i < gridWidthInPixels && seedRow + j >= 0 &&
                        seedRow + j < gridHeightInPixels)
                    {
                        if (i == 0 && j == 0) continue;
                        var elevation = smoothTerrain.GetElevationFromGridCoordinates(seedCol + i, seedRow + j);
                        if (elevation == NODATA) continue;
                        var outFlowIndex = (seedRow + j)*gridWidthInPixels + seedCol + i;
                        if ((!demPixels[outFlowIndex].InWatershed) &&
                            seedElevation + heightTolerance - elevation > lowestElevation)
                        {
                            lowestElevation = seedElevation + heightTolerance - elevation;
                            outFlowCol = seedCol + i;
                            outFlowRow = seedRow + j;
                            direction = GetFlowDirection(i, j);
                            noNeighbors = false;
                        }

                    }
                }
            return lowestElevation;
        }

        /// <summary>
        /// Given a seed pixel, find a low neighbor that's closest to the desired direction of water flow
        /// </summary>
        /// <param name="desiredAngle"></param>
        /// <param name="seedCol"></param>
        /// <param name="seedRow"></param>
        /// <param name="seedElevation"></param>
        /// <param name="lowestElevation"></param>
        /// <param name="noNeighbors"></param>
        /// <param name="outFlowRow"></param>
        /// <param name="outFlowCol"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private float GetDirectionalNeighbor(double desiredAngle, int seedCol, int seedRow, float seedElevation, float lowestElevation, ref bool noNeighbors, out int outFlowRow, out int outFlowCol, out int direction)
        {
            outFlowRow = outFlowCol = direction = -1;
            lowestElevation = 0;
            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                {
                    if (seedCol + i >= 0 && seedCol + i < gridWidthInPixels && seedRow + j >= 0 &&
                        seedRow + j < gridHeightInPixels)
                    {
                        if (i == 0 && j == 0) continue;
                        var elevation = smoothTerrain.GetElevationFromGridCoordinates(seedCol + i, seedRow + j);
                        if (elevation == NODATA) continue;
                        var outFlowIndex = (seedRow + j) * gridWidthInPixels + seedCol + i;
                        if ((!demPixels[outFlowIndex].InWatershed) &&
                            seedElevation + heightTolerance - elevation > lowestElevation)
                        {
                            double angleDiff = AngleDifference(GetFlowDirection(i,j), desiredAngle);
                            angleDiff *= 180 / Math.PI;
                            if (angleDiff <= 150 /*degrees*/)
                            {//PREVENT ABOUT TURNS IN RUNOFF
                                lowestElevation = seedElevation + heightTolerance - elevation;
                                outFlowCol = seedCol + i;
                                outFlowRow = seedRow + j;
                                direction = GetFlowDirection(i, j);
                                noNeighbors = false;
                            }
                        }

                    }
                }
            return lowestElevation;
        }

        private static double AngleDifference(int direction, double angle)
        {
            if (direction == -1) return 0;
            double neighborAngle = quantizedAngle[direction];
            double diff = Math.Min(Math.Abs(neighborAngle - angle), 2*Math.PI-neighborAngle + angle );
            diff = Math.Min(diff, 2 * Math.PI - angle + neighborAngle);
            return diff;
        }

        private static double GetAverageDirection(IList<int> direction)
        {
            if (direction == null || direction.Count == 0) return -1;
            double sumx=0, sumy=0;
            int count = 0;
            for(int i=0;i<direction.Count;i++)
            {
                if (direction[i] != -1)
                {
                    var angle = quantizedAngle[direction[i]];
                    sumx += Math.Cos(angle);
                    sumy += Math.Sin(angle);
                    count++;
                }
            }
            if (count == 0) return -1;
            var avgx = sumx/count;
            var avgy = sumy/count;
            if (Math.Abs(avgx) < 0.000001 && Math.Abs(avgy) < 0.000001) return 0;
            double averageAngle = Math.Atan2(avgy, avgx);
            if (averageAngle < 0) 
                averageAngle = 2*Math.PI + averageAngle;
            return averageAngle;

        }

        /// <summary>
        /// The neighborhood directions are as follows
        /// 
        /// 0 1 2
        /// 7 x 3
        /// 6 5 4
        /// 
        /// where x is the center pixel
        /// 
        /// For flat neighborhoods, the direction is -1
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        private static int GetFlowDirection(int i, int j)
        {
            switch (i)
            {
                case -1:
                    switch (j)
                    {
                        case -1:
                            return 0;
                        case 0:
                            return 7;
                        case 1:
                            return 6;
                    }
                    break;
                case 0:
                    switch (j)
                    {
                        case -1:
                            return 1;
                        case 1:
                            return 5;
                    }
                    break;
                case 1:
                    switch (j)
                    {
                        case -1:
                            return 2;
                        case 0:
                            return 3;
                        case 1:
                            return 4;
                    }
                    break;
            }
            return -1;
        }

        private class RowCol
        {
            public RowCol(int c, int r)
            {
                Column = c;
                Row = r;
            }

            public bool InWatershed { get; set; }
            public int Column { get; set; }

            public int Row { get; set; }

            public int Direction { get; set; } //The directions are as illustrated:
            // 0 1 2
            // 7 x 3
            // 6 5 4
            //
            //where x is the current pixel
            public float Accumulation { get; set; }
        }

        /// <summary>
        /// Get the seed points from a zone of influence
        /// </summary>
        /// <param name="seedPoints"></param>
        private void GetSeedPoints(IList seedPoints)
        {
            var col = testSeedColumn;
            var row = testSeedRow;
            seedPoints.Add(new Point(col, row));
        }

        private static double ConvertGallonsToMetersCubed(double gallons)
        {
            //1 meter cubed = 264.172052358 gallons
            return (gallons / MCubedToGallons);
        }
    }
}