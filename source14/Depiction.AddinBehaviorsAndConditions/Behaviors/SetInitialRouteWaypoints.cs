﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("SetRouteWaypoints", "Set initial route waypoints", "Set initial route waypoints")]
    public class SetInitialRouteWaypoints : BaseBehavior
    {
        public override ParameterInfo[] Parameters
        {
            get { return null; }
        }

        public string FriendlyName
        {
            get { return "Initiate route waypoints"; }
        }

        public string Description
        {
            get { return "Sets initial route waypoints."; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                var zoi = subscriber.ZoneOfInfluence;
                var zoiCoords = zoi.Geometry.Geometry.Coordinates;
                var coordCount = zoiCoords.Length;
                if (coordCount <= 1) return new BehaviorResult();

                for(int i =0;i<coordCount;i++)
                {
                    var latLong = new LatitudeLongitude(zoiCoords[i].Y, zoiCoords[i].X);
                   
                    if(i == 0)
                    {
                        var waypoint =   subscriber.GetWaypointWithName("start");
                        if(waypoint != null) waypoint.Location = latLong;
                    }else if(i==coordCount-1)
                    {
                        var waypoint = subscriber.GetWaypointWithName("end");
                        if (waypoint != null) waypoint.Location = latLong;
                    }else
                    {
                        var waypoint = new DepictionElementWaypoint
                                           {
                                               Location = latLong,
                                               IconSize = 16,
                                               Name = "WayPoint"
                                           };
                        subscriber.InsertWaypointBeforeLast(waypoint);
                    }
                }
            }
            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") || subscriber.ElementType.Equals("Depiction.Plugin.DriveTimeRoute") || subscriber.ElementType.Equals("Depiction.Plugin.RouteWaypoint"))
            {
                var zoi = subscriber.ZoneOfInfluence;
                var zoiCoords = zoi.Geometry.Geometry.Coordinates;
                var coordCount = zoiCoords.Length;
                if (coordCount <= 1) return new BehaviorResult();

                for (int i = 0; i < coordCount; i++)
                {
                    var latLong = new LatitudeLongitude(zoiCoords[i].Y, zoiCoords[i].X);

                    if (i == 0)
                    {
                        var waypoint = subscriber.GetWaypointWithName("start");
                        if (waypoint != null) waypoint.Location = latLong;
                    }
                    else if (i == coordCount - 1)
                    {
                        var waypoint = subscriber.GetWaypointWithName("end");
                        if (waypoint != null) waypoint.Location = latLong;
                    }
                    else
                    {
                        var waypoint = new DepictionElementWaypoint
                                           {
                                               Location = latLong,
                                               IconSize = 16,
                                               Name = "WayPoint"
                                           };
                        subscriber.InsertWaypointBeforeLast(waypoint);
                    }

                }
            }

            return new BehaviorResult();
        }
    }
}