﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.ValueTypes;
using GisSharpBlog.NetTopologySuite.Geometries;
using ParameterInfo = Depiction.API.ValueTypes.ParameterInfo;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("FindRoute", "Find a route", "Find a route")]
    public class FindRouteBehavior : BaseBehavior
    {
        const string directionPropertyName = "Directions";
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("RoadNetwork", typeof (IDepictionElementBase))
                        {
                            ParameterName = "Road Network", 
                            ParameterDescription = "The road network to use when calculating the route"
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Find a route"; }
        }

        public string Description
        {
            get { return "Find a route"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            //Part of epic hack
            foreach (var val in parameterBag.Values)
            {
                if (val == null)
                {
                    SetRouteToNodes(subscriber);
                    return new BehaviorResult();
                }
            }
            //Minor hack
            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                SetRouteToNodes(subscriber);
                return new BehaviorResult();
            }
            //end
            var roadNetwork = (IDepictionElement)parameterBag["RoadNetwork"];

            IRoadGraph roadGraph = roadNetwork != null ? roadNetwork.GetPropertyByInternalName("RoadGraph").Value as IRoadGraph : null;

            if (roadGraph == null)
                return new BehaviorResult();
            if (roadGraph.Graph.EdgeCount == 0)
            {
                return new BehaviorResult();
            }
            //SetRouteToNodes(subscriber);
            FindNewRoute(roadGraph, subscriber);
            //TempHack for updating visible route length
            // subscriber.UpdateToolTip();//HAck
            return new BehaviorResult();
        }
        private static void SetRouteToNodes(IDepictionElement route)
        {
            if (route.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") || route.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                var completeRouteCoords = new List<Coordinate>();
                var geomFact = new GeometryFactory();
                foreach (var node in route.Waypoints)
                {
                    var location = node.Location;
                    completeRouteCoords.Add(new Coordinate(location.Longitude, location.Latitude));
                }

                var geom = new DepictionGeometry(geomFact.CreateLineString(completeRouteCoords.ToArray()));
                lock (route.ZoneOfInfluence)
                {
                    route.SetZOIGeometryAndUpdatePosition(geom);
                }

                if (route.HasPropertyByInternalName(directionPropertyName))
                {
                    route.SetPropertyValue(directionPropertyName, "blank");
                }
                else
                {
                    var directionsProperty = new DepictionElementProperty("Directions", "blank");
                    directionsProperty.IsHoverText = false;
                    route.AddPropertyOrReplaceValueAndAttributes(directionsProperty);
//                    route.UseEnhancedPermaText = false;
                }
            }
        }
        private static void FindNewRoute(IRoadGraph roadNetwork, IDepictionElement route)
        {
            if (roadNetwork == null) return;
            
            var roadNodes = new List<RoadNode>();
            var elementWaypoints = route.Waypoints;
            if (elementWaypoints.Length <= 1) return;
            bool attachToRoadNetworkNodes;
            if(!route.GetPropertyValue("SnapToRoadNetworkNodes", out attachToRoadNetworkNodes))
            {
                attachToRoadNetworkNodes = false;
            }
            // bool attachToRoadNetworkNodes = false;

            for (int i = 0; i < elementWaypoints.Length; i++)
            {
                var nearestNode = roadNetwork.FindNearestNode(elementWaypoints[i].Location);
                if(attachToRoadNetworkNodes)
                {
                    elementWaypoints[i].UpdateLocationWithoutChangeNotification(nearestNode.Vertex);
                }
                roadNodes.Add(nearestNode);
            }

            lock (roadNetwork)
            {
                route.SetPropertyValue("Blocked", false);
                bool hasFreeformSegments;
                var completeRouteCoords = new List<Coordinate>();
                var completeRouteSegments = new List<IList<RoadSegment>>();
                var completeRouteTimes = new List<double>();
                //var completeDirections = "";
                //var completeRouteDistance = 0d;
                var completeRouteTime = 0d;
                for (int i = 0; i < elementWaypoints.Length - 1; i++)
                {
                    var startNode = roadNodes[i];
                    var endNode = roadNodes[i + 1];
                    if (!startNode.Equals(endNode))
                    {
                        double? estimatedTime;
                        var partialRoute = roadNetwork.RouteFinder.FindRoute(new[] {startNode, endNode}, route, 
                                                                             out hasFreeformSegments, out estimatedTime);
                        if(partialRoute != null) completeRouteSegments.Add(partialRoute);
                        if (estimatedTime != null)
                        {
                            completeRouteTime += (double) estimatedTime;
                            completeRouteTimes.Add((double)estimatedTime);
                        }
                        var coords = new List<Coordinate>();
                        //double partialDistance = 0;
                        if (partialRoute != null && partialRoute.Count >= 1)
                        {
                            
                            bool firstVertex = true;
                            foreach (var edge in partialRoute)
                            {
                                if (firstVertex)
                                {
                                    coords.Add(new Coordinate(edge.Source.Vertex.Longitude, edge.Source.Vertex.Latitude));
                                }
                                coords.Add(new Coordinate(edge.Target.Vertex.Longitude, edge.Target.Vertex.Latitude));
                                if (firstVertex)
                                    firstVertex = false;
                            }
                            if (!attachToRoadNetworkNodes)
                            {
                                var startPosition = elementWaypoints[i].Location;
                                var endPosition = elementWaypoints[i + 1].Location;

                                coords.Insert(0, new Coordinate(startPosition.Longitude, startPosition.Latitude));
                                coords.Add(new Coordinate(endPosition.Longitude, endPosition.Latitude));
                            }                            
                        }
                        else
                        {//Every so often routes will just die
                            var startPosition = elementWaypoints[i].Location;
                            var endPosition = elementWaypoints[i + 1].Location;

                            coords.Insert(0, new Coordinate(startPosition.Longitude, startPosition.Latitude));
                            coords.Add(new Coordinate(endPosition.Longitude, endPosition.Latitude));
                            //partialDistance += startPosition.DistanceTo(endPosition, measureSystem,measureScale);
                        }
                        completeRouteCoords.AddRange(coords);
                        
                        //if (partialRoute != null)
                        //{
                        //    completeDirections += directionsService.GetRawDirections(partialRoute, out partialDistance);
                        //}
                        //completeRouteDistance += partialDistance;
                    }
                }
                var geomFact = new GeometryFactory();
                var geom = new DepictionGeometry(geomFact.CreateLineString(completeRouteCoords.ToArray()));

                var directionsService = new RouteDirectionsService();
                string completeDirections = directionsService.GetRouteDirections(completeRouteSegments, completeRouteTimes);
           
                if (!string.IsNullOrEmpty(completeDirections))
                {
                    if (route.HasPropertyByInternalName(directionPropertyName))
                    {
                        route.SetPropertyValue(directionPropertyName, completeDirections);
                    }
                    else
                    {
                        var directionsProperty = new DepictionElementProperty(directionPropertyName, completeDirections);
                        directionsProperty.IsHoverText = true;
                        route.AddPropertyOrReplaceValueAndAttributes(directionsProperty);
                        route.UseEnhancedPermaText = true;
                    }
                }else
                {
                    var directionsProperty = new DepictionElementProperty("Directions", "blank");
                    directionsProperty.IsHoverText = false;
                    route.AddPropertyOrReplaceValueAndAttributes(directionsProperty);
                    route.UseEnhancedPermaText = false;
                }

                lock (route.ZoneOfInfluence)
                {
                    route.SetZOIGeometryAndUpdatePosition(geom);
                }

            }
        }
        #region direction getting
        
        #endregion
        
    }
}