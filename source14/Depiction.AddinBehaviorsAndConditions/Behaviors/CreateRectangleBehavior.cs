using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CreateRectangle", "Calculate a simple rectangle", "Calculates a simple  rectangle")]
    public class CreateRectangleBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters = 
            new[]
                {
                    new ParameterInfo("Width", typeof (Distance))
                        {ParameterName = "Width", ParameterDescription = "The new Width for the rectangle"},
                    new ParameterInfo("Length", typeof (Distance))
                        {ParameterName = "Length", ParameterDescription = "The length of the rectangle"},
                    new ParameterInfo("Rotation", typeof (double))
                        {ParameterName = "Rotation", ParameterDescription = "The rotation of the rectangle"}
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Create a rectagular zone of influence"; }
        }

        public string Description
        {
            get
            {
                return
                    "Set the subscribers zone of influence to a rectangle of the dimensions provided by the parameters.";
            }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            var width = (Distance) parameterBag["Width"];
            var length = (Distance) parameterBag["Length"];
            var rotation = (double)parameterBag["Rotation"];

            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;
            
            var points = ShapeCreatingService.CreateRectangle(width.GetValue(measureSystem,measureScale), length.GetValue(measureSystem,measureScale));

            List<Point> newPointList = TransformPoints(points, rotation);
            var pointDistances = MapCoordinateBounds.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);
            var shape = ShapeCreatingService.GeneratePolygonZOIFromPointList(subscriber.Position, newPointList,pointDistances);
            subscriber.SetZOIGeometry(new DepictionGeometry(shape));
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        public List<Point> TransformPoints(List<Point> pointList, double angle)
        {
            double theta = (angle - 90) * Math.PI / 180;
            var newPointList = new List<Point>();
            for (int i = 0; i < pointList.Count; i++)
            {
                double x = pointList[i].X*Math.Cos(theta) - pointList[i].Y*Math.Sin(theta);
                double y = pointList[i].X*Math.Sin(theta) + pointList[i].Y*Math.Cos(theta);
                newPointList.Add(new Point(x, y));
            }

            return newPointList;
        }
    }
}