﻿using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Depiction.Main
{
    public static class DebugUtils
    {
        public static void SetCertificatePolicy()
        {
            ServicePointManager.ServerCertificateValidationCallback += RemoteCertificateValidate;
        }

        private static bool RemoteCertificateValidate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyerrors)
        {
            return true;
        }
    }
}
