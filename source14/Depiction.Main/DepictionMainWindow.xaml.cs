﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.ExceptionHandling;
using Depiction.API.HelperObjects;
using Depiction.API.MessagingObjects;
using Depiction.API.MVVM;
using Depiction.CoreModel;
using Depiction.Main.Properties;
using Depiction.View;
using Depiction.View.Dialogs.DialogBases;
using Depiction.View.Dialogs.LicenseDialogs;
using Depiction.View.Dialogs.MainWindowDialogs;
using Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using form = System.Windows.Forms;
using APISettings = Depiction.API.Properties.Settings;
namespace Depiction.Main
{
    /// <summary>
    /// Interaction logic for DepictionMainWindow.xaml
    /// </summary>
    public partial class DepictionMainWindow : IDepictionMainWindow
    {
        public static readonly RoutedCommand BlowUpDepictionCommand = new RoutedUICommand("Blowup", "Blowup", typeof(DepictionMainWindow));

        public static readonly RoutedCommand ShowToolbarCommand = new RoutedUICommand("Show toolbar", "ShowToolbar", typeof(DepictionMainWindow));
        public static readonly RoutedCommand MapControlCommand = new RoutedCommand("KeyBoardMapControl", typeof(DepictionMainWindow));

        //ALL THESE DIALOG and dialog viewmodels are in hack form. They are virutally impossible to keep track of, so hopefully no more need to get added.
        private readonly DepictionMessageBoxController messageCenter = new DepictionMessageBoxController();
        private readonly DepictionApplication depictionMainApp;
        private readonly DepictionFileBrowser filebrowser = new DepictionFileBrowser();
        private readonly OptionsDialog optionsDialog;
        private readonly ElementDefinitionEditorDialogWindow elementDefinitionEditDialog;
        private readonly InteractionRepositoryDialog interactionRepositoryDialog;
        private readonly DepictionHelperMenuDialog applictionHelpDialog;
        private readonly BugSubmitter bugSubmitterDialog;
        private readonly AboutDialog aboutDialog;
        private readonly EulaDialog eulaDialog;
        private readonly UpdateDialog updateDialog;
        private readonly LicenseDialog licenseDialog;
        private readonly ToolsMenu toolsDialog;
        private readonly FileMenu fileDialog;

        private readonly AdvancedMenu advancedDialog;

        private readonly DepictionPrintDialog depictionPrintDialog;
        private readonly FileInformationDialog depictionFileInformationDialog;
        private readonly TipControlView depictionStartTipDialog;
        #region Properties


        public IMessageDialogController MessageDialogs
        {
            get { return messageCenter; }
        }
        #endregion

        #region COnstructor
        public DepictionMainWindow()
        {
            InitializeComponent();

            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.HighQuality);
            Loaded += DepictionMainWindow_Loaded;
            Closing += DepictionMainWindow_Closing;
            Closed += DepictionMainWindow_Closed;
            GotFocus += DepictionMainWindow_GotFocus;
            DataContextChanged += DepictionMainWindow_DataContextChanged;
            if (Application.Current != null)
            {
                depictionMainApp = Application.Current as DepictionApplication;
                if (depictionMainApp != null)
                {
                    depictionMainApp.saveLoadManager.DepictionSaveCompleted += saveLoadManager_DepictionSaveCompleted;
                    depictionMainApp.saveLoadManager.DepictionLoadCompleted += saveLoadManager_DepictionLoadCompleted;

                }
            }
            optionsDialog = new OptionsDialog();
            applictionHelpDialog = new DepictionHelperMenuDialog();
            applictionHelpDialog.InitialLocation = new Point(340, 60);
            bugSubmitterDialog = new BugSubmitter();
            aboutDialog = new AboutDialog();
            eulaDialog = new EulaDialog();
            updateDialog = new UpdateDialog();
            licenseDialog = new LicenseDialog();
            toolsDialog = new ToolsMenu();
            fileDialog = new FileMenu();

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                case ProductInformationBase.DepictionRW:
                    advancedDialog = new AdvancedMenu();
                    break;
            }

            depictionPrintDialog = new DepictionPrintDialog();
            elementDefinitionEditDialog = new ElementDefinitionEditorDialogWindow();
            elementDefinitionEditDialog.InitialLocation = new Point(350, 100);
            interactionRepositoryDialog = new InteractionRepositoryDialog();
            interactionRepositoryDialog.InitialLocation = new Point(350, 120);
            depictionFileInformationDialog = new FileInformationDialog();
            depictionStartTipDialog = new TipControlView();
            // Fogbugz command
            ConnectHelperBindings();
        }

        void DepictionMainWindow_Closed(object sender, EventArgs e)
        {
            if (depictionMainApp != null)
            {
                depictionMainApp.saveLoadManager.DepictionSaveCompleted -= saveLoadManager_DepictionSaveCompleted;
                depictionMainApp.saveLoadManager.DepictionLoadCompleted -= saveLoadManager_DepictionLoadCompleted;
            }
        }

        public void ConnectToSizeLocationChange()
        {
            SizeChanged += DepictionMainWindow_SizeChanged;
            StateChanged += DepictionMainWindow_StateChanged;
            LocationChanged += DepictionMainWindow_LocationChanged;
        }

        void DepictionMainWindow_LocationChanged(object sender, EventArgs e)
        {
            Settings.Default.MainWindowLocation = RestoreBounds;
        }

        void DepictionMainWindow_StateChanged(object sender, EventArgs e)
        {
            Settings.Default.MainWindowState = WindowState;
        }

        void DepictionMainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Settings.Default.MainWindowLocation = RestoreBounds;
        }
        #endregion

        #region for testing exception handling

        private void ConnectHelperBindings()
        {
            CommandBindingCollection commandBindings = CommandBindings;
            InputBindingCollection inputBindings = InputBindings;
            var inputBinding = new KeyBinding(BlowUpDepictionCommand, new KeyGesture(Key.Pause));
            inputBinding.CommandParameter = true;
            inputBindings.Add(inputBinding);
            var cmdBinding = new CommandBinding(BlowUpDepictionCommand, BlowUpDepictionExecute);
            commandBindings.Add(cmdBinding);
        }

        private static void BlowUpDepictionExecute(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                throw new NullReferenceException();
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex);
            }
        }

        #endregion

        #region Datacontext change events
        void DepictionMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            optionsDialog.Owner = this;
            applictionHelpDialog.Owner = this;
            updateDialog.Owner = this;
            bugSubmitterDialog.Owner = this;
            aboutDialog.Owner = this;
            eulaDialog.Owner = this;
            licenseDialog.Owner = this;
            depictionPrintDialog.Owner = this;
            elementDefinitionEditDialog.Owner = this;
            interactionRepositoryDialog.Owner = this;
            depictionFileInformationDialog.Owner = this;
            toolsDialog.Owner = this;
            fileDialog.Owner = this;

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                case ProductInformationBase.DepictionRW:
                    advancedDialog.Owner = this;
                    break;
            }
            depictionStartTipDialog.Owner = this;
        }

        void DepictionMainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldVal = e.OldValue as DepictionAppViewModel;
            var newVal = e.NewValue as DepictionAppViewModel;
            if (oldVal != null)
            {//This one should never really happen because the main datacontext is only set once
                DepictionAppViewModel.GetFileNameToLoadAndImporter -= appViewModel_AskForLoadFileName;
                DepictionAppViewModel.GetFileNameToSaveAndExporter -= appViewModel_AskForSaveFileName;
            }
            if (newVal != null)
            {
                DepictionAppViewModel.GetFileNameToLoadAndImporter += appViewModel_AskForLoadFileName;
                DepictionAppViewModel.GetFileNameToSaveAndExporter += appViewModel_AskForSaveFileName;

                optionsDialog.DataContext = newVal.DepictionPropertySettingsDialogVM;
                applictionHelpDialog.DataContext = newVal.AppGeneralHelpDialogVM;
                applictionHelpDialog.Tag = newVal;

                toolsDialog.DataContext = newVal.ToolsDialogVM;
                toolsDialog.Tag = newVal;
                fileDialog.DataContext = newVal.FileDialogVM;
                fileDialog.Tag = newVal;
                updateDialog.DataContext = newVal.UpdateDialogVM;
                bugSubmitterDialog.DataContext = newVal.BugSubmitterDialogVM;
                aboutDialog.DataContext = newVal.AboutDialogVM;
                eulaDialog.DataContext = newVal.EulaDialogVM;

                licenseDialog.DataContext = newVal.LicenseDialogVM;
                depictionPrintDialog.DataContext = newVal.PrintDialogVM;
                elementDefinitionEditDialog.DataContext = newVal.ElementDefinitionEditorVm;
                interactionRepositoryDialog.DataContext = newVal.InteractionRepositoryVM;
                depictionFileInformationDialog.DataContext = newVal.FileInformationVM;
                depictionStartTipDialog.DataContext = newVal.DepictionStartTipDialogVM;

                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                    case ProductInformationBase.DepictionRW:
                        advancedDialog.DataContext = newVal.AdvancedDialogVM;
                        advancedDialog.Tag = newVal;
                        break;
                }
                if (depictionMainApp != null)
                {
                    DepictionNotifications.DataContext = DepictionAccess.NotificationService;
                }
            }
        }

        #endregion

        #region New command

        private void NewCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    return;
            }
//#if IS_READER
//            return;
//#endif
            if (DataContext == null) return;
            var mainApp = DataContext as DepictionAppViewModel;
            if (mainApp == null) return;
            mainApp.ChangeAppScreenCommand.Execute(CurrentAppDepictionScreen.Location);
        }

        #endregion

        #region Open region

        private void OpenCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            OpenDepictionFileWithFullView(null, false);
        }

        private bool changeSampleName;
        //Kind of duplicates opendicitionFileWithFullView, but this one comes from double
        //clicking a dpn file
        public void OpenDepictionFromDoubleClickAndLoadPrototypes(string fileName)
        {
            if (depictionMainApp == null) return;
            if (DataContext == null) return;

            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;
            var depictApp = Application.Current as DepictionApplication;
            if (depictApp == null) return;
            if (string.IsNullOrEmpty(fileName)) return;
            //Ensure the prototype library loads before the depiction
            var message = new DepictionMessage("Loading element definitions");
            var definitionLibraryLoader = new BackgroundWorker();
            definitionLibraryLoader.DoWork += delegate(object sender, DoWorkEventArgs e)
            {
                DepictionAccess.NotificationService.DisplayMessage(message);
                
                depictApp.LoadElementDefinitions(DepictionAccess.ProductInformation.ResourceAssemblyName);
                DepictionAccess.NotificationService.RemoveMessage(message);

            };
            definitionLibraryLoader.RunWorkerCompleted += delegate(object sender, RunWorkerCompletedEventArgs args)
                                                              {
                                                                  //Ok now load the requested depiction
                                                                  var saveDialog = new SaveLoadDialog();
                                                                  saveDialog.Title = string.Format("Opening {0}", Path.GetFileNameWithoutExtension(fileName));
                                                                  saveDialog.DataContext = appViewModel.SaveLoadDialogVM;
                                                                  //Order of the next two is important TODO fix the race condition, which 2? the change sample name or the dialog?
                                                                  depictionMainApp.LoadADepictionFromFileName(fileName);
                                                                  appViewModel.SaveLoadDialogVM.IsDialogVisible = true;
                                                              };
            definitionLibraryLoader.RunWorkerAsync();
        }

        public void OpenDepictionFileWithFullView(string fileName, bool isSample)
        {

            if (depictionMainApp == null) return;
            if (DataContext == null) return;

            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;

            if (string.IsNullOrEmpty(fileName))
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        break;
                    default:
                        if (!CheckForAndDoASaveBeforeChange()) return;
                        break;
                }
//#if (!IS_READER)
//                //Ask to save current depiction assuming there is one
//                if (!CheckForAndDoASaveBeforeChange()) return;
//#endif
                fileName = filebrowser.GetDepictionToLoad();
            }
            if (string.IsNullOrEmpty(fileName)) return;
            var saveDialog = new SaveLoadDialog();
            saveDialog.Title = string.Format("Opening {0}", Path.GetFileNameWithoutExtension(fileName));
            saveDialog.DataContext = appViewModel.SaveLoadDialogVM;
            //Order of the next two is important TODO fix the race condition, which 2? the change sample name or the dialog?
            depictionMainApp.LoadADepictionFromFileName(fileName);
            changeSampleName = isSample;
            appViewModel.SaveLoadDialogVM.IsDialogVisible = true;
        }

        void saveLoadManager_DepictionLoadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (DataContext == null) return;
            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;
            if (changeSampleName && DepictionAccess.CurrentDepiction != null)
            {
                var saveDirectory = APISettings.Default.LastDepictionSaveDirectory;
                if (string.IsNullOrEmpty(saveDirectory))
                {
                    saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                var name = Path.GetFileName(DepictionAccess.CurrentDepiction.DepictionsFileName);
                DepictionAccess.CurrentDepiction.DepictionsFileName = Path.Combine(saveDirectory, name);
            }
            changeSampleName = false;
            appViewModel.SaveLoadDialogVM.IsDialogVisible = false;
        }
        #endregion

        #region Save and Save as region

        private void SaveCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    return;
            }
//#if IS_READER
//            return;
//#endif
            var currentDep = depictionMainApp.CurrentDepiction;
            var currentFileName = currentDep.DepictionsFileName;
            if (!SaveCurrentDepictionAs(currentFileName))
            {
                depictionMainApp.ContinueApplicationRequest = false;
            }
        }

        void saveLoadManager_DepictionSaveCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (DataContext == null) return;
            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;
            appViewModel.SaveLoadDialogVM.IsDialogVisible = false;
            appViewModel.UpdateDepictionTitle();
        }
        private void CanExecuteSaveHandler(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            if (DataContext == null) return;
            var mainApp = DataContext as DepictionAppViewModel;
            if (mainApp == null) return;
            if (mainApp.MapViewModel != null) e.CanExecute = true;
        }

        private void SaveAsCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    return;
            }
//#if IS_READER
//            return;
//#endif
            if (e.Parameter is bool)//Used when changing from map to exit,new,or welcome screen
            {
                var askForSave = (bool)e.Parameter;
                if (askForSave)
                {
                    if (!CheckForAndDoASaveBeforeChange())
                    {
                        depictionMainApp.ContinueApplicationRequest = false;
                        return;
                    }
                }
            }
            else
            {
                if (!SaveCurrentDepictionAs(null))
                {
                    depictionMainApp.ContinueApplicationRequest = false;
                }
            }
        }

        public bool SaveCurrentDepictionAs(string fileName)
        {
            if (depictionMainApp == null) return false;
            if (DataContext == null) return false;
            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return false;

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = filebrowser.GetDepictionToSave();
            }
            if (string.IsNullOrEmpty(fileName)) return false;
            var saveDialog = new SaveLoadDialog();
            saveDialog.Title = string.Format("Saving {0}", Path.GetFileNameWithoutExtension(fileName));
            saveDialog.DataContext = appViewModel.SaveLoadDialogVM;
            //Order of the next two is important TODO fix the race condition
            depictionMainApp.SaveCurrentDepictionWithFileName(fileName);
            appViewModel.SaveLoadDialogVM.IsDialogVisible = true;
            //            saveDialog.ShowDialog();//ggrrr can't use this yet

            return true;
        }
        private void IsADepictionBeingViewed(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            if (DataContext == null) return;

            var mainApp = DataContext as DepictionAppViewModel;
            if (mainApp == null) return;

            if (mainApp.CurrentScreen.Equals(CurrentAppDepictionScreen.World)) e.CanExecute = true;
        }
        #endregion

        #region Print region

        private void PrintCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    return;
            }
//#if(IS_READER)
//    return;
//#endif
            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;
            appViewModel.PrintDialogVM.IsDialogVisible = true;
        }

        #endregion

        #region Help Region
        private void HelpCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            var helpFile = "welcome.html";
            if (e.Parameter != null)
            {
                helpFile = e.Parameter.ToString();
            }
            string targetApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Docs", string.Format("{0}.chm", DepictionAccess.ProductInformation.ProductNameInternalUsage));
#if DEBUG
            var message = "Help is not available for debug builds.";
            if (!File.Exists(targetApp))
            {
                targetApp = Path.Combine(@"D:\depiction\geo\source\Docs\Public\",
                                         string.Format("{0}.chm", DepictionAppViewModel.ProductInformationType.ProcessName));
                //                targetApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                //                                             string.Format("{0}.chm", DepictionAppViewModel.ProductInformationType.ProcessName));
            }
#else
            var message = string.Format("Help is not available.  Cannot find help file at {0}", targetApp);
#endif
            if (!File.Exists(targetApp))
            {
                //                DepictionAccess.NotificationService.SendNotification(message);
            }
            else
            {
                form.Help.ShowHelp(null, targetApp, form.HelpNavigator.Topic, helpFile);
            }
        }
        #endregion

        #region Window closing event handlers

        void DepictionMainWindow_Closing(object sender, CancelEventArgs e)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    break;
                default:
                    if (!CheckForAndDoASaveBeforeChange())
                    {
                        e.Cancel = true;
                        return;
                    }
                    break;
            }
//#if(!IS_READER)
//            if (!CheckForAndDoASaveBeforeChange())
//            {
//                e.Cancel = true;
//                return;
//            }
//#endif
            //Random attempt to close things
            //            var app = Application.Current as DepictionApplication;
            //            if(app != null)
            //            {
            //                app.SetNewDepictionStory(null);
            //            }
        }

        #endregion

        #region Showing a file browser
        public KeyValuePair<string, IDepictionImporterBase> appViewModel_AskForLoadFileName(string startDir, FileFilter importFilefilter)
        {
            return filebrowser.GetElementFileToLoad(startDir, importFilefilter);
        }

        private KeyValuePair<string, IDepictionElementExporter> appViewModel_AskForSaveFileName(string startDir, FileFilter exportFileFilters)
        {
            return filebrowser.GetElementFileToSaveWithExporter(startDir, exportFileFilters);
        }
        #endregion

        #region for quick add toolbar
        private void ShowHideQuickAddToolbar(object sender, ExecutedRoutedEventArgs e)
        {
            APISettings.Default.QuickAddVisible = !APISettings.Default.QuickAddVisible;
            APISettings.Default.Save();
        }
        #endregion

        #region Helper methods

        //This can be changed thanks to the new messageing serviced
        //THese might not be needed thanks to the fake IView connection 
        public MessageBoxResult CheckForChangeAskIfSaveShouldHappen()
        {
            if (depictionMainApp == null) return MessageBoxResult.No;
            if (depictionMainApp.CurrentDepiction == null) return MessageBoxResult.No;
            if (!depictionMainApp.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds.IsValid) return MessageBoxResult.No;
            if (!depictionMainApp.CurrentDepiction.DepictionNeedsSaving) return MessageBoxResult.No;
            return DepictionAccess.NotificationService.DisplayInquiryDialog(this, "Would you like to save the current depiction?",
                                                                  "Save current depiction", MessageBoxButton.YesNoCancel);
            //return DepictionMessageBox.ShowDialog(this, "Would you like to save the current depiction?", "Save current depiction", MessageBoxButton.YesNoCancel);
        }

        public bool CheckForAndDoASaveBeforeChange()
        {
            var result = CheckForChangeAskIfSaveShouldHappen();
            if (result.Equals(MessageBoxResult.Yes))
            {
                var currentDep = depictionMainApp.CurrentDepiction;
                var currentFileName = currentDep.DepictionsFileName;
                if (!SaveCurrentDepictionAs(currentFileName))
                {
                    return false;
                }
            }
            if (result.Equals(MessageBoxResult.Cancel))
            {
                return false;
            }
            return true;
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            var dc = DataContext as DepictionAppViewModel;

            if (dc != null && dc.MapViewModel != null)//&& !(e.Source is IDepictionController)
            {
                dc.MapViewModel.CurrentMainWindowPoint = e.GetPosition(mapScreen);
            }
            base.OnPreviewMouseMove(e);
        }

        void DepictionMainWindow_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = FocusManager.GetFocusedElement(this);
            string msg;
            if (focused == null)
                msg = "*** (no focused element)";
            else
                msg = string.Format("*** Focused element: {0} ({1})", (focused is FrameworkElement) ? (focused as FrameworkElement).Name : "n/a", focused.GetType());
            Debug.WriteLine(msg);
        }

        #endregion

        private void MapControlCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            var appViewModel = DataContext as DepictionAppViewModel;
            if (appViewModel == null) return;
            if (appViewModel.MapViewModel == null) return;
            if (e.Parameter is PanDirection)
            {
                appViewModel.MapViewModel.PanCommand.Execute(e.Parameter);
            }
            else if (e.Parameter is ZoomDirection)
            {
                appViewModel.MapViewModel.ZoomCommand.Execute(e.Parameter);
            }
        }
    }
}