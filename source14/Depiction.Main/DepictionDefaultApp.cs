﻿
//#define PREP
//#define READER
//#define INTIFIC

using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;
using Depiction.API.MEFRepository;
using Depiction.APIUnmanaged;
using Depiction.CoreModel;
using Depiction.CoreModel.HelperClasses;
using Depiction.Main.Properties;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using APISettings = Depiction.API.Properties.Settings;
using MainSettings = Depiction.Main.Properties.Settings;
using ViewSettings = Depiction.View.Dialogs.Properties.Settings;

#if !READER//USE_LICENSE? maybe create it
//how annoying, the dll still has to be present sicne main uses it as a reference. Something will have to be done so we can removed dlls dependencies that are not used
//for certain products
using Depiction.LicenseManager;
#endif
namespace Depiction.Main
{
    public class DepictionDefaultApp : DepictionApplication
    {
        private DepictionAppViewModel appVM;
        private SplashScreen splash;
        private FogBugzPoller bugPoller;
#if !READER
        private LicensingManager licenseManager;
#endif

        private Window appWindow;
        public DepictionDefaultApp()
        {
            var defaultResourceDictionary = new ResourceDictionary
                                 {
                                     Source =
                                         new Uri(string.Format("pack://application:,,,/{0};component/DepictionDefaultResources.xaml", "Depiction.Main"))
                                 };
            Resources.MergedDictionaries.Add(defaultResourceDictionary);
// ReSharper disable ConvertToConstant.Local
// ReSharper disable RedundantAssignment
            var resourceAssemblyName = "Resources.Product.Depiction";
            var productClassName = "DepictionDebugInformation";
// ReSharper restore RedundantAssignment
// ReSharper restore ConvertToConstant.Local
#if DEBUG
            DebugUtils.SetCertificatePolicy();//Not sure why this is needed on only my (davidl) comp so far, basically it tells webservice to trust everything
#endif
#if PREP
            resourceAssemblyName = "Resources.Product.Prep";
    productClassName = "DepictionPrepInformation";
//            var productClass = Activator.CreateInstance(Type.GetType(string.Format("{0}.{1},{0}", resourceAssemblyName, productClassName), true)) as ProductInformationBase;
//            //            var resourceAssembly = Assembly.LoadFrom("Resources.Product.Depiction.dll");
//            //            var productType = resourceAssembly.GetType("Resources.Product.Depiction.DepictionDebugInformation");
//            //            var productClass = Activator.CreateInstance(productType) as ProductInformationBase;
//            DepictionAccess.ProductInformation = productClass;// new DepictionDebugInformation();
#elif READER
              resourceAssemblyName = "Resources.Product.Reader";
            productClassName = "DepictionReaderInformation";
#elif INTIFIC
            resourceAssemblyName = "Resources.Product.DepictionRW";
            productClassName = "DepictionRWInformation";
#elif DEBUG
#else

            productClassName = "Depiction14ProductInformation";
#endif
            try
            {
                var productClass = Activator.CreateInstance(Type.GetType(string.Format("{0}.{1},{0}", resourceAssemblyName, productClassName), true)) as ProductInformationBase;
                DepictionAccess.ProductInformation = productClass;

            }
            catch (Exception)
            {
                Shutdown();
                return;
                //                Console.WriteLine("Depiction stop here and give an error message saying that there are no resource dll.");
                //                resourceAssemblyName = "Resources.Product.Depiction";
                //                productClassName = "DepictionDebugInformation";
                //                var productClass = Activator.CreateInstance(Type.GetType(string.Format("{0}.{1},{0}", resourceAssemblyName, productClassName), true)) as ProductInformationBase;
                //                DepictionAccess.ProductInformation = productClass;
            }
            //Don't forget that this is an extension of DepictionApplication, so all the reall work is done in that class
            //  Startup += App_Startup;

            ShutdownMode = ShutdownMode.OnMainWindowClose;
            Startup += DepictionApp_Startup;

            Exit += App_Exit;
            //Exception handling part
            DispatcherUnhandledException += App_DispatcherUnhandledException;
        }

        void DepictionApp_Startup(object sender, StartupEventArgs e)
        {
            //initialize proxy settings
            if (WebRequest.DefaultWebProxy != null)
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(APISettings.Default.ProxyUserName, APISettings.Default.ProxyPassword);

            var productResourceAssembly = DepictionAccess.ProductInformation.ResourceAssemblyName;
            try
            {
                var assembly = Assembly.Load(new AssemblyName(productResourceAssembly));
                splash = new SplashScreen(assembly, DepictionAccess.ProductInformation.SplashScreenPath);//Has to be a regular resource
                if (splash != null) splash.Show(false);
            }
            catch (Exception)
            {
                //                DepictionExceptionHandler.HandleException(ex,false,true);//not in for now because DepictionPath is not set yet.
                splash = null;
            }
            var imageDictionary = new ResourceDictionary
                                 {
                                     Source =
                                         new Uri(string.Format("pack://application:,,,/{0};component/ImageResources.xaml", productResourceAssembly))
                                 };
            var iconDictionary = new ResourceDictionary
            {
                Source =
                    new Uri(string.Format("pack://application:,,,/{0};component/ElementIconDictionary.xaml", productResourceAssembly))
            };
//            ImageSource icon = null;
//            var iconResourceLocation = DepictionAccess.ProductInformation.AppIconResourceName;
//            if (!string.IsNullOrEmpty(iconResourceLocation))
//            {
//                var iconURI =
//                    new Uri(string.Format("pack://application:,,,/{0};component/{1}", productResourceAssembly, iconResourceLocation));
//                icon = BitmapFrame.Create(iconURI);
//            }
            appWindow = new DepictionMainWindow();// { Icon = icon };
            Current.Resources.MergedDictionaries.Add(imageDictionary);
            Current.Resources.MergedDictionaries.Add(iconDictionary);

            MainWindow = appWindow;
            MainWindow.WindowState = WindowState.Minimized;
            appWindow.Show();
            try
            {
                if (!CheckLicense())
                {
                    Current.Shutdown();
                    return;
                }
            }catch(Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex,true,true);
            }

            var startupArgs = e.Args;
            var startupFile = string.Empty;
            if (startupArgs.Length > 0)
            {
                startupFile = startupArgs[0];
                if (!File.Exists(startupFile))
                {
                    startupFile = string.Empty;
                }
            }
            #region startup stuff

            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var typeFileName = "DepictionDataTypes.xml";
            var fullPath = Path.Combine(baseDir, typeFileName);
            DepictionCoreTypes.DoFullTypeUpdateFromFileAndDefaults(fullPath);




            GdalEnvironment.SetupEnvironment();

            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledException;
            #endregion
            StartDepictionAppServices();

            ComposeDepictionAddonsAndSetupTileServices();
            

            ILicenseService licenseService = null;
#if !READER
            if (licenseManager != null)
            {
                licenseService = licenseManager.LicenseService;
            }
#endif

            bugPoller = new FogBugzPoller();

            appVM = new DepictionAppViewModel(bugPoller, licenseService);
            appWindow.DataContext = appVM;
            
            appVM.TileDisplayerVM.AddToAvailableTilers(AddinRepository.Instance.Tilers);
            //If the is a normal load (no dpn load request) load the definitions in the background
            if (string.IsNullOrEmpty(startupFile))
            {
                SetPrototypeLibraryFromBackground();
            }

            UpdateMainWindowSize(appWindow);

            MainWindow.Focus();
            if (splash != null) splash.Close(TimeSpan.FromSeconds(0));
            ((DepictionMainWindow)appWindow).ConnectToSizeLocationChange();

            //Hacks to deal with windows initial size/position issues
            if (MainWindow.WindowState == WindowState.Minimized)
            {
                MainWindow.WindowState = WindowState.Normal;
            }
            MainWindow.Activate();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    appVM.DepictionStartTipDialogVM.TipControlType = TipControlTypeEnum.StartTipControl;
                    if (!appVM.DepictionStartTipDialogVM.HideControlOnStartup)
                    {
                        appVM.DepictionStartTipDialogVM.CreateFlowDocumentFromEmbeddedResourceFile("DepictionPrepInitialTipText.rtf");
                        appVM.DepictionStartTipDialogVM.IsDialogVisible = true;
                    }
                    break;
            }

            if (!string.IsNullOrEmpty(startupFile))
            {
                ((DepictionMainWindow)appWindow).OpenDepictionFromDoubleClickAndLoadPrototypes(startupFile);
            }
        }

        private bool CheckLicense()
        {
//            licenseManager = new LicensingManager();
//            if (licenseManager.CheckLicense()) return true;
//            Current.Shutdown();
//            return false;
#if READER
            return true;
#elif DEBUG
            return true;
#else
            licenseManager = new LicensingManager();
            if (licenseManager.CheckLicense()) return true;
            Current.Shutdown();
            return false;
#endif

        }

        #region Start up and shutdown things

        void UpdateMainWindowSize(Window mainWindow)
        {
            var window = mainWindow;
            if (window == null) return;
            if (!Settings.Default.MainWindowLocation.IsEmpty)
            {
                if (Settings.Default.MainWindowState.Equals(WindowState.Normal))
                {
                    var bounds = Settings.Default.MainWindowLocation;
                    window.Top = bounds.Top;
                    window.Left = bounds.Left;
                    window.Width = bounds.Width;
                    window.Height = bounds.Height;
                }
                else
                {
                    window.WindowState = WindowState.Maximized;
                }
            }
            else
            {
                window.WindowState = WindowState.Maximized;
            }
        }

        void App_Exit(object sender, ExitEventArgs e)
        {
#if !READER
            licenseManager = null;
#endif
            //GC.Collect();
            //None of these seem to work.
            if (DepictionAccess.InteractionsRunner != null)
            {
                DepictionAccess.InteractionsRunner.CancelAsync();
                DepictionAccess.InteractionsRunner.EndAsync();
            }

            MainSettings.Default.Save();
            ViewSettings.Default.Save();
            APISettings.Default.Save();
            Debug.WriteLine("Saving dialog location information");

            Console.WriteLine("The app is leaving, have a nice day.");
            DispatcherUnhandledException -= App_DispatcherUnhandledException;
            if (bugPoller != null) bugPoller.Terminate();
        }

        #endregion

        #region Exception handling, rather exception pass through

        private void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            DepictionExceptionHandler.HandleException(e.ExceptionObject as Exception);
        }
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            DepictionExceptionHandler.HandleException(e.Exception);
            e.Handled = true;
        }

        #endregion
    }
}
