using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Depiction.Serialization
{
    [Serializable]
    [XmlRoot(ElementName = "dictionary", Namespace = SerializationConstants.DepictionXmlNameSpace)]
    public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable, IDeepCloneable<SerializableDictionary<TKey, TValue>>
    {
        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }


        public SerializableDictionary()
        {

        }

        public SerializableDictionary(IDictionary<TKey, TValue> dictionary)
            : base(dictionary)
        {

        }

        public SerializableDictionary(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }


        public void ReadXml(System.Xml.XmlReader reader)
        {
            //var keySerializer = new XmlSerializer(typeof(TKey));
            //var valueSerializer = new XmlSerializer(typeof(TValue));

            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement("item");
                TKey key = SerializationService.DeserializeObject<TKey>("key", reader);
                TValue value = SerializationService.DeserializeObject<TValue>("value", reader);
                reader.ReadEndElement();

                Add(key, value);
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("SimpleSerializableDictionary");
            foreach (TKey key in Keys)
            {
                writer.WriteStartElement("item");
                SerializationService.SerializeObject("key", key, writer);
                TValue value = this[key];
                SerializationService.SerializeObject("value", value, writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
        #endregion

        #region Implementation of IDeepCloneable

        public SerializableDictionary<TKey, TValue> DeepClone()
        {//Hopefully this is a deep copy, although i don't think it is sure thing, especially not on the 
            //value
            var cloneDict = new SerializableDictionary<TKey, TValue>();
            foreach (var key in Keys)
            {
                cloneDict.Add(key, this[key]);
            }
            return cloneDict;
        }

        #endregion
    }
}