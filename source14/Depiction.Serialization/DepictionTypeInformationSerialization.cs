using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Xml;

namespace Depiction.Serialization
{
    //This thing is made static so it can be used by both serialization and coremodel/API. Also i would consider this a highly hack class
    public class DepictionTypeInformationSerialization
    {
        static public bool TypeFileExists
        {
            get; private set;
        }
        static public int DictionaryCountForTests
        {
            get { return simpleTypeNameToFullNameDictionary.Count; }
        }
        #region total save type helper. NOt sure if i would consider this good practice or a non hack, but it start for making types more user friendly
        static private Dictionary<string, string> simpleTypeNameToFullNameDictionary = new Dictionary<string, string>();
       
        static public void ResetTypeDictionaryForTests()
        {
            simpleTypeNameToFullNameDictionary.Clear();
            TypeFileExists = false;
        }
        
        private static bool AddKeyValue(string key, string value)
        {
            value = value.Replace("APINew", "API");
            if (simpleTypeNameToFullNameDictionary.ContainsKey(key)) return false;
            simpleTypeNameToFullNameDictionary.Add(key,value);
            return true;
        }
        public static string AddTypeToDictionaryGetSimpleName(Type type)
        {
            if (type == null) return string.Empty;
            var simpleName = type.Name;
            if (!simpleTypeNameToFullNameDictionary.ContainsKey(simpleName))
            {
                simpleTypeNameToFullNameDictionary.Add(simpleName, type.AssemblyQualifiedName);
            }
            return simpleName;
        }
        public static string GetFullTypeStringFromSimpleTypeString(string simpleType)
        {
            if (simpleTypeNameToFullNameDictionary.ContainsKey(simpleType))
            {
                return simpleTypeNameToFullNameDictionary[simpleType];
            }
            return string.Empty;
        }
        public static Type GetFullTypeFromSimpleTypeString(string simpleType)
        {
            if (simpleTypeNameToFullNameDictionary.ContainsKey(simpleType))
            {
                var type = Type.GetType(simpleTypeNameToFullNameDictionary[simpleType]);
                return type;
            }
            return Type.GetType(simpleType);
        }

        public static void UpdateSerializationServiceTypeDictionaryWithFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return;
            }
            TypeFileExists = true;
            //            rootPath = Path.GetDirectoryName(fileName);
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            if (simpleTypeNameToFullNameDictionary == null)
                simpleTypeNameToFullNameDictionary = new Dictionary<string, string>();
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = SerializationService.GetXmlReader(stream))
                    {
                        reader.ReadStartElement("elementTypes");
                        while (!reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            var key = reader.GetAttribute("key");
                            var value = reader.GetAttribute("value");
                            reader.Read();
                            AddKeyValue(key, value);
                        }
                        reader.ReadEndElement();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From DeserializeObject:" + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        public static void SaveSerializationServiceTypeDictionaryToFile(string fileName)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    using (var writer = SerializationService.GetXmlWriter(stream))
                    {
                        if (simpleTypeNameToFullNameDictionary != null && simpleTypeNameToFullNameDictionary.Keys.Count > 0)
                        {
                            writer.WriteStartElement("elementTypes");
                            //                writer.WriteStartElement("actions");
                            foreach (var action in simpleTypeNameToFullNameDictionary)
                            {
                                writer.WriteStartElement("typePair");
                                writer.WriteAttributeString("key", action.Key);
                                writer.WriteAttributeString("value", action.Value);
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine("From serialize :" + ex.Message); }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        #endregion
    }
}