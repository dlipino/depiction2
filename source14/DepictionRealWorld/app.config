<?xml version="1.0"?>
<configuration>
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"/>
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"/>
  </configSections>
  
  <loggingConfiguration name="Logging Application Block" tracingEnabled="true" defaultCategory="General" logWarningsWhenNoCategoriesMatch="true">
    <listeners>
      <add fileName="Depiction.Error.log" rollSizeKB="1000" timeStampPattern="yyyy-MM-dd" rollFileExistsBehavior="Increment" rollInterval="None" formatter="Text Formatter" header="----------------------------------------" footer="----------------------------------------" listenerDataType="Depiction.API.ExceptionHandling.UserScopedRollingFlatFileTraceListenerData, Depiction.API" traceOutputOptions="None" type="Depiction.API.ExceptionHandling.UserScopedRollingFlatFileTraceListener, Depiction.API" name="Rolling Flat File Trace Listener"/>
    </listeners>
    <formatters>
      <add template="Timestamp: {timestamp}
Message: {message}
Category: {category}
Priority: {priority}
EventId: {eventid}
Severity: {severity}
Title:{title}
Machine: {machine}
Application Domain: {appDomain}
Process Id: {processId}
Process Name: {processName}
Win32 Thread Id: {win32ThreadId}
Thread Name: {threadName}
Extended Properties: {dictionary({key} - {value}
)}" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Text Formatter"/>
    </formatters>
    <categorySources>
      <add switchValue="All" name="General">
        <listeners>
          <add name="Rolling Flat File Trace Listener"/>
        </listeners>
      </add>
    </categorySources>
    <specialSources>
      <allEvents switchValue="All" name="All Events"/>
      <notProcessed switchValue="All" name="Unprocessed Category"/>
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Rolling Flat File Trace Listener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>
  <exceptionHandling>
    <exceptionPolicies>
      <add name="Default Policy">
        <exceptionTypes>
          <add type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None" name="Exception">
            <exceptionHandlers>
              <add logCategory="General" eventId="100" severity="Error" title="Enterprise Library Exception Handling" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" priority="0" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Logging Handler"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="Handle and Resume Policy">
        <exceptionTypes>
          <add type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="None" name="Exception"/>
        </exceptionTypes>
      </add>
      <add name="Propagate Policy">
        <exceptionTypes>
          <add type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow" name="Exception"/>
        </exceptionTypes>
      </add>
      <add name="Replace Policy">
        <exceptionTypes>
          <add type="System.Security.SecurityException, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="ThrowNewException" name="SecurityException">
            <exceptionHandlers>
              <add exceptionMessage="Replaced Exception: User is not authorized to peform the requested action." exceptionMessageResourceType="" replaceExceptionType="System.ApplicationException, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.ReplaceHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Replace Handler"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
      <add name="Wrap Policy">
        <exceptionTypes>
          <add type="System.Data.DBConcurrencyException, System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="ThrowNewException" name="DBConcurrencyException">
            <exceptionHandlers>
              <add exceptionMessage="Wrapped Exception: A recoverable error occurred while attempting to access the database." exceptionMessageResourceType="" wrapExceptionType="ExceptionHandlingQuickStart.BusinessLayer.BusinessLayerException, ExceptionHandlingQuickStart.BusinessLayer" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WrapHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=3.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" name="Wrap Handler"/>
            </exceptionHandlers>
          </add>
        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>
  <system.serviceModel>
    <bindings>
      <customBinding>
        <binding name="CustomBinding_IDepictionWebService">
          <security defaultAlgorithmSuite="Default" authenticationMode="UserNameOverTransport" requireDerivedKeys="true" securityHeaderLayout="Strict" includeTimestamp="false" keyEntropyMode="CombinedEntropy" messageSecurityVersion="WSSecurity11WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10">
            <localClientSettings cacheCookies="true" detectReplays="false" replayCacheSize="900000" maxClockSkew="00:05:00" maxCookieCachingTime="Infinite" replayWindow="00:05:00" sessionKeyRenewalInterval="10:00:00" sessionKeyRolloverInterval="00:05:00" reconnectTransportOnFailure="true" timestampValidityDuration="00:05:00" cookieRenewalThresholdPercentage="60"/>
            <localServiceSettings detectReplays="false" issuedCookieLifetime="10:00:00" maxStatefulNegotiations="128" replayCacheSize="900000" maxClockSkew="00:05:00" negotiationTimeout="00:01:00" replayWindow="00:05:00" inactivityTimeout="00:02:00" sessionKeyRenewalInterval="15:00:00" sessionKeyRolloverInterval="00:05:00" reconnectTransportOnFailure="true" maxPendingSessions="128" maxCachedCookies="1000" timestampValidityDuration="00:05:00"/>
            <secureConversationBootstrap/>
          </security>
          <textMessageEncoding maxReadPoolSize="64" maxWritePoolSize="16" messageVersion="Default" writeEncoding="utf-8">
            <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384"/>
          </textMessageEncoding>
          <httpsTransport manualAddressing="false" maxBufferPoolSize="524288" maxReceivedMessageSize="65536" allowCookies="false" authenticationScheme="Anonymous" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" keepAliveEnabled="true" maxBufferSize="65536" proxyAuthenticationScheme="Anonymous" realm="" transferMode="Buffered" unsafeConnectionNtlmAuthentication="false" useDefaultWebProxy="true" requireClientCertificate="false"/>
        </binding>
      </customBinding>
    </bindings>
    <client>
  
<!-- the replacement request server, hopefully this doesn't mess up the release build-->
      <!--<endpoint address="https://buydepiction.depiction.local/DepictionWebService.svc" binding="customBinding" bindingConfiguration="CustomBinding_IDepictionWebService" contract="DepictionWebServiceOriginal.IDepictionWebService" name="ReleaseQuickstartService"/>-->
<!--      <endpoint address="https://buydepiction.depiction.local/DepictionWebService.svc" binding="customBinding" bindingConfiguration="CustomBinding_IDepictionWebService" contract="DepictionWebServiceOriginal.IDepictionWebService" name="DebugQuickstartService"/>-->
<!--      <endpoint address="https://debug.depiction.com/DepictionWebService.svc" binding="customBinding" bindingConfiguration="CustomBinding_IDepictionWebService" contract="DepictionWebServiceOriginal.IDepictionWebService" name="DebugQuickstartService"/>-->
      <endpoint address="https://request.depiction.com/DepictionWebService.svc" binding="customBinding" bindingConfiguration="CustomBinding_IDepictionWebService" contract="DepictionWebServiceOriginal.IDepictionWebService" name="ReleaseQuickstartService"/>
    </client>
  </system.serviceModel>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">

      <dependentAssembly>
        <assemblyIdentity name="CarbonTools.Core" publicKeyToken="e17062bfb214b90c"/>
        <bindingRedirect oldVersion="3.0.0.0-65535.65535.65535.65535" newVersion="3.1.0.0000"/>
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="CarbonTools.Data" publicKeyToken="e17062bfb214b90c"/>
        <bindingRedirect oldVersion="3.0.0.0-65535.65535.65535.65535" newVersion="3.1.0.0000"/>
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="CarbonTools.Content.OGC" publicKeyToken="e17062bfb214b90c"/>
        <bindingRedirect oldVersion="3.0.0.0-65535.65535.65535.65535" newVersion="3.1.0.0000"/>
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="CarbonTools.Content.ESRI" publicKeyToken="e17062bfb214b90c"/>
        <bindingRedirect oldVersion="3.0.0.0-65535.65535.65535.65535" newVersion="3.1.0.0000"/>
      </dependentAssembly>

      <dependentAssembly>
        <assemblyIdentity name="CarbonTools.Content.GoogleEarth" publicKeyToken="e17062bfb214b90c"/>
        <bindingRedirect oldVersion="3.0.0.0-65535.65535.65535.65535" newVersion="3.1.0.0000"/>
      </dependentAssembly>
      <!-- gives config files the ability to see addons-->
      <probing privatePath="Default Add-ons;Add-ons;"/>
    </assemblyBinding>
  </runtime>
<startup><supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/></startup></configuration>
