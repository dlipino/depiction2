using System;
using System.Threading;
using Depiction.API.Interfaces;
using Depiction.API.Service;

namespace Depiction.LicenseManager.Model
{
    /// <summary>
    /// Depiction's interface to the licensing system to determine whether 
    /// this is a purchased version of Depiction. Arg this is a total hack for now 1.4
    /// </summary>
    public class LicenseService : ILicenseService
    {
        private static Semaphore _accessSS;

        private SoftwareShieldService Sshield;
        #region Properties


        public string ErrorMessage { get { return Sshield.ErrorMessage; } }
        public DateTime ExpirationDate
        {
            get
            {
                return Sshield.ExpirationDate;
            }
        }


        public virtual DateTime CurrentDate { get { return DateTime.Now; } }

        public string TrialPeriodMesage
        {
            get
            {
                var daysLeft = DaysLeftInTrialPeriod();
                if(daysLeft<0)
                {
                    return string.Format("Trial period has expired.");
                }

                return string.Format("Trial period will expire in {0} days",daysLeft);
            }
        }
//        public bool IsValidLicense
//        {
//            get
//            {
//                if (Sshield == null)
//                    throw new Exception(string.Format("Unable to access license service. This should not have happened. Please contact {0}", DepictionAccess.ProductInformation.CompanyName));
//                return Sshield.IsValidLicense;
//            }
//        }

        public DepictionLicenseState LicenseState
        {
            get
            {
                return Sshield.LicenseState;
            }
        }

//        public bool IsActivatedLicense
//        {
//            get
//            {
//                if (Sshield == null)
//                    throw new Exception(string.Format("Unable to access license service. This should not have happened. Please contact {0}",
//                        DepictionAccess.ProductInformation.CompanyName));
//                return Sshield.IsActivated;
//            }
//        }

        public string SerialNumber
        {
            get
            {
                if (Sshield == null)
                    return string.Empty;

                return Sshield.SerialNumber;
            }
        }

       #endregion

        #region Constructors

        public LicenseService(string licenseFilename)
            : this(licenseFilename, "austrian_6_abode_._epitome", "featuring_CAMPANILE_2_newscast", 7)
        {
        }

        public LicenseService(string licenseFilename, string mainLicenseFilePassword, string globalAuthorizationCodePassword, int fingerPrintOptionsCode)
        {
            bool doesNotExist = false;
            try
            {
                _accessSS = Semaphore.OpenExisting("AccessSoftwareShield");
            }
            catch (WaitHandleCannotBeOpenedException)
            {
                doesNotExist = true;
            }
            if (doesNotExist)
            {
                _accessSS = new Semaphore(1, 1, "AccessSoftwareShield");
            }
            try
            {
                _accessSS.WaitOne();

                Sshield = new SoftwareShieldService(this, licenseFilename, mainLicenseFilePassword,
                                                    globalAuthorizationCodePassword, fingerPrintOptionsCode);
            }
            finally
            {
                _accessSS.Release();
            }
        }

     
        #endregion
        public int DaysLeftInTrialPeriod()
        {
//            return -1;
            if (Sshield == null) return 1;
            var daysLeft = (Sshield.ExpirationDate - DateTime.Today).Days;
            return daysLeft;
        }
        protected void RefreshLicenseInfo()
        {
            Sshield.UpdateLicenseInfo();
        }
        public void EndLicenseService()
        {
            if (Sshield == null) return;
            Sshield.EndSSCP();
            Sshield = null;
        }

        public bool ActivateLicense(string authCode, bool checkProductId, out string message)
        {
            return Sshield.ActivateLicense(authCode, checkProductId, out message);
        }
       

        public void Buy()
        {
            string url = "http://www.depiction.com/purchase";
            DepictionInternetConnectivityService.OpenBrowserInOrderTo(url, "buy a copy of Depiction");
        }

        public void RequestManualActivation(string number, int requestAuthCodeID)
        {
            Sshield.RequestManualActivation(number, requestAuthCodeID);
        }

        public void DeactivateLicense()
        {
            if (Sshield != null)
                Sshield.DeactivateLicense();
        }

        public bool ActivateManually(string manualActivationCode, out string message)
        {
            return Sshield.ActivateManually(manualActivationCode, out message);
        }
    }
}