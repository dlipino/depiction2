using System;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.DialogBases;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;
using Depiction.LicenseManager.Model;
using Depiction.LicenseManager.View;

namespace Depiction.LicenseManager
{
    public class LicensingManager
    {
        private ILicenseService licenseService;
        private string modernFullLicenseName = string.Empty;
        private string fullLicenseToUse = string.Empty;
        private bool usingLegacyLicense;
        private ILicensingUI licensingUI;
        //        public bool HasValidLicenseFile
        //        {
        //            get { return licenseService.HasValidLicenseFile; }
        //        }

        public ILicenseService LicenseService
        {
            // should not really expose this.  Only needed once in DepictionwindowActions.  TODO: Find a better way to do that.
            get { return licenseService; }

        }

        public LicensingManager()
        {
            //Legacy lic files, simple version. A more complete version would check to see if the file is authorized then set it to new value if it
            //is not
            var legacyPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction");
            var legacyFullFile = string.Empty;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Release:
                    legacyFullFile = Path.Combine(legacyPath, "licensev1.lic");
                    break;
            }
            //Check for legacy and take note
            if (File.Exists(legacyFullFile))
            {
                usingLegacyLicense = true;
            }
            //Now deal with the new stuff
            var licenseDir = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction_Inc");
            if (string.IsNullOrEmpty(DepictionAccess.ProductInformation.LicenseFileName))
            {
                modernFullLicenseName = string.Empty;
            }
            else
            {
                modernFullLicenseName = Path.Combine(licenseDir, DepictionAccess.ProductInformation.LicenseFileName);
            }

            if (usingLegacyLicense)
            {
                fullLicenseToUse = legacyFullFile;
            }else
            {

                fullLicenseToUse = modernFullLicenseName;
            }
            licensingUI = new LicensingUI();
        }

        //        public LicensingManager(ILicensingUI licensingUI, ILicenseService licenseService)
        //            : this()
        //        {
        //            this.licensingUI = licensingUI;
        //            this.licenseService = licenseService;
        //        }


        //        public void EndLicenseService()
        //        {
        //            if (licenseService == null) return;
        //            licenseService.EndLicenseService();
        //            licenseService = null;
        //        }
        public bool CheckLicense()
        {

            var productName = DepictionAccess.ProductInformation.ProductName;
            var companyName = DepictionAccess.ProductInformation.CompanyName;
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Checking License");
            try
            {
                if (licenseService == null)
                {
                    licenseService = new LicenseService(fullLicenseToUse);
                    //This should only happen if there is an unactivated version of depiction on the comp
                    //hopefully that is a very small percentage of users.
                    Console.WriteLine(licenseService.LicenseState);
                    if (usingLegacyLicense && licenseService.LicenseState.Equals(DepictionLicenseState.UnActivated))
                    {
                        fullLicenseToUse = modernFullLicenseName;
                        licenseService = new LicenseService(fullLicenseToUse);
                    }
                }
                switch (licenseService.LicenseState)
                {
                    case DepictionLicenseState.LicenseFileMissing:
                        var msg = string.Format("You must have a valid license to run {0}. Please contact {1}. Shutting down.",
                                               companyName, productName);
                        DepictionMessageBox.ShowDialog(msg, "Missing license file", MessageBoxButton.OK);
                        //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(msg);
                        return false;
                    case DepictionLicenseState.InvalidLicense:

                        var msg2 = string.Format("{0} could not run because:\n\n{1} ", productName,
                                                 licenseService.ErrorMessage);
                        DepictionMessageBox.ShowDialog(msg2, "Invalid License", MessageBoxButton.OK);
                        //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(msg2);
                        return false;
                    default:
                        //                        if (licenseService.IsTrialMode)
                        //                            return CheckTrialModeLicense();
                        return CheckPurchasedLicense();
                }
            }
            catch (Exception e)
            {
                DepictionAccess.NotificationService.DisplayInquiryDialog(DepictionExceptionHandler.MessageFromExceptionAndInnerException(e), "Depiction start error", MessageBoxButton.OK);
                DepictionExceptionHandler.HandleException(e,false,true);
                //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Exception in CheckLicense", e);
                //                CommonData.depictionAccessInternalHandle.NotificationService.SendNotificationDialog(
                //                    ExceptionHandler.MessageFromExceptionAndInnerException(e));
            }
            return false;
        }


        private bool CheckPurchasedLicense()
        {
            switch (licenseService.LicenseState)
            {
                case DepictionLicenseState.UnActivated:
                    if (licensingUI.LaunchPurchaseActivationUI(licenseService))
                        return true;
                    // return CheckPurchasedLicense();
                    //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Purchased License Unactivated");
                    return false;
                case DepictionLicenseState.ActivatedExpired:
                    //                    if (DepictionAccess._productInformation.RenewableLicense && licensingUI.LaunchRenewalUI(licenseService))
                    //                        return CheckPurchasedLicense();
                    // CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Purchased License Expired");
                    return true;

                case DepictionLicenseState.ActivatedUnexpired:
                case DepictionLicenseState.ActivatedNoExpiration:
                    return true;
                case DepictionLicenseState.UnActivatedInTrialPeriod:
                    if (licensingUI.LaunchPurchaseActivationUI(licenseService))
                        return true;
                    return false;

                default:
                    //  CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("License Check unsuccessful: Unexpected value for LicenseState ");
                    return false;
            }
        }
        
        public bool DeactivateLicense()
        {
            licenseService.DeactivateLicense();
            return true;// licenseService.IsDeactivated;
        }
    }
}