using Depiction.API.Interfaces;

namespace Depiction.LicenseManager.View
{
    public interface ILicensingUI
    {
        bool LaunchPurchaseActivationUI(ILicenseService licenseService);
    }
}