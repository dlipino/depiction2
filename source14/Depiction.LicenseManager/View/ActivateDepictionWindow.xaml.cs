﻿using System;
using System.Windows;
using System.Windows.Controls;
using Depiction.API;
using Depiction.API.DialogBases;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;

namespace Depiction.LicenseManager.View
{
    /// <summary>
    /// Interaction logic for ActivateDepictionWindow.xaml
    /// </summary>
    public partial class ActivateDepictionWindow
    {
        public ActivateDepictionWindow()
        {
            InitializeComponent();
            DataContextChanged += ActivateDepictionWindow_DataContextChanged;
            serialNumberText.TextChanged += serialNumberText_TextChanged;
            manualActivationText.TextChanged += serialNumberText_TextChanged;
        }

        void ActivateDepictionWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dc = DataContext as ILicenseService;
            if (dc == null) return;
            var daysLeft = dc.DaysLeftInTrialPeriod();
            if (daysLeft < 0) trialButton.IsEnabled = false;
        }

        public static readonly string PurchaseProductText = string.Format("Purchase {0}", DepictionAccess.ProductInformation.ProductName); 

        private void buyButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as ILicenseService;
            if (dc == null) return;
            dc.Buy();
        }
        private void trialButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void RequestManualActivationButton_Click(object sender, RoutedEventArgs e)
        {
            var licenseService = DataContext as ILicenseService;
            if (licenseService == null) return;
            licenseService.RequestManualActivation(serialNumberText.Text, DepictionAccess.ProductInformation.LicenseActivationID);
 
        }

        private static string CleanUpSerialNumber(string serialNumber)
        {
            return serialNumber.ToUpper().Replace('I', '1').Replace('O', '0').Replace('S', '5').Replace('Z', '2');
        }

        public static void serialNumberText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = ((TextBox) sender);
            int index = textBox.CaretIndex;
            textBox.Text = CleanUpSerialNumber(textBox.Text);
            textBox.CaretIndex = index;
        }
        private void OkButton_Click(object sender, RoutedEventArgs args)
        {
            var licenseService = DataContext as ILicenseService;
            
            if (licenseService == null) return;
            var productName = DepictionAccess.ProductInformation.ProductName;
            try
            {
                string message;
                bool activated = licenseService.ActivateLicense(serialNumberText.Text, true, out message);
                if (activated)
                {
                    DepictionMessageBox.ShowDialog(this,
                        string.Format("Congratulations! Your copy of {0} is now activated.",
                                      productName),
                        string.Format("{0} activated", productName));
                    DialogResult = true;
                    Close();
                }
                else
                {
                    DepictionMessageBox.ShowDialog(this,
                        string.Format("Your copy of {0} failed to activate.<br/><br/>{1}", productName,
                                      message),
                        string.Format("{0} not activated", productName));
                }
            }
            catch (Exception e)
            {
                DepictionMessageBox.ShowDialog(this,
                    DepictionExceptionHandler.MessageFromExceptionAndInnerException(e), "Depiction Error");
            }
        }
        private void btnManuallyActivate_Click(object sender, RoutedEventArgs args)
        {
            var licenseService = DataContext as ILicenseService;
            if (licenseService == null) return;
            var productName = DepictionAccess.ProductInformation.ProductName;
            try
            {
                string message;
                bool activated = licenseService.ActivateManually(manualActivationText.Text, out message);
                if (activated)
                {
                    DepictionMessageBox.ShowDialog(this,
                        string.Format("Congratulations! Your copy of {0} is now activated.", productName),
                        string.Format("{0} activated", productName));
                    DialogResult = true;
                    Close();
                }
                else
                {
                    DepictionMessageBox.ShowDialog(this,
                        string.Format("Manual activation failed for your copy of {0}.<br/><br/>{1}",
                        productName,
                                      message),
                        string.Format("{0} not activated", productName));
                }
            }
            catch (Exception ex)
            {
                DepictionMessageBox.ShowDialog(this,
                    DepictionExceptionHandler.MessageFromExceptionAndInnerException(ex), "Depiction error");
            }

        }
    }
}