﻿using System;
using System.Windows;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.PanZoomWidget
{
    /// <summary>
    /// Interaction logic for PanZoomControlView.xaml
    /// </summary>
    public partial class PanZoomControlView
    {
        public PanZoomControlView()
        {
            InitializeComponent();
//            Loaded += PanZoomControlView_Loaded;
            DataContextChanged += PanZoomControlView_DataContextChanged;
            Settings.Default.PropertyChanged += Default_PropertyChanged;
        }

        void PanZoomControlView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldViewModel = e.OldValue as DepictionMapQuickAddAndTileViewModel;
            var newViewModel = e.NewValue as DepictionMapQuickAddAndTileViewModel;
            
            if (oldViewModel != null)
            {
                if(oldViewModel.WorldModel != null)
                {
                    oldViewModel.WorldModel.DepictionViewportChange -= WorldModel_DepictionViewportChange;
                }
              
            }
            if (newViewModel != null)
            {
                if(newViewModel.WorldModel != null)
                {
                    newViewModel.WorldModel.DepictionViewportChange += WorldModel_DepictionViewportChange;
                    UpdateScaleVisualString();
                }
            }
        }

        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("measurementScale", StringComparison.InvariantCultureIgnoreCase) ||
                e.PropertyName.Equals("measurementSystem", StringComparison.InvariantCultureIgnoreCase))
            {
                UpdateScaleVisualString();
            }
        }

        void WorldModel_DepictionViewportChange(IMapCoordinateBounds oldBounds, IMapCoordinateBounds newBounds)
        {
            UpdateScaleVisualString();
        }

        protected double[] validScaleValues = new double[]
                                                  {.005, .01,.025,.05,.1,.25, .50, 1, 2, 5, 10, 25, 50, 100, 200, 300, 400, 500, 1000};
        protected void UpdateScaleVisualString()
        {

            var transform = ScaleRect.TransformToAncestor(Application.Current.MainWindow);
            var transRect = transform.TransformBounds(ScaleRect.RenderedGeometry.Bounds);
            transRect.Width = 150;
            if (WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel != null)
            {
                var geoCoords = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(transRect);
                //var distanceString = geoCoords.TopLeft.DistanceToAsString(geoCoords.TopRight, Settings.Default.MeasurementSystem, MeasurementScale.Large);
                var scaleDistance = geoCoords.TopLeft.DistanceTo(geoCoords.TopRight, Settings.Default.MeasurementSystem, MeasurementScale.Large);
                //Based on scale and total size of canvas we can probably find the best scale without doing these calculations
                double minDistance = Double.PositiveInfinity;
                double validScale = 1;
                for(int i=0;i<validScaleValues.Length;i++)// dist in validScaleValues)
                {
                    var newDist = Math.Abs(validScaleValues[i] - scaleDistance);
                    if(newDist <minDistance)
                    {
                        minDistance = newDist;
                        validScale = validScaleValues[i];
                    }
                }
                var finalCoord = geoCoords.TopLeft.TranslateTo(90, validScale, Settings.Default.MeasurementSystem,
                                              MeasurementScale.Large);
                
                var pixelCoord = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.WorldToGeoCanvas(finalCoord);
                ScaleRect.Width = pixelCoord.X -transRect.Left ;
                bool isSingle = false;
                if (validScale == 1)
                {
                    isSingle = true;
                }
                var scaleUnits =
                    new Distance().GetUnits(Settings.Default.MeasurementSystem, MeasurementScale.Large, isSingle);
                scaleText.Text = String.Format("{0:0.###} {1}", validScale, scaleUnits);  
            }
            
        }
//        void PanZoomControlView_Loaded(object sender, RoutedEventArgs e)
//        {
//            //This seems to work on first load, not sure what will happen on a save/reload/new
//            var mainWindow = Application.Current.MainWindow;
//            if (mainWindow == null) return;
//
//            var context = DataContext as WorldMapViewModel;
//            if (context == null) return;
//
//            UpdateScaleVisualString();
//
//            var bindings = mainWindow.InputBindings;
//
//            // Zoom in coarse key binding (PageUp)
//            InputBinding inputBinding = new KeyBinding(context.ZoomCommand, new KeyGesture(Key.PageUp));
//            bindings.Add(inputBinding);
//
//            // Zoom in fine key binding (Ctrl-PageUp)
//            inputBinding = new KeyBinding(context.ZoomCommand, new KeyGesture(Key.PageUp, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//
//            // Zoom out coarse key binding (PageDown)
//            inputBinding = new KeyBinding(context.ZoomCommand, new KeyGesture(Key.PageDown));
//            bindings.Add(inputBinding);
//
//            // Zoom out fine key binding (Ctrl-PageDown)
//            inputBinding = new KeyBinding(context.ZoomCommand, new KeyGesture(Key.PageDown, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//
//            // Pan left coarse key binding (Left arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Left));
//            bindings.Add(inputBinding);
//
//            // Pan left fine key binding (Ctrl - Left arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Left, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//
//            // Pan right coarse key binding (Right arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Right));
//            bindings.Add(inputBinding);
//
//            // Pan right fine key binding (Ctrl - Right arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Right, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//
//            // Pan Up coarse key binding (Up arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Up));
//            bindings.Add(inputBinding);
//
//            // Pan Up fine key binding (Ctrl - Up arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Up, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//
//            // Pan Down coarse key binding (Down arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Down));
//            bindings.Add(inputBinding);
//
//            // Pan Down fine key binding (Ctrl - Down arrow)
//            inputBinding = new KeyBinding(context.PanCommand, new KeyGesture(Key.Down, ModifierKeys.Control));
//            bindings.Add(inputBinding);
//        }
    }
}
