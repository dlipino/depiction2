﻿using System;
using System.Windows.Threading;
using Depiction.API;

namespace Depiction.View.AlertsExpander
{
    /// <summary>
    /// Interaction logic for Alerts.xaml
    /// </summary>
    public partial class Alerts
    {
        public Alerts()
        {
            InitializeComponent();
            var messageService= DepictionAccess.NotificationService;
            if(messageService != null)
            {
                messageService.MessageAdded += messageService_MessageAdded;
            }
        }

        void messageService_MessageAdded()
        {
            ExpandAlerts();
        }
        private void ExpandAlerts()
        {
            if (Dispatcher != null)
            {
                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(ExpandAlerts));
                    return;
                }
            }
            AlertExpander.IsExpanded = true;
        }
    }
}