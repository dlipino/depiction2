﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.RegionSelector
{
    public class RegionSelectorContentControl : ContentControl
    {
        private bool eventsAttached = false;
        static RegionSelectorContentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RegionSelectorContentControl), new FrameworkPropertyMetadata(typeof(RegionSelectorContentControl)));
        }
        public RegionSelectorContentControl()
        {//How do un hook these things?
            IsVisibleChanged += RegionSelectorContentControl_IsVisibleChanged;

        }

        private void MainWindow_KeyEvent(object sender, KeyEventArgs e)
        {
            if (IsVisible)
            {
                if (e.Key.Equals(Key.LeftShift) || e.Key.Equals(Key.RightShift))
                {
                    if (e.IsDown)
                    {
                        IsHitTestVisible = false;
                    }
                    if (e.IsUp)
                    {
                        IsHitTestVisible = true;
                    }
                }
            }else
            {
                e.Handled = false;
            }
        }

        void RegionSelectorContentControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!e.NewValue.Equals(true))
            {
                DettachMainWindowKeyboardEvents();
                return;
            }
            AttachMainWindowKeyboardEvents();
            var context = DataContext as DepictionBasicMapViewModel;
            if (context == null) return;
            if(!context.RegionExists)
            {
                context.RegionSelectionBounds = GetDefaultSelectionBounds();
            }
        }

        protected void AttachMainWindowKeyboardEvents()
        {
            if (Application.Current != null && Application.Current.MainWindow != null && !eventsAttached)
            {
                Application.Current.MainWindow.KeyDown += MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp += MainWindow_KeyEvent;
                eventsAttached = true;
            }
        }

        protected void DettachMainWindowKeyboardEvents()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.KeyDown -= MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp -= MainWindow_KeyEvent;
                eventsAttached = false;
            }
        }
        private Rect GetDefaultSelectionBounds()
        {
            var parent = Application.Current.MainWindow;// Parent as FrameworkElement;
            if(parent == null) return new Rect(0,0,100,100);

            //Not sure if this part acceptable in MVVM 
            var aW = parent.ActualWidth;
            var aH = parent.ActualHeight;

            var w = aW * .6;
            var h = aH * .6;
            var t = aH * .2;
            var l = aW * .2;
            return new Rect(l, t, w, h);
        }
    }
}
