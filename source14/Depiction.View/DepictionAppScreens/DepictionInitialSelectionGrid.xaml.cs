﻿using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.DepictionAppScreens
{
    /// <summary>
    /// Interaction logic for DepictionInitialSelectionGrid.xaml
    /// </summary>
    public partial class DepictionInitialSelectionGrid
    {
        public DepictionInitialSelectionGrid()
        {
                InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
//                     initialCreateDepictionButton.Visibility = Visibility.Collapsed;
//                        settingButton.Visibility = Visibility.Collapsed;
//                        initialTutorialsButton.Visibility = Visibility.Collapsed;
//                        initialSamplesButton.Visibility = Visibility.Collapsed;
                    initialButtonStackPanel.Children.Remove(initialCreateDepictionButton);
                    initialButtonStackPanel.Children.Remove(settingButton);
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(initialSamplesButton);

                    break;
                case ProductInformationBase.Prep:
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    break;
                case ProductInformationBase.DepictionRW:
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    initialButtonStackPanel.Children.Remove(initialSamplesButton);
                    break;
                default:
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    break;
            }

//#if PREP
//            initialButtonStackPanel.Children.Remove(initialTutorialsButton);
//#endif
        }
    }
}
