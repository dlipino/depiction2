﻿using System;
using System.Windows;

namespace Depiction.View.DepictionAppScreens
{
    /// <summary>
    /// Interaction logic for DepictionStartScreen.xaml
    /// </summary>
    public partial class DepictionStartScreen
    {
        public DepictionStartScreen()
        {
            Initialized += DepictionStartScreen_Initialized;
            InitializeComponent();
          
        }

        void DepictionStartScreen_Initialized(object sender, EventArgs e)
        {
//#if PREP
//            var defaultImageRD = new ResourceDictionary();
//            defaultImageRD.Source = new Uri("DepictionPrep.Resources;Component/PrepMainWindowImageResources.xaml", UriKind.RelativeOrAbsolute);
//            Resources.MergedDictionaries.Add(defaultImageRD);
//#else
//            var defaultImageRD = new ResourceDictionary();
//            defaultImageRD.Source = new Uri("Depiction.DefaultResources;Component/MainWindowImageResources.xaml",UriKind.RelativeOrAbsolute);
//            Resources.MergedDictionaries.Add(defaultImageRD);
//#endif
        }
    }
}
