﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Depiction.API.MVVM;
using Depiction.CoreModel.SampleModels;

namespace Depiction.View.DepictionAppScreens
{
    /// <summary>
    /// Interaction logic for DepictionSampleGrid.xaml
    /// </summary>
    public partial class DepictionSampleGrid 
    {
        SampleDepictions sampleHack = new SampleDepictions();
        public DepictionSampleGrid()
        {
            InitializeComponent();
            string path = AppDomain.CurrentDomain.BaseDirectory;
#if DEBUG
            path =  @"c:\DepictionSamples";
#else
            path = Path.Combine(path, "samples");
#endif
            sampleListBox.ItemsSource = sampleHack.FindSampleDepictions(path,5);
        }
        private void sampleListBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LoadSampleFromListBoxSelection();
        }
        private void loadSampleButton_Click(object sender, RoutedEventArgs e)
        {
            LoadSampleFromListBoxSelection();
        }
        private void LoadSampleFromListBoxSelection()
        {
            var selectedSample = sampleListBox.SelectedItem as SampleFile;
            if (selectedSample == null) return;
            LoadDepictionFromSample(selectedSample);
        }
        private void LoadDepictionFromSample(SampleFile sampleFile)
        {
            var app = Application.Current.MainWindow as IDepictionMainWindow;
            if (app == null) return;
            if (sampleFile == null) return;
            app.OpenDepictionFileWithFullView(sampleFile.FilePath,true);

        }

      
    }
}
