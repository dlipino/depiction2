﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.ViewHelpers
{
    public class IconVisual : FrameworkElement
    {
        //Im really not sure how this is supposed to be faster, since it still uses a framework element
        private DrawingVisual iconToShow;
        private DrawingVisual iconBorder;
        private Brush iconBrush;
        //        private ImageSource IconImageSource;
        #region Dep props

        public SolidColorBrush IconBorderBrush
        {
            get { return (SolidColorBrush)GetValue(IconBorderBrushProperty); }
            set { SetValue(IconBorderBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconBorderBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconBorderBrushProperty =
            DependencyProperty.Register("IconBorderBrush", typeof(SolidColorBrush), typeof(IconVisual), new UIPropertyMetadata(null, IconBorderBrushChanged));

        private static void IconBorderBrushChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var iconVisual = d as IconVisual;
            if (iconVisual == null) return;
            iconVisual.RedrawIconAndBorder();
        }

        public ImageSource IconImageSource
        {
            get { return (ImageSource)GetValue(IconImageSourceProperty); }
            set { SetValue(IconImageSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconImageSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconImageSourceProperty =
            DependencyProperty.Register("IconImageSource", typeof(ImageSource), typeof(IconVisual), 
            new UIPropertyMetadata(null, IconSourceChanged));

        private static void IconSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var iconVisual = d as IconVisual;
            if (iconVisual == null) return;
            iconVisual.RedrawIconAndBorder();
        }

        public DepictionIconBorderShape BorderShapeType
        {
            get { return (DepictionIconBorderShape)GetValue(BorderShapeTypeProperty); }
            set { SetValue(BorderShapeTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BorderShapeType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BorderShapeTypeProperty =
            DependencyProperty.Register("BorderShapeType", typeof(DepictionIconBorderShape), typeof(IconVisual), new UIPropertyMetadata(DepictionIconBorderShape.None, IconBorderTypeChange));

        private static void IconBorderTypeChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var iconVisual = d as IconVisual;
            if (iconVisual == null) return;
            iconVisual.RedrawIconAndBorder();
        }
        public static readonly DependencyProperty ActiveProperty =
       DependencyProperty.Register("Active",
           typeof(bool),
           typeof(IconVisual), new UIPropertyMetadata(true, IconSourceChanged));

       
//           new FrameworkPropertyMetadata(true,
//               FrameworkPropertyMetadataOptions.AffectsMeasure |
//               FrameworkPropertyMetadataOptions.AffectsRender));

        public bool Active
        {
            set { SetValue(ActiveProperty, value); }
            get { return (bool)GetValue(ActiveProperty); }
        }
        #endregion

        protected override int VisualChildrenCount
        {
            get
            {
                var count = 0;
                if (iconToShow != null) count++;
                if (iconBorder != null) count++;
                return count;
            }
        }

        #region constructor
        public IconVisual()
        {
            Loaded += IconVisual_Loaded;
            SizeChanged += IconVisual_SizeChanged;

        }
        #endregion

        #region event handlers

        void IconVisual_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RedrawIconAndBorder();
        }

        void IconVisual_Loaded(object sender, RoutedEventArgs e)
        {
            RedrawIconAndBorder();
        }

        #endregion

        protected override Visual GetVisualChild(int index)
        {
            int upper = 1;
            if (iconBorder != null) upper = 2;
            if (index < 0 || index >= upper) throw new IndexOutOfRangeException("Not that many children in visual.");
            if (upper == 2)
            {
                if (index == 0) return iconBorder;
                if (index == 1) return iconToShow;
                return null;
            }

            return iconToShow;
        }

        #region helper methods

        private void RedrawIconAndBorder()
        {
            RedrawIconToShow();
            RedrawIconBorder();

        }

        private void RedrawIconToShow()
        {
            RemoveVisualChild(iconToShow);
            RemoveLogicalChild(iconToShow);
            if (IconImageSource == null)
            {

                iconBrush = null;
                iconToShow = CreateCircleGeomAtCenter(new Point(Width / 2d, Height / 2), 0);
            }
            else
            {
                iconToShow = CreateImageVisual(new Point(-Width / 2d, -Height / 2), Width);
            }
            AddVisualChild(iconToShow);
            AddLogicalChild(iconToShow);
        }

        private void RedrawIconBorder()
        {
            RemoveVisualChild(iconBorder);
            RemoveLogicalChild(iconBorder);
            if (IconBorderBrush != null)
            {//|| !BorderShapeType.Equals(DepictionIconBorderShape.None), take care of in methods
                iconBorder = CreateBorder(new Point(), Width * 1.5);
            }
            else
            {
                iconBorder = null;
            }

            AddVisualChild(iconBorder);
            AddLogicalChild(iconBorder);
        }


        #endregion
        #region Icon drawing helpers
        private DrawingVisual CreateBorder(Point center, double shapeDim)
        {
            Geometry geom = null;
            var halfSize = shapeDim / 2d;
            
            switch (BorderShapeType)
            {
                case DepictionIconBorderShape.None:
                    return null;
                case DepictionIconBorderShape.Circle:
                    geom = new EllipseGeometry(center, halfSize, halfSize);
                    break;
                case DepictionIconBorderShape.Triangle:
                    var triangleOffset = halfSize*.2;
                    var trianglePoints = new EnhancedPointListWithChildren();
                    trianglePoints.IsClosed = true;
                    trianglePoints.IsFilled = false;
                    trianglePoints.Outline.Add(new Point(0, -halfSize - triangleOffset));
                    trianglePoints.Outline.Add(new Point(-halfSize, halfSize - triangleOffset));
                    trianglePoints.Outline.Add(new Point(halfSize, halfSize - triangleOffset));
                    var triangleDrawPoints = new List<EnhancedPointListWithChildren>();
                    triangleDrawPoints.Add(trianglePoints);
                    geom = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(triangleDrawPoints);
                    break;
                case DepictionIconBorderShape.Square:
                    geom = new RectangleGeometry(new Rect(new Point(-halfSize, -halfSize), new Size(shapeDim, shapeDim)));

                    break;
            }

            if (geom == null) return null;
            geom.Freeze();
            var pen = new Pen(IconBorderBrush, 1);
            pen.Freeze();
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext dc = drawingVisual.RenderOpen())
            {
                dc.DrawGeometry(null, pen, geom);
            }
            return drawingVisual;
        }

        private DrawingVisual CreateCircleGeomAtCenter(Point center, double shapeDim)
        {

            EllipseGeometry ellGeo = new EllipseGeometry(center, shapeDim / 2d, shapeDim / 2d);
            ellGeo.Freeze();

            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext dc = drawingVisual.RenderOpen())
            {
                dc.DrawGeometry(iconBrush, null, ellGeo);
            }
            return drawingVisual;
        }
        private DrawingVisual CreateImageVisual(Point topLeft, double shapeDim)
        {
            Rect drawArea = new Rect(topLeft, new Size(shapeDim, shapeDim));

            DrawingVisual drawingVisual = new DrawingVisual();
            IconImageSource.Freeze();
            using (DrawingContext dc = drawingVisual.RenderOpen())
            {
                dc.DrawImage(IconImageSource, drawArea);
                if (Application.Current.Resources.Contains("NonActiveIcon") &&
                    !Active)
                {
                    var rawImage = Application.Current.Resources["NonActiveIcon"] as ImageSource;
                    if (rawImage != null) dc.DrawImage(rawImage, drawArea);
                }
            }

            return drawingVisual;
        }
        #endregion
    }
}
