﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Depiction.View.ViewHelpers.Adorners
{
    public class AllElementDragAdorner : Adorner
    {
        /// <summary>
        /// 
        /// </summary>
        protected UIElement child;
        protected VisualBrush brush;
        protected UIElement owner;

        public AllElementDragAdorner(UIElement owner, ImageSource dragIcon, Dictionary<UIElement, Rect> adornerElements, double opacity, Point offSetFromCenter)
            : base(owner)
        {
            System.Diagnostics.Debug.Assert(owner != null);
            System.Diagnostics.Debug.Assert(adornerElements.Count > 0);
            IsHitTestVisible = false;
            this.owner = owner;
            var adornerUI = adornerElements.Keys.ToList();
            var adornerSize = adornerElements.Values.ToList();
            var t = new Canvas {Opacity = opacity};

            var adornerBounds = adornerSize[0];
            var startLoc = adornerBounds.TopLeft;
            //All math related to offSetFromCenter had a 50/50 chance of being right, there for little thought was 
            //put into the operation.
            startLoc.X += offSetFromCenter.X;
            startLoc.Y += offSetFromCenter.Y;
            var r = new Image { Width = adornerBounds.Width, Height = adornerBounds.Height };
            Canvas.SetTop(r, adornerBounds.Top - offSetFromCenter.Y);
            Canvas.SetLeft(r, adornerBounds.Left - offSetFromCenter.X);

            if (dragIcon != null) r.Source = dragIcon;
            Panel.SetZIndex(r,1);
            t.Children.Add(r);

            if (adornerUI.Count > 1)//This should work with the ZOI, in theory
            {
                var w = new Rectangle();
                var bounds = adornerSize[1];
                w.Width = bounds.Width;
                w.Height = bounds.Height;
                Canvas.SetTop(w, -bounds.Y - offSetFromCenter.Y);
                Canvas.SetLeft(w, -bounds.X - offSetFromCenter.X);
                w.Fill = new VisualBrush(adornerUI[1]);
                Panel.SetZIndex(w,0);//this is different
                t.Children.Add(w);
            }
            child = t;
        }

        #region Stuff for updating

        private double leftOffset;
        public double LeftOffset
        {
            get { return leftOffset; }
            set
            {
                leftOffset = value;
                UpdatePosition();
            }
        }

        private double topOffset;
        public double TopOffset
        {
            get { return topOffset; }
            set
            {
                topOffset = value;
                UpdatePosition();
            }
        }

        private void UpdatePosition()
        {
            var adorner = (AdornerLayer)Parent;
            if (adorner != null)
            {
                adorner.Update(AdornedElement);
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            return child;
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        protected override Size MeasureOverride(Size finalSize)
        {
            child.Measure(finalSize);
            return child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            child.Arrange(new Rect(child.DesiredSize));
            return finalSize;
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            var result = new GeneralTransformGroup();
            result.Children.Add(base.GetDesiredTransform(transform));
            result.Children.Add(new TranslateTransform(leftOffset, topOffset));
            return result;
        }
        #endregion
    }
}