﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Depiction.View.ViewHelpers.Adorners
{
    public class ElementAddAdorner : Adorner
    {
        //This is actually probably more general than it needs to be ie lots of cruft
        protected VisualBrush brush;
        protected UIElement owner;
        
        public ElementAddAdorner(UIElement owner, ImageSource iconImage, double opacity)
            : base(owner)
        {
            this.owner = owner;
            SetIcon(iconImage);
            IsHitTestVisible = false;
            Opacity = opacity;
        }

        public void SetIcon(ImageSource iconImage)
        {
            var im = new Image { Source = iconImage };
            brush = new VisualBrush(im);
        }
        protected override void OnRender(DrawingContext drawingContext)
        {
            var size = 20d/owner.RenderTransform.Value.M11;
            var pos = Mouse.GetPosition(owner);
            var newPos = new Point(-size/2 + pos.X, -size/2 + pos.Y);
            var adornedElementRect = new Rect(newPos, new Size(size, size));
            drawingContext.DrawRectangle(brush, new Pen(Brushes.Transparent, .01), adornedElementRect);
        }

        public void UpdateAdorner()
        {
            var adorner = (AdornerLayer)Parent;
            if (adorner != null)
            {
                adorner.Update(AdornedElement);
            }
        }
    }
}