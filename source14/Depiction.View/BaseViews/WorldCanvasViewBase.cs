﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Depiction.API.Properties;
using Depiction.View.ViewMouseEventHelpers;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.BaseViews
{
    public class WorldCanvasViewBase : DragCanvasViewBase
    {
        protected Point PreviousPosition = new Point(double.NaN, double.NaN);
        protected List<Rectangle> grabRects = new List<Rectangle>();
        public Thumb Dragger { get; protected set; }

        #region Constructor
        public WorldCanvasViewBase()
        {
            //Done in xaml
            Loaded += DepictionWorldCanvasView_Loaded;
            Unloaded += DepictionWorldCanvasView_Unloaded;
            DataContextChanged += DepictionWorldCanvasView_DataContextChanged;
            
        }
        #endregion
        #region Constructo events and events caused by xaml

        void DepictionWorldCanvasView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newContext = e.NewValue as WorldMapViewModel;
            var oldContext = e.OldValue as WorldMapViewModel;

            if (oldContext != null)
            {
                oldContext.PropertyChanged -= ViewModel_PropertyChanged;
                oldContext.DepictionPixelViewportChangeRequested -= ViewModel_DepictionPixelViewportChangeRequested;
                GC.Collect();
            }
            if (newContext != null)
            {
                QuadrantOpacity = .3;
                QuadrantFillBrush = DefaultFill;
                newContext.DepictionPixelViewportChangeRequested += ViewModel_DepictionPixelViewportChangeRequested;
                newContext.PropertyChanged += ViewModel_PropertyChanged;
                //Hmmmm, is this needed now because of all the viewport code modifications? Nope, still needed because context gets set after
                //vm is set
                if (IsLoaded)
                {
                    if (newContext.PixelViewport.IsEmpty)
                    {
                        CenterPixelPointInContainer(newContext.CurrentCenter);
                    }
                    else
                    {
                        newContext.RequestPixelViewportChangeFromView(newContext.PixelViewport);
                    }
                }
            }
        }

        Rect ViewModel_DepictionPixelViewportChangeRequested(Rect requested)
        {
            var dataContext = DataContext as WorldMapViewModel;
            if(dataContext == null) return requested;
            var rect = GetActualPixelViewportFromRequested(requested,dataContext);
            return rect;
        }
        virtual protected void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var name = e.PropertyName;
            if(name.Equals("Scale") || name.Equals("Translate"))
            {
                WorldTransformChanged(name.Equals("Scale"));
            }else if(name.Equals("CurrentCenter"))
            {
                var context = DataContext as WorldMapViewModel;
                if (context == null) return;
                CenterPixelPointInContainer(context.CurrentCenter);
            }
        }
        public void SetVisibleViewportFromModel(WorldMapViewModel mapViewModel)
        {
            if (mapViewModel.PixelViewport.Equals(Rect.Empty))
            {
                CenterPixelPointInContainer(new Point());
            }
            else { CenterRectInContainer(mapViewModel.PixelViewport); }
        }

        void DepictionWorldCanvasView_Loaded(object sender, RoutedEventArgs e)
        {
            //Connect things to keep track of when the parent size changes
            var container = Parent as FrameworkElement;
            if (container != null)
            {
                container.SizeChanged += container_SizeChanged;
            }
            var context = DataContext as WorldMapViewModel;
            if (context != null)
            {
                SetVisibleViewportFromModel(context);
            }

            var inputBindings = InputBindings;
            var commandBinding = CommandBindings;
            //A slight hack to make sure the wheel events are always handled when over the world
            //New type of slight hack since the parent is now a canvas which is inside the decorator

            var decorator = VisualTreeHelper.GetParent(Parent) as AdornerDecorator;
            if (decorator != null)
            {
                var parent = decorator.Parent as Panel;
                while (parent != null)
                {
                    if (parent.Name.Equals("DepictionMap"))
                    {
                        inputBindings = parent.InputBindings;
                        commandBinding = parent.CommandBindings;
                        break;
                    }
                    parent = parent.Parent as Panel;
                }
            }

            //This only happens once since the WorldView is never removed and readded to the MainAppWindow (so far)
            //I'd love to do this in XAML but for lack of knowledge doing it here now.
            var ib = new MouseBinding(NavigationCommands.DecreaseZoom, new MouseWheelGesture(MouseWheelAction.WheelUp));
            inputBindings.Add(ib);

            ib = new MouseBinding(NavigationCommands.DecreaseZoom, new MouseWheelGesture(MouseWheelAction.WheelUp, ModifierKeys.Control));
            inputBindings.Add(ib);

            ib = new MouseBinding(NavigationCommands.IncreaseZoom, new MouseWheelGesture(MouseWheelAction.WheelDown));
            inputBindings.Add(ib);

            ib = new MouseBinding(NavigationCommands.IncreaseZoom, new MouseWheelGesture(MouseWheelAction.WheelDown, ModifierKeys.Control));
            inputBindings.Add(ib);

            var zoomIn = new CommandBinding(NavigationCommands.IncreaseZoom, increaseZoomCommandExecuted);
            commandBinding.Add(zoomIn);

            var zoomOut = new CommandBinding(NavigationCommands.DecreaseZoom, decreaseZoomCommandExecuted);
            commandBinding.Add(zoomOut);
        }
        void DepictionWorldCanvasView_Unloaded(object sender, RoutedEventArgs e)
        {
            //Hmm i guess this will never really happen since it is embedded into the main window
            InputBindings.Clear();//I hope this only clears out the mouse wheel actions
            CommandBindings.Clear();
            var container = Parent as FrameworkElement;
            if (container != null)
            {
                container.SizeChanged -= container_SizeChanged;
            }
        }

        protected void WorldDragDelta(object sender, DragDeltaEventArgs e)
        {
            var context = DataContext as WorldMapViewModel;
            if (context != null)
            {
                var x = e.HorizontalChange * context.Scale;
                var y = e.VerticalChange * context.Scale;
                context.PanInDesiredDirection(PanDirection.Mouse, new Point(x, y));
            }
        }

        #endregion

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null || fe.DataContext == null) return; //|| !fe.DataContext.Equals(this)
            if (Dragger != null) ViewModelHelperMethods.AttachMouseToDragger(e, Dragger);
            base.OnMouseLeftButtonDown(e);
        }

        #region public methods

        public Rect CalculateVisibleViewportInUnScaledPixelsForMapViewModel( WorldMapViewModel mapViewModel)
        {
            if (mapViewModel == null) return Rect.Empty;
            if (Parent == null) return Rect.Empty;
            var container = Parent as FrameworkElement;
            if (container == null) return Rect.Empty;
            var containerPixelSize = new Size(container.ActualWidth, container.ActualHeight);

            var worldCanvasHolderRect = new Rect(0, 0, containerPixelSize.Width, containerPixelSize.Height);
            var northOffset = mapViewModel.WestNorthTranslate.Y;
            var westOffset = mapViewModel.WestNorthTranslate.X;
            var worldCanvasRect = new Rect(westOffset, northOffset, ViewModelConstants.MapSize, ViewModelConstants.MapSize); //the non transformed Rect of the world canvas

            //Rect transformWorldCanvasRect = mapViewModel.WorldScalePanTransformVM.TransformBounds(worldCanvasRect);
            Rect transformWorldCanvasRect = RenderTransform.TransformBounds(worldCanvasRect);
            var intersect = Rect.Intersect(worldCanvasHolderRect,transformWorldCanvasRect);
            if (intersect.IsEmpty) return Rect.Empty;
            if (mapViewModel.WorldScalePanTransformVM.Inverse == null) return Rect.Empty;
            var untransformedRect = mapViewModel.WorldScalePanTransformVM.Inverse.TransformBounds(intersect);
            return untransformedRect;
        }

        public Rect GetActualPixelViewportFromRequested(Rect requestedPixelRect, WorldMapViewModel mapViewModel)
        {//What does this do?
            if (requestedPixelRect.IsEmpty) return requestedPixelRect;
            CenterRectInContainer(requestedPixelRect);
            return CalculateVisibleViewportInUnScaledPixelsForMapViewModel(mapViewModel);
        }

        #endregion

        #region viewport changed event handlers

        void container_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            CenterPixelPointInContainer(new Point(double.NaN, double.NaN));
        }

        virtual protected void WorldTransformChanged(bool scaleChanged)
        {
            var container = Parent as FrameworkElement;
            if (container != null)
            {
                var context = DataContext as WorldMapViewModel;
                if(context == null) return;
                context.PixelViewport = CalculateVisibleViewportInUnScaledPixelsForMapViewModel(context);
            }
        }

        #endregion

        #region zoom events methods

        private void decreaseZoomCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var context = DataContext as WorldMapViewModel;
            if (context != null)
            {
                var zoomPoint = Mouse.GetPosition(this);
                if (Settings.Default.ZoomAboutCenter) zoomPoint = new Point(double.NaN, double.NaN);
                context.ZoomInDirectionAroundPoint(ZoomDirection.In,zoomPoint );
            }
        }

        private void increaseZoomCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var context = DataContext as WorldMapViewModel;
            if (context != null)
            {
                var zoomPoint = Mouse.GetPosition(this);
                if(Settings.Default.ZoomAboutCenter) zoomPoint = new Point(double.NaN,double.NaN);
                context.ZoomInDirectionAroundPoint(ZoomDirection.Out, zoomPoint);
            }
        }

        #endregion
        #region Center on point or rect command
        protected void CenterPixelPointInContainer(Point centerPoint)
        {
            var context = DataContext as WorldMapViewModel;
            if (context == null) return;
            var container = Parent as FrameworkElement;
            if (container == null) return;
            var size = new Size(container.ActualWidth, container.ActualHeight);
            CenterOnLocation(centerPoint, size);
        }

        protected void CenterRectInContainer(Rect rectToCenter)
        {
            var context = DataContext as WorldMapViewModel;
            if (context == null) return;
            var container = Parent as FrameworkElement;
            if (container == null) return;
            var size = new Size(container.ActualWidth, container.ActualHeight);
            FitPixelRectIntoBounds(rectToCenter, size);
        }

        public void FitPixelRectIntoBounds(Rect requestedRectToFit, Size boundToFit)
        {
            var wMult = boundToFit.Width / requestedRectToFit.Width;
            var hMult = boundToFit.Height / requestedRectToFit.Height;
            var mult = wMult > hMult ? wMult : hMult;
            if(requestedRectToFit.Width > requestedRectToFit.Height)
            {
                mult = wMult;
            }else mult = hMult;
            
            var context = DataContext as WorldMapViewModel;
            if (context == null) return;
            context.SetScale(mult);
            CenterOnLocation(new Point(requestedRectToFit.Left + requestedRectToFit.Width / 2, requestedRectToFit.Top + requestedRectToFit.Height / 2), boundToFit);
        }
        public void CenterOnLocation(Point inLocation, Size boundsToCenterIn)
        {
            var context = DataContext as WorldMapViewModel;
            if (context == null) return;
            var location = inLocation;
            if (inLocation.X.Equals(double.NaN)) location = context.CalcCenterFromViewport();

            var centerW = boundsToCenterIn.Width / 2d;
            var centerH = boundsToCenterIn.Height / 2d;
            var transform = context.WorldScalePanTransformVM;

            var windowToMapTrans = transform.Inverse;
            var center = windowToMapTrans.Transform(new Point(centerW, centerH));
            var x = location.X;
            var y = location.Y;
            var newCen = new Point(center.X - x, center.Y - y);
            var transCenter = transform.Transform(newCen);
            context.SetShift(transCenter.X, transCenter.Y);
        }
        #endregion
    }
}