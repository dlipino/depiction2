﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.Converters;
using Depiction.View.DepictionItemsControlViews;
using Depiction.View.Dialogs;
using Depiction.View.Resources.ThumbResources;
using Depiction.View.ViewHelpers;
using Depiction.View.ViewHelpers.Adorners;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.BaseViews
{
    //This really needs to be cleaned up, maybe for 1.4
    public class DepictionItemsControlViewBase : ItemsControl//, INotifyPropertyChanged
    {
        //The whole drag thing needs to be cleaned up a bit, that will come after things in MVVM land have more or less settled down
        private Thumb draggerMouseIsOver;
        private MouseButtonEventArgs draggerMouseIsOverMouseButtonArgs;
        private RevealerThumb dragPassThroughElement;
        private bool dragEventsAttached;

        #region Variables related to dragging elements

        protected Point PreDragClickPoint = new Point(double.NaN, double.NaN);
        protected bool IsDragging;
        protected AllElementDragAdorner allElementDragAdorner;
        protected Point offSetFromCenter;
        protected AdornerLayer adornerLayer;
        protected FrameworkElement adornerHolder;

        #endregion

        public DepictionItemsControlViewBase()
        {
            Unloaded += DepictionItemsControlViewBase_Unloaded;
            Loaded += DepictionItemsControlViewBase_Loaded;
            DataContextChanged += DepictionItemsControlViewBase_DataContextChanged;
        }

        #region Dep props

        public DepictionDisplayerType DisplayerType
        {
            get { return (DepictionDisplayerType)GetValue(DisplayerTypeProperty); }
            set { SetValue(DisplayerTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayerType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayerTypeProperty =
            DependencyProperty.Register("DisplayerType", typeof(DepictionDisplayerType), typeof(DepictionItemsControlViewBase),
            new UIPropertyMetadata(DepictionDisplayerType.None, DisplayerTypeChanged));

        public double WorldScale
        {
            get { return (double)GetValue(WorldScaleProperty); }
            set { SetValue(WorldScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldInverseScale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldScaleProperty =
            DependencyProperty.Register("WorldScale", typeof(double), typeof(DepictionItemsControlViewBase), new UIPropertyMetadata(1d, WorldScaleChanged));

        //What the heck is this for?! oh wait, this is to set the dragability of the items on the viewbase
        public bool AreItemsDraggable
        {
            get { return (bool)GetValue(AreItemsDraggableProperty); }
            set { SetValue(AreItemsDraggableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AreItemsDraggable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AreItemsDraggableProperty =
            DependencyProperty.Register("AreItemsDraggable", typeof(bool), typeof(DepictionItemsControlViewBase), new UIPropertyMetadata(false, ChangeItemDraggableStatus));

        #endregion
        #region virtuals
        virtual public void DoSomethingOnDisplayerTypeChange(DepictionDisplayerType oldType, DepictionDisplayerType newType)
        {
        }
        virtual public void DoStuffOnScaleChange(double oldScale, double newScale)
        {
            //            Console.WriteLine("Hopefully this was overwritten");
        }


        void DepictionItemsControlViewBase_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newValue = e.NewValue as IElementDisplayerViewModel;
            if (newValue != null) DoStuffOnScaleChange(WorldScale, WorldScale);
        }
        #endregion

        #region Dep props change events

        private static void DisplayerTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var itemsControl = d as DepictionItemsControlViewBase;
            if (itemsControl == null) return;
            itemsControl.DoSomethingOnDisplayerTypeChange((DepictionDisplayerType)e.OldValue, (DepictionDisplayerType)e.NewValue);
        }

        private static void WorldScaleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var itemsControl = d as DepictionItemsControlViewBase;
            if (itemsControl == null) return;
            itemsControl.DoStuffOnScaleChange((double)e.OldValue, (double)e.NewValue);
            MapZoneOfInfluenceViewModel.ViewModelScaleHack = (double)e.NewValue;//Real big hack

            if (itemsControl.DataContext is IElementRevealerDisplayerViewModel)
            {
                var revealerDC = (IElementRevealerDisplayerViewModel)itemsControl.DataContext;
                revealerDC.Scale = (double)e.NewValue;
            }
        }

        private static void ChangeItemDraggableStatus(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var itemsControl = d as DepictionItemsControlViewBase;
            if (itemsControl == null) return;
            var oldVal = (bool)e.OldValue;
            var newVal = (bool)e.NewValue;
            if (oldVal == newVal) return;
            if (newVal) itemsControl.AddMouseDragEvents();
            else itemsControl.RemoveMouseDragEvents();
        }

        #endregion

        #region load and unload events

        void DepictionItemsControlViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            var template = "DisplayerControlTemplate";
            var itemsPanel = "DisplayerPanelTemplate";
            if (AreItemsDraggable) AddMouseDragEvents();
            if (Application.Current != null)
            {
                var appDict = Application.Current.Resources;

                if (!string.IsNullOrEmpty(template) && appDict.Contains(template))
                    Template = appDict[template] as ControlTemplate;
                if (!string.IsNullOrEmpty(itemsPanel) && appDict.Contains(itemsPanel))
                    ItemsPanel = appDict[itemsPanel] as ItemsPanelTemplate;
            }
        }

        private void DepictionItemsControlViewBase_Unloaded(object sender, RoutedEventArgs e)
        {
            RemoveMouseDragEvents();
            Unloaded -= DepictionItemsControlViewBase_Unloaded;
        }
        public void RemoveMouseDragEvents()
        {
            PreviewMouseLeftButtonDown -= DepictionBaseItemsControl_PreviewMouseLeftButtonDown;
            PreviewMouseMove -= DepictionBaseItemsControl_MouseMove;
            dragEventsAttached = false;
        }
        public void AddMouseDragEvents()
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    return;
            }
            //#if IS_READER
            //            return;
            //#endif
            if (dragEventsAttached) return;
            PreviewMouseLeftButtonDown += DepictionBaseItemsControl_PreviewMouseLeftButtonDown;
            PreviewMouseMove += DepictionBaseItemsControl_MouseMove;
            dragEventsAttached = true;
        }
        #endregion
        #region Pre drag phace
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {//Used as a pass through for mouse left button down so that 
            //things that don't need to be dragged send the drag to the draggable world canvas.
            //Not sure what difference using Preview would do.
            bool cantDragElement = false;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    cantDragElement = true;
                    break;
                default:
                    if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                    {
                        e.Handled = false;
                        base.OnMouseLeftButtonDown(e);
                        return;
                    }
                    var src = e.OriginalSource as FrameworkElement;
                    if (src == null) return;

                    //Temp hack for element node dragging
                    var elementWaypoint = src.DataContext as IDepictionElementWaypoint;
                    if (elementWaypoint != null)
                    {
                        var tag = src.Tag as IMapDraggable;
                        if (tag != null)
                        {
                            cantDragElement = !tag.IsElementIconDraggable;
                        }
                    }//end temp hack
                    else
                    {

                        var elementToDrag = src.DataContext as IMapDraggable;
                        if (elementToDrag == null) return; //not sure when this would happen

                        cantDragElement = !elementToDrag.IsElementZOIDraggable;

                        if (src is Shape && elementToDrag is MapElementViewModel &&
                            !((MapElementViewModel)elementToDrag).IsZOIEditable)
                        {
                            cantDragElement = true;
                        }
                    }
                    break;
            }
            //#if IS_READER
            //            cantDragElement = true;
            //#else
            //            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            //            {
            //                e.Handled = false;
            //                base.OnMouseLeftButtonDown(e);
            //                return;
            //            }
            //            var src = e.OriginalSource as FrameworkElement;
            //            if (src == null) return;
            //        
            //            //Temp hack for element node dragging
            //            var elementWaypoint = src.DataContext as IDepictionElementWaypoint;
            //            if (elementWaypoint != null)
            //            {
            //                var tag = src.Tag as IMapDraggable;
            //                if(tag != null)
            //                {
            //                    cantDragElement = !tag.IsElementIconDraggable;
            //                }
            //            }//end temp hack
            //            else
            //            {
            //                
            //                var elementToDrag = src.DataContext as IMapDraggable;
            //                if (elementToDrag == null) return; //not sure when this would happen
            //
            //                cantDragElement = !elementToDrag.IsElementZOIDraggable;
            //
            //                if (src is Shape && elementToDrag is MapElementViewModel &&
            //                    !((MapElementViewModel) elementToDrag).IsZOIEditable)
            //                {
            //                    cantDragElement = true;
            //                }
            //            }
            //#endif
            if (cantDragElement)
            {//This finds revealers/background canvas
                var parent = Parent as WorldCanvasViewBase;
                if (parent != null)
                {
                    var mainDC = parent.DataContext as DepictionMapAndMenuViewModel;
                    if (mainDC != null && !mainDC.WorldCanvasModeVM.Equals(DepictionCanvasMode.Normal))
                    {
                        base.OnMouseLeftButtonDown(e);
                        return;
                    }
                    draggerMouseIsOver = parent.Dragger;
                    VisualTreeHelper.HitTest(parent, null, FindTopLevelDraggerCallback, new PointHitTestParameters(e.GetPosition(this)));
                    if (dragPassThroughElement != null) draggerMouseIsOver = dragPassThroughElement;


                    //So we are over an element that can't be dragged, but because we might be in the middle 
                    //of a double click do not give focus to the dragger until the move event. So we story 
                    //the mouse event for future usage.
                    if (draggerMouseIsOver != null)
                    {
                        draggerMouseIsOverMouseButtonArgs = e;
                    }
                }
                dragPassThroughElement = null;
                e.Handled = true;
            }
            else
            {
                draggerMouseIsOverMouseButtonArgs = null;
                e.Handled = true;
            }
            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (draggerMouseIsOver != null && draggerMouseIsOverMouseButtonArgs != null && e.LeftButton == MouseButtonState.Pressed)
            {//Wow this is a hackity hack hack hack, somebody please find a better way of passing control to
                //a thumb dragger
                ViewModelHelperMethods.AttachMouseToDragger(draggerMouseIsOverMouseButtonArgs, draggerMouseIsOver);
                draggerMouseIsOverMouseButtonArgs = null;
            }
            base.OnMouseMove(e);
        }

        private void DepictionBaseItemsControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //Save the click position for checkign the displacement before a drag
            var parent = Parent as Panel;
            PreDragClickPoint = e.GetPosition(parent);
        }

        private void DepictionBaseItemsControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && !IsDragging && !PreDragClickPoint.X.Equals(double.NaN) && Mouse.Captured == null)
            {
                var owner = sender as FrameworkElement;
                var parent = Parent as Panel;
                if (parent != null) owner = parent;
                Point position = e.GetPosition(owner);
                var scale = 1d;
                //Used to make sure the proper no scaled drag distance is found
                if (parent != null && parent.RenderTransform != null)
                    scale = parent.RenderTransform.Value.M11;
                var difX = Math.Abs(position.X - PreDragClickPoint.X) * scale;
                var difY = Math.Abs(position.Y - PreDragClickPoint.Y) * scale;

                //The min drag distance is 4, but it is too large, for small items the drag is too difficult
                if (difX > SystemParameters.MinimumHorizontalDragDistance / 4d ||
                    difY > SystemParameters.MinimumVerticalDragDistance / 4d)
                {//Hackish?
                    if (draggerMouseIsOverMouseButtonArgs == null)
                    {
                        //Hmmmm something fish here
                        StartDrag(e);
                    }
                }
            }
            //If you are moving and not clicked reset the last click point
            //this seems to have fixed the jumping on my comp while connected remotely.
            if (e.LeftButton.Equals(MouseButtonState.Released))
            {
                if (PreDragClickPoint.X != double.NaN) PreDragClickPoint = new Point(double.NaN, double.NaN);
            }
        }

        #endregion

        private void StartDrag(MouseEventArgs e)
        {
            Debug.WriteLine("Starting the elemtn drag");
            //Setup
            var src = e.OriginalSource as FrameworkElement;
            if (src == null) return;

            var elementWaypoint = src.DataContext as IDepictionElementWaypoint;
            if (elementWaypoint != null)
            {
                DragElementNode(elementWaypoint, e, src);
                return;
            }
            var zoiEditChecker = src.DataContext as MapElementViewModel;
            if (zoiEditChecker != null && zoiEditChecker.IsElementInEditMode)
            {
                zoiEditChecker.ZOI.UpdateModelFromViewModel();
            }

            var dragElement = src.DataContext as IMapDraggable;
            if (dragElement == null) return;

            if (!dragElement.IsElementZOIDraggable) return;

            var dragScope = Parent as WorldCanvasViewBase;
            if (dragScope == null) return;
            dragScope.Dragger.Focus();
            adornerHolder = Parent as Panel; //Base scale depends one which is used

            ImageSource source = dragElement.IconSource;

            var startDragPosition = e.GetPosition(dragScope);
            offSetFromCenter = new Point();
            dragScope.AllowDrop = true;
            dragScope.Drop += dragScope_Drop;

            //The '-' is arbitrary
            var difX = startDragPosition.X - dragElement.ElementImagePosition.X;
            var difY = startDragPosition.Y - dragElement.ElementImagePosition.Y;
            offSetFromCenter = new Point(difX, difY);//new Point();//

            Dictionary<UIElement, Rect> contElement = new Dictionary<UIElement, Rect>();
            if (this is AnnotationItemsControl)
            {
                var aControl = (AnnotationItemsControl)this;
                contElement = aControl.GetVisualsForElement(dragElement);
            }
            else
            {
                var elementDisplayer = Tag as BaseElementDisplayerView;
                if (elementDisplayer != null) contElement = elementDisplayer.GetVisualsForElement(dragElement);
            }
            //Use the scaled version of the parent so that the ZOI always matches the viewable scale
            allElementDragAdorner = new AllElementDragAdorner(adornerHolder, source, contElement, 0.5, offSetFromCenter);

            adornerLayer = AdornerLayer.GetAdornerLayer(adornerHolder);
            adornerLayer.Add(allElementDragAdorner);

            IsDragging = true;
            GiveFeedbackEventHandler handler = dragScope_GiveFeedback;
            dragScope.GiveFeedback += handler;
            // The DragOver event ... 
            DragEventHandler draghandler = dragScope_DragOver;
            dragScope.PreviewDragOver += draghandler;
            //Might be changable to element model?

            var data = new DataObject(src.DataContext);
            if (this is AnnotationItemsControl)
            {
                data = new DataObject(typeof(IMapDraggable), src.DataContext);
            }
            if (zoiEditChecker == null && !(this is AnnotationItemsControl)) data = new DataObject(dragElement);
            try
            {
                DragDrop.DoDragDrop(this, data, DragDropEffects.All);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {

                //Clean up, I actually don't know how this works
                dragScope.AllowDrop = false;
                AdornerLayer.GetAdornerLayer(adornerHolder).Remove(allElementDragAdorner);
                dragScope.GiveFeedback -= handler;
                dragScope.PreviewDragOver -= draghandler;
                dragScope.Drop -= dragScope_Drop;
                IsDragging = false;
            }
        }

        protected void DragElementNode(IDepictionElementWaypoint dragElementWaypoint, MouseEventArgs e, UIElement uiElement)
        {
            var dragScope = Parent as WorldCanvasViewBase;
            if (dragScope == null) return;
            adornerHolder = Parent as Panel; //Base scale depends one which is used
            if (adornerHolder == null) return;
            dragScope.Dragger.Focus();


            var source = (ImageSource)DepictionConverters.DepictionIconPathToImageSource.Convert(dragElementWaypoint.IconPath, null, null, null);
            var startDragPosition = e.GetPosition(dragScope);
            offSetFromCenter = new Point();
            dragScope.AllowDrop = true;
            dragScope.Drop += dragScope_Drop;
            //The '-' is arbitrary
            var difX = (dragElementWaypoint.IconSize / 2) / WorldScale;
            var difY = (dragElementWaypoint.IconSize / 2) / WorldScale;
            offSetFromCenter = new Point(difX, difY);//new Point();//

            var contElement = new Dictionary<UIElement, Rect>();
            contElement.Add(uiElement, new Rect(new Point(), uiElement.RenderSize));
            //Use the scaled version of the parent so that the ZOI always matches the viewable scale
            allElementDragAdorner = new AllElementDragAdorner(adornerHolder, source, contElement, 0.5, offSetFromCenter);

            adornerLayer = AdornerLayer.GetAdornerLayer(adornerHolder);
            adornerLayer.Add(allElementDragAdorner);

            IsDragging = true;
            GiveFeedbackEventHandler handler = dragScope_GiveFeedback;
            dragScope.GiveFeedback += handler;
            // The DragOver event ... 
            DragEventHandler draghandler = dragScope_DragOver;
            dragScope.PreviewDragOver += draghandler;

            var data = new DataObject(typeof(IDepictionElementWaypoint), dragElementWaypoint);
            DragDrop.DoDragDrop(this, data, DragDropEffects.All);

            //Clean up, I actually don't know how this works
            dragScope.AllowDrop = false;
            AdornerLayer.GetAdornerLayer(adornerHolder).Remove(allElementDragAdorner);
            dragScope.GiveFeedback -= handler;
            dragScope.PreviewDragOver -= draghandler;
            dragScope.Drop -= dragScope_Drop;
            IsDragging = false;
        }

        #region Drag events

        private void dragScope_DragOver(object sender, DragEventArgs e)
        {
            //            if (UIHelperMethods.MouseTracker != null) UIHelperMethods.MouseTracker.UpdatePosition(e);
            if (allElementDragAdorner != null && adornerHolder != null)
            {
                var scale = 1d;
                if (adornerHolder.RenderTransform != null)
                    scale = adornerHolder.RenderTransform.Value.M11;
                //Adjust for any possible scaling the adorner holder has.

                allElementDragAdorner.LeftOffset = e.GetPosition(adornerHolder).X * scale;
                allElementDragAdorner.TopOffset = e.GetPosition(adornerHolder).Y * scale;
            }
            //Hack to get location updates while dragging
            var main = Application.Current.MainWindow;
            var dc = main.DataContext as DepictionAppViewModel;

            if (dc != null && dc.MapViewModel != null)//&& !(e.Source is IDepictionController)
            {
                dc.MapViewModel.CurrentMainWindowPoint = e.GetPosition(main);
            }
            //Endhack
            // e.Handled = true; Keeping this adds a + to the drag cursor
        }

        private void dragScope_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            e.UseDefaultCursors = false;

            if (e.Effects.Equals(DragDropEffects.None))
            {
                if (allElementDragAdorner.Visibility != Visibility.Hidden)
                {
                    allElementDragAdorner.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                if (allElementDragAdorner.Visibility != Visibility.Visible)
                {
                    allElementDragAdorner.Visibility = Visibility.Visible;
                }
            }
        }

        void dragScope_Drop(object sender, DragEventArgs e)
        {
            var data = e.Data;
            var parent = sender as FrameworkElement;
            if (parent == null) return;

            var waypoint = data.GetData(typeof(IDepictionElementWaypoint)) as IDepictionElementWaypoint;
            if (waypoint != null)
            {
                var tLoc = e.GetPosition(this);
                waypoint.Location = DepictionConverters.LatLongToMapPixelCoordinates.ConvertBack(tLoc, null, null, null) as LatitudeLongitude;

                return;
            }

            var rawData = data.GetData(typeof(MapElementViewModel));
            if (rawData == null)
            {
                rawData = data.GetData(typeof(IMapDraggable));
            }
            var elem = rawData as IMapDraggable;
            if (elem != null)
            {
                var tLoc = e.GetPosition(this);
                //The - is fairly arbitrary since there was a 50/50 chanve of getting it right

                var newPosition = new Point(tLoc.X - offSetFromCenter.X, tLoc.Y - offSetFromCenter.Y);
                elem.SetElementPosition(newPosition);
            }
        }
        #endregion

        #region helper mehtods
        private HitTestResultBehavior FindTopLevelDraggerCallback(HitTestResult result)
        {
            var fe = result.VisualHit as FrameworkElement;
            if (fe == null) return HitTestResultBehavior.Continue;
            if (fe.TemplatedParent is RevealerThumb)
            {
                var toDrag = (RevealerThumb)(fe.TemplatedParent);
                if (toDrag.IsHitTestVisible)
                {
                    dragPassThroughElement = toDrag;
                    return HitTestResultBehavior.Stop;
                }
            }
            return HitTestResultBehavior.Continue;
        }
        #endregion

    }
}
