﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.View.TileWidget
{
    public class TileImageTypeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TileImageTypes)
            {
                var requiredMode = parameter.ToString();
                var currentMode = ((TileImageTypes)value).ToString().ToLower();
                if (requiredMode.ToLower().Equals(currentMode))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {

                var selected = (bool)value;
                if (selected) return (TileImageTypes)Enum.Parse(typeof(TileImageTypes), parameter.ToString());
            }
            return TileImageTypes.Unknown;
        }
    }
}