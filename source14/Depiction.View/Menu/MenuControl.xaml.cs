using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.AddElements;
using Depiction.View.Dialogs.DialogBases;
using Depiction.View.Dialogs.MainWindowDialogs;
using Depiction.View.Dialogs.Properties;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Menu
{
    public partial class MenuControl
    {
        protected readonly AddContentDialog addContentDialog;
        protected readonly DisplayContentDialog displayContentDialog;
        protected readonly ManageContentDialog manageContentDialog;
        protected readonly QuickstartDataDialog quickStartDataDialog;

        protected readonly AddinDisplayerDialog addinDisplayDialog;//This one should not be here

        protected readonly TipControlView depictionTipControlDialog;
        public MenuControl()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                case ProductInformationBase.DepictionRW:
                    wrapPanel.Children.Remove(cbToolsTab);
                    wrapPanel.Children.Remove(cbManageTab);
                    wrapPanel.Children.Remove(cbDisplayTab);
                    break;
                case ProductInformationBase.Reader:
                    cbAddTab.Visibility = Visibility.Collapsed;
                    cbManageTab.Visibility = Visibility.Collapsed;
                    cbDisplayTab.Visibility = Visibility.Collapsed;
                    cbAdvancedTab.Visibility = Visibility.Collapsed;
//                    wrapPanel.Children.Remove(cbAddTab);
//                    wrapPanel.Children.Remove(cbManageTab);
//                    wrapPanel.Children.Remove(cbDisplayTab);
//                    wrapPanel.Children.Remove(cbAdvancedTab);
                    break;
                default:
                    //                    wrapPanel.Children.Remove(cbAdvancedTab);
                    cbAdvancedTab.Visibility = Visibility.Collapsed;
                    break;
            }
            quickStartDataDialog = new QuickstartDataDialog();

            displayContentDialog = new DisplayContentDialog();
            displayContentDialog.InitialLocation = new Point(100, 80);
            manageContentDialog = new ManageContentDialog();
            manageContentDialog.InitialLocation = new Point(100, 100);

            addContentDialog = new AddContentDialog();
            addContentDialog.InitialLocation = new Point(50, 80);
            depictionTipControlDialog = new TipControlView();

            addinDisplayDialog = new AddinDisplayerDialog();

            DataContextChanged +=MenuControl_DataContextChanged;
            Loaded += MenuControl_Loaded;
        }

        void MenuControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                addContentDialog.Owner = Application.Current.MainWindow;
                displayContentDialog.Owner = Application.Current.MainWindow;
                manageContentDialog.Owner = Application.Current.MainWindow;
                depictionTipControlDialog.Owner = Application.Current.MainWindow;
                quickStartDataDialog.Owner = Application.Current.MainWindow;

                addinDisplayDialog.Owner = Application.Current.MainWindow;
            }
        }

        private void MenuControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDC = e.OldValue as DepictionMapAndMenuViewModel;
            var newDC = e.NewValue as DepictionMapAndMenuViewModel;
            if (oldDC != null)
            {
                oldDC.DisplayContentDialogViewModel.IsDialogVisible = false;
                oldDC.ManageContentDialogViewModel.IsDialogVisible = false;
                oldDC.AddContentDialogViewModel.IsDialogVisible = false;
                //Quick start has to be before tipcontrol since closing quickstart will open tip control
                //yes it is ugly.
                oldDC.QuickStartDialogViewModel.IsDialogVisible = false;
                oldDC.TipControlDialogViewModel.IsDialogVisible = false;
                oldDC.AddinDisplayerDialogViewModel.IsDialogVisible = false;

                oldDC.RequestShowElementProperty -= elementInfoShower_RequestShowElementProperty;
                oldDC.RequestShowAnnotationProperty -= annotationInfoShower_RequestShowAnnotationProperty;

                quickStartDataDialog.DataContext = null;
                quickStartDataDialog.Tag = null;

                displayContentDialog.DataContext = null;
                displayContentDialog.Tag = null;

                manageContentDialog.DataContext = null;
                manageContentDialog.Tag = null;

                addContentDialog.DataContext = null;
                addContentDialog.Tag = null;

                addinDisplayDialog.DataContext = null;
                addinDisplayDialog.Tag = null;

                depictionTipControlDialog.DataContext = null;
                depictionTipControlDialog.Tag = null;
                CloseAllItemCreatedDialogs();
            }
            if (newDC != null)
            {
                newDC.RequestShowElementProperty += elementInfoShower_RequestShowElementProperty;
                newDC.RequestShowAnnotationProperty += annotationInfoShower_RequestShowAnnotationProperty;

                quickStartDataDialog.DataContext = newDC.QuickStartDialogViewModel;

                displayContentDialog.DataContext = newDC.DisplayContentDialogViewModel;
                displayContentDialog.Tag = newDC;

                manageContentDialog.DataContext = newDC.ManageContentDialogViewModel;
                manageContentDialog.Tag = newDC;

                addContentDialog.DataContext = newDC.AddContentDialogViewModel;
                addContentDialog.Tag = newDC;

                addinDisplayDialog.DataContext = newDC.AddinDisplayerDialogViewModel;
                addinDisplayDialog.Tag = newDC;


                depictionTipControlDialog.DataContext = newDC.TipControlDialogViewModel;
                //depictionTipControlDialog.Tag = newDC;
            }
        }

        bool annotationInfoShower_RequestShowAnnotationProperty(MapAnnotationViewModel annotationVM)
        {
            if (!annotationVM.EditingProperties)
            {
                var propertiesDialog = new AnnotationEditingDialog();
                //                var converter = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel;
                //                propertiesDialog.StartPosition = converter.WorldToGeoCanvas(annotationVM.annotationModel.MapLocation);
                propertiesDialog.DataContext = annotationVM;
                propertiesDialog.Tag = DataContext; //Hack, more or less, to connect the main map data context
                propertiesDialog.ShowDialog();
            }
            return true;
        }
        //Not completely MVVM since there is no way to test this without the visuals around
        void elementInfoShower_RequestShowElementProperty(ElementPropertyDialogContentVM elementsToShowInfoFor)
        {
            var defaultTop = 60;
            var defaultLeft = 30;

            var newDialogDataContextID = elementsToShowInfoFor.ContentKey;
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null) return;

            var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();

            foreach (var control in sameTypeList)
            {
                var controlDC = control.DataContext as ElementPropertyDialogContentVM;
                if (controlDC != null)
                {
                    var key = controlDC.ContentKey;
                    if (!string.IsNullOrEmpty(key) && key.Equals(newDialogDataContextID))
                    {
                        if (!control.IsVisible)
                        {
                            control.Show();
                        }
                        control.Activate();
                        //control.DoFocusVisualStoryboard();

                        return;
                    }
                }
                if (control.Left.Equals(defaultLeft) && control.Top.Equals(defaultLeft))
                {
                    defaultLeft += 20;
                    defaultTop += 20;
                }
            }
            var propertiesDialog = new ElementPropertiesDialog();
            propertiesDialog.DataContext = elementsToShowInfoFor;
            //propertiesDialog.Content = elementsToShowInfoFor;
            propertiesDialog.Tag = DataContext;//Hack, more or less
            propertiesDialog.Top = defaultTop;
            propertiesDialog.Left = defaultLeft;
            propertiesDialog.Show();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null) return;

            Settings.Default.Reset();
            foreach (var childWindow in mainWindow.OwnedWindows)
            {
                var depictionWindow = childWindow as DepictionDialogWindow;
                if (depictionWindow == null) continue;
                depictionWindow.SetLocationToStoredValue();
            }
        }

        private void CloseAllItemCreatedDialogs()
        {
            //            var propertiesControls = DialogCanvas.Children.OfType<ElementPropertiesDialogControlFrame>().ToList();
            //            foreach (var propControl in propertiesControls)
            //            {
            //                propControl.IsShown = false;
            //            }
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow != null)
            {
                var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();

                foreach (var control in sameTypeList)
                {
                    control.Close();
                }
            }

//            var annotationControls = DialogCanvas.Children.OfType<AnnotationEditingDialog>().ToList();
//            foreach (var annotControl in annotationControls)
//            {
//                annotControl.Close();//.IsShown = false;
//            }
        }
    }
}