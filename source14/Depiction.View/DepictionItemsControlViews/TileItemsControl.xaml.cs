﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Depiction.ViewModels.ViewModels.TileViewModels;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for TileItemsControl.xaml
    /// </summary>
    public partial class TileItemsControl
    {
        #region Dep props
        public Rect WorldViewportRectPixels
        {
            get { return (Rect)GetValue(WorldViewportRectPixelsProperty); }
            set { SetValue(WorldViewportRectPixelsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldViewportRectPixels.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldViewportRectPixelsProperty =
            DependencyProperty.Register("WorldViewportRectPixels", typeof(Rect), typeof(TileItemsControl), new UIPropertyMetadata(Rect.Empty,ViewportPixelChange));

        #endregion

        #region events for dep props
        private static void ViewportPixelChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }
        #endregion

        #region Constructor

        public TileItemsControl()
        {
            InitializeComponent();
            IsHitTestVisible = false;
            Focusable = false;
        }

        #endregion

        private static void Animate(DependencyObject target, string property, double from, double to, int duration)
        {
            var animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new TimeSpan(0, 0, 0, 0, duration);
            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, new PropertyPath(property));

            var storyBoard = new Storyboard();
            storyBoard.Children.Add(animation);
            //storyBoard.Completed += completed;
            storyBoard.Begin();
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            var image = sender as Image;
            if (image == null) return;
            image.Opacity = 0;
            Animate(image, "Opacity", 0, 1, 600);
            var tileViewModel = image.DataContext as TileViewModel;
            if (tileViewModel == null) return;
            tileViewModel.ImageOpened = true;
        }
    }
}