﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for PermaTextItemsControlView.xaml
    /// </summary>
    public partial class PermaTextItemsControlView
    {
        public PermaTextItemsControlView()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    break;
                default:
                    MouseDoubleClick += PermaTextItemsControlView_MouseDoubleClick;
                    break;
            }
//#if !IS_READER
//            MouseDoubleClick += PermaTextItemsControlView_MouseDoubleClick;
//#endif
        }

        void PermaTextItemsControlView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var parent = Parent as FrameworkElement;
            if (parent == null) return;
            var parentDC = parent.DataContext as DepictionMapAndMenuViewModel;
            if (parentDC == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;

            if (senderDC == null)
            {
                senderDC = fe.Tag as MapElementViewModel;
                if (senderDC == null) return;
            }
            parentDC.ShowElementPropertyInfoCommand.Execute(senderDC);
        }

        public override void DoSomethingOnDisplayerTypeChange(DepictionDisplayerType oldType, DepictionDisplayerType newType)
        {
            var dataContext = DataContext as IElementDisplayerViewModel;
            if (dataContext == null) return;
            switch (newType)
            {
                case DepictionDisplayerType.MainMap:
                    Panel.SetZIndex(this, ViewModelZIndexs.BackdropLabelDisplayerZ);
                    break;
                case DepictionDisplayerType.Revealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.RevealerLabelDisplayerZ);
                    break;
                case DepictionDisplayerType.TopRevealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.TopRevealerLabelDisplayerZ);
                    break;
            }
        }

        private void PermaText_Drag(object sender, DragDeltaEventArgs e)
        {
            var t = sender as Thumb;
            if (t == null) return;
            var dc = t.DataContext as MapElementViewModel;
            if (dc == null) return;
            dc.PermaTextX += e.HorizontalChange;
            dc.PermaTextY += e.VerticalChange;
            var x = dc.PermaTextX;
            var y = dc.PermaTextY;
            dc.PermaTextCenterX = x + t.ActualWidth / 2;
            dc.PermaTextCenterY = y + t.ActualHeight / 2;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var cp = sender as ContentControl;
            if (cp == null) return;

            var c = cp.DataContext as MapElementViewModel;
            if (c == null) return;
            c.UsePermaText = false;
        }
        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)//Resharper LIES!!!!
        {
            Process.Start(e.Uri.ToString());

        }

        private void labelElement_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var fe = sender as FrameworkElement;
            if (fe == null) return;
            var dc = fe.DataContext as MapElementViewModel;
            if (dc == null) return;
            var x = dc.PermaTextX;
            var y = dc.PermaTextY;
            dc.PermaTextCenterX = x + fe.ActualWidth / 2;
            dc.PermaTextCenterY = y + fe.ActualHeight / 2;
        }
    }
    public class PermaTextViewTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return (DataTemplate)((FrameworkElement)container).FindResource("EnhancedPermaTextTemplate");
            return (DataTemplate)((FrameworkElement)container).FindResource("SimplePermaTextTemplate");
            //                var vm = item as 
            //                if (item is CollectionViewGroup)
            //                    return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("PropertyChangingTreeTemplate");
            //                if (item is MenuElementPropertyViewModel)
            //                {
            //                    return (DataTemplate)((FrameworkElement)container).FindResource("PropertyDataTemplate");
            //                }
            return base.SelectTemplate(item, container);
        }
    }
}
