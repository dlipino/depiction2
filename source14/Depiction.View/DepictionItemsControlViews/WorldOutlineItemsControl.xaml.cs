﻿using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for WorldOutlineItemsControl.xaml
    /// </summary>
    public partial class WorldOutlineItemsControl
    {
        double magicNoShowScale = .6 / ViewModelConstants.MapSizeMulti;
        public WorldOutlineItemsControl()
        {
            InitializeComponent();
        }
        
        public override void DoStuffOnScaleChange(double oldScale, double newScale)
        {
            if (newScale == 0 || newScale > magicNoShowScale)
            {
                if(IsVisible) Visibility = System.Windows.Visibility.Collapsed;
            }else
            {
                if(!IsVisible) Visibility = System.Windows.Visibility.Visible;
                else
                {
                    foreach(DepictionGeometryViewModel shape in Items)
                    {
                        shape.StrokeSize = 1d/newScale;
                    }
                }
            }
        }
    }
}