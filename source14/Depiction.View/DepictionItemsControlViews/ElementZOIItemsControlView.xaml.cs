﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.View.ViewHelpers.Adorners;
using Depiction.View.ViewMouseEventHelpers;
using Depiction.View.WorldMapViews;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for ElementZOIItemsControlView.xaml
    /// </summary>
    public partial class ElementZOIItemsControlView
    {
        private MouseClickOrDoubleClickArbiter clickArbiter = null;
        private int doubleClickLagCheckTimeMS = 300;
        private ZOIEditingAdorner zoiEditAdorner;


        public ElementZOIItemsControlView()
        {
            InitializeComponent();

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    break;
                default:
                    PreviewMouseLeftButtonUp += ClickArb;
                    PreviewMouseLeftButtonDown += ElementZOIItemsControlViewArbiter_MouseLeftButtonDown;

                    clickArbiter = new MouseClickOrDoubleClickArbiter(this, doubleClickLagCheckTimeMS);
                    clickArbiter.DoubleClick += ElementZOIItemsControlViewArbiter_PreviewMouseDoubleClick;
                    clickArbiter.Click += ElementZOIItemsControlViewArbiter_MouseLeftButtonUp;
                    break;
            }
            //#if !IS_READER
            //            PreviewMouseLeftButtonUp += ClickArb;
            //            PreviewMouseLeftButtonDown += ElementZOIItemsControlViewArbiter_MouseLeftButtonDown;
            //
            //            clickArbiter = new MouseClickOrDoubleClickArbiter(this, doubleClickLagCheckTimeMS);
            //            clickArbiter.DoubleClick += ElementZOIItemsControlViewArbiter_PreviewMouseDoubleClick;
            //            clickArbiter.Click += ElementZOIItemsControlViewArbiter_MouseLeftButtonUp;
            //#endif
        }

        #region Click arbitration region

        private void ClickArb(object sender, MouseButtonEventArgs e)
        {
            if (clickArbiter != null) clickArbiter.HandleClick(sender, e);
        }

        #endregion

        #region Mouse click/button pressed event handling
        private void ElementZOIItemsControlViewArbiter_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (!dc.IsZOIEditable)
                {
                    return;
                }
                EnableZOIEditMode(vh, dc);
                e.Handled = true;
            }
        }
        void ElementZOIItemsControlViewArbiter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState.Equals(MouseButtonState.Released))
            {
                var parent = Parent as DepictionWorldCanvasView;
                if (parent == null) return;
                if (!parent.WorldCanvasMode.Equals(DepictionCanvasMode.Normal)) return;
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (!dc.IsZOIEditable)
                {
                    return;
                }
                EnableZOIEditMode(vh, dc);
            }
        }

        void ElementZOIItemsControlViewArbiter_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var parent = Parent as FrameworkElement;
            if (parent == null) return;
            var parentDC = parent.DataContext as DepictionMapAndMenuViewModel;
            if (parentDC == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;
            if (senderDC == null) return;
            if (senderDC.UseEnhancedPermaText) senderDC.UsePermaText = true;
            else parentDC.ShowElementPropertyInfoCommand.Execute(senderDC);
            e.Handled = true;
        }

        #endregion

        private void EnableZOIEditMode(FrameworkElement visual, MapElementViewModel viewModel)
        {
            var fe = visual;
            if (fe == null || viewModel == null) return;
            //This part is ugly, but i can't think of a good way to give the ElementViewModels the commands they need
            //This is better than the image one
            var parent = Parent as FrameworkElement;
            if (parent == null) return;
            var pDC = parent.DataContext as DepictionEnhancedMapViewModel;
            if (pDC == null) return;
            if (pDC.CurrentEditElement != null) return;
            pDC.StartZOIEditCommand.Execute(viewModel);
            var myAdornerLayer = AdornerLayer.GetAdornerLayer(fe);
            zoiEditAdorner = new ZOIEditingAdorner(fe, 1 / WorldScale);
            myAdornerLayer.Add(zoiEditAdorner);
            //Order is important, because of propertychanging on view from the startzoieditcommand
            //Beware of double attaching
            viewModel.PropertyChanged += viewModel_PropertyChanged;
            ConnectToMainWindow();
        }

        private bool connectedToMainWindow = false;
        void ConnectToMainWindow()
        {
            if (!connectedToMainWindow && Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.PreviewKeyDown += MainWindow_PreviewKeyEvent;
                //Application.Current.MainWindow.PreviewMouseRightButtonDown += MainWindow_PreviewMouseRightButtonDown;
                connectedToMainWindow = true;
            }
        }

        void DisconnectFromMainWindow()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.PreviewKeyDown -= MainWindow_PreviewKeyEvent;
                //Application.Current.MainWindow.PreviewMouseRightButtonDown -= MainWindow_PreviewMouseRightButtonDown;
                connectedToMainWindow = false;
            }
        }

        private void MainWindow_PreviewKeyEvent(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Escape) && zoiEditAdorner != null)
            {
                var fe = zoiEditAdorner.AdornedElement as FrameworkElement;
                if (fe == null) return;
                var dc = fe.DataContext as MapElementViewModel;
                if (dc == null) return;
                DisableZOIEditMode(dc);
                e.Handled = true;
            }
            SetCursor(sender as FrameworkElement);
        }

        void viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var elementVM = sender as MapElementViewModel;
            if (elementVM == null) return;
            //This should only be connected when the element is in edit mode
            if (e.PropertyName.Equals("IsElementInEditMode"))
            {
                if (!elementVM.IsElementInEditMode)
                {
                    // elementVM.PropertyChanged -= viewModel_PropertyChanged;
                    DisableZOIEditMode(elementVM);
                }
                //Just in case
                // elementVM.PropertyChanged -= viewModel_PropertyChanged;
            }
            else if (e.PropertyName.Equals("MapPixelLocation"))
            {//The element was dragged while in
                if (zoiEditAdorner != null)
                {
                    zoiEditAdorner.UpdateDrawnAdorner(elementVM);
                }
            }
        }
        private void DisableZOIEditMode(MapElementViewModel viewModel)
        {
            if (zoiEditAdorner != null)
            {
                zoiEditAdorner.RemoveZOIAdorner();
                if (viewModel == null)
                {
                    var fe = zoiEditAdorner.AdornedElement as FrameworkElement;
                    if (fe == null) return;
                    var dc = fe.DataContext as MapElementViewModel;
                    if (dc != null)
                    {
                        dc.PropertyChanged -= viewModel_PropertyChanged;
                    }
                }
            }

            zoiEditAdorner = null;
            if (viewModel != null)
            {
                viewModel.PropertyChanged -= viewModel_PropertyChanged;
            }
            DisconnectFromMainWindow();

            var parent = Parent as FrameworkElement;

            if (parent == null) return;
            var pDC = parent.DataContext as DepictionEnhancedMapViewModel;
            if (pDC == null) return;

            pDC.EndZOIEditCommand.Execute(viewModel);

            GC.Collect();
        }
        #region Overrides
        public override void DoSomethingOnDisplayerTypeChange(DepictionDisplayerType oldType, DepictionDisplayerType newType)
        {
            var dataContext = DataContext as IElementDisplayerViewModel;
            if (dataContext == null) return;
            switch (newType)
            {
                case DepictionDisplayerType.MainMap:
                    Panel.SetZIndex(this, ViewModelZIndexs.BackdropZOIDisplayerZ);
                    break;
                case DepictionDisplayerType.Revealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.RevealerZOIDisplayerZ);
                    break;
                case DepictionDisplayerType.TopRevealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.TopRevealerZOIDisplayerZ);
                    break;
            }
        }
        public override void DoStuffOnScaleChange(double oldScale, double newScale)
        {
            if (zoiEditAdorner != null) zoiEditAdorner.AdjustAdornerScale(1 / newScale);

            var dataContext = DataContext as IElementDisplayerViewModel;
            if (dataContext == null) return;
            if (dataContext.ViewModelManager != null)
            {
                foreach (var elementVM in dataContext.ViewModelManager.ZOIElements)
                {
                    //                    if (elementVM.ZOI.ZOIGeometryType.Equals(DepictionGeometryType.LineString))
                    //                    {
                    elementVM.ZOI.UpdateLineThickness(newScale);
                    //                    }
                }
            }
        }

        #endregion
        void SetCursor(FrameworkElement fe)
        {
            if (fe == null) return;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                var dc = fe.DataContext as MapElementViewModel;

                if (dc != null)
                {
                    if (dc.ElementModel.ClickActions != null && dc.ElementModel.ClickActions.Count != 0)
                    {
                        fe.Cursor = ((FrameworkElement)Application.Current.Resources["CursorInsert"]).Cursor;
                    }
                }
            }
            else
            {
                fe.Cursor = null;
            }
        }


        private void ElementZOIItemsControlView_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Keyboard.Focus(sender as IInputElement);
            SetCursor(sender as FrameworkElement);
        }

        private void ElementZOIItemsControlView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //Temp hack
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                var fe = sender as FrameworkElement;
                if (fe == null) return;
                var dc = fe.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (dc.ElementModel.ClickActions == null || dc.ElementModel.ClickActions.Count == 0) return;
                var clickActions = dc.ElementModel.ClickActions;
                var desiredName = "AddRouteWaypoint";
                foreach (var action in clickActions)
                {
                    if (action.Key.Equals(desiredName))
                    {
                        foreach (var addin in AddinRepository.Instance.GetBehaviors())
                        {
                            if (addin.Key.BehaviorName.Equals(desiredName))
                            {
                                var latLong = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(e.GetPosition(this));
                                addin.Value.DoBehavior(dc.ElementModel, new[] { latLong });
                            }
                        }
                    }
                }
                Debug.WriteLine("Ntotihg yet");
            }
        }
    }
}