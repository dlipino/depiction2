﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.API.CoreEnumAndStructs;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.Revealers
{
    public class RevealerControl : Control
    {
//        static RevealerControl()//Doesn't work, but could be fun to experiment with it later
//        {
//            DefaultStyleKeyProperty.OverrideMetadata(typeof(RevealerControl), new FrameworkPropertyMetadata(typeof(RevealerControl)));
//        }

        public bool IsTopRevealer
        {
            get { return (bool)GetValue(IsTopRevealerProperty); }
            set { SetValue(IsTopRevealerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsTopRevealer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsTopRevealerProperty =
            DependencyProperty.Register("IsTopRevealer", typeof(bool), typeof(RevealerControl), new UIPropertyMetadata(false, IsTopRevealerChange));

        private static void IsTopRevealerChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var isTop = (bool)e.NewValue;
            var frame = d as FrameworkElement;
            if (frame == null) return;
            var revealer = frame.DataContext as IElementRevealerDisplayerViewModel;

            if (revealer == null) return;
            var topRevealer = DepictionBasicMapViewModel.topRevealer;
            if (isTop)
            {
                if (topRevealer != null && !topRevealer.ModelID.Equals(revealer.ModelID))
                {
                    topRevealer.DisplayerType = DepictionDisplayerType.Revealer;
                }

                revealer.DisplayerType = DepictionDisplayerType.TopRevealer;
                DepictionBasicMapViewModel.topRevealer = revealer;
            }
            else
            {
                if (topRevealer != null && !topRevealer.ModelID.Equals(revealer.ModelID))
                {
                    revealer.DisplayerType = DepictionDisplayerType.Revealer;
                }
            }
        }
    
        public RevealerControl()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.KeyDown += MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp += MainWindow_KeyEvent;
            }
        }

        private void MainWindow_KeyEvent(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.LeftShift) || e.Key.Equals(Key.RightShift))
            {
                var dc = DataContext as IElementRevealerDisplayerViewModel;
                if (dc == null) return;
                if (e.IsDown) { dc.MouseByPassEnabled = true; }
                if (e.IsUp) { dc.MouseByPassEnabled = false; }
            }
        }
    }
}
