﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.Revealers
{
    public class SpecialRevealerControllerView : Canvas
    {
        List<ElementRevealerDisplayerView> RevealerList = new List<ElementRevealerDisplayerView>();
        #region COnstructor

        public SpecialRevealerControllerView()
        {
            DataContextChanged += SpecialRevealerControllerView_DataContextChanged;
        }

        #endregion

        public void DetachCollectionListener(ObservableCollection<IElementRevealerDisplayerViewModel> collection)
        {
            if (collection == null) return;
            collection.Clear();
            collection.CollectionChanged -= RevealersChanged;
        }
        public void AttachCollectionListener(ObservableCollection<IElementRevealerDisplayerViewModel> collection)
        {
            if (collection == null) return;
            foreach (var newItem in collection)
            {
                if (newItem != null)
                {
                    var revealer = new ElementRevealerDisplayerView();
                    revealer.DataContext = newItem;
                    Children.Add(revealer);
                    RevealerList.Add(revealer);
                }
            }
            collection.CollectionChanged += RevealersChanged;
        }

        private void RevealersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                foreach (var newItem in e.NewItems)
                {
                    var rVM = newItem as IElementRevealerDisplayerViewModel;
                    if (rVM != null)
                    {
                        var revealer = new ElementRevealerDisplayerView();
                        revealer.DataContext = rVM;
                        Children.Add(revealer);
                        RevealerList.Add(revealer);
                    }
                }
            }
            if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                foreach (var oldItem in e.OldItems)
                {
                    var rVM = oldItem as IElementRevealerDisplayerViewModel;
                    if (rVM != null)
                    {
                        var matching = new List<FrameworkElement>();
                        foreach(var child in Children)
                        {
                            var fe = child as FrameworkElement;
                            if(fe != null && rVM.Equals(fe.DataContext))
                            {
                                matching.Add(fe);
                            }
                        }
                        foreach(var ui in matching)
                        {
                            Children.Remove(ui);
                        }
                    }
                }
            }
        }

        void SpecialRevealerControllerView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var context = e.NewValue as ObservableCollection<IElementRevealerDisplayerViewModel>;
            var oldContext = e.OldValue as ObservableCollection<IElementRevealerDisplayerViewModel>;
            Children.Clear();
            RevealerList.Clear();
            if (context == null)
            {
                if(oldContext != null)
                {
                    DetachCollectionListener(oldContext);
                    Children.Clear();
                }
                return;
            }
            
            DetachCollectionListener(oldContext);
            AttachCollectionListener(context);
        }
    }
}