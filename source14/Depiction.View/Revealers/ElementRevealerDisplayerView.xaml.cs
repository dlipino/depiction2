﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.Revealers
{
    /// <summary>
    /// Interaction logic for ElementDisplayerView.xaml. This actually should never display anything and can't really be added
    /// using XAML because the children of this thing are the 
    /// </summary>
    public partial class ElementRevealerDisplayerView
    {

        #region Constructor

        public ElementRevealerDisplayerView()
        {
            InitializeComponent();
            Loaded += ElementRevealerDisplayerView_Loaded;
            Unloaded += ElementRevealerDisplayerView_Unloaded;
        }

        #endregion
        #region Load unload events

        void ElementRevealerDisplayerView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent == null) return;
            //Since this is a revealer, the real parent is two trips up.
            var firstParent = Parent as Panel;//the special revealer holder.
            if (firstParent == null) return;
            var panelParent = firstParent.Parent as Panel;
            if (panelParent == null) return;
            mainParent = panelParent;
            if (originalChildren.Count != 0)
            {
                foreach (var uiElement in originalChildren)
                {
                    panelParent.Children.Add(uiElement);
                }
            }
            else
            {
                while (TempRevealerHoldingGrid.Children.Count != 0)
                {
                    var child = TempRevealerHoldingGrid.Children[0];
                    TempRevealerHoldingGrid.Children.Remove(child);
                    panelParent.Children.Add(child);
                    originalChildren.Add(child);
                    var itemsControl = child as ItemsControl;
                    if (itemsControl != null) itemsControl.Tag = this;
                }
            }
        }

        void ElementRevealerDisplayerView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (mainParent == null) return;
            foreach (var uiElement in originalChildren)
            {
                mainParent.Children.Remove(uiElement);
                //Not sure if this is legal on the unload
                //                TempHoldingGrid.Nodes.Add(uiElement);
            }
        }
        #endregion

        private void RevealerBorder_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var cc = sender as RevealerControl;
            if(cc != null)
            {
                var rev = cc.DataContext as IElementRevealerDisplayerViewModel;
                if (rev == null) return;
                if (rev.RevealerMaximized) return;
                rev.RevealerMaximized = true;
            }
            e.Handled = true;
        }
    }
}