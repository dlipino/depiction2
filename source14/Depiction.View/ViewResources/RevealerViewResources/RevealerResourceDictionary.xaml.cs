﻿using System;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.ViewResources.RevealerViewResources
{
    public partial class RevealerResourceDictionary
    {
        private void Thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = sender as Thumb;
            if (thumb == null) return;
            var rev = thumb.DataContext as IElementRevealerDisplayerViewModel;
            if (rev == null) return;
            rev.MinContentLocationX += e.HorizontalChange;
            rev.MinContentLocationY += e.VerticalChange;
        }
        private void CloseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var cc = sender as ContentControl;
            if (cc != null)
            {
                var rev = cc.DataContext as IElementRevealerDisplayerViewModel;
                if (rev == null) return;
                rev.PermaTextVisible = false;
            }
            e.Handled = true;
        }

        private void EnterKeyCheck(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var tb = e.Source as TextBox;
                if (tb != null)
                {
                    //The name of the textBox is in this case TB

                    BindingExpression be = tb.GetBindingExpression(TextBox.TextProperty);
                    if(be != null) be.UpdateSource();
                }
            }
        }
    }
}