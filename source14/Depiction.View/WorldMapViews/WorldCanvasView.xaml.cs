﻿namespace Depiction.View.WorldMapViews
{
    public partial class WorldCanvasView
    {
        #region Constructor
        public WorldCanvasView()
        {
            InitializeComponent();
            grabRects.Add(northEastRectangle);
            grabRects.Add(northWestRectangle);
            grabRects.Add(southEastRectangle);
            grabRects.Add(southWestRectangle);
            Dragger = MainDragger;
        }
        #endregion

    }
}