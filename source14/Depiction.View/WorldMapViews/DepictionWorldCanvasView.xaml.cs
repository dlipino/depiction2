﻿namespace Depiction.View.WorldMapViews
{
    public partial class DepictionWorldCanvasView
    {
        #region Constructor
        public DepictionWorldCanvasView()
        {
            InitializeComponent();
            grabRects.Add(northEastRectangle);
            grabRects.Add(northWestRectangle);
            grabRects.Add(southEastRectangle);
            grabRects.Add(southWestRectangle);
            Dragger = MainDragger;
        }
        #endregion
        protected override void OnPreviewMouseMove(System.Windows.Input.MouseEventArgs e)
        {
//            var dc = DataContext as DepictionMapAndMenuViewModel;
//
//            if (dc != null )
//            {
//                dc.CurrentMainWindowPoint = e.GetPosition(Application.Current.MainWindow);
//            }
            base.OnPreviewMouseMove(e);
        }

    }
}