﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.View.Dialogs.AddElements;
using Depiction.View.Dialogs.CopyElementToLibrary;
using Depiction.View.Dialogs.ElementZOIEditing;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.WorldMapViews
{
    /// <summary>
    /// Interaction logic for DepictionMapWithHelperDialogsView.xaml
    /// This thing shares duties with MapMenuAndDialogCanvas.xaml
    /// </summary>
    public partial class DepictionMapWithHelperDialogsView
    {
        //May or may not be a hack but i need to get the helper dialog windows back, these things are all over the place
        private readonly AddElementsHelpingWindow addElementsHelpWindow;
        private readonly CopyElementToLibraryWindow copyElementToLibraryWindow;
        private readonly ElementZOIEditHelpingWindow elementZOIEditHelpingWindow;
        //
        private bool areaSelectionEnable;
        Point startPoint;
        private Rect rectangleSelectRegion;
        private List<Point> arbitraryPoints = new List<Point>();
        #region COnstructor

        public DepictionMapWithHelperDialogsView()
        {
            InitializeComponent();
            
            DataContextChanged += DepictionMapWithHelperDialogsView_DataContextChanged;

            addElementsHelpWindow = new AddElementsHelpingWindow();
            addElementsHelpWindow.InitialLocation = new Point(50, 80);
            copyElementToLibraryWindow = new CopyElementToLibraryWindow();
            elementZOIEditHelpingWindow = new ElementZOIEditHelpingWindow();
            Loaded += MapMenuAndDialogCanvas_Loaded;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.DepictionRW:
//                    SelectAndQuickAddPanel.Children.Remove(QuickAddToolbar);
                    break;
                case ProductInformationBase.Reader:
                    SelectAndQuickAddPanel.IsEnabled = false;
                    SelectAndQuickAddPanel.Children.Remove(QuickAddToolbar);
                    SelectAndQuickAddPanel.Children.Remove(SelectionStartButton);
                    break;
            }
        }

        void MapMenuAndDialogCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                addElementsHelpWindow.Owner = Application.Current.MainWindow;
                copyElementToLibraryWindow.Owner = Application.Current.MainWindow;
                elementZOIEditHelpingWindow.Owner = Application.Current.MainWindow;
            }
        }
        #endregion

        void DepictionMapWithHelperDialogsView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldValue = e.OldValue as DepictionBasicMapViewModel;
            var newValue = e.NewValue as DepictionBasicMapViewModel;

            if (oldValue != null)
            {
                oldValue.CanvasVMModeChanged -= mapViewModel_CanvasModeChanged;
                addElementsHelpWindow.DataContext = null;
                elementZOIEditHelpingWindow.DataContext = null;
//                //I guess this should happen all the time?//Moved to mapmenanddialogcanvas
//                var mainWindow = Application.Current.MainWindow;
//                if (mainWindow != null)
//                {
//                    var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();
//
//                    foreach (var control in sameTypeList)
//                    {
//                        control.Close();
//                    }
//                }
            }
            if (newValue != null)
            {
                newValue.CanvasVMModeChanged += mapViewModel_CanvasModeChanged;
                addElementsHelpWindow.DataContext = newValue;
                elementZOIEditHelpingWindow.DataContext = newValue;
            }
        }

        void mapViewModel_CanvasModeChanged(DepictionCanvasMode canvasMode)
        {
            var dc = DataContext as DepictionMapQuickAddAndTileViewModel;
            switch (canvasMode)
            {
                case DepictionCanvasMode.Normal:
                    addElementsHelpWindow.Close();
                    copyElementToLibraryWindow.Close();
                    //total hack for now, race condition between the close and setting canvas mode back to normal
                    try
                    {//Man this type of error is annoying, hopefully a good fix is found someday, and hopefully it isnt a very serious issue.
                        elementZOIEditHelpingWindow.Close();
                    }catch{}
                    break;
                case DepictionCanvasMode.SuspendedAdd:
                    addElementsHelpWindow.Close();
                    break;
                case DepictionCanvasMode.AnnotationAdd:
                case DepictionCanvasMode.ElementAdd:
                case DepictionCanvasMode.ZOIAdd:
                    addElementsHelpWindow.Show();
                    break;
                case DepictionCanvasMode.ZOIEdit:
                    elementZOIEditHelpingWindow.Tag = dc.CurrentEditElement;
                    elementZOIEditHelpingWindow.Show();
                    break;
            }

            var newEnabled = canvasMode.Equals(DepictionCanvasMode.AreaSelection) || canvasMode.Equals(DepictionCanvasMode.ArbitraryAreaSelection);
            if (!newEnabled)
            {
                PreviewMouseLeftButtonUp -= DepictionMapWithHelperDialogsView_PreviewMouseLeftButtonUp;
                CanvasForPrinting.PreviewMouseLeftButtonDown -= DepictionMapWithHelperDialogsView_MouseLeftButtonDown;
                CanvasForPrinting.PreviewMouseRightButtonUp -= DepictionMapWithHelperDialogsView_MouseRightButtonUp;
                CanvasForPrinting.PreviewMouseMove -= DepictionMapWithHelperDialogsView_PreviewMouseMove;
                areaSelectionEnable = false;
                Cursor = null;

            }
            else
            {
                if (!areaSelectionEnable)
                {
                    PreviewMouseLeftButtonUp += DepictionMapWithHelperDialogsView_PreviewMouseLeftButtonUp;
                    CanvasForPrinting.PreviewMouseLeftButtonDown += DepictionMapWithHelperDialogsView_MouseLeftButtonDown;
                    CanvasForPrinting.PreviewMouseRightButtonUp += DepictionMapWithHelperDialogsView_MouseRightButtonUp;
                    CanvasForPrinting.PreviewMouseMove += DepictionMapWithHelperDialogsView_PreviewMouseMove;
                    Cursor = Cursors.Cross;
                    AreaSelectionRectangle.Data = null;
                    areaSelectionEnable = true;
                    arbitraryPoints = new List<Point>();
                    rectangleSelectRegion = new Rect();
                }
            }
        }

        void DepictionMapWithHelperDialogsView_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!(e.OriginalSource is Button))
            {
                var currentPoint = e.GetPosition(this);
                if (e.LeftButton.Equals(MouseButtonState.Pressed) && WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.AreaSelection))
                {
                    //                   
                    if (!startPoint.X.Equals(double.NaN))
                    {
                        var width = currentPoint.X - startPoint.X;
                        var height = currentPoint.Y - startPoint.Y;
                        if (width < 0)
                        {
                            rectangleSelectRegion.X = startPoint.X + width;
                            rectangleSelectRegion.Width = -width;
                        }
                        else
                        {
                            rectangleSelectRegion.Width = width;
                        }
                        if (height < 0)
                        {
                            rectangleSelectRegion.Y = startPoint.Y + height;
                            rectangleSelectRegion.Height = -height;
                        }
                        else
                        {
                            rectangleSelectRegion.Height = height;
                        }
                        var rectGeom = AreaSelectionRectangle.Data as RectangleGeometry;
                        if (rectGeom != null)
                            rectGeom.Rect = rectangleSelectRegion; 
                        e.Handled = true;
                    }
                }
                else if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.ArbitraryAreaSelection))
                {
                    if (arbitraryPoints.Count > 0)
                    {
                        var streamGeometry = new StreamGeometry();
                        StreamGeometryContext context = streamGeometry.Open();
                        bool start = false;
                        foreach (var point in arbitraryPoints)
                        {
                            if (!start)
                            {
                                context.BeginFigure(point, true, true);
                                start = true;
                            }
                            else
                            {
                                context.LineTo(point, true, true);
                            }
                        }
                        context.LineTo(currentPoint, true, true);
                        context.Close();
                        streamGeometry.Freeze();

                        AreaSelectionRectangle.Data = streamGeometry;
                    }
                }
            }
        }
        void DepictionMapWithHelperDialogsView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(this);
            if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.AreaSelection))
            {
                //                if (e.ClickCount == 2)
                //                {
                //                    CloseArbitraryRegion(false);
                //                }
                //                else
                //                {
                areaSelectionEnable = true;
                rectangleSelectRegion = new Rect(startPoint, startPoint);
                AreaSelectionRectangle.Data = new RectangleGeometry(rectangleSelectRegion);
                //                }
                e.Handled = true;

            }
            else if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.ArbitraryAreaSelection))
            {
                if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                {
                    var count = arbitraryPoints.Count;
                    if (count > 1) arbitraryPoints.RemoveAt(count - 1);
                }
                else
                {
                    if (e.ClickCount == 2)
                    {
                        CloseArbitraryRegion(false);
                    }
                    else
                    {
                        areaSelectionEnable = true;
                        arbitraryPoints.Add(startPoint);
                    }
                }
                e.Handled = true;
            }
        }

        private void CloseArbitraryRegion(bool hasLastPoint)
        {
            var topContext = DataContext as DepictionMapQuickAddAndTileViewModel;
            if (topContext == null) return;
            if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.ArbitraryAreaSelection))
            {
                if (!hasLastPoint)
                {
                    var endPoint = Mouse.GetPosition(this);
                    arbitraryPoints.Add(endPoint);
                }
                var latLongList = new List<ILatitudeLongitude>();
                foreach (var point in arbitraryPoints)
                {
                    latLongList.Add(
                        WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(point));
                }
                topContext.GetAllElementThatInteresectGeoBounds(latLongList);
            }
            else if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.AreaSelection))
            {
                if (AreaSelectionRectangle.Data != null)
                {
                    var data = AreaSelectionRectangle.Data.Bounds;
                    var geoCoords = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(data);

                    var latLongList = new List<ILatitudeLongitude> { geoCoords.TopLeft, geoCoords.TopRight, geoCoords.BottomRight, geoCoords.BottomLeft };
                    topContext.GetAllElementThatInteresectGeoBounds(latLongList);
                }
            }
            startPoint = new Point(double.NaN, double.NaN);
            arbitraryPoints.Clear();
            AreaSelectionRectangle.Data = null;

            //For now end region selection after the first time
            var dc = DataContext as DepictionBasicMapViewModel;
            if (dc != null)
            {
                dc.WorldCanvasModeVM = DepictionCanvasMode.Normal;//There as a bunch of back code that undoes many mouse thigns
                Cursor = null;
            }
        }

        void DepictionMapWithHelperDialogsView_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.ArbitraryAreaSelection) && !(e.OriginalSource is Button))
            {
                CloseArbitraryRegion(false);
            }
            var dc = DataContext as DepictionBasicMapViewModel;
            if (dc != null)
            {
                dc.WorldCanvasModeVM = DepictionCanvasMode.Normal;//There as a bunch of back code that undoes many mouse thigns
                Cursor = null;
            }
            e.Handled = true;
        }

        void DepictionMapWithHelperDialogsView_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (WorldCanvas.WorldCanvasMode.Equals(DepictionCanvasMode.AreaSelection) && !(e.OriginalSource is Button))
            {
                CloseArbitraryRegion(false);
                startPoint = new Point(double.NaN, double.NaN);
                AreaSelectionRectangle.Data = null;
            }
        }
    }
}
