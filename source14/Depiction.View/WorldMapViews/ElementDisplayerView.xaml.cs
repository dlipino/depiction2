﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.API.Interfaces;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.WorldMapViews
{
    /// <summary>
    /// Interaction logic for ElementDisplayerView.xaml. This actually should never display anything and can't really be added
    /// using XAML because the children of this thing are the 
    /// </summary>
    public partial class ElementDisplayerView
    {
        #region Constructor

        public ElementDisplayerView()
        {
            InitializeComponent();
            //hmm i guess this guy is also only loaded once
            Loaded += ElementDisplayerView_Loaded;
            Unloaded += ElementDisplayerView_Unloaded;
            LayoutUpdated += ElementDisplayerView_LayoutUpdated;
        }

        void ElementDisplayerView_LayoutUpdated(object sender, System.EventArgs e)
        {
#if DEBUG
            if (DepictionDisplayerBase.stopWatch != null &&
                DepictionDisplayerBase.stopWatch.IsRunning)
            {
                Debug.WriteLine(string.Format("Total update time {0} seconds",
                                              (DepictionDisplayerBase.stopWatch.Elapsed.Seconds)));
                DepictionDisplayerBase.stopWatch.Stop();
            }
#endif
            Mouse.OverrideCursor = null;
        }


        #endregion
        //There is some code duplication, but it is pretty easily managed later, should be at least
        #region Load unload events

        void ElementDisplayerView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent == null) return;
            var panelParent = Parent as Panel;
            if (panelParent == null) return;
            mainParent = panelParent;
            if (originalChildren.Count != 0)
            {
                foreach (var uiElement in originalChildren)
                {
                    panelParent.Children.Add(uiElement);
                }
            }
            else
            {
                while (TempHoldingGrid.Children.Count != 0)
                {
                    var child = TempHoldingGrid.Children[0];
                    TempHoldingGrid.Children.Remove(child);
                    panelParent.Children.Add(child);
                    originalChildren.Add(child);
                    var itemsControl = child as ItemsControl;
                    if (itemsControl != null) itemsControl.Tag = this;
                }
            }
        }

        void ElementDisplayerView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (mainParent == null) return;
            foreach (var uiElement in originalChildren)
            {
                mainParent.Children.Remove(uiElement);
                //Not sure if this is legal on the unload
                //                TempHoldingGrid.Nodes.Add(uiElement);
            }
        }
        #endregion


    }
}