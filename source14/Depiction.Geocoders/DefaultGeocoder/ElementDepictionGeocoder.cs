﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;

namespace Depiction.Geocoders.DefaultGeocoder
{
    [Geocoder("ElementGeocoder",DisplayName = "Default Depiction Geocoder")]
    public class ElementGeocoder : IDepictionGeocoder
    {
        #region Variables

        const string noResponseFromGeocoder = "No response received from GeoCoder.";
        const string parseResponseFailedForGeoCoder = "Unable to process response from the geocoder.";
        public const string InvalidGeoLocationText = "Could not find this location, please be more specific: {0}";
        private string URI;
        private Dictionary<string, string> geocoderParameters;

        #endregion

        #region properties 
        public bool StoreResultsInDepiction
        {
            get { return true; }
        }


        public string GeocoderName
        {
            get { return "Element geocoder"; }
        }
        #endregion 
        #region GeocoderAddinView Members

        public GeocodeResults GeocodeAddress(string addressString, bool isLatLong)
        {
            throw new System.NotImplementedException();
        }

        public GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country)
        {
            string fullAddress = string.Format("{0} {1} {2} {3} {4}", street, city, state, zipcode, country);
            return GeocodeAddress(fullAddress);
        }

       

        public bool IsAddressGeocoder
        {
            get { return true; }
        }

        public bool IsLatLongGeocoder
        {
            get { return false; }
        }

        public void ReceiveParameters(Dictionary<string, string> parameters)
        {
            if (parameters != null && parameters.ContainsKey("URI"))
                URI = parameters["URI"];

            if (parameters != null)
            {
                geocoderParameters = new Dictionary<string, string>();
                foreach (var entry in parameters)
                {
                    geocoderParameters.Add(entry.Key, entry.Value);
                }
            }
        }

        public GeocodeResults GeocodeAddress(string addressString)
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable || string.IsNullOrEmpty(URI))
                return new GeocodeResults("Internet not available for geocoding");
            //
            //unparsed addresses come either from ADD ELEMENT via address OR LIVE REPORTS
            //
            //geocode this ONLY if this geocoder is PRIMARY for Live Reports
            if (geocoderParameters.ContainsKey("LiveReports"))
            {
                if (!geocoderParameters["LiveReports"].Equals("Primary")) return new GeocodeResults("This geocoder cannot be used for live reports");
            }
            string url = string.Format("{0}&location={1}", URI, Uri.EscapeDataString(addressString));

            WebRequest geocoderRequest = WebRequest.Create(url);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return new GeocodeResults(ParseResponse(responseStream), true);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return new GeocodeResults(string.Format(InvalidGeoLocationText, addressString));
                throw;
            }

        }

        private static LatitudeLongitude ParseResponse(Stream responseStream)
        {
            LatitudeLongitude position;

            if (responseStream != null)
            {
                string latitude = string.Empty;
                string longitude = string.Empty;
                var reader = new XmlTextReader(responseStream);
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name != "Succeeded" && reader.Name != "ResultSet")
                            {
                                if (reader.Name.Equals("Latitude",StringComparison.InvariantCultureIgnoreCase) 
                                    || reader.Name.Equals("Longitude",StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string text = reader.ReadString();
                                    if (reader.Name.Equals("Latitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        latitude = text;
                                    }
                                    if (reader.Name.Equals("Longitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        longitude = text;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (XmlException ex)
                {
                    throw new Exception(parseResponseFailedForGeoCoder, ex);
                }
                finally
                {
                    reader.Close();
                }

                position = new LatitudeLongitude(latitude, longitude);
            }
            else
            {
                throw new Exception(noResponseFromGeocoder);
            }

            return position;
        }

        #endregion
    }
}