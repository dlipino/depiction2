﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using Depiction.API;

namespace Depiction.Geocoders.USCGeocoder
{
    public static class AddressParser
    {
        public static string ParseCSZ(string inputString, out string city, out string state, out string zip)
        {
            state = "";
            city = "";
            inputString = ParseOutZip(inputString, out zip);
            inputString = inputString.Trim();

            if (String.IsNullOrEmpty(zip))
            {
                //all bets are off
                //USC geocoder REQUIRES zip, what part of that did we not understand?
                return inputString;
            }
            string stateFromGeoNames;
            string cityFromGeoNames;

            GetCityFromGeoNames(zip, out cityFromGeoNames, out stateFromGeoNames);
            //get city name out of the address string
            inputString = inputString.ToLower().Replace(cityFromGeoNames.ToLower(), "");
            city = cityFromGeoNames;

            //check for commas in input string
            if (inputString.Contains(","))
            {
                int index = inputString.LastIndexOf(",");
                if (index > 0)
                {
                    state = inputString.Substring(index + 1, inputString.Length - index - 1);
                    state = state.Trim();
                    if (IsValidState(state))
                    {
                        inputString = inputString.Replace(state, "");
                        inputString = inputString.Trim();
                        return CleanupCommas(inputString);
                    }
                    //try trimming it further
                    var splitString = state.Split(new[] { ' ' });
                    if (splitString.Length > 1)
                    {
                        state = splitString[splitString.Length - 1];
                        state = state.Trim();
                        if (IsValidState(state))
                        {
                            inputString = inputString.Replace(state, "");
                            inputString = inputString.Trim();
                            return CleanupCommas(inputString);
                        }
                    }
                    //no valid state was found
                    state = "";
                }
            }
            else //no commas between city, state, or zip
            {
                var fields = inputString.Split(new[] { ' ' });
                if (fields.Length > 1)
                {
                    state = fields[fields.Length - 1];
                    if (IsValidState(state))
                    {
                        inputString = inputString.Replace(state, "");
                        inputString = inputString.Trim();
                        return CleanupCommas(inputString);

                    }
                    state = "";
                }

            }
            //if state wasn't obtainable from the addressString, get it from geonames
            if (String.IsNullOrEmpty(state))
                state = stateFromGeoNames;

            return inputString;
        }

        public static void GetCityFromGeoNames(string zip, out string cityFromGeoNames, out string stateFromGeoNames)
        {
            cityFromGeoNames = stateFromGeoNames = "";
            var geoNamesURL = String.Format("http://ws.geonames.org/postalCodeSearch?postalcode={0}&maxRows=20", zip);
            string xmlPath = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, string.Format("Geonames{0}", Guid.NewGuid().ToString("N")));
            xmlPath = getXML(geoNamesURL, xmlPath, 0);
            try
            {
                getPlaceName(xmlPath, out cityFromGeoNames, out stateFromGeoNames);
            }
            catch (Exception)
            {

            }
            return;
        }

        private static string getXML(string url, string fileName, int exceptionCount)
        {
            var webClient = new WebClient();
            var buffer = new byte[0];

            try
            {
                buffer = webClient.DownloadData(url);
            }
            catch (WebException)
            {
                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    return getXML(url, fileName, exceptionCount + 1);
            }


            // if this file was created while we were waiting, let's not redo it
            return DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName) ?? DepictionAccess.PathService.CacheFile(new MemoryStream(buffer), fileName);
        }

        private static void getPlaceName(string xmlPath, out string cityName, out string stateName)
        {
            string placeName = "";
            cityName = stateName = "";
            using (var reader = new XmlTextReader(xmlPath))
            {
                while (reader.Read())
                {
                    if (reader.NodeType != XmlNodeType.Element) continue;

                    if (reader.Name == "code")
                    {
                        ExtractName(reader, out cityName, out stateName);
                        if (!String.IsNullOrEmpty(cityName))
                        {
                            return;
                        }
                    }
                }
            }
            return;
        }
        private static void ExtractName(XmlTextReader reader, out string cityName, out string stateName)
        {
            string placeName = "";
            string country = "";
            cityName = stateName = "";
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (!String.IsNullOrEmpty(cityName) && !String.IsNullOrEmpty(country) && !String.IsNullOrEmpty(stateName))
                        if (country.Equals("US")) return;
                }

                if (reader.NodeType != XmlNodeType.Element) continue;

                if (reader.Name == "name")
                {
                    if (reader.Read())
                        cityName = reader.Value;
                }
                if (reader.Name == "countryCode")
                {
                    if (reader.Read())
                        country = reader.Value;
                }
                if (reader.Name == "adminCode1")
                {
                    if (reader.Read())
                        stateName = reader.Value;
                }
            }

            return;
        }
        private static string CleanupCommas(string input)
        {
            string returnString;
            returnString = input.Trim(',');
            return returnString.Trim();

        }

        private static bool IsValidState(string state)
        {
            state = state.ToUpper();
            if (state_list.ContainsKey(state))
            {
                return true;
            }
            if (state_list.ContainsValue(state))
                return true;

            return false;
        }

        private static string ParseOutZip(string input, out string zip)
        {
            var returnString = input;
            zip = "";
            var fields = input.Split(new[] { ',' });
            if (fields.Length == 1)//no commas
            {
                fields = input.Split(new[] { ' ' });//space as a delimiter
                if (fields.Length == 1) //no commas OR space
                {
                    if (IsValidZipCode(input))
                    {
                        zip = input;
                        returnString = "";
                        return returnString;
                    }
                }
                foreach (string s in fields)
                {
                    if (IsValidZipCode(s))
                    {
                        zip = s;
                        returnString = input.Replace(zip, "");
                        return returnString;
                    }
                }
            }
            //there was at least one comma
            foreach (string s in fields)
            {
                var fs = s.Split(new[] { ' ' });
                foreach (var f in fs)
                {
                    if (IsValidZipCode(f))
                    {
                        zip = f;
                        returnString = input.Replace(zip, "");
                        return returnString;
                    }
                }
            }

            return returnString;
        }

        public static bool IsValidZipCode(string value)
        {
            var re = @"^\d{5}([\-]\d{4})?$";
            return Regex.Match(value, re).Success;
        }
        #region US States
        private static readonly Dictionary<string, string> state_list = new Dictionary<string, string>
                                                                            {

                                                                                {"AL", "Alabama"},
                                                                                {"AK", "Alaska"},
                                                                                {"AZ", "Arizona"},
                                                                                {"AR", "Arkansas"},
                                                                                {"CA", "California"},
                                                                                {"CO", "Colorado"},
                                                                                {"CT", "Connecticut"},
                                                                                {"DE", "Delaware"},
                                                                                {"DC", "District Of Columbia"},
                                                                                {"FL", "Florida"},
                                                                                {"GA", "Georgia"},
                                                                                {"HI", "Hawaii"},
                                                                                {"ID", "Idaho"},
                                                                                {"IL", "Illinois"},
                                                                                {"IN", "Indiana"},
                                                                                {"IA", "Iowa"},
                                                                                {"KS", "Kansas"},
                                                                                {"KY", "Kentucky"},
                                                                                {"LA", "Louisiana"},
                                                                                {"ME", "Maine"},
                                                                                {"MD", "Maryland"},
                                                                                {"MA", "Massachusetts"},
                                                                                {"MI", "Michigan"},
                                                                                {"MN", "Minnesota"},
                                                                                {"MS", "Mississippi"},
                                                                                {"MO", "Missouri"},
                                                                                {"MT", "Montana"},
                                                                                {"NE", "Nebraska"},
                                                                                {"NV", "Nevada"},
                                                                                {"NH", "New Hampshire"},
                                                                                {"NJ", "New Jersey"},
                                                                                {"NM", "New Mexico"},
                                                                                {"NY", "New York"},
                                                                                {"NC", "North Carolina"},
                                                                                {"ND", "North Dakota"},
                                                                                {"OH", "Ohio"},
                                                                                {"OK", "Oklahoma"},
                                                                                {"OR", "Oregon"},
                                                                                {"PA", "Pennsylvania"},
                                                                                {"RI", "Rhode Island"},
                                                                                {"SC", "South Carolina"},
                                                                                {"SD", "South Dakota"},
                                                                                {"TN", "Tennessee"},
                                                                                {"TX", "Texas"},
                                                                                {"UT", "Utah"},
                                                                                {"VT", "Vermont"},
                                                                                {"VA", "Virginia"},
                                                                                {"WA", "Washington"},
                                                                                {"WV", "West Virginia"},
                                                                                {"WI", "Wisconsin"},
                                                                                {"WY", "Wyoming"}
                                                                            };
        #endregion
    }
}