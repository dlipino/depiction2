using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Depiction.APINew.AddinObjects.Objects;
using Depiction.APINew.Interfaces;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.TileServiceHelpers;
using Depiction.APINew.ValueTypes;
using Depiction.APIUnmanaged.Service;
using Depiction.APIUnmanaged.ValueObjects;
using Depiction.CoreModel.TileServiceHelpers;
using OpenStreetMapTiler.NAIP_MapServer;

namespace OpenStreetMapTiler
{
    [Export(typeof(ITileProvider))]
    public class NAIPTilerProvider : TileProvider
    {
        protected const int MaxZoomLevel = 18;
        private const int MinimumTilesAcross = 4;

        private NAIP_Coverage_MapServer server;
        private NAIP_Coverage_MapServer Server
        {
            get
            {
                if (server == null)
                    server = new NAIP_Coverage_MapServer();
                return server;
            }
        }
        protected override double PixelsPerTile
        {
            get { return 256; }
        }

        public NAIPTilerProvider()
        {
            cacheService = new TileCacheService(@"C:\tileCache\NAIP");
        }

        protected override int GetZoomLevel(IMapCoordinateBounds boundingBox)
        {//, int minTilesAcross, int maxZoomLevel
            int i;

            for (i = 1; i < MaxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right) * (1 << i) / 360D;
                if (tilesAcross > MinimumTilesAcross) break;
            }

            return i;
        }

        public override string DisplayName
        {
            get { return "Imagery (NAIP)"; }
        }

        public override string SourceName
        {
            get { return "NAIP"; }
        }

        protected override int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / ((1 << zoom));
            double metersX = EARTHRADIUS * DegToRad(latLong.Longitude);
            var x = (int)((EARTHHALFCIRC + metersX) / eqMetersPerTile);
            return x;
        }

        protected override int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double latRad = DegToRad(latLong.Latitude);
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double prj = Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad));
            double metersY = EARTHRADIUS * prj;
            var y = (int)((EARTHHALFCIRC - metersY) / eqMetersPerTile);
            return y;
        }

        protected override double TileColToTopLeftLong(int col, int zoomLevel)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoomLevel);
            double metersX = (eqMetersPerTile * col) - EARTHHALFCIRC;
            double lonRad = metersX / EARTHRADIUS;
            return RadToDeg(lonRad);
        }

        protected override double TileRowToTopLeftLat(int row, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double metersY = EARTHHALFCIRC - (row * eqMetersPerTile);
            double latRad = Math.Atan(Math.Sinh(metersY / EARTHRADIUS));
            double latDeg = RadToDeg(latRad);
            return latDeg;
        }

        protected override TileModel GetTile(int row, int column, int zoom)
        {
            var topLeft = new LatitudeLongitude(TileRowToTopLeftLat(row, zoom),
                                                TileColToTopLeftLong(column, zoom));
            var bottomRight = new LatitudeLongitude(TileRowToTopLeftLat(row + 1, zoom),
                                                    TileColToTopLeftLong(column + 1, zoom));

            var tileKey = SourceName + "_" + row + "_" + column + "_" + zoom + ".png";
            var fullImagePath = cacheService.GetCacheFullStoragePath(tileKey);
            //            string imagePath = DepictionAccess.RetrieveCachedTileFilenameIfExists(tileKey);

            if (File.Exists(fullImagePath))
                return new TileModel(zoom, tileKey, fullImagePath, topLeft, bottomRight);

            IMapCoordinateBounds boundingBox = new MapCoordinateBounds(topLeft, bottomRight);
            //            boundingBox.MapImageSize = new Size(256, 256);
            if (!GetAndSaveNAIPBitmap(fullImagePath, boundingBox, (int)PixelsPerTile, (int)PixelsPerTile))
            {
                return null;
            }


            return new TileModel(zoom, tileKey, fullImagePath, boundingBox.TopLeft, boundingBox.BottomRight);
        }
        override protected string TileImageFileBuilder(IMapCoordinateBounds boundingBox, List<TileModel> tiles,
                                                Size totalSize)
        {
            var imagePath = TiledImageBuilderService.GetTiledImage(boundingBox, tiles, totalSize, CreateTileFileName(boundingBox));
            return imagePath;
        }

        private bool GetAndSaveNAIPBitmap(string fileName, IMapCoordinateBounds boundingBox, int imageWidth, int imageHeight)
        //        private bool GetAndSaveNAIPBitmap(string fileName,IMapCoordinateBounds boundingBox, out IMapCoordinateBounds resultingBox, int imageWidth, int imageHeight)
        {
            try
            {
                var defaultMapName = Server.GetDefaultMapName();
                MapDescription mapDescription = new MapDescription();
                //var geographicCoordinateSystem = new GeographicCoordinateSystem() { };

                var mapExtent = new MapExtent();
                var envelope = new EnvelopeN();
                envelope.XMin = boundingBox.Left;
                envelope.XMax = boundingBox.Right;
                envelope.YMin = boundingBox.Bottom;
                envelope.YMax = boundingBox.Top;
                mapExtent.Extent = envelope;
                mapDescription.MapArea = mapExtent;
                //envelope.SpatialReference = geographicCoordinateSystem;

                //var extent = new EnvelopeN() { XMin = -132.342642270881, XMax = -60.4046960034264, YMin = 1.68104843656771, YMax = 73.6189947040223 } };

                mapDescription.Name = defaultMapName;
                LayerDescription[] layerDescriptions = new[] 
                { 
                    new LayerDescription { LayerID = 2, Visible = false},
                    new LayerDescription { LayerID = 3, Visible = false},
                    new LayerDescription { LayerID = 4, Visible = false},
                    new LayerDescription { LayerID = 5, Visible = false},
                    new LayerDescription { LayerID = 6, Visible = false},
                    new LayerDescription { LayerID = 7, Visible = false},
                    new LayerDescription { LayerID = 8, Visible = false},
                    new LayerDescription { LayerID = 394, Visible = true},
                    new LayerDescription { LayerID = 395, Visible = true},
                    new LayerDescription { LayerID = 396, Visible = true},
                    new LayerDescription { LayerID = 397, Visible = true},
                    new LayerDescription { LayerID = 398, Visible = true},
                    new LayerDescription { LayerID = 399, Visible = true},
                    new LayerDescription { LayerID = 400, Visible = true},
                    new LayerDescription { LayerID = 401, Visible = true},
                    new LayerDescription { LayerID = 402, Visible = true},
                    new LayerDescription { LayerID = 403, Visible = true},
                    new LayerDescription { LayerID = 404, Visible = true},
                    new LayerDescription { LayerID = 405, Visible = true},
                    new LayerDescription { LayerID = 406, Visible = true},
                    new LayerDescription { LayerID = 407, Visible = true},
                    new LayerDescription { LayerID = 408, Visible = true},
                    new LayerDescription { LayerID = 409, Visible = true},
                    new LayerDescription { LayerID = 410, Visible = true},
                    new LayerDescription { LayerID = 411, Visible = true},
                    new LayerDescription { LayerID = 412, Visible = true},
                    new LayerDescription { LayerID = 413, Visible = true},
                    new LayerDescription { LayerID = 414, Visible = true},
                    new LayerDescription { LayerID = 415, Visible = true},
                    new LayerDescription { LayerID = 416, Visible = true},
                    new LayerDescription { LayerID = 417, Visible = true},
                    new LayerDescription { LayerID = 418, Visible = true},
                    new LayerDescription { LayerID = 419, Visible = true},
                    new LayerDescription { LayerID = 420, Visible = true},
                    new LayerDescription { LayerID = 421, Visible = true},
                    new LayerDescription { LayerID = 422, Visible = true},
                    new LayerDescription { LayerID = 423, Visible = true},
                    new LayerDescription { LayerID = 424, Visible = true},
                    new LayerDescription { LayerID = 425, Visible = true},
                    new LayerDescription { LayerID = 426, Visible = true},
                    new LayerDescription { LayerID = 427, Visible = true},
                    new LayerDescription { LayerID = 428, Visible = true},
                    new LayerDescription { LayerID = 429, Visible = true},
                    new LayerDescription { LayerID = 430, Visible = true},
                    new LayerDescription { LayerID = 431, Visible = true},
                    new LayerDescription { LayerID = 432, Visible = true},
                    new LayerDescription { LayerID = 433, Visible = true},
                    new LayerDescription { LayerID = 434, Visible = true},
                    new LayerDescription { LayerID = 435, Visible = true},
                    new LayerDescription { LayerID = 436, Visible = true},
                    new LayerDescription { LayerID = 437, Visible = true},
                    new LayerDescription { LayerID = 438, Visible = true},
                    new LayerDescription { LayerID = 439, Visible = true},
                    new LayerDescription { LayerID = 440, Visible = true},
                    new LayerDescription { LayerID = 441, Visible = true},
                };

                mapDescription.LayerDescriptions = layerDescriptions;
                ImageDescription imageDescription = new ImageDescription();
                ImageDisplay imageDisplay = new ImageDisplay { ImageDPI = 96, ImageHeight = imageHeight, ImageWidth = imageWidth };
                imageDescription.ImageDisplay = imageDisplay;
                imageDescription.ImageType = new ImageType { ImageFormat = esriImageFormat.esriImageJPG, ImageReturnType = esriImageReturnType.esriImageReturnMimeData };

                var mapImage = Server.ExportMapImage(mapDescription, imageDescription);
                var resultingBox = new MapCoordinateBounds();
                resultingBox.Left = ((EnvelopeN)mapImage.Extent).XMin;
                resultingBox.Right = ((EnvelopeN)mapImage.Extent).XMax;
                resultingBox.Top = ((EnvelopeN)mapImage.Extent).YMax;
                resultingBox.Bottom = ((EnvelopeN)mapImage.Extent).YMin;
                var stream = new MemoryStream(mapImage.ImageData);

                ImageElementInfo imageElementInfo = new ImageElementInfo("NAIP", "Imagery (NAIP)");
                imageElementInfo.SetMap(resultingBox.TopLeft, resultingBox.BottomRight, stream);
                imageElementInfo.Crop(boundingBox, imageWidth, imageHeight);
                if (!imageElementInfo.SaveImageElement(fileName))
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}