﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged.Service;
using Depiction.CoreModel.TileServiceHelpers;

namespace Depiction.NAIPTileImporter
{
    [Export(typeof(ITileProvider))]
    [DepictionDefaultImporterMetadata("SeamlessOrthoimageryTileImporter", Description = "Imagery (HRO Seamless)",DisplayName = "Imagery (HRO Seamless)")]
    public class SeamlessOrthoimageryTileImporter : TileProvider
    {
        protected const int MaxZoomLevel = 20;
        private const int MinimumTilesAcross = 4;
        
        #region Properties
        public override TileImageTypes TileImageType
        {
            get { return TileImageTypes.Satellite; }
        }

        public override string DisplayName
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        return "Imagery (HRO Seamless)";
                    default:
                        return "Imagery (HRO Seamless)";
                }
            }
        }

        public override string SourceName
        {
            get { return "HRO Seamless"; }
        }
        
        protected override double PixelsPerTile
        {
            get { return 256; }
        }
        
        #endregion

        #region Constructor

        public SeamlessOrthoimageryTileImporter()
        {
            cacheService = new TileCacheService(SourceName);
        }
        
        #endregion

        protected override int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / ((1 << zoom));
            double metersX = EARTHRADIUS * DegToRad(latLong.Longitude);
            var x = (int)((EARTHHALFCIRC + metersX) / eqMetersPerTile);
            return x;
        }

        protected override int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double latRad = DegToRad(latLong.Latitude);
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double prj = Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad));
            double metersY = EARTHRADIUS * prj;
            var y = (int)((EARTHHALFCIRC - metersY) / eqMetersPerTile);
            return y;
        }

        protected override double TileColToTopLeftLong(int col, int zoomLevel)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoomLevel);
            double metersX = (eqMetersPerTile * col) - EARTHHALFCIRC;
            double lonRad = metersX / EARTHRADIUS;
            return RadToDeg(lonRad);
        }

        protected override double TileRowToTopLeftLat(int row, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double metersY = EARTHHALFCIRC - (row * eqMetersPerTile);
            double latRad = Math.Atan(Math.Sinh(metersY / EARTHRADIUS));
            double latDeg = RadToDeg(latRad);
            return latDeg;
        }

        protected override TileModel GetTile(int row, int column, int zoom)
        {
            var cacheName = SourceName + "_" + row + "_" + column + "_" + zoom + ".png";
            string fullImageName = cacheService.GetCacheFullStoragePath(cacheName);
            var topLeft = new LatitudeLongitude(TileRowToTopLeftLat(row, zoom),
                                                TileColToTopLeftLong(column, zoom));
            var bottomRight = new LatitudeLongitude(TileRowToTopLeftLat(row + 1, zoom),
                                                    TileColToTopLeftLong(column + 1, zoom));
            if (File.Exists(fullImageName) && !ReplaceCachedImages) return new TileModel(zoom, cacheName, fullImageName, topLeft, bottomRight);
            
            IMapCoordinateBounds boundingBox = new MapCoordinateBounds(topLeft, bottomRight);
            WmsDataProvider wmsDataProvider = new WmsDataProvider("http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EDC_Ortho_HRO/ImageServer/WMSServer", "0", boundingBox, "image/png");
            wmsDataProvider.ImageHeight = wmsDataProvider.ImageWidth = 256;
            var response = wmsDataProvider.GetData();
            //because the wmsDataprovider picks an unusable name, the file will get moved 
            if(File.Exists(fullImageName))
            {
                File.Delete(fullImageName);
            }
            if (response.ResponseFile == null) return null;
            File.Move(response.ResponseFile,fullImageName);

            return new TileModel(zoom, cacheName, fullImageName, topLeft, bottomRight);
            //return new TileModel(zoom, Path.GetFileName(response.ResponseFile), response.ResponseFile, topLeft, bottomRight);
        }

        override protected string TileImageFileBuilder(IMapCoordinateBounds boundingBox, List<TileModel> tiles,
                                                       Size totalSize)
        {
            var imagePath = TiledImageBuilderService.GetTiledImage(boundingBox, tiles, totalSize, CreateTileFileName(boundingBox));
            return imagePath;
        }

        protected override int GetZoomLevel(IMapCoordinateBounds boundingBox)
        {
            int i;

            for (i = 1; i < MaxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right) * (1 << i) / 360D;
                if (tilesAcross > MinimumTilesAcross) break;
            }
            return i;
        }
    }
}