﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Xml;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.ImporterExtensions.WeatherImporter
{
    [DepictionDefaultImporterMetadata("UnitedStatesNationalWeatherImporter", Description = "Get the weather forecast from the region",
        DisplayName = "Weather Forecast (NOAA 24-hour)", ElementTypesSupported = new[] { "Depiction.Plugin.USNationalWeatherService24HourForecast" })]
    public class UnitedStatesNationalWeatherGatherer : AbstractDepictionDefaultImporter
    {

        public const string importerName = "Weather Forecast (NOAA 24-hour)";
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new WeatherForecastProviderService();

            var name = string.Format("Getting {0}", importerName);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }

    public class WeatherForecastProviderService : BaseDepictionBackgroundThreadOperation
    {
        private const int maxPointsPerQuery = 30;
        private DateTime forecastDate;
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
            var elementArray = GatherWeatherElements(regionBounds);
            if(parameters.ContainsKey("Tag"))
            {
                var tag = parameters["Tag"].ToString();
                foreach(var prototype in elementArray)
                {
                    prototype.Tags.Add(tag);
                }
            }
            return elementArray;
        }

        protected override void ServiceComplete(object args)
        {
            var list = args as List<IElementPrototype>;
            if (list != null && list.Count > 0 && DepictionAccess.CurrentDepiction != null)
                DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(list, true,false);

        }

        #region helper methods
        private IElementPrototype BuildWeatherElement(XmlReader xmlReader)
        {
            //This doesn't play well with updating
            var weatherPrototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.USNationalWeatherService24HourForecast");
            var newName = string.Format("24-hour forecast as of {0}", forecastDate.ToString("h:mm tt d MMMM yyyy"));
          
          if (weatherPrototype.SetPropertyValue("DisplayName", newName) !=true)
            {
                var nameProp = new DepictionElementProperty("DisplayName", "Name", newName, null, null);
                nameProp.VisibleToUser = true;
                nameProp.Deletable = false;
                nameProp.Editable = true;
                nameProp.IsHoverText = true;
                weatherPrototype.AddPropertyOrReplaceValueAndAttributes(nameProp);
            }

            while (xmlReader.Read() && !(xmlReader.Name == "app:Forecast_Gml2Point" && xmlReader.NodeType == XmlNodeType.EndElement))
            {
                if (xmlReader.NodeType != XmlNodeType.Element) continue;

                switch (xmlReader.Name)
                {
                    case "app:maximumTemperature":
                        var maxTemp = new Temperature(MeasurementSystem.Imperial, MeasurementScale.Normal, xmlReader.ReadElementContentAsDouble());
                        if (maxTemp.GetInitialValue() == 9999) return null;
                        weatherPrototype.SetPropertyValue("maximumTemperature", maxTemp);
                        break;
                    case "app:minimumTemperature":
                        var minTemp = new Temperature(MeasurementSystem.Imperial, MeasurementScale.Normal, xmlReader.ReadElementContentAsDouble());
                        weatherPrototype.SetPropertyValue("minimumTemperature", minTemp);
                        break;
                    case "app:rainAmount6Hourly":
                        var rainAmount6Hour = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, xmlReader.ReadElementContentAsDouble());
                        weatherPrototype.SetPropertyValue("rainAmount6Hourly", rainAmount6Hour);
                        break;
                    case "app:probOfPrecip12hourly":
                        weatherPrototype.SetPropertyValue("probOfPrecip12hourly", xmlReader.ReadElementContentAsDouble());
                        break;
                    case "app:windSpeed":
                        weatherPrototype.SetPropertyValue("windSpeed", xmlReader.ReadElementContentAsDouble());
                        break;
                    case "app:windDirection":
                        weatherPrototype.SetPropertyValue("windDirection", xmlReader.ReadElementContentAsDouble());
                        break;
                    case "app:weatherPhrase":
                        weatherPrototype.SetPropertyValue("weatherPhrase", xmlReader.ReadElementContentAsString());
                        break;
                    case "gml:coordinates":
                        string[] latLon = xmlReader.ReadElementContentAsString().Split(',');
                        var location = new LatitudeLongitude(latLon[1], latLon[0]);
                        weatherPrototype.SetInitialPositionAndZOI(new LatitudeLongitude(latLon[1], latLon[0]), null);
                        var eid = "Weather at :" + location.ToXmlSaveString();
                        if(weatherPrototype.SetPropertyValue("eid", eid) == false)
                        {
                            var eidProp = new DepictionElementProperty("eid", "Element user ID", eid, null, null);
                            eidProp.VisibleToUser = true;
                            eidProp.Deletable = false;
                            eidProp.Editable = false;
                            weatherPrototype.AddPropertyOrReplaceValueAndAttributes(eidProp);
                        }
                        break;
                }
            }
            weatherPrototype.UsePropertyNameInHoverText = true;
            return weatherPrototype;
        }

        public List<IElementPrototype> GatherWeatherElements(IMapCoordinateBounds area)
        {
            LatitudeLongitude[][] weatherPointsToRetrieve = splitRegionIntoGrid(area);
            var weatherElements = new List<IElementPrototype>();
            UpdateStatusReport(string.Format("Getting weather elements"));
            foreach (var weatherPoints in weatherPointsToRetrieve)
            {
                string weatherPointsString = string.Empty;

                foreach (LatitudeLongitude weatherPoint in weatherPoints)
                {
                    weatherPointsString += String.Format(CultureInfo.InvariantCulture, "{0},{1} ", weatherPoint.Latitude, weatherPoint.Longitude);
                }

                forecastDate = DateTime.Now;
                if (ServiceStopRequested) return null;
                string requestUrl = String.Format(CultureInfo.InvariantCulture, "http://www.weather.gov/forecasts/xml/OGC_services/ndfdOWSserver.php?SERVICE=WFS&Request=GetFeature&VERSION=1.0.0&latLonList={0}&TYPENAME=Forecast_Gml2Point&PropertyName=maxt,mint,pop12,wspd,wdir,wx,qpf", weatherPointsString.Trim());
                weatherElements.AddRange(GetWeatherElementsFromWeb(requestUrl, 0));
            }

            return weatherElements;
        }

        private List<IElementPrototype> GetWeatherElementsFromWeb(string url, int exceptionCount)
        {
            var webClient = new WebClient();
            var buffer = new byte[0];

            try
            {
                buffer = webClient.DownloadData(url);
            }
            catch (WebException)
            {
                if (ServiceStopRequested) return null;
                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    return GetWeatherElementsFromWeb(url, exceptionCount + 1);
            }

            if (ServiceStopRequested) return null;

            var elements = new List<IElementPrototype>();

            using (var memoryStream = new MemoryStream(buffer))
            {
                try
                {
                    using (var xmlReader = new XmlTextReader(memoryStream))
                    {
                        while (xmlReader.Read())
                        {
                            //                            if (IsCanceled) return null;
                            if (xmlReader.Name != "app:Forecast_Gml2Point") continue;
                            var elementToAdd = BuildWeatherElement(xmlReader);
                            if (elementToAdd == null) continue;
                            elements.Add(elementToAdd);
                        }
                    }
                }
                catch
                {
                    // Sometimes an exception here may be a web request that the server didn't handle correctly.
                    // Retry it a few times for each set just in case ...
                    if (exceptionCount < 5)
                        return GetWeatherElementsFromWeb(url, exceptionCount + 1);
                }
            }

            return elements;
        }

        private LatitudeLongitude[][] splitRegionIntoGrid(IMapCoordinateBounds region)
        {
            var weatherPointSet = new List<LatitudeLongitude>();
            var weatherPointSets = new List<LatitudeLongitude[]>();
            var widthDistance = Math.Abs(region.Right - region.Left);
            var heightDistance = Math.Abs(region.Bottom - region.Top);

            double columns = Math.Ceiling(Math.Log(widthDistance + 1, 1.2)) + 1;
            double columnWidth = widthDistance / columns;
            double rows = Math.Ceiling(Math.Log(heightDistance + 1, 1.2)) + 1;
            double rowHeight = heightDistance / rows;

            //            double columns = Math.Ceiling(Math.Log(region.Width + 1, 1.2)) + 1;
            //            double columnWidth = region.Width / columns;
            //            double rows = Math.Ceiling(Math.Log(region.Height + 1, 1.2)) + 1;
            //            double rowHeight = region.Height / rows;

            int setCount = 0;
            for (int col = 1; col < columns; col++)
                for (int row = 1; row < rows; row++)
                {
                    weatherPointSet.Add(new LatitudeLongitude(region.Bottom + row * rowHeight, region.Left + col * columnWidth));
                    setCount++;

                    if (setCount < maxPointsPerQuery) continue;

                    setCount = 0;
                    weatherPointSets.Add(weatherPointSet.ToArray());
                    weatherPointSet.Clear();
                }

            if (weatherPointSet.Count > 0)
                weatherPointSets.Add(weatherPointSet.ToArray());

            return weatherPointSets.ToArray();
        }
        #endregion
    }
}