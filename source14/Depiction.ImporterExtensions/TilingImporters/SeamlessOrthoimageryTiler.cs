﻿using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.ImporterExtensions.TilingImporters.TileImporterBases;

namespace Depiction.ImporterExtensions.TilingImporters
{

    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("SeamlessOrthoimageryTileImporter", Description = "Imagery (HRO Seamless)",
    //    DisplayName = "Imagery (HRO Seamless)", ImporterSources = new[] { InformationSource.Web })]
    public class SeamlessOrthoimageryTiler : SeamlessTilerBase
    {
        public SeamlessOrthoimageryTiler()
        {
            tileCacheService = new TileCacheService(SourceName);
        }

        public override string SourceName
        {
            get { return "HRO Seamless"; }
        }

        public override string DisplayName
        {
            get
            {
                return "Imagery (Seamless Urban)";
            }
        }

        public override string LegacyImporterName
        {
            get { return "SeamlessOrthoimageryTileImporter"; }
        }

        protected override string URL { get { return "http://raster.nationalmap.gov/arcgis/services/Orthoimagery/USGS_EDC_Ortho_HRO/ImageServer/WMSServer"; } }
        protected override string layerName { get { return "0"; } }
        protected override int MaxZoomLevel { get { return 20; } }

    }
    
  
    
}
