﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged.Service;

namespace Depiction.ImporterExtensions.TilingImporters.TileImporterBases
{
    public abstract class SeamlessTilerBase : ITiler
    {
        protected abstract string URL { get; }
        protected abstract string layerName { get; }
        public abstract string DisplayName { get; }
        public abstract string SourceName { get; }
        public abstract string LegacyImporterName { get; }
        public int PixelWidth { get { return 256; } }
        public bool DoesOwnCaching { get { return false; } }
        protected abstract int MaxZoomLevel { get; }
        protected const double EARTHCIRCUM = EARTHRADIUS * 2.0 * Math.PI;
        protected const double EARTHHALFCIRC = EARTHCIRCUM / 2;
        protected const double EARTHRADIUS = 6378137;
        protected TileCacheService tileCacheService;
        

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            double widthInDegrees = TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel) - TileColToTopLeftLong(tileToGet.Column, zoomLevel);
            double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);
            var tile = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                            null,
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel), TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
            tile.FetchStrategy = TileFetchStrategy;
            return tile;
        }

        public string CreateTileFileName(IMapCoordinateBounds bounds)
        {
            var fileName = SourceName;
            var boundString = string.Format("_{0:0.##}_{1:0.##}_to_{2:0.##}_{3:0.##}", bounds.Top, bounds.Left, bounds.Bottom, bounds.Right);
            fileName += boundString;
            return fileName + ".jpg";
        }
        public TileImageTypes TileImageType { get { return TileImageTypes.Satellite; } }


        public int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i;

            for (i = 1; i < MaxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right) * (1 << i) / 360D;
                if (tilesAcross > minTilesAcross) break;
            }
            return i;
        }

        private static double RadToDeg(double d)
        {
            return d * 180 / Math.PI;
        }


        private static double DegToRad(double d)
        {
            return d * Math.PI / 180.0;
        }


        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoomLevel);
            double metersX = (eqMetersPerTile * col) - EARTHHALFCIRC;
            double lonRad = metersX / EARTHRADIUS;
            return RadToDeg(lonRad);
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double metersY = EARTHHALFCIRC - (row * eqMetersPerTile);
            double latRad = Math.Atan(Math.Sinh(metersY / EARTHRADIUS));
            double latDeg = RadToDeg(latRad);
            return latDeg;
        }


        public int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / ((1 << zoom));
            double metersX = EARTHRADIUS * DegToRad(latLong.Longitude);
            var x = (int)((EARTHHALFCIRC + metersX) / eqMetersPerTile);
            return x;
        }

        public int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double latRad = DegToRad(latLong.Latitude);
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double prj = Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad));
            double metersY = EARTHRADIUS * prj;
            var y = (int)((EARTHHALFCIRC - metersY) / eqMetersPerTile);
            return y;
        }


        public IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
            int col = LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastCol = LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);
            int lastRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);

            foreach (var tileToGet in tilesToGet)
            {

                var tile = GetTileModel(tileToGet, zoomLevel);
                tiles.Add(tile);
            }

            return tiles;
        }

        private byte[] TileFetchStrategy(TileModel model)
        {
            WmsDataProvider wmsDataProvider = new WmsDataProvider(URL, layerName, new MapCoordinateBounds(model.TopLeft, model.BottomRight), "image/png");
            wmsDataProvider.ImageHeight = wmsDataProvider.ImageWidth = 256;
            var response = wmsDataProvider.GetData();
            if (response.ResponseFile == null) return null;
            return File.ReadAllBytes(response.ResponseFile);
        }

        //<summary>
        //Returns the list of tiles to get ordered in increasing distance from the center
        //of the area being tiled.
        //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles =
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }
    }
}