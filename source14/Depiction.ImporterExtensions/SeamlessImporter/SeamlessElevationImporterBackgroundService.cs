﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.Service.FileReading;

namespace Depiction.ImporterExtensions.SeamlessImporter
{
    class SeamlessElevationImporterBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Seamless Elevation Data");
            return true;            
        }

        private string elevationUnit = "m";
        private int currentPiece;
        private int numberOfPieces;
        protected override object ServiceAction(object args)
        {
            List<IDepictionElement> elements = null;
            var argDictionary = args as Dictionary<string, object>;
            if (argDictionary == null) return null;
            if (!argDictionary.ContainsKey("RegionBounds")) return null;
            var area = argDictionary["RegionBounds"] as IMapCoordinateBounds;
            string layer = "N1G";
            string requestUrl =
               string.Format("http://extract.cr.usgs.gov/requestValidationServiceClient/sampleRequestValidationServiceProxy/getTiledDataDirectURLs2.jsp?TOP={0}&BOTTOM={1}&LEFT={2}&RIGHT={3}&LAYER_IDS={4}&JSON=true", 
               area.Top, area.Bottom, area.Left, area.Right, layer);


            var buffer = DownloadString(requestUrl, 0);
            //TerrainData terrainData = new TerrainData();

            TileCacheService tileCacheService = new TileCacheService("SeamlessElevation");

            var collection = Regex.Matches(buffer, @"""DOWNLOAD_URL"":""(?'url'[^""]*)""");

            currentPiece = 1;
            numberOfPieces = collection.Count;
            foreach (Match match in collection)
            {
                string url = match.Groups["url"].Value;
                string filename = Regex.Match(url, @"FNAME=(.*zip)").Groups[1].Value;
                string filePath = tileCacheService.GetCacheFullStoragePath(filename);
                if (!File.Exists(filePath))
                {
                    UpdateStatusReport(string.Format("Downloading data piece {0} of {1}", currentPiece, numberOfPieces));
                    DownloadFile(url, filePath, 0);
                }

                var archiveDir = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, Path.GetFileNameWithoutExtension(filename));
                if (!Directory.Exists(archiveDir))
                {
                    DepictionPersistenceHelper.UnzipFileToDirectory(filePath, archiveDir);
                }
                var files = Directory.GetFiles(archiveDir, "w001001.adf", SearchOption.AllDirectories);

                var elevationDataFileName = files[0];

                {
                    try
                    {
                        
                        var terrainConverter = new CoverageDataConverter(elevationDataFileName, elevationUnit, area, true);

                        UpdateStatusReport(string.Format("Converting data piece {0} of {1} to elevation", currentPiece, numberOfPieces));
                        elements = terrainConverter.ConvertDataToElements();
                        foreach (var element in elements)
                        {
                            element.Tags.Add("Elevation - Seamless");
                        }
//                        AddElevationElementToDepiction(elements);
                    }
                    catch
                    {
                        return null;
                    }
                }
                currentPiece++;
            }
            return elements;
        }

        protected override void ServiceComplete(object args)
        {
            UpdateStatusReport("Adding element to depiction");
            var elementsEnum = args as IEnumerable<IDepictionElement>;
            if (elementsEnum == null) return;

            var elements = elementsEnum.ToList();
            AddElevationElementToDepiction(elements);
        }

        private void AddElevationElementToDepiction(List<IDepictionElement> elements)
        {
            UpdateStatusReport("Adding element to depiction");
            if (elements == null) return;
            if (elements.Any() && DepictionAccess.CurrentDepiction != null)
            {
                //Temp hack to make sure multiple elevation revealers don't get created.
                var ele = DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("Elevation");
                if (ele == null)
                {
                    switch (DepictionAccess.ProductInformation.ProductType)
                    {
                        default: 
                            DepictionAccess.CurrentDepiction.CreateAndAddRevealer(elements, "Elevation - Seamless", null);
                           
//                            DepictionAccess.CurrentDepiction.AddElementToDepictionElementList(elements.First(), true);
                            break;
                    }
                }
            }
        }

        private void DownloadFile(string url, string fileName, int exceptionCount)
        {
            var webClient = new WebClient();

            UpdateStatusReport(string.Format("Downloading data piece {0} of {1}", currentPiece, numberOfPieces));
            try
            {
                webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
                Task.WaitAll(webClient.DownloadFileTaskAsync(url, fileName));
            }
            catch (WebException)
            {

                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    DownloadFile(url, fileName, exceptionCount + 1);
            }

        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            UpdateStatusReport(string.Format("Downloading data piece {0} of {1} ({2}%)", currentPiece, numberOfPieces, e.ProgressPercentage));
        }

        private string DownloadString(string url, int exceptionCount)
        {

            
            var webClient = new WebClient();
            string buffer = "";

            try
            {
                
                buffer = webClient.DownloadString(url);
            }
            catch (WebException)
            {
                //if (ServiceStopRequested) return null;
                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    return DownloadString(url, exceptionCount + 1);
            }
            //if (ServiceStopRequested) return null;
            return buffer;
        }
    }
}
