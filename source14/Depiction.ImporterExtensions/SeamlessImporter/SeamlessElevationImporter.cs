﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.ImporterExtensions.SeamlessImporter
{
    [DepictionDefaultImporterMetadata("DepictionSeamlessImporter", DisplayName = "Elevation Data (Seamless)",
        Description = "The U.S. Geological Survey (USGS) provides elevation data across the U.S. at varying levels of detail via the National Elevation Dataset (NED). \"30m\" resolution provides the elevation above sea level at a point every 30 meters. Additional data can be found at http://seamless.usgs.gov")]
    public class SeamlessElevationImporter : AbstractDepictionDefaultImporter
    {
        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new SeamlessElevationImporterBackgroundService();
            var name = string.Format("Elevation Data (Seamless)");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }
}
