﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.ImporterExtensions
{
    [DepictionNonDefaultImporterMetadata("OpenStreetMapHospitalImporter", 
        ImporterSources = new[] { InformationSource.Web },
        Description = "Gets hospitals from OpenStreetMap",
        DisplayName = "Hospitals (OpenStreetMap)")]
    public class OSMHospitalImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "hospital"; } }
        protected override string ElementType { get { return "Depiction.Plugin.Hospital"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapPoliceStationImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "Gets police stations from OpenStreetMap",
        DisplayName = "Police Stations (OpenStreetMap)")]
    public class OSMPoliceImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "police"; } }
        protected override string ElementType { get { return "Depiction.Plugin.PoliceStation"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapFireStationImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "Gets fire stationsfrom OpenStreetMap",
        DisplayName = "Fire Stations (OpenStreetMap)")]
    public class OSMFireStationImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "fire_station"; } }
        protected override string ElementType { get { return "Depiction.Plugin.FireStation"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapSchoolImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "Gets schools from OpenStreetMap",
        DisplayName = "Schools (OpenStreetMap)")]
    public class OSMSchoolImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "school"; } }
        protected override string ElementType { get { return "Depiction.Plugin.School"; } }
    }

    public abstract class OSMPointDataImporter : NondefaultDepictionImporterBase
    {
        protected abstract string LayerName { get; }
        protected abstract string ElementType { get; }
        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {

            var operationParams = new Dictionary<string, object>();
            operationParams.Add("Area", depictionRegion);
            operationParams.Add("layerName", LayerName);
            operationParams.Add("elementType", ElementType);
            foreach (var param in parameters)
            {
                operationParams.Add(param.Key, param.Value);
            }
            var streetmapService = new OpenStreetMapPointDataImporterService();
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(streetmapService);
            streetmapService.StartBackgroundService(operationParams);
        }
    }
}
