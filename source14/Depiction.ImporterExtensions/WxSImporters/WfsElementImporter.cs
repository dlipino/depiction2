﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.Service.CoreBackgroundServices;

namespace Depiction.ImporterExtensions.WxSImporters
{
    [DepictionDefaultImporterMetadata("WfsElementGatherer", Description = "Gets Wfs element data", DisplayName = "Default Wfs Element Importer")]
    public class WfsElementImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object dataLocationInfo, string defaultElementType, IMapCoordinateBounds area, Dictionary<string, string> parameters)
        {
            var operationParams = new Dictionary<string, object>();
            operationParams.Add("Area", area);
            operationParams.Add("ServiceType", "WFS");
            foreach (var pair in parameters)
            {
                operationParams.Add(pair.Key, pair.Value);
            }
            var background = new WfsElementProviderBackground();

            var name = "Wfs Element Importer";
            if (parameters.ContainsKey("name"))
            {
                name = parameters["name"];
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(background);
            background.UpdateStatusReport(name);
            background.StartBackgroundService(operationParams);
        }
    }
}