﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.APIUnmanaged.Service;

namespace Depiction.ImporterExtensions.WxSImporters
{
    [DepictionDefaultImporterMetadata("WmsElementImporter", Description = "Gets Wms data from the web", DisplayName = "Default WMS element importer")]
    class WmsElementImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object elementLocation, string defaultElementType,IMapCoordinateBounds area, Dictionary<string, string> parameters)
        {
            var operationParams = new Dictionary<string, object>();
            operationParams.Add("Area", area);
            foreach (var pair in parameters)
            {
                operationParams.Add(pair.Key, pair.Value);
            }
            var background = new WmsElementProviderBackground();
                
            var name = "Wms Element Gatherer";
            if (parameters.ContainsKey("name"))
            {
                name = parameters["name"];
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(background);
            background.UpdateStatusReport(name);
            background.StartBackgroundService(operationParams);
        }
    }

    
}