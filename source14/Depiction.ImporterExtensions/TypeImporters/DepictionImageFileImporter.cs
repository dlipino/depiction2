﻿using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.HelperClasses;

namespace Depiction.ImporterExtensions.TypeImporters
{
    [DepictionDefaultImporterMetadata("ImageReader",
        new[] { ".jpg", ".jpeg",".bmp", ".png", ".tif", ".tiff", ".gif", ".giff" },
        new[] { "Depiction.Plugin.AutoDetect", "Depiction.Plugin.Image", "Depiction.Plugin.Image2" },
        FileTypeInformation = "Depiction image types",
        DisplayName = "Default Depiction image importer",
        ElementTypesNotSupported = new[] { "Depiction.Plugin.Elevation" })]
    //clearly the supported element types is not working as expected. and the whole importer thing is a mess
    public class DepictionImageFileImporter : AbstractDepictionDefaultImporter
    {

        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            var fileName = elementLocation.ToString();
            if (!File.Exists(fileName)) return;
            if(defaultElementType.Contains("AutoDetect"))
            {
                defaultElementType = "Depiction.Plugin.Image";
            }
            var imageAddingService = new DepictionImageElementAddingService(defaultElementType);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(imageAddingService);
            var name = string.Format("Reading image : {0}", Path.GetFileName(fileName));
            imageAddingService.UpdateStatusReport(name);
            imageAddingService.StartBackgroundService(fileName);
        }

    }
}