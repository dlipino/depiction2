﻿using System;
using System.Collections.Generic;
using System.Linq;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.Service.FileReading;

namespace Depiction.ImporterExtensions.TypeImporters
{
    [DepictionDefaultImporterMetadata("DepictionElevationFileReader", 
        new[] { ".adf", ".tiff", ".tif", ".gtiff", ".hgt", ".dem", ".bt" },
        new[] { "Depiction.Plugin.Elevation", "Depiction.Plugin.AutoDetect" },
        FileTypeInformation="Elevation",DisplayName = "Depiction elevation file reader",
        ElementTypesNotSupported = new[] { "Depiction.Plugin.Image" })]
    public class DepictionElevationFileImporter : AbstractDepictionDefaultImporter
    {
        #region Implementation of IDepictionDefaultImporter

        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            if (elementLocation == null) return;
            var fileName = elementLocation.ToString();
            var parameters = ConvertStringStringDictionayrToStringObject(inParameters);
            parameters.Add("Area", depictionRegion);
            parameters.Add("fileName", fileName);
            var elevationProvider = new DepictionElevationElementProvider();

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(elevationProvider);
            elevationProvider.UpdateStatusReport("Starting elevation importer");
            elevationProvider.StartBackgroundService(parameters);
        }

        #endregion

        private Dictionary<string, object> ConvertStringStringDictionayrToStringObject(Dictionary<string, string> param)
        {
            var parameters = new Dictionary<string, object>();
            if (param != null)
            {
                foreach (var pair in param)
                {
                    parameters.Add(pair.Key, pair.Value);
                }
            }
            return parameters;
        }
    }

    public class DepictionElevationElementProvider : BaseDepictionBackgroundThreadOperation
    {
        private string elevationUnit = "m";

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            bool unitSpecified = false;
            UpdateStatusReport("Creating elevation element");
            if (args.ContainsKey("units"))
            {
                unitSpecified = true;
                elevationUnit = args["units"].ToString();
            }
            if (!unitSpecified)
            {
                var inMeters =
                    DepictionMessageService.DisplayModalYesNoMessageBox(
                        "Depiction only supports units of METERS or FEET for elevation data. Does this file contain elevation in METERS?",
                        "Elevation data units");
                if (inMeters == null) return false;
                elevationUnit = ((bool)inMeters) ? "m" : "ft";
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            try
            {
                var fileName = parameters["fileName"] as string;
                var depictionRegion = parameters["Area"] as IMapCoordinateBounds;

                CoverageDataConverter terrainConverter = new CoverageDataConverter(fileName, elevationUnit, depictionRegion, true);

                UpdateStatusReport("Converting data to elevation");
                var elements = terrainConverter.ConvertDataToElements();
                if(parameters.ContainsKey("Tag"))
                {
                    var tag = parameters["Tag"] as string;
                    if (!string.IsNullOrEmpty(tag))
                    {
                        foreach (var element in elements)
                        {
                            element.Tags.Add(tag);
                        }
                    }
                }
                return elements;
            }
            catch(Exception ex)
            {
                UpdateStatusReport("Error creating elevation. See error log for details.");
                DepictionExceptionHandler.HandleException(ex, false, true);
                return null;
            }
        }

        protected override void ServiceComplete(object args)
        {
            UpdateStatusReport("Adding element to depiction");
            var elementsEnum = args as IEnumerable<IDepictionElement>;
            if (elementsEnum == null) return;
            var elements = elementsEnum.ToList();
            if (elements.Any() && DepictionAccess.CurrentDepiction != null)
            {
                //Temp hack to make sure multiple elevation revealers don't get created.
                var ele = DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("Elevation");
                if(ele == null)
                {
                    switch (DepictionAccess.ProductInformation.ProductType)
                    {
                        case ProductInformationBase.Prep:
                            DepictionAccess.CurrentDepiction.AddElementToDepictionElementList(elements.First(), false);
                            break;
                        default:
                            DepictionAccess.CurrentDepiction.CreateAndAddRevealer(elements, "Elevation", null);
                            break;
                    }
                }
//                var geoBounds = MapCoordinateBounds.GetGeoRectThatIsPercentage(
//                    DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds,
//                    .8,null);
//
//                //Order is important, for now
//                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, false);
//                var revealer = new DepictionRevealer("Elevation", geoBounds);
//                DepictionAccess.CurrentDepiction.AddRevealer(revealer);
//                revealer.AddElementList(elements);
            }
        }

        #endregion
    }
}