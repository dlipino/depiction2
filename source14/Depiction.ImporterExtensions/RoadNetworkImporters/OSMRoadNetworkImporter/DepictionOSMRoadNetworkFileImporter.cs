﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.ElementLibrary;
using Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter.OSMObjects;

namespace Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter
{
    [DepictionDefaultImporterMetadata("DepictionOSMRoadNetworkFileImporter", new[] { ".xml" },
        FileTypeInformation = "Roadnetwork files", DisplayName = "Depiction roadnetwork reader.",
        ElementTypesSupported = new[] { "Depiction.Plugin.RoadNetwork", "Depiction.Plugin.AutoDetect" })]
    public class DepictionOSMRoadNetworkFileImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            if (elementLocation == null) return;
            var fileName = elementLocation.ToString();
            var parameters = new Dictionary<string, object>();
            parameters.Add("FileName", fileName);
            parameters.Add("ElementType", defaultElementType);
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new OSMRoadNetworkFileReaderService();
            var name = string.Format("Reading file : {0}", Path.GetFileName(fileName));

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }

    public class OSMRoadNetworkFileReaderService : BaseDepictionBackgroundThreadOperation
    {
        readonly Dictionary<int, OSMNode> serviceNodes = new Dictionary<int, OSMNode>();
        readonly Dictionary<int, OSMWay> serviceWays = new Dictionary<int, OSMWay>();
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var fileName = parameters["FileName"].ToString();
            var elementType = parameters["ElementType"].ToString();
            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;

            if (string.IsNullOrEmpty(fileName)) return null;

            try
            {
                OSMNode[] nodesInFile;
                OSMWay[] waysInFile;
                OSMHelperMethods.GetNodesAndWays(fileName, out nodesInFile, out waysInFile, this);

                lock (serviceNodes)
                {
                    foreach (var node in nodesInFile)
                    {
                        if (ServiceStopRequested) break;
                        if (!serviceNodes.ContainsKey(node.NodeID))
                            serviceNodes.Add(node.NodeID, node);
                    }
                }

                lock (serviceWays)
                {
                    foreach (var way in waysInFile)
                    {
                        if (ServiceStopRequested) break;
                        if (!serviceWays.ContainsKey(way.WayID))
                            serviceWays.Add(way.WayID, way);
                    }
                }
                var taggedList = OSMHelperMethods.GetTaggedEdgeListFromOSMData(serviceNodes, serviceWays, this);
                if (taggedList == null) return null;


                var element = GeneratePrimaryRoadNetworkElement(taggedList,
                                                                string.Format("Roadnetwork : {0}",
                                                                              Path.GetFileName(fileName)));
                return element;
            }
            catch (Exception ex)
            {
                // Don't keep a cached file that failed to process.
                DepictionExceptionHandler.HandleException(ex, true, true);
            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            var element = args as IDepictionElement;
            if (element == null) return;

            if (DepictionAccess.CurrentDepiction != null)
            {
                UpdateStatusReport("Adding roadnetwork to depiction");
                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new[] { element }, false);
                //DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, true);
            }
        }

        #endregion
        #region Element creation helpers

        public IDepictionElement GeneratePrimaryRoadNetworkElement(IEnumerable<RoadSegment> segments, string displayName)
        {
            RoadGraph primaryRoadGraph = null;
            var roadNetwork =
                DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("PrimaryRoadNetwork");
            if (roadNetwork == null)
            {
                var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.RoadNetwork");
                roadNetwork = ElementFactory.CreateElementFromPrototype(prototype);
            }
            else
            {
                if (!roadNetwork.GetPropertyValue("RoadGraph", out primaryRoadGraph))
                {
                    primaryRoadGraph = null;
                }
            }
            if (roadNetwork == null) return null;
            roadNetwork.SetPropertyValue("displayname", displayName);
            if (ServiceStopRequested) return null;

            var roadGraph = AugmentExistingRoadGraph(segments, primaryRoadGraph);
            //This might be redundent
            roadNetwork.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RoadGraph", string.Empty, roadGraph, null, null) { VisibleToUser = false });
            if (ServiceStopRequested) return null;
            var geom = roadGraph.ConvertToGeometry();
            if (geom == null) return null;

            if (primaryRoadGraph == null)
            {
                var zoi = new ZoneOfInfluence(geom);
                if (zoi.IsEmpty) return null;
                roadNetwork.SetInitialPositionAndZOI(null, zoi);
            }
            else
            {
                roadNetwork.SetZOIGeometryAndUpdatePosition(geom);
            }
            roadNetwork.Tags.Add("PrimaryRoadNetwork");
            // if (roadNetwork.ZoneOfInfluence.IsEmpty) return null;
            if (ServiceStopRequested) return null;
            return roadNetwork;
        }
        protected RoadGraph AugmentExistingRoadGraph(IEnumerable<RoadSegment> roadSegments, RoadGraph startingRoadGraph)
        {
            var roadGraph = startingRoadGraph;
            if (roadGraph == null)
                roadGraph = new RoadGraph();

            //construct the graph from road segments
            foreach (var ls in roadSegments)
            {
                roadGraph.AddVertex(ls.Source);
                roadGraph.AddVertex(ls.Target);
                roadGraph.AddEdge(ls);
            }
            return roadGraph;
        }
        #endregion


       
    }
}