using System;
using System.Collections.Generic;

namespace Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter.OSMObjects
{
    public class OSMWay
    {
        public int WayID { get; set; }
        public DateTime TimeStamp { get; set; }
        public string User { get; set; }
        public int[] NodeIDs { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}