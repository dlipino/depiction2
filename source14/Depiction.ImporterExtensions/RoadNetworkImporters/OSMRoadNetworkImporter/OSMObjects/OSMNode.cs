using System.Collections.Generic;
using Depiction.API.ValueTypes;

namespace Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter.OSMObjects
{
    public class OSMNode
    {
        public int NodeID { get; set; }
        public LatitudeLongitude LatLong { get; set; }
        public Dictionary<string, string> Tags { get; set; }
        //public string User { get; set; }
        //public DateTime TimeStamp { get; set; }
    }
}