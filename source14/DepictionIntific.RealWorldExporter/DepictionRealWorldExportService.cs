﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Xml;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.DepictionComparers;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.TypeConverter;
using OSGeo.GDAL;
using OSGeo.OGR;
using OSGeo.OSR;
using ProjNet.Converters.WellKnownText;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;
using Driver = OSGeo.OGR.Driver;
using GDAL = OSGeo.GDAL;

namespace DepictionIntific.RealWorldExporter
{
    public class DepictionRealWorldExportService : BaseDepictionBackgroundThreadOperation
    {
        #region constants for the xml file
        const string TisProject = "TisTerrainProcessorProject";
        const string OutPutFile = "OutputFile";
        private const string Sources = "Sources";
        private const string Source = "Source";
        private const string ProjectConfig = "ProjectConfig";
        private const string Name = "Name";
        private const string ProcessConfig = "ProcessConfig";
        private const string RequiredElevation = "RequiredElevation";
        private const string Min = "Min";
        private const string Max = "Max";
        private const string RequiredSpatialBounds = "RequiredSpatialBounds";
        private const string SpatialRef = "SpatialRef";
        private const string MinX = "MinX";
        private const string MinY = "MinY";
        private const string MaxX = "MaxX";
        private const string MaxY = "MaxY";
        private const string Streams = "Streams";
        private const string RequiredMetersPerSample = "RequiredMetersPerSample";
        #endregion
        Dictionary<wkbGeometryType,DepictionGeometryType> wkbToDepictionTypeDictionary = new Dictionary<wkbGeometryType, DepictionGeometryType>
                                                                                             {
                                                                                                 {wkbGeometryType.wkbLineString, DepictionGeometryType.LineString},
                                                                                                 {wkbGeometryType.wkbMultiLineString, DepictionGeometryType.MultiLineString},
                                                                                                 {wkbGeometryType.wkbPolygon, DepictionGeometryType.Polygon},
                                                                                                 {wkbGeometryType.wkbMultiPolygon, DepictionGeometryType.MultiPolygon}
                                                                                             }; 
        private string message = "";
        #region Service method region
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            //
            var fileName = parameters["FileName"].ToString();
            var elementList = parameters["ElementList"] as IEnumerable<IDepictionElement>;
            WriteElementXMLAndFiles(fileName, elementList);
            return null;
        }

        protected override void ServiceComplete(object args)
        {

        }
        #endregion
        #region xml writer help region

        public static XmlWriter GetXmlWriter(Stream stream)
        {
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, CheckCharacters = false };
            return XmlWriter.Create(stream, settings);
        }

        #endregion

        #region base writer for the xml and elements (not sure if xml is even needed)
        public void WriteElementXMLAndFiles(string fileName, IEnumerable<IDepictionElement> elementList)
        {
            if (elementList == null || !elementList.Any()) return;

            if (string.IsNullOrEmpty(fileName)) return;

            var elevationElements = elementList.Where(t => t.ElementType.Contains("Plugin.Elevation"));
            var imageElements = elementList.Where(t => t.ElementType.Contains("Plugin.Image"));
            var roadElements = elementList.Where(t => t.ElementType.Contains("Plugin.RoadNetwork"));

            //Limit shape file output to loaded and user drawn shapes (ie no real elements like waterbodies etc)
            var shapeElements = elementList.Where(t => (t.ElementType.Contains("UserDrawnPolygon") || t.ElementType.Contains("UserDrawnLine") ||
                            t.ElementType.Contains("LoadedPolygon") || t.ElementType.Contains("LoadedLines")));

            var homePath = Path.GetDirectoryName(fileName);
            if (string.IsNullOrEmpty(homePath)) return;
            using (var stream = new FileStream(fileName, FileMode.Create))
            {
                CultureInfo culture = Thread.CurrentThread.CurrentCulture;
                try
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                    using (var writer = GetXmlWriter(stream))
                    {
                        writer.WriteStartElement(TisProject);

                        writer.WriteStartElement(OutPutFile);
                        var tpmExtension = ".tpm";
                        var fileNameNoExtension = Path.GetFileNameWithoutExtension(fileName);
                        writer.WriteString(fileNameNoExtension+tpmExtension);//how does this file appear? is it magic?
                        writer.WriteEndElement();//OutPutFile

                        writer.WriteStartElement(Sources);
                        #region Elevation writing
                        if (elevationElements.Any())
                        {
                            var elevationSaved = false;
                            var elevPath = Path.Combine(homePath, "Source", "Elevation");
                            if (!Directory.Exists(elevPath))
                            {
                                Directory.CreateDirectory(elevPath);
                            }
                            var count = 0;
                            foreach (var elevationElement in elevationElements)
                            {
                                var terrainProp = elevationElement.GetPropertyByInternalName("Terrain");
                                if (terrainProp == null) continue;
                                var value = terrainProp.Value as Terrain;
                                if (value == null) continue;
                                var name = "elevation.tif";
                                if (count != 0)
                                {
                                    name = string.Format("elevation_{0}.tif", count);
                                }
                                var outName = Path.Combine(elevPath, name);
                                //The exported elevation doesn't load quite right, might have something to do with warping
                                //or lack there of
                                value.SaveToGeoTiff(outName);
                                count++;
                                elevationSaved = true;
                            }

                            if (elevationSaved)
                            {
                                writer.WriteStartElement(Source);
                                writer.WriteString(".\\Source\\Elevation\\*.tif");
                                writer.WriteEndElement(); //Source
                            }
                        }
                        #endregion
                        #region Image exportring

                        if (imageElements.Any())
                        {
                            var updateImageDir = false;
                            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
                            var imagePath = Path.Combine(homePath, "Source", "Imagery");
                            if (!Directory.Exists(imagePath))
                            {
                                Directory.CreateDirectory(imagePath);
                            }
                            var imageCount = 0;
                            foreach (var imageElement in imageElements)
                            {
                                if (imageElement.ImageMetadata == null) continue;

                                var iconPath = imageElement.ImageMetadata.DepictionImageName;
                                var imageSource = DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath);
                                var fileNameWithTiffExtension = Path.GetFileNameWithoutExtension(iconPath.Path) + ".jpg";
                                var fullName = Path.Combine(imagePath, fileNameWithTiffExtension);
                                var newName = string.Format("geoTiff.tif");
                                if (imageCount > 0)
                                {
                                    newName = string.Format("geoTiff_{0}.tif", imageCount);
                                }
                                if (BitmapSaveLoadService.SaveBitmap(imageSource as BitmapSource, fullName))
                                {
                                    updateImageDir = true;
                                    WriteImageElementToGeoTiff(geoTiffDriver, imageElement, Path.Combine(imagePath, newName), fullName);
                                }
                            }

                            if (updateImageDir)
                            {
                                writer.WriteStartElement(Source);
                                writer.WriteString(".\\Source\\Imagery\\*.tif");
                                writer.WriteEndElement(); //Source
                            }
                        }
                        #endregion

                        #region shape region
                        if (shapeElements.Any())
                        {
                            var shapesSaved = false;
                            var shapePath = Path.Combine(homePath, "Source", "Shapes");
                            if (!Directory.Exists(shapePath))
                            {
                                Directory.CreateDirectory(shapePath);
                            }else
                            {
                                var files = Directory.GetFiles(shapePath);
                                foreach(var file in files)
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }catch{}
                                }
                            }
                            var shapeFileName = Path.Combine(shapePath, "depictionRealWorldShapes.shp");
                            WriteShapeElementsToSHPFile(shapeFileName, shapeElements);
                            writer.WriteStartElement(Source);
                            writer.WriteString(".\\Source\\Shapes\\*.shp");
                            writer.WriteEndElement(); //Source
//                            var shapeFileName = Path.Combine(shapePath, "depictionRealWorldShapes.gml");
//                            WriteShapeElementsGMLFile(shapeFileName, shapeElements);
//                            writer.WriteStartElement(Source);
//                            writer.WriteString(".\\Source\\Shapes\\*.gml");
//                            writer.WriteEndElement(); //Source
                        }
                        #endregion
                        #region road network saving

                        if (roadElements.Any())
                        {
                            var roadSaved = false;
                            var roadPath = Path.Combine(homePath, "Source", "Roads");
                            if (!Directory.Exists(roadPath))
                            {
                                Directory.CreateDirectory(roadPath);
                            }
                            else
                            {
                                var files = Directory.GetFiles(roadPath);
                                foreach (var file in files)
                                {
                                    try
                                    {
                                        File.Delete(file);
                                    }
                                    catch { }
                                }
                            }
                            foreach (var roadElement in roadElements)
                            {
                                WriteRoadNetworkToSHPFile(Path.Combine(roadPath, "roadnetwork.shp"), roadElement);
                                roadSaved = true;
                            }
                            if (roadSaved)
                            {
                                writer.WriteStartElement(Source);
                                writer.WriteString(".\\Source\\Roads\\*.shp");
                                writer.WriteEndElement(); //Source
                            }
//                            foreach (var roadElement in roadElements)
//                            {
//                                WriteRoadNetworkToGMLFile(Path.Combine(roadPath, "roadnetwork.gml"), roadElement);
//                                roadSaved = true;
//                            }
//                            if (roadSaved)
//                            {
//                                writer.WriteStartElement(Source);
//                                writer.WriteString(".\\Source\\Roads\\*.gml");
//                                writer.WriteEndElement(); //Source
//                            }

                        }
                        #endregion
                        #region Source type
//                        writer.WriteStartElement(Source);
//                        writer.WriteString("&lt; Material CommandID = \"ASSIGN_DEFAULT\" / &gt");
//                        writer.WriteEndElement(); //Source

                        #endregion
                        writer.WriteEndElement();//Sources

                        writer.WriteStartElement(ProjectConfig);
                        writer.WriteStartElement(Name);
                        writer.WriteString("E1M0_I1_R0");
                        writer.WriteEndElement();//Name
                        writer.WriteEndElement();//ProjectConfig
                        #region Option file stuff
                        /*
                        writer.WriteStartElement(ProcessConfig);

                        writer.WriteStartElement(RequiredElevation);
                        writer.WriteStartElement(Min);
                        writer.WriteValue(-10);
                        writer.WriteEndElement();//Min
                        writer.WriteStartElement(Max);
                        writer.WriteValue(1005.5);
                        writer.WriteEndElement();//Max
                        writer.WriteEndElement();//RequiredElevation

                        writer.WriteStartElement(RequiredSpatialBounds);
                        writer.WriteStartElement(SpatialRef);
                        writer.WriteString("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
                        writer.WriteEndElement();//SpatialRef
                        writer.WriteStartElement(MinX);
                        writer.WriteValue(-157.9040572);
                        writer.WriteEndElement();//MinX
                        writer.WriteStartElement(MinY);
                        writer.WriteValue(21.2521397);
                        writer.WriteEndElement();//MinY
                        writer.WriteStartElement(MaxX);
                        writer.WriteValue(-157.7910541);
                        writer.WriteEndElement();//MaxX
                        writer.WriteStartElement(MaxY);
                        writer.WriteValue(21.3599904);
                        writer.WriteEndElement();//MaxY
                        writer.WriteEndElement();//RequiredSpatialBounds

                        writer.WriteStartElement(Streams);
                        writer.WriteStartElement(RequiredMetersPerSample);
                        writer.WriteValue(4);
                        writer.WriteEndElement();//RequiredMetersPerSample
                        writer.WriteStartElement(RequiredMetersPerSample);
                        writer.WriteValue(1);
                        writer.WriteEndElement();//RequiredMetersPerSample
                        writer.WriteStartElement(RequiredMetersPerSample);
                        writer.WriteValue(.0125);
                        writer.WriteEndElement();//RequiredMetersPerSample
                        writer.WriteEndElement();//Streams

                        writer.WriteEndElement();//ProcessConfig
                        */
                        #endregion
                        writer.WriteEndElement();//TisTerrainProcessorProject
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = culture;
                }
            }
        }
        #endregion
        #region Writing to file
        #region Write gml file
        public void WriteRoadNetworkToGMLFile(string fileName, IDepictionElement roadNetworkElement)
        {
            Driver drv = Ogr.GetDriverByName("GML");
            DataSource ds = drv.CreateDataSource(fileName, new string[] { });
            var message = "Writing roadnetwork as linestring";
            UpdateStatusReport(message);
            var lineStringLayer = CreateLayer(wkbGeometryType.wkbMultiLineString25D, ds, "roadNetwork");
            AddElementsToLayerByGeometryType(lineStringLayer, DepictionGeometryType.MultiLineString, new List<IDepictionElement> { roadNetworkElement });
        }

        public void WriteShapeElementsGMLFile(string fileName, IEnumerable<IDepictionElement> elementList)
        {
            Driver drv = Ogr.GetDriverByName("GML");
            DataSource ds = drv.CreateDataSource(fileName, new string[] { });

            var message = "Writing line string layer 1/2";
            UpdateStatusReport(message);
            var lineStringLayer = CreateLayer(wkbGeometryType.wkbLineString, ds, "lineStringLayer");
            AddElementsToLayerByGeometryType(lineStringLayer, DepictionGeometryType.LineString, elementList);
            AddElementsToLayerByGeometryType(lineStringLayer, DepictionGeometryType.MultiLineString, elementList);

            message = "Writing line polygon layer 2/2";
            UpdateStatusReport(message);
            var polygonLayer = CreateLayer(wkbGeometryType.wkbPolygon, ds, "polygonLayer");
            AddElementsToLayerByGeometryType(polygonLayer, DepictionGeometryType.Polygon, elementList);
            AddElementsToLayerByGeometryType(polygonLayer, DepictionGeometryType.MultiPolygon, elementList);

            ds.Dispose();
            if (ServiceStopRequested)
            {//I think this deletes all the associated gml files
                drv.DeleteDataSource(fileName);
            }
        }
        #endregion
        #region Write shp file
        public void WriteShapeElementsToSHPFile(string fileName, IEnumerable<IDepictionElement> elementList)
        {
            Driver drv = Ogr.GetDriverByName("ESRI Shapefile");
            if (drv == null) return;
            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            DataSource ds = drv.CreateDataSource(fileName, null);
            //Create a dummy layer to hold the element property and name info? maybe?
            var lineStringlayer = ds.CreateLayer("lineString", srs, wkbGeometryType.wkbLineString, null);
            var added = AddElementsToLayerByGeometryType(lineStringlayer, DepictionGeometryType.LineString, elementList);
            if (!added)
            {
                var count = ds.GetLayerCount();
                ds.DeleteLayer(count - 1);
            }
            lineStringlayer.Dispose();

            var multiLineStringLayer = ds.CreateLayer("multiLineString", srs, wkbGeometryType.wkbLineString, null);
            added = AddElementsToLayerByGeometryType(multiLineStringLayer, DepictionGeometryType.MultiLineString, elementList);
            //the remove is a temp hack. The smarter way of doing it is not creating the layer in the first place
            if (!added)
            {
                var count = ds.GetLayerCount();
                ds.DeleteLayer(count - 1);
            }
            multiLineStringLayer.Dispose();

            var polygonLayer = ds.CreateLayer("polygon", srs, wkbGeometryType.wkbPolygon, null);
            added = AddElementsToLayerByGeometryType(polygonLayer, DepictionGeometryType.Polygon, elementList);
            if (!added)
            {
                var count = ds.GetLayerCount();
                ds.DeleteLayer(count - 1);
            }
            polygonLayer.Dispose();

            var multiPolygonLayer = ds.CreateLayer("multiPolygon", srs, wkbGeometryType.wkbMultiPolygon, null);
            added =AddElementsToLayerByGeometryType(multiPolygonLayer, DepictionGeometryType.MultiPolygon, elementList);
            if (!added)
            {
                var count = ds.GetLayerCount();
                ds.DeleteLayer(count - 1);
            }
            multiPolygonLayer.Dispose();

            //For some reason it does not like creating thefield
//            var fdefn = new FieldDefn("ElementType", FieldType.OFTString);
//            fdefn.SetWidth(32);
//            layer.CreateField(fdefn, 0);
//            var elementsWithLineString = GetElementsWithMatchingGeometry(DepictionGeometryType.LineString, elementList);
//            foreach (var element in elementsWithLineString)
//            {
//                Feature feature = new Feature(layer.GetLayerDefn());
//                //            feature.SetField("ElementType", "thing");
//                var geom = GeometryToOgrConverter.ZOIToOgrGeometry(element.ZoneOfInfluence);
//                feature.SetGeometry(geom);
//                layer.CreateFeature(feature);
//            }


            ds.Dispose();

        }
        public void WriteRoadNetworkToSHPFile(string fileName, IDepictionElement roadNetworkElement)
        {
            Driver drv = Ogr.GetDriverByName("ESRI Shapefile");
            if (drv == null) return;
            var message = "Writing roadnetwork as linestring";
            UpdateStatusReport(message);

            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            DataSource ds = drv.CreateDataSource(fileName, null);
            var roadNetworkLayer = ds.CreateLayer("roadNetworkLayer", srs, wkbGeometryType.wkbMultiLineString, null);
            AddElementsToLayerByGeometryType(roadNetworkLayer, DepictionGeometryType.MultiLineString, new List<IDepictionElement> { roadNetworkElement });
            roadNetworkLayer.Dispose();
            ds.Dispose();
        }

        #endregion
        #region layer creation helpers

        private Layer CreateLayer(wkbGeometryType geomType, DataSource ds, string layerName)
        {
            Layer layer;
            layer = ds.CreateLayer(layerName, null, geomType, new string[] { });
            AddFieldDefinitionToLayer(layer, "ElementType", 32);
            return layer;
        }
        private void AddFieldDefinitionToLayer(Layer targetLayer, string fieldName, int fieldWidth)
        {
            //.shp file dont like tis call, so there are a couple catches later on to make sure things don't 
            //totally fall apart.
            var fdefn = new FieldDefn(fieldName, FieldType.OFTString);
            fdefn.SetWidth(fieldWidth);
            targetLayer.CreateField(fdefn, 1);//not sure what the return is used for yet
        }

        #endregion
        #region information selection helpers

        private bool CanExportElementType(string elementType)
        {
            var exportableElements = new List<string> { "UserDrawnLine", "UserDrawnPolygon", "LoadedLines", "LoadedPolygon" };
            foreach (var typeName in exportableElements)
            {
                if (elementType.Contains(typeName)) return true;
            }
            return false;
        }

        private string GetValueAsString(IElementProperty prop)
        {
            var stringVal = DepictionTypeConverter.ChangeType(prop.Value, typeof(string)).ToString();
            if (prop.Value.GetType().ToString().Equals(stringVal)) return null;
            return stringVal;
        }
        private List<IDepictionElement> GetElementsWithMatchingGeometry(DepictionGeometryType geomTypeForLayer, IEnumerable<IDepictionElement> elementList)
        {
            var geoElements =
                elementList.Where(
                    t => t.ZoneOfInfluence != null && t.ZoneOfInfluence.DepictionGeometryType.Equals(geomTypeForLayer));
            return geoElements.ToList();
        }
        private IEnumerable<IElementProperty> GetUnionOfProperties(IEnumerable<IDepictionElement> elementList)
        {
            //Quick and dirty
            var propertyUnion = new List<IElementProperty>();
            var comparer = new PropertyComparers.PropertyNameAndTypeComparer<IElementProperty>();
            foreach (var element in elementList)
            {
                if (propertyUnion.Count == 0)
                {
                    propertyUnion = element.OrderedCustomProperties.ToList();
                }
                else
                {
                    propertyUnion =
                        propertyUnion.Union(element.OrderedCustomProperties, comparer).ToList();

                }
            }
            return propertyUnion;
        }
        #endregion

        private void WriteImageElementToGeoTiff(GDAL.Driver imageDriver, IDepictionElement imageElement, string newFileName, string origImageName)
        {
            //The exported elevation doesn't load quite right, might have something to do with warping
            //or lack there of
            if (imageDriver == null) return;
            var imageInfo = imageElement.ImageMetadata;
            if (imageInfo == null) return;

            try
            {
                var dsOriginal = Gdal.Open(origImageName, Access.GA_ReadOnly);
                var saveOptions = new string[0];// { "TFW=YES" };
                var geoTiff = imageDriver.CreateCopy(newFileName, dsOriginal, 0, saveOptions, null, null);
                const string popularVisualisationWKT = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
                                                            "GEOGCS[\"Popular Visualisation CRS\",  " +
                                                                "DATUM[\"WGS84\",    " +
                                                                    "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
                                                                "AUTHORITY[\"EPSG\",\"6055\"]], " +
                                                           "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
                                                           "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
                                                           "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
                                                       "PROJECTION[\"Mercator\"]," +
                                                       "PARAMETER[\"semi_minor\",6378137]," +
                                                       "PARAMETER[\"False_Easting\", 0]," +
                                                       "PARAMETER[\"False_Northing\", 0]," +
                                                       "PARAMETER[\"Central_Meridian\", 0]," +
                                                       "PARAMETER[\"Latitude_of_origin\", 0]," +
                                                       "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
                                                       "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
                                                       "AUTHORITY[\"EPSG\",\"3785\"]]";
                IProjectedCoordinateSystem popularVisualizationCS = CoordinateSystemWktReader.Parse(popularVisualisationWKT) as IProjectedCoordinateSystem;

                const string geowkt = "GEOGCS[\"WGS 84\"," +
                                      "DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
                                      "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
                                      "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
                IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geowkt) as IGeographicCoordinateSystem;

                CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();

                //// MORE THAN MEETS THE EYE!!
                var transformer = ctfac.CreateFromCoordinateSystems(geoCS, popularVisualizationCS);

                var topleft = WorldToGeoCanvas(imageInfo.GeoBounds.TopLeft, transformer);
                var bottomRight = WorldToGeoCanvas(imageInfo.GeoBounds.BottomRight, transformer);
                var topLeftLatLong = imageInfo.GeoBounds.TopLeft;
                //var tlUTM = topLeftLatLong.ConvertToUTM();
                var botLatLong = imageInfo.GeoBounds.BottomRight;
                //var brUTM = botLatLong.ConvertToUTM();
                var imagePixWidth = dsOriginal.RasterXSize;//pixWidth of baseimage
                var imagePixHeight = dsOriginal.RasterYSize;//pixheight of baseimage

                var widthDis = bottomRight.X - topleft.X;
                var heightDis = bottomRight.Y - topleft.Y;
                // top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
                double pixelWidth = Math.Abs(widthDis / imagePixWidth);
                double pixelHeight = Math.Abs(heightDis / imagePixHeight);
                //Alright so there are oddities with the saved TFW files and the ones that depiction uses as the truth. I believe the gdal since it seems more consistent
                //with what sources say about the tfw files. ie we shouldn't use the  + (pixelWidth / 2) and  - (pixelHeight / 2)
                //The removal of half pixel height is because spatial reference is expecting the mid point (real world length) of the topleft coordinate pixel
                //                var wgs84Transform = new double[] { tlUTM.EastingMeters + (pixelWidth / 2), pixelWidth, 0, tlUTM.NorthingMeters - (pixelHeight / 2), 0, -pixelHeight };
                var wgs84Transform = new double[] { topleft.X, pixelWidth, 0, topleft.Y, 0, -pixelHeight };
                geoTiff.SetGeoTransform(wgs84Transform);
                geoTiff.SetProjection(popularVisualisationWKT);
                geoTiff.FlushCache();
                geoTiff.Dispose();

            }
            catch
            {
                //Not really sure if this is even needed
            }
        }

        public System.Windows.Point WorldToGeoCanvas(ILatitudeLongitude latLon, ICoordinateTransformation transformer)
        {
            double[] pointCartesian = transformer.MathTransform.Transform(new[] { latLon.Longitude, latLon.Latitude });
            return new System.Windows.Point(pointCartesian[0], pointCartesian[1]);
        }

        private bool AddElementsToLayerByGeometryType(Layer targetLayer, DepictionGeometryType geomTypeForLayer, IEnumerable<IDepictionElement> elementList)
        {
            //Add those properties that have integer, double, or string type values
            if (ServiceStopRequested) return false;
            
            var elementsWithCorrectGeomType = GetElementsWithMatchingGeometry(geomTypeForLayer, elementList);
            if (!elementsWithCorrectGeomType.Any()) return false;
            var elementPropertiesUnion = GetUnionOfProperties(elementsWithCorrectGeomType);

            //Write out properties in fomr of fields so that the layer understands what is adding in
            //This is done brute force method since everything gets turned into a string
            //And it only export string, int, and double values
            var fieldWidth = 256;
            foreach (var prop in elementPropertiesUnion)
            {
                if (prop.Value is string || prop.Value is Int32 || prop.Value is Double)
                {
                    try { AddFieldDefinitionToLayer(targetLayer, prop.InternalName, fieldWidth); }
                    catch { }
                }
            }
            //Now write out the data for the features
            List<Feature> featureList = new List<Feature>();
            int count = 0;
            var total = elementsWithCorrectGeomType.Count();
            foreach (var element in elementsWithCorrectGeomType)
            {
                var newMessage = string.Format(message + ": Feature list for {0} of {1}", count, total);
                UpdateStatusReport(newMessage);
                var geom = GeometryToOgrConverter.ZOIToOgrGeometry(element.ZoneOfInfluence);
                if (geom != null)
                {
                    Feature feature = new Feature(targetLayer.GetLayerDefn());
                    feature.SetGeometry(geom);
                    try
                    {
                        feature.SetField("ElementType", element.ElementType);
                    }catch
                    {
                        //tends to fail with .shp files
                    }

                    foreach (var prop in element.OrderedCustomProperties)
                    {
                        var propValue = GetValueAsString(prop);
                        if (propValue != null)
                        {
                            try
                            {
                                feature.SetField(prop.InternalName, propValue);
                            }
                            catch
                            {//catches the no field error silently
                            }
                        }
                    }
                    featureList.Add(feature);
                }
                foreach (var feature in featureList)
                {
                    if (ServiceStopRequested) return false;
                    targetLayer.CreateFeature(feature);
                }
            }
            return true;
        }
        #endregion
    }

}
