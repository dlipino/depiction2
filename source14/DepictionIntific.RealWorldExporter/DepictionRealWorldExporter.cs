﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;

namespace DepictionIntific.RealWorldExporter
{
    [DepictionElementExporterMetadata("DepictionRealWorldFileExporter", new[] { ".xml" },
       FileTypeInformation = "Depiction RealWorld file", DisplayName = "Depiction RealWorld writer")]
    public class DepictionRealWorldExporter : IDepictionElementExporter
    {
        public object AddonMainView
        {
            get { return null; }
        }

        public string AddonConfigViewText
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }

        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }

        public void ExportElements(object location, IEnumerable<IDepictionElement> elements)
        {
            if (location == null) return;
            var fileName = location.ToString();
            WriteElements(fileName, elements, true);
        }
        public void WriteElements(string fileName, IEnumerable<IDepictionElement> elementList, bool background)
        {
            var realworldWriter = new DepictionRealWorldExportService();
            if (background)
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("FileName", fileName);
                parameters.Add("ElementList", elementList);
                var name = string.Format("Writing RW file : {0}", Path.GetFileName(fileName));
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(realworldWriter);
                realworldWriter.UpdateStatusReport(name);
                realworldWriter.StartBackgroundService(parameters);
            }
            else
            {
                realworldWriter.WriteElementXMLAndFiles(fileName, elementList);
            }
        }
    }
}
