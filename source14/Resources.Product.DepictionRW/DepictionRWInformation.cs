﻿using System.Collections.Specialized;
using System.IO;
using Depiction.API.AbstractObjects;

namespace Resources.Product.DepictionRW
{
    public class DepictionRWInformation : ProductInformationBase
    {
        public override string ProductType { get { return DepictionRW; } }

        override public string DirectoryNameForCompany { get { return "Depiction_RW"; } }
        public override string ProductName
        {
            get { return "Depiction: RealWorld"; }
        }
        public override string ProductNameInternalUsage { get { return "DepictionRealWorld"; } }
        public override string ResourceAssemblyName
        {
            get { return "Resources.Product.DepictionRW"; }
        }

        public override string SplashScreenPath
        {
            get { return "Images/SplashScreen.png"; }
        }
//        public override string AppIconResourceName { get { return "Images/DepictionRealWorld.ico"; } }
        public override string LicenseFileName { get { return string.Empty; } }//for now
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",
                                     ProductVersion, DepictionCopyright);
            }
        }
        
        override public StringCollection ProductQuickAddElements
        {
            get
            {
                return new StringCollection { "UserDrawnLine","UserDrawnPolygon" };
            }
        }
        public override string EulaFileAssemblyLocation
        {
            get { return "Docs.Depiction_EULA_TIS.xps"; }
        }
    }
}
