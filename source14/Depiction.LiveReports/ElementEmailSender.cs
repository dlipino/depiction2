using System.Collections.Generic;
using System.Windows;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.LiveReports.ViewModels;
using Depiction.LiveReports.Views;

namespace Depiction.LiveReports
{
    [DepictionElementExporterMetadata("ElementEmailSender",DisplayName = "Depiction element by email exporter")]
    public class ElementEmailSender : IDepictionElementExporter
    {
        public void ExportElements(object location, IEnumerable<IDepictionElement> elements)
        {
            var view = new ElementEmailSenderView();
            if (elements == null) return;
            var viewModel = new EmailSenderSetupVM(elements, ElementEmailSenderView.selectedIndex);
            view.DataContext = viewModel;
            var window = (Application.Current.MainWindow as IDepictionMainWindow);
            if (window == null) return;
            viewModel.IsDialogVisible = true;
        }

        #region Implementation of IDepictionElementIOStream

        public object AddonMainView
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }

        public string AddonConfigViewText { get { return null; } }

        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }



        #endregion
    }
}