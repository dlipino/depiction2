﻿using System.Configuration;

namespace Depiction.LiveReports.EmailConfiguration
{
    public class IncomingEmailAccountRecords : ConfigurationElementCollection
    {
        public IncomingEmailSettings this[int index]
        {
            get { return BaseGet(index) as IncomingEmailSettings; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new IncomingEmailSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((IncomingEmailSettings)element).UserName + ((IncomingEmailSettings)element).Pop3Server;
        }

        public void Add(IncomingEmailSettings EmailAccount)
        {
            BaseAdd(EmailAccount);
        }

        public void Insert(IncomingEmailSettings EmailAccount, int Index)
        {
            BaseAdd(Index, EmailAccount);
        }
    }
}