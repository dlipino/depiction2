using System.Configuration;

namespace Depiction.LiveReports.EmailConfiguration
{
    abstract public class BasicEmailSettings : ConfigurationElement
    {
        [ConfigurationProperty("UserName", DefaultValue = "", IsRequired = true)]
        public string UserName
        {
            get { return (string)this["UserName"]; }
            set { this["UserName"] = value; }
        }

        [ConfigurationProperty("UseSSL", DefaultValue = false, IsRequired = true)]
        public bool UseSSL
        {
            get { return (bool)this["UseSSL"]; }
            set { this["UseSSL"] = value; }
        }

    }
}