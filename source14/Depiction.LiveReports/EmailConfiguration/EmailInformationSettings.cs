using System;
using System.Configuration;
using Depiction.API;
using Depiction.API.DepictionConfiguration;
using Depiction.API.ExceptionHandling;

namespace Depiction.LiveReports.EmailConfiguration
{
    static public class EmailInformationSettings
    {
        private const string liveReportSectionName = "LiveReportSettings";

        #region incoming email account stuff

        public static IncomingEmailAccountRecords GetIncomingEmailAccounts()
        {//grr this is causing problems
            EmailSettingsConfigurationSection emailSection;
            var config = GetUserSettingsLostConfig(out emailSection);

            if (config == null) return null;
            return config.Sections[liveReportSectionName] != null ? ((EmailSettingsConfigurationSection)config.GetSection(liveReportSectionName)).IncomingEmailAccountRecords : null;
        }
        public static void AddIncomingEmailAccount(IncomingEmailSettings EmailAddressToAdd)
        {
            EmailSettingsConfigurationSection emailSection;
            Configuration config = GetUserSettingsLostConfig(out emailSection);

            emailSection.IncomingEmailAccountRecords.Add(EmailAddressToAdd);
            emailSection.SectionInformation.ForceSave = true;
            try
            {
                config.Save(ConfigurationSaveMode.Full);
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Could not update incoming email list.");
            }
        }
        #endregion

        #region outgoing email info region

        public static OutgoingEmailAccountRecords GetOutgoingEmailAccounts()
        {
            Configuration config = DepictionUserConfigurationManager.DepictionUserConfiguration();//.LiveReportUserConfiguration();
            return config.Sections[liveReportSectionName] != null ? ((EmailSettingsConfigurationSection)config.GetSection(liveReportSectionName)).OutgoingEmailAccountRecords : null;
        }
        public static void RemoveOutgoingEmailAccount(OutgoingEmailSettings emailAddressToRemove)
        {
            EmailSettingsConfigurationSection emailSection;
            Configuration config = GetUserSettingsLostConfig(out emailSection);

            emailSection.OutgoingEmailAccountRecords.Remove(emailAddressToRemove);
            emailSection.SectionInformation.ForceSave = true;
            try
            {
                config.Save(ConfigurationSaveMode.Full);
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Could not update outgoing email list.");
            }
        }
        public static void AddOutgoingEmailAccount(OutgoingEmailSettings emailAddressToAdd)
        {
            EmailSettingsConfigurationSection emailSection;
            Configuration config = GetUserSettingsLostConfig(out emailSection);

            emailSection.OutgoingEmailAccountRecords.AddOrReplaceExisting(emailAddressToAdd);
            emailSection.SectionInformation.ForceSave = true;
            try
            {
                config.Save(ConfigurationSaveMode.Full);
            }catch(Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Could not update outgoing email list.");
            }
        }
        
        #endregion


        private static Configuration GetUserSettingsLostConfig(out EmailSettingsConfigurationSection emailSection)
        {
            Configuration config = DepictionUserConfigurationManager.DepictionUserConfiguration(); //LiveReportsUserConfigurationManager.LiveReportUserConfiguration();
            if (config == null)
            {
                emailSection = null;
                return null;
            }
            try
            {
                var sections = config.Sections;
                if (config.GetSection(liveReportSectionName) as EmailSettingsConfigurationSection == null) //Sections.Get(liveReportSectionName)==null )//config.Sections[liveReportSectionName] == null)
                {
                    emailSection = new EmailSettingsConfigurationSection();
                    emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                    config.Sections.Add(liveReportSectionName, emailSection);
                }
                else
                {
                    var section = config.GetSection(liveReportSectionName);
                    if ((section is EmailSettingsConfigurationSection))
                    {
                        emailSection = (EmailSettingsConfigurationSection)section;
                    }else
                    {
                        config.Sections.Remove(liveReportSectionName);
                        config.Save();
                        emailSection = new EmailSettingsConfigurationSection();
                        emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                        config.Sections.Add(liveReportSectionName, emailSection);
                    }
                }
            }
            catch(ConfigurationErrorsException configError )
            {
                DepictionExceptionHandler.HandleException(configError, false, true);
                var sections = config.SectionGroups;
                config.Sections.Remove(liveReportSectionName);
                config.Save();
                emailSection = new EmailSettingsConfigurationSection();
                emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                config.Sections.Add(liveReportSectionName, emailSection);
                return config;
            }
            catch(Exception ex){
                DepictionExceptionHandler.HandleException(ex,false,true);
                //This isn't actually fixing the problem, but not the right time to dig into this.
                config.Sections.Remove(liveReportSectionName);
                config.Save();
                emailSection = new EmailSettingsConfigurationSection();
                emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                config.Sections.Add(liveReportSectionName, emailSection);
            }
          
            return config;
        }
    }

    public class EmailSettingsConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("IncomingEmailAccountRecords")]
        public IncomingEmailAccountRecords IncomingEmailAccountRecords
        {
            get { return (IncomingEmailAccountRecords)this["IncomingEmailAccountRecords"] ?? new IncomingEmailAccountRecords(); }
        }
        [ConfigurationProperty("OutgoingEmailAccountRecords")]
        public OutgoingEmailAccountRecords OutgoingEmailAccountRecords
        {
            get { return (OutgoingEmailAccountRecords)this["OutgoingEmailAccountRecords"] ?? new OutgoingEmailAccountRecords(); }
        }
    }
}