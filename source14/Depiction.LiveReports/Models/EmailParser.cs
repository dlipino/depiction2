using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DpnPorting;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;

namespace Depiction.LiveReports.Models
{
    public static class EmailParser
    {
        static private string positionKey = "position";
        static private string displayNameKey = "displayname";
        #region Main Parsers
        public static IElementPrototype GetRawPrototypeFromEmailSubjectAndBody(string subject, string body)
        {
            return GetRawPrototypeFromEmailSubjectAndBody(subject, body, null);
        }
        public static IElementPrototype GetRawPrototypeFromEmailSubjectAndBody(string subject, string body, IDepictionGeocoder[] nonStandardGeocoders)
        {
            var propertiesToIgnore = new List<string> { "email subject", "email body", "email from", "email to", "email error", "email sent" };
            var subjectProperties = ParseEmailSubjectIntoProperties(subject);
            var mainBodyProperties = ParseEmailBodyStringIntoProperties(body, propertiesToIgnore);

            var combinedProperties = new Dictionary<string, object>();
            var zoiString = string.Empty;
            var elementType = string.Empty;
            var wayPoints = new IDepictionElementWaypoint[0];

            foreach (var key in mainBodyProperties.Keys)
            {
                if (key.Equals("elementType", StringComparison.OrdinalIgnoreCase))
                {
                    elementType = mainBodyProperties[key].ToString();
                }
                else if (key.Equals("zoneofinfluence", StringComparison.OrdinalIgnoreCase))
                {
                    zoiString = mainBodyProperties[key].ToString();
                }
                else if (key.Equals("wayPoints", StringComparison.InvariantCultureIgnoreCase))
                {
                    wayPoints = mainBodyProperties[key] as IDepictionElementWaypoint[];
                }
                else
                {
                    var lowerKey = key.ToLowerInvariant();
                    if (combinedProperties.ContainsKey(lowerKey))
                    {
                        combinedProperties[lowerKey] = mainBodyProperties[key];
                    }
                    else
                    {
                        combinedProperties.Add(key.ToLowerInvariant(), mainBodyProperties[key]);
                    }
                }
            }
            var position = GetPositionFromProperties(mainBodyProperties, true, nonStandardGeocoders);

            if (position != null && position.IsValid)
            {
                if (combinedProperties.ContainsKey(positionKey)) { combinedProperties[positionKey] = position; }
                else { combinedProperties.Add(positionKey.ToLowerInvariant(), position); }
            }
            //Subject superseeds mainbody when it comes to info
            foreach (var key in subjectProperties.Keys)
            {
                if (key.Equals("elementType", StringComparison.OrdinalIgnoreCase))
                {
                    elementType = subjectProperties[key].ToString();
                }
                else if (key.Equals("zoneofinfluence", StringComparison.OrdinalIgnoreCase))
                {
                    zoiString = subjectProperties[key].ToString();
                }
                else if (key.Equals("wayPoints", StringComparison.InvariantCultureIgnoreCase))
                { //This should never happen
                    wayPoints = subjectProperties[key] as IDepictionElementWaypoint[];
                }
                else
                {
                    var lowerKey = key.ToLowerInvariant();
                    if (combinedProperties.ContainsKey(lowerKey))
                    {
                        combinedProperties[lowerKey] = subjectProperties[key];
                    }
                    else
                    {
                        combinedProperties.Add(key.ToLowerInvariant(), subjectProperties[key]);
                    }
                }
            }
            position = GetPositionFromProperties(subjectProperties, true, nonStandardGeocoders);
            if (position != null && position.IsValid)
            {
                if (combinedProperties.ContainsKey(positionKey)) { combinedProperties[positionKey] = position; }
                else { combinedProperties.Add(positionKey.ToLowerInvariant(), position); }
            }

            if (string.IsNullOrEmpty(elementType))
            {
                elementType = DepictionStringService.AutoDetectElementString;

                object name;
                if (combinedProperties.TryGetValue(displayNameKey, out name))
                {
                    if (!string.IsNullOrEmpty(name.ToString()) && DepictionAccess.ElementLibrary != null)
                    {
                        var fakeProto = DepictionAccess.ElementLibrary.GuessPrototypeFromString(name.ToString());
                        if (fakeProto != null)
                        {
                            elementType = fakeProto.ElementType;
                        }
                    }
                }
            }
            var zoiDescription = DepictionGeometryType.Point.ToString();
            if (!string.IsNullOrEmpty(zoiString))
            {
                if (zoiString.ToLowerInvariant().Contains("line"))
                {
                    zoiDescription = DepictionGeometryType.LineString.ToString();
                }
                else if (zoiString.ToLowerInvariant().Contains("polygon"))
                {
                    zoiDescription = DepictionGeometryType.Polygon.ToString();
                }
            }
            var fakePrototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(elementType, zoiDescription);
            IElementPrototype prototype = null;

            if (fakePrototype == null)
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(elementType);
            }
            else
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(fakePrototype.ElementType);
            }

            foreach (var property in combinedProperties)
            {
                var actualValue = property.Value;
                IElementProperty realProp = null;
                if (fakePrototype != null)
                {
                    var key = property.Key;
                    var propData = fakePrototype.GetPropertyByInternalName(key, true);
                    if (propData != null)
                    {
                        realProp = propData.DeepClone();
                        realProp.PropertySource = PropertySource.EMAIL;
                        try
                        {
                            object match = actualValue.ToString();
                            if (!realProp.InternalName.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = DepictionTypeConverter.ChangeType(actualValue, realProp.ValueType);
                            }
                            if (match == null) match = actualValue;
                            realProp.SetPropertyValue(match);
                        }
                        catch (Exception ex)
                        {

                        }
                        prototype.AddPropertyOrReplaceValueAndAttributes(realProp, true, false);
                    }
                }
                if (realProp == null)
                {
                    object objectValue = actualValue.ToString();
                    if (!property.Key.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                    {
                        objectValue = DepictionTypeConverter.ChangeTypeByGuessing(actualValue);//This is strange 
                    }

                    if (objectValue == null) objectValue = actualValue;

                    var prop = new DepictionElementProperty(property.Key,property.Key, objectValue);
                    //prop.Deletable = false;
                    prop.PropertySource = PropertySource.EMAIL;
                    prototype.AddPropertyOrReplaceValueAndAttributes(prop, true, false);
                }
            }
            if (!string.IsNullOrEmpty(zoiString))
            {
                object finalPos;
                combinedProperties.TryGetValue(positionKey, out finalPos);
                prototype.SetInitialPositionAndZOI(finalPos as ILatitudeLongitude,
                                                   new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
            }
            if (wayPoints != null && wayPoints.Length > 0)
            {
                prototype.ReplaceWaypointsWithoutNotification(wayPoints);
            }
            return prototype;
        }

        #endregion
        public static Dictionary<string, object> ParseEmailSubjectIntoProperties(string subjectString)
        {
            var subjectPropertyPairs = new Dictionary<string, object>();
            string subjectRemainder = subjectString;

            subjectRemainder = StripLeadingMARS(subjectRemainder);
            subjectRemainder = StripLeadingReFw(subjectRemainder);
            //Element type must be the first?
            var prototypeType = ParseElementTypeFromSubject(subjectRemainder, out subjectRemainder);
            if (!string.IsNullOrEmpty(prototypeType))
            {
                subjectPropertyPairs.Add("elementtype", prototypeType);
            }
            subjectRemainder = ParseLabel(subjectRemainder, subjectPropertyPairs);

            try
            {
                var position = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(subjectRemainder);

                if (position != null && position.IsValid)
                {
                    subjectPropertyPairs.Add("position", position);
                }
                else
                {
                    if (!string.IsNullOrEmpty(subjectRemainder))
                    {
                        subjectPropertyPairs.Add("rawposition", subjectRemainder);
                    }
                }

            }
            catch { }

            return subjectPropertyPairs;
        }
        #region Subject parsing helpers
        private static string StripLeadingReFw(string subject)
        {
            while (subject.ToLower().StartsWith("fw:") || subject.ToLower().StartsWith("re:"))
            {
                subject = subject.Substring(3).Trim();
            }
            return subject;
        }

        // to comply with trac #2157 - support for MARS from Winlink
        private static string StripLeadingMARS(string subject)
        {
            return Regex.Replace(subject, "^//MARS [MOPRZ]/", "");
        }

        /// <summary>
        /// If the subject line starts with the name of a known element type, 
        /// set record.ElementType to this element's type and return the remainder of the subject line.
        /// Else, return the entire subject line unaltered.
        /// </summary>

        /// <returns>The rest of the subject line.</returns>
        public static string ParseElementTypeFromSubject(string subjectFragment, out string subjectRemainder)//TODO this shouldnt' be public, but it is needed for tests
        {
            var type = string.Empty;
            subjectRemainder = subjectFragment;
            if (!subjectFragment.Contains(":"))
            {
                return string.Empty;
            }
            string[] parts = subjectFragment.Split(":".ToCharArray(), 2);
            type = parts[0].Trim();
            subjectRemainder = parts[1].Trim();//Not quite sure what this is
            var convertedType = DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(type);
            return convertedType;
        }

        /// <summary>
        /// Label is material before the first comma.
        /// If no comma, include everything as label.
        /// Return the non-label portion of the subject.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="displayNamePair"></param>
        /// <returns></returns>
        private static string ParseLabel(string subject, Dictionary<string, object> displayNamePair)
        {
            string displayName = "";
            if (!subject.Contains(","))
            {
                displayName = subject;
                subject = "";
            }
            else
            {
                const int numberOfParts = 2;
                string[] parts = subject.Split(",".ToCharArray(), numberOfParts);

                displayName = parts[0].Trim();
                subject = parts[1].Trim();
            }
            if (!string.IsNullOrEmpty(displayName))
            {
                displayNamePair.Add(displayNameKey, displayName);
            }
            return subject;
        }

        #endregion

        public static Dictionary<string, object> ParseEmailBodyStringIntoProperties(string bodyString, List<string> propertyNamesToIgnore)
        {
            string body = bodyString;

            var subjectPropertyPairs = new Dictionary<string, object>();
            string[] lines = body.Split("\n".ToCharArray());
            var typeNameList = new List<string> { "Element type", "ElementType" };//"type", 

            var lineCopies = new List<string>(lines);
            var typeFound = false;
            foreach (string line in lines)
            {
                KeyValuePair<string, string> property;
                var originalElementComplete = ParseEachLine(propertyNamesToIgnore, line, out property);
                if (originalElementComplete)
                {
                    break;
                }
                if (!string.IsNullOrEmpty(property.Key) && property.Value != null)
                {
                    if (!typeFound)
                    {
                        foreach (var typeName in typeNameList)
                        {
                            if (typeName.Equals(property.Key, StringComparison.InvariantCultureIgnoreCase))
                            {
                                var convertedType =
                                    DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(property.Value);
                                if (subjectPropertyPairs.ContainsKey("elementtype"))
                                {
                                    subjectPropertyPairs["elementtype"] = convertedType;
                                }
                                else
                                {
                                    subjectPropertyPairs.Add("elementtype", convertedType);
                                }
                                typeFound = true;
                                break;
                            }
                        }
                        if (typeFound) continue;
                    }
                    //Hmm i think this is done again later on.
                    if (property.Key.Equals("zoneofInfluence", StringComparison.InvariantCultureIgnoreCase) ||
                        property.Key.Equals("ZOI", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (subjectPropertyPairs.ContainsKey("zoneofinfluence"))
                        {
                            subjectPropertyPairs["zoneofinfluence"] = property.Value;
                        }
                        else
                        {
                            subjectPropertyPairs.Add("zoneofinfluence", property.Value);
                        }
                    }
                    else
                    {
                        var lowerKey = property.Key.ToLowerInvariant();
                        object propValue = "";
                        //ugh this is ugly
                        if (lowerKey.Equals("eid"))
                        {
                            propValue = property.Value;
                        }
                        else
                        {
                            propValue = PropertyValueTypeConverter.ConvertType(property.Value);
                            if (propValue.Equals(property.Value))
                            {
                                propValue = DepictionTypeConverter.ChangeTypeByGuessing(property.Value);
                            }
                        }

                        if (subjectPropertyPairs.ContainsKey(lowerKey))
                        {
                            subjectPropertyPairs[lowerKey] = propValue;
                        }
                        else
                        {
                            subjectPropertyPairs.Add(lowerKey, propValue);
                        }
                    }
                }
                lineCopies.RemoveAt(0);
            }
            //The the waypoints/122 element children
            var wayPoints = CreateWaypointsFromStringArray(lineCopies.ToArray(), propertyNamesToIgnore);
            if (wayPoints.Count > 0)
            {
                subjectPropertyPairs.Add("waypoints", wayPoints.ToArray());
            }

            return subjectPropertyPairs;
        }
        #region main body helpers
        private static List<IDepictionElementWaypoint> CreateWaypointsFromStringArray(IEnumerable<string> dataStringLines, List<string> propertyNamesToIgnore)
        {
            var waypoints = new List<IDepictionElementWaypoint>();
            var propNameDict = new Dictionary<string, string>();
            var lineCopy = new List<string>(dataStringLines);
            int markerCount = 0;
            bool is122Waypoint = false;
            foreach (var line in dataStringLines)
            {
                KeyValuePair<string, string> property;
                var markerLine = ParseEachLine(propertyNamesToIgnore, line, out property);
                if (markerLine)
                {
                    if (markerCount > 0) break;
                    markerCount++;
                    if (line.ToLowerInvariant().Contains("elementchild"))
                    {
                        is122Waypoint = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(property.Key) && !string.IsNullOrEmpty(property.Value))
                    {
                        propNameDict.Add(property.Key, property.Value);
                    }
                }
                lineCopy.RemoveAt(0);
            }
            var waypoint = new DepictionElementWaypoint();
            if (is122Waypoint)
            {
                foreach (var keyValue in propNameDict)
                {
                    if (keyValue.Key.Equals("zoneofInfluence", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var zoiString = keyValue.Value;
                        var latLong =
                            new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)).GetVertices()[0];
                        waypoint.UpdateLocationWithoutChangeNotification(latLong);
                    }
                    else if (keyValue.Key.EndsWith("path", StringComparison.InvariantCultureIgnoreCase))
                    {
                        waypoint.IconPath = new DepictionIconPath(keyValue.Value);
                    }
                    else if (keyValue.Key.Equals("name", StringComparison.InvariantCultureIgnoreCase))
                    {
                        waypoint.Name = keyValue.Value;
                    }
                }
                waypoint.IconSize = 18;
            }
            else
            {
                foreach (var keyValue in propNameDict)
                {
                    if (keyValue.Key.Equals("Location", StringComparison.InvariantCultureIgnoreCase))
                    {
                        waypoint.UpdateLocationWithoutChangeNotification(LatitudeLongitudeTypeConverter.ConvertStringToLatLong(keyValue.Value));
                    }
                    else if (keyValue.Key.EndsWith("IconPath", StringComparison.InvariantCultureIgnoreCase))
                    {
                        waypoint.IconPath = new DepictionIconPath(keyValue.Value);
                    }
                    else if (keyValue.Key.Equals("Name", StringComparison.InvariantCultureIgnoreCase))
                    {
                        waypoint.Name = keyValue.Value;
                    }
                }
            }
            if (dataStringLines.Count() != 0)
            {
                waypoints.Add(waypoint);
            }
            if (lineCopy.Count != 0)
            {

                waypoints.AddRange(CreateWaypointsFromStringArray(lineCopy.ToArray(), propertyNamesToIgnore));
                return waypoints;
            }
            return waypoints;
        }
        #endregion
        //TODO i know (think) this is a duplicate of something, nto sure where it is though
        static private ILatitudeLongitude GetPositionFromProperties(Dictionary<string, object> properties, bool replace, IDepictionGeocoder[] usableGeocoders)
        {
            ILatitudeLongitude defaultPosition = null;
            if (properties.ContainsKey("position"))
            {
                var initialPosition = properties["position"];
                defaultPosition = initialPosition as ILatitudeLongitude;
                if (defaultPosition == null && initialPosition is string)
                {
                    defaultPosition = new LatitudeLongitude(initialPosition.ToString());
                    if (!defaultPosition.IsValid)
                    {
                        defaultPosition = null;
                    }
                }
                
            }
            if (properties.ContainsKey("rawposition"))
            {
                var rawPosition = properties["rawposition"].ToString();
                if (DepictionAccess.GeoCodingService != null)
                {
                    var result = DepictionAccess.GeoCodingService.GeoCodeRawStringAddress(rawPosition, usableGeocoders);
                    if (result != null)
                    {
                        defaultPosition = result.Position;
                    }
                    else
                    {
                        defaultPosition = null;
                    }
                }
                else
                {
                    defaultPosition = null;
                }
            }
            if (replace && defaultPosition != null)
            {
                return defaultPosition;
            }
            object latitude = "";
            bool foundLat = false;
            foreach (var latName in LiveReportHelpers.LatitudePropertyNames)
            {
                foundLat = properties.TryGetValue(latName.ToLowerInvariant(), out latitude);
                if (foundLat) break;
            }
            object longitude = "";
            bool foundLong = false;
            foreach (var longName in LiveReportHelpers.LongitudePropertyNames)
            {
                foundLong = properties.TryGetValue(longName.ToLowerInvariant(), out longitude);
                if (foundLong) break;
            }

            ILatitudeLongitude position = new LatitudeLongitude();
            if (foundLong && foundLat)
            {
                try
                {
                    position = new LatitudeLongitude(latitude.ToString(), longitude.ToString());
                }
                catch { }
            }

            if (!position.IsValid && DepictionAccess.GeoCodingService != null)
            {
                foreach (var propertyNames in LiveReportHelpers.LegalGeocodeFieldCombinations)
                {
                    if (HasAllTheseProperties(properties, propertyNames))
                    {
                        var locationString = GetGeocodableAddress(properties, propertyNames);
                        var result = DepictionAccess.GeoCodingService.GeoCodeRawStringAddress(locationString, usableGeocoders);
                        if (result != null)
                        {
                            defaultPosition = result.Position;
                        }
                        else
                        {
                            defaultPosition = null;
                        }
                    }
                }
            }

            if (position != null && position.IsValid)
            {
                return position;
            }
            return defaultPosition;

        }
        #region geodcoding helpers
        private static bool HasAllTheseProperties(Dictionary<string, object> propertyHolder, IEnumerable<string> propertyNames)
        {
            foreach (string field in propertyNames)
            {
                if (!propertyHolder.ContainsKey(field.ToLowerInvariant()))
                    return false;
            }
            return true;
        }
        private static string GetGeocodableAddress(Dictionary<string, object> propertyHolder, IEnumerable<string> propertyNames)
        {
            string location = String.Empty;

            foreach (string field in propertyNames)
            {
                object propValue = "";
                //If it gets to this method the property should exist.
                if (propertyHolder.TryGetValue(field.ToLowerInvariant(), out propValue))
                {
                    if (location.Length > 0)
                        location += ", ";
                    location += propValue;
                }

            }
            return location;
        }

        #endregion
        private static bool ParseEachLine(List<string> propertyNamesToIgnore, string line, out KeyValuePair<string, string> extractedProperty)
        {
            //If true is returned then it is a child element (this is from legacy).
            string[] parts = line.Split(":".ToCharArray());
            extractedProperty = new KeyValuePair<string, string>();
            var regex = new Regex(":");
            parts = regex.Split(line, 2);

            string valueString = "";// parts[1].Trim();
            if (parts.Length > 1)
            {
                valueString = parts[1].Trim();
            }
            else
            {
                return false;
            }
            string propertyName = parts[0].ToLowerInvariant().Trim();
            if (propertyName.Length == 0)//|| valueString.Length == 0
                return false;
            if (propertyName.Equals("elementchild") || propertyName.Equals("elementwaypoint"))
            {
                return true;
            }
            
            //This is not needed and can sometimes mess things up
            if (!Regex.Match(propertyName, "^[a-zA-Z][a-zA-Z0-9 ]*$").Success)
                return false;

            if (propertyNamesToIgnore != null
                && propertyNamesToIgnore.Contains(propertyName))
                return false;
            extractedProperty = new KeyValuePair<string, string>(propertyName, valueString);
            return false;
        }


        /// <summary>
        /// Creates an EID using the messageID if an EID was not present.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="messageID"></param>
        public static void CreateEIDIfMissing(IElementPropertyHolder element, string messageID)
        {
            object elementID;

            if (!element.GetPropertyValue("EID", out elementID))
            {
                var prop = new DepictionElementProperty("EID", messageID);
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }

            //Hack to deal with eid's that end of as numbers
            if (!(elementID is string))
            {
                element.RemovePropertyWithInternalName("eid", false);
                var prop = new DepictionElementProperty("EID", elementID.ToString());
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }
        }
    }
}