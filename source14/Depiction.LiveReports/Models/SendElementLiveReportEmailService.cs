using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.LiveReports.EmailConfiguration;

namespace Depiction.LiveReports.Models
{
    /// <summary>
    /// A service to send elements to an email account.
    /// Useful for communication among depictions via live reports.
    /// </summary>
    public class SendElementLiveReportEmailService : BaseDepictionBackgroundThreadOperation
    {
        private bool isBackgroundOperation = false;
        private int completedEmails = 0;
        private int inCompleteEmails = 0;
        private int totalEmailsToSend = 0;
        #region Constructor

        public SendElementLiveReportEmailService():this(false){}

        public SendElementLiveReportEmailService(bool useBackground)
        {
            isBackgroundOperation = useBackground;
        }

        #endregion

        public void StartSendingElements(IEnumerable<IDepictionElement> elements, OutgoingEmailSettings emailInfo)
        {
            var name = string.Format("Sending elements: {0}", elements.Count());
            if (isBackgroundOperation)
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("Elements", elements);
                parameters.Add("EmailInfo", emailInfo);
                
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(this);
                UpdateStatusReport(name);
                StartBackgroundService(parameters);
            }else
            {
                SendElements(elements.ToList(), emailInfo);
            }
        }

        public void SendElements(List<IDepictionElement> elements, OutgoingEmailSettings emailInfo)
        {
            var errorMessage = string.Empty;
            if (string.IsNullOrEmpty(emailInfo.ToAddress))
            {
                errorMessage = "Could not send email. Please specify \"To\" address";
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                return;
            }
            if (string.IsNullOrEmpty(emailInfo.FromAddress))
            {
                errorMessage = "Could not send email. Please specify \"From\" address";
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                return;
            }
            if (string.IsNullOrEmpty(emailInfo.SMTPServer))
            {
                errorMessage = "Could not send email. Please specify \"Outgoing email server\".";
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                return;
            }

            
            var toAddresses = emailInfo.ToAddress.Split(new[] { ',', ';' });
            completedEmails = 0;
            inCompleteEmails = 0;
            totalEmailsToSend = elements.Count;

            var mailClient = new SmtpClient(emailInfo.SMTPServer) { DeliveryMethod = SmtpDeliveryMethod.Network, EnableSsl = emailInfo.UseSSL, Port = emailInfo.PortNumber };
            mailClient.ServicePoint.MaxIdleTime = 10;
            //mailClient.EnableSsl = true;
            if (emailInfo.UseAuthentication)
            {
                mailClient.Credentials = mailClient.Credentials = new NetworkCredential(emailInfo.UserName, emailInfo.Password);
            }
            mailClient.SendCompleted += mailClient_SendCompleted;
            //mailClient.Send(message);
            foreach (var element in elements)
            {
                try
                {
                    var email = new ElementEmail(element);

                    var message = new MailMessage();
                    message.From = new MailAddress(emailInfo.FromAddress);
                    for (int i = 0; i < toAddresses.Length; i++)
                        message.To.Add(toAddresses[i]);
                    message.Subject = email.Subject;
                    message.BodyEncoding = System.Text.Encoding.ASCII;

                    // Our email reader does not know how to handle quoted-printable transfer-encoding (which is the default, and which we really should support)
                    // but because of that limitation, send in 7bit encoding.
                    AlternateView plainView = AlternateView.CreateAlternateViewFromString(email.Body, new ContentType("text/plain"));
                    plainView.TransferEncoding = TransferEncoding.SevenBit;
                    message.AlternateViews.Add(plainView);
                    mailClient.Send(message);
                }
                catch (Exception ex)
                {
                    errorMessage =
                        string.Format(
                            "Could not send email for element {0}.\n\nAre the To and From legitimate email addresses?\n\nInternal error was: {1}",
                            element.ElementType, ex.Message);
                    DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                }
            }
        }

        static void mailClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            var message = string.Format("Email message sent successfully");
            if (e.Error != null)
            {
                message = string.Format("Could not send email message for element : {0}\n\nInternal error was: {1}",
                                        ((MailMessage) e.UserState).Subject,
                                        e.Error.Message);
                DepictionAccess.NotificationService.DisplayMessageString(message);
            }
            else
                DepictionAccess.NotificationService.DisplayMessageString(message, 10);
        }

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var elements = parameters["Elements"] as List<IDepictionElement>;
            var emailInfo = parameters["EmailInfo"] as OutgoingEmailSettings;
            SendElements(elements, emailInfo);
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            //            DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Email message sent successfully"), 10));
        }

        #endregion
    }
}