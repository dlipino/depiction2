using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.LiveReports.Models;
using Depiction.LiveReports.ViewModels;
using Depiction.LiveReports.Views;

namespace Depiction.LiveReports
{
    [DepictionDefaultImporterMetadata("LiveReportImporter", DisplayName = "Depiction's Live Report importer")]
    public class LiveReportImporter : AbstractDepictionDefaultImporter
    {
        //Awkward. I don't think this VM can be used
        private LiveReportSettupVM liveReportVM = new LiveReportSettupVM();

        #region Implementation of IDepictionElementIOStream
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            LiveReportSourceInfo sourceInfo = new LiveReportSourceInfo();

            sourceInfo.Pop3Server = liveReportVM.SelectedEmailAccount.Pop3Server;
            var rawName = liveReportVM.SelectedEmailAccount.UserName;
            sourceInfo.SimpleUserName = rawName;
            sourceInfo.UseSSL = liveReportVM.SelectedEmailAccount.UseSSL;
            sourceInfo.Password = liveReportVM.EmailPassword;
            int port = -1;
            if(!int.TryParse(liveReportVM.SelectedEmailAccount.PortNumber, out port))
            {
                port = -1;
            }
            sourceInfo.PortNumber = port;
            var readerModel = new DepictionEmailReaderBackgroundService(sourceInfo);
            double minuteRefresh;
            if(double.TryParse(liveReportVM.RefreshInterval, out minuteRefresh))
            {
                readerModel.RefreshRateMinutes = minuteRefresh;
            }

            if (!readerModel.IsValidEmailAddressPassword()) return;
            liveReportVM.UpdateIncomingAccounts();
            var name = string.Format("Email: {0}", sourceInfo.CompleteUserName);
            
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(readerModel);
            readerModel.UpdateStatusReport(name);
            readerModel.StartBackgroundService(readerModel);
        }

//        //THis is kind of separate from the view stuff, so it is not safe to mix them together
//        public void GetElementsFromStreamLocation(object streamInfo, string defaultElementType, IMapCoordinateBounds depictionRegion)
//        {
//            LiveReportSourceInfo sourceInfo = new LiveReportSourceInfo();
//
//            sourceInfo.Pop3Server = liveReportVM.SelectedEmailAccount.Pop3Server;
//            var rawName = liveReportVM.SelectedEmailAccount.UserName;
//            sourceInfo.SimpleUserName = rawName;
//            sourceInfo.UseSSL = liveReportVM.SelectedEmailAccount.UseSSL;
//            sourceInfo.Password = liveReportVM.EmailPassword;
//
//            var readerModel = new DepictionEmailReaderBackgroundService(sourceInfo);
//            if (!readerModel.IsValidEmailAddressPassword()) return;
//            liveReportVM.UpdateIncomingAccounts();
//            var name = string.Format("Email: {0}", sourceInfo.CompleteUserName);
//            readerModel.UpdateStatusReport(name);
//            DepictionAccess.BackgroundServiceManager.AddBackgroundService(readerModel);
//            readerModel.StartBackgroundService(readerModel);
//        }

        override public object AddonConfigView
        {
            get
            {
                LiveReportsFullView addinView = new LiveReportsFullView();
                addinView.DataContext = AddinViewModel;
                return addinView;
            }
        }
        override public ViewModelBase AddinViewModel
        {
            get { return liveReportVM; }
        }

        #endregion


    }
}