﻿using System.Windows.Controls;
using Depiction.API.MVVM;
using Depiction.LiveReports.EmailConfiguration;
using Depiction.LiveReports.ViewModels;

namespace Depiction.LiveReports.Views
{
    /// <summary>
    /// Interaction logic for ElementEmailSenderView.xaml
    /// </summary>
    public partial class ElementEmailSenderView 
    {
        static internal int selectedIndex = 0;
        public ElementEmailSenderView()
        {
            InitializeComponent();
            //Title = "Send email report";
            //Ick
            Closed += ElementEmailSenderView_Closed;
            DataContextChanged += ElementEmailSenderView_DataContextChanged;
        }

        void ElementEmailSenderView_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {

            var oldDC = e.OldValue as DialogViewModelBase;
            var newDC = e.NewValue as DialogViewModelBase;
            if(oldDC != null)
            {
                oldDC.PropertyChanged -= DialogDataContext_PropertyChanged;
            }
            if (newDC != null)
            {
                newDC.PropertyChanged += DialogDataContext_PropertyChanged;
                Title = newDC.DialogDisplayName;
            }

        }

        void DialogDataContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("IsDialogVisible"))
           {
               var dc = DataContext as DialogViewModelBase;
               
//               if (dc != null && !dc.IsDialogVisible) IsShown = false;
           }
        }

//        public void UpdateToLastSelected()
//        {
//            var cmbox = contentPresenter.ContentTemplate.FindName("cmbBoxForAddress", contentPresenter) as ComboBox;
//            if (cmbox != null)
//            {
//                cmbox.SelectedIndex = selectedIndex;
//            }
//        }

        void ElementEmailSenderView_Closed(object sender, System.EventArgs e)
        {
            var cmbox = cmbBoxForAddress;
            if (cmbox != null)
            {
                selectedIndex = cmbox.SelectedIndex;
                cmbox.SelectedIndex = 0;
            }
        }
        

        private void txtPassword_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            var dc = DataContext as EmailSenderSetupVM;
            if (dc == null) return;
            var pwBox = sender as PasswordBox;
            if(pwBox == null) return;
            dc.EmailSendSettings.Password = pwBox.Password; 
        }

//        private void btnSendToEmailFromDialog_Click(object sender, System.Windows.RoutedEventArgs e)
//        {
//            IsShown = false;
//        }

        private void cmbBoxForAddress_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dc = DataContext as EmailSenderSetupVM;
            if(dc == null) return;
            var cmbox = sender as ComboBox;
            if (cmbox == null) return;
            dc.EmailSendSettings = cmbox.SelectedItem as OutgoingEmailSettings;

            var pw =txtPassword  as PasswordBox;
            if (pw != null) pw.Password = string.Empty;

        }
    }
}