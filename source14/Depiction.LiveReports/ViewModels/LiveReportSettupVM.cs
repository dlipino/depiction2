using Depiction.API.MVVM;
using Depiction.LiveReports.EmailConfiguration;

namespace Depiction.LiveReports.ViewModels
{
    public class LiveReportSettupVM : ViewModelBase
    {
        #region Variables

        IncomingEmailAccountRecords records = new IncomingEmailAccountRecords();
        private IncomingEmailSettings currentEmailAccount;

        #endregion

        #region Properties

        public string EmailPassword { get; set; }
        public string RefreshInterval { get; set; }

        public IncomingEmailSettings SelectedEmailAccount
        {
            get { return currentEmailAccount; }
            set
            {
                currentEmailAccount = value; NotifyPropertyChanged("SelectedEmailAccount");
            }
        }

        public IncomingEmailAccountRecords IncomingEmailAccountList
        {
            get
            {
                return records;
            }
            private set
            {
                records = value; NotifyPropertyChanged("IncomingEmailAccountList");

            }
        }

        #endregion

        #region Constructor

        public LiveReportSettupVM()
        {
            UpdateIncomingAccounts();
            RefreshInterval = "2";

        }
        #endregion

        #region Helpers

        public void UpdateIncomingAccounts()
        {
            IncomingEmailAccountRecords incomingEmailAccounts = new IncomingEmailAccountRecords();////No clue how this is supposed to work
            IncomingEmailAccountRecords currentAccounts = EmailInformationSettings.GetIncomingEmailAccounts() ?? new IncomingEmailAccountRecords();
            // Add an email account with empty user name and server; this will create a
            // binding with "New account..." on the dialog ...
            incomingEmailAccounts.Insert(new IncomingEmailSettings { ShowAdvancedFields = true, IsNewRecord = true, NewRecordName = "Other Pop3 account...", ExplanatoryText = "Live reports requires your email service to use the POP3 protocol. For more information, see the section \"Live Reports\" in Depiction help." }, 0);
            incomingEmailAccounts.Insert(new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Yahoo! Mail Plus account", Pop3Server = "plus.pop.mail.yahoo.com", SuggestedDomain = "@yahoo.com", UseSSL = false, ExplanatoryText = "Note: Live reports requires the paid Yahoo! Mail Plus account, since the free Yahoo! Mail accounts do not support the POP3 access that Live reports uses." }, 0);
            incomingEmailAccounts.Insert(new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Microsoft Hotmail account", Pop3Server = "pop3.live.com", SuggestedDomain = "@hotmail.com", UseSSL = true, ExplanatoryText = "Note: Free Microsoft Hotmail accounts can only be logged in to every 15 minutes." }, 0);
            incomingEmailAccounts.Insert(new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Google Gmail account", Pop3Server = "pop.gmail.com", SuggestedDomain = "@gmail.com", ExplanatoryText ="", UseSSL = true }, 0);

            foreach (var account in currentAccounts)
            {
                incomingEmailAccounts.Add((IncomingEmailSettings)account);
            }

            IncomingEmailAccountList = incomingEmailAccounts;
        }
        #endregion
    }
}