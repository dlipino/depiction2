using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.LiveReports.EmailConfiguration;
using Depiction.LiveReports.Models;

namespace Depiction.LiveReports.ViewModels
{
    public class EmailSenderSetupVM :DialogViewModelBase
    {
        OutgoingEmailAccountRecords records = new OutgoingEmailAccountRecords();
        private DelegateCommand<List<IDepictionElement>> sendElementsCommand;
        private DelegateCommand<OutgoingEmailSettings> removeAccoundCommand;
        private IEnumerable<IDepictionElement> elementsToSend = new List<IDepictionElement>();
        private OutgoingEmailSettings currentEmailSettings = null;
        
        #region properties

        public OutgoingEmailSettings EmailSendSettings
        {
            get { return currentEmailSettings; }
            set { currentEmailSettings = value; NotifyPropertyChanged("EmailSendSettings"); }
        }
        
        public IEnumerable<IDepictionElement> ElementsToSend
        {
            get
            {
                return elementsToSend;
            }
        }
        
        public OutgoingEmailAccountRecords OutgoingEmailAccountList
        {
            get
            {
                return records;
            }
            private set
            {
                records = value; NotifyPropertyChanged("OutgoingEmailAccountList");
            }
        }

        public ICommand SendElementEmailCommand
        {
            get
            {
                if(sendElementsCommand == null)
                {
                    sendElementsCommand = new DelegateCommand<List<IDepictionElement>>(SendElementsByEmail, CanSendEmail);
                    sendElementsCommand.Text = "Send to email address";
                }
                return sendElementsCommand;
            }
        }

        public ICommand RemoveAccountCommand
        {
            get
            {
                if (removeAccoundCommand == null)
                {
                    removeAccoundCommand = new DelegateCommand<OutgoingEmailSettings>(RemoveAccount, CanRemoveAccount);
                    removeAccoundCommand.Text = "Remove email account";
                }
                return removeAccoundCommand;
            }
        }


        #endregion

        #region COnstructor

        public EmailSenderSetupVM(IEnumerable<IDepictionElement> elements,int index)
        {
            if (elements == null)
            {
                DialogDisplayName = string.Format("No elements to send");
            }
            else
            {
                var count = elements.Count();
                if (count == 1)
                {
                    DialogDisplayName = string.Format("Send {0} email report", count);
                }
                else
                {
                    DialogDisplayName = string.Format("Send {0} email reports", count);
                }
            }
            elementsToSend = elements;
            UpdateAccountList(index);
        }

        #endregion
        private void UpdateAccountList(int targetIndex)
        {
            var accounts = EmailInformationSettings.GetOutgoingEmailAccounts() ?? new OutgoingEmailAccountRecords();

            accounts.AddOrReplaceExisting(new OutgoingEmailSettings(true) { AccountName = "New Account" });
            OutgoingEmailAccountList = accounts;

            if(targetIndex ==-1)
            {
                EmailSendSettings = OutgoingEmailAccountList[OutgoingEmailAccountList.Count -1];
                return;
            }
            if (targetIndex < OutgoingEmailAccountList.Count)
            {
                EmailSendSettings = OutgoingEmailAccountList[targetIndex];
            }
            else
            {
                EmailSendSettings = OutgoingEmailAccountList[0];
            }
        }
        private void SendElementsByEmail(List<IDepictionElement> obj)
        {
            var emailService = new SendElementLiveReportEmailService(true);
            var emailData = EmailSendSettings;
            emailData.AccountName = emailData.UserName;
            if (EmailSendSettings.IsNew)
            {
                EmailSendSettings.IsNew = false;
                EmailInformationSettings.AddOutgoingEmailAccount(emailData);
                OutgoingEmailAccountList.Insert(new OutgoingEmailSettings(true) { AccountName = emailData.UserName },0);
            }else
            {//Update?
                EmailInformationSettings.AddOutgoingEmailAccount(emailData);
            }
            emailService.StartSendingElements(elementsToSend, emailData);
            IsDialogVisible = false;
        }

        private bool CanSendEmail(List<IDepictionElement> arg)
        {
            if (arg == null) return false;
            if (EmailSendSettings == null) return false;
            return EmailSendSettings.IsAllInformationPresent();
        }

        private bool CanRemoveAccount(OutgoingEmailSettings accountToDelete)
        {
            if (accountToDelete.IsNew) return false;
            return true;
        }

        private void RemoveAccount(OutgoingEmailSettings accountToDelete)
        {
            EmailInformationSettings.RemoveOutgoingEmailAccount(accountToDelete);
            UpdateAccountList(-1);
        }
    }
}