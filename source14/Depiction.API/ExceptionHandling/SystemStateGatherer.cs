﻿using System;
using System.Text;
using System.Management;

namespace Depiction.API.ExceptionHandling
{
    public class SystemStateGatherer
    {
        public static string GetSystemInformation()
        {
            var sb = new StringBuilder();

            sb.Append("\r\nSystem Information:");
            sb.Append("\r\nOperating System: " + Environment.OSVersion.VersionString);
            sb.Append("\r\nProcessor Count: " + Environment.ProcessorCount);
            sb.Append("\r\nProcessor Identifier: " + Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"));
            sb.Append("\r\nProcessor Architecture: " + Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"));

            return sb.ToString();
        }

        public static string GetSystemMemoryInformation()
        {
            var sb = new StringBuilder();
//            var mos = new ManagementObjectSearcher();
//
//            ObjectQuery winQuery = new ObjectQuery("SELECT * FROM Win32_LogicalMemoryConfiguration");
//
//ManagementObjectSearcher searcher = new ManagementObjectSearcher(winQuery);
//
//foreach (ManagementObject item in searcher.Get())
//{
//Console.WriteLine("Total Space = " + item["TotalPageFileSpace"]);
//Console.WriteLine("Total Physical Memory = " + item["TotalPhysicalMemory"]);
//Console.WriteLine("Total Virtual Memory = " + item["TotalVirtualMemory"]);
//Console.WriteLine("Available Virtual Memory = " + item["AvailableVirtualMemory"]);
//}

//            sb.Append("\r\nSystem Memory Information:");
//
//            sb.Append("\r\nOperating System: " + Environment..VersionString);
//            sb.Append("\r\nProcessor Count: " + Environment.ProcessorCount);
//            sb.Append("\r\nProcessor Identifier: " + Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"));
//            sb.Append("\r\nProcessor Architecture: " + Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"));

            return sb.ToString();
        }
    }
}