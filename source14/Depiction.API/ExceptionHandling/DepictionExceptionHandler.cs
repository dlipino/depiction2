﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Depiction.API.ExceptionHandling
{
    /// <summary>
    /// The DepictionExceptionHandler handles exceptions.
    /// </summary>
    public static class DepictionExceptionHandler
    {
        /// <summary>
        /// Policy for logging exceptions.
        /// </summary>
        public static bool ExceptionLoggingEnabled = true;

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void HandleException(Exception ex)
        {
            HandleException(ex, true,true);
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        public static void HandleException(string message, Exception ex)
        {
            HandleException(new ApplicationException(message, ex));
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        /// <param name="shouldShowMessage">if set to <c>true</c> [should show message].</param>
        public static void HandleException(string message, Exception ex, bool shouldShowMessage)
        {
            HandleException(new ApplicationException(message, ex), shouldShowMessage,false);
        }
        public static void HandleException(string message, Exception ex, bool shouldShowMessage,bool shouldLogException)
        {
            HandleException(new ApplicationException(message, ex), shouldShowMessage,shouldLogException);
        }

        public static void HandleException(Exception ex, bool shouldShowMessage)
        {
            HandleException(ex, shouldShowMessage, true);
        }

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="shouldShowMessage">if set to <c>true</c> [should show message].</param>
        public static void HandleException(Exception ex, bool shouldShowMessage,bool shouldLogException)
        {
            if (ex == null)
            {
                return;
            }
            bool rethrow = false;

            // TODO: This should be changed; new enterprise library checkin screwed this up
            try
            {
                if (ExceptionLoggingEnabled && shouldLogException)
                {
                   // var exeConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                   // var appSettings = ConfigurationManager.GetSection("loggingConfiguration");
                    rethrow = ExceptionPolicy.HandleException(ex, "Default Policy");
                }
            }
            catch (Exception e)
            {
                DepictionAccess.NotificationService.SendNotificationDialog(string.Format("{0}<br/><br/>Could not log this exception because:<br/>{1}<br/><br/>Please contact {2}", 
                    ex.Message, e.Message, DepictionAccess.ProductInformation.CompanyName));
            }

            if (shouldShowMessage && OnErrorHandled != null)
                OnErrorHandled(ex);
            if (rethrow)
                throw ex;
        }

//        public static void SetExceptionLogging(bool LogExceptions)
//        {
//            ShouldBeLogged = LogExceptions;
//        }

        /// <summary>
        /// Occurs when an error is handled.
        /// </summary>
        public static event ErrorHandledEventArgs OnErrorHandled;

        /// <summary>
        /// Returns string containing all of the exception messages, regardless of the
        /// number of inner exceptions.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string MessageFromExceptionAndInnerException(Exception ex)
        {
            if (ex.InnerException == null)
                return ex.Message;
            return string.Format("{0}\n\nInternal error: {1}", ex.Message, MessageFromExceptionAndInnerException(ex.InnerException));
        }
        /// <summary>
        /// Arguments used when an error is handled.
        /// </summary>
        public delegate void ErrorHandledEventArgs(Exception ex);
    }
}