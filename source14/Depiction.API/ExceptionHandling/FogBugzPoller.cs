﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using FogBugz;

namespace Depiction.API.ExceptionHandling
{
    public class FogBugzPoller
    {
        private readonly List<Exception> exceptionQueue = new List<Exception>();
        private Timer pollTimer;
        public Stack ExceptionMessageStack = new Stack();
        public event Action MessageAddedToStack;

        public FogBugzPoller()
        {
            DepictionExceptionHandler.OnErrorHandled += OnErrorHandled;
            pollTimer = new Timer(CheckForThrownExceptions, null, 0, 5000);
        }

//        ~FogBugzPoller()
//        {
//            Terminate();
//        }

        public void Terminate()
        {
            if (pollTimer != null)
            {
                Console.WriteLine("Ending fogbugz.");
                pollTimer.Dispose();
                pollTimer = null;
            }
            DepictionExceptionHandler.OnErrorHandled -= OnErrorHandled;
        }
        private void CheckForThrownExceptions(object state)
        {
            HandleExceptions();
        }

        #region Private methods
        private void HandleExceptions()
        {
            Exception[] exceptions;
            lock (exceptionQueue)
            {
                exceptions = exceptionQueue.ToArray();
                exceptionQueue.Clear();
            }
            if(exceptions.Length>0)
            {
                ExceptionMessageStack.Push(CreateExceptionString(exceptions));


                if(MessageAddedToStack != null) MessageAddedToStack.Invoke();
            }
        }
        private string CreateExceptionString(Exception[] exceptionsThatOccurred)
        {
            var sb = new StringBuilder();
            var depictionStory = DepictionAccess.CurrentDepiction;
            if (depictionStory != null)
            {

                sb.AppendFormat("Current region extent: {0}\r\n", depictionStory.DepictionGeographyInfo.DepictionRegionBounds);
                sb.AppendFormat("Current story name: {0}\r\n", depictionStory.Metadata.Title);
                sb.AppendFormat("Number of revealers: {0}\r\n", depictionStory.Revealers.Count);
                
                sb.AppendFormat("Current number of elements: {0}\r\n", depictionStory.CompleteElementRepository.AllElements.Count);
            }
            sb.AppendLine(SystemStateGatherer.GetSystemInformation());
            sb.AppendLine(SystemStateGatherer.GetSystemMemoryInformation());
            sb.AppendFormat("Application version: {0} (build {1}\r\n", VersionInfo.FullVersion(false), VersionInfo.BuildNumber);
            sb.AppendLine(GetExceptionsStackTrace(exceptionsThatOccurred));
            string exceptionInfo = sb.ToString();
            if (exceptionsThatOccurred.Length > 0)
                return exceptionInfo;
            return string.Empty;
        }

        private static string GetExceptionsStackTrace(IEnumerable<Exception> exceptions)
        {
            var sb = new StringBuilder();

            foreach (Exception exception in exceptions)
            {
                sb.AppendLine();
                sb.Append(GetStackTrace(exception));
                sb.AppendLine();
                sb.AppendLine("End of exception");
            }

            return sb.ToString();
        }
        private static string GetStackTrace(Exception ex)
        {
            var sb = new StringBuilder();
            var st = new StackTrace(ex, true);
            StackFrame[] frames = st.GetFrames();

            sb.AppendLine(ex.Message);

            if (frames != null)
                foreach (StackFrame frame in frames)
                {
                    sb.AppendLine(frame.GetFileName() + ": " + frame.GetMethod() + ": " + frame.GetFileLineNumber());
                }

            if (ex.InnerException != null)
                sb.Append("\r\n\r\n" + GetStackTrace(ex.InnerException));

            return sb.ToString();
        }

        #endregion

        #region public methods
        public void OnErrorHandled(Exception ex)
        {
            lock (exceptionQueue)
            {
                exceptionQueue.Add(ex);
            }
        }

        public void SubmitBug(string exceptionInfo, string userEmail, string userDescription)
        {
            var bug = new BugReport("https://simiogeo.fogbugz.com/scoutSubmit.asp", "Depiction Bug Submitter");

            //TODO: Right now we only have the "Misc" area. We need to talk about what areas we need
            bug.Project = "Depiction";
            bug.Area = "Misc";

            // Believe it or not, Fogbugz requires Description to be a non-zero length string.
            bug.Description = string.IsNullOrEmpty(userDescription) ? "<empty>" : userDescription;
            bug.ExtraInformation = exceptionInfo;

            if (!string.IsNullOrEmpty(userEmail))
                bug.Email = userEmail;

            // Never force a new issue to be created; always append in the title is the same
            bug.ForceNewBug = false;
            try
            {
                bug.Submit();
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(String.Format("Could not send this bug report. Are you connected to the Internet?\r\nInternal error is: {0}", ex.Message));
            }
        }

        #endregion
    }
}