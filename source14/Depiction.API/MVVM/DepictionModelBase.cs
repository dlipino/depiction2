using System.ComponentModel;

namespace Depiction.API.MVVM
{
    public class DepictionModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool DisableNotification { get; set; }
        #region Property change notifiier

        protected void NotifyModelPropertyChangedInstant(string propertyName)
        {
            if (DisableNotification) return;
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void NotifyModelPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null || DisableNotification)
                return;
            //Should need the dispatcher since no UI stuff is controlled by this, hopefully
//            if (DepictionAccess.DispatchAvailableAndNeeded())
//            {
//                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<string>(NotifyModelPropertyChanged), propertyName);
//                return;
//            }

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}