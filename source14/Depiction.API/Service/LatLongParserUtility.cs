﻿using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.API.Service
{
    public  class LatLongParserUtility
    {
        //Can't null means it couldn't actually parse the thing, false means it parsed the value but it was not a real world coord, true means everything is ok
        public static bool? ParseForLatLong(string addressString, out double latitude, out double longitude, out string parseMessage)
        {
            var options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            latitude = longitude = double.NaN;
            parseMessage = "";
            // 40.446195, -79.948862
            var decimalSeparator = Regex.Escape(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            var signedReal = string.Format(@"[\-]*\d+((?<separator>[{0}\.])\d+)?", decimalSeparator);

            var listSeparator = Regex.Escape(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
            var signedRealSignedReal = string.Format(@"^\s*(?<lat>{0})[\s,{1}]+(?<long>{2})\s*$", signedReal, listSeparator, signedReal);
            var matches = Regex.Matches(addressString, signedRealSignedReal, options);
            if (matches.Count == 1)
            {
                var match = matches[0];
                if (match.Groups["separator"].Value == ".")
                {
                    latitude = double.Parse(match.Groups["lat"].Value, CultureInfo.InvariantCulture);
                    longitude = double.Parse(match.Groups["long"].Value, CultureInfo.InvariantCulture);
                }
                else
                {
                    latitude = double.Parse(match.Groups["lat"].Value, CultureInfo.CurrentCulture);
                    longitude = double.Parse(match.Groups["long"].Value, CultureInfo.CurrentCulture);
                }
                return ValidateLatLong(latitude, longitude, out parseMessage);
            }

            // 40.446195N 79.948862W
            var unsignedReal = string.Format(@"\d+((?<separator>[{0}\.])\d+)?", decimalSeparator);
            var realNWrealEW = string.Format(@"^\s*(?<lat>{0})\s*(?<ns>[NnSs]?)[\s,{1}]+(?<long>{2})\s*(?<ew>[EeWw]?)\s*$", unsignedReal, listSeparator, unsignedReal);
            matches = Regex.Matches(addressString, realNWrealEW, options);
            if (matches.Count == 1)
            {
                var match = matches[0];

                if (match.Groups["separator"].Value == ".")
                {
                    latitude = double.Parse(match.Groups["lat"].Value, CultureInfo.InvariantCulture);
                    if (match.Groups["ns"].Value.ToLower().Equals("s"))
                        latitude = -latitude;
                    longitude = double.Parse(match.Groups["long"].Value, CultureInfo.InvariantCulture);
                    if (match.Groups["ew"].Value.ToLower().Equals("w"))
                        longitude = -longitude;
                }
                else
                {
                    latitude = double.Parse(match.Groups["lat"].Value, CultureInfo.CurrentCulture);
                    if (match.Groups["ns"].Value.ToLower().Equals("s"))
                        latitude = -latitude;
                    longitude = double.Parse(match.Groups["long"].Value, CultureInfo.CurrentCulture);
                    if (match.Groups["ew"].Value.ToLower().Equals("w"))
                        longitude = -longitude;
                }
                return ValidateLatLong(latitude, longitude, out parseMessage);
            }

            var degreeMinutesSecondsFormats =
                new List<string> 
                    {
                        // 40:26:46N,79:56:55W - minutes & seconds are optional
                        string.Format(@"^\s*(?<latDegrees>\d+)\s*(:\s*(?<latMinutes>\d+)\s*(:\s*(?<latSeconds>{0}))?)?\s*(?<ns>[NnSs]?)[\s,{1}]+(?<longDegrees>\d+)\s*:\s*((?<longMinutes>\d+)\s*(:\s*(?<longSeconds>{0}))?)?\s*(?<ew>[EeWw]?)\s*$",
                                      unsignedReal, listSeparator),
                        // 40°26'46"N 79°56'55"W - minutes & seconds are optional
                        string.Format(@"^\s*(?<latDegrees>\d+)°\s*((?<latMinutes>\d+)'\s*((?<latSeconds>{0})""\s*)?)?(?<ns>[NnSs]?)[\s,{1}]+(?<longDegrees>\d+)°\s*((?<longMinutes>\d+)'\s*((?<longSeconds>{0})""\s*)?)?(?<ew>[EeWw]?)\s*$",
                                      unsignedReal, listSeparator),
                        //40d 26' 46" N 79d 56' 55" W - minutes & seconds are optional
                        string.Format(@"^\s*(?<latDegrees>\d+)[dD]\s*((?<latMinutes>\d+)'\s*((?<latSeconds>{0})""\s*)?)?(?<ns>[NnSs]?)[\s,{1}]+(?<longDegrees>\d+)[dD]\s*((?<longMinutes>\d+)'\s*((?<longSeconds>{0})""\s*)?)?(?<ew>[EeWw]?)\s*$",
                                      unsignedReal, listSeparator)
                    };

            foreach (var pattern in degreeMinutesSecondsFormats)
            {
                matches = Regex.Matches(addressString, pattern, options);
                if (matches.Count == 1)
                {
                    return ComputeDegreesMinutesSeconds(matches, out latitude, out longitude, out parseMessage);
                }
            }

            //40° 26.7717, -79° 5693172
            var degreesWithReal = string.Format(@"^\s*(?<latDegrees>[\-]*\d+)°\s*(?<latMinutes>{0})[\s,{1}]+(?<longDegrees>[\-]*\d+)°\s*(?<longMinutes>{0})\s*$",
                                                unsignedReal, listSeparator);
            matches = Regex.Matches(addressString, degreesWithReal, options);
            if (matches.Count == 1)
            {
                var match = matches[0];
                var latDegrees = double.Parse(match.Groups["latDegrees"].Value, CultureInfo.InvariantCulture);
                var latMinutes = double.Parse(match.Groups["latMinutes"].Value, CultureInfo.InvariantCulture);
                if (latDegrees < 0)
                    latitude = latDegrees - latMinutes / 60;
                else
                    latitude = latDegrees + latMinutes / 60;

                var longDegrees = double.Parse(match.Groups["longDegrees"].Value, CultureInfo.InvariantCulture);
                var longMinutes = double.Parse(match.Groups["longMinutes"].Value, CultureInfo.InvariantCulture);
                if (longDegrees < 0)
                    longitude = longDegrees - longMinutes / 60;
                else
                    longitude = longDegrees + longMinutes / 60;

                return ValidateLatLong(latitude, longitude, out parseMessage);
            }
            //40 26.7717, -79 5693172
            //same as above but without °, the degree symbol
            var nodegreesWithReal = string.Format(@"^\s*(?<latDegrees>[\-]*\d+)\s*(?<latMinutes>{0})[\s,{1}]+(?<longDegrees>[\-]*\d+)\s*(?<longMinutes>{0})\s*$",
                                                  unsignedReal, listSeparator);
            matches = Regex.Matches(addressString, nodegreesWithReal, options);
            if (matches.Count == 1)
            {
                var match = matches[0];
                var latDegrees = double.Parse(match.Groups["latDegrees"].Value, CultureInfo.InvariantCulture);
                var latMinutes = double.Parse(match.Groups["latMinutes"].Value, CultureInfo.InvariantCulture);
                if (latDegrees < 0)
                    latitude = latDegrees - latMinutes / 60;
                else
                    latitude = latDegrees + latMinutes / 60;

                var longDegrees = double.Parse(match.Groups["longDegrees"].Value, CultureInfo.InvariantCulture);
                var longMinutes = double.Parse(match.Groups["longMinutes"].Value, CultureInfo.InvariantCulture);
                if (longDegrees < 0)
                    longitude = longDegrees - longMinutes / 60;
                else
                    longitude = longDegrees + longMinutes / 60;

                return ValidateLatLong(latitude, longitude, out parseMessage);
            }

            //40° 26.7717N, 79° 55.93172W
            var degreesWithRealNWSE = string.Format(@"^\s*(?<latDegrees>\d+)°\s*(?<latMinutes>{0})\s*(?<ns>[NnSs]?)[\s,{1}]+(?<longDegrees>\d+)°\s*(?<longMinutes>{0})\s*(?<ew>[EeWw]?)\s*$",
                                                    unsignedReal, listSeparator);
            matches = Regex.Matches(addressString, degreesWithRealNWSE, options);
            if (matches.Count == 1)
            {
                var match = matches[0];
                var latDegrees = double.Parse(match.Groups["latDegrees"].Value, CultureInfo.InvariantCulture);
                var latMinutes = double.Parse(match.Groups["latMinutes"].Value, CultureInfo.InvariantCulture);
                latitude = latDegrees + latMinutes / 60;
                if (match.Groups["ns"].Value.ToLower().Equals("s"))
                    latitude = -latitude;

                var longDegrees = double.Parse(match.Groups["longDegrees"].Value, CultureInfo.InvariantCulture);
                var longMinutes = double.Parse(match.Groups["longMinutes"].Value, CultureInfo.InvariantCulture);
                longitude = longDegrees + longMinutes / 60;
                if (match.Groups["ew"].Value.ToLower().Equals("w"))
                    longitude = -longitude;
                return ValidateLatLong(latitude, longitude, out parseMessage);
            }
            //40 26.7717N, 79 55.93172W
            //same format as above, but without the degree symbol
            var degWithRealNWSE = string.Format(@"^\s*(?<latDegrees>\d+)\s*(?<latMinutes>{0})\s*(?<ns>[NnSs]?)[\s,{1}]+(?<longDegrees>\d+)\s*(?<longMinutes>{0})\s*(?<ew>[EeWw]?)\s*$",
                                                unsignedReal, listSeparator);
            matches = Regex.Matches(addressString, degWithRealNWSE, options);
            if (matches.Count == 1)
            {
                var match = matches[0];
                var latDegrees = double.Parse(match.Groups["latDegrees"].Value, CultureInfo.InvariantCulture);
                var latMinutes = double.Parse(match.Groups["latMinutes"].Value, CultureInfo.InvariantCulture);
                latitude = latDegrees + latMinutes / 60;
                if (match.Groups["ns"].Value.ToLower().Equals("s"))
                    latitude = -latitude;

                var longDegrees = double.Parse(match.Groups["longDegrees"].Value, CultureInfo.InvariantCulture);
                var longMinutes = double.Parse(match.Groups["longMinutes"].Value, CultureInfo.InvariantCulture);
                longitude = longDegrees + longMinutes / 60;
                if (match.Groups["ew"].Value.ToLower().Equals("w"))
                    longitude = -longitude;
                return ValidateLatLong(latitude, longitude, out parseMessage);
            }
            //This will fail on numbers that use a ',' as a decimal
            string[] latLonString = addressString.Split(',');
            if (latLonString.Length == 2)
            {
                if (latLonString[0].Trim().Equals(double.NaN.ToString()) && latLonString[1].Trim().Equals(double.NaN.ToString()))
                {
                    return true;
                }
            }
            return null;
        }

        /// <summary>
        /// Given a match, compute the lat/long values from the degrees, minutes, and seconds.
        /// Minutes and seconds are optional.
        /// </summary>
        /// <param name="matches">Matches from a regex operation</param>
        /// <param name="latitude">Latitude in double</param>
        /// <param name="longitude">Longitude in double</param>
        /// <param name="message">error message from parsing...use if return value is false</param>
        /// <returns></returns>
        private static bool ComputeDegreesMinutesSeconds(MatchCollection matches, out double latitude, out double longitude, out string message)
        {
            var match = matches[0];
            double latDegrees = int.Parse(match.Groups["latDegrees"].Value);
            double latMinutes = 0;
            if (match.Groups["latMinutes"].Success)
                latMinutes = int.Parse(match.Groups["latMinutes"].Value);
            double latSeconds = 0;
            if (match.Groups["latSeconds"].Success)
                latSeconds = double.Parse(match.Groups["latSeconds"].Value, CultureInfo.InvariantCulture);
            latitude = latDegrees + latMinutes / 60 + latSeconds / 3600;
            if (match.Groups["ns"].Value.ToLower().Equals("s"))
                latitude = -latitude;

            double longDegrees = int.Parse(match.Groups["longDegrees"].Value);
            double longMinutes = 0;
            if (match.Groups["longMinutes"].Success)
                longMinutes = int.Parse(match.Groups["longMinutes"].Value);
            double longSeconds = 0;
            if (match.Groups["longSeconds"].Success)
                longSeconds = double.Parse(match.Groups["longSeconds"].Value, CultureInfo.InvariantCulture);
            longitude = longDegrees + longMinutes / 60 + longSeconds / 3600;
            if (match.Groups["ew"].Value.ToLower().Equals("w"))
                longitude = -longitude;

            return ValidateLatLong(latitude, longitude, out message);

        }

        public static bool ValidateLatLong(double latitude, double longitude, out string message)
        {
            message = "";

            if (latitude == LatitudeLongitudeBase.INVALID_VALUE && longitude == LatitudeLongitudeBase.INVALID_VALUE)
                return true;
            if (latitude < -85 || latitude > 85)
            {
                if (latitude < -90 || latitude > 90)
                {
                    message = string.Format("Latitude of {0} is invalid; must be between -90 and 90.", latitude);
                    return false;
                }
                message = string.Format("Latitude of {0} is in a non mercator zone; must be between -85 and 85.", latitude);
                return false;
            }
            if (longitude < -180 || longitude > 180)
            {
                message = string.Format("Longitude of {0} is invalid; must be between -180 and 180.", longitude);
                return false;
            }

            return true;
        }

    }
}