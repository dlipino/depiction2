using System.Globalization;
using System.Text.RegularExpressions;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;

namespace Depiction.API.Service
{
    public class UtmParserUtility
    {
        //Hack for now
        public static bool ParseForUTMLocationToLatLong(string inUtmLocation, out double latitude, out double longitude, out string parseMessage)
        {
            var options = RegexOptions.IgnoreCase | RegexOptions.Singleline;

            // 10T 548895.0 5270328.0
            var parts = inUtmLocation.Split(' ');

            string[] digits = Regex.Split(inUtmLocation, @"\D+", options);
            var letter = Regex.Match(inUtmLocation, @"[a-z]", options);

            latitude = double.NaN;
            longitude = double.NaN;
            
            if(parts.Length != 3)
            {
                parseMessage = "Incorrect number count";
                return false;
            }
            if(letter.Length != 1)
            {
                parseMessage = "Incorrect letter count";
                return false;
            }
                     //   var outString = string.Format("{0}{1} {2:0} {3:0}", UTMZoneNumber, UTMZoneLetterString, EastingMeters,
                      //                    NorthingMeters);
            var utmLocation = new UTMLocation();
            utmLocation.UTMZoneNumber = int.Parse(digits[0]);
            utmLocation.UTMZoneLetterString = letter.Value;
            utmLocation.EastingMeters = double.Parse(parts[1]);
            utmLocation.NorthingMeters = double.Parse(parts[2]);

            var latLong = utmLocation.UTMToLatLong();
            latitude = latLong.Latitude;
            longitude = latLong.Longitude;

            return LatLongParserUtility.ValidateLatLong(latitude, longitude, out parseMessage);
        }

    }
}