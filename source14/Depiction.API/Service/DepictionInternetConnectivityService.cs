﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows;

namespace Depiction.API.Service
{
    ///<summary>
    /// This class provides access to several services related to Depiction's connection to the web.
    ///</summary>
    public static class DepictionInternetConnectivityService
    {
        //todo: create a networkMonitor library that has a timer and alerts the UI when network status has changed.
        private static bool InternetReachable { get; set; }

        /// <summary>
        /// Is the internet currently reachable?
        /// </summary>
        public static bool IsInternetAvailable
        {
            get
            {
                InternetReachable = false;
                try
                {
                    Dns.GetHostEntry("www.google.com");
                    InternetReachable = true;
                    return true;
                }
                catch (Exception)
                {
                    return InternetReachable;
                }
            }
        }

        ///<summary>
        /// Wire up Depiction to list for changes in Depiction's connection to the web.
        ///</summary>
        public static void ListenForNetworkChange()
        {
            NetworkChange.NetworkAvailabilityChanged += NetworkChange_NetworkAvailabilityChanged;
        }

        private static void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            InternetReachable = false;
            try
            {
                Dns.GetHostEntry("www.google.com");
                InternetReachable = true;
            }
                // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
                // ReSharper restore EmptyGeneralCatchClause
            {
                // do nothing.
            }
        }

        /// <summary>
        /// Opens the given url in your default browser.
        /// If this fails, it will open a MessageBox with the message:
        /// 
        /// "Could not open browser. To {1}, please open your browser of choice and go to {0}."
        /// where {0} is replaced by the url and {1} is replaced by the purpose
        /// </summary>
        /// <param name="url"></param>
        /// <param name="purpose"></param>
        /// <returns></returns>
        public static bool OpenBrowserInOrderTo(string url, string purpose)
        {
            try
            {
                Process.Start(url);
                return true;
            }
            catch
            {
//                DepictionAccess.NotificationService.SendNotificationDialog(string.Format("Could not open browser to {1}.<br/><br/>Please open a web browser and go to {0}.",
//                                                                                         url, purpose),
//                                                                           "Problem Opening Web Browser");
                return false;
            }
        }
    }
}