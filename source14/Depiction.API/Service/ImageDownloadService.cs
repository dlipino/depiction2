﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Depiction.APINew.Service
{
    /// <summary>
    /// A service for downloading images from web services.
    /// </summary>
    public static class ImageDownloadService
    {
        /// <summary>
        /// Download an image of this height and width from the web service at this url.
        /// </summary>
        /// <param name="url">The url containing the web service request to provide an image.</param>
        /// <param name="imageHeight">The number of pixels high to retrieve.</param>
        /// <param name="imageWidth">The number of pixels wide to retrieve.</param>
        /// <returns></returns>
        public static BitmapSource DownloadImage(string url, int imageHeight, int imageWidth)
        {
            BitmapDecoder decoder = null;
            var uri = new Uri(url);
            if (Path.HasExtension(url))
            {
                switch (Path.GetExtension(url).ToLower())
                {
                    case ".gif":
                    case ".giff":
                        decoder = new GifBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                        break;
                    case ".tiff":
                    case ".tif":
                        decoder = new TiffBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                        break;
                    case ".png":
                        decoder = new PngBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                        break;
                    default:
                        decoder = new JpegBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                        break;
                }
            }
            if(decoder == null) return null;
            var defaultImage = decoder.Frames[0];
            BitmapFrame frozen = (BitmapFrame)defaultImage.GetAsFrozen();
            defaultImage = frozen;
            defaultImage.Freeze();

            return defaultImage;
        }
    }
}
