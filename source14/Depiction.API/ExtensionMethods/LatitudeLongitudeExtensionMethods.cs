﻿using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using GeoFramework;

namespace Depiction.API.ExtensionMethods
{
    /// <summary>
    /// Anonymous method that converts the LatitudeLongitude to Geoposition.
    /// </summary>
    public static class LatitudeLongitudeExtensionMethods
    {
        /// <summary>
        /// Gets the geo position.
        /// </summary>
        /// <param name="latlon">The latlon.</param>
        /// <returns></returns>
        public static Position GetGeoPosition(this ILatitudeLongitude latlon)
        {
            return new Position(new Latitude(latlon.Latitude), new Longitude(latlon.Longitude));
        }

        public static UTMLocation ConvertToUTM(this ILatitudeLongitude latlon)
        {
            var position = latlon.GetGeoPosition();
            return new UTMLocation { EastingMeters = position.Easting.ToMeters().Value, NorthingMeters = position.Northing.ToMeters().Value, UTMZoneNumber = position.ZoneNumber, UTMZoneLetter = position.ZoneLetter};
        }
        public static ILatitudeLongitude UTMToLatLong(this UTMLocation utm)
        {
            var position = new Position(utm.UTMZoneLetter, utm.UTMZoneNumber, new Distance(utm.EastingMeters, DistanceUnit.Meters),
                                        new Distance(utm.NorthingMeters, DistanceUnit.Meters));
            var x = position.Longitude.DecimalDegrees;
            var y = position.Latitude.DecimalDegrees;

            return new LatitudeLongitudeBase(y, x);
        }
        //TODO consolodate the duplication between this and the mapcoordinatesbounds distance calcs
        public static double DistanceTo(this ILatitudeLongitude originLatLon, ILatitudeLongitude destinationLatLon, 
                                        MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            return DistanceToAsGeoDistance(originLatLon, destinationLatLon, specificMeasurement, specificScale).Value;
        }
        public static string DistanceToAsString(this ILatitudeLongitude originLatLon, ILatitudeLongitude destinationLatLon,
                                        MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            return DistanceToAsGeoDistance(originLatLon, destinationLatLon, specificMeasurement, specificScale).ToString();
        }
        public static Distance DistanceToAsGeoDistance(this ILatitudeLongitude originLatLon, ILatitudeLongitude destinationLatLon, 
                                        MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var distance = originLatLon.GetGeoPosition().DistanceTo(destinationLatLon.GetGeoPosition());

            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            switch (systemAndScale)
            {
                case BaseMeasurement.ImperialSmall:
                    return distance.ToInches();
                case BaseMeasurement.ImperialNormal:
                    return distance.ToFeet();
                case BaseMeasurement.ImperialLarge:
                    return distance.ToStatuteMiles();
                case BaseMeasurement.MetricSmall:
                    return distance.ToCentimeters();
                case BaseMeasurement.MetricNormal:
                    return distance.ToMeters();
                case BaseMeasurement.MetricLarge:
                    return distance.ToKilometers();
            }
            return distance;
        }

        public static double LongitudeRadians(this ILatitudeLongitude latLon)
        {
            return latLon.GetGeoPosition().Longitude.ToRadians();
        }

        public static double LatitudeRadians(this ILatitudeLongitude latLon)
        {
            return latLon.GetGeoPosition().Latitude.ToRadians();
        }

        public static ILatitudeLongitude TranslateTo(this ILatitudeLongitude latLon, double bearing, double distance, 
            MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            var distanceUnit = DistanceUnit.StatuteMiles;
            switch (systemAndScale)
            {
                case BaseMeasurement.ImperialSmall:
                    distanceUnit = DistanceUnit.Inches;
                    break;
                case BaseMeasurement.ImperialNormal:
                    distanceUnit = DistanceUnit.Feet;
                    break;
                case BaseMeasurement.ImperialLarge:
                    distanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case BaseMeasurement.MetricSmall:
                    distanceUnit = DistanceUnit.Centimeters;
                    break;
                case BaseMeasurement.MetricNormal:
                    distanceUnit = DistanceUnit.Meters;
                    break;
                case BaseMeasurement.MetricLarge:
                    distanceUnit = DistanceUnit.Kilometers;
                    break;
            }
            return TranslateTo(latLon, bearing, distance, distanceUnit);
        }

        public static ILatitudeLongitude TranslatePosition(this ILatitudeLongitude latLon, double easting, double northing, DistanceUnit distanceUnit)
        {
            return latLon.TranslateTo(Azimuth.East, easting, distanceUnit).TranslateTo(Azimuth.North, northing, distanceUnit);
        }

        public static ILatitudeLongitude TranslateTo(this ILatitudeLongitude latLon, double bearing, double distance, DistanceUnit distanceUnit)
        {
            Distance gfDistance = new Distance(distance, distanceUnit);
            var newPosition = latLon.GetGeoPosition().TranslateTo(bearing, gfDistance);
            return new LatitudeLongitude(newPosition.Latitude.DecimalDegrees, newPosition.Longitude.DecimalDegrees);
        }
    }
}