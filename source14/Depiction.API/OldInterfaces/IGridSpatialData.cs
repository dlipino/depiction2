using System.Windows.Media.Imaging;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.OldInterfaces
{
    public interface IGridSpatialData : IDepictionSpatialData
    {
        ILatitudeLongitude TopLeft { get; }
        ILatitudeLongitude BottomRight { get; }
        int PixelWidth { get; }
        int PixelHeight { get; }
        BitmapSource GenerateBitmap();
        byte GetValue(double longitude, double latitude);
        byte GetValue(ILatitudeLongitude pos);
        byte GetValueAtColumnRow(int column, int row);
        float y(int yVal);
        float x(int xVal);
    }
}