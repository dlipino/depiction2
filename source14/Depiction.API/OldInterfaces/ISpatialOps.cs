using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.OldInterfaces
{
    public interface ISpatialOps
    {
        bool Intersects(IDepictionGeometry sp1, IDepictionSpatialData sp2);
        bool Intersects(IGridSpatialData sp1, IDepictionSpatialData sp2);
      
    }
}