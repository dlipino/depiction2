﻿using System;

namespace Depiction.API.ProductTypes
{
    public class DepictionProductInformation : ProductInformationBase
    {
        #region Properties that may be different between Release and Debug

        public override string DirectoryNameForCompany { get { return "Depiction"; } }
        public override string MetricsServiceName { get { return "ReleaseMetricsService"; } }
        public override string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        public override string LicensingServiceName { get { return "ReleaseLicensingService"; } }
        public override string ProductValidatorName { get { return "ReleaseDepictionProductValidator"; } }
        public override string ProcessName { get { return "Depiction"; } }

        #endregion

        #region Properties that vary only on _productInformation "brand"

        public override string UiConfigXamlUri { get { return "Depiction.DefaultResources;Component/ProductResources/UiConfigDefault.xaml"; } }
        public override string FilePassword { get { return null; } }
        public override string FileExtension { get { return ".dpn"; } }
        public override string ProductName { get { return "Depiction"; } }
        public override string CompanyName { get { return "Depiction, Inc."; } }
        public override string SupportEmail { get { return "support@depiction.com"; } }

        public override string StoryName { get { return "depiction"; } }
        public override string SplashScreenPath { get { return "DefaultImages/SplashScreens/DepictionSplash.png"; } }
        public override string LicenseFileName { get { return "licensev1.lic"; } }
        public override int LicenseActivationID { get { return 5; } }
        public override int LicenseRenewalID { get { throw new NotImplementedException(); } }
        public override bool RenewableLicense { get { return false; } }
        public override int ProductID { get { return 1; } }
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",
                                     ProductVersion, DepictionCopyright);
            }
            
        }

        #endregion
    }
}