﻿using Depiction.API.ProductTypes;

namespace Depiction.API.ProductTypes
{
    public class DepictionReaderProductInformation : DepictionProductInformation
    {
        override public string DirectoryNameForCompany { get { return "Depiction_Reader"; } }
        public override string ProcessName { get { return "Depiction_Reader"; } }
        public override string UiConfigXamlUri { get { return "Depiction.DefaultResources;Component/ProductResources/UiConfigReader.xaml"; } }
        public override string SplashScreenPath { get { return "DefaultImages/SplashScreens/FreeReaderSplash.png"; } }
        override public string LicenseFileName
        {
            get { return string.Empty; }
        }

    }
}