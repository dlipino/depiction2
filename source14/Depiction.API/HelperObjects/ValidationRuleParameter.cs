﻿using System;

namespace Depiction.API.HelperObjects
{
    /// <summary>
    /// Type and value information for the parameters to pass to a validation rule.
    /// </summary>
    public class ValidationRuleParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationRuleParameter"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="type">The value's type.</param>
        public ValidationRuleParameter(string value, Type type)
        {
            Value = value;
            Type = type;
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The value's type.</value>
        public Type Type { get; set; }
    }
}