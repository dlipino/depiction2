﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Media;
using Depiction.API.Interfaces.DepictionTypeInterfaces;

namespace Depiction.API.HelperObjects
{
    /// <summary>
    /// Uses interpolation on a piecewise-linear function to map values to colors.
    /// </summary>
    public class ColorInterpolator 
    {
        private static readonly TypeConverter toDoubleConverter = TypeDescriptor.GetConverter(typeof(double));
        private readonly IList<double> valueList = new List<double>();
        private readonly IList<Color> colorList;
        private readonly bool singleColor;

        /// <summary>
        /// Creates and initializes the mapping object.  Based on the two input lists, 
        /// creates a piecewise-linear mapping function.  The two lists must each contain
        /// a minimum of two elements, and they must contain the same number of elements.
        /// </summary>
        /// <param name="domainPoints">List of abscissa values that define the break points between segments of the mapping function.</param>
        /// <param name="colorPoints">List of ordinate values that define the break points between segments of the mapping function.</param>
        public ColorInterpolator(IList<object> domainPoints, IList<Color> colorPoints)
        {
            // Require a minimum of two points, and same number of elements in both lists.
            Debug.Assert(domainPoints != null && domainPoints.Count >= 2);
            Debug.Assert(colorPoints != null && colorPoints.Count == domainPoints.Count);

            // Must be able to convert from domain type to double, and values must increase monotonically.
            for (int i = 0; i < domainPoints.Count; ++i)
            {
                object obj = domainPoints[i];
                double upper;
                bool success = TryConvertToDouble(obj, out upper);
                Debug.Assert(success);

                if (i > 0)
                {
                    var prevObj = domainPoints[i - 1];
                    double lower;
                    Debug.Assert(TryConvertToDouble(prevObj, out lower));
                    Debug.Assert(upper >= lower);
                }

                valueList.Add(upper);
            }

            // Hack!  make it work for a single color
            if (double.IsNegativeInfinity(valueList[0]) && double.IsPositiveInfinity(valueList.Last()))
                singleColor = true;

            colorList = colorPoints;
        }

        /// <summary>
        /// Applies the mapping function to a specified input value.  
        /// </summary>
        /// <param name="value">The input value to map.  May be any type that can be converted to a double.</param>
        /// <returns>The color that corresponds to the input value.</returns>
        public Color GetColorForValue(object value)
        {
            if (singleColor) return colorList[0];

            double doubleVal;
            if (!TryConvertToDouble(value, out doubleVal))
                throw new ArgumentException("Input value of type " + value.GetType().FullName +
                                            " cannot be converted to type double.");

            // Find the segment within which to interpolate
            #region Inscrutable logic -- apologies (allan)

            // Check for below minimum value; if so, return the minimum color
            if (doubleVal <= valueList[0])
                return colorList[0];

            // Find which segment the value lies within
            int lowerIndex;
            for (lowerIndex = 0; lowerIndex < colorList.Count; ++lowerIndex)
            {
                if (lowerIndex < colorList.Count - 1
                    && doubleVal < valueList[lowerIndex + 1]) break;
            }

            // If the above loop made it through the last pass 
            // (lowerIndex == colorList.Count), return the maximum color
            if (lowerIndex >= colorList.Count)
                return colorList.Last();

            #endregion Inscrutable logic -- apologies (allan)

            // Now we know which segment we're interpolating.  lowerIndex is 
            // the index of the lower endpoint of the segment.

            // Get the double values of the segment endpoints
            double lowerVal = valueList[lowerIndex];
            double upperVal = valueList[lowerIndex + 1];

            // Compute the interval from the lower point to the value, as a 
            // fraction of the interval from lowerVal to upperVal
            var fraction = (float)((doubleVal - lowerVal) / (upperVal - lowerVal));

            // Get the color endpoints
            Color lowerColor = colorList[lowerIndex];
            Color upperColor = colorList[lowerIndex + 1];

            // The following magic seemed like it had promise, but the RGB 
            // channel values that resulted weren't what I expected, so I'm 
            // implementing it explicitly.
            //return lowerColor + (upperColor - lowerColor) * fraction;

            // Now do the interpolation
            var returnval = new Color
                                {
                                    A = (byte)((lowerColor.A) + (fraction * (upperColor.A - lowerColor.A))),
                                    R = (byte)((lowerColor.R) + (fraction * (upperColor.R - lowerColor.R))),
                                    G = (byte)((lowerColor.G) + (fraction * (upperColor.G - lowerColor.G))),
                                    B = (byte)((lowerColor.B) + (fraction * (upperColor.B - lowerColor.B)))
                                };

            return returnval;
        }

        /// <summary>
        /// Attempts to convert an object to a double value, according to the 
        /// following rule:
        /// 
        /// If the input is a double, and not NaN, that number is returned as success.
        /// If the input is a string that can be parsed to a double, that value is returned.
        /// If the input is a string that cannot be parsed to a double, the string's hashcode is returned.
        /// If a type converter is available for the value, the type conversion is performed and the result returned.
        /// 
        /// If all of the above conversions fail, the output is set to the hashcode of the input and the method returns true.
        /// </summary>
        /// <param name="input">The object to be converted from.</param>
        /// <param name="output">The returned double value.</param>
        /// <returns>True if the input was converted to a double that is not NaN.</returns>
        public static bool TryConvertToDouble(object input, out double output)
        {
            if (input is double)
            {
                output = (double)input;
                return !double.NaN.Equals(output);
            }

            if (input is string)
            {
                var s = input as string;
                if (double.TryParse(s, out output)) return true;

                output = s.GetHashCode();
                return true;
            }
            if (input is IMeasurement)
            {
                output = ((IMeasurement)input).GetCurrentSystemDefaultScaleValue();
                return true;
            }
            if (toDoubleConverter.CanConvertFrom(input.GetType()))
            {
                output = (double)toDoubleConverter.ConvertFrom(input);
                return true;
            }

            output = input.GetHashCode();
            return true;
        }
    }
}