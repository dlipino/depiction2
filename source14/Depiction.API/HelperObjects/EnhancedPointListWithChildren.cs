﻿using System.Collections.Generic;
using System.Windows;

namespace Depiction.API.HelperObjects
{
    public class EnhancedPointListWithChildren : EnhancedPointList
    {
        public List<EnhancedPointList> Children { get; set; }

        public EnhancedPointListWithChildren()
        {
            Children = new List<EnhancedPointList>();
        }

        public override void ShiftPoints(Point offset)
        {
            foreach(var child in Children)
            {
                child.ShiftPoints(offset);
            }
            base.ShiftPoints(offset);
        }
        
    }

    public class EnhancedPointList
    {
        public List<Point> Outline { get; set; }
        public bool IsClosed { get; set; }
        public bool IsFilled { get; set; }
        public PointListType TypeOfPointList { get; set; }  

        public EnhancedPointList()
        {
            Outline = new List<Point>();
        }

        virtual public void ShiftPoints(Point offset)
        {
            //Why doesn't offset do the trick?
            for(int i = 0;i<Outline.Count;i++)
            {
                var old = Outline[i];
                Outline[i] = new Point(old.X+offset.X,old.Y+offset.Y);
            }
        }
    }

    public enum PointListType
    {
        Line,
        Shell,
        Hole
    }
}