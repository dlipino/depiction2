using System;
using Depiction.API.Interfaces.MessagingInterface;

namespace Depiction.API.MessagingObjects
{
    public class DepictionMessage : IDepictionMessage
    {
        private string messageId = Guid.NewGuid().ToString();

        #region Implementation of IDepictionMessage

        public string MessageId { get { return messageId; } }
        public DepictionMessageType MessageType { get; private set; }

        /// <summary>
        /// Display time in seconds
        /// </summary>
        public double MessageDisplayTime { get; private set; }

        public string Message { get; private set; }

        public DateTime MessageDateTime { get; private set; }

        #endregion
        #region Constructor

        public DepictionMessage(string message)
            : this(message, DepictionMessageType.Information, double.NaN)
        {

        }
        public DepictionMessage(string message, double displayTimeS)
            : this(message, DepictionMessageType.Information, displayTimeS)
        {
        }
        public DepictionMessage(string message, DepictionMessageType messageType, double displayTimeSec)
        {
            Message = message;
            MessageType = messageType;
            MessageDisplayTime = displayTimeSec;
        }

        #endregion

//        #region Overrides of Freezable
//
//        protected override Freezable CreateInstanceCore()
//        {
//            return new DepictionMessage(Message,MessageType);
//        }
//
//        #endregion
    }
}