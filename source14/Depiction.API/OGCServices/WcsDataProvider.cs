﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.WebDataInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.TerrainObjects;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.API.Properties;
using Depiction.API.ExtensionMethods;

namespace Depiction.API.OGCServices
{
    //This one uses most of the classin in OGCServices
    public class WcsDataProvider : IDataRetriever
    {
        #region fields

        private readonly IMapCoordinateBounds boundingBox;
        private readonly string dataDescriptor;
        private readonly string imageFormat;
        private readonly string layerName;
        private readonly string request;
        private readonly string serverUrl;
        private string url = string.Empty;
        private RepeaterService repeater;
        private bool isCanceled = false;
        protected TileCacheService cacheService;
        #endregion
        public bool ReplaceCachedImages { get { return Settings.Default.ReplaceCachedFiles; } }
        #region constructor

        public WcsDataProvider(IMapCoordinateBounds boundingBox, string url, string layer, string format, string req, string dataDescriptor)
        {
            this.boundingBox = CreateDilatedBoundingBox(boundingBox, 0.1);
            isCanceled = false;
            serverUrl = url;
            layerName = layer;
            imageFormat = format;
            request = req;
            this.dataDescriptor = dataDescriptor;
            if (req == "GetMap")
                this.dataDescriptor = serverUrl + " layer:" + layerName;
        }

        #endregion
        public string RequestFileData(string fullTempFileName, out Response responseOut)
        {
            responseOut = RequestData();
            
            if (responseOut != null && responseOut.IsRequestSuccessful)
            {
                return responseOut.ResponseFile;
            }
            return null;
        }

        public void Cancel()
        {
            isCanceled = true;
        }

        public string Url
        {
            get { return url; }
        }
        #region methods

        /// <summary>
        /// Create a bounding box that is a wee bit bigger than the region bounds,
        /// so that we can then crop the erroneous edge pixels that come from the data source.
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="paddingPercent"></param>
        /// <returns></returns>
        private static IMapCoordinateBounds CreateDilatedBoundingBox(IMapCoordinateBounds bbox, double paddingPercent)
        {
            double offsetDistanceX = Math.Abs(bbox.Left - bbox.Right) * paddingPercent;
            double offsetDistanceY = Math.Abs(bbox.Top - bbox.Bottom) * paddingPercent;
            //pad the region bbox with paddingPercent*100% on all sides so WCS request can accomodate
            //cropping and warping from Geo to Mercator, etc.
            //
            return new MapCoordinateBounds
            {
                Top = bbox.Top + offsetDistanceY,
                Bottom = bbox.Bottom - offsetDistanceY,
                Left = bbox.Left - offsetDistanceX,
                Right = bbox.Right + offsetDistanceX,
                MapImageSize = bbox.MapImageSize
            };
        }

        private Response RequestData()
        {
            var mapRequest = new MapRequestBuilder(new Uri(serverUrl));
            if (request == "GetCoverage")
            {
                cacheService = new TileCacheService("WCSElevation");
                mapRequest.Request = "GetCoverage";
                mapRequest.Service = "WCS";
                mapRequest.Coverage = layerName;
                mapRequest.Crs = "EPSG:4269";
            }
            else if (request == "GetMap")
            {
                mapRequest.Request = "GetMap";
                mapRequest.Service = "WMS";
                mapRequest.Layers = layerName;
            }
            mapRequest.Version = "1.0.0";
            mapRequest.Format = imageFormat;

            mapRequest.Styles = string.Empty;
            int totalWidth, totalHeight;
            //derive height and width from the requirement that it be 30m/pixel
            mapRequest.Width = totalWidth = GetImageWidth(boundingBox, 30);
            mapRequest.Height = totalHeight = GetImageHeight(boundingBox, 30);
            mapRequest.BoundingBox = string.Format(CultureInfo.InvariantCulture, "{0},{1},{2},{3}", boundingBox.Left, boundingBox.Bottom, boundingBox.Right, boundingBox.Top);

            mapRequest.ClientInfo["Purpose"] = "draw";

            bool tooLarge = IsRegionLarge(boundingBox);
            if (tooLarge)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    string.Format("Your depiction area exceeds 15000 square miles. We cannot get you elevation data for this large an area. Reduce your depiction area and try again.", 5));
                return null;
            }
            //split boundingBox into 0.25 degree square tiles
            var bboxList = QuarterDegreeTiles(boundingBox);

            var webRequests = new List<DepictionWebRequest>();
            var fullCacheFileNames = new List<string>();

            foreach (IMapCoordinateBounds box in bboxList)
            {
                mapRequest.BoundingBox = string.Format(CultureInfo.InvariantCulture, "{0},{1},{2},{3}", box.Left, box.Bottom, box.Right, box.Top);
                mapRequest.Width = GetImageWidth(box, 30);
                mapRequest.Height = GetImageHeight(box, 30);
                url = mapRequest.Uri.ToString();
                var cacheFileName = dataDescriptor + "_" + box.Top + "_" + box.Left + "_" + box.Bottom + "_" +
                                      box.Right + ".tif";
                var fullCacheFileName = string.Empty;
                if (cacheService == null)
                {
                    fullCacheFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath,
                                                       string.Format("WMS{0}.tif", Guid.NewGuid().ToString("N")));
                }
                else
                {
                     fullCacheFileName = cacheService.GetCacheFullStoragePath(cacheFileName);
                    
                }
                webRequests.Add(new DepictionWebRequest(dataDescriptor, Url, fullCacheFileName));
                fullCacheFileNames.Add(fullCacheFileName);
            }

            var terrainData = new TerrainData();
            int width = totalWidth;
            int height = totalHeight;
            //terrainData is going to accumulate all the strips of terrain coming from the Geoserver
            //The Geoserver data is in Geo coord system
            //So, better that terrainData is in Geo too
            //We will convert it to Mercator after ALL THE STITCHIN IS DONE!
            //This will fix the terrain gap problem that's been plaguing us
            //BHARATH
            //Oct 8 2009

            const string NAD83_WKT = "GEOGCS[\"NAD83\"," +
                                     "DATUM[\"North_American_Datum_1983\"," +
                                     "SPHEROID[\"GRS 1980\",6378137,298.257222101," +
                                     "AUTHORITY[\"EPSG\",\"7019\"]]," +
                                     "AUTHORITY[\"EPSG\",\"6269\"]]," +
                                     "PRIMEM[\"Greenwich\",0," +
                                     "AUTHORITY[\"EPSG\",\"8901\"]]," +
                                     "UNIT[\"degree\",0.01745329251994328," +
                                     "AUTHORITY[\"EPSG\",\"9122\"]]";
            string projectionFilename = string.Empty;
            if (cacheService == null)
            {
                projectionFilename = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath,
                                               string.Format("projection{0}.txt", Guid.NewGuid().ToString("N")));
                File.WriteAllText(projectionFilename, NAD83_WKT);
            }
            else
            {
                projectionFilename = cacheService.GetCacheFullStoragePath("elevationProject.txt");
                if (!File.Exists(projectionFilename) || ReplaceCachedImages)
                {
                    File.WriteAllText(projectionFilename, NAD83_WKT);
                }
            }

            //create terrainData in NAD83 Geo coord system
            //this grid will accumulate the tiles coming from the web requests (or the local cache)

            terrainData.CreateFromExisting(boundingBox.Bottom, boundingBox.Top, boundingBox.Right, boundingBox.Left,
                                           width, height, true, 0, projectionFilename, NAD83_WKT.Length);

            bool atleastOneTileDownSampled = false;
            var msg = "Downloading Terrain Tiles";
            var notification = new DepictionMessage(msg);
            DepictionAccess.NotificationService.DisplayMessage(notification);

            int tileNumber = 0;

            foreach (DepictionWebRequest req in webRequests)
            {
                msg = string.Format("Loading Terrain Tile {0} of {1}", tileNumber + 1, webRequests.Count);
                DepictionAccess.NotificationService.RemoveMessage(notification);
                notification = new DepictionMessage(msg);
                DepictionAccess.NotificationService.DisplayMessage(notification);

                if (isCanceled)
                {
                    DepictionAccess.NotificationService.RemoveMessage(notification);
                    return null;
                }
                var fullCacheFileName = fullCacheFileNames[tileNumber++];
                if (cacheService != null)
                {
                    if (!File.Exists(fullCacheFileName) || ReplaceCachedImages)
                    {
                        fullCacheFileName = string.Empty;
                    }
                }

                if (!string.IsNullOrEmpty(fullCacheFileName))
                {
                    if (isCanceled)
                    {
                        DepictionAccess.NotificationService.RemoveMessage(notification);
                        return null;
                    }
                    //tif image was already cached. Use it.
                    req.Response.IsRequestSuccessful = true;
                    req.Response.ResponseFile = fullCacheFileName;
                }
                else
                {//Download it fresh from the server
                    if (isCanceled)
                    {
                        DepictionAccess.NotificationService.RemoveMessage(notification);
                        return null;
                    }
                    repeater = new RepeaterService(req, 5);
                    repeater.Execute();
                    if (req.Response.IsRequestCanceled) return null;
                }
                if (isCanceled)
                {
                    DepictionAccess.NotificationService.RemoveMessage(notification);
                    return null;
                }
                if (req.Response.IsRequestSuccessful)
                {
                    //cache req.Response.ResponseFile, which is a GeoTiff image
                    //cache only if the request is successful
                    if (cacheService == null)
                    {
                        if (!File.Exists(fullCacheFileName))
                            DepictionAccess.PathService.CacheTerrainFile(req.Response.ResponseFile, fullCacheFileName);
                    }
                    else
                    {
                        if (!File.Exists(fullCacheFileName) || ReplaceCachedImages)
                        {
                            if(!string.IsNullOrEmpty(fullCacheFileName))
                            {
                                File.Copy(req.Response.ResponseFile,fullCacheFileName,true);
                            }
                        }
                    }
                    bool downSampled;
                    //req.Response.ResponseFile contains the TIF file, hopefully
                    terrainData.Add(req.Response.ResponseFile, "", out downSampled, "m", (tileNumber == 1) ? false : true, false);
                    //TRAC #2183 fix:
                    //the first call to Add will set useExistingGridSpacing to false, meaning:
                    //the first time anything real is added to the placeholder grid (terrainData)
                    //the spacing of that new data is used.
                    //
                    //As additional tiles are added, the existing spacing of the grid (set to be the spacing of the first tile)
                    //will be used.
                    //This is because of the issue in TRAC #2183.
                    //
                    //When multiple 0.25x0.25 deg tiles are used to cover a depiction region, the spacing
                    //of the terrain grid (terrainData) was switched between various spacings. The tiles, coming
                    //in from Geoserver, were not all of the same spacing, but subtly different.
                    //This led to large shifts observed in the terrain data

                    if (!atleastOneTileDownSampled && downSampled)
                        atleastOneTileDownSampled = true;
                }
                else return null;
            }
            if (atleastOneTileDownSampled)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    string.Format("Elevation data was too large. Reducing resolution to fit in memory."),3);

            }


            string combinedFile = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, string.Format("WMS{0}.tiff", Guid.NewGuid().ToString("N")));
            if (isCanceled)
            {
                DepictionAccess.NotificationService.RemoveMessage(notification);
                return null;
            }

            terrainData.SaveToGeoTiff(combinedFile);

            //terrainData, saved as GeoTiff is in Geo coordinate system, NAD83 datum.
            //
            //terrain data better be in Mercator before it's brought into Depiction
            //
            //So, reproject it

            msg = string.Format("Processing the terrain data");
            DepictionAccess.NotificationService.RemoveMessage(notification);
            notification = new DepictionMessage(msg);
            DepictionAccess.NotificationService.DisplayMessage(notification);
            var mercatorData = new TerrainData();
            mercatorData.CreateFromExisting(boundingBox.Bottom, boundingBox.Top, boundingBox.Right, boundingBox.Left,
                                            width, height, true, 0, "", 0);
            bool down;
            mercatorData.Add(combinedFile, "", out down, "m", false, false);
            //useExistingGridSpacing is set to false since only one file is being added. You can modify
            //the spacing of the placeholder terrain data (mercatorData) to be the spacing of this file.

            mercatorData.SaveToGeoTiff(combinedFile);
            DepictionAccess.NotificationService.RemoveMessage(notification);
            mapRequest.BoundingBox = string.Format(CultureInfo.InvariantCulture, "{0},{1},{2},{3}", bboxList[0].Left, bboxList[0].Bottom, bboxList[bboxList.Count - 1].Right, bboxList[0].Top);

            if (isCanceled) return null;
            url = mapRequest.Uri.ToString();
            return new Response { ResponseFile = combinedFile, IsRequestSuccessful = true };
        }

        private static bool IsRegionLarge(IMapCoordinateBounds box)
        {

            var bigArea = 15000d; /* square miles */
            var sqMeters = bigArea * 1609d * 1609d;

            var topLeft = box.TopLeft;
            var topRight = box.TopRight;
            var bottomLeft = box.BottomLeft;

            var currentWidth = topLeft.DistanceTo(topRight, MeasurementSystem.Metric, MeasurementScale.Normal);
            var currentHeight = topLeft.DistanceTo(bottomLeft, MeasurementSystem.Metric, MeasurementScale.Normal);
            double originalArea = currentWidth * currentHeight;

            bool largeRegion = false;
            if (originalArea > sqMeters)
            {
                largeRegion = true;
            }
            return largeRegion;
        }

        /// <summary>
        /// Split bounding box into 0.25x0.25 degree tiles
        /// </summary>
        /// <param name="box"></param>
        /// <returns></returns>
        private static List<IMapCoordinateBounds> QuarterDegreeTiles(IMapCoordinateBounds box)
        {
            const double cellWidth = 0.25;
            double startLat = FindNearestRightBoundary(box.BottomLeft.Latitude, cellWidth) - cellWidth;
            double endLat = FindNearestRightBoundary(box.TopLeft.Latitude, cellWidth);
            double startLon = FindNearestRightBoundary(box.TopLeft.Longitude, cellWidth) - cellWidth;
            double endLon = FindNearestRightBoundary(box.TopRight.Longitude, cellWidth);

            var bboxList = new List<IMapCoordinateBounds>();
            const double offset = 0.0000000001; //this offset is to account for a Geoserver WCS b u g

            for (double lon = startLon; lon < endLon; lon += cellWidth)
            {
                for (double lat = startLat; lat < endLat; lat += cellWidth)
                {
                    var bbox = new MapCoordinateBounds(new LatitudeLongitude(lat + cellWidth, lon + offset),
                                               new LatitudeLongitude(lat, lon + cellWidth));
                    bboxList.Add(bbox);
                }
            }

            return bboxList;
        }
        private static double FindNearestRightBoundary(double left, double cellWidth)
        {
            int binLeft = (int)(left / cellWidth) + ((left < 0) ? 0 : 1);
            return binLeft * cellWidth;
        }

        private static int GetImageWidth(IMapCoordinateBounds box, double mpp)
        {
            double mperLon = MetersPerLongitude(box.Bottom);
            double degreeDiff = Math.Abs(box.Left - box.Right);

            double distance = mperLon * degreeDiff;
            double width = distance / mpp;
            return (int)width;
        }

        private static int GetImageHeight(IMapCoordinateBounds box, double mpp)
        {
            double degreeDiff = Math.Abs(box.Top - box.Bottom);

            double height = (METERS_PER_LATITUDE * degreeDiff) / mpp;

            return (int)height;
        }

        private const double METERS_PER_LATITUDE = 111317.1;
        private static double MetersPerLongitude(double latitude)
        {
            return METERS_PER_LATITUDE * Math.Cos(latitude / 180.0 * Math.PI);
        }

        #endregion

    }
}
