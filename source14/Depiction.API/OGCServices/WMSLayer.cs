﻿namespace Depiction.API.OGCServices
{//I have no clue what this does dlp apr 2011
    public class WMSLayer
    {
        private readonly WMSServer server; // The WMS server object representing the server holding this layer.
        private readonly System.Xml.XPath.XPathNavigator nav;		// Keep constant; never give this out. Give out only clones,
        public System.Xml.XPath.XPathNavigator Navigator	// <-- using this accessor.
        {
            get { return nav.Clone(); }
        }

        // The XPathNavigator is to the XML capabilities document of the WMS server.
        internal WMSLayer(System.Xml.XPath.XPathNavigator Nav, WMSServer Server)
        {
            nav = Nav;
            server = Server;
        }

        public WMSServer Server
        {
            get { return server; }
        }

        // The following are utilities for Layers.

        public bool HasParentLayer
        {
            get
            {
                // Returns true if this layer is a child of another layer. Only the top-most layer of a
                // WMS server will return null from this accessor.
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"../../Layer"));
                return iter.Count > 0;
            }
        }

        public WMSLayer GetParentLayer()
        {
            WMSLayer parent = null;
            if (HasParentLayer)
            {
                System.Xml.XPath.XPathNavigator parentNode = Navigator;
                if (parentNode.MoveToParent())
                {
                    parent = new WMSLayer(parentNode, server);
                }
            }
            return parent;
        }

        internal static WMSLayer[] GetLayers(System.Xml.XPath.XPathNavigator parentNode, WMSServer server)
        {
            // The incoming parentNode might not be a Layer node, but we assume it is here in
            // order to re-use the Layers property of the Layer class. If parentNode has
            // no Layer children, then the Layers property returns an empty array.
            WMSLayer layerParent = new WMSLayer(parentNode, server);
            return layerParent.Layers;
        }

        public class UriAndFormatType // This is an often-used data type in the WMS schema.
        {
            public string Uri;
            public string Format;

            internal UriAndFormatType()
            {
                Format = string.Empty;
                Uri = string.Empty;
            }

            public override bool Equals(System.Object obj)
            {
                if (obj == null)
                    return false;

                if (this == obj)
                    return true;

                if (obj.GetType() != GetType())
                    return false;

                UriAndFormatType t = (UriAndFormatType)obj;
                return Uri.Equals(t.Uri) && Format.Equals(t.Format);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public bool IsEmpty
            {
                get { return Uri.Equals(string.Empty); }
            }
        }

        // All the following use XPath search strings to find various bits of information in the
        // server's WMS capabilities description.

        internal static UriAndFormatType[] GetUriAndFormatInstances(System.Xml.XPath.XPathNavigator node, string pattern)
        {
            System.Xml.XPath.XPathNodeIterator iter = node.Select(WMSCapabilities.ExpandPattern(pattern));
            UriAndFormatType[] retVal = new UriAndFormatType[iter.Count];
            while (iter.MoveNext())
            {
                int i = iter.CurrentPosition - 1;
                retVal[i] = new UriAndFormatType();
                retVal[i].Format = WMSCapabilities.GetStringInstance(iter.Current, @"./Format");
                retVal[i].Uri = WMSCapabilities.GetOnlineResourceInstance(iter.Current, @"./OnlineResource");
            }
            return retVal;
        }

        //
        // The following are non-inherited properties.
        //

        public string Name
        {
            get { return WMSCapabilities.GetStringInstance(nav, @"./Name"); }
        }

        public string Title
        {
            get { return WMSCapabilities.GetStringInstance(nav, @"./Title"); }
        }

        public string Abstract
        {
            get { return WMSCapabilities.GetStringInstance(nav, @"./Abstract"); }
        }

        public string[] KeywordList
        {
            get { return WMSCapabilities.GetStringsInstance(nav, @"./KeywordList/Keyword"); }
        }

        public class IdentifierType
        {
            public string Identifier;
            public string Authority;

            internal IdentifierType()
            {
                Identifier = string.Empty;
                Authority = string.Empty;
            }
        }

        public IdentifierType[] Identifiers
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Identifier"));
                IdentifierType[] retVal = new IdentifierType[iter.Count];
                while (iter.MoveNext())
                {
                    int i = iter.CurrentPosition - 1;
                    retVal[i] = new IdentifierType();
                    retVal[i].Identifier = iter.Current.Value;
                    retVal[i].Authority = WMSCapabilities.GetStringInstance(iter.Current, @"./@Authority");
                }
                return retVal;
            }
        }

        public UriAndFormatType[] DataUris
        {
            get { return GetUriAndFormatInstances(nav, @"./DataUrl"); }
        }

        public UriAndFormatType[] FeatureListUris
        {
            get { return GetUriAndFormatInstances(nav, @"./FeatureListUrl"); }
        }

        public class MetadataUriType
        {
            public UriAndFormatType MetadataUri;
            public string Type;

            internal MetadataUriType()
            {
                Type = string.Empty;
                MetadataUri = new UriAndFormatType();
            }
        }

        public MetadataUriType[] MetadataUris
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter =
                    nav.Select(WMSCapabilities.ExpandPattern(@"./MetadataUrl"));
                MetadataUriType[] retVal = new MetadataUriType[iter.Count];
                while (iter.MoveNext())
                {
                    int i = iter.CurrentPosition - 1;
                    retVal[i] = new MetadataUriType();
                    retVal[i].Type = WMSCapabilities.GetStringInstance(iter.Current, @"./@Type");
                    UriAndFormatType[] t = GetUriAndFormatInstances(iter.Current, @".");
                    if (t.Length > 0)
                        retVal[i].MetadataUri = t[0];
                }
                return retVal;
            }
        }

        public WMSLayer[] Layers
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Layer"));
                WMSLayer[] retVal = new WMSLayer[iter.Count];
                while (iter.MoveNext())
                {
                    retVal[iter.CurrentPosition - 1] = new WMSLayer(iter.Current.Clone(), server);
                }
                return retVal;
            }
        }

        //
        // The following are properties that have "Replace" inheritance.
        //

        public class BoundingBoxType
        {
            public string Srs;
            public double MinX;
            public double MinY;
            public double MaxX;
            public double MaxY;
            public double ResX;
            public double ResY;

            public override string ToString()
            {
                string retVal = MinY + " to " + MaxY + " Latitude, " + MinX + " to " + MaxX + " Longitude";
                if (Srs != null && !Srs.Equals(string.Empty))
                {
                    retVal += ", SRS: " + Srs;
                }
                if (ResX != 0 || ResY != 0)
                {
                    retVal += ", Resolution = (" + ResX + ", " + ResY + ")";
                }
                return retVal;
            }

        }

        public BoundingBoxType LatLonBoundingBox
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter =
                    nav.Select(WMSCapabilities.ExpandPattern(@"./LatLonBoundingBox"));
                BoundingBoxType retVal = new BoundingBoxType();
                if (iter.MoveNext())
                {
                    retVal.Srs = "EPSG:4326";
                    retVal.MinX = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MinX|./@minX"));
                    retVal.MinY = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MinY|./@minY"));
                    retVal.MaxX = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MaxX|./@maxX"));
                    retVal.MaxY = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MaxY|./@maxY"));
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.LatLonBoundingBox;
                    }
                }
                return retVal;
            }
        }

        public BoundingBoxType[] BoundingBoxes
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./BoundingBox"));
                BoundingBoxType[] retVal = new BoundingBoxType[iter.Count];
                if (iter.MoveNext())
                {
                    do
                    {
                        int i = iter.CurrentPosition - 1;
                        retVal[i] = new BoundingBoxType();
                        retVal[i].Srs = WMSCapabilities.GetStringInstance(iter.Current, @"./@Srs");
                        retVal[i].MinX = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MinX|./@minX"));
                        retVal[i].MinY = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MinY|./@minY"));
                        retVal[i].MaxX = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MaxX|./@maxX"));
                        retVal[i].MaxY = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@MaxY|./@maxY"));
                        string resX = WMSCapabilities.GetStringInstance(iter.Current, @"./@ResX|./@resX");
                        if (resX.Length > 0)
                        {
                            retVal[i].ResX = double.Parse(resX);
                        }
                        string resY = WMSCapabilities.GetStringInstance(iter.Current, @"./@ResY|./@resY");
                        if (resY.Length > 0)
                        {
                            retVal[i].ResY = double.Parse(resY);
                        }
                    } while (iter.MoveNext());
                }
                else
                {
                    // See whether some are defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.BoundingBoxes;
                    }
                }
                return retVal;
            }
        }

        public class ExtentType
        {
            public string Name;
            public string Default;
            public string Extent;

            public ExtentType()
            {
                Name = string.Empty;
                Default = string.Empty;
                Extent = string.Empty;
            }

            public bool IsEmpty
            {
                get
                {
                    return Name.Equals(string.Empty) && Default.Equals(string.Empty) && Extent.Equals(string.Empty);
                }
            }

            public override string ToString()
            {
                string retVal = "";
                if (!Name.Equals(string.Empty))
                {
                    retVal += Name;
                }
                if (!Extent.Equals(string.Empty))
                {
                    retVal += ", " + Extent;
                }
                if (!Default.Equals(string.Empty))
                {
                    retVal += ", Default = " + Default;
                }
                return retVal;
            }
        }

        public ExtentType[] Extents
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Extent"));
                ExtentType[] retVal = new ExtentType[iter.Count];
                if (iter.MoveNext())
                {
                    do
                    {
                        int i = iter.CurrentPosition - 1;
                        retVal[i] = new ExtentType();
                        retVal[i].Extent = iter.Current.Value;
                        retVal[i].Name = WMSCapabilities.GetStringInstance(iter.Current, @"./@Name");
                        retVal[i].Default = WMSCapabilities.GetStringInstance(iter.Current, @"./@Default");
                    } while (iter.MoveNext());
                }
                else
                {
                    // See whether some are defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.Extents;
                    }
                }
                return retVal;
            }
        }

        public class ScaleHintType
        {
            // The WMS 1.1.1 spec doesn't demand that these values be numbers. Therefore 
            // treat them as strings. The application can easily parse them as numbers if
            // appropriate. (Using double.Parse())
            public string Min;
            public string Max;

            public ScaleHintType()
            {
                Min = string.Empty;
                Max = string.Empty;
            }

            public bool IsEmpty
            {
                get
                {
                    return Min.Equals(string.Empty) && Max.Equals(string.Empty);
                }
            }
        }

        public ScaleHintType ScaleHint
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./ScaleHint"));
                ScaleHintType retVal = new ScaleHintType();
                if (iter.MoveNext())
                {
                    retVal.Min = WMSCapabilities.GetStringInstance(iter.Current, @"./@Min");
                    retVal.Max = WMSCapabilities.GetStringInstance(iter.Current, @"./@Max");
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.ScaleHint;
                    }
                }
                return retVal;
            }
        }

        public class LogoOrLegendUriType
        {
            public UriAndFormatType Uri;
            public double Width;
            public double Height;

            internal LogoOrLegendUriType()
            {
                Uri = new UriAndFormatType();
            }

            public bool IsEmpty
            {
                get { return Uri.IsEmpty; }
            }
        }

        private static LogoOrLegendUriType GetLogoOrLegendUriInstance(System.Xml.XPath.XPathNavigator node, string pattern)
        {
            System.Xml.XPath.XPathNodeIterator iter = node.Select(WMSCapabilities.ExpandPattern(pattern));
            LogoOrLegendUriType retVal = new LogoOrLegendUriType();
            if (iter.MoveNext())
            {
                UriAndFormatType[] t = GetUriAndFormatInstances(iter.Current, @".");
                if (t.Length > 0)
                    retVal.Uri = t[0];
                retVal.Width = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@Width"));
                retVal.Height = double.Parse(WMSCapabilities.GetStringInstance(iter.Current, @"./@Height"));
            }
            return retVal;
        }

        public class AttributionType
        {
            public string Title;
            public string Uri;
            public LogoOrLegendUriType LogoUri;

            internal AttributionType()
            {
                Title = string.Empty;
                Uri = string.Empty;
                LogoUri = new LogoOrLegendUriType();
            }

            public bool IsEmpty
            {
                get
                {
                    return Title.Equals(string.Empty)
                           && Uri.Equals(string.Empty)
                           && LogoUri.IsEmpty;
                }
            }
        }

        public AttributionType Attribution
        {
            get
            {
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Attribution"));
                AttributionType retVal = new AttributionType();
                if (iter.MoveNext())
                {
                    retVal.Title = WMSCapabilities.GetStringInstance(iter.Current, @"./Title");
                    retVal.Uri = WMSCapabilities.GetOnlineResourceInstance(iter.Current, @"./OnlineResource");
                    retVal.LogoUri = GetLogoOrLegendUriInstance(iter.Current, @"./LogoUrl");
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.Attribution;
                    }
                }
                return retVal;
            }
        }

        public bool Queryable
        {
            get
            {
                bool retVal = false;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@Queryable").Trim();
                if (!q.Equals(string.Empty))
                {
                    retVal = q.Equals("1");
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.Queryable;
                    }
                }
                return retVal;
            }
        }

        public bool Opaque
        {
            get
            {
                bool retVal = false;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@Opaque").Trim();
                if (!q.Equals(string.Empty))
                {
                    retVal = q.Equals("1");
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.Opaque;
                    }
                }
                return retVal;
            }
        }

        public bool NoSubsets
        {
            get
            {
                bool retVal = false;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@NoSubsets|./@noSubsets").Trim();
                if (!q.Equals(string.Empty))
                {
                    retVal = q.Equals("1");
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.NoSubsets;
                    }
                }
                return retVal;
            }
        }

        public int Cascaded
        {
            get
            {
                int retVal = 0;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@Cascaded");
                if (!q.Equals(string.Empty))
                {
                    retVal = int.Parse(q);
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.Cascaded;
                    }
                }
                return retVal;
            }
        }

        public int FixedWidth
        {
            get
            {
                int retVal = 0;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@FixedWidth|./@fixedWidth");
                if (!q.Equals(string.Empty))
                {
                    retVal = int.Parse(q);
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.FixedWidth;
                    }
                }
                return retVal;
            }
        }

        public int FixedHeight
        {
            get
            {
                int retVal = 0;
                string q = WMSCapabilities.GetStringInstance(nav, @"./@FixedHeight|./@fixedHeight");
                if (!q.Equals(string.Empty))
                {
                    retVal = int.Parse(q);
                }
                else
                {
                    // See whether one is defined higher up the Layer hierarchy.
                    WMSLayer parent = GetParentLayer();
                    if (parent != null)
                    {
                        retVal = parent.FixedHeight;
                    }
                }
                return retVal;
            }
        }

        //
        // The following properties have "Add" inheritance.
        //

        public class DimensionType
        {
            public string Name;
            public string Units;
            public string UnitSymbol;

            public DimensionType()
            {
                Name = string.Empty;
                Units = string.Empty;
                UnitSymbol = string.Empty;
            }

            public override bool Equals(System.Object obj)
            {
                if (obj == null)
                    return false;

                if (this == obj)
                    return true;

                if (obj.GetType() != GetType())
                    return false;

                DimensionType d = (DimensionType)obj;
                return Name.Equals(d.Name)
                       && Units.Equals(d.Units)
                       && UnitSymbol.Equals(d.UnitSymbol);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public override string ToString()
            {
                string retVal = Name + ", " + Units;
                if (!UnitSymbol.Equals(string.Empty))
                {
                    retVal += " (" + UnitSymbol + ")";
                }
                return retVal;
            }

        }

        public DimensionType[] Dimensions
        {
            get
            {
                // Collect any values from ancestors.
                System.Collections.ArrayList retVal = new System.Collections.ArrayList();
                if (HasParentLayer)
                {
                    DimensionType[] parentValues = GetParentLayer().Dimensions;
                    if (parentValues.Length > 0)
                    {
                        retVal.AddRange(parentValues);
                    }
                }

                // Add those uniquely defined at this Layer.
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Dimension"));
                DimensionType dim = new DimensionType();
                while (iter.MoveNext())
                {
                    dim = new DimensionType();
                    dim.Name = WMSCapabilities.GetStringInstance(iter.Current, @"./@Name");
                    dim.Units = WMSCapabilities.GetStringInstance(iter.Current, @"./@Units");
                    dim.UnitSymbol = WMSCapabilities.GetStringInstance(iter.Current, @"./@UnitSymbol|./@unitSymbol");
                    if (!retVal.Contains(dim))
                    {
                        retVal.Add(dim);
                    }
                }
                return retVal.ToArray(dim.GetType()) as DimensionType[];
            }
        }

        public string[] Srs
        {
            get
            {
                // Collect any values from ancestors.
                System.Collections.ArrayList retVal = new System.Collections.ArrayList();
                if (HasParentLayer)
                {
                    string[] parentValues = GetParentLayer().Srs;
                    if (parentValues.Length > 0)
                    {
                        retVal.AddRange(parentValues);
                    }
                }

                // Add those uniquely defined at this Layer.
                string[] local = WMSCapabilities.GetStringsInstance(nav, @"./Srs");
                foreach (string s in local)
                {
                    if (!retVal.Contains(s))
                    {
                        retVal.Add(s);
                    }
                }
                return retVal.ToArray(string.Empty.GetType()) as string[];
            }
        }

        public class AuthorityUriType
        {
            public string Name;
            public string Uri;

            internal AuthorityUriType()
            {
                Name = string.Empty;
                Uri = string.Empty;
            }

            public override bool Equals(System.Object obj)
            {
                if (obj == null)
                    return false;

                if (this == obj)
                    return true;

                if (obj.GetType() != GetType())
                    return false;

                return Name.Equals(((AuthorityUriType)obj).Name)
                       && Uri.Equals(((AuthorityUriType)obj).Uri);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public AuthorityUriType[] AuthorityUris
        {
            get
            {
                // Collect any values from ancestors.
                System.Collections.ArrayList retVal = new System.Collections.ArrayList();
                if (HasParentLayer)
                {
                    AuthorityUriType[] parentValues = GetParentLayer().AuthorityUris;
                    if (parentValues.Length > 0)
                    {
                        retVal.AddRange(parentValues);
                    }
                }

                // Add those uniquely defined at this Layer.
                System.Xml.XPath.XPathNodeIterator iter =
                    nav.Select(WMSCapabilities.ExpandPattern(@"./AuthorityUrl"));
                AuthorityUriType auth = new AuthorityUriType();
                while (iter.MoveNext())
                {
                    auth = new AuthorityUriType();
                    auth.Name = WMSCapabilities.GetStringInstance(iter.Current, @"./@Name");
                    auth.Uri = WMSCapabilities.GetOnlineResourceInstance(iter.Current, @"./OnlineResource");
                    if (!retVal.Contains(auth))
                    {
                        retVal.Add(auth);
                    }
                }
                return retVal.ToArray(auth.GetType()) as AuthorityUriType[];
            }
        }

        public class StyleType
        {
            public string Name;
            public string Title;
            public string Abstract;
            public UriAndFormatType StyleUri;
            public UriAndFormatType StyleSheetUri;
            public LogoOrLegendUriType LegendUri;

            internal StyleType()
            {
                Name = string.Empty;
                Title = string.Empty;
                Abstract = string.Empty;
                StyleUri = new UriAndFormatType();
                StyleSheetUri = new UriAndFormatType();
                LegendUri = new LogoOrLegendUriType();
            }

            public override bool Equals(System.Object obj)
            {
                if (obj == null)
                    return false;

                if (this == obj)
                    return true;

                if (obj.GetType() != GetType())
                    return false;

                StyleType s = (StyleType)obj;
                return Name.Equals(s.Name)
                       && Title.Equals(s.Title)
                       && Abstract.Equals(s.Abstract)
                       && StyleUri.Equals(s.StyleUri)
                       && StyleSheetUri.Equals(s.StyleSheetUri)
                       && LegendUri.Equals(s.LegendUri);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }

        public StyleType[] Styles
        {
            get
            {
                // Collect any values from ancestors.
                System.Collections.ArrayList retVal = new System.Collections.ArrayList();
                if (HasParentLayer)
                {
                    StyleType[] parentValues = GetParentLayer().Styles;
                    if (parentValues.Length > 0)
                    {
                        retVal.AddRange(parentValues);
                    }
                }

                // Add those uniquely defined at this Layer.
                System.Xml.XPath.XPathNodeIterator iter = nav.Select(WMSCapabilities.ExpandPattern(@"./Style"));
                StyleType style = new StyleType();
                while (iter.MoveNext())
                {
                    style = new StyleType();
                    style.Name = WMSCapabilities.GetStringInstance(iter.Current, @"./Name");
                    style.Title = WMSCapabilities.GetStringInstance(iter.Current, @"./Title");
                    style.Abstract = WMSCapabilities.GetStringInstance(iter.Current, @"./Abstract");
                    UriAndFormatType[] su = GetUriAndFormatInstances(iter.Current, @"./StyleUrl");
                    if (su.Length > 0)
                        style.StyleUri = su[0];
                    UriAndFormatType[] ssu = GetUriAndFormatInstances(iter.Current, @"./StyleSheetUrl");
                    if (ssu.Length > 0)
                        style.StyleSheetUri = ssu[0];
                    style.LegendUri = GetLogoOrLegendUriInstance(iter.Current, @"./LegendUrl");
                    if (!retVal.Contains(style))
                    {
                        retVal.Add(style);
                    }
                }
                return retVal.ToArray(style.GetType()) as StyleType[];
            }
        }
    }
}
