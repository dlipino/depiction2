﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.WebDataInterfaces;

namespace Depiction.API.OGCServices
{
    public class WxsDataService : IDataRetriever
    {
        private readonly Dictionary<string, object> filterParameters;
        private readonly IMapCoordinateBounds regionExtent;
        private IRepeatableRequest request;

        public WxsDataService(Dictionary<string, object> filterParameters, IMapCoordinateBounds regionExtent)
        {
            this.filterParameters = filterParameters;
            this.regionExtent = regionExtent;
        }

        public string RequestFileData(string fullTempFileName, out Response responseOut)
        {
            string layerName = "", serverType = "";
            string connectionString = "";
            foreach (var param in filterParameters)
            {
                switch (param.Key.ToLower())
                {
                    case "url":
                        connectionString = param.Value.ToString();
                        break;
                    case "servicetype":
                        serverType = param.Value.ToString();
                        break;
                    case "layername":
                        layerName = param.Value.ToString();
                        break;
                    case "username":
                        break;
                    case "userpassword":
                        break;
                    default:
                        break;
                }
            }

            if(string.IsNullOrEmpty(fullTempFileName))
            {
                fullTempFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" +
                                                                                        serverType + ".gml");
            }

            request = new WxsRequest(layerName, serverType, fullTempFileName, connectionString, regionExtent);
            
            var repeater = new RepeaterService(request, 5);
            repeater.Execute();

            responseOut = request.Response;

            if (!request.Response.IsRequestSuccessful)
            {
                return null;
            }

            return fullTempFileName;
        }

        public void Cancel()
        {
            if (request != null)
                request.Cancel();
        }
    }

}
