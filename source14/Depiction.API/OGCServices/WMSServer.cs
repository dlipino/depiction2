﻿namespace Depiction.API.OGCServices
{
    /// <summary>
    /// Represents a WMS server and holds its capabilities description.
    /// </summary>
    public class WMSServer
    {
        private WMSCapabilities capabilities;
        private bool Errors = false;


        public bool HasErrors
        {
            get
            {
                return Errors;
            }
        }

        public WMSServer(string filePath)
        {
            capabilities = new WMSCapabilities(filePath, this);
            Errors = capabilities.HasErrors;
        }


        public WMSCapabilities Capabilities
        {
            get { return capabilities; }
        }

        public System.Uri Uri
        {
            get { return new System.Uri(capabilities.GetCapabilitiesRequestUri); }
        }
        public MapRequestBuilder CreateMapRequest(string uri)
        {
            //map request
            return new MapRequestBuilder(new System.Uri(uri));
        }
        public MapRequestBuilder CreateMapRequest()
        {
            //
            //TODO: to be refactored into a WCSServer class
            //
            //
            //TODO: This whole set of classes -- WMSServer, WebMapService, WMSRetriever, to the extent
            //TODO: possible needs to start using CarbonTools for doing things such as GetCapabilities
            //
            //TODO: We just found a WMS http://wms.jpl.nasa.gov/wms.cgi that this limited class cannot handle
            //TODO: Carbontools handles the getcapabilities for this URL just fine
            //TODO: Bharath July 31 2008
            if (capabilities.GetMapRequestUri == "")
            {
                //maybe it is coverage request
                return new MapRequestBuilder(new System.Uri("http://portal.depiction.com/geoserver/wcs"));

            }
            else
            {
                //map request
                return new MapRequestBuilder(new System.Uri(capabilities.GetMapRequestUri));
            }
        }
    }
}
