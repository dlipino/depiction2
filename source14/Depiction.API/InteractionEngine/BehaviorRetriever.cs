﻿using System.Windows;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.MEFRepository;
using Depiction.API.Interfaces;

namespace Depiction.API.InteractionEngine
{
    public static class BehaviorRetriever
    {

        public static BaseBehavior RetrieveBehavior(string behaviorName)
        {
//            var app = Application.Current as IDepictionApplication;
//            if(app == null)
//            {//This happens in unit tests.
//                return null;
//            }

            var behaviors = AddinRepository.Instance.GetBehaviors();

            if(behaviors == null) return null;
            foreach(var behavior in behaviors)
            {
                if (behavior.Key.BehaviorName.Equals(behaviorName))
                    return behavior.Value;
            }
            return null;
        }

    }

}
