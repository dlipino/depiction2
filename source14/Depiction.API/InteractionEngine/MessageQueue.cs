﻿using System;
using System.Collections.Generic;
using System.Threading;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.InteractionEngine
{
    public class MessageQueue
    {
        private readonly Queue<DepictionInteractionMessage> queue = new Queue<DepictionInteractionMessage>();
        private readonly EventWaitHandle waitHandle;
        private int activityTimeout = 1000;

        public MessageQueue()
        {
            waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
        }

        public int MessageCount
        {
            get { return queue.Count; }
        }

        public bool Empty
        {
            get { return MessageCount == 0; }
        }

        public int ActivityTimeout
        {
            get { return activityTimeout; }
            set { activityTimeout = value; }
        }

        public void AddMessages(List<IDepictionElement> triggereElements)
        {
            lock (queue)
            {
                try
                {
                    foreach(var element in triggereElements)
                    {
                        if(element != null)
                        {
                            queue.Enqueue(new DepictionInteractionMessage(element));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception in MessageQueue.AddMessage: {0}", e.Message);
                }
                finally
                {
                    waitHandle.Set();
                }
            }
        }
        public void AddMessage(IDepictionElement triggeredElement)
        {
            lock (queue)
            {
                try
                {
                    if(triggeredElement != null)
                    {
                        queue.Enqueue(new DepictionInteractionMessage(triggeredElement));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception in MessageQueue.AddMessage: {0}", e.Message);
                }
                finally
                {
                    waitHandle.Set();
                }
            }
        }

        public DepictionInteractionMessage Get()
        {
            lock (queue)
            {
                return queue.Dequeue();
            }
        }

        public IList<DepictionInteractionMessage> PopAll()
        {
            IList<DepictionInteractionMessage> result;
            lock (queue)
            {
                result = queue.ToArray();
                queue.Clear();
            }

            return result;
        }

        public void CancelWait()
        {
            waitHandle.Set();
        }
        public void StopWait()
        {
            waitHandle.Set();
            waitHandle.Close();
        }

        /// <summary>
        /// Simple scheme to collect messages until the user takes a break for a specified
        /// time that can be set by the user. Without this change the first message kicks
        /// off the interaction rules. All subsequent changes until the interaction rules finish
        /// are collected and run again. With this change no rules are run until it looks like
        /// the user is done.
        /// </summary>
        /// <returns></returns>
        public IList<DepictionInteractionMessage> WaitAndPopAll()
        {
            // Wait until the first message arrives.
            WaitHandle.WaitAny(new[] { waitHandle });
            waitHandle.Reset();
            //The while !empty condition tests if the wait handle was set in CancelWait.
            while (!Empty)
            {
                // Now wait again, and see if the timeout period passed without another message arriving.
                if (WaitHandle.WaitAny(new[] { waitHandle }, activityTimeout, true) == WaitHandle.WaitTimeout)
                {
                    // Timeout expired without any new messages arriving.
                    return PopAll();
                }
                // Wait again.
                waitHandle.Reset();
            }
            return null;
        }

        public void Clear()
        {
            lock (queue)
            {
                queue.Clear();
            }
        }
    }
}
