﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;

namespace Depiction.API.InteractionEngine
{
    public struct ValidationResult
    {
        public bool IsValid;
        public ValidationEventArgs EventArgs;
        public string schemaVersion;
    } ;

    public class SchemaValidator
    {
        public const string SchemaVersionAttributeTag = "schemaVersion";
        public const string defaultRootElementPath = "./interactionRules";
        public string RootElementPath { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public SchemaValidator()
        {
            RootElementPath = defaultRootElementPath;
        }

        private readonly Dictionary<string, XmlSchema> schemaDictionary = new Dictionary<string, XmlSchema>();

        /// <summary>
        /// Reference to the assembly containing the schema resources.
        /// </summary>
        public Assembly SchemaResourceAssembly
        {
            get
            {
                return schemaResourceAssembly;
            }
            set
            {
                schemaResourceAssembly = value;
                LoadAllSchemasFromResourcePath();
            }
        }
        private Assembly schemaResourceAssembly;

        /// <summary>
        /// The path within the assembly where the schema resources are located.
        /// </summary>
        public string PathToSchemas
        {
            get
            {
                return pathToSchemas;
            }
            set
            {
                pathToSchemas = value;
                LoadAllSchemasFromResourcePath();
            }
        }
        private string pathToSchemas;

        protected void LoadAllSchemasFromResourcePath()
        {
            if (SchemaResourceAssembly == null || PathToSchemas == null)
            {
                schemaDictionary.Clear();
                return;
            }

            LoadAllSchemasFromResourcePath(SchemaResourceAssembly, PathToSchemas);
        }

        protected void LoadAllSchemasFromResourcePath(Assembly assembly, string path)
        {
            string[] resourceNames = assembly.GetManifestResourceNames();
            string fullPathBase = assembly.GetName().Name + "." + path + ".";

            foreach (string name in resourceNames)
            {
                if (!Regex.Match(name, "^" + fullPathBase).Success) continue;

                Stream schemaStream = assembly.GetManifestResourceStream(name);
                if (null != schemaStream)
                {
                    ValidationEventArgs eventArgs = null;
                    XmlSchema schema = XmlSchema.Read(schemaStream, delegate(object sender, ValidationEventArgs e)
                    {
                        eventArgs = e;
                    }

                        );

                    if (null != schema && null == eventArgs && schema.Version != null)
                    {
                        schemaDictionary.Add(schema.Version, schema);
                    }
                }
            }
        }

        public void Clear()
        {
            schemaResourceAssembly = null;
            pathToSchemas = null;
            schemaDictionary.Clear();
        }

        protected string GetInstanceSchemaVersion(XmlDocument doc)
        {
            XmlNode rootNode = doc.SelectSingleNode(RootElementPath);
            if (rootNode != null)
            {
                XmlAttribute versionAttribute = rootNode.Attributes[SchemaVersionAttributeTag];
                return versionAttribute.Value;
            }
            return null;
        }

        public bool HaveSchemaForVersion(string version)
        {
            return (schemaDictionary != null) ? schemaDictionary.ContainsKey(version) : false;
        }

        public ValidationResult Validate(XmlDocument document)
        {
            string schemaVersion = GetInstanceSchemaVersion(document);
            if (schemaVersion == null || !HaveSchemaForVersion(schemaVersion))
            {
                return new ValidationResult { IsValid = false, EventArgs = null, schemaVersion = null };
            }

            document.Schemas.Add(schemaDictionary[schemaVersion]);

            ValidationEventArgs eventArgs = null;
            document.Validate(delegate(object sender, ValidationEventArgs e)
            {
                eventArgs = e;
            }
                );

            return new ValidationResult
            {
                IsValid = (eventArgs == null),
                EventArgs = eventArgs,
                schemaVersion = schemaVersion
            };
        }

        public static ValidationResult ValidateXmlStream(Stream xmlStream, Stream schemaStream)
        {
            //Duplicates some functionality from Validate (see above), but less tied to specific directories, etc.  Can be used standalone.
            // TODO: reconcile 
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlStream);
            ValidationEventArgs eventArgs = null;
            var schema = XmlSchema.Read(schemaStream, delegate(object sender, ValidationEventArgs e) { eventArgs = e; });
            xmlDoc.Schemas.Add(schema);

            eventArgs = null;
            xmlDoc.Validate(delegate(object sender, ValidationEventArgs e) { eventArgs = e; });
            if (eventArgs != null)
            {
                return new ValidationResult { EventArgs = eventArgs, IsValid = false };
            }
            return new ValidationResult { IsValid = true };
        }
    }
}
