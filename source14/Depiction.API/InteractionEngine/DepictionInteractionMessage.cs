﻿using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.InteractionEngine
{
    public class DepictionInteractionMessage
    {
        private readonly IDepictionElement publisher;
        public DepictionInteractionMessage(IDepictionElement publisher)
        {
            this.publisher = publisher;
        }

        public IDepictionElement Publisher
        {
            get { return publisher; }
        }
    }
}
