﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Depiction.API.Interfaces;

namespace Depiction.API.InteractionEngine
{
    [XmlRoot(ElementName = "Behavior")]
    public class BehaviorSerializer : BehaviorConditionSerializer
    {//the condition and behaviors have divereged
        public BehaviorSerializer(string name, IEnumerable<IElementFileParameter> parameters)
            : base(name, parameters)
        { }
        public BehaviorSerializer()
        { }
    }
}
