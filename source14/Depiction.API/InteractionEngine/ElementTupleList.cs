﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.Serialization;

namespace Depiction.API.InteractionEngine
{
    public class ElementTupleList : IXmlSerializable
    {

        private readonly Dictionary<IDepictionElement, List<IDepictionElement>> triggerToAffectedDictionary = new Dictionary<IDepictionElement, List<IDepictionElement>>();
        private readonly Dictionary<IDepictionElement, List<IDepictionElement>> affectedToTriggersDictionary = new Dictionary<IDepictionElement, List<IDepictionElement>>();

        public void Add(IDepictionElement element1, IDepictionElement element2)
        {
            Debug.Assert(!ReferenceEquals(element1, element2));
            addToDictionary(element1, element2, triggerToAffectedDictionary);
            addToDictionary(element2, element1, affectedToTriggersDictionary);
        }

        private static void addToDictionary(IDepictionElement element1, IDepictionElement element2, Dictionary<IDepictionElement, List<IDepictionElement>> dictionary)
        {
            if (!dictionary.ContainsKey(element1))
                dictionary.Add(element1, new List<IDepictionElement> { element2 });
            else
            {
                List<IDepictionElement> affectedElements;
                dictionary.TryGetValue(element1, out affectedElements);
                affectedElements.Add(element2);
            }
        }

        public int Count
        {
            get { return triggerToAffectedDictionary.Count; }
        }

        public void Remove(IDepictionElement trigger, IDepictionElement affected)
        {
            removeFromDictionary(trigger, affected, triggerToAffectedDictionary);
            removeFromDictionary(affected, trigger, affectedToTriggersDictionary);
        }

        private static void removeFromDictionary(IDepictionElement element1, IDepictionElement element2, Dictionary<IDepictionElement, List<IDepictionElement>> dictionary)
        {
            List<IDepictionElement> elements;
            dictionary.TryGetValue(element1, out elements);
            if (elements != null)
            {
                elements.Remove(element2);
                if (elements.Count == 0)
                    dictionary.Remove(element1);
            }
        }

        public bool Exists(IDepictionElement element1, IDepictionElement element2)
        {
            List<IDepictionElement> elements;
            triggerToAffectedDictionary.TryGetValue(element1, out elements);
            return (elements != null) && elements.Contains(element2);
        }

        public void Clear()
        {
            triggerToAffectedDictionary.Clear();
            affectedToTriggersDictionary.Clear();
        }

        public ICollection<IDepictionElement> GetAffectedElementsRecursive(IDepictionElement trigger)
        {
            var elements = new HashSet<IDepictionElement>();
            AddAffectedElementToListRecursive(elements, trigger);
            elements.Remove(trigger);
            return elements;
        }

        private void AddAffectedElementToListRecursive(HashSet<IDepictionElement> elementList, IDepictionElement trigger)
        {
            List<IDepictionElement> elements;
            elementList.Add(trigger);
            if (triggerToAffectedDictionary.TryGetValue(trigger, out elements))
            {
                foreach (var element in elements)
                {
                    if (!elementList.Contains(element))
                        AddAffectedElementToListRecursive(elementList, element);
                }
            }

        }

        public IList<IDepictionElement> GetAffectedElements(IDepictionElement trigger)
        {
            List<IDepictionElement> elements;
            triggerToAffectedDictionary.TryGetValue(trigger, out elements);
            return elements;
        }

        public IList<IDepictionElement> GetTriggerElements(IDepictionElement affected)
        {
            List<IDepictionElement> elements;
            affectedToTriggersDictionary.TryGetValue(affected, out elements);
            return elements;
        }

        private List<KeyValuePair<string, string>> restoredElementKeys;

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            restoredElementKeys = SerializationService.Deserialize<List<KeyValuePair<string, string>>>("restoredElementKeys", reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var elementKeys = new List<KeyValuePair<string, string>>();
            foreach (var kvp in triggerToAffectedDictionary)
            {
                foreach (var value in kvp.Value)
                    elementKeys.Add(new KeyValuePair<string, string>(kvp.Key.ElementKey, value.ElementKey));
            }
            SerializationService.Serialize("restoredElementKeys", elementKeys, writer);
        }

        public void RestoreElements(IEnumerable<IDepictionElement> elements)
        {
            foreach (var elementKey in restoredElementKeys)
            {
                IDepictionElement element1 = null, element2 = null;
                foreach (var element in elements)
                {
                    if (element.ElementKey.Equals(elementKey.Key))
                        element1 = element;
                    if (element.ElementKey.Equals(elementKey.Value))
                        element2 = element;
                }
                if (element1 != null && element2 != null)
                    Add(element1, element2);
            }
            restoredElementKeys = null;
        }

    }

}
