﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces;
using Depiction.Serialization;

namespace Depiction.API.InteractionEngine
{
    public class InteractionRuleRepository : IInteractionRuleRepository, IXmlSerializable
    {
        private List<IInteractionRule> interactionRules = new List<IInteractionRule>();

        #region IInteractionRuleRepository Members

        public void LoadRules(IEnumerable<IInteractionRule> interactionRulesToAdd)
        {
            if (interactionRulesToAdd != null)
            {
                foreach (var interactionRule in interactionRulesToAdd)
                {
                    AddInteractionRule(interactionRule);
                }
            }
        }

        public bool DoesRuleWithElementTypeAndTriggerNameExist(string elementType,string triggerProperty)
        {
            //For current debugging purposes don't use linq, once stability is reached start using linq
            foreach(var iRule in interactionRules)
            {
                var rule = iRule as InteractionRule;
                if (rule == null) continue;
                var elemToTriggerDictionary = rule.elementAndPropertyTriggerDictionary;
                if(elemToTriggerDictionary.ContainsKey(elementType))
                {
                    if (string.IsNullOrEmpty(triggerProperty)) return true;
                    var propTriggers = elemToTriggerDictionary[elementType];
                    if (propTriggers.Contains(triggerProperty)) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Adds the specified interaction rule to the repository if it does not already exist.
        /// </summary>
        /// <param name="interactionRule">The interaction rule to add.</param>
        /// <returns>Whether the rule was added.</returns>
        public bool AddInteractionRule(IInteractionRule interactionRule)
        {
            return AddInteractionRule(interactionRule, true);
        }

        public bool AddInteractionRule(IInteractionRule interactionRule, bool runInteractions)
        {
            if (interactionRules.Contains(interactionRule))
                return false;
            interactionRules.Add(interactionRule);

            if (DepictionAccess.CurrentDepiction != null && DepictionAccess.CurrentDepiction.CompleteElementRepository != null && runInteractions)
            {
                foreach (var element in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements.Where(e => interactionRule.Publisher.Equals(e.ElementType)))
                {
                    //element.UpdateInteractions();
                }
            }

            return true;
        }

        /// <summary>
        /// Remove the subscription to the specified rule
        /// </summary>
        public void RemoveInteractionRule(IInteractionRule rule)
        {
            if (!interactionRules.Contains(rule)) return;
            interactionRules.Remove(rule);

            if (DepictionAccess.CurrentDepiction != null &&
                DepictionAccess.CurrentDepiction.CompleteElementRepository != null)
            {
                foreach (var element in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements)
                {
                   // element.UpdateInteractions();
                }
            }
        }

        /// <summary>
        /// Gets all interaction rules for which the specified element type is either the 
        /// publisher or a subscriber.
        /// </summary>
        /// <param name="elementType">Type of element to search for.</param>
        /// <returns></returns>
        public IInteractionRule[] GetInteractionRules(string elementType)
        {
            IEnumerable<IInteractionRule> selectedRules =
                from r in interactionRules
                where r.Subscribers.Any(s => s == elementType) || r.Publisher == elementType
                select r;

            return selectedRules.ToArray();
        }

        /// <summary>
        /// Gets the interaction rules.
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<IInteractionRule> InteractionRules
        {
            get { return interactionRules.AsReadOnly(); }
        }

        /// <summary>
        /// Gets the interaction rules count.
        /// </summary>
        /// <returns></returns>
        public int Count
        {
            get { return interactionRules.Count; }
        }

        public void CopyInteractionRules(string oldElementType, string newElementType)
        {
            var rulesToAdd = new List<IInteractionRule>();
            foreach (InteractionRule rule in interactionRules)
            {
                if (rule.Publisher.Equals(oldElementType))
                {
                    IInteractionRule newRule = rule.DeepClone();
                    newRule.Publisher = newElementType;
                    rulesToAdd.Add(newRule);
                }

                if (rule.Subscribers.Any(s => s == oldElementType))
                {
                    rule.Subscribers.Add(newElementType);
                }
            }

            interactionRules.AddRange(rulesToAdd);
        }

        public void Clear()
        {
            interactionRules.Clear();
        }

        #endregion

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool readStart = false;
            if (reader.Name.Equals("InteractionRuleRepository"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            interactionRules =
                new List<IInteractionRule>(SerializationService.DeserializeItemList<IInteractionRule>("Rules", typeof(InteractionRule), reader));
            
            if(readStart && reader.NodeType.Equals(XmlNodeType.EndElement)) reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var activeRules = new List<IInteractionRule>();
            foreach(var rule in interactionRules)
            {
                if(!rule.IsDisabled)
                {
                    activeRules.Add(rule);
                }
            }
            SerializationService.SerializeItemList("Rules", typeof(InteractionRule), activeRules, writer);
        }
    }

}
