using System.Configuration;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.DepictionConfiguration
{
    public class GeolocatedLocationElement : ConfigurationElement
    {
        [ConfigurationProperty("LocationName", DefaultValue = "", IsRequired = true)]
        public string LocationName
        {
            get { return (string)this["LocationName"]; }
            set { this["LocationName"] = value; }
        }

        [ConfigurationProperty("LocationLatLong", DefaultValue = "", IsRequired = true)]
        public ILatitudeLongitude LocationLatLong
        {
            get { return (ILatitudeLongitude)this["LocationLatLong"]; }
            set { this["LocationLatLong"] = value; }
        }
    }

    public class GeolocatedLocationRecords : ConfigurationElementCollection
    {
        public GeolocatedLocationElement this[int index]
        {
            get { return BaseGet(index) as GeolocatedLocationElement; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new GeolocatedLocationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((GeolocatedLocationElement)element).LocationName;
        }

        public void Add(ConfigurationElement geolocationPair)
        {
            BaseAdd(geolocationPair);
        }

        public void Insert(ConfigurationElement geolocationPair, int index)
        {
            BaseAdd(index, geolocationPair);
        }
    }
}