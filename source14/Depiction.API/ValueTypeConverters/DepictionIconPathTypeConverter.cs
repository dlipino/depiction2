﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Service;

namespace Depiction.API.ValueTypeConverters
{
    public class DepictionIconPathTypeConverter : TypeConverter, IValueConverter
    {
        public static ImageSource ConvertDepictionIconPathToImageSource(DepictionIconPath iconPath)
        {
            if (iconPath.IsValid && !string.IsNullOrEmpty(iconPath.Path))
            {
                switch (iconPath.Source)
                {
                    case IconPathSource.EmbeddedResource:
                        object rawImage = Application.Current.Resources[iconPath.Path];
                        if (rawImage is Drawing)
                            return new DrawingImage(rawImage as Drawing);
                        if (rawImage is ImageSource)
                            return rawImage as ImageSource;
                        break;
                    case IconPathSource.File:
                        var imageSource = GetImageSourceStringFromImageLibrarys(iconPath.Path);
                        if(imageSource != null) return imageSource;
                        //ok so the first attempt didn't work, maybe it has a name that
                        //came from 1.2 (full path)
                        var nonPathName = Path.GetFileName(iconPath.Path);
                        imageSource = GetImageSourceStringFromImageLibrarys(nonPathName);
                        if (imageSource != null) return imageSource;
                        
                        //Makes it this far, it must be a normal file not in a depiction image library.
                        //This is actually bad
                        if (File.Exists(iconPath.Path))
                        {
                            return BitmapSaveLoadService.LoadImageFromURLAndCreateThumb(iconPath.Path, 100, 100);
                        }
                        break;
                    //return Application.Current.Resources["CannotFindExpectedIcon"] as ImageSource;
                }
            }
            return Application.Current.Resources["CannotFindExpectedIcon"] as ImageSource;
            //return null;
        }
        #region helper methods
        static private ImageSource GetImageSourceStringFromImageLibrarys(string imageSourceName)
        {
            var app = Application.Current as IDepictionApplication;
            if (app != null)
            {
                var currentDepiction = app.CurrentDepiction;
                if (currentDepiction != null)
                {
                    var dict = currentDepiction.ImageResources;
                    if (dict.Contains(imageSourceName))
                    {

                        return dict[imageSourceName] as ImageSource;
                    }
                    if (app.ImageResources.Contains(imageSourceName))
                    {
                        return app.ImageResources[imageSourceName] as ImageSource;
                    }
                }
                else
                {
                    if (app.ImageResources.Contains(imageSourceName))
                    {
                        return app.ImageResources[imageSourceName] as ImageSource;
                    }
                }
            }
            return null;
        }
        #endregion
        #region boy this seems like a bad idea

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converter = new DepictionIconPathTypeConverter();
            var obj = converter.ConvertTo(value, typeof(ImageSource));
            return obj;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion

        public new static object ConvertFromString(string value)
        {
            return DepictionIconPath.Parse(value);
        }
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {

            if (destType == typeof(string)) return true;
            if (destType == typeof(ImageSource)) return true;
            return false;
        }
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value == null) return null;
            if (destinationType == typeof(string))
            {
                var iconPath = (DepictionIconPath)value;
                return iconPath.ToString();
            }
            if (destinationType == typeof(ImageSource))
            {
                var iconPath = (DepictionIconPath)value;

                if (Application.Current == null) return null;
                var imageSource = ConvertDepictionIconPathToImageSource(iconPath);
                if (imageSource != null)
                {
                    return imageSource;
                }
                //This will always fail because as of dec 2 2010 the Depiction.Default is nto an image source
                return Application.Current.Resources["Depiction.Default"] as ImageSource;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                return DepictionIconPath.Parse((string)value);
            }
            return base.ConvertFrom(context, info, value);
        }


    }
}