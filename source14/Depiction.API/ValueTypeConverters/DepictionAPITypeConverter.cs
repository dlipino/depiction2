using System;

namespace Depiction.API.ValueTypeConverters
{
    static public class DepictionAPITypeConverter
    {
        public static object CheapNumberTypeConverter(object value, Type newType)
        {
            if (!(value is int || value is double)) return null;
            if (newType.Equals(typeof(int)))
            {
                return Convert.ToInt32(value);
            }
            if (newType.Equals(typeof(double)))
            {
                return Convert.ToDouble(value);
            }
            return null;
        }
    }
}