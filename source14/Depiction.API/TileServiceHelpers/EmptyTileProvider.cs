﻿using System;
using System.Collections.Generic;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.TileServiceHelpers
{
    public class EmptyTiler : ITiler
    {
        public int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            return 0;
        }

        public IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            return null;
        }

        public string CreateImageFromTilesInBounds(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            return null;
        }

        public TileImageTypes TileImageType
        {
            get { return TileImageTypes.None; }
        }

        public string DisplayName
        {
            get { return "None"; }
        }

        public string SourceName
        {
            get { return "None"; }
        }

        public string LegacyImporterName
        {
            get { throw new NotImplementedException(); }
        }

        public int PixelWidth
        {
            get { return 0; }
        }

        public bool DoesOwnCaching
        {
            get { return false; }
        }

        public int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            throw new NotImplementedException();
        }

        public int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            throw new NotImplementedException();
        }

        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            throw new NotImplementedException();
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            throw new NotImplementedException();
        }

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            throw new NotImplementedException();
        }
    }
    public class EmptyTileProvider : AbstractDepictionDefaultImporter, ITileProvider
    {
        #region Implementation of ITileProvider
        public bool ReplaceCachedImages { get { return false; } }

        public TileImageTypes TileImageType
        {
            get { return TileImageTypes.None; }
        }

        public string DisplayName
        {
            get { return "None"; }
        }

        public string SourceName
        {
            get { return "None"; }
        }

        public void RetrieveTiles(IMapCoordinateBounds boundingBox)
        {
        }

        public string CreateImageFromTilesInBounds(IMapCoordinateBounds boundingBox)
        {
            return null;
        }

        public void CancelRequest()
        {
        }

        public event Action<TileModel> BackgroundTileReceived;

        public event Action<string> TileServiceInformation;

        #endregion


        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {

        }
    }
}
