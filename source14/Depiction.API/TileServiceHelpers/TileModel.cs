﻿using System;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.API.TileServiceHelpers
{
    /// <summary>
    ///  Information about one tile used for tiling the canvas in the background of a Depiction.
    /// </summary>
    public class TileModel : IEquatable<TileModel>
    {
        ///// <summary>
        ///// Create a reference to one tile.
        ///// </summary>
        ///// <param name="tileZoomLevel">The zoom level at which the style exists, as determined by Depiction.</param>
        ///// <param name="tileKey">The file name containing this tile.</param>
        ///// <param name="tilePath">The full path to the file containing this tile, including the file name.</param>
        ///// <param name="topLeft">The latitude and longitude of the top left corner of this tile.</param>
        ///// <param name="bottomRight">The latitude and longitude of the bottom right corner of this tile.</param>
        public TileModel(int tileZoomLevel, string tileKey, string tilePath, ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            TileIndex = new TileIndex(0,0,tileZoomLevel);
            TileKey = tileKey;
            TilePath = tilePath;
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public TileModel(int tileZoomLevel, int row, int column, Uri tileUri, ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            TileIndex = new TileIndex(column, row, tileZoomLevel);
            TileUri = tileUri;
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public TileIndex TileIndex { get; set; }
        //public string TileTag { get; set; }
        /// <summary>
        /// The zoom level at which the tile exists.
        /// </summary>
        public int TileZoomLevel { get { return TileIndex.TileZoomLevel; } }
        public int Row { get { return TileIndex.Row; } }
        public int Column { get { return TileIndex.Col; } }


        /// <summary>
        /// The file name containing this tile.
        /// </summary>
        public string TileKey { get; private set; }

        /// <summary>
        /// The full path to the file containing this tile, including the file name.
        /// </summary>
        public string TilePath { get; private set; }

        /// <summary>
        /// The latitude and longitude of the top left corner of this tile.
        /// </summary>
        public ILatitudeLongitude TopLeft { get; set; }

        /// <summary>
        /// The latitude and longitude of the bottom right corner of this tile.
        /// </summary>
        public ILatitudeLongitude BottomRight { get; set; }

        public byte[] Image { get; set; }
        public Uri TileUri { get; set; }

        public Func<TileModel, byte[]> FetchStrategy { get; set; }

        public int UTMZone { get; set; }  // damn you terraserver

        public bool Equals(TileModel other)
        {
            return TileIndex.Equals(other.TileIndex);
        }
    }
}