﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Depiction.API.TileServiceHelpers
{
    public class MemoryCache<T> : INotifyPropertyChanged
    {
        //for future implemenations or replacements of this class look 
        //into .net 4.0 System.Collections.Concurrent namespace.
        #region Fields

        private readonly Dictionary<TileIndex, T> bitmaps
          = new Dictionary<TileIndex, T>();

        private readonly Dictionary<TileIndex, DateTime> touched
          = new Dictionary<TileIndex, DateTime>();

        private readonly object syncRoot = new object();
        private readonly int maxTiles;
        private readonly int minTiles;

        #endregion

        #region Properties

        public int TileCount
        {
            get
            {
                return bitmaps.Count;
            }
        }

        #endregion

        #region Public Methods

        public MemoryCache(int minTiles, int maxTiles)
        {
            if (minTiles >= maxTiles) throw new ArgumentException("minTiles should be smaller than maxTiles");
            if (minTiles < 0) throw new ArgumentException("minTiles should be larger than zero");
            if (maxTiles < 0) throw new ArgumentException("maxTiles should be larger than zero");

            this.minTiles = minTiles;
            this.maxTiles = maxTiles;
        }

        public void Add(TileIndex index, T item)
        {
            lock (syncRoot)
            {
                if (bitmaps.ContainsKey(index))
                {
                    bitmaps[index] = item;
                    touched[index] = DateTime.Now;
                }
                else
                {
                    touched.Add(index, DateTime.Now);
                    bitmaps.Add(index, item);
                    if (bitmaps.Count > maxTiles) CleanUp();
                    OnNotifyPropertyChange("TileCount");
                }
            }
        }

        public void Remove(TileIndex index)
        {
            lock (syncRoot)
            {
                if (!bitmaps.ContainsKey(index)) return; //ignore if not exists
                touched.Remove(index);
                bitmaps.Remove(index);
                OnNotifyPropertyChange("TileCount");
            }
        }

        public T Find(TileIndex index)
        {
            lock (syncRoot)
            {
                if (!bitmaps.ContainsKey(index)) return default(T);

                touched[index] = DateTime.Now;
                return bitmaps[index];
            }
        }

        public void Clear()
        {
            lock (syncRoot)
            {
                bitmaps.Clear();
                touched.Clear();
            }
        }

        #endregion

        #region Private Methods

        private void CleanUp()
        {
            lock (syncRoot)
            {
                //Purpose: Remove the older tiles so that the newest x tiles are left.
                TouchPermaCache(touched);
                DateTime cutoff = GetCutOff(touched, minTiles);
                IEnumerable<TileIndex> oldItems = GetOldItems(touched, ref cutoff);
                foreach (TileIndex index in oldItems)
                {
                    Remove(index);
                }
            }
        }

        private static void TouchPermaCache(Dictionary<TileIndex, DateTime> touched)
        {
            var keys = new List<TileIndex>();
            //This is a temporary solution to preserve level zero tiles in memory.
            foreach (TileIndex index in touched.Keys) if (index.TileZoomLevel == 0) keys.Add(index);
            foreach (TileIndex index in keys) touched[index] = DateTime.Now;
        }

        private static DateTime GetCutOff(Dictionary<TileIndex, DateTime> touched, int lowerLimit)
        {
            var times = new List<DateTime>();
            foreach (DateTime time in touched.Values)
            {
                times.Add(time);
            }
            times.Sort();
            return times[times.Count - lowerLimit];
        }

        private static IEnumerable<TileIndex> GetOldItems(Dictionary<TileIndex, DateTime> touched, ref DateTime cutoff)
        {
            var oldItems = new List<TileIndex>();
            foreach (TileIndex index in touched.Keys)
            {
                if (touched[index] < cutoff)
                {
                    oldItems.Add(index);
                }
            }
            return oldItems;
        }

        #endregion

        #region INotifyPropertyChanged Members

        protected virtual void OnNotifyPropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
