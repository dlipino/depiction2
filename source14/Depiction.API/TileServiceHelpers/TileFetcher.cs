﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;

//using DepictionSilverlight.Tilers;

namespace Depiction.API.TileServiceHelpers
{
    public class TileFetcher
    {
        #region Fields

        private readonly MemoryCache<TileModel> memoryCache;
        private readonly ITiler tileSource;
        private IMapCoordinateBounds extent;
        //private double resolution;
        private readonly IList<TileModel> tilesInProgress = new List<TileModel>();
        private IList<TileModel> missingTiles = new List<TileModel>();
        private readonly IDictionary<TileModel, int> retries = new Dictionary<TileModel, int>();
        private const int ThreadMax = 6;
        private int threadCount;
        private readonly AutoResetEvent waitHandle = new AutoResetEvent(true);
        //private readonly IFetchStrategy strategy = new FetchStrategy();
        private const int MaxRetries = 2;
        private Thread loopThread;
        private volatile bool isThreadRunning;
        private volatile bool isViewChanged;
        private int tilesAcross;
        private TileCacheService cacheService;
        private static bool replaceCachedImages { get { return Settings.Default.ReplaceCachedFiles; } }
        #endregion

        #region EventHandlers
        
        public event Action<TileModel> DataChanged;

        #endregion

        #region Constructors Destructors

        public TileFetcher(ITiler tileSource)
        {
            if (tileSource == null) throw new ArgumentException("TileProvider can not be null");
            this.tileSource = tileSource;
            this.cacheService = new TileCacheService(tileSource.SourceName); 
            //if (memoryCache == null) throw new ArgumentException("MemoryCache can not be null");
            //this.memoryCache = memoryCache;
            memoryCache = new MemoryCache<TileModel>(600, 1000);
        }

        #endregion

        #region Public Methods

        public void ViewChanged(IMapCoordinateBounds newExtent, int tilesAcross)
        {
            this.tilesAcross = tilesAcross;
            extent = newExtent;
            //resolution = newResolution;
            isViewChanged = true;
            waitHandle.Set();
            if (!isThreadRunning) { StartThread(); }
        }

        private void StartThread()
        {
            isThreadRunning = true;
            loopThread = new Thread(TileFetchLoop);
            loopThread.IsBackground = true;
            loopThread.Name = "LoopThread";
            loopThread.Start();
        }

        public void AbortFetch()
        {
            isThreadRunning = false;
            waitHandle.Set();
        }

        #endregion

        #region Private Methods

        private void TileFetchLoop()
        {
            try
            {
#if !SILVERLIGHT //In Silverlight you can not specify a thread priority
                Thread.CurrentThread.Priority = ThreadPriority.BelowNormal;
#endif
                while (isThreadRunning)
                {
                    if (tileSource == null) waitHandle.Reset();

                    waitHandle.WaitOne();

                    if (isViewChanged && (tileSource != null))
                    {

                        missingTiles = tileSource.GetTiles(extent, tilesAcross, 18);
                        //missingTiles = strategy.GetTilesWanted(tileSource.Schema, extent.ToExtent(), level);
                        retries.Clear();
                        isViewChanged = false;
                    }

                    missingTiles = GetTilesMissing(missingTiles, memoryCache, retries);

                    FetchTiles();

                    if (missingTiles.Count == 0) { waitHandle.Reset(); }

                    if (threadCount >= ThreadMax) { waitHandle.Reset(); }
                }
            }
            finally
            {
                isThreadRunning = false;
            }
        }

        private static IList<TileModel> GetTilesMissing(IEnumerable<TileModel> infosIn, MemoryCache<TileModel> memoryCache, IDictionary<TileModel, int> retries)
        {
            IList<TileModel> tilesOut = new List<TileModel>();
            foreach (TileModel info in infosIn)
            {
                if ((!retries.Keys.Contains(info) || retries[info] < MaxRetries))

                    tilesOut.Add(info);
            }
            return tilesOut;
        }

        private void FetchTiles()
        {
            foreach (TileModel info in missingTiles)
            {
                if (threadCount >= ThreadMax) return;
                FetchTile(info);
            }
        }

        private void FetchTile(TileModel info)
        {
            TileModel tileModel;
            //first a number of checks
            if (tilesInProgress.Contains(info)) return;
            if (retries.Keys.Contains(info) && retries[info] >= MaxRetries) return;
            
            if ((tileModel = memoryCache.Find(info.TileIndex)) != null)
            {
                if (tileModel.Image != null && DataChanged != null && isThreadRunning)
                    DataChanged(tileModel);
                return;
            }

            //now we can go for the request.
            lock (tilesInProgress) { tilesInProgress.Add(info); }
            if (!retries.Keys.Contains(info)) retries.Add(info, 0);
            else retries[info]++;
            lock (this)
            {
                threadCount++;
            }
            StartFetchOnThread(info);
        }

        private void StartFetchOnThread(TileModel info)
        {
            //var fetchOnThread = new FetchOnThread(tileSource.Provider, info, LocalFetchCompleted);
            //var thread = new Thread(fetchOnThread.FetchTile);
            var worker = new BackgroundWorker();
            worker.DoWork += WorkerOnDoWork;
            worker.RunWorkerCompleted += WorkerOnRunWorkerCompleted;
            worker.RunWorkerAsync(info);

//#if !SILVERLIGHT
//            //In Wpf we use Wpf's own feature rendering which can only be done on a STA thread.
//            thread.SetApartmentState(ApartmentState.STA);
//#endif
//            thread.Name = "Tile Fetcher";
//            thread.Start();
        }


        private void WorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            
            if (!isThreadRunning) return;
            var tileModel = (TileModel) doWorkEventArgs.Argument;
            doWorkEventArgs.Result = tileModel;
            DoFetchTile(tileSource, tileModel, cacheService);

            //var tileViewModel = new TileViewModel(tileModel);
            //var bitmapImage = new BitmapImage();
            //bitmapImage.SetSource(new MemoryStream(image));
            
            //tileViewModel.TileSource = bitmapImage;

            
        }

        public static void DoFetchTile(ITiler tileSource, TileModel tileModel, TileCacheService cacheService)
        {
            try
            {
                var cacheName = tileSource.SourceName + "_" + tileModel.TileIndex.Row + "_" + tileModel.TileIndex.Col + "_" + tileModel.TileIndex.TileZoomLevel + ".jpg";
                string imagePath = cacheService.GetCacheFullStoragePath(cacheName);
                byte[] image;
                if (File.Exists(imagePath) && !replaceCachedImages && !tileSource.DoesOwnCaching)
                {
                    image = RequestHelper.FetchImage(imagePath);
                }
                else if (tileModel.FetchStrategy != null)
                {
                    image = tileModel.FetchStrategy(tileModel);
                    cacheService.SaveTileImage(image, cacheName, true);
                }
                else
                {
                    image = RequestHelper.FetchImage(tileModel.TileUri);
                    if (!tileSource.DoesOwnCaching)
                        cacheService.SaveTileImage(image, cacheName, true);
                }    //return new TileModel(zoom, cacheName, imagePath, topLeft, bottomRight);

                tileModel.Image = image;



            }
            catch (WebException e)
            {

            }
        }

        private void WorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!isThreadRunning) return;
            var tileModel = (TileModel) e.Result;
            //todo remove object sender
            try
            {
                if (e.Error == null && e.Cancelled == false && isThreadRunning && e.Result != null)
                    memoryCache.Add(tileModel.TileIndex, tileModel);//)e.TileInfo.Index, new MemoryStream(e.Image));
            }
            catch (Exception ex)
            {
                //e.Error = ex;
            }
            //finally
            //{
            lock (this)
            {
                threadCount--;
            }
            lock (tilesInProgress)
            {
                if (tilesInProgress.Contains(tileModel))
                    tilesInProgress.Remove(tileModel);
            }
            waitHandle.Set();
            //}

            if (tileModel.Image != null && DataChanged != null && isThreadRunning)
                DataChanged(tileModel);
        }
        
        #endregion
    }
}
