﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Xml;
using Depiction.FrameworkElements.Helpers.HtmlXamlConverter;

namespace Depiction.API.DialogBases.TextConverters
{
    public class HtmlToFlowDocumentConverter : IValueConverter
    {
        //Wow i feel really bad about this, hopefully doesnt stick around for more than a month dec 2011
        static public FlowDocument StringToFlowDocument(string stringToConvert)
        {
            string xaml = HtmlToXamlConverter.ConvertHtmlToXaml(stringToConvert, true) as string;
            StringReader stringReader = new StringReader(xaml);

            XmlReader xmlReader = XmlReader.Create(stringReader);
            FlowDocument doc = XamlReader.Load(xmlReader) as FlowDocument;
            doc.FontSize = 12;
            doc.PagePadding = new Thickness(0);
            return doc;
        }
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return StringToFlowDocument(value.ToString());
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}