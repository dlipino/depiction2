﻿using System;
using System.Windows.Data;

namespace Depiction.API.DialogBases.TextConverters
{
    public class InputStringToFlowDocumentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                var stringValue = value.ToString();
                //Check to see if it is html
                if (stringValue.Contains("<"))
                {
                    return HtmlToFlowDocumentConverter.StringToFlowDocument(stringValue);

                }
                return PlainTextToFlowDocumentConverter.PlainTextToFlowDocument(stringValue);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}