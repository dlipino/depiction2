﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Depiction.API.MVVM;

namespace Depiction.API.DialogBases
{
    public class DepictionDialogWindowBase : Window
    {//This window is designed for showing and hiding. Basically a window that never gets destroyed (ie not modal)
        private Point initialLocation;// = new Point(double.NaN,double.NaN);
        private bool sizeChangeListenerAttached;
        public Style DepictionWindowStyle
        {
            get
            {
                Style frameStyle = null;
                string styleName = "DepictionWindowStyle";
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    if (Application.Current.Resources.Contains(styleName))
                    {
                        frameStyle = Application.Current.Resources[styleName] as Style;
                    }
                }
                return frameStyle;
            }
        }

        //Hacky, please fix dec 2011 davidl
        public Point InitialLocation
        {
            get { return initialLocation; }
            set
            {
                if (WindowStartupLocation != WindowStartupLocation.Manual)
                {
                    initialLocation = value;
                    WindowStartupLocation = WindowStartupLocation.Manual;
                    Left = initialLocation.X;
                    Top = initialLocation.Y;
                }
            }
        }
        public string AssociatedHelpFile { get; set; }

        #region Dependency props
        public SolidColorBrush TitleBackground
        {
            get { return (SolidColorBrush)GetValue(TitleBackgroundProperty); }
            set { SetValue(TitleBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBackgroundProperty =
            DependencyProperty.Register("TitleBackground", typeof(SolidColorBrush), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(Brushes.Black));


        public SolidColorBrush TitleForeground
        {
            get { return (SolidColorBrush)GetValue(TitleForegroundProperty); }
            set { SetValue(TitleForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleForegroundProperty =
            DependencyProperty.Register("TitleForeground", typeof(SolidColorBrush), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(Brushes.White));

        public bool ShowInfoButton
        {
            get { return (bool)GetValue(ShowInfoButtonProperty); }
            set { SetValue(ShowInfoButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInfoButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInfoButtonProperty =
            DependencyProperty.Register("ShowInfoButton", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));

        public bool Resizeable
        {
            get { return (bool)GetValue(ResizeableProperty); }
            set { SetValue(ResizeableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Resizeable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResizeableProperty =
            DependencyProperty.Register("Resizeable", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));


        //HideOnClose is a total hack and used as an immedieate fix to the problem with long lasting windows being completely close (when the needt be hidden prematurely
        //THis is a hack property
        public bool HideOnClose
        {
            get { return (bool)GetValue(HideOnCloseProperty); }
            set { SetValue(HideOnCloseProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HideOnClose.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HideOnCloseProperty =
            DependencyProperty.Register("HideOnClose", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));

        

        #endregion

        public DepictionDialogWindowBase()
        {
            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.HighQuality);
            Style = DepictionWindowStyle;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            Initialized += BaseDepictionWindow_Initialized;
            PreviewMouseDown += BaseDepictionWindow_PreviewMouseDown;
            AssociatedHelpFile = "welcome.html";
            DataContextChanged += BaseDepictionWindow_DataContextChanged;
            AttachChangeListener();
            Closing += BaseDepictionWindow_Closing;//Hack for fixing depiction dialog closing bug
            var bindings = CommandBindings;
            var commandBinding = new CommandBinding(ApplicationCommands.Stop, Close_Executed);
            bindings.Add(commandBinding);

            if (Application.Current != null)
            {
                var mainWindow = Application.Current.MainWindow;
                if (mainWindow != null)
                {
                    foreach (CommandBinding binding in mainWindow.CommandBindings)
                    {
                        var routed = binding.Command as RoutedUICommand;
                        if (routed != null)
                        {
                            if (routed.Name.Equals("Help"))
                            {
                                bindings.Add(binding);
                            }
                        }
                    }
                }
            }
        }

       

        #region Size changes
        void AttachChangeListener()
        {
            if (!sizeChangeListenerAttached)
            {
                SizeChanged += BaseDepictionWindow_SizeChanged;

                LocationChanged += BaseDepictionWindow_LocationChanged;
                sizeChangeListenerAttached = true;
            }
        }

        void BaseDepictionWindow_LocationChanged(object sender, EventArgs e)
        {
            LocationOrSizeChanged_Updated();
        }
        void BaseDepictionWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            LocationOrSizeChanged_Updated();
        }
        virtual protected void LocationOrSizeChanged_Updated()
        {

        }
        virtual public void SetLocationToStoredValue()
        {
            
        }

        #endregion
        void BaseDepictionWindow_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //Legacy code that caused focus issues with combobox that were in the window in questions
           // var gotFocus = Focus();
           // if (!gotFocus)
           // {
           //     Debug.WriteLine("Could not get focus for this window");
           // }
        }

        void BaseDepictionWindow_Closing(object sender, CancelEventArgs e)
        {
            //Hack fix, need give this class a proper fixing soon, it is a mish mash right now.
            var modal = System.Windows.Interop.ComponentDispatcher.IsThreadModal;
            if (modal) return;
            //Hack so that depiction dialogs closed from the toolbar can be reopened.
            var dc = DataContext as IDepictionDialogVM;
            if (dc != null || HideOnClose)
            {
                SoftCloseWindow();
                e.Cancel = true;
            }
        }

        void BaseDepictionWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newDC = e.NewValue as IDepictionDialogVM;
            var oldDC = e.OldValue as IDepictionDialogVM;
            if (oldDC != null)
            {
                oldDC.PropertyChanged -= DataContext_PropertyChanged;
            }
            if (newDC == null) return;
            newDC.PropertyChanged += DataContext_PropertyChanged;
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var abVM = sender as IDepictionDialogVM;
            if (abVM == null) return;
            var prop = e.PropertyName;
            if (prop.Equals("IsDialogVisible"))
            {
                MainThreadShow(abVM.IsDialogVisible);
            }
        }

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SoftCloseWindow();
        }

        void BaseDepictionWindow_Initialized(object sender, EventArgs e)
        {
            var mainWindow = Application.Current.MainWindow;
            if (!ReferenceEquals(this, mainWindow))
            {
                if (mainWindow.IsLoaded) Owner = mainWindow;
            }
        }

        private void title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public override void OnApplyTemplate()
        {
            var close = (Button)Template.FindName("PART_Close", this);
            if (null != close)
            {
                close.IsCancel = true;
                close.IsDefault = true;
                //                close.Click+=close_Click;
                //                if(removeClose)
                //                {
                //                    RemoveTopCloseButton();
                //                }

                //Why would anybody want to do this? more importantly what is it always false for me (davidl)
                //                if (false == _cancel.IsVisible)
                //                {
                //                    _close.IsCancel = false;
                //                }
            }

            var title = (FrameworkElement)Template.FindName("PART_Title", this);
            if (null != title)
            {
                title.MouseLeftButtonDown += title_MouseLeftButtonDown;
            }
            base.OnApplyTemplate();
        }

        virtual protected void SoftCloseWindow()
        {
            var modal = System.Windows.Interop.ComponentDispatcher.IsThreadModal;
            if (modal)
            {
                Close();
            }
            else
            {
                var dc = DataContext as IDepictionDialogVM;
                if (dc == null) Hide();//Odd, why would we hide instead of close? because if the window is static 
                    //closing this makes Show impossible.
                else
                {
                    if (Owner != null)
                    {
                        Owner.Activate();
                    }
                    dc.IsDialogVisible = false;
                   
                }
            }
        }

        private bool hasBeenShown;
        virtual protected void MainThreadShow(bool show)
        {//not sure if this works right
            if (Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<bool>(MainThreadShow), show);
                return;
            }
            if (show)
            {
                if (!hasBeenShown && !initialLocation.Equals(new Point()))
                {
                    hasBeenShown = true;
                    Left = initialLocation.X;
                    Top = initialLocation.Y;
                }
                Show();
            }
            else
            {
                Hide();
                if (Owner != null)
                {
                    Owner.Focus();
                }
            }
        }
    }
}