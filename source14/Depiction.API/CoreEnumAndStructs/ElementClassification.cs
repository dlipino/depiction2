namespace Depiction.API.CoreEnumAndStructs
{
    /// <summary>
    /// A list of all types of Depiction elements (things that get displayed on the map). This is used to determine the Z order
    /// of the elements that get drawn. The method is not very flexible, so if somebody can think of a better idea...
    /// </summary>
    public enum ElementClassification
    {
        /// <summary>
        /// 
        /// </summary>
        PrincipleElement = 0, // SimObject must be 0 to maintain the default
        /// <summary>
        /// 
        /// </summary>
        Coverage = 1,
        /// <summary>
        /// 
        /// </summary>
        MapsAndImagery = 4,
        /// <summary>
        /// 
        /// </summary>
        LabelCoverage = 3,
        HiResMapsAndImagery=5,
        UserImagery = 6,//who uses this
        UsableCoverage =7//Used by flood or any large element that can be used by story ie rules apply
    }

    public enum DepictionDisplayerType
    {
        None, MainMap, Revealer, TopRevealer
    }

    public enum RevealerShapeType
    {
        Rectangle, Circle
    }
        /// <summary>
    /// Used to override natural sort order when elements are displayed in lists
    /// </summary>
    public enum DepictionSortPriority
    {
        Low,
        Normal,
        High
    }
    
}