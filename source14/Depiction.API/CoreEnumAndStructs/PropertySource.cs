﻿namespace Depiction.API.CoreEnumAndStructs
{
    /// <summary>
    /// Indicates the source of the definition for a property of an element.
    /// </summary>
    public enum PropertySource
    {        //Hmm not too happy about this here, but it is a good temp location for when we start moving things around (ie doesn't get in the
        //way of anything and will be easy to refactor.
        /// <summary>
        /// Defined in the element definition file (DML).
        /// </summary>
        DML,
        /// <summary>
        /// Read from a OGR data source; see http://www.gdal.org/ogr/.
        /// </summary>
        OGR,
        /// <summary>
        /// Read from a comma separated value (CSV) file.
        /// </summary>
        CSV,
        /// <summary>
        /// Read from an e-mail via a live report.
        /// </summary>
        EMAIL
    }
    public enum WxSInformationType
    {
        WFS,
        WMS,
        WCS
    }

    public enum InformationSource
    {
        File,
        Web
    }

    public enum DepictionDialogType
    {
        ManageContent,
        ManageContentNonGeoAligned,
        PropertyEditor,
        PropertyEditorHoverText
    }

    public enum DepictionIconSize
    {
        Lilliputian,
        Tiny,
        Small,
        Medium,
        Large,
        ExtraLarge
    }
    public enum DepictionIconBorderShape
    {
        None, Circle, Triangle, Square
    }
    public enum ElementColorMode
    {
        NotAValidMode,
        NoChange,
        RemoveColor,
        ThematicMapping
    }
}