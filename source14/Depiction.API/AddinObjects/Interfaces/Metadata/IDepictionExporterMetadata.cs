using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IDepictionExporterMetadata : IDepictionExtensionIOMetadata
    {
    }
}