using Depiction.API.CoreEnumAndStructs;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IDepictionNonDefaultImporterMetadata : IDepictionExtensionIOMetadata
    {
        string Company { get; }

        InformationSource[]  ImporterSources { get; }
        string[] ValidRegions { get; }
    }
}