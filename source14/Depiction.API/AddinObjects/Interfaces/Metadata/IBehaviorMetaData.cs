﻿namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IBehaviorMetaData : IBaseMetadata
    {
        string BehaviorName { get; }
        string BehaviorDisplayName { get; }
        string BehaviorDescription { get; }
    }
}