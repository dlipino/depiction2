using Depiction.API.MVVM;

namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IDepictionAddonBase
    {
        object AddonMainView { get; }
        string AddonConfigViewText { get; }
        object AddonConfigView { get; }
        object AddonActivationView { get; }
        //Not sure why this is here
        ViewModelBase AddinViewModel { get; }
    }

}