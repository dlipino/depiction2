namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IDepictionDefaultImporter : IDepictionImporterBase
    {
        bool Activated { get; }
    }

}