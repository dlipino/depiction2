using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IDepictionElementExporter : IDepictionAddonBase
    {
        void ExportElements(object location, IEnumerable<IDepictionElement> elements);
    }
}