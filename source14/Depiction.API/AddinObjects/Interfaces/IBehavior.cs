﻿using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IBehavior
    {
        /// <summary>
        /// Defines the name and type for each of the paramters required by this behavior.
        /// These are the parameters passed into the InternalDoBehavior method.
        /// The behavior will only be invoked if the number and type of parameters match what InternalDoBehavior expects.
        /// </summary>
        ParameterInfo[] Parameters { get; }
        BehaviorResult InternalDoBehavior(IDepictionElementBase subscriber, Dictionary<string, object> parameterBag);
        BehaviorResult DoBehavior(IDepictionElementBase subscriber, object[] inputParameters);
    }
}