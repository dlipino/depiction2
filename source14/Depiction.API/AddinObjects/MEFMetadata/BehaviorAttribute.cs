﻿using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class BehaviorAttribute : DepictionAddonBaseMetadata, IBehaviorMetaData
    {
        public BehaviorAttribute(string behaviorName, string behaviorDisplayName, string behaviorDescription)
            : base(typeof(IBehavior))
        {
            Name =DisplayName = behaviorName;
            
            this.BehaviorName = behaviorName;
            this.BehaviorDisplayName = behaviorDisplayName;
            this.BehaviorDescription = behaviorDescription;
        }
        public string BehaviorName { get; set; }

        public string BehaviorDisplayName { get; set; }

        public string BehaviorDescription { get; set; }
    }
}