using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionDefaultImporterMetadata : DepictionAddonBaseMetadata, IDepictionDefaultImporterMetadata
    {
        #region variables
        
        protected string[] extensionsSupported = new string[0];
        protected string[] typesSupported = new string[0];
        protected string[] typesNotSupported = new string[0];
        protected InformationSource[] importerSources = new InformationSource[0];
        
        #endregion
        
        #region Properties

        public string FileTypeInformation { get; set; }

        public string[] ExtensionsSupported
        {
            get { return extensionsSupported; }
        }

        public string[] ElementTypesSupported
        {
            get { return typesSupported; }
            set{ typesSupported = value;}
        }
        public string[] ElementTypesNotSupported
        {
            get { return typesNotSupported; }
            set{ typesNotSupported = value;}
        }
        public InformationSource[] ImporterSources
        {
            get { return importerSources; }
            set { importerSources = value; }
        }
        #endregion

        #region constructors

        public DepictionDefaultImporterMetadata(Type metadataForType) : base(metadataForType)
        {
            Name = DisplayName = "Importer";
        }

        public DepictionDefaultImporterMetadata(string name)
            : base(typeof(IDepictionDefaultImporter))
        {
            Name = name;
        }
        public DepictionDefaultImporterMetadata(string name, string[] extensions)
            : this(name)
        {
            extensionsSupported = extensions;
        }
        public DepictionDefaultImporterMetadata(string name, string[] extensions, string[] types)
            : this(name)
        {
            extensionsSupported = extensions;
            typesSupported = types;
        }
        #endregion



    }
}