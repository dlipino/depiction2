using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionAddonBaseMetadata : ExportAttribute, IBaseMetadata
    {
        #region Variables

        protected string addonPackage = string.Empty;
        protected string name = "Addon";
        protected string displayName = "Add-on";
        protected string author = "Nobody";
        protected string description = "";
        protected string company = string.Empty;

        protected AddonUsage[] addonUsages = new AddonUsage[0];
        #endregion

        #region Constructor

        public DepictionAddonBaseMetadata() : this(typeof(IDepictionAddonBase))
        {
        }

        protected DepictionAddonBaseMetadata(Type metadataForType) : base(metadataForType) { }

        #endregion

        #region Implementation of IBaseMetadata

        virtual public string AddonPackage
        {
            get { return addonPackage; }
            set { addonPackage = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public string Company
        {
            get { return company; }
            set { company = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public AddonUsage[] AddonUsages
        {
            get { return addonUsages; }
            set { addonUsages = value; }
        }

        public bool IsDefault { get; set; }


        #endregion
    }
}