using System.Collections.Generic;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;

namespace Depiction.API.AddinObjects.AbstractObjects
{
    abstract public class AbstractDepictionDefaultImporter : IDepictionDefaultImporter
    {
        virtual public bool Activated { get { return true; } }
        public event ElementImportCompleteEventHandler ElementImportingComplete;

        //These might not even be needed, assuming Depiction has a way of
        //addin contentcontrols, windows etc
        virtual public object AddonMainView { get { return null; } }
        virtual public object AddonConfigView { get { return null; } }
        virtual public string AddonConfigViewText { get { return null; } }
        virtual public object AddonActivationView { get { return null; } }

        virtual public ViewModelBase AddinViewModel { get { return null; } }
        

        public abstract void ImportElements(object elementLocation, string defaultElementType,
                                            IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters);
    }
}