namespace Depiction.API.AddinObjects
{
    public enum AddonUsage
    {
        FileImporter,
        QuickStartImporter,
        TileImporter,
        HighResTileImporter,
        FileExporter,
        Condition,
        Behavior,
        GeoCoder
    }
}