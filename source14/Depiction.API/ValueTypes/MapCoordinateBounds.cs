﻿using System;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ExtensionMethods;
using Depiction.API.ValueTypes;
using Depiction.Serialization;
using GeoFramework;
using Distance=GeoFramework.Distance;

namespace Depiction.API.ValueTypes
{
    [Serializable]//Serializable is used by the terra server tiling
    public class MapCoordinateBounds : IMapCoordinateBounds,IXmlSerializable
    {
        #region Properties

        public double Top { get; set; }
        public double Bottom { get; set; }
        public double Right { get; set; }
        public double Left { get; set; }

        /// <summary>
        /// Size of map to return when requesting a map.
        /// </summary>
        public Size MapImageSize { get; set; }


        /// <summary>
        /// Top left (northwest) corner of the box
        /// </summary>
        public ILatitudeLongitude TopLeft
        {
            get { return new LatitudeLongitude(Top, Left); }
        }

        /// <summary>
        /// Bottom right (southeast) corner of the box
        /// </summary>
        public ILatitudeLongitude BottomRight
        {
            get { return new LatitudeLongitude(Bottom, Right); }
        }

        /// <summary>
        /// Top right (northeast) corner of the box
        /// </summary>
        public ILatitudeLongitude TopRight
        {
            get { return new LatitudeLongitude(Top, Right); }
        }

        /// <summary>
        /// Bottom left (southwest) corner of the box
        /// </summary>
        public ILatitudeLongitude BottomLeft
        {
            get { return new LatitudeLongitude(Bottom, Left); }
        }
        /// <summary>
        /// Center of the bounding box; calculated with Lat = (Top + Bottom / 2), Lon = (Right + Left) /2
        /// </summary>
        public ILatitudeLongitude Center
        {
            get { return new LatitudeLongitude(((Bottom + Top) / 2), ((Right + Left) / 2)); }
        }

        public bool Contains(ILatitudeLongitude location)
        {
            if (location == null) return false;
            var lat = location.Latitude;
            if (!location.IsValid) return false;
            if (lat < Bottom || lat > Top) return false;
            var lon = location.Longitude;
            if (lon < Left || lon > Right) return false;
            return true;
        }

        public bool IsValid
        {
            get
            {
                if (Top.Equals(double.NaN) || Left.Equals(double.NaN) || Bottom.Equals(double.NaN) || Right.Equals(double.NaN))
                    return false;
                return true;
            }
        }
        #endregion

        public MapCoordinateBounds()
        {
            Top = Left = Bottom = Right = double.NaN;
        }
        ///<summary>
        /// Initializes a new instance of the <see cref="MapCoordinateBounds"/> class.
        ///</summary>
        ///<param name="topLeft">The top left coordinate of the bounding box.</param>
        ///<param name="bottomRight">The bottom right coordinate of the bounding box.</param>
        public MapCoordinateBounds(ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            Top = topLeft.Latitude;
            Left = topLeft.Longitude;
            Bottom = bottomRight.Latitude;
            Right = bottomRight.Longitude;
        }
        public double GetMidLineHorizontalDistance(MeasurementSystem system, MeasurementScale scale)
        {
            var start = new LatitudeLongitude((Top + Bottom) / 2, Left);
            var end = new LatitudeLongitude((Top + Bottom) / 2, Right);
            return start.DistanceTo(end, system, scale);//GetDistanceFromLocationToOtherLocation(start, end, system, scale);
        }
        public double GetMidLineVerticalDistance(MeasurementSystem system, MeasurementScale scale)
        {
            var start = new LatitudeLongitude(Top, (Left + Right) / 2);
            var end = new LatitudeLongitude(Bottom, (Left + Right) / 2);
            return start.DistanceTo(end, system, scale);//GetDistanceFromLocationToOtherLocation(start, end, system, scale);
        }
        static public DistanceUnit ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(MeasurementSystem system, MeasurementScale scale)
        {
            var systemAndScale = (int)system + (int)scale;
            DistanceUnit geoDistanceUnit;
            switch (systemAndScale)
            {
                case BaseMeasurement.ImperialSmall:
                    geoDistanceUnit = DistanceUnit.Inches;
                    break;
                case BaseMeasurement.ImperialNormal:
                    geoDistanceUnit = DistanceUnit.Feet;
                    break;
                case BaseMeasurement.ImperialLarge:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case BaseMeasurement.MetricSmall:
                    geoDistanceUnit = DistanceUnit.Centimeters;
                    break;
                case BaseMeasurement.MetricNormal:
                    geoDistanceUnit = DistanceUnit.Meters;
                    break;
                case BaseMeasurement.MetricLarge:
                    geoDistanceUnit = DistanceUnit.Kilometers;
                    break;
                default:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
            }
            return geoDistanceUnit;
        }
        static public ILatitudeLongitude GetLatLongAtDistanceFromOrigin(ILatitudeLongitude start,double distance, MeasurementSystem system, MeasurementScale scale,double bearing)
        {
            var startGeoPos = new Position(new Latitude(start.Latitude), new Longitude(start.Longitude));
            var geoDistanceUnit = ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(system, scale);
          
            var geoDistance = new Distance(distance, geoDistanceUnit);
            var endGeoPos = startGeoPos.TranslateTo(bearing, geoDistance);
            return new LatitudeLongitude(endGeoPos.Latitude, endGeoPos.Longitude);
        }


        #region ToString
        public override string ToString()
        {
            var info = string.Format("TopLeft: {0},{1}", Top, Left);
            info += "\n";
            info += string.Format("BottomRight: {0},{1}", Bottom, Right);
            return info;
        }
        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (MapCoordinateBounds)obj;
            if (!Equals(Top, other.Top)) return false;
            if (!Equals(Left, other.Left)) return false;
            if (!Equals(Bottom, other.Bottom)) return false;
            if (!Equals(Right, other.Right)) return false;

            return true;
        }
        #endregion

        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (!reader.Name.Equals("MapCoordinateBounds")) return;
           //Decimals might have cause problmes.
            Top = double.Parse(reader.GetAttribute("top"));
            Left = double.Parse(reader.GetAttribute("left"));
            Bottom = double.Parse(reader.GetAttribute("bottom"));
            Right = double.Parse(reader.GetAttribute("right"));
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MapCoordinateBounds");
            var typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(GetType());
            writer.WriteAttributeString("type", typeName);
            writer.WriteAttributeString("top", Top.ToString("R"));
            writer.WriteAttributeString("left", Left.ToString("R"));
            writer.WriteAttributeString("bottom", Bottom.ToString("R"));
            writer.WriteAttributeString("right", Right.ToString("R"));
            writer.WriteEndElement();
        }

        #endregion

        //            #region some helpers that should probably hit api or core
        static public IMapCoordinateBounds GetGeoRectThatIsPercentage(IMapCoordinateBounds bounds, double sizePercentage, 
            ILatitudeLongitude startLocation,int count)
        {
            var relativeStart = .2;
            var multi = 1;
            if (sizePercentage <= 1)
            {
                if(count <0)
                {
                    relativeStart = (1 - sizePercentage) / 2;
                }else
                {
                    relativeStart = .05 + count/20.0;
                }
            }
            else
            {
                multi = -1;
                relativeStart = (1 - sizePercentage) / 2;
                //relativeStart *= multi;
            }
            var latDistance = Math.Abs(bounds.Top - bounds.Bottom);
            var longDistance = Math.Abs(bounds.Left - bounds.Right);
            var newLatDist = latDistance*sizePercentage;
            var newLongDist = longDistance*sizePercentage;
            var start = startLocation;
           
            if(start == null)
            {
                start = new LatitudeLongitude(bounds.Top - (latDistance * relativeStart), 
                                bounds.Left+(longDistance * relativeStart));
//                start = new LatitudeLongitude(bounds.Top, bounds.Left);
            }
            var newBottomRight = new LatitudeLongitude(start.Latitude - (newLatDist), start.Longitude + newLongDist);
            if(newBottomRight.IsValid && start.IsValid) return new MapCoordinateBounds(start, newBottomRight);
            return bounds;
        }

        public void ExpandToInclude(MapCoordinateBounds other)
        {
            //if (other.IsNull)
            //    return;
            //if (IsNull)
            //{
            //    _minx = other.MinX;
            //    _maxx = other.MaxX;
            //    _miny = other.MinY;
            //    _maxy = other.MaxY;
            //}
            //else
            {
                if (other.Left < Left)
                    Left = other.Left;
                if (other.Right> Right)
                    Right = other.Right;
                if (other.Bottom < Bottom)
                    Bottom = other.Bottom;
                if (other.Top > Top)
                    Top = other.Top;
            }
        }


        /// <summary>
        /// Gets the maximum extent of this envelope across both dimensions.
        /// </summary>
        /// <returns></returns>
        public double MaxExtent
        {
            get
            {
                //if (IsNull) return 0.0;
                double w = Width;
                double h = Height;
                if (w > h) return w;
                return h;
            }
        }

        public double Height
        {
            get { return Top - Bottom; }
        }

        public double Width
        {
            get { return Right - Left; }
        }

        public Ordinate LongestAxis()
        {
            if (Math.Abs(MaxExtent - Width) < .000001)
                return Ordinate.X;
            return Ordinate.Y;
        }

        public bool Intersects(IMapCoordinateBounds other)
        {
            if (other == null)
                return false;
            return !(other.Left > Right|| other.Right < Left || other.Bottom > Top || other.Top < Bottom);
        }
    }


    public enum Ordinate
    {        
        /// <summary>
        /// X Ordinate = 0.
        /// </summary>
        X = 0,

        /// <summary>
        /// Y Ordinate = 1.
        /// </summary>
        Y = 1,

        /// <summary>
        /// Z Ordinate = 2.
        /// </summary>
        Z = 2,

        /// <summary>
        /// M Ordinate = 3
        /// </summary>
        M = 3,

        /// <summary>
        /// Ordinate at index 2
        /// </summary>
        Ordinate2 = 2,

        /// <summary>
        /// Ordinate at index 3
        /// </summary>
        Ordinate3 = 3,

        /// <summary>
        /// Ordinate at index 4
        /// </summary>
        Ordinate4 = 4,

        /// <summary>
        /// Ordinate at index 5
        /// </summary>
        Ordinate5 = 5,

        /// <summary>
        /// Ordinate at index 6
        /// </summary>
        Ordinate6 = 6,

        /// <summary>
        /// Ordinate at index 7
        /// </summary>
        Ordinate7 = 7,

        /// <summary>
        /// Ordinate at index 8
        /// </summary>
        Ordinate8 = 8,

        /// <summary>
        /// Ordinate at index 9
        /// </summary>
        Ordinate9 = 9,

        /// <summary>
        /// Ordinate at index 10
        /// </summary>
        Ordinate10 = 10,

        /// <summary>
        /// Ordinate at index 11
        /// </summary>
        Ordinate11 = 11,

        /// <summary>
        /// Ordinate at index 12
        /// </summary>
        Ordinate12 = 12,

        /// <summary>
        /// Ordinate at index 13
        /// </summary>
        Ordinate13 = 13,

        /// <summary>
        /// Ordinate at index 14
        /// </summary>
        Ordinate14 = 14,

        /// <summary>
        /// Ordinate at index 15
        /// </summary>
        Ordinate15 = 15,

        /// <summary>
        /// Ordinate at index 16
        /// </summary>
        Ordinate16 = 16,

        /// <summary>
        /// Ordinate at index 17
        /// </summary>
        Ordinate17 = 17,

        /// <summary>
        /// Ordinate at index 18
        /// </summary>
        Ordinate18 = 18,

        /// <summary>
        /// Ordinate at index 19
        /// </summary>
        Ordinate19 = 19,

        /// <summary>
        /// Ordinate at index 20
        /// </summary>
        Ordinate20 = 20,

        /// <summary>
        /// Ordinate at index 21
        /// </summary>
        Ordinate21 = 21,

        /// <summary>
        /// Ordinate at index 22
        /// </summary>
        Ordinate22 = 22,

        /// <summary>
        /// Ordinate at index 23
        /// </summary>
        Ordinate23 = 23,

        /// <summary>
        /// Ordinate at index 24
        /// </summary>
        Ordinate24 = 24,

        /// <summary>
        /// Ordinate at index 25
        /// </summary>
        Ordinate25 = 25,

        /// <summary>
        /// Ordinate at index 26
        /// </summary>
        Ordinate26 = 26,

        /// <summary>
        /// Ordinate at index 27
        /// </summary>
        Ordinate27 = 27,

        /// <summary>
        /// Ordinate at index 28
        /// </summary>
        Ordinate28 = 28,

        /// <summary>
        /// Ordinate at index 29
        /// </summary>
        Ordinate29 = 29,

        /// <summary>
        /// Ordinate at index 30
        /// </summary>
        Ordinate30 = 30,

        /// <summary>
        /// Ordinate at index 31
        /// </summary>
        Ordinate31 = 31,

        /// <summary>
        /// Ordinate at index 32
        /// </summary>
        Ordinate32 = 32,
    }
}