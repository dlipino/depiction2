﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.ValueTypeConverters;

namespace Depiction.API.ValueTypes
{
    ///<summary>
    /// Class to represent nodes in a road network
    ///</summary>
    public class RoadNode : IXmlSerializable
    {
        //Bad dog
       // static private LatitudeLongitudeTypeConverter converter = new LatitudeLongitudeTypeConverter();

        #region constructors

        public RoadNode()
        {

        }
        ///<summary>
        /// Class to represent nodes in a road network
        ///</summary>
        ///<param name="v1">Latlon of the vertex</param>
        public RoadNode(LatitudeLongitude v1)
        {
            if (v1 != null)
            {
                var lat = v1.Latitude;
                var lon = v1.Longitude;
                Vertex = new LatitudeLongitude(lat, lon);
            }
            NodeID = 0;
        }

        #endregion

        public LatitudeLongitude Vertex { get; set; }

        public int NodeID { get; set; }

        #region Methods
        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            // These two lines handle null or wrong type
            var other = obj as RoadNode;
            if (other == null) return false;

            return (Vertex.Latitude == other.Vertex.Latitude && Vertex.Longitude == other.Vertex.Longitude);
        }
        ///<summary>
        /// Equality operator for RoadNode
        ///</summary>
        ///<param name="a"></param>
        ///<param name="b"></param>
        ///<returns></returns>
        public static bool operator ==(RoadNode a, RoadNode b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Vertex.Latitude == b.Vertex.Latitude && a.Vertex.Longitude == b.Vertex.Longitude;
        }

        ///<summary>
        /// Not equals operator that compares two road nodes
        ///</summary>
        ///<param name="a"></param>
        ///<param name="b"></param>
        ///<returns></returns>
        public static bool operator !=(RoadNode a, RoadNode b)
        {
            return !(a == b);
        }
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = Vertex.Latitude.GetHashCode() >> 3;
            hashCode ^= Vertex.Longitude.GetHashCode();
            return hashCode;
        }
        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            Vertex = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(reader.GetAttribute("Location"));
            NodeID = int.Parse(reader.GetAttribute("NodeID"));
            reader.ReadStartElement("RoadNode");
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("RoadNode");
            writer.WriteAttributeString("Location", Vertex.ToXmlSaveString());
            writer.WriteAttributeString("NodeID",NodeID.ToString());
            writer.WriteEndElement();
        }
    }
}
