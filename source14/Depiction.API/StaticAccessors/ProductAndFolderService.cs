﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;

namespace Depiction.API.StaticAccessors
{
    static public class ProductAndFolderService
    {
//        public const string AutoDetectElementString = "Depiction.Plugin.AutoDetect";
        public static readonly List<string> ImageFileTypes = new List<string> {
            ".jpg",
            ".png",
            ".bmp",
            ".gif",
            ".tif",
            ".tiff"
        };
        public static readonly Dictionary<DepictionIconSize, double> AvailableIconSizes = new Dictionary<DepictionIconSize, double>
                                                               {
                                                                   { DepictionIconSize.Lilliputian,12d }, 
                                                                   { DepictionIconSize.Tiny, 16d} , 
                                                                   {DepictionIconSize.Small, 20d }, 
                                                                   { DepictionIconSize.Medium, 24d }, 
                                                                   {DepictionIconSize.Large, 32d }, 
                                                                   { DepictionIconSize.ExtraLarge, 38d }
                                                               };

        static public Dictionary<double, string> GetIconSizeNames()
        {
            return new Dictionary<double, string>
                                                               {
                                                                   { 12d,"Lilliputian" }, 
                                                                   { 16d,"Tiny"} , 
                                                                   {20d,"Small" }, 
                                                                   {24d,"Medium" }, 
                                                                   {32d, "Large"}, 
                                                                   { 38d,"Extra-Large"}
                                                               };
        }
        static public List<string> SevenColors = new List<string>
                                                {
                                                    Brushes.Blue.ToString(),
                                                    Brushes.Red.ToString(),
                                                    Brushes.Green.ToString(),
                                                    Brushes.Yellow.ToString(),
                                                    Brushes.Orange.ToString(),
                                                    Brushes.Aquamarine.ToString(),
                                                    Brushes.Salmon.ToString()
                                                };
        //THese should probalby only be getters
//        static public IElementPrototypeLibrary Library { get; set; }

        static public Dictionary<DepictionIconPath,ImageSource> DefaultIconDictionary
        {
            get
            {
                var outDict = new Dictionary<DepictionIconPath, ImageSource>();
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    var merged = Application.Current.Resources.MergedDictionaries;
                    if (merged != null)
                    {
                        
                        foreach (var dict in merged)
                        {
                            if (dict.Source != null && dict.Source.ToString().Contains("ElementIconDictionary"))
                            {
                                foreach (DictionaryEntry entry in dict)
                                {
                                    var bitmapSource = (ImageSource)entry.Value;
                                    var newKey = new DepictionIconPath(IconPathSource.EmbeddedResource,(string) (entry.Key));
                                    outDict.Add(newKey, bitmapSource);
                                }
                            }
                        }
                    }
                } 
                return outDict;
            }
        }
        static public Dictionary<DepictionIconPath, ImageSource> UserImageDictionary
        {
            get
            {
                var outDict = new Dictionary<DepictionIconPath, ImageSource>();
                if (Application.Current != null)
                {
                    var app = Application.Current as IDepictionApplication;
                    if (app != null)
                    {
                        var currentDepiction = app.CurrentDepiction;
                        if (currentDepiction != null)
                        {
                            var dict = currentDepiction.ImageResources;
                            foreach (DictionaryEntry entry in dict)
                            {
                                var bitmapSource = (ImageSource) entry.Value;
                                var newKey = new DepictionIconPath(IconPathSource.File, (string) (entry.Key));
                                outDict.Add(newKey, bitmapSource);
                            }
                        }
                        var appDict = app.ImageResources;
                        foreach (DictionaryEntry entry in appDict)
                        {
                            var bitmapSource = (ImageSource)entry.Value;
                            var newKey = new DepictionIconPath(IconPathSource.File, (string)(entry.Key));
                            outDict.Add(newKey, bitmapSource);  
                        }
                    }
                }
                return outDict;
            }
        }
      
        //HMMM is this really going to be needed in the future?
        public static DepictionReadableFileTypes GetFileType(string filename)
        {
            var extension = Path.GetExtension(filename).ToLowerInvariant();
            switch (extension)
            {
                case ".gml":
                    return DepictionReadableFileTypes.GML;
                case ".shp":
                    return DepictionReadableFileTypes.Shapefile;
                case ".gpx":
                    return DepictionReadableFileTypes.GPX;
                case ".kml":
                    return DepictionReadableFileTypes.KML;
                case ".kmz":
                    return DepictionReadableFileTypes.KMZ;
                default:
                    return DepictionReadableFileTypes.Unknown;
            }
        }
    }
}
