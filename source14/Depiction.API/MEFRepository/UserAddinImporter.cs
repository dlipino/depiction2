using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using Depiction.APINew.AddinObjects.CSE490Addins;

namespace Depiction.APINew.MEFRepository
{
    public class UserAddinImporter
    {
        [ImportMany(typeof(IDepictionExampleAddin))]
        private Lazy<IDepictionExampleAddin, IDepictionExampleAddinMetadata>[] LazyUserAddins { get; set; }
        public Dictionary<IDepictionExampleAddinMetadata, IDepictionExampleAddin> UserAddins { get; private set; }

        public void ImportUserAddins()
        {
            if(DepictionAccess.PathService == null) return;

            var userAddinDir = DepictionAccess.PathService.UserInstalledAddinsDirectoryPath;
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(userAddinDir));
            foreach (var folder in Directory.GetDirectories(userAddinDir, "*", SearchOption.AllDirectories))
                catalog.Catalogs.Add(new DirectoryCatalog(folder));
            var container = new CompositionContainer(catalog);

            container.ComposeParts(this);

            UserAddins = new Dictionary<IDepictionExampleAddinMetadata, IDepictionExampleAddin>();
            foreach (var behavior in LazyUserAddins)
            {
                UserAddins.Add(behavior.Metadata, behavior.Value);
            }
        }

    }
}