﻿using CarbonTools.Content.Features;

namespace Depiction.API.Interfaces.WebDataInterfaces
{
    //This is legacy tech
    public class Response
    {
        public string ResponseFile { get; set; }

        public bool IsRequestSuccessful { get; set; }

        public bool IsRequestCanceled { get; set; }

        public DataFeatures DataFeatures { get; set; }
    }
}
