using System.Windows;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.GeoTypeInterfaces
{
    /// <summary>
    /// Converts the pixel coordinates to geo coordinates and vice versa.
    /// </summary>
    public interface IGeoConverter
    {
        /// <summary>
        /// Worlds to geo canvas.
        /// </summary>
        /// <param name="latLon">The p.</param>
        /// <returns></returns>
        Point WorldToGeoCanvas(ILatitudeLongitude latLon);

        Rect WorldToGeoCanvas(IMapCoordinateBounds mapBounds);
        /// <summary>
        /// Geoes the canvas to world.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        ILatitudeLongitude GeoCanvasToWorld(Point p);

        IMapCoordinateBounds GeoCanvasToWorld(Rect rect);

        double Offsetx { get; }
        double Offsety { get; }
    }
}