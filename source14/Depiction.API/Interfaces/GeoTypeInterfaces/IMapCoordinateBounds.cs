﻿using System.Windows;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ValueTypes;

namespace Depiction.API.Interfaces.GeoTypeInterfaces
{
    public interface IMapCoordinateBounds
    {
        double Top { get; set; }
        double Bottom { get; set; }
        double Right { get; set; }
        double Left { get; set; }
        Size MapImageSize { get; set; }//This shouldn't go here because it deals with the pixel size which can vary from comp to comp
        ILatitudeLongitude TopRight { get; }
        ILatitudeLongitude TopLeft { get; }
        ILatitudeLongitude BottomLeft { get; }
        ILatitudeLongitude BottomRight { get; }
        ILatitudeLongitude Center { get; }
        bool Contains(ILatitudeLongitude location);
        bool IsValid { get; }
        double Height { get; }
        double Width { get; }
        double GetMidLineHorizontalDistance(MeasurementSystem system, MeasurementScale scale);
        double GetMidLineVerticalDistance(MeasurementSystem system, MeasurementScale scale);
        Ordinate LongestAxis();
        bool Intersects(IMapCoordinateBounds other);
    }
}