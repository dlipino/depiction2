﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.API.Interfaces.GeoTypeInterfaces
{
    [TypeConverter("Depiction.API.ValueTypeConverters.LatitudeLongitudeTypeConverter")]
    public interface ILatitudeLongitude : IDeepCloneable<ILatitudeLongitude>
    {
        double Latitude { get; }
        double Longitude { get; }

        bool IsValid { get; }
        string ToXmlSaveString();
        double this[Ordinate longaxis] { get; }
        ILatitudeLongitude Subtract(ILatitudeLongitude summand);
        ILatitudeLongitude Add(ILatitudeLongitude summand);
    }
    [TypeConverter("Depiction.API.ValueTypeConverters.LatitudeLongitudeTypeConverter")]
    public class LatitudeLongitudeBase : ILatitudeLongitude
    {
        public const double INVALID_VALUE = double.NaN;

        #region Implementation of ILatitudeLongitude

        public double Latitude { get; protected set; }
        public double Longitude { get; protected set; }

        public bool IsValid { get { return (!Latitude.Equals(INVALID_VALUE) && !Longitude.Equals(INVALID_VALUE)); } }

        #endregion

        protected LatitudeLongitudeBase()
        {
            Latitude = INVALID_VALUE;
            Longitude = INVALID_VALUE;
        }
        public LatitudeLongitudeBase(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
        public string ToXmlSaveString()
        {
            return Latitude.ToString("R", CultureInfo.InvariantCulture) + "," +
                   Longitude.ToString("R", CultureInfo.InvariantCulture);
        }

        public double this[Ordinate longaxis]
        {
            get
            {
                if (longaxis.Equals(Ordinate.X))
                    return Longitude;
                if (longaxis.Equals(Ordinate.Y))
                    return Latitude;
                return double.NaN;
            }
        }

        public ILatitudeLongitude Subtract(ILatitudeLongitude summand)
        {
            return summand == null
                ? this
                : this.Add(new LatitudeLongitude(-summand.Latitude, -summand.Longitude));
        }
        
        public ILatitudeLongitude Add(ILatitudeLongitude summand)
        {
            //if (self == null && summand == null)
            //    return null;
            if (summand == null)
                return this;
            //if (self == null)
            //    return summand;

            // for now we only care for 2D
            return new LatitudeLongitude(Latitude + summand.Latitude, Longitude + summand.Longitude);
        }
        protected bool ToleranceEquals(double start, double other)
        {
            if (double.IsNaN(start) && double.IsNaN(other)) return true;
            if (double.IsNaN(start) && !double.IsNaN(other)) return false;
            if (!double.IsNaN(start) && double.IsNaN(other)) return false;
            double difference = Math.Abs(start * tolerance);
            if (Math.Abs(start - other) <= difference) return true;
            return false;
        }

        private const double tolerance = .0000001;//Can't remeber how many decimal places until tolerances is about 1 inch

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as LatitudeLongitudeBase;
            if (other == null) return false;
            if (!ToleranceEquals(Latitude, other.Latitude)) return false;
            if (!ToleranceEquals(Longitude, other.Longitude)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = Latitude.GetHashCode() >> 3;
            hashCode ^= Longitude.GetHashCode();
            return hashCode;
        }
        #region deepcloning, which isn'treally used?
        virtual public ILatitudeLongitude DeepClone()
        {
            return new LatitudeLongitudeBase(Latitude, Longitude);
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        #endregion

        public double Distance(LatitudeLongitude other)
        {
            ICoordinate startCoordinate = new Coordinate(Longitude, Latitude);
            ICoordinate endCoordinate = new Coordinate(other.Longitude, other.Latitude);
            return startCoordinate.Distance(endCoordinate);
        }
    }
}