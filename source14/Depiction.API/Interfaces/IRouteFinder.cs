﻿using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.API.Interfaces
{
    /// <summary>
    /// Contract to find the optimized route.
    /// </summary>
    public interface IRouteFinder
    {
        /// <summary>
        /// Find the route based on the given road network nodes
        /// </summary>
        /// <param name="waypointList">The nodes representing various waypoints along the route</param>
        /// <param name="route"></param>
        /// <param name="hasFreeformSegments">Indicates whether or not the route is entirely connected by the road network</param>
        /// <param name="estimatedTime"></param>
        /// <returns>Coordinates representing the lowest cost route path based on the given waypoints</returns>
        IList<RoadSegment> FindRoute(IEnumerable<RoadNode> waypointList, IDepictionElement route, out bool hasFreeformSegments, out double? estimatedTime);
        

    }
}
