﻿using System;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;

namespace Depiction.API.Interfaces
{
    public interface ITileProvider : IDepictionDefaultImporter
    {
        event Action<TileModel> BackgroundTileReceived;
        event Action<string> TileServiceInformation;
        string DisplayName { get; }
        string SourceName { get; }
        bool ReplaceCachedImages { get; }
        TileImageTypes TileImageType { get; }

        void RetrieveTiles(IMapCoordinateBounds boundingBox);
        string CreateImageFromTilesInBounds(IMapCoordinateBounds boundingBox);
        void CancelRequest();
    }
}
