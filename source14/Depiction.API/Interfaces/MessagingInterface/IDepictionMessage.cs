using System;

namespace Depiction.API.Interfaces.MessagingInterface
{
    public interface IDepictionMessage
    {
        string MessageId { get; }
        DepictionMessageType MessageType { get; }
        double MessageDisplayTime { get; }
        string Message { get; }
        DateTime MessageDateTime { get; }
    }
}