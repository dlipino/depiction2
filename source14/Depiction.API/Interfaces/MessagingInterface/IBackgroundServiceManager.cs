using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Depiction.API.Interfaces.MessagingInterface
{
    public interface IBackgroundServiceManager
    {
        ReadOnlyCollection<IDepictionBackgroundService> CurrentServices { get; }
        event NotifyCollectionChangedEventHandler BackgroundServicesModified;
        void AddBackgroundService(IDepictionBackgroundService service);
        void CancelAllServices();
        void ResetServiceManager();
    }
}