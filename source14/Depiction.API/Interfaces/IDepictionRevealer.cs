﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces
{
    public interface IDepictionRevealer : IDepictionBaseDisplayer
    {
        IMapCoordinateBounds RevealerBounds { get; set; }
        ReadOnlyCollection<IRevealerProperty> RevealerProperties { get; }
        List<IDepictionElement> GetDisplayedElements();
        bool GetPropertyValue<T>(string propName, out T value);
        void AddProperty<T>(string propertyName, T val);
        bool SetPropertyValue(string propertyName, object value);
        bool SetPropertyValue(string propertyName, object value,bool overwriteType);
    }
}
