﻿using System.Collections.Generic;

namespace Depiction.API.Interfaces
{
    public interface IQuickstartItem
    {
        string Name { get; set; }
        string AddinName { get; set; }
        string Description { get; set; }
        Dictionary<string, string> Parameters { get; set; }
        string ElementType { get; }
    }
}
