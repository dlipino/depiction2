﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces
{
    public interface IDepictionBaseDisplayer
    {
        event NotifyCollectionChangedEventHandler VisibleElementsChange;

        string DisplayerName { get; set; }
        string DisplayerID { get; }

        IDepictionBaseDisplayer BaseDisplayer { get; }
        
        DepictionDisplayerType DisplayerType { get; set; }
        
        ReadOnlyCollection<string> ElementIdsInDisplayer { get; }

        bool AddElementList(IEnumerable<IDepictionElement> element);
        bool RemoveElementList(List<IDepictionElement> element);
        bool IsElementPresent(string elementKey);
        string VisibleElementCount();
    }
}
