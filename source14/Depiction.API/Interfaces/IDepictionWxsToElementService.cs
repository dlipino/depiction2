﻿using System.Collections.Generic;
using CarbonTools.Content.OGC.Capabilities;

namespace Depiction.API.Interfaces
{
    public interface IDepictionWxsToElementService
    {
        string WebServiceType { get; set; }
        string ServiceURL { get; set; }
        string LayerNames { get; set; }
        string FirstLayerTitle { get; set; }
        string Style { get; set; }
        string ImageFormat { get; set; }
        void ClearAdditionalInfo();
        void ResetOGCData();

        /// <summary>
        /// Obtains GetCapability of the serverUrl passed
        /// Also sets the serverOnlineAddress string to be the OnlineResource
        /// address contained in the GetCapabilities document
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="serverType"></param>
        /// <returns>list of layer names contained in the server</returns>
        Dictionary<string, string> GetLayerNamesFromServer(string serverUrl, string serverType);

        List<string> GetMapStylesFromLayer(string layerName);

        ///
        List<string> GetFormatsFromLayer(string layerName);

        void CancelCababiliesSearch();
        HandlerOGCCapabilities GetOGCCapabilities(SourceOGCCapabilities ogcCapSource);
    }
}
