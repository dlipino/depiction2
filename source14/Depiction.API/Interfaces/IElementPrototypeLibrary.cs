﻿using System;
using System.Collections.ObjectModel;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces
{
    public interface IElementPrototypeLibrary
    {
        event Action PrototypesUpdated;
        ReadOnlyCollection<IElementPrototype> DefaultPrototypes { get; }
        ReadOnlyCollection<IElementPrototype> UserPrototypes { get; }
        ReadOnlyCollection<IElementPrototype> LoadedDepictionPrototypes { get; }
        ReadOnlyCollection<IElementPrototype> AllDepictionDefinitions { get; }
        int PrototypeCount { get; }
        //For a race condition
        bool DefaultsSet { get; }
        bool UsersSet { get; }
        bool LoadedSet { get; }

        void SetDefaultPrototypesFromPath(string path,bool notifyOfChange);
        void SetUserPrototypesFromPath(string path, bool notifyOfChange);
        void SetLoadedPrototypesFromPath(string path, bool notifyOfChange);//Not sure when this is used, i think it for 122 loading/converting
        void SetAddinPrototypesAndIcons(bool notifyOfChanges);
        void NotifyPrototypeLibraryChange();
        bool CanGuessPrototypeFromString(string name);
        IElementPrototype GuessPrototypeFromString(string name);
        IElementPrototype GetPrototypeByFullTypeName(string type);
        IElementPrototype GetPrototypeByTypeDisplayName(string type);

        void SaveElementPrototypeLibraryToPath(string startPath,bool saveOnlyElementsInCurrentDepiction);
        void UpdateElementPrototypeLibraryFromTempLoadedDpnLocation(string baseTempDpnLocation);//This one is used for dpn loading
        bool DeleteElementPrototypeFromLibrary(IElementPrototype prototypeToDelete);
        bool AddElementPrototypeToLibrary(IElementPrototype prototypeToAdd);
        bool AddElementPrototypeToLibrary(IElementPrototype prototypeToAdd,bool replaceIfExists);
        IElementPrototype GetPrototypeFromAutoDetect(string elementType, string shapeType);

    }
}
