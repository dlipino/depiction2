using System.Collections.Generic;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;

namespace Depiction.API.Interfaces
{
    public interface IUTMTiler : ITiler
    {
    }

    public interface ITiler
    {
        int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel);
        IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel);
        TileImageTypes TileImageType { get; }
        string DisplayName { get; }
        string SourceName { get; }
        string LegacyImporterName { get; }
        int PixelWidth { get; }
        bool DoesOwnCaching { get; }
        int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom);
        int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom);
        double TileColToTopLeftLong(int col, int zoomLevel);
        double TileRowToTopLeftLat(int row, int zoom);
        TileModel GetTileModel(TileXY tileToGet, int zoomLevel);
    }

    public struct TileXY
    {
        public int Column { get; set; }
        public int Row { get; set; }
        public int UTMZoneNumber { get; set; }//sadface
    }
}