﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces
{
    public interface IDepictionStory
    {
        event NotifyCollectionChangedEventHandler RevealerListChange;
        event NotifyCollectionChangedEventHandler AnnotationListChange;
        event NotifyCollectionChangedEventHandler ElementListChange;

        event Action<IEnumerable<string>, DepictionDialogType[]> RequestedElementsInDialogType;
        event Action<IMapCoordinateBounds, IMapCoordinateBounds> DepictionViewportChange;//<Old,New,notifyviewmodel>
        event Action<IMapCoordinateBounds, IMapCoordinateBounds> DepictionRegionChange;//<Old,New>
        event Func<IMapCoordinateBounds, IMapCoordinateBounds> DepictionViewportChangeRequested;//requested,what can be displayed, connected to the view
        IElementRepository CompleteElementRepository { get; }
        /// <summary>
        /// Provides access to the repository of interactions.
        /// </summary>
        IInteractionRuleRepository InteractionRuleRepository { get; set; }

        IDepictionBaseDisplayer MainMap { get; }

        ReadOnlyCollection<IDepictionRevealer> Revealers { get; }

        ReadOnlyCollection<IDepictionAnnotation> DepictionAnnotations { get; }

        bool DepictionNeedsSaving { get; set; }
        string DepictionsFileName { get; set; }

        IDepictionStoryMetadata Metadata { get; }
        IDepictionGeographicInfo DepictionGeographyInfo { get; }

        IMapCoordinateBounds ViewportBounds { get; }
        IMapCoordinateBounds RegionBounds { get; set; }
        RasterImageResourceDictionary ImageResources { get; }

        void RequestVisibleViewportBounds(IMapCoordinateBounds newBounds);
        void UpdateModelViewportBounds(IMapCoordinateBounds newBounds);

        List<IDepictionElement> GetElementsWithIds(IEnumerable<string> elementIds);

        void AddRevealer(IDepictionRevealer revealer);
        void DeleteRevealer(IDepictionRevealer revealer);

        void AddElementGroupToDepictionElementList(IEnumerable<IDepictionElement> elements);
        void AddElementGroupToDepictionElementList(IEnumerable<IDepictionElement> elements, bool addToMainMap);
        void AddElementToDepictionElementList(IDepictionElement element, bool addToMainMap);

        //List<string> CreateOrUpdateDepictionElementListFromPrototypeList(IEnumerable<IElementPrototype> elementPrototypes, bool addToMainMap);
        List<string> CreateOrUpdateDepictionElementListFromPrototypeList(IEnumerable<IElementPrototype> elementPrototypes, bool addToMainMap,bool markAsUpdated);
        void CreateOrUpdateDepictionElementListFromPrototypeList(IEnumerable<IElementPrototype> elementPrototypes, out List<IDepictionElement> createdElements,
                out List<IDepictionElement> updateElements , bool addToMainMap, bool markAsUpdated);

        bool DeleteElementsWithIdsFromDepictionElementList(List<string> elementIds);
        void RequestElementsWithIdsViewing(IEnumerable<string> elementIds,DepictionDialogType[] dialogs);

        void AddElementListToMainMap(IEnumerable<IDepictionElement> elements);
        void RemoveElementListFromMainMap(List<IDepictionElement> element);

        void AddAnnotation(IDepictionAnnotation annotation);
        void RemoveAnnotation(IDepictionAnnotation annotation);


    }
}
