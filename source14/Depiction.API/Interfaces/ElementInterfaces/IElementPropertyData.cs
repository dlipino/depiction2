namespace Depiction.APINew.Interfaces.ElementInterfaces
{
    public interface IElementPropertyData : IElementPropertyBase,IDeepCloneable<IElementPropertyData>
    {
        //This class is here just to make sure that IElementProperty has something that is readonly
        void SetInitialValue(object restoreValue);
    }
}