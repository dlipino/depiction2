﻿using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IDepictionImageMetadata: IDeepCloneable<IDepictionImageMetadata>
    {
        DepictionIconPath DepictionImageName { get; }
        string ImageFilename { get; set; }//should this be a depiction specific name?
        double TopLeftLatitude { get; set; }

        double TopLeftLongitude { get; set; }
        double BottomRightLatitude { get; set; }
        double BottomRightLongitude { get; set; }
        double RotationDegrees { get; set; }
        bool IsGeoReferenced { get; }
        bool CanBeManuallyGeoAligned { get; }
        IMapCoordinateBounds GeoBounds { get; }
    }
}