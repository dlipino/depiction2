﻿using System;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    /// <summary>
    /// An element property.
    /// </summary>
    public interface IElementProperty : IElementPropertyBase, IDeepCloneable<IElementProperty>
    {
        #region attributes
        bool IsHoverText { get; set; }
        #endregion

        int Rank { get; set; }
       
        DateTime LastModified { get; }

        PropertySource PropertySource { get; set; }
    }
}