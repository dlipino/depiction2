﻿using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IZoneOfInfluence : IXmlSerializable
    {
        IDepictionElementBase Owner { get; }
        bool IsEmpty { get; }
        DepictionGeometryType DepictionGeometryType { get; }
        IDepictionGeometry Geometry { get; }
        ILatitudeLongitude[] GetVertices();
        
        //NOt good, 
        void ShiftZoneOfInfluence(ILatitudeLongitude translateDistance);
    }
}