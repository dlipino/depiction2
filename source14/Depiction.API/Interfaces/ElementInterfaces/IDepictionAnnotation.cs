﻿using System.Xml.Serialization;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IDepictionAnnotation : IXmlSerializable, IBaseDepictionMapType
    {
        ILatitudeLongitude MapLocation { get; set; }
        string AnnotationID { get; }
        string AnnotationText { get; set; }
        //All in pixel sizes
        double ContentLocationX { get; set; }
        double ContentLocationY { get; set; }
        double PixelWidth { get; set; }
        double PixelHeight { get; set; }
        double ScaleWhenCreated { get; set; }
        double IconSize { get; }

        bool IsAnnotationVisible { get; }
        bool IsTextCollapsed { get; set; }
        string BackgroundColor { get; set; }
        string ForegroundColor { get; set; }
    }
}