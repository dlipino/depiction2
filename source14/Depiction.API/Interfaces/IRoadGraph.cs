﻿using System;
using System.Collections.ObjectModel;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using QuickGraph;

namespace Depiction.API.Interfaces
{
    public interface IRoadGraph
    {
        AdjacencyGraph<RoadNode, RoadSegment> Graph { get; }
        void DisableEdge(RoadSegment edge);
        RoadNode FindNearestNode(RoadNode inCoord);
        RoadNode FindNearestNode(ILatitudeLongitude inCoord);
        IRouteFinder RouteFinder { get; }
        bool ContainsVertex(RoadNode vertex);
        double GetEdgeWeight(RoadSegment edge);
        Func<RoadSegment, double> GetDriveTimeFunc(IDepictionElement route);
        ReadOnlyCollection<RoadSegment> DisabledEdges { get; }
    }
}
