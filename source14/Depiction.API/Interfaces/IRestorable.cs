﻿namespace Depiction.API.Interfaces
{
    public interface IRestorable
    {
        //void Restore();
        void Restore(bool notifyPropertyChange);
    }
}
