﻿using System.IO;

namespace Depiction.API.Interfaces.DepictionTypeInterfaces
{
    //The locations for this class are mostly all wrong, but time has not been spent to fix it.
    public interface IApplicationPathService
    {
        /// <summary>
        /// Gets the plugin directory path, 
        /// e.g. C:\Users\username\AppData\Local\Depiction\Elements
        /// </summary>
        string UserElementsDirectoryPath
        {
            get;
        }

        /// <summary>
        /// Gets the plugin directory path,
        /// e.g. C:\Users\username\AppData\Local\Depiction\UserAddins.
        /// </summary>
        string UserInstalledAddinsDirectoryPath
        {
            get;
        }

        /// <summary>
        /// The path to the directory that Depiction uses for storing Images, such as images used for element icons,
        /// e.g. C:\Users\username\AppData\Local\Depiction\ElementIcons.
        /// </summary>
        string UserElementIconDirectoryPath
        {
            get;
        }

        /// <summary>
        /// Gets the user interaction rules directory path,
        /// e.g. C:\Users\username\AppData\Local\Depiction\InteractionRules.
        /// </summary>
        string UserInteractionRulesDirectoryPath
        {
            get;
        }

//        /// <summary>
//        /// Gets the interaction rules file name.  For the initial implementation, 
//        /// all user rules are stored in a single known file.  This obviates 
//        /// managing separate files when modifying and saving user rules.
//        /// </summary>
//        /// <value>The interaction rules file name.</value>
//        string UserInteractionRulesFileName
//        {
//            get;
//        }

        /// <summary>
        /// Gets the temporary folder path for this instance of Depiction, 
        /// e.g. C:\Users\username\AppData\Local\Temp\Depiction\fe5f196d-a1b4-4df6-80d8-58de55283558.
        /// </summary>
        string FolderInTempFolderRootPath
        {
            get;
        }

        /// <summary>
        /// The path to the temporary directory that Depiction uses for storing user-defined elements
        /// for this session of Depiction,
        /// e.g. C:\Users\username\AppData\Local\Temp\Depiction\fe5f196d-a1b4-4df6-80d8-58de55283558\Elements.
        /// </summary>
        string DepictionElementPath
        {
            get;
        }

        string DepictionAddinPath
        { get;
        }

        /// <summary>
        /// The path to the directory where third-party providers may place DML files for use by Depiction,
        /// e,g, C:\Program Files\Common Files\Depiction\Elements.
        /// </summary>
        string ReadOnlyAddInElementsPath
        {
            get;
        }

        /// <summary>
        /// The path of the directory that will contain user-defined elements, 
        /// interaction rules, etc., e.g. C:\Users\username\AppData\Local\Depiction\Depiction.
        /// </summary>
        string AppDataDirectoryPath { get; }

        string DepictionCacheDirectory { get; }

        /// <summary>
        /// A unique temporary folder created for this instance of Depiction.
        /// Deleted when this instance of Depiction exits,
        /// e.g. C:\Users\username\AppData\Local\Temp\Depiction.
        /// </summary>
        string TempFolderRootPath { get; }


        /// <summary>
        /// Gets the user data path,
        /// e.g. C:\Users\username\AppData\Local\Depiction.
        /// </summary>
        /// <returns></returns>
        string GetUserDataPath();

        /// <summary>
        /// Will try to create this directory, if it doesn't exist.
        /// Returns the directory if found or could create it.
        /// Returns null if cannot find or create it.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        string TryThisDirectory(string directory);

        void RemoveInstanceTempFolder();
        string RetrieveCachedFilePathIfCached(string fileName);
        void DeleteCachedFile(string fileName);
        string CacheFile(Stream buffer, string fileName);
        void CacheTerrainFile(string filename, string key);
    }
}