﻿using System.Collections.Generic;
using Depiction.APINew.OldInterfaces;
using Depiction.APINew.ValueTypeInterfaces;
using GeoAPI.Geometries;
using Point = System.Windows.Point;


namespace Depiction.APINew.TerrainObjects
{
    public class SimpleFloodModel
    {
        public static IGridSpatialData GenerateSimpleFloodGridFromTerrain(ITerrain terrain, ILatitudeLongitude seedPoint, double metersAboveSeaLevel)
        {
            var elevationOfFlood = metersAboveSeaLevel;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;

            int gridWidth = terrain.GetGridWidthInPixels();
            int gridHeight = terrain.GetGridHeightInPixels();
            var topLeft = terrain.GetTopLeftPosition();
            var bottomRight = terrain.GetBottomRightPosition();

            var surfGrid = new GridSpatialData(gridWidth, gridHeight, topLeft, bottomRight);

            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    double elev = terrain.GetValueAtGridCoordinate(i, j);
                    if (elev < elevationOfFlood)
                        surfGrid.SetZ(i, j, 1);
                }
            }
            return surfGrid;
        }

        // void Coverage::FloodContours(double startX, double startY, double offset, double horizontalResolution, System::Collections::ArrayList^ cList)
        //{

        //    if(!m_pGrid->HasData())
        //    {
        //        return ;
        //    }

        //    //get the current corner values
        //    GetCurrentCornersAndHeightExtents();
        //    if(!m_pGrid->HasData())
        //    {
        //        return ;
        //    }

        //    if(m_surfaceGrid==NULL || needRefreshing)
        //    {
        //        GenerateQuikGridFromVTPGrid(m_pGrid,true);
        //        needRefreshing=false;
        //    }

        //    //remove
        //    //m_pGrid->SaveToGeoTIFF("C:\\downloads\\elevation.tif");
        //    ThresholdQuikGrid(offset, horizontalResolution);

        //    Contour(*m_thresholdedGrid,0, cList);

        //    //
        //    //convert image/grid coordinates to latlon
        //    //
        //    ConvertImageToWorldCoordinates(cList, 1);

        //}


        public static IDepictionSpatialData GenerateSimpleFloodGeometryFromTerrain(ITerrain terrain, ILatitudeLongitude seedPoint, double metersAboveSeaLevel)
        {
            var elevationOfFlood = metersAboveSeaLevel;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;


            var point = GetXYFromLatLon(terrain.GetTopLeftPosition(), terrain.GetBottomRightPosition(), terrain.GetGridWidthInPixels(), terrain.GetGridHeightInPixels(), seedPoint);
            var sGrid = FloodFillService.QuickFill(terrain, point, elevationOfFlood);
            var surfGrid = sGrid.ExpandByOnePixel();

            //surfGrid.GimmeANegative(3);
            //var bitmap = surfGrid.GenerateBitmap();

            //BitmapSaveToFileHelper.SaveBitmap(bitmap, @"c:\temp\testbitmap.png");
            var geometry = GenerateSimpleFloodFromSurfaceGrid(surfGrid);
            //if (geometry.IsValid)
            return new DepictionGeometry(geometry);

            return null;
        }

        public static IGeometry GenerateSimpleFloodFromSurfaceGrid(IGridSpatialData surfGrid)
        {
            //surfGrid.GimmeANegative(3);
            var contours = ContourClass.Contour(surfGrid, 0);
            var reduced = ReducePoints(contours);
            var worldContours = ConvertImageToWorldCoordinates(surfGrid, reduced, 1);
            IPolygon geometry = GeoProcessor2.ConvertToPolygon(worldContours);
            geometry.Buffer(0.000001);
            return geometry;

        }


        private static Point GetXYFromLatLon(ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight, int pixWidth, int pixHeight, ILatitudeLongitude latlon)
        {
            double latHeight = topLeft.Latitude - bottomRight.Latitude;
            double lonWidth = bottomRight.Longitude - topLeft.Longitude;
            double lon = latlon.Longitude - topLeft.Longitude;
            double lat = latlon.Latitude - bottomRight.Latitude;

            var x = (int)(lon * pixWidth / lonWidth);
            var y = (int)(lat * pixHeight / latHeight);
            return new Point(x, y);
        }

        public static IList<List<Point>> ReducePoints(IList<List<Point>> pointList)
        {
            var newPoints = new List<List<Point>>();
            foreach (var points in pointList)
            {
                newPoints.Add(ReducePointsFrom(points));
            }
            return newPoints;
        }
        private enum direction { none, up, down, left, right };

        public static List<Point> ReducePointsFrom(List<Point> points)
        {
            var newPoints = new List<Point>();
            Point lastPoint = new Point(double.MaxValue, double.MaxValue);
            var firstTime = true;
            direction dir = direction.none;
            foreach (var point in points)
            {

                var lastDir = dir;
                if (firstTime)
                {
                    firstTime = false;
                    lastPoint = point;
                    continue;
                }

                if (point.X == lastPoint.X)
                {
                    if (point.Y > lastPoint.Y)
                        dir = direction.up;
                    else if (point.Y < lastPoint.Y)
                        dir = direction.down;
                }
                else if (point.Y == lastPoint.Y)
                {
                    if (point.X > lastPoint.X)
                        dir = direction.left;
                    else if (point.X < lastPoint.X)
                        dir = direction.right;
                }

                var dirchanged = (dir != lastDir);
                if (dirchanged)
                {
                    newPoints.Add(lastPoint);
                }
                lastPoint = point;
            }
            if (lastPoint.X != double.MaxValue)
                newPoints.Add(lastPoint);
            return newPoints;
        }


        private static ILatitudeLongitude GetWorldCoordinates(IGridSpatialData grid, int col, int row)
        {
            double lon, lat;
            double heightLat = grid.HeightLat;
            double widthLon = grid.WidthLon;
            lon = (double)col / (grid.PixelWidth - 1) * widthLon + grid.TopLeft.Longitude;
            lat = grid.BottomRight.Latitude + (double)row / (grid.PixelHeight - 1) * heightLat;
            return new LatitudeLongitude(lat, lon);
        }


        public static IList<List<ILatitudeLongitude>> ConvertImageToWorldCoordinates(IGridSpatialData grid, IList<List<Point>> cList, int offset)
        {
            var worldCoordinates = new List<List<ILatitudeLongitude>>();
            double x, y;
            double currentX = -10000, currentY = -10000;

            for (int i = 0; i < cList.Count; i++)
            {
                var aList = cList[i];
                var nList = new List<ILatitudeLongitude>();
                worldCoordinates.Add(nList);
                for (int j = 0; j < aList.Count; j++)
                {
                    x = aList[j].X - offset;
                    y = aList[j].Y - offset;
                    if (x != currentX || y != currentY)
                    {
                        currentX = x;
                        currentY = y;
                        nList.Add(GetWorldCoordinates(grid, (int)x, (int)y));
                    }
                    else
                    {
                        j--;
                    }
                }

            }

            return worldCoordinates;

        }










    }
}