// ItemPropertyChangedEventArgs.cs by Charles Petzold, December 2008, so totally copied. Hopefully improvements will
//be made by time RC is reached. Also, so old.
using System.ComponentModel;

namespace Depiction.API.EnhancedTypes
{
    public class ItemPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        object item;

        public ItemPropertyChangedEventArgs(object item,
                                            string propertyName)
            : base(propertyName)
        {
            this.item = item;
        }

        public object Item
        {
            get { return item; }
        }
    }

    public delegate void ItemPropertyChangedEventHandler(object sender,
                                                         ItemPropertyChangedEventArgs args);
}