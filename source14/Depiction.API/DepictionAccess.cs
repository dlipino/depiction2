using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;
using Depiction.API.AbstractObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.MessagingInterface;
using Depiction.API.Service;
using GisSharpBlog.NetTopologySuite.Geometries;


[assembly: InternalsVisibleTo("Depiction")]
[assembly: InternalsVisibleTo("Depiction.Main")]
[assembly: InternalsVisibleTo("DepictionRW")]
[assembly: InternalsVisibleTo("DepictionPrep")]
[assembly: InternalsVisibleTo("DepictionReader")]
[assembly: InternalsVisibleTo("Depiction.UnitTests")]

namespace Depiction.API
{
    public class DepictionAccess
    {
        //public const string AddonDirName = "Add-ons";

        static private IDepictionNotificationService notificationService = new DepictionMessageService();
        static private IBackgroundServiceManager backgroundService = new BackgroundServiceManager();
        private static InteractionsLibrary interactionsLibrary;
        private static IElementPrototypeLibrary elementLibrary;
        private static GeometryFactory geometryFactory;
        #region helper methods for background to UI thread,hmm not nsure if this is right,
        static public Dispatcher DepictionDispatcher
        {
            get
            {
                if (Application.Current != null)
                {
                    return Application.Current.Dispatcher;
                }
                return null;
            }
        }
        static public bool DispatchAvailableAndNeeded()
        {
            var dispatcher = DepictionDispatcher;
            if (dispatcher == null) return false;
            if (!dispatcher.CheckAccess()) return true;
            return false;
        }
        #endregion

        #region various services

        static public GeometryFactory DepictionGeomFactory
        {
            get
            {
                if (geometryFactory == null)
                {
                    geometryFactory = new GeometryFactory();
                }
                return geometryFactory;
            }
        }

        private static IDepictionWxsToElementService depictionWxsToElementService;
        static public IDepictionWxsToElementService DepictionWxsToElementService
        {
            get { return depictionWxsToElementService; }
            set { depictionWxsToElementService = value; }
        }

        static public IBackgroundServiceManager BackgroundServiceManager
        {
            get
            {
                if (backgroundService == null)
                {
                    backgroundService = new BackgroundServiceManager();
                }
                return backgroundService;
            }
        }
        static public IDepictionNotificationService NotificationService
        {
            get
            {
                if (notificationService == null)
                {
                    notificationService = new DepictionMessageService();
                }
                return notificationService;
            }
        }
        //Hack, can't create here yet because of circular dependencies.
        static public IElementPrototypeLibrary ElementLibrary
        {
            get
            {
                return elementLibrary;
            }
            set
            {
                if (value == null)//For reset
                {
                    elementLibrary = null;
                    return;
                }
                if (elementLibrary == null)
                {
                    elementLibrary = value;
                }
            }
        }

        static public IApplicationPathService PathService { get; set; }
        static public IDepictionGeocodingService GeoCodingService { get; set; }
        #endregion

        static public IDepictionStory CurrentDepiction
        {
            get
            {
                var currentApp = Application.Current as IDepictionApplication;
                if (currentApp == null) return null;
                return currentApp.CurrentDepiction;
            }
        }
        static public InteractionsLibrary InteractionsLibrary
        {
            get
            {
                if (interactionsLibrary == null)
                {
                    interactionsLibrary = new InteractionsLibrary();
                }
                return interactionsLibrary;
            }
        }

        static public Router InteractionsRunner { get; set; }
        static public IGeoConverter GeoCanvasToPixelCanvasConverter { get; set; }

        private static ProductInformationBase _productInformation = new ProductInformationDefault();


        static public ProductInformationBase ProductInformation
        {
            get
            {
                if (_productInformation == null)
                {
                }
                return _productInformation;
            }
            //This setter needs to be moved to an internal API setting mechanism not one that will be used by 3rd party developers
            internal set
            {
                if(value == null)
                {
                    _productInformation = new ProductInformationDefault();//For resetting tests
                }else
                {
                    _productInformation = value;
                }
            }
        }

    }
}