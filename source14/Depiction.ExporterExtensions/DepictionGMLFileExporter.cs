using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.TypeConverter;
using OSGeo.OGR;

namespace Depiction.ExporterExtensions
{
    [DepictionElementExporterMetadata("DepictionGMLFileExporter", new[] { ".gml" },
        FileTypeInformation = "GML file", DisplayName = "Depiction GML writer")]
    public class DepictionGMLFileExporter : IDepictionElementExporter
    {
        public void WriteElements(string fileName, IEnumerable<IDepictionElement> elementList, bool background)
        {
            var gmlwriter = new DepictionGMLWriterService();
            if (background)
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("FileName", fileName);
                parameters.Add("ElementList", elementList);
                var name = string.Format("Writing GML file : {0}", Path.GetFileName(fileName));
//                var message = "Geography Markup Language (GML) is an industry standard for exchanging information between different systems.\n\n" +
//                               "The GML file: \n" +
//                                "  - Might not retain some of the rich properties and behaviors of certain elements.\n" +
//                             "  - Cannot store Route, Elevation data or Image elements.\n\n" +
//                             "Do you want to continue exporting?";
//
//                var result = DepictionAccess.NotificationService.DisplayInquiryDialog(message, "Export element(s) to GML file",
//                                                                         MessageBoxButton.YesNo);
//                if (result.Equals(MessageBoxResult.No)) return;
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(gmlwriter);
                gmlwriter.UpdateStatusReport(name);
                gmlwriter.StartBackgroundService(parameters);
            }
            else
            {
                gmlwriter.DoWriteElements(fileName, elementList);
            }
        }
        public object AddonMainView
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }


        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }

        public string AddonConfigViewText { get { return null; } }

        public void ExportElements(object location, IEnumerable<IDepictionElement> elements)
        {
            if (location == null) return;
            var fileName = location.ToString();
            WriteElements(fileName, elements, true);
        }
    }

    public class DepictionGMLWriterService : BaseDepictionBackgroundThreadOperation
    {
        private string message = "";

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var fileName = parameters["FileName"].ToString();
            var elementList = parameters["ElementList"] as IEnumerable<IDepictionElement>;
            DoWriteElements(fileName, elementList);
            return null;
        }

        protected override void ServiceComplete(object args)
        {
        }

        #endregion

        #region public things

        public void DoWriteElements(string fileName, IEnumerable<IDepictionElement> elementList)
        {
            if (GetExportableElementCount(elementList) <= 0) return;


            Driver drv = Ogr.GetDriverByName("GML");
            DataSource ds = drv.CreateDataSource(fileName, new string[] { });

            message = "Writing line string layer 1/3";
            UpdateStatusReport(message);
            var lineStringLayer = CreateLayer(wkbGeometryType.wkbLineString, ds, "lineStringLayer");
            AddElementsToLayer(lineStringLayer, DepictionGeometryType.LineString, elementList);
            AddElementsToLayer(lineStringLayer, DepictionGeometryType.MultiLineString, elementList);

            message = "Writing line polygon layer 2/3";
            UpdateStatusReport(message);
            var polygonLayer = CreateLayer(wkbGeometryType.wkbPolygon, ds, "polygonLayer");
            AddElementsToLayer(polygonLayer, DepictionGeometryType.Polygon, elementList);
            AddElementsToLayer(polygonLayer, DepictionGeometryType.MultiPolygon, elementList);

            message = "Writing line point layer 3/3";
            UpdateStatusReport(message);
            var pointLayer = CreateLayer(wkbGeometryType.wkbPoint, ds, "pointLayer");
            AddElementsToLayer(pointLayer, DepictionGeometryType.Point, elementList);
            AddElementsToLayer(pointLayer, DepictionGeometryType.MultiPoint, elementList);
            ds.Dispose();
            if (ServiceStopRequested)
            {//I think this deletes all the associated gml files
                drv.DeleteDataSource(fileName);
            }
        }

        #endregion


        #region Older things


        private Layer CreateLayer(wkbGeometryType geomType, DataSource ds, string layerName)
        {
            Layer layer;
            layer = ds.CreateLayer(layerName, null, geomType, new string[] { });

            AddFieldDefToLayer(layer, "ElementType", 32);
            AddFieldDefToLayer(layer, "ElementKey", 32);
            AddFieldDefToLayer(layer, "ZOIBorder", 17);
            AddFieldDefToLayer(layer, "ZOIFill", 17);
            //            AddFieldDefToLayer(layer, "StrokeColor", 17);
            //            AddFieldDefToLayer(layer, "FillColor", 17);
            return layer;
        }
        private void AddFieldDefToLayer(Layer layer, string fieldName, int fieldWidth)
        {
            var fdefn = new FieldDefn(fieldName, FieldType.OFTString);
            fdefn.SetWidth(fieldWidth);
            layer.CreateField(fdefn, 1);
        }
        #endregion

        #region useful private helpers

        private List<Feature> GetFeatureListByType(DepictionGeometryType geomType, IEnumerable<IDepictionElement> elementList, Layer layer)
        {
            if (ServiceStopRequested) return null;
            int count = 0;
            var total = elementList.Count();
            List<Feature> featureList = new List<Feature>();
            foreach (var elem in elementList)
            {
                count++;
                var newMessage = string.Format(message + ": Feature list for {0} of {1}", count, total);
                UpdateStatusReport(newMessage);
                if (ServiceStopRequested) return null;

                if (elem.ZoneOfInfluence != null && elem.ZoneOfInfluence.DepictionGeometryType.Equals(geomType) && CanExportType(elem.ZoneOfInfluence.DepictionGeometryType))
                {

                    var geom = GeometryToOgrConverter.ZOIToOgrGeometry(elem.ZoneOfInfluence);
                    if (geom != null)
                    {
                        Feature feature = new Feature(layer.GetLayerDefn());
                        feature.SetGeometry(geom);
                        feature.SetField("ElementType", elem.ElementType);
                        feature.SetField("ElementKey", elem.ElementKey);
                        //Still not sure why the color is in this
                        Color color;
                        elem.GetPropertyValue("ZOIBorder", out color);
                        feature.SetField("ZOIBorder", (string)DepictionTypeConverter.ChangeType(color, typeof(string)));
                        elem.GetPropertyValue("ZOIFill", out color);
                        feature.SetField("ZOIFill", (string)DepictionTypeConverter.ChangeType(color, typeof(string)));

                        foreach (var prop in elem.OrderedCustomProperties)
                        {
                            var propValue = GetValueAsString(prop);
                            if (propValue != null)
                            {
                                try
                                {
                                    feature.SetField(prop.InternalName, propValue);
                                }
                                catch { }
                            }
                        }
                        featureList.Add(feature);
                    }

                }
            }
            return featureList;
        }

        private void AddElementsToLayer(Layer layer, DepictionGeometryType geomType, IEnumerable<IDepictionElement> elementList)
        {
            //
            //Add those properties that have integer, double, or string type values
            //
            if (ServiceStopRequested) return;
            int count = 0;
            var total = elementList.Count();
            foreach (var elem in elementList)
            {
                count++;
                var newMessage = string.Format(message + ":Layer for {0} of {1}", count, total);
                UpdateStatusReport(newMessage);
                if (ServiceStopRequested) return;
                if (elem.ZoneOfInfluence != null && elem.ZoneOfInfluence.DepictionGeometryType.Equals(geomType) &&
                    CanExportType(elem.ZoneOfInfluence.DepictionGeometryType))
                {
                    foreach (var prop in elem.OrderedCustomProperties)
                    {
                        if (prop.Value is string || prop.Value is Int32 || prop.Value is Double)
                            AddFieldDefToLayer(layer, prop.InternalName, 256);
                    }
                }
            }
            List<Feature> featureList = GetFeatureListByType(geomType, elementList, layer);
            if (featureList == null) return;
            foreach (var feature in featureList)
            {
                if (ServiceStopRequested) return;
                layer.CreateFeature(feature);
            }
        }

        #endregion

        #region Private helpers


        private int GetExportableElementCount(IEnumerable<IDepictionElement> elementList)
        {
            var cnt = 0;
            foreach (var elem in elementList)
            {
                if (elem.ZoneOfInfluence != null && CanExportType(elem.ZoneOfInfluence.DepictionGeometryType))
                    cnt++;
            }
            return cnt;
        }
        private bool CanExportType(DepictionGeometryType geomType)
        {
            switch (geomType)
            {

                case DepictionGeometryType.Point:
                case DepictionGeometryType.MultiPoint:
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    return true;

            }
            return false;
        }
        private string GetValueAsString(IElementProperty prop)
        {
            var stringVal = DepictionTypeConverter.ChangeType(prop.Value, typeof(string)).ToString();
            if (prop.Value.GetType().ToString().Equals(stringVal)) return null;
            return stringVal;
            //            if (prop.Value is string || prop.Value is Int32 || prop.Value is Double)
            //                return (string)DepictionTypeConverter.ChangeType(prop.Value, typeof(string));
            //            return null;
        }
        #endregion
    }
}