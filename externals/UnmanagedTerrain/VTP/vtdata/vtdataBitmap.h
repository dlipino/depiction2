//
// vtBitmap.h
//
// Copyright (c) 2003-2007 Virtual Terrain Project
// Free for all uses, see license.txt for details.
//

#ifndef VTBITMAP_H
#define VTBITMAP_H

#include "vtdata/vtDIB.h"

/**
 * This class provides an encapsulation of "bitmap" behavior, which can
 * either use the Win32 DIBSection methods, or the wxWindows Bitmap methods.
 *
 * Set USE_DIBSECTIONS to 1 to get the DIBSection functionality.
 */
class vtBitmap : public vtBitmapBase
{
public:
	vtBitmap();
	virtual ~vtBitmap();

	bool Allocate(int iXSize, int iYSize, int iDepth = 24);
	bool IsAllocated() const;
	void SetPixel24(int x, int y, unsigned char r, unsigned char g, unsigned char b);
	void SetPixel24(int x, int y, const RGBi &rgb)
	{
		SetPixel24(x, y, rgb.r, rgb.g, rgb.b);
	}
	void GetPixel24(int x, int y, RGBi &rgb) const;

	void SetPixel32(int x, int y, const RGBAi &rgba);
	void GetPixel32(int x, int y, RGBAi &rgba) const;

	unsigned char GetPixel8(int x, int y) const;
	void SetPixel8(int x, int y, unsigned char color);

	unsigned int GetWidth() const;
	unsigned int GetHeight() const;
	unsigned int GetDepth() const;

	void ContentsChanged();

	bool ReadPNGFromMemory(unsigned char *buf, int len);
	bool WriteJPEG(const char *fname, int quality);


protected:
	bool Allocate8(int iXSize, int iYSize);
	bool Allocate24(int iXSize, int iYSize);

	int m_imageWidth, m_imageHeight, m_imageDepth;
#if USE_DIBSECTIONS
	// A DIBSection is a special kind of bitmap, handled as a HBITMAP,
	//  created with special methods, and accessed as a giant raw
	//  memory array.
	unsigned char *m_pScanline;
	int m_iScanlineWidth;

#endif
};

#endif // VTBITMAP_H

