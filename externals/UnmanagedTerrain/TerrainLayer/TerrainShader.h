#include "TerrainLayer.h"


namespace DepictionTerrainLayer 
{
	/**
	* This small class describes how to map elevation (as from a heightfield)
	* onto a set of colors.
	*/

	class TerrainColorMap
	{
	private:
		
	public:
		TerrainColorMap();
		bool Load(const char *fname);
		void Add(float elev, const RGBi &color);
		void RemoveAt(int num);
		int Num() const;
		void GenerateColors(std::vector<RGBi> &table, int iTableSize, float fMin, float fMax) const;

		bool m_bBlend;
		bool m_bRelative;
		std::vector<float> m_elev;
		std::vector<RGBi> m_color;
	};
}