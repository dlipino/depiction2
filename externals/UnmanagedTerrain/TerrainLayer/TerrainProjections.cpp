#include "TerrainLayer.h"


namespace DepictionTerrainLayer
{
	///<summary>
	///Return a string that contains the WKT for the projection system
	///</summary>
	System::String^ TerrainLayer::GetProjectionWkt()
	{
		char *str;
		vtProjection proj = m_pGrid->GetProjection();
		proj.exportToWkt(&str);
		System::String^ wktString = gcnew System::String(str);
		return wktString;
	}

	///<summary>
	///Add a new grid after checking for compatibility of coordinate systems
	///If different, the new grid is transformed to the coordinate system of the present grid
	///</summary>
	bool TerrainLayer::AddLayerWithCheck(vtElevationGrid** newGrid)
	{
		vtProjection proj;
		proj = (*newGrid)->GetProjection();

		vtProjection origProj = *m_proj;

		// check for Projection conflict

		bool success = false;
		if (!m_pGrid->HasData())//FIRST TIME LOADING OF AN ELEVATION FILE
		{
			//int i = 0; //// TODO: what should we do here???
		}
		else if ( !(origProj == proj))
		{
			success = TransformCoords(newGrid);
		}

		return success;
	}

	///<summary>
	///Actually reproject the new grid to the coordinate system of the present grid
	///</summary>
	bool TerrainLayer::TransformCoords(vtElevationGrid** newGrid)
	{
		vtProjection proj_new = (*newGrid)->GetProjection();
		vtProjection origProj = *m_proj;



		if (origProj == proj_new)
			return true; // No conversion necessary

		bool success = false;
		if (newGrid)
		{
			// Check to see if the projections differ *only* by datum
			vtProjection test = *m_proj;
			test.SetDatum(proj_new.GetDatum());
			if (test == proj_new)
			{
				success = (*newGrid)->ReprojectExtents(proj_new);
			}
			else
			{
				bool bUpgradeToFloat = false;

				if (!(*newGrid)->IsFloatMode())
				{
					bUpgradeToFloat = true;
				}

				vtElevationGrid *grid_new = new vtElevationGrid; //new vtElevationGrid(bbox,*m_imageWidth,*m_imageHeight,true,m_pGrid->GetProjection());
			

				//what do you do at the boundaries of this new grid where
				//you may not have any points from the original grid projecting onto?
				//
				success = grid_new->ConvertProjection(*newGrid, origProj,
					bUpgradeToFloat, NULL/*progress_callback*/);

				if (success)
				{
					(*newGrid)->FreeData();
					delete *newGrid;
					*newGrid = grid_new;
				}
				else
				{
					//ERROR
					delete grid_new;
				}
			}
		}
		
		return success;
	}
}