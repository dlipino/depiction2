#include "vtdata\ElevationGrid.h"
#include "vtdata\vtTin.h"


namespace DepictionTerrainLayer
{
	class TinLayer
	{

		private:
			vtTin* m_tin;
			vtElevationGrid* m_pGrid;
			int m_numCols, m_numRows;

		public:
			TinLayer()
			{
				m_tin = new vtTin();
			}
			~TinLayer()
			{
				delete m_tin;
			}
			void AddVertices(System::Collections::ArrayList^ x , System::Collections::ArrayList^  y, System::Collections::ArrayList^  z, int xdim, int ydim);
			void AddTriangles(System::Collections::ArrayList^ indices);
			vtElevationGrid* GetElevationGrid(){ return m_pGrid;}
			vtElevationGrid* Griddify(int xdim, int ydim, bool speed);
			
			//bool FindAltitudeOnEarth(const DPoint2 &p, float &fAltitude, bool bTrue = false) const;
			//bool TestTriangle(int tri, const DPoint2 &p, float &fAltitude) const;
	};
}
