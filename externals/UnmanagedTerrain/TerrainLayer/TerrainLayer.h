// TerrainLayer.h

#pragma once

#include "vtdata\ElevationGrid.h"
#include "vtdata\vtDIB.h"
#include "vtdata\Filepath.h"
#include "vtdata\Unarchive.h"

#include <limits.h>			// for SHRT_MIN
#define UNKNOWN	SHRT_MIN
#define NODATA -(SHRT_MAX) //10000
#define DEFAULTELEVATION 0
#define FOREGROUND 1
#define BACKGROUND 0
#define DESIREDCOLOR 2

#define SHADING_BIAS 200
#define MINFLOODRESOLUTION 30 //meters in resolution below which resampling will take place to assist quick flooding

#define USENEWGRIDSPACING true


struct Point3d { float x, y, z;} ; // The scattered data points....

#include "surfgrid.h"
#include "contour.h" 

//enum from projections.cpp
enum DATUM { ADINDAN = 0, ARC1950, ARC1960, AUSTRALIAN_GEODETIC_1966,
			 AUSTRALIAN_GEODETIC_1984, CAMP_AREA_ASTRO, CAPE,
			 EUROPEAN_DATUM_1950, EUROPEAN_DATUM_1979, GEODETIC_DATUM_1949,
			 HONG_KONG_1963, HU_TZU_SHAN, INDIAN, NAD27, NAD83,
			 OLD_HAWAIIAN_MEAN, OMAN, ORDNANCE_SURVEY_1936, PUERTO_RICO,
			 PULKOVO_1942, PROVISIONAL_S_AMERICAN_1956, TOKYO, WGS_72, WGS_84,
			 UNKNOWN_DATUM = -1, NO_DATUM = -2, DEFAULT_DATUM = -3 };

namespace DepictionTerrainLayer 
{
	/**
	* This small class describes how to map elevation (as from a heightfield)
	* onto a set of colors.
	*/

	class TerrainColorMap
	{
	public:
		TerrainColorMap();
		bool Load(const char *fname);
		void Add(float elev, const RGBi &color);
		void RemoveAt(int num);
		int Num() const;
		void GenerateColors(std::vector<RGBi> &table, int iTableSize, float fMin, float fMax) const;
		void SetupDefaultColors();
		bool m_bBlend;
		bool m_bRelative;
		std::vector<float> m_elev;
		std::vector<RGBi> m_color;
	};

	public ref class TerrainLayer 
	{
	private:
		static bool firstTime;
		vtElevationGrid* m_pGrid;//pointer to the class
		vtElevationGrid* m_resampledGrid;
		vtProjection* m_proj;
		vtProjection* m_projGeo;
		int m_imageWidth;
		int m_imageHeight;
		bool needRefreshing;
		float m_minHeight;
		float m_maxHeight;
		bool m_downSampled;

		void SetCorners(vtElevationGrid* grid, DLine2* corners);
		float GetBestLayerData(DPoint2 pointCoord,vtElevationGrid*);
		float GetInterpolatedElevationValueInternal(double x, double y);
		void ConvertImageToWorldCoordinates(System::Collections::ArrayList^ contourList,int offset);
		void GetCurrentCornersAndHeightExtents();
		bool ColorDIBFromTable(vtDIB *pBM,std::vector<RGBi> &table, float fMin, float fMax, bool progress_callback(int));
		bool ShadeQuick(vtDIB *pBM, float fLightFactor, bool bTrue, bool progress_callback(int));
		bool ImportFromVariousFormats(vtElevationGrid* m_newGrid, char* fnameCharArray, char* fileExt);

	public:
		TerrainLayer()
		{
			m_pGrid = new vtElevationGrid();
			m_resampledGrid = new vtElevationGrid();
			m_projGeo = new vtProjection();
			m_projGeo->SetGeogCSFromDatum(NAD83);//NAD83
			//set m_proj to Geo by default
			m_proj = new vtProjection();
			m_proj->SetGeogCSFromDatum(NAD83);//NAD83
		}
		~TerrainLayer()//Dispose
		{
			this->!TerrainLayer();
		}

		!TerrainLayer()//Finalize
		{
			if (m_pGrid != NULL)
			{
				m_pGrid->FreeData(); 
				delete m_pGrid;
			} 
			if (m_resampledGrid != NULL) 
			{
				m_resampledGrid->FreeData();
				delete m_resampledGrid;
			}
			delete m_projGeo;
			delete m_proj;
		}

		void Scale(float);
		static void SetGDalPath(System::String^ path);
		bool HasBeenDownSampled();
		float GetInterpolatedElevationValue(double x, double y);
		float GetElevationValue(double x, double y);
		float GetElevationFromGridCoordinates(int col, int row);
		float GetConvolvedValue(int col, int row, int kernel);
		float GetClosestElevationValue(double x, double y);
		bool SetElevationValue(double x, double y, float value);
		bool SetElevationValue(int col, int row, float value);
		bool AddElevationValue(double x, double y, float value);
		bool AddElevationValue(int col, int row, float value);
		bool ImportFromFileWithoutExtents(System::String^ fnameString,double north, double south, double east, double west);
		bool ImportFromFile(System::String^ filenameString, System::String^ coordinateSystemString, System::String^ fileExtension, bool useExistingGridSpacing, bool cropGridBeforeProjection);
		bool Create(double north, double south, double east, double west, int width, int height, bool floatgrid, float defaultValue, System::String^ projFilename,
			int wktLength);
		void ResampleElevationGrid(int numRows, int numCols);
		void ResampleElevationGrid(vtElevationGrid* newGrid, bool useExistingGridSpacing);
		vtElevationGrid* MyCropElevationGrid(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight, bool useNewSpacing);
		/*bool ConvertProjection(vtElevationGrid *pOld,const vtProjection &NewProj, float bUpgradeToFloat, bool progress_callback(int));
		void ComputeExtentsFromCorners();*/
		vtElevationGrid* MyResampleElevationGrid(vtElevationGrid* newGrid, int* width, int* height, bool useNewSpacing);
        vtElevationGrid* TransformAndCrop(vtElevationGrid* newGrid,double topLeftX,double topLeftY,double bottomRightX,double bottomRightY,int* width,int* height);
		void CropElevationGrid(double topLeftX, double topLeftY, double bottomRightX, double bottomRightY);
		IPoint2 GetImageCoordinates(double x, double y);
		int GetRow(double lat);
		int GetColumn(double lon);
		double GetLatitude(int row);
		double GetLongitude(int col);
		DPoint2 GetWorldCoordinates(int col, int row);
		DPoint2 GetWorldCoordinates(int col, int row, int gridWidth, int gridHeight);
		System::String^ GetProjectionWkt();
		bool AddLayerWithCheck(vtElevationGrid** newGrid);
		bool TransformCoords(vtElevationGrid** newGrid);
		bool SaveAsPNG16(System::String^ fileName);
		bool SaveToGeoTIFF(System::String^ fileName);
		bool SaveToBT(System::String^ fileName);
		bool SaveTo24BitRGB(System::String^ fileName, int quality, System::String^ colormapFileName);
		void GetCorners(System::Collections::ArrayList^ cornerPts, bool geo);
		int GetWidthInPixels();
		int GetHeightInPixels();
		bool HasElevation(double x, double y);
		bool HasNoData(double x, double y);
		bool IsFloatMode() { return m_pGrid->IsFloatMode(); }
		bool CastRayToSurface(const FPoint3 &point, const FPoint3 &dir,FPoint3 &result);
		bool IntersectTwoPoints(System::Collections::ArrayList^ point,double x1, double y1, double x2, double y2);
		double DistanceOverTerrain(double lat1, double lon1, double lat2, double lon2, bool ignoreElevation);
		double MyDistanceOverTerrain(FPoint3 &point1,
									  FPoint3 &point2, DPoint2 spacing, vtProjection* projGeo, vtProjection* proj);

	};
}
