// This is the main DLL file for holding a grid of terrain data.
#include "TerrainLayer.h"
//#include "TinLayer.h"
#include "vtdata/ElevationShader.h"
#include "vtdata/HeightField.h"
#include "cpl_conv.h"

#include <iostream>
#include <fstream>
using System::String;
using System::Collections::ArrayList;


namespace DepictionTerrainLayer
{

	//
	//create a new grid with the specified geographic corners
	//
	bool TerrainLayer::Create(double south, double north, double east, double west, int width, int height, bool floatgrid, float defaultValue,
		System::String^ projFilename, int wktLength)
	{		
		
		if (m_projGeo == NULL)
		{
			m_projGeo = new vtProjection();
			m_projGeo->SetGeogCSFromDatum(NAD83);//NAD83
		}

		char* wktArray = NULL;
		if (wktLength > 1)
		{
			wktArray = new char[wktLength];
			char* projFilenameArray = new char[projFilename->Length + 1];
			sprintf(projFilenameArray, "%s", projFilename);

			FILE *fp = vtFileOpen(projFilenameArray, "r");
			fgets(wktArray, wktLength, fp);
			fclose(fp);
			delete [] projFilenameArray;
		}


		DRECT corners;
		corners.top = north;
		corners.bottom = south;
		corners.left = west;
		corners.right = east;

		//
		//default projection is mercator
		//
		//
		vtProjection* newProj = new vtProjection();

		if (wktLength > 1)
			newProj->SetTextDescription("wkt",wktArray);
		else // Use mercator.
			newProj->SetTextDescription("wkt","PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");
						
		//vtElevationGrid* geoGrid = new vtElevationGrid(corners,width,height,floatgrid,*newProj);

		//north,south,east,west are all in geog coord system, i.e., latlon
		//convert to mercator
		DPoint2 topLeftInMerc, bottomRightInMerc, bottomLeftInMerc, topRightInMerc;
		topLeftInMerc.x = west;
		topLeftInMerc.y = north;
		bottomRightInMerc.x = east;
		bottomRightInMerc.y = south;
		bottomLeftInMerc.x = west;
		bottomLeftInMerc.y = south;
		topRightInMerc.x = east;
		topRightInMerc.y = north;

		if(*m_projGeo != *newProj)
		{
			m_pGrid->TransformPoint(m_projGeo, newProj, &topLeftInMerc);
			m_pGrid->TransformPoint(m_projGeo, newProj, &bottomRightInMerc);
			m_pGrid->TransformPoint(m_projGeo, newProj, &bottomLeftInMerc);
			m_pGrid->TransformPoint(m_projGeo, newProj, &topRightInMerc);
		}
		

		corners.top = topLeftInMerc.y;
		corners.bottom = bottomRightInMerc.y;
		corners.left = topLeftInMerc.x;
		corners.right = bottomRightInMerc.x;

		vtElevationGrid* tempGrid = new vtElevationGrid(corners, width, height, floatgrid, *newProj);
		if (!tempGrid->IsValidGrid())
		{
			delete [] wktArray;
			delete tempGrid;
			delete newProj;
			return false;
		}
		//set initial values of zero
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				tempGrid->SetValue(i, j, defaultValue);
			}
		}

		m_imageWidth = width;
		m_imageHeight = height;

		delete newProj;
		if (m_pGrid != NULL)
		{
			m_pGrid->FreeData();
			delete m_pGrid;
		}
		m_pGrid = tempGrid;
		m_proj = new vtProjection();
		if (wktLength > 1)
		{
			m_proj->SetTextDescription("wkt", wktArray);
		}
		else // Use mercator.
			m_proj->SetTextDescription("wkt", "PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");
	
		needRefreshing = true;
		delete [] wktArray;
		return true;
	}

    void TerrainLayer::SetGDalPath(System::String^ path)
	{
		char* pathChars = new char[path->Length + 1];
		sprintf(pathChars, "%s", path);
		CPLPushFinderLocation(pathChars);
		delete [] pathChars;
	}

	float TerrainLayer::GetBestLayerData(DPoint2 pointCoord, vtElevationGrid* grid)
	{
		float elevValue, bestData = UNKNOWN;

		for(int i = 0; i < 2; i++)//we only have the first elevation data layer and the next
			//one under consideration
		{
			vtElevationGrid *grid = (vtElevationGrid*)(i == 0) ? m_pGrid : grid;
			elevValue = grid->GetFilteredValue(pointCoord);
			if(elevValue != UNKNOWN)bestData = elevValue;
		}

		return bestData;
	}

	bool TerrainLayer::IntersectTwoPoints(System::Collections::ArrayList^ point, double x1, double y1, double x2, double y2)
	{
		bool success = false;

		FPoint3 center;
		center.x = x1;
		center.y = y1;
		center.z = GetElevationValue(x1, y1);

		FPoint3 dir;
		dir.x = x2;
		dir.y = y2;
		dir.z = GetElevationValue(x2, y2);

		FPoint3 result;

		success = CastRayToSurface(center, dir, result);
		point->Add((double)result.x);
		point->Add((double)result.y);
		return success;
	}

	bool TerrainLayer::CastRayToSurface(const FPoint3 &point,
		const FPoint3 &dir, FPoint3 &result)
	{
		bool intersects = m_pGrid->CastRayToSurface(point, dir, result);

		return intersects;
	}

	double TerrainLayer::DistanceOverTerrain(double lat1, double lon1, double lat2, double lon2, bool ignoreElevation)
	{
		
		FPoint3 point1;
		FPoint3 point2;

		DPoint2 spacing = m_pGrid->GetSpacingD();
		point1.x = lon1; 
		point1.y = lat1;
		DPoint2 point2d1;
		point2d1.x = lon1; point2d1.y = lat1;
		//transform point from latlong to projections (mercator)
		m_pGrid->TransformPoint(m_projGeo, m_proj, &point2d1);
		point1.z = (m_pGrid != NULL) ? m_pGrid->GetFilteredValue(DPoint2(point2d1.x, point2d1.y)) : 0;

		point2.x = lon2; 
		point2.y = lat2;
		DPoint2 point2d2;
		point2d2.x = lon2;
		point2d2.y = lat2;
		//transform point from latlong to projections (mercator)
		m_pGrid->TransformPoint(m_projGeo, m_proj, &point2d2);
		point2.z = (m_pGrid != NULL) ? m_pGrid->GetFilteredValue(DPoint2(point2d2.x, point2d2.y)) : 0;

		//if no elevation, merely return the geodesic distance
		if(m_pGrid == NULL || ignoreElevation)
			return vtProjection::GeodesicDistance(DPoint2(point1.x, point1.y), DPoint2(point2.x, point2.y));

		//if no elevation, merely return the geodesic distance
		if(point1.z == UNKNOWN || point1.z == NODATA || point2.z == UNKNOWN || point2.z == NODATA)
			return vtProjection::GeodesicDistance(DPoint2(point1.x, point1.y), DPoint2(point2.x, point2.y));

		return MyDistanceOverTerrain(point1, point2, spacing, m_projGeo, m_proj);
	}

	double TerrainLayer::MyDistanceOverTerrain(FPoint3 &point1,
									   FPoint3 &point2, DPoint2 spacing, vtProjection* projGeo, vtProjection* proj)
	{
		double distance = 0;
		float alt;

		double geodesicDistance =  vtProjection::GeodesicDistance(DPoint2(point1.x, point1.y), DPoint2(point2.x, point2.y));
		//convert point1 and point2, which are in geo coords, to Mercator
		//
		DPoint2 p1Geo, p2Geo;
		p1Geo.x = point1.x;
		p1Geo.y = point1.y;
		p2Geo.x = point2.x;
		p2Geo.y = point2.y;

		m_pGrid->TransformPoint(projGeo, proj, &p1Geo);
		m_pGrid->TransformPoint(projGeo, proj, &p2Geo);
		point1.x = p1Geo.x;
		point1.y = p1Geo.y;
		point2.x = p2Geo.x;
		point2.y = p2Geo.y;

		//ok, point1 and point2 are in MERCATOR now
		
		// special case: straight up or down
		FPoint3 dir = point2 - point1;
		float mag2 = sqrt(dir.x * dir.x +dir.y * dir.y);
		if (fabs(mag2) < .000001)
			return  vtProjection::GeodesicDistance(DPoint2(p1Geo.x, p1Geo.y), DPoint2(p2Geo.x, p2Geo.y));

		// adjust magnitude of dir until 2D component has a good magnitude
		float smallest = std::min(spacing.x, spacing.y);
		int steps = (int) (mag2 / smallest) + 1;
		if (steps < 2)
			steps = 2;
		dir /= (float) steps;

		FPoint3 p = point1;
		FPoint3 previous = point1;
		//convert to geographic coord system
		DPoint2 previous2D;
		previous2D.x = previous.x;
		previous2D.y = previous.y;
		m_pGrid->TransformPoint(proj, projGeo, &previous2D);
		previous.x = previous2D.x;
		previous.y = previous2D.y;
		//previous is now in geographic coordinates

		DPoint2 pointGeo;
		double segmentDistance;
		for (int i = 0; i < steps + 1; i++)
		{
			//variable p is in mercator
			alt = m_pGrid->GetFilteredValue(DPoint2(p.x, p.y));
			pointGeo.x = p.x;
			pointGeo.y = p.y;
			//transform point from projections (mercator) to geo
			m_pGrid->TransformPoint(proj, projGeo, &pointGeo);
			segmentDistance = vtProjection::GeodesicDistance(DPoint2(previous.x, previous.y), DPoint2(pointGeo.x, pointGeo.y));
			//segmentDistance = sqrt(segmentDistance*segmentDistance + (double)((previous.z-alt)*(previous.z-alt)) );//abs(alt-previous.z);
			distance += segmentDistance;
			previous.x = pointGeo.x;
			previous.y = pointGeo.y;
			previous.z = alt;
			p += dir;
		}

		return distance;
	}


}