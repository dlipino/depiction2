﻿using System;
using System.Diagnostics;

namespace Depiction2.Utilities.Memory
{
    public class MemUtil
    {
        public static long GetProcessMem()
        {
            //memory in bytes
            var proc = Process.GetCurrentProcess();
            var workMem = proc.WorkingSet64;
            long sMem = proc.PrivateMemorySize64;
            long sPageMem = proc.PagedMemorySize64;
            var gcMem = GC.GetTotalMemory(true);
            var memString = string.Format("wMem {2} :Mem {0} :pageMem {1} :gcMem {3}", sMem, sPageMem, workMem, gcMem);
            Console.WriteLine(memString);
            return sMem;
        }

        public static void DoMassGC()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}