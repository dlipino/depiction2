﻿using System;

namespace Depiction2.Utilities
{
    public class LatLongToStringUtility
    {
        
        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>


        public string DoSignedDegreesFractionalMinutes(double lat, double lon, string separator)
        {
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(lat, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(lon, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3:00}° {4:00.00000}",
                                 latDeg, latMin, separator, longDeg, longMin);
        }

        public string DoDegreesMinutesSeconds(double lat, double lon, string separator, string degreeSign)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(lat, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(lon, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}{1}{2:00}'{3:00}\"{4}{5}{6:00}{7}{8:00}'{9:00}\"{10}",
                                 latDeg, degreeSign, latMin, latSec, NS(lat), separator, longDeg, degreeSign, longMin, longSec, EW(lon));
        }

        public string DoDegreesFractionalMinutes(double lat, double lon, string separator)
        {
            //40° 26.77170N, 79° 55.93172W
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(lat, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(lon, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3}{4:00}° {5:00.00000}{6}",
                                 Math.Abs(latDeg), latMin, NS(lat), separator, Math.Abs(longDeg), longMin, EW(lon));
        }
        public string DoColonIntegerSeconds(double lat, double lon, string separator)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(lat, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(lon, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}:{1:00}:{2:00}{3}{4}{5:00}:{6:00}:{7:00}{8}",
                                 latDeg, latMin, latSec, NS(lat), separator, longDeg, longMin, longSec, EW(lon));
        }
        public string DoColonFractionalSeconds(double lat, double lon, string separator)
        {
            int latDeg;
            int latMin;
            double latSec;
            int longDeg;
            int longMin;
            double longSec;
            ExtractDegreesMinutesSeconds(lat, out latDeg, out latMin, out latSec, 3);
            ExtractDegreesMinutesSeconds(lon, out longDeg, out longMin, out longSec, 3);
            return string.Format("{0:00}:{1:00}:{2:00.000}{3}{4}{5:00}:{6:00}:{7:00.000}{8}",
                                 latDeg, latMin, latSec, NS(lat), separator, longDeg, longMin, longSec, EW(lon));
        }

        public string DoSignedDecimal(double lat, double lon, string separator)
        {
            return string.Format("{0:F5}{1}{2:F5}", lat, separator, lon);
        }

        public string DoDecimal(double lat, double lon, string separator)
        {
            var ns = (lat < 0) ? "S" : "N";
            var ew = (lon < 0) ? "W" : "E";
            return string.Format("{0:00.00000}{1}{2}{3:00.00000}{4}", Math.Abs(lat), ns, separator, Math.Abs(lon), ew);
        }

        private string EW(double lon)
        {
           return (lon < 0) ? "W" : "E"; 
        }

        private string NS(double lat)
        {
            return (lat < 0) ? "S" : "N"; 
        }

        /// <summary>
        /// Degrees will have same sign as value.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutes(double value, out int deg, out double min, int decimals)
        {
            deg = (int)Math.Truncate(value);
            var fraction = Math.Abs(value) - Math.Floor(Math.Abs(value));
            min = fraction * 60;
            if (Math.Round(min, decimals) >= 60)
            {
                min = 0;
                deg += 1 * Math.Sign(deg);
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out int sec)
        {
            double dSec;
            ExtractDegreesMinutesSeconds(value, out deg, out min, out dSec, 0);
            sec = (int)Math.Round(dSec);
            if (sec == 60)
            {
                min += 1;
                sec = 0;
            }
            if (min == 60)
            {
                deg += 1;
                min = 0;
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out double sec, int decimals)
        {
            deg = (int)Math.Floor(Math.Abs(value));
            double temp;
            if (deg != 0)
                temp = 60 * (Math.Abs(value) % deg);
            else
                temp = 60 * Math.Abs(value);
            min = (int)Math.Floor(temp);
            sec = (temp - min) * 60;
            if (Math.Round(sec, decimals) >= 60)
            {
                sec = 0;
                min += 1;
            }
            if (min >= 60)
            {
                min = 0;
                deg += 1;
            }
        }
    }
}