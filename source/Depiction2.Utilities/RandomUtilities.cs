﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Depiction2.Utilities
{
    public class RandomUtilities
    {
        public static object CheapNumberTypeConverter(object value, Type newType)
        {
            if (!(value is int || value is double)) return null;
            if (newType.Equals(typeof(int)))
            {
                return Convert.ToInt32(value);
            }
            if (newType.Equals(typeof(double)))
            {
                return Convert.ToDouble(value);
            }
            return null;
        }

        static public Rect GetRectInteriorToRect(Rect viewport)
        {
            var aW = viewport.Width;
            var aH = viewport.Height;

            var w = aW * .6;
            var h = aH * .6;
            var t = aH * .2 + viewport.Top;
            var l = aW * .2 + viewport.Left;
            var pixelSize = new Rect(l, t, w, h);
            return pixelSize;
        }

        //The timing for the y inversion is suspect, mostly because of the wpf draw Rect
        static public List<Point> ConvertRectToClosedPointList(Rect sb, Func<Point, Point> pointConverter, bool invertY)
        {
            var screenPointList = new List<Point> { sb.TopLeft, sb.TopRight, sb.BottomRight, sb.BottomLeft, sb.TopLeft };
            var canvasList = new List<Point>();
            foreach (var p in screenPointList)
            {
                var mp = p;
                if(pointConverter != null) mp = pointConverter(p);
                if (invertY) canvasList.Add(new Point(mp.X, -mp.Y));
                else canvasList.Add(mp);
            }
            return canvasList;
        }
    }
}