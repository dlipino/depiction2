﻿using System.IO;
using System.Reflection;
using NUnit.Framework;

namespace Depiction2.SimpleRoutes.UnitTests
{
    public class TestUtilities
    {
        static internal string GetFullFileName(string fileName, string holdingFolder)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("TestFiles", holdingFolder);
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            Assert.IsTrue(File.Exists(fullFileName), "Could not find file for osm roadnetwork test");
            return fullFileName;
        }
    }
}