﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Core.StoryEntities;
using Depiction2.SimpleRoutes.OSMRoadNetwork;
using NUnit.Framework;

namespace Depiction2.SimpleRoutes.UnitTests.OSMRoadNetwork
{
    [TestFixture]
    public class OSMWebImporterServiceTest
    {
        private OSMWebImportService testService;
        private DepictionFolderService folderService;



        [SetUp]
        public void SetUp()
        {
            testService = new OSMWebImportService();
            folderService = new DepictionFolderService(true);
        }

        [TearDown]
        public void TearDown()
        {
            testService = null;
            folderService.Close();
        }

        [Test]
        public void AreaBreakUpTest()
        {
            //results come from 1.4
            var topLeft = new Point(-122.398367, 47.630625766462);
            var botRight = new Point(-122.259551, 47.5743016156473);

            var dRegion = new DepictionRegion(topLeft, botRight);
            var dCart = dRegion.GeoRectBounds;
            var cartRegion = new CartRect(topLeft, botRight);
            var cartSet = new List<ICartRect> { cartRegion, dCart };
            foreach (var cart in cartSet)
            {
                var result = testService.BreakupAreaIntoTiles(cart);
                var expecteFC = 3840;
                var expecteLC = 3849;
                var expecteFR = 2157;
                var expecteLR = 2161;
                var expecteTT = 50.0;
                Assert.AreEqual(expecteFC, result[0]);
                Assert.AreEqual(expecteLC, result[1]);
                Assert.AreEqual(expecteFR, result[2]);
                Assert.AreEqual(expecteLR, result[3]);
                Assert.AreEqual(expecteTT, result[4]);
            }
        }

        [Test]
        [Ignore]
        public void SimpleRoadNetworkGetTest()
        {
            var fullUrl = OSMWebImportService.current14OsmUrl;
            var result = testService.SaveRoadNetworkFileFromUrl(fullUrl, null, 0);
        }

    }
}