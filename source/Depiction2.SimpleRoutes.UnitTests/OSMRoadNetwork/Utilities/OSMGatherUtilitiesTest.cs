﻿using System.Collections.Generic;
using Depiction2.SimpleRoutes.OSMRoadNetwork;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;
using NUnit.Framework;

namespace Depiction2.SimpleRoutes.UnitTests.OSMRoadNetwork.Utilities
{
    [TestFixture]
    public class OSMGatherUtilitiesTest
    {

        [Test]
        public void GetFromDefaultWebSourceTest()
        {
            var fileName = "OSMRoadNetwork_row_2158_col_3843.xml";
            var fullName = TestUtilities.GetFullFileName(fileName, "OSMRoadNetwork");
            OSMNode[] nodesInFile;
            OSMWay[] waysInFile;
            OSMGatherUtilities.GetNodesAndWaysFromFile(fullName, out nodesInFile, out waysInFile, null);
            var nodes = new Dictionary<long, OSMNode>();
            var ways = new Dictionary<long, OSMWay>();
            OSMWebImportService.CombineNodesAndWays(nodesInFile, waysInFile, nodes, ways, false);
            var roadSegments = OSMGatherUtilities.GetTaggedEdgeListFromOSMData(nodes, ways, null, null);
            var roadGraph = OSMWebImportService.BuildRoadGraph(roadSegments);

            var geomString = roadGraph.ConvertToMultilinewkt();
        }
    }
}