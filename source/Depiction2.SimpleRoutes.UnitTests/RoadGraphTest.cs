﻿using System.Windows;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;
using NUnit.Framework;

namespace Depiction2.SimpleRoutes.UnitTests
{
    [TestFixture]
    public class RoadGraphTest
    {
        [Test]
        public void PointDistanceTest()
        {
            var p = new Point(0, 1);
            var p1 = new Point(0, 5);
            var d = RoadGraph.PointDistance(p, p1);
            Assert.AreEqual(4, d);
        }
    }
}