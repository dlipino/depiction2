﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Resources;
using Depiction2.GdalExtension.GeometryItems;
using Depiction2.GdalExtension.Terrain;

namespace Depiction2.GdalExtension.Resources
{
    [ResourceExtensionMetadata(DisplayName = "Gdal Resource", Name = "ResourcesGdal", Author = "Depiction Inc.", ExtensionPackage = "Gdal", ResourceName = "GdalResources")]
    public class GdalResources : IDepictionResourceExtension
    {
        public ResourceDictionary ExtensionResources
        {
            get
            {
                var uri = new Uri("/Depiction2.GdalExtension;Component/Resources/Images/GdalResources.xaml", UriKind.RelativeOrAbsolute);
                var rd = new ResourceDictionary { Source = uri };
                return rd;
            }
        }

        public Dictionary<string, Type> ExtensionTypes
        {
            get
            {
                return new Dictionary<string, Type>
                           {
                              {"Terrain", typeof (RestorableTerrainData)},//for legacy loading
                               {typeof (RestorableTerrainData).Name, typeof (RestorableTerrainData)},
                               {typeof (GeometryGdal).Name, typeof (GeometryGdal)}
                           };
            }
        }

        public void Dispose()
        {

        }
    }
}