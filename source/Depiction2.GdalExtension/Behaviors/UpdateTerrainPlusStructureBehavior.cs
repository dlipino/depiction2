﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Measurement;
using Depiction2.API.Service;
using Depiction2.API.Utilities;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.Utilities;
using Depiction2.GdalExtension.Terrain;

namespace Depiction2.GdalExtension.Behaviors
{
    [BehaviorMetadata("UpdateTerrainPlusStructureBehavior", "Update terrain data structure", "Change terrain")]
    public class UpdateTerrainPlusStructureBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Zone", typeof(IElement)), 
                    new DepictionParameterInfo("Height", typeof(Distance))
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }
        
        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {

            var terrainProp = subscriber.GetDepictionProperty("Terrain");

            RestorableTerrainData terrainCoverage;
            if (terrainProp == null)
            {
                //                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Behavior {0} could not find elevation data.", FriendlyName));
                return new BehaviorResult { SubscriberHasChanged = false };
            }
            //            subscriber.RemovePropertyIfNameAndTypeMatch(terrainProp);

            terrainCoverage = terrainProp.Value as RestorableTerrainData;
            if (terrainCoverage == null)
            {
                //                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Behavior {0} could not find elevation data.", FriendlyName));
                return new BehaviorResult { SubscriberHasChanged = false };
            }

            var elemWithZOI = parameterBag["Zone"] as IElement;
            var heightProp = parameterBag["Height"] as Distance;
            if (heightProp == null || elemWithZOI == null) return new BehaviorResult();

            var originalBarrierZOI = elemWithZOI.ElementGeometry;
            var rectangleHeight = heightProp.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);

            lock (terrainCoverage)
            {
                try
                {
                    int width = terrainCoverage.GetGridWidthInPixels();

                    int height = terrainCoverage.GetGridHeightInPixels();
                    double gridSpacing = terrainCoverage.GetGridResolution();
                    //                    var geomFactory = new GeometryFactory();

                    //find the bounding box of the rectangle and
                    //update the terrain only within the bounding box

                    var geomToUse = originalBarrierZOI;
                    var zoiCoords = new List<Point>();
//                    if (DepictionAccess.DStory != null)
//                    {
//                        var projChangers = DepictionAccess.ExtensionLibrary.ProjectionChangersArray;
//                        var changer = projChangers[0].Value;
//                        changer.SetTargetAndSourceProj(DepictionAccess.DStory.CurrentCoordinateSystemString, terrainCoverage.DepictionDatum);
//                        geomToUse = originalBarrierZOI.Clone();
//                        changer.TransformGeometry(geomToUse);
//                    }
                    zoiCoords = geomToUse.GeometryPoints.ToList();
                    double minLon = 1000, maxLon = -1000, minLat = 1000, maxLat = -1000;
                    for (int i = 0; i < zoiCoords.Count; i++)
                    {
                        var coord = zoiCoords[i];
                        if (coord.X < minLon) minLon = coord.X;
                        if (coord.X > maxLon) maxLon = coord.X;
                        if (coord.Y < minLat) minLat = coord.Y;
                        if (coord.Y > maxLat) maxLat = coord.Y;
                    }
                    //find the pixel row/col of the min/max coordinates of the rectangle
                    int minRow, maxRow, minCol, maxCol;
                    //remember that the bottom most row (lowest latitude) is Row 0
                    //with Row height-1 at the top (max latitude)
                    //
                    maxRow = (maxRow = terrainCoverage.GetRow(maxLat)) < 0 ? 0 : maxRow;
                    minRow = (minRow = terrainCoverage.GetRow(minLat)) > height - 1
                                 ? height - 1
                                 : minRow;
                    minCol = (minCol = terrainCoverage.GetColumn(minLon)) < 0 ? 0 : minCol;
                    maxCol = (maxCol = terrainCoverage.GetColumn(maxLon)) > width - 1
                                 ? width - 1
                                 : maxCol;

                    //pad the bounding box row/col extent by 1, just to cover
                    //for the dreaded n-1 problem
                    minRow = (minRow - 1) >= 0 ? (minRow - 1) : 0;
                    minCol = (minCol - 1) >= 0 ? (minCol - 1) : 0;
                    maxRow = (maxRow + 1) > (height - 1) ? (height - 1) : maxRow + 1;
                    maxCol = (maxCol + 1) > (width - 1) ? (width - 1) : maxCol + 1;

                    for (int row = minRow; row <= maxRow; row++)
                    {
                        for (int col = minCol; col <= maxCol; col++)
                        {
                            double lat = terrainCoverage.GetLatitude(row);
                            double lon = terrainCoverage.GetLongitude(col);

                            var gridCellBox = GeoProcessor.CreateGridCellPolygon(lon, lat, gridSpacing);

                            if (geomToUse.Intersects(gridCellBox))
                            {
                                float elevValue = terrainCoverage.GetValueAtGridCoordinate(col, row);
                                terrainCoverage.SetValueAtGridCoordinate(col, row, (float)rectangleHeight + elevValue);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //                    DepictionAccess.NotificationService.DisplayMessageString(
                    //                        string.Format("Error modifying the terrain.\n\nInternal erorr: {0}", ex.Message));
                    return new BehaviorResult { SubscriberHasChanged = false };
                }
                terrainCoverage.ClearVisual();
                //Create the modified image, which should replace what ever image is associated with this terrain element
                var folderService = new DepictionFolderService(false);
                var tempFolder = folderService.FolderName;
                var imageName = string.Format("modifiedTerrain-{0}.jpg", subscriber.ElementId);
                var createdVisual = terrainCoverage.GenerateTerrainVisual(tempFolder, imageName);
                if (createdVisual)
                {
                    var fullName = Path.Combine(tempFolder, imageName);
                    var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullName);
                    DepictionAccess.DStory.AllImages.AddImage(imageName, image, true);
                    subscriber.ImageResourceName = imageName;
                }
                folderService.Close();
            }

            return new BehaviorResult { SubscriberHasChanged = true };
        }
    }
}