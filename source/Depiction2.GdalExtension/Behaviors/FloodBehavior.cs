﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Measurement;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.GdalExtension.Terrain.Utilities;
using Depiction2.Terrain.Utilities;

namespace Depiction2.GdalExtension.Behaviors
{
    [BehaviorMetadata("SimpleFlood", "Calculate a simple flood", "Calculates a simple flood")]
    public class FloodBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Terrain", typeof(ICoverage))
                        {
                            ParameterDescription = "Elevation Data", 
                            ParameterName = "The elevation data to use when calculating the flood"
                        }, 
                    new DepictionParameterInfo("FloodHeight", typeof(Distance))
                        {
                            ParameterDescription = "Indicates how many meters above sea level the flood will rise", 
                            ParameterName = "Height above sea level"
                        }
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var floodOffset = (Distance)parameterBag["FloodHeight"];
            var floodOffsetMeters = floodOffset.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            if (subscriber.GeoLocation == null || floodOffsetMeters < 0)//|| !subscriber.Position.IsValid 
                return new BehaviorResult();

            var terrainData = parameterBag["Terrain"] as ICoverage;


            DoFlood(subscriber, terrainData, floodOffsetMeters);
            return new BehaviorResult { SubscriberHasChanged = true };
        }


        private static void DoFlood(IElement Flood, ICoverage terrain, double metersAboveFloodOrigin)
        {
            //var elevationData = CreateCoverage(terrain);
            var pos = new Point();
            if (Flood.GeoLocation != null)
                pos = Flood.GeoLocation.Value;

            float elevationAtOrigin = terrain.GetInterpolatedElevationValue(pos);
            double elevationOfFlood = elevationAtOrigin + metersAboveFloodOrigin;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;
            var floodGeometry = SimpleFloodModel.GenerateSimpleFloodGeometryFromTerrain(terrain, pos, elevationOfFlood);
            //var floodGeometry = DepictionAccess.DepictionGeomFactory.CreateFloodGeometryFromTerrain(terrain, pos, elevationOfFlood);
            if (floodGeometry == null)
            {
                //                DepictionAccess.NotificationService.DisplayMessageString("Invalid Flood polygon generated. Please try again.");
                return;
            }

            lock (Flood.ElementGeometry)
            {
                {

                    Flood.UpdatePrimaryPointAndGeometry(pos, floodGeometry);//SetZOIGeometry(floodGeometry);
                }
            }
        }
    }
}
