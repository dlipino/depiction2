﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.GdalExtension.EnvironmentSetters;

namespace Depiction2.GdalExtension.WxS
{
    [RegionSourceMetadata(Name = "GdalWfsImporter", DisplayName = "Generic Wfs importer", Author = "Depiction Inc.", 
        GenericName="WfsElementGatherer", ShortName = "Wfs Importer")]
    public class GdalWfsImporterExtension : IRegionSourceExtension
    {
        private string importerNameProp = "ImporterName";
        private string areaProp = "Area";
        private string wfsAddress = "Source";
        private string typeProp = "elementType";
        private string hoverText = "hoverText";
        private string addressFromqs = "url";
        private string layertitle = "layerName";

        public GdalWfsImporterExtension()
        {
            OGREnvironment.SetOGREnvironment();
            GDALEnvironment.SetGDALEnvironment();
        }
        public void Dispose()
        {

        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> parameters)
        {
            var dict = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            dict.Add("Area", regionOfInterest);

            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    dict.Add(item.Key, item.Value);
                }
            }

            var service = new GdalWfsBackgroundService();
            var name = string.Format("Starting wfs gather");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }
    }
}