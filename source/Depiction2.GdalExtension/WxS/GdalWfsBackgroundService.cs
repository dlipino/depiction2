﻿using System;
using System.Collections.Generic;
using Depiction2.API;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.GdalExtension.GeometryItems;
using OSGeo.OGR;

namespace Depiction2.GdalExtension.WxS
{
    public class GdalWfsBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        public const string areaKey = "Area";
        public const string typeKey = "elementType";
        public const string hoverText = "hoverText";
        public const string sourceAddressKey = "url";
        public const string layerKey = "layerName";

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var info = args as Dictionary<string, object>;
            if (info == null) return null;
            ICartRect area = null;
            var address = string.Empty;
            var layerName = string.Empty;
            var requestedElementType = string.Empty;
            if(info.ContainsKey(areaKey))
            {
                area = info[areaKey] as ICartRect;
            }
            if (info.ContainsKey(sourceAddressKey))
            {
                address = info[sourceAddressKey].ToString();
            }
            else if (info.ContainsKey(sourceAddressKey))
            {
                address = info[sourceAddressKey].ToString();
            }
            if(info.ContainsKey(layerKey))
            {
                layerName = info[layerKey].ToString();
            }
            if (info.ContainsKey(typeKey))
            {
                requestedElementType = info[typeKey].ToString();
            }
            var complateAddress = string.Format("{0}?&SERVICE=WFS", address);
            var wfsDataSource = Ogr.Open(complateAddress, 0);
            var layer = wfsDataSource.GetLayerByName(layerName);
            Console.WriteLine(layer.GetGeomType());
            Console.WriteLine(layer.GetFeatureCount(0));
      
            var topy = 47.630625766462;
            var leftx = -122.398367;
            var boty = 47.5743016156473;
            var rightx = -122.259551;
            if(area !=null)
            {
                 topy = area.Top;
                 leftx = area.Left;
                 boty = area.Bottom;
                 rightx = area.Right;
            }
            layer.SetSpatialFilterRect(leftx, boty, rightx, topy);
            var templateData = new List<Dictionary<string, object>>();
            var spatialFilter = layer.GetSpatialFilter();
            Feature f = layer.GetNextFeature();

            while (f != null)
            {
                var dataHolder = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
                var completeGeom = f.GetGeometryRef();
                var geom = completeGeom.Intersection(spatialFilter);
                //TODO connect this to the depiction geometry service
                dataHolder.Add(PropertyKeys.geometryKey, new GeometryGdal(geom));
                dataHolder.Add(PropertyKeys.elementTypeKey,requestedElementType);
                var fieldCount = f.GetFieldCount();
                for (int j = 0; j < fieldCount; j++)
                {
                    var fref = f.GetFieldDefnRef(j);
                    var fType = fref.GetFieldType();
                    var fName = fref.GetName();

                    switch (fType)
                    {
                        case FieldType.OFTInteger:
                            dataHolder.Add(fName, f.GetFieldAsInteger(j));
                            break;
                        case FieldType.OFTReal:
                            dataHolder.Add(fName, f.GetFieldAsDouble(j));
                            break;
                        case FieldType.OFTString:
                        case FieldType.OFTWideString:
                            dataHolder.Add(fName, f.GetFieldAsString(j));
                            break;
                    }
                }

                templateData.Add(dataHolder);
                f = layer.GetNextFeature();
            }
            return templateData;
        }

        protected override void ServiceComplete(object args)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var dataList = args as List<Dictionary<string, object>>;
            story.UpdateRepo(dataList, null, null);
        }
    }
}