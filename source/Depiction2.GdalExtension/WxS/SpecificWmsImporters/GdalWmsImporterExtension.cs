﻿using System;
using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.GdalExtension.EnvironmentSetters;

namespace Depiction2.GdalExtension.WxS.SpecificWmsImporters
{
    [RegionSourceMetadata(Name = "OpenStreetMapsGatherer", DisplayName = "OSM Wms importer", Author = "Depiction Inc.",
        ShortName = "OSM-WMS Importer")]
    public class OsmWmsImporterExtension : IRegionSourceExtension
    {
        private string typeKey = "elementType";
        private string serviceAddressKey = "url";
        private string layerNameKey = "layerName";
        public OsmWmsImporterExtension()
        {
            OGREnvironment.SetOGREnvironment();
            GDALEnvironment.SetGDALEnvironment();
        }
        public void Dispose()
        {

        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> parameters)
        {
            var dict = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            dict.Add("Area", regionOfInterest);

            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    dict.Add(item.Key, item.Value);
                }
            }
            dict.Add(serviceAddressKey,"http://ows.terrestris.de/osm/service?");
            dict.Add(layerNameKey,"OSM-WMS");

            var service = new GdalWmsBackgroundService();
            var name = string.Format("Starting osm gatherer");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }
    }
}