using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Tools;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Extensions;
using Depiction2.GdalExtension.Terrain.Utilities;
using OSGeo.GDAL;

namespace Depiction2.GdalExtension.WxS
{
    [RegionSourceMetadata(Name = "GdalWcsImporter", DisplayName = "Generic Wcs importer", Author = "Depiction Inc.",
       GenericName = "WcsElementGatherer", ShortName = "Wcs Importer")]
    public class GdalWcsBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        private ICartRect depictionRegion;
        string elevationUnit = "m";
        private string tags = "";
        private TileCacheService cacheService;
        private IDepictionRegion sourceOwner = null;


        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            cacheService = new TileCacheService("WcsCache");
            if (args.ContainsKey("Tag"))
            {
                tags = args["Tag"].ToString();
            }
            if(args.ContainsKey("Region"))
            {
                sourceOwner = args["Region"] as IDepictionRegion;
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            if (!parameters.ContainsKey("Area")) return null;
            var area = parameters["Area"] as ICartRect;
            depictionRegion = area;
            try
            {

                var cacheFileName = GetRequestedFileName(area, parameters);
                var terrainConverter = new CoverageDataConverter(cacheFileName, elevationUnit, true, null);

                UpdateStatusReport("Converting data to elevation");
                var elements = terrainConverter.ConvertDataToElements();
                if (parameters.ContainsKey("Tag"))
                {
                    var tag = parameters["Tag"] as string;
                    if (!string.IsNullOrEmpty(tag))
                    {
                        foreach (var element in elements)
                        {
                            element.AddTag(tag);
                        }
                    }
                }
                return elements;
            }
            catch (Exception ex)
            {
                UpdateStatusReport("Error creating elevation. See error log for details.");
                return null;
            }
        }

        protected override void ServiceComplete(object args)
        {
            UpdateStatusReport("Adding element to depiction");
            var elementsEnum = args as IEnumerable<IElement>;
            if (elementsEnum == null) return;
            var elements = elementsEnum.ToList();
            if (elements.Any() && DepictionAccess.DStory != null)
            {
                if(sourceOwner != null)
                {
                    foreach(var elem in elements)
                    {
                        //is it cheaper to keep the actual element or just the id (memory wise)
                        sourceOwner.AddManagedElementId(elem.ElementId);
                    }
                }
                var story = DepictionAccess.DStory;
                //For now allow multiple elevations
                story.AddElements(elements);
                story.AddRevealerWithElements("Elevation - File", elements);
            }
        }


        private string GetRequestedFileName(ICartRect area, Dictionary<string, object> parameters)
        {
            depictionRegion = area;
            string url = "";
            if (parameters.ContainsKey("url"))
                url = parameters["url"].ToString();

            if (parameters.ContainsKey("units"))
                elevationUnit = parameters["units"].ToString() ?? "m";
            string layerName = "";
            if (parameters.ContainsKey("layerName"))
                layerName = parameters["layerName"].ToString() ?? "";

            string imageFormat = "GeoTIFF";
            if (parameters.ContainsKey("format"))
                imageFormat = parameters["format"].ToString();

            string request = "";
            if (parameters.ContainsKey("request"))
                request = parameters["request"].ToString() ?? "";

            var descriptor = "";
            if (parameters.ContainsKey("name"))
                descriptor = parameters["name"].ToString() ?? "";

            var rawData = GetWcsDataSetFromPartialInfo(area, url, layerName, "");
            var fullName = cacheService.GetCacheFullStoragePath(area + ".tiff");
            var gtiff = Gdal.GetDriverByName("GTIFF");
            var outData = gtiff.CreateCopy(fullName, rawData, 0, new string[0], null, null);

            rawData.Dispose();
            gtiff.Dispose();
            outData.Dispose();
            return fullName;
        }
        static internal Dataset GetWcsDataSetFromUrl(string wcsRequest)
        {
            return Gdal.Open(wcsRequest, Access.GA_ReadOnly);

        }
        static internal Dataset GetWcsDataSetFromPartialInfo(ICartRect roi, string wmsAddress, string layerName, string wmsVersion)
        {
//            http://depictioninc.com/wcs/?service=wcs&AcceptVersions=1.1.0&request=GetCapabilities
            // -122.398367,47.574302,-122.259551,47.630626
                //-122.4999999999,47.5,-122.25,47.75
            // http://depictioninc.com/wcs/?STYLES=&VERSION=1.0.0&WIDTH=626&CRS=EPSG:4269&HEIGHT=927&REQUEST=GetCoverage&FORMAT=GeoTIFF&COVERAGE=depiction:test&EXCEPTIONS=application/vnd.ogc.se_xml&SERVICE=WCS&SRS=EPSG:4326&BBOX=-122.398367,47.574302,-122.259551,47.630626&TRANSPARENT=TRUE

            var requestedWidth = GetImageWidth(roi, 30);//512);
            var requestedHeight = GetImageHeight(roi, 30);//512);
            var resX = 30;
            var resY = 30;
            var serviceType = "service=WCS";
            var wmsVersionString = "&version=1.0.0";
            var requestType = "&request=GetCoverage";
            var transparent = "&TRANSPARENT=TRUE";
            var exceptions = "&EXCEPTIONS=application/vnd.ogc.se_xml";
            if (!string.IsNullOrEmpty(wmsVersion))
            {
                wmsVersionString = string.Format("&version{0}", wmsVersion);
            }

            var layer = string.Format("&COVERAGE={0}", layerName);//should be layers but for depiction it seems to be coverage
            var srs = "&CRS=EPSG:4269";
            var bbox = string.Format("&bbox={0},{1},{2},{3}", roi.Left, roi.Bottom, roi.Right, roi.Top);
            var style = "&Styles=";
            var format = "&FORMAT=GeoTIFF";
            if (!wmsAddress.EndsWith("?")) wmsAddress += "?";
            var widthheight = string.Format("&width={0}&height={1}", requestedWidth, requestedHeight);
            var resxy = string.Format("&resx={0}&resy={1}", resX, resY);//doesnt seem to work so added the w/h hack from 1.4
            var request = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}", wmsAddress, serviceType, wmsVersionString, requestType,
                                       layer, srs, bbox, style, format, widthheight, exceptions, transparent);
            return GetWcsDataSetFromUrl(request);

        }
        #region hacks from 1.4
        private const double METERS_PER_LATITUDE = 111317.1;
        private static double MetersPerLongitude(double latitude)
        {
            return METERS_PER_LATITUDE * Math.Cos(latitude / 180.0 * Math.PI);
        }
        private static int GetImageWidth(ICartRect box, double mpp)
        {
            double mperLon = MetersPerLongitude(box.Bottom);
            double degreeDiff = Math.Abs(box.Left - box.Right);

            double distance = mperLon * degreeDiff;
            double width = distance / mpp;
            return (int)width;
        }

        private static int GetImageHeight(ICartRect box, double mpp)
        {
            double degreeDiff = Math.Abs(box.Top - box.Bottom);

            double height = (METERS_PER_LATITUDE * degreeDiff) / mpp;

            return (int)height;
        }
        #endregion
    }
}