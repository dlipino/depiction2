﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.Utilities;
using Depiction2.Core.Service;
using OSGeo.GDAL;

[assembly: InternalsVisibleTo("Depiction2.GdalExtension.UnitTests")]
namespace Depiction2.GdalExtension.WxS
{
    public class GdalWmsBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        public const string roiKey = "Area";
        public const string typeKey = "elementType";
        public const string serviceAddressKey = "url";//comes from legacy quickstart parameter key
        public const string layerNameKey = "layerName";
        private string layerName = String.Empty;

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var info = args as Dictionary<string, object>;
            if (info == null) return null;
            var area = GetProperty(roiKey, info) as ICartRect;
            var address = GetProperty(serviceAddressKey, info).ToString();
            layerName = GetProperty(layerNameKey, info).ToString();

            var memData = GetTopLevelWmsOverView(area, address, layerName, null);
            var geoTrans = new double[6];
            memData.GetGeoTransform(geoTrans);

            var tempFolder = new DepictionFolderService(false);
            var driver = Gdal.GetDriverByName("jpeg");
            var outFile = Path.Combine(tempFolder.FolderName, "final.jpg");
            var pngData = driver.CreateCopy(outFile, memData, 0, new string[0], null, null);
            driver.Dispose();
            pngData.Dispose();
            var image = DepictionImageResourceDictionary.GetBitmapFromFile(outFile);
            var geom = DepictionGeometryService.CreateGeometry(area);//may not be accurate, depends on the spatial transform
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(null, geom);


            var simpleFName = Path.GetFileName(outFile);
            if (DepictionAccess.DStory == null)
            {
                //TODO throw some sort of error
            }
            else
            {
                DepictionAccess.DStory.AllImages.AddImage(simpleFName, image, true);
            }
            element.ImageResourceName = simpleFName;

            element.VisualState = ElementVisualSetting.Image;
            return element;
        }

        protected override void ServiceComplete(object args)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var elem = args as IElement;
            if (elem == null) return;
            story.AddElement(elem, true);
        }
        #region helpers
        private object GetProperty(string key, Dictionary<string, object> source)
        {
            if (source.ContainsKey(key))
            {
                return source[key];
            }
            return null;
        }
        //negligable error checking
        static internal Dataset GetTopLevelWmsOverView(ICartRect roi, string wmsAddress, string layerName, string wmsVersion)
        {
            //http://ows.terrestris.de/osm/service?

            var requestedWidth = 512;
            var requestedHeight = 512;
            var serviceType = "service=WMS";
            var wmsVersionString = "&version=1.1.1";
            if (!string.IsNullOrEmpty(wmsVersion))
            {
                wmsVersionString = string.Format("&version{0}", wmsVersion);
            }

            var requestType = "&request=GetMap";
            var layer = String.Format("&layers={0}", layerName);
            var srs = "&SRS=EPSG:4326";
            var bbox = String.Format("&bbox={0},{1},{2},{3}", roi.Left, roi.Bottom, roi.Right, roi.Top);
            var style = "&Style=";
            var format = "&FORMAT=image/jpeg";
            if (!wmsAddress.EndsWith("?")) wmsAddress += "?";
            var widthheight = String.Format("&width={0}&height={1}", requestedWidth, requestedHeight);
            var request = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", wmsAddress, serviceType, wmsVersionString, requestType,
                                       layer, srs, bbox, style, format, widthheight);
            var data = Gdal.Open(request, Access.GA_ReadOnly);
            var srcGeoTransform = new double[6];
            data.GetGeoTransform(srcGeoTransform);
            var srcProjection = data.GetProjection();
            var srcBandCount = data.RasterCount;

            var srcBandType = data.GetRasterBand(1).DataType;
            var srcOverviewCount = data.GetRasterBand(1).GetOverviewCount();
            //Turn the data into something that can be written to file correctly
            var options = new string[0];
            var mem = Gdal.GetDriverByName("MEM");

            var xOffset = 0;
            var yOffset = 0;
            Dataset memData = null;// mem.Create("", dstWidth, dstHeight, 0, srcBandType, options);
            var dstGeoTransform = new double[6];
            dstGeoTransform[0] = srcGeoTransform[0] + (xOffset * srcGeoTransform[1]) + (yOffset * srcGeoTransform[2]);
            dstGeoTransform[1] = srcGeoTransform[1];
            dstGeoTransform[2] = srcGeoTransform[2];
            dstGeoTransform[3] = srcGeoTransform[3] + (xOffset * srcGeoTransform[4]) + (yOffset * srcGeoTransform[5]);
            dstGeoTransform[4] = srcGeoTransform[4];
            dstGeoTransform[5] = srcGeoTransform[5];

            //Get the top most overview (least detail) for now ie zoom level
            for (int i = 1; i < srcBandCount + 1; i++)
            {
                var srcBand = data.GetRasterBand(i);

                var ovBand = srcBand.GetOverview(srcOverviewCount - 1);
                var x = ovBand.XSize;
                var y = ovBand.YSize;

                if (memData == null)
                {
                    memData = mem.Create("", x, y, 0, srcBandType, options);
                }
                memData.SetGeoTransform(dstGeoTransform);
                memData.SetProjection(srcProjection);
                var buffer = new byte[x * y];
                ovBand.ReadRaster(xOffset, yOffset, x, y, buffer, x, y, 0, 0);
                memData.AddBand(ovBand.DataType, options);
                var memBand = memData.GetRasterBand(i);
                memBand.WriteRaster(0, 0, x, y, buffer, x, y, 0, 0);
            }

            data.Dispose();

            return memData;
        }
        #endregion
    }
}