﻿using System;
using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.GdalExtension.EnvironmentSetters;

namespace Depiction2.GdalExtension.WxS
{
    [RegionSourceMetadata(Name = "GdalWmsImporter", DisplayName = "Generic Wms importer", Author = "Depiction Inc.",
        ShortName = "Wms Importer", GenericName="WMSElementImporter")]
    public class GdalWmsImporterExtension : IRegionSourceExtension
    {
        public GdalWmsImporterExtension()
        {
            OGREnvironment.SetOGREnvironment();
            GDALEnvironment.SetGDALEnvironment();
        }
        public void Dispose()
        {

        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> parameters)
        {
            var dict = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            dict.Add(GdalWmsBackgroundService.roiKey, regionOfInterest);

            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    dict.Add(item.Key, item.Value);
                }
            }

            var service = new GdalWmsBackgroundService();
            var name = string.Format("Starting wfs gather");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }
    }
}