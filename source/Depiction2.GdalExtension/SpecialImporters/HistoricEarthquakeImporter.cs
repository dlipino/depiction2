using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.GdalExtension.EnvironmentSetters;
using OSGeo.OGR;

namespace Depiction2.GdalExtension.SpecialImporters
{
    //    [DepictionNonDefaultImporterMetadata("USGSRecentEarthquakeImporter",
    //        Description = "Recent magnitude 1 or greater earthquakes from around the world recorded by the U.S. Geological Survey (USGS) using a variety of global data sources and partners. Additional info: <a href=\"http://earthquake.usgs.gov/eqcenter/\">http://earthquake.usgs.gov/eqcenter/</a>",
    //        DisplayName = "Earthquakes -- Past 7 Days (USGS)",
    //        ImporterSources = new[] { InformationSource.Web },AddonPackage="Preparedness")]
    public class HistoricEarthquakeImporter : IRegionSourceExtension
    {
        public HistoricEarthquakeImporter()
        {
            OGREnvironment.SetOGREnvironment();
            GDALEnvironment.SetGDALEnvironment();
        }
        public void Dispose()
        {
        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> inParameters)
        {
            //            http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-age.kmz 
            //        http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week_age.kml
            var parameters = new Dictionary<string, object>();
            parameters.Add(EarthQuakeElementProviderBackground.roiKey, regionOfInterest);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new EarthQuakeElementProviderBackground();
            var importerName = "Earthquakes -- Past 7 Days (USGS)";
            var name = string.Format(importerName);
            var elementTypeKey = EarthQuakeElementProviderBackground.typeKey;
            var epicenterType = "Depiction.Plugin.Epicenter";
            if (parameters.ContainsKey(elementTypeKey))
            {
                parameters[elementTypeKey] = epicenterType;
            }
            else
            {
                parameters.Add(elementTypeKey, epicenterType);
            }
            var nameKey = "name";
            if (parameters.ContainsKey(nameKey))
            {
                parameters[nameKey] = name;
            }
            else
            {
                parameters.Add(nameKey, name);
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }

    public class EarthQuakeElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        private string name = "Earthquakes -- Past 7 Days (USGS)";
        public const string roiKey = "Area";
        public const string typeKey = "elementType";
        public const string serviceAddressKey = "url";//comes from legacy quickstart parameter key
        public const string layerNameKey = "layerName";

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            //            var wfsDataSource = Ogr.Open(complateAddress, 0);
            //            var earthQuakeKMZLocation = new Uri("http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-age.kmz");//old and depricated
            var earthQuakeKMZLocation = new Uri(" http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week_age.kml");

            var fileName = "1.0_week_age.kml";
            UpdateStatusReport(string.Format("Downloading earthquake information"));
            var fullName = Path.Combine(DepictionAccess.PathService.DepictionCacheHomeDirectory, fileName);
            using (var wbClient = new WebClient())
            {
                wbClient.DownloadFile(earthQuakeKMZLocation, fullName);
            }

            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var elementType = parameters[typeKey].ToString();
            var regionBounds = parameters[roiKey] as ICartRect;
            if (File.Exists(fullName))
            {
                UpdateStatusReport(string.Format("Reading {0}.", fileName));
                var prototypes = ProcessKMZKMLFileIntoProperties(fullName, regionBounds);
              
                UpdateStatusReport(string.Format("Finished reading {0} elements,preparing to display.", prototypes.Count));
                return prototypes;
            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            if (DepictionAccess.DStory == null) return;
            var prototypes = args as List<Dictionary<string, object>>;
            if (prototypes == null || !prototypes.Any())
            {
                //                var message =
                //                    string.Format(
                //                        "Could not add {0} The source may be unavailable or there is no data for the requested area", name);
                //                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return;
            }

            DepictionAccess.DStory.UpdateRepo(prototypes,null,null);
        }

        #endregion
        #region helpers
        static public List<Dictionary<string, object>> ProcessKMZKMLFileIntoProperties(string filePath, ICartRect depictionRegion)
        {
            var data = Ogr.Open(filePath, Convert.ToInt32(true));
            var layerc = data.GetLayerCount();
            var r = data.GetRefCount();
            var templateProperties = new List<Dictionary<string, object>>();


            for (int i = 0; i < layerc; i++)
            {
                var layer = data.GetLayerByIndex(i);
                Console.WriteLine(layer.GetName() + " Count: " + layer.GetFeatureCount(i));
                var feature = layer.GetNextFeature();
                var gc = layer.GetGeometryColumn();
                var ld = layer.GetLayerDefn();

                while (feature != null)
                {
                    var featureData = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
                    var dr = feature.GetDefnRef();
                    var fc = dr.GetFieldCount();
                    var rc = dr.GetReferenceCount();

                    featureData.Add(PropertyKeys.geometryKey, feature.GetGeometryRef());
                    var s1 = feature.GetStyleString();
                    Console.WriteLine("Field count " + fc + " Style: " + s1);

                    var id = feature.GetFID();
                    for (int j = 0; j < fc; j++)
                    {
                        var fref = feature.GetFieldDefnRef(j);
                        var fType = fref.GetFieldType();
                        var fName = fref.GetName();
                        Console.WriteLine(fType);
                        switch (fType)
                        {
                            case FieldType.OFTInteger:
                                featureData.Add(fName, feature.GetFieldAsInteger(j));
                                break;
                            case FieldType.OFTReal:
                                featureData.Add(fName, feature.GetFieldAsDouble(j));
                                break;
                            case FieldType.OFTString:
                            case FieldType.OFTWideString:
                                featureData.Add(fName, feature.GetFieldAsString(j));
                                break;
                            case FieldType.OFTDate:
                            case FieldType.OFTDateTime:

                                var s = feature.GetFieldAsString(j);
                                if (!string.IsNullOrEmpty(s))
                                {
                                    int year, month, day, hour, min, sec, flag;
                                    feature.GetFieldAsDateTime(j, out year, out month, out day, out hour, out min, out sec, out flag);
                                    featureData.Add(fName, new DateTime(year, month, day, hour, min, sec));
                                }

                                break;
                        }
                    }
                    templateProperties.Add(featureData);
                    feature = layer.GetNextFeature();
                }

            }
            data.Dispose();
            return templateProperties;
        }
        #endregion
    }
}