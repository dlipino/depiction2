using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.GdalExtension.WxS;

namespace Depiction2.GdalExtension.SpecialImporters
{
    //data source no longer works
//    [RegionSourceMetadata(Name = "HistoricalWildfires2002_2009", DisplayName = "Wildfires 2002-2010 (USGS)", Author = "Depiction Inc.",
//        Description = "Names and perimeters of recent wildfires in the U.S., provided by the Geospatial Multi-Agency Coordination Group or GeoMAC and hosted by the U.S. Geological Survey (USGS). Additional info: <a href=\"http://rmgsc.cr.usgs.gov/rmgsc/apps.shtml\">http://rmgsc.cr.usgs.gov/rmgsc/apps.shtml</a>",
//    ShortName = "Historic Wildfires", RequiresParameters = false)]
    public class HistoricalWildfireImporter : IRegionSourceExtension
    {

        public void Dispose()
        {

        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add(GdalWmsBackgroundService.roiKey, regionOfInterest);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new GdalWmsBackgroundService();
            var name = string.Format("Wildfires 2002-2010 (USGS)");
            var layerNames =
                "2002fires,2003fires,2004fires,2005fires,2006fires,2007fires,2008fires,2009fires,2010fires,prevmod,prevper,prevperlab,25,hms,modis,actperim";
            parameters.Add(GdalWmsBackgroundService.layerNameKey, layerNames);
   
            if (!parameters.ContainsKey("name"))
            {
                parameters.Add("name", name);
            }
            //this also comes from the quickstart
           // var url = "http://wildfire.cr.usgs.gov/wmsconnector/com.esri.wms.Esrimap/geomac_wms";
            var url = "http://wildfire.cr.usgs.gov/ArcGIS/services/geomac_dyn/MapServer/WMSServer";
            if(!parameters.ContainsKey("url"))
            {
                parameters.Add("url", url);
            }

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }
}