﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Depiction2.API.Extension.Tools.Geometry;
using Depiction2.Base.Geo;
using Geometry = OSGeo.OGR.Geometry;

namespace Depiction2.GdalExtension.GeometryItems
{
    [GeometryExtensionMetadata(DisplayName = "Geometries according to GDAL", Name = "GeometryGdal", Author = "Depiction Inc.", ExtensionPackage = "Default")]
    public class GeometryGdalFactory : IGeometryExtension
    {
        public void Dispose()
        {
        }

        public IDepictionGeometry CreateGeometry(object geometrySource)
        {
            if (geometrySource == null) return new GeometryGdal();
            if (geometrySource is Point)
            {
                return new GeometryGdal((Point)geometrySource);
            }
            if (geometrySource is IEnumerable<Point>)
            {
                return new GeometryGdal((IEnumerable<Point>)geometrySource);
            }
            if (geometrySource is string)
            {
                return new GeometryGdal((string)geometrySource);
            }
            if (geometrySource is Geometry)
            {
                return new GeometryGdal((Geometry)geometrySource);
            }
            return null;
        }

        public StreamGeometry CreateStreamGeometryFromDepictionGeometry(IDepictionGeometry geometry)
        {
            return GeometryGdalUtilities.CreateDrawStreamGeometryFromGeometryWrapList(geometry);
        }

    }
}