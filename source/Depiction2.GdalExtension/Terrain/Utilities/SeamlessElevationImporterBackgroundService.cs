﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Depiction2.API;
using Depiction2.API.Tools;
using Depiction2.API.Utilities;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Extensions;
using Depiction2.Terrain.Utilities;

namespace Depiction2.Terrain.ImportExtensions
{
    class SeamlessElevationImporterBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        private string elevationUnit = "m";
        private int currentPiece;
        private int numberOfPieces;

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Seamless Elevation Data");
            return true;            
        }

        protected override object ServiceAction(object args)
        {
            List<IElement> elements = null;
            var argDictionary = args as Dictionary<string, object>;
            if (argDictionary == null) return null;
            if (!argDictionary.ContainsKey("RegionBounds")) return null;
            var area = argDictionary["RegionBounds"] as ICartRect;
            if (area == null) return null;
            string layer = "N3F01";
            string requestUrl =
               string.Format("http://extract.cr.usgs.gov/requestValidationServiceClient/sampleRequestValidationServiceProxy/getTiledDataDirectURLs2.jsp?TOP={0}&BOTTOM={1}&LEFT={2}&RIGHT={3}&LAYER_IDS={4}&JSON=true", 
               area.Top, area.Bottom, area.Left, area.Right, layer);


            var buffer = DownloadString(requestUrl, 0);
            //TerrainData terrainData = new TerrainData();

            TileCacheService tileCacheService = new TileCacheService("SeamlessElevation");

            var collection = Regex.Matches(buffer, @"""DOWNLOAD_URL"":""(?'url'[^""]*)""");

            currentPiece = 1;
            numberOfPieces = collection.Count;
            foreach (Match match in collection)
            {
                string url = match.Groups["url"].Value;
                string filename = Regex.Match(url, @"FNAME=(.*zip)").Groups[1].Value;
                string filePath = tileCacheService.GetCacheFullStoragePath(filename);
                if (!File.Exists(filePath))
                {
                    UpdateStatusReport(string.Format("Downloading data piece {0} of {1}", currentPiece, numberOfPieces));
                    DownloadFile(url, filePath, 0);
                }

                var archiveDir = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, Path.GetFileNameWithoutExtension(filename));
                if (!Directory.Exists(archiveDir))
                {
                   if(!ZipHelpers.UnzipFileToDirectory(filePath, archiveDir))
                   {
                       UpdateStatusReport("Error unzipping file");
                       return null;
                   }
                }
                var files = Directory.GetFiles(archiveDir, "w001001.adf", SearchOption.AllDirectories);

                var elevationDataFileName = files[0];

                {
                    try
                    {
                        var elevationElement = elements != null ? elements[0] : null;
                        var terrainConverter = new CoverageDataConverter(elevationDataFileName, elevationUnit, area, true, elevationElement);

                        UpdateStatusReport(string.Format("Converting data piece {0} of {1} to elevation", currentPiece, numberOfPieces));
                        elements = terrainConverter.ConvertDataToElements();
                        foreach (var element in elements)
                        {
                            element.AddTag("Elevation - Seamless");
                        }
                        //AddElevationElementToDepiction(elements);
                    }
                    catch
                    {
                        return null;
                    }
                }
                currentPiece++;
            }
            return elements;
        }

        protected override void ServiceComplete(object args)
        {
            UpdateStatusReport("Adding element to depiction");
            var elementsEnum = args as IEnumerable<IElement>;
            if (elementsEnum == null) return;

            var elements = elementsEnum.ToList();
            AddElevationElementToDepiction(elements);
        }

        private void AddElevationElementToDepiction(List<IElement> elements)
        {
            UpdateStatusReport("Adding element to depiction");
            if (elements == null) return;
            if (elements.Any() && DepictionAccess.DStory != null)
            {
                var story = DepictionAccess.DStory;
                //For now allow multiple elevations
                story.AddElements(elements);
                
                story.AddRevealerWithElements("Elevation - Seamless", elements);
            }
        }

        private void DownloadFile(string url, string fileName, int exceptionCount)
        {
            var webClient = new WebClient();

            UpdateStatusReport(string.Format("Downloading data piece {0} of {1}", currentPiece, numberOfPieces));
            try
            {
                webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
                webClient.DownloadFile(url, fileName);
            }
            catch (WebException)
            {

                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    DownloadFile(url, fileName, exceptionCount + 1);
            }

        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            UpdateStatusReport(string.Format("Downloading data piece {0} of {1} ({2}%)", currentPiece, numberOfPieces, e.ProgressPercentage));
        }

        private string DownloadString(string url, int exceptionCount)
        {

            
            var webClient = new WebClient();
            string buffer = "";

            try
            {
                
                buffer = webClient.DownloadString(url);
            }
            catch (WebException)
            {
                //if (ServiceStopRequested) return null;
                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    return DownloadString(url, exceptionCount + 1);
            }
            //if (ServiceStopRequested) return null;
            return buffer;
        }
    }
}
