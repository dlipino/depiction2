﻿using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;

namespace Depiction2.GdalExtension.Terrain.Utilities
{
    public class CoverageDataConverter
    {
        static private readonly object addElevationToStoryLock = new object();
        #region fields

        // use a static synchronizing object so that all instances of this converter can be accessed one at a time
        private readonly string _fileName;
        private readonly bool _cropGridBeforeProjection;
        private readonly IElement _elevationElement;
        private string elevationUnit;

        #endregion

        #region constructor

        public CoverageDataConverter(string elevationFileName, string unit, bool cropGridBeforeProjection, IElement elevationElement)
        {
            _cropGridBeforeProjection = cropGridBeforeProjection;
            _elevationElement = elevationElement;
            _fileName = elevationFileName;
            elevationUnit = unit;
        }

        #endregion

        public List<IElement> ConvertDataToElements()
        {
            var elements = new List<IElement>();
            //            lock (addElevationToStoryLock)
            //            {
            var elevation = _elevationElement ?? CreateElementWithElevationProperty();
            if (elevation == null) return null;
            RestorableTerrainData terrain = null;
            if (_fileName != string.Empty)
            {
                var elevationProp = elevation.GetDepictionProperty("Terrain");
                if (elevationProp != null)
                {
                    terrain = elevationProp.Value as RestorableTerrainData;
                }
                if (terrain == null)
                {
                    return null;
                }

                bool downSampled;
                //
                //TRAC #2183 fix:
                //when individual files are added via Add/File/Elevation data, you want
                //the spacing of the loaded file to supercede the spacing of the existing terrain
                //data in your story
                //That has been the convention in Depiction -- new terrain spacing overrides the existing resolution.

                //Why can't we have multiple terrain data, and have elements 'hook' into the one they want?

                var tempTerrainData = terrain.AddTerrainDataFromFile(_fileName, "", elevationUnit, true,
                                                                     out downSampled, false, _cropGridBeforeProjection /*don't use existing grid spacing -- use new one*/);
                if (tempTerrainData == null) return null;
                if (!terrain.AddTerrainData(tempTerrainData)) return null;

                var geom = DepictionGeometryService.CreateGeometry(terrain.TerrainBounds);
                elevation.UpdatePrimaryPointAndGeometry(null, geom);
                terrain.RestoreProperty(true, elevation);//will this even work?
                elevation.VisualState = ElementVisualSetting.Image;

            }
            elements.Add(elevation);
            //            }
            return elements;
        }

        /// <summary>
        /// Returns the single elevation element. 
        /// If one has not been created yet, it creates a new one with
        /// a well-formed and empty Terrain object.
        /// </summary>
        /// <returns></returns>
        private IElement CreateElementWithElevationProperty()
        {
            System.Console.WriteLine("creating elevation element");
            IElement elevation = null;
            if (DepictionAccess.TemplateLibrary != null)
            {

                var prototype = DepictionAccess.TemplateLibrary.FindElementTemplate("Depiction.Plugin.Elevation");
                elevation = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(prototype, null);
            }
            if (elevation == null)
            {
                elevation = new DepictionElement("Generic.Elevation");
            }
            var terrainLocal = new RestorableTerrainData { HasGoodData = false };
            //Initial value was gutting used, but didn't work very well because of referencing and restoring.
            var terrainProperty = new ElementProperty("Terrain", "Terrain", terrainLocal);
            elevation.AddUserProperty("Terrain", "Terrain", terrainLocal);
            elevation.AddTag("Elevation");
            return elevation;
        }
    }
}