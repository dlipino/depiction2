﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

namespace Depiction2.Terrain.Utilities.Wxs
{ /// <summary>
    /// This request may time out after the duration of the Timeout property has elapsed, 
    /// but the only thing that matters for the user is whether it succeeded or was cancelled.  
    /// Use the HasSucceeded and IsCanceled properties.
    /// </summary>
    public class DepictionWebRequest : Depiction2.Terrain.Utilities.Wxs.IRepeatableRequest
    {
        private bool finishing;
        private int msDownCounter;
        private WebRequest webRequest;
        private string lastErrorMessage;
        public string LastErrorMessage { get { return lastErrorMessage; } }

        public DepictionWebRequest(string requestor, string connection, string intermediateFileName)
        {
            Response = new Response();
            Response.ResponseFile = intermediateFileName;
            Requestor = requestor;
            Connection = connection;
        }

        public bool IsHandlingRequest { get; private set; }

        #region IRepeatableRequest Members

        public string Requestor { get; private set; }

        public string Connection { get; private set; }

        public Response Response { get; private set; }

        private const int Timeout = 30000;

        public void Execute()
        {
            IsHandlingRequest = true;
//            if (!DepictionInternetConnectivityService.IsInternetAvailable) return;
            webRequest = WebRequest.Create(Connection);
            webRequest.Timeout = Timeout;
            webRequest.BeginGetResponse(FinishRequest, null);
            const int sleepMSec = 500;
            msDownCounter = Timeout;
            while (RequestIsInProcess())
            {
                Thread.Sleep(sleepMSec);
                msDownCounter -= sleepMSec;
            }

            if (IsHandlingRequest && !finishing)
            {
                webRequest.Abort();
            }
        }

        public void Cancel()
        {
            if (finishing) return;
            Response.IsRequestCanceled = true;
            if (webRequest != null)
                webRequest.Abort();
        }

        #endregion

        private bool RequestIsInProcess()
        {
            if (!IsHandlingRequest)
                return false;
            return !IsHandlingRequest || msDownCounter > 0;
        }

        private void FinishRequest(IAsyncResult requestResult)
        {
            lock (this)
            {
                try
                {
                    if (!IsHandlingRequest)
                        return;
                    finishing = true;
                    if (Response.IsRequestCanceled || msDownCounter <= 0)
                    {
                        // Do not do anything.
                    }
                    else if (requestResult.IsCompleted)
                    {
                        using (WebResponse webResponse = webRequest.EndGetResponse(requestResult))
                        {
                            if (webResponse != null)
                            {
                                using (Stream stream = webResponse.GetResponseStream())
                                {
                                    copyStreamToFile(stream, Response.ResponseFile);
                                    stream.Close();
                                    Response.IsRequestSuccessful = true;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //depictionAccessInternalHandle.NotificationService.SendNotification(string.Format(StringConstants.UnableToRetrieveThingPlusErrorMessage, requestor, ex.Message));
                    lastErrorMessage = ex.Message;
                    finishing = false;
                    Response.IsRequestSuccessful = false;
                }
                IsHandlingRequest = false;
            }
        }

        private static void copyStreamToFile(Stream stream, string destination)
        {
            Debug.Assert(stream != null);

            if (string.IsNullOrEmpty(destination)) return;

            using (var bs = new BufferedStream(stream))
            {
                using (FileStream os = File.OpenWrite(destination))
                {
                    var buffer = new byte[2 * 4096];
                    int nBytes;
                    while ((nBytes = bs.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        os.Write(buffer, 0, nBytes);
                    }
                    os.Flush();
                    os.Close();
                }
            }
        }
    }
}
