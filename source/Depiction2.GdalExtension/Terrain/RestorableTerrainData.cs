﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Xml;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.Base.Utilities;

[assembly: InternalsVisibleTo("Depiction2.GdalExtension.UnitTests")]
namespace Depiction2.GdalExtension.Terrain
{
    [DataContract]
    public class RestorableTerrainData : ICoverage, IRestorable, IXmlLoadable
    {
        public TerrainData ModifiedTerrainData { get; private set; }
        private TerrainData originalTerrainData;

        private readonly object addTerrainDataFromFileLock = new object();

        #region Constructor

        public RestorableTerrainData()
        {
            Init(new TerrainData());
        }
        public RestorableTerrainData(TerrainData terrainData)
        {
            Init(terrainData);
            CreateTerrainPlusStructure();
        }

        private void Init(TerrainData initTerrainData)
        {
            originalTerrainData = initTerrainData;
            ModifiedTerrainData = new TerrainData();
        }
        #endregion

        #region Properties

        public bool HasGoodData
        {
            get { return originalTerrainData.HasGoodData; }
            set { originalTerrainData.HasGoodData = value; }
        }

        public ICartRect TerrainBounds
        {
            get
            {
                if (originalTerrainData == null) return null;
                var rect = new CartRect(originalTerrainData.GetTopLeftPos(), originalTerrainData.GetBottomRightPos());
                return rect;
            }
        }
        #endregion

        #region private heper methods
        private string GetColorMapFile(string tempDir)
        {
            //legacy file
            var cmtName = "default_relative.cmt";
            var helperFile = string.Empty;
            var assembly = Assembly.GetCallingAssembly();

            var dllLocation = Path.GetDirectoryName(assembly.Location);
            if (!string.IsNullOrEmpty(dllLocation))
            {
                var resourceColorMapFile = Path.Combine(dllLocation, "Resources", cmtName);
                if (File.Exists(resourceColorMapFile))
                {
                    helperFile = resourceColorMapFile;
                }
            }
            if (string.IsNullOrEmpty(helperFile))
            {
                //i guess we have to create the file
                var fullOutCmt = Path.Combine(tempDir, cmtName);
                var fileText = "colormap1" + Environment.NewLine +
                               "relative: 1" + Environment.NewLine +
                               "size 5" + Environment.NewLine +
                               "elev 0.000000 color 32 176 32" + Environment.NewLine +
                               "elev 0.000000 color 64 224 64" + Environment.NewLine +
                               "elev 0.000000 color 224 192 160" + Environment.NewLine +
                               "elev 0.000000 color 224 128 16" + Environment.NewLine +
                               "elev 0.000000 color 224 224 224";

                using (var writer = new StreamWriter(fullOutCmt))
                {
                    writer.Write(fileText);
                }
                helperFile = fullOutCmt;
            }
            return helperFile;
        }
        public bool GenerateTerrainVisual(string tempDir, string fileName)
        {
            if (string.IsNullOrEmpty(tempDir) || string.IsNullOrEmpty(fileName)) return false;
            if (!Directory.Exists(tempDir)) return false;
            var tempFileName = Path.Combine(tempDir, fileName);
            //how lame, since the gdal stuff is not sure to be there pull it from resource

            var helperFile = GetColorMapFile(tempDir);

            bool createdVisual = SaveTo24BitRGB(tempFileName, 90, helperFile);
            return createdVisual;
        }

        private bool CreateTerrainPlusStructure()
        {
            lock (originalTerrainData)
            {
                if (originalTerrainData == null || !originalTerrainData.HasGoodData) return false;
            }
            var tempTerrainPlusStructure = new TerrainData();
            if (tempTerrainPlusStructure.CreateFromExisting(originalTerrainData))
            {
                lock (ModifiedTerrainData)
                {
                    ModifiedTerrainData = tempTerrainPlusStructure;
                }
                return true;
            }
            return false;
        }

        internal static float GetElevationAndReadoutFromDataGrid(TerrainData dataGrid, Point location, string units, int precision, out string readout)
        {
            readout = ", Elevation: ";
            float elevation;
            if (!dataGrid.HasElevation(location))
            {
                readout += "Unknown";
                return 0;
            }

            try
            {
                if (dataGrid.HasNoData(location) || !dataGrid.HasGoodData)
                {
                    readout += "No Data";
                    return 0;
                }
                elevation = dataGrid.GetClosestElevationValue(location);

                if (units == "ft") elevation = elevation * 3.2808399F;

                var prec = precision;
                readout += String.Format("{0} {1}",
                                         elevation.ToString("F" + prec), units);
                return elevation;
            }
            catch (Exception e)
            {
                //DepictionExceptionHandler.HandleException("Unable to get elevation data for location", e, false);
                readout = "";
                return 0;
            }
        }

        /// <summary>
        /// Create a bounding box that is a wee bit bigger than the region bounds,
        /// so that we can then crop the erroneous edge pixels that come from the data source.
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="paddingPercent"></param>
        /// <returns></returns>
        private static Rect CreateDilatedBoundingBox(Rect bbox, double paddingPercent)
        {
            double offsetDistanceX = Math.Abs(bbox.Left - bbox.Right) * paddingPercent;
            double offsetDistanceY = Math.Abs(bbox.Top - bbox.Bottom) * paddingPercent;
            //pad the region bbox with paddingPercent*100% on all sides so WCS request can accomodate
            //cropping and warping from Geo to Mercator, etc.
            //
            return new Rect(bbox.X - offsetDistanceX, bbox.Y - offsetDistanceY, bbox.Width + (2 * offsetDistanceX), bbox.Height + (2 * offsetDistanceY));
            //            return new Rect
            //            {
            //                Top = bbox.Top + offsetDistanceY,
            //                Bottom = bbox.Bottom - offsetDistanceY,
            //                Left = bbox.Left - offsetDistanceX,
            //                Right = bbox.Right + offsetDistanceX,
            //                MapImageSize = bbox.MapImageSize
            //            };
        }
        #endregion

        #region public helper methods

        public void RestoreProperty(bool notifyChange, IElement propertyOwner)
        {
            CreateTerrainPlusStructure();
            //            if (DepictionAccess.DStory == null) return;
            //            var elevation =
            //                DepictionAccess.DStory.AllElements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.Elevation"));

            //            if (elevation != null)
            //            {
            lock (propertyOwner)
            {
                var folderService = new DepictionFolderService(false);
                var tempFolder = folderService.FolderName;
                var imageName = string.Format("baseTerrain-{0}.jpg", propertyOwner.ElementId);
                var createdVisual = GenerateTerrainVisual(tempFolder, imageName);
                if (createdVisual)
                {
                    var fullName = Path.Combine(tempFolder, imageName);
                    var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullName);
                    if (DepictionAccess.DStory != null)
                    {
                        DepictionAccess.DStory.AllImages.AddImage(imageName, image, true);
                    }
                    propertyOwner.ImageResourceName = imageName;
                }
                folderService.Close();
            }
            //            }
        }

        public void ClearVisual()
        {
        }
        /// <summary>
        /// Get coverage value at a given location in string format
        /// </summary>
        public string GetValueForDisplay(Point location, MeasurementSystem measureSystem, int precision)
        {
            float trueElevation;
            float modifiedElevation;
            string units = string.Empty;
            string trueElevationReadout;
            string modifiedElevationReadout;

            if (measureSystem.Equals(MeasurementSystem.Imperial))
                units = "ft";
            else
                units = "m";

            trueElevation = GetElevationAndReadoutFromDataGrid(originalTerrainData, location, units, precision, out trueElevationReadout);
            modifiedElevation = GetElevationAndReadoutFromDataGrid(ModifiedTerrainData, location, units, precision,
                                                                   out modifiedElevationReadout);
            if (trueElevation == modifiedElevation)
                return trueElevationReadout;
            var prec = precision;
            var elevationChange = modifiedElevation - trueElevation;
            return String.Format("{0} ({1} {2} structure)",
                                 modifiedElevationReadout,
                                 elevationChange.ToString("F" + prec),
                                 units);
        }

        public bool LineOfSight(Point point1, Point point2, ArrayList intersectionList)
        {
            double x1, y1, x2, y2;
            x1 = point1.X;
            y1 = point1.Y;
            x2 = point2.X;
            y2 = point2.Y;
            var success = originalTerrainData.LineOfSight(x1, y1, x2, y2, intersectionList);
            return success;
        }
        public void Create(double south, double north, double east, double west, int width, int height, bool floatgrid, int defaultValue, string wktString, bool updateVisual)
        {
            originalTerrainData = new TerrainData();
            originalTerrainData.CreateFromExisting(south, north, east, west, width, height, floatgrid, defaultValue, wktString);
            CreateTerrainPlusStructure();
        }
        public bool AddTerrainData(TerrainData data)
        {
            lock (originalTerrainData)
            {
                Init(data);
            }
            return CreateTerrainPlusStructure();
        }
        #endregion

        #region Terran file methods

        public void SaveToGeoTiff(string filename)
        {
            ModifiedTerrainData.SaveToGeoTiff(filename);
        }

        public bool SaveTo24BitRGB(string filename, int quality, string colormapFileName)
        {
            //            DepictionAccess.NotificationService.SendNotification(
            //                string.Format("Generating visual for elevation data. Please wait..."), 10);
            return ModifiedTerrainData.SaveTo24BitRGB(filename, quality, colormapFileName);
        }

        //        public bool AddFromFileWithoutExtents(string filename, double top, double bottom, double right, double left)
        //        {
        //            var result = originalTerrainData.AddFromFileWithoutExtents(filename, top, bottom, right, left);
        //            return result;
        //        }
        public TerrainData AddTerrainDataFromFile(string filename, string coordSystem, string elevationUnit, bool showDownSampleMessage, out bool downSampled, bool useExistingGridSpacing, bool cropGridBeforeProjection)
        {
            TerrainData tempTerrainData;
            downSampled = false;

            lock (addTerrainDataFromFileLock)
            {
                tempTerrainData = new TerrainData();
                tempTerrainData.LoadElevationFile(filename);
                return tempTerrainData;

                //                if (!terrainData.HasGoodData)
                //                {
                //                    tempTerrainData = new TerrainData();
                //                    //
                //                    //this terraindata object gets 'written' over by the Add method when we
                //                    //add actual data from a file...so, initial dimensions are not important -- the latlong bounds are important, though.
                //                    //
                //                    var regionExtent = new Rect();//DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
                //                    var paddedBox = CreateDilatedBoundingBox(regionExtent, 0.1);
                //                    var creationSuccess = tempTerrainData.CreateFromExisting(paddedBox.Bottom,
                //                                                                             paddedBox.Top,
                //                                                                             paddedBox.Right,
                //                                                                             paddedBox.Left, 5,
                //                                                                             5, true, -32768, "", 0);
                //                    //tempTerrainData is in Mercator, which is default if you don't pass a projection file name
                //                    if (!creationSuccess)
                //                    {
                //                        //DepictionAccess.NotificationService.SendNotification(
                //                        //    string.Format("Unable to add elevation data due to insufficient memory"), 10);
                //                        return null;
                //
                //                    }
                //
                //                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
                //                    //crop it to the region bounding box
                //                    tempTerrainData.CropElevationData(regionExtent);
                //                    if (downSampled && showDownSampleMessage)
                //                    {
                //                        // DepictionAccess.NotificationService.SendNotification(
                //                        //     string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));
                //
                //                    }
                //                    return tempTerrainData;
                //                }
                //terrainData already exists
                tempTerrainData = new TerrainData();
                if (tempTerrainData.CreateFromExisting(originalTerrainData))
                {
                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
                    if (downSampled && showDownSampleMessage)
                    {
                        //DepictionAccess.NotificationService.SendNotification(
                        //    string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));

                    }
                    return tempTerrainData;
                }
                return null;
            }
        }
        #endregion

        #region TerrainData methods

        public int GetGridWidthInPixels()
        {
            return ModifiedTerrainData.GetGridWidthInPixels();
        }

        public int GetGridHeightInPixels()
        {
            return ModifiedTerrainData.GetGridHeightInPixels();
        }

        public double GetGridResolution()
        {
            return ModifiedTerrainData.GetGridResolution();
        }

        public int GetRow(double lat)
        {
            return ModifiedTerrainData.GetRow(lat);
        }

        public int GetColumn(double lon)
        {
            return ModifiedTerrainData.GetColumn(lon);
        }

        public double GetLatitude(int row)
        {
            return ModifiedTerrainData.GetLatitude(row);
        }

        public double GetLongitude(int col)
        {
            return ModifiedTerrainData.GetLongitude(col);
        }

        public float GetValueAtGridCoordinate(int col, int row)
        {
            return ModifiedTerrainData.GetElevationValueFromGridCoordinate(col, row);

        }

        public float GetElevationValueClosest(Point latitudeLongitude)
        {
            return ModifiedTerrainData.GetClosestElevationValue(latitudeLongitude);
        }

        public void SetValueAtGridCoordinate(int col, int row, float value)
        {
            ModifiedTerrainData.SetElevationValue(col, row, value);
        }

        public float GetInterpolatedElevationValue(Point latitudeLongitude)
        {
            return ModifiedTerrainData.GetInterpolatedElevationValue(latitudeLongitude);
        }

        public float GetConvolvedValue(int col, int row, int kernel)
        {
            return ModifiedTerrainData.GetConvolvedValue(col, row, kernel);
        }

        #endregion

        #region TerrainPlusStructure methods

        public Point GetTopLeftPosition()
        {
            return ModifiedTerrainData.GetTopLeftPos();
        }

        public Point GetBottomRightPosition()
        {
            return ModifiedTerrainData.GetBottomRightPos();
        }

        public string GetProjectionInWkt()
        {
            return ModifiedTerrainData.GetProjectionInWkt();
        }

        public bool IsFloatMode()
        {
            return ModifiedTerrainData.IsFloatMode();
        }

        #endregion

        #region json serializtion items

        [DataMember]
        private List<string> TerrainFiles { get; set; }
        [DataMember]
        private List<Point3D> TerrainDifferences { get; set; }
        [OnSerializing]
        internal void SaveSourceFiles(StreamingContext context)
        {
            TerrainFiles = new List<string>(originalTerrainData.SourceFiles);
            //Copy the files that were used to create the roadgraph to the serialization folder
            var tempFolder = StorySerializationService.Instance.DataSerializationFolder;
            if (string.IsNullOrEmpty(tempFolder) || !Directory.Exists(tempFolder))
            {
                DepictionMessageService.Instance.AddStoryMessage("Could not save terrain data.", 2);
                return;
            }
            //First look in the story files folder
            if (DepictionAccess.PathService == null)
            {
                DepictionMessageService.Instance.AddStoryMessage("Unable to find current story save path; unable to save terrian data.", 2);
                return;
            }
            var currentStoryFolder = DepictionAccess.PathService.CurrentStoryDataDirectory;
            var missingFiles = new List<string>();
            foreach (var sourceFile in originalTerrainData.SourceFiles)
            {
                var fullOrigFile = Path.Combine(currentStoryFolder, sourceFile);
                var fullCopyFile = Path.Combine(tempFolder, sourceFile);
                if (!File.Exists(fullOrigFile))
                {
                    missingFiles.Add(sourceFile);
                    continue;
                }
                File.Copy(fullOrigFile, fullCopyFile, true);
            }

            if (missingFiles.Count != 0)
            {
                DepictionMessageService.Instance.AddStoryMessage("Unable to save all data files.", 2);
            }
            //Find the differences between the terrains
            TerrainDifferences = ModifiedTerrainData.FindDifferenceWith(originalTerrainData);

        }
        [OnDeserialized]
        internal void CreateTerrainFromFiles(StreamingContext context)
        {
            var dataFolder = StorySerializationService.Instance.DataSerializationFolder;
            if (!Directory.Exists(dataFolder))
            {
                DepictionMessageService.Instance.AddApplicationMessage("Could not find current story directory");
                return;
            }
            var missingFile = new List<string>();
            foreach (var dataSource in TerrainFiles)
            {
                var loadedFilePath = Path.Combine(dataFolder, dataSource);

                if (!File.Exists(loadedFilePath))
                {
                    missingFile.Add(dataSource);
                    continue;
                }
                originalTerrainData.LoadElevationFile(loadedFilePath);
            }

            CreateTerrainPlusStructure();
            ModifiedTerrainData.SetElevationValues(TerrainDifferences);

        }
        #endregion

        #region XmlSerialization stuff

        public void ReadFromXml(XmlReader reader)
        {
            if (originalTerrainData == null)
            {
                originalTerrainData = new TerrainData();
            }
            var difList = new List<Point3D>();
            var helperFileDirectory = StorySerializationService.Instance.DataSerializationFolder;

            reader.ReadToFollowing("terrainData");
            if (reader.Name.Equals("terrainData"))
            {
                reader.Read();
                if (reader.Name.Equals("hasData"))
                {
                    originalTerrainData.HasGoodData = reader.ReadElementContentAsBoolean();
                    if (!originalTerrainData.HasGoodData)
                    {
                        //does something about the xaml
                        return;
                    }
                }
                string fileName = string.Empty;
                if (reader.Name.Equals("elevationFileName"))
                {
                    fileName = reader.ReadElementContentAsString();
                }
                var elevName = Path.Combine(helperFileDirectory, fileName);
                originalTerrainData.LoadElevationFile(elevName);
                reader.ReadEndElement();
            }
            if (reader.Name.Equals("TerrainDifferences"))
            {
                var type = typeof(Point3D);
                var serializer = new DataContractSerializer(type);
                reader.ReadStartElement();
                while (reader.IsStartElement("terrainDifference"))
                {
                    reader.ReadStartElement("terrainDifference");

                    var ret = (Point3D)serializer.ReadObject(reader, false);
                    reader.ReadEndElement();
                    difList.Add(ret);
                }

                CreateTerrainPlusStructure();
                ModifiedTerrainData.SetElevationValues(difList);
                reader.ReadEndElement();
            }
            else
                CreateTerrainPlusStructure();
        }


        #endregion
    }
}