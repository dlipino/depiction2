﻿using System;
using System.Windows;
using Depiction2.API.Extension.Tools.Projection;
using Depiction2.Base.Geo;
using Depiction2.GdalExtension.EnvironmentSetters;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction2.GdalExtension.ProjectionItems
{
    [ProjectionExtensionMetadata(DisplayName = "Projections according to GDAL", Name = "ProjectionGdal", Author = "Depiction Inc.", ExtensionPackage = "Default")]
    public class GdalProjector : IProjectionExtension
    {
        private string sourceCS = string.Empty;
        private string targetCS = string.Empty;
        private SpatialReference sourceSpatial;
        private SpatialReference targetSpatial;
        private CoordinateTransformation sourceToTargetTransform;
        private CoordinateTransformation targetToSourceTransform;

        #region properties

        public string SourceCoordinateSystem
        {
            get { return sourceCS; }
        }


        public string TargetCoordinateSystem
        {
            get { return targetCS; }
        }

        #endregion

        #region construction/destruction
        public GdalProjector()
        {
            OGREnvironment.SetOGREnvironment();
        }
        public void Dispose()
        {
            if (sourceToTargetTransform != null) sourceToTargetTransform.Dispose();
            if (targetToSourceTransform != null) targetToSourceTransform.Dispose();
            if (sourceSpatial != null) sourceSpatial.Dispose();
            if (targetSpatial != null) targetSpatial.Dispose();
        }

        #endregion

        #region methods
        public void UpdateCoordinateSystems(string sourceSystem, string targetSystem)
        {
            var updateSource = !Equals(sourceCS, sourceSystem) && !string.IsNullOrEmpty(sourceSystem);

            if (updateSource)
            {
                sourceCS = sourceSystem;
                ClearTransformsAndSpatial(sourceSpatial);
                sourceSpatial = null;
                sourceSpatial = new SpatialReference(sourceCS);
            }
            var updateTarget = !Equals(targetCS, targetSystem) && !string.IsNullOrEmpty(targetSystem);
            if (updateTarget)
            {
                targetCS = targetSystem;
                ClearTransformsAndSpatial(targetSpatial);
                targetSpatial = null;
                targetSpatial = new SpatialReference(targetCS);
            }
            if (!updateSource && !updateTarget) return;
            targetToSourceTransform = new CoordinateTransformation(targetSpatial, sourceSpatial);
            sourceToTargetTransform = new CoordinateTransformation(sourceSpatial, targetSpatial);

        }
        private void ClearTransformsAndSpatial(SpatialReference sr)
        {
            if (targetToSourceTransform != null)
            {
                targetToSourceTransform.Dispose();
                targetToSourceTransform = null;
            }
            if (sourceToTargetTransform != null)
            {
                sourceToTargetTransform.Dispose();
                sourceToTargetTransform = null;
            }
            //order might be important?
            if (sr != null)
            {
                sr.Dispose();
            }
        }
        #region point transforms
        public Point PointFromSourceToTarget(double x, double y)
        {
            return TransformPoint(new Point(x, y), sourceToTargetTransform);
        }
        public Point PointFromTargetToSource(double x, double y)
        {
            return TransformPoint(new Point(x, y), targetToSourceTransform);
        }
        private Point TransformPoint(Point p, CoordinateTransformation transform)
        {
            if (transform == null) return p;

            var transPoints = new[] { p.X, p.Y };
            try
            {
                transform.TransformPoint(transPoints);
            }
            catch
            {
                transPoints[0] = double.NaN;
                transPoints[1] = double.NaN;
            }
            return new Point(transPoints[0], transPoints[1]);
        }
        #endregion

        #region geometry transforms
        //So sketchy to do this. One doens'st know if the original is getting modified or
        //if it is a new clone.
        public IDepictionGeometry GeometryCloneFromSourceToTarget(IDepictionGeometry geometry)
        {
            var clone = geometry.Clone();
            if (!GeometryFromSourceToTarget(clone)) return geometry;
            return clone;
        }

        public bool GeometryFromSourceToTarget(IDepictionGeometry geometry)
        {
            return TransformGeometry(geometry, sourceToTargetTransform);
        }

        private bool TransformGeometry(IDepictionGeometry geometry, CoordinateTransformation transform)
        {
            if (transform == null) return false;
            var clonedGeom = geometry.GeometryObject as Geometry;
            if (clonedGeom == null) return false;
            try
            {
                clonedGeom.Transform(sourceToTargetTransform);//TransformTo??
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion
        #endregion
    }
}