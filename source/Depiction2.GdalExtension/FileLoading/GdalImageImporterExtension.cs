﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Service;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.GeometryItems;
using Depiction2.GdalExtension.ProjectionItems;
using OSGeo.GDAL;

namespace Depiction2.GdalExtension.FileLoading
{
    [DepictionFileLoaderMetadata(Name = "GDALFileImporter", Description = "Gdal image importer",
     DisplayName = "Gdal File Importer", Author = "Depiction Inc.",
     SupportedExtensions = new[] { ".tiff", ".tif" })]
    public class GdalImageImporterExtension : IFileLoaderExtension
    {

        public GdalImageImporterExtension()
        {
            GDALEnvironment.SetGDALEnvironment();
        }
        public void Dispose()
        {

        }
        
        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName, ICartRect boundingRect)
        {
            var GdalImageType = "Gdal.Plugin.Image";
            var scaffold = defaultTemplate;
            if (defaultTemplate == null && DepictionAccess.TemplateLibrary != null)
            {
                scaffold = DepictionAccess.TemplateLibrary.FindElementTemplate(GdalImageType);
            }
            var dict = new Dictionary<string, object>();
            dict.Add("FileName", sourceLocation);
            dict.Add("ElementType", scaffold);
            var service = new GdalImageReadingService();
            var shortName = Path.GetFileName(sourceLocation);
            var name = string.Format("Loading image file: {0}", shortName);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }
    }

    public class GdalImageReadingService : BaseDepictionBackgroundThreadOperation
    {

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var info = args as Dictionary<string, object>;
            if (info == null) return null;
            var fileName = info["FileName"] as string;
            if (!File.Exists(fileName)) return null;

            var gatheredInfo = GetInformationFromGdalFile(fileName);
            var scaffKey = "ElementType";
            gatheredInfo.Add(scaffKey, info[scaffKey]);
            gatheredInfo.Add("VisualState", ElementVisualSetting.Image);
            return gatheredInfo;
        }

        protected override void ServiceComplete(object args)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var dataObject = args as Dictionary<string, object>;
            if (dataObject == null) return;
            var dataList = new List<Dictionary<string, object>>
                                   {
                                       dataObject
                                   };


            story.UpdateRepo(dataList, null, null);
        }

        //    adfGeoTransform[0] /* top left x */
        //    adfGeoTransform[1] /* w-e pixel resolution */
        //    adfGeoTransform[2] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[3] /* top left y */
        //    adfGeoTransform[4] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[5] /* n-s pixel resolution */
        public Dictionary<string, object> GetInformationFromGdalFile(string fileName)
        {
            var dsOriginal = Gdal.Open(fileName, Access.GA_ReadOnly);
            var geoCoords = new double[6];
            dsOriginal.GetGeoTransform(geoCoords);
            var pixWidht = dsOriginal.RasterXSize;
            var pixHeight = dsOriginal.RasterYSize;
            var geoPointList = new List<Point>();
            var rotation = 0d;

            var origProj = dsOriginal.GetProjection();
            var projector = new GdalProjector();
            projector.UpdateCoordinateSystems(origProj, CoordinateSystemService.DepictionGeographicCoordinateSystem);

            if (!(geoCoords[0].Equals(0) && geoCoords[2].Equals(0) && geoCoords[3].Equals(0) && geoCoords[4].Equals(0)))
            {
                var topLeft = new Point(geoCoords[0], geoCoords[3]);
                var botRight = new Point(topLeft.X + (pixWidht * geoCoords[1]), topLeft.Y + (pixHeight * geoCoords[5]));

                var tlConv = projector.PointFromSourceToTarget(topLeft.X, topLeft.Y);
                var brConv = projector.PointFromSourceToTarget(botRight.X, botRight.Y);
                //            var geoString = string.Format("Polygon (({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", tlConv.X, tlConv.Y, brConv.X, brConv.Y);
                geoPointList = new List<Point>
                                   {
                                       tlConv,
                                       new Point(brConv.X, tlConv.Y),
                                       brConv,
                                       new Point(tlConv.X, brConv.Y),
                                       tlConv
                                   };
                rotation = geoCoords[4];

            }

            var propObjectList = new Dictionary<string, object>();
            propObjectList.Add("ElementGeometry", new GeometryGdal(geoPointList));
            propObjectList.Add("ImageRotation", rotation);

            var image = DepictionImageResourceDictionary.GetBitmapFromFile(fileName);
            var simpleFName = Path.GetFileName(fileName);
            if (DepictionAccess.DStory == null)
            {
                //TODO throw some sort of error
            }
            else
            {
                DepictionAccess.DStory.AllImages.AddImage(simpleFName, image, true);
            }

            propObjectList.Add("ImageResourceName", simpleFName);

            return propObjectList;
        }
    }
}