﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Service;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.GeometryItems;
using Depiction2.Utilities.Memory;
using OSGeo.OGR;
using OSGeo.OSR;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.GdalExtension.FileLoading
{

//    [DepictionFileImporterMetadata(new[] { ".shp" }, DisplayName = "OGR File Importer", Name = "OGRFileImporter",
//         Description = "OGR Vector File reader.")]
    [DepictionFileLoaderMetadata(Name = "OGRFileImporter", Description = "OGR Vector File reader.",
 DisplayName = "OGR File Importer", Author = "Depiction Inc.",SupportedExtensions = new[] { ".shp" })]
    public class OGRFileImporterExtension : IFileLoaderExtension
    {

        #region testing to make sure this thing is allows depiction to exit
        public OGRFileImporterExtension()
        {
            OGREnvironment.SetOGREnvironment();
        }
        public void Dispose()
        {
     
        }

        ~OGRFileImporterExtension()
        {
        }

        #endregion

        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName,ICartRect sourceBounds)
        {
            var start = MemUtil.GetProcessMem();
            if (DepictionAccess.DStory == null)
            {
                //Log the error that cant have things without the story
                return;
            }


            var parameters = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            parameters.Add("Area", sourceBounds);
            parameters.Add("FileName", sourceLocation);
            parameters.Add("ElementType", defaultTemplate);
            //            parameters.Add("RegionBounds", depictionRegion);
            parameters.Add("PropertyIdName", idPropertyName);
            bool findProperties = !string.IsNullOrEmpty(idPropertyName);
            parameters.Add("CreateProperties", findProperties);


            var operationThread = new OgrFileReadingService();
            var name = string.Format("Reading : {0}", Path.GetFileName(sourceLocation));

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);

            //            var startTime = DateTime.Now;
            //            UpdateDepictionWithFile(fileName, scaffold, DepictionAccess.DefaultGCS, true, idPropertyName);

            //            var endtime = DateTime.Now;
            //            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(string.Format("{0} : Time to create elements for {1}", elapsedTime, fileName));
            //            //            if (elementList == null)//Give some sot o
            //            //            {
            //            //                //Give some error, although im pretty sure something along the way should have thown something
            //            //                return;
            //            //            }
            //            //
            //            //            startTime = DateTime.Now;
            //            //            DepictionAccess.DStory.AddElements(elementList);
            //            //            DepictionAccess.DStory.MainDisplayer.AddDisplayElementsWithMatchingIds(elementList.Select(t => t.ElementId).ToList());
            //
            //            endtime = DateTime.Now;
            //            elapsedTime = string.Format("It took {0} to create elements", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(elapsedTime);
            //            Debug.WriteLine("Mem after element create");
            //            MemUtil.GetProcessMem();
            //            startTime = DateTime.Now;
            //            //the draw thing
            //            endtime = DateTime.Now;
            //            elapsedTime = string.Format("It took {0} to send elements to binding ", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(elapsedTime);
            //            Debug.WriteLine("Mem after element to bindings");
            //            var end = MemUtil.GetProcessMem();
            //            Debug.WriteLine(string.Format("Mem used full set {0}", end - start));
        }

        //generally used for testing



        #region element creation helpers
        static public HashSet<string> GetOgrFeaturePropertiesNames(Feature feature, FeatureDefn layerFieldDef)
        {
            var propScaffoldList = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            for (int iField = 0; iField < feature.GetFieldCount(); iField++)
            {
                FieldDefn fdef = layerFieldDef.GetFieldDefn(iField);
                //if a field is not set, do not import it
                if (feature.IsFieldSet(iField))
                {
                    propScaffoldList.Add(fdef.GetNameRef());
                }
            }
            return propScaffoldList;
        }

        static public Dictionary<string,object> GetOgrFeatureProperties(Feature feature, FeatureDefn layerFieldDef)
        {
            var propScaffoldList = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            for (int iField = 0; iField < feature.GetFieldCount(); iField++)
            {
                FieldDefn fdef = layerFieldDef.GetFieldDefn(iField);
                //if a field is not set, do not import it
                if (feature.IsFieldSet(iField))
                {
                    var type = feature.GetFieldType(iField);

                    switch (type)
                    {
                        case FieldType.OFTReal:
                             propScaffoldList.Add(fdef.GetNameRef(), feature.GetFieldAsDouble(iField));
                            break;
                        case FieldType.OFTInteger:
                            propScaffoldList.Add(fdef.GetNameRef(), feature.GetFieldAsInteger(iField));
                            break;
//                        case FieldType.OFTDate:
//                            propScaffold.TypeString = typeof(DateTime).Name;
//                            break;
                        default:
                            propScaffoldList.Add(fdef.GetNameRef(), feature.GetFieldAsString(iField).Trim());
                            break;
                    }
                }
            }
            return propScaffoldList;
        }

        public HashSet<string> FindPropertyIntersections(string fileName)
        {
            DataSource dataSource = null;
            try
            {
                dataSource = Ogr.Open(fileName, 0);
            }
            catch
            {
                //log an error
                return null;
            }
            HashSet<string> propList = null;
            var layerCount = dataSource.GetLayerCount();
            for (var i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                FeatureDefn layerFieldDef = layer.GetLayerDefn();
                //More than one layers with geometries?
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var props = GetOgrFeaturePropertiesNames(nextFeature, layerFieldDef);
                    if (propList == null)
                    {
                        propList = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
                        propList.UnionWith(props);
                    }
                    else
                    {
                        propList.IntersectWith(props);
                    }
                }
            }
            return propList;
        }

        #endregion
    }

    public class OgrFileReadingService : BaseDepictionBackgroundThreadOperation
    {
        private string elementFile;
        private string elementScaffold;
        private bool _createProperties;
        private string propIdName;
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            elementFile = parameters["FileName"].ToString();
            elementScaffold = parameters["ElementType"].ToString();
            _createProperties = (bool)parameters["CreateProperties"];
            propIdName = parameters["PropertyIdName"].ToString();
            //            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (!File.Exists(elementFile)) return null;

            if (string.IsNullOrEmpty(elementScaffold)) return null;

            return CreateElementPropertiesFromFile(elementFile, _createProperties, propIdName);

        }

        protected override void ServiceComplete(object args)
        {
            Console.WriteLine("Starting the update");
            var startTime = DateTime.Now;
            var scaffold = DepictionAccess.TemplateLibrary.FindElementTemplate(elementScaffold);
            var geomAndProps = args as List<Dictionary<string, object>>;
            if (geomAndProps == null) return;
            var story = DepictionAccess.DStory;
            if (story == null) return;
            story.UpdateRepo(geomAndProps, propIdName, scaffold);

            var endtime = DateTime.Now;
            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);
        }


        //        public void UpdateDepictionWithFile(string elementFile, IElementScaffold elementScaffold, string targetProjSystem, bool createProperties, string propIdName)
        //        {
        //            //Not sure what is up with this
        //            if (!File.Exists(elementFile))
        //            {
        //                elementFile = elementFile.Remove(0, 2);
        //            }
        //            if (!File.Exists(elementFile)) return;
        //            if (elementScaffold == null) return;
        //            string projSystem;
        //            var geomAndProps = CreateGeometryWrapsFromFile(elementFile, targetProjSystem, out projSystem, createProperties, propIdName);
        //            if (geomAndProps == null) return;
        //            var story = DepictionAccess.DStory;
        //            if (story == null) return;
        //            story.UpdateRepo(geomAndProps, propIdName, elementScaffold);
        //        }

//        public List<IElement> CreateElementsFromFile(string elementFile, IElementScaffold elementScaffold, string targetProjSystem, bool createProperties)
//        {
//            //Not sure what is up with this
//            if (!File.Exists(elementFile))
//            {
//                elementFile = elementFile.Remove(0, 2);
//            }
//            if (!File.Exists(elementFile)) return null;
//            if (elementScaffold == null) return null;
//            string projSystem;
//            string idProp = string.Empty;
//            var geomAndProps = CreateGeometryWrapsFromFile(elementFile, targetProjSystem, out projSystem, createProperties, idProp);
//            if (geomAndProps == null) return null;
//
//            var elementList = new List<IElement>();
//            foreach (var geomAndProp in geomAndProps)
//            {
//                var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(elementScaffold, geomAndProp.geometry);
//                ElementAndElemTemplateService.UpdateElementWithProperties(element, geomAndProp.properties, null);
//                elementList.Add(element);
//            }
//            return elementList;
//        }

        public List<Dictionary<string,object>> CreateElementPropertiesFromFile(string fileName, bool createProperties, string idName)
        {
            DataSource dataSource = null;
            var geometries = new List<Dictionary<string, object>>();
            try
            {
                dataSource = Ogr.Open(fileName, 0);
            }
            catch
            {
                //log an error
                return null;
            }

            var layerCount = dataSource.GetLayerCount();
            var depictionSR = new SpatialReference(CoordinateSystemService.DepictionGeographicCoordinateSystem);
            
            for (var i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                FeatureDefn layerFieldDef = layer.GetLayerDefn();
                //More than one layers with geometries?
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var geomAndProp = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
                    if (createProperties)
                    {
                        geomAndProp = OGRFileImporterExtension.GetOgrFeatureProperties(nextFeature, layerFieldDef);
                    }
                    var mainGeom = nextFeature.GetGeometryRef();
                    try
                    {

                        mainGeom.TransformTo(depictionSR);
                        geomAndProp.Add("ElementGeometry",new GeometryGdal(mainGeom));
                        
                    }
                    catch (Exception ex)
                    {
                        //TODO log the error
                    }
                    geometries.Add(geomAndProp);
                }
            }
            return geometries;
        }
    }
}
