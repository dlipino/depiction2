﻿using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.GdalExtension.Tiling
{
//    [TilerExtensionMetadata(Name = "SeamlessOrthoimageryTileImporter", Description = "Imagery (HRO Seamless)", DisplayName = "Imagery (The National Map)", TilerType = TileImageTypes.Satellite)]
    public class TNMLargeScaleImageryTiler : SeamlessTilerBase
    {
        public TNMLargeScaleImageryTiler()
        {
            tileCacheService = new TileCacheService(SourceName);
        }

        public override string SourceName
        {
            get { return "The National Map Imagery"; }
        }

        public override string DisplayName
        {
            get
            {
                return "Imagery (The National Map)";
            }
        }

        public override string LegacyImporterName
        {
            get { return "TNMOrthoimageryTileImporter"; }
        }


        protected override string URL { get { return "http://raster.nationalmap.gov/ArcGIS/services/TNM_Large_Scale_Imagery_Overlay/MapServer/WMSServer"; } }
        protected override string layerName { get { return "0,1,2"; } }
        protected override int MaxZoomLevel { get { return 20; } }

        public override string CacheLocation
        {
            get { return "TNMOrthoimageryTileImporter"; }
        }
    }



}
