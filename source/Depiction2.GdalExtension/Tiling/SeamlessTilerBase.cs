﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Geo;
using Depiction2.Base.Service.Tiling;
using Depiction2.Core.Geo;
using Depiction2.Utilities;
using OSGeo.GDAL;

namespace Depiction2.GdalExtension.Tiling
{
    public abstract class SeamlessTilerBase : ITilerExtension
    {
        protected abstract string URL { get; }
        protected abstract string layerName { get; }
        public abstract string DisplayName { get; }
        public abstract string SourceName { get; }
        public abstract string LegacyImporterName { get; }
        public int PixelWidth { get { return 256; } }
        public bool DoesOwnCaching { get { return false; } }
        protected abstract int MaxZoomLevel { get; }
        protected const double EARTHCIRCUM = EARTHRADIUS * 2.0 * Math.PI;
        protected const double EARTHHALFCIRC = EARTHCIRCUM / 2;
        protected const double EARTHRADIUS = 6378137;
        protected TileCacheService tileCacheService;
        public abstract string CacheLocation { get; }
        

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            double widthInDegrees = TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel) - TileColToTopLeftLong(tileToGet.Column, zoomLevel);
            double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);
            var tile = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                            null,
                            new LatitudeLongitudeDepictionBase(TileRowToTopLeftLat(tileToGet.Row, zoomLevel), TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                            new LatitudeLongitudeDepictionBase(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
            tile.FetchStrategy = TileFetchStrategy;
            return tile;
        }

        public string CreateTileFileName(ICartRect bounds)
        {
            return CreateTileFileName(SourceName, bounds);
        }
        private string CreateTileFileName(string name, ICartRect bounds)
        {
            var fileName = name;
            var boundString = string.Format("_{0:0.##}_{1:0.##}_to_{2:0.##}_{3:0.##}", bounds.Top, bounds.Left, bounds.Bottom, bounds.Right);
            fileName += boundString;
            return fileName + ".jpg";
        }
        public TileImageTypes TileImageType { get { return TileImageTypes.Satellite; } }


        public int GetZoomLevel(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i = 1;
            double width = boundingBox.Right - boundingBox.Left;

            for (i = 1; i < MaxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right) * (1 << i) / 360D;
                if (tilesAcross > minTilesAcross) break;
            }
            return i;
        }

        private static double RadToDeg(double d)
        {
            return d * 180 / Math.PI;
        }


        private static double DegToRad(double d)
        {
            return d * Math.PI / 180.0;
        }


        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoomLevel);
            double metersX = (eqMetersPerTile * col) - EARTHHALFCIRC;
            double lonRad = metersX / EARTHRADIUS;
            return RadToDeg(lonRad);
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double metersY = EARTHHALFCIRC - (row * eqMetersPerTile);
            double latRad = Math.Atan(Math.Sinh(metersY / EARTHRADIUS));
            double latDeg = RadToDeg(latRad);
            return latDeg;
        }


        public int LongitudeToColAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / ((1 << zoom));
            double metersX = EARTHRADIUS * DegToRad(latLong.Longitude);
            var x = (int)((EARTHHALFCIRC + metersX) / eqMetersPerTile);
            return x;
        }

        public int LatitudeToRowAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            double latRad = DegToRad(latLong.Latitude);
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double prj = Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad));
            double metersY = EARTHRADIUS * prj;
            var y = (int)((EARTHHALFCIRC - metersY) / eqMetersPerTile);
            return y;
        }


        public IList<TileModel> GetTiles(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.Top, boundingBox.Right), zoomLevel);
            int col = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.Bottom, boundingBox.Left), zoomLevel);
            int lastCol = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.Top, boundingBox.Right), zoomLevel);
            int lastRow = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.Bottom, boundingBox.Left), zoomLevel);
            if (row < 0) row = 0;
            if (col < 0) col = 0;
//            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
//            int row = LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
//            int col = LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
//            int lastCol = LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);
//            int lastRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);

            foreach (var tileToGet in tilesToGet)
            {
                var tile = GetTileModel(tileToGet, zoomLevel);
                tiles.Add(tile);
            }

            return tiles;
        }

        private byte[] TileFetchStrategy(TileModel model)
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable)
            {
                return null;
            }

            var tlP = new Point(Math.Round(model.TopLeft.Longitude,6), Math.Round(model.TopLeft.Latitude,6));
            var brP = new Point(Math.Round(model.BottomRight.Longitude,6), Math.Round(model.BottomRight.Latitude,6));
            var cartBounds = new CartRect(tlP, brP);
            var outFile = model.TileIndex.Row + "_" + model.TileIndex.Col + "_" + model.TileIndex.TileZoomLevel + ".jpg";//tileSource.CacheLocation + "_" + 
//            var outFile = tileCacheService.GetCacheFullStoragePath(CreateTileFileName(cartBounds));
            if (!File.Exists(outFile))
            {
                var wmsAddress = URL;
                if (!URL.EndsWith("?")) wmsAddress = URL + "?";
                var requestedWidth = 256;
                var requestedHeight = 256;
                var serviceType = "service=WMS";
                var version = "&version=1.1.1";
                var requestType = "&request=GetMap";
                var layer = string.Format("&layers={0}", layerName);
                var srs = "&SRS=EPSG:4326";
                //              var bbox = string.Format("&bbox={0},{1},{2},{3}", roi.Left, roi.Bottom, roi.Right, roi.Top);
                var bbox = string.Format("&bbox={0},{1},{2},{3}", tlP.X, brP.Y, brP.X, tlP.Y);
                var style = "&Styles=";
                var format = "&FORMAT=image/jpeg";
                var widthheight = string.Format("&width={0}&height={1}", requestedWidth, requestedHeight);
                var request = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", wmsAddress, serviceType, version,
                                            requestType,
                                            layer, srs, bbox, style, format, widthheight);
                Dataset requestData = null;
                try
                {
                    //TODO not sure why, but every so often (and even frequently) this gives a file does not exist exception.
                    requestData = Gdal.Open(request, Access.GA_ReadOnly);
                }catch(Exception ex)
                {
                    return null;
                }
                var srcGeoTransform = new double[6];
                requestData.GetGeoTransform(srcGeoTransform);
                var srcProjection = requestData.GetProjection();
                var srcBandCount = requestData.RasterCount;

                var srcBandType = requestData.GetRasterBand(1).DataType;
                var srcOverviewCount = requestData.GetRasterBand(1).GetOverviewCount();
                //Turn the data into something that can be written to file correctly
                var options = new string[0];
                var mem = Gdal.GetDriverByName("MEM");

                var xOffset = 0;
                var yOffset = 0;
                OSGeo.GDAL.Dataset memData = null; // mem.Create("", dstWidth, dstHeight, 0, srcBandType, options);
                var dstGeoTransform = new double[6];
                dstGeoTransform[0] = srcGeoTransform[0] + (xOffset*srcGeoTransform[1]) + (yOffset*srcGeoTransform[2]);
                dstGeoTransform[1] = srcGeoTransform[1];
                dstGeoTransform[2] = srcGeoTransform[2];
                dstGeoTransform[3] = srcGeoTransform[3] + (xOffset*srcGeoTransform[4]) + (yOffset*srcGeoTransform[5]);
                dstGeoTransform[4] = srcGeoTransform[4];
                dstGeoTransform[5] = srcGeoTransform[5];

                //Get the top most overview (least detail) for now ie zoom level
                for (int i = 1; i < srcBandCount + 1; i++)
                {
                    var srcBand = requestData.GetRasterBand(i);

                    var ovBand = srcBand.GetOverview(srcOverviewCount - 1);
                    var x = ovBand.XSize;
                    var y = ovBand.YSize;

                    if (memData == null)
                    {
                        memData = mem.Create("", x, y, 0, srcBandType, options);
                    }
                    memData.SetGeoTransform(dstGeoTransform);
                    memData.SetProjection(srcProjection);
                    var buffer = new byte[x*y];
                    ovBand.ReadRaster(xOffset, yOffset, x, y, buffer, x, y, 0, 0);
                    memData.AddBand(ovBand.DataType, options);
                    var memBand = memData.GetRasterBand(i);
                    memBand.WriteRaster(0, 0, x, y, buffer, x, y, 0, 0);
                }

                requestData.Dispose();
                var driver = Gdal.GetDriverByName("jpeg");
                var jpgImage = driver.CreateCopy(outFile, memData, 0, new string[0], null, null);
                driver.Dispose();
                jpgImage.Dispose();

            }
            return File.ReadAllBytes(outFile);
        }

        //<summary>
        //Returns the list of tiles to get ordered in increasing distance from the center
        //of the area being tiled.
        //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles =
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }

        public void Dispose()
        {
            
        }
    }
}