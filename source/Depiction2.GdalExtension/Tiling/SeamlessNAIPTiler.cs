﻿using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Service.Tiling;
using Depiction2.GdalExtension.EnvironmentSetters;

namespace Depiction2.GdalExtension.Tiling
{
//    [TilerExtensionMetadata(Name = "NAIPSeamlessImporter", Description = "Imagery (NAIP)", DisplayName = "Imagery (NAIP)", TilerType = TileImageTypes.Satellite)]
    public sealed class SeamlessNAIPTiler : SeamlessTilerBase
    {
        public SeamlessNAIPTiler()
        {
            OGREnvironment.SetOGREnvironment();
            GDALEnvironment.SetGDALEnvironment();

            tileCacheService = new TileCacheService(SourceName);
        }

        protected override string URL { get { return "http://raster.nationalmap.gov/ArcGIS/services/Orthoimagery/USGS_EDC_Ortho_NAIP/ImageServer/WMSServer"; } }

        protected override string layerName { get { return "0"; } }

        public override string DisplayName
        {
            get
            {
                return "Imagery (NAIP)";
            }
        }

        public override string SourceName
        {
            get { return "NAIP"; }
        }
        public override string LegacyImporterName
        {
            get { return "NAIPSeamlessImporter"; }
        }
        protected override int MaxZoomLevel { get { return 18; } }

        public override string CacheLocation
        {
            get { return "NAIPSeamlessImporter"; }
        }
    }

}