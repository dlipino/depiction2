﻿using System.Linq;
using Depiction2.Base.Utilities;
using Depiction2.Terrain.Terrain14;
using Depiction2.Terrain.Utilities;
using NUnit.Framework;

namespace Depiction2.Terrain.UnitTests.Utilities
{
    [TestFixture]
    public class CoverageDataConverterTest
    {
        [Test]
        public void GenericElevationFileLoad()
        {
            var fullFile = TestUtilities.GetFullFileName("SeattleElevation.tiff", string.Empty);
            var converter = new CoverageDataConverter(fullFile, "m", null, false, null);
            var elems = converter.ConvertDataToElements();
            Assert.IsTrue(elems.Any());
            var elevation = elems.FirstOrDefault();
            Assert.IsNotNull(elevation,"Elevation was null");
            var elevProp = elevation.GetDepictionProperty("Terrain");
            Assert.IsNotNull(elevProp,"Must have an elevation property");
            var elevVal = elevProp.Value as TerrainCoverage;
            Assert.IsNotNull(elevVal,"Must have good terrain coverage");
            var elevData = elevVal.TerrainBounds;
            Assert.IsNotNull(elevData, "Needs a geolocation");
            

        }
    }
}