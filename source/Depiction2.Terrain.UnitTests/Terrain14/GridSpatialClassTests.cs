﻿using System.IO;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Utilities;
using Depiction2.Terrain.Terrain14;
using Depiction2.Terrain.Utilities;
using NUnit.Framework;

namespace Depiction2.Terrain.UnitTests.Terrain14
{
    [TestFixture(Description = "Take from old Depiction so not sure how valid this is.")]
    public class GridSpatialClassTests
    {
        
        private static TerrainData GetTestTerrainData()
        {
            var coverageData = new TerrainData();
            coverageData.CreateFromExisting(47.4, 47.6, -122.2, -122.4, 3, 3, true, 0, "" /*default WGS84*/, 1);
            return coverageData;
        }

        [Test]
        public void CanCreateFloodCoverageGridFromTerrain()
        {
            var terrainData = GetTestTerrainData();
            var centerPoint = new Point(-122.3, 47.5);
            terrainData.SetElevationValue(10.4F, centerPoint);
            var terrain = new TerrainCoverage(terrainData);
            var grid = SimpleFloodModel.GenerateExpandedSurfaceFlood(terrain, centerPoint, 3.2);
            Assert.IsNotNull(grid);

        }


        [Test]
        public void GenerateBitmapFromGridSpatialData()
        {
            var grid = new GridSpatialData(500, 500, new Point(50, 50), new Point(60, 40));
            for (int i = 0; i < 200; i++)
                for (int j = 400; j < 450; j++)
                    grid.SetZ(i, j, 1);

            var temp = new DepictionFolderService(true);

            var filename = temp.GetATempFilenameInDir();//Path.Combine(folderName, "spatialdataBitmap.png");
            var bitmap = grid.GenerateBitmap();
            DepictionImageResourceDictionary.SaveBitmap(bitmap, filename);
            Assert.IsNotNull(bitmap);
            Assert.IsTrue(File.Exists(filename));
            temp.Close();
        }

        [Test]
        public void CanGenerateBitmapFromGridSpatialData()
        {
            using (var tf = new DepictionFolderService(true))
            {
                var folderName = tf.FolderName;

                var terrainData = GetTestTerrainData();
                var centerPoint = new Point(-122.3, 47.5);// new LatitudeLongitude(47.5, -122.3);
                terrainData.SetElevationValue(10.4F, centerPoint);
                var terrain = new TerrainCoverage(terrainData);

                var grid = (GridSpatialData)SimpleFloodModel.GenerateSimpleFloodGridFromTerrain(terrain, 3.2);
                grid.SetZ(1, 0, 1);
                grid.SetZ(0, 1, 1);
                grid.SetZ(1, 1, 1);
                grid.SetZ(2, 1, 1);
                var filename = Path.Combine(folderName, "spatialdataBitmap.png");
                var bitmap = grid.GenerateBitmap();
                DepictionImageResourceDictionary.SaveBitmap(bitmap, filename);
                Assert.IsNotNull(bitmap);

                var jpgName = "spatialdatajpg.jpg";
                Assert.IsTrue(terrain.GenerateTerrainVisual(folderName, jpgName));

                tf.Close();
            }
        }


        [Test]
        public void GetValueTest()
        {
            var grid = new GridSpatialData(3, 4, new Point(10, 14), new Point(13, 10));
            grid.SetZ(1, 1, 1);
            Assert.AreEqual(1, grid.GetValue(11.2, 11.2));
            Assert.AreEqual(0, grid.GetValue(10.9, 11.2));
            Assert.AreEqual(1, grid.GetValue(11.0, 11.2));
            Assert.AreEqual(1, grid.GetValue(11.9, 11.2));
            Assert.AreEqual(1, grid.GetValue(11.999999, 11.2));
            Assert.AreEqual(0, grid.GetValue(12.0000, 11.2));
            Assert.AreEqual(0, grid.GetValue(11.00001, 10.9999));
            Assert.AreEqual(1, grid.GetValue(11.00001, 11.9999));
        }

        [Test]
        public void ExpandByOnePixelTest()
        {
            var grid = new GridSpatialData(3, 3, new Point(2, 5), new Point(5, 2));
            grid.SetZ(1, 2, 3);
            grid.SetZ(1, 1, 3);
            var newGrid = grid.ExpandByOnePixel();
            Assert.AreEqual(3, newGrid.GetValueAtRowColumn(2, 3));
            Assert.AreEqual(3, newGrid.GetValueAtRowColumn(2, 2));
        }
    }
}
