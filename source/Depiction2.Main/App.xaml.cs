﻿//#define D14
//#define DReader14

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Service;
using Depiction2.Core.Product;
using Depiction2.Core.Service;
using Depiction2.Core.Service.WindowViewModelMapping;
using Depiction2.Core.ViewModels;
using Depiction2.ViewControls;
using Depiction2.ViewDialogs;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Main
{
    public partial class App
    {
        #region Variables

        private Depiction14Window testWindowMain;
        private DepictionAppViewModel appViewModel;
        private bool mRequestClose = false;

        private SplashScreen splash;

        #endregion
        #region Constructor
        public App()
        {
           
//            AppConfig.Change("holder.config");
        }
        #endregion
        #region private helpers

        private void SetProductAndTheme()
        {
#if D14
            DepictionAccess.ProductInformation = new Depiction2_14Information();
#else
            DepictionAccess.ProductInformation = new Depiction2DebugInformation();
#endif

            var resourceName = DepictionAccess.ProductInformation.ResourceAssemblyName;
            var splashImage = DepictionAccess.ProductInformation.SplashScreenPath;
            var themeAssemblyName = string.Format("pack://application:,,,/{0};component/{1}",
                resourceName, DepictionAccess.ProductInformation.ThemeLocation);

            var defaultResourceDictionary = new ResourceDictionary
            {
                Source = new Uri(themeAssemblyName)
            };
            Resources.MergedDictionaries.Add(defaultResourceDictionary);

            if (!string.IsNullOrEmpty(splashImage))
            {
                try
                {
                    var assembly = Assembly.Load(new AssemblyName(resourceName));
                    splash = new SplashScreen(assembly, splashImage);//Has to be a regular resource
                    if (splash != null) splash.Show(false);
                }
                catch (Exception)
                {
                    //                DepictionExceptionHandler.HandleException(ex,false,true);//not in for now because DepictionPath is not set yet.
                    splash = null;
                }
            }
        }
      
        #endregion

        #region Startup

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (DateTime.Now > new DateTime(2015, 3, 30))
            {
                NotificationService.Instance.LogMessage("Depiction has expired");
                Current.Shutdown(1);
                return;
            }
            var startInfo = string.Format("Starting Depiction2 {0}", DateTime.Now);
            NotificationService.Instance.LogMessage(startInfo);
            SetProductAndTheme();
            //Orders is important, path service is used in geocoderservice.
            AppHelper.InitializeLibraryAndPathService();
            ExtensionService.Instance.LoadResourcesIntoStory();
            DepictionAccess.GeocoderService = new GeocoderService();
            //Hack: Loading stories was causing a composition error when setting up the ogr extension for the first time (ogr_wrap couldn't
            //be found
            CoordinateSystemService.Instance.SetStoryCoordinateAndDisplaySystem(CoordinateSystemService.DepictionGeographicCoordinateSystem,
                                                                                CoordinateSystemService.DepictionGeographicCoordinateSystem);
            // Configure service locator
            ServiceLocator.RegisterSingleton<IDialogService, DialogService>();
            ServiceLocator.RegisterSingleton<IWindowViewModelMappings, WindowViewModelMappings>();
            appViewModel = new DepictionAppViewModel();  // Construct ViewModel and TestMainWindow
            testWindowMain = new Depiction14Window();

            appViewModel.OnSessionEnding = OnSessionEnding;

            testWindowMain.Closing += OnClosing;

            // When the ViewModel asks to be closed, it closes the window via attached behaviour.
            // We use this event to shut down the remaining parts of the application
            appViewModel.RequestClose += delegate
            {
                // Make sure close down event is processed only once
                if (mRequestClose == false)
                {
                    mRequestClose = true;
                    // Save session data and close application
                    OnClosed(testWindowMain.DataContext as DepictionAppViewModel, testWindowMain);
                }
            };

            testWindowMain.DataContext = appViewModel; // Attach ViewModel to DataContext of ViewModel
            CommandConnector.InitMainWindowCommandBinding(testWindowMain);  // Initialize RoutedCommand bindings
            CommandConnector.InitMainAppDialogToggleCommandBindings(testWindowMain);
            testWindowMain.Show();

            if (splash != null) splash.Close(TimeSpan.FromSeconds(0));
        }

        #endregion

        #region Shutdown process
        /// <summary>
        /// Determine whether Application TestMainWindow is ready to close down or
        /// whether close down should be cancelled - and cancel it.
        /// 
        /// This event is typically raised when the user clicks the window close button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = OnSessionEnding();
        }

        /// <summary>
        /// This executes when Windows is 'forced' to shut down the application.
        /// We can either tell Windows to wait (cancel App shutdown) or get out of its way.
        /// 
        /// See http://msdn.microsoft.com/en-us/library/system.windows.application.sessionending.aspx for details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_SessionEnding(object sender, SessionEndingCancelEventArgs e)
        {
            e.Cancel = OnSessionEnding();
        }

        /// <summary>
        /// Evaluate whether application is ready to shutdown and print a corresponding message if not (true to cancel).
        /// </summary>
        /// <returns></returns>
        private bool OnSessionEnding()
        {
            var tVM = testWindowMain.DataContext as DepictionAppViewModel;
            if (tVM != null)
            {
                var storyIsSafe = tVM.CloseCurrentDepiction();
                if (!(tVM.IsReadyToClose && storyIsSafe))
                {
                    //cancel was requested
                   
                    return true; // Cancel close down request if ViewModel is not ready, yet
                }
            }
//            DepictionAccess.ExtensionLibrary.Decompose();
            return false;
        }

        private void OnClosed(DepictionAppViewModel appVM, Window win)
        {
            try
            {
                Console.WriteLine("Closing down apllication.");
            }
            catch (System.Exception exp)
            {
                MessageBox.Show(exp.ToString(), "Error in OnClosed method", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
