﻿using System.Collections.Generic;
using Depiction2.API;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;

namespace Depiction2.Main.DefaultSetters
{
    public class CreateDefaults
    {
        static public List<IElementTemplate> GetDefaultDepictionElementTemplates()
        {
            return new List<IElementTemplate>{CreateAutoDetectElementTemplate(),CreateImageElementTemplate(),CreatePOIElementTemplate()};
        }
        static protected ElementTemplate CreateAutoDetectElementTemplate()
        {
            var scaf = new ElementTemplate(DepictionAccess.AutoDetectElement) { DisplayName = "Auto-Detect", IsMouseAddable = false};
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("Author","Depiction Inc."));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("Description", "Used when an importer needs to guess output type." ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("IconResourceName", "UnknownIcon" ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ( "ZOIShapeType", "Unknown" ));
            
            return scaf;
        }

        static protected ElementTemplate CreateImageElementTemplate()
        {
            var scaf = new ElementTemplate(DepictionAccess.DefaultImageElement) { DisplayName = "Basic Image", IsMouseAddable = false};
            scaf.InfoProperties.Add(new DefaultTemplateProperty ( "Author", "Depiction Inc." ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("Description", "Basic image element" ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("IconResourceName",  "Depiction.Image" ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("ZOIShapeType", "Polygon" ));
            
            return scaf;
        }
        static protected ElementTemplate CreatePOIElementTemplate()
        {
            var scaf = new ElementTemplate(DepictionAccess.DefaultPointElement) { DisplayName = "Simple Point" };
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("Author", "Depiction Inc." ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("Description",  "Generic map point" ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty ("IconResourceName", "Depiction.PointOfInterest" ));
            scaf.InfoProperties.Add(new DefaultTemplateProperty("ZOIShapeType", "Point"));
            
            return scaf;
        }


    }
}