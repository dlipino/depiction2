﻿using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Core.Interactions;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Main.DefaultSetters;

namespace Depiction2.Main
{
    public class AppHelper
    {
        static internal void InitializeLibraryAndPathService()
        {
            DepictionAccess.InteractionLibrary = InteractionRuleRepository.Instance;
            DepictionAccess.PathService = new DepictionPathService(DepictionAccess.ProductInformation.ProductAppDirectoryName);
            DepictionAccess.PathService.ClearLoadFileDirectory();
            //Race condition, library depends on the pathservice
            var library = new ElementTemplateLibrary();
            library.AddElementTemplatesToDefault(CreateDefaults.GetDefaultDepictionElementTemplates());
            DepictionAccess.TemplateLibrary = library;
        }
    }
}