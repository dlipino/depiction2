﻿using System.Threading;
using System.Windows.Input;
using Depiction2.API;
using Timers = System.Timers.Timer;

namespace Depiction2.Main
{
    public partial class Depiction14Window
    {
        public Depiction14Window()
        {
            InitializeComponent();
#if DEBUG
            PreviewKeyDown += Depiction14Window_PreviewKeyDown;
#endif
        }
        #region testing for the visuals of the message service
        //Debug helpers
        void Depiction14Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var mess = DepictionAccess.MessageService;
            var message = string.Format("KE rpress {0}", e.Key);
            if(e.Key.Equals(Key.D1))
            {
                mess.AddStoryMessage(message,1);
            }
            else if (e.Key.Equals(Key.D5))
            {
                mess.AddStoryMessage(message, 5);
            }else if(e.Key.Equals(Key.B))
            {
                var thread = new Thread(new ThreadStart(TimedCount));
                thread.Start();
            }
            else
            {
                mess.AddApplicationMessage(message);
            }

            e.Handled = false;
        }

        private void TimedCount()
        {
            var messageTimer = new Timers() {Interval = 2000};
            messageTimer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
            {
                var t = e.SignalTime;
                var message = string.Format("From other {0}", t);
                DepictionAccess.MessageService.AddStoryMessage(message, 2);

            };
            var stopTimer = new Timers(10000);
            stopTimer.Elapsed += delegate
                                        {
                                            stopTimer.Stop();
                                            messageTimer.Stop();
                                                                                  
                                        };

            messageTimer.Start();
            stopTimer.Start();
        }
        
        #endregion
    }
}
