﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

[assembly: InternalsVisibleTo("Depiction2.Story2IO.UnitTests")]
namespace Depiction2.Story2IO
{
    //TODO getting messy, too many static calls that are not very organized. No control/knowledge of when the utilities are initialized or not. May 11 2014
    public class JsonUtilities
    {
        private const string BadSerialBinding = "Serialization Binder cannot be null.";
        private TypeNameDictionarySerializationBinder typebinder = null;//new TypeNameDictionarySerializationBinder();
        #region construction and instance
        private static JsonUtilities _instance;
        public static JsonUtilities Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new JsonUtilities();
                }
                return _instance;
            }
        }
        public SerializationBinder SerialBinder
        {
            get { return typebinder; }
        }
        protected JsonUtilities() { }
        #endregion
        #region private helpers
        private JsonSerializerSettings CreateSettings(SerializationBinder typeBinder, IList<JsonConverter> converters)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                Binder = typeBinder
            };
            if (converters != null)
            {
                settings.Converters = converters;
            }
            settings.Converters.Add(new StringEnumConverter());
            return settings;
        }
        #endregion
        internal bool HasType(Type requestedType)
        {
            var key = requestedType.Name;
            return typebinder.TypeDictionary.ContainsKey(key);
        }
        internal void ResetInstance()
        {
            typebinder.TypeDictionary.Clear();
        }
        public void UpdateTypeBinder(Dictionary<string, Type> typeDictionary)
        {
            if(typebinder == null)
            {
                typebinder = new TypeNameDictionarySerializationBinder(typeDictionary);
                return;
            }
            foreach (var key in typeDictionary.Keys)
            {
                if (!typebinder.TypeDictionary.ContainsKey(key))
                {
                    typebinder.TypeDictionary.Add(key, typeDictionary[key]);
                }
            }
        }

        public string GetJsonObjectString(object elementToSave, SerializationBinder customBinder, IList<JsonConverter> converters)
        {
            SerializationBinder binder = customBinder;
            if (customBinder == null)
            {
                binder = typebinder;
            }
            var settings = CreateSettings(binder, converters);
            var json = JsonConvert.SerializeObject(elementToSave, elementToSave.GetType(), Formatting.Indented, settings);
            return json;
        }

        public T GetTypeJsonString<T>(string jsonString, SerializationBinder customBinder, IList<JsonConverter> converters)
        {
            SerializationBinder binder = customBinder;
            if (customBinder == null)
            {
                if (typebinder == null)
                {
                    throw new NullReferenceException(BadSerialBinding);
                }
                binder = typebinder;
            }
            var settings = CreateSettings(binder, converters);
            var item = JsonConvert.DeserializeObject<T>(jsonString, settings);

            return item;
        }

        public object GetObjectJsonString(string jsonString, SerializationBinder customBinder, IList<JsonConverter> converters)
        {
            SerializationBinder binder = customBinder;
            if (customBinder == null)
            {
                if(typebinder == null)
                {
                    throw new NullReferenceException(BadSerialBinding);
                }
                binder = typebinder;
            }
            var settings = CreateSettings(binder, converters);
            var item = JsonConvert.DeserializeObject(jsonString, settings);

            return item;
        }
        
        #region constants for file format
        public const string Story2FileExtension = ".dn2";
        public const string MetadataName = "DepictionFileMetadata.json";
        public const string StoryInformationFile = "StoryInformation.json";
        public const string ElementsDir = "Elements";
        public const string GeolocatedElements = "ElementsGeolocated.json";
        public const string NonGeolocatedElements = "ElementsNonGeolocated.json";
        public const string ScaffoldElementsDir = "Scaffolds";
        public const string ScaffoldExtension = ".dej";
        public const string AnnotationsDir = "Annotations";
        public const string AnnotationsFile = "ElementsGeolocated.json";
        public const string RegionsDir = "Regions";
        public const string MainViewFile = "MainView.json";
        public const string DataRegionsFile = "DataRegions.json";
        public const string RevealerFile = "Revealers.json";
        public const string InteractionRuleDir = "InteractionRules";
        public const string InteractionRuleExtension = ".dij";
        public const string InteractionRulesFile = "InteractionRules.dij";
        public const string ImagesDir = "Images";
        public const string DataDir = "Data";
        /**
 *Zip file format for Depiction 2.0 saves
 *DepictioFileMetadata.json
 *StoryInformation.json
 *   /Elements
 *    ElementsGeolocated.json
 *    ElementsNonGeolocated.json
 *   /Annotations
 *    Annotations.json
 *   /Displayers
 *    DepictionMain.json
 *    DepictionRevealers.json
 *   /ElementLibrary
 *    *.dml
 *    /icons
 *     Icons files, with names that dont give image type.
 *   /ImageLibrary
 *    DepictImages.xml
 *    /binaryfiles
 *     all images used, include icons. They usually don't have useful names or extensions
 *   /InteractionRules
 *    InteractionRules.son         
 */
        #endregion
    }



}