﻿using System;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Depiction2.Story2IO.Converters
{
    public class RevealerPropertyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
//            var k = objectType.GetInterface(typeof(IRevealerProperty).Name);
//            var g = typeof(IRevealerProperty).IsAssignableFrom(objectType);
//            var d = objectType.IsAssignableFrom(typeof(IRevealerProperty));
            return typeof(IRevealerProperty).IsAssignableFrom(objectType);

        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            var rev = value as IRevealerProperty;
            if (rev == null)
            {
                writer.WriteNull();
                return;
            }
            writer.WriteStartObject();
            writer.WritePropertyName("Name");
            serializer.Serialize(writer, rev.PropertyName);
            if (rev.PropertyType.IsEnum)
            {
                //Writes out full type. This can be reduced in size.
                writer.WritePropertyName("Type");
                serializer.Serialize(writer, rev.PropertyType);
            }

            writer.WritePropertyName("Value");
            serializer.Serialize(writer, rev.Value);

            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var typeName = jsonObject["Type"];
            Type valType = null;
            if (typeName != null)
            {
                valType = Type.GetType(typeName.Value<string>());
            }else
            {
                  return JsonConvert.DeserializeObject<RevealerProperty>(jsonObject.ToString());
            }

            var name = (string)jsonObject["Name"];
            var valToken = jsonObject["Value"];
            var val = JsonConvert.DeserializeObject(valToken.ToString(), valType);


            return new RevealerProperty(name,val);
        }

    }
}