﻿using System;
using System.Collections.Generic;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.StoryEntities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Depiction2.Story2IO.Converters
{
    public class ElementPropertyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(IElementProperty).IsAssignableFrom(objectType);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            var rev = value as IElementProperty;
            if (rev == null)
            {
                writer.WriteNull();
                return;
            }
            writer.WriteStartObject();
            writer.WritePropertyName("ProperName");
            serializer.Serialize(writer, rev.ProperName);

            writer.WritePropertyName("DisplayName");
            serializer.Serialize(writer, rev.DisplayName);
            if (rev.ValueType.IsEnum)
            {
                //Writes out full type. This can be reduced in size.
                writer.WritePropertyName("ValueType");
                serializer.Serialize(writer, rev.ValueType);
            }else if(!rev.ValueType.IsValueType)
            {
                writer.WritePropertyName("ValueType");
                serializer.Serialize(writer, rev.ValueType);
            }

            writer.WritePropertyName("Value");
            serializer.Serialize(writer, rev.Value);
            writer.WritePropertyName("LegacyValidationRules");
            serializer.Serialize(writer, rev.LegacyValidationRules);
            writer.WritePropertyName("LegacyPostSetActions");
            serializer.Serialize(writer, rev.LegacyPostSetActions);

            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var typeName = jsonObject["ValueType"];
            Type valType = null;
            if (typeName != null)
            {
                valType = Type.GetType(typeName.Value<string>());
            }
            //            else//creates a circular call
            //            {
            //                return serializer.Deserialize(jsonObject.CreateReader(), typeof (ElementProperty));
            //            }

            var name = (string)jsonObject["ProperName"];
            var dname = (string)jsonObject["DisplayName"];
            var valToken = jsonObject["Value"];
            var val = serializer.Deserialize(valToken.CreateReader(), valType);

            var prop = new ElementProperty(name, dname, val);

            var vRulesT = jsonObject["LegacyValidationRules"];
            var vRules = serializer.Deserialize<IValidationRule[]>(vRulesT.CreateReader());
            var actionsT = jsonObject["LegacyPostSetActions"];
            var actions = serializer.Deserialize<Dictionary<string, string[]>>(actionsT.CreateReader());
            prop.LegacyPostSetActions = actions;
            prop.LegacyValidationRules = vRules;
//            serializer.Populate(jsonObject.CreateReader(), prop);
            return prop;
        }

    }
}