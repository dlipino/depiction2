﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Depiction2.Story2IO
{
    public class TypeNameDictionarySerializationBinder : SerializationBinder
    {
        public Dictionary<string, Type> TypeDictionary { get; private set; }

        public TypeNameDictionarySerializationBinder()
            : this(null)
        {

        }
        public TypeNameDictionarySerializationBinder(Dictionary<string, Type> validTypes)
        {
            if (validTypes == null)
            {
                TypeDictionary = new Dictionary<string, Type>();
                return;
            }

            TypeDictionary = validTypes;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }

        public override Type BindToType(string assemblyName, string typeName)
        {
            if (TypeDictionary.ContainsKey(typeName)) return TypeDictionary[typeName];

            return null;
        }
    }
}