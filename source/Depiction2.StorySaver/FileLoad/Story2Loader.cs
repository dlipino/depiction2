﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.API.Service;
using Depiction2.API.Tools;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Ionic.Zip;

namespace Depiction2.Story2IO.FileLoad
{
    [DepictionStoryLoaderMetadata(Author = "Depiction Inc.", Description = "Loads .dpn files from 2.0 saves.", DisplayName = "Story loader 2.0",
       ExpectedVersion = "2.0", ExtensionPackage = "Default", Name = "Dpn20Loader", FileExtension = JsonUtilities.Story2FileExtension)]
    public class Story2Loader : IDepictionStoryLoader
    {
        private const string TemporaryFolderError = "Unable to create temporary holding folder.";
        #region construction/destruction

        public Story2Loader()
        {
        }

        public void Dispose()
        {
        }
        #endregion
        public IStory GetStoryFromFile(string zippedStoryFileName)
        {
            Story newStory = null;
            if (!File.Exists(zippedStoryFileName)) return null;
            if (TypeService.Instance.NameTypeDictonary.Count == 0)
            {
                TypeService.Instance.LoadCompleteDictionary();
            }
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);
            try
            {
                if (!ReadFileMetadata(zippedStoryFileName)) throw new Exception("Could not load metadata file");
                newStory = ReadStoryStart(zippedStoryFileName) as Story;
                if (newStory == null)
                {
                    throw new Exception("Could not load full story from file");
                }
                
                GetStoryDataFromZip(zippedStoryFileName);
                //Order is important, although it shouldn't be, but for now it is because
                //interactions are getting triggered when loading.
                var scaffoldsFromDpn = ScaffoldsFromStoryZip(zippedStoryFileName);
                IElementTemplateLibrary libraryToUse = null;
                //Should only happen in tests, but it is still bad form to have this occur
                //maybe there should be another method to grab the library or add it to the
                //global one.
                if (DepictionAccess.TemplateLibrary != null)
                {
                    DepictionAccess.TemplateLibrary.AddStoryElementTemplates(scaffoldsFromDpn);
                }
                else
                {
                    libraryToUse = new ElementTemplateLibrary();
                    libraryToUse.AddStoryElementTemplates(scaffoldsFromDpn);
                }
                //Now get teh element from the story file that are in the story
                var geoElements = StoryElementLoadFromZip(zippedStoryFileName,
                                                          Path.Combine(JsonUtilities.ElementsDir,
                                                                       JsonUtilities.GeolocatedElements));
                newStory.AddElements(geoElements, false);
                var nongeo = StoryElementLoadFromZip(zippedStoryFileName,
                                                     Path.Combine(JsonUtilities.ElementsDir,
                                                                  JsonUtilities.NonGeolocatedElements));
                newStory.AddElements(nongeo, false);
                //Now attach the associated elements, first pass 5/12/2014
                var elementsWithAssociates = newStory.AllElements.Where(t => t.HasAssociatedElements);
                foreach (var element in elementsWithAssociates)
                {
                    IElement element1 = element;
                    var associated = newStory.AllElements.Where(t => element1.AssociatedElementIds.Contains(t.ElementId)).ToList();
                    element.AttachElements(associated);
                }

                var rules = GetInteractionRulesFromZip(zippedStoryFileName);
                foreach(var rule in rules)
                {
                    rule.RuleSource = "LoadedFile";
                }
                newStory.LoadNonDefaultInteractions(rules);
                newStory.InitializeInteractionRouter();
                var images = GetImagesFromZip(zippedStoryFileName);
                newStory.SetStoryImages(images);
                var annots = GetAnnotationsFromZipFile(zippedStoryFileName);
                newStory.AddAnnotations(annots);

                newStory.MainDisplayer = LoadMainDisplayer(zippedStoryFileName);
                newStory.MainDisplayer.ConnectToStory(newStory);

                var revs = LoadRevealers(zippedStoryFileName);
                foreach (var dis in revs)
                {
                    var rev = dis as IElementRevealer;
                    if (rev != null)
                    {
                        newStory.LoadRevealer(rev);
                        rev.ConnectToStory(newStory);
                    }
                }

                var regions = LoadRegions(zippedStoryFileName);
                foreach (var region in regions)
                {
                    newStory.AddRegion(region);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {

            }
            return newStory;
        }
        #region information saves
        public bool ReadFileMetadata(string zipFileName)
        {
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var elementsEntry = zipFile[JsonUtilities.MetadataName];
                if (elementsEntry == null) return false;
                using (var reader = new StreamReader(elementsEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    var metadata = JsonUtilities.Instance.GetObjectJsonString(json, null, null);
                }
            }
            return true;
        }

        public IStory ReadStoryStart(string zipFileName)
        {

            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var elementsEntry = zipFile[JsonUtilities.StoryInformationFile];
                if (elementsEntry == null) return null;
                using (var reader = new StreamReader(elementsEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    var storyInfo = JsonUtilities.Instance.GetTypeJsonString<Story>(json, null, null);
                    return storyInfo;
                }
            }
        }
        #endregion

        #region story data
        public void GetStoryDataFromZip(string zipFileName)
        {
            var storageFolder = StorySerializationService.Instance.DataSerializationFolder;
            
            if (string.IsNullOrEmpty(storageFolder) || !Directory.Exists(storageFolder))
            {
                throw new Exception(TemporaryFolderError);
            }
            var culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var zipFileEntryTarget = JsonUtilities.DataDir;
                zipFile.FlattenFoldersOnExtract = true;
                var entryQuery = string.Format("name={0}/*", zipFileEntryTarget);
                var entries = zipFile.SelectEntries(entryQuery);
                foreach (var entry in entries)
                {
                    if (entry.IsDirectory)
                    {
                        continue;
                    }
                    entry.Extract(storageFolder);
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
        }
        #endregion

        #region elements
        public List<IElement> StoryElementLoadFromZip(string zipFileName, string zipFileEntryTarget)
        {
            var readElements = new List<IElement>();
            if (string.IsNullOrEmpty(zipFileName)) return readElements;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var elementsEntry = zipFile[zipFileEntryTarget];
                if (elementsEntry == null) return readElements;
                using (var reader = new StreamReader(elementsEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    readElements = JsonUtilities.Instance.GetTypeJsonString<List<IElement>>(json, null, null);
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return readElements;

        }
        #endregion

        #region scaffolds
        public List<IElementTemplate> GetElementTemplatesFromDir(string scaffoldDirectory)
        {
            var prototypes = new List<IElementTemplate>();
            if (!Directory.Exists(scaffoldDirectory)) return prototypes;
            var fileList = Directory.GetFiles(scaffoldDirectory, string.Format("*{0}", JsonUtilities.ScaffoldExtension));
            foreach (var fileName in fileList)
            {
                try
                {
                    using (var dmlFileStream = new StreamReader(fileName))
                    {
                        var jsonString = dmlFileStream.ReadToEnd();
                        var scaffold = JsonUtilities.Instance.GetTypeJsonString<ElementTemplate>(jsonString, null, null);
                        if (scaffold != null) prototypes.Add(scaffold);
                    }
                }
                catch (Exception ex)
                {
                    var errorMessage = string.Format("{0} is not a valid dml file", Path.GetFileName(fileName));
                    //                 DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 10);
                    //return null;
                }
            }
            return prototypes;
        }
        public List<IElementTemplate> GetEmbeddedElementTemplatesFromResource(string resoureName)
        {
            var folderService = new DepictionFolderService(false);
            var tempFolder = folderService.FolderName;

            if (!ResourceReaderService.CopyAssemblyEmbeddedResourcesToDirectory(resoureName, "dml2", true, tempFolder))
            {
                folderService.Close();
                return new List<IElementTemplate>();
            }
            var readElements = GetElementTemplatesFromDir(tempFolder);
            folderService.Close();
            return readElements;
        }

        public List<IElementTemplate> ScaffoldsFromStoryZip(string zipFileName)
        {
            var scaffoldList = new List<IElementTemplate>();
            if (string.IsNullOrEmpty(zipFileName)) return scaffoldList;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var entryQuery = string.Format("name=*{0}", JsonUtilities.ScaffoldExtension);

                var entries = zipFile.SelectEntries(entryQuery);
                foreach (var entry in entries)
                {
                    using (var reader = new StreamReader(entry.OpenReader()))
                    {
                        var json = reader.ReadToEnd();
                        var scaffold = JsonUtilities.Instance.GetTypeJsonString<ElementTemplate>(json, null, null);
                        scaffoldList.Add(scaffold);
                    }
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return scaffoldList;

        }
        #endregion

        #region Interaction rules
        public IList<IInteractionRule> GetInteractionRulesFromZip(string zipFileName)
        {
            var readRules = new List<IInteractionRule>();
            if (string.IsNullOrEmpty(zipFileName)) return readRules;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var dir = Path.Combine(JsonUtilities.InteractionRuleDir, JsonUtilities.InteractionRulesFile);
                var entry = zipFile[dir];
                if (entry == null) return readRules;
                readRules = GetRepositoryFromStream(entry.OpenReader());
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return readRules;
        }

        public IList<IInteractionRule> GetInteractionRulesFromFile(string fileName)
        {
            var rules = new List<IInteractionRule>();

            using (var dmlFileStream = new FileStream(fileName, FileMode.Open))
            {
                rules = GetRepositoryFromStream(dmlFileStream);
            }
            return rules;
        }

        public IList<IInteractionRule> GetInteractionRulesFromDirectory(string rulesDirectory)
        {
            var rules = new List<IInteractionRule>();
            if (!Directory.Exists(rulesDirectory)) return rules;
            var fileList = Directory.GetFiles(rulesDirectory, string.Format("*{0}", JsonUtilities.InteractionRuleExtension));
            foreach (var file in fileList)
            {
                rules.AddRange(GetInteractionRulesFromFile(file));
            }
            return rules;
        }

        public List<IInteractionRule> GetRepositoryFromStream(Stream repositoryStream)
        {
            var rules = new List<IInteractionRule>();
            using (var reader = new StreamReader(repositoryStream))
            {
                var json = reader.ReadToEnd();
                rules = JsonUtilities.Instance.GetTypeJsonString<List<IInteractionRule>>(json, null, null);
            }
            return rules;
        }

        #endregion

        #region Images

        public DepictionImageResourceDictionary GetImagesFromZip(string zipFileName)
        {
            var dict = new DepictionImageResourceDictionary();
            if (string.IsNullOrEmpty(zipFileName)) return dict;
            var storageFolder = StorySerializationService.Instance.DataSerializationFolder;
          
            if (string.IsNullOrEmpty(storageFolder) || !Directory.Exists(storageFolder))
            {
                throw new Exception(TemporaryFolderError);
            }
            var culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            var imageFolder = string.Empty;
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                var zipFileEntryTarget = JsonUtilities.ImagesDir;
                var entryQuery = string.Format("name={0}/*", zipFileEntryTarget);
                var entries = zipFile.SelectEntries(entryQuery);
                foreach (var entry in entries)
                {
                    if (entry.IsDirectory)
                    {
                        imageFolder = Path.Combine(storageFolder, entry.FileName);
                        if (!Directory.Exists(imageFolder) && entries.Count > 1)
                        {
                            Directory.CreateDirectory(imageFolder);
                        }
                        continue;
                    }
                    entry.Extract(storageFolder);
                    var name = entry.FileName;

                    var fullName = Path.Combine(storageFolder, name);
                    var miniName = Path.GetFileName(fullName);
                    var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullName);
                    if (image != null && !string.IsNullOrEmpty(miniName))
                    {
                        dict.AddImage(miniName, image);
                    }
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return dict;
        }

        #endregion

        #region annotations

        public List<IDepictionAnnotation> GetAnnotationsFromZipFile(string zipName)
        {
            var annots = new List<IDepictionAnnotation>();
            if (string.IsNullOrEmpty(zipName)) return annots;
            var culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(zipName))
            {
                var zipFileEntryTarget = Path.Combine(JsonUtilities.AnnotationsDir, JsonUtilities.AnnotationsFile);
                var annotationEntry = zipFile[zipFileEntryTarget];
                if (annotationEntry == null)
                {
                    Thread.CurrentThread.CurrentCulture = culture;
                    return annots;
                }
                using (var reader = new StreamReader(annotationEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    annots = JsonUtilities.Instance.GetTypeJsonString<List<IDepictionAnnotation>>(json, null, null);
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return annots;
        }
        #endregion

        #region Depiction region loading

        public IElementDisplayer LoadMainDisplayer(string zipName)
        {
            IElementDisplayer mainDisplayer = null;
            using (var zipFile = ZipFile.Read(zipName))
            {
                var zipFileEntryTarget = Path.Combine(JsonUtilities.RegionsDir, JsonUtilities.MainViewFile);
                var annotationEntry = zipFile[zipFileEntryTarget];
                if (annotationEntry == null) return null;
                using (var reader = new StreamReader(annotationEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    mainDisplayer = JsonUtilities.Instance.GetTypeJsonString<ElementDisplayer>(json, null, null);
                }
            }
            return mainDisplayer;
        }
        public List<IElementDisplayer> LoadRevealers(string zipName)
        {
            List<IElementDisplayer> revealers = new List<IElementDisplayer>();

            using (var zipFile = ZipFile.Read(zipName))
            {
                var zipFileEntryTarget = Path.Combine(JsonUtilities.RegionsDir, JsonUtilities.RevealerFile);
                var annotationEntry = zipFile[zipFileEntryTarget];
                if (annotationEntry == null) return revealers;
                using (var reader = new StreamReader(annotationEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    revealers = JsonUtilities.Instance.GetTypeJsonString<List<IElementDisplayer>>(json, null, null);
                }
            }
            return revealers;
        }

        public List<IDepictionRegion> LoadRegions(string zipName)
        {
            List<IDepictionRegion> regions = null;
            using (var zipFile = ZipFile.Read(zipName))
            {
                var zipFileEntryTarget = Path.Combine(JsonUtilities.RegionsDir, JsonUtilities.DataRegionsFile);
                var annotationEntry = zipFile[zipFileEntryTarget];
                using (var reader = new StreamReader(annotationEntry.OpenReader()))
                {
                    var json = reader.ReadToEnd();
                    regions = JsonUtilities.Instance.GetTypeJsonString<List<IDepictionRegion>>(json, null, null);
                }
            }
            return regions;
        }
        #endregion

        #region other helpers that are duplicated
        //Was used to find missing icons for 1.4, but not sure if that is needed for 2.0
        //        private Dictionary<string, ImageSource> FindNondefaultImages(Dictionary<string, ImageSource> loadedImages)
        //        {
        //            var defaultKeys = DepictionAccess.DefaultIconDictionary.Keys.ToList();
        //            var loadedKeys = loadedImages.Keys.ToList();
        //            var nonDefaultIcons = loadedKeys.Except(defaultKeys);
        //            var leftOver = new Dictionary<string, ImageSource>();
        //            foreach (var iconName in nonDefaultIcons)
        //            {
        //                leftOver.Add(iconName, loadedImages[iconName]);
        //            }
        //            return leftOver;
        //        }
        #endregion


    }
}