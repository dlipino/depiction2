﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using Depiction2.API;
using Depiction2.API.Extension.Exporter.Story;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;
using Ionic.Zip;

namespace Depiction2.Story2IO.FileSave
{
    [DepictionStorySaverMetadata(Author = "Depiction Inc.", Description = "Saves stories in 2.0 format (json)", DisplayName = "Story saver 2.0",
        ExpectedVersion = "2.0", ExtensionPackage = "Default", Name = "Dpn20Saver",
        StoryExtension = JsonUtilities.Story2FileExtension, ScaffoldExtension = JsonUtilities.ScaffoldExtension)]
    public class Story2Saver : IDepictionStorySaver
    {
        #region construction desturction

        public void Dispose()
        {

        }



        #endregion
        public bool SaveStoryToTempFolderFileAndZip(IStory storyToSave, string tempFolder, string zipFileName)
        {
            if (!Directory.Exists(tempFolder)) return false;
            var tempDir = tempFolder;
            //order is important, must setup the type stuff before any saving is done.
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);
            CreateFileMetadata(tempDir);
            CreateStoryInformationFile(tempDir, storyToSave);
            

            StoryElementsSave(tempDir, storyToSave);

            var scaffolds = new List<IElementTemplate>();
            if (DepictionAccess.TemplateLibrary != null)
            {
                scaffolds.AddRange(DepictionAccess.TemplateLibrary.LoadedStoryElementTemplates);
                scaffolds.AddRange(DepictionAccess.TemplateLibrary.ProductElementTemplates);
            }
            StoryScaffoldSave(tempDir, scaffolds, storyToSave.AllElements, null);
            AnnotationSave(tempDir, storyToSave, null);
            InteractionRepositorySave(tempDir, storyToSave.InteractionRules.AllInteractionRules);
            SaveStoryRegions(tempDir, storyToSave, null);
            ImageDictionarySave(tempDir, storyToSave.AllImages, storyToSave, null);

            ZipFolder(tempDir, zipFileName);
            return true;
        }

        public void ZipFolder(string homeFolder, string fullZipName)
        {
            using (var zip = new ZipFile())
            {
                zip.AddDirectory(homeFolder);
                zip.Save(fullZipName);
            }
        }
        public void CreateFileMetadata(string homeFolder)
        {
            var cacheFile = Path.Combine(homeFolder, JsonUtilities.MetadataName);
            var fileVersion = "2.0.alpha";
            var json = JsonUtilities.Instance.GetJsonObjectString(fileVersion, null, null);

            //write string to file
            File.WriteAllText(cacheFile, json);
        }

        public void CreateStoryInformationFile(string homeFolder, IStory story)
        {
            var cacheFile = Path.Combine(homeFolder, JsonUtilities.StoryInformationFile);
            var json = JsonUtilities.Instance.GetJsonObjectString(story, null, null);

            //write string to file
            File.WriteAllText(cacheFile, json);
        }
        
        #region elements and scaffolds
        public void StoryElementsSave(string homeFolder, IElementRepository repo)
        {
            var geoElements = repo.AllElements.Where(t => t.GeoLocation != null);
            var dir = Path.Combine(homeFolder, JsonUtilities.ElementsDir);
            Directory.CreateDirectory(dir);
            if (geoElements.Any())
            {
                var fileName = Path.Combine(dir, JsonUtilities.GeolocatedElements);
                var json = JsonUtilities.Instance.GetJsonObjectString(geoElements, null, null);
                File.WriteAllText(fileName, json);
            }

            var nonGeoElements = repo.AllElements.Where(t => t.GeoLocation == null);
            if (nonGeoElements.Any())
            {
                var fileName = Path.Combine(dir, JsonUtilities.NonGeolocatedElements);
                var json = JsonUtilities.Instance.GetJsonObjectString(nonGeoElements, null, null);
                File.WriteAllText(fileName, json);
            }
        }

        public void StoryScaffoldSave(string homeFolder, IEnumerable<IElementTemplate> allScaffolds, IEnumerable<IElement> elementsInStory, string zipName)
        {
            var scaffoldDir = Path.Combine(homeFolder, JsonUtilities.ScaffoldElementsDir);
            IEnumerable<IElementTemplate> toSave = null;
            if (elementsInStory != null)
            {
                toSave = from e in allScaffolds
                         from s in elementsInStory
                         where s.ElementType.Equals(e.ElementType)
                         select e;
            }
            else
            {
                toSave = allScaffolds;
            }

            foreach (var scaf in toSave)
            {
                SaveScaffold(scaffoldDir, string.Empty, scaf);
            }

            if (!string.IsNullOrEmpty(zipName))
            {
                ZipFolder(homeFolder, zipName);
            }
        }

        public string SaveScaffold(string homeFolder, string scaffoldFileName, IElementTemplate template)
        {
            var scaffoldString = JsonUtilities.Instance.GetJsonObjectString(template, null, null);
            if (string.IsNullOrEmpty(homeFolder)) return scaffoldString;

            if (string.IsNullOrEmpty(scaffoldFileName))
            {
                scaffoldFileName = string.Format("{0}{1}", template.ElementType, JsonUtilities.ScaffoldExtension);
            }

            var scaffoldFile = Path.Combine(homeFolder, scaffoldFileName);

            if (!Directory.Exists(homeFolder)) Directory.CreateDirectory(homeFolder);
            File.WriteAllText(scaffoldFile, scaffoldString);
            return scaffoldString;
        }
        public bool SaveScaffoldToFile(IElementTemplate templateToSave, string fullFileName)
        {
            var folder = Path.GetDirectoryName(fullFileName);
            var fileName = Path.GetFileName(fullFileName);
            SaveScaffold(folder, fileName, templateToSave);
            return true;
        }
        #endregion
        
        #region interactions saving
        public string InteractionRepositorySave(string homeFolder, IEnumerable<IInteractionRule> interactionRules)
        {
            var json = JsonUtilities.Instance.GetJsonObjectString(interactionRules, null, null);

            var dir = Path.Combine(homeFolder, JsonUtilities.InteractionRuleDir);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            var fileName = Path.Combine(dir, JsonUtilities.InteractionRulesFile);
            File.WriteAllText(fileName, json);
            return fileName;
        }
        #endregion
       
        #region images
        public string ImageDictionarySave(string homeFolder, DepictionImageResourceDictionary imageDictionary, IElementRepository elementsWithImages, string zipName)
        {
            if (imageDictionary == null) return string.Empty;
            var dir = Path.Combine(homeFolder, JsonUtilities.ImagesDir);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            var imageNamesToSave = new List<string>();
            if (elementsWithImages != null)
            {
                var usedImages = new HashSet<string>();
                foreach (var image in elementsWithImages.AllElements)
                {
                    usedImages.Add(image.ImageResourceName);
                    usedImages.Add(image.IconResourceName);
                }
                imageNamesToSave.AddRange(usedImages);
            }
            else
            {
                imageNamesToSave.AddRange(imageDictionary.Keys.Cast<string>());
            }
            foreach (var name in imageNamesToSave)
            {
                var fileName = Path.Combine(dir, name);
                var imageToSave = (BitmapSource)imageDictionary.GetImage(name);
                if (imageToSave != null) DepictionImageResourceDictionary.SaveBitmap(imageToSave, fileName);
            }
            if (!string.IsNullOrEmpty(zipName))
            {
                ZipFolder(homeFolder, zipName);
            }
            return string.Empty;
        }
        #endregion
        
        #region annotations

        public string AnnotationSave(string homeFolder, IStory annotationSource, string zipName)
        {
            var dir = Path.Combine(homeFolder, JsonUtilities.AnnotationsDir);
            Directory.CreateDirectory(dir);
            if (annotationSource.AllAnnotations.Any())
            {
                var fileName = Path.Combine(dir, JsonUtilities.AnnotationsFile);
                var json = JsonUtilities.Instance.GetJsonObjectString(annotationSource.AllAnnotations, null, null);
                File.WriteAllText(fileName, json);
            }

            if (!string.IsNullOrEmpty(zipName))
            {
                ZipFolder(homeFolder, zipName);
            }
            return string.Empty;
        }

        #endregion
        
        #region region/area saving
        public void SaveStoryRegions(string homeFolder, IStory fullStory, string zipName)
        {
            var dir = Path.Combine(homeFolder, JsonUtilities.RegionsDir);
            Directory.CreateDirectory(dir);

            var mainRegionFile = Path.Combine(dir, JsonUtilities.MainViewFile);
            var json = JsonUtilities.Instance.GetJsonObjectString(fullStory.MainDisplayer, null, null);
            File.WriteAllText(mainRegionFile, json);

            var revealerFile = Path.Combine(dir, JsonUtilities.RevealerFile);
            json = JsonUtilities.Instance.GetJsonObjectString(fullStory.RevealerDisplayers, null, null);
            File.WriteAllText(revealerFile, json);

            var staticRegionsFile = Path.Combine(dir, JsonUtilities.DataRegionsFile);

            json = JsonUtilities.Instance.GetJsonObjectString(fullStory.AllRegions, null, null);
            File.WriteAllText(staticRegionsFile, json);

            if (!string.IsNullOrEmpty(zipName))
            {
                ZipFolder(homeFolder, zipName);
            }
        }

        #endregion
    }
}