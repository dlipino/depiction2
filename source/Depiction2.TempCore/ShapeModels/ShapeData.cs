﻿using System;
using System.Windows;
using Depiction2.TempAPI.Models;
using Depiction2.Utilities.Drawing;

namespace Depiction2.TempCore.ShapeModels
{
    public class ShapeData : ModelBase, IDisposable
    {
        #region properties
        
        public EnhancedPointListWithChildren PointList
        {
            get { return (EnhancedPointListWithChildren)GetValue(ShapeGeometryProperty); }
            set { SetValue(ShapeGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShapeGeometryProperty =
            DependencyProperty.Register("PointList", typeof(EnhancedPointListWithChildren), typeof(ShapeData),
            new FrameworkPropertyMetadata(null));


        #endregion

        #region constructor and destruction

        public void Dispose()
        {
            PointList.Dispose();
            PointList = null;
        }

        #endregion
    }
}