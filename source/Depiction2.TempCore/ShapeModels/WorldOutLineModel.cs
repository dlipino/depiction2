﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempAPI.Models;
using Depiction2.TempCore.ShapeView;
using Depiction2.Utilities.Drawing;

namespace Depiction2.TempCore.ShapeModels
{
    public class WorldOutLineModel : ModelBase
    {
        private static WorldOutLineModel instance = new WorldOutLineModel();
        public static WorldOutLineModel Instance
        {
            get { return instance; }
        }
        #region outline stuff
        /// <summary>
        /// The list of rectangles that is displayed both in the main window and in the overview window.
        /// </summary>
        private RangeObservableCollection<ShapeDataViewModel> shapes = null;
        private RangeObservableCollection<ShapeData> shapesList = null;

        private List<ShapeData> rawShapes = new List<ShapeData>();
        private List<string> stringShapes = new List<string>();

        /// <summary>
        /// The list of rectangles that is displayed both in the main window and in the overview window.
        /// Should be quad tree
        /// </summary>
        public RangeObservableCollection<ShapeDataViewModel> ShapesViewModels
        {
            get
            {
                if (shapes == null) shapes = new RangeObservableCollection<ShapeDataViewModel>();
                return shapes;
            }
        }
        public RangeObservableCollection<ShapeData> ShapesList
        {
            get
            {
                if (shapesList == null) shapesList = new RangeObservableCollection<ShapeData>();
                return shapesList;
            }
        }
        public int ListCount
        {
            get
            {
                if (shapesList == null) return 0;
                return shapesList.Count;
            }
        }
        public int ShapeVMCount
        {
            get
            {
                if (shapes == null) return 0;
                return shapes.Count;
            }
        }

        public int RawShapeCount
        {
            get
            {
                if (rawShapes == null) return 0;
                return rawShapes.Count;
            }
        }
        public int StringShapeCount
        {
            get
            {
                if (stringShapes == null) return 0;
                return stringShapes.Count;
            }
        }
        #endregion

        #region public methods
        #region list shapes
        public void SetShapeList()
        {
            if (shapesList == null) shapesList = new RangeObservableCollection<ShapeData>();
            shapesList.AddRange(rawShapes);
            NotifyShapeChanges();
        }
        public void ClearShapesList()
        {
            if (shapesList != null) shapesList.Clear();
            shapesList = null;
            NotifyShapeChanges();
        }
        #endregion
        #region string shapes, shape string did not work well at all, the memory usage became bloated
        public void AddStringShapes(List<string> shapeStrings)
        {
            if (stringShapes == null) stringShapes = new List<string>();
            stringShapes.AddRange(shapeStrings);
            NotifyShapeChanges();
        }
        public void ClearStringShapes()
        {
            if (stringShapes != null)
            {
                for (int i = 0; i < stringShapes.Count; i++)
                {
                    stringShapes[i] = null;
                }
                stringShapes.Clear();
                stringShapes = null;
            }
            NotifyShapeChanges();
        }
        #endregion

        #region rawshapes

        public void AddRawShapes(List<ShapeData> shapeList)
        {
            if (rawShapes == null) rawShapes = new List<ShapeData>();
            rawShapes.AddRange(shapeList);
            NotifyShapeChanges();
        }

        public void ClearRawShapes()
        {
            if (rawShapes != null)
            {
                for (int i = 0; i < rawShapes.Count; i++)
                {
                    rawShapes[i].Dispose();
                    rawShapes[i] = null;
                }
                rawShapes.Clear();
                rawShapes = null;
            }
            NotifyShapeChanges();
        }
        #endregion
        #region Draw shapes

        public void ClearDrawShapes()
        {
            if (shapes != null) shapes.Clear();
            shapes = null;
            NotifyShapeChanges();
        }
        public void SetDrawShapesViewModel()
        {
            if (rawShapes == null) return;
            if (shapes == null)
            {
                shapes = new RangeObservableCollection<ShapeDataViewModel>();
            }

            var shapesvm = new List<ShapeDataViewModel>();

            var totalUnion = new Rect(); 
            foreach (var shape in rawShapes)
            {
                var bounds = DrawingHelpers.FindEnclosingRectForPoints(shape.PointList.Points);
//
//                totalUnion.Union(bounds);
//                shape.PointList.ShiftPoints(new Point(-bounds.Left, -bounds.Top));
                shapesvm.Add(new ShapeDataViewModel
                                 {
                                     ShapeGeometry = DrawingHelpers.CreateGeometryFromEnhancedPointListWithChildren(shape.PointList),
                                     OffsetX = bounds.Left,
                                     OffsetY = bounds.Top
                                 });
            }
//            Console.WriteLine("The orig union " + totalUnion);
//            totalUnion = new Rect();
//            foreach(var shapevm in shapesvm)
//            {
//                var unoffset = new Rect(new Point(shapevm.OffsetX, shapevm.OffsetY), shapevm.ShapeGeometry.Bounds.Size);
//                totalUnion.Union(unoffset);
//            }
//            Console.WriteLine("The mod union " + totalUnion);
            shapes.AddRange(shapesvm);
            NotifyShapeChanges();
        }

        #endregion

        #endregion
        #region

        private void NotifyShapeChanges()
        {
            OnPropertyChanged("ShapesViewModels");
            OnPropertyChanged("ShapeVMCount");
            OnPropertyChanged("RawShapeCount");
            OnPropertyChanged("StringShapeCount");
            OnPropertyChanged("ShapesList");

            OnPropertyChanged("ListCount");

        }
        #endregion
    }
}