﻿using System;
using System.Linq;
using Depiction2.Base.StoryEntities;
using Depiction2.TempAPI.LegacyInterfaces;
using Depiction2.TempAPI.TempInterfaces;
using Depiction2.TempExtensionCore;

namespace Depiction2.TempCore.ExtensionMethods
{
    static public class TempElementExtensions
    {
        public static bool SetPropertyValue(this IElement tempElement,string propName, object value)
        {
            var prop = tempElement.GetPropertyTemp(propName);
            if (prop == null) return false;

            prop.SetValue(value);
            if (prop.LegacyPostSetActions != null  )
            {
              
                    foreach (var action in prop.LegacyPostSetActions)
                    {
                        var key = action.Key;
                        var behaviour = DepictionExtensionLibrary.Instance.RetrieveBehavior(key);//.Where(t => t.Key.BehaviorName.Equals(key)).ToList();
                        if (behaviour != null)
                        {
                            var behaviorParams = LegacyHelpers.BuildParametersForPostSetActions(action.Value);
                            behaviour.DoBehavior(tempElement, behaviorParams);
                        }
                    }
            }
//            return new DepictionValidationResult(ValidationResultValues.Valid, string.Empty);
            return true;
        }
    }
}