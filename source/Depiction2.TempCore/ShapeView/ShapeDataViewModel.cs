﻿using System;
using System.Windows;
using System.Windows.Media;
using Depiction2.TempCore.Extensions;

namespace Depiction2.TempCore.ShapeView
{
    public class ShapeDataViewModel : DependencyObject
    {
        public int Index { get; set; }//Hopeuflly this is temp
        public double OffsetX { get; set; }
        public double OffsetY { get; set; }
        public double Width
        {
            get
            {
                if (ShapeGeometry == null) return 0;
                return ShapeGeometry.Bounds.Width;
            }
        }
        public double Height
        {
            get
            {
                if (ShapeGeometry == null) return 0;
                return ShapeGeometry.Bounds.Height;
            }
        }

        public Rect Bounds { get; private set; }
        public double Area { get; private set; }
        public StreamGeometry ShapeGeometry
        {
            get { return (StreamGeometry)GetValue(ShapeGeometryProperty); }
            set { SetValue(ShapeGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShapeGeometryProperty =
            DependencyProperty.Register("ShapeGeometry", typeof(StreamGeometry), typeof(ShapeDataViewModel),
             new PropertyMetadata(ShapeGeometryChanged));

        private static void ShapeGeometryChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var svm = d as ShapeDataViewModel;
            if(svm == null) return;
            var geom = e.NewValue as StreamGeometry;
            if(geom == null) return;
            svm.Bounds = geom.Bounds;
            svm.Area = geom.Bounds.Area();
        }
    }
}