﻿using System.IO;
using System.Reflection;
using System.Windows;
using Depiction.ProjectionSystem.DepictionProjection;
using Depiction.Views;
using Depiction2.TempAPI.TempInterfaces;
using Depiction2.TempCore;
using Depiction2.TempCore.TempEntities;
using Microsoft.Win32;

namespace Depiction2.TempMain
{
    /// <summary>
    /// Interaction logic for MapWindow.xaml
    /// </summary>
    public partial class MapWindow
    {
        public MapWindow()
        {
            InitializeComponent();
        }

        private void Loaddpn_btn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            var location = @"D:\DepictionData";
            if (!Directory.Exists(location))
            {
                location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (!string.IsNullOrEmpty(location))
                {
                    string path = Path.Combine(location, "Datasets");

                    if (!Directory.Exists(path))
                    {
                        location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    }
                    else
                    {
                        location = path;
                    }
                }
            }

            dialog.InitialDirectory = location;
            var dpnToLoad = string.Empty;
            if (dialog.ShowDialog(this) == true)
            {
                dpnToLoad = dialog.FileName;
            }

            if (string.IsNullOrEmpty(dpnToLoad)) return;

            var legacyLoader = DepictionAccess.ExtensionLibrary.LegacyExtensionArray[0].Value;
            var story = legacyLoader.LoadStoryFromFile(dpnToLoad);
            DepictionAccess.DStory = story;
            story.InteractionsRunner.BeginAsync();
        }

        private void LoadExtension_Btn_Click(object sender, RoutedEventArgs e)
        {
            var window = new ImporterExtensionWindow();
            window.Owner = this;
            window.Show();
        }

        private void CreateStoryButton_Click(object sender, RoutedEventArgs e)
        {
            if(DepictionAccess.DStory == null)
            {
                DepictionAccess.DStory = new TempStory();
            }

            ConnectStory(DepictionAccess.DStory);
        }

        private void ChangeProj_Click(object sender, RoutedEventArgs e)
        {
            if(DepictionAccess.DStory != null)
            {
                DepictionAccess.DStory.UpdateCurrentCoordinateSystem(DepictionProjectionSystem.mercatorProjectedCoordinateSystem);
            }

        }
        private void ConnectStory(ITempStory story)
        {
            WorldController.DataContext = story;
        }

        private void OptimizeyButton_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory != null)
            {
                DepictionAccess.DStory.AllElements.Optimize();
            }
        }

        private void CenterMap_Click(object sender, RoutedEventArgs e)
        {
            if(DepictionAccess.DStory == null)
            {
                WorldController.WorldOffset = new Point();
                WorldController.WorldScale = 1;
            }else
            {

                var story = DepictionAccess.DStory;
                var extent = story.AllElements.Extent;
                //Try and make the extent match the canvase extent
                var viewExtent = new Rect(0, 0, WorldController.ActualHeight, WorldController.ActualHeight);
                var tScale = viewExtent.Width / extent.Width;
//                WorldController.WorldScale = tScale;

                var center = extent.GetCenter();
                center = new Point(center.X*tScale,-center.Y*tScale);
//                WorldController.WorldOffset = new Point(-127, -42);//center.X,-center.Y);//center;

                WorldController.SetViewScaleAndOffsetFromViewModelValues(tScale,center);
            }
        }
    }
}
