﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction.Views;
using Depiction2.Externals;
using Depiction2.TempCore.Extensions;
using Depiction2.TempCore.ShapeModels;
using Depiction2.TempCore.ShapeView;
using Depiction2.Utilities.Memory;
using ZoomableControlTake2;
using ZoomableControlTake2.SpatialIndexing;

namespace Depiction2.TempMain
{
    /// <summary>
    /// Interaction logic for TestMainWindow.xaml
    /// </summary>
    public partial class TestMainWindow
    {
        public TestMainWindow()
        {
            InitializeComponent();

            panZoom.DataContext = WorldOutLineModel.Instance;
        }


        private void exceptionLog_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                throw new System.IO.FileNotFoundException();
            }
            catch (Exception ex)
            {
                //The reference to log4net should not be needed once all the projects are built to the same location.
                DepictionLogger.LogException(ex);
            }
        }
        private void importer_Click(object sender, RoutedEventArgs e)
        {
//            var importer = DepictionAccess.ExtensionLibrary.ImporterExtensionArray[0].Value;
//            ImporterExtensionWindow.LoadFiles(importer);
            var window = new ImporterExtensionWindow();
            window.Owner = this;
            window.Show();
        }

        private void clearraw_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.ClearRawShapes();
        }

        private void clearshapeList_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.ClearShapesList();
        }

        private void makequad_CLick(object sender, RoutedEventArgs e)
        {
            var quad = SpatialIndexingSpecials.CreateSpatialIndexRecursive(WorldOutLineModel.Instance.ShapesViewModels);
        }

        private void makepriorityquad_Click(object sender, RoutedEventArgs e)
        {
            var _tree = new PriorityQuadTree<ShapeDataViewModel>();
            foreach(var item in WorldOutLineModel.Instance.ShapesViewModels)
            {
                _tree.Insert(item,item.Bounds,item.Bounds.Area());
            }
        }
        private void clearvm_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.ClearDrawShapes();
        }

        private void makevm_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.SetDrawShapesViewModel();
        }
        #region Binding stuff
        private void unbindBtn_click(object sender, RoutedEventArgs e)
        {
            panZoom.UnbindShapes();
        }
        private void bindBtn_click(object sender, RoutedEventArgs e)
        {
            if(panZoom.IsVisible)
            {
                panZoom.BindStreamShapes();

            }else
            {
                ZoomListBox.AddGeometries(WorldOutLineModel.Instance.ShapesViewModels.ToList());
            }
            
           

        }

        #endregion
        private void gcBtn_click(object sender, RoutedEventArgs e)
        {
           MemUtil.DoMassGC();
        }

    }
}
