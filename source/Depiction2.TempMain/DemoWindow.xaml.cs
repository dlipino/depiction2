﻿using System.Linq;
using System.Windows;
using Depiction.Views;
using Depiction2.TempCore.ShapeModels;

namespace Depiction2.TempMain
{
    /// <summary>
    /// Interaction logic for DemoWindow.xaml
    /// </summary>
    public partial class DemoWindow
    {
        public DemoWindow()
        {
            InitializeComponent(); 

        }

        private void LoadExtension_Btn_Click(object sender, RoutedEventArgs e)
        {
            var window = new ImporterExtensionWindow();
            window.Owner = this;
            window.Show();
        }

        private void DrawShapes_Btn_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.SetDrawShapesViewModel();
            ZoomListBox.AddGeometries(WorldOutLineModel.Instance.ShapesViewModels.ToList());
        }

        private void ClearShapes_Btn_Click(object sender, RoutedEventArgs e)
        {
            WorldOutLineModel.Instance.ClearRawShapes();
            WorldOutLineModel.Instance.ClearDrawShapes();
            ZoomListBox.ClearGeometries();
        }
    }
}
