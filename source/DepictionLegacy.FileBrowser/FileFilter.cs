﻿using System.Collections.Generic;
using System.Linq;

namespace DepictionLegacy.FileBrowser
{
    /// <summary>
    /// A container for the information needed to describe how to filter the list of files shown in a file open dialog,
    /// for instance.
    /// </summary>
    public class FileFilter
    {

        #region static helpers
        public static FileFilter AllTypesFilter
        {
            get
            {
                return new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = true };
            }
        }

        public static FileFilter CreateElementTypeFilter(string source, string extension)
        {
            var ext = ".dml";
            if (!string.IsNullOrEmpty(extension))
            {
                ext = extension;
            }
            return CreateFileFilter(ext, "Depiction Element", true, source);

        }
        public static FileFilter CreateInteractionTypeFilter(string source, string extension)
        {
            var ext = ".xml";
            if (!string.IsNullOrEmpty(extension))
            {
                ext = extension;
            }
            return CreateFileFilter(ext, "Depiction Interaction", true, source);

        }

        public static FileFilter CreateStoryTypeFilter(string source, string extension, string description)
        {
            var ext = ".dpn";
            if (!string.IsNullOrEmpty(extension))
            {
                ext = extension;
            }
            return CreateFileFilter(ext, description, false, source);

        }
        #endregion
        public string Description { get; set; }

        public string Wildcard { get; set; }

        public bool Selected { get; set; }
        public string FilterSource { get; set; }

        public static FileFilter CreateFileFilter(string extension, string simpleDescription, bool selected, string filterSource)
        {
            return CreateFileFilter(new List<string> { extension }, simpleDescription, selected, filterSource);
        }

        public static FileFilter CreateFileFilter(IEnumerable<string> extensions, string simpleDescription, bool selected, string filterSource)
        {
            var extensionString = string.Empty;
            var first = true;
            var extList = extensions.ToList();
            foreach (var extension in extList)
            {
                if (!first) extensionString += ", ";
                extensionString += string.Format("*{0}", extension);
                if (first)
                {
                    first = false;
                }
            }
            first = true;
            var wildCards = string.Empty;
            foreach (var extension in extList)
            {
                if (!first) wildCards += "; ";
                wildCards += string.Format("*{0}", extension);
                if (first)
                {
                    first = false;
                }
            }
            var filter = new FileFilter
            {
                Description = string.Format("{0} ({1})", simpleDescription, extensionString),
                Wildcard = wildCards,
                Selected = selected,
                FilterSource = filterSource               
            };
            return filter;
        }
    }
}