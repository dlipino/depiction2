﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DepictionLegacy.FileBrowser.Properties;
using Microsoft.Win32;

namespace DepictionLegacy.FileBrowser
{
    public class DepictionFileBrowser
    {
        public static string OpenFileDialog(string title, string startPath, Dictionary<string, KeyValuePair<string, IEnumerable<string>>> sourceNameDescriptionExtension, out string selectedSource)
        {
            var fileName = string.Empty;
            selectedSource = string.Empty;
            var filters = new List<FileFilter>();
            foreach (var source in sourceNameDescriptionExtension)
            {
                var fileFilter = FileFilter.CreateFileFilter(source.Value.Value, source.Value.Key, false, source.Key);
                filters.Add(fileFilter);
            }
            FileFilter selectedFilter;
            fileName = ChooseLoadFile(startPath, filters.ToArray(), out selectedFilter, title);

            //            dialog.Multiselect = false;
            //            int selected;
            ////            var filterStrings = "Data Sources (*.txt, *.xml)|*.txt*;*.xml|All Files|*.*";
            //            var filterStrings = GetFilters(filters.ToArray(), out selected);
            //            dialog.Filter = filterStrings;
            //            if ((bool)dialog.ShowDialog())
            //            {
            //                //               selectedFilter = fileFilters[dialog.FilterIndex - 1];
            //                return dialog.FileName;
            //            }
            if (selectedFilter != null) selectedSource = selectedFilter.FilterSource;
            return fileName;
        }
        static public string OpenFileDialog(string startDir)//string title, string startPath, List<KeyValuePair<string,List<string>>> descriptionAndExtension )
        {
            var fileName = string.Empty;

            var dialog = new OpenFileDialog();
            var filterStrings = "All Files|*.*";
            dialog.Filter = filterStrings;
            if (!string.IsNullOrEmpty(startDir) && Directory.Exists(startDir))
            {
                dialog.InitialDirectory = startDir;
            }
            if ((bool)dialog.ShowDialog())
            {
                return dialog.FileName;
            }
            return fileName;
        }

        #region prebuilt loader/savers
        static public string ChooseLoadFile(string defaultDirectory, FileFilter[] fileFilters, out FileFilter selectedFilter, string title)
        {
            int selectedFilterIndex;
            string filter = GetFilters(fileFilters, out selectedFilterIndex);
            selectedFilter = null;
            var dialog = new OpenFileDialog { Filter = filter, FilterIndex = selectedFilterIndex + 1, };

            dialog.CheckPathExists = true;
            if (string.IsNullOrEmpty(title)) dialog.Title = "Select depiction file to load";
            else dialog.Title = title;
            if (Directory.Exists(defaultDirectory)) dialog.InitialDirectory = defaultDirectory;

            if ((bool)dialog.ShowDialog())
            {
                selectedFilter = fileFilters[dialog.FilterIndex - 1];
                return dialog.FileName;
            }
            return null;
        }

        static public string ChooseSaveFile(string initialSelection, FileFilter[] fileFilters, out FileFilter selectedFilter, string title)
        {
            int selectedFilterIndex;
            selectedFilter = null;
            string filter = GetFilters(fileFilters, out selectedFilterIndex);

            var dialog = new SaveFileDialog { Filter = filter, FilterIndex = selectedFilterIndex + 1, };

            if (string.IsNullOrEmpty(title)) dialog.Title = "Set file save name";
            else dialog.Title = title;
            if (File.Exists(initialSelection))
            {
                dialog.FileName = initialSelection;
            }
            else if (Directory.Exists(initialSelection)) dialog.InitialDirectory = initialSelection;

            if ((bool)dialog.ShowDialog())
            {
                selectedFilter = fileFilters[dialog.FilterIndex - 1];
                return dialog.FileName;
            }
            return null;
        }
        #endregion
        #region Helpers
        /// <summary>
        /// Create file filters as string and selected the initial filter index.
        /// </summary>
        /// <param name="fileFilters"></param>
        /// <param name="selectedFilterIndex"></param>
        /// <returns></returns>
        private static string GetFilters(FileFilter[] fileFilters, out int selectedFilterIndex)
        {
            string filter = string.Empty;
            selectedFilterIndex = 0;
            if (fileFilters.Length == 0) return filter;
            selectedFilterIndex = fileFilters.Length - 1;
            for (int i = 0; i < fileFilters.Length; i++)
            {
                filter += string.Format("{0}|{1}|", fileFilters[i].Description, fileFilters[i].Wildcard);
                if(fileFilters[i].Selected)
                {
                    selectedFilterIndex = i;
                }
            }

            filter = filter.Trim('|');
            filter += ";";
            return filter;
        }
        #endregion helpers

        #region depiction file loaders

        private const string productfileExtension = ".dpn";
        private const string productName = "Depiction";
        private const string storyName = "Depiction";

        private static readonly FileFilter[] DepictionOnlyFileFilter = new[]
                                        {
                                            FileFilter.CreateFileFilter(productfileExtension,productName,true,string.Empty)
                                        };
        private static readonly FileFilter[] fileOpenSaveDialogFilterString = new[]
                                        {
                                            FileFilter.CreateFileFilter(productfileExtension,productName,true,string.Empty),
                                                                                       
                                            new FileFilter
                                                {
                                                    Description = "All Files (*.*)", 
                                                    Wildcard = "*.*"
                                                } 
                                        };

        /// <summary>
        /// Get a depiction file name to load, an empty string return means the action was canceled
        /// </summary>
        /// <returns></returns>
        static public string GetDepictionToLoad(List<FileFilter> loadFileFilters, out string selectedSource)
        {
            string fileToOpen;
            var saveDirectory = Settings.Default.DepictionFileLoadDirectory;

            if (string.IsNullOrEmpty(saveDirectory) || !Directory.Exists(saveDirectory))
            {
                saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }

            FileFilter selectedFilter;
            fileToOpen = ChooseLoadFile(saveDirectory, loadFileFilters.ToArray(), out selectedFilter, "Select depiction story file name");
            if (string.IsNullOrEmpty(fileToOpen))
            {
                selectedSource = string.Empty;
                return string.Empty;
            }

            //            var fileExtenstion = Path.GetExtension(fileToOpen);
            //            //var depictionProduct = new DepictionProductInformation();//Why would this be used?
            //            if (!fileExtenstion.Equals(productfileExtension, StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                if (fileExtenstion.Equals(productfileExtension, StringComparison.InvariantCultureIgnoreCase))
            //                {
            //                    // WARN that will not be able to save to DPN.
            //                    var title = string.Format("Opening file created by {0}", productfileExtension);
            //                    var message = string.Format(
            //                        "This file was created by {0}, not {2}. You may open it, but will only be able to save it as a {1} file readable only by {2}; it will NOT be readable by {0}.\n\nDo you wish to continue?",
            //                        productName,
            //                        storyName,
            //                        productName);
            //                    //                    var confirmer = DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title,
            //                    //                                                                   MessageBoxButton.YesNo);
            //                    //                    if (confirmer.Equals(MessageBoxResult.No))
            //                    //                    {
            //                    //                        return string.Empty;
            //                    //                    }
            //                }
            //                else
            //                {
            //                    var message = "Cannot open this type of file.";
            //                    var title = "File type error";
            //                    //                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
            //                    return string.Empty;
            //                }
            //            }

            Settings.Default.DepictionFileLoadDirectory = Path.GetDirectoryName(fileToOpen);
            Settings.Default.Save();
            selectedSource = selectedFilter.FilterSource;
            return fileToOpen;
        }

        /// <summary>
        /// Get a depiction file name to load, an empty string return means the action was canceled
        /// </summary>
        /// <returns></returns>
        static public string GetDepictionSaveFileName(List<FileFilter> loadFileFilters, out string selectedSource)
        {
            string fileToOpen;
            selectedSource = string.Empty;
            var loadDirectory = Settings.Default.DepictionSaveFileDirectory;

            if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
            {
                loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            FileFilter selectedFilter;
            fileToOpen = ChooseSaveFile(loadDirectory, loadFileFilters.ToArray(), out selectedFilter, "Select depiction to load");
            if (string.IsNullOrEmpty(fileToOpen))
            {
                return string.Empty;
            }
            selectedSource = selectedFilter.FilterSource;
            Settings.Default.DepictionSaveFileDirectory = Path.GetDirectoryName(fileToOpen);
            Settings.Default.Save();
            return fileToOpen;
        }


        public string LoadImageFile()
        {
            string fileToOpen;
            try
            {
                var loadDirectory = string.Empty;// Settings.Default.DepictionFileLoadDirectory;

                if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
                {
                    loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                FileFilter selectedFilter;
                fileToOpen = ChooseLoadFile(loadDirectory,  fileOpenSaveDialogFilterString, out selectedFilter, "Select depiction to load");
                if (string.IsNullOrEmpty(fileToOpen))
                {
                    return string.Empty;
                }

                if (!Path.HasExtension(fileToOpen))
                {
                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    //                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return string.Empty;
                }

                var fileExtenstion = Path.GetExtension(fileToOpen);
                //var depictionProduct = new DepictionProductInformation();//Why would this be used?
                if (string.IsNullOrEmpty(fileExtenstion))//!fileExtenstion.Equals(productfileExtension, StringComparison.InvariantCultureIgnoreCase)))
                {

                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    //                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return string.Empty;
                }

                //                Settings.Default.DepictionFileLoadDirectory = Path.GetDirectoryName(fileToOpen);
                //                Settings.Default.Save();
            }
            finally
            {

            }
            return fileToOpen;
        }
        #endregion

        #region scaffold loading methods

        static public string GetScaffoldToLoad(List<FileFilter> loadFileFilters, out string selectedSource)
        {
            string fileToOpen;
            var saveDirectory = Settings.Default.ScaffoldFileLoadDirectory;

            if (string.IsNullOrEmpty(saveDirectory) || !Directory.Exists(saveDirectory))
            {
                saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            FileFilter selectedFilter;
            fileToOpen = ChooseLoadFile(saveDirectory,  loadFileFilters.ToArray(), out selectedFilter, "Select element template file name");
            if (string.IsNullOrEmpty(fileToOpen))
            {
                selectedSource = string.Empty;
                return string.Empty;
            }

            Settings.Default.ScaffoldFileLoadDirectory = Path.GetDirectoryName(fileToOpen);
            Settings.Default.Save();
            selectedSource = selectedFilter.FilterSource;
            return fileToOpen;
        }


        static public string GetScaffoldSaveFileName(List<FileFilter> loadFileFilters, out string selectedSource)
        {
            string fileToOpen;
            selectedSource = string.Empty;
            var loadDirectory = Settings.Default.ScaffoldFileSaveDirectory;

            if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
            {
                loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            FileFilter selectedFilter;
            fileToOpen = ChooseSaveFile(loadDirectory, loadFileFilters.ToArray(), out selectedFilter, "Select element template to load");
            if (string.IsNullOrEmpty(fileToOpen))
            {
                return string.Empty;
            }
            selectedSource = selectedFilter.FilterSource;
            Settings.Default.ScaffoldFileSaveDirectory = Path.GetDirectoryName(fileToOpen);
            Settings.Default.Save();
            return fileToOpen;
        }

        #endregion
    }
}