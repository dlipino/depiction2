﻿using NUnit.Framework;

namespace Depiction2.DotSpatialExtension.UnitTests
{
    [TestFixture(Description = "Figure out string works with the lat/long and utm with the")]
    public class LatitudeLongitudeDotSpatialTest
    {
        [Test]
        public void HowToGoFromPositionToAUTM()
        {
            var loc1 = "47.61799N, 122.32226W";
            var lat = 47.61799;
            var lon = -122.32226;
            var utmZone = 10;
            var llds = new LatitudeLongitudeDotSpatial(loc1);
            Assert.AreEqual(lat, llds.Latitude);
            Assert.AreEqual(lon, llds.Longitude);
            Assert.AreEqual(utmZone,llds.UTMZone);
            var easting = 550928.35;
            var northing = 5274065.32;
            //Map Datum NAD83/WGS84 //northern hemispher
            //            Zone: 10 
            //Easting: 550928.35
            //Northing: 5274065.32
//            SpatialReference oUTM = new SpatialReference(string.Empty);
//            SpatialReference poLatLong;
//            CoordinateTransformation poTransform,uoTransform;
//
//            oUTM.SetProjCS("UTM 10 / WGS84");
//            oUTM.SetWellKnownGeogCS("WGS84");
//            //true = 1
//            //false = 0, i hate binary choices
//            oUTM.SetUTM(utmZone, 1);
//
//            poLatLong = oUTM.CloneGeogCS();
//
//            poTransform = new CoordinateTransformation(oUTM, poLatLong);
//            var results = new double[3];
//            poTransform.TransformPoint(results,easting,northing,0);
//            var lonOut = results[0];
//            Assert.AreEqual(lon,lonOut,.000001);
//            var latOut = results[1];
//            Assert.AreEqual(lat, latOut, .000001);
//            uoTransform = new CoordinateTransformation(poLatLong, oUTM);
//            uoTransform.TransformPoint(results, llds.Longitude, llds.Latitude, 0);
//            var eOut = results[0];
//            Assert.AreEqual(easting, eOut, .01);
//            var nOut = results[1];
//            Assert.AreEqual(northing, nOut, .01);
        }

    }
}