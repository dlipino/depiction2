﻿using System.Windows;

namespace Depiction2.TempAPI.ExtensionClasses
{
    public class CartesianRect
    {
        public static CartesianRect Empty
        {
            get { return new CartesianRect(); }
        }
        #region Properties

        public double X { get; set; }
        public double Y { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public double Top { get { return Y + Height; } }
        public double Bottom { get { return Y; } }
        public double Left { get { return X; } }
        public double Right { get { return X + Width; } }

        public bool IsEmpty
        {
            get
            {
                if (double.IsPositiveInfinity(X) && double.IsPositiveInfinity(Y) && double.IsNegativeInfinity(Width) && double.IsNegativeInfinity(Height))
                    return true;
                return false;
            }
        }
        #endregion
        #region Constructor

        public CartesianRect()
        {
            X = Y = double.PositiveInfinity;
            Width = Height = double.NegativeInfinity;

        }
        public CartesianRect(double x, double y, double width, double height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        #endregion

    }
}