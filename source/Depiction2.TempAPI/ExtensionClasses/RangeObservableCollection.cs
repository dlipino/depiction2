﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Depiction2.TempAPI.ExtensionClasses
{
    //This was copied from the web, but has been modified, it currently hasnt been stress tested. I also have doubts about this actually working
    //a thread safe observable collection might be a better way to go.
    public class RangeObservableCollection<T> : ObservableCollection<T>
    {
        public EventHandler CollectionCleared;
        private bool _suppressNotification = false;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!_suppressNotification)
                base.OnCollectionChanged(e);
        }

        virtual public void AddRange(IEnumerable<T> list)
        {
            
            if (list == null)
                throw new ArgumentNullException("list");

            _suppressNotification = true;

            foreach (var item in list)
            {
                Add(item);
            }
            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
//            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add,(IList)list));
        }

        virtual public void RemoveRange(IEnumerable<T> itemsToRemove )
        {
            if (itemsToRemove == null)
                throw new ArgumentNullException("itemsToRemove");

            _suppressNotification = true;

            foreach (var item in itemsToRemove)
            {
                Remove(item);
            }
            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
//            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove,(IList)itemsToRemove));//dang the reset 
        }
        protected override void ClearItems()
        {
            if (CollectionCleared != null)
                CollectionCleared(this, EventArgs.Empty);

            base.ClearItems();
        }
    }
}