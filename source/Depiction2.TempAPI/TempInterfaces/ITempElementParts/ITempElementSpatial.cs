﻿using System.Windows;

namespace Depiction2.TempAPI.TempInterfaces.ITempElementParts
{
    public interface ITempElementSpatial
    {
        //Used for the spatial index thing
        int Index { get; set; }//Hopeuflly this is temp
        Rect GeoBounds { get; }
        double GeoArea { get; }
        //End of temp spatial index thing 
    }
}