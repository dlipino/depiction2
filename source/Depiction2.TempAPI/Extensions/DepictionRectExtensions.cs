﻿using System.Windows;

namespace Depiction2.TempAPI.Extensions
{
    static public class DepictionRectExtensions
    {
        public static Rect CreateRectFromCart(Point botLeftCart,Vector directionCart)
        {
            return new Rect(new Point(botLeftCart.X,-botLeftCart.Y),new Vector(directionCart.X,-directionCart.Y));
        }
        public static double TopCart(this Rect rect)
        {
            return -rect.Top;
        }

        public static double BottomCart(this Rect rect)
        {
            return -rect.Bottom;
        }

        public static Point TopLeftCart(this Rect rect)
        {
            return new Point(rect.TopLeft.X, -rect.TopLeft.Y);
        }

        public static Point BottomRightCart(this Rect rect)
        {
            return new Point(rect.BottomRight.X, -rect.BottomRight.Y);
        }

        public static Point CenterCart(this Rect rect)
        {
            return new Point( rect.Width / 2 + rect.Left,-(rect.Height / 2 + rect.Top));
        }

        public static double CenterOfLongestAxis(this Rect rect)
        {
            var centerOfLongAxis = rect.Height / 2 + rect.Bottom;
            if (rect.Width > rect.Height)
            {
                centerOfLongAxis = rect.Width / 2 + rect.Left;
            }
            return centerOfLongAxis;
        }

        
        public static double Area(this Rect rect)
        {
            return rect.Width * rect.Height;
        }
    }
}