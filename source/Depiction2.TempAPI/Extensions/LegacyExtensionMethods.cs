﻿using System;
using System.Linq;
using Depiction2.Base.Story;
using Depiction2.Base.StoryEntities;
using Depiction2.TempAPI.TempInterfaces;

namespace Depiction2.TempAPI.Extensions
{
    public static class LegacyExtensionMethods
    {
        //Copied and slightly modified from 1.2, used for interaction rule runner (mostly)
        //mostly a hack method
        public static object Query(this IElement elementToQuery, string queryString)
        {
            if (elementToQuery == null)
                return null;

            if (queryString.IndexOf(".") == 0)
            {
                queryString = queryString.Substring(1);

                while (queryString.Contains("."))
                {
                    queryString = queryString.Substring(queryString.IndexOf('.') + 1);
                }

                if (queryString.Trim() == String.Empty)
                    return elementToQuery;
                if (queryString.Trim().Equals("ZOI", StringComparison.InvariantCultureIgnoreCase) ||
                    queryString.Trim().Equals("ZoneOfInfluence", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
//                    return elementToQuery.ZoneOfInfluence;
                }
                var prop = elementToQuery.GetPropertyTemp(queryString);
                if (prop == null) return null;
                return prop.Value;
            }

            return null;
        }

//        public static ITempProperty GetProperty(this ITempElement tempElement, string propName)
//        {
//            var props = tempElement.UserPropertyNames.Where(t => t.Name.Equals(propName)).ToList();
//            if (!props.Any()) return null;
//            if (props.Count() > 1) return null;
//            var prop = props.First();
//            return prop;
//        }
    }
}