﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Depiction.ProjectionSystem;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempAPI.TempInterfaces;

namespace Depiction2.TempAPI.QuadItemSource
{
    public class TempSpatialItemsSource<T> : RangeObservableCollection<T>, ZoomableCanvas.ISpatialItemsSource where T : ITempElement
    {
        private Rect _desiredGeoExtent = Rect.Empty;

        /// <summary>
        /// We use a PriorityQuadTree to implement our spatial index.
        /// </summary>
        private readonly PriorityQuadTree<T> _tree = new PriorityQuadTree<T>();

        /// <summary>
        /// Holds the accurate extent of all item geoBounds in the index.  This may be different from _tree.Extent.
        /// </summary>
        private Rect _extent = Rect.Empty;

        /// <summary>
        /// Holds the last query used in order to know when to raise the <see cref="QueryInvalidated"/> event.
        /// </summary>
        private Rect _lastQuery = Rect.Empty;

        /// <summary>
        /// Occurs when the value of the <see cref="Extent"/> property has changed.
        /// </summary>
        public event EventHandler ExtentChanged;

        /// <summary>
        /// Occurs when the results of the last query are no longer valid and should be re-queried.
        /// </summary>
        public event EventHandler QueryInvalidated;

        /// <summary>
        /// Get a list of the items that intersect the given geoBounds.
        /// </summary>
        /// <param name="geoBounds">The geoBounds to test.</param>
        /// <returns>
        /// List of zero or more items that intersect the given geoBounds, returned in the order given by the priority assigned during Insert.
        /// </returns>
        public IEnumerable<int> Query(Rect areaRect)
        {
            var geoBounds = LocationConverter.ConvertDrawToCartisianRect(areaRect);
            var activeBounds = GetActiveBounds(geoBounds, _desiredGeoExtent);
            //Comes in as pixel geoBounds, converter them to geo geoBounds
            _lastQuery = activeBounds;
            var ordered = _tree.GetItemsIntersecting(_lastQuery).OrderByDescending(i => i.GeoArea);
            return ordered.Select(i => i.Index);
        }

        private Rect GetActiveBounds(Rect geoBounds, Rect desiredGeoExtent)
        {
            if (desiredGeoExtent.IsEmpty)
            {
                return geoBounds;
            }
            var bCopy = desiredGeoExtent;

            bCopy.Intersect(geoBounds);
            return bCopy;
        }

        public Rect DesiredGeoExtent
        {
            get { return _desiredGeoExtent; }
            set
            {
                _desiredGeoExtent = value;
                if (ExtentChanged != null)
                {
                    ExtentChanged(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Gets the computed minimum required rectangle to contain all of the items in the index.  This property is also settable for efficiency the future extent of the items is known.
        /// </summary>
        public Rect Extent
        {
            get
            {
                if (_extent.IsEmpty)
                {
                    foreach (var item in this)
                    {
                        _extent.Union(item.GeoBounds);
                    }
                }
                return _extent;
            }
        }

        /// <summary>
        /// Adds or inserts the given <see cref="count"/> of items at the given <see cref="index"/>.
        /// </summary>
        /// <param name="index">The index at which to insert the items.</param>
        /// <param name="count">The number of items to insert.</param>
        /// <remarks>
        /// All items are inserted with geoBounds of <see cref="Rect.Empty"/>, meaning they wi ll be returned from all queries.
        /// </remarks>
        override public void AddRange(IEnumerable<T> itemsToAdd)
        {
            var list = itemsToAdd.ToList();
            var indexStart = this.Count;
            var count = 0;
            foreach (var item in list)// (int i = 0; i < itemsToAdd.Count(); i++)
            {
                item.Index = indexStart + count;
                _tree.Insert(item, item.GeoBounds, 0);
                count++;
            }

            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
            base.AddRange(list);
        }

        /// <summary>
        /// Clears and resets the spatial index to hold the given <see cref="count"/> of items.
        /// </summary>
        /// <param name="count">The number of items within the index.</param>
        public void Reset()//int count)
        {
            _extent = Rect.Empty;
            this.Clear();
            _tree.Clear();
            //            InsertRange(0, count);

            if (ExtentChanged != null)
            {
                ExtentChanged(this, EventArgs.Empty);
            }

            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Optimizes the spatial index based on the current extent if optimization is warranted.
        /// </summary>
        public void Optimize()
        {
            var treeExtent = _tree.Extent;
            var realExtent = Extent;
            if (treeExtent.Top - realExtent.Top > treeExtent.Height ||
                treeExtent.Left - realExtent.Left > treeExtent.Width ||
                realExtent.Right - treeExtent.Right > treeExtent.Width ||
                realExtent.Bottom - treeExtent.Bottom > treeExtent.Height)
            {
                _tree.Extent = realExtent;

                if (QueryInvalidated != null)
                {
                    QueryInvalidated(this, EventArgs.Empty);
                }
            }
        }
    }
}