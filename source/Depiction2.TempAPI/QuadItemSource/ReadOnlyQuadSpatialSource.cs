﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Depiction.ProjectionSystem;
using Depiction2.TempAPI.TempInterfaces;

namespace Depiction2.TempAPI.QuadItemSource
{
    public class ReadOnlyQuadSpatialSource<T> : ReadOnlyObservableCollection<T>, ZoomableCanvas.ISpatialItemsSource where T : ISpatialElement
    {
        
        public ReadOnlyQuadSpatialSource(ObservableCollection<T> list,string ownerName ) : base(list)
        {
            if(list is ObservableQuadSource<T>)
            {
                var oqs = list as ObservableQuadSource<T>;
                _tree = oqs.Tree;
            }
            _areaOwnerName = ownerName;
        }

        public event EventHandler ExtentChanged;
        public event EventHandler QueryInvalidated;
        private string _areaOwnerName = string.Empty;
        private Rect _desiredGeoExtent = Rect.Empty;

        private readonly PriorityQuadTree<T> _tree = new PriorityQuadTree<T>();
        /// <summary>
        /// Holds the last query used in order to know when to raise the <see cref="QueryInvalidated"/> event.
        /// </summary>
        private Rect _lastQuery = Rect.Empty;
        /// <summary>
        /// Holds the accurate extent of all item geoBounds in the index.  This may be different from _tree.Extent.
        /// </summary>
        private Rect _extent = Rect.Empty;

        public HashSet<string> DisplayedElementIds = new HashSet<string>(); 
        /// <summary>
        /// Gets the computed minimum required rectangle to contain all of the items in the index.  This property is also settable for efficiency the future extent of the items is known.
        /// </summary>
        public Rect Extent
        {
            get
            {
                if (_extent.IsEmpty)
                {
                    foreach (var item in this)
                    {
                        _extent.Union(item.SimpleBounds);
                    }
                }
                return _extent;
            }
        }

        public Rect DesiredGeoExtent
        {
            get { return _desiredGeoExtent; }
            set
            {
                _desiredGeoExtent = value;
                if (ExtentChanged != null)
                {
                    ExtentChanged(this, EventArgs.Empty);
                }
            }
        }
        public IEnumerable<int> Query(Rect areaRect)
        {
            var geoBounds = LocationConverter.ConvertPanelToCartisianRect(areaRect);
            var activeBounds = GetActiveBounds(geoBounds, _desiredGeoExtent);
            //Comes in as pixel geoBounds, converter them to geo geoBounds
            _lastQuery = activeBounds;
            var inView = _tree.GetItemsIntersecting(_lastQuery).ToList();
//            return inView.Select(i => i.Index);
            var matchId = inView.Where(x => DisplayedElementIds.Contains(x.ElemId)).ToList();

            return matchId.OrderByDescending(i => i.SimpleBoundsArea).Select(i => i.Index);
//            return _tree.Select(i => i.Index);
        }

        private Rect GetActiveBounds(Rect geoBounds, Rect desiredGeoExtent)
        {
            if (desiredGeoExtent.IsEmpty)
            {
                return geoBounds;
            }
            var bCopy = desiredGeoExtent;

            bCopy.Intersect(geoBounds);
            return bCopy;
        }
    }
}