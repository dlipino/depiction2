﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempAPI.TempInterfaces;

namespace Depiction2.TempAPI.QuadItemSource
{
    //Maybe add dispoose? for testing 1/21/2013
    public class ObservableQuadSource<T> : RangeObservableCollection<T> where T : ISpatialElement
    {
        /// <summary>
        /// We use a PriorityQuadTree to implement our spatial index.
        /// </summary>
        private readonly PriorityQuadTree<T> _tree = new PriorityQuadTree<T>();

        internal PriorityQuadTree<T> Tree { get { return _tree; } }
        /// <summary>
        /// Holds the accurate extent of all item geoBounds in the index.  This may be different from _tree.Extent.
        /// </summary>
        private Rect _extent = Rect.Empty;
        /// <summary>
        /// Gets the computed minimum required rectangle to contain all of the items in the index.  This property is also settable for efficiency the future extent of the items is known.
        /// </summary>
        public Rect Extent
        {
            get
            {
                if (_extent.IsEmpty)
                {
                    foreach (var item in this)
                    {
                        _extent.Union(item.SimpleBounds);
                    }
                }
                return _extent;
            }
        }

        //TODO these need to be tested and optimized April 15 2013
        /// <summary>
        /// Adds or inserts the given <see cref="count"/> of items at the given <see cref="index"/>.
        /// </summary>
        /// <param name="index">The index at which to insert the items.</param>
        /// <param name="count">The number of items to insert.</param>
        /// <remarks>
        /// All items are inserted with bounds of <see cref="Rect.Empty"/>, meaning they will be returned from all queries.
        /// </remarks>
        override public void AddRange(IEnumerable<T> itemsToAdd)
        {
            var list = itemsToAdd.ToList();
//            var indexStart = this.Count;
//            var count = 0;
//            foreach (var item in list)// (int i = 0; i < itemsToAdd.Count(); i++)
//            {
//                item.Index = indexStart + count;
//                _tree.Insert(item, item.SimpleBounds, 0);
//                count++;
//            }
            base.AddRange(list);
            UpdateGeoArea();
        }
        override public void RemoveRange(IEnumerable<T> itemsToRemove)
        {
            var list = itemsToRemove.ToList();
//            foreach (var item in list)// (int i = 0; i < itemsToAdd.Count(); i++)
//            {
//                _tree.Remove(item);
//            }
            base.RemoveRange(list);
            UpdateGeoArea();
        }
        public void AddSingle(T element)
        {
            base.Add(element);
            UpdateGeoArea();
        }
        public void RemoveSingle(T element)
        {
            base.Remove(element);
            UpdateGeoArea();
        }
        /// <summary>
        /// Clears and resets the spatial index to hold the given <see cref="count"/> of items.
        /// </summary>
        /// <param name="count">The number of items within the index.</param>
        public void Reset()//int count)
        {
            Clear();
            _tree.Clear();
        }

        /// <summary>
        /// Update the geo area after a projection change,add, or remove. This is a brute force way of readjusting the
        /// quad tree.
        /// </summary>
        public void UpdateGeoArea()
        {
            _extent = Rect.Empty;
            _tree.Clear();
            var count = 0;
            foreach(var T in this)
            {
                T.Index = count;
                _tree.Insert(T, T.SimpleBounds, 0);
                count++;
            }
//            Optimize();
        }
        /// <summary>
        /// Optimizes the spatial index based on the current extent if optimization is warranted.
        /// </summary>
        public void Optimize()
        {
            var treeExtent = _tree.Extent;
            var realExtent = Extent;
            if (treeExtent.Top - realExtent.Top > treeExtent.Height ||
                treeExtent.Left - realExtent.Left > treeExtent.Width ||
                realExtent.Right - treeExtent.Right > treeExtent.Width ||
                realExtent.Bottom - treeExtent.Bottom > treeExtent.Height)
            {
                _tree.Extent = realExtent;

//                if (QueryInvalidated != null)
//                {
//                    QueryInvalidated(this, EventArgs.Empty);
//                }
            }
        }
    }
}