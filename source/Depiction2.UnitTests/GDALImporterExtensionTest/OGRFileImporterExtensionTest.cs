﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Depiction.GDALImporterExtension;
using Depiction2.API;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.Service.Types;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Scaffold;
using NUnit.Framework;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction2.UnitTests.GDALImporterExtensionTest
{
    [TestFixture]
    public class OGRFileImporterExtensionTest
    {
        //OGRFileImporterExtension.cs
        private OGRFileImporterExtension ogrImporter;
        private OgrFileReadingService ogrReadingService;
        private IElementScaffold genericScaffoldNoProps;
        #region helpers
        public const string gcs =
                  "GEOGCS[\"GCS_WGS_1984\"," +
                      "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                      "PRIMEM[\"Greenwich\",0]," +
                      "UNIT[\"Degree\",0.0174532925199433]" +
                  "]";

        static internal string GetFullFileName(string fileName, string holdingFolder)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("GDALImporterExtensionTest", "TestFiles", holdingFolder);
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            Assert.IsTrue(File.Exists(fullFileName), "Could not find file for ogr import test");
            return fullFileName;
        }

        #endregion

        [SetUp]
        protected void Setup()
        {
            ogrImporter = new OGRFileImporterExtension();
            ogrImporter.InitiateImporter();
            ogrReadingService = new OgrFileReadingService();
            Assert.AreEqual(44, Ogr.GetDriverCount());
            var scaffold = new ElementScaffold("Generic");
            genericScaffoldNoProps = scaffold;
            DepictionAccess.NameTypeService = new DepictionSimpleTypeService();
        }

        [TearDown]
        protected void TearDown()
        {
            genericScaffoldNoProps = null;
            ogrImporter.Dispose();
            ogrReadingService = null;
            OGREnvironment.UnsetOGREnvironment();
            DepictionAccess.NameTypeService = null;
        }

        [Test]
        public void DoesWorldCountryShapeFileLoad()
        {
            DateTime startTime;
            DateTime endTime;
            string elapsedTime;
            var fileName = GetFullFileName("countries.shp", "countriesShp");

            startTime = DateTime.Now;
            var elements = ogrReadingService.CreateElementsFromFile(fileName, genericScaffoldNoProps, gcs, true);
            endTime = DateTime.Now;
            elapsedTime = string.Format("It took {0} create {1} geometries with info", (endTime - startTime).TotalSeconds, elements.Count);
            Debug.WriteLine(elapsedTime);

            Assert.IsNotNull(elements);
            foreach (var element in elements)
            {
                Assert.AreEqual(ElementVisualSetting.Geometry, element.VisualState);
            }

            Assert.AreEqual(248, elements.Count);
            var first = elements.First();
            Assert.AreNotEqual(0, first.AdditionalPropertyNames.Count);
            startTime = DateTime.Now;
            elements = ogrReadingService.CreateElementsFromFile(fileName, genericScaffoldNoProps, gcs, false);
            endTime = DateTime.Now;
            elapsedTime = string.Format("It took {0} create {1} geometries without info", (endTime - startTime).TotalSeconds, elements.Count);
            Debug.WriteLine(elapsedTime);

            //Now test for speed without the property creation

        }

        [Test]
        public void GetLayerPropertiesTest()
        {
            var propsInCommonCount = 11;//value taken after the first run
            var fileName = GetFullFileName("countries.shp", "countriesShp");

            var props = ogrImporter.FindPropertyIntersections(fileName);
            Assert.AreEqual(propsInCommonCount, props.Count);


            var elements = ogrReadingService.CreateElementsFromFile(fileName, genericScaffoldNoProps, gcs, true);
            Assert.AreEqual(248, elements.Count);
            foreach (var elem in elements)
            {
                var properNames = elem.AdditionalPropertyNames.Select(p => p.Item1);
                Assert.IsTrue(props.IsSubsetOf(properNames));
            }
        }

        [Test]
        public void MissplacedTestForElementRepositoryUpdating()
        {//From the previous test grabbing 
            //[1] = "GMI_CNTRY"
            //[0] = "FIPS_CNTRY"
            var fileName = GetFullFileName("countries.shp", "countriesShp");

            var repo = new Story();

            var fileProjSystem = string.Empty;
            var idProp = "GMI_CNTRY";
            var idProp2 = "FIPS_CNTRY";
            var unknownProp = "bla";
            var propToChange = "POP_CNTRY";
            var newVal = 2;
            var propsAndGeo = ogrReadingService.CreateGeometryWrapsFromFile(fileName, string.Empty, out fileProjSystem, true, idProp);
            var propsAndGeo2 = ogrReadingService.CreateGeometryWrapsFromFile(fileName, string.Empty, out fileProjSystem, true, idProp2);
            foreach (var geomAndPropse in propsAndGeo2)
            {
                var popProp = geomAndPropse.properties.FirstOrDefault(t => t.ProperName.Equals(propToChange)) as PropertyScaffold;
                if (popProp == null) continue;
                popProp.ValueString = newVal.ToString(CultureInfo.InvariantCulture);

            }
            var countryCount = 248;
            Assert.AreEqual(countryCount, propsAndGeo.Count, "Did not create correct number of property/geometry holders");
            repo.UpdateRepo(propsAndGeo, idProp, genericScaffoldNoProps);
            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not create correct number of elements");

            //Try to duplicate the add
            repo.UpdateRepo(propsAndGeo, idProp, genericScaffoldNoProps);
            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not replace elements that had matching ids");
            foreach (DepictionElement elem in repo.AllElements)
            {
                if(elem == null) continue;
                foreach (var prop in elem._additionalProperties)
                {
                    Assert.IsNotNull(prop.ValueType);
                }
            }


            repo.UpdateRepo(propsAndGeo2, idProp2, genericScaffoldNoProps);
            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not replace elements that had different matching ids");
            foreach(var elem in repo.AllElements)
            {
                var prop = elem.GetProperty(propToChange);
                Assert.IsNotNull(prop);
                Assert.AreEqual(newVal,prop.Value);
            }

            repo.UpdateRepo(propsAndGeo, unknownProp, genericScaffoldNoProps);
            Assert.AreEqual(countryCount * 2, repo.AllElements.Count, "Did no add elements with nonmatching ids");


        }


        [Test]
        [Ignore("This tests a larger shp file and does really help with simple bugs")]
        public void DoesLummiIslandFileLoad()
        {
            var fileName = GetFullFileName("WC_AR_TaxParcel.shp", "LummiTaxParcelsShp");
            var elements = ogrReadingService.CreateElementsFromFile(fileName, genericScaffoldNoProps, gcs, false);
            Assert.IsNotNull(elements);

            Assert.AreEqual(106781, elements.Count);
        }

        [Test]
        [Ignore("Test to help optimize the .shp file reading, not really a functionality test")]
        public void FunWithTime()
        {
            var fileName = GetFullFileName("WC_AR_TaxParcel.shp", "LummiTaxParcelsShp");
            DateTime startTime;
            DateTime endTime;
            string elapsedTime;
            List<GeomAndProps> geomAndPropList;
            string filePCS;
            //Base for now properties is 15secs
            //             startTime = DateTime.Now;
            //             elementList = ogrImporter.CreateGeometryWrapsFromFile(fileName, gcs, out filePCS, false);
            //
            //             endTime = DateTime.Now;
            //             elapsedTime = string.Format("It took {0} create {1} geometries without info", (endTime - startTime).TotalSeconds, elementList.Count);
            //            Console.WriteLine(elapsedTime);

            startTime = DateTime.Now;
            geomAndPropList = ogrReadingService.CreateGeometryWrapsFromFile(fileName, gcs, out filePCS, true, string.Empty);

            endTime = DateTime.Now;
            elapsedTime = string.Format("It took {0} create {1} geometries without info", (endTime - startTime).TotalSeconds, geomAndPropList.Count);
            Console.WriteLine(elapsedTime);
        }


        [Test]
        [Ignore("Having fun with coordinate systems")]
        public void FunWithJson()
        {
            var testFile2 = @"D:\DepictionData\WorldOutline\countries.shp";
            Assert.IsTrue(File.Exists(testFile2));
            var dataSource = Ogr.Open(testFile2, 0);
            var layerCount = dataSource.GetLayerCount();
            for (int i = 0; i < layerCount; i++)
            {
                Feature nextFeature;
                var layer = dataSource.GetLayerByIndex(i);
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var mainGeom = nextFeature.GetGeometryRef();
                    string[] stuff = new string[0];
                    var result = mainGeom.ExportToJson(stuff);
                }
            }
        }
        [Test]
        [Ignore("Having fun with coordinate systems")]
        public void FunWithGeographicAndProjectedCoordinateSystems()
        {
            var sr = new SpatialReference("");

            sr.SetProjCS("Mercator");
            sr.SetWellKnownGeogCS("WGS84");
            sr.SetMercator(0, 0, 1, 0, 0);
            //            var s = sr.GetSemiMinor();
            //            sr.SetGeogCS("Popular Visualisation CRS", "WGS84", "WGS84", 6378137.0, 298.257223563, "Greenwich", 0,
            //                         "degree", 0.0174532925199433);
            string res;
            sr.ExportToWkt(out res);
        }
    }
}