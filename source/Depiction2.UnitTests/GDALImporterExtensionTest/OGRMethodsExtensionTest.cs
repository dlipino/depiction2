﻿using Depiction.GDALImporterExtension;
using NUnit.Framework;

namespace Depiction2.UnitTests.GDALImporterExtensionTest
{
    [TestFixture]
    public class OGRMethodsExtensionTest
    {
        [Test]
        public void GetPointsFromWKTStringTest()
        {
           var polyString ="MULTIPOLYGON (((-122.685339999569 48.705254240177702,-122.685331550771 48.705178311196804,-122.685340671717 48.705259965151001,-122.685339999569 48.705254240177702)),((-122.685344671197 48.705294079734202,-122.68534071345201 48.705260315426401,-122.686929367148 48.705266050634698,-122.686964944312 48.705377529338698,-122.685350551155 48.705344250038202,-122.685344671197 48.705294079734202)))";

            var methods = new OGRMethodsExtension();

            var output = methods.GetPointsFromWKTString(polyString);
            Assert.AreEqual(1,output.Count);
            Assert.IsNotNull(output[0]);
            Assert.AreEqual(1,output[0].ChildLists.Count);

            var singleString =
                "POLYGON ((-122.704768668802 48.721920393974102,-122.704533267889 48.721921476008397,-122.70453789825299 48.721638669650403,-122.704773285149 48.721640398076502,-122.704768668802 48.721920393974102))";
            output = methods.GetPointsFromWKTString(singleString);
            Assert.AreEqual(1, output.Count);
            Assert.IsNotNull(output[0]);
            Assert.IsNull(output[0].ChildLists);
        }
    }
}