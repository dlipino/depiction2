﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Depiction.GDALImporterExtension;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.StoryEntities.Scaffold;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction2.UnitTests.GDALImporterExtensionTest
{
    [TestFixture]
    public class GdalFileImporterExtensionTest
    {
        //OGRFileImporterExtension.cs
        private GdalFileImporterExtension gdalImporter;
        private IElementScaffold genericScaffold;
        private DepictionFolderService _depictionFolder;
        #region helpers
        //           adfGeoTransform[0] /* top left x */
        //    adfGeoTransform[1] /* w-e pixel resolution */
        //    adfGeoTransform[2] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[3] /* top left y */
        //    adfGeoTransform[4] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[5] /* n-s pixel resolution */
        [Test]
        public void CreateAGeocodedImage()
        {
            //create geo reference image via gdal
            var testImageName = "UnLocatedImage.jpg";
            var fullName = OGRFileImporterExtensionTest.GetFullFileName(testImageName, "Images");
            var dsOriginal = Gdal.Open(fullName, Access.GA_ReadOnly);
            var origWidth = 1920;
            var origHeight = 1200;
            var newWidth = 1920;
            var newHeight = 1200;
            var outImage = "geolocated.tif";
            var tempOutImage = Path.Combine(_depictionFolder.FolderName, outImage);
            //            var saveOptions = new string[] { "TFW=YES" }; 
            var saveOptions = new string[] { };
            var topLeft = new Point(-122.36085, 47.61819);
            var botRight = new Point(-122.30937, 47.58558);
            var dis = botRight - topLeft;
            var disPoint = new Point(dis.X / origWidth, dis.Y / origHeight);

            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
            var gcsTransform = new double[] { topLeft.X, disPoint.X, 0, topLeft.Y, 0, disPoint.Y };
            geoTiff.SetGeoTransform(gcsTransform);
            var outTrans = new double[6];
            geoTiff.GetGeoTransform(outTrans);
            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            string gsWkt;
            srs.ExportToWkt(out gsWkt);
            geoTiff.SetProjection(gsWkt);

            geoTiff.FlushCache();
            geoTiff.Dispose();
        }

        #endregion
        #region setup/teardown
        [SetUp]
        protected void Setup()
        {
            gdalImporter = new GdalFileImporterExtension();
            gdalImporter.InitiateImporter();
            Assert.AreEqual(98, Gdal.GetDriverCount());
            var scaffold = new ElementScaffold("Generic");
            genericScaffold = scaffold;
            _depictionFolder = new DepictionFolderService(true);
        }

        [TearDown]
        protected void TearDown()
        {
            genericScaffold = null;
            gdalImporter.Dispose();
            GDALEnvironment.SetGDALEnvironment();
            _depictionFolder.Close();
        }
        #endregion
        [Test]
        public void ReadGeoLocatedTiffImageTest()
        {
            var fileName = "geolocated.tif";
            var fullFileName = OGRFileImporterExtensionTest.GetFullFileName(fileName, "Images");
            var projector = new DepictionGdalProjectionConverter();
            var element = gdalImporter.GetImageFromFile(fullFileName, genericScaffold, projector);

            var geom = element.ElementGeometry;
            var tlF = new Point(geom.Bounds.Left, geom.Bounds.Bottom);
            var brF = new Point(geom.Bounds.Right, geom.Bounds.Top);
            var tlExp = new Point(-122.36085, 47.61819);
            var brExp = new Point(-122.30937, 47.58558);
            var rotExp = 0;

            Assert.AreEqual(tlExp, tlF);
            Assert.AreEqual(brExp, brF);
            Assert.AreEqual(rotExp, element.ImageRotation);
            Assert.AreEqual(fileName, element.ImageResourceName);

            Assert.AreEqual(ElementVisualSetting.Image, element.VisualState);

        }

        [Test]
        public void ReadNonGeoLocatedTiffImageTest()
        {
            var fileName = "nongeolocated.tif";
            var fullFileName = OGRFileImporterExtensionTest.GetFullFileName(fileName, "Images");
            var projector = new DepictionGdalProjectionConverter();
            var element = gdalImporter.GetImageFromFile(fullFileName, genericScaffold, projector);

            var geom = element.ElementGeometry;
            Assert.AreEqual(Rect.Empty, geom.Bounds);
            var rotExp = 0;

            Assert.AreEqual(rotExp, element.ImageRotation);
            Assert.AreEqual(fileName, element.ImageResourceName);

            Assert.AreEqual(ElementVisualSetting.Image, element.VisualState);

        }
        //        [Ignore("This sucks")]
        //        [Test]
        //        public void GeoTiffImportTest()
        //        {
        //            var tempDir = temp.FolderName;
        //            var baseName = "tfwImage";
        //            var imageName = baseName + ".tif";
        //            var tempImageName = Path.Combine(tempDir, imageName);
        //            string fileStart = "Depiction.UnitTests.TestImages.WorldFile.";
        //            string assemblyName = "Depiction.UnitTests";
        //            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
        //            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
        //            var dsOriginal = Gdal.Open(tempImageName, Access.GA_ReadOnly);
        //            var tempOut = "C:\\dlp\\depTest";
        //            var outName = "outtiffRoundTrip.tif";
        //            var tempOutImage = Path.Combine(tempOut, outName);
        //            //            var saveOptions = new string[] { "TFW=YES" };
        //            var saveOptions = new string[] { };
        //            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
        //
        //            IProjectedCoordinateSystem popularVisualizationCS = CoordinateSystemWktReader.Parse(popularVisualisationWKT) as IProjectedCoordinateSystem;
        //            //Cant get an inverse out of this one
        //            //            const string geowkt = "GEOGCS[\"WGS 84\"," +
        //            //                                  "DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
        //            //                                  "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
        //            //                                  "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
        //            //            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geowkt) as IGeographicCoordinateSystem;
        //
        //
        //            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geoWKT) as IGeographicCoordinateSystem;
        //
        //
        //            CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();
        //
        //            //// MORE THAN MEETS THE EYE!!
        //            var transformer = ctfac.CreateFromCoordinateSystems(geoCS, popularVisualizationCS);
        //
        //            var topLeftLatLong = new LatitudeLongitude(47.9320413898329, -122.374351441618);
        //            var botLatLong = new LatitudeLongitude(47.5776691792137, -121.954224837974);
        //
        //            var topleft = WorldToGeoCanvas(topLeftLatLong, transformer);
        //            var bottomRight = WorldToGeoCanvas(botLatLong, transformer);
        //            var imagePixWidth = dsOriginal.RasterXSize;//pixWidth of baseimage
        //            var imagePixHeight = dsOriginal.RasterYSize;//pixheight of baseimage
        //
        //            var widthDis = bottomRight.X - topleft.X;
        //            var heightDis = bottomRight.Y - topleft.Y;
        //            // top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
        //            double pixelWidth = Math.Abs(widthDis / imagePixWidth);
        //            double pixelHeight = Math.Abs(heightDis / imagePixHeight);
        //            //Alright so there are oddities with the saved TFW files and the ones that depiction uses as the truth. I believe the gdal since it seems more consistent
        //            //with what sources say about the tfw files. ie we shouldn't use the  + (pixelWidth / 2) and  - (pixelHeight / 2)
        //            //The removal of half pixel height is because spatial reference is expecting the mid point (real world length) of the topleft coordinate pixel
        //            //                var wgs84Transform = new double[] { tlUTM.EastingMeters + (pixelWidth / 2), pixelWidth, 0, tlUTM.NorthingMeters - (pixelHeight / 2), 0, -pixelHeight };
        //            var wgs84Transform = new double[] { topleft.X, pixelWidth, 0, topleft.Y, 0, -pixelHeight };
        //            geoTiff.SetGeoTransform(wgs84Transform);
        //            geoTiff.SetProjection(popularVisualisationWKT);
        //            geoTiff.FlushCache();
        //            geoTiff.Dispose();
        //
        //            var reloadTiff = Gdal.Open(tempOutImage, Access.GA_ReadOnly);
        //            var outTrans = new double[6];
        //            reloadTiff.GetGeoTransform(outTrans);
        //            var proj = reloadTiff.GetProjection();
        //            var projRef = reloadTiff.GetProjectionRef();
        //            var topLeftPoint = new System.Windows.Point(outTrans[0], outTrans[3]);
        //            var topLeftCart = GeoCanvasToWorld(topLeftPoint, transformer);
        //        }
    }
}