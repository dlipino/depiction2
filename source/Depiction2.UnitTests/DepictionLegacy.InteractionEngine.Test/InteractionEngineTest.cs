﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Depiction2.Base.Extensions;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Extensions;
using Depiction2.Core.Interactions;
using Depiction2.Core.Interactions.Helpers;
using Depiction2.Core.StoryEntities;
using Depiction2.TempExtensionCore;
using NUnit.Framework;

namespace Depiction2.UnitTests.DepictionLegacy.InteractionEngine.Test
{
    [TestFixture]
    public class InteractionEngineTest
    {
        private InteractionGraph testGraph = null;
        private List<IElement> testElements = null;
        private Story testStory;

        const string propertySetBehaviour = "SetProperty";
        private const string selfChangePropName = "Changed";
        private const string objectElemName = "object";
        private const string redElemName = "red";
        private const string greenElemName = "green";
        #region helpers 1.4


        private static List<IElement> AddStockElements()
        {
            var elementRepository = new List<IElement>();
            var elementList = new List<IElement> { CreateSimpleElement(objectElemName), 
                CreateSimpleElement("notPartOfGraph"), 
                CreateSimpleElement(redElemName), 
                CreateSimpleElement(greenElemName), 
                CreateSimpleElement(greenElemName) };

            elementRepository.AddRange(elementList);

            return elementRepository;
        }
        //        string propertyInternalName, string displayName, object inValue, object initialValue,
        //                IValidationRule[] valRules, SerializableDictionary<string, string[]> postSetActs
        private static IElement CreateSimpleElement(string elementType)
        {
            var element = new DepictionElement(elementType);
            var prop = new ElementProperty("DisplayName");//("DisplayName", string.Empty, "Mock", "Mock", null, null);
           
            prop.SetValue("Mock");
            element.AddUserProperty(prop);
            //            element.RemovePropertyIfNameAndTypeMatch(prop);
            //            element.AddPropertyOrReplaceValueAndAttributes(prop);
            //            element.SetInitialPositionAndZOI(new LatitudeLongitude(1, 2), null);
            return element;
        }

        private static IElement CreateElementWithTestPostSetAction(string elementType)
        {
            var element = new DepictionElement(elementType);

            var postSetAction = new Dictionary<string, string[]>();
            postSetAction.Add(propertySetBehaviour, new[] { selfChangePropName + ":false" });

            var prop = new ElementProperty("DisplayName");//("DisplayName", string.Empty, "Mock", "Mock", null, postSetAction);

            prop.SetValue("Mock");
            prop.LegacyPostSetActions = postSetAction;
            element.AddUserProperty(prop);
            prop = new ElementProperty(selfChangePropName);//(selfChangePropName, selfChangePropName, true, true, null, null);

            prop.SetValue(true);
            element.AddUserProperty(prop);

            //            element.SetInitialPositionAndZOI(new LatitudeLongitude(1, 2), null);
            var elementProp = element.GetProperty("DisplayName");
            Assert.IsNotNull(elementProp);
            Assert.IsNotNull(elementProp.LegacyPostSetActions);
            return element;
        }

        #endregion

        #region legacy helpers

        private static IElement FindFirstElementByInternalTypeName(IEnumerable<IElement> elements, string elementType)
        {
            try
            {
                var element =
                    elements.FirstOrDefault(
                        t => t.ElementType.Equals(elementType, StringComparison.InvariantCultureIgnoreCase));

                return element;
            }
            catch
            {
                return null;
            }
        }
        private static IEnumerable<IElement> FindElementsByInternalTypeName(IEnumerable<IElement> elements, string elementType)
        {
            try
            {
                var element =
                    elements.Where(
                        t => t.ElementType.Equals(elementType, StringComparison.InvariantCultureIgnoreCase));

                return element;
            }
            catch
            {
                return null;
            }
        }
        private static List<DepictionInteractionMessage> GetDirtyList(IEnumerable<IElement> elementRepository)
        {
            var dirtyList = new List<DepictionInteractionMessage>();
            foreach (var obj in elementRepository) dirtyList.Add(new DepictionInteractionMessage(obj));
            return dirtyList;
        }

        #endregion
        #region helpers

        private static IInteractionRule CreateNewInteractionRule(string triggerElementType, string affectedElementType)
        {
            return CreateNewInteractionRule(Guid.NewGuid().ToString(), triggerElementType, affectedElementType);
        }

        private static IInteractionRule CreateNewInteractionRule(string ruleName, string triggerElementType, string affectedElementType)
        {
            var rule = new InteractionRule { Publisher = triggerElementType, Name = ruleName };
            rule.Subscribers.Add(affectedElementType);
            return rule;
        }

        private static InteractionRule CreateNewInteractionRule(string ruleName, string triggerElementType, string affectedElementType, string propertyName, string newValue)
        {
            var rule = new InteractionRule { Publisher = triggerElementType, Name = ruleName };
            rule.Subscribers.Add(affectedElementType);
            rule.Behaviors.Add(propertySetBehaviour, new[] { new ElementFileParameter { Type = ElementFileParameterType.Subscriber, ElementQuery = propertyName }, 
                new ElementFileParameter { Type = ElementFileParameterType.Value, ElementQuery = newValue } });
            return rule;
        }
        private static InteractionRule CreateRuleWithBehaviorAndCondition(string ruleName, string triggerElementType, string affectedElementType, string propertyName, string newValue,
            string conditionName, HashSet<string> conditionTriggerProperties)
        {
            var rule = new InteractionRule { Publisher = triggerElementType, Name = ruleName };
            rule.Subscribers.Add(affectedElementType);
            rule.Behaviors.Add(propertySetBehaviour, new[] { new ElementFileParameter { Type = ElementFileParameterType.Subscriber, ElementQuery = propertyName }, 
                new ElementFileParameter { Type = ElementFileParameterType.Value, ElementQuery = newValue } });
            rule.Conditions.Add(conditionName, conditionTriggerProperties);
            return rule;
        }
        private static IElementProperty CreateProperty(string name)
        {
            var prop = new ElementProperty(name);
            return prop;
        }

        #endregion

        [SetUp]
        public void Setup()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
            testElements = new List<IElement>();
            testStory = new Story();
//            Assert.IsTrue(DepictionExtensionLibrary.Instance.AreBehavioursInitialized(),"Behaviours are no there yet");
            
        }
        [TearDown]
        public void TearDown()
        {
            testElements = null;
            testStory = null;
            DepictionExtensionLibrary.Instance.Decompose();
        }

        [Test]
        public void SelfChangingElementTest()
        {
            var element = CreateElementWithTestPostSetAction("SelfTester");
            testElements.Add(element);
            var val = element.Query("." + selfChangePropName);
            Assert.AreEqual(true, val);
            element.SetPropertyValue("displayName", "other");
            Assert.AreEqual(false, element.Query("." + selfChangePropName));
        }

        [Test]
        public void DoesForegroundThreadRouterWithNoInputEndAfterItStarts()
        {
            var ruleRepo = new InteractionRuleRepository();
            var router = new Router(testStory, ruleRepo);

            var element = CreateElementWithTestPostSetAction("SelfTester");
            testStory.AddElement(element);
            router.DoSingleInteractionPassNoMessages();
        }
        [Test]
        public void DoesForegroundThreadRouterFinishWithSingleInput()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var router = new Router(testStory, ruleRepo);

            testStory.AddElement(CreateSimpleElement(objectElemName));
            testStory.AddElement(CreateSimpleElement(redElemName));

            router.DoSingleInteractionPassNoMessages();
        }
        [Test]
        public void DoesForegroundThreadRouterFinishWithMultipleInput()
        {

            var ruleRepo = new InteractionRuleRepository();
            //            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var router = new Router(testStory, ruleRepo);

            testStory.AddElement(CreateSimpleElement(objectElemName));
            testStory.AddElement(CreateSimpleElement(redElemName));
            testStory.AddElement(CreateSimpleElement(redElemName));
            testStory.AddElement(CreateSimpleElement(redElemName));
            testStory.AddElement(CreateSimpleElement(redElemName));

            router.DoSingleInteractionPassNoMessages();
        }

        #region tests from 1.2
        [Test]
        public void AreAllRulesAppliedAfterExecuting()
        {
            var elementRepository = AddStockElements();
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);

            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, greenElemName).Query(".DisplayName"));
            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
            var greens = FindElementsByInternalTypeName(elementRepository, greenElemName);
            Assert.AreEqual(2, greens.Count());
            foreach (var elem in greens)
            {
                Assert.AreEqual("greenie", elem.Query(".DisplayName"));
            }
            Assert.AreEqual("reddie", FindFirstElementByInternalTypeName(elementRepository, redElemName).Query(".DisplayName"));
            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, objectElemName).Query(".DisplayName"));
            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, "notPartOfGraph").Query(".DisplayName"));

        }

        [Test]
        public void AreOnlyDirtyElementsAffected()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var elementRepository = AddStockElements();
            var dirtyList = new List<DepictionInteractionMessage>();

            var greenElements = FindElementsByInternalTypeName(elementRepository, greenElemName);
            foreach (var element in greenElements)
            {
                var interactionMessage = new DepictionInteractionMessage(element);
                dirtyList.Add(interactionMessage);
            }

            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, dirtyList);
            Assert.AreEqual(3, graph.VertexCount);
            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), dirtyList);
            Assert.AreEqual(0, graph.VertexCount);
        }
        [Test]
        public void DoesChangingATriggeredElementExecuteRule()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));

            var elementRepository = AddStockElements();
            // all objects not dirty
            var interactionMessage =
                new DepictionInteractionMessage(FindFirstElementByInternalTypeName(elementRepository, "green"));
            var dirtyList = new List<DepictionInteractionMessage> { interactionMessage };
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, dirtyList);
            Assert.AreEqual(2, graph.VertexCount);
            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), dirtyList);
        }

        [Test]
        public void DoesExecuteInteractionsFinish()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var elementRepository = AddStockElements();
            //Order is important, i think
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, GetDirtyList(elementRepository));
            Assert.AreEqual(4, graph.VertexCount);
            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void DoesGraphBuilderReturnGraph()
        {

            // first rule object->green
            var rule1 = new InteractionRule { Publisher = "object", Name = "rule1" };
            rule1.Subscribers.Add("green");
            // rule2 object->red
            var rule2 = new InteractionRule { Publisher = "object", Name = "rule2" };
            rule2.Subscribers.Add("red");

            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(rule1);
            ruleRepo.AddInteractionRule(rule2);

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, GetDirtyList(elementRepository));
            Assert.AreEqual(4, graph.VertexCount);
            Assert.AreEqual(3, graph.EdgeCount);
        }

        [Test]
        public void DoesRebuildExcludeEdges()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, GetDirtyList(elementRepository));
            graph.ExcludeEdge(FindFirstElementByInternalTypeName(elementRepository, "object"),
                              FindFirstElementByInternalTypeName(elementRepository, "green"),
                              ruleRepo.InteractionRules[0]);
            graph.RebuildInterElementInteractionGraph(elementRepository);
            Assert.AreEqual(2, graph.EdgeCount);
        }

        [Test]
        public void DoesRebuildExcludeVertices()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(new ReadOnlyCollection<IElement>(elementRepository), GetDirtyList(elementRepository));
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "red"));
            graph.RebuildInterElementInteractionGraph(elementRepository);
            Assert.AreEqual(3, graph.VertexCount);
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "green"));
            graph.RebuildInterElementInteractionGraph(elementRepository);
            Assert.AreEqual(2, graph.VertexCount);
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "object"));
            graph.RebuildInterElementInteractionGraph(elementRepository);
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void DoesTraversalTraverseEveryVertex()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(elementRepository, GetDirtyList(elementRepository));

            graph.ExecuteCurrentInteractionGraph();
            foreach (var vertex in graph.Vertices)
            {
                Assert.AreEqual(1, vertex.NumberIncomingEdgesFired);
            }
        }

        [Test]
        public void DoWeHandleCyclesAndCompleteAnyway()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("red", "blue"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("blue", "green"));

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void IsReadonlyCollectionDynamicallyUpdated()
        {
            var ruleRepo = new InteractionRuleRepository();
            var rules = ruleRepo.InteractionRules;
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            Assert.AreEqual(1, rules.Count);
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            Assert.AreEqual(2, rules.Count);
        }

        [Test]
        public void SecondCycleTest()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("red", "green"));

            var elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);

            graph.ExecuteInteractions(new ReadOnlyCollection<IElement>(elementRepository), GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }
        #endregion
    }
}