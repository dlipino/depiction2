﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using Depiction2.API.Service;
using NUnit.Framework;
using Newtonsoft.Json;

namespace Depiction2.UnitTests
{
    [TestFixture]
    public class JsonTests
    {
        private DepictionFolderService folderService;
        private string tempFile;

        [SetUp]
        public void FolderSetup()
        {
            folderService = new DepictionFolderService(true);
            tempFile = folderService.GetATempFilenameInDir("json");
        }

        [TearDown]
        public void FolderTearDown()
        {
            folderService.Close();
        }

        [Test]
        public void JsonDictionaryToFileAndBackTest()
        {
            var geocodeCache = new Dictionary<string, Point>();
            geocodeCache.Add("first",new Point(3,8));

            var jsonS = JsonConvert.SerializeObject(geocodeCache);
            File.WriteAllText(tempFile, jsonS);
            var stringResult = File.ReadAllText(tempFile);
            var newCache = JsonConvert.DeserializeObject<Dictionary<string, Point>>(stringResult);
            Assert.IsNotNull(newCache);
            Assert.AreEqual(geocodeCache,newCache);

            using (FileStream fs = File.Open(tempFile, FileMode.OpenOrCreate))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, geocodeCache);
            }

            Dictionary<string, Point> readResult = null; 
            using (FileStream fs = File.Open(tempFile, FileMode.OpenOrCreate))
            using (var sw = new StreamReader(fs))
            using (var jw = new JsonTextReader(sw))
            {
                var serializer = new JsonSerializer();
                var result = serializer.Deserialize<Dictionary<string, Point>>(jw);
                readResult = result as Dictionary<string, Point>;
            }

            Assert.IsNotNull(readResult);
            Assert.AreEqual(geocodeCache,readResult);
        }


        [Test]
        public void JsonListKeyValuePairsToFileAndBackTest()
        {
            var geocodeCache = new List<KeyValuePair<string, Point>>();
            geocodeCache.Add(new KeyValuePair<string, Point>("first", new Point(3, 8)));
            geocodeCache.Add(new KeyValuePair<string, Point>("second", new Point(2, 7)));

            var jsonS = JsonConvert.SerializeObject(geocodeCache);
            File.WriteAllText(tempFile, jsonS);
            var stringResult = File.ReadAllText(tempFile);
            var newCache = JsonConvert.DeserializeObject<List<KeyValuePair<string, Point>>>(stringResult);
            Assert.IsNotNull(newCache);
            Assert.AreEqual(geocodeCache, newCache);

            using (FileStream fs = File.Open(tempFile, FileMode.OpenOrCreate))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, geocodeCache);
            }

            List<KeyValuePair<string, Point>> readResult = null;
            using (FileStream fs = File.Open(tempFile, FileMode.OpenOrCreate))
            using (var sw = new StreamReader(fs))
            using (var jw = new JsonTextReader(sw))
            {
                var serializer = new JsonSerializer();
                var result = serializer.Deserialize<List<KeyValuePair<string, Point>>>(jw);
                readResult = result as List<KeyValuePair<string, Point>>;
            }

            Assert.IsNotNull(readResult);
            Assert.AreEqual(geocodeCache, readResult);
        }
    }
}