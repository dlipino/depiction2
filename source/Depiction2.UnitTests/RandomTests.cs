﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace Depiction2.UnitTests
{
    [TestFixture]
    public class RandomTests
    {
        private class PropHolder
        {
            public string ElementType { get; set; }
            public PropHolder()
            {
                ElementType = "none";
            }
        }
        [Test]
        [Ignore]
        public void RandomTest()
        {
            var availableElementNames = new List<PropHolder>();
            var matchingElements =
                availableElementNames.Where(t => t.ElementType.Contains("t")).ToList();
        }
        [Test]
        [Ignore]
        public void REgexTest()
        {
            var valueStrings = new[] {"123N,435W", "NaN,NaN", "343.6,13"};
            foreach(var valueString in valueStrings)
            {
                if (!Regex.IsMatch(valueString, "[WwnNeEsS]"))//not north because of NaN
                {
                    int j = 1;
                }
                else
                {
                    int j = 0;
                }
            }
        }
//        [Test]
//        [Ignore]
//        public void NumberStringDifference()
//        {
//            Assert.IsTrue("1.4.3.1234">"1.3.3.1234");
//        }
    }
}