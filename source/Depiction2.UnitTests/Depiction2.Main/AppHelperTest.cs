﻿using System;
using System.Linq;
using Depiction2.API;
using Depiction2.Core.Product;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.Main;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Main
{
    [TestFixture]
    public class AppHelperTest
    {
        private int defaultAppElementCount = 4;

        [SetUp]
        public void SetUp()
        {
            Assert.IsNull(DepictionAccess.PathService);
            Assert.IsNull(DepictionAccess.ScaffoldLibrary);
            DepictionAccess.ProductInformation = new Depiction2UnitTest();
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            DepictionAccess.ExtensionLibrary.ComposeFromDir(dir);
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ProductInformation = null;
            var depictionPathService = DepictionAccess.PathService as DepictionPathService;
            if (depictionPathService != null)
            {
                (depictionPathService).CleanForTest();
            }
            DepictionAccess.PathService = null;
            var library = DepictionAccess.ScaffoldLibrary as ElementScaffoldLibrary;
            if(library != null)
            {
                library.Dispose();
            }
            DepictionAccess.ScaffoldLibrary = null;
            DepictionAccess.ExtensionLibrary.Decompose();

        }
        [Test]
        public void InitializeLibraryAndPathServiceTest()
        {
            AppHelper.InitializeLibraryAndPathService();
            Assert.AreEqual(defaultAppElementCount, DepictionAccess.ScaffoldLibrary.ProductElementScaffolds.Count());

        }
        [Test]
        public void InitializeLibraryAndPathServicePart2Test()
        {
            AppHelper.InitializeLibraryAndPathService();
            Assert.AreEqual(defaultAppElementCount, DepictionAccess.ScaffoldLibrary.ProductElementScaffolds.Count());

        }
    }
}