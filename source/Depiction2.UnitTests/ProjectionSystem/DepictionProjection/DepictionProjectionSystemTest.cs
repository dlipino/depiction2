﻿using NUnit.Framework;
using OSGeo.OSR;

namespace Depiction2.UnitTests.ProjectionSystem.DepictionProjection
{
    [TestFixture]
    public class DepictionProjectionSystemTest
    {

        [Test]
        public void UTMToGeoConversionTest()
        {
            var lat = 47.61799;
            var lon = -122.32226;
            var utmZone = 10;

            var easting = 550928.35;
            var northing = 5274065.32;
            var projs = Osr.GetProjectionMethods();
            //Map Datum NAD83/WGS84 //northern hemispher
            //            Zone: 10 
            //Easting: 550928.35
            //Northing: 5274065.32
            SpatialReference oUTM = new SpatialReference(string.Empty);
            SpatialReference poLatLong;
            CoordinateTransformation poTransform, uoTransform;

            oUTM.SetProjCS("UTM 10 / WGS84");
            oUTM.SetWellKnownGeogCS("WGS84");
            //true = 1
            //false = 0, i hate binary choices
            oUTM.SetUTM(utmZone, 1);

            poLatLong = oUTM.CloneGeogCS();

            poTransform = new CoordinateTransformation(oUTM, poLatLong);
            var results = new double[3];
            poTransform.TransformPoint(results, easting, northing, 0);
            var lonOut = results[0];
            Assert.AreEqual(lon, lonOut, .000001);
            var latOut = results[1];
            Assert.AreEqual(lat, latOut, .000001);
            uoTransform = new CoordinateTransformation(poLatLong, oUTM);
            uoTransform.TransformPoint(results, lon, lat, 0);
            var eOut = results[0];
            Assert.AreEqual(easting, eOut, .01);
            var nOut = results[1];
            Assert.AreEqual(northing, nOut, .01);
        }
        public const string WorkingMercator = "PROJCS[\"WGS 84 / World Mercator\",GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
         "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]]," +
         "PROJECTION[\"Mercator_1SP\"],PARAMETER[\"central_meridian\",0],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",0],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"3395\"]," +
         "AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH]]";

        [Test]
        public void MercatorCreationTest()
        {
            var referenceSRS = new SpatialReference(WorkingMercator);
            var referenceWCS = new SpatialReference("");
            referenceWCS.SetWellKnownGeogCS("WGS84");

            var testMerc = referenceWCS.Clone();
            testMerc.SetMercator(0, 0, 1, 0, 0);
            var lat = 47.61799;
            var lon = -122.32226;

            var baseTrans = new CoordinateTransformation(referenceWCS, referenceSRS);
            double[] baseRes = new[] { lon, lat };
            baseTrans.TransformPoint(baseRes);

            var testTrans = new CoordinateTransformation(referenceWCS, testMerc);
            double[] testRes = new[] { lon, lat };
            testTrans.TransformPoint(testRes);
            for (int i = 0; i < baseRes.Length;i++ )
            {
                Assert.AreEqual(baseRes[i], testRes[i]);
            }
        }
        [Test]
        public void ProjectionSettingTest()
        {
            var badProj = "moooo";
            var proj1 = "SRS_PT_MERCATOR_1SP";
            var proj2 = "SRS_PT_NEW_ZEALAND_MAP_GRID";// "SRS_PT_MERCATOR_2SP";
            var referenceSRS = new SpatialReference(WorkingMercator);
            var srs = new SpatialReference("");
            int result;

            result = srs.SetWellKnownGeogCS("WGS84");
            string geoString;
            srs.ExportToPrettyWkt(out geoString, 1);
            Assert.AreEqual(1, srs.IsGeographic());


            result = srs.SetMercator(0, 0, 1, 0, 0);
            srs.AutoIdentifyEPSG();
            string projString;
            srs.ExportToPrettyWkt(out projString, 1);
            Assert.AreEqual(1, srs.IsProjected());

            result = srs.SetProjection(proj2);
            string proj2String;
            srs.ExportToPrettyWkt(out proj2String, 1);
            Assert.AreEqual(1, srs.IsProjected());

            result = srs.SetUTM(10, 1);
            string UTMString;
            srs.ExportToPrettyWkt(out UTMString, 1);
            Assert.AreEqual(1, srs.IsProjected());

            var d = srs.SetProjCS(proj1);

        }
    }
}