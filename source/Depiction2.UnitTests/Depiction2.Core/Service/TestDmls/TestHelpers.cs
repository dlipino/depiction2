﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.Element;

namespace Depiction2.UnitTests.Depiction2.Core.Service.TestDmls
{
    public class TestHelpers
    {
        public const string TesterName = "Sfseyt"; 
        public const string TesterNameDisplay = "fyfwj";
        public Dictionary<string, string> DefaultTestProperties { get; set; }
        public TestHelpers()
        {
            DefaultTestProperties = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            DefaultTestProperties.Add(PropNames.ZOIBorderColor, Brushes.Black.ToString());
            DefaultTestProperties.Add(PropNames.ZOIFillColor, Brushes.Red.ToString());
            DefaultTestProperties.Add(PropNames.ZOILineThickness, 1.ToString());
            DefaultTestProperties.Add(PropNames.ZOIShapeType, ZOIShapeType.Point.ToString());
            DefaultTestProperties.Add(PropNames.IconBorderShape, DepictionIconBorderShape.None.ToString());
            DefaultTestProperties.Add(PropNames.IconBorderColor, Brushes.Orange.ToString());
            DefaultTestProperties.Add(PropNames.IconResourceName, "Something");
            DefaultTestProperties.Add(PropNames.IconSize, ((double)DepictionIconSize.Medium).ToString());
            DefaultTestProperties.Add(PropNames.VisualState, ElementVisualSetting.Icon.ToString());
            DefaultTestProperties.Add(PropNames.DisplayName, TesterNameDisplay);//not really a property in the template
            DefaultTestProperties.Add(PropNames.Author, "why");
            DefaultTestProperties.Add(PropNames.Description, "some help here");
            DefaultTestProperties.Add(PropNames.IsMouseAddable, true.ToString());
            DefaultTestProperties.Add(PropNames.DefinitionSource, "Test");
            DefaultTestProperties.Add(PropNames.HideFromLibrary, true.ToString());
            DefaultTestProperties.Add(PropNames.ShowPropertiesOnCreate, true.ToString());
            DefaultTestProperties.Add(PropNames.IsDraggable,false.ToString());
            DefaultTestProperties.Add(PropNames.IsGeometryEditable, true.ToString());
        }

        public IElementTemplate CreateDefaultTemplate()
        {
            var template = new ElementTemplate(TesterName);
            template.DisplayName = TesterNameDisplay;
            foreach(var prop in DefaultTestProperties)
            {
                template.InfoProperties.Add(new DefaultTemplateProperty(prop.Key,prop.Value));
            }
            return template;
        }
    }
}