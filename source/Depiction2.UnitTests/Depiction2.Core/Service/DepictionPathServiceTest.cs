﻿using System.IO;
using Depiction2.API.Service;
using Depiction2.Core.Service;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.Service
{
    [TestFixture]
    public class DepictionPathServiceTest
    {

        [SetUp]
        public void SetUp()
        {
            
        }
        [TearDown]
        public void TearDown()
        {
            
        }
        [Test]
        public void DoesTempFolderGetCreated()
        {
            var pathService = new DepictionPathService("Test");
            var mainPath = pathService.AppDataDirectoryPath;
            var tempDir = pathService.TempFolderRootPath;
            var tempSubDir = pathService.CreateTempSubFolder();
            var tempSubSubDir = pathService.EnsurePath(Path.Combine(tempSubDir, "random"));
            // Example #2: Write one string to a text file. 
            string text = "Ayadday yadday ddyay d " +
                           "glhf ";
            // WriteAllText creates a file, writes the specified string to the file, 
            // and then closes the file.
            File.WriteAllText(Path.Combine(tempSubDir,"out.txt"), text);
            Assert.IsTrue(Directory.Exists(pathService.AppDataDirectoryPath));
            Assert.IsTrue(Directory.Exists(tempDir));
            Assert.IsTrue(Directory.Exists(tempSubSubDir));

            Assert.IsTrue(Directory.Exists(tempSubDir));

            pathService.CleanTempFolder();
            Assert.IsFalse(Directory.Exists(tempSubDir));
            Assert.IsFalse(Directory.Exists(tempSubSubDir));
            pathService.DoCleanup(pathService.AppDataDirectoryPath);
            Assert.IsFalse(Directory.Exists(mainPath));
        }
    }
}