﻿using System.Collections.Generic;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Service;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.Service
{
    [TestFixture]
    public class IndependentElemTemplateServiceTest
    {
        [Test]
        public void CreateElementFromDictionary()
        {
            var dictionary = new Dictionary<string, object>();
            dictionary.Add(PropertyKeys.autherKey,"meeee");

            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(dictionary);
            Assert.AreEqual(0,dictionary.Count);
            Assert.AreEqual(null,element.GeoLocation);
            Assert.AreEqual(ElementVisualSetting.Icon, element.VisualState);
            

        }
//        [Test]
//        public void UpdateElementFromDictionary()
//        {
//
//        }
//        [Test]
//        public void TransformStringToObjectDictionary()
//        {
//
//        }
    }
}