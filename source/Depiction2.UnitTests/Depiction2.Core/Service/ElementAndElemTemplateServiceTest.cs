﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.API.ValidationRules;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.UnitTests.Depiction2.Core.Service.TestDmls;
using Depiction2.UnitTests.Depiction2.Core.StoryEntities.Scaffold;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.Service
{
    [TestFixture]
    public class ElementAndElemTemplateServiceTest
    {
        private IElementTemplate _emptyTemplate;
        private TestHelpers _helpers = new TestHelpers();

        [SetUp]
        public void Setup()
        {
            _emptyTemplate = ElementTemplateTest.CreateEmptyTemplate();
            Assert.IsTrue(ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.UnitTests.dll"),"Could not load testing geometry extension");
            Assert.IsTrue(DepictionGeometryService.Activated, "Geometry service is not active.");
        }
        [TearDown]
        public void TearDown()
        {
            _emptyTemplate = null;
            ExtensionService.Instance.Decompose();
        }

        [Test]
        public void AreTemplatePropertiesPassedToElement()
        {
            var template = _helpers.CreateDefaultTemplate();
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(template, null);
            Assert.AreEqual(element.ElementType, TestHelpers.TesterName);
            Assert.AreEqual(element.DisplayName, TestHelpers.TesterNameDisplay);
            foreach(var prop in _helpers.DefaultTestProperties)
            {
                switch(prop.Key)
                {
                    case PropNames.DisplayName:
                        Assert.AreEqual(prop.Value, element.DisplayName);
                        break;
                    case PropNames.Author:
                        Assert.AreEqual(prop.Value, element.Author);
                        break;
                    case PropNames.Description:
                        Assert.AreEqual(prop.Value, element.Description);
                        break;
                    case PropNames.ZOIBorderColor:
                        Assert.AreEqual(prop.Value,element.ZOIBorderColor);
                        break;
                    case PropNames.ZOIFillColor:
                        Assert.AreEqual(prop.Value, element.ZOIFillColor);
                        break;
                    case PropNames.IconBorderShape:
                        Assert.AreEqual(prop.Value, element.IconBorderShape);
                        break;
                    case PropNames.IconBorderColor:
                        Assert.AreEqual(prop.Value, element.IconBorderColor);
                        break;
                    case PropNames.IconResourceName:
                        Assert.AreEqual(prop.Value, element.IconResourceName);
                        break;
                    case PropNames.ZOILineThickness:
                        Assert.AreEqual(double.Parse(prop.Value), element.ZOILineThickness);
                        break;
                    case PropNames.ZOIShapeType:
                        Assert.AreEqual(prop.Value, element.ZOIShapeType);
                        break;
                    case PropNames.IconSize:
                        Assert.AreEqual(prop.Value, element.IconSize.ToString());
                        break;
                    case PropNames.VisualState:
                        Assert.AreEqual(prop.Value, element.VisualState.ToString());
                        break;
                    case PropNames.IsGeometryEditable:
                        Assert.AreEqual(prop.Value, element.IsGeometryEditable.ToString());
                        break;

//                    case PropNames.IsMouseAddable:
//                        Assert.AreEqual(prop.Value, element.IsMouseAddable);
//                        break;
//                    case PropNames.DefinitionSource:
//                        Assert.AreEqual(prop.Value, element.DefinitionSource);
//                        break;
//                    case PropNames.HideFromLibrary:
//                        Assert.AreEqual(prop.Value, element.ZOIBorderColor);
//                        break;
//                    case PropNames.ShowPropertiesOnCreate:
//                        Assert.AreEqual(prop.Value, element.ZOIBorderColor);
//                        break;
                }
            }

        }
        [Test]
        public void ElementWithNoGeometryCreationTest()
        {
            var name = _emptyTemplate.DisplayName;
            var type = _emptyTemplate.ElementType;

            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(_emptyTemplate, null);
            Assert.AreEqual(name, element.DisplayName);
            Assert.AreEqual(type, element.ElementType);
            //            Assert.IsNotNull(elementGeom);
            Assert.IsNull(element.GeoLocation);//not relaly sure what should be expected
            Assert.IsNotNull(element.ElementGeometry);
//            var elementGeom = element.ElementGeometry;
            //            Assert.AreEqual(DepictionGeometryType.Unknown, elementGeom.GeometryType);
        }
        [Test]
        public void ElementWithPointGeometryCreationTest()
        {
            var singlePoint = new Point(-1, 1);
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(_emptyTemplate,
                                                                                   new List<Point> { singlePoint });
            Assert.AreEqual(ElementVisualSetting.Icon, element.VisualState);
            var elementGeom = element.ElementGeometry;
            Assert.IsNotNull(element.GeoLocation);
            Assert.IsTrue(element.IsDraggable);
            Assert.AreEqual(singlePoint, element.GeoLocation);
            Assert.AreEqual(DepictionGeometryType.Point, elementGeom.GeometryType);
        }
        [Test]
        public void ElementWithLineGeometryCreationTest()
        {
            var startPoint = new Point(0, 0);
            var lineList = new List<Point> { startPoint, new Point(3, 4), new Point(9, 5) };
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(_emptyTemplate, lineList);
            Assert.AreEqual(ElementVisualSetting.Geometry, element.VisualState);
            var elementGeom = element.ElementGeometry;
            Assert.IsNotNull(element.GeoLocation);
            Assert.IsTrue(element.IsDraggable);
            //                        Assert.AreEqual(startPoint, element.GeoLocation);//Ignore this for now
            Assert.AreEqual(DepictionGeometryType.LineString, elementGeom.GeometryType);
        }
        [Test]
        public void ElementWithPolygonGeometryCreationTest()
        {
            var polyList = new List<Point> { new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 0), new Point(0, 0) };
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(_emptyTemplate, polyList);
            Assert.AreEqual(ElementVisualSetting.Geometry, element.VisualState);
            var elementGeom = element.ElementGeometry;
            var pointCenter = new Point(.5, .5);
            Assert.IsNotNull(element.GeoLocation);
            Assert.IsTrue(element.IsDraggable);
            Assert.AreEqual(pointCenter, element.GeoLocation);
            Assert.AreEqual(DepictionGeometryType.Polygon, elementGeom.GeometryType);
        }
        [Test]
        public void ElementWithPointAndPostCreateGeometryCreationTest()
        {
            var singlePoint = new Point(0, 0);
            var scaffold = ElementTemplateTest.CreateTemplateWithActions();
            scaffold.OnCreateActions = new Dictionary<string, string[]> { { "someth", new[] { "toerh" } } };
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(scaffold, new List<Point> { singlePoint });
            Assert.AreEqual(ElementVisualSetting.Geometry | ElementVisualSetting.Icon, element.VisualState);
        }

        [Test]
        [Ignore("Must load a geometry extension first")]
        public void IsElementCreatedWithGeometryMovable()
        {
            var singlePoint = new Point(-1, 1);
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(_emptyTemplate, new List<Point> { singlePoint });

            Assert.AreEqual(singlePoint, element.GeoLocation);
            var newPosition = new Point(5, 4);
            element.UpdatePrimaryPointAndGeometry(newPosition, null);
            Assert.AreEqual(newPosition, element.GeoLocation);

            singlePoint = new Point(-4, 6);
            element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(_emptyTemplate, new List<Point> { singlePoint });
            Assert.AreEqual(singlePoint, element.GeoLocation);
            newPosition = new Point(10, 60);
            element.UpdatePrimaryGeoLocationAndShiftGeometry(newPosition);
            Assert.AreEqual(newPosition, element.GeoLocation);
        }

        [Test]
        public void ElementWithActionsValueSharingTest()
        {
            var scaffold = ElementTemplateTest.CreateTemplateWithActions();
            var postAddActionsCount = scaffold.OnCreateActions.Count;
            var postClickActionsCount = scaffold.OnClickActions.Count;

            var first = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(scaffold, null);
            var second = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(scaffold, null);
            Assert.AreEqual(postAddActionsCount, first.PostAddActions.Count);
            Assert.AreEqual(postClickActionsCount, first.OnClickActions.Count);
            first.OnClickActions.Clear();
            Assert.AreEqual(postClickActionsCount, scaffold.OnClickActions.Count, "Scaffold should always have click actions");
            Assert.AreEqual(0, first.OnClickActions.Count);
            Assert.AreEqual(postClickActionsCount, second.OnClickActions.Count);

            var firstKey = scaffold.OnClickActions.Keys.First();
            var valueList = scaffold.OnClickActions[firstKey];

            Assert.IsFalse(string.IsNullOrEmpty(valueList[0]));
            var clonedValues = second.OnClickActions[firstKey];
            clonedValues[0] = string.Empty;
            Assert.IsFalse(string.IsNullOrEmpty(valueList[0]), "Changing a location in the value changed the value for everybody");

            var otherFirstKey = scaffold.OnCreateActions.Keys.First();
            var otherValueList = scaffold.OnCreateActions[otherFirstKey];
            Assert.IsFalse(string.IsNullOrEmpty(otherValueList[0]));
            var otherCloneValues = second.PostAddActions[otherFirstKey];
            otherCloneValues[0] = string.Empty;
            Assert.IsFalse(string.IsNullOrEmpty(otherValueList[0]), "chaning values changes it for everybody");
        }

        [Test]
        public void ElementPropertyFromPropertyScaffoldTest()
        {
            var rule1 = new MinValueValidationRule();
            var rule2 = new DataTypeValidationRule();
            var rules = new IValidationRule[] { rule1, rule2 };

            var scaffoldProperty = new PropertyTemplate();
            scaffoldProperty.ProperName = "One";
            scaffoldProperty.TypeString = typeof(string).ToString();
            scaffoldProperty.LegacyValidationRules = rules;

            var realProperty = ElementAndElemTemplateService.CreatePropertyFromScaffold(scaffoldProperty);

            Assert.IsNotNull(scaffoldProperty.LegacyValidationRules[0]);
            realProperty.LegacyValidationRules[0] = null;
            Assert.IsNotNull(scaffoldProperty.LegacyValidationRules[0]);

        }

        [Test]
        public void ElementCreationFromScaffoldAndPropertyUpdateTest()
        {
            var baseProps = new List<IPropertyTemplate>();
            var modBaseProps = new List<IPropertyTemplate>();
            var modBaseValues = new List<object>();
            var baseValues = new List<object>();
            var userProps = new List<IPropertyTemplate>();
            #region property scaffold creation
            for (int i = 'A'; i < 'G'; i++)
            {
                var propVal = i;
                var modVal = i * 2;
                baseValues.Add(propVal);
                modBaseValues.Add(modVal);
                baseProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString(),
                    ValueString = propVal.ToString(),
                    TypeString = typeof(int).Name
                });
                modBaseProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString().ToLower(),
                    ValueString = modVal.ToString(),
                    TypeString = typeof(int).Name
                });

            }
            for (int i = 'g'; i < 'm'; i++)
            {
                var propVal = i.ToString();
                var modVal = propVal + "fizzle";
                baseValues.Add(propVal);

                modBaseValues.Add(modVal);
                baseProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString(),
                    ValueString = i.ToString(),
                    TypeString = typeof(string).Name
                });
                modBaseProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString().ToLower(),
                    ValueString = modVal,
                    TypeString = typeof(string).Name
                });
            }

            for (int i = 'M'; i < 'U'; i++)
            {
                userProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString(),
                    ValueString = i.ToString(),
                    TypeString = typeof(double).Name
                });
            }
            for (int i = 'U'; i < 'Z'; i++)
            {
                userProps.Add(new PropertyTemplate
                {
                    ProperName = i.ToString(),
                    ValueString = i.ToString(),
                    TypeString = typeof(string).Name
                });
            }
            #endregion
            var elementScaffold = new ElementTemplate("Generic");
            elementScaffold.Properties = baseProps;

            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(elementScaffold,
                                                                                     new[] { new Point(0, 0) });

            Assert.AreEqual(baseProps.Count, element.DefaultElementPropertyNames.Count, "Initial creation count doesn't match");

            Assert.AreEqual(baseProps.Count, baseValues.Count);
            for (var i = 0; i < baseProps.Count; i++)
            {
                var elemProp = element.GetDepictionProperty(baseProps[i].ProperName);
                Assert.IsNotNull(elemProp, "cant find the property");
                Assert.AreEqual(baseValues[i], elemProp.Value, string.Format("at {0}", i));
            }

            ElementAndElemTemplateService.UpdateElementWithProperties(element, modBaseProps, null);
            Assert.AreEqual(baseProps.Count, element.DefaultElementPropertyNames.Count, "Post update base count doens't match");

            Assert.AreEqual(baseProps.Count, baseValues.Count);
            for (var i = 0; i < modBaseProps.Count; i++)
            {
                var elemProp = element.GetDepictionProperty(modBaseProps[i].ProperName);
                Assert.IsNotNull(elemProp, "cant find the property");
                Assert.AreEqual(modBaseValues[i], elemProp.Value, string.Format("at {0}", i));
            }

            ElementAndElemTemplateService.UpdateElementWithProperties(element, userProps, null);
            Assert.AreEqual(baseProps.Count, element.DefaultElementPropertyNames.Count, "Initial creation count doesn't match");
            Assert.AreEqual(userProps.Count, element.AdditionalPropertyNames.Count, "Initial creation count doesn't match");


        }
    }
}