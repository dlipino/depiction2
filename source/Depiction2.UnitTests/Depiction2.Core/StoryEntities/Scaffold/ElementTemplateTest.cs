﻿using System.Collections.Generic;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.StoryEntities.Scaffold
{
    [TestFixture]
    public class ElementTemplateTest
    {
         static public ElementTemplate CreateEmptyTemplate()
         {
             var scaffold = new ElementTemplate("Generic") {DisplayName = "Empty element"};

             return scaffold;
         }
         static public ElementTemplate CreateTemplateWithActions()
         {
             var scaffold = CreateEmptyTemplate();
             scaffold.OnClickActions = new Dictionary<string, string[]>{{"ClickAction1",new[]{"first","second"}}};
             scaffold.OnCreateActions = new Dictionary<string, string[]> { { "CreateAction1", new[] { "third", "fourth" } } };
             return scaffold;
         }
    }
}