﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.StoryEntities.Scaffold
{
    [TestFixture]
    public class ElementTemplateLibraryTest
    {
        [Test]
        public void AreDuplicatesNoAddedToLibraryTest()
        {
            var lib = new ElementTemplateLibrary();
            var duplicateName = "First";
            var scaffolds = new List<IElementTemplate>
                               {
                                new ElementTemplate(duplicateName),
                                new ElementTemplate("Second"),
                                new ElementTemplate("Third")
                               };
            lib.AddElementTemplatesToDefault(scaffolds);

            Assert.AreEqual(3, lib.ProductElementTemplates.Count());
            Assert.AreEqual(0, lib.LoadedStoryElementTemplates.Count());
            var dupScaffold = new ElementTemplate(duplicateName);
            lib.AddStoryElementTemplates(new List<IElementTemplate>{dupScaffold});
            Assert.AreEqual(3, lib.ProductElementTemplates.Count());
            Assert.AreEqual(0, lib.LoadedStoryElementTemplates.Count());

        }
    }
}