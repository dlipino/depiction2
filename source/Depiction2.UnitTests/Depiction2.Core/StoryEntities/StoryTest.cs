﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.StoryEntities
{
    [TestFixture]
    public class StoryTest
    {
        [Test]
        public void AddRemoveElementTest()
        {
            using (var story = new Story(null))
            {
                int totalElements = 3;
                var elementIds = new List<string>();
                var addedElements = new List<IElement>();
                for (int i = 0; i < totalElements; i++)
                {
                    var elem = new DepictionElement("random");
                    elementIds.Add(elem.ElementId);
                    elem.SetGeoLocationFromString(string.Format("{0},{0}", i));
                    elem.SetGeometryFromWtkString(string.Empty);
                    elem.MatchLocationAndGeometry();
                    addedElements.Add(elem);
                    story.AddElement(elem);
                }

                Assert.AreEqual(3, story.AllElements.Count);

                story.RemoveElements(addedElements);

                Assert.AreEqual(0, story.AllElements.Count);
            }
        }


        [Test]
        [Ignore]
        public void LocationInfoTest()
        {
//            var dir = AppDomain.CurrentDomain.BaseDirectory;
//            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
//            var story = new Story();
//            var terrainElement = new DepictionElement();
//            var terrainProp = new ElementProperty("Terrain");
//            terrainProp.SetValue(new TerrainCoverage());
//            terrainElement.AddUserProperty(terrainProp);
//            story.AddElements(new List<IElement> { terrainElement });
//
//            var info = story.LocationInfoFromProjLocation(0, 0);
//
//            DepictionExtensionLibrary.Instance.Decompose();
        }
    }
}