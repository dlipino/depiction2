﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.StoryEntities
{
    [TestFixture]
    public class StoryTestsWithAddons
    {
        [SetUp]
        public void Setup()
        {
            Assert.IsTrue(ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.UnitTests.dll"), "Could not load testing geometry extension");
            Assert.IsTrue(DepictionGeometryService.Activated, "Geometry service is not active.");
        }
        [TearDown]
        public void TearDown()
        {
            ExtensionService.Instance.Decompose();
        }

        [Test]
        public void GetElementsInGeometryTest()
        {
            var story = new Story(null);
            var elem = new DepictionElement("Genenr");
            elem.UpdatePrimaryPointAndGeometry(new Point(1, 1), null);
            story.AddElement(elem);
            var elem1 = new DepictionElement("Second");
            elem1.UpdatePrimaryPointAndGeometry(new Point(3, 3), null);
            story.AddElement(elem1);

            var otherGeom =
                DepictionGeometryService.CreateGeometry(new List<Point>
                                         {
                                             new Point(2, 2),
                                             new Point(2, -2),
                                             new Point(-2, -2),
                                             new Point(-2, 2),
                                             new Point(2, 2)
                                         });
            var overlap = story.GetElementsInGeometry(otherGeom);
            Assert.IsNotNull(overlap);
            Assert.AreEqual(1, overlap.Count());
        }
    }
}