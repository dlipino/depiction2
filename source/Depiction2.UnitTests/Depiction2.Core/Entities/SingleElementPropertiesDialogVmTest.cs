﻿using System.ComponentModel;
using System.Linq;
using Depiction2.API.Measurement;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.Entities
{
    [TestFixture]
    public class SingleElementPropertiesDialogVmTest
    {
        [Test]
        public void DoClrPropertyChangesTriggerChange()
        {
            var initialName = "Start";
            var finalName = "End";
            var initialColor = "Black";
            var finalColor = "Orange";
            var initialDrag = true;
            var finalDrag = false;
            var element = new DepictionElement("blah");
            element.DisplayName = initialName;
            element.ZOIBorderColor = initialColor;
            element.IsDraggable = initialDrag;

            var elementVm = new EditElementViewModel(element);
            var disProp = elementVm.InformationProperties.FirstOrDefault(t => t.ProperName.Equals(PropNames.DisplayName));
            Assert.IsNotNull(disProp);
            Assert.AreEqual(false, disProp.UseAsHoverText);
            Assert.AreEqual(false, disProp.OrigIsHoverText);

            disProp.PropertyValue = finalName;
            elementVm.ApplyInformationChanges();
            Assert.AreEqual(finalName, element.DisplayName);

            disProp = elementVm.VisualProperties.FirstOrDefault(t => t.ProperName.Equals(PropNames.ZOIBorderColor));
            Assert.IsNotNull(disProp);
            disProp.PropertyValue = finalColor;
            elementVm.ApplyVisualPropertyChanges();

            Assert.AreEqual(finalColor, element.ZOIBorderColor);

            disProp = elementVm.MapControlProperties.FirstOrDefault(t => t.ProperName.Equals(PropNames.IsDraggable));
            Assert.IsNotNull(disProp);
            disProp.PropertyValue = finalDrag;
            elementVm.ApplyMapControlPropertyChanges();

            Assert.AreEqual(finalDrag, element.IsDraggable);

        }

        [Test]
        public void AreViewModelsConnected()
        {
            var initialColor = "Black";
            var finalColor = "Orange";

            var element = new DepictionElement("blah");
            element.ZOIFillColor = initialColor;

            var editVm = new EditElementViewModel(element);
            var mapVm = new MapElementViewModel(element);
            Assert.AreEqual(initialColor, element.ZOIFillColor);
            Assert.AreEqual(initialColor, mapVm.ZOIFill);
            bool triggered = false;
            mapVm.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
                        {
                            if (Equals("ZOIFill", e.PropertyName))
                            {
                                triggered = true;
                            }
                        };
            var disProp = editVm.VisualProperties.FirstOrDefault(t => t.ProperName.Equals(PropNames.ZOIFillColor));
            Assert.IsNotNull(disProp);
            Assert.AreEqual(initialColor, disProp.PropertyValue);

            disProp.PropertyValue = finalColor;
            editVm.ApplyVisualPropertyChanges();

            Assert.AreEqual(finalColor, element.ZOIFillColor);
            Assert.AreEqual(finalColor, mapVm.ZOIFill);
            Assert.IsTrue(triggered);
        }
        [Test]
        [Ignore("Not ready to test measurement stuff yet")]
        public void UserPropertyChangeTest()
        {
            var name = "size";
            var value = new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 30);
            var element = new DepictionElement("blah");
            element.AddElementDefaultProperty(new ElementProperty(name,name,value));
            bool triggered = false;
            element.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            {
                if (Equals("ZOIFill", e.PropertyName))
                {
                    triggered = true;
                }
            };
            var editVm = new EditElementViewModel(element);
            var disProp = editVm.DefaultProperties.FirstOrDefault(t => t.ProperName.Equals(name));
            Assert.IsNotNull(disProp);
            Assert.AreEqual(value,disProp.PropertyValue);


        }

        [Test]
        [Ignore]
        public void DoPropertiesFromLoadedElementsTriggerPropertyChangeEvents()
        {
            //            DepictionAccess.NameTypeService = new DepictionSimpleTypeService();
            //
            //            var dir = AppDomain.CurrentDomain.BaseDirectory;
            //            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
            //            var storyLoader = new Depiction14StoryLoader();
            //            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("DepictionThreeShapeElements.dpn");
            //            var story = storyLoader.GetStoryFromFile(fullName);
            //            Assert.IsNotNull(story);
            //            var elements = story.AllElements;
            //            Assert.IsNotNull(elements);
            //            Assert.AreEqual(3,elements.Count);
            //
            //
            //            var circleElement = elements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.CircleShape"));
            //
            //            Assert.IsNotNull(circleElement);
            //            Assert.AreEqual(4, circleElement.DefaultElementPropertyNames.Count);
            //            Assert.AreEqual(3, circleElement.AdditionalPropertyNames.Count);
            //            var radiusProp = circleElement.GetProperty("Radius");
            //            Assert.IsNotNull(radiusProp);
            //            var propTriggered = false;
            //            var origArea = circleElement.ElementGeometry.Bounds;
            //            Assert.AreNotEqual(Rect.Empty, origArea);
            //            circleElement.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
            //            {
            //                if (Equals("Radius", e.PropertyName))
            //                {
            //                    propTriggered = true;
            //                }
            //            };
            //            var dialogVm = new SingleElementPropertiesDialogVm(circleElement);
            //            dialogVm.ActiveTab = ElementPropertiesDialogTab.DefaultProperties;
            //
            //            Assert.AreEqual(3, dialogVm.UserProperties.Count);
            //            Assert.AreEqual(4, dialogVm.DefaultProperties.Count);
            //
            //            var radPropVm = dialogVm.DefaultProperties.Find(t => t.PropertyName.Equals("Radius"));
            //            Assert.IsNotNull(radPropVm);
            //            radPropVm.PropertyValue = new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 12);
            //
            //            dialogVm.ApplyChangesCommand.Execute(null);
            //
            //            Assert.IsTrue(propTriggered);
            //            var newArea = circleElement.ElementGeometry.Bounds;
            //            Assert.AreNotEqual(Rect.Empty, newArea);
            //            Assert.AreNotEqual(origArea, newArea);
            //
            //            DepictionExtensionLibrary.Instance.Decompose();
            //            DepictionAccess.NameTypeService = null;
        }
    }
}