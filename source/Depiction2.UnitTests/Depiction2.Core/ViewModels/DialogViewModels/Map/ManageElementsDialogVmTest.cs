﻿using System;
using System.Collections.Generic;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.DialogViewModels.Map;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.ViewModels.DialogViewModels.Map
{
    [TestFixture]
    public class ManageElementsDialogVmTest
    {
        private Story storyToTestOn;
        private ManageElementsDialogVM dialogVm;
        private List<string> elementIds;

        [SetUp]
        public void SetupForDialogVmTest()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
            storyToTestOn = new Story();
            AddElementsToStory();
            dialogVm = new ManageElementsDialogVM(storyToTestOn);
        }
        [TearDown]
        public void TearDown()
        {
            DepictionExtensionLibrary.Instance.Decompose();
        }

        protected void AddElementsToStory()
        {
            int totalElements = 3;
            elementIds = new List<string>();
            for (int i = 0; i < totalElements; i++)
            {
                var elem = new DepictionElement("random");
                elementIds.Add(elem.ElementId);
                elem.SetGeoLocationFromString(string.Format("{0},{0}", i));
                elem.SetGeometryFromWtkString(string.Empty);
                elem.MatchLocationAndGeometry();
                storyToTestOn.AddElement(elem);
            }

            Assert.AreEqual(3, storyToTestOn.AllElements.Count);
        }

        [Test]
        public void DeleteElementsCommandTest()
        {
            var geoLocatedList = dialogVm.GeolocatedElements;
            Assert.AreEqual(3, dialogVm.GeoLocatedElementCount);
            dialogVm.GeolocatedElements.SelectElementsById(elementIds, true);
            Assert.AreEqual(3, geoLocatedList.SelectedCount);
            var selectedModels = geoLocatedList.SelectedElementModels;
            Assert.AreEqual(3, geoLocatedList.SelectedElementModels.Count);
            dialogVm.DeleteSelectedElementsCommand.Execute(geoLocatedList);
            Assert.AreEqual(0, storyToTestOn.AllElements.Count);
            Assert.AreEqual(0, dialogVm.GeoLocatedElementCount);

        }
    }
}