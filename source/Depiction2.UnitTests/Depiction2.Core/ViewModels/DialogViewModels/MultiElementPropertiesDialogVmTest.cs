﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.DialogViewModels;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.ViewModels.DialogViewModels
{
    [TestFixture]
    public class MultiElementPropertiesDialogVmTest
    {
        //TODO add test to ensure that deepcloning is occuring properly

        #region element creat helpers
        private List<string> userPropsFull = new List<string> { "a", "B", "C", "d", "e" };
        private List<object> userPropsVal = new List<object> { 1, "trust", true, 2d, 34.3 };
        private List<object> userPropsOtherVal = new List<object> { 3, "else", false, -2d, 98.9 };
        private List<string> userPropsMissing = new List<string> { "b", "C", "E" };
        private List<object> userPropsMissingVal = new List<object> { "far out", false, -8.9 };
        private List<string> extraPropsFull = new List<string> { "1", "2", "3", "4", "5" };
        private List<object> extraPropsVal = new List<object> { false, "trust", 13, 54, 34.3 };
        private List<string> extraPropsMissing = new List<string> { "4", "6", "3" };


        #endregion
        #region construction helpers
        private List<IElement> ConstructElementListWithBaseProperties()
        {
            var elements = new List<IElement>();
            //create the base value
            var first = new DepictionElement("Generic");
            for (int i = 0; i < userPropsFull.Count; i++)
            {
                var prop = new ElementProperty(userPropsFull[i]);
                prop.SetValue(userPropsVal[i]);
                first.AddElementDefaultProperty(prop);
            }
//            for (int i = 0; i < extraPropsFull.Count; i++)
//            {
//                var prop = new TempElementProperty(extraPropsFull[i]);
//                prop.SetValue(extraPropsVal[i]);
//                first.AddUserProperty(prop);
//            }
            elements.Add(first);

            var second = new DepictionElement("Generic");
            for (int i = 0; i < userPropsFull.Count; i++)
            {
                var prop = new ElementProperty(userPropsFull[i]);
                prop.SetValue(userPropsOtherVal[i]);
                second.AddElementDefaultProperty(prop);
            }
            elements.Add(second);
            var third = new DepictionElement("Generic");
            for (int i = 0; i < userPropsMissing.Count; i++)
            {
                var prop = new ElementProperty(userPropsMissing[i]);
                prop.SetValue(userPropsMissingVal[i]);
                third.AddElementDefaultProperty(prop);
            }
            elements.Add(third);
            return elements;
        }
        #endregion
        [Test]
        public void MultElementDialogViewModelContructorTest()
        {
            var basePropCount = 5;
            var elemList = ConstructElementListWithBaseProperties();
            var dialogVm = new MultipleElementPropertiesDialogViewModel(elemList);
            var allProps = dialogVm.AllCommonProperties;
            Assert.AreEqual(basePropCount, allProps.Count);
            var baseProps = dialogVm.BaseCommonProperties;
            Assert.AreEqual(basePropCount, baseProps.Count);
            var extraProps = dialogVm.ExtraCommonProperties;
            Assert.AreEqual(0, extraProps.Count);
        }
        [Test]
        public void SimpleChangeTest()
        {
            var elemList = ConstructElementListWithBaseProperties();
            var dialogVm = new MultipleElementPropertiesDialogViewModel(elemList);
            dialogVm.ActiveTab = MultiElementPropertiesDialogTab.Properties;
            var allProps = dialogVm.AllCommonProperties;
            var last = allProps.Last();
            var lastName = last.ProperName;
            Assert.AreEqual(typeof(double),last.PropertyType);
            last.PropertyValue = 43.2;
            dialogVm.ApplyChangesCommand.Execute(null);
            foreach(var elem in elemList)
            {
                var val = elem.GetDepictionProperty(lastName).Value;
                Assert.AreEqual(43.2,val);
            }
            last.PropertyValue = 7d;
            dialogVm.ApplyChangesCommand.Execute(null);
            foreach (var elem in elemList)
            {
                var val = elem.GetDepictionProperty(lastName).Value;
                Assert.AreEqual(7d, val);
            }

            var secondProp = allProps.FirstOrDefault(t=>t.ProperName.Equals("C"));
            Assert.IsNotNull(secondProp);
            Assert.IsTrue(secondProp.PropertyType == typeof(bool));
            secondProp.PropertyValue = false;
            var secondName = secondProp.ProperName;
            dialogVm.ApplyChangesCommand.Execute(null);
            foreach (var elem in elemList)
            {
                var val = elem.GetDepictionProperty(secondName).Value;
                Assert.AreEqual(false, val);
            }

            secondProp.PropertyValue = true; 
            dialogVm.ApplyChangesCommand.Execute(null);
            foreach (var elem in elemList)
            {
                var val = elem.GetDepictionProperty(secondName).Value;
                Assert.AreEqual(true, val);
            }
        }
        [Test]
        [Ignore]
        public void DoPropertiesFromLoadedElementsTriggerPropertyChangeEvents()
        {
//            DepictionAccess.NameTypeService = new DepictionSimpleTypeService();
//
//            var dir = AppDomain.CurrentDomain.BaseDirectory;
//            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
//            var storyLoader = new Depiction14StoryLoader();
//            var fullName = Legacy14ElementLoaderTest.GetFullFileNameAndUpdateTypes("DepictionThreeShapeElements.dpn");
//            var story = storyLoader.GetStoryFromFile(fullName);
//            Assert.IsNotNull(story);
//            var elements = story.AllElements;
//            Assert.IsNotNull(elements);
//            Assert.AreEqual(3, elements.Count);
//
//
//            var circleElement = elements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.CircleShape"));
//
//            Assert.IsNotNull(circleElement);
//            Assert.AreEqual(4, circleElement.DefaultElementPropertyNames.Count);
//            Assert.AreEqual(3, circleElement.AdditionalPropertyNames.Count);
//            var radiusProp = circleElement.GetProperty("Radius");
//            Assert.IsNotNull(radiusProp);
//            var propTriggered = false;
//            var origArea = circleElement.ElementGeometry.Bounds;
//            Assert.AreNotEqual(Rect.Empty, origArea);
//            circleElement.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
//            {
//                if (Equals("Radius", e.PropertyName))
//                {
//                    propTriggered = true;
//                }
//            };
//            var dialogVm = new SingleElementPropertiesDialogVm(circleElement);
//            dialogVm.ActiveTab = ElementPropertiesDialogTab.DefaultProperties;
//
//            Assert.AreEqual(3, dialogVm.UserProperties.Count);
//            Assert.AreEqual(4, dialogVm.DefaultProperties.Count);
//
//            var radPropVm = dialogVm.DefaultProperties.Find(t => t.PropertyName.Equals("Radius"));
//            Assert.IsNotNull(radPropVm);
//            radPropVm.PropertyValue = new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 12);
//
//            dialogVm.ApplyChangesCommand.Execute(null);
//
//            Assert.IsTrue(propTriggered);
//            var newArea = circleElement.ElementGeometry.Bounds;
//            Assert.AreNotEqual(Rect.Empty, newArea);
//            Assert.AreNotEqual(origArea, newArea);
//
//            DepictionExtensionLibrary.Instance.Decompose();
//            DepictionAccess.NameTypeService = null;
        }
    }
}