﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Collections;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.UnitTests.TestUtilities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.Core.ViewModels.Helpers
{
    [TestFixture]
    public class QuadSpatialSourceTests
    {
        [Test]
        [Ignore("Requires a function geometry extension")]
        public void QuadSourceAndReadonlyQuadSourceConnectionTest()
        {
            var elementVms = new ObservableCollection<MapElementViewModel>();
            ReadOnlyObservableSpatialItemSource<MapElementViewModel> readOnlyElementVms;

            var elems = new List<IElement> { new DepictionElement("Car"), new DepictionElement("Bus") };
           
            foreach(var item in elems)
            {
                elementVms.Add(new MapElementViewModel(item));
            }
            readOnlyElementVms = new ReadOnlyObservableSpatialItemSource<MapElementViewModel>(elementVms);

            Assert.AreEqual(elementVms.Count, readOnlyElementVms.Count);

            elementVms.RemoveAt(0);

            Assert.AreEqual(elementVms.Count, readOnlyElementVms.Count);
            elementVms.Clear();
            Assert.AreEqual(0,elementVms.Count);
            Assert.AreEqual(elementVms.Count, readOnlyElementVms.Count);
            foreach (var item in elems)
            {
                elementVms.Add(new MapElementViewModel(item));
            }
            var single = new DepictionElement("Image");
            var singleVm = new MapElementViewModel(single);
            elementVms.Add(singleVm);
            Assert.AreEqual(elementVms.Count, readOnlyElementVms.Count);

            elementVms.Remove(singleVm);

            Assert.AreEqual(elementVms.Count, readOnlyElementVms.Count);
        }
        [Test]
        public void ReadOnlyCollectionMatchingTest()
        {
            var quadElements = new ModQuadTree<IElement>();
            var allElements = new List<IElement>();
            var elements = new List<IElement> {new DepictionElement("Web")};
            allElements.AddRange(elements);

//            var readonlies = new ReadOnlyCollection<IElement>(quadElements.ToList());
            var readonlyOrig = new ReadOnlyCollection<IElement>(allElements);
            Assert.AreEqual(1, readonlyOrig.Count);
            Assert.AreEqual(1, readonlyOrig.Count);

            var more = new List<IElement> { new DepictionElement("Add") };
            allElements.AddRange(more);
            Assert.AreEqual(2, readonlyOrig.Count);
            Assert.AreEqual(2, allElements.Count);
        }
    
        [Test]
        public void ModQuadTreeExtentTest()
        {
            var quadTree = new ModQuadTree<IElement>();
            var elem = new DepictionElement("Genenr");
            elem.UpdatePrimaryPointAndGeometry(new Point(1, 1), new GeometryTester(new Point(1, 1)));
            quadTree.AddElements(new List<IElement> {elem});
            var extent = quadTree.Extent;

            var elem1 = new DepictionElement("Second");
            elem1.UpdatePrimaryPointAndGeometry(new Point(3, 3), new GeometryTester(new Point(3,3)));
            quadTree.AddElements(new List<IElement> { elem1 });
            extent = quadTree.Extent;
        }
    }

}