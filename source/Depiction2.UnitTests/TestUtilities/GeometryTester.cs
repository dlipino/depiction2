﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities;

namespace Depiction2.UnitTests.TestUtilities
{
    public class GeometryTester : IDepictionGeometry
    {
        private List<Point> pointList = new List<Point>();
        private Rect bounds = Rect.Empty;
        private ICartRect rectBounds = null;
        public object GeometryObject { get { return null; } }
        public IEnumerable<Point> GeometryPoints
        {
            get { return pointList; }
        }
        public DepictionGeometryType GeometryType
        {
            get
            {
                var type = DepictionGeometryType.Unknown;
                if (!pointList.Any()) return type;
                if (pointList.Count == 1)
                {
                    return DepictionGeometryType.Point;
                }
                if (pointList.Count == 2)
                {
                    return DepictionGeometryType.LineString;
                }
                if (pointList.Count >= 3)
                {
                    if(!pointList.First().Equals(pointList.Last())) return DepictionGeometryType.LineString;
                    return DepictionGeometryType.Polygon;
                }
                return type;
            }
        }

        public string WktGeometry
        {
            get { return string.Empty; }
        }

        public Rect Bounds
        {
            get
            {
                UpdateBounds();//Hackish
                return bounds;
            }
        }
        public ICartRect RectBounds
        {
            get
            {
                UpdateBounds();//Hackish
                return rectBounds;
            }
        }
        public bool IsSimple
        {
            get { return true; }
        }
        public bool IsValid
        {
            get { return pointList.Any(); }
        }

        #region constructor/destructor
        public GeometryTester()
        {
        }
        public GeometryTester(Point singlePoint)
        {
            pointList.Add(singlePoint);
            UpdateBounds();
        }

        public GeometryTester(IEnumerable<Point> geometryPoints)
        {
            pointList = new List<Point>(geometryPoints);
            UpdateBounds();
        }

        public void Dispose()
        {

        }

        #endregion

        private void UpdateBounds()
        {
            var minx = double.MaxValue;
            var maxx = double.MinValue;
            var miny = double.MaxValue;
            var maxy = double.MinValue;

            foreach (var point in pointList)
            {
                var x = point.X;
                var y = point.Y;
                if (y < miny) miny = y;
                if (y > maxy) maxy = y;
                if (x < minx) minx = x;
                if (x > maxx) maxx = x;
            }
            if(minx.Equals(double.MaxValue))
            {
                bounds = Rect.Empty;
                rectBounds = null;
            }else
            {
                bounds = new Rect(new Point(minx, miny), new Point(maxx, maxy));
                rectBounds = new CartRect(new Point(minx, maxy), new Point(maxx, miny));
            }
            
        }

        public void SetGeometry(List<Point> geometryPoints)
        {
            pointList=new List<Point>(geometryPoints);
        }
        
        public bool Intersects(IDepictionGeometry otherGeometry)
        {
            throw new NotImplementedException();
        }

        public bool Within(IDepictionGeometry otherGeometry)
        {
            return otherGeometry.Bounds.Contains(bounds);
        }

        public bool Contains(IDepictionGeometry otherGeometry)
        {
            return bounds.Contains(otherGeometry.Bounds);
        }

        public void ShiftGeometry(Point shiftDistance)
        {
            var shifted = new List<Point>();
            foreach(var p in pointList)
            {
                p.Offset(shiftDistance.X,shiftDistance.Y);
            }
        }

        //        protected void ShiftGeometry(OSGeo.OGR.Geometry geom, Point shiftDistance)
        //        {
        //            if (geom == null) return;
        //            var pointCount = geom.GetPointCount();
        //            var geomCount = geom.GetGeometryCount();
        //
        //            var geomType = geom.GetGeometryType();
        //            if (pointCount != 0)
        //            {
        //
        //                for (int j = 0; j < pointCount; j++)
        //                {
        //                    var x = geom.GetX(j);
        //                    var y = geom.GetY(j);
        //                    geom.SetPoint_2D(j, x - shiftDistance.X, y - shiftDistance.Y);
        //                }
        //            }
        //            if (geomCount != 0)
        //            {
        //                for (int i = 0; i < geomCount; i++)
        //                {
        //                    ShiftGeometry(geom.GetGeometryRef(i), shiftDistance);
        //                }
        //            }
        //        }

        public IDepictionGeometry Clone()
        {
            return new GeometryTester(pointList);
        }

    }
}