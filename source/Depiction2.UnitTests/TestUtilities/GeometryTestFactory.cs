﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Depiction2.API.Extension.Tools.Geometry;
using Depiction2.Base.Geo;

namespace Depiction2.UnitTests.TestUtilities
{
    [GeometryExtensionMetadata(DisplayName = "Geometries according to test", Name = "GeometryTester", Author = "Depiction Inc.", ExtensionPackage = "Test")]
    public class GeometryTestFactory : IGeometryExtension
    {
        public void Dispose()
        {
        }

        public IDepictionGeometry CreateGeometry(object geometrySource)
        {
            if (geometrySource == null) return new GeometryTester();
            if (geometrySource is Point)
            {
                return new GeometryTester((Point)geometrySource);
            }
            if (geometrySource is IEnumerable<Point>)
            {
                return new GeometryTester((IEnumerable<Point>)geometrySource);
            }
            return null;
        }

        public StreamGeometry CreateStreamGeometryFromDepictionGeometry(IDepictionGeometry geometry)
        {
            return null;
        }

    }
}