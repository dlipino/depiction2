﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Geo;
using NUnit.Framework;

namespace Depiction2.UnitTests.TestUtilities
{
    [TestFixture]
    public class TestUtilitiesTests
    {
        GeometryTester empty = new GeometryTester();
        GeometryTester empty1 = new GeometryTester();

        GeometryTester mainRect = new GeometryTester(new List<Point> { new Point(2, 2), new Point(2, -2), new Point(-2, -2), new Point(-2, 2), new Point(2, 2) });

        GeometryTester internalPoint = new GeometryTester(new Point(1, 1));
        GeometryTester externalPoint = new GeometryTester(new Point(3, 3));
        GeometryTester intersectingPoint = new GeometryTester(new Point(2, 2));

        GeometryTester intersectingRect = new GeometryTester(new List<Point> { new Point(3, 3), new Point(3, -1), new Point(-1, -1), new Point(-1, 3), new Point(3, 3) });
        GeometryTester internalRect = new GeometryTester(new List<Point> { new Point(1, 1), new Point(1, -1), new Point(-1, -1), new Point(-1, 1), new Point(1, 1) });
        GeometryTester externalRect = new GeometryTester(new List<Point> { new Point(4, 4), new Point(4, 3), new Point(3, 3), new Point(3, 4), new Point(4, 4) });

        GeometryTester intersectLine = new GeometryTester(new List<Point> { new Point(1, 3), new Point(1, -3) });
        GeometryTester nonIntersectLine = new GeometryTester(new List<Point> { new Point(4, 3), new Point(4, -3) });
        GeometryTester withinLine = new GeometryTester(new List<Point> { new Point(1, 0), new Point(-1, 0) });

        [Test]
        public void GeometryTypeVerificationTest()
        {
            Assert.AreEqual(DepictionGeometryType.Point, internalPoint.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Point, externalPoint.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Point, intersectingPoint.GeometryType);


            Assert.AreEqual(DepictionGeometryType.Polygon, mainRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, intersectingRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, internalRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, externalRect.GeometryType);

            Assert.AreEqual(DepictionGeometryType.LineString, intersectLine.GeometryType);
            Assert.AreEqual(DepictionGeometryType.LineString, nonIntersectLine.GeometryType);
        }
        [Test]
        [Ignore("Not implemented for the basic geometry class")]
        public void IntersectionTest()
        {
            //ensure all intesection type give a non exception
            //first test double nulls.
            Assert.IsFalse(empty.Intersects(empty1));
            Assert.IsFalse(empty1.Intersects(empty));

            Assert.IsTrue(mainRect.Intersects(intersectLine));
            Assert.IsTrue(intersectLine.Intersects(mainRect));
            Assert.IsFalse(mainRect.Intersects(nonIntersectLine));

            Assert.IsTrue(mainRect.Intersects(internalPoint));
            Assert.IsTrue(mainRect.Intersects(intersectingPoint));
            Assert.IsFalse(mainRect.Intersects(externalPoint));
            Assert.IsTrue(mainRect.Intersects(intersectLine));

            Assert.IsTrue(intersectLine.Intersects(internalPoint));
        }
        [Test]
        [Ignore("The basic geometry class does not do a within operations")]
        public void WithinTest()
        {
            Assert.False(mainRect.Within(withinLine));
            Assert.True(withinLine.Within(mainRect));

            Assert.False(intersectLine.Within(mainRect));
            Assert.IsTrue(internalPoint.Within(mainRect));
            Assert.IsFalse(externalPoint.Within(mainRect));
            Assert.False(intersectingPoint.Within(mainRect));

            Assert.True(intersectLine.Within(intersectLine));//sanity test
            Assert.IsFalse(nonIntersectLine.Within(mainRect));
            Assert.False(internalPoint.Within(withinLine));
        }
        [Test]
        public void ContainsTest()
        {
            Assert.True(mainRect.Contains(withinLine));
            Assert.False(withinLine.Contains(mainRect));

            Assert.False(intersectLine.Contains(mainRect));
            Assert.False(mainRect.Contains(intersectLine));//really?
            Assert.IsTrue(mainRect.Contains(internalPoint));
            Assert.False(internalPoint.Contains(withinLine));
            Assert.IsFalse(mainRect.Contains(externalPoint));
            Assert.True(mainRect.Contains(intersectingPoint));//This is different from the gdal geometry

            Assert.True(intersectLine.Contains(intersectLine));//sanity test
            Assert.IsFalse(nonIntersectLine.Contains(mainRect));
        }
    }
}