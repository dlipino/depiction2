﻿using System.Globalization;
using System.Threading;
using Depiction2.Base.Utilities;
using Depiction2.Utilities;
using NUnit.Framework;

namespace Depiction2.UnitTests.UtilitiesTest
{
    [TestFixture]
    public class LatLongParserUtilityTest
    {
        private double latR;
        private double lonR;
        private string parseMessage;
        CultureInfo euro = CultureInfo.CreateSpecificCulture("fr-BE");//"fr-FR");
        CultureInfo us = CultureInfo.CreateSpecificCulture("en-US");
        private CultureInfo currentCulture;

        [SetUp]
        public void Setup()
        {
            currentCulture = CultureInfo.CurrentCulture;
            CultureInfo culture;
//            if (Thread.CurrentThread.CurrentCulture.Name == "fr-FR")
//                culture = CultureInfo.CreateSpecificCulture("en-US");
//            else
//                culture = CultureInfo.CreateSpecificCulture("fr-FR");

           
        }
        [TearDown]
        public void TeadDown()
        {
            Thread.CurrentThread.CurrentCulture = currentCulture;
            Thread.CurrentThread.CurrentUICulture = currentCulture;
        }

        [Test]
        public void ParseLatLongStringMultCultureTest()
        {

            Thread.CurrentThread.CurrentCulture = us;
            var lat = 32.5;
            var lon = -56.93;
            var latlongString = string.Format("{0},{1}", lat, lon);
            
            LatLongParserUtility.ParseForLatLong(latlongString, out latR, out lonR, out parseMessage);
            Assert.AreEqual(lat,latR);
            Assert.AreEqual(lon,lonR);

            Thread.CurrentThread.CurrentCulture = euro;
            latlongString = string.Format("{0},{1}", lat, lon);

            LatLongParserUtility.ParseForLatLong(latlongString, out latR, out lonR, out parseMessage);
            Assert.AreEqual(lat, latR);
            Assert.AreEqual(lon, lonR);
        }
    }
}