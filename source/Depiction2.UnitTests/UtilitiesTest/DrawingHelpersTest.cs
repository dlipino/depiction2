﻿

using System.Windows;
using Depiction2.Base.Utilities.Drawing;
using NUnit.Framework;

namespace Depiction2.UnitTests.UtilitiesTest
{
    [TestFixture]
    public class DrawingHelpersTest
    {
        [Test]
        public void FindEnclosingRectForPointsTest()
        {
            var points = new[] {new Point(-1, 0), new Point(0, -1), new Point(1, 0), new Point(0, 1)};
            var rect = DrawingHelpers.FindEnclosingRectForPoints(points);
            //for cartesiaaan
            var expectedRect = new Rect(new Point(-1, 1), new Point(1, -1));
            Assert.AreEqual(expectedRect,rect);
        }
    }
}