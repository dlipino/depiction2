﻿using System.IO;
using Depiction2.API.Service;
using NUnit.Framework;

namespace Depiction2.UnitTests.UtilitiesTest
{
    [TestFixture]
    public class TempFolderServiceLegacyTest
    {
        [Test]
        public void DoesCloseDeleteTempDir()
        {
            var folderServce = new DepictionFolderService(true);
            var folderName = folderServce.FolderName;
            Assert.IsTrue(Directory.Exists(folderName));
            folderServce.Close();
            Assert.IsFalse(Directory.Exists(folderName));
        }
    }
}