﻿using System;
using System.IO;
using System.Reflection;
using Depiction2.API.Service;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.API.Service
{
    [TestFixture]
    public class ExtensionServiceTest
    {
        [Test]
        [Ignore("Manual test")]
        public void RandomExtensionLoadTest()
        {
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsFalse(string.IsNullOrEmpty(assemblyFolder));
            var home = Path.Combine(assemblyFolder, "Depiction2.API", "Service");
//            AppDomain.CurrentDomain.BaseDirectory
            var firstExt = Path.Combine(home,"First");
            var secondExt = Path.Combine(home, "Second");
            Assert.IsTrue(Directory.Exists(firstExt));
            Assert.IsTrue(Directory.Exists(secondExt));

            var extService = new ExtensionService();
            extService.LoadExtensionFromDir(firstExt);
            Assert.IsNotNull(extService.DepictionStoryFileReaders);
            Assert.AreEqual(1,extService.DepictionStoryFileReaders.Count);
            Assert.AreEqual(0, extService.GeometryTools.Count);

            extService.LoadExtensionFromDir(secondExt);
            Assert.AreEqual(1, extService.GeometryTools.Count);
        }
    }
}