﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Depiction2.API.Measurement;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.API.Measurment
{
    [TestFixture]
    public class GeneralConverterConnectionTest
    {
        [Test]
        public void AreMeasurementsCorrectlyAttachedToTypeConverters()
        {
            var depictionUnitTypes = new List<Type>
                                         {
                                             typeof (Speed),
                                             typeof (Distance),
                                             typeof (Weight),
                                             typeof (Area),
                                             typeof (Volume),
                                             typeof (Temperature),
                                             typeof (Angle)
                                         };
            TypeConverter converter = null;
            foreach (var type in depictionUnitTypes)
            {
                converter = TypeDescriptor.GetConverter(type);
                Assert.IsTrue(converter.CanConvertFrom(typeof(string)), string.Format("Cant convert from {0}", type.Name));
            }

        }
        //        var speedString = "1 mph";
        //        var distanceString = "3.4 feet";
        //        var weightString = "45 lbs";
        //        var areaString = "5 square km";
        //        var volumeString = "13.5 liters";
        //        var temperatureString = "33 celsius";
        [Test]
        public void CultureStringTestStuff()
        {
            var expected = 4367.5;
            var speedStringUS = "4,367.5 mph";
            var speedStringEuro = "4.367,5 mph";
            var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            var euro = CultureInfo.CreateSpecificCulture("fr-BE");//"uk");
            var us = CultureInfo.CreateSpecificCulture("en");

            var converter = TypeDescriptor.GetConverter(typeof(Speed));
            var speedUs = (Speed)converter.ConvertFromString(null,us, speedStringUS);
            Assert.IsNotNull(speedUs);
            var usValue = speedUs.InitialNumericValue;
            var speedEuro = (Speed)converter.ConvertFromString(null, euro, speedStringEuro);
            Assert.IsNotNull(speedEuro);
            var euroValue = speedEuro.InitialNumericValue;

            Assert.AreEqual(expected, usValue, "US value did not match");
            Assert.AreEqual(expected, euroValue, "Euro value did not match");
        }
    }
}