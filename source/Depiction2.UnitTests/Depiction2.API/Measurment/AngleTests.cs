﻿using Depiction2.API.Measurement;
using Depiction2.Utilities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.API.Measurment
{
    [TestFixture]
    public class AngleTests
    {
        [Test]
        public void DoesAngleDeepCloneWork()
        {
            var angle1 = new Angle();
            angle1.Value = 1;

            var angle2 = angle1.DeepClone();
            angle2.Value = 3;
            Assert.AreEqual(1,angle1.Value);
        }
        [Test]
        public void NumberTypeConvesion()
        {
            int integer = 1;
            double doub = 2.43d;

            var converteredInt = RandomUtilities.CheapNumberTypeConverter(doub, typeof(int));
            Assert.AreEqual(typeof(int), converteredInt.GetType());
            Assert.AreEqual(2, converteredInt);
            Assert.IsNotNull(converteredInt);

            var converteredDouble = RandomUtilities.CheapNumberTypeConverter(integer, typeof(double));
            Assert.IsNotNull(converteredDouble);
            Assert.AreEqual(typeof(double), converteredDouble.GetType());
            Assert.AreEqual(1d, converteredDouble);
        }

    }
}