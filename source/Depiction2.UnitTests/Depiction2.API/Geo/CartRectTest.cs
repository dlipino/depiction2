﻿using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.API.Geo
{
    [TestFixture]
    public class CartRectTest
    {
        private Rect panelRect;
        private CartRect cartRect;

        [Test]
        public void NullConstructionTest()
        {
            var cart = new CartRect(null);
            Assert.IsTrue(cart.IsEmpty);
        }

        [Test]
        public void CopyConstructionTest()
        {
            var tl = new Point(-1, 10);
            var br = new Point(5, -2);
            cartRect = new CartRect(tl, br);
            Assert.AreEqual(tl.X, cartRect.Left);
            Assert.AreEqual(tl.Y, cartRect.Top);
            Assert.AreEqual(br.X, cartRect.Right);
            Assert.AreEqual(br.Y, cartRect.Bottom);
            Assert.AreEqual(6, cartRect.Width);
            Assert.AreEqual(12, cartRect.Height);

            var cart = new CartRect(cartRect);
            Assert.AreEqual(cartRect, cart);
        }

        [Test]
        public void ContainsPointTest()
        {
            var cart = new CartRect(new Point(0, 10), new Point(10, 0));
            var containP = new Point(5, 5);
            var nonContainP = new Point(-5, -5);
            Assert.IsTrue(cart.ContainsPoint(containP));
            Assert.IsFalse(cart.ContainsPoint(nonContainP));
        }

        [Test]
        public void TopCartTest()
        {
            var bottomLeft = new Point(1, 1);
            var firstSize = new Vector(1, 1);
            panelRect = new Rect(bottomLeft, firstSize);
            cartRect = new CartRect(bottomLeft, firstSize);
            Assert.AreEqual(1, panelRect.Top);
            Assert.AreEqual(2, panelRect.Bottom);
            Assert.AreEqual(1, panelRect.Left);
            Assert.AreEqual(2, panelRect.Right);
            Assert.AreEqual(2, cartRect.Top);
            Assert.AreEqual(1, cartRect.Bottom);
            Assert.AreEqual(1, cartRect.Left);
            Assert.AreEqual(2, cartRect.Right);
            Assert.AreEqual(new Point(1.5, 1.5), panelRect.GetCenter());
            Assert.AreEqual(new Point(1.5, 1.5), cartRect.Center);
        }
        [Test]
        public void PointContructionTest()
        {
            var topLeftCart = new Point(1, 1);
            var bottomRightCart = new Point(2, -1);
            cartRect = new CartRect(topLeftCart, bottomRightCart);
            Assert.AreEqual(1, cartRect.Left);
            Assert.AreEqual(2, cartRect.Right);
            Assert.AreEqual(1, cartRect.Top);
            Assert.AreEqual(-1, cartRect.Bottom);
        }

        [Test]
        public void RectConstructorTest()
        {
            panelRect = new Rect(new Point(1, 2), new Size(3, 3));
            Assert.AreEqual(2, panelRect.Top);
            Assert.AreEqual(5, panelRect.Bottom);
            Assert.AreEqual(1, panelRect.Left);
            Assert.AreEqual(4, panelRect.Right);

            cartRect = new CartRect(panelRect, false);
            Assert.AreEqual(-2, cartRect.Top);
            Assert.AreEqual(-5, cartRect.Bottom);
            Assert.AreEqual(1, cartRect.Left);
            Assert.AreEqual(4, cartRect.Right);

            cartRect = new CartRect(panelRect, true);
            Assert.AreEqual(5, cartRect.Top);
            Assert.AreEqual(2, cartRect.Bottom);
            Assert.AreEqual(1, cartRect.Left);
            Assert.AreEqual(4, cartRect.Right);
        }

        [Test]
        public void PointSizeContructorTest()
        {
            cartRect = new CartRect(new Point(1, 1), new Size(3, 3));
            Assert.AreEqual(1, cartRect.Bottom);
            Assert.AreEqual(4, cartRect.Top);
            Assert.AreEqual(1, cartRect.Left);
            Assert.AreEqual(4, cartRect.Right);
        }

        [Test]
        public void RectIntesectTest()
        {
            var first = new CartRect(new Point(1, 2), new Size(3, 5));
            var intersecting = new CartRect(new Point(2, 3), new Size(3, 3));
            var nonIntersecting = new CartRect(new Point(-1, 1), new Size(1, 1));
            var huge = new CartRect(new Point(-5, -5), new Size(15, 15));
            Assert.IsTrue(first.IntersectWith(intersecting));
            Assert.IsFalse(first.IntersectWith(nonIntersecting));
            Assert.IsTrue(first.IntersectWith(huge));

        }
    }
}