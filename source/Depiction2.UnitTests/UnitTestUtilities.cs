﻿using NUnit.Framework;

namespace Depiction2.UnitTests
{
    public class UnitTestUtilities
    {
        const string noDec = "0";
        const string oneDec = "0.0";
        const string twoDec = "0.00";
        const string threeDec = "0.000";
        const string fourDec = "0.0000";
        const string fiveDec = "0.00000";
        static public void AssertDoubleEquality(double expected, double calced, int decimals)
        {
            //Assert.AreEqual(expected,calced,1d/decimals);
            switch (decimals)
            {
                case 0:
                    Assert.AreEqual(expected.ToString(noDec), calced.ToString(noDec));
                    break;
                case 1:
                    Assert.AreEqual(expected.ToString(oneDec), calced.ToString(oneDec));
                    break;
                case 2:
                    Assert.AreEqual(expected.ToString(twoDec), calced.ToString(twoDec));
                    break;
                case 3:
                    Assert.AreEqual(expected.ToString(threeDec), calced.ToString(threeDec));
                    break;
                case 4:
                    Assert.AreEqual(expected.ToString(fourDec), calced.ToString(fourDec));
                    break;
                case 5:
                    Assert.AreEqual(expected.ToString(fiveDec), calced.ToString(fiveDec));
                    break;
            }
        }
    }
}