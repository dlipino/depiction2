﻿using System;
using System.Windows;
using System.Windows.Input;
using Depiction2.Utilities;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.DrawControls
{
    [TestFixture]
    public class WorldControlGridTests
    {
        Point WorldOffset = new Point();
        private double WorldScale = 1;
        [Test]
        public void ZoomToPointTest()
        {
            var mouseScreenLocation = new Point(34,56);
            var mouseDelta = 120;
            var oneLineDelta = Mouse.MouseWheelDeltaForOneLine;
            var delta = mouseDelta;// e.Delta;
            var x = Math.Pow(2, delta / 3.0 / oneLineDelta);
            WorldScale *= x;

            // Adjust the offset to make the point under the mouse stay still.
            var position = (Vector)mouseScreenLocation;
            WorldOffset = (Point)((Vector)(WorldOffset + position) * x - position);

            var scaleOffset = ((Point)((Vector)WorldOffset / WorldScale));
            var latLong = new Point(scaleOffset.X, -scaleOffset.Y);

            //Convert things back to WorldPixelOffset
            var conv = new Point(latLong.X, -latLong.Y);
            var viewPoint = WorldScale*((Vector) conv);// - (Vector)WorldOffset / WorldScale);


        }
        [Test]
        public void NumberTypeConvesion()
        {
            int integer = 1;
            double doub = 2.43d;

            var converteredInt = RandomUtilities.CheapNumberTypeConverter(doub, typeof(int));
            Assert.AreEqual(typeof(int), converteredInt.GetType());
            Assert.AreEqual(2, converteredInt);
            Assert.IsNotNull(converteredInt);

            var converteredDouble = RandomUtilities.CheapNumberTypeConverter(integer, typeof(double));
            Assert.IsNotNull(converteredDouble);
            Assert.AreEqual(typeof(double), converteredDouble.GetType());
            Assert.AreEqual(1d, converteredDouble);
        }

    }
}