﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.GeometryEditor.DrawShapes;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.GeometryEditor
{
    [TestFixture]
    public class DrawPolylineTest
    {

        private List<Point> basePoints = new List<Point> { new Point(0, 0), new Point(1, 1), new Point(6, 8), new Point(7, 6) };
        [Test]
        public void PolylineRemoveTest()
        {

            var initVertCount = basePoints.Count;
            var polyline = GeometryEditCanvasTest.CreatePolyline(basePoints);
            var removeVert = polyline.ShapeVertices[1];
            polyline.RemoveVertex(removeVert);
            Assert.AreEqual(initVertCount - 1, polyline.ShapeVertices.Count); 
            foreach (var shapeVertex in polyline.ShapeVertices)
            {
                Assert.AreEqual(1, shapeVertex.PartOf.Count);
            }

        }

        [Test]
        public void PolylineInsertTest()
        {
            var initVertCount = basePoints.Count;
            var polyline = GeometryEditCanvasTest.CreatePolyline(basePoints);
            var insP = new Point(4, 5);

            var beforeVert = polyline.ShapeVertices[1];
            polyline.InsertVertexAfter(beforeVert, new DrawPoint(insP));
            Assert.AreEqual(initVertCount + 1, polyline.ShapeVertices.Count);
            foreach (var shapeVertex in polyline.ShapeVertices)
            {
                Assert.AreEqual(1,shapeVertex.PartOf.Count);
            }

        }
    }
}