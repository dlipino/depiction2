﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.GeometryEditor;
using Depiction2.GeometryEditor.DrawShapes;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.GeometryEditor
{
    [TestFixture]
    public class DrawPolygonTest
    {

        private List<Point> basePoints = new List<Point> { new Point(0, 0), new Point(1, 1), new Point(6, 8), new Point(7, 6) };
        [Test]
        public void PolygonRemoveTest()
        {
            var initVertCount = basePoints.Count;
            var f = GeometryEditCanvasTest.CreatePolygon(basePoints);
            var vert1 = f.ShapeVertices[0];
            f.RemoveVertex(vert1);
            foreach (var vert in f.ShapeVertices)
            {
                Assert.AreEqual(3, vert.PartOf.Count);
            }
            var vertCount = initVertCount - 1;
            Assert.AreEqual(vertCount, f.ShapeVertices.Count);

            Assert.AreEqual(1 + vertCount * 2, f.ShapeParts.Count);
        }
        [Test]
        public void PolygonInsertVertTest()
        {
            var initVertCount = basePoints.Count;
            var f = GeometryEditCanvasTest.CreatePolygon(basePoints);

            var vert1 = f.ShapeVertices[1];
            f.InsertVertexAfter(vert1, new DrawPoint(new Point(3, 4)));
            foreach (var vert in f.ShapeVertices)
            {
                Assert.AreEqual(3, vert.PartOf.Count);
            }
            Assert.AreEqual(initVertCount+1,f.ShapeVertices.Count);
        }
    }
}