﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.GeometryEditor;
using Depiction2.GeometryEditor.DrawShapes;
using NUnit.Framework;

namespace Depiction2.UnitTests.Depiction2.GeometryEditor
{
    [TestFixture]//Test the functionality of the add/remove of the canvas. Cant test the draw results.
    public class GeometryEditCanvasTest
    {
      
        private List<IEditableShape> drawnShapes;
        private List<Point> basePoints = new List<Point> { new Point(0, 0), new Point(1, 1), new Point(6, 8), new Point(7,6) };
        
        [SetUp]
        public void Setup()
        {
            drawnShapes = new List<IEditableShape>();
        }
        [TearDown]
        public void Teardown()
        {
            drawnShapes = null;
        }

        static internal DrawPolygon CreatePolygon(List<Point> polyPoints )
        {
            Assert.IsNotNull(polyPoints);
            var polygon = new DrawPolygon();
            foreach (var p in polyPoints)
            {
                polygon.AddVertex(new DrawPoint(p));
            }
            polygon.CompleteShape();

            Assert.IsNotNull(polygon);
            var initVertCount = polyPoints.Count;
            Assert.AreEqual(1 + initVertCount * 2, polygon.ShapeParts.Count);
            foreach (var vert in polygon.ShapeVertices)
            {
                Assert.AreEqual(3, vert.PartOf.Count);
            }
            return polygon;
        }

        static internal DrawPolyline CreatePolyline(List<Point> polyPoints )
        {
            Assert.IsNotNull(polyPoints);
            var polyline = new DrawPolyline();
            foreach (var p in polyPoints)
            {
                polyline.AddVertex(new DrawPoint(p));
            }
            polyline.CompleteShape();

            Assert.IsNotNull(polyline);
            Assert.AreEqual(1 + polyPoints.Count, polyline.ShapeParts.Count);
            foreach (var vert in polyline.ShapeVertices)
            {
                Assert.AreEqual(1, vert.PartOf.Count);
            }
            return polyline;
        }

    
    
    
    }
}