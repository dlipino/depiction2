﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Interactions;
using Depiction2.Core.Interactions.Helpers;
using Depiction2.Core.StoryEntities;
using Depiction2.TempExtensionCore;
using DepictionLegacy.ConditionsAndBehaviours.Conditions;
using NUnit.Framework;

namespace Depiction2.UnitTests.DepictionLegacy.ConditionsAndBehaviours.Conditions
{
    [TestFixture]
    public class IntersectionConditionTest
    {
        private IElement inElement;
        private IDepictionGeometry inGeometry;
        private IElement targetElement;
        private IDepictionGeometry targetGeometry;
        private IElement outElement;
        private IDepictionGeometry outGeometry;

        private string area = "Area";
        private string target = "Other";
        private string propToChange = "Active";
        const string propertySetBehaviour = "SetProperty";
        private Story testStory;


        [SetUp]
        public void Setup()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
            inElement = CreateElementWithActiveProperty(target, new List<Point> {new Point()});
            outElement = CreateElementWithActiveProperty(target, new List<Point> {new Point(5, 5)});
            targetElement = CreateElementWithActiveProperty(area,
                                                            new List<Point>
                                                                {
                                                                    new Point(1, 1),
                                                                    new Point(1, -1),
                                                                    new Point(-1, -1),
                                                                    new Point(-1, 1),
                                                                    new Point(1, 1)
                                                                });
            testStory = new Story();
            
        }
        private IElement CreateElementWithActiveProperty(string elemType, List<Point> geometryPoints )
        {
            var elem = new DepictionElement(elemType);
            var geom = DepictionGeometryService.CreateGeometry(geometryPoints);
            elem.UpdatePrimaryPointAndGeometry(null,geom);
            var prop = new ElementProperty(propToChange);
            prop.SetValue(true);
            elem.AddUserProperty(prop);
            return elem;
        }
        [TearDown]
        public void TearDown()
        {
            testStory = null;
            DepictionExtensionLibrary.Instance.Decompose();
        }

        [Test]
        public void EvaluateTest()
        {
            var intersectionCondition = new IntersectionCondition();
            Assert.IsTrue(intersectionCondition.Evaluate(inElement, targetElement, null));
            Assert.IsTrue(intersectionCondition.Evaluate(targetElement, inElement, null));

            Assert.IsFalse(intersectionCondition.Evaluate(outElement, targetElement, null));
            Assert.IsFalse(intersectionCondition.Evaluate(targetElement, outElement, null));
        }

        [Test]
        public void CheckInteractiongraphWithIntesection()
        {
            var ruleRepo = new InteractionRuleRepository();
            
            var rule = new InteractionRule { Publisher = area, Name = "Overlap" };
            rule.Subscribers.Add(target);
            rule.Behaviors.Add(propertySetBehaviour, new[] { new ElementFileParameter { Type = ElementFileParameterType.Subscriber, ElementQuery = propToChange }, 
                new ElementFileParameter { Type = ElementFileParameterType.Value, ElementQuery = "false" } });
            rule.Conditions.Add("IntersectionCondition", new HashSet<string> { "ElementGeometry" });
            ruleRepo.AddInteractionRule(rule);

            var dir = AppDomain.CurrentDomain.BaseDirectory;
            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
            var conditionName = "IntersectionCondition";
            var condition = DepictionExtensionLibrary.Instance.RetrieveCondition(conditionName);
            Assert.IsNotNull(condition);

            var router = new Router(testStory, ruleRepo);
            router.SetupRouterInteractionGraphAndEventListeners();

            testStory.AddElement(targetElement);
            testStory.AddElement(outElement);
            testStory.AddElement(inElement);

            inElement.PropertyChanged += inElement_PropertyChanged;

            router.DoSingleInteractionPassNoMessages();

            CheckActiveProp(inElement, false);
            CheckActiveProp(outElement, true);
            
            DepictionExtensionLibrary.Instance.Decompose();
            inElement.PropertyChanged -= inElement_PropertyChanged;
        }
        private void CheckActiveProp(IElement elem, bool value)
        {
            var activeProp = elem.GetProperty(propToChange);
            Assert.IsNotNull(activeProp);
            Assert.AreEqual(value, activeProp.Value); 
        }
        void inElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Assert.AreEqual(propToChange,e.PropertyName);
        }
    }
}