﻿using Depiction2.Base.Geo;

namespace Depiction.Legacy.Types
{
    public class ImageMetedata14
    {
        public string ImageName { get; set; }
        public ICartRect Bounds { get; set; }
        public double Rotation { get; set; }
    }
}