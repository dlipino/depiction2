﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Depiction.Legacy
{
    public class DepictionFileFormatInfoLegacy : IXmlSerializable
    {
        const int DepictionFileVersion130=1;//Base 1.3 file format
        public const int MostModernDepictionVersionFileNumber = DepictionFileVersion130;
        #region ElementProperties

        public int DepictionFileVersion { get; private set; }
        public string AppVersion { get; set; }

        public string MinimumAppVersionForDepiction { get; set; }
//        public List<DepictionSubtreeInfoBase> DepictionSubtrees
//        {
//            get { return depictionSubtrees; }
//            internal set { depictionSubtrees = value; }
//        }

        #endregion

//        private List<DepictionSubtreeInfoBase> depictionSubtrees = DefaultDepictionSubtrees;
        #region Constructor

//        public DepictionFileFormatInfoLegacy() : this( VersionInfo.AppVersion + "." + VersionInfo.BuildNumber, VersionInfo.AppVersion + "." + VersionInfo.BuildNumber) { }
//        public DepictionFileFormatInfoLegacy(List<DepictionSubtreeInfoBase> subtrees)
//            : this( VersionInfo.AppVersion + "." + VersionInfo.BuildNumber, VersionInfo.AppVersion + "." + VersionInfo.BuildNumber)
//        {
//            depictionSubtrees = subtrees;
//        }
        public DepictionFileFormatInfoLegacy(string appVersion, string minApp)
        {
            DepictionFileVersion = MostModernDepictionVersionFileNumber;
            AppVersion = appVersion;
            MinimumAppVersionForDepiction = minApp;
        }
        #endregion

        #region File serialization setup
//
//        //Currently assumes one file/subtree, but this doesn't have to be the case, especially for lists.
//        static public List<DepictionSubtreeInfoBase> DefaultDepictionSubtrees = new List<DepictionSubtreeInfoBase>
//                                   {
//                                       new DepictionSubtreeInfoBase("","","DepictionMetadata.xml","DepictionInformation",
//                                           typeof(DepictionStoryMetadata)){UserReadableName = "Depiction Information"},
//                                       new DepictionSubtreeInfoBase("DepictionData","","DepictionGeoInformation.xml","DepictionGeoInformation",
//                                           typeof(DepictionGeographicInfo)){UserReadableName = "Region information"},
//                                       new DepictionSubtreeInfoBase("DepictionData","Images","DepictionImages.xml","DepictionImages",
//                                           typeof(RasterImageResourceDictionary)){UserReadableName = "User Images"},
//                                       new DepictionSubtreeInfoBase("DepictionData","AllDepictionElements","","DepictionElements",
//                                           typeof(ElementRepository)){UserReadableName = "All Elements"},
//                                       new DepictionSubtreeInfoBase("DepictionData","Annotations","DepictionAnnotations.xml","DepictionAnnotations",
//                                           typeof(List<IDepictionAnnotation>)){UserReadableName = "Annotations"},
//                                       new DepictionSubtreeInfoBase("DepictionData","Displayers","DepictionRevealers.xml","DepictionRevealers",
//                                           typeof(List<DepictionRevealer>)){UserReadableName = "Revealers"},
//                                       new DepictionSubtreeInfoBase("DepictionData","Displayers","DepictionElementBackdrop.xml","DepictionBackdrop",
//                                           typeof(DepictionElementBackdrop)){UserReadableName = "Main map"},
//                                       new DepictionSubtreeInfoBase("DepictionData","ElementLibrary",string.Empty,string.Empty,
//                                           typeof(ElementPrototypeLibrary)){UserReadableName = "Default Element Library"},
//                                       new DepictionSubtreeInfoBase("DepictionData","InteractionRules","DepictionInteractionRules.xml","DepictionInteractions",
//                                           typeof(InteractionRuleRepository)){UserReadableName = "Active Interaction Library"}
//                                   };

        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var fileInfo = (DepictionFileFormatInfoLegacy)obj;

//            // use this pattern to compare reference members
//            if (depictionSubtrees.Count != fileInfo.depictionSubtrees.Count) return false;
//            for (int i = 0; i < depictionSubtrees.Count; i++)
//            {
//                if (!Equals(depictionSubtrees[i], fileInfo.depictionSubtrees[i])) return false;
//            }

            if (!DepictionFileVersion.Equals(fileInfo.DepictionFileVersion)) return false;
            if (!AppVersion.Equals(fileInfo.AppVersion)) return false;
            if (!MinimumAppVersionForDepiction.Equals(fileInfo.MinimumAppVersionForDepiction)) return false;

            return true;
        }
        #endregion

        #region Xml serialization
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        public void ReadXml(XmlReader reader)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            if (SerializationServiceLegacy.IsSaveLoadCancelled() == true) { return; }
            var mainNodeName = reader.Name;
            reader.ReadStartElement();
            DepictionFileVersion = int.Parse(reader.ReadElementContentAsString("DepictionFileVersion", ns));

            if (reader.Name.Equals("AppVersion"))
                AppVersion = reader.ReadElementContentAsString("AppVersion", ns);
            if (reader.Name.Equals("MinimumAppVersionForDepiction"))
                MinimumAppVersionForDepiction = reader.ReadElementContentAsString("MinimumAppVersionForDepiction", ns);
            else
                MinimumAppVersionForDepiction = "unknown";
            //            if (reader.InternalName.Equals("BuildNumber"))
            //                reader.ReadElementContentAsString("BuildNumber", ns);

//            if (reader.Name.Equals("DepictionSubtrees"))
//            {
//                //Hopefully this does not fail, or there will be problems, will need to test this when the code is more developed.
//                //This part of the read must always succeed, without this the rest of the read is lost.
//                var loadedSubtrees =
//                    SerializationConstantsLegacy.DeserializeItemList<DepictionSubtreeInfoBase>("DepictionSubtrees",
//                                                                                       typeof(DepictionSubtreeInfoBase),
//                                                                                       reader);
//                DepictionSubtrees = new List<DepictionSubtreeInfoBase>(loadedSubtrees);
//            }

            while (!(reader.NodeType.Equals(XmlNodeType.EndElement) && reader.Name.Equals(mainNodeName)))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            if (SerializationServiceLegacy.IsSaveLoadCancelled() == true) return;
            writer.WriteStartElement("DepictionFileFormatInformation",ns);
            writer.WriteElementString("DepictionFileVersion", ns, DepictionFileVersion.ToString());
            writer.WriteElementString("AppVersion", ns, AppVersion);
            writer.WriteElementString("MinimumAppVersionForDepiction", ns, MinimumAppVersionForDepiction);
//            SerializationService.SerializeItemList("DepictionSubtrees", DepictionSubtrees, writer);
            writer.WriteEndElement();

        }

        #endregion
    }
}