﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Depiction.Legacy
{
    public class SerializationServiceLegacy
    {
        static SerializationServiceLegacy()
        {
            rootPath = String.Empty;
        }

        #region Variables
        //I don't think this loks in API for serialization contracts
        private static readonly Dictionary<Type, DataContractSerializer> serializers = new Dictionary<Type, DataContractSerializer>();
        private static string rootPath;
        public static string SerializedImageFileFolder
        {
            get { return Path.Combine(rootPath, "binaryFiles"); }
        }

        #endregion

        #region BackgroundThread region
        private static BackgroundWorker saveLoadThread;
        //This takes care of its own removal
        public static BackgroundWorker SaveLoadThread
        {
            get { return saveLoadThread; }
        }
        public static DoWorkEventArgs ThreadConnection { get; set; }

        static void saveLoadThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            SaveLoadThread.RunWorkerCompleted -= saveLoadThread_RunWorkerCompleted;
            saveLoadThread = null;
            ThreadConnection = null;
            Console.WriteLine(saveLoadThread + " serialization save load theread");
        }
        static public void CancelLoadOrSave()
        {
            if (SaveLoadThread != null)
            {
                SaveLoadThread.CancelAsync();
                if (ThreadConnection != null) ThreadConnection.Cancel = true;
            }
        }
        static public bool? IsSaveLoadCancelled()
        {
            var bg = SaveLoadThread;
            if (bg == null) return null;
            if (bg.CancellationPending)
            {
                return true;
            }
            return false;
        }
        static public bool? UpdateProgressReport(double percent, object info)
        {
            var bg = SaveLoadThread;
            if (bg == null) return null;
            if (bg.CancellationPending)
            {
                return false;
            }
            bg.ReportProgress((int)percent, info);
            return true;
        }
        static public bool? SetSaveLoadBackgroundThread(BackgroundWorker thread)
        {
            ThreadConnection = null;
            if (saveLoadThread == null)
            {
                saveLoadThread = thread;
                if (saveLoadThread != null)
                {
                    saveLoadThread.RunWorkerCompleted += saveLoadThread_RunWorkerCompleted;
                    return true;
                }
                return null;
            }
            //Never interrupt a busy thread
            if (!saveLoadThread.IsBusy)
            {
                saveLoadThread = thread;
                if (saveLoadThread != null)
                {
                    saveLoadThread.RunWorkerCompleted += saveLoadThread_RunWorkerCompleted;
                    return true;
                }
                return null;
            }
            return false;
        }

        #endregion

        #region Helper methods

        public static XmlReader GetXmlReader(Stream stream)
        {
            var settings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true, CheckCharacters = false };
            return XmlReader.Create(stream, settings);
        }
        public static XmlWriter GetXmlWriter(Stream stream)
        {
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, CheckCharacters = false };
            return XmlWriter.Create(stream, settings);
        }

        public static DataContractSerializer GetSerializer(Type type)
        {
            DataContractSerializer serializer;

            if (!serializers.TryGetValue(type, out serializer))
            {
                var xmlRootAttribute = type.GetCustomAttributes(typeof(XmlRootAttribute), false);
                if (xmlRootAttribute.Length > 0)
                    serializer = new DataContractSerializer(type, ((XmlRootAttribute)xmlRootAttribute[0]).ElementName, SerializationConstantsLegacy.DepictionXmlNameSpace);
                else if (type.IsGenericType || type.IsArray)
                    serializer = new DataContractSerializer(type);
                else
                    serializer = new DataContractSerializer(type, type.Name, SerializationConstantsLegacy.DepictionXmlNameSpace);

                serializers.Add(type, serializer);
            }

            return serializer;
        }
        #endregion

        #region XML Serialization
        public static void Serialize<T>(string localName, T obj, XmlWriter writer)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            writer.WriteStartElement(localName);//, ns);
            GetSerializer(typeof(T)).WriteObject(writer, obj);
            writer.WriteEndElement();
        }

        public static void SerializeContent<T>(T obj, XmlWriter writer)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            GetSerializer(typeof(T)).WriteObjectContent(writer, obj);
        }

        public static void SerializeObject(string localName, object obj, Type type, XmlWriter writer)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            writer.WriteStartElement(localName);//, ns);
            SerializeObject(obj, type, writer);
            //            GetSerializer(type).WriteObject(writer, obj);
            writer.WriteEndElement();
        }
        //Automatically surround with Type, and just writes internal data, to read back you need the correct deserializer
        public static void SerializeObject<T>(string localName, T obj, XmlWriter writer)
        {
            SerializeObject(localName, obj, typeof(T), writer);
        }

        /// Serializes an object, not surrounding it with additional tags. Why isn't there a shorter version of this? (davidl)
        public static void SerializeObject(object obj, Type type, XmlWriter writer)
        {
            if (obj is IXmlSerializable)
            {
                ((IXmlSerializable)obj).WriteXml(writer);
            }
            else
            {

                GetSerializer(type).WriteObject(writer, obj);
            }
        }

        #endregion

        #region XML Deserialization

        public static T Deserialize<T>(XmlReader reader)
        {
            var ret = (T)GetSerializer(typeof(T)).ReadObject(reader);
            //reader.ReadEndElement();
            return ret;
        }

        public static T Deserialize<T>(string localName, XmlReader reader)
        {
            //var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            reader.ReadStartElement(localName);//, ns);
            var ret = (T)GetSerializer(typeof(T)).ReadObject(reader);
            reader.ReadEndElement();
            return ret;
        }
        public static object DeserializeObject<T>(string localName, Type type, XmlReader reader)
        {
            //var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            reader.ReadStartElement(localName);//, ns);
            object ret;
            if (type.GetInterface(typeof(IXmlSerializable).Name) != null)
            {
                //                var construct = type.GetConstructor(Type.EmptyTypes);//Future reference
                ret = Activator.CreateInstance(type);
                ((IXmlSerializable)ret).ReadXml(reader);
            }
            else
            {
                if (type.Equals(typeof(object)) && reader.GetAttribute("type") != null)
                {
                    var inType = DepictionLegacyTypeInformationSerialization.GetFullTypeFromSimpleTypeString(reader.GetAttribute("type"));
                    ret = Activator.CreateInstance(inType);
                    ((IXmlSerializable)ret).ReadXml(reader);
                }
                else
                {
                    ret = GetSerializer(type).ReadObject(reader);
                }
            }
            reader.ReadEndElement();
            return ret;
        }

        public static T DeserializeObject<T>(string localName, XmlReader reader)
        {
            return (T)DeserializeObject<T>(localName, typeof(T), reader);
        }
        public static object DeserializeObject(Type type, XmlReader reader)
        {
            object ret;
            if (type.GetInterface(typeof(IXmlSerializable).Name) != null)
            {
                ret = Activator.CreateInstance(type);
                ((IXmlSerializable)ret).ReadXml(reader);
            }
            else
            {
                ret = GetSerializer(type).ReadObject(reader);
            }

            return ret;
        }

        public static T DeserializeObject<T>(XmlReader reader)
        {
            return (T)DeserializeObject(typeof(T), reader);
        }

        #endregion

        #region Load from/Save to File

        public static void SaveToXmlFile<T>(T objectToSave, string fileName, string localName)
        {
            rootPath = Path.GetDirectoryName(fileName);
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    using (var writer = GetXmlWriter(stream))
                    {
                        SerializeObject(localName, objectToSave, writer);
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine("From serialize :" + ex.Message); }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        public static T LoadFromStream<T>(Stream stream, string localName)
        {
            using (var reader = GetXmlReader(stream))
            {
                //var xmlDoc = new XmlDocument();
                return DeserializeObject<T>(localName, reader);
            }

        }

        public static T LoadFromXmlFile<T>(string filePath, string localName)
        {
            rootPath = Path.GetDirectoryName(filePath);
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    return LoadFromStream<T>(stream, localName);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From DeserializeObject:" + ex.Message);
                return default(T);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        #endregion

        #region List Serialization/Deserialization


        /// <summary>
        /// Serializes a list of items of all one CLR type
        /// </summary>
        /// <typeparam name="T"></typeparam>

        public static void SerializeItemList<T>(string localName, Type type, IEnumerable<T> elementList, XmlWriter writer,bool useDepictionNameSpace)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            if(useDepictionNameSpace)
            {
                writer.WriteStartElement(localName, ns);
            }else
            {
                writer.WriteStartElement(localName);
            }
            foreach (var element in elementList)
            {
                SerializeObject(element, type, writer);
            }
            writer.WriteEndElement();
        }
        public static void SerializeItemList<T>(string localName, Type type, IEnumerable<T> elementList, XmlWriter writer)
        {
           SerializeItemList(localName,type,elementList,writer,false);
        }

        /// <summary>
        /// Used from outside of a to save elements save command
        /// </summary>
        /// <typeparam name="T" Interface type of the list to save></typeparam>
        /// <typeparam name="B" Implemented type of the list that is getting saved></typeparam>
        /// <param name="fileName"></param>
        /// <param name="elementList"></param>
        /// <param name="localName"></param>
        public static void SerializeItemListOneFile<T, B>(string fileName, IEnumerable<T> elementList, string localName)
        {
            rootPath = Path.GetDirectoryName(fileName);
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            XmlWriter writer;

            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    using (writer = GetXmlWriter(stream))
                    {
                        writer.WriteStartElement(localName);//, ns);
                        foreach (var element in elementList)
                        {
                            SerializeObject(element, typeof(B), writer);
                        }
                        writer.WriteEndElement();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From serialize :" + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }
        /// <summary>
        ///  Used to load lists from a random file
        /// </summary>
        /// <typeparam name="T" Interface of the final list></typeparam>
        /// <typeparam name="B" Base type of the thing that was saved in the list></typeparam>
        /// <param name="fileName"></param>
        /// <param name="localName"></param>
        /// <returns></returns>
        public static IList<T> DeserializeItemListFromOneFile<T, B>(string fileName, string localName)
        {
            rootPath = Path.GetDirectoryName(fileName);
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            var elementList = new List<T>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            XmlReader reader;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (reader = GetXmlReader(stream))
                    {
                        reader.ReadStartElement(localName);//, ns);
                        if (reader.NodeType != XmlNodeType.None)
                        {
                            while (reader.NodeType != XmlNodeType.EndElement)
                            {
                                var element = DeserializeObject(typeof(B), reader);
                                elementList.Add((T)element);
                            }
                            reader.ReadEndElement();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From DeserializeObject:" + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
            return elementList;
        }

        /// <summary>
        /// Serializes a list of items of all one CLR type. Generally used from within an elements savtoXml method
        /// </summary>
        /// <typeparam name="T"></typeparam>

        public static void SerializeItemList<T>(string localName, IEnumerable<T> elementList, XmlWriter writer)//Type type,
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            writer.WriteStartElement(localName);//, ns);
            foreach (var element in elementList)
            {
                //Console.WriteLine(element.GetType() + " from serializeItemList, the other ");
                SerializeObject(element, element.GetType(), writer);
            }
            writer.WriteEndElement();
        }

        /// <summary>
        /// Deserializes a list of items of all one CLR type. Returns empyt list of the type
        /// if the node type does not match.
        /// </summary>
        /// This method needs a bit of work, not sure why it doesnt just use the T
        /// <typeparam name="T"></typeparam>
        public static IList<T> DeserializeItemList<T>(string localName, Type type, XmlReader reader)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            var elementList = new List<T>();

            //Return empty list if the node type is empty (davidl). would it be better to return a null?
            if (reader.NodeType.Equals(XmlNodeType.EndElement)) return elementList;
            try
            {
                bool listIsEmpty = reader.IsEmptyElement;
                reader.ReadStartElement(localName);//, ns);
                if (!listIsEmpty)
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        var interType = type;
                        if (interType == null)
                        {
                            var typeName = reader.Name;
                            interType = Type.GetType(typeName);
                        }
                        try
                        {//If a single object gives a problem keep going
                            var element = DeserializeObject(interType, reader);
                            elementList.Add((T)element);
                        }catch(Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                        
                        //                        reader.MoveToContent();

                    }
                    reader.ReadEndElement();
                }
            }
            catch (Exception ex) { return new List<T>(); }

            return elementList;
        }

        /// <summary>
        /// Serializes a heterogeneous list of items, used when a writer is available
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localName"></param>
        /// <param name="itemName"></param>
        /// <param name="elementList"></param>
        /// <param name="writer"></param>
        public static void SerializeHeterogeneousItemList<T>(string localName, string itemName, IEnumerable<T> elementList, XmlWriter writer)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            writer.WriteStartElement(localName);//, ns);

            foreach (var element in elementList)
            {
                var type = element.GetType();
//                writer.WriteElementString(itemName + "_typeName", ns, GetSimpleSerializableTypeName(type));
                var typeName = DepictionLegacyTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(type);
           
                writer.WriteElementString(itemName + "_typeName", typeName);
                SerializeObject(itemName, element, type, writer);
            }

            writer.WriteEndElement();
        }
        /// <summary>
        /// DeserializeHeterogeneousItemList, used when a reader is avaible (with a classes read method) 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="localName"></param>
        /// <param name="itemName"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static IList<T> DeserializeHeterogeneousItemList<T>(string localName, string itemName, XmlReader reader)
        {
            var ns = SerializationConstantsLegacy.DepictionXmlNameSpace;
            var elementList = new List<T>();

            bool listIsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(localName);//, ns);
            if (!listIsEmpty)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    var type = DepictionLegacyTypeInformationSerialization.GetFullTypeFromSimpleTypeString(reader.ReadElementContentAsString(itemName + "_typeName", ns));

                    var element = (T)DeserializeObject<T>(itemName, type, reader);

                    elementList.Add(element);
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
            return elementList;
        }

        #endregion

        #region Things that might get the axe
        /// Deserializes an object, that has not been surrounded with additional tags

        public static object DeserializeObject(string localName, Type type, Stream stream)
        {
            using (var reader = GetXmlReader(stream))
            {
                return DeserializeObject<object>(localName, type, reader);
            }
        }

        public static Stream SerializeObject(string localName, object obj, Type type, Stream stream)
        {
            //There was something about invariantculter before
            using (var writer = GetXmlWriter(stream))
            {
                SerializeObject(localName, obj, type, writer);
            }
            return stream;
        }

        public static void SerializeToFolder<T>(string localName, T objectToSave, string xmlFileName, string folderName)
        {
            rootPath = folderName;
            if (!Directory.Exists(rootPath))
                Directory.CreateDirectory(rootPath);
            var fullFileName = Path.Combine(folderName, xmlFileName);
            SaveToXmlFile(objectToSave, fullFileName, localName);
            rootPath = "";
        }
        public static T DeserializeFromFolder<T>(string localName, string xmlFileName, string folderName)
        {
            rootPath = folderName;
            var fullFileName = Path.Combine(folderName, xmlFileName);
            var retVal = LoadFromXmlFile<T>(fullFileName, localName);
            rootPath = "";
            return retVal;
        }

        public static T DeserializeFromFolder<T>(string localName, Stream xmlStream, string folderName)
        {
            rootPath = folderName;
            T retVal;
            using (var reader = GetXmlReader(xmlStream))
            {
                retVal = DeserializeObject<T>(localName, reader);
            }
            rootPath = "";
            return retVal;
        }
        #endregion
    }
}