﻿using System.Globalization;
using System.IO;
using System.Threading;
using Depiction2.Base.Interactions;
using Depiction2.Core.Interactions;
using Depiction2.Utilities.Legacy;
using Ionic.Zip;

namespace Depiction.Legacy.Depiction14PartLoaders
{
    public class Depiction14InteractionRuleLoader
    {
        #region Depiction interaction file reading

        public static IInteractionRuleRepository GetInteractionsFromDPNFile(string fileName)
        {
            IInteractionRuleRepository interactionRules = null;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var fullElemFileName = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.InteractionDir + "/" + Depiction14FileOpenHelpers.InteractionFile;
                var formatZipEntry = zipFile[fullElemFileName];

                if (formatZipEntry != null)
                {
                    interactionRules = GetInteractionRepoFromStream(formatZipEntry.OpenReader());
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return interactionRules;
        }

        private  static IInteractionRuleRepository GetInteractionRepoFromStream(Stream interactionRepoStream)
        {
            var interactionRuleRepo = new InteractionRuleRepository();
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(interactionRepoStream))
            {
                reader.Read();
                interactionRuleRepo.ReadXml(reader);
            }
            return interactionRuleRepo;
        }

//        private List<IInteractionRule> GetInteractionsFromStream(Stream interactionFileStream)
//        {
//            var readInteractions = new List<IInteractionRule>();
//            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(interactionFileStream))
//            {
//                reader.Read(); //Start the read process
//                if (reader.Name.Equals("DepictionInteractions"))
//                {
//                    reader.ReadToFollowing("InteractionRule");
//                }
//
//                while (reader.Name.Equals("InteractionRule") && reader.NodeType.Equals(XmlNodeType.Element))
//                {
//                    reader.ReadStartElement();
//
//                    var interactionRule = new TempInteraction();
//                    //Get the properties
//                    while (!reader.Name.Equals("InteractionRule"))
//                    {
//                        if (reader.Name.Equals("Name"))
//                        {
//                            reader.Skip();
//                        }
//                        else if (reader.Name.Equals("Conditions"))
//                        {
//                            reader.Skip();
//                        }
//                        else if (reader.Name.Equals("Subscribers"))
//                        {
//                            reader.Skip();
//                        }
//                        else if (reader.Name.Equals("Behaviors"))
//                        {
//                            reader.Skip();
//                        }
//                        else if (reader.Name.Equals("Publisher"))
//                        {
//                            var type = reader.GetAttribute("Type");
//                            reader.Skip();
//                        }
//                        else
//                        {
//                            reader.Skip();
//                        }
//                    }
//
//                    readInteractions.Add(interactionRule);
//                    reader.ReadEndElement();
//                }
//
//
//            }
//            return readInteractions;
//        }
        #endregion
    }
}