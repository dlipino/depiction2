﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Xml;
using Depiction2.API;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Depiction2.Utilities.Legacy;
using Ionic.Zip;

namespace Depiction.Legacy.Depiction14PartLoaders
{
    public class Depiction14DisplayerLoader
    {
        #region Depiction Displayer file reading
        public static IEnumerable<IElementDisplayer> GetDepictionDisplayersFromDPNFile(string fileName)
        {
            var displayers = new List<IElementDisplayer>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var displayerDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.DisplayersDir;
                var backdropFile = displayerDir + "/" + Depiction14FileOpenHelpers.BackdropFile;
                var revealerFile = displayerDir + "/" + Depiction14FileOpenHelpers.RevealersFile;

                var formatZipEntry = zipFile[backdropFile];
                if (formatZipEntry != null)
                {
                    displayers.AddRange(GetDepictionDisplayersFromStream(formatZipEntry.OpenReader()));
                }

                formatZipEntry = zipFile[revealerFile];

                if (formatZipEntry != null)
                {
                    displayers.AddRange(GetDepictionDisplayersFromStream(formatZipEntry.OpenReader()));
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return displayers;
        }
        static private List<IElementDisplayer> GetDepictionDisplayersFromStream(Stream interactionFileStream)
        {
            var readDisplayers = new List<IElementDisplayer>();
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(interactionFileStream))
            {
                reader.Read(); //Start the read process
                IElementDisplayer displayer = null;
                bool isRevealer = false;
                if (reader.Name.Equals("DepictionBackdrop") || reader.Name.Equals("DepictionRevealers"))
                {
                    if (reader.Name.Equals("DepictionRevealers"))
                    {
                        isRevealer = true;
                    }
                    reader.ReadToFollowing("DepictionElementDisplayer");
                }

                while (reader.Name.Equals("DepictionElementDisplayer") && reader.NodeType.Equals(XmlNodeType.Element))
                {
                    var id = reader.GetAttribute("DisplayerID");
                    var name = reader.GetAttribute("DisplayerName");
                    if (!isRevealer) displayer = new ElementDisplayer(id);
                    else displayer = new Revealer(name,id);
                    var displayerType = reader.GetAttribute("DisplayerType");
                    displayer.DisplayerName = name;
                    reader.ReadStartElement();

                    //Get the properties
                    while (!reader.Name.Equals("DepictionElementDisplayer"))
                    {
                        if (reader.Name.Equals("elementIDsInDisplayer"))
                        {
                            if (reader.IsEmptyElement)
                            {
                                reader.Skip();
                            }
                            else
                            {
                                reader.ReadStartElement();
                                var idList = new List<string>();
                                while (!(reader.Name.Equals("elementIDsInDisplayer") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                                {
                                    var s = reader.ReadString();
                                    reader.ReadEndElement();
                                    idList.Add(s);
                                }

                                displayer.AddDisplayElementsWithMatchingIds(idList);
                                reader.ReadEndElement();
                            }
                        }
                        else if (reader.Name.Equals("RevealerBounds"))
                        {
                            reader.ReadToDescendant("MapCoordinateBounds");
                            var t = StringToDouble(reader.GetAttribute("top"));
                            var l = StringToDouble(reader.GetAttribute("left"));
                            var b = StringToDouble(reader.GetAttribute("bottom"));
                            var r = StringToDouble(reader.GetAttribute("right"));
                            var rectList = new List<Point>
                                   {
                                       new Point(l, t),
                                       new Point(r, t),
                                       new Point(r, b),
                                       new Point(l, b),
                                       new Point(l, t)
                                   };
                            displayer.SetBounds(new Point(l, t), new Point(r, b));
                            reader.Skip();
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("RevealerProperties") && isRevealer)
                        {
                            reader.Read();

                            while (!reader.Name.Equals("RevealerProperties") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                            {
                                var attName = reader.GetAttribute("name");
                                var attVal = reader.GetAttribute("value");
                                var attValType = reader.GetAttribute("valueType");
                                var revealer = displayer as Revealer;
                                if (attName != null && attVal != null && attValType != null)
                                {
                                    if (attName.Equals("RevealerShape"))
                                    {
                                        var prop = new RevealerProperty("RevealerShape", DisplayerViewType.Circle);

                                        if (attVal.Equals("Rectangle"))
                                        {
                                            prop = new RevealerProperty("RevealerShape", DisplayerViewType.Rectangle);
                                        }
                                        revealer.AddOrReplaceProperty(prop);
                                    }
                                    else
                                    {
//                                        var type = DepictionLegacyTypeInformationSerialization.GetFullTypeFromSimpleTypeString(attValType);
                                        var type = DepictionAccess.GetTypeFromName(attValType);
                                        var prop = new RevealerProperty(attName, attVal, type);
                                        revealer.AddOrReplaceProperty(prop);
                                    }
                                }
                                reader.Read();
                            }
                            //                            <property valueType="Boolean" value="True" name="Maximized"/> 
                            //                               <property valueType="Boolean" value="True" name="BorderVisible"/> 
                            //                             <property valueType="Boolean" value="False" name="Anchored"/> 
                            //                              <property valueType="Double" value="0.5" name="Opacity"/> 
                            //                               <property valueType="RevealerShapeType" value="Rectangle" name="RevealerShape"/> 
                            //                              <property valueType="Boolean" value="True" name="TopMenu"/> 
                            //                             <property valueType="Boolean" value="True" name="BottomMenu"/> 
                            //                             <property valueType="Boolean" value="False" name="SideMenu"/> 
                            //                             <property valueType="Boolean" value="True" name="PermaTextVisible"/> 
                            //                             <property valueType="Double" value="-25" name="PermaTextX"/> 
                            //                              <property valueType="Double" value="-25" name="PermaTextY"/>
                            reader.ReadEndElement();
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }

                    readDisplayers.Add(displayer);
                    reader.ReadEndElement();
                }
            }
            return readDisplayers;
        }
        static private double StringToDouble(string sVal)
        {
            if (string.IsNullOrEmpty(sVal)) return double.NaN;
            double dVal;
            if (double.TryParse(sVal, out dVal))
            {
                return dVal;
            }
            return double.NaN;
        }
        #endregion
    }
}