﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using Depiction.Legacy.DmlLoaders;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.Utilities.Legacy;
using Ionic.Zip;

namespace Depiction.Legacy.Depiction14PartLoaders
{
    public class Depiction14ElementLibraryLoader
    {
        static public List<IElementScaffold> GetLibraryElementsFrom14Dpn(string fileName)
        {
            var scaffolds = new List<IElementScaffold>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var elementLibraryDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.LibraryDir + "/";
                var elementIconDir = elementLibraryDir + "/" + Depiction14FileOpenHelpers.LibraryIconBinDir;
                var dmlNames = zipFile.EntryFileNames.Where(t => t.StartsWith(elementLibraryDir) && t.EndsWith(".dml"));

                foreach (var dmlName in dmlNames)
                {
                    var formatZipEntry = zipFile[dmlName];
                    if (formatZipEntry != null)
                    {
                        var scaffold = Dml14ToScaffold.GetScaffoldFrom14Stream(formatZipEntry.OpenReader());
                        if(scaffold != null) scaffolds.Add(scaffold);
                    }
                }
            }

            //1.4 dpn do not story the waypoint, end, and start
            if (scaffolds.Any(t => t.ElementType.Contains("Route")))
            {
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("start")) == null)
                {
                    scaffolds.Add(CreateScaffold("start", "Route start", "Depiction.RouteStart"));
                }
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("end")) == null)
                {
                    scaffolds.Add(CreateScaffold("end", "Route end", "Depiction.RouteEnd"));
                }
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("WayPoint")) == null)
                {
                    scaffolds.Add(CreateScaffold("WayPoint", "Route way point", "Depiction.RouteWayPoint"));
                }
            }
            //Label them
            foreach(ElementScaffold scaffold in scaffolds)
            {
                scaffold.Source = ElementScaffoldLibrary.StorySource;
            }

            Thread.CurrentThread.CurrentCulture = culture;
            return scaffolds;
        }

        static public Dictionary<string, ImageSource> GetImagesFrom14DpnLibraryFile(string fileName)
        {
            var entryLocation = Depiction14FileOpenHelpers.DepictionDataDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryIconBinDir;
            return Depiction14ImageLoader.GetImagesFromZipFileDirectory(fileName, entryLocation);
        }

        static ElementScaffold CreateScaffold(string type, string displayName, string resourceName)
        {
            var elementType = type;
            var wayScaffold = new ElementScaffold(elementType)
            {
                DisplayName = displayName
            };

            var iconResourceName = resourceName;
            var iconNameProp = new PropertyScaffold()
            {
                ProperName = "IconPath",
                DisplayName = "Icon Path",
                ValueString = iconResourceName
            };
            wayScaffold.Properties.Add(iconNameProp);
            var iconSizeProp = new PropertyScaffold()
            {
                ProperName = "IconSize",
                DisplayName = "IconSize",
                TypeString = "Double",
                ValueString = "20"
            };
            wayScaffold.Properties.Add(iconSizeProp);

            var dragProp = new PropertyScaffold()
            {
                ProperName = "Draggable",
                DisplayName = "Draggable",
                ValueString = "True"
            };
            wayScaffold.Properties.Add(dragProp);

            return wayScaffold;
        }
    }
}