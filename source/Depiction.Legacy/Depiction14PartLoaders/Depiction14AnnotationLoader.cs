﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Xml;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Depiction2.Utilities.Legacy;
using Ionic.Zip;

namespace Depiction.Legacy.Depiction14PartLoaders
{
    public class Depiction14AnnotationLoader
    {
        static public List<IDepictionAnnotation> GetAnnotationsFromDPNFile(string fileName)
        {
            var readAnnotations = new List<IDepictionAnnotation>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var fullElemFileName = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.AnnotDir + "/" + Depiction14FileOpenHelpers.AnnotFile;
                var formatZipEntry = zipFile[fullElemFileName];

                if (formatZipEntry != null)
                {
                    readAnnotations = GetAnnotationsFromStream(formatZipEntry.OpenReader());
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return readAnnotations;
        }

        static private List<IDepictionAnnotation> GetAnnotationsFromStream(Stream annotationFileStream)
        {
            var readAnnotations = new List<IDepictionAnnotation>();
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(annotationFileStream))
            {
                reader.Read(); //Start the read process
                if (reader.Name.Equals("DepictionAnnotations"))
                {
                    reader.ReadToFollowing("DepictionAnnotation");
                }

                while (reader.Name.Equals("DepictionAnnotation") && reader.NodeType.Equals(XmlNodeType.Element))
                {
                    reader.ReadStartElement();

                    var annotation = new DepictionAnnotation();
                    //Get the properties
                    while (!reader.Name.Equals("DepictionAnnotation"))
                    {//optimization can happen here
                        if (reader.Name.Equals("MapLocation"))
                        {
                            reader.ReadStartElement();
                            if (reader.Name.Equals("LatitudeLongitude"))
                            {
                                double x, y;
                                
                                double.TryParse(reader.GetAttribute("longitude"), out x);
                                double.TryParse(reader.GetAttribute("latitude"), out y);
                                annotation.GeoLocation = new Point(x,y);
                                reader.Skip();
                            }
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("AnnotationText"))
                        {
                            reader.ReadStartElement();
                            annotation.AnnotationText = reader.ReadString();
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("ContentLocationX"))
                        {
                            reader.ReadStartElement();
                            annotation.ContentLocationX = double.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("ContentLocationY"))
                        {
                            reader.ReadStartElement();
                            annotation.ContentLocationY = double.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("PixelWidth"))
                        {
                            reader.ReadStartElement();
                            annotation.PixelWidth = double.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("PixelHeight"))
                        {
                            reader.ReadStartElement();
                            annotation.PixelHeight = double.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("ScaleWhenCreated"))
                        {
                            reader.ReadStartElement();
                            annotation.ScaleWhenCreated = double.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("IsTextCollapsed"))
                        {
                            reader.ReadStartElement();
                            annotation.IsTextCollapsed = bool.Parse(reader.ReadString());
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("BackgroundColor"))
                        {
                            reader.ReadStartElement();
                            annotation.BackgroundColor = reader.ReadString();
                            reader.ReadEndElement();
                        }
                        else if (reader.Name.Equals("ForegroundColor"))
                        {
                            reader.ReadStartElement();
                            annotation.ForegroundColor = reader.ReadString();
                            reader.ReadEndElement();
                        }
                        else
                        {
                            reader.Skip();
                        }
                    }

                    readAnnotations.Add(annotation);
                    reader.ReadEndElement();
                }
            }
            return readAnnotations;
        }
    }
}