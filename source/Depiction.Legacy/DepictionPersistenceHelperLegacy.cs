﻿using System;
using System.IO;
using Ionic.Zip;

namespace Depiction.Legacy
{
    public class DepictionPersistenceHelperLegacy
    {
        // THESE TWO VARIABLES MUST BE INCREMENTED TOGETHER! basically this is the 8th file format change.
        public const int CurrentDepictionFileVersion = 9;
        // The Application version in which this DepictionFileVersion was introduced.
        private const string MinimumAppVersionForDepictionFileVersion = "1.2.1";
        //Variables that should set road network/elevation changes to the state they were before the
        //save since versions less than 5 did not save the change.
        public static int OriginalVersionOfCurrentLoadedDepiction { get; private set; }
        public const int VersionsThatMustRunInteractionsAfterLoading = 5;//Don't change this one
        public const int VersionThatGavePositionToShapes = 9;//8, there was a bug somewhere which made this not work;//Don't change this one
        public const int DepictionRoadgraphWithOneWay = 9; //Or this one


        #region Zip and unzip files

        public static void PutDirectoryIntoZipFile(string fileName, string archiveDir)
        {
            using(var zip = new ZipFile())
            {
                zip.AddDirectory(archiveDir);
                zip.Save(fileName);
            }
        }

        public static void UnzipFileToDirectory(string fileName, string archiveDir)
        {
            if (Directory.Exists(archiveDir))
                Directory.Delete(archiveDir, true);
            try
            {
                using (var zip = new ZipFile(fileName))
                {
                    zip.ExtractAll(archiveDir);
                }
            }catch(Exception  )
            {
                //Grrr total bad hacks
            }
        }
        #endregion

    }
}