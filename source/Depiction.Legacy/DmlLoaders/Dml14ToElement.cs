﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using Depiction.Legacy.Types;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.API.Geo;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Base.Utilities;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Element;
using Depiction2.GeoWrapper;
using Depiction2.Utilities;
using Depiction2.Utilities.Legacy;
using DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork;

namespace Depiction.Legacy.DmlLoaders
{
    public class Dml14ToElement
    {
        //used to read the saved active element file 
        static public List<IElement> GetElementsFrom14Stream(Stream elementFileStream, string dataFile, IElementScaffoldLibrary library)
        {
            var readElements = new List<IElement>();
            int expectedElemCount = 0;
            var internalCount = 0;
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(elementFileStream))
            {
                reader.Read(); //Start the read process
                if (reader.Name.Equals("ElementList"))
                {
                    //Should only be one value, the elementCount
                    if (!int.TryParse(reader.GetAttribute(0), out expectedElemCount))
                    {
                        return readElements;
                    }
                    reader.ReadToFollowing("DepictionElement");
                }
                var elements = ReadSingleElement(reader, dataFile, library);
                readElements.AddRange(elements);

                reader.Close();
            }
            return readElements;
        }

        //This actually does just read one element, but, if the element happens to be a route, it creates
        // the start,end and waypoints.
        static public List<IElement> ReadSingleElement(XmlReader reader, string dataFile, IElementScaffoldLibrary library)
        {
            var nodeName = "DepictionElement";
            var readElements = new List<IElement>();
            while (reader.Name.Equals(nodeName) && reader.NodeType.Equals(XmlNodeType.Element))
            {
                var parentId = reader.GetAttribute("elementKey");
                var elementType = reader.GetAttribute("elementType");
                var singleElement = new DepictionElement(parentId, elementType);
                var elementProperties = new List<IElementProperty>();
                ImageMetedata14 elementImageData = null;
                var elementZOIString = string.Empty;
                singleElement.DisplayName = reader.GetAttribute("displayName");
                var startDepth = reader.Depth;
                bool usingPermaText;
                bool.TryParse(reader.GetAttribute("usingPermaText"), out usingPermaText);
                reader.ReadStartElement();
                //Get the properties
                while (!reader.Name.Equals(nodeName))
                {
                    if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        elementProperties = ReadPropertyList(reader, dataFile, singleElement);
                    }
                    else if (reader.Name.Equals("ImageMetadata"))
                    {
                        elementImageData = GetImageMetadata14(reader, startDepth);
                    }
                    else if (reader.Name.Equals("ZoneOfInfluence"))
                    {
                        reader.ReadToDescendant("geometryWkt");
                        elementZOIString = reader.ReadString();
                        //counting to the tree level would be clever way of doing this
                        //cant remember why this works, but it is designed to read until the ZoneOfInfluence
                        //end element, actually it is supposed to read past, untill the next element on the
                        //same level as the zoneofinfluence.
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("Tags"))
                    {
                        while (!(reader.Name.Equals("Tags") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                        {
                            var tag = reader.ReadElementString();
                            singleElement.AddTag(tag);
                        }
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("generateZoi"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "generateZoi");
                        singleElement.PostAddActions = zoiGenerateActions;//maybe append?
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onCreate"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "onCreate");
                        singleElement.PostAddActions = zoiGenerateActions;//maybe appened?
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onClick"))
                    {
                        var clickActions = GenericReaders.ReadElementActions(reader, "onClick");
                        singleElement.OnClickActions = clickActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("PermaText"))
                    {
                        var permaText = new DepictionPermaText();
                        reader.Read();

                        permaText.ReadXml(reader);
                        permaText.IsVisible = usingPermaText;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                        singleElement.PermaText = permaText;
                    }
                    #region Depiction 2 stuff for reading waypoitns as elements
                    else if (reader.Name.Equals("elementWaypoints"))
                    {
                        //Create the waypoint
                        while (reader.Read() && reader.Name.Equals("elementWaypoint"))
                        {
                            elementType = reader.GetAttribute("name");
                            var waypointId = string.Empty;
                            if (!string.IsNullOrEmpty(parentId))
                            {
                                waypointId = Guid.NewGuid().ToString();
                            }
                            var wayScaffold = new DepictionElement(waypointId, elementType);

                            var iconResourceName = reader.GetAttribute("IconPath");
                            var names = iconResourceName.Split(':');
                            if (names.Length > 1)
                            {
                                //For 1.4 icon resource names
                                iconResourceName = names[1];
                            }
                            wayScaffold.IconResourceName = iconResourceName;
                            var iconSizeString = reader.GetAttribute("IconSize");
                            double iconSize;
                            if (iconSizeString == null) iconSize = 20;
                            else
                            {
                                double.TryParse(iconSizeString, out iconSize);
                            }
                            wayScaffold.IconSize = iconSize;

                            var position = reader.GetAttribute("Location");
                            if (!string.IsNullOrEmpty(position))
                            {
                                double lat, lon;
                                string message;
                                LatLongParserUtility.ParseForLatLong(position, out lat, out lon, out message);
                                var p = new Point(lon, lat);
                                wayScaffold.UpdatePrimaryPointAndGeometry(p, null);
                            }
                            wayScaffold.IsDraggable = true;
                            wayScaffold.VisualState = ElementVisualSetting.Icon;
                            readElements.Add(wayScaffold);
                            singleElement.AttachElement(wayScaffold);
                        }
                        reader.ReadEndElement();
                    }
                    #endregion
                    else
                    {
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();
                //We got all the info we need about the element
                //Update the geometry
                SetGeometry(singleElement, elementImageData, elementZOIString, elementProperties);
                IElementScaffold scaffold = null;
                if (library != null)
                {
                    scaffold = library.FindScaffold(singleElement.ElementType);
                }
                ElementAndScaffoldService.UpdateElementWithProperties(singleElement, elementProperties, scaffold);

                //this will generally happen in 1.4 loads, the icon state should be set by
                //the visual property holder thing.
                if (!string.IsNullOrEmpty(singleElement.ImageResourceName))
                {
                    singleElement.VisualState = singleElement.VisualState | ElementVisualSetting.Image;
                }
                else
                {
                    if (!singleElement.ElementGeometry.GeometryType.Equals(DepictionGeometryType.Point))
                    {
                        singleElement.VisualState = singleElement.VisualState | ElementVisualSetting.Geometry;
                    }
                    else
                    {
                        if (singleElement.VisualState == ElementVisualSetting.None)
                        {
                            singleElement.VisualState = ElementVisualSetting.Icon;
                        }
                    }
                }

                readElements.Insert(0, singleElement);//insert because, well, no real reason i guess, just to make the new element come before
                //waypoints
            }
            return readElements;
        }

        #region property reading helper
        static public List<IElementProperty> ReadPropertyList(XmlReader reader, string fileSourceName, IElement owningElement)
        {
            if (!reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase)) return null;
            var outList = new List<IElementProperty>();
            while (reader.Read() && reader.Name.Equals("property"))
            {
                var propDisplayName = reader.GetAttribute("displayName");
                var propName = reader.GetAttribute("name");
                var isHoverText = reader.GetAttribute("isHoverText");
                if (!string.IsNullOrEmpty(isHoverText))
                {
                    if (isHoverText.Contains("T"))
                    {
                        owningElement.AddHoverTextProperty(propName,false);
                    }
                }

                if (string.IsNullOrEmpty(propName)) propName = "Generic";//This should actually be considered fail
                TempElementProperty elementProperty = null;

                var propertyTypeString = reader.GetAttribute("typeName");
                var propertyType = DepictionAccess.GetTypeFromName(propertyTypeString);
                //As this is from 1.4 based elements need to do some converting of types
                var valueString = reader.GetAttribute("value");
                if (!string.IsNullOrEmpty(valueString))
                {
                    //minor fixes for 1.4 dpns
                    if (propName.Equals("IconPath"))
                    {
                        var names = valueString.Split(':');
                        if (names.Length > 1)
                        {
                            //For 1.4 icon resource names
                            valueString = names[1];
                        }
                    }
                }
                if (propertyType == null)
                {
                    elementProperty = new TempElementProperty(propName,propDisplayName, valueString);
                }
                else
                {
                    if (valueString != null)
                    {
                        var value = DepictionTypeConverter.ChangeType(valueString, propertyType);

                        elementProperty = new TempElementProperty(propName, propDisplayName, value);
                    }
                    else
                    {

                        elementProperty = new TempElementProperty(propName, propertyType) { DisplayName = propDisplayName };
                    }
                }
                var postSetActions = new Dictionary<string, string[]>();
                var validationRules = new IValidationRule[0];
                //The non-empty property should happen when a property has post set actions
                //the value should only exists for roadgraph and terrain
                #region special properties
                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    while (!reader.Name.Equals("property") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                    {
                        if (reader.Name.Equals("value"))
                        {
                            var oldFileLocation = DepictionAccess.LastDpnFileLocation;
                            if (propName.Equals("RoadGraph") && !string.IsNullOrEmpty(fileSourceName))
                            {
                                DepictionAccess.LastDpnFileLocation = fileSourceName;
                            }
                            reader.Read();
                            elementProperty.SetValue(Depiction14FileOpenHelpers.DeserializeObject(propertyType, reader));
                            reader.ReadEndElement();
                            DepictionAccess.LastDpnFileLocation = oldFileLocation;
                        }
                        //1.4
                        if (reader.Name.Equals("restoreValue"))//INcomplete
                        {
                        }

                        if (reader.Name.Equals("postSetActions"))
                        {
                            postSetActions = GenericReaders.ReadElementActions(reader, "postSetActions");
                            elementProperty.LegacyPostSetActions = postSetActions;
                        }
                        if (reader.Name.Equals("validationRules"))
                        {
                            validationRules = GenericReaders.ReadValidationRules(reader, DepictionAccess.NameTypeService, propertyTypeString).ToArray();
                            elementProperty.LegacyValidationRules = validationRules;
                        }
                    }
                    //got to set the value of the property
                }
                #endregion
                outList.Add(elementProperty);
            }

            reader.ReadEndElement();
            return outList;
        }
        #endregion

        #region imagedata from 14
        private static void SetGeometry(DepictionElement element, ImageMetedata14 imageMetadata, string geometryString, IEnumerable<IElementProperty> properties)
        {
            if (imageMetadata != null)
            {
                var imageBounds = imageMetadata.Bounds;
                if (!imageBounds.IsEmpty && imageBounds.IsValid)
                {
                    var geoString = string.Format("Polygon (({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", imageBounds.Left, imageBounds.Top, imageBounds.Right, imageBounds.Bottom);
                    element.SetGeometryFromString(geoString);
                }
                element.ImageResourceName = imageMetadata.ImageName;
                element.ImageRotation = imageMetadata.Rotation;
            }
            else
            {
                element.ElementGeometry = new GeometryGdalWrap(geometryString);
                //Hack for roadnetwork
                if (string.IsNullOrEmpty(geometryString))
                {
                    var roadGraph = properties.FirstOrDefault(t => t.ProperName.Equals("RoadGraph"));
                    if (roadGraph != null)
                    {
                        //make a complex geometry interface in base.
                        element.ElementGeometry = new GeometryGdalWrap(((RoadGraph)(roadGraph.Value)).ConvertToMultilinewkt());
                    }
                }
            }
        }

        static private ImageMetedata14 GetImageMetadata14(XmlReader reader, int startDepth)
        {
            if (!reader.Name.Equals("ImageMetadata")) return null;
            var geoImage = new ImageMetedata14();
            reader.ReadToDescendant("DepictionImageMetadata");
            reader.ReadStartElement();
            double latTL = double.NaN, longTL = double.NaN, latBR = double.NaN, longBR = double.NaN;

            while (
                !(reader.Name.Equals("DepictionImageMetadata") &&
                  reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
                if (reader.Name.Equals("ImageFilename"))
                {
                    geoImage.ImageName = reader.ReadString();
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("TopLeftLatitude"))
                {
                    double.TryParse(reader.ReadString(), out latTL);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("TopLeftLongitude"))
                {
                    double.TryParse(reader.ReadString(), out longTL);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("BottomRightLatitude"))
                {
                    double.TryParse(reader.ReadString(), out latBR);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("BottomRightLongitude"))
                {
                    double.TryParse(reader.ReadString(), out longBR);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("Rotation"))
                {
                    double rotation;
                    double.TryParse(reader.ReadString(), out rotation);
                    geoImage.Rotation = rotation;
                    reader.ReadEndElement();
                }
                else
                {
                    reader.Skip();
                }
            }
            geoImage.Bounds = new CartRect(new Point(longTL, latTL), new Point(longBR, latBR));
            while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.ReadEndElement();
            }
            return geoImage;
        }
        #endregion
    }
}