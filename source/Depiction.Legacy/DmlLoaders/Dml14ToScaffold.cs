﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Depiction2.API;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.Utilities.Legacy;

namespace Depiction.Legacy.DmlLoaders
{
    public class Dml14ToScaffold
    {
        static public List<IElementScaffold> GetAllScaffoldsFromDirectory14(string directory)
        {
            var prototypes = new List<IElementScaffold>();
            if (!Directory.Exists(directory)) return prototypes;
            var fileList = Directory.GetFiles(directory, "*.dml");
            foreach (var fileName in fileList)
            {
                var scaffold = GetScaffoldFrom14DmlFile(fileName);

                if (scaffold != null)
                {
                    prototypes.Add(scaffold);
                }
            }
            return prototypes;
        }

        static public IElementScaffold GetScaffoldFrom14DmlFile(string fileName)
        {
            IElementScaffold scaffold = null;
            try
            {
                using (var dmlFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    scaffold = GetScaffoldFrom14Stream(dmlFileStream);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = string.Format("{0} is not a valid dml file", Path.GetFileName(fileName));
                //                 DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 10);
                return null;
            }
            return scaffold;
        }
        static public IElementScaffold GetScaffoldFrom14Stream(Stream sintleElementStream)
        {
            IElementScaffold scaffold = null;
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(sintleElementStream))
            {
                reader.MoveToContent(); //Not sure why this has to be here
                scaffold = GetScaffoldFrom14XmlReader(reader);
                reader.Close();
            }
            return scaffold;
        }

        static private IElementScaffold GetScaffoldFrom14XmlReader(XmlReader reader)
        {
            var nodeName = "depictionElement";
            ElementScaffold elemScaffold = null;
            while (reader.Name.Equals(nodeName,StringComparison.InvariantCultureIgnoreCase) && reader.NodeType.Equals(XmlNodeType.Element))
            {
                var parentId = reader.GetAttribute("elementKey");
                if (!string.IsNullOrEmpty(parentId))
                {
                    //Do something because scaffold are not supposed to have ids
                }
                elemScaffold = new ElementScaffold(reader.GetAttribute("elementType"));
               
                elemScaffold.DisplayName = reader.GetAttribute("displayName");
                var startDepth = reader.Depth;
                bool usingPermaText;
                bool.TryParse(reader.GetAttribute("usingPermaText"), out usingPermaText);
                reader.ReadStartElement();
                //Get the properties
                while (!reader.Name.Equals(nodeName,StringComparison.InvariantCultureIgnoreCase))
                {
                    if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        elemScaffold.Properties = CreatePropertyScaffoldListFrom14Properties(reader);
                    }
                    else if (reader.Name.Equals("ImageMetadata"))
                    {
                        reader.Skip();//As far as i know there should be any image metadata
                    }
                    else if (reader.Name.Equals("ZoneOfInfluence"))
                    {
                        reader.Skip();//tehre should also be no geometry, as far as i know
                    }
                    else if (reader.Name.Equals("Tags"))
                    {
                        while (!(reader.Name.Equals("Tags") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                        {
                            var tag = reader.ReadElementString();
                            elemScaffold.Tags.Add(tag);
                        }
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("generateZoi"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "generateZoi");
                        elemScaffold.OnCreateActions = zoiGenerateActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onCreate"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "onCreate");
                        elemScaffold.OnCreateActions = zoiGenerateActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onClick"))
                    {
                        var clickActions = GenericReaders.ReadElementActions(reader, "onClick");
                        elemScaffold.OnClickActions = clickActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("PermaText"))
                    {
                        reader.Skip();
                    }
                    else if (reader.Name.Equals("elementWaypoints"))
                    {
                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();
            }
            //something should happen it null
            return elemScaffold;
        }


        static private List<IPropertyScaffold> CreatePropertyScaffoldListFrom14Properties(XmlReader reader)
        {
            if (!reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase)) return null;
            var propertyList = new List<IPropertyScaffold>();
            while (reader.Read() && reader.Name.Equals("property"))
            {
                var propDisplayName = reader.GetAttribute("displayName");
                var propName = reader.GetAttribute("name");
                if (string.IsNullOrEmpty(propName)) propName = "Generic";
                var valueString = reader.GetAttribute("value");
                var propertyTypeString = reader.GetAttribute("typeName");
                var propScaffold = new PropertyScaffold
                {
                    ProperName = propName,
                    DisplayName = propDisplayName,
                    TypeString = propertyTypeString
                };
                if (!string.IsNullOrEmpty(valueString))
                {
                    //minor fixes for 1.4 dpns
                    if (propName.Equals("IconPath"))
                    {
                        var names = valueString.Split(':');
                        if (names.Length > 1)
                        {
                            //For 1.4 icon resource names
                            valueString = names[1];
                        }
                    }

                    propScaffold.ValueString = valueString;
                }
                var postSetActions = new Dictionary<string, string[]>();
//                var validationRules = new List<KeyValuePair<string, KeyValuePair<string, string>[]>>(); 
                //The non-empty property should happen when a property has post set actions
                //the value should only exists for roadgraph and terrain
                #region special properties
                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    while (!reader.Name.Equals("property") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                    {
                        if (reader.Name.Equals("value"))
                        {
                            if (propName != null)
                            {
                                propScaffold.ValueString = reader.ReadInnerXml();
                                propScaffold.IsValueInXml = true;
                            }
                            reader.ReadEndElement();
                        }
                        else
                        {
                            propScaffold.ValueString = valueString;
                        }
                        //1.4
                        if (reader.Name.Equals("restoreValue"))//INcomplete
                        {
                            //                                        reader.ReadStartElement("restoreValue");
                            //                                        restoreValue = SerializationService.DeserializeObject(propertyType, reader);
                            //                                        reader.ReadEndElement();
                        }

                        if (reader.Name.Equals("postSetActions"))
                        {
                            postSetActions = GenericReaders.ReadElementActions(reader, "postSetActions");
                            propScaffold.LegacyPostSetActions = postSetActions;
                        }
                        if (reader.Name.Equals("validationRules"))
                        {
//                            propScaffold.LegacyValidationRuleBase = GenericReaders.ReadValidationRuleAsScaffold(reader);
                            propScaffold.LegacyValidationRules = GenericReaders.ReadValidationRules(reader,
                                                                                                    DepictionAccess.NameTypeService,
                                                                                                    propertyTypeString).ToArray();
                        }
                    }
                    //got to set the value of the property
                }
                #endregion
                propertyList.Add(propScaffold);
            }

            reader.ReadEndElement();
            return propertyList;
        }
    }
}