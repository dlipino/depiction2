﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using Depiction2.API.Converters;
using Depiction2.Base.Service;
using Depiction2.Base.ValidationRules;

namespace Depiction.Legacy.DmlLoaders
{
    public class GenericReaders
    {
        #region validation rule reading
        public static IEnumerable<ValidationRuleScaffold> ReadValidationRuleAsScaffold(XmlReader reader)
        {
            var ruleList = new List<ValidationRuleScaffold>();
            if (!reader.Name.Equals("validationRules")) return ruleList;
            reader.ReadStartElement("validationRules");
            while (reader.Name.Equals("validationRule"))
            {
                if (reader.IsEmptyElement) continue;
                var ruleScaffold = new ValidationRuleScaffold();
                var paramList = new List<KeyValuePair<string, string>>();
                ruleScaffold.ValidationRuleName = reader.GetAttribute("type");
                reader.ReadStartElement("validationRule");
                if (reader.Name.Equals("parameters"))
                {
                    if (reader.IsEmptyElement)
                    {
                        reader.Read();
                    }
                    else
                    {
                        reader.ReadStartElement("parameters");
                        while (reader.Name.Equals("parameter"))
                        {
                            var paramValue = reader.GetAttribute("value");
                            var paramType = reader.GetAttribute("type");
                            paramList.Add(new KeyValuePair<string, string>(paramValue, paramType));

                            reader.Read();
                        }
                        reader.ReadEndElement();
                    }
                }
                ruleScaffold.Parameters = paramList.ToArray();
                ruleList.Add(ruleScaffold);
                reader.ReadEndElement();
            }
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("validationRules", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra");
                reader.Read();
            }
            reader.ReadEndElement();
            return ruleList;
        }

        //Not sure what the simpleExpected type thing is for
        public static List<IValidationRule> ReadValidationRules(XmlReader reader, ISimpleNameTypeService typeConverterService, string simpleExpectedType)
        {
           if(typeConverterService == null)
           {
               throw new Exception("Cannot create a validation rule without a big type converter");
           }
           if (string.IsNullOrEmpty(simpleExpectedType)) simpleExpectedType = "String";

            var validationRules = new List<IValidationRule>();
            if (!reader.Name.Equals("validationRules")) return validationRules;
            reader.ReadStartElement("validationRules");
            while (reader.Name.Equals("validationRule"))
            {
                if (!reader.IsEmptyElement)
                {
                    var parameters = new List<object>();
                    var readRuleType = reader.GetAttribute("type");

                    //Exception catching done in the methods, pretty sure it is not a good ida
                    Type ruleType = typeConverterService.GetTypeFromName(readRuleType);

                    if (ruleType == null)
                    {
                        throw new Exception(string.Format("Unable to locate the desired type:"));
                    }
                    reader.ReadStartElement("validationRule");
                    if (reader.Name.Equals("parameters"))
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.Read();
                        }
                        else
                        {
                            reader.ReadStartElement("parameters");
                            while (reader.Name.Equals("parameter"))
                            {
                                object value = reader.GetAttribute("value");
                                var typeString = reader.GetAttribute("type");

                                //Fix for a bug in 1.3.1.11445, not sure what the bug was
                                if (typeString.Equals("ValidationRuleParameter"))
                                {
                                    try
                                    {
                                        //Create an actual type (if it is a real type name)
                                        var type = Type.GetType(value.ToString());
                                        //if it gets this far it is a runtime type
                                        if (type != null)
                                        {
                                            typeString = "RuntimeType";
                                        }
                                        else
                                        {
                                            //We know the second non runtime type of datatypevalidation rule is a string and not the type of
                                            //the property it is checking
                                            if (readRuleType.Equals("DataTypeValidationRule"))
                                            {
                                                typeString = "String";
                                            }
                                            else
                                            {
                                                typeString = simpleExpectedType;
                                            }
                                        }
                                        //Actually the above is kind of redundent with what happens on a runtimetype
                                        //but that is mostly due to legacy code
                                    }
                                    catch { }
                                }

                                if (typeString.Contains("RuntimeType") && value != null)
                                {
                                    //Used for type validator
                                    var valString = value.ToString();
                                    var result = typeConverterService.GetTypeFromName(valString);
                                    parameters.Add(result);
                                }
                                else
                                {
                                    var type = typeConverterService.GetTypeFromName(typeString);
                                    if (type != null)
                                    {
                                        value = DepictionTypeConverter.ChangeType(value, type);
                                        parameters.Add(value);
                                    }
                                }
                                reader.Read();
                            }
                            reader.ReadEndElement();
                        }
                    }
                    var rule = (IValidationRule)Activator.CreateInstance(ruleType, parameters.ToArray());
                    if (rule.IsRuleValid) validationRules.Add(rule);
                }
                reader.ReadEndElement();
            }

            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("validationRules", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra");
                reader.Read();
            }
            reader.ReadEndElement();
            return validationRules;
        }
        #endregion

        #region action reading
        public static Dictionary<string, string[]> ReadElementActions(XmlReader reader, string actionElementName)
        {
            if (!reader.Name.Equals(actionElementName, StringComparison.InvariantCultureIgnoreCase))
                return new Dictionary<string, string[]>();

            var dictionary = new Dictionary<string, string[]>();
            var nodeName = reader.Name;

            reader.ReadStartElement(nodeName);
            if (reader.Name.Equals("actions"))
            {
                reader.Read();
                while (reader.Name.Equals("action"))
                {
                    var keyValue = ReadActionKeyValuePair(reader);
                    if (keyValue.Key != null)
                    {
                        dictionary.Add(keyValue.Key, keyValue.Value);
                    }
                }
                reader.ReadEndElement();
            }
            else if (reader.Name.Equals("action"))
            {
                while (reader.Name.Equals("action"))
                {
                    var keyValue = ReadActionKeyValuePair(reader);
                    if (keyValue.Key != null)
                    {
                        dictionary.Add(keyValue.Key, keyValue.Value);
                    }
                }
            }
            reader.ReadEndElement();
            return dictionary;
        }

        private static KeyValuePair<string, string[]> ReadActionKeyValuePair(XmlReader reader)
        {
            if (!reader.Name.Equals("action")) return new KeyValuePair<string, string[]>();
            var key = reader.GetAttribute("name");
            if (reader.IsEmptyElement)
            {
                reader.Read();
                return new KeyValuePair<string, string[]>(key, new string[0]);
            }
            reader.Read();
            var parameters = new List<string>();
            if (reader.Name.Equals("parameters"))
            {
                if (reader.IsEmptyElement)
                {
                    reader.Read();
                }
                else
                {
                    reader.Read();
                    while (reader.Name.Equals("parameter"))
                    {
                        var query = reader.GetAttribute("elementQuery");
                        if (query != null)
                        {
                            parameters.Add(query);
                        }
                        reader.Read();
                    }
                    reader.Read();

                }
            }
            reader.ReadEndElement();
            if (key == null) return new KeyValuePair<string, string[]>();
            return new KeyValuePair<string, string[]>(key, parameters.ToArray());

        }
        #endregion
    }
}