﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Depiction.Legacy.Depiction14PartLoaders;
using Depiction.Legacy.DmlLoaders;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.Interactions;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.Utilities.Legacy;

namespace Depiction.Legacy
{
//    [DepictionStoryLoaderMetadata]
    public class Depiction14StoryLoader : IDepictionStoryLoader
    {
        public IStory GetStoryFromFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return null;
            var fileInfo = Depiction14DpnInformationLoader.GetFileInformationFromDPNFile(fileName);
            if (string.IsNullOrEmpty(fileInfo.AppVersion)) return null;

            var scaffoldsFromDpn = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(fileName);
            IElementScaffoldLibrary libraryToUse = null; 
            //Should only happen in tests, but it is still bad form to have this occur
            //maybe there should be another method to grab the library or add it to the
            //global one.
            if (DepictionAccess.ScaffoldLibrary != null)
            {
                DepictionAccess.ScaffoldLibrary.AddStoryScaffolds(scaffoldsFromDpn);
                libraryToUse = DepictionAccess.ScaffoldLibrary;
            }else
            {
                libraryToUse =  new ElementScaffoldLibrary();
                libraryToUse.AddStoryScaffolds(scaffoldsFromDpn);
            }

            var elements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fileName, libraryToUse);
            var geoInformation = Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(fileName);
            var annotations = Depiction14AnnotationLoader.GetAnnotationsFromDPNFile(fileName);
            var displayers = Depiction14DisplayerLoader.GetDepictionDisplayersFromDPNFile(fileName).ToList();
            var interactionRepo = Depiction14InteractionRuleLoader.GetInteractionsFromDPNFile(fileName);

            var story = new Story();
            story.LoadElements(elements);
            story.AddAnnotations(annotations);
            //SetGeoInformation is something from legacy and is what is used
            //to determin the zoom, i don't think it is ever updated.
            story.SetGeoInformation(geoInformation);
            story.SetInteractionRules(interactionRepo);
            
            //The elements to display are set duing the save process
            if (displayers.Count > 0)
            {
                story.MainDisplayer = displayers[0];
                story.MainDisplayer.ConnectToStory(story);
                var revList = displayers.GetRange(1, displayers.Count - 1);

                foreach (var dis in revList)
                {
                    var rev = dis as IElementRevealer;
                    if (rev != null)
                    {
                        story.LoadRevealer(rev);
                        rev.ConnectToStory(story);
                    }
                }
            }
            
            var imageResourceDictionary = Depiction14ImageLoader.GetDepictionImageNamesFromDPNFile(fileName);
            var savedElementTypeIcons = Depiction14ElementLibraryLoader.GetImagesFrom14DpnLibraryFile(fileName);

            var missingIcons = FindNondefaultImages(savedElementTypeIcons);
            story.SetStoryImages(imageResourceDictionary);
            story.AllImages.AppendImages(missingIcons);

            //Hack a way to get the center and zoom
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                var viewWidth = Application.Current.MainWindow.ActualWidth;
                var storyWidth = story.VisibleExtent.Width;
                var scale = viewWidth / storyWidth;
                var center = story.VisibleExtent.Center;
                //   cheat a bit since we know the scale is 1
                story.AdjustZoom(scale, center);
            }
            return story;
        }
        public List<IElementScaffold> GetScaffoldsFromDir(string dirName)
        {
            return Dml14ToScaffold.GetAllScaffoldsFromDirectory14(dirName);
        }

        #region interaction rules loading for 1.4

        public IList<IInteractionRule> GetInteractionRulesFromFile(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                return LoadRulesFromXmlStream(stream);
            }
        }
        public IList<IInteractionRule> GetInteractionRulesFromDirectory(string path)
        {
            var rules = new List<IInteractionRule>();
            var files = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var interactionRules = GetInteractionRulesFromFile(file);
                if (interactionRules != null)
                    rules.AddRange(interactionRules);
            }

            return rules;
        }

        private IList<IInteractionRule> LoadRulesFromXmlStream(Stream stream)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var reader = Depiction14FileOpenHelpers.GetXmlReader(stream))
                {

                    return Depiction14FileOpenHelpers.DeserializeItemList<IInteractionRule>("InteractionRules", typeof(InteractionRule), reader);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }
        #endregion
        private Dictionary<string, ImageSource> FindNondefaultImages(Dictionary<string, ImageSource> loadedImages)
        {
            var defaultKeys = DepictionAccess.DefaultIconDictionary.Keys.ToList();
            var loadedKeys = loadedImages.Keys.ToList();
            var nonDefaultIcons = loadedKeys.Except(defaultKeys);
            var leftOver = new Dictionary<string, ImageSource>();
            foreach (var iconName in nonDefaultIcons)
            {
                leftOver.Add(iconName, loadedImages[iconName]);
            }
            return leftOver;

        }

        public void Dispose()
        {
            Debug.WriteLine("Done with the story loader");
        }
    }
}

//        public const string mercatorProjectedCoordinateSystem14 = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
//                                                         "GEOGCS[\"Popular Visualisation CRS\",  " +
//                                                             "DATUM[\"WGS84\",    " +
//                                                                 "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
//                                                             "AUTHORITY[\"EPSG\",\"6055\"]], " +
//                                                        "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
//                                                        "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
//                                                        "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
//                                                    "PROJECTION[\"Mercator\"]," +
//                                                    "PARAMETER[\"semi_minor\",6378137]," +
//                                                    "PARAMETER[\"False_Easting\", 0]," +
//                                                    "PARAMETER[\"False_Northing\", 0]," +
//                                                    "PARAMETER[\"Central_Meridian\", 0]," +
//                                                    "PARAMETER[\"Latitude_of_origin\", 0]," +
//                                                    "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
//                                                    "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
//                                                    "AUTHORITY[\"EPSG\",\"3785\"]]";
