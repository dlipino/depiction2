﻿using System.Configuration;
using Depiction.LiveReports.EmailConfiguration;

namespace Depiction2.LiveReports.Setting14
{
    public class IncomingEmailAccountRecords : ConfigurationElementCollection
    {
        public IncomingEmailSettings this[int index]
        {
            get { return BaseGet(index) as IncomingEmailSettings; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new IncomingEmailSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((IncomingEmailSettings)element).UserName + ((IncomingEmailSettings)element).Pop3Server;
        }
        public void AddOrReplaceExisting(IncomingEmailSettings emailAccount)
        {
            try
            {
                var index = BaseIndexOf(emailAccount);
                if (index != -1)
                {
                    this[index] = emailAccount;
                }
                else
                {
                    BaseAdd(emailAccount, true);
                }
            }
            catch
            {//Hack for sure
                var index = BaseIndexOf(emailAccount);
                this[index] = emailAccount;
            }
        }
        public void Remove(IncomingEmailSettings emailAccount)
        {
            var index = BaseIndexOf(emailAccount);
            if (index >= 0)
            {
                BaseRemove(emailAccount);
            }
        }
//        public void Add(IncomingEmailSettings EmailAccount)
//        {
//            BaseAdd(EmailAccount);
//        }

        public void Insert(IncomingEmailSettings emailAccount, int index)
        {
            BaseAdd(index, emailAccount);
        }
    }
}