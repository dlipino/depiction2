using System.Configuration;
using Depiction.LiveReports.EmailConfiguration;

namespace Depiction2.LiveReports.Setting14
{
    public class IncomingEmailSettings : BasicEmailSettings
    {
        [ConfigurationProperty("Pop3Server", DefaultValue = "", IsRequired = true)]
        public string Pop3Server
        {
            get { return (string)this["Pop3Server"]; }
            set { this["Pop3Server"] = value; }
        }
        [ConfigurationProperty("PortNumber", DefaultValue = "Default", IsRequired = true)]
        public string PortNumber
        {
            get { return (string)this["PortNumber"]; }
            set { this["PortNumber"] = value; }
        }

        public bool IsNewRecord { get; set; }

        public string NewRecordName { get; set; }

        public bool ShowAdvancedFields { get; set; }

        public string SuggestedDomain { get; set; }

        public string ExplanatoryText { get; set; }

        public IncomingEmailSettings()
        {
            IsNewRecord = false;
            ShowAdvancedFields = false;
        }

        public IncomingEmailSettings(string userName, string pop3Server, bool useSSL, string rawTags)
            : this()
        {
            UserName = userName;
            Pop3Server = pop3Server;
            UseSSL = useSSL;
            DepictionTags = rawTags;
        }

    }
}