using System;
using System.Configuration;
using Depiction.LiveReports.EmailConfiguration;
using Depiction2.LiveReports.Setting14;

namespace Depiction.LiveReports.EmailConfiguration
{
    public class OutgoingEmailSettings : BasicEmailSettings
    {
        public string Password { get; set; }
        public bool IsNew { get; set; }

        [ConfigurationProperty("ToAddress", DefaultValue = "", IsRequired = true)]
        public string ToAddress
        {
            get { return (string)this["ToAddress"]; }
            set { this["ToAddress"] = value; }
        }

        [ConfigurationProperty("AccountName", DefaultValue = "", IsRequired = true)]
        public string AccountName
        {
            get { return (string)this["AccountName"]; }
            set { this["AccountName"] = value; }
        }
        [ConfigurationProperty("FromAddress", DefaultValue = "", IsRequired = true)]
        public string FromAddress
        {
            get { return (string)this["FromAddress"]; }
            set { this["FromAddress"] = value; }
        }

        [ConfigurationProperty("SMTPServer", DefaultValue = "", IsRequired = true)]
        public string SMTPServer
        {
            get { return (string)this["SMTPServer"]; }
            set { this["SMTPServer"] = value; }
        }

        [ConfigurationProperty("UseAuthentication", DefaultValue = true, IsRequired = true)]
        public bool UseAuthentication
        {
            get { return (bool)this["UseAuthentication"]; }
            set { this["UseAuthentication"] = value; }
        }
        [ConfigurationProperty("PortNumber", DefaultValue = 25, IsRequired = true)]
        public int PortNumber
        {
            get { return (int)this["PortNumber"]; }
            set { this["PortNumber"] = value; }
        }
        [ConfigurationProperty("Identifier", DefaultValue = "", IsRequired = false, IsKey = true)]
        public string Identifier
        {
            get { return (string)this["Identifier"]; }
            set { this["Identifier"] = value; }
        }
        #region Constructors

        public OutgoingEmailSettings(bool isNew)
        {
            IsNew = isNew;
        }
        public OutgoingEmailSettings():this(false)
        {
            if(string.IsNullOrEmpty(Identifier)) Identifier = Guid.NewGuid().ToString();
        }
        #endregion

        public bool IsAllInformationPresent()
        {
            if (string.IsNullOrEmpty(ToAddress)) return false;
            if (string.IsNullOrEmpty(FromAddress)) return false;
            if (string.IsNullOrEmpty(SMTPServer)) return false;
            if (PortNumber <= 0) return false;
            if (UseAuthentication)
            {
                if (string.IsNullOrEmpty(UserName)) return false;
            }
            //            if (string.IsNullOrEmpty(Password)) return false;
            return true;
        }

        public OutgoingEmailSettings CopySettings()
        {
            var setting = new OutgoingEmailSettings(false);
            setting.AccountName = AccountName;
            setting.FromAddress = FromAddress;
            setting.ToAddress = ToAddress;
            setting.Password = Password;
            setting.PortNumber = PortNumber;
            setting.SMTPServer = SMTPServer;
            setting.UseAuthentication = UseAuthentication;
            setting.UseSSL = UseSSL;
            setting.DepictionTags = DepictionTags;

            return setting;
        }
    }
}