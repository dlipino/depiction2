using System.Configuration;

namespace Depiction2.LiveReports.Setting14
{
    abstract public class BasicEmailSettings : ConfigurationElement
    {
        [ConfigurationProperty("UserName", DefaultValue = "", IsRequired = true)]
        public string UserName
        {
            get { return (string)this["UserName"]; }
            set { this["UserName"] = value; }
        }

        [ConfigurationProperty("UseSSL", DefaultValue = false, IsRequired = true)]
        public bool UseSSL
        {
            get { return (bool)this["UseSSL"]; }
            set { this["UseSSL"] = value; }
        }
        [ConfigurationProperty("DepictionTags", DefaultValue = "", IsRequired = false)]
        public string DepictionTags
        {
            get { return (string)this["DepictionTags"]; }
            set { this["DepictionTags"] = value; }
        }

    }
}