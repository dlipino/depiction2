using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.LiveReports.Service14;
using Depiction2.LiveReports.ViewModels;
using Depiction2.LiveReports.Views;

namespace Depiction2.LiveReports
{
    [SourceLoaderMetadata(Name = "LiveReportSource", DisplayName = "Depiction's Live Report importer", Author = "Depiction Inc.",DisplayConfigInAddDialog = true,
        ShortName = "Live Reports")]
    public class LiveReportImporter : ISourceLoaderExtension
    {
        //Awkward. I don't think this VM can be used
        private LiveReportSettupVM liveReportVM;
        LiveReportsFullView addinView = new LiveReportsFullView();

        public object ElementAddConfigView
        {
            get
            {
                if (addinView == null) return null;
                addinView.DataContext = liveReportVM;
                return addinView;
            }
        }

        public LiveReportImporter()
        {
            liveReportVM = new LiveReportSettupVM(this);
        }

        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName)
        {
            var sourceInfo = liveReportVM.CreateLiveReportInfo();
            StartLiveReportReader(sourceInfo);
        }
        public void StartLiveReportReader(LiveReportSourceInfo sourceInfo)
        {
            var readerModel = new DepictionEmailReaderBackgroundService(sourceInfo);
            double minuteRefresh;
            if (double.TryParse(liveReportVM.RefreshInterval, out minuteRefresh))
            {
                readerModel.RefreshRateMinutes = minuteRefresh;
            }

            if (!readerModel.IsValidEmailAddressPassword()) return;
            liveReportVM.UpdateIncomingAccounts();
            var name = string.Format("Email: {0}", sourceInfo.CompleteUserName);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(readerModel);
            readerModel.UpdateStatusReport(name);
            readerModel.StartBackgroundService(readerModel);
        }
        public void FakeStart()
        {
            var readerModel = new DepictionEmailReaderBackgroundService(true);
            double minuteRefresh;
            if (double.TryParse(liveReportVM.RefreshInterval, out minuteRefresh))
            {
                readerModel.RefreshRateMinutes = minuteRefresh;
            }

         
            var name = string.Format("Email: Checking the timer");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(readerModel);
            readerModel.UpdateStatusReport(name);
            readerModel.StartBackgroundService(readerModel); 
        }
        public void Dispose()
        {
            liveReportVM.Dispose();
        }
    }
}