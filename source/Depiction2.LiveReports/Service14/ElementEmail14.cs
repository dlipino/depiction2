using System;
using System.Collections.Generic;
using System.Globalization;
using Depiction2.Base.StoryEntities;

namespace Depiction2.LiveReports.Service14
{
    /// <summary>
    /// A service to send elements to an email account.
    /// Useful for communication among depictions via live reports.
    /// </summary>
    public class ElementEmail14
    {
        private readonly string subject;
        private string body;
        List<string> propertiesToIgnore = new List<string> { "emailsubject", "emailbody", "emailfrom", "emailto", "position", "bordercolor" };

        public string Subject
        {
            get { return subject; }
        }

        public string Body
        {
            get { return body; }
            set { body = value; }
        }

//        public ElementEmail14(IElement element)
//        {
//            ElementSynchronizationService.PrepareElementForSynchronization(element);
//            subject = string.Format("{0}: ,{1}", element.ElementType, element.Position.ToXmlSaveString());
//            body = string.Empty;
//            body = PackElementPropertiesIntoBody(body, element.OrderedCustomProperties, false);
//            body += "ZoneOfInfluence: " + element.ElementGeometry.WktGeometry;
//            foreach (var waypoint in element.Waypoints)
//            {
//                body += "\nElementWaypoint:\n";
//                body += "Name:" + waypoint.Name + "\n";
//                body += "IconPath:" + waypoint.IconPath + "\n";
//                body += "Location:" + waypoint.Location.ToXmlSaveString() + "\n";
//            }
//        }

        //TODO hmm there are some oddities 
//        protected string PackElementPropertiesIntoBody(string inBody, IEnumerable<IElementProperty> props, bool isChild)
//        {
//            if (isChild) inBody += "\nElementChild:\n";
//            foreach (var prop in props)
//            {
//                if (!prop.VisibleToUser)
//                {
//                    if (!(prop.InternalName.Equals("IconPath", StringComparison.OrdinalIgnoreCase) ||
//                          prop.InternalName.Equals("IconSize", StringComparison.OrdinalIgnoreCase)))
//                        continue;
//                }
//                if (propertiesToIgnore.Contains(prop.InternalName.ToLower())) continue;
//                if (prop.ValueType == typeof(string))
//                    inBody = inBody +
//                             string.Format(CultureInfo.InvariantCulture, "{0}: {1}\n", prop.InternalName, prop.Value);
//                else
//                {
//                    string typeName = DepictionCoreTypes.FriendlyName(prop.ValueType);
//                    if (typeName == null)
//                        typeName = prop.ValueType.AssemblyQualifiedName;
//
//                    inBody = inBody +
//                             string.Format(CultureInfo.InvariantCulture, "{0}: [{1}] {2}\n", prop.InternalName,
//                                           typeName, DepictionTypeConverter.ChangeType(prop.Value, typeof(string)));
//                }
//            }
//            return inBody;
//        }
    }
}