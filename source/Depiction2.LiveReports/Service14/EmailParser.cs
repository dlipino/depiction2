using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using Depiction.LiveReports.Models;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.API.Service;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;

[assembly: InternalsVisibleTo("Depiction2.LiveReports.UnitTest")]
namespace Depiction2.LiveReports.Service14
{
    public class EmailParser
    {
        static public string FinalPositionKey = "PositionString";
        static public string SubjectPositionKey = "SubjectPosition";
        public const string TagKey = "TagList";
        static private string displayNameKey = "DisplayName";
        public const string depictionTagOpener = "//DT{";
        public const string depictionTagCloser = "}//";


        #region Main Parsers
        public static List<Dictionary<string, string>> GetElementPropertiesFromEmailSubjectAndBody(string baseSubject, string body)
        {
            var propertiesToIgnore = new List<string> { "email subject", "email body", "email from", "email to", "email error", "email sent" };
            var typeNameList = new List<string> { "Element type", "ElementType" };//"type", 


            var subjectProperties = ParseEmailSubjectIntoProperties(baseSubject);
            //Also gets waypoints, for now assume the first value is the element
            var allBodyParts = ParseEmailBodyFrom14SendIntoProperties(body);
            var mainBodyProperties = allBodyParts.FirstOrDefault();
            if (mainBodyProperties == null)
            {
                mainBodyProperties = new Dictionary<string, string>();
            }
            //combine the subject and main body for the primary element
            foreach (var prop in subjectProperties)
            {
                var key = prop.Key;
                if (mainBodyProperties.ContainsKey(key))
                {
                    //Something should happen here
                }
                else
                {
                    mainBodyProperties.Add(key, prop.Value);
                }
            }
            var allElementProps = new List<Dictionary<string, string>>();
            AdjustPositionFromProperties(mainBodyProperties, false);
            allElementProps.Add(mainBodyProperties);
            for (int i = 1; i < allBodyParts.Count; i++)
            {
                allElementProps.Add(allBodyParts[i]);
            }
            return allElementProps;
        }

        public static Dictionary<string, object> ConvertValuesToCorrectType(Dictionary<string, string> originals, string typeBase, bool geocodePosition)
        {
            IElementTemplate referenceTemplate = null;
            if (DepictionAccess.TemplateLibrary != null)
            {
                referenceTemplate = DepictionAccess.TemplateLibrary.FindElementTemplate(typeBase);
            }
            var results = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            var removeKeys = new List<string>();
            foreach (var kvp in originals)
            {
                var key = kvp.Key;
                var valAndType = ParseTypeAndStringValue(kvp.Value);
                var val = valAndType[0];
                var typeString = string.Empty;
                if (valAndType.Length > 1)
                {
                    typeString = valAndType[1];
                }
                if (key.Equals("zoneofinfluence", StringComparison.OrdinalIgnoreCase))
                {
                    if (DepictionGeometryService.Activated)
                    {
                        results.Add("ElementGeometry", DepictionGeometryService.CreateGeomtryFromWkt(val));
                    }
                    else
                    {
                        results.Add("WtkGeometry", val);
                    }
                    removeKeys.Add(key);
                }
                else if (key.Equals(FinalPositionKey))
                {
                    removeKeys.Add(FinalPositionKey);
                    double lat, lon;
                    string message;
                    var createdLatLong = LatLongParserUtility.ParseForLatLong(val, out lat, out lon, out message);
                    Point? geoPosition = new Point(lon, lat);
                    if (createdLatLong == null && DepictionAccess.GeocoderService != null)
                    {
                        var input = new GeocodeInput();
                        input.CompleteAddress = val;
                        geoPosition = DepictionAccess.GeocoderService.GeocodeInput(input, true);
                    }
                    results.Add("GeoPosition", geoPosition);
                }
                else if (key.Equals(TagKey))
                {
                    var tlist = new List<string>();
                    var ts = val.Split(',');
                    foreach (var t in ts)
                    {
                        tlist.Add(t);
                    }
                    results.Add(TagKey, tlist);
                }
                else
                {
                    var type = ElementPropertiesInformation.FindTypeForProperty(kvp.Key, referenceTemplate);
                    //The 1.4 email has a simple type name sent in the email

                    if (type == null)
                    {
                        if (referenceTemplate != null)
                        {

                        }
                        else
                        {
                            //try the simple type
                            if (DepictionAccess.NameTypeService != null)
                            {
                                type = DepictionAccess.NameTypeService.GetTypeFromName(typeString);
                            }
                        }
                    }

                    if (type == null)
                    {
                        results.Add(key, val);
                    }
                    else
                    {
                        var convVal = DepictionTypeConverter.ChangeType(val, type);
                        results.Add(key, convVal);
                    }
                }
            }
            return results;
        }

        #endregion

        #region subject parsing
        public static Dictionary<string, string> ParseEmailSubjectIntoProperties(string subjectString)
        {
            var subjectPropertyPairs = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            string subjectRemainder = subjectString;

            subjectRemainder = StripLeadingMARS(subjectRemainder);
            subjectRemainder = StripLeadingReFw(subjectRemainder);

            //Element type must be the first?
            var tags = string.Empty;
            var tagParse = ParseTagsFromSubject(subjectRemainder);
            if (tagParse.Length > 1)
            {
                tags = tagParse[1];
                subjectPropertyPairs.Add(TagKey, tags);
            }
            subjectRemainder = tagParse[0];
            var prototypeType = ParseElementTypeFromSubject(subjectRemainder, out subjectRemainder);
            if (!string.IsNullOrEmpty(prototypeType))
            {
                subjectPropertyPairs.Add("elementtype", prototypeType);
            }
            subjectRemainder = ParseLabel(subjectRemainder, subjectPropertyPairs);
            //The remainder should be location info
            if (!string.IsNullOrEmpty(subjectRemainder))
            {
                subjectPropertyPairs.Add(SubjectPositionKey, subjectRemainder);
            }

            return subjectPropertyPairs;
        }
        #region Subject parsing helpers

        static internal string[] ParseTagsFromSubject(string subjectWithTags)
        {
            var output = new List<string>();
            var result = Regex.Split(subjectWithTags, depictionTagOpener);
            if (result.Length == 1)
            {
                output.Add(result[0].Trim());
            }
            else if (result.Length > 1)
            {
                output.Add(result[0].Trim());
                output.Add(Regex.Replace(result[1], depictionTagCloser, "").Trim());

            }
            return output.ToArray();
        }
        static public HashSet<string> ParseRawTags(string rawTags)
        {
            var filterHash = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            if (!string.IsNullOrEmpty(rawTags))
            {
                var expectedTags = rawTags.Split(',');
                foreach (var tag in expectedTags)
                {
                    filterHash.Add(tag.Trim());
                }
            }
            return filterHash;
        }

        static public string[] SplitAndTrimString(string inString, string splitter)
        {
            var splitStrings = Regex.Split(inString, splitter);
            for (int i = 0; i < splitStrings.Length; i++)
            {
                splitStrings[i] = splitStrings[i].Trim();
            }
            return splitStrings;
        }

        private static string StripLeadingReFw(string subject)
        {
            return Regex.Replace(subject, "re:|fw:|fwd:", "", RegexOptions.IgnoreCase);
        }

        // to comply with trac #2157 - support for MARS from Winlink
        private static string StripLeadingMARS(string subject)
        {
            return Regex.Replace(subject, "^//MARS [MOPRZ]/", "");
        }

        internal static string ParseElementTypeFromSubject(string subjectFragment, out string subjectRemainder)
        {
            var type = string.Empty;
            subjectRemainder = subjectFragment;
            if (!subjectFragment.Contains(":"))
            {
                return string.Empty;
            }
            string[] parts = subjectFragment.Split(":".ToCharArray(), 2);
            type = parts[0].Trim();
            subjectRemainder = parts[1].Trim();//Not quite sure what this is
            return type;
        }

        /// <summary>
        /// Label is material before the first comma.
        /// If no comma, include everything as label.
        /// Return the non-label portion of the subject.
        /// </summary>
        private static string ParseLabel(string subject, Dictionary<string, string> displayNamePair)
        {
            string displayName;
            if (!subject.Contains(","))
            {
                displayName = subject;
                subject = "";
            }
            else
            {
                const int numberOfParts = 2;
                string[] parts = subject.Split(",".ToCharArray(), numberOfParts);

                displayName = parts[0].Trim();
                subject = parts[1].Trim();
            }
            if (!string.IsNullOrEmpty(displayName))
            {
                displayNamePair.Add(displayNameKey, displayName);
            }
            return subject;
        }

        #endregion

        #endregion

        #region body parsing

        static internal List<List<string>> SplitBodyIntoParts(string fullBody)
        {
            var newLineBreaks = fullBody.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> single = null;// new List<string>();
            var allSingles = new List<List<string>>();
            var lastCompleteLine = string.Empty;
            var bodyStart = false;
            foreach (var line in newLineBreaks)
            {

                if (bodyStart == false || //not all emails start with display name, especially not in tests
                    line.StartsWith("DisplayName", StringComparison.CurrentCultureIgnoreCase) ||
                    line.StartsWith("elementchild", StringComparison.CurrentCultureIgnoreCase) ||
                    line.StartsWith("elementwaypoint", StringComparison.CurrentCultureIgnoreCase))
                {
                    if(single != null && !string.IsNullOrEmpty(lastCompleteLine))
                    {
                        single.Add(lastCompleteLine);
                    }
                    single = new List<string>();
                    allSingles.Add(single);
                    bodyStart = true;
                }
                if (single == null) continue;
                if(line.Contains(":"))
                {
                    single.Add(lastCompleteLine);
                    lastCompleteLine = line;
                }else
                {
                    lastCompleteLine += (" " + line);//line;//
                }
            }
            if (single != null && !string.IsNullOrEmpty(lastCompleteLine))
            {
                single.Add(lastCompleteLine);
            }
            return allSingles;
        }

        public static List<Dictionary<string, string>> ParseEmailBodyFrom14SendIntoProperties(string bodyString)
        {
            var allResults = new List<Dictionary<string, string>>();
            var body = bodyString;
            var allELementParts = SplitBodyIntoParts(body);
            var primary = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var element in allELementParts)
            {
                foreach (string line in element)
                {
                    ParseLineIntoResult(line, primary);
                }
                allResults.Add(primary);
            }
            return allResults;
        }

        static internal string[] ParseTypeAndStringValue(string fullValueString)
        {
            //Check for a type
            if (fullValueString.StartsWith("["))
            {
                var results = new List<string>();
                var parsed = fullValueString.Split(' ');
                if (parsed.Length > 1)
                {
                    var typeParse = parsed[0];
                    results.Add(fullValueString.Replace(typeParse, "").Trim());
                    var type = Regex.Match(typeParse, @"\w+");

                    results.Add(type.Value);
                }
                return results.ToArray();

            }

            return new[] { fullValueString };
        }
        #endregion
        //TODO i know (think) this is a duplicate of something, nto sure where it is though
        static private bool AdjustPositionFromProperties(Dictionary<string, string> properties, bool replace)
        {
            var positionString = string.Empty;
            var positionKey = string.Empty;
            var allPositions = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

            if (properties.ContainsKey(SubjectPositionKey))
            {
                positionKey = SubjectPositionKey;
                allPositions.Add(positionKey, properties[positionKey]);
                properties.Remove(positionKey);
            }

            if (properties.ContainsKey("position"))
            {
                positionKey = "position";
                allPositions.Add(positionKey, properties[positionKey]);
                properties.Remove(positionKey);
            }

            if (properties.ContainsKey(FinalPositionKey))
            {
                positionKey = FinalPositionKey;
                allPositions.Add(positionKey, properties[positionKey]);
                properties.Remove(positionKey);
            }

            if (properties.ContainsKey("location"))
            {
                positionKey = "location";
                allPositions.Add(positionKey, properties[positionKey]);
                properties.Remove(positionKey);
            }


            string latitude = "", longitude = "";
            bool foundLat = false;
            foreach (var latName in LiveReportHelpers.LatitudePropertyNames)
            {
                var name = latName.ToLowerInvariant();
                foundLat = properties.TryGetValue(name, out latitude);
                if (foundLat)
                {
                    properties.Remove(name);
                    break;
                }
            }

            bool foundLong = false;
            foreach (var longName in LiveReportHelpers.LongitudePropertyNames)
            {
                var name = longName.ToLowerInvariant();
                foundLong = properties.TryGetValue(longName.ToLowerInvariant(), out longitude);
                if (foundLong)
                {
                    properties.Remove(name);
                    break;
                }
            }
            if (!positionKey.Equals(SubjectPositionKey))
            {
                if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
                {
                    positionKey = "latLongCombo";
                    var latlongString = string.Format("{0},{1}", latitude, longitude);
                    allPositions.Add(positionKey, latlongString);
                }
            }

            if (string.IsNullOrEmpty(positionKey)) return false;
            properties.Add(FinalPositionKey, allPositions[positionKey]);
            return true;
        }
        #region geodcoding helpers
        private static bool HasAllTheseProperties(Dictionary<string, object> propertyHolder, IEnumerable<string> propertyNames)
        {
            foreach (string field in propertyNames)
            {
                if (!propertyHolder.ContainsKey(field.ToLowerInvariant()))
                    return false;
            }
            return true;
        }
        private static string GetGeocodableAddress(Dictionary<string, object> propertyHolder, IEnumerable<string> propertyNames)
        {
            string location = String.Empty;

            foreach (string field in propertyNames)
            {
                object propValue = "";
                //If it gets to this method the property should exist.
                if (propertyHolder.TryGetValue(field.ToLowerInvariant(), out propValue))
                {
                    if (location.Length > 0)
                        location += ", ";
                    location += propValue;
                }

            }
            return location;
        }

        #endregion
        private static bool ParseLineIntoResult(string line, Dictionary<string, string> result)
        {
            //If true is returned then it is a child element (this is from legacy).
            //string[] parts = line.Split(":".ToCharArray());
            var regex = new Regex(":");
            var parts = regex.Split(line, 2);

            string valueString;// parts[1].Trim();
            if (parts.Length > 1)
            {
                valueString = parts[1].Trim();
            }
            else
            {
                return false;
            }
            string propertyName = parts[0].Trim();
            if (propertyName.Length == 0)//|| valueString.Length == 0
                return false;

            //i think this ensures that property names cannot be numbers(davidl 10/10/2013)
            if (!Regex.Match(propertyName, "^[a-zA-Z][a-zA-Z0-9 ]*$").Success) return false;

            //1.4 conversion
            if (propertyName.Equals("IconPath"))
            {
                var names = valueString.Split(':');
                if (names.Length > 1)
                {
                    //For 1.4 icon resource names
                    valueString = names[1];
                }
                propertyName = "IconResourceName";
            }

            if (!result.ContainsKey(propertyName))
            {
                result.Add(propertyName, valueString);
            }
            else
            {
                result[propertyName] = valueString;
            }
            return false;
        }


        /// <summary>
        /// Creates an EID using the messageID if an EID was not present.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="messageID"></param>
        //        public static void CreateEIDIfMissing(IElementPropertyHolder element, string messageID)
        //        {
        //            object elementID;
        //
        //            if (!element.GetPropertyValue("EID", out elementID))
        //            {
        //                var prop = new DepictionElementProperty("EID", messageID);
        //                prop.Editable = false;
        //                prop.VisibleToUser = true;
        //                prop.Deletable = false;
        //                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
        //                return;
        //            }
        //
        //            //Hack to deal with eid's that end of as numbers
        //            if (!(elementID is string))
        //            {
        //                element.RemovePropertyWithInternalName("eid", false);
        //                var prop = new DepictionElementProperty("EID", elementID.ToString());
        //                prop.Editable = false;
        //                prop.VisibleToUser = true;
        //                prop.Deletable = false;
        //                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
        //                return;
        //            }
        //        }
    }
}