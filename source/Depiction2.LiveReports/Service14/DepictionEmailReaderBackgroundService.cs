using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading;
using Depiction.LiveReports.Models;
using Depiction2.API;
using Depiction2.Base.Abstract;
using Depiction2.Base.StoryEntities;
using Depiction2.LiveReports.LiveReportSettings;
using Net.Mail;
using Net.Mime;

namespace Depiction2.LiveReports.Service14
{
    public class DepictionEmailReaderBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        public const string LiveReportTag = "source [live reports]: ";
        private bool testProcess = false;
        #region Variables
        private Timer collectionStopWatch;
        private EventWaitHandle timeWaiter;
        private bool readingEmails = false;
        private readonly Hashtable emailLog = new Hashtable();
        private LiveReportSourceInfo sourceInfo = new LiveReportSourceInfo();
        private int reportsRead;
        //        private List<Pop3ListItem> usedPop3Items = new List<Pop3ListItem>();
        #endregion

        #region properties

        public LiveReportSourceInfo SourceInfo { get { return sourceInfo; } }
        public double RefreshRateMinutes { get; set; }

        public override bool IsServiceRefreshable
        {
            get { return true; }
        }

        public int ReportsRead
        {
            get { return reportsRead; }
            private set { reportsRead = value; }
        }

        #endregion

        #region Constructor


        public DepictionEmailReaderBackgroundService(LiveReportSourceInfo liveReportSourceInfo)//, string elementType)
        {
            sourceInfo = liveReportSourceInfo;
            RefreshRateMinutes = 2;
        }
        public DepictionEmailReaderBackgroundService(bool isTesting)
        {
            testProcess = isTesting;
            RefreshRateMinutes = 2;
        }
        #endregion
        #region Helpers
        public class Pop3ListItemComparer<T> : IEqualityComparer<T> where T : Pop3ListItem
        {
            #region Implementation of IEqualityComparer<MapElementViewModel>

            public bool Equals(T x, T y)
            {
                if (!Equals(x.MessageId, y.MessageId)) return false;
                if (!Equals(x.Octets, y.Octets)) return false;
                return true;
            }

            public int GetHashCode(T obj)
            {
                int hashCode = obj.MessageId.GetHashCode() >> 3;
                hashCode ^= obj.Octets.GetHashCode();
                return hashCode;
            }
            #endregion
        }

        internal bool ConnectionTester()
        {
            var port = sourceInfo.PortNumber;
            if (port < 0)
            {
                port = sourceInfo.UseSSL ? 995 : 110;
            }
            using (var client = new Pop3Client(new Pop3ClientParams(sourceInfo.Pop3Server, port, sourceInfo.UseSSL,
                                                        sourceInfo.CompleteUserName, sourceInfo.Password)))
            {
                try
                {
                    client.Authenticate();
                    client.Stat();
                }
                catch (Pop3Exception ex)
                {
                    if (ex.Message.Contains("-ERR [IN-USE]"))
                    {
                        return false;
                    }
                }
                catch (NullReferenceException nullEx)
                {
                    return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    try
                    {
                        if (client.CurrentState == Pop3State.Transaction)
                        {
                            client.Noop();
                        }
                        client.Quit();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return true;
        }

        #endregion

        internal Dictionary<MailMessageEx, MimeEntity> GetNonreadMessagesFromEmailLocation()
        {
            var messages = new Dictionary<MailMessageEx, MimeEntity>();
            //retrieve MimeMessage/MimeEntities from the server
            int totalMessages = 0;
            int totalRead = 0;
            int readCount = 0;
            int emailErrorCount = 0;
            var port = sourceInfo.PortNumber;
            var localpop3Items = new List<Pop3ListItem>();
            if (port < 0)
            {
                port = sourceInfo.UseSSL ? 995 : 110;
            }
            using (var client = new Pop3Client(new Pop3ClientParams(sourceInfo.Pop3Server, port, sourceInfo.UseSSL, sourceInfo.CompleteUserName, sourceInfo.Password)))
            {
                try
                {
                    client.Authenticate();
                    client.Stat();

                    var itemList = client.List();//What does this get?
                    if (itemList == null) return messages;
                    //With the tolist you get a possible reenumeration issue, i cant
                    //remember what it is or how to fix at the moment.
                    var unreadItems = itemList.Except(localpop3Items, new Pop3ListItemComparer<Pop3ListItem>()).ToList();

                    totalMessages = unreadItems.Count;
                    foreach (Pop3ListItem item in unreadItems)
                    {
                        readCount++;
                        if (ServiceStopRequested) break;
                        UpdateStatusReport(string.Format("Reading {0} of {1} emails", readCount, totalMessages));
                        try
                        {
                            var key = client.RetrMailMessageEx(item);
                            if (ServiceStopRequested) break;
                            var value = client.RetrMimeEntity(item);
                            if (ServiceStopRequested) break;
                            messages.Add(key, value);
                            totalRead++;
                        }
                        catch (NotSupportedException nsex)
                        {
                            emailErrorCount++;
                            //                            var errorMessage = string.Format("Could not convert email {0} of {1} into an element",
                            //                                                             messageCount, totalMessages);
                            //                            DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 10);
                        }
                    }
                    localpop3Items.AddRange(unreadItems);
                }
                catch (Pop3Exception ex)
                {
                    if (ex.Message.Contains("-ERR [IN-USE]"))
                    {
                        //                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Email account \"{0}\" is in use. Will retry shortly.", SourceInfo.CompleteUserName)));
                        return new Dictionary<MailMessageEx, MimeEntity>(); ;
                    }
                }
                catch (NullReferenceException nullEx)
                {
                    //                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Could not access all emails from {0}. Returning {1} of {2}",
                    //                                                                                                          SourceInfo.CompleteUserName, totalRead, totalMessages), 15));
                    return new Dictionary<MailMessageEx, MimeEntity>();

                }
                catch (Exception ex)
                {
                    //                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Could not access all emails from {0}. Will retry shortly.", SourceInfo.CompleteUserName), 15));
                    //#if DEBUG
                    //                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(
                    //                                                                           string.Format("DEBUG: problem reading emails from account {0} from POP3 server {1}.\n\nInternal error was: {2}",
                    //                                                                                         SourceInfo.CompleteUserName, SourceInfo.Pop3Server, ex.Message)));
                    //#endif

                    return new Dictionary<MailMessageEx, MimeEntity>();
                }
                finally
                {
                    try
                    {
                        if (client.CurrentState == Pop3State.Transaction)
                        {
                            client.Noop();
                        }
                        client.Quit();
                    }
                    catch (Exception ex)
                    {
                        messages = new Dictionary<MailMessageEx, MimeEntity>();
                        //DepictionExceptionHandler.HandleException(ex, false);
                    }
                }
            }
            UpdateStatusReport(string.Format("Read {0} of {1} emails", totalRead, totalMessages));
            if (ServiceStopRequested) return new Dictionary<MailMessageEx, MimeEntity>();
            return messages;
        }

        #region static helpers

        public static List<Dictionary<string, object>> GetRawPrototypeFromEmailWithUniqueID(string subject, string body, string from, string to, string deliveryDate, string messageID)
        {
            DateTime emailTime = DateTime.MinValue;
            DateTime.TryParse(deliveryDate, out emailTime);

            var prototype = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subject, body);

            var all = new List<Dictionary<string, object>>();
            foreach (var proto in prototype)
            {
                all.Add(EmailParser.ConvertValuesToCorrectType(proto, null, true));
            }
            return all;
        }
        #region these arent used all that much in the real app
        //        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID)
        //        {
        //            return GetElementFromEmail(subject, body, from, to, deliveryDate, messageID, string.Empty, null);
        //        }
        //        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID,
        //                                                            IDepictionGeocoder[] usableGeocoders)
        //        {
        //            return GetElementFromEmail(subject, body, from, to, deliveryDate, messageID, string.Empty, usableGeocoders);
        //        }
        //
        //        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID, string defaultElementType,
        //                                                            IDepictionGeocoder[] usableGeocoders)
        //        {
        //            var rawDataFilledPrototype = GetRawPrototypeFromEmailWithUniqueID(subject, body, from, to, deliveryDate, messageID, usableGeocoders);
        //
        //            var element = ElementFactory.CreateElementFromPrototype(rawDataFilledPrototype);
        //
        //            if (element == null) return null;
        //
        //            element.UpdatePropertyValue("draggable", false, false);
        //
        //            return element;
        //        }
        #endregion

        private static string GetPlainTextBody(MailMessage message, MimeEntity entity)
        {
            Stream stream = null;
            foreach (MimeEntity child in entity.Children)
            {
                if (child.ContentType.MediaType.Equals(MediaTypes.TextPlain))
                {
                    stream = child.Content;
                }
            }

            if (stream == null)
            {
                foreach (AlternateView view in message.AlternateViews)
                {
                    if (view.ContentType.MediaType.Equals(MediaTypes.TextPlain))
                    {
                        stream = view.ContentStream;
                    }
                }
            }

            if (stream != null)
            {
                stream.Position = 0;
                var streamReader = new StreamReader(stream);
                return streamReader.ReadToEnd();
            }

            //fall back to sending the entire mime content
            return message.Body;
        }

        #endregion

        #region Methods

        /// <summary>
        ///THis whole class is a mess, it is half way to full static but it still doesn't believe itself
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> CreatePropertyListsFromEmailLocation(string filterTags)
        {
            var prototypes = new List<Dictionary<string, object>>();
            var messages = GetNonreadMessagesFromEmailLocation();
            if (messages == null) return prototypes;
            var filterHash = EmailParser.ParseRawTags(filterTags);

            foreach (var key in messages.Keys)
            {
                if (ServiceStopRequested) break;
                if (key.MessageId != null && !emailLog.ContainsKey(key.MessageId))
                {
                    var subj = key.Subject ?? "";
                    if (filterHash.Count > 0)
                    {
                        var subjparts = EmailParser.ParseEmailSubjectIntoProperties(subj);
                        if (subjparts.ContainsKey(EmailParser.TagKey))
                        {
                            if (!filterHash.Overlaps(EmailParser.ParseRawTags(subjparts[EmailParser.TagKey])))
                            {
                                continue;
                            }
                        }
                    }
                    var from = key.From.ToString();
                    var to = key.To.ToString();
                    var deliveryDate = key.Headers[MailHeaders.Date];
                    string body = GetPlainTextBody(key, messages[key]) ?? "";
                    var prototype = GetRawPrototypeFromEmailWithUniqueID(subj, body, from, to, deliveryDate, key.MessageId);
                    if (prototype != null)
                    {
                        prototypes.AddRange(prototype);
                    }

                    emailLog.Add(key.MessageId, true);
                }
            }
            ReportsRead += prototypes.Count;
            return prototypes;
        }

        public bool IsValidEmailAddressPassword()
        {
            //            if (!DepictionInternetConnectivityService.IsInternetAvailable)
            //            {
            //                DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Please check internet connection"));
            //                //return false;
            //            }
            try
            {
                // Validate the account settings
                using (var email = new Pop3Client(new Pop3ClientParams(SourceInfo.Pop3Server, SourceInfo.UseSSL ? 995 : 110, SourceInfo.UseSSL, SourceInfo.CompleteUserName, SourceInfo.Password)))
                {
                    email.Authenticate();
                    email.Stat();
                    email.Noop();
                    email.Quit();
                }
                //                EmailInformationSettings.AddIncomingEmailAccount(new IncomingEmailSettings(SourceInfo.SimpleUserName, SourceInfo.Pop3Server, SourceInfo.UseSSL, SourceInfo.RawTags));
            }
            catch (Exception ex)
            {
                if (ex is Pop3Exception)
                {
                    if (ex.Message.Contains("-ERR [IN-USE]"))
                    {
                        return true;
                    }
                    //                    if (ex.InnerException != null && ex.InnerException is SocketException)
                    //                    {
                    //                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Could not connect to server " + SourceInfo.Pop3Server, 10));
                    //
                    //                    }
                    //                    else
                    //                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Invalid login credentials for: " + SourceInfo.CompleteUserName, 10));
                }
                else if (ex is ArgumentNullException)
                {
                    var exc = ex as ArgumentNullException;

                    //                    DepictionAccess.NotificationService.DisplayMessage(
                    //                        new DepictionMessage(string.Format("Please enter a {0} for: {1} ", exc.ParamName, SourceInfo.CompleteUserName), 10));
                }
                else
                {
                    //                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Unknown login exception for: " + SourceInfo.CompleteUserName, 10));
                }
                return false;
            }
            return true;
        }

        public void AddReadEmailsToDepictionStory(IStory currentDepiction, List<Dictionary<string, object>> rawPrototypes, string idPropName)
        {
            if (ServiceStopRequested) return;
            if (currentDepiction != null)
            {
                currentDepiction.UpdateRepo(rawPrototypes, idPropName, null);
            }
        }
        #endregion

        #region protected and private helpers

        private int ConvertMinutesToMilliSeconds(double minutes)
        {
            return (int)(minutes * 60 * 1000);
        }

        protected void TimedEmailReader(object args)
        {
            UpdateStatusReport("Updating live reports");
            Thread.Sleep(100);
            collectionStopWatch.Change(Timeout.Infinite, Timeout.Infinite);
            var randomNumberGenerator = new Random();
            readingEmails = true;

            try
            {
                if (ServiceStopRequested)
                {
                    readingEmails = false;
                    return;
                }
                if (!testProcess)
                {
                    var rawPrototypes = CreatePropertyListsFromEmailLocation(sourceInfo.RawTags);
                    if (ServiceStopRequested)
                    {
                        readingEmails = false;
                        return;
                    }
                    UpdateStatusReport(string.Format("Read {0} email elements", rawPrototypes.Count));
                    if (ServiceStopRequested)
                    {
                        readingEmails = false;
                        return;
                    }
                    AddReadEmailsToDepictionStory(DepictionAccess.DStory, rawPrototypes, null);
                }


            }
            catch (Exception ex)
            {
                Debug.WriteLine("There was an exception in Live Report reader");
                Debug.WriteLine(ex.Message);
            }

            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Waiting for next live report poll");
                    Thread.Sleep(100);
                    // Randomly vary the sleep period to reduce the chance of colliding
                    // with other Depictions accessing the same POP3 account.
                    var millisecondsToSleep = randomNumberGenerator.Next(100, 200);
                    var refreshRateMillisecond = (int)(RefreshRateMinutes * 60 * 1000);
                    var waitTime = refreshRateMillisecond + millisecondsToSleep;
                    collectionStopWatch.Change(waitTime, waitTime);
                }
            }
            readingEmails = false;
        }


        #endregion
        #region Overrides of BaseDepictionBackgroundThreadOperation

        public override void RefreshResult()
        {
            if (ServiceStopRequested || readingEmails) return;

            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Reading Live Report elements");
                    Thread.Sleep(100);
                    collectionStopWatch.Change(0, ConvertMinutesToMilliSeconds(RefreshRateMinutes));
                }
            }
        }
        override public void StopBackgroundService()
        {
            Debug.WriteLine(DateTime.Now + " Stopping the service");
            ServiceStopRequested = true;
            collectionStopWatch.Dispose();
            collectionStopWatch = null;
            timeWaiter.Set();
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            if (!testProcess)
            {
                if (!IsValidEmailAddressPassword()) return false;
            }
            UpdateStatusReport("Updating live reports");
            Thread.Sleep(100);
            timeWaiter = new EventWaitHandle(false, EventResetMode.ManualReset);

            return true;
        }
        protected override object ServiceAction(object args)
        {
            collectionStopWatch = new Timer(TimedEmailReader, this, 0, ConvertMinutesToMilliSeconds(RefreshRateMinutes));
            timeWaiter.WaitOne();
            return null;
        }
        protected override void ServiceComplete(object args)
        {
            Debug.WriteLine("done with service");
        }

        #endregion
    }
}