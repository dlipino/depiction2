using System.Configuration;

namespace Depiction2.LiveReports.LiveReportSettings
{
    abstract public class BasicEmailSettings
    {
        public string UserName { get; set; }
        public bool UseSSL { get; set; }
        public string DepictionTags { get; set; }
    }
}