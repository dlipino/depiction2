using System.Configuration;

namespace Depiction2.LiveReports.LiveReportSettings
{
    public class IncomingEmailSettings : BasicEmailSettings
    {
        public string Pop3Server { get; set; }
        public string PortNumber { get; set; }

        public bool IsNewRecord { get; set; }

        public string NewRecordName { get; set; }

        public bool ShowAdvancedFields { get; set; }

        public string SuggestedDomain { get; set; }

        public string ExplanatoryText { get; set; }

        public IncomingEmailSettings()
        {
            IsNewRecord = false;
            ShowAdvancedFields = false;
        }

        public IncomingEmailSettings(string userName, string pop3Server, bool useSSL, string rawTags)
            : this()
        {
            UserName = userName;
            Pop3Server = pop3Server;
            UseSSL = useSSL;
            DepictionTags = rawTags;
        }

    }
}