using System.Collections.Generic;
using System.Collections.ObjectModel;
using Depiction2.Base.Helpers;
using Depiction2.LiveReports.LiveReportSettings;
using Depiction2.LiveReports.Service14;

namespace Depiction2.LiveReports.ViewModels
{
    public class LiveReportSettupVM : ViewModelBase
    {
        #region Variables

        private LiveReportImporter importerMain;
        ObservableCollection<IncomingEmailSettings> records = new ObservableCollection<IncomingEmailSettings>();
        private IncomingEmailSettings currentEmailAccount;

        #endregion

        #region Properties

        public string EmailPassword { get; set; }
        public string RefreshInterval { get; set; }

        public IncomingEmailSettings SelectedEmailAccount
        {
            get { return currentEmailAccount; }
            set
            {
                currentEmailAccount = value; OnPropertyChanged("SelectedEmailAccount");
            }
        }

        public ObservableCollection<IncomingEmailSettings> IncomingEmailAccountList
        {
            get
            {
                return records;
            }
            private set
            {
                records = value; OnPropertyChanged("IncomingEmailAccountList");

            }
        }

        #endregion

        #region Constructor

        public LiveReportSettupVM(LiveReportImporter importerSource)
        {
            UpdateIncomingAccounts();
            RefreshInterval = "2";
            importerMain = importerSource;

        }
        #endregion

        #region Helpers
        public void StartTheThing()
        {
            importerMain.StartLiveReportReader(CreateLiveReportInfo());
        }
        public void StartFakeThing()
        {
            importerMain.FakeStart();
        }
        public void UpdateIncomingAccounts()
        {
//            IncomingEmailAccountRecords incomingEmailAccounts = new IncomingEmailAccountRecords();////No clue how this is supposed to work
//            IncomingEmailAccountRecords currentAccounts = EmailInformationSettings.GetIncomingEmailAccounts() ?? new IncomingEmailAccountRecords();
            var defaultSettins = new List<IncomingEmailSettings>();
            // Add an email account with empty user name and server; this will create a
            // binding with "New account..." on the dialog ...
            defaultSettins.Add(new IncomingEmailSettings { ShowAdvancedFields = true, IsNewRecord = true, NewRecordName = "Other Pop3 account...", ExplanatoryText = "Live reports requires your email service to use the POP3 protocol. For more information, see the section \"Live Reports\" in Depiction help." });
            defaultSettins.Add(new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Yahoo! Mail Plus account", Pop3Server = "plus.pop.mail.yahoo.com", SuggestedDomain = "@yahoo.com", UseSSL = false, ExplanatoryText = "Note: Live reports requires the paid Yahoo! Mail Plus account, since the free Yahoo! Mail accounts do not support the POP3 access that Live reports uses." });
            defaultSettins.Add(new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Microsoft Hotmail account", Pop3Server = "pop3.live.com", SuggestedDomain = "@hotmail.com", UseSSL = true, ExplanatoryText = "Note: Free Microsoft Hotmail accounts can only be logged in to every 15 minutes." });
            defaultSettins.Insert(0,new IncomingEmailSettings { IsNewRecord = true, NewRecordName = "Google Gmail account", Pop3Server = "pop.gmail.com", SuggestedDomain = "@gmail.com", ExplanatoryText = "", UseSSL = true });

            foreach (var account in defaultSettins)
            {
                IncomingEmailAccountList.Add(account);
//                incomingEmailAccounts.AddOrReplaceExisting((IncomingEmailSettings)account);
            }

//            IncomingEmailAccountList = incomingEmailAccounts;
        }

        public LiveReportSourceInfo CreateLiveReportInfo()
        {
            var sourceInfo = new LiveReportSourceInfo();

            sourceInfo.Pop3Server = SelectedEmailAccount.Pop3Server;
            var rawName = SelectedEmailAccount.UserName;
            sourceInfo.SimpleUserName = rawName;
            sourceInfo.UseSSL = SelectedEmailAccount.UseSSL;
            sourceInfo.Password = EmailPassword;
            sourceInfo.RawTags = SelectedEmailAccount.DepictionTags;
            int port = -1;
            if (!int.TryParse(SelectedEmailAccount.PortNumber, out port))
            {
                port = -1;
            }
            sourceInfo.PortNumber = port;
            return sourceInfo;
        }
        #endregion
    }
}