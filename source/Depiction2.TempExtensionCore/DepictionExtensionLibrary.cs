﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Depiction2.API.Extension.Base;
using Depiction2.API.Extension.Geocoder;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Interactions.Interactions;
using Depiction2.Externals;
using Depiction2.TempExtensionCore.Interfaces;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]

[assembly: InternalsVisibleTo("Depiction2.API")]
namespace Depiction2.TempExtensionCore
{
    //The singleton thing is making test act strange especially since depictionAccess is using it. 
    public class DepictionExtensionLibrary
    {

        #region global gets? i guess

        public string BaseExtensionDir { get; private set; }

        #endregion
        #region non legacy imports/lazy imports

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IDepictionImporterExtension, IDepictionExtensionMetadataBase>[] ExtensionInitializerArray { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IDepictionImporterExtension, IDepictionFileImporterMetadata>[] FileImporterExtensionArray { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IDepictionStoryLoader, IDepictionExtensionMetadataBase>[] DepictionVersionFileReaders { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IDepictionGdalExtension, IDepictionExtensionMetadataBase>[] OGRExtensionArray { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IDepictionProjectionChanger, IDepictionExtensionMetadataBase>[] ProjectionChangersArray { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public Lazy<IGeocoderExtension, IGeocoderExtensionMetadata>[] GeocoderArray { get; private set; }

//        [ImportMany(AllowRecomposition = true)]
//        public Lazy<IGeocoderExtension, IGeocoderMetadata>[] GeocoderArray { get; private set; }

        #endregion

        #region variables

        private CompositionContainer extensionContainer;
        #endregion
        #region Instance things
        //this kind of makes generalized tests hellish
        private static DepictionExtensionLibrary instance;
        public static DepictionExtensionLibrary Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DepictionExtensionLibrary();
                }
                return instance;
            }
        }

        #endregion
        #region Constructor/destructor
        internal DepictionExtensionLibrary()
        {
            BaseExtensionDir = string.Empty;
        }

        ~DepictionExtensionLibrary()
        {
            Decompose();
        }
        #endregion

        public void Compose()
        {
            var catalog = new AggregateCatalog();
            BaseExtensionDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Extensions");

            if (Directory.Exists(BaseExtensionDir))
            {
                var allSubDirs = Directory.GetDirectories(BaseExtensionDir, "*");
                foreach (var folder in allSubDirs)
                {
                    catalog.Catalogs.Add(new DirectoryCatalog(folder));
                }
            }
            else
            {
                var ex = new DirectoryNotFoundException("Could not find base extension directory.");
                DepictionLogger.LogException(ex);
            }
            extensionContainer = new CompositionContainer(catalog);
            extensionContainer.ComposeParts(this);

            //Initiates the importers, kind of a hack for now. 
            //Primarily used for setting up the gdal/ogr
            foreach (var ext in ExtensionInitializerArray)
            {
                var meta = ext.Metadata;
//                Console.WriteLine(meta.Activated);
                var val = ext.Value;
                if (val != null)
                {
                    val.InitiateImporter();
                }
            }
        }

        internal bool ComposeFromDir(string dir)
        {
            if (!Directory.Exists(dir)) return false;
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(dir));
            extensionContainer = new CompositionContainer(catalog);
            extensionContainer.ComposeParts(this);
            BaseExtensionDir = dir;
            return true;
        }

        public void Decompose()
        {
            if (extensionContainer != null)
            {
                extensionContainer.Dispose();
            }
            extensionContainer = null;
            //instance = null causes the test cycle to crash: SetProperty cant be found
            //Must be careful to recreate if the decompose is called (in tests)
        }
        #region Access helpers
        public IEnumerable<IDepictionExtensionMetadataBase> GetAllMetadata()
        {
            //So does this load all extensions when called? ie break the whole idea
            //of lazy
            var allExtensionMeta = new List<IDepictionExtensionMetadataBase>();
            //            foreach (var lazy in ImporterExtensionArray)
            //            {
            //                allExtensionMeta.Add(lazy.Metadata);
            //            }
            foreach (var lazy in DepictionVersionFileReaders)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }
            foreach (var lazy in OGRExtensionArray)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }
            foreach (var lazy in ProjectionChangersArray)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }
            foreach (var lazy in NativeBehaviors)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }
            foreach (var lazy in NativeConditions)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }
            foreach (var lazy in FileImporterExtensionArray)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }

            foreach (var lazy in GeocoderArray)
            {
                allExtensionMeta.Add(lazy.Metadata);
            }

            return allExtensionMeta;
        }
        #endregion


        #region legacy thinsg for interactiions

        [ImportMany(typeof(BaseBehavior))]
        private Lazy<BaseBehavior, IDepictionExtensionMetadataBase>[] NativeBehaviors { get; set; }

        [ImportMany(typeof(ICondition))]
        private Lazy<ICondition, IDepictionExtensionMetadataBase>[] NativeConditions { get; set; }

        public bool AreBehavioursInitialized()
        {
            return NativeBehaviors != null;
        }

        public BaseBehavior RetrieveBehavior(string behaviorName)
        {
            if (NativeBehaviors == null)
            {
                return null;
            }
            var behaviorKeyValue = NativeBehaviors.FirstOrDefault(t => t.Metadata.Name.Equals(behaviorName));
            if (behaviorKeyValue == null) return null;
            var behavior = behaviorKeyValue.Value;

            return behavior;
        }

        public ICondition RetrieveCondition(string conditionName)
        {
            if (NativeConditions == null) return null;
            var conditionKeyValue = NativeConditions.FirstOrDefault(t => t.Metadata.Name.Equals(conditionName));
            if (conditionKeyValue == null) return null;
            var condition = conditionKeyValue.Value;

            return condition;
        }
        #endregion

        #region geocoder

        public Lazy<IGeocoderExtension, IGeocoderExtensionMetadata> RetrieveGeocoder(string geocoderName)
        {
            if (GeocoderArray == null) return null;
            var geocoderValue = GeocoderArray.FirstOrDefault(t => t.Metadata.Name.Equals(geocoderName));
            return geocoderValue;
        }

        #endregion
    }
}