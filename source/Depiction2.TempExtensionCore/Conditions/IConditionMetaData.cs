﻿namespace Depiction2.Base.ExtensionFramework.Conditions
{
    public interface IConditionMetaData : ILegacyExtensionMetadata
    {
        string ConditionName { get; }
        string ConditionDescription { get; }
    }
}