﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.Base.ExtensionFramework;
using Depiction2.Base.Interactions.Interactions;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Conditions
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class ConditionAttribute : DepictionAddonBaseMetadata, IDepictionExtensionMetadataBase
    {
        public ConditionAttribute(string conditionName, string conditionDisplayName, string conditionDescription)
            : base(typeof(ICondition))
        {
            Name = conditionName;
             DisplayName = conditionDisplayName;

           Description = conditionDescription;
        }

        public bool Activated
        {
            get { return true; }
        }
    }
}