﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.Base.ExtensionFramework;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Behaviors
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class BehaviorAttribute : DepictionAddonBaseMetadata, IDepictionExtensionMetadataBase
    {
        public BehaviorAttribute(string behaviorName, string behaviorDisplayName, string behaviorDescription)
            : base(typeof(IBehavior))
        {
            Name = behaviorName;

            DisplayName = behaviorDisplayName;
            Description = behaviorDescription;
        }

        public bool Activated
        {
            get { return true; }
        }
    }
}