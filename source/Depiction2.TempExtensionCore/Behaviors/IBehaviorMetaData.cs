﻿namespace Depiction2.Base.ExtensionFramework.Behaviors
{
    public interface IBehaviorMetaData: ILegacyExtensionMetadata
    {
        string BehaviorName { get; }
        string BehaviorDisplayName { get; }
        string BehaviorDescription { get; }
    }
}