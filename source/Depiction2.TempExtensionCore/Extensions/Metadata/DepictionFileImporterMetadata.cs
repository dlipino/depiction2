﻿using System;
using System.ComponentModel.Composition;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Extensions.Metadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionFileImporterMetadata : DepictionImporterMetadata, IDepictionFileImporterMetadata
    {
        #region variables
        protected string[] supportedExtensions = new string[0];
        #endregion
        #region properties
        public string[] SupportedExtensions { get { return supportedExtensions; } }
        #endregion

        #region constructor, this part is very important

        public DepictionFileImporterMetadata()
            : base(typeof(IDepictionImporterExtension))
        {
            Name = "Importer";
            DisplayName = "File Importer";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
        }
        public DepictionFileImporterMetadata(string[] extensions)
            : this()
        {
            if (extensions != null) supportedExtensions = extensions;
        }
        #endregion

    }
}