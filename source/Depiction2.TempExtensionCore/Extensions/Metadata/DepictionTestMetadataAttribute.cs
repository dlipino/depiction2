﻿using System;
using System.ComponentModel.Composition;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Extensions.Metadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionTestMetadataAttribute : ExportAttribute, IDepictionExtensionMetadataBase
    {
        #region Properties
        public bool Activated
        {
            get { return CheckActivation(); }
        }

        public string Author { get; set; }

        #endregion
        
        #region Constructor
        
        public DepictionTestMetadataAttribute()
            : base(typeof(IDepictionExtensionBase))
        {

        }
        #endregion
        #region Methods

        public bool CheckActivation()
        {
            return true;
        }
        #endregion

    }
}