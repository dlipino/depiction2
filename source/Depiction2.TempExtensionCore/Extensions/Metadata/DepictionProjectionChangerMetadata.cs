﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Extensions.Metadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionProjectionChangerMetadata : ExportAttribute, IDepictionExtensionMetadataBase
    {
        #region Properties

        public string Description { get; set; }

        public bool Activated
        {
            get { return true; }
        }

        public string ExtensionPackage { get; set; }

        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Author { get; set; }

        #endregion

        #region constructor, this part is very important

        public DepictionProjectionChangerMetadata()
            : base(typeof(IDepictionProjectionChanger))
        {
            Name = DisplayName = "GenericProjectionChanger";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
            Description = "A generic projection changer";
        }
        #endregion
    }
}