﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction2.TempExtensionCore.Extensions.Metadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionGdalExtensionMetadata : ExportAttribute, IDepictionExtensionMetadataBase
    {
        #region Properties

        public string Description { get; private set; }

        public bool Activated
        {
            get { return CheckActivation(); }
        }

        public string ExtensionPackage { get; private set; }

        public string Name { get; private set; }
        public string DisplayName { get; private set; }
        public string Author { get; set; }

        #endregion
        #region constructor, this part is very important

        public DepictionGdalExtensionMetadata()
            : base(typeof(IDepictionGdalExtension))
        {
            Name = "Gdal";
            DisplayName = "Gdal Extensions";
            Description = "Extension that inclues Gdal 1.9.1 library.";
            ExtensionPackage = "Default";
            Author = "Depiction Inc";
        }
        #endregion
        #region Methods

        public bool CheckActivation()
        {
            return true;
        }
        #endregion
    }
}