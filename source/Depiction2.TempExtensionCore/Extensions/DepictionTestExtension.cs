﻿using Depiction2.TempExtensionCore.Extensions.Metadata;

namespace Depiction2.TempExtensionCore.Extensions
{
    [DepictionTestMetadata(Author = "Me")]
    public class DepictionTestExtension : IDepictionExtensionBase
    {

    }
}