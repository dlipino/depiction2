﻿using Depiction2.API.Extension.Base;

namespace Depiction2.TempExtensionCore.Interfaces
{
    public interface IDepictionFileImporterMetadata : IDepictionExtensionMetadataBase
    {
        string[] SupportedExtensions { get; }
    }
}