﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;

namespace Depiction2.TempExtensionCore.Interfaces
{
    //get things to work first, the clean up
    public interface IDepictionProjectionChanger : IDepictionExtensionBase, IDepictionStoryProjectionService
    {
        string CurrentCSWkt { get; }
        string OtherCSWkt { get; }

//        void SetOtherSpatialReference(string wktString);
//        Point TransformPoint(double x, double y,bool toDepiction);
        List<Point> TransformPoints(IEnumerable<Point> transPoints,bool toDepiction);
        void TransformGeometry(IDepictionGeometry geometry,bool toDepiction);
    }
}