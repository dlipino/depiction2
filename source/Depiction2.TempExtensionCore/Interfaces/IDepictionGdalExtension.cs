﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Utilities.Drawing;

namespace Depiction2.TempExtensionCore.Interfaces
{
    public interface IDepictionGdalExtension : IDepictionExtensionBase
    {
        List<EnhancedPointListWithChildren> GetPointsFromWKTString(string wktString);
        List<EnhancedPointListWithChildren> GetTransformedPointsFromWKTString(string wktString, string sourceProj,string targetProj);
    }
}