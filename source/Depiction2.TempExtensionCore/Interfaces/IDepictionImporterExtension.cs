﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Base;

namespace Depiction2.TempExtensionCore.Interfaces
{
    public interface IDepictionImporterExtension : IDepictionExtensionBase
    {
        void InitiateImporter();
        void ImportElements(string elementLocations, string fullElementType, string idPropertyName);
        HashSet<string> FindPropertyIntersections(string fileName);
        Window GetSettingsWindow();
    }
}