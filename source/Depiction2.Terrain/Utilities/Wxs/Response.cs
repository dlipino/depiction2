﻿namespace Depiction2.Terrain.Utilities.Wxs
{
    //This is legacy tech
    public class Response
    {
        public string ResponseFile { get; set; }

        public bool IsRequestSuccessful { get; set; }

        public bool IsRequestCanceled { get; set; }

//        public DataFeatures DataFeatures { get; set; }
    }
}
