﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API.OGCServices;
using Depiction2.API;
using Depiction2.Base.Geo;

namespace Depiction2.Terrain.Utilities.Wxs
{
    public class WxsDataService : IDataRetriever
    {
        private readonly Dictionary<string, object> filterParameters;
        private readonly ICartRect regionExtent;
        private IRepeatableRequest request;

        public WxsDataService(Dictionary<string, object> filterParameters, ICartRect regionExtent)
        {
            this.filterParameters = filterParameters;
            this.regionExtent = regionExtent;
        }

        public string RequestFileData(string fullTempFileName, out Response responseOut)
        {
            string layerName = "", serverType = "";
            string connectionString = "";
            foreach (var param in filterParameters)
            {
                switch (param.Key.ToLower())
                {
                    case "url":
                        connectionString = param.Value.ToString();
                        break;
                    case "servicetype":
                        serverType = param.Value.ToString();
                        break;
                    case "layername":
                        layerName = param.Value.ToString();
                        break;
                    case "username":
                        break;
                    case "userpassword":
                        break;
                    default:
                        break;
                }
            }

            if(string.IsNullOrEmpty(fullTempFileName))
            {
                fullTempFileName = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, Guid.NewGuid() + "-" +
                                                                                        serverType + ".gml");
            }

            request = new WxsRequest(layerName, serverType, fullTempFileName, connectionString, regionExtent);
            
            var repeater = new RepeaterService(request, 5);
            repeater.Execute();

            responseOut = request.Response;

            if (!request.Response.IsRequestSuccessful)
            {
                return null;
            }

            return fullTempFileName;
        }

        public void Cancel()
        {
            if (request != null)
                request.Cancel();
        }
    }

}
