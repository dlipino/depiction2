﻿namespace Depiction2.Terrain.Utilities.Wxs
{
    //Legacy tech
    /// <summary>
    /// 
    /// </summary>
    public interface IDataRetriever
    {
        /// <summary>
        /// Returns the full path to the GML file produced by the add-on data provider.
        /// </summary>
        /// <param name="fullTempFileName"></param>
        /// <param name="responseOut"></param>
        /// <returns></returns>
        string RequestFileData(string fullTempFileName, out Response responseOut);

        void Cancel();
    }
}
