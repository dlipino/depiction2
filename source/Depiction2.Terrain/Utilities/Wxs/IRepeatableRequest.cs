﻿namespace Depiction2.Terrain.Utilities.Wxs
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepeatableRequest
    {
        /// <summary>
        /// Gets the requestor.
        /// </summary>
        /// <value>The requestor.</value>
        string Requestor { get; }

        Response Response { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        void Execute();

        string LastErrorMessage { get; }

        /// <summary>
        /// Cancel this request.
        /// </summary>
        void Cancel();
    }
}
