﻿    //***************************************************************
    //                     Contour plotting of a grid.
    //                               
    //               Copyright (c) 1993 - 2002 by John Coulthard
    //
    //    This source code is free software; you can redistribute it and/or
    //    modify it under the terms of the GNU Lesser General Public
    //    License as published by the Free Software Foundation; either
    //    version 2.1 of the License, or (at your option) any later version.
    //
    //    This library is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    //    Lesser General Public License for more details.
    //
    //    You should have received a copy of the GNU Lesser General Public
    //    License along with this library; if not, write to the Free Software
    //    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    //
    //************************************************************************
    //
    //   This code is a transcription of a FORTRAN IV subroutine called
    //   CNTOUR that I wrote in the late 60's. (that is why it contains
    //   goto's). The last known bug was removed from CNTOUR in 1972 and I
    //   did not wish to change the structure of such a stable piece of code.
    //
    //   The source code for CNTOUR was published in the SHARE user's group
    //   library. The code was widely distributed and heavily used.


    //  Ported to C# to working (if ugly state) by Depiction, Inc 4/2009

    //***************************************************************
    // Requires a simple class defined in surfgrid.h and surfgrid.cpp to
    // supply the grid. 
    //




//// Forward declare functions in switches.cpp.
//void SwitchClear(int i, int j);
//int SwitchGet  (int i, int j);
//int SwitchSet  (int i, int j);


//static int XReferencePoint,  // We are contouring between a
//           YReferencePoint,  //    Reference point and a
//           XSubPoint,        //    Sub point. These are used to scan
//            YSubPoint,        //    the grid and mark beginings of lines.
//           NumberOfX,        // Number of X and Y grid lines.
//            NumberOfY,
//           Drawing;

//static float ContourValue;    // Value we are contouring.

//static float prevX, prevY;

//// Forward declare functions in contour.cpp
//static void TraceContour( csGrid grid, System::Collections::ArrayList^ cList );
//static void Interpolate( float, float, float, float, float, float, System::Collections::ArrayList^ cList);

//// DoLineTo must be supplied by the calling routine. 
//void DoLineTo( float x, float y, int drawtype );

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using Depiction2.Terrain.Terrain14;
using Depiction2.Terrain.Utilities;


namespace LGPLPrison
{

    public class SurfGrid
    {
        public int xsize()
        {
            throw new NotImplementedException();
        }

        public int ysize()
        {
            throw new NotImplementedException();
        }

        public float z(int x, int y)
        {
            return 0;
        }
    
internal float x(int XTracePoint)
{
 	throw new NotImplementedException();
}

        public float y(int point)
        {
            throw new NotImplementedException();
        }
    }
public static class ContourClass
{
  private static int contourNumber = 0;
  private static int YReferencePoint = 0;
  private static int YSubPoint       = 0;
  private static int XReferencePoint = 0;
  private static int XSubPoint       = 0;
  private static bool Drawing = false;
  private static int NumberOfX;        // Number of X and Y grid lines
  private static int NumberOfY;        // Number of X and Y grid lines
  private static float ContourValue;    // Value we are contouring.
  private static float prevX, prevY;


//****************************************************************
//                    C o n t o u r
//****************************************************************

   public static IList<List<Point>> Contour( IGridSpatialData Zgrid, float TourValue )

// This function conducts a search of all of the elements of the
// rectangular grid looking for existence of the contour line
// represented by TourValue.

// Once the value is found function TraceContour is called to
// actually trace the contour through the grid. It ultimately
// calls function DoLineTo which must be supplied to actually
// draw the line.

// An array of switches is maintained to flag whether a given
// grid location has been already contoured or not (to prevent
// tracing a contour line more than once.

{
   NumberOfX = Zgrid.PixelWidth;
   NumberOfY = Zgrid.PixelHeight;
   SwitchClear(NumberOfX, NumberOfY);
   ContourValue = TourValue;
   contourNumber=0;
   var contourList = new List<List<Point>>();

// Search for a bottom edge contour
   YReferencePoint = 0;
   YSubPoint = 0;
   
  
   for( XReferencePoint = 1; XReferencePoint<NumberOfX; XReferencePoint++ )
   {
     if (Zgrid.GetValueAtColumnRow(XReferencePoint, 0) <= ContourValue) continue;
     XSubPoint = XReferencePoint - 1;
     if (Zgrid.GetValueAtColumnRow(XSubPoint, 0) > ContourValue) continue;
     TraceContour( Zgrid, contourList );
   }

// Search for a right edge contour
   
   XReferencePoint = NumberOfX - 1;
   XSubPoint       = XReferencePoint;

   for( YReferencePoint =1; YReferencePoint<NumberOfY; YReferencePoint++)
   {
    if( Zgrid.GetValueAtColumnRow(NumberOfX -1, YReferencePoint) <= ContourValue) continue;
    YSubPoint = YReferencePoint -1;
    if (Zgrid.GetValueAtColumnRow(NumberOfX - 1, YSubPoint) > ContourValue) continue;
    TraceContour( Zgrid, contourList);
   }

// Search the top edge.
    
   YReferencePoint = NumberOfY-1;
   YSubPoint       = YReferencePoint;

   for( XReferencePoint = NumberOfX-2; XReferencePoint>=0; XReferencePoint--)
   {
       if (Zgrid.GetValueAtColumnRow(XReferencePoint, NumberOfY - 1) <= ContourValue) continue;
    XSubPoint = XReferencePoint + 1;
    if (Zgrid.GetValueAtColumnRow(XSubPoint, NumberOfY - 1) > ContourValue) continue;
    
    TraceContour( Zgrid, contourList );
    
   }

// Search the left edge
   
   XReferencePoint = 0;
   XSubPoint       = 0;

   for ( YReferencePoint = NumberOfY-2; YReferencePoint>=0;YReferencePoint--)
   {
       if (Zgrid.GetValueAtColumnRow(0, YReferencePoint) <= ContourValue) continue;
    YSubPoint = YReferencePoint + 1;
    if (Zgrid.GetValueAtColumnRow(0, YSubPoint) > ContourValue) continue;
    TraceContour( Zgrid, contourList );
   }

// Search the interior of the array
   
   for( YReferencePoint = 1; YReferencePoint<NumberOfY-1; YReferencePoint++)
   {
    for( XReferencePoint =1; XReferencePoint<NumberOfX;   XReferencePoint++)
    {
     XSubPoint = XReferencePoint-1;
     if (Zgrid.GetValueAtColumnRow(XReferencePoint, YReferencePoint) <= ContourValue) continue;
     if (Zgrid.GetValueAtColumnRow(XSubPoint, YReferencePoint) > ContourValue) continue;
     if( SwitchGet(XReferencePoint,YReferencePoint)) continue;

     YSubPoint = YReferencePoint;
     TraceContour( Zgrid, contourList );
    }
   }
 SwitchClear( 0, 0 ); // All done! delete memory for switches.
       return contourList;
}

//*******************************************************************
//                  T r a c e   C o n t o u r
//********************************************************************

   static void TraceContour(IGridSpatialData Zgrid, List<List<Point>> contourList)
{
  int[] XLookupTable = {  0, 1, 1,  0, 9, 0, -1, -1, 0};
  int[] YLookupTable = { -1, 0, 0, -1, 9, 1,  0,  0, 1};
  bool[] DiagonalTest = {  false, true, false, true, false, true,  false,  true, false};

    float XMidPoint;
    float YMidPoint;
    float ZMidPoint;
    int Locate;
    int XNext;
    int YNext;
    int XMidNext;
    int YMidNext;

  int XTracePoint = XReferencePoint;
  int YTracePoint = YReferencePoint;
  int XTraceSubPoint = XSubPoint;
  int YTraceSubPoint = YSubPoint;
  Drawing = false ;
  
  HorizontalOrVerticalCode:               // Warning - target of a goto!!!!

  Interpolate( Zgrid.x( XTracePoint), Zgrid.y( YTracePoint),
	       Zgrid.GetValueAtColumnRow( XTracePoint, YTracePoint),
	       Zgrid.x( XTraceSubPoint), Zgrid.y( YTraceSubPoint),
           Zgrid.GetValueAtColumnRow(XTraceSubPoint, YTraceSubPoint), contourList);

  Locate = 3*(YTracePoint-YTraceSubPoint) +
		  XTracePoint-XTraceSubPoint + 4;
      Debug.Assert( Locate >=0 && Locate <9 ); 
  
  XNext = XTraceSubPoint + XLookupTable[ Locate ];
  YNext = YTraceSubPoint + YLookupTable[ Locate ];

  // Test to see if the next point is past an edge.

  if( (XNext >= NumberOfX) || (XNext < 0) ||
      (YNext >= NumberOfY) || (YNext < 0)   ) return; 
  
  // Check - if vertical line and been contoured before - all done.

  if( (Locate == 5) && SwitchSet(XTracePoint,YTracePoint) ) return;


  if( !DiagonalTest[Locate] )
  {
      if (Zgrid.GetValueAtColumnRow(XNext, YNext) > ContourValue)
    { XTracePoint = XNext;
      YTracePoint = YNext; } 
    else
    { XTraceSubPoint = XNext;
      YTraceSubPoint = YNext; }
                                              goto HorizontalOrVerticalCode;  
   }
  
  // Diagonals get special treatment = the midpoint of the rectangle
  // has a midpoint which is calculated and used as a contour point.
    
  XMidPoint = ( Zgrid.x( XTracePoint) + Zgrid.x(XNext) )*0.5F;
  YMidPoint = ( Zgrid.y( YTracePoint) + Zgrid.y(YNext) )*0.5F;

  Locate = 3*(YTracePoint-YNext) + XTracePoint - XNext + 4;
      //assert( (Locate >= 0) && (Locate <9) );

  XMidNext = XNext + XLookupTable[Locate];
  YMidNext = YNext + YLookupTable[Locate];
  //assert( ( XMidNext >= 0) && (XMidNext < NumberOfX) );
  //assert( ( YMidNext >= 0) && (YMidNext < NumberOfY) );

  ZMidPoint = ( Zgrid.GetValueAtColumnRow(XTracePoint,YTracePoint) +
		Zgrid.GetValueAtColumnRow(XTraceSubPoint, YTraceSubPoint) +
		Zgrid.GetValueAtColumnRow(XNext, YNext) +
		Zgrid.GetValueAtColumnRow(XMidNext, YMidNext ) )*0.25F;

  if( ZMidPoint > ContourValue)                  goto MidPointGTContourCode;
  // Midpoint less than contour value 
  Interpolate( Zgrid.x(XTracePoint), Zgrid.y(YTracePoint),
	       Zgrid.GetValueAtColumnRow(XTracePoint,YTracePoint),
	       XMidPoint, YMidPoint, ZMidPoint, contourList);

  if( Zgrid.GetValueAtColumnRow(XMidNext,YMidNext) <= ContourValue )

  // Turn off sharp right.... 
  { XTraceSubPoint = XMidNext;
    YTraceSubPoint = YMidNext;
                                              goto HorizontalOrVerticalCode; 
  }

  Interpolate( Zgrid.x(XMidNext), Zgrid.y(YMidNext),
	       Zgrid.GetValueAtColumnRow(XMidNext, YMidNext),
	       XMidPoint, YMidPoint, ZMidPoint, contourList);

  if( Zgrid.GetValueAtColumnRow(XNext,YNext) <= ContourValue)
  // Continue straight through.... 
  { XTracePoint = XMidNext;
    YTracePoint = YMidNext;
    XTraceSubPoint = XNext;
    YTraceSubPoint = YNext;
                                              goto HorizontalOrVerticalCode; 
  }
  // Wide left turn.
  Interpolate(Zgrid.x(XNext), Zgrid.y(YNext),
	      Zgrid.GetValueAtColumnRow(XNext,YNext),
	      XMidPoint, YMidPoint, ZMidPoint, contourList);

  XTracePoint = XNext;
  YTracePoint = YNext;
                                              goto HorizontalOrVerticalCode;

MidPointGTContourCode:                                  // Target of a goto!

  Interpolate( XMidPoint, YMidPoint, ZMidPoint,
	       Zgrid.x(XTraceSubPoint), Zgrid.y(YTraceSubPoint),
	       Zgrid.GetValueAtColumnRow(XTraceSubPoint,YTraceSubPoint), contourList );

  // It may be a sharp left turn.
  if( Zgrid.GetValueAtColumnRow(XNext,YNext) > ContourValue)
  {XTracePoint = XNext;
   YTracePoint = YNext;
                                              goto HorizontalOrVerticalCode;  
  }
  // no 
  Interpolate( XMidPoint, YMidPoint, ZMidPoint,
	       Zgrid.x(XNext), Zgrid.y(YNext),
	       Zgrid.GetValueAtColumnRow(XNext,YNext), contourList );
  // Continue straight through? 
  if( Zgrid.GetValueAtColumnRow( XMidNext,YMidNext) > ContourValue )
  // yes 
  { XTraceSubPoint = XNext;
    YTraceSubPoint = YNext;
    XTracePoint = XMidNext;
    YTracePoint = YMidNext;
                                              goto HorizontalOrVerticalCode;
   }

  // Wide right turn
  Interpolate( XMidPoint, YMidPoint, ZMidPoint,
	       Zgrid.x(XMidNext), Zgrid.y(YMidNext),
	       Zgrid.GetValueAtColumnRow(XMidNext,YMidNext), contourList );

  XTraceSubPoint = XMidNext;
  YTraceSubPoint = YMidNext;
                                              goto HorizontalOrVerticalCode;

}
//**********************************************************************
//                     I n t e r p o l a t e
//
// This routine interpolates between the two points and calls to
// plot the line tracing out the contour. 
//**********************************************************************
static void Interpolate( float XRef, float YRef, float ZRef,
                        float XSub, float YSub, float ZSub, List<List<Point>> contourList) 
{
  float Xdistance,
              Ydistance,
              temp,
              Fraction,
              XPlotLocation,
              YPlotLocation;
               
 if( ZSub < 0.0 ) { Drawing = false; return; }// Don't contour negative areas.

 Xdistance = XRef - XSub;
 Ydistance = YRef - YSub;
 temp = ZRef - ZSub; // watch out- underflow!!!!!
 if( temp > 0.0 )    
      Fraction = (ZRef - ContourValue)/ temp ; 
 else Fraction = 0.0F;
 if ( Fraction > 1.0 ) Fraction = 1.0F;
 XPlotLocation = XRef - Fraction*Xdistance;
 YPlotLocation = YRef - Fraction*Ydistance;
 
 //add this location to the contourList
 if(Drawing)
 {
	 if(!(prevX==XPlotLocation&&prevY==YPlotLocation))
	 {
		//add to existing contour list
		contourList[contourNumber-1].Add( new Point(XPlotLocation,YPlotLocation));
	 }		 
	 prevX = XPlotLocation;
	 prevY = YPlotLocation;
 }
 else
 {
	 //create new contour
	 contourList.Add(new List<Point>());
	 //increment contour number
	 contourNumber++;
	 prevX = prevY = -1;
 }

 //DoLineTo( XPlotLocation, YPlotLocation, Drawing );
    Drawing = true;
 
}

//******************************************************************
//                       S w i t c h e s
//
//  Contouring routine to get and set bit switches. The purpose is
//  flag any reference point in the grid defining the surface if
//  a contour has been traced past it.
//
//********************************************************************

static byte ON = 1;
static int CharPosition, BitOffset;
private static byte[] BitArray;
static int SizeOfBitArray = 0;
const int BitsPerChar = 8 ;
static int FirstDimension = 0;

//****************************************************************
//               S w i t c h    C l e a r
//****************************************************************
static void SwitchClear( int i, int j)
{
// Routine allocates space for i by j bit switches and clears
// them all to zero.

// If i and j are zero just delete the previous array of switches. 

 int SizeNeeded  =(i*j)/BitsPerChar+1;

 if( SizeNeeded <= 1 ) 
   { 
     SizeOfBitArray = 0; 
     return; 
   } 

 FirstDimension = i;
 //assert(FirstDimension > 0 );

 if( SizeNeeded != SizeOfBitArray )
   { 
     BitArray = new byte[SizeNeeded];
     SizeOfBitArray = SizeNeeded;
   }
}
//*********************************************************
//          S w i t c h   P o s n
//*********************************************************
static void SwitchPosn(int i, int j)
{
//  Calculate the location of the switch for SwitchSet and SwitchGet.

 int BitPosition = j*FirstDimension + i;
 CharPosition = BitPosition/BitsPerChar;
 BitOffset = BitPosition - CharPosition*BitsPerChar;
     //assert( CharPosition <= SizeOfBitArray );
}
//*********************************************************
//            S w i t c h   S e t
//*********************************************************
static bool SwitchSet( int i, int j)
{
// Set's switch (i,j) on . It returns the old value of the switch.

 SwitchPosn( i, j );
 byte b = BitArray[CharPosition];
 if( (b &( ON<<BitOffset)) > 0)  return true;
 b |= (byte) (ON<<BitOffset);
 BitArray[CharPosition] = b;
 return false;

}
//***********************************************************
//             S w i t c h   G e t
//***********************************************************
    static bool SwitchGet( int i, int j)
{
// Returns the value of switch (i,j).
 SwitchPosn( i, j );
 if ((BitArray[CharPosition] & (ON << BitOffset)) > 0) return true;
 return false;
}


}
}