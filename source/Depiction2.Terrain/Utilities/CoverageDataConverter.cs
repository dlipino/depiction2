﻿using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Terrain.Terrain14;

namespace Depiction2.Terrain.Utilities
{
    public class CoverageDataConverter
    {
        static private readonly object addElevationToStoryLock = new object();
        #region fields

        // use a static synchronizing object so that all instances of this converter can be accessed one at a time
        private readonly string _fileName;
        private readonly ICartRect boundingBox;
        private readonly bool _cropGridBeforeProjection;
        private readonly IElement _elevationElement;
        private string elevationUnit;

        #endregion

        #region constructor

        public CoverageDataConverter(string elevationFileName, string unit, ICartRect bounds, bool cropGridBeforeProjection, IElement elevationElement)
        {
            boundingBox = bounds;
            _cropGridBeforeProjection = cropGridBeforeProjection;
            _elevationElement = elevationElement;
            _fileName = elevationFileName;
            elevationUnit = unit;
        }

        #endregion

        public List<IElement> ConvertDataToElements()
        {
            var elements = new List<IElement>();
//            lock (addElevationToStoryLock)
//            {
                var partialFileName = string.Empty;
//                var elevation = _elevationElement ?? CreateElevationElement();
                //var elevation = GetElevationElement();
//                if (elevation == null) return null;
                var elevation = CreateElevationElement();
                TerrainCoverage terrain = null;
                if (_fileName != string.Empty)
                {
                    if (_fileName == "GetMap")
                    {
//                        SaveWithKnownExtent(_fileName);//?????
                    }
                    //if (IsCancelled) return new ImportedElements();
                    var elevationProp = elevation.GetDepictionProperty("Terrain");
                    if (elevationProp != null)
                    {
                        terrain = elevationProp.Value as TerrainCoverage;
                    }
                    if (terrain == null)
                    {
                        return null; //elevation.Query<Terrain>(".Terrain");}
                    }
                    //if (IsCancelled) return new ImportedElements();

                    bool downSampled;
                    //
                    //TRAC #2183 fix:
                    //when individual files are added via Add/File/Elevation data, you want
                    //the spacing of the loaded file to supercede the spacing of the existing terrain
                    //data in your story
                    //That has been the convention in Depiction -- new terrain spacing overrides the existing resolution.

                    //Why can't we have multiple terrain data, and have elements 'hook' into the one they want?

                    //                    var pos = new LatitudeLongitudeBase(47.60044, -122.33962);
                    var tempTerrainData = terrain.AddTerrainDataFromFile(_fileName, "", elevationUnit, true,
                                                                         out downSampled, false, _cropGridBeforeProjection /*don't use existing grid spacing -- use new one*/);
                    if (tempTerrainData == null) return null;
                    //if (IsCancelled) return new ImportedElements();
                    if (!terrain.AddTerrainData(tempTerrainData)) return null;

                    var geom = DepictionGeometryService.CreateGeometry(terrain.TerrainBounds);
                    elevation.UpdatePrimaryPointAndGeometry(null,geom);
                    terrain.RestoreProperty(true, elevation);//will this even work?
                    elevation.VisualState = ElementVisualSetting.Image;
//                    if(DepictionAccess.DStory != null)
//                    {
//                        var reg = DepictionAccess.DStory.PrimaryRegion;
//                    }

                }
                else
                {
                    //                    var message = new DepictionMessage("Unable to retrieve elevation data", 4);
                    //                    DepictionAccess.NotificationService.DisplayMessage(message);
                }

//                if (!string.IsNullOrEmpty(partialFileName))
//                {
//                    var prop = new DepictionElementProperty("EID", partialFileName);
//                    prop.VisibleToUser = false;
//                    elevation.AddPropertyOrReplaceValueAndAttributes(prop, false);
//                }
                elements.Add(elevation);
//            }
            return elements;
        }

        /// <summary>
        /// Returns the single elevation element. 
        /// If one has not been created yet, it creates a new one with
        /// a well-formed and empty Terrain object.
        /// </summary>
        /// <returns></returns>
        private IElement CreateElevationElement()
        {
            //            var elevation =
            //                DepictionAccess.DStory.AllElements.FirstOrDefault(t=>t.Tags.Contains("Elevation"));
            //            if (elevation == null)
            //            {
            System.Console.WriteLine("creating elevation element");
            IElement elevation = null;
            if (DepictionAccess.TemplateLibrary != null)
            {

                var prototype = DepictionAccess.TemplateLibrary.FindElementTemplate("Depiction.Plugin.Elevation");
                elevation = ElementAndScaffoldService.CreateElementFromScaffoldAndGeometry(prototype, null);
            }
            if (elevation == null)
            {
                elevation = new DepictionElement("Generic.Elevation");
            } 
            var terrainLocal = new TerrainCoverage { HasGoodData = false };
            //Initial value was gutting used, but didn't work very well because of referencing and restoring.
            var terrainProperty = new ElementProperty("Terrain", "Terrain", terrainLocal);
            elevation.AddUserProperty("Terrain", "Terrain", terrainLocal);
            elevation.AddTag("Elevation");
            //            }
            return elevation;
        }

//        private void SaveWithKnownExtent(string filename)
//        {
//            //save data.DataFilename as GeoTiff...can we do that?
//            var terrain = new Terrain();
//            terrain.AddFromFileWithoutExtents(filename, boundingBox.Top, boundingBox.Bottom,
//                                              boundingBox.Right, boundingBox.Left);
//            //the file would now be saved as a georeferenced GeoTiff file with the
//            //supplied extents. It will be in a geographic coord system with a WGS84 datum
//        }
    }
}