﻿using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Terrain.ImportExtensions
{

    //        Description = "The U.S. Geological Survey (USGS) provides elevation data across the U.S. at varying levels of detail via the National Elevation Dataset (NED). \"30m\" resolution provides the elevation above sea level at a point every 30 meters. Additional data can be found at http://seamless.usgs.gov")]
    //    [RegionSourceMetadata(Name = "DepictionSeamlessImporter", DisplayName = "Elevation Data (Seamless)", Author = "Depiction Inc.",
    //   ShortName = "Seamless Elevation")]

    public class SeamlessElevationImporter : IRegionSourceExtension
    {
        #region construction /destruction
        public void Dispose()
        {

        }
        #endregion

        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }

        public void SimpleElementImport(ICartRect region, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", region);

            var operationThread = new SeamlessElevationImporterBackgroundService();
            var name = string.Format("Elevation Data (Seamless)");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }

}
