﻿using System;
using System.Collections.Generic;
using System.Linq;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Extensions;
using Depiction2.Terrain.Utilities;

namespace Depiction2.Terrain.ImportExtensions
{
    //    [DepictionDefaultImporterMetadata("DepictionElevationFileReader", 
    //        new[] { ".adf", ".tiff", ".tif", ".gtiff", ".hgt", ".dem", ".bt" },
    //        new[] { "Depiction.Plugin.Elevation", "Depiction.Plugin.AutoDetect" },
    //        FileTypeInformation="Elevation",DisplayName = "Depiction elevation file reader",
    //        ElementTypesNotSupported = new[] { "Depiction.Plugin.Image" })]
    [DepictionFileLoaderMetadata(Name = "DepictionElevationFileReader", Description = "Depiction elevation file reader",
    DisplayName = "Elevation Files", Author = "Depiction Inc.",
    SupportedExtensions = new[] { ".adf", ".tiff", ".tif", ".gtiff", ".hgt", ".dem", ".bt" })]
    public class DepictionElevationFileImporter : IFileLoaderExtension
    {
        #region construction /destruction

        public void Dispose()
        {

        }
        #endregion

        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName, ICartRect boundingRect)
        {
            if (string.IsNullOrEmpty(sourceLocation)) return;
            var parameters = new Dictionary<string, object>();//ConvertStringStringDictionayrToStringObject(inParameters);
            if(boundingRect != null)
            {
                parameters.Add("Area", boundingRect);
            }
            parameters.Add("fileName", sourceLocation);
            var elevationProvider = new DepictionElevationElementProvider();

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(elevationProvider);
            elevationProvider.UpdateStatusReport("Starting elevation importer");
            elevationProvider.StartBackgroundService(parameters);
        }


        private Dictionary<string, object> ConvertStringStringDictionayrToStringObject(Dictionary<string, string> param)
        {
            var parameters = new Dictionary<string, object>();
            if (param != null)
            {
                foreach (var pair in param)
                {
                    parameters.Add(pair.Key, pair.Value);
                }
            }
            return parameters;
        }


    }

    public class DepictionElevationElementProvider : BaseDepictionBackgroundThreadOperation
    {
        private string elevationUnit = "m";


        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            bool unitSpecified = false;
            UpdateStatusReport("Creating elevation element");
            if (args.ContainsKey("units"))
            {
                unitSpecified = true;
                elevationUnit = args["units"].ToString();
            }
            if (!unitSpecified)
            {
                //TODO get the units figured out
                //                var inMeters =
                //                    DepictionMessageService.DisplayModalYesNoMessageBox(
                //                        "Depiction only supports units of METERS or FEET for elevation data. Does this file contain elevation in METERS?",
                //                        "Elevation data units");
                //                if (inMeters == null) return false;
                //                elevationUnit = ((bool)inMeters) ? "m" : "ft";
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            try
            {
                var fileName = parameters["fileName"] as string;
                ICartRect depictionRegion = null;
                if(parameters.ContainsKey("Area"))
                {
                     depictionRegion = parameters["Area"] as ICartRect;
                }

                var terrainConverter = new CoverageDataConverter(fileName, elevationUnit, depictionRegion, true, null);

                UpdateStatusReport("Converting data to elevation");
                var elements = terrainConverter.ConvertDataToElements();
                if (parameters.ContainsKey("Tag"))
                {
                    var tag = parameters["Tag"] as string;
                    if (!string.IsNullOrEmpty(tag))
                    {
                        foreach (var element in elements)
                        {
                            element.AddTag(tag);
                        }
                    }
                }
                return elements;
            }
            catch (Exception ex)
            {
                UpdateStatusReport("Error creating elevation. See error log for details.");
                //                DepictionExceptionHandler.HandleException(ex, false, true);
                return null;
            }
        }

        protected override void ServiceComplete(object args)
        {
            UpdateStatusReport("Adding element to depiction");
            var elementsEnum = args as IEnumerable<IElement>;
            if (elementsEnum == null) return;
            var elements = elementsEnum.ToList();
            if (elements.Any() && DepictionAccess.DStory != null)
            {
                var story = DepictionAccess.DStory;
                //For now allow multiple elevations
                story.AddElements(elements);
                story.AddRevealerWithElements("Elevation - File", elements);
            }
        }

    }
}