using System.Collections.Generic;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Terrain.Utilities.Wxs;

namespace Depiction2.Terrain.ImportExtensions
{
    public class WcsElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        private ICartRect depictionRegion;
        string elevationUnit = "m";
        private string tags = "";
        private WcsDataProvider dataProvider = null;

        #region Overrides of BaseDepictionBackgroundThreadOperation

        public override void StopBackgroundService()
        {
            if (dataProvider != null)
            {
                dataProvider.Cancel();
            }
            base.StopBackgroundService();
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            if (args.ContainsKey("Tag"))
            {
                tags = args["Tag"].ToString();
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            if (!parameters.ContainsKey("Area")) return null;
            var area = parameters["Area"] as ICartRect;
            depictionRegion = area;
            return GetRequestedFileName(area, parameters);
        }

        protected override void ServiceComplete(object args)
        {
            var fileName = args as string;
            if (fileName == null) return;
            var elevationFileLoader = new DepictionElevationFileImporter();

            var parameters = new Dictionary<string, string>();
            parameters.Add("units", elevationUnit);
            UpdateStatusReport("Retrieved elevation file");
            //TODO make tags sendable as a list
            if (string.IsNullOrEmpty(tags))
            {
                //parameters.Add("Tag", Tags.DefaultQuickStartTag);
            }
            else
            {
                parameters.Add("Tag", tags);
            }
            elevationFileLoader.ImportElements(fileName, null, string.Empty, depictionRegion);
        }

        #endregion

        private string GetRequestedFileName(ICartRect area, Dictionary<string, object> parameters)
        {
            depictionRegion = area;
            string url = "";
            if (parameters.ContainsKey("url"))
                url = parameters["url"].ToString();

            if (parameters.ContainsKey("units"))
                elevationUnit = parameters["units"].ToString() ?? "m";
            string layerName = "";
            if (parameters.ContainsKey("layerName"))
                layerName = parameters["layerName"].ToString() ?? "";

            string imageFormat = "GeoTIFF";
            if (parameters.ContainsKey("format"))
                imageFormat = parameters["format"].ToString();

            string request = "";
            if (parameters.ContainsKey("request"))
                request = parameters["request"].ToString() ?? "";

            var descriptor = "";
            if (parameters.ContainsKey("name"))
                descriptor = parameters["name"].ToString() ?? "";

            dataProvider = new WcsDataProvider(area, url, layerName, imageFormat, request, descriptor);
            Response data;
            dataProvider.RequestFileData("", out data);

            if (data != null && data.IsRequestSuccessful)
            {
                return data.ResponseFile;
            }
            return null;
        }
    }
}