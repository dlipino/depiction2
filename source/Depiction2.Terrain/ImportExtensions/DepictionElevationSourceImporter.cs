﻿using System;
using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Terrain.ImportExtensions
{
    //    {[description, The U.S. Geological Survey (USGS) provides elevation data across the U.S. at varying levels of detail via the National Elevation Dataset (NED). "30m" resolution provides the elevation above sea level at a point every 30 meters. Depiction uses this in a grid to provide a 3D model of the Earth's surface for flood, line-of-sight and other interactions. Hosted by Depiction.]}
    //            Description = "The U.S. Geological Survey (USGS) provides elevation data across the U.S. at varying levels of detail via the National Elevation Dataset (NED). \"30m\" resolution provides the elevation above sea level at a point every 30 meters. Additional data can be found at http://seamless.usgs.gov")]
    [RegionSourceMetadata(Name = "DepictionNEDElevation", DisplayName = "Elevation NED (Depiction)", Author = "Depiction Inc.",
   ShortName = "NED Elevation (Depiction)", RequiresParameters = false)]

    public class DepictionElevationSourceImporter : IRegionSourceExtension
    {
        public void Dispose()
        {

        }
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            SimpleElementImport(regionOfInterest.RegionRect, sourceInfo.Parameters);
        }
        public void SimpleElementImport(ICartRect region, Dictionary<string, string> inParameters)
        {
            //            var parameters = new Dictionary<string, object>();
            //            parameters.Add("RegionBounds", region);

            var operationThread = new WcsElementProviderBackground();

            var name = string.Format("Elevation (NED 30m - Depiction)");
            var operationParams = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            operationParams.Add("name", name);
            operationParams.Add("url", "http://depictioninc.com/wcs/");

            operationParams.Add("layerName", "depiction:test");
            operationParams.Add("format", "GeoTIFF");
            operationParams.Add("units", "m");
            operationParams.Add("request", "GetCoverage");
            operationParams.Add("Area", region);
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(operationParams);
        }


    }
}
