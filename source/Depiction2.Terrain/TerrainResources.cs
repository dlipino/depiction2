﻿using System;
using System.Windows;
using Depiction2.API.Extension.Resources;

namespace Depiction2.Terrain
{
    [ResourceExtensionMetadata(DisplayName = "Terrain Resource", Name = "ResourcesTerrain", Author = "Depiction Inc.", ExtensionPackage = "Default", ResourceName = "TerrainResources")]
    public class TerrainResources : IDepictionResourceExtension
    {

        public ResourceDictionary ExtensionResources
        {
            get
            {
                var uri = new Uri("/Depiction2.Terrain;Component/Resources/Images/TerrainResources.xaml",UriKind.RelativeOrAbsolute);
                var rd = new ResourceDictionary {Source = uri};
                return rd;
            }
        }

        public void Dispose()
        {

        }
    }
}