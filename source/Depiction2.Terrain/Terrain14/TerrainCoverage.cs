﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using Depiction2.API;
using Depiction2.API.Geo;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.Base.Utilities;

namespace Depiction2.Terrain.Terrain14
{
    public class TerrainCoverage : ICoverage, IRestorable
    {
        public TerrainData TerrainPlusStructure { get; private set; }
        private TerrainData terrainData;

        private readonly object addTerrainDataFromFileLock = new object();

        #region Constructor

        public TerrainCoverage()
        {
            Init(new TerrainData());
        }
        public TerrainCoverage(TerrainData terrainData)
        {
            Init(terrainData);
            CreateTerrainPlusStructure();
        }

        private void Init(TerrainData initTerrainData)
        {
            terrainData = initTerrainData;
            TerrainPlusStructure = new TerrainData();
        }
        #endregion

        #region Properties

//        public string DepictionDatum
//        {
//            get
//            {
//                return
//                "GEOGCS[\"GCS_WGS_1984\"," +
//             "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
//             "PRIMEM[\"Greenwich\",0]," +
//             "UNIT[\"Degree\",0.0174532925199433]" +
//            "]";
//            }
//        }

        public bool HasGoodData
        {
            get { return terrainData.HasGoodData; }
            set { terrainData.HasGoodData = value; }
        }

        public ICartRect TerrainBounds
        {
            get
            {
                if (terrainData == null) return null;
                var rect = new CartRect(terrainData.GetTopLeftPos(),terrainData.GetBottomRightPos());
                return rect;
            }
        }
        #endregion

        #region private heper methods
        private string GetColorMapFile(string tempDir)
        {
            //legacy file
            var cmtName = "default_relative.cmt";
            var helperFile = string.Empty;
            var assembly = Assembly.GetCallingAssembly();
            
            var dllLocation = Path.GetDirectoryName(assembly.Location);
            if (!string.IsNullOrEmpty(dllLocation))
            {
                var resourceColorMapFile = Path.Combine(dllLocation, "Resources", cmtName);
                if(File.Exists(resourceColorMapFile))
                {
                    helperFile = resourceColorMapFile;
                }
            }
            if(string.IsNullOrEmpty(helperFile))
            {
                //i guess we have to create the file
                var fullOutCmt = Path.Combine(tempDir, cmtName);
                var fileText = "colormap1" + Environment.NewLine +
                               "relative: 1" + Environment.NewLine +
                               "size 5" + Environment.NewLine +
                               "elev 0.000000 color 32 176 32" + Environment.NewLine +
                               "elev 0.000000 color 64 224 64" + Environment.NewLine +
                               "elev 0.000000 color 224 192 160" + Environment.NewLine +
                               "elev 0.000000 color 224 128 16" + Environment.NewLine +
                               "elev 0.000000 color 224 224 224";

               using (var writer = new StreamWriter(fullOutCmt))
               {
                   writer.Write(fileText);
               }
                helperFile = fullOutCmt;
            }
            return helperFile;
        }
        public bool GenerateTerrainVisual(string tempDir, string fileName)
        {
            if (string.IsNullOrEmpty(tempDir) || string.IsNullOrEmpty(fileName)) return false;
            if (!Directory.Exists(tempDir)) return false;
            var tempFileName = Path.Combine(tempDir, fileName);
            //how lame, since the gdal stuff is not sure to be there pull it from resource

            var helperFile = GetColorMapFile(tempDir);

            bool createdVisual = SaveTo24BitRGB(tempFileName, 90, helperFile);
            return createdVisual;
        }

        private bool CreateTerrainPlusStructure()
        {
            lock (terrainData)
            {
                if (terrainData == null || !terrainData.HasGoodData) return false;
            }
            var tempTerrainPlusStructure = new TerrainData();
            if (tempTerrainPlusStructure.CreateFromExisting(terrainData))
            {
                lock (TerrainPlusStructure)
                {
                    TerrainPlusStructure = tempTerrainPlusStructure;
                }
                return true;
            }
            return false;
        }

        private static float GetElevationAndReadoutFromDataGrid(TerrainData dataGrid, Point location, string units,int precision, out string readout)
        {
            readout = ", Elevation: ";
            float elevation;
            if (!dataGrid.HasElevation(location))
            {
                readout += "Unknown";
                return 0;
            }

            try
            {
                if (dataGrid.HasNoData(location) || !dataGrid.HasGoodData)
                {
                    readout += "No Data";
                    return 0;
                }
                elevation = dataGrid.GetClosestElevationValue(location);

                if (units == "ft") elevation = elevation * 3.2808399F;

                var prec = precision;
                readout += String.Format("{0} {1}",
                                         elevation.ToString("F" + prec), units);
                return elevation;
            }
            catch (Exception e)
            {
                //DepictionExceptionHandler.HandleException("Unable to get elevation data for location", e, false);
                readout = "";
                return 0;
            }
        }

        /// <summary>
        /// Create a bounding box that is a wee bit bigger than the region bounds,
        /// so that we can then crop the erroneous edge pixels that come from the data source.
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="paddingPercent"></param>
        /// <returns></returns>
        private static Rect CreateDilatedBoundingBox(Rect bbox, double paddingPercent)
        {
            double offsetDistanceX = Math.Abs(bbox.Left - bbox.Right) * paddingPercent;
            double offsetDistanceY = Math.Abs(bbox.Top - bbox.Bottom) * paddingPercent;
            //pad the region bbox with paddingPercent*100% on all sides so WCS request can accomodate
            //cropping and warping from Geo to Mercator, etc.
            //
            return new Rect(bbox.X - offsetDistanceX, bbox.Y - offsetDistanceY, bbox.Width + (2 * offsetDistanceX), bbox.Height + (2 * offsetDistanceY));
            //            return new Rect
            //            {
            //                Top = bbox.Top + offsetDistanceY,
            //                Bottom = bbox.Bottom - offsetDistanceY,
            //                Left = bbox.Left - offsetDistanceX,
            //                Right = bbox.Right + offsetDistanceX,
            //                MapImageSize = bbox.MapImageSize
            //            };
        }
        #endregion

        #region public helper methods

        public void RestoreProperty(bool notifyChange, IElement propertyOwner)
        {
            CreateTerrainPlusStructure();
//            if (DepictionAccess.DStory == null) return;
//            var elevation =
//                DepictionAccess.DStory.AllElements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.Elevation"));

//            if (elevation != null)
//            {
            lock (propertyOwner)
                {
                    var folderService = new DepictionFolderService(false);
                    var tempFolder = folderService.FolderName;
                    var imageName = string.Format("baseTerrain-{0}.jpg", propertyOwner.ElementId);
                    var createdVisual = GenerateTerrainVisual(tempFolder, imageName);
                    if (createdVisual)
                    {
                        var fullName = Path.Combine(tempFolder, imageName);
                        var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullName);
                        if(DepictionAccess.DStory !=null)
                        {
                            DepictionAccess.DStory.AllImages.AddImage(imageName, image, true);
                        }
                        propertyOwner.ImageResourceName = imageName;
                    }
                    folderService.Close();
                }
//            }
        }

        public void ClearVisual()
        {
        }
        /// <summary>
        /// Get coverage value at a given location in string format
        /// </summary>
        public string GetValueForDisplay(Point location, MeasurementSystem measureSystem, int precision)
        {
            float trueElevation;
            float modifiedElevation;
            string units = string.Empty;
            string trueElevationReadout;
            string modifiedElevationReadout;

            if (measureSystem.Equals(MeasurementSystem.Imperial))
                units = "ft";
            else
                units = "m";

            trueElevation = GetElevationAndReadoutFromDataGrid(terrainData, location, units, precision, out trueElevationReadout);
            modifiedElevation = GetElevationAndReadoutFromDataGrid(TerrainPlusStructure, location, units, precision,
                                                                   out modifiedElevationReadout);
            if (trueElevation == modifiedElevation)
                return trueElevationReadout;
            var prec = precision;
            var elevationChange = modifiedElevation - trueElevation;
            return String.Format("{0} ({1} {2} structure)",
                                 modifiedElevationReadout,
                                 elevationChange.ToString("F" + prec),
                                 units);
        }

        public bool LineOfSight(Point point1, Point point2, ArrayList intersectionList)
        {
            double x1, y1, x2, y2;
            x1 = point1.X;
            y1 = point1.Y;
            x2 = point2.X;
            y2 = point2.Y;
            var success = terrainData.LineOfSight(x1, y1, x2, y2, intersectionList);
            return success;
        }
        public void Create(double south, double north, double east, double west, int width, int height, bool floatgrid, int defaultValue, string projFilename, int wktLength, bool updateVisual)
        {
            terrainData = new TerrainData();
            terrainData.CreateFromExisting(south, north, east, west, width, height, floatgrid, defaultValue, projFilename, wktLength);
            CreateTerrainPlusStructure();
        }
        public bool AddTerrainData(TerrainData data)
        {
            lock (terrainData)
            {
                Init(data);
            }
            return CreateTerrainPlusStructure();
        }
        #endregion

        #region Terran file methods

        public void SaveToGeoTiff(string filename)
        {
            TerrainPlusStructure.SaveToGeoTiff(filename);
        }

        public bool SaveTo24BitRGB(string filename, int quality, string colormapFileName)
        {
            //            DepictionAccess.NotificationService.SendNotification(
            //                string.Format("Generating visual for elevation data. Please wait..."), 10);
            return TerrainPlusStructure.SaveTo24BitRGB(filename, quality, colormapFileName);
        }

        public bool AddFromFileWithoutExtents(string filename, double top, double bottom, double right, double left)
        {
            var result = terrainData.AddFromFileWithoutExtents(filename, top, bottom, right, left);
            return result;
        }
        public TerrainData AddTerrainDataFromFile(string filename, string coordSystem, string elevationUnit, bool showDownSampleMessage, out bool downSampled, bool useExistingGridSpacing, bool cropGridBeforeProjection)
        {
            TerrainData tempTerrainData;
            downSampled = false;

            lock (addTerrainDataFromFileLock)
            {
                tempTerrainData = new TerrainData();
                tempTerrainData.LoadElevationFile(filename);
                return tempTerrainData;

//                if (!terrainData.HasGoodData)
//                {
//                    tempTerrainData = new TerrainData();
//                    //
//                    //this terraindata object gets 'written' over by the Add method when we
//                    //add actual data from a file...so, initial dimensions are not important -- the latlong bounds are important, though.
//                    //
//                    var regionExtent = new Rect();//DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
//                    var paddedBox = CreateDilatedBoundingBox(regionExtent, 0.1);
//                    var creationSuccess = tempTerrainData.CreateFromExisting(paddedBox.Bottom,
//                                                                             paddedBox.Top,
//                                                                             paddedBox.Right,
//                                                                             paddedBox.Left, 5,
//                                                                             5, true, -32768, "", 0);
//                    //tempTerrainData is in Mercator, which is default if you don't pass a projection file name
//                    if (!creationSuccess)
//                    {
//                        //DepictionAccess.NotificationService.SendNotification(
//                        //    string.Format("Unable to add elevation data due to insufficient memory"), 10);
//                        return null;
//
//                    }
//
//                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
//                    //crop it to the region bounding box
//                    tempTerrainData.CropElevationData(regionExtent);
//                    if (downSampled && showDownSampleMessage)
//                    {
//                        // DepictionAccess.NotificationService.SendNotification(
//                        //     string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));
//
//                    }
//                    return tempTerrainData;
//                }
                //terrainData already exists
                tempTerrainData = new TerrainData();
                if (tempTerrainData.CreateFromExisting(terrainData))
                {
                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
                    if (downSampled && showDownSampleMessage)
                    {
                        //DepictionAccess.NotificationService.SendNotification(
                        //    string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));

                    }
                    return tempTerrainData;
                }
                return null;
            }
        }
        #endregion

        #region TerrainData methods

        public int GetGridWidthInPixels()
        {
            return TerrainPlusStructure.GetGridWidthInPixels();
        }

        public int GetGridHeightInPixels()
        {
            return TerrainPlusStructure.GetGridHeightInPixels();
        }

        public double GetGridResolution()
        {
            return TerrainPlusStructure.GetGridResolution();
        }

        public int GetRow(double lat)
        {
            return TerrainPlusStructure.GetRow(lat);
        }

        public int GetColumn(double lon)
        {
            return TerrainPlusStructure.GetColumn(lon);
        }

        public double GetLatitude(int row)
        {
            return TerrainPlusStructure.GetLatitude(row);
        }

        public double GetLongitude(int col)
        {
            return TerrainPlusStructure.GetLongitude(col);
        }

        public float GetValueAtGridCoordinate(int col, int row)
        {
            return TerrainPlusStructure.GetElevationValueFromGridCoordinate(col, row);

        }

        public float GetElevationValueClosest(Point latitudeLongitude)
        {
            return TerrainPlusStructure.GetClosestElevationValue(latitudeLongitude);
        }

        public void SetValueAtGridCoordinate(int col, int row, float value)
        {
            TerrainPlusStructure.SetElevationValue(col, row, value);
        }

        public float GetInterpolatedElevationValue(Point latitudeLongitude)
        {
            return TerrainPlusStructure.GetInterpolatedElevationValue(latitudeLongitude);
        }

        public float GetConvolvedValue(int col, int row, int kernel)
        {
            return TerrainPlusStructure.GetConvolvedValue(col, row, kernel);
        }

        #endregion

        #region TerrainPlusStructure methods

        public Point GetTopLeftPosition()
        {
            return TerrainPlusStructure.GetTopLeftPos();
        }

        public Point GetBottomRightPosition()
        {
            return TerrainPlusStructure.GetBottomRightPos();
        }

        public string GetProjectionInWkt()
        {
            return TerrainPlusStructure.GetProjectionInWkt();
        }

        public bool IsFloatMode()
        {
            return TerrainPlusStructure.IsFloatMode();
        }

        #endregion

        #region XmlSerialization stuff

//        public const string DepictionXmlNameSpace = "http://depiction.com";
//        public void ReadXml(XmlReader reader)
//        {
//            //Hack really hack hack
//            if (string.IsNullOrEmpty(DepictionAccess.LastDpnFileLocation)) return;
//            ReadXml(reader,DepictionAccess.LastDpnFileLocation);
//        }
//        public void ReadXml(XmlReader reader, string parentZipFile)
//        {
//            string ns = DepictionXmlNameSpace;
//            if (terrainData == null)
//            {
//                terrainData = new TerrainData();
//            }
//            var difList = new List<Point3D>();
//            //Legacy 1.2.2 
//            if (reader.Name.Equals("Terrain"))
//            {
//                reader.Read();
//                if (reader.Name.Equals("terrainData"))
//                {
//                    reader.ReadStartElement();
//                    terrainData.ReadXml(reader, parentZipFile);
//                    reader.ReadEndElement();
//                }
//                if (reader.Name.Equals("terrainPlusStructure"))
//                {
//                    reader.ReadStartElement();
//                    terrainData.ReadXml(reader, parentZipFile);
//                    reader.ReadEndElement();
//                }
//                else if (reader.Name.Equals("TerrainDifferences"))
//                {
//                    reader.ReadStartElement("TerrainDifferences", ns);
//                    while (reader.IsStartElement("terrainDifference"))
//                    {
//                        reader.ReadStartElement("terrainDifference");//, ns);
//                        var ret = (Point3D)Depiction14FileOpenHelpers.GetSerializer(typeof(Point3D)).ReadObject(reader);
//                        reader.ReadEndElement();
//                        difList.Add(ret);
//                        //                        difList.Add(SerializationService.Deserialize<Point3D>("terrainDifference", reader));
//                    }
//                    reader.ReadEndElement();
//                    CreateTerrainPlusStructure();
//                    TerrainPlusStructure.SetElevationValues(difList);
//                }
//                else { CreateTerrainPlusStructure(); }
//                //reader.ReadEndElement();
//                return;
//            }
//
//            if (reader.Name.Equals("terrainData"))
//            {
//                reader.ReadStartElement();
//                terrainData.ReadXml(reader, parentZipFile);
//                reader.ReadEndElement();
//            }
//            if (reader.Name.Equals("terrainPlusStructure"))
//            {
//                reader.ReadStartElement();
//                terrainData.ReadXml(reader, parentZipFile);
//                reader.ReadEndElement();
//            }
//            else if (reader.Name.Equals("TerrainDifferences"))
//            {
//                reader.ReadStartElement("TerrainDifferences", ns);
//                while (reader.IsStartElement("terrainDifference"))
//                {
//                    reader.ReadStartElement("terrainDifference");//, ns);
//                    var ret = (Point3D)Depiction14FileOpenHelpers.GetSerializer(typeof(Point3D)).ReadObject(reader);
//                    reader.ReadEndElement();
//                    difList.Add(ret);
//                }
//
//                CreateTerrainPlusStructure();
//                TerrainPlusStructure.SetElevationValues(difList);
//                reader.ReadEndElement();
//            }
//            else
//                CreateTerrainPlusStructure();
//        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        
        public void WriteXml(XmlWriter writer)
        {
            //            var ns = SerializationConstants.DepictionXmlNameSpace;
            //            SerializationService.SerializeObject("terrainData", terrainData, writer);
            //            var difs = TerrainPlusStructure.FindDifferenceWith(terrainData);
            //            if (difs.Count > 0)
            //            {
            //                writer.WriteStartElement("TerrainDifferences", ns);
            //                foreach (var dif in difs)
            //                {
            //                    SerializationService.SerializeObject("terrainDifference", dif, writer);
            //                }
            //                writer.WriteEndElement();
            //            }

        }

        #endregion
    }
}