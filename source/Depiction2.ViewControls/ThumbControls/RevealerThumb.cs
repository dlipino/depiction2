﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.EditingViewModels;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.RevealerViewModels;

namespace Depiction2.ViewControls.ThumbControls
{
    public enum GeneralThumbType
    {
        None,
        Main,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        TopMiddle,
        BottomMiddle,
        LeftMiddle,
        RightMiddle,
        ScaleUp,
        ScaleDown
    }

    public class RevealerThumb : Thumb
    {
        #region variables

        private const double VisibleMinSize = 2;
        private const double ThumbWidth = 12;
        private const double ThumbHeight = 12;

        #endregion

        #region Dep props


        public double WorldScale
        {
            get { return (double)GetValue(WorldScaleProperty); }
            set { SetValue(WorldScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldScale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldScaleProperty =
            DependencyProperty.Register("WorldScale", typeof(double), typeof(RevealerThumb), new UIPropertyMetadata(1d));



        public GeneralThumbType ThumbType
        {
            get { return (GeneralThumbType)GetValue(ThumbTypeProperty); }
            set { SetValue(ThumbTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThumbType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThumbTypeProperty =
            DependencyProperty.Register("ThumbType", typeof(GeneralThumbType), typeof(RevealerThumb),
            new UIPropertyMetadata(GeneralThumbType.None, SetThumbType));
        #region Events for dep props

        private static void SetThumbType(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thumb = d as RevealerThumb;
            if (thumb == null) return;
            var newValue = (GeneralThumbType)e.NewValue;
            switch (newValue)
            {
                case GeneralThumbType.Main:
                    thumb.Cursor = Cursors.SizeAll;
                    break;
                case GeneralThumbType.Top:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    break;
                case GeneralThumbType.Bottom:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
                case GeneralThumbType.Right:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    break;
                case GeneralThumbType.Left:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    break;
                case GeneralThumbType.TopRight:
                    thumb.Cursor = Cursors.SizeNESW;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    break;
                case GeneralThumbType.TopLeft:
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    thumb.Cursor = Cursors.SizeNWSE;
                    break;
                case GeneralThumbType.BottomRight:
                    thumb.Cursor = Cursors.SizeNWSE;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
                case GeneralThumbType.BottomLeft:
                    thumb.Cursor = Cursors.SizeNESW;
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    break;
                case GeneralThumbType.TopMiddle:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Top;
                    thumb.HorizontalAlignment = HorizontalAlignment.Center;
                    thumb.Width = ThumbWidth;
                    thumb.Height = ThumbHeight;
                    thumb.Margin = new Thickness(0, -ThumbHeight / 2d, 0, 0);
                    break;
                case GeneralThumbType.BottomMiddle:
                    thumb.Cursor = Cursors.SizeNS;
                    thumb.VerticalAlignment = VerticalAlignment.Bottom;
                    thumb.HorizontalAlignment = HorizontalAlignment.Center;
                    thumb.Width = ThumbWidth;
                    thumb.Height = ThumbHeight;
                    thumb.Margin = new Thickness(0, 0, 0, -ThumbHeight / 2d);
                    break;
                case GeneralThumbType.LeftMiddle:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.VerticalAlignment = VerticalAlignment.Center;
                    thumb.HorizontalAlignment = HorizontalAlignment.Left;
                    thumb.Width = ThumbWidth;
                    thumb.Height = ThumbHeight;
                    thumb.Margin = new Thickness(-ThumbHeight / 2d, 0, 0, 0);
                    break;
                case GeneralThumbType.RightMiddle:
                    thumb.Cursor = Cursors.SizeWE;
                    thumb.VerticalAlignment = VerticalAlignment.Center;
                    thumb.HorizontalAlignment = HorizontalAlignment.Right;
                    thumb.Width = ThumbWidth;
                    thumb.Height = ThumbHeight;
                    thumb.Margin = new Thickness(0, 0, -ThumbHeight / 2d, 0);
                    break;
            }
        }

        #endregion
        #endregion

        #region Constructor

        public RevealerThumb()
        {
            DragStarted += RevealerThumbRotate_DragStarted;
            DragDelta += RevealerThumb_DragDelta;
            Focusable = true;
        }
        #endregion
        #region a hack for shift mouse move  -> rotation, thanks to copy paster from 1.4

        private Point centerPoint;
        private Vector startVector;
        private Panel rotationElementMainParentCanvas;
        private double initialAngle = 0;
        private void RevealerThumbRotate_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (!(Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                rotationElementMainParentCanvas = null;
            }
            var thumb = sender as Thumb;
            if (thumb == null) return;
            var dc = thumb.DataContext as ImageRegistrationViewModel;
            if (dc == null) return;
            var rotation = dc.Rotation;
            if (rotation == null) return;//Should never be null;
            initialAngle = rotation.Angle;
            //Need to find what ever the thumb is rotating (for the size)

            var thumbParent = thumb.Parent as FrameworkElement;
            if (thumbParent == null) return;

            rotationElementMainParentCanvas = VisualTreeHelper.GetParent(thumbParent) as Panel;

            if (rotationElementMainParentCanvas != null)
            {
                centerPoint = thumbParent.TranslatePoint(
                    new Point(thumbParent.ActualWidth * thumbParent.RenderTransformOrigin.X,
                              thumbParent.ActualHeight * thumbParent.RenderTransformOrigin.Y),
                    rotationElementMainParentCanvas);

                Point startPoint = Mouse.GetPosition(rotationElementMainParentCanvas);
                startVector = Point.Subtract(startPoint, centerPoint);
            }
        }
        #endregion
        private void RevealerThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = sender as RevealerThumb;
            if (thumb == null) return;
            var delta = new Point(e.HorizontalChange, e.VerticalChange);
            var context = thumb.DataContext as ElementDisplayerMapViewModel;
            if (context != null)
            {
                if(!ChangingRevealerValues(context, delta, thumb))
                {
                    e.Handled = false;
                }
                return;
            }

            //Man this got really ugly really fast
            //If there is no data context just mess with the control, Image registration basicaly
            //i hate copy/pasting legacy stuff

            var thumbType = thumb.ThumbType;
            var tp = thumb.DataContext as ImageRegistrationViewModel;
            if (tp != null)
            {
                if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) &&
                    rotationElementMainParentCanvas != null)
                {

                    RotateThumb.DoARotationOnThumbWithInputs(thumb, rotationElementMainParentCanvas,
                        startVector, centerPoint, initialAngle);
                }
                else
                {
                    ImageRegistration(tp, thumbType, delta);
                }
                return;
            }
            var cc = thumb.TemplatedParent as Control;
            if (cc != null)
            {
                RegionSelectorSizeChanging(cc, delta, thumbType);

            }
        }

        #region region creation
        private void RegionSelectorSizeChanging(Control cc, Point delta, GeneralThumbType thumbType)
        {
            var minSize = 20;
            var hChange = delta.X;
            var vChange = delta.Y;
            var left = Canvas.GetLeft(cc);
            var top = Canvas.GetTop(cc);
            if(double.IsNaN(left))
            {
                Canvas.SetLeft(cc,0);
                left = 0;
            }
            if (double.IsNaN(top))
            {
                Canvas.SetLeft(cc, 0);
                top = 0;
            }
            var finalH = cc.ActualHeight;
            var finalW = cc.ActualWidth;
            var minW = cc.MinWidth;
            var minH = cc.MinHeight;
            if (minW.Equals(double.NaN))
            {
                minW = minSize;
            }
            if (minH.Equals(double.NaN))
            {
                minH = minSize;
            }
            Point dragDelta = new Point(hChange, vChange);
            var thumbTypeString = thumbType.ToString().ToLower();

            switch (thumbType)
            {
                case GeneralThumbType.Main:
                    left += dragDelta.X;
                    top += dragDelta.Y;
                    break;
                default:
                    var outSize = SimpleShifter(dragDelta.X, dragDelta.Y, minW, minH, thumbTypeString,
                                                              new Rect(left, top, finalW, finalH));
                    finalH = outSize.Height;
                    finalW = outSize.Width;
                    top = outSize.Top;
                    left = outSize.Left;
                    break;
            }
            if (finalW != cc.Width) cc.Width = finalW;
            if (finalH != cc.Height) cc.Height = finalH;
            Canvas.SetLeft(cc, left);
            Canvas.SetTop(cc, top);

            var outRect = new Rect(left, top, finalW, finalH);
      

        }
        #endregion
        private bool ChangingRevealerValues(ElementDisplayerMapViewModel revealerContext, Point originalShift, RevealerThumb thumb)
        {
            var rev = thumb.DataContext as ElementRevealerMapViewModel;
            if (rev != null)
            {
                if (rev.RevealerAnchored) return false;
            }
            if (thumb.ThumbType.Equals(GeneralThumbType.None))
            {
                //Wonder what this was for, i guess it is for the when the revealer is minimized
                
                if (rev == null) return false;
                rev.MinContentLocationX += originalShift.X;//.HorizontalChange;
                rev.MinContentLocationY += originalShift.Y;//.VerticalChange;
            }

            var left = revealerContext.DrawLeft;
            var top = revealerContext.DrawTop;
            var w = revealerContext.DrawWidth;
            var h = revealerContext.DrawHeight;

            var scaledHorzChange = originalShift.X / WorldScale;// / revealerContext.Scale;
            var scaledVertChange = originalShift.Y / WorldScale;// / revealerContext.Scale;

            var minSize = VisibleMinSize / WorldScale;// / revealerContext.Scale;
            double finalW = w;// double.NaN;
            double finalH = h;// double.NaN;

            var thumbTypeString = thumb.ThumbType.ToString().ToLower();
            switch (thumb.ThumbType)
            {
                case GeneralThumbType.Main:
                    left += scaledHorzChange;//originalShift.X;
                    top += scaledVertChange;//originalShift.Y;
                    break;
                default://Ick?, yes definetly ick
                    var outSize = SimpleShifter(scaledHorzChange, scaledVertChange, minSize, minSize, thumbTypeString,
                                          revealerContext.DrawBounds);

                    finalH = outSize.Height;
                    finalW = outSize.Width;
                    top = outSize.Top;
                    left = outSize.Left;

                    break;
            }

            revealerContext.UpdateGeoBoundsFromDrawRect(new Rect(left, top, finalW, finalH));
            return true;
        }

        private Rect SimpleShifter(double dx, double dy, double minX, double minY, string thumbType, Rect inArea)
        {
            var w = inArea.Width;
            var h = inArea.Height;
            var top = inArea.Top;
            var left = inArea.Left;
            var finalW = w;
            var finalH = h;
            if (thumbType.Contains("right"))
            {
                w += dx;
                if (w > minX) finalW = w;

            }
            else if (thumbType.Contains("left"))
            {
                w -= dx;
                if (w >= minX)
                {
                    left += dx;
                    finalW = w;
                }
            }

            if (thumbType.Contains("top"))
            {
                h -= dy;
                if (h > minY)
                {
                    finalH = h;
                    top += dy;
                }

            }
            else if (thumbType.Contains("bottom"))
            {
                h += dy;
                if (h > minY) finalH = h;

            }
            return new Rect(left, top, finalW, finalH);
        }


        static public void ImageRegistration(ImageRegistrationViewModel tp, GeneralThumbType thumbType, Point delta)
        {
            //At this point we are just dealing with a thumb in a canvas (hopefully
            var preserveAspect = tp.PreserveAspectRatio;
            var minSize = 20;
            var hChange = delta.X;
            var vChange = delta.Y;
            var left = tp.Left;
            var top = tp.Top;

            var finalH = tp.Height;
            var finalW = tp.Width;
            Point dragDelta = new Point(hChange, vChange);
            double radAngle = 0;

            double vertARDelta = vChange * (tp.AspectRatio);
            double horzARDelta = hChange / tp.AspectRatio;

            RotateTransform rotateTransform = tp.Rotation;// RenderTransform as RotateTransform;
            if (rotateTransform != null)
            {
                dragDelta = rotateTransform.Transform(dragDelta);
                radAngle = RotateThumb.DegreesToRadians(rotateTransform.Angle);
            }
            var thumbTypeString = thumbType.ToString().ToLower();
            //Check for Aspect ratio preservation
            if (preserveAspect)
            {
                if (thumbTypeString.Contains("right"))
                {
                    //The inner ifs were discovered by guess and check
                    //mostly due to tiredness (and some lazyness,davidl)
                    if (thumbTypeString.Contains("top"))
                    {
                        hChange = -vertARDelta;
                    }
                    else
                    {
                        thumbTypeString += "bottom";
                        vChange = horzARDelta;
                    }
                }
                else if (thumbTypeString.Contains("left"))
                {
                    if (thumbTypeString.Contains("top"))
                    {
                        hChange = vertARDelta;
                    }
                    else
                    {
                        thumbTypeString += "bottom";
                        vChange = -horzARDelta;
                    }
                }
                else if (thumbTypeString.Contains("top"))
                {
                    thumbTypeString += "right";
                    hChange = -vertARDelta;
                }
                else if (thumbTypeString.Contains("bottom"))
                {
                    thumbTypeString += "right";
                    hChange = vertARDelta;
                }
            }

            switch (thumbType)
            {
                case GeneralThumbType.Main:
                    left += dragDelta.X;
                    top += dragDelta.Y;
                    break;
                default:
                    double xShift = 0;
                    double yShift = 0;
                    if (thumbType.Equals(GeneralThumbType.ScaleDown) || thumbType.Equals(GeneralThumbType.ScaleUp))
                    {
                        xShift -= (dragDelta.X / 2);
                        yShift -= (dragDelta.Y / 2);
                        thumbTypeString += "bottomright";
                    }
                    var outSize = tp.AspectAndRotationShifter(hChange, vChange, minSize, thumbTypeString,
                                          new Rect(left, top, finalW, finalH), preserveAspect, radAngle, new Point(.5, .5));

                    finalH = outSize.Height;
                    finalW = outSize.Width;
                    top = outSize.Top + yShift;
                    left = outSize.Left + xShift;

                    break;
            }
            tp.ScreenImagePixelBounds = new Rect(left, top, finalW, finalH);
            tp.AspectRatio = finalW / finalH;
        }
    }
}