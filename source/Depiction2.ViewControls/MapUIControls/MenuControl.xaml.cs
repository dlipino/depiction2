using System;
using System.Diagnostics;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;

namespace Depiction2.ViewControls.MapUIControls
{
    public partial class MenuControl
    {
        public MenuControl()
        {
            InitializeComponent();
        }


        private void ToMercatorMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory != null)
            {
                DepictionAccess.DStory.ChangeDisplayCoordinateSystem(CoordinateSystemService.mercatorProjectedCoordinateSystem);
            }
        }

        private void ToGeoCSMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory != null)
            {
                DepictionAccess.DStory.ChangeDisplayCoordinateSystem(CoordinateSystemService.DepictionGeographicCoordinateSystem);
            }
        }

        private void CenterMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory == null)
            {
               
            }
            else
            {
                var story = DepictionAccess.DStory;

                var extent = story.ElementExtent;
                if(!extent.IsEmpty)
                {
                    var center = extent.Center;
                    story.SetCenter(center);
                }
            }
        }

        private void InteractionsStart_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory == null)
            {

            }
            else
            {

                var story = DepictionAccess.DStory;
                if(!story.InteractionsRunner.BeginAsync())
                {
                    Console.WriteLine("bad dog");
                }
            }
        }

        private void InteractionsStop_Click(object sender, RoutedEventArgs e)
        {
            if (DepictionAccess.DStory == null)
            {

            }
            else
            {
                var story = DepictionAccess.DStory;
                story.InteractionsRunner.EndAsync();
                Debug.WriteLine("Interactions stopped");
            }
        }
    }
}