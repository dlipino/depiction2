﻿using System;
using System.ComponentModel;
using System.Windows;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Measurement;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Core.ViewModels.PanAndZoom;

namespace Depiction2.ViewControls.MapUIControls
{
    /// <summary>
    /// Interaction logic for PanZoomControlView.xaml
    /// </summary>
    public partial class PanZoomControlView
    {
        public PanZoomControlView()
        {
            InitializeComponent();
            DataContextChanged += PanZoomControlView_DataContextChanged;
            //            Settings.Default.PropertyChanged += Default_PropertyChanged;
        }

        private void PanZoomControlView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDc = e.OldValue as PanAndZoomControlViewModel;
            if (oldDc != null)
            {
                oldDc.PropertyChanged -= dataContext_PropertyChanged;
            }

            var newDc = e.NewValue as PanAndZoomControlViewModel;
            if (newDc != null)
            {
                UpdateScaleVisualString();
                newDc.PropertyChanged += dataContext_PropertyChanged;
            }
        }

        void dataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("ScaleChange"))
            {
                UpdateScaleVisualString();
            }
        }


        //        void PanZoomControlView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        //        {
        //            var oldViewModel = e.OldValue as DepictionMapQuickAddAndTileViewModel;
        //            var newViewModel = e.NewValue as DepictionMapQuickAddAndTileViewModel;
        //            
        //            if (oldViewModel != null)
        //            {
        //                if(oldViewModel.WorldModel != null)
        //                {
        //                    oldViewModel.WorldModel.DepictionViewportChange -= WorldModel_DepictionViewportChange;
        //                }
        //              
        //            }
        //            if (newViewModel != null)
        //            {
        //                if(newViewModel.WorldModel != null)
        //                {
        //                    newViewModel.WorldModel.DepictionViewportChange += WorldModel_DepictionViewportChange;
        //                    UpdateScaleVisualString();
        //                }
        //            }
        //        }
        //
        //        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //        {
        //            if (e.PropertyName.Equals("measurementScale", StringComparison.InvariantCultureIgnoreCase) ||
        //                e.PropertyName.Equals("measurementSystem", StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                UpdateScaleVisualString();
        //            }
        //        }
        //
        //        void WorldModel_DepictionViewportChange(IMapCoordinateBounds oldBounds, IMapCoordinateBounds newBounds)
        //        {
        //            UpdateScaleVisualString();
        //        }
        //
        protected double[] validScaleValues = new double[] { .005, .01, .025, .05, .1, .25, .50, 1, 2, 5, 10, 25, 50, 100, 200, 300, 400, 500, 1000 };
        protected void UpdateScaleVisualString()
        {
            if(!DepictionPositionService.IsActive)
            {
                ScaleRect.Width = 0;
                scaleText.Text = "No measurement service";
                return;
            }

            var transform = ScaleRect.TransformToAncestor(Application.Current.MainWindow);
            var transRect = transform.TransformBounds(ScaleRect.RenderedGeometry.Bounds);
            transRect.Width = 150;
            if (GeoToDrawLocationService.ScreenToCartCanvas == null) return;

            var tl = GeoToDrawLocationService.ScreenToCartCanvas(transRect.TopLeft);
            var tr = GeoToDrawLocationService.ScreenToCartCanvas(transRect.TopRight);
            var scaleDistance = DepictionPositionService.Instance.DistanceBetween(tl, tr, Settings.Default.MeasurementSystem,
                                                                         MeasurementScale.Large);
//            var scaleDistance = tl.DistanceTo(tr, Settings.Default.MeasurementSystem, MeasurementScale.Large);
            //                var geoCoords = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(transRect);
            //                //var distanceString = geoCoords.TopLeft.DistanceToAsString(geoCoords.TopRight, Settings.Default.MeasurementSystem, MeasurementScale.Large);
            //                var scaleDistance = geoCoords.TopLeft.DistanceTo(geoCoords.TopRight, Settings.Default.MeasurementSystem, MeasurementScale.Large);
            //                //Based on scale and total size of canvas we can probably find the best scale without doing these calculations
            double minDistance = Double.PositiveInfinity;
            double validScale = 1;
            for (int i = 0; i < validScaleValues.Length; i++)// dist in validScaleValues)
            {
                var newDist = Math.Abs(validScaleValues[i] - scaleDistance);
                if (newDist < minDistance)
                {
                    minDistance = newDist;
                    validScale = validScaleValues[i];
                }
            }
            //                var finalCoord = tl.TranslateTo(90, validScale, Settings.Default.MeasurementSystem,
            //                                              MeasurementScale.Large);
            var finalCoord = DepictionPositionService.Instance.TranslatePoint(tl, 90, validScale, Settings.Default.MeasurementSystem,
                                                                     MeasurementScale.Large);
            var pixelCoord = GeoToDrawLocationService.CartCanvasToScreen(finalCoord);
            ScaleRect.Width = pixelCoord.X - transRect.Left;
            bool isSingle = false;
            if (validScale == 1)
            {
                isSingle = true;
            }
            var scaleUnits =
                new Distance().GetUnits(Settings.Default.MeasurementSystem, MeasurementScale.Large, isSingle);
            scaleText.Text = String.Format("{0:0.###} {1}", validScale, scaleUnits);

        }
    }
}
