﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.ViewControls
{
    public partial class StoryHotelView 
    {
        public StoryHotelView()
        {
            InitializeComponent();

            StoryView.ContextMenuOpening += MapControlGrid_ContextMenuOpening;
            StoryView.SizeChanged += StoryViewSizeChanged;
            DataContextChanged += StoryHotelView_DataContextChanged;
        }

        void StoryHotelView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //This might be the fast way of disconnecting the DepictionStoryView x:name=StoryView from the datacontext.
            //currently going through a disconnect method in the actual class
           // throw new System.NotImplementedException();
        }

        void StoryViewSizeChanged(object sender, SizeChangedEventArgs e)
        {
            //probably want to update panel extent too
            GeoToDrawLocationService.ScreenPixelSize = e.NewSize;
            var dc = DataContext as StoryViewModelHotel;
            if (dc == null) return;
            StoryView.SetViewScaleAndOffsetFromViewModelValues(dc.StoryZoom, dc.VirtualPanelOffset);
        }

        #region Context menu things
        void MapControlGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            OpenContextMenu(e);
            ContextMenu.IsOpen = true;
            //For some reason the contextmenu takes over control so the next left click doesn't 
            //do anything (close window, click button etc.
            //            ContextMenu.StaysOpen = true;
            e.Handled = true;
        }
        Dictionary<string, MapElementViewModel> contextMenuElements = new Dictionary<string, MapElementViewModel>();
        Dictionary<string, DepictionAnnotationVM> contextMenuAnnotations = new Dictionary<string, DepictionAnnotationVM>();


        //         var longName = elementVM.ToolTipText;
        //                    var shortName = elementVM.ToolTipText;
        //                    if (string.IsNullOrEmpty(longName))
        //                    {
        //                        shortName = elementVM.DisplayName;
        //                        longName = elementVM.DisplayName;
        //                    }
        //                    else
        //                    {
        //                        string[] lines = shortName.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        //                        var more = string.Empty;
        //                        if (lines.Length > 1)
        //                        {
        //                            shortName = lines[0];
        //                            more = "...";
        //                        }
        //                        if (shortName.Length > MaxContextMenuLength)
        //                        {
        //                            shortName = shortName.Substring(0, MaxContextMenuLength);
        //                            more = "...";
        //                        }
        //                        shortName += more;
        //                    }
        private void OpenContextMenu(ContextMenuEventArgs e)
        {
            var location = Mouse.GetPosition(this);
            var cartLocation = GeoToDrawLocationService.ScreenToCartCanvas(location);

            var latLongLocation = cartLocation;

            ContextMenu = new ContextMenu();
            var dc = DataContext as StoryViewModelHotel;
            if (dc != null)
            {
                foreach (var command in dc.StoryCommands)
                {
                    var menuItem = new MenuItem { Command = command, Header = "Something", CommandParameter = latLongLocation };
                    var textCommand = command as CommandInfoBase;
                    if (textCommand != null)
                    {
                        menuItem.Header = textCommand.Text;
                    }
                    ContextMenu.Items.Add(menuItem);
                }

                foreach (var command in dc.MapLocationCommands)
                {
                    var menuItem = new MenuItem { Command = command, Header = "Something", CommandParameter = latLongLocation };
                    var textCommand = command as CommandInfoBase;
                    if (textCommand != null)
                    {
                        menuItem.Header = textCommand.Text;
                    }
                    ContextMenu.Items.Add(menuItem);
                }
            }

            contextMenuAnnotations.Clear();
            contextMenuElements.Clear();
            VisualTreeHelper.HitTest(this, null, ElementContextMenuCallBack, new PointHitTestParameters(location));
            if (dc != null)
            {
                if (contextMenuAnnotations.Count > 0)
                {
                    ContextMenu.Items.Add(new Separator());
                }

                foreach (var annotKey in contextMenuAnnotations.Keys)
                {
                    var annotationItem = new MenuItem { Header = string.Format("Annotation: {0}", contextMenuAnnotations[annotKey].AnnotationText) };
                    annotationItem.MaxWidth = 200;
                    var annotModel = contextMenuAnnotations[annotKey].annotationModel;
                    foreach (var annotCommands in dc.GenericAnnotationCommands)
                    {
                        annotationItem.Items.Add(CreateSubMenuItemFromCommand(annotCommands, annotModel));
                    }
                    ContextMenu.Items.Add(annotationItem);
                }
            }
            if (contextMenuElements.Count > 0)
            {
                ContextMenu.Items.Add(new Separator());
            }

            foreach (var elementKey in contextMenuElements.Keys)
            {
                var elementVM = contextMenuElements[elementKey];
                var elemModel = elementVM._elementModel;
                var longName = elemModel.DisplayName;
                var shortName = elemModel.DisplayName;
                #region short/long name
                //                    if (string.IsNullOrEmpty(longName))
                //                    {
                //                        shortName = elementVM.DisplayName;
                //                        longName = elementVM.DisplayName;
                //                    }
                //                    else
                //                    {
                //                        string[] lines = shortName.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                //                        var more = string.Empty;
                //                        if (lines.Length > 1)
                //                        {
                //                            shortName = lines[0];
                //                            more = "...";
                //                        }
                //                        if (shortName.Length > MaxContextMenuLength)
                //                        {
                //                            shortName = shortName.Substring(0, MaxContextMenuLength);
                //                            more = "...";
                //                        }
                //                        shortName += more;
                //                    }
                //
                #endregion
                var subMenuItems = new List<object>();
                var draggableItem = CreateCheckableItemForDraggable(elemModel, null);
                if (draggableItem != null)
                {
                    subMenuItems.Add(draggableItem);
                    subMenuItems.Add(new Separator());
                }

                foreach (var elemCommand in dc.GenericElementCommands)
                {
                    subMenuItems.Add(CreateSubMenuItemFromCommand(elemCommand, elemModel));
                }

                if (contextMenuElements.Count == 1)
                {
                    foreach (var item in subMenuItems)
                    {
                        ContextMenu.Items.Add(item);
                    }
                }
                else
                {
                    var elementItem = new MenuItem { Header = shortName };
                    elementItem.ToolTip = longName;
                    foreach (var item in subMenuItems)
                    {
                        elementItem.Items.Add(item);
                    }
                    ContextMenu.Items.Add(elementItem);
                }
            }
        }
        private HitTestResultBehavior ElementContextMenuCallBack(HitTestResult result)
        {
            var res = result.VisualHit;
            var fe = res as FrameworkElement;
            if (fe == null)
            {
                var dv = res as DrawingVisual;
                if (dv != null)
                {
                    fe = dv.Parent as FrameworkElement;
                }
            }

            if (fe != null && fe.IsHitTestVisible)
            {
                if (fe.DataContext is DepictionAnnotationVM)
                {
                    var annotation = (DepictionAnnotationVM)fe.DataContext;
                    if (!contextMenuAnnotations.ContainsKey(annotation.AnnotationID))
                    {
                        contextMenuAnnotations.Add(annotation.AnnotationID, annotation);
                    }
                }
                else
                {
                    var elementDC = fe.DataContext as MapElementViewModel;
                    if (elementDC != null && !contextMenuElements.ContainsKey(elementDC.ElemId))
                    {
                        contextMenuElements.Add(elementDC.ElemId, elementDC);
                    }
                }
            }
            return HitTestResultBehavior.Continue;
        }

        private MenuItem CreateSubMenuItemFromCommand(ICommand command, object commandParam)
        {
            var arbitraryMenuItem = new MenuItem();
            arbitraryMenuItem.Command = command;
            MapElementViewModel mapMapElementViewModel = null;
            //            if (commandParam is DepictionMapElementVM)
            //            {
            //                mapMapElementVm = (DepictionMapElementVM)commandParam;
            //                arbitraryMenuItem.CommandParameter = new List<string> { mapMapElementVm.ElemId };
            //            }
            //            //            else if (commandParam is DepictionElementParent)
            //            //            {
            //            //                arbitraryMenuItem.CommandParameter = commandParam as DepictionElementParent;
            //            //            }
            //            else
            //            {
            arbitraryMenuItem.CommandParameter = commandParam;
            //            }
            var textCommand = command as CommandInfoBase;
            if (textCommand != null)
            {
                var text = textCommand.Text;
                //                if (text.Contains("permatext") && mapMapElementVm != null)
                //                {
                //                    arbitraryMenuItem.Header = mapMapElementVm.UsePermaText
                //                                                   ? DepictionMapAndMenuViewModel.hidePermatext
                //                                                   : DepictionMapAndMenuViewModel.showPermatext;// "Hide permatext" : "Show permatext";
                //                }
                //                else
                {
                    arbitraryMenuItem.Header = textCommand.Text;
                }
            }
            else
            {
                arbitraryMenuItem.Header = "Unknown action";
            }

            return arbitraryMenuItem;
        }

        private MenuItem CreateCheckableItemForDraggable(object commandParam, string propertyName)
        {
            var element = commandParam as IElement;
            if (element == null) return null;
            bool draggable = element.IsDraggable;
            //            if (!element.GetPropertyValue("Draggable", out draggable) || !element.GetPropertyByInternalName("Draggable").VisibleToUser)
            //            {
            //                return null;
            //            }
            var arbitraryMenuItem = new MenuItem();
            arbitraryMenuItem.IsCheckable = true;
            arbitraryMenuItem.IsChecked = draggable;
            arbitraryMenuItem.Click += isDraggableMenuItem_Click;
            arbitraryMenuItem.Header = "Draggable";
            arbitraryMenuItem.DataContext = element;
            return arbitraryMenuItem;
        }

        void isDraggableMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var fe = sender as FrameworkElement;
            if (fe == null) return;
            var element = fe.DataContext as IElement;
            if (element == null) return;
            element.IsDraggable = !element.IsDraggable;
            element.TriggerPropertyChange("IsDraggable");
            //            if (element.GetPropertyValue("Draggable", out draggable))
            //            {
            //                element.UpdatePropertyValue("Draggable", !draggable);
            //            }
        }

        #endregion


    }
}
