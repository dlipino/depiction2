﻿using System.Windows;
using System.Windows.Controls;

namespace Depiction2.ViewControls.RevealerControls
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class RevealerItemsControl
    {
        public ZoomableCanvas ZoomableCanvas;

        #region Constructor

        public RevealerItemsControl()
        {
            InitializeComponent();
            ClipToBounds = false;
        }

        #endregion

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
    }
}
