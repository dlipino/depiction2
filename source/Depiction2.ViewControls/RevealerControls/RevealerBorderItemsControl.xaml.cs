﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Core.ViewModels.RevealerViewModels;

namespace Depiction2.ViewControls.RevealerControls
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class RevealerBorderItemsControl
    {
        public ZoomableCanvas ZoomableCanvas;

        #region Constructor

        public RevealerBorderItemsControl()
        {
            InitializeComponent();
            //            CacheMode = new BitmapCache();
            ClipToBounds = false;
        }

        #endregion

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("what");
        }

        private void ItemsControl_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var cc = fe.DataContext as ElementRevealerMapViewModel;
            if (cc == null) return;
            if (cc.RevealerMaximized) return;
            cc.RevealerMaximized = true;
            e.Handled = true;
        }

        private void Revealer_MouseEnter(object sender, MouseEventArgs e)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var fe = sender as FrameworkElement;
            if (fe == null) return;
            var dc = fe.DataContext as ElementRevealerMapViewModel;
            if (dc == null) return;
            story.TopRevealerId = dc.RevealerId;
        }
    }
}
