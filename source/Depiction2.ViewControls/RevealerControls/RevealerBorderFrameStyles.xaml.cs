﻿using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Depiction2.ViewControls.RevealerControls
{
    partial class RevealerBorderFrameStyles
    {
        private void EnterKeyCheck(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var tb = e.Source as TextBox;
                if (tb != null)
                {
                    //The name of the textBox is in this case TB
                    BindingExpression be = tb.GetBindingExpression(TextBox.TextProperty);
                    if (be != null) be.UpdateSource();
                }
            }
        }
    }
}
