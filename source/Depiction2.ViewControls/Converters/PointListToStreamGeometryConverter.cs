﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.Base.Utilities.Drawing;

namespace Depiction2.ViewControls.Converters
{
    public class PointListToStreamGeometryConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var pointList = value as EnhancedPointListWithChildren;

            return DrawingHelpers.CreateGeometryFromEnhancedPointListWithChildren(pointList);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}