﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.ViewControls.Converters
{
    public class ElementVisibilityConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var paramString = parameter.ToString();
            ElementVisualSetting expectedVisualSetting;
            Enum.TryParse(paramString, true, out expectedVisualSetting);
            ElementVisualSetting givenValue;

            var objectString = value.ToString();
            Enum.TryParse(objectString, true, out givenValue);
            if((givenValue & expectedVisualSetting) == expectedVisualSetting)
            {
                return Visibility.Visible;
            }
            
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}