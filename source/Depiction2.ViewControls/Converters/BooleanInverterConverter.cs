﻿using System;
using System.Windows.Data;

namespace Depiction2.ViewControls.Converters
{
    public class BooleanInverterConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = true;
            try
            {
                bool val = (bool)value;
                result = !val;
            }
            catch
            {
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = true;
            try
            {
                bool val = (bool)value;
                result = !val;
            }
            catch
            {
            }

            return result;
        }

        #endregion
    }
}