﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.EditingViewModels;
using Depiction2.DrawControls.ThumbControls;

namespace Depiction2.DrawControls.ImageRegistrationTool
{
    public class ElementImageRegistrationContentControl : ContentControl
    {

//        private readonly ElementImageRegistrationHelperWindow elementImageRegistartionHelperWindow;
        //I guess this isn't the best use of this, might be ter as a UserControl, although i rally don't know.
        static ElementImageRegistrationContentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementImageRegistrationContentControl), new FrameworkPropertyMetadata(typeof(ElementImageRegistrationContentControl)));
        }

        #region variables

        private bool isConnectedToKeyboard;
        private double sizeChange = 2;
        #endregion
        #region Constructor
        public ElementImageRegistrationContentControl()
        {
//            elementImageRegistartionHelperWindow = new ElementImageRegistrationHelperWindow();
            IsVisibleChanged += ElementImageRegistrationContentControl_IsVisibleChanged;
            Loaded += ElementImageRegistrationContentControl_Loaded;
        }

        #endregion
        #region even connectors

        void ElementImageRegistrationContentControl_Loaded(object sender, RoutedEventArgs e)
        {
//            if (Application.Current != null && Application.Current.MainWindow != null)
//            {
//                elementImageRegistartionHelperWindow.Owner = Application.Current.MainWindow;
//            }
        }

        void ElementImageRegistrationContentControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
//            var newVal = e.NewValue;
//            if (!(newVal is bool)) return;
//            var boolVal = (bool) newVal;
//            if(boolVal)
//            {
//                elementImageRegistartionHelperWindow.DataContext = DataContext;//This might only need to be set once
//                elementImageRegistartionHelperWindow.Content = Content;
//                elementImageRegistartionHelperWindow.Show();
//                ConnectToKeyboard();
//            }else
//            {
//                try
//                {
//                    elementImageRegistartionHelperWindow.Content = null;
//                    elementImageRegistartionHelperWindow.DataContext = null;//not sure if this is really needed
//                    elementImageRegistartionHelperWindow.Close();
//                }catch{}
//                DisconnectFromKeyboard();
//            }
        }

        void ElementImageRegistrationContentControl_KeyDown(object sender, KeyEventArgs e)
        {
            var view = sender as ElementImageRegistrationContentControl;
            if (view == null) return;
            var viewModel = view.Content as ImageRegistrationViewModel;
            if (viewModel == null) return;
            var key = e.Key;
            if (key.Equals(Key.Up) || key.Equals(Key.Down) || key.Equals(Key.Left) || key.Equals(Key.Right) ||
                key.Equals(Key.PageUp) || key.Equals(Key.PageDown))
            {
                GeneralThumbType fakeThumb = GeneralThumbType.Main;
                var point = new Point(0, 0);
                switch(key)
                {
                    case Key.Up:
                        point = new Point(0, -sizeChange);
                        break;
                        case Key.Down:
                        point = new Point(0, sizeChange);
                        break;
                        case Key.Left:
                        point = new Point(-sizeChange, 0);
                        break;
                        case Key.Right:
                        point = new Point(sizeChange, 0);
                        break;
                        case Key.PageUp:
                        fakeThumb = GeneralThumbType.ScaleUp;
                        point = new Point(sizeChange, sizeChange);
                        break;
                        case Key.PageDown:
                        fakeThumb = GeneralThumbType.ScaleDown;
                        point = new Point(-sizeChange, -sizeChange);
                        break;
                        
                }
                ImageRegistration(viewModel,fakeThumb,point);
                e.Handled = true;
            }
        }

        void ElementImageRegistrationContentControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (IsMouseOver)
            {
                var viewModel = Content as ImageRegistrationViewModel;
                if (viewModel == null) return;
                var point = new Point(-sizeChange, -sizeChange);

                GeneralThumbType fakeThumb = GeneralThumbType.ScaleDown;
                if (e.Delta > 0)
                {
                    fakeThumb = GeneralThumbType.ScaleUp;
                    point = new Point(sizeChange, sizeChange);
                }
                ImageRegistration(viewModel, fakeThumb, point);
                e.Handled = true;
            }
        }
        #endregion
        protected void ConnectToKeyboard()
        {
            if (isConnectedToKeyboard) return;
            isConnectedToKeyboard = true;

            KeyDown += ElementImageRegistrationContentControl_KeyDown;
            MouseWheel += ElementImageRegistrationContentControl_MouseWheel;
        }

        protected void DisconnectFromKeyboard()
        {
            isConnectedToKeyboard = false;
            KeyDown -= ElementImageRegistrationContentControl_KeyDown;
            MouseWheel -= ElementImageRegistrationContentControl_MouseWheel;
        }

        static public void ImageRegistration(ImageRegistrationViewModel tp, GeneralThumbType thumbType, Point delta)
        {
            //At this point we are just dealing with a thumb in a canvas (hopefully
            var preserveAspect = tp.PreserveAspectRatio;
            var minSize = 20;
            var hChange = delta.X;
            var vChange = delta.Y;
            var left = tp.Left;
            var top = tp.Top;

            var finalH = tp.Height;
            var finalW = tp.Width;
            Point dragDelta = new Point(hChange, vChange);
            double radAngle = 0;

            double vertARDelta = vChange * (tp.AspectRatio);
            double horzARDelta = hChange / tp.AspectRatio;

            RotateTransform rotateTransform = tp.Rotation;// RenderTransform as RotateTransform;
            if (rotateTransform != null)
            {
                dragDelta = rotateTransform.Transform(dragDelta);
                radAngle = RotateThumb.DegreesToRadians(rotateTransform.Angle);
            }
            var thumbTypeString = thumbType.ToString().ToLower();
            //Check for Aspect ratio preservation
            if (preserveAspect)
            {
                if (thumbTypeString.Contains("right"))
                {
                    //The inner ifs were discovered by guess and check
                    //mostly due to tiredness (and some lazyness,davidl)
                    if (thumbTypeString.Contains("top"))
                    {
                        hChange = -vertARDelta;
                    }
                    else
                    {
                        thumbTypeString += "bottom";
                        vChange = horzARDelta;
                    }
                }
                else if (thumbTypeString.Contains("left"))
                {
                    if (thumbTypeString.Contains("top"))
                    {
                        hChange = vertARDelta;
                    }
                    else
                    {
                        thumbTypeString += "bottom";
                        vChange = -horzARDelta;
                    }
                }
                else if (thumbTypeString.Contains("top"))
                {
                    thumbTypeString += "right";
                    hChange = -vertARDelta;
                }
                else if (thumbTypeString.Contains("bottom"))
                {
                    thumbTypeString += "right";
                    hChange = vertARDelta;
                }
            }

            switch (thumbType)
            {
                case GeneralThumbType.Main:
                    left += dragDelta.X;
                    top += dragDelta.Y;
                    break;
                default:
                    double xShift = 0;
                    double yShift = 0;
                    if (thumbType.Equals(GeneralThumbType.ScaleDown) || thumbType.Equals(GeneralThumbType.ScaleUp))
                    {
                        xShift -= (dragDelta.X / 2);
                        yShift -= (dragDelta.Y / 2);
                        thumbTypeString += "bottomright";
                    }
                    var outSize = tp.AspectAndRotationShifter(hChange, vChange, minSize, thumbTypeString,
                                          new Rect(left, top, finalW, finalH), preserveAspect, radAngle, new Point(.5, .5));

                    finalH = outSize.Height;
                    finalW = outSize.Width;
                    top = outSize.Top + yShift;
                    left = outSize.Left + xShift;

                    break;
            }
            tp.ScreenImagePixelBounds = new Rect(left, top, finalW, finalH);
            tp.AspectRatio = finalW / finalH;
        }
    }
}
