﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using Depiction2.Core.ViewModels.EditingViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Utilities;

namespace Depiction2.ViewControls.ImageRegistrationTool
{
    /// <summary>
    /// Interaction logic for ImageRegistrationUserControl.xaml
    /// </summary>
    public partial class ImageRegistrationUserControl
    {
//        public Func<Point, Point> ScreenToCanvas { get; set; }
//        public Func<Point, Point> CanvasToScreen { get; set; }

        public ImageRegistrationUserControl()
        {
            InitializeComponent();
            DataContextChanged += ImageRegistrationUserControl_DataContextChanged;
            MouseRightButtonUp += ImageRegistrationUserControl_MouseRightButtonUp;
            MouseLeftButtonDown += ImageRegistrationUserControl_MouseLeftButtonDown;
        }


        void ImageRegistrationUserControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
       
        void ImageRegistrationUserControl_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var newdc = DataContext as ImageRegistrationViewModel;
            if (newdc == null) return;
            newdc.CompleteRegistration();
            e.Handled = true;
        }

        void ImageRegistrationUserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newdc = e.NewValue as ImageRegistrationViewModel;
            if (newdc != null)
            {
                newdc.PropertyChanged += newdc_PropertyChanged;
            }
        }

        void newdc_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            var irDc = sender as ImageRegistrationViewModel;
            if (irDc == null) return;
            if (irDc._elementModel == null) return;
            if (propName.Equals("ImageElement"))
            {
                var insideRect = irDc._elementModel.SimpleBounds.WpfRect;
                //A request to start editing the image geolcation has come through
                if (insideRect.IsEmpty)
                {
                    insideRect = RandomUtilities.GetRectInteriorToRect(new Rect(new Point(0, 0), RenderSize));
                }
                else
                {
                    //convert the things to screen location
                    insideRect = irDc._elementModel.SimpleBounds.WpfRect;//make this come in as cart coords
//                    var b = irDc._elementModel.ElementGeometry.GeometryPoints.ToList();
//                    var tlb = CanvasToScreen(new Point(b[0].X, -b[0].Y));
//                    var brb = CanvasToScreen(new Point(b[2].X, -b[2].Y));
//                    var h = Math.Abs(tlb.Y - brb.Y);
//                    var w = Math.Abs(tlb.X - brb.X);
                    var tlm = new Point(insideRect.Left, -insideRect.Bottom);
                    var brm = new Point(insideRect.Right, -insideRect.Top);
                    var tl = GeoToDrawLocationService.CartCanvasToScreen(insideRect.BottomLeft);// CanvasToScreen(tlm);
                    var br = GeoToDrawLocationService.CartCanvasToScreen(insideRect.TopRight); // CanvasToScreen(brm);
//                    insideRect = new Rect(tl, new Size(w,h));
                    insideRect = new Rect(tl,br);
                    Background = Brushes.Transparent;

                    //insideRect = GetRectInteriorToViewport(new Rect(new Point(0, 0), RenderSize));
                }

                irDc.UpdateRegistrationPixelBounds(insideRect);
            }else if(propName.Equals("ScreenImagePixelBounds"))
            {
                var sb = irDc.ScreenImagePixelBounds;
                irDc.GeoCanvasImageBounds = TurnScreenRectToCartisianCanvas(sb);
            }
        }
        protected List<Point> TurnScreenRectToCartisianCanvas(Rect sb)
        {
            var screenPointList = new List<Point> { sb.TopLeft, sb.TopRight, sb.BottomRight, sb.BottomLeft, sb.TopLeft };
            var canvasList = new List<Point>();
            foreach (var p in screenPointList)
            {
                canvasList.Add(GeoToDrawLocationService.ScreenToCartCanvas(p));
            }
            return canvasList;
        }
    }
}
