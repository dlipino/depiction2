﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.RevealerViewModels;

namespace Depiction2.ViewControls
{
    public partial class ElementViewGrid
    {
        protected List<UIElement> originalChildren = new List<UIElement>();
        protected DepictionStoryView mainParent;
        protected Point leftMouseDownLocation = new Point(double.NaN,double.NaN);

        public ElementViewGrid()
        {
            InitializeComponent();
            Background = Brushes.Transparent; //null;//
            DataContextChanged += MapControlHolder_DataContextChanged;
            //hmm i guess this guy is also only loaded once
            //This changes the parent in order to maintain the z order of icons even if there is a revealer involved
            Loaded += ElementDisplayerView_Loaded;
            Unloaded += ElementDisplayerView_Unloaded;
            //Mouse control stuff ie drag/drop 
            PointItems.MouseMove += ElementController_MouseMove;
            PointItems.MouseLeftButtonDown += ElementController_MouseLeftDown;
            PointItems.MouseLeftButtonUp += ElementController_MouseLeftUp;
            GeometryItems.MouseMove += ElementController_MouseMove;
            GeometryItems.MouseLeftButtonDown += ElementController_MouseLeftDown;
            GeometryItems.MouseLeftButtonUp += ElementController_MouseLeftUp;
//            ImageItems.MouseMove += ElementController_MouseMove;
//            ImageItems.MouseLeftButtonDown += ElementController_MouseLeftDown;
//            ImageItems.MouseLeftButtonUp += ElementController_MouseLeftUp;
//            MouseMove += ElementController_MouseMove;
//            MouseLeftButtonDown += ElementController_MouseLeftDown;
//            MouseLeftButtonUp += ElementController_MouseLeftUp;
        }

        private void MapControlHolder_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldContext = e.OldValue as ElementDisplayerMapViewModel;
            if (oldContext != null)
            {
                ClearItems();
            }
            var newContext = e.NewValue as ElementDisplayerMapViewModel;
            if (newContext != null)
            {
                ConnectStoryToDrawControls(newContext);
            }
        }

        #region connectors

        private void ClearItems()
        {
            PointItems.ItemsSource = null;
            GeometryItems.ItemsSource = null;
            ImageItems.ItemsSource = null;
            PermaTextItems.ItemsSource = null;

            var displayer = DataContext as ElementDisplayerMapViewModel;
            if (displayer != null)
            {
                displayer.PropertyChanged -= displayer_PropertyChanged;
            }
        }

        private void ConnectStoryToDrawControls(ElementDisplayerMapViewModel displayer)
        {
            PointItems.ItemsSource = displayer.LiveElements;
            GeometryItems.ItemsSource = displayer.LiveElements;
            ImageItems.ItemsSource = displayer.LiveElements;
            PermaTextItems.ItemsSource = displayer.LiveElements;
            displayer.PropertyChanged += displayer_PropertyChanged;
        }

        private void displayer_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("RevealerClip"))
            {
                UpdateClip();
            }
        }

        public void UpdateClip()
        {
            var world = Parent as DepictionStoryView;
            if (world == null) return;
            var displayer = DataContext as ElementDisplayerMapViewModel;
            if (displayer == null) return;
            ImageItems.AdjustZoomPanel();
            GeometryItems.AdjustZoomPanel();
            PointItems.AdjustZoomPanel();
            PermaTextItems.AdjustZoomPanel();
            //What is going on here?
            var drawTL = new Point(displayer.DrawLeft, displayer.DrawTop);
            var cTL = world.GetScreenPoint(drawTL);
            var cW = displayer.DrawWidth * world.WorldScale;

            var cH = displayer.DrawHeight * world.WorldScale;
            Geometry clip = null;
            var revealer = DataContext as ElementRevealerMapViewModel;

            if (revealer != null)
            {
                switch (revealer.ShapeType)
                {
                    case DisplayerViewType.Rectangle:
                        clip = new RectangleGeometry(new Rect(cTL.X, cTL.Y, cW, cH));
                        break;
                    case DisplayerViewType.Circle:
                        clip = new EllipseGeometry(new Rect(cTL.X, cTL.Y, cW, cH));
                        break;

                    case DisplayerViewType.Minimized:
                        clip = new RectangleGeometry(new Rect(0, 0, 0, 0));
                        break;
                }
            }
            Clip = clip;
        }

        #endregion

        #region Load unload events

        //used to properly set up the element displayers with a correct z index
        private void ElementDisplayerView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent == null) return;
            var panelParent = Parent as DepictionStoryView;
            if (panelParent == null) return;
            mainParent = panelParent;
            if (originalChildren.Count != 0)
            {
                foreach (var uiElement in originalChildren)
                {
                    panelParent.Children.Add(uiElement);
                }
            }
            else
            {
                while (Children.Count != 0)
                {
                    var child = Children[0];
                    Children.Remove(child);
                    panelParent.Children.Add(child);
                    originalChildren.Add(child);
                    var itemsControl = child as ItemsControl;
                    //Pass the parent on as a tag
                    if (itemsControl != null) itemsControl.Tag = this;
                }
            }
        }

        private void ElementDisplayerView_Unloaded(object sender, RoutedEventArgs e)
        {
            if (mainParent == null) return;
            foreach (var uiElement in originalChildren)
            {
                mainParent.Children.Remove(uiElement);
            }
        }

        #endregion
        
        private void ElementController_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftMouseDownLocation.X.Equals(double.NaN)) return;
            var position = e.GetPosition(this);
            if (Equals(position, leftMouseDownLocation)) return;
            //Check for element move stuff
            var fe = e.OriginalSource as FrameworkElement;
            if (fe != null && e.LeftButton == MouseButtonState.Pressed)
            {
                var dc = fe.DataContext as MapElementViewModel;
                if (dc != null && dc.IsDraggable)
                {
                    var dragElement = new DataObject(dc);
                    DragDrop.DoDragDrop(this, dragElement, DragDropEffects.Move);
                    leftMouseDownLocation = new Point(double.NaN,double.NaN);
                    e.Handled = true;
                }
            }
        }

        private void ElementController_MouseLeftDown(object sender, MouseButtonEventArgs e)
        {
            var p = e.GetPosition(this);
            //hack for for adding way points to a route
            if (IsAnyShiftDown())
            {
                var os = e.OriginalSource as FrameworkElement;
                if (os == null) return;
                var dc = os.DataContext as MapElementViewModel;
                if (dc == null) return;
                dc.DoOnClickActions(p);
                e.Handled = true;
                return;
            }
            leftMouseDownLocation = p;
        }
        private void ElementController_MouseLeftUp(object sender, MouseButtonEventArgs e)
        {
            leftMouseDownLocation = new Point(double.NaN,double.NaN);
        }
        private bool IsAnyShiftDown()
        {
            return Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.LeftShift);
        }
    }
}