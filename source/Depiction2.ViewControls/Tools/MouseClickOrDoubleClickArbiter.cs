using System.ComponentModel;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;

//Can't remember where this was copy pasted from
namespace Depiction2.ViewControls.Tools
{
    public class MouseClickOrDoubleClickArbiter
    {
        public event MouseButtonEventHandler Click;
        public event MouseButtonEventHandler DoubleClick;
        private readonly BackgroundWorker clickWait;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MouseClickOrDoubleClickArbiter"/> is clicked.
        /// </summary>
        /// <value><c>true</c> if clicked; otherwise, <c>false</c>.</value>
        private bool Clicked { get; set; }
        //private bool DoubleClicked { get; set; }
        /// <summary>
        /// Gets or sets the control.
        /// </summary>
        /// <value>The control.</value>
        public Control Control { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        /// <value>The timeout.</value>
        public int Timeout { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MouseClickOrDoubleClickArbiter"/> class.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="timeout"></param>
        public MouseClickOrDoubleClickArbiter(Control control, int timeout)
        {
            Clicked = false;
            Control = control;
            Timeout = timeout;
            clickWait = new BackgroundWorker();
            clickWait.DoWork += clickWait_DoWork;
        }
        
        /// <summary>
        /// Handles the click.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        public void HandleClick(object sender, MouseButtonEventArgs e)
        {
            lock (this)
            {
                if (Clicked)
                {
                    Clicked = false;
                    OnDoubleClick(sender, e);
                }
                else
                {
                    Clicked = true;
                    if (!clickWait.IsBusy)
                        clickWait.RunWorkerAsync(e);
                }
            }
        }

        private void clickWait_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(Timeout);
            lock (this)
            {
                if (Clicked)
                {
                    Clicked = false;
                    OnClick(this, (MouseButtonEventArgs)e.Argument);
                }
            }
        }

        /// <summary>
        /// Called when [click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void OnClick(object sender, MouseButtonEventArgs e)
        {
            MouseButtonEventHandler handler = Click;

            if (handler != null)
                Control.Dispatcher.BeginInvoke(handler, sender, e);
        }

        /// <summary>
        /// Called when [double click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseButtonEventArgs"/> instance containing the event data.</param>
        private void OnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MouseButtonEventHandler handler = DoubleClick;
            if (handler != null)
            {
                handler(sender, e);
            }
        }
    }
}