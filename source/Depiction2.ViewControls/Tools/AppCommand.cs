﻿using System.Diagnostics;
using System.Windows.Input;

namespace Depiction2.ViewControls.Tools
{
    public class AppCommand
    {
        private static RoutedUICommand exit;
        private static RoutedUICommand toMainMenu;
        private static RoutedUICommand _startNewDepiction;
        private static RoutedUICommand openFile;
        private static RoutedUICommand navigateBrowserTo;
        private static RoutedUICommand toggleSettingsDialog;
        private static RoutedUICommand toggleAboutDialog;
        private static RoutedUICommand toggleEULADialog;
        private static RoutedUICommand toggleToolsDialog;
        private static RoutedUICommand toggleExtensionDialog;
        private static RoutedUICommand toggleInteractionLibraryDialog;
        private static RoutedUICommand toggleInteractionEditorDialog;
        private static RoutedUICommand toggleScaffoldEditorDialog;
        private static RoutedUICommand toggleScaffoldExplorerDialog;

//        private static RoutedUICommand showDialog;

        static AppCommand()
        {
            InputGestureCollection inputs = null;

            // Initialize the exit command
            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.F4, ModifierKeys.Alt, "Alt+F4"));
            exit = new RoutedUICommand("Exit Depiction", "Exit", typeof(AppCommand), inputs);

            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.D, ModifierKeys.Control, "Ctrl+D"));
            AppCommand.toggleSettingsDialog = new RoutedUICommand("Show Settings", "ShowDialog", typeof(AppCommand));

            inputs = new InputGestureCollection();
            AppCommand.navigateBrowserTo = new RoutedUICommand("Browse Web Site...", "NavigateBrowserTo", typeof(AppCommand));

            toMainMenu = new RoutedUICommand("To Welcome Screen", "ToMainMenu", typeof(AppCommand));

            openFile = new RoutedUICommand("Open File", "OpenFile", typeof(AppCommand));
            _startNewDepiction = new RoutedUICommand("New Depiction", "NewDepiction", typeof(AppCommand));
            toggleAboutDialog = new RoutedUICommand("About Dialog", "AboutDialog", typeof(AppCommand));
            toggleEULADialog = new RoutedUICommand("Eula Dialog", "EulaDialog", typeof(AppCommand));
            toggleToolsDialog = new RoutedUICommand("Tools Dialog", "ToolDialog", typeof(AppCommand));

            toggleExtensionDialog = new RoutedUICommand("Extension Libaray Dialog", "ExtensionLibrary", typeof(AppCommand));
            toggleInteractionLibraryDialog = new RoutedUICommand("Interaction Libaray Dialog", "InteractionsLibrary", typeof(AppCommand));
            toggleInteractionEditorDialog = new RoutedUICommand("Interaction Editor Dialog", "InteractionEditor", typeof(AppCommand));
            toggleScaffoldEditorDialog = new RoutedUICommand("Scaffold Editor Dialog", "ScaffoldEditor", typeof(AppCommand));
            toggleScaffoldExplorerDialog = new RoutedUICommand("Scaffold Explorer Dialog", "ScaffoldExplorer", typeof(AppCommand));
        }

        public static RoutedUICommand Exit
        {
            get { return exit; }
        }
        public static RoutedUICommand StartNewDepiction
        {
            get { return _startNewDepiction; }
        }
        public static RoutedUICommand OpenFile
        {
            get { return openFile; }
        }
        public static RoutedUICommand ToMainMenu
        {
            get { return toMainMenu; }
        }
        public static RoutedUICommand NavigateBrowserTo
        {
            get { return navigateBrowserTo; }
        }
        public static RoutedUICommand ToggleSettingDialog
        {
            get { return toggleSettingsDialog; }
        }
        public static RoutedUICommand ToggleAboutDialog
        {
            get { return toggleAboutDialog; }
        }
        public static RoutedUICommand ToggleEulaDialog
        {
            get { return toggleEULADialog; }
        }
        public static RoutedUICommand ToggleToolsDialog
        {
            get { return toggleToolsDialog; }
        }
        public static RoutedUICommand ToggleExtensionDialog
        {
            get { return toggleExtensionDialog; }
        }
        public static RoutedUICommand ToggleInteractionLibraryDialog
        {
            get { return toggleInteractionLibraryDialog; }
        }
        public static RoutedUICommand ToggleInteractionEditorDialog
        {
            get { return toggleInteractionEditorDialog; }
        }
        public static RoutedUICommand ToggleScaffoldEditorDialog
        {
            get { return toggleScaffoldEditorDialog; }
        }
        public static RoutedUICommand ToggleScaffoldExplorerDialog
        {
            get { return toggleScaffoldExplorerDialog; }
        }

        static public void NavigateTo(object p)
        {
            string URL = p as string;

            if (URL != null)
                Process.Start(new ProcessStartInfo(URL));
        } 
    }
}