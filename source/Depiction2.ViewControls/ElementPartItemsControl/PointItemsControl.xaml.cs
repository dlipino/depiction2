﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels;
using Depiction2.ViewControls.DrawPanelBase;

namespace Depiction2.ViewControls.ElementPartItemsControl
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class PointItemsControl
    {
        public ZoomableCanvasD2 ZoomableCanvas;

        #region Constructor

        public PointItemsControl()
        {
            InitializeComponent();
//            CacheMode = new BitmapCache();
            ClipToBounds = true;
            MouseDoubleClick += PointItemsControl_MouseDoubleClick;
        }

        void PointItemsControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var parent = Tag as ElementViewGrid;
            if (parent == null) return;
            var hotelDc = parent.Tag as StoryViewModelHotel;
            if (hotelDc == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;
            if (senderDC == null) return;
            hotelDc.RequestPropertyShowForElementList(new List<IElement> { senderDC._elementModel });
            e.Handled = true;
        }

        #endregion

        //investigate usage
        public void AdjustZoomPanel()
        {
            if (ZoomableCanvas != null) ZoomableCanvas.InvalidateReality();
        }

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvasD2)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
    }
}
