﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels;
using Depiction2.ViewControls.DrawPanelBase;
using Depiction2.ViewControls.Tools;

namespace Depiction2.ViewControls.ElementPartItemsControl
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class GeometyItemsControl
    {
        public ZoomableCanvasD2 ZoomableCanvas;
        //Not really sure how the click arbiter works, ah yes it creates a gap between
        //click and double click, havent tested to see if the gap is needed in 2.0
        private MouseClickOrDoubleClickArbiter clickArbiter = null;
        private int doubleClickLagCheckTimeMS = 300;

        #region Constructor

        public GeometyItemsControl()
        {
            InitializeComponent();
            //            CacheMode = new BitmapCache();
            ClipToBounds = true;
            //Copied from 1.4
            PreviewMouseLeftButtonUp += ClickArb;
            PreviewMouseLeftButtonDown += GeometryItemsControl_MouseLeftButtonDown;
            clickArbiter = new MouseClickOrDoubleClickArbiter(this, doubleClickLagCheckTimeMS);
            clickArbiter.DoubleClick += GeometryItemsControl_PreviewMouseDoubleClick;
            clickArbiter.Click += GeometryItemsControl_MouseLeftButtonUp;
        }
        #endregion

        public void AdjustZoomPanel()
        {
            if (ZoomableCanvas != null) ZoomableCanvas.InvalidateReality();
        }

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvasD2)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }

        #region click controlling
        private void ClickArb(object sender, MouseButtonEventArgs e)
        {
            if (clickArbiter != null) clickArbiter.HandleClick(sender, e);
        }

        private void GeometryItemsControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //pretty sure 2.0 doesnt have to impletement hack since it shuts down that map while editing
            //Hack fix for shift click editing of incomplete routes/shapes while doing the edit.
            //            inAddSuspend = HackCheckForInSuspend();
            //            !inAddSuspend &&
            if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                var parent = Tag as ElementViewGrid;
                if (parent == null) return;
                var hotelDc = parent.Tag as StoryViewModelHotel;
                if (hotelDc == null) return;
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (!dc._elementModel.IsGeometryEditable) return;
                hotelDc.RequestGeometryEditForElement(dc._elementModel);
                e.Handled = true;
            }
        }
        void GeometryItemsControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState.Equals(MouseButtonState.Released))
            {
                var parent = Tag as ElementViewGrid;
                if (parent == null) return;
                var hotelDc = parent.Tag as StoryViewModelHotel;
                if (hotelDc == null) return;
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;

                if (!dc._elementModel.IsGeometryEditable) return;
                hotelDc.RequestGeometryEditForElement(dc._elementModel);
                e.Handled = true;
            }
        }

        void GeometryItemsControl_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var parent = Tag as ElementViewGrid;
            if (parent == null) return;
            var hotelDc = parent.Tag as StoryViewModelHotel;
            if (hotelDc == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;
            if (senderDC == null) return;
            hotelDc.RequestPropertyShowForElementList(new List<IElement>{senderDC._elementModel});
            e.Handled = true;
        }

        #endregion
    }
}
