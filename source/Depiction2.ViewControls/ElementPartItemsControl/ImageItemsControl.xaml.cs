﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels;
using Depiction2.ViewControls.DrawPanelBase;
using Depiction2.ViewControls.Tools;

namespace Depiction2.ViewControls.ElementPartItemsControl
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class ImageItemsControl
    {
        public ZoomableCanvasD2 ZoomableCanvas;
        private MouseClickOrDoubleClickArbiter clickArbiter = null;
        private int doubleClickLagCheckTimeMS = 300;

        #region Constructor

        public ImageItemsControl()
        {
            InitializeComponent();
            ClipToBounds = true;
            //Copied from 1.4
            PreviewMouseLeftButtonUp += ClickArb;
            PreviewMouseLeftButtonDown += ImageItemsControl_MouseLeftButtonDown;
            clickArbiter = new MouseClickOrDoubleClickArbiter(this, doubleClickLagCheckTimeMS);
            clickArbiter.DoubleClick += ImageItemsControl_PreviewMouseDoubleClick;
            clickArbiter.Click += ImageItemsControl_MouseLeftButtonUp;
        }



        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvasD2)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
        #endregion
        #region public helpers
        public void AdjustZoomPanel()
        {
            if (ZoomableCanvas != null) ZoomableCanvas.InvalidateReality();
        }
        #endregion

        #region click stuff, basically a copy paste from geometryitems control
        private void ClickArb(object sender, MouseButtonEventArgs e)
        {
            if (clickArbiter != null) clickArbiter.HandleClick(sender, e);
        }

        private void ImageItemsControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                var parent = Tag as ElementViewGrid;
                if (parent == null) return;
                var hotelDc = parent.Tag as StoryViewModelHotel;
                if (hotelDc == null) return;
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (!dc._elementModel.IsGeometryEditable)
                {
                    return;
                }
                hotelDc.RequestImageRegistration(dc._elementModel);
                e.Handled = true;
            }
        }
        private void ImageItemsControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState.Equals(MouseButtonState.Released))
            {
                var parent = Tag as ElementViewGrid;
                if (parent == null) return;
                var hotelDc = parent.Tag as StoryViewModelHotel;
                if (hotelDc == null) return;
                var vh = e.OriginalSource as FrameworkElement;
                if (vh == null) return;
                var dc = vh.DataContext as MapElementViewModel;
                if (dc == null) return;
                if (!dc._elementModel.IsGeometryEditable)
                {
                    return;
                }
                hotelDc.RequestImageRegistration(dc._elementModel);
                e.Handled = true;
            }
        }
        private void ImageItemsControl_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var parent = Tag as ElementViewGrid;
            if (parent == null) return;
            var hotelDc = parent.Tag as StoryViewModelHotel;
            if (hotelDc == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;
            if (senderDC == null) return;
            hotelDc.RequestPropertyShowForElementList(new List<IElement> { senderDC._elementModel });
//            if (senderDC._elementModel.UseEnhancedInformationText)
//            {
//                senderDC._elementModel.DisplayInformationText = true;
//            }
//            else
//            {
            //                if (senderDC._elementModel.IsGeometryEditable)
//                {
//                    return;
//                }
//                hotelDc.RequestImageRegistration(senderDC._elementModel);
//            }
            e.Handled = true;
        }



        #endregion

    }
}
