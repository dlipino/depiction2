﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Core.ViewModels.RevealerViewModels;
using Depiction2.ViewControls.Annotations;

namespace Depiction2.ViewControls
{
    public partial class DepictionStoryView
    {
        #region dep props

        public double WorldScale
        {
            get { return (double)GetValue(WorldScaleProperty); }
            set { SetValue(WorldScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldScale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldScaleProperty =
            DependencyProperty.Register("WorldScale", typeof(double), typeof(DepictionStoryView), new UIPropertyMetadata(1d));


        public Point WorldOffset
        {
            get { return (Point)GetValue(WorldOffsetProperty); }
            set { SetValue(WorldOffsetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldOffset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldOffsetProperty =
            DependencyProperty.Register("WorldOffset", typeof(Point), typeof(DepictionStoryView), new UIPropertyMetadata(new Point()));

        #endregion

        #region Datacontext holding

        private bool isDataContextStory = false;
        #endregion

        #region construction
        public DepictionStoryView()
        {
            InitializeComponent();
            Background = Brushes.Transparent;
            DataContextChanged += WorldControlGrid_DataContextChanged;
            //The mouse controls are turned somewhere else

            //drag and drop stuff? maybe, hopefully, pretty please
            AllowDrop = true;
            DragEnter += WorldControlGrid_DragEnter;
            DragOver += WorldControlGrid_DragOver;
            DragLeave += WorldControlGrid_DragLeave;
            Drop += WorldControlGrid_Drop;
            //Mouse stuff in its own method

            //Hack to convert view screen to drawn canvas coordinate
            GeoToDrawLocationService.ScreenToCartCanvas = ScreenToCartCanvasPoint;
            GeoToDrawLocationService.CartCanvasToScreen = CartCanvasToScreenPoint;
        }

        #endregion
        #region drag and drop
        void WorldControlGrid_Drop(object sender, DragEventArgs e)
        {
            //Why is the geolocation set differently on for annotation and the element?! dlp 4/8/2013
            var annotationData = e.Data.GetData(typeof(DepictionAnnotationVM)) as DepictionAnnotationVM;
            if (annotationData != null)
            {
                var annotPosition = e.GetPosition(this);
                var geoPosition = GetCanvasPoint(annotPosition);
                annotationData.SetModelGeoLocationFromProjectedPoint(-geoPosition.Y, geoPosition.X);
                return;
            }

            var elementData = e.Data.GetData(typeof(MapElementViewModel)) as MapElementViewModel;

            if (elementData == null) return;
            var position = e.GetPosition(this);
            var drawCanvasPoint = GetCanvasPoint(position);
            if (!leftMouseDownPosition.X.Equals(double.NaN))
            {
                var start = GetCanvasPoint(leftMouseDownPosition);
                elementData.ShiftModelGeoLocation(new Point(start.X, -start.Y), new Point(drawCanvasPoint.X, -drawCanvasPoint.Y));
                ResetDragState();
            }
            else
            {
                //TODO if the draw location does not match the projected system things have to change
                // negate y value
                var projectedPoint = new Point(drawCanvasPoint.X, -drawCanvasPoint.Y);
                elementData.SetModelGeoLocationFromProjectedLocation(projectedPoint);
            }
        }

        void WorldControlGrid_DragLeave(object sender, DragEventArgs e)
        {

        }

        void WorldControlGrid_DragOver(object sender, DragEventArgs e)
        {
            UpdateGeoInformationUnderScreenPoint(e.GetPosition(this));
        }

        void WorldControlGrid_DragEnter(object sender, DragEventArgs e)
        {

        }

        #endregion

        #region connectors
        void WorldControlGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldContext = e.OldValue as StoryViewModelHotel;
            if(oldContext != null)
            {
                DisconnectStoryFromMap(oldContext);
            }
            //Why is there no disconnect method
            var newContext = e.NewValue as StoryViewModelHotel;
            if (newContext != null)
            {
                ConnectStoryToMap(newContext);
                isDataContextStory = true;
            }
        }
        private void ConnectStoryToMap(StoryViewModelHotel storyVm)
        {
            //not sure how connecting things up from xaml affects the clean up process
            //            TileControl.ItemsSource = storyVm.TileDisplayerVm.Tiles;
            //            AnnotationItems.ItemsSource = storyVm.AnnotationVm.AllAnnotationVms;
            //            RevealerBorders.ItemsSource = storyVm.DisplayerVm.RevealerVms;
            MainDisplayerView.DataContext = storyVm.DisplayerVm.MainDisplayerVm;
//            TempDisplayerView.DataContext = storyVm.SimpeDisplayerVm;

            //oddity with how the region is drawn, there will be some oddities when the region can turn into a revealer
            foreach (var revealer in storyVm.DisplayerVm.RevealerVms)
            {
                if(revealer is ElementRevealerMapViewModel)
                {
                    var elemControlGrid = new ElementViewGrid();
                    elemControlGrid.DataContext = revealer;
                    elemControlGrid.Tag = storyVm;//Is this ever used?
                    Children.Add(elemControlGrid);
                }
            }

            UpdateAllRevealerClips();

            storyVm.PropertyChanged += newContext_PropertyChanged;
            storyVm.DisplayerVm.RevealerVms.CollectionChanged += RevealerVms_CollectionChanged;
            //Trigger the map location change, still buggy tiler not working quite right
            storyVm.UpdateViewFromStory();
            //            SetViewScaleAndOffsetFromViewModelValues(storyVm.StoryZoom, storyVm.VirtualPanelOffset);
            //no need to disconnect since the boolean unsures that it does not happen twice, hopefully
            ConnectNormalMouseActions();
        }
        private void DisconnectStoryFromMap(StoryViewModelHotel storyVm)
        {
            MainDisplayerView.DataContext = null;
            //remove all the elementview grids that belong to revealers
            var revealers = Children.OfType<ElementViewGrid>().ToList();
            foreach(var revealer in revealers)
            {
                if (Equals("MainDisplayerView", revealer.Name)) continue;
                Children.Remove(revealer);
            }
            storyVm.PropertyChanged -= newContext_PropertyChanged;
            storyVm.DisplayerVm.RevealerVms.CollectionChanged -= RevealerVms_CollectionChanged;
        }

        #endregion

        #region storyvm event connectors/data context

        void RevealerVms_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                var newRevealers = e.NewItems;
                foreach (var newRevealer in newRevealers)
                {
                    if (newRevealer is ElementRevealerMapViewModel)
                    {
                        var elemControlGrid = new ElementViewGrid();
                        elemControlGrid.DataContext = newRevealer;
                        elemControlGrid.Tag = DataContext;//Pass along the main datacontext, because the view gets split from the orignal parent
                        Children.Add(elemControlGrid);
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                //TODO optimize, especially if this is how elements are removed.
                var oldRevealers = e.OldItems;
                foreach (var oldRevealer in oldRevealers)
                {
                    if (oldRevealer is ElementRevealerMapViewModel)
                    {
                        var toDelete = oldRevealer as ElementRevealerMapViewModel;
                        var ecgs = Children.OfType<ElementViewGrid>().ToList();
                        //This probably doesnt work anymore since everything was changed to view models
                        foreach (var ecg in ecgs)
                        {
                            var dc = ecg.DataContext as ElementRevealerMapViewModel;
                            if (dc != null && dc.DisplayerId.Equals(toDelete.DisplayerId))
                            {
                                Children.Remove(ecg);
                                ecg.DataContext = null;
                                ecg.Tag = null;
                            }
                        }
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {

            }
        }

        void newContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var storyVM = DataContext as StoryViewModelHotel;
            if (storyVM == null) return;
            //Comes converted from view model, kind of messy
            if (e.PropertyName.Equals("VirtualPanelOffset"))
            {
                SetViewScaleAndOffsetFromViewModelValues(storyVM.StoryZoom, storyVM.VirtualPanelOffset);
            }
            else if (e.PropertyName.Equals("StoryZoom"))
            {
                SetViewScaleAndOffsetFromViewModelValues(storyVM.StoryZoom, storyVM.VirtualPanelOffset);
            }
        }
        #endregion

        #region helpers

        private void UpdateAllRevealerClips()
        {
            var elementDisplayers = Children.OfType<ElementViewGrid>();
            foreach (var elementDisplayer in elementDisplayers)
            {
                elementDisplayer.UpdateClip();
            }
        }
        protected void UpdateGeoInformationUnderScreenPoint(Point screenPosition)
        {
            //Value does not include any kind of shifting to deal with jitter/large position values
            //need to use and external screen position because the coordinates don't change 
            //when in drag mode.
            var drawCanvasPoint = ScreenToCartCanvasPoint(screenPosition);
            if (DataContext is StoryViewModelHotel)
            {
                var dc = DataContext as StoryViewModelHotel;
                var small = drawCanvasPoint;
                dc.PanZoomVm.DisplayStoryInformationForProjLocation(small.Y, small.X);
            }
        }
        protected void ResetDragState()
        {
            startMoving = false;
            mapToDrag = null;
            leftMouseDownPosition = new Point(double.NaN, double.NaN);
            if (IsMouseCaptured)
            {
                //                lastMovePosition = new Point(double.NaN, double.NaN);
                //                leftMouseDownPosition = new Point(double.NaN, double.NaN);

                ReleaseMouseCapture();
            }
        }
        #endregion

        #region private helpers for location conversion and scaling. unit tests would be nice

        public Point GetScreenPoint(Point canvasPoint)
        {
            var viewPoint = WorldScale * ((Vector)canvasPoint - (Vector)WorldOffset / WorldScale);
            return (Point)viewPoint;
        }
        public Point CartCanvasToScreenPoint(Point cartCanvasPoint)
        {
            var wpfCanvasPoint = new Point(cartCanvasPoint.X, -cartCanvasPoint.Y);
            return GetScreenPoint(wpfCanvasPoint);
        }
        //Cavnas to geo/projection coordinate is comes from another conversion. also not the the ouput of this is still
        //in draw coords ie -y
        // ConvertDrawPointToGeoPoint(Point drawPoint, IDepictionStoryProjectionService projector)
        public Point GetCanvasPoint(Point screenPoint)
        {
            if (MainDisplayerView == null || MainDisplayerView.ImageItems == null || MainDisplayerView.ImageItems.ZoomableCanvas == null)
                return screenPoint;

            var offset = MainDisplayerView.ImageItems.ZoomableCanvas.Offset;
            var scale = MainDisplayerView.ImageItems.ZoomableCanvas.Scale;
            return (Point)(((Vector)offset + (Vector)screenPoint) / scale);
        }
        public Point ScreenToCartCanvasPoint(Point screenPoint)
        {
            var cp = GetCanvasPoint(screenPoint);
            return new Point(cp.X, -cp.Y);
        }
        // seems to do the same as above
        //        public Point ConvertViewPointToDrawPoint(Point viewPoint)
        //        {
        //            var scalePos = (Vector)viewPoint / WorldScale;
        //            var mapPoint = (Point)((Vector)WorldOffset / WorldScale + scalePos);
        //
        //            return mapPoint;
        //        }
        //

        private Vector centerOffset = new Vector();
        //Updates the global canvas pixel extent
        public void SetViewScaleAndOffsetFromViewModelValues(double viewScale, Point virtualTopLeftViewOffset)
        {
            WorldScale = viewScale;
            //            WorldOffset = virtualTopLeftViewOffset;

            var w = GeoToDrawLocationService.ScreenPixelSize.Width;
            var h = GeoToDrawLocationService.ScreenPixelSize.Height;
            var offsetPoint = GeoToDrawLocationService.ScreenToCartCanvas(new Point(0, 0));
            var o = (Vector)offsetPoint;
            var shift = GeoToDrawLocationService.ScreenToCartCanvas(new Point(w / 2, -h / 2));
            var s = (Vector)shift;
            var n = (o - s) * WorldScale;
            centerOffset = n;//new Vector(0, 0);//
            WorldOffset = virtualTopLeftViewOffset + centerOffset;//shifts the topleft to the center

            GeoToDrawLocationService.CanvasPixelExtent = MainDisplayerView.ImageItems.ZoomableCanvas.ActualViewbox;
            if(DepictionAccess.DStory != null)
            {
                var geoBounds =
                    GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(GeoToDrawLocationService.CanvasPixelExtent);
                var cartRect = geoBounds;
                var story = DepictionAccess.DStory;
                var tl = new Point(cartRect.Left, cartRect.Top);
                var br = new Point(cartRect.Right, cartRect.Bottom);
                story.MainDisplayer.SetBounds(tl,br);
            }
            UpdateAllRevealerClips();
        }

        #endregion

        #region panel control via mouse

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            UpdateGeoInformationUnderScreenPoint(e.GetPosition(this));
            base.OnPreviewMouseMove(e);
        }
        private bool mapMoveConnected = false;
        //No disconnect?
        private void ConnectNormalMouseActions()
        {
            if (!mapMoveConnected)
            {
                MouseWheel += MapControlGrid_MouseWheel;
                PreviewMouseWheel += DepictionStoryView_PreviewMouseWheel;
                MouseLeftButtonDown += FindDisplayer_MouseLeftDown;
                PreviewMouseLeftButtonUp += EndMapRevealerDrag_PreviewMouseLeftButtonUp;
                MouseMove += DragRevealerOrMap_MouseMove;
                mapMoveConnected = true;
            }
        }
        
        void DepictionStoryView_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Source is AnnotationItemsControl)
            {
                MapControlGrid_MouseWheel(sender, e);
                e.Handled = true;
            }
            e.Handled = false;
        }
        
        #region map/revealer mouse events

        private FrameworkElement mapToDrag;
        private Point leftMouseDownPosition;
        private Point lastMovePosition;
        private bool startMoving = false;
        void FindDisplayer_MouseLeftDown(object sender, MouseButtonEventArgs e)
        {

            VisualTreeHelper.HitTest(this, null, FindTopLevelDisplayer, new PointHitTestParameters(e.GetPosition(this)));
            leftMouseDownPosition = e.GetPosition(this);
            startMoving = false;
            lastMovePosition = leftMouseDownPosition;
        }

        private HitTestResultBehavior FindTopLevelDisplayer(HitTestResult result)
        {
            var fe = result.VisualHit as ElementViewGrid;
            if (fe == null)
            {
                mapToDrag = null;
                return HitTestResultBehavior.Continue;
            }
            var rvm = fe.DataContext as ElementRevealerMapViewModel;
            if (rvm != null)
            {
                if (!rvm.RevealerAnchored)
                {
                    mapToDrag = fe;
                    return HitTestResultBehavior.Stop;
                }
            }
            var mapVm = fe.DataContext as ElementDisplayerMapViewModel;
            if (mapVm == null)
            {
                mapToDrag = null;
                return HitTestResultBehavior.Continue; //This would be bad
            }
            mapToDrag = fe;
            return HitTestResultBehavior.Stop;
        }

        void EndMapRevealerDrag_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ResetDragState();
        }
        void DragRevealerOrMap_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = false;
            if (mapToDrag == null) return;

            var currentPosition = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!startMoving)
                {
                    if (currentPosition == leftMouseDownPosition)
                    {
                        return;
                    }
                    startMoving = true;
                }
                var pixelMouseDelta = currentPosition - lastMovePosition;
                var rev = mapToDrag.DataContext as ElementRevealerMapViewModel;
                if (rev != null && !rev.RevealerAnchored)
                {
                    CaptureMouse();
                    //icky
                    if (DepictionAccess.DStory != null)
                    {
                        DepictionAccess.DStory.TopRevealerId = rev.DisplayerId;
                    }
                    var left = rev.DrawLeft;
                    var top = rev.DrawTop;
                    var w = rev.DrawWidth;
                    var h = rev.DrawHeight;

                    var scaledHorzChange = pixelMouseDelta.X / WorldScale;
                    var scaledVertChange = pixelMouseDelta.Y / WorldScale;

                    left += scaledHorzChange;
                    top += scaledVertChange;
                    rev.UpdateGeoBoundsFromDrawRect(new Rect(left, top, w, h));

                }
                else if (mapToDrag.DataContext is ElementDisplayerMapViewModel)
                {
                    //kind of strange but i guess the main element displayer never really moves
                    var mainDc = DataContext as StoryViewModelHotel;
                    if (mainDc == null) return;

                    CaptureMouse();
                    //                            //To keep the view in charge of itself
                    //                            WorldOffset = WorldOffset - pixelMouseDelta;
                    mainDc.ShiftOffsetWithPanelPixelVector(pixelMouseDelta);
                }
                e.Handled = true;
                lastMovePosition = currentPosition;

            }
        }

        #endregion

        void MapControlGrid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var mapdc = DataContext as StoryViewModelHotel;
            if (mapdc != null)
            {
                var oneLineDelta = Mouse.MouseWheelDeltaForOneLine;
                var delta = e.Delta;
                var x = Math.Pow(2, delta / 3.0 / oneLineDelta);
                // Adjust the offset to make the point under the mouse stays still.
                var position = (Vector)e.GetPosition(this);
                //voodoo math to find the new center point after the scale change
                var newOffset = (Point)((Vector)(WorldOffset + position) * x - position - centerOffset);
                //This is doing some pre scaling before sending it to the viewmodel which sends it
                //to the story model. Still not sure where all point convserion should take place.
                var updateScale = WorldScale * x;
                //                //There are issues with getting bad data from a projection, so keeping
                //                //the view in charge of itself might be useful
                //                WorldScale = updateScale;
                //                WorldOffset = newOffset;
                var scaleOffset = ((Point)((Vector)(newOffset) / updateScale));
                var projPoint = new Point(scaleOffset.X, -scaleOffset.Y);
                mapdc.AdjustStoryModelZoom(x, projPoint);
                e.Handled = true;
            }
        }

        #endregion
    }
}
