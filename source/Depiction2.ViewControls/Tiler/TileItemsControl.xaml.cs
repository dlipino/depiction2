﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Depiction2.Core.ViewModels.Tiling;

namespace Depiction2.ViewControls.Tiler
{
    /// <summary>
    /// Interaction logic for TileItemsControl.xaml
    /// </summary>
    public partial class TileItemsControl
    {
        protected ZoomableCanvas ZoomableCanvas { get; set; }
        #region Dep props
        public Rect WorldViewportRectPixels
        {
            get { return (Rect)GetValue(WorldViewportRectPixelsProperty); }
            set { SetValue(WorldViewportRectPixelsProperty, value); }
        }
        public double Scale
        {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); }
        }

        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(TileItemsControl), new UIPropertyMetadata(0.0));

        public double Offset
        {
            get { return (double)GetValue(OffsetProperty); }
            set { SetValue(OffsetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Offset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OffsetProperty =
            DependencyProperty.Register("Offset", typeof(double), typeof(TileItemsControl), new UIPropertyMetadata(0.0));

        // Using a DependencyProperty as the backing store for WorldViewportRectPixels.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldViewportRectPixelsProperty =
            DependencyProperty.Register("WorldViewportRectPixels", typeof(Rect), typeof(TileItemsControl), new UIPropertyMetadata(Rect.Empty, ViewportPixelChange));

        #endregion

        #region events for dep props
        private static void ViewportPixelChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }
        #endregion

        #region Constructor

        public TileItemsControl()
        {
            InitializeComponent();
            IsHitTestVisible = false;
            Focusable = false;
        }

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
        #endregion

        private static void Animate(DependencyObject target, string property, double from, double to, int duration)
        {
            var animation = new DoubleAnimation();
            animation.From = from;
            animation.To = to;
            animation.Duration = new TimeSpan(0, 0, 0, 0, duration);
            Storyboard.SetTarget(animation, target);
            Storyboard.SetTargetProperty(animation, new PropertyPath(property));

            var storyBoard = new Storyboard();
            storyBoard.Children.Add(animation);
            //storyBoard.Completed += completed;
            storyBoard.Begin();
        }

        private void Image_ImageOpened(object sender, RoutedEventArgs e)
        {
            var image = sender as Image;
            if (image == null) return;
            image.Opacity = 0;
            Animate(image, "Opacity", 0, 1, 600);
            var tileViewModel = image.DataContext as TileViewModel;
            if (tileViewModel == null) return;
            tileViewModel.ImageOpened = true;
        }

       

    }
}
