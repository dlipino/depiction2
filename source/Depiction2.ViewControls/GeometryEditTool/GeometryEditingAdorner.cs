﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.ElementViewModels;
using Depiction2.Utilities.Drawing;

namespace Depiction2.DrawControls.GeometryEditTool
{
    public class GeometryEditingAdorner : Adorner
    {
        private double inverseScale = 1;
        private VisualCollection visualChildren;
        private Thumb dragger;
        private TranslateTransform dragTranslate = new TranslateTransform();
        private GeometryEditingThumb helpDrag;
        public bool IsShiftDown { get { return (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)); } }
        private bool eventsAttached;
        #region Constructor

        public GeometryEditingAdorner(FrameworkElement adornedElement, double currentInverseScale)
            : base(adornedElement)
        {
            visualChildren = new VisualCollection(this);
            var context = adornedElement.DataContext as MapElementViewModel;
            if (context == null) return;
            var model = context._elementModel;
//            if(!context.IsElementInEditMode)
//            {
//                context.IsElementInEditMode = true;
//            } 
            dragger = new Thumb();
            dragger.Opacity = 0;
            Width = 0;
            Height = 0;
            ClipToBounds = false;
            visualChildren.Add(dragger);
            dragger.Name = "nosee";
            dragger.RenderTransform = dragTranslate;
            inverseScale = currentInverseScale;

            var points = model.ElementGeometry.GeometryPoints;
            var enhanced = new EnhancedPointListWithChildren();
            enhanced.Points = points.ToList();
            CreateAdorner(new List<EnhancedPointListWithChildren>{enhanced});
            AttachEvents();
        }
        #endregion

        #region public helpers

        public void UpdateDrawnAdorner(MapElementViewModel modified)
        {
            var model = modified._elementModel;
            var points = model.ElementGeometry.GeometryPoints;
            var enhanced = new EnhancedPointListWithChildren();
            enhanced.Points = points.ToList();
            CreateAdorner(new List<EnhancedPointListWithChildren> { enhanced });
        }

        #endregion

        protected void AttachEvents()
        {
            if (eventsAttached) return;
            eventsAttached = true;
            dragger.DragDelta += dragger_DragDelta;
            dragger.DragCompleted += dragger_DragCompleted;
            PreviewMouseLeftButtonDown += ZOIEditingAdorner_MouseLeftButtonDown;
            Application.Current.MainWindow.KeyDown += MainWindow_KeyDown;
            Application.Current.MainWindow.KeyUp += MainWindow_KeyDown;

        }
        protected void RemoveEvents()
        {
            dragger.DragDelta -= dragger_DragDelta;
            dragger.DragCompleted -= dragger_DragCompleted;
            PreviewMouseLeftButtonDown -= ZOIEditingAdorner_MouseLeftButtonDown;
            Application.Current.MainWindow.KeyDown -= MainWindow_KeyDown;
            Application.Current.MainWindow.KeyUp -= MainWindow_KeyDown;
        }
        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
           
            if (e.Key.Equals(Key.LeftShift) || e.Key.Equals(Key.RightShift))
            {
                foreach (var t in visualChildren)
                {
                    var zt = t as GeometryEditingThumb;
                    if (zt != null)
                    {
                        zt.ShiftDown = IsShiftDown;
                    }
                }
            }
        }

        void dragger_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            var context = ((FrameworkElement)AdornedElement).DataContext as MapElementViewModel;
            if (context == null) return;
            UpdateDrawnAdorner(context);
        }

        void dragger_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var shift = new Point(e.HorizontalChange, e.VerticalChange);
            dragTranslate.X += shift.X;
            dragTranslate.Y += shift.Y;
            //the thumbs have the point list 
            dragHelper(shift, helpDrag);
            InvalidateVisual();//do the onRendered
            var context = ((FrameworkElement)AdornedElement).DataContext as MapElementViewModel;
            if (context == null) return;
//            context.ZOI.ZOIToDraw = context.ZOI.ZOIToDraw;
        }

        protected void dragHelper(Point shift, GeometryEditingThumb thumb)
        {
            if (thumb == null) return;
            var type = thumb.EditThumbType;
            var vStart = thumb.LineStart;
            var eEnd = thumb.LineEnd;
            switch (type)
            {
                case GeometryEditThumbType.Vertex:

                    var index = thumb.parentPoints.Points.IndexOf(vStart);
                    var newPoint = new Point(vStart.X + shift.X, vStart.Y + shift.Y);
                    thumb.LineStart = newPoint;
                    thumb.parentPoints.Points[index] = newPoint;
                    thumb.LocationTransform.X += shift.X;
                    thumb.LocationTransform.Y += shift.Y;

                    break;
                case GeometryEditThumbType.Edge:
                    var sIndex = thumb.parentPoints.Points.IndexOf(vStart);
                    var newStart = new Point(vStart.X + shift.X, vStart.Y + shift.Y);
                    thumb.parentPoints.Points[sIndex] = newStart;
                    thumb.LineStart = newStart;

                    var eIndex = thumb.parentPoints.Points.IndexOf(eEnd);
                    var newEnd = new Point(eEnd.X + shift.X, eEnd.Y + shift.Y);
                    thumb.LineEnd = newEnd;
                    thumb.parentPoints.Points[eIndex] = newEnd;
                    break;
            }
        }

        void ZOIEditingAdorner_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var fr = e.OriginalSource as FrameworkElement;
            if (fr == null) return;
            var dc = fr.DataContext as GeometryEditingThumb;
            if (dc == null) return;
            helpDrag = dc;
            if (IsShiftDown)
            {
//                var context = ((FrameworkElement)AdornedElement).DataContext as MapElementViewModel;
//                if (context == null) return;
//                if (helpDrag.EditThumbType.Equals(GeometryEditThumbType.Vertex))
//                {
//                    if (context.ZOI.RemovePoint(dc.LineStart))
//                    {
//                        CreateAdorner(context.ZOI.ZOIToDraw);
//                        InvalidateVisual();
//                    }
//                }
//                else if (helpDrag.EditThumbType.Equals(GeometryEditThumbType.Edge))
//                {
//                    if (context.ZOI.InsertPointBefore(dc.LineEnd, e.GetPosition(this)))
//                    {
//                        CreateAdorner(context.ZOI.ZOIToDraw);
//                        InvalidateVisual();
//                    }
//                }
            }
            else
            {
//                ViewModelHelperMethods.AttachMouseToDragger(e, dragger);
            }
        }

        public void AdjustAdornerScale(double newInverseScale)
        {
            if (inverseScale == newInverseScale) return;
            inverseScale = newInverseScale;
            AdjustScaleOfDragThumbs(inverseScale);
            InvalidateVisual();
        }
        protected void AdjustScaleOfDragThumbs(double inScale)
        {
            foreach (var t in visualChildren)
            {
                var zt = t as GeometryEditingThumb;
                if (zt != null)
                {
                    zt.AdjustVisualsForScale(inScale);
                }
            }
        }
        protected void CreateAdorner(List<EnhancedPointListWithChildren> allPoints)
        {
            if (allPoints == null) return;
            visualChildren.Clear();
            visualChildren.Add(dragger);
            foreach (var list in allPoints)
            {
                var prev = new Point(double.NaN, double.NaN);
                foreach (var point in list.Points)
                {
                    var zoiThumb = new GeometryEditingThumb(point, list);
                    zoiThumb.LineThickness = 2 * inverseScale;
                    zoiThumb.Background = Brushes.Transparent;
                    visualChildren.Add(zoiThumb);
                    if (!prev.X.Equals(double.NaN))
                    {
                        visualChildren.Insert(0, new GeometryEditingThumb(prev, point, list) { LineThickness = 2 * inverseScale });
                    }
                    prev = point;
                }
                if (list.IsClosed)
                {
                    visualChildren.Insert(0, new GeometryEditingThumb(prev, list.Points[0], list) { LineThickness = 2 * inverseScale });
                }
            }
            AdjustScaleOfDragThumbs(inverseScale);
            InvalidateArrange();
            InvalidateVisual();
            InvalidateMeasure();
            UpdateLayout();
        }
        public void RemoveZOIAdorner()
        {
            var fe = (FrameworkElement)AdornedElement;
            var myAdornerLayer = AdornerLayer.GetAdornerLayer(fe);
            if (myAdornerLayer == null) return;
            Adorner[] toRemoveArray = myAdornerLayer.GetAdorners(fe);
            if (toRemoveArray != null)
            {
                RemoveEvents();
                myAdornerLayer.Remove(this);
            }
        }
        #region overrides
        protected override void OnRender(DrawingContext drawingContext)
        {
//            var context = ((FrameworkElement)AdornedElement).DataContext as MapElementViewModel;
//            if (context == null) return;
//            var points = context.ZOI.ZOIToDraw;
//            var circleThickness = 1 * inverseScale;
//            var lineThickness = 4 * inverseScale;
//            var brush = Brushes.Gray;
//            var linePen = new Pen(Brushes.Red, lineThickness);
//            var circlePen = new Pen(Brushes.Red, circleThickness);
//            var cirleSize = 7.5 * inverseScale;
//
//            foreach (var list in points)
//            {
//                var prev = new Point(double.NaN, double.NaN);
//                foreach (var point in list.Outline)
//                {
//                    if (!prev.X.Equals(double.NaN))
//                    {
//                        drawingContext.DrawLine(linePen, prev, point);
//                    }
//                    prev = point;
//                }
//                if (list.IsClosed)
//                {
//                    drawingContext.DrawLine(linePen, prev, list.Outline[0]);
//                }
//            }
//            foreach (var list in points)
//            {
//                foreach (var point in list.Outline)
//                {
//                    drawingContext.DrawEllipse(brush, circlePen, point, cirleSize, cirleSize);
//                }
//            }

            base.OnRender(drawingContext);
        }
        protected override int VisualChildrenCount
        {
            get { return visualChildren.Count; }
        }
        protected override Visual GetVisualChild(int index)
        {
            return visualChildren[index];
        }
        protected override Size ArrangeOverride(Size finalSize)
        {
//            foreach (FrameworkElement vc in visualChildren)
//            {
//                var size = new Size(3000000, 300000);
//                if (vc.Name.Equals("nosee"))
//                {
//                    size = vc.DesiredSize;
//                }
//
//                var rect = new Rect(new Point(), size);
//                if (vc is ZOIEditingThumb)
//                {
//                    var p = (ZOIEditingThumb)vc;
//                    
//                    if (p.EditThumbType.Equals(ZOIEditThumbType.Vertex))
//                    {
//                        rect = new Rect(new Point(), new Size(p.Width, p.Height));
//                    }
//                    else if (p.EditThumbType.Equals(ZOIEditThumbType.Edge))
//                    {
//                        //                        rect = new Rect(p.LineStart,p.LineEnd);
//                        //                        Console.WriteLine(p.DesiredSize);
//                        //                                                var x = p.LineEnd.X > p.LineStart.X ? p.LineStart.X : p.LineStart.X;
//                        //                                                var y = p.LineEnd.Y > p.LineStart.Y ? p.LineStart.Y : p.LineStart.Y;
//                        //                        rect = new Rect(new Point(), p.DesiredSize);
//                    }
//                }
//                vc.Arrange(rect);
//            }
            return base.ArrangeOverride(finalSize);
        }
        #endregion

    }
}