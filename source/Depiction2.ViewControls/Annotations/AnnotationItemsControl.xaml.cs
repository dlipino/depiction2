﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Depiction2.Core.ViewModels;

namespace Depiction2.ViewControls.Annotations
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class AnnotationItemsControl
    {
        public ZoomableCanvas ZoomableCanvas;

        #region Constructor

        public AnnotationItemsControl()
        {
            InitializeComponent();
            ClipToBounds = true;
            MouseMove += AnnotationItemsControl_MouseMove;
        }

        void AnnotationItemsControl_MouseMove(object sender, MouseEventArgs e)
        {
            var fe = e.OriginalSource as FrameworkElement;
            //make sure that the annotations image is grabbed and not the text
            if (fe != null && e.LeftButton == MouseButtonState.Pressed && (fe is Image))
            {
                var dc = fe.DataContext as DepictionAnnotationVM;
                if (dc != null)
                {
                    var dragElement = new DataObject(dc);
                    DragDrop.DoDragDrop(this, dragElement, DragDropEffects.Move);
                    e.Handled = true;
                }
            }
        }

        #endregion

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }

        private void Annotation_Drag(object sender, DragDeltaEventArgs e)
        {
            var t = sender as Thumb;
            if (t == null) return;
            var dc = t.DataContext as DepictionAnnotationVM;
            if (dc == null) return;
            dc.ContentLocationX += e.HorizontalChange;
            dc.ContentLocationY += e.VerticalChange;
        }

        private void annotationResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var f = sender as FrameworkElement;
            if (f == null) return;
            var tp = f.TemplatedParent as ContentPresenter;
            if (tp == null) return;
            var annotContext = tp.DataContext as DepictionAnnotationVM;
            if (annotContext == null) return;
            annotContext.Width += e.HorizontalChange;
            annotContext.Height += e.VerticalChange;
            e.Handled = true;
        }

        private void ItemsControl_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var clickTarget = e.OriginalSource as FrameworkElement;
            if(clickTarget != null)
            {
                if(clickTarget.Name.Equals("IconImage"))
                {
                    var vm = clickTarget.DataContext as DepictionAnnotationVM;
                    if(vm != null)
                    {
                        if(vm.Collapsed.Equals(Visibility.Visible))
                        {
                            vm.Collapsed = Visibility.Collapsed;
                        }else
                        {
                            vm.Collapsed = Visibility.Visible;
                        }
                    }
                }
            }

        }
    }
}
