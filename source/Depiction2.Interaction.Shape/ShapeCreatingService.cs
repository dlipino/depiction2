using System;
using System.Collections.Generic;
using System.Windows;

namespace Depiction2.Interaction.Shape
{
    /// <summary>
    /// Service to render circles and octagons.
    /// </summary>
    public class ShapeCreatingService
    {
        /// <summary>
        /// Creates a circle of this radius centered at the origin.
        /// </summary>
        /// <param name="radius">The radius.</param>
        /// <returns>The circle.</returns>
        public static List<Point> CreateCircle(double radius)
        {
            return CreateArc(radius, 0.0, 360.0);
        }

        /// <summary>
        /// Creates an ellispe centered at the origin with a the length along x-axis and b the length along y-axis.
        /// </summary>
        /// <param name="a">The length of the major axis</param>
        /// <param name="b">The length of the minor axis</param>
        /// <param name="centered">0 centered at origin, 1, centered at left focus, -1 centered at right focus</param>
        /// <returns>The ellipse.</returns>
        public static List<Point> CreateEllipse(double a, double b, int centered)
        {
            return CreateEllipse(a, b, centered, 0, 360);
        }

        /// <summary>
        /// Creates an arc of an ellispe centered at the origin with a the length along x-axis and b the length along y-axis.
        /// </summary>
        /// <param name="a">The length of the major axis</param>
        /// <param name="b">The length of the minor axis</param>
        /// <param name="centered">0 centered at origin, 1, centered at left focus, -1 centered at right focus</param>
        /// <param name="stopAngle">Stop angle for the arc</param>
        /// <param name="startAngle">Start angle for the arc</param>
        /// <returns>The arc.</returns>
        public static List<Point> CreateEllipse(double a, double b, int centered, int startAngle, int stopAngle)
        {
            // bugbug - do parameter checking - are a and b both positive?

            const int steps = 19;

            // we are given the full length and width of ellipse, divide by two to get length 
            // from origin to ellipse along each axis
            a /= 2;
            b /= 2;

            // normally want the ellipse offset to one of the focus. offset is 
            // calculated and added to the x values to move the ellipse from origin centered
            // to the focus centered.

            double offset = 0.0;
            if (centered != 0)
            {
                offset = Math.Sqrt((a * a) - (b * b));
                if (centered < 0)
                {
                    offset *= -1.0;
                }
            }

            var points = new List<Point>();
            if (Math.Abs(startAngle - stopAngle) < 360)
            {
                //not a full circle...we need a pie-shape
                points.Add(new Point(0, 0));
            }

            // bugbug need to check if we are adding the same points twice when a and b are small
            for (int i = startAngle; i < stopAngle; i += (int)Math.Abs((double)(startAngle - stopAngle)) / steps)
            // bugbug need to watch for round off error
            {
                double alpha = Math.PI * i / 180.0;
                double sinAlpha = Math.Sin(alpha);
                double cosAlpha = Math.Cos(alpha);
                double x = offset + (a * cosAlpha);
                double y = (b * sinAlpha);
                points.Add(new Point(x, y));
            }
            // close the polygon by connecting back to first point
            points.Add(new Point(offset + a, 0));

            return points;
        }

        /// <summary>
        /// Creates an arc of a circle centered at the origin with the given radius and starting and ending angles.
        /// </summary>
        /// <param name="rad">radius of the arc</param>
        /// <param name="stopAngle">Stop angle for the arc</param>
        /// <param name="startAngle">Start angle for the arc</param>
        /// <returns>The arc.</returns>
        // creates 
        public static List<Point> CreateArc(double rad, double startAngle, double stopAngle)
        {
            const int steps = 19;

            var points = new List<Point>();
            bool isArc = false;
            if (stopAngle - startAngle < 360)
            {
                //not a full circle...we need a pie-shape
                points.Add(new Point(0, 0));
                isArc = true;
            }

            // sanity check on arguments to avoid infinite loop
            if (startAngle > stopAngle)
            {
                //                DepictionAccess.NotificationService.SendNotification("Cannot create an arc whose start orientation is greater than its end orientation.");
                return points;
            }

            // bugbug need to check if we are adding the same points twice when a and b are small (possibly moot now that we're using doubles)
            for (double i = startAngle; i <= stopAngle; i += (stopAngle - startAngle) / steps)
            {
                double alpha = Math.PI * (i - 90) / 180.0;
                double sinAlpha = Math.Sin(alpha);
                double cosAlpha = Math.Cos(alpha);
                double x = (rad * cosAlpha);
                double y = (rad * sinAlpha);
                points.Add(new Point(x, y));
            }

            if (isArc)
            {
                points.Add(new Point(0, 0));
            }

            return points;
        }


        /// <summary>
        /// Creates an octagon centered at the origin.
        /// </summary>
        /// <param name="radius">The radius.</param>
        /// <returns>The octagon.</returns>
        public static List<Point> CreateOctagon(double radius)
        {
            var returnvalue = new List<Point>();

            returnvalue.AddRange(new[] { new Point(-(radius / 2), radius), new Point(radius / 2, radius), new Point(radius, radius / 2), new Point(radius, -(radius / 2)), new Point(radius / 2, -radius), new Point(-radius / 2, -radius), new Point(-radius, -radius / 2), new Point(-radius, radius / 2), new Point(-(radius / 2), radius) });

            return returnvalue;
        }

  
        /// <summary>
        /// Creates a rectangle centered at the origin.
        /// </summary>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        /// <returns>The rectangle.</returns>
        public static List<Point> CreateRectangle(double height, double width)
        {
            var returnvalue = new List<Point>();

            returnvalue.AddRange(new[] { new Point(-(width / 2), height / 2), new Point(width / 2, height / 2), new Point(width / 2, -(height / 2)), new Point(-(width / 2), -(height / 2)), new Point(-(width / 2), height / 2) });

            return returnvalue;
        }

        ///<summary>
        /// Adds to this element a zone of influence that is a polygon described the list of points.
        ///</summary>
        ///<param name="pointList">The polygon to use for this element's zone of influence.</param>
//        public static List<Point> GeneratePolygonZOIFromPointList(Point? center, List<Point> pointList,DistanceUnit distanceType)
//        {
//            throw new NotImplementedException();
//            if (center == null) return null;
//            var p = (Point) center;
//            if (pointList == null || pointList.Count <= 1) throw new Exception("Cannot generate polygon from empty point list.");
//            var latlong = new LatitudeLongitudeDotSpatial(p.Y, p.X);
//            if (!latlong.IsValid) throw new Exception("Cannot generate polygon with no center point.");
//
////            var geometryFactory = new GeometryFactory();
////
////            var coordinates = new CoordinateList();
////            var latlong = center as LatitudeLongitude;
////            if (latlong == null) return null;
//            var outList = new List<Point>();
//            foreach (var point in pointList)
//            {
//                var newPos = latlong.TranslatePositionToPoint(point.X, -point.Y, distanceType);
//                outList.Add(newPos);
////                coordinates.Add(new Coordinate(newPos.Longitude, newPos.Latitude));
//            }
////            ILinearRing linearRing = geometryFactory.CreateLinearRing(coordinates.ToArray());
//            return outList;
//        }
    }
}