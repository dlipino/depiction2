﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Resources;

namespace Depiction2.Interaction.Shape
{
    [ResourceExtensionMetadata(DisplayName = "Shape Resource", Name = "ResourcesShape", Author = "Depiction Inc.", ExtensionPackage = "Default", ResourceName = "ShapeResources")]
    public class ShapeResources : IDepictionResourceExtension
    {

        public ResourceDictionary ExtensionResources
        {
            get
            {
                var uri = new Uri("/Depiction2.Interaction.Shape;Component/Resources/Images/ShapeResources.xaml", UriKind.RelativeOrAbsolute);
                var rd = new ResourceDictionary { Source = uri };
                return rd;
            }
        }

        public Dictionary<string, Type> ExtensionTypes
        {
            get { return new Dictionary<string, Type>(); }
        }

        public void Dispose()
        {

        }
    }
}