using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Interaction.Shape.ShapeCreators
{
//    [Export(typeof(BaseBehavior))]
//    [Behavior("CreateRectangle", "Calculate a simple rectangle", "Calculates a simple  rectangle")]
    [BehaviorMetadata("CreateRectangle", "Calculate a simple rectangle", "Calculates a simple  rectangle")]
    public class CreateRectangleBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] depictionParameters = 
            new[]
                {
                    new DepictionParameterInfo("Width", typeof (Distance))
                        {ParameterName = "Width", ParameterDescription = "The new Width for the rectangle"},
                    new DepictionParameterInfo("Length", typeof (Distance))
                        {ParameterName = "Length", ParameterDescription = "The length of the rectangle"},
                    new DepictionParameterInfo("Rotation", typeof (double))
                        {ParameterName = "Rotation", ParameterDescription = "The rotation of the rectangle"}
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return depictionParameters; }
        }

        public string FriendlyName
        {
            get { return "Create a rectagular zone of influence"; }
        }

        public string Description
        {
            get
            {
                return
                    "Set the subscribers zone of influence to a rectangle of the dimensions provided by the parameters.";
            }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var width = (Distance) parameterBag["Width"];
            var length = (Distance) parameterBag["Length"];
            var rotation = (double)parameterBag["Rotation"];

            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;

            var points = ShapeCreationService.CreateRectangle(width.GetValue(measureSystem, measureScale), length.GetValue(measureSystem, measureScale));

            List<Point> newPointList = TransformPoints(points, rotation);
            var pointDistances = DotSpatialHelper.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);
            var shape = DotSpatialHelper.GeneratePolygonZOIFromPointList(subscriber.GeoLocation, newPointList, pointDistances);

            var geom = DepictionGeometryService.CreateGeometry(shape);
            subscriber.UpdatePrimaryPointAndGeometry(subscriber.GeoLocation, geom);
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        public List<Point> TransformPoints(List<Point> pointList, double angle)
        {
            double theta = (angle - 90) * Math.PI / 180;
            var newPointList = new List<Point>();
            for (int i = 0; i < pointList.Count; i++)
            {
                double x = pointList[i].X*Math.Cos(theta) - pointList[i].Y*Math.Sin(theta);
                double y = pointList[i].X*Math.Sin(theta) + pointList[i].Y*Math.Cos(theta);
                newPointList.Add(new Point(x, y));
            }

            return newPointList;
        }
    }
}