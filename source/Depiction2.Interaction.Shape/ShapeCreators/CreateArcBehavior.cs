﻿using System.Collections.Generic;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.API.ValidationRules;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Interaction.Shape.ShapeCreators
{
    [BehaviorMetadata("CreateArc", "Calculate a simple arc", "Calculates a simple  arc")]
    public class CreateArcBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] depictionParameters =
            new[]
                {
                    new DepictionParameterInfo("Radius", typeof (Distance))
                        {
                            ParameterName = "Radius",
                            ParameterDescription = "The new radius for the arc shape",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (Distance),
                                                                   "The new radius value must be a valid distance")
                                    }
                        },
                    new DepictionParameterInfo("Orientation", typeof (double))
                        {
                            ParameterName = "Orientation",
                            ParameterDescription = "The new orientation angle for the arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new orientation value must be a number")
                                    }
                        },
                    new DepictionParameterInfo("Width", typeof (double))
                        {
                            ParameterName = "Width",
                            ParameterDescription = "The new angle width for the arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new angle width value must be a number")
                                    }
                        }
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return depictionParameters; }
        }

        public string FriendlyName
        {
            get { return "Create arc"; }
        }

        public string Description
        {
            get { return "Set the element's zone of influence to the specified arc"; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var radius = (Distance)parameterBag["Radius"];
            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;
            var angleObject = parameterBag["Orientation"];
            var angleValue = 0d;
            if(angleObject is Angle)
            {
                angleValue = ((Angle)angleObject).Value;
            }else if(angleObject is double)
            {
                angleValue = (double) angleObject;   
            }
            double startAngle = angleValue - (double)parameterBag["Width"] / 2;
            double stopAngle = angleValue + (double)parameterBag["Width"] / 2;

            var points = ShapeCreationService.CreateArc(radius.GetValue(measureSystem,measureScale), startAngle, stopAngle);
            
            if (points.Count < 3) return new BehaviorResult();
            var pointDistances = DotSpatialHelper.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);
            var polygon = DotSpatialHelper.GeneratePolygonZOIFromPointList(subscriber.GeoLocation, points, pointDistances);
            var geom = DepictionGeometryService.CreateGeometry(polygon);
            subscriber.UpdatePrimaryPointAndGeometry(subscriber.GeoLocation, geom);
            return new BehaviorResult { SubscriberHasChanged = true };
        }
    }
}