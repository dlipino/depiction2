using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Interaction.Shape.ShapeCreators
{
    [BehaviorMetadata("CreateShape", "Create a shape using input parameters", "Give element zoi a desired shape")]
    public class CreateShapeBehavior : BaseBehavior
    {
        List<DepictionParameterInfo> arbitraryParams = new List<DepictionParameterInfo>();

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return arbitraryParams.ToArray(); }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var radius = (Distance)parameterBag["Radius"];
            var measureSystem = Settings.Default.MeasurementSystem;
            var measureScale = Settings.Default.MeasurementScale;

            var shapeToCreate = "Circle";
            if(parameterBag.ContainsKey("Shape"))
            {
                shapeToCreate = (string)parameterBag["Shape"];
            }
            List<Point> points;
            switch (shapeToCreate)
            {
                case "Octagon":
                    points = ShapeCreationService.CreateOctagon(radius.GetValue(measureSystem,measureScale));
                    break;
                case "Circle":
                    points = ShapeCreationService.CreateCircle(radius.GetValue(measureSystem, measureScale));
                    break;
                default:
//                    string message = string.Format("Element \"{0}\" has a \"Shape\" property with an invalid value: \"{1}\" . This could be an error in the element's .dml file. Will use a default zone of influence for this element.",
//                        subscriber.DisplayName(), shapeToCreate);
                    points = ShapeCreationService.CreateCircle(new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 50).GetValue(measureSystem, measureScale));
//                    DepictionAccess.NotificationService.SendNotification(message);
                    break;
            }
            var pointDistances = DotSpatialHelper.ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(measureSystem, measureScale);
            var geom = DepictionGeometryService.CreateGeometry(DotSpatialHelper.GeneratePolygonZOIFromPointList(subscriber.GeoLocation, points, pointDistances));
            subscriber.UpdatePrimaryPointAndGeometry(subscriber.GeoLocation, geom);
            return new BehaviorResult { SubscriberHasChanged = true };
 
        }
    }
}