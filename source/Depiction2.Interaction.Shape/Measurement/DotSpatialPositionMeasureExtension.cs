﻿using System.Windows;
using Depiction2.API.Extension.Tools.Positioning;
using Depiction2.Base.Measurement;
using DotSpatial.Positioning;

namespace Depiction2.Interaction.Shape.Measurement
{
    [PositionMeasureExtensionMetadata(DisplayName = "Position measurements using DotSpatial", Name = "DotSpatialMeasurements", Author = "Depiction Inc.", ExtensionPackage = "Default")]
    public class DotSpatialPositionMeasureExtension : IPositionMeasureExtension
    {
        public void Dispose()
        {

        }

        public Point TranslatePoint(Point originPoint, double degreeDirction, double distance, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            var distanceUnit = DistanceUnit.StatuteMiles;
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    distanceUnit = DistanceUnit.Inches;
                    break;
                case MeasurementConstants.ImperialNormal:
                    distanceUnit = DistanceUnit.Feet;
                    break;
                case MeasurementConstants.ImperialLarge:
                    distanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case MeasurementConstants.MetricSmall:
                    distanceUnit = DistanceUnit.Centimeters;
                    break;
                case MeasurementConstants.MetricNormal:
                    distanceUnit = DistanceUnit.Meters;
                    break;
                case MeasurementConstants.MetricLarge:
                    distanceUnit = DistanceUnit.Kilometers;
                    break;
            }
            return originPoint.TranslateTo(new Angle(degreeDirction), distance, distanceUnit);
        }

        public double DistanceBetween(Point start, Point end, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            return start.DistanceTo(end, specificMeasurement, specificScale);
        }
    }
}