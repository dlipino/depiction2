﻿using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using DotSpatial.Positioning;

namespace Depiction2.Interaction.Shape
{
    public class LatitudeLongitudeDotSpatial : IDepictionLatitudeLongitude
    {
        public double Latitude { get { return position.Latitude.DecimalDegrees; } }
        public double Longitude { get { return position.Longitude.DecimalDegrees; } }
        public int UTMZone { get { return position.Longitude.UtmZoneNumber; } }
        private Position position;

        public LatitudeLongitudeDotSpatial(double lat, double lon)
        {
            position = new Position(new Latitude(lat), new Longitude(lon));
        }

        public LatitudeLongitudeDotSpatial(string latlongString)
        {
            position = new Position(latlongString);
        }
        #region to string helpers

        

        #endregion
        #region Helpers that are dotspatial only


        public bool IsValid
        {
            get { return !position.IsInvalid; }
        }

        public LatitudeLongitudeDotSpatial TranslateTo(Azimuth bearing, double distance, DistanceUnit measurementSystem)
        {
            var pos = new Position(position);
            Distance gfDistance = new Distance(distance, measurementSystem);
            var newPos = pos.TranslateTo(bearing, gfDistance);
            return new LatitudeLongitudeDotSpatial(newPos.Latitude.DecimalDegrees, newPos.Longitude.DecimalDegrees);
        }
        public LatitudeLongitudeDotSpatial TranslateTo(double bearing, double distance, DistanceUnit measurementSystem)
        {
            var pos = new Position(position);
            Distance gfDistance = new Distance(distance, measurementSystem);
            var newPos = pos.TranslateTo(bearing, gfDistance);
            return new LatitudeLongitudeDotSpatial(newPos.Latitude.DecimalDegrees, newPos.Longitude.DecimalDegrees);
        }
        public IDepictionLatitudeLongitude TranslatePositionToLatLong(double easting, double northing, DistanceUnit translateUnits)
        {
            return TranslateTo(Azimuth.East, easting, translateUnits).TranslateTo(Azimuth.North, northing, translateUnits);

        }

        public Point TranslatePositionToPoint(double easting, double northing, DistanceUnit translateUnits)
        {
            var latLong = TranslateTo(Azimuth.East, easting, translateUnits).TranslateTo(Azimuth.North, northing, translateUnits);
            return new Point(latLong.Longitude,latLong.Latitude);

        }

        public double DistanceTo(LatitudeLongitudeDotSpatial destinationLatLon,MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            return DistanceToAsGeoDistance(this, destinationLatLon, specificMeasurement, specificScale).Value;
        }

        public Distance DistanceToAsGeoDistance(LatitudeLongitudeDotSpatial originLatLon, LatitudeLongitudeDotSpatial destinationLatLon,
                                    MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var distance = originLatLon.ConvertToDotSpatialPosition().DistanceTo(destinationLatLon.ConvertToDotSpatialPosition());

            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    return distance.ToInches();
                case MeasurementConstants.ImperialNormal:
                    return distance.ToFeet();
                case MeasurementConstants.ImperialLarge:
                    return distance.ToStatuteMiles();
                case MeasurementConstants.MetricSmall:
                    return distance.ToCentimeters();
                case MeasurementConstants.MetricNormal:
                    return distance.ToMeters();
                case MeasurementConstants.MetricLarge:
                    return distance.ToKilometers();
            }
            return distance;
        }

        #endregion
    }
}