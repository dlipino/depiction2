using System.Collections.Generic;
using System.Linq;
using Depiction2.API.Service;
using Depiction2.Base.Utilities;
using Depiction2.LiveReports.Service14;
using NUnit.Framework;

namespace Depiction2.LiveReports.UnitTest.Service14
{
    [TestFixture]
    public class EMailParsingTests
    {
        private string finalPositionKey = EmailParser.FinalPositionKey;
        private string subjectPositionKey = EmailParser.SubjectPositionKey;
        private string displayNameKey = "displayname";
        private DepictionFolderService tempFolder;
        //These tests should be in the addin that creates them. For now mash everything into the main depiction app.
        #region Setup/Teardown
        [SetUp]
        protected void SetUp()
        {
            //            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            //            var prototype = new ElementPrototype("MockType", "Mock element");
            //            DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(prototype);
            //            tempFolder = new DepictionFolderService(true);
            //
            //            var testProduct = new TestProductInformation();
            //            DepictionAccess.ProductInformation = testProduct;
        }

        [TearDown]
        protected void TearDown()
        {
            //            DepictionAccess.ElementLibrary = null;
            //            tempFolder.Close();
            //
            //            DepictionAccess.ProductInformation = null;
        }

        #endregion

        //This test doesn't entirely belong here, but what ever
        [Test]
        [Ignore]
        public void IsCorrectElementPrototypeCreated()
        {
            //            var dmlsToLoad = new List<string> { "Person.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //
            //            Assert.AreEqual(2, DepictionAccess.ElementLibrary.UserPrototypes.Count);
            //
            //            var subject = "Person: me,20,40";
            //            var props = EmailParser.ParseEmailSubjectIntoProperties(subject);
            //            var keyName = "elementtype";
            //            Assert.IsTrue(props.ContainsKey(keyName));
            //
            //            Assert.AreEqual("Person", props[keyName].ToString());
            //            var prototype = ElementFactory.CreateRawPrototypeOfType(props[keyName].ToString());
            //            var element = ElementFactory.CreateElementFromPrototype(prototype);
            //            Assert.IsNotNull(element);
            //            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
        }
        [Test]
        [Ignore]
        public void LiveReportsWith122TypesGetCorrectPrototypeFromSubject()
        {
            //            var convertedType = "Depiction.Plugin.UserDrawnPolygon";
            //            var subjectText = "Depiction.Plugin.ShapeUserDrawn: ,35.71973, -78.58180";
            //
            //            var prototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subjectText, string.Empty);
            //            Assert.IsNotNull(prototype);
            //            Assert.AreEqual(convertedType, prototype.ElementType);
        }
        [Test]
        [Ignore]
        public void LiveReportsWith122TypesGetCorrectPrototypeFromEmailBody()
        {
            //            var convertedType = "Depiction.Plugin.UserDrawnPolygon";
            //            var bodyString = "ElementType : Depiction.Plugin.ShapeUserDrawn";
            //            var updatedPrototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty, bodyString);
            //            Assert.IsNotNull(updatedPrototype);
            //            Assert.AreEqual(convertedType, updatedPrototype.ElementType);
        }
        [Test]
        [Ignore]
        public void ZoneOfInfluenceInEmailBodyFrom122IsReadCorretly()
        {
            //            var dmlsToLoad = new List<string> { "LoadedShapes.dml" };//, "AutoDetect.dml" 
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false, true);
            //
            //            Assert.AreEqual(3, DepictionAccess.ElementLibrary.UserPrototypes.Count);//Why was it 3 before, ok i guess there is a mock element in here somewhere
            //            var typeName = "Depiction.Plugin.LoadedPolygon";
            //            var zoiString =
            //                "POLYGON((-78.5783839670058 35.7165146698669,-78.5795345351876 35.7234187393739,-78.5856119285 35.7208737096267,-78.5856119285 35.7181266205267,-78.5783839670058 35.7165146698669))";
            //            var bodyString = "ZoneOfInfluence: " + zoiString;
            //
            //            var updatedPrototype = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(string.Empty, bodyString);
            //            Assert.IsNotNull(updatedPrototype);
            //            Assert.AreEqual(typeName, updatedPrototype.ElementType);
            //
            //            var element = ElementFactory.CreateElementFromPrototype(updatedPrototype);
            //            Assert.IsNotNull(element);
            //            var modernZOI = new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString));
            //            Assert.AreEqual(modernZOI.ToString(), element.ZoneOfInfluence.ToString());
        }

        [Test]
        public void LatLongWithCommaSubject()
        {
            var expLat = 20;
            var expLong = 40;
            var expType = "hello";
            var subject = string.Format("{0},{1},{2}", expType, expLat, expLong);
            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);

            Assert.IsTrue(dictionary.ContainsKey(displayNameKey));
            Assert.AreEqual(expType, dictionary[displayNameKey]);

            Assert.IsTrue(dictionary.ContainsKey(subjectPositionKey));
            double lat, lon;
            string message;
            var pos = LatLongParserUtility.ParseForLatLong(dictionary[subjectPositionKey], out lat, out lon, out message);
            Assert.AreEqual(true, pos);
            Assert.AreEqual(expLat, lat);
            Assert.AreEqual(expLong, lon);

        }
        [Test]
        public void LatLongWithNoCommaSubject()
        {
            var expLat = 20;
            var expLong = 40;
            var expType = "hello";
            var subject = string.Format("{0},{1} {2}", expType, expLat, expLong);

            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);

            Assert.IsTrue(dictionary.ContainsKey(displayNameKey));
            Assert.AreEqual(expType, dictionary[displayNameKey]);

            Assert.IsTrue(dictionary.ContainsKey(subjectPositionKey));
            double lat, lon;
            string message;
            var pos = LatLongParserUtility.ParseForLatLong(dictionary[subjectPositionKey], out lat, out lon, out message);
            Assert.AreEqual(true, pos);
            Assert.AreEqual(expLat, lat);
            Assert.AreEqual(expLong, lon);

        }

        [Test]
        [Ignore("Start of a test to figure out why all emails are getting turned into elements.")]
        public void EnsureBadSubjectDoesNotCreateAnElement()
        {
            var badSubject =
                "Extra 20% Off Select Sales on Mobile! Saturday Selects From Park & Bond, kate spade new york Bedding Blowout, Weekend Wear, Wardrobe Essentials, Casual-Chic Dresses and more Starts Today at Noon ET";
            var unimportantBody = "something";
            var prototype = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(badSubject, unimportantBody);


        }
        [Test]
        public void SubjectMissingLocation()
        {
            var subject = "a label without location";
            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);
            Assert.IsTrue(dictionary.ContainsKey(displayNameKey));
            Assert.AreEqual(subject, dictionary[displayNameKey]);

            Assert.IsFalse(dictionary.ContainsKey(subjectPositionKey));
        }

        [Test]
        [Ignore("Subject parser no longer converts the subject position value to a lat/long")]
        public void SubjectWithBadLocation()
        {
            var subject = "label,...............";
            var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
            Assert.IsNotNull(dictionary);

            Assert.IsFalse(dictionary.ContainsKey(subjectPositionKey)); ;
        }
        [Test]
        [Ignore("The email parser is not in charge of guessing the correct type, that is the scaffoldlibraries problem")]
        public void ElementTypeGuessedFromSubject()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //
            //            Assert.AreEqual(2, DepictionAccess.ElementLibrary.UserPrototypes.Count);
            //
            var subject = "car";
            var dictionary = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subject, string.Empty);
            //
            //            var element = ElementFactory.CreateElementFromPrototype(dictionary);
            //            Assert.IsNotNull(element);
            //
            //            Assert.AreEqual("Depiction.Plugin.Car", element.ElementType);
        }

        [Test]
        public void TestZeroOrOnePairInEmailBody()
        {
            CheckEmailBodyHasNProperties(" p1 : ", 1);
            CheckEmailBodyHasNProperties(" 42 : v2 ", 0);
            CheckEmailBodyHasNProperties(" 42p : v2 ", 0);
            CheckEmailBodyHasNProperties(" : v1", 0);

            CheckEmailBodyHasNProperties("prop : value", 1);
            CheckEmailBodyHasNProperties(" a b : v2 ", 1);
            CheckEmailBodyHasNProperties("p1 : v1\n\np2 v2", 1);
        }
        private Dictionary<string, string> CheckEmailBodyHasNProperties(string bodyString, int expectedNumberOfPropertiesFromBody)
        {
            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];

            Assert.AreEqual(expectedNumberOfPropertiesFromBody, recipe.Count);
            return recipe;
        }
        //        private object GetRecipePropValue(Dictionary<string, object> recipe, string propName)
        //        {
        //            if (recipe.properties == null) return null;
        //            var prop = recipe.properties.FirstOrDefault(t => t.ProperName.Equals(propName));
        //            if (prop == null) return null;
        //            return prop.ValueString;
        //        }
        [Test]
        public void TestMultiplePairsInBody()
        {
            var prototype = CheckEmailBodyHasNProperties("p1 : v1\n\n \t p2 : v2 ", 2);
            Assert.IsTrue(prototype.ContainsKey("p1"));
            Assert.AreEqual("v1", prototype["p1"]);
            Assert.IsTrue(prototype.ContainsKey("p2"));
            Assert.AreEqual("v2", prototype["p2"]);
        }

        [Test]
        public void HandleDuplicateProperties()
        {
            var bodyString = "p1 : v1\n\n p1 : v2";
            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];

            Assert.AreEqual(1, recipe.Count);
            //Drops the first value, for now
            Assert.IsTrue(recipe.ContainsKey("p1"));
            Assert.AreEqual("v2", recipe["p1"]);
            //            var hasP1 = GetRecipePropValue(recipe, "p1");
            //            Assert.IsNotNull(hasP1);
            //            Assert.AreEqual("v2", hasP1);
        }
        [Test]
        public void HandleDuplicatePropertiesIsCaseInsensitive()
        {
            var bodyString = "p1 : v1\n\n P1 : v2";

            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];
            Assert.AreEqual(1, recipe.Count);
            //Drops the first value, for now
            Assert.IsTrue(recipe.ContainsKey("p1"));
            Assert.AreEqual("v2", recipe["p1"]);
            //            var hasP1 = GetRecipePropValue(recipe, "p1");
            //            Assert.IsNotNull(hasP1);
            //            Assert.AreEqual("v2", hasP1);
        }
        [Test]
        public void HandlePropertiesWithNoValue()
        {
            var bodyString = "p1 : \n\n P2 : v2"; // Adds "Email error" property, too. Not really but it might be useful
            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(string.Empty, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];
            Assert.AreEqual(2, recipe.Count);
            //Drops the first value, for now
            Assert.IsTrue(recipe.ContainsKey("p1"));
            Assert.AreEqual("", recipe["p1"]);

            //            var hasP1 = GetRecipePropValue(recipe, "p1");
            //            Assert.IsNotNull(hasP1);
            //            Assert.AreEqual("", hasP1);
        }

        [Test]
        [Ignore("Nothing gets ignored at this stage")]
        public void ParseBodyMayIgnoreSpecifiedProperties()
        {
            var bodyString = "NaMe1 : v1";
            //            var dict = EmailParser.ParseEmailBodyStringIntoProperties(bodyString, new List<string> { "name1" });
            //            Assert.AreEqual(0, dict.Keys.Count);
        }
        [Test]
        [Ignore("Nothing gets ignored at this stage")]
        public void ParseBodyOnlyIgnoresSpecifiedProperties()
        {
            var bodyString = "p1 : v1";
            //            var dict = EmailParser.ParseEmailBodyStringIntoProperties(bodyString, new List<string> { "not a match" });
            //            Assert.AreEqual(1, dict.Keys.Count);
        }

        [Test]
        public void LatLonInSubjectTakesPriorityOverBody()
        {
            var subjectString = "Person: me, 10, 11";
            var bodyString = "latitude: 20\nlongitude: 21";

            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];
            Assert.IsTrue(recipe.ContainsKey(finalPositionKey));
            var posString = recipe[finalPositionKey];
            Assert.IsNotNull(posString);
            double lat, lon;
            string message;
            var pos = LatLongParserUtility.ParseForLatLong(posString, out lat, out lon, out message);
            Assert.AreEqual(true, pos);
            Assert.AreEqual(10, lat);
            Assert.AreEqual(11, lon);
        }

        [Test]
        public void LatLonTakenFromBody()
        {
            var subjectString = "Person: me";
            var bodyString = "latitude: 20\nlongitude: 21";
            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];

            Assert.IsTrue(recipe.ContainsKey(finalPositionKey));
            var posString = recipe[finalPositionKey];
            double lat, lon;
            string message;
            var pos = LatLongParserUtility.ParseForLatLong(posString, out lat, out lon, out message);
            Assert.AreEqual(true, pos);
            Assert.AreEqual(20, lat);
            Assert.AreEqual(21, lon);
        }

        [Test]
        public void PositionTakenFromBody()
        {
            var subjectString = "Person: me";
            var bodyString = "Position: 20, 21";

            var recipes = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            Assert.AreEqual(1, recipes.Count);
            var recipe = recipes[0];
            Assert.IsTrue(recipe.ContainsKey(finalPositionKey));

            var posString = recipe[finalPositionKey];
            double lat, lon;
            string message;
            var pos = LatLongParserUtility.ParseForLatLong(posString, out lat, out lon, out message);
            Assert.AreEqual(true, pos);
            Assert.AreEqual(20, lat);
            Assert.AreEqual(21, lon);
        }
        [Test]
        public void HandlesRepliesOrForwardedEmailSubjectLines()
        {
            string[] subjects ={
                    "RE: FW: Person: me, 10 11",
                    "FW: RE: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "Re: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "RE: Person: me, 10 11",
                    "FW: Person: me, 10 11",
                    "Fwd: Person:me,10 11",
                    "Fwd: Re: Fwd: Fw: Person:me,10 11"};

            foreach (var subject in subjects)
            {
                var dictionary = EmailParser.ParseEmailSubjectIntoProperties(subject);
                Assert.IsNotNull(dictionary);

                Assert.IsTrue(dictionary.ContainsKey(displayNameKey));
                Assert.AreEqual("me", dictionary[displayNameKey]);

                Assert.IsTrue(dictionary.ContainsKey(subjectPositionKey));
                double lat, lon;
                string message;
                var pos = LatLongParserUtility.ParseForLatLong(dictionary[subjectPositionKey], out lat, out lon, out message);
                Assert.AreEqual(true, pos);
                Assert.AreEqual(10, lat);
                Assert.AreEqual(11, lon);
                //                object position;
                //                var hasPosition = dictionary.TryGetValue(positionKey, out position);
                //                Assert.IsTrue(hasPosition);
                //                Assert.AreEqual(new LatitudeLongitude(10, 11), position);
                //
                //                object displayName;
                //                var hasDisplayName = dictionary.TryGetValue(displayNameKey, out displayName);
                //                Assert.IsTrue(hasDisplayName);
                //                Assert.AreEqual("me", displayName);
            }
        }
        [Test]
        [Ignore("Type conversion is not the email readers problem")]
        public void YesNoWorksForBoolean()
        {
            var subject = "label,__notAnAddress__";
            var body = "Latitude: 10\nLongitude: 20\nActive: yes";
            //            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            //            UnitTestHelperMethods.PropertyTester("Active", true, prototype);
            //
            //
            //            subject = "label,__notAnAddress__";
            //            body = "Latitude: 10\nLongitude: 20\nActive: no";
            //            prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            //            UnitTestHelperMethods.PropertyTester("Active", false, prototype);
        }

        [Test]
        [Ignore("Type conversion is not the email readers problem")]
        public void TrueFalseWorksForBoolean()
        {
            var subject = "label,__notAnAddress__";
            var body = "Latitude: 10\nLongitude: 20\nActive: true";
            //            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            //            UnitTestHelperMethods.PropertyTester("Active", true, prototype);
            //
            //
            //            subject = "label,__notAnAddress__";
            //            body = "Latitude: 10\nLongitude: 20\nActive: false";
            //            prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            //            UnitTestHelperMethods.PropertyTester("Active", false, prototype);
        }


    }
}