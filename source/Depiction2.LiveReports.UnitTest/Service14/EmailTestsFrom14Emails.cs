﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Core.Service;
using Depiction2.LiveReports.Service14;
using NUnit.Framework;

namespace Depiction2.LiveReports.UnitTest.Service14
{
    [TestFixture]
    public class EmailTestsFrom14Emails
    {
        [Test]
        public void GeoCodeElement14Test()
        {
            var subjectString = "Car: My car, 22518 92nd Ave W, Edmonds WA 98020";
            var bodyString = "";
            var res = EmailParser.ParseEmailSubjectIntoProperties(subjectString);
            Assert.AreEqual(3, res.Count);
        }
        [Test]
        public void SubjectTagParseTest()
        {
            var tag = "from_debug, work, tag";
            var sub = "Depiction.Plugin.UserDrawnLine: ,47.609249385057296,-122.3434539787234";
            var subjectString = string.Format(" {0} //DT{{{1}}}//", sub, tag);

            var res = EmailParser.ParseTagsFromSubject(subjectString);
            Assert.AreEqual(2, res.Length);
            Assert.AreEqual(res[0], sub);
            Assert.AreEqual(res[1], tag);

            subjectString = string.Format(" {0}", sub);
            res = EmailParser.ParseTagsFromSubject(subjectString);
            Assert.AreEqual(1, res.Length);
            Assert.AreEqual(res[0], sub);

        }
        [Test]
        public void Parse14ValueAndTypeTest()
        {
            var typeString = "Distance";
            var valString = "0.953200173480936 km";

            var totalString = string.Format("[{0}] {1}", typeString, valString);
            var parsed = EmailParser.ParseTypeAndStringValue(totalString);
            Assert.AreEqual(2, parsed.Length);
            Assert.AreEqual(valString, parsed[0]);
            Assert.AreEqual(typeString, parsed[1]);
        }

        [Test]
        public void BuildingElement14Test()
        {
            var geomString = "POINT (-122.33942198382849 47.597313842868111)";
            #region the message
            var subjectString =
                "Depiction.Plugin.Building: ,47.597313842868111,-122.33942198382849 //DT{from_debug, work, tag}//";
            //15 user props when no matching element is found
            var bodyString = string.Format(
                @"DisplayName: Building
Author: Random, Inc.
Description: Use to represent a basic building
PrimaryUse:
Street:
City:
State:
ZipCode:
PointOfContact:
Phone:
Email:
NumberOfFloors: [Integer] 0
Height: [Distance] 0.34 meters
Value: [Integer] 0
FloorSpace: [Area] 0 square meters
MaxOccupants: [Integer] 0
Notes:
EID: 20131015151341.224.4
IconBorderColor: [Color] #FFFF0000
IconBorderShape: [DepictionIconBorderShape] None
IconPath: [DepictionIconPath] embeddedresource:Depiction.Building
IconSize: [Number] 56
Draggable: [Bool] False
ZoneOfInfluence: {0}", geomString);
            #endregion
            var allStringProps = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            var updateValues = new List<Dictionary<string, object>>();
            foreach (var props in allStringProps)
            {
                updateValues.Add(EmailParser.ConvertValuesToCorrectType(props, null, false));
            }
            Assert.AreEqual(1, updateValues.Count);
            var newValues = updateValues[0];

            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(newValues);
            Assert.IsFalse(element.IsDraggable);
            Assert.AreEqual("None", element.IconBorderShape);
            Assert.AreEqual("#FFFF0000", element.IconBorderColor);
            Assert.AreEqual("Depiction.Building", element.IconResourceName);
            Assert.AreEqual(56, element.IconSize);
            Assert.AreEqual(new Point(-122.33942198382849, 47.597313842868111), element.GeoLocation);
            var propCount = 15;
            if (!DepictionGeometryService.Activated)
            {
                propCount++;
                var lgeomString = element.GetDepictionProperty("WtkGeometry");
                Assert.IsNotNull(lgeomString);
                Assert.AreEqual(geomString, lgeomString.Value);
            }
            Assert.AreEqual(propCount, element.AdditionalPropertyNames.Count);
            Assert.AreEqual(3, element.Tags.Count);
        }

        [Test]
        public void CircleShape14Test()
        {
            var geomString = "POLYGON ((-122.326776021321 47.608683750264198,-122.326117928521 47.608609479064199,-122.325531150421 47.608394714764202,-122.32507927332099 47.608062731664198," +
                             "-122.324811265121 47.607649507264199,-122.324756168821 47.607199821964201,-122.324919954921 47.606762406764197,-122.325284874521 47.606384661164199," +
                             "-122.325811383021 47.606107518464199,-122.326442425121 47.605961009264199,-122.327109617421 47.605961009264199,-122.327740659521 47.606107518464199," +
                             "-122.328267168021 47.606384661164199,-122.328632087621 47.606762406764197,-122.32879587372101 47.607199821964201,-122.328740777421 47.607649507264199," +
                             "-122.32847276922099 47.608062731664198,-122.32802089222101 47.608394714764202,-122.327434114021 47.608609479064199,-122.326776021321 47.608683750264198))";
            #region email text
            var subjectString = "Depiction.Plugin.CircleShape: ,47.607313032787381,-122.32677602127659 //DT{from_debug}//";
            var bodyString =
                    string.Format(
                        @"DisplayName: Circle
Author: Depiction, Inc.
Description: Generic circle element
Radius: [Distance] 152.399999768352 meters
ZOILineThickness: [Number] 2
CanEditZoi: [Bool] False
Perimeter: [Distance] 0.953200173480936 km
Area: [Area] 0.0716052031641408 square km
IconBorderColor: [Color] #FFFF0000
IconBorderShape: [DepictionIconBorderShape] None
IconPath: [DepictionIconPath] embeddedresource:Depiction.Circle
IconSize: [Number] 24
ZOIFill: [Color] #FF000000
ZOIBorder: [Color] #FFCC3300
Draggable: [Bool] True
ShowIcon: [Bool] True
EID: 20131015150502.035.1
ZoneOfInfluence: {0}", geomString);
            #endregion
            var allStringProps = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            var updateValues = new List<Dictionary<string, object>>();
            foreach (var props in allStringProps)
            {
                updateValues.Add(EmailParser.ConvertValuesToCorrectType(props, null, false));
            }
            Assert.AreEqual(1, updateValues.Count);
            var newValues = updateValues[0];
            Assert.AreEqual(1, updateValues.Count);
            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(newValues);
            if (!DepictionGeometryService.Activated)
            {
                var lgeomString = element.GetDepictionProperty("WtkGeometry");
                Assert.IsNotNull(lgeomString);
                Assert.AreEqual(geomString, lgeomString.Value);
            }
        }



        [Test]
        public void UserDrawnLineShape14Test()
        {
            //Forwarded emails appear to break up the gome string
            var geomStringWithLineBreak = string.Format("LINESTRING (-122.34345397872301{0}47.609249385057304,-122.333237638298 47.608486612136801,{0}-122.333237638298 47.605024656755198," +
                             "-122.330268787234 47.603381614699899,-122.316996276596 47.604789939621398,-122.315948446809 47.603146890193202)",Environment.NewLine);
            var geomStringNoLineBreak = geomStringWithLineBreak.Replace(Environment.NewLine, " ");
            #region email info
            var subjectString =
                "Depiction.Plugin.UserDrawnLine: ,47.609249385057296,-122.3434539787234 //DT{from_debug, work, tag}//";
            var bodyString =
                string.Format(
                    @"DisplayName: Line - user drawn{0}
Author: Depiction, Inc.{0}
Description: A simple line. Click to add vertices.{0}
ZOILineThickness: [Number] 2{0}
CanEditZoi: [Bool] True{0}
Length: [Distance] 2.65529077910998 km{0}
IconBorderColor: [Color] #FFFF0000{0}
IconBorderShape: [DepictionIconBorderShape] None{0}
IconPath: [DepictionIconPath] embeddedresource:Depiction.Lines{0}
IconSize: [Number] 24{0}
ZOIBorder: [Color] #FFCC3300{0}
Draggable: [Bool] True{0}
ShowIcon: [Bool] True{0}
EID: 20131015150622.840.3{0}
ZoneOfInfluence: {1}", Environment.NewLine,geomStringWithLineBreak);
            #endregion
            var allStringProps = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            var updateValues = new List<Dictionary<string, object>>();
            foreach (var props in allStringProps)
            {
                updateValues.Add(EmailParser.ConvertValuesToCorrectType(props, null, false));
            }
            Assert.AreEqual(1, updateValues.Count);
            var newValues = updateValues[0];
            Assert.AreEqual(1, updateValues.Count);
            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(newValues);
            if (!DepictionGeometryService.Activated)
            {
                var lgeomString = element.GetDepictionProperty("WtkGeometry");
                Assert.IsNotNull(lgeomString);
                Assert.AreEqual(geomStringNoLineBreak, lgeomString.Value);
            }
        }

        [Test]
        public void UserPolygonShape14Test()
        {
            var subject =
                "Depiction.Plugin.UserDrawnPolygon: ,47.599789422366015,-122.32571533717501 //DT{from_debug}//";
            var main =
                @"DisplayName: Shape - user drawn
Author: Depiction, Inc.
Description: Freeform polygon. Click to add vertices.
ZOILineThickness: [Number] 2
Notes:
CanEditZoi: [Bool] True
Perimeter: [Distance] 3.02590283031147 km
Area: [Area] 0.477563507269784 square km
IconBorderColor: [Color] #FFFF0000
IconBorderShape: [DepictionIconBorderShape] None
IconPath: [DepictionIconPath] embeddedresource:Depiction.Shapes
IconSize: [Number] 24
ZOIFill: [Color] #FF000000
ZOIBorder: [Color] #FFCC3300
Draggable: [Bool] True
ShowIcon: [Bool] True
EID: 20131015150534.241.2
ZoneOfInfluence: POLYGON ((-122.334372787234 47.598334462852598,-122.330006829787 47.602618547919903,-122.31996512766 47.601855654334997,-122.31979048936201 47.597806263690003,-122.322672021277 47.596221634208199,-122.329570234043 47.598803968742999,-122.334372787234 47.598334462852598))";
        }

        [Test]
        public void Route14EmailTest()
        {
            #region email info 1.4
            var subjectString = @"Depiction.Plugin.RouteUserDrawn: ,29.954275441568278,-90.09251542964661 //DT{from_debug}//";
            var bodyString =
                @"DisplayName: Route - user drawn
Author: Depiction, Inc.
Description: Drag the start and end points to create a land, air or sea route. With your cursor over the route, hold the Shift key while clicking (Shift+click) to add waypoints.
Notes:
IconDraggable: [Bool] True
Length: [Distance] 11.6266418781934 km
ZOILineThickness: [Number] 2
Directions: blank
EID: 20131010011943.689.1
IconBorderColor: [Color] #FFFF0000
IconBorderShape: [DepictionIconBorderShape] None
IconPath: [DepictionIconPath] embeddedresource:Depiction.RouteUserDrawn
IconSize: [Number] 24
ZOIBorder: [Color] #FF0000FF
ZoneOfInfluence: LINESTRING (-90.09251542964661 29.954275441568267,-90.078346652849632 29.956667404143307,-90.072487384249399 29.945719104380867,-90.079625038726064 29.9354137211809,-90.07025020896566 29.933113266691574,-90.056827157263271 29.959611279075997,-90.048943777692017 29.947007202212749,-90.05746635020148 29.932377110022376)
ElementWaypoint:
Name:start
IconPath:embeddedresource:Depiction.RouteStart
Location:29.954275441568267,-90.09251542964661

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.956667404143307,-90.078346652849632

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.945719104380867,-90.0724873842494

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.9354137211809,-90.079625038726064

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.933113266691574,-90.07025020896566

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.959611279075997,-90.056827157263271

ElementWaypoint:
Name:WayPoint
IconPath:embeddedresource:Depiction.RouteWayPoint
Location:29.947007202212749,-90.048943777692017

ElementWaypoint:
Name:end
IconPath:embeddedresource:Depiction.RouteEnd
Location:29.932377110022376,-90.05746635020148";
            #endregion

            var allStringProps = EmailParser.GetElementPropertiesFromEmailSubjectAndBody(subjectString, bodyString);
            Assert.AreEqual(9,allStringProps.Count);
        }

    }
}