﻿using System.Linq;
using Depiction2.Core.StoryEntities;
using Depiction2.LiveReports.Service14;
using NUnit.Framework;

namespace Depiction2.LiveReports.UnitTest.Service14
{
    [TestFixture]
    [Ignore("Tests a lot of things and should used to verify full tests")]
    public class FullEmailReadTests
    {
        private string emailSource = "depictionunittest@gmail.com";
        private LiveReportSourceInfo sourceInfo;
        private DepictionEmailReaderBackgroundService readService;

        [SetUp]
        public void Setup()
        {
            sourceInfo = new LiveReportSourceInfo();
            sourceInfo.Pop3Server = "pop.gmail.com";
            sourceInfo.UseSSL = true;
            sourceInfo.SimpleUserName = "depictionunittest";
            sourceInfo.Password = "Depiction2013";
            sourceInfo.PortNumber = -1;
            readService = new DepictionEmailReaderBackgroundService(sourceInfo);
        }

        [TearDown]
        public void TearDown()
        {
            sourceInfo = null;
        }

        [Test]
        public void ConnectionTest()
        {
            Assert.IsTrue(readService.ConnectionTester());
        }

        [Test]
        public void SimpleReadTest()
        {
            var messages = readService.GetNonreadMessagesFromEmailLocation();
            Assert.AreEqual(1, messages.Count);
            var readPropList = readService.CreatePropertyListsFromEmailLocation("");
            Assert.AreEqual(1, readPropList.Count);
            readPropList = readService.CreatePropertyListsFromEmailLocation("none");
            Assert.AreEqual(0, readPropList.Count);
        }
        [Test]
        public void EmailReadAndStoryUpdateTest()
        {
            var readPropList = readService.CreatePropertyListsFromEmailLocation("");
            var count = readPropList.Count;

            var story = new Story(null);
            story.UpdateRepo(readPropList,null,null);
            Assert.AreEqual(count,story.AllElements.Count);
            var geo = story.AllElements.Where(t => t.GeoLocation != null);
            Assert.AreEqual(1,geo.Count());

        }
    }
}