namespace Depiction2.Base.Measurement
{
    public class MeasurementConstants
    {
        public const int MetricSmall = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Small);
        public const int MetricNormal = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Normal);
        public const int MetricLarge = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Large);
        public const int ImperialSmall = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Small);
        public const int ImperialNormal = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Normal);
        public const int ImperialLarge = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Large);
    }
    /// <summary>
    /// A set of buckets representing the scale and measurement system (imperial or metric) that 
    /// Depiction can use for the values of an element's property.
    /// </summary>
    ///  Depiction2.Base.Measurement.MeasurementSystem
    public enum MeasurementSystem
    {
        /// <summary>
        /// Medium-sized values in the Imperial measurement system.
        /// </summary>
        Metric = 0,
        Imperial = 3

    }
    //    Depiction2.Base.Measurement.MeasurementScale
    public enum MeasurementScale
    {
        Small = 0,
        Normal = 1,
        Large = 2
    }
}