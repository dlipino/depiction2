﻿namespace Depiction2.Base.Product
{
    //Not a proper abastract class, set up for debug mode be default
    public abstract class ProductInformationBase
    {
        //Semi hack for dealing with product type, for now. Hopefully and internal library will resolve this
        public const string Release14 = "Depiction14";
        public const string Debug = "DepictionDebug";
        public const string Reader = "DepictionReader";
        public const string Prep = "DepictionPrep";
        public const string Test = "DepictionUnitTest";
        public const string DepictionRW = "DepictionRW";
        public const string DefaultStoryExtension = ".dpn";

        public virtual string ProductType { get { return Debug; } }

        public string DepictionCopyright { get { return "© 2008-2014 Depiction, Inc. All rights reserved. \"Depiction\" and \"More than Mapping\" are either registered trademarks or trademarks of Depiction, Inc. All rights reserved."; } }
        public string ProductVersion { get { return string.Format("{0}™ {1}", ProductName, FullAppVersion); } }


        virtual public string ProductAppDirectoryName { get { return "Depiction2_Debug"; } }

        #region Properties that vary only on _productInformation "brand"

        public virtual string ResourceAssemblyName { get { return string.Empty; } }
        public virtual string SplashScreenPath { get { return string.Empty; } }
        public virtual string ThemeLocation { get { return string.Empty; } }

        public virtual string FileExtension { get { return DefaultStoryExtension; } }
        public virtual string ProductName { get { return "Depiction Debug"; } }
        public virtual string FullAppVersion { get { return "2.0"; } }
        public virtual string ProductNameInternalUsage { get { return "Depiction"; } }
        public virtual string CompanyName { get { return "Depiction, Inc."; } }
        public virtual string SupportEmail { get { return "support@depiction.com"; } }

        public virtual string StoryName { get { return "depiction"; } }
        //        public virtual string LicenseFileDirectory { get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction_Inc"); } }

        public virtual string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.", ProductVersion, DepictionCopyright);
            }
        }

        public virtual string AboutFileAssemblyLocation
        {
            get { return "Docs.About.xps"; }
        }
        public virtual string EulaFileAssemblyLocation
        {
            get { return "Docs.DepictionEula.xps"; }
        }
        #endregion
    }
}