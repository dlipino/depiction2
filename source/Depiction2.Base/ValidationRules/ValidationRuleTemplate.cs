﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Depiction2.Base.Service;

namespace Depiction2.Base.ValidationRules
{
    public class ValidationRuleTemplate
    {
        public string ValidationRuleName { get; set; }

        public KeyValuePair<string, string>[] Parameters { get; set; }

        static public IValidationRule ConvertToValidationRule(ValidationRuleTemplate template, 
            ISimpleNameTypeService stringToTypeConverter)
        {
            var ruleType = stringToTypeConverter.GetTypeFromName(template.ValidationRuleName);
            var finalParameters = new List<object>();
            foreach(var paramDescriptor in template.Parameters)
            {
                var typeString = paramDescriptor.Value;
                var typeValue = paramDescriptor.Key;
                if (typeString.Contains("RuntimeType") && typeValue != null)
                {
                    //Used for type validator
                    var result = stringToTypeConverter.GetTypeFromName(typeValue);
                    finalParameters.Add(result);
                }
                else
                {
                    var type = stringToTypeConverter.GetTypeFromName(typeString);
                    if (type != null)
                    {
                       TypeConverter converter = TypeDescriptor.GetConverter(type);
                        var convertedValue = converter.ConvertFrom(typeValue);
//                        var convVal = DepictionTypeConverter.ChangeType(typeValue, type);
                        finalParameters.Add(convertedValue);
                    }
                }
            }

            var rule = (IValidationRule)Activator.CreateInstance(ruleType, finalParameters.ToArray());
            return rule.IsRuleValid ? rule : null;
        }
    }
}