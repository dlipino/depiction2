﻿namespace Depiction2.Base
{
    public interface IDeepCloneable<T> : IDeepCloneable
    {
        new T DeepClone();
    }
    public interface IDeepCloneable
    {
        object DeepClone();
    }
}
