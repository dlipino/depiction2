﻿using System;
using System.Runtime.Serialization;
using System.Windows;

namespace Depiction2.Base.Geo
{
    //The implementation of this is so terrible i want to cry, especially since the unit test don't really exists.
    //so many strange choices
    [DataContract]
    public class CartRect : ICartRect
    {
        #region variables

        private Rect _wpfRectBase;

        [DataMember]
        private Point TopLeft { get; set; }
        [DataMember]
        private Point BottomRight { get; set; }
        #endregion

        #region properties that are not serialized

        public Rect WpfRect
        {
            get
            {
                return new Rect(new Point(Left, -Top), _wpfRectBase.Size);
            }
        }
        public bool IsEmpty
        {
            get { return _wpfRectBase.IsEmpty; }
        }

        public bool IsValid
        {
            get
            {
                if (_wpfRectBase.Top.Equals(double.NaN) || _wpfRectBase.Bottom.Equals(double.NaN) || _wpfRectBase.Right.Equals(double.NaN) || _wpfRectBase.Left.Equals(double.NaN))
                {
                    return false;
                }
                return true;
            }
        }

        public double Top
        {
            get { return Math.Round(-_wpfRectBase.Top, 6); }
        }

        public double Bottom
        {
            get
            {
                return Math.Round(-_wpfRectBase.Bottom, 6);
            }
        }

        public double Left
        {
            get
            {
                return Math.Round(_wpfRectBase.Left, 6);
            }
        }

        public double Right
        {
            get
            {
                return Math.Round(_wpfRectBase.Right, 6);
            }
        }
        public double Height
        {
            get { return Math.Round(_wpfRectBase.Height, 6); }
        }

        public double Width
        {
            get { return Math.Round(_wpfRectBase.Width, 6); }
        }
        public Point LeftTop { get { return new Point(Left, Top); } }
        public Point RightTop { get { return new Point(Right, Top); } }
        public Point LeftBottom { get { return new Point(Left, Bottom); } }
        public Point RightBottom { get { return new Point(Right, Bottom); } }

        public Size Size
        {
            get { return new Size(Width, Height); }
        }

        public Point Center
        {
            get
            {
                return new Point(Math.Round(_wpfRectBase.Width / 2 + _wpfRectBase.Left, 6),
                                Math.Round(-(_wpfRectBase.Height / 2 + _wpfRectBase.Top), 6));
            }
        }

        public double Area
        {
            get
            {
                return Math.Round(_wpfRectBase.Width * _wpfRectBase.Height, 6);
            }
        }

        #endregion

        #region Constructor

        public CartRect()
        {
            _wpfRectBase = Rect.Empty;
        }
        public CartRect(ICartRect copy)
        {
            if (copy == null)
            {
                _wpfRectBase = Rect.Empty;
                return;
            }
            _wpfRectBase = new Rect(new Point(copy.Left, -copy.Top), copy.Size);
        }

        public CartRect(Point botLeftCart, Size rectSize)
            : this(botLeftCart, new Vector(rectSize.Width, rectSize.Height))
        {

        }
        public CartRect(Point botLeftCart, Vector directionCart)
        {
            _wpfRectBase = new Rect(new Point(botLeftCart.X, -botLeftCart.Y), new Vector(directionCart.X, -directionCart.Y));
        }

        public CartRect(Rect wpfRect, bool keepSign)
        {
            if (!keepSign)
            {
                _wpfRectBase = new Rect(new Point(wpfRect.X, wpfRect.Y), wpfRect.Size);
            }
            else
            {
                // new Point(1, 1), new Size(3, 3)
                _wpfRectBase = new Rect(new Point(wpfRect.Left, -wpfRect.Bottom), wpfRect.Size);
            }
        }
        public CartRect(Point topLeft, Point bottomRight)
        {
            _wpfRectBase = new Rect(new Point(topLeft.X, -topLeft.Y), new Point(bottomRight.X, -bottomRight.Y));
        }

        #endregion

        #region action things
        public void Union(ICartRect other)
        {
            var otherCart = new CartRect(new Point(other.Left, other.Bottom), new Size(other.Width, other.Height));
            _wpfRectBase.Union(otherCart._wpfRectBase);
        }

        public bool IntersectWith(ICartRect other)
        {
            if (other == null) return false;
            var otherCart = new CartRect(new Point(other.Left, other.Bottom), new Size(other.Width, other.Height));
            return _wpfRectBase.IntersectsWith(otherCart._wpfRectBase);
        }

        public bool ContainsPoint(Point point)
        {
            var negY = new Point(point.X, -point.Y);
            return _wpfRectBase.Contains(negY);
        }
        #endregion

        #region serialization help
        [OnSerializing]
        internal void OnSerializingMethod(StreamingContext context)
        {
            TopLeft = _wpfRectBase.TopLeft;
            BottomRight = _wpfRectBase.BottomRight;
        }
        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            _wpfRectBase = new Rect(TopLeft, BottomRight);
        }
        #endregion
        #region

        public override bool Equals(object obj)
        {
            var otherRect = obj as ICartRect;
            if (otherRect == null) return false;
            if (!Equals(Top, otherRect.Top)) return false;
            if (!Equals(Bottom, otherRect.Bottom)) return false;
            if (!Equals(Left, otherRect.Left)) return false;
            if (!Equals(Right, otherRect.Right)) return false;
            return true;
        }

        public override string ToString()
        {
            return string.Format("({0},{1}) ({2},{3})", Left, Top, Right, Bottom);
        }

        #endregion

    }
}