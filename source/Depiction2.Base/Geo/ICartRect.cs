﻿using System.Windows;

namespace Depiction2.Base.Geo
{
    public interface ICartRect
    {
        Rect WpfRect { get; }
        bool IsEmpty { get; }
        bool IsValid { get; }

        double Top { get; }
        double Bottom { get; }
        double Right { get; }
        double Left { get; }

        Point LeftTop { get; }
        Point RightTop { get; }
        Point LeftBottom { get;}
        Point RightBottom { get; }

        double Height { get; }
        double Width { get; }
        Size Size { get; }

        double Area { get; }
        Point Center { get; }
        void Union(ICartRect other);
        bool IntersectWith(ICartRect other);
        bool ContainsPoint(Point point);
    }
}