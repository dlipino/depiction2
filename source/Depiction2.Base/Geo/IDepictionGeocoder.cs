﻿using System.Windows;
using Depiction2.Base.Service;

namespace Depiction2.Base.Geo
{
    public interface IDepictionGeocoder
    {
        Point? GeocodeInput(GeocodeInput input);
    }
}