﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Depiction2.Base.Geo
{
    public interface IDepictionGeometry : IDisposable
    {
        DepictionGeometryType GeometryType { get; }
        string WktGeometry { get; }
        bool IsValid { get; }
        bool IsSimple { get; }
        Rect Bounds { get; }
        ICartRect RectBounds { get; }

        object GeometryObject { get; }
        IEnumerable<Point> GeometryPoints { get; }
        void SetGeometry(List<Point> geometryPoints);
        bool Intersects(IDepictionGeometry otherGeometry);
        bool Within(IDepictionGeometry otherGeometry);
        bool Contains(IDepictionGeometry otherGeometry);
        void ShiftGeometry(Point shiftDistance);//Vector maybe?

        IDepictionGeometry Clone();
    }
    /// <summary>
    /// The type of geometry that a Zone of Influence can be. Shoult match
    /// types for wtk elements
    /// </summary>
    public enum DepictionGeometryType
    {
        Unknown,
        /// <summary>
        /// A point.
        /// </summary>
        Point,

        /// <summary>
        /// A single line.
        /// </summary>
        LineString,

        /// <summary>
        /// A polygon.
        /// </summary>
        Polygon,

        /// <summary>
        /// An ordered list of points.
        /// </summary>
        MultiPoint,

        /// <summary>
        /// An ordered list of lines.
        /// </summary>
        MultiLineString,

        /// <summary>
        /// An ordered list of polygons.
        /// </summary>
        MultiPolygon,

        //        /// <summary>
        //        /// A regular grid of data covering a given geographical area.
        //        /// </summary>
        //        Coverage,Image,
        //        GeometryNotSet
    } ;
}