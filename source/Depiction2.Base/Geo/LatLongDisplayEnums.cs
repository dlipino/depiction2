namespace Depiction2.Base.Geo
{
    public enum LatitudeLongitudeFormat
    {

        Decimal,
        ColonFractionalSeconds,
        ColonIntegerSeconds,
        DegreesFractionalMinutes,
        DegreesMinutesSeconds,
        DegreesWithDCharMinutesSeconds,
        SignedDecimal,
        SignedDegreesFractionalMinutes,
        UTM
    }

    public enum LatitudeLongitudeSeparator
    {
        CommaAndSpace,
        Space,
        Comma,
    }
}