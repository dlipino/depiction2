﻿namespace Depiction2.Base.Geo
{
    public interface IDepictionLatitudeLongitude
    {
        double Latitude { get; }
        double Longitude { get; }
    }
}