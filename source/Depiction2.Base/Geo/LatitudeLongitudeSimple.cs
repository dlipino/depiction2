﻿namespace Depiction2.Base.Geo
{
    public class LatitudeLongitudeSimple : IDepictionLatitudeLongitude
    {
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }

        public LatitudeLongitudeSimple(double lat, double lon)
        {
            Latitude = lat;
            Longitude = lon;
        }
    }
}