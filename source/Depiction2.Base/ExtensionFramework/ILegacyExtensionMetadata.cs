namespace Depiction2.Base.ExtensionFramework
{
    public interface ILegacyExtensionMetadata
    {
        string ExtensionPackage { get; }

        string Name { get; }
        string Author { get; }
        string Company { get; }
        string DisplayName { get; }
        string Description { get; }
        bool IsDefault { get; }

//        AddonUsage[] AddonUsages { get; }
    }
}