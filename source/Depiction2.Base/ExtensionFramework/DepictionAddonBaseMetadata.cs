using System;
using System.ComponentModel.Composition;

namespace Depiction2.Base.ExtensionFramework
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionAddonBaseMetadata : ExportAttribute, ILegacyExtensionMetadata
    {
        #region Variables

        protected string _extensionPackage = string.Empty;
        protected string name = "Addon";
        protected string displayName = "Add-on";
        protected string author = "Nobody";
        protected string description = "";
        protected string company = string.Empty;

        #endregion

        #region Constructor

        protected DepictionAddonBaseMetadata(Type metadataForType) : base(metadataForType) { }

        #endregion

        #region Implementation of IBaseMetadata

        virtual public string ExtensionPackage
        {
            get { return _extensionPackage; }
            set { _extensionPackage = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public string Company
        {
            get { return company; }
            set { company = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        

        public bool IsDefault { get; set; }


        #endregion
    }
}