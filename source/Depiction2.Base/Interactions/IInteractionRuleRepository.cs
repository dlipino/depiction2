﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Depiction2.Base.Interactions
{

    public interface IInteractionRuleRepository
    {
        //Will hopefully connect these up in the future
//        event NotifyCollectionChangedEventHandler RuleRepositoryChange;
        //        bool AddInteractionRule(IInteractionRule rule);
        //        int AddInteractionsFromDirectory(string dirName);
        //        bool AddInteractionRule(IInteractionRule rule, bool runInteractions);
        //        void RemoveInteractionRule(IInteractionRule rule);
        int Count { get; }        
        ReadOnlyCollection<IInteractionRule> AllInteractionRules { get; }

        IEnumerable<IInteractionRule> DefaultInteractionRules { get; }
        IEnumerable<IInteractionRule> ActiveStoryInteractionRules { get; } 

        bool DoesRuleWithElementTypeAndTriggerNameExist(string elementType, string triggerProperty);
        


        void LoadDefaultRulesIntoRepository(IEnumerable<IInteractionRule> interactionRulesToAdd);
        void LoadNonDefaultRulesIntoRepository(IEnumerable<IInteractionRule> interactionRulesToAdd);
        IInteractionRule[] GetInteractionRules(string elementType);


        void CopyInteractionRules(string type, string elementType);

        void Clear();
    }
}
