﻿namespace Depiction2.Base.Interactions
{
    /// <summary>
    /// Interface for ElementFileParameter.
    /// </summary>
    public interface IElementFileParameter
    {
      
        string ElementQuery { get; set; }

        ElementFileParameterType Type { get; set; }

        bool Equals(object obj);

     
        int GetHashCode();
    }

    public enum ElementFileParameterType
    {
        Subscriber,
        Publisher,
        Value,
        NotThere
    }
}
