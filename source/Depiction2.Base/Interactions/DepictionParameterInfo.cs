﻿using System;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Base.Interactions
{
    public class DepictionParameterInfo
    {
        public string ParameterKey { get; private set; }
        public string ParameterName { get; set; }
        public string ParameterDescription { get; set; }
        public Type ParameterType { get; private set; }
        public string[] PossibleValues { get; set; }
        public IValidationRule[] ValidationRules { get; set; }

        public DepictionParameterInfo(string parameterKey, Type parameterType)
        {
            ParameterKey = parameterKey;
            ParameterType = parameterType;
            ParameterDescription = "";
            ParameterName = "";
        }
    }
}
