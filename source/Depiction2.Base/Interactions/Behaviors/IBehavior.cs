﻿using System.Collections.Generic;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Base.Interactions.Behaviors
{
    public interface IBehavior
    {
        /// <summary>
        /// Defines the name and type for each of the paramters required by this behavior.
        /// These are the parameters passed into the InternalDoBehavior method.
        /// The behavior will only be invoked if the number and type of parameters match what InternalDoBehavior expects.
        /// </summary>
        DepictionParameterInfo[] DepictionParameters { get; }
//        BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag);
        BehaviorResult DoBehavior(IElement subscriber, object[] inputParameters);
    }
}