﻿using System;

namespace Depiction2.Base.Interactions
{
    public interface IInteractionRouter : IDisposable
    {
        IInteractionRuleRepository AvailableInteractions { get; }
        int RulePairs { get; }
        bool BeginAsync();
        void EndAsync();
    }
}