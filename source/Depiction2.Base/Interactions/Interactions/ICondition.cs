using System.Collections.Generic;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Base.Interactions.Interactions
{
    public interface ICondition
    {
        string Name { get; }
        HashSet<string> ConditionTriggerProperties { get; }
        /// <summary>
        /// Executes this instance.
        /// </summary>
        bool Evaluate(IElement triggerElement, IElement affectedElement, object[] inputParameters);
    }
}