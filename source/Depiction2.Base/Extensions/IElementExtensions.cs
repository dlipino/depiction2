﻿using System;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Base.Extensions
{
    static public class IElementExtensions
    {

        //Copied and slightly modified from 1.2, used for interaction rule runner (mostly)
        //mostly a hack method
        public static object Query(this IElement elementToQuery, string queryString)
        {
            if (elementToQuery == null)
                return null;

            if (queryString.IndexOf(".") == 0)
            {
                queryString = queryString.Substring(1);

                while (queryString.Contains("."))
                {
                    queryString = queryString.Substring(queryString.IndexOf('.') + 1);
                }

                if (queryString.Trim() == String.Empty)
                    return elementToQuery;
                if (queryString.Trim().Equals("ZOI", StringComparison.InvariantCultureIgnoreCase) ||
                    queryString.Trim().Equals("ZoneOfInfluence", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                    //                    return elementToQuery.ZoneOfInfluence;
                }
                var prop = elementToQuery.GetDepictionProperty(queryString);
                if (prop == null) return null;
                return prop.Value;
            }

            return null;
        }
    }
}