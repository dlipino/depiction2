﻿using System;
using Depiction2.Base.Geo;

namespace Depiction2.Base.Events
{
    public class GeoLocationEventArgs : EventArgs
    {
        public IDepictionLatitudeLongitude PreviousLocation { get; private set; }
        public IDepictionLatitudeLongitude NewLocation { get; private set; }
//        public bool TriggerAssociatedChanges { get; set; }

        public GeoLocationEventArgs(IDepictionLatitudeLongitude previous, IDepictionLatitudeLongitude newLoc)
        {
            PreviousLocation = previous;
            NewLocation = newLoc;
//            TriggerAssociatedChanges = true;
        }
    }
}