﻿using System;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Base.Events
{
    public class PropertyChangeEventArg:EventArgs
    {
        public string PropertyName { get; private set; }
        public IElement Element { get; private set; }

        public PropertyChangeEventArg(IElement element, string propertyName)
        {
            Element = element;
            PropertyName = propertyName;
        }
    }
}