﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Depiction2.Base.Utilities
{
    public class DepictionImageResourceDictionary : ResourceDictionary
    {
        public ImageSource GetImage(string name)
        {
            //Should have some sort of exists checks
            if (!Contains(name)) return null;
            return this[name] as ImageSource;
        }
        public bool AddImage(string name, ImageSource image)
        {
            return AddImage(name, image, false);
        }
        public bool AddImage(string name, ImageSource image, bool replaceIfExists)
        {
            if (Contains(name))
            {
                if (!replaceIfExists)
                {
                    return false;
                }
                Remove(name);
            }

            Add(name, image);
            return true;
        }
        public void AppendImages(Dictionary<string, ImageSource> moreImages)
        {
            var loadedKeys = moreImages.Keys.ToList();
            foreach (var iconName in loadedKeys)
            {
                if (this[iconName] == null)
                {
                    Add(iconName, moreImages[iconName]);
                }
            }
        }
        #region static image file loading helpers
        public static BitmapSource GetBitmapFromFile(string filePath)
        {
            if (!File.Exists(filePath)) return null;
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            return GetBitmapFromStream(stream);
        }
        public static BitmapSource GetBitmapFromStream(Stream imageFileStream)
        {
            BitmapSource source = null;
            try
            {
                using (imageFileStream)
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = imageFileStream;
                    bitmapImage.EndInit();
                    source = bitmapImage;
                }

            }
            catch (NotSupportedException)
            {
                return null;
            }
            //Try to freeze
            try
            {

                source.Freeze();
            }
            catch
            {
                //Not sure why the freeze issue happens in the initial 1.4 dpn test file.
            }
            return source;
        }

        public static ImageSource GetBitmapFromTrueStream(Stream imageStream, string extensionWithDot)
        {
            using (imageStream)
            {
                BitmapDecoder decoder;
                if (!string.IsNullOrEmpty(extensionWithDot))
                {
                    try
                    {
                        switch (Path.GetExtension(extensionWithDot).ToLower())
                        {
                            case ".gif":
                            case ".giff": //Getting a strange exception with gif when trying to create a WriteableBitmap
                            //                                var bitmapImage = new BitmapImage();
                            //                                bitmapImage.BeginInit();
                            //                                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                            //                                bitmapImage.UriSource = new Uri(fullName);
                            //                                bitmapImage.EndInit();
                            //                                bitmapImage.Freeze();
                            //                                return bitmapImage;
                            //                            decoder = new GifBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.None);
                            //                            BitmapFrame gifFrame = decoder.Frames[0];
                            //                            gifFrame.Freeze();
                            //                            return gifFrame;
                            //                            break;
                            case ".tiff":
                            case ".tif":
                                decoder = new TiffBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                                BitmapCacheOption.Default);
                                break;
                            case ".png":
                                decoder = new PngBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                               BitmapCacheOption.Default);
                                break;
                            case ".bmp":
                                decoder = new BmpBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                               BitmapCacheOption.Default);
                                break;
                            case ".jpg":
                                decoder = new JpegBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                                BitmapCacheOption.Default);
                                break;
                            default:
                                decoder = BitmapDecoder.Create(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                               BitmapCacheOption.Default);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Kind of a random guess because we either use jpg or png
                        decoder = new PngBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                       BitmapCacheOption.Default);
                    }
                }
                else
                {
                    decoder = BitmapDecoder.Create(imageStream, BitmapCreateOptions.PreservePixelFormat,
                                                   BitmapCacheOption.Default);
                }
                BitmapFrame frame = decoder.Frames[0];

                //                double scale = GetTransormScale(imageHeight, imageWidth, frame.PixelWidth, frame.PixelHeight);
                //                BitmapSource thumbnail = ScaleBitmap(frame, scale);
                // this will disconnect the stream from the image completely ...
                var writable = new WriteableBitmap(frame);
                writable.Freeze();
                return writable;
            }
        }

        #endregion

        #region file helper things, from legacy

        public static bool SaveBitmap(BitmapSource bitmap, string fileNamePath)
        {
            var path = Path.GetDirectoryName(fileNamePath);
            if (string.IsNullOrEmpty(path)) return false;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (bitmap != null)
            {
                //BitmapEncoder encoder = new BmpBitmapEncoder();
                BitmapEncoder encoder = new PngBitmapEncoder();//Default save type
                if (Path.HasExtension(fileNamePath))
                {
                    switch (Path.GetExtension(fileNamePath).ToLower())
                    {
                        case ".gif":
                        case ".giff":
                            encoder = new GifBitmapEncoder();
                            break;
                        case ".tiff":
                        case ".tif":
                            encoder = new TiffBitmapEncoder();
                            break;
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                        case ".jpeg":
                            encoder = new JpegBitmapEncoder();
                            ((JpegBitmapEncoder)encoder).QualityLevel = 80;
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                }
                try
                {
                    using (var stream = new FileStream(fileNamePath, FileMode.Create))
                    {
                        var frame = bitmap as BitmapFrame;

                        if (frame == null)
                        {
                            encoder.Frames.Add(BitmapFrame.Create(bitmap));
                        }
                        else
                        {
                            encoder.Frames.Add(frame);
                        }
                        encoder.Save(stream);
                    }
                }
                catch
                {
                    return false;
                }

                return true;
            }

            return false;
        }
        #endregion
    }

}