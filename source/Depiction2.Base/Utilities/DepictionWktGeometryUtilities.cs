﻿using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Depiction2.Base.Utilities
{
    public class DepictionWktGeometryUtilities
    {
        private const string Point = "POINT";
        private const string Polygon = "POLYGON";
        private const string Line = "LINESTRING ";
        private const string MultiPoint = "MULTIPOINT";
        private const string MultiPolygon = "MULTIPOLYGON";
        private const string MultiLine = "MULTILINESTRING";

        //Assumes that polygon if first and last point are equal
        static public string PointListToSingleWkt(List<Point> pointsForGeometry)
        {
            string outString = string.Empty;
            if(pointsForGeometry == null || pointsForGeometry.Count == 0)
            {
                return outString;
            }

            var build = new StringBuilder();
            var typeName = string.Empty;
            var pCount = pointsForGeometry.Count;
            if (pCount == 1)
            {
                typeName = Point;
            }
            else
            {
                if(pCount<3)
                {
                    typeName = Line;
                }else
                {
                    if(pointsForGeometry[0].X.Equals(pointsForGeometry[pCount-1].X) && pointsForGeometry[0].Y.Equals(pointsForGeometry[pCount-1].Y))
                    {
                        typeName = Polygon;
                    }else
                    {
                        typeName = Line;
                    }
                }
            }
            build.Append(typeName);
            build.Append(" (");
            if (typeName.Equals(Polygon)) build.Append("(");
            var pastFirst = false;
            foreach (var point in pointsForGeometry)
            {
                if(pastFirst)
                {
                    build.Append(", ");
                }else
                {
                    pastFirst = true;
                }
                build.Append(string.Format("{0} {1}", point.X, point.Y));
            }

            if (typeName.Equals(Polygon)) build.Append(")");
            build.Append(")");
            return build.ToString();
        }
        //("({0} {1}, {2} {3})",
        static public string StringLineListToMultilineWkt(List<string> wtkLineList)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("MULTILINESTRING (");
            var count = 0;
            foreach (var lineS in wtkLineList)
            {

                if (count != 0)
                {
                    builder.Append(",");
                }
                builder.Append(lineS);
                count++;
            }
            builder.Append(")");
            return builder.ToString();
        }
    }
}