﻿using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Depiction2.Base.Utilities
{
    public class TestUtilities
    {
        static public string GetFullFileName(string fileName, string subFolder1, string subFolder2)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (string.IsNullOrEmpty(currentAssemblyDirectoryName)) return string.Empty;
            //            Debug.Assert();
            //            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = currentAssemblyDirectoryName;
            if(!string.IsNullOrEmpty(subFolder1))
            {
                fileDir = Path.Combine(currentAssemblyDirectoryName, subFolder1);
            }
            if (!string.IsNullOrEmpty(subFolder2))
            {
                fileDir = Path.Combine(fileDir, subFolder2);
            }
            
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            //            Assert.IsTrue(File.Exists(fullFileName), "Could not find file for terrain test");
            return fullFileName;
        }
        static public string GetFullFileName(string fileName, string holdingFolder)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (string.IsNullOrEmpty(currentAssemblyDirectoryName)) return null;
            //            Debug.Assert();
            //            Assert.IsNotNull(currentAssemblyDirectoryName);

            var fileDir = "TestFiles";
            if (!string.IsNullOrEmpty(holdingFolder))
            {
                fileDir = Path.Combine("TestFiles", holdingFolder);
            }
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            //            Assert.IsTrue(File.Exists(fullFileName), "Could not find file for terrain test");
            return fullFileName;
        }
    }
}