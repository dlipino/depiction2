﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Depiction2.Base.Utilities.Drawing
{
    public class DrawingHelpers
    {
        #region Bounds finder
        static public Rect FindEnclosingRectForPoints(IEnumerable<Point> pointList)
        {
            var pointArray = pointList.ToArray();
            if (!pointArray.Any()) return Rect.Empty;
            var points = pointArray;
            var minX = points.Min(x => x.X);
            var minY = points.Min(x => x.Y);
            var maxX = points.Max(x => x.X);
            var maxY = points.Max(x => x.Y);
            //so this rect depends on what coord system is getting used
            //for cartesian it is minx,maxy and maxX,minY
            return new Rect(new Point(minX,maxY),new Point(maxX,minY));
        }
        #endregion
        #region drawable geometry stuff
        //This method does not close the stream
        static private void DrawPointListOntoStreamGeometry(StreamGeometryContext streamContext, EnhancedPointListWithChildren enhancedPoints)
        {
            if (enhancedPoints == null) return;
            bool start = false;
            var isClosed = enhancedPoints.IsClosed;
            var isFilled = enhancedPoints.IsFilled;

            foreach (var p in enhancedPoints.Points)
            {
                if (!start)
                {
                    streamContext.BeginFigure(p, isFilled, isClosed);
                    start = true;
                }
                else
                {
                    streamContext.LineTo(p, true, true);
                }
            }
            var childPoints = enhancedPoints.ChildLists;
            if (childPoints == null) return;
            foreach (var child in childPoints)
            {
                bool drawStarted = false;
                var isHoleClosed = child.IsClosed;
                var isHoleFilled = child.IsFilled;

                foreach (var p in child.Points)
                {
                    if (!drawStarted)
                    {
                        streamContext.BeginFigure(p, isHoleFilled, isHoleClosed);
                        drawStarted = true;
                    }
                    else
                    {
                        streamContext.LineTo(p, true, true);
                    }
                }
            }
        }

        static public StreamGeometry CreateGeometryFromEnhancedPointListWithChildren(EnhancedPointListWithChildren enhancedPoints)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (enhancedPoints == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            DrawPointListOntoStreamGeometry(context, enhancedPoints);

            context.Close();
            streamGeometry.Freeze();
            return streamGeometry;

        }

        static public StreamGeometry CreateGeometryFromEnhancedPointListWithChildrenList(List<EnhancedPointListWithChildren> zoiPixelPointCollectionList)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (zoiPixelPointCollectionList == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            foreach (var pl in zoiPixelPointCollectionList)
            {
                DrawPointListOntoStreamGeometry(context, pl);
            }

            context.Close();
            streamGeometry.Freeze();

            return streamGeometry;
        }
        
        #endregion
    }
}