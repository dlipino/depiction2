﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Depiction2.Base.Utilities.Drawing
{
    public class PointListToMapStreamGeometryConverter: IValueConverter
    {
        #region static helpers

        static public EnhancedPointListWithChildren ConvertCartisianToDrawEnhancedList(EnhancedPointListWithChildren toConvert)
        {
            var pointList = toConvert;

            var converted = new EnhancedPointListWithChildren();
            converted.Points = ConvertCartisianToDrawList(pointList.Points);
            converted.IsClosed = pointList.IsClosed;
            converted.IsFilled = pointList.IsFilled;
            converted.TypeOfPointList = pointList.TypeOfPointList;

            if (pointList.ChildLists != null)
            {
                foreach (var elist in pointList.ChildLists)
                {
                    var enhancedList = new EnhancedPointList();
                    enhancedList.Points = ConvertCartisianToDrawList(elist.Points);
                    enhancedList.IsFilled = elist.IsFilled;
                    enhancedList.IsClosed = elist.IsClosed;
                    enhancedList.TypeOfPointList = elist.TypeOfPointList;

                    converted.AddChild(enhancedList);
                }
            }
            return converted;
        }

        static public StreamGeometry CreateDrawStreamFromCartisianList(EnhancedPointListWithChildren pointsToConvert)
        {
            var pointList = ConvertCartisianToDrawEnhancedList(pointsToConvert);

            return DrawingHelpers.CreateGeometryFromEnhancedPointListWithChildren(pointList);
        }

        static private List<Point> ConvertCartisianToDrawList(List<Point> origPointList)
        {
            var convertedPointList = new List<Point>();
            if (origPointList != null)
            {
                foreach (var p in origPointList)
                {
                    convertedPointList.Add(LocationConverter.ConvertCartisianToPanelPoint(p));
                }
            }
            return convertedPointList;
        }

        #endregion

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var inValue = value as EnhancedPointListWithChildren;
            return CreateDrawStreamFromCartisianList(inValue);
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}