﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Depiction2.Base.Utilities.Drawing
{
    #region helper enums
    public enum PointListType
    {
        Points,
        Line,
        MultiLine,
        Shell,
        Hole
    }
    #endregion

    #region A point list that gives indication of open or closed

    public class EnhancedPointList : IDisposable
    {
        
//        public IEnumerable<Point> {GetHashCode(){return }}
        public List<Point> Points { get; set; }
        public Point? Offset { get; set; }
        public PointListType TypeOfPointList { get; set; }
        public bool IsClosed { get; set; }
        public bool IsFilled { get; set; }

        #region Construction/Destruction stuff

        public EnhancedPointList()
        {
            TypeOfPointList = PointListType.Points;
            
            Points = new List<Point>();
        }

        virtual public void Dispose()
        {
            if (Points != null)
            {
                Points.Clear();
            }
            Points = null;
        }

        #endregion

        public void Add(Point point)
        {
            Points.Add(point);
        }

        virtual public bool IsEmpty()
        {
            if (Points.Count == 0) return true;
            return false;
        }

        virtual public void ShiftPoints(Point offset)
        {
            //Why doesn't offset do the trick?
            for (int i = 0; i < Points.Count; i++)
            {
                var old = Points[i];
                Points[i] = new Point(old.X + offset.X, old.Y + offset.Y);
            }
            Offset = offset;
        }
    }

    #endregion

    #region Point list with children, ie islands

    public class EnhancedPointListWithChildren : EnhancedPointList
    {
        public int MainCount { get { return Points.Count; } }
        public List<EnhancedPointList> ChildLists { get; set; }

        #region Construction/Destruction

        public EnhancedPointListWithChildren()
        {
            ChildLists =null;//= new List<EnhancedPointList>();
        }
        
        public Rect GetBounds()
        {
            var totalRect = Rect.Empty;
            if(ChildLists != null)
            {
                foreach(var child in ChildLists)
                {
                    var cRect = DrawingHelpers.FindEnclosingRectForPoints(child.Points);
                    
                    if(!cRect.Equals(Rect.Empty))
                    {
                        if(totalRect.Equals(Rect.Empty))
                        {
                            totalRect = cRect;
                        }else
                        {
                            totalRect.Union(cRect);
                        }
                    }
                }
            }

            var mainRect = DrawingHelpers.FindEnclosingRectForPoints(Points);
            if(!mainRect.Equals(Rect.Empty))
            {
                if(totalRect.Equals(Rect.Empty))
                {
                    totalRect.Union(totalRect);
                }else
                {
                    totalRect = mainRect;
                }
            }
            return totalRect;
        }
        public override void Dispose()
        {
            base.Dispose();
            if(ChildLists != null)
            {
                foreach (var enhancedPointList in ChildLists)
                {
                    enhancedPointList.Dispose();
                }
            }
            ChildLists = null;
        }
        
        #endregion
        #region helpers

        public void AddChild(EnhancedPointList child)
        {
            if(ChildLists == null)
            {
                ChildLists = new List<EnhancedPointList>();
            }
            ChildLists.Add(child);
        }

        #endregion

        public override bool IsEmpty()
        {
            if (!base.IsEmpty()) return false;
            if (ChildLists == null) return true;
            if (ChildLists.Count != 0) return false;
            foreach(var cPoints in ChildLists)
            {
                if (!cPoints.IsEmpty()) return false;
            }
            return true;
        }

        public override void ShiftPoints(Point offset)
        {
            if(ChildLists != null)
            {
                foreach (var child in ChildLists)
                {
                    child.ShiftPoints(offset);
                }
            }
            
            base.ShiftPoints(offset);
        }
    }

    #endregion


}