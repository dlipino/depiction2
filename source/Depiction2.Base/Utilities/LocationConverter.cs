﻿using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.Utilities
{
    public class LocationConverter
    {
        static public ICartRect ConvertPanelToCartisianRect(Rect drawRect)
        {
            if (drawRect.IsEmpty) return null;
            var corner1 = drawRect.TopLeft;
            var corner2 = drawRect.BottomRight;
            var geoRect = new CartRect(ConvertPanelToCartisianPoint(corner1), ConvertPanelToCartisianPoint(corner2));
            return geoRect;
        }
        static public Rect ConvertCartesianToDrawRect(ICartRect drawRect)
        {
            if (drawRect.IsEmpty) return Rect.Empty;
            var corner1 = new Point(drawRect.Left,drawRect.Top);
            var corner2 = new Point(drawRect.Right, drawRect.Bottom);
            var geoRect = new Rect(ConvertCartisianToPanelPoint(corner1), ConvertCartisianToPanelPoint(corner2));
            return geoRect;
        }

        static public Point ConvertCartisianToPanelPoint(Point cartPosition)
        {
            //assume the values come in as lat long values
            //which means x= long, y = lat and the long should be negated
            return new Point(cartPosition.X, -cartPosition.Y);
        }

        static public Point ConvertPanelToCartisianPoint(Point pixelLocation)
        {
            return new Point(pixelLocation.X, -pixelLocation.Y);
        }

        static public Point ConvertCanvasPointToMapDrawPoint(Point canvasPoint, double scale, Point offset)
        {
            var scalePos = (Vector)canvasPoint / scale;
            var virtualPanelVector =(Vector) offset*scale;// new Vector(panelPixelShiftVector.X / zoom, -panelPixelShiftVector.Y / zoom);
            var mapPoint = new Point(offset.X + scalePos.X, offset.Y - scalePos.Y);
//            var mapPoint = (Point)((Vector)offset + scalePos);
            return mapPoint;
        }
        //        public Point ConvertViewPointToDrawPoint(Point viewPoint)
        //        {
        //            var scalePos = (Vector)viewPoint / WorldScale;
        //            var mapPoint = (Point)((Vector)WorldOffset / WorldScale + scalePos);
        //
        //            return mapPoint;
        //        }
        //
        //        public Point ConvertDrawPointToViewPoint(Point mapPoint)
        //        {
        //            var viewPoint = WorldScale * ((Vector)mapPoint - (Vector)WorldOffset / WorldScale);
        //            return (Point)viewPoint;
        //        }
    }
}