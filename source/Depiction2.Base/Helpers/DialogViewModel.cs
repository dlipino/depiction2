﻿using System;
using System.Windows.Input;

namespace Depiction2.Base.Helpers
{
    public class DialogViewModel : ViewModelBase
    {
        public event EventHandler RequestClose;
        #region the request close command 
//        is not very well thought out yet, but it should help with all the close buttons 
        //the do nothing

        private DelegateCommand requestCloseCommand;
        public ICommand RequestDialogCloseCommand
        {
            get
            {
                if(requestCloseCommand == null)
                {
                    requestCloseCommand = new DelegateCommand(RequestDialogClose);
                }
                return requestCloseCommand;
            }
        }
        #endregion
        protected bool _modal = true;
        protected string _id;
        protected bool _isHidden = false;

        public bool IsModal
        {
            get { return _modal; }
            set { _modal = value; }
        }

        public bool IsHidden { get { return _isHidden; } set { _isHidden = value; OnPropertyChanged(()=>IsHidden); } }
        public bool HideOnClose { get; set; }

        public string Id
        {
            get { return _id; }
        }

        public string Title { get; set; }
        #region dialog closing stuff, which might not be what is needed for depictions main windows
        //Really don't know what it does or how much it overlaps with the request close a9asside from the rutnrn value
        //im guessing this is used in modal dialogs
        private bool? mDialogCloseResult;
        public bool? DialogCloseResult
        {
            get
            {
                return mDialogCloseResult;
            }

            set
            {
                
                if (mDialogCloseResult != value)
                {
                    mDialogCloseResult = value;
                    OnPropertyChanged(() => DialogCloseResult);
                }
            }
        }
        #endregion
        #region Constructor/destructions

        public DialogViewModel():this(Guid.NewGuid().ToString())
        {
            
        }
        public DialogViewModel(string id)
        {
            _id = id;
            IsHidden = true;
        }
        protected override void OnDispose()
        {
            //Kind of a hack, but when the dispose is called need to make sure that any viewmodels that are using it and are getting
            //closed don't go into hide mode.
            HideOnClose = false;
            base.OnDispose();
        }
        #endregion
        public void RequestDialogClose()
        {
            if (RequestClose != null)
            {
                RequestClose(this,new EventArgs());
            }
        }

         
     
    }
}