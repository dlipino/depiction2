﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Depiction2.Base.Helpers
{
    public class DepictionDialogWindowBase : Window
    {

        private static RoutedUICommand closeWindow;
        public static RoutedUICommand CloseWindow
        {
            get { return closeWindow; }
        }
        private Point initialLocation;// = new Point(double.NaN,double.NaN);
        private bool sizeChangeListenerAttached;
        public Style DepictionWindowStyle
        {
            get
            {
                Style frameStyle = null;
                string styleName = "DepictionWindowStyle";
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    if (Application.Current.Resources.Contains(styleName))
                    {
                        frameStyle = Application.Current.Resources[styleName] as Style;
                    }
                }
                return frameStyle;
            }
        }

        public string AssociatedHelpFile { get; set; }

        #region Dependency props
        public SolidColorBrush TitleBackground
        {
            get { return (SolidColorBrush)GetValue(TitleBackgroundProperty); }
            set { SetValue(TitleBackgroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBackgroundProperty =
            DependencyProperty.Register("TitleBackground", typeof(SolidColorBrush), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(Brushes.Black));


        public SolidColorBrush TitleForeground
        {
            get { return (SolidColorBrush)GetValue(TitleForegroundProperty); }
            set { SetValue(TitleForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleForegroundProperty =
            DependencyProperty.Register("TitleForeground", typeof(SolidColorBrush), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(Brushes.White));

        public bool ShowInfoButton
        {
            get { return (bool)GetValue(ShowInfoButtonProperty); }
            set { SetValue(ShowInfoButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInfoButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInfoButtonProperty =
            DependencyProperty.Register("ShowInfoButton", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));

        public bool Resizeable
        {
            get { return (bool)GetValue(ResizeableProperty); }
            set { SetValue(ResizeableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Resizeable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResizeableProperty =
            DependencyProperty.Register("Resizeable", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));


        //HideOnClose is a total hack and used as an immedieate fix to the problem with long lasting windows being completely close (when the needt be hidden prematurely
        //THis is a hack property
        public bool HideOnClose
        {
            get { return (bool)GetValue(HideOnCloseProperty); }
            set { SetValue(HideOnCloseProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HideOnClose.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HideOnCloseProperty =
            DependencyProperty.Register("HideOnClose", typeof(bool), typeof(DepictionDialogWindowBase), new UIPropertyMetadata(false));



        #endregion

        public DepictionDialogWindowBase()
        {
            RenderOptions.SetBitmapScalingMode(this, BitmapScalingMode.HighQuality);
            Style = DepictionWindowStyle;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            Initialized += BaseDepictionWindow_Initialized;
            AssociatedHelpFile = "welcome.html";
            AttachSizeChangeListener();

            closeWindow = new RoutedUICommand("Close Window", "Close", typeof(DepictionDialogWindowBase));
            CommandBindings.Add(new CommandBinding(CloseWindow, (s, e) =>
            {
                e.Handled = true;
                Close();
            }));
        }



        #region Size changes
        void AttachSizeChangeListener()
        {
            if (!sizeChangeListenerAttached)
            {
                SizeChanged += BaseDepictionWindow_SizeChanged;

                LocationChanged += BaseDepictionWindow_LocationChanged;
                sizeChangeListenerAttached = true;
            }
        }

        void BaseDepictionWindow_LocationChanged(object sender, EventArgs e)
        {
            LocationOrSizeChanged_Updated();
        }
        void BaseDepictionWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            LocationOrSizeChanged_Updated();
        }
        virtual protected void LocationOrSizeChanged_Updated()
        {

        }
        virtual public void SetLocationToStoredValue()
        {

        }

        #endregion


        void BaseDepictionWindow_Initialized(object sender, EventArgs e)
        {
            var mainWindow = Application.Current.MainWindow;
            if (!ReferenceEquals(this, mainWindow))
            {
                if (mainWindow.IsLoaded) Owner = mainWindow;
            }
        }

        private void title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public override void OnApplyTemplate()
        {
            var close = (Button)Template.FindName("PART_Close", this);
            if (null != close)
            {
                close.IsCancel = true;
                
            }

            var title = (FrameworkElement)Template.FindName("PART_Title", this);
            if (null != title)
            {
                title.MouseLeftButtonDown += title_MouseLeftButtonDown;
            }
            base.OnApplyTemplate();
        }

    }
}