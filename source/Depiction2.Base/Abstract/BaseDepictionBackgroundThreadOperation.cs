using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using Depiction2.Base.Service;

namespace Depiction2.Base.Abstract
{
    abstract public class BaseDepictionBackgroundThreadOperation : IDepictionBackgroundService
    {
        public event Action<IDepictionBackgroundService> BackgroundServiceFinished;
        public event Action<string> BackgroundServiceStatusReportUpdate;

        #region Variables
        private Guid serviceID = Guid.NewGuid();
        private BackgroundWorker backgroundWorker = new BackgroundWorker();
        private bool serviceComplete;
        #endregion

        #region Properties
        public bool ServiceStopRequested
        {
            get;
            protected set;
        }

        public Guid ServiceID
        {
            get { return serviceID; }
        }
        virtual public string ServiceName
        {
            get { return ""; }
        }
        public string ServiceStatusReport
        {
            get;
            private set;
        }
        virtual public bool IsServiceRefreshable
        {
            get { return false; }
        }

        #endregion

        #region Constructor

        protected BaseDepictionBackgroundThreadOperation()
        {
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;
            backgroundWorker.ProgressChanged += backgroundWorker_ProgressChanged;
            backgroundWorker.DoWork += backgroundWorker_DoWork;
            backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
        }

        #endregion
        //set up the service, return false if something goes wrong, in foreground thread
        protected abstract bool ServiceSetup(Dictionary<string,object> args);
        //The actual service, in background thread
        protected abstract object ServiceAction(object args);
        //What happens when the service is compelte, foreground thread
        protected abstract void ServiceComplete(object args);

        public virtual void RefreshResult()
        {
        }

    
        public void StartBackgroundService(object arg)
        {
            if (backgroundWorker.IsBusy) return;

            ServiceStopRequested = false;
            //UpdateStatusReport("Starting service");
            if (!ServiceSetup(arg as Dictionary<string, object>))
            {
                //Call an ealry cancel? because this might have already been added, or just clean up
                //the possible race like condition
                if (BackgroundServiceFinished != null)
                {
                    BackgroundServiceFinished.Invoke(this);
                }
                return;
            }
            backgroundWorker.RunWorkerAsync(arg);
        }
        virtual public void StopBackgroundService()
        {
            ServiceStopRequested = true;
            UpdateStatusReport("Cancel Requested");
            //backgroundWorker.CancelAsync();

        }
        public void UpdateStatusReport(string update)
        {
            var message = update;
            if (ServiceStopRequested)
            {
                message = "Canceling";
                if(backgroundWorker.IsBusy)
                {
                    try
                    {
                        backgroundWorker.ReportProgress(0, message);
                        backgroundWorker.CancelAsync();   
                    }catch{}
                }else
                {
                    if (BackgroundServiceFinished != null)
                    {
                        BackgroundServiceFinished.Invoke(this);
                    }
                }
               
            }
            else if (serviceComplete) SendStatusChange(message);
            else
            {
                try
                {
                    backgroundWorker.ReportProgress(0, message);
                }catch(Exception ex)
                {
                    Console.WriteLine("Annoying double cancel error with background thread.");
                }
            }
        }
        protected void SendStatusChange(string update)
        {
            ServiceStatusReport = update;
            if (BackgroundServiceStatusReportUpdate != null)
            {
                BackgroundServiceStatusReportUpdate.Invoke(ServiceStatusReport);
            }
        }
        #region event handling

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            serviceComplete = true;
            if(e.Error != null)
            {
                
            }else if(e.Cancelled)
            {
                
            }else
            {
                var result = e.Result;
                ServiceComplete(result);
            }
            
            if (BackgroundServiceFinished != null)
            {
                BackgroundServiceFinished.Invoke(this);
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            serviceComplete = false;
            Thread.Sleep(1000);//Give UI time to catch up
            e.Result = ServiceAction(e.Argument);
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SendStatusChange(e.UserState.ToString());
        }

        #endregion

    }
}