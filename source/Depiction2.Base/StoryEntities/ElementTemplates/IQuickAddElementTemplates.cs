﻿using System.Collections.Generic;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    //Duplicate IElementTemplateLibraryFilter
    public interface IQuickAddElementTemplates
    {
        string ListName { get; set; }
        IEnumerable<string> ScaffoldNames { get; }
        bool AddElementType(string name);
        bool RemoveElementType(string name);
        bool ClearElements();
    }
}