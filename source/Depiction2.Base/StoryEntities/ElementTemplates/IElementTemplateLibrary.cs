﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    public interface IElementTemplateLibrary : INotifyPropertyChanged, IDisposable
    {
        IEnumerable<IElementTemplateLibraryFilter> LibraryFilters { get; } 
        IEnumerable<IElementTemplate> ProductElementTemplates { get; }
        IEnumerable<IElementTemplate> LoadedStoryElementTemplates{ get; }

        IElementTemplate FindElementTemplate(string scaffoldType);
        void AddElementTemplatesToDefault(IEnumerable<IElementTemplate> templates);
        void AddStoryElementTemplates(List<IElementTemplate> loadedStoryElementTemplates);
    }
}