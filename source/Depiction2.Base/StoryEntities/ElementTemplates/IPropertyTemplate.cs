﻿using System.Collections.Generic;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    public interface IPropertyTemplate
    {
        string ProperName { get; }
        string DisplayName { get; }
        string TypeString { get; }
        string ValueString { get; }
        IValidationRule[] LegacyValidationRules { get; }
        Dictionary<string, string[]> LegacyPostSetActions { get; }
    }
}