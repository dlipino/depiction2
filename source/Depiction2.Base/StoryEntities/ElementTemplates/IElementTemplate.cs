﻿using System;
using System.Collections.Generic;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    public interface IElementTemplate
    {
        string ElementType { get; }
        string DisplayName { get; }
        string IconSource { get; }
        bool ShowPropertiesOnCreate { get; set; }

        string DefinitionSource { get; }
        bool HideFromLibrary { get; }

        //adding purpose variables, these are subject to change, might be scaffold only 
        //properties
        bool IsMouseAddable { get; }
        string ShapeType { get; }//For adding purposes, so shape type is not a good 

        List<DefaultTemplateProperty> InfoProperties { get; }
        HashSet<string> Tags { get; }
        List<IPropertyTemplate> Properties { get; }//properties that make this different from a generic element
        HashSet<string> HoverTextProps { get; }
        Dictionary<string, string[]> OnCreateActions { get; }
        Dictionary<string, string[]> OnClickActions { get; }
    }

    public class ElementTemplateTypeComparer<T> : IEqualityComparer<T> where T : IElementTemplate
    {
        public bool Equals(T x, T y)
        {
            if (x.ElementType.Equals(y.ElementType, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
        }

        public int GetHashCode(T obj)
        {
            return obj.ElementType.ToLower().GetHashCode();
        }
    }
}