﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    [DataContract]
    public class DefaultTemplateProperty
    {
        //The way to descibe the clr props
        [DataMember, DefaultValue("")]
        public string ProperName { get; set; }
        [DataMember, DefaultValue("")]
        public string ValueString { get; set; }

        public DefaultTemplateProperty(string name, string stringValue)
        {
            ProperName = name;
            ValueString = stringValue;
        }
        public override string ToString()
        {
            return string.Format("{0} : {1}", ProperName, ValueString);
        }
    }
}