﻿using System.Collections.Generic;

namespace Depiction2.Base.StoryEntities.ElementTemplates
{
    public interface IElementTemplateLibraryFilter
    {
        string FilterName
        {
            get;
            set;
        }
        IEnumerable<string> ElementTemplateNames { get; }
        bool AddElementType(string name);
        bool RemoveElementType(string name);
        void ClearElements();

        bool SaveFilter();
    }
}