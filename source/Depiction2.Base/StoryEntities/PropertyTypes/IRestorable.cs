﻿namespace Depiction2.Base.StoryEntities.PropertyTypes
{
    public interface IRestorable
    {
        void RestoreProperty(bool notifyPropertyChange, IElement propertyOwner);
    }
}
