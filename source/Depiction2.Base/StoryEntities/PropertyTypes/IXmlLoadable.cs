﻿using System.Xml;

namespace Depiction2.Base.StoryEntities.PropertyTypes
{
    public interface IXmlLoadable
    {
        void ReadFromXml(XmlReader reader);
    }
}