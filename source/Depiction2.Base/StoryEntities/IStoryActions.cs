﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Base.StoryEntities
{
    public interface IStoryActions
    {
        event Action<IElementTemplate, bool> PlaceTempElement;

        event Action<IElementTemplate, bool> StoryRequestMouseAdd;
        event Action<IEnumerable<IElement>> RequestElementView;

        void TriggerElementPlacement(IElementTemplate template, IEnumerable<Point> geometryPoints);
        void TriggerRequestElementView(IEnumerable<IElement> elements);
        void TriggerStoryRequestMouseAdd(IElementTemplate template, bool multiAdd);
    }
}