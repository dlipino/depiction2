﻿using System;

namespace Depiction2.Base.StoryEntities
{
    public interface IRevealerProperty
    {
        object Value { get;  }
        string PropertyName { get; set; }
        Type PropertyType { get; }
        bool SetValue(string stringValue, Type valueType);
        bool SetValue(object value, bool changeType);
    }
}