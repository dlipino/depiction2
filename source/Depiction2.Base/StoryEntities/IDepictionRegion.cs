﻿using System.Collections.Generic;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;

namespace Depiction2.Base.StoryEntities
{
    public interface IDepictionRegion : IElementDisplayer
    {

        IEnumerable<string> ConnectedDataSources { get; }
        IDepictionGeometry RegionGeometry { get; }
        ICartRect RegionRect { get; }
        void AddManagedElementId(string elementId);
        bool AddDataSource(RegionDataSourceInformation dataSource);
        bool RemoveDataSource(RegionDataSourceInformation dataSource);

        void UpdateSourceData();
    }
}