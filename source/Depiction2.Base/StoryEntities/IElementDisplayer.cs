﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.StoryEntities
{
    public interface IElementDisplayer : INotifyPropertyChanged, IDisposable
    {
        event NotifyCollectionChangedEventHandler DisplayedElementsChangedIdList;
      
        string DisplayerName { get; set; }
        string DisplayerId { get; }
        ReadOnlyCollection<string> ElementIds { get; }
        double GeoTop { get; }
        double GeoBottom { get; }
        double GeoLeft { get; }
        double GeoRight { get; }
        ICartRect GeoRectBounds { get; }

        void ConnectToStory(IStory mainStory);
        void AddDisplayElementsWithMatchingIds(IEnumerable<string> elementIdsToShow);
        void RemoveElementsWithMatchingIds(IEnumerable<string> elementIdsToRemove);

        void SetBounds(IDepictionGeometry drawBounds);
        //Cartesian
        void SetBounds(Point tl, Point br);
    }
}