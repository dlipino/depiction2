﻿using System;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface IElementVisual
    {
        //The permatext controls

        bool UsePropertyNamesInInformationText { get; set; }
        bool DisplayInformationText { get; set; }
        bool UseEnhancedInformationText { get; set; }

        ElementVisualSetting VisualState { get; set; }
        
        string ZOIFillColor { get; set; }
        string ZOIBorderColor { get; set; }

        double ZOILineThickness { get; set; }
        string ZOIShapeType { get; set; }
        //Icon info
        string IconBorderShape { get; set; }
        string IconBorderColor { get; set; }

        double IconSize { get; set; }
        string IconResourceName { get; set; }

        //Image
        string ImageResourceName { get; set; }
        double ImageRotation { get; set; }
    }

    [Flags]//one would think it sets up the power of twos automatically
    public enum ElementVisualSetting
    {
//        None=0,
//        Icon = 1,
//        Geometry = 2,
//        Image= 4
        None =0x0,
        Icon = 0x1,
        Geometry = 0x2,
        Image = 0x4,
      //  PermaText = 0x8//this will get put in eventually.
    }
}