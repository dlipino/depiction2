﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public class ElementPropertiesInformation
    {
        static private readonly IDictionary<string, Type> BaseProperties = new Dictionary<string, Type>(StringComparer.CurrentCultureIgnoreCase);
        static private readonly IDictionary<string, Type> GeomProperties = new Dictionary<string, Type>(StringComparer.CurrentCultureIgnoreCase);
        static private readonly IDictionary<string, Type> VisualProperties = new Dictionary<string, Type>(StringComparer.CurrentCultureIgnoreCase);
        //How precise does this need to be, match case or not?
        static void FillProperties()
        {
            //              string ElementId { get; }
            //        string ElementType { get; }
            //        string DisplayName { get; }
            //
            //        string Author { get; set; }
            //        string Description { get; set; }
            BaseProperties.Add("ElementId", typeof(string));
            BaseProperties.Add("ElementType", typeof(string));
            BaseProperties.Add("DisplayName", typeof(string));
            BaseProperties.Add("Author", typeof(string));
            BaseProperties.Add("Description", typeof(string));
            //            Point? GeoLocation { get; }
            //        IDepictionGeometry ElementGeometry { get; }
            GeomProperties.Add("GeoLocation", typeof(Point?));
            GeomProperties.Add("ElementGeometry", typeof(IDepictionGeometry));
            #region  names
            //             bool UsePropertyNamesInInformationText { get; set; }
            //        bool DisplayInformationText { get; set; }
            //        bool UseEnhancedInformationText { get; set; }
            //
            //        ElementVisualSetting VisualState { get; set; }
            //        
            //        string ZOIFillColor { get; set; }
            //        string ZOIBorderColor { get; set; }
            //
            //        double ZOILineThickness { get; set; }
            //        string ZOIShapeType { get; set; }
            //        //Icon info
            //        string IconBorderShape { get; set; }
            //        string IconBorderColor { get; set; }
            //
            //        double IconSize { get; set; }
            //        string IconResourceName { get; set; }
            //
            //        //Image
            //        string ImageResourceName { get; set; }
            //        double ImageRotation { get; set; }
            #endregion
            VisualProperties.Add("UsePropertyNamesInInformationText", typeof(bool));
            VisualProperties.Add("DisplayInformationText", typeof(bool));
            VisualProperties.Add("UseEnhancedInformationText", typeof(bool));

            VisualProperties.Add("VisualState", typeof(ElementVisualSetting));
            VisualProperties.Add("ZOIFillColor", typeof(string));
            VisualProperties.Add("ZOIBorderColor", typeof(string));
            VisualProperties.Add("ZOILineThickness", typeof(double));
            VisualProperties.Add("ZOIShapeType", typeof(string));
            VisualProperties.Add("IconBorderShape", typeof(string));
            VisualProperties.Add("IconBorderColor", typeof(string));
            VisualProperties.Add("IconSize", typeof(double));
            VisualProperties.Add("IconResourceName", typeof(string));
            VisualProperties.Add("ImageResourceName", typeof(string));
            VisualProperties.Add("ImageRotation", typeof(double));
        }

        static public Type FindTypeForProperty(string propertyName, IElementTemplate templateReference)
        {
            if(BaseProperties.Count ==0)
            {
                FillProperties();
            }
            if (BaseProperties.ContainsKey(propertyName))
            {
                return BaseProperties[propertyName];
            }
            if (GeomProperties.ContainsKey(propertyName))
            {
                return GeomProperties[propertyName];
            }
            if (VisualProperties.ContainsKey(propertyName))
            {
                return VisualProperties[propertyName];
            }
            if (templateReference == null) return null;
            return null;
        }
    }
}