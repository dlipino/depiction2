﻿using System;
using System.Windows;
using Depiction2.Base.Events;
using Depiction2.Base.Geo;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface IElementGeo
    {
        event EventHandler<GeoLocationEventArgs> GeoLocationChange;
        Point? GeoLocation { get; }
        Point? GeometryCentroid { get; }
        IDepictionGeometry ElementGeometry { get; }
        bool IsGeometryEditable { get; set; }
        ICartRect SimpleBounds { get; }
        double SimpleBoundsArea { get; }

        void UpdatePrimaryGeoLocationAndShiftGeometry(Point? newGeoLocation);
        void GeoShiftPrimaryGeoLocationAndShiftGeometry(Vector geoShift);
        void UpdatePrimaryPointAndGeometry(Point? primaryPoint, IDepictionGeometry geometry);
        void UpdatePrimaryPointWithoutAssociatedEvents(Point primaryPoint);
    }
}