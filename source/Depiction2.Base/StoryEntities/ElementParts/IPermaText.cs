﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface IPermaText : IXmlSerializable, IDeepCloneable<IPermaText>//, INotifyPropertyChanged
    {
        bool IsDefault { get; } //Used to save space while saving,not sure if it is still relevant
        bool IsVisible { get; set; }
        double PermaTextX { get; set; }
        double PermaTextY { get; set; }
        double PixelWidth { get; set; }
        double PixelHeight { get; set; }
        double MinPixelHeight { get; }
        double MinPixelWidth { get; }
        bool IsEnhancedPermaText { get; set; }
    }
}