﻿using System.ComponentModel;
using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface ITempElementMap : INotifyPropertyChanged//, ISpatialElement
    {
        string HoverText { get; }
        Point? GeoLocation { get; }
        Point? GeoBoundsCentroid { get; }
        IDepictionGeometry ElementGeometry { get; }
        Rect SimpleBounds { get; }
        double SimpleBoundsArea { get; }

        string ElementVisibilityFlag { get;}
        bool IsDraggable { get; }

        string ZOIFillColor { get; }
        string ZOIBorderColor { get; }

        double ZOILineThickness { get; }

        string ElementBorderShape { get; }

        //Icon info
        double IconSize { get; }
        string IconName { get; }

        //Somestuff about the image that might be associated with the element

        string GeoImageResourceName { get; }
        double GeoImageRotation { get; }


        void UpdatePrimaryGeoLocationAndShiftGeometry(Point? newGeoLocation);
        void UpdatePrimaryPointAndGeometry(Point? primaryPoint, IDepictionGeometry geometry);

    }
}