﻿using System.Windows;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface ITempElementBasic
    {
        string ElemId { get; }
        string ElementType { get; }

        Point? GeoLocation { get; }
    }
}