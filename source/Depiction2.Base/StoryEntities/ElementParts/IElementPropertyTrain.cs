﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Depiction2.Base.Events;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface IElementPropertyTrain
    {
        event Action<IElementProperty> PropertyRemoved;
        event Action<IElementProperty> PropertyAdded;
        //the ones that come from the dml
        ReadOnlyCollection<Tuple<string, string>> DefaultElementPropertyNames { get; }
        //extra properties that are added when reading a file etc.
        ReadOnlyCollection<Tuple<string, string>> AdditionalPropertyNames { get; }

        void AddUserProperty(IElementProperty newProp);
        void AddUserProperty(string properName, string displayName, object value);
        void DeleteUserProperty(string propertyName);
        void DeleteUserProperty(IElementProperty propToDelete);


//        bool UpdatePropertyValue(string propertyName, object value, bool triggerChange);
        void AdjustElementWithPropertyHack(IElementProperty updateProp);

        IEnumerable<string> HoverTextProperties { get; }
        bool AddHoverTextProperty(string name,bool triggerEvent);
        bool RemoveHoverTextProperty(string name, bool triggerEvent);
        //Setting user property values is still messy, mostly because of change event control
        IElementProperty GetDepictionProperty(string properName);
        bool HasProperty(string propName);//checks for c# props and depiction props
        List<IElementProperty> GetMatchingProps(Func<IElementProperty, bool> funct);
    }
}