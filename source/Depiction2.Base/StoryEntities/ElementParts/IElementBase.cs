﻿using System;
using System.Collections.ObjectModel;

namespace Depiction2.Base.StoryEntities.ElementParts
{
    public interface IElementBase : IDisposable
    {

        string ElementId { get; }
        string ElementType { get; }
        string DisplayName { get; set; }

        string Author { get; set; }
        string Description { get; set; }

        ReadOnlyCollection<string> Tags { get; }

        bool AddTag(string tag);
        bool RemoveTag(string tag);
    }
}