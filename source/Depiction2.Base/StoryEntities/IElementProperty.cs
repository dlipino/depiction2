﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Base.StoryEntities
{
    public interface IElementProperty : INotifyPropertyChanged
    {
        bool IsReadOnly { get; }
        string ProperName { get; }
        string DisplayName { get; set; }
        object Value { get; }
        Type ValueType { get; }

        bool SetValue(object newValue);
        bool TrySetValueFromString(string stringValue);

        #region legacy stuff

        IValidationRule[] LegacyValidationRules { get; }
        Dictionary<string, string[]> LegacyPostSetActions { get; }

        #endregion
    }
    public class PropertyNameComparerIgnoreCase<T> : IEqualityComparer<T> where T : IElementProperty
    {
        public static PropertyNameComparerIgnoreCase<T> Default = new PropertyNameComparerIgnoreCase<T>();
        public bool Equals(T x, T y)
        {
            if (x.ProperName.Equals(y.ProperName, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
        }

        public int GetHashCode(T obj)
        {
            return obj.ProperName.ToLower().GetHashCode();
        }
    }
//    public class PropertyNameAndTypeComparer<T> : IEqualityComparer<T> where T : IElementProperty
//    {
//        public bool IsNumber(object val)

//        {
//            if (val == null) return false;
//            if (val is double || val is int || val is float)
//            {
//                return true;
//            }
//            return false;
//        }
//
//        public bool Equals(T x, T y)
//        {
//            bool typeIsComparable = false;
//            if (x.Value.G == y.ValueType)
//            {
//                typeIsComparable = true;
//            }
//            if (IsNumber(x.Value) && IsNumber(y.Value))
//            {
//                typeIsComparable = true;
//            }
//
//            return string.Equals(x.InternalName, y.InternalName, StringComparison.CurrentCultureIgnoreCase) && typeIsComparable;
//        }
//
//        public int GetHashCode(T obj)
//        {
//            return obj.InternalName.ToLower().GetHashCode();
//        }
//
//    }
}