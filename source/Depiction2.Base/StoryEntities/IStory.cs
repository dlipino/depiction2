﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;

namespace Depiction2.Base.StoryEntities
{
    public interface IStory : IElementRepository, INotifyPropertyChanged
    {
        event EventHandler StoryChanged;

        event NotifyCollectionChangedEventHandler AnnotationCollectionChanged;
        event NotifyCollectionChangedEventHandler RevealerCollectionChanged;
        event NotifyCollectionChangedEventHandler RegionCollectionChanged;

        IInteractionRouter InteractionsRunner { get; }//hhhmmm
        ICartRect ElementExtent { get; }
        ICartRect VisibleExtent { get; set; }

        #region parts that need to be saved

        IStoryDetails StoryDetails { get; }
        IElementDisplayer MainDisplayer { get; }
        IEnumerable<IDepictionRegion> AllRegions { get; } 

        double StoryZoom { get; }
        Point StoryFocalPoint { get; }
        string ElementCoordinateSystem { get; }
        string DisplayCoordinateSystem { get; }

        ReadOnlyCollection<IElementRevealer> RevealerDisplayers { get; }
        IEnumerable<IDepictionAnnotation> AllAnnotations { get; }

        IInteractionRuleRepository InteractionRules { get; }
        DepictionImageResourceDictionary AllImages { get; }

        #endregion

        void SetStoryImages(DepictionImageResourceDictionary imageDictionary);
        void InitializeInteractionRouter();
        //These seem like a bad thing
        void LoadNonDefaultInteractions(IList<IInteractionRule> nonDefaultRules);

        #region region stuff

        void AddRegion(IDepictionRegion newRegion);
        void RemoveRegion(IDepictionRegion regionToRemove);

        #endregion
        
        #region revealer stuff

        string TopRevealerId { get; set; }
        void AddNewRevealer(ICartRect goeBounds);
        void LoadRevealer(IElementRevealer revealer);
        void DeleteRevealer(IElementRevealer revealer);
        void DeleteRevealerWithId(string id);

        #endregion

        #region annotation add/remove

        void AddAnnotationAtLocation(double lat, double lon);
        void DeleteAnnotation(IDepictionAnnotation annotation);

        #endregion

        #region spatial reference/coordinate system methods

        string InformationForGeographicLocation(double latitude, double longitude);
        void ChangeDisplayCoordinateSystem(string newDisplayCoordinateSystem);

        #endregion
        
        void SetCenter(Point newCenter);

        //used in commands in view model, might not be needed
        void AdjustZoom(double multiplier, Point? zoomCenter);
        void Zoom(ZoomDirection direction, Point? zoomCenter);
        void ShiftStoryExtent(PanDirection direction);
        void ShiftStoryExtent(Vector direction);

    }

    public enum PanDirection
    {
        Left = 0,
        Right = 1,
        Up = 2,
        Down = 3,
        Mouse = 4,
        LeftFine,
        RightFine,
        UpFine,
        DownFine
    }
    public enum ZoomDirection
    {
        In = 0, Out = 1, InFine, OutFine
    }
}