﻿using System.ComponentModel;
using System.Windows;

namespace Depiction2.Base.StoryEntities
{
    public interface IDepictionAnnotation : INotifyPropertyChanged
    {
        Point GeoLocation { get; set; }

        string AnnotationID { get; }
        string AnnotationText { get; set; }
        //All in pixel sizes, these describe the look of the annotation and associated text
        double ContentLocationX { get; set; }
        double ContentLocationY { get; set; }
        double PixelWidth { get; set; }
        double PixelHeight { get; set; }
        double ScaleWhenCreated { get; set; }
        double IconSize { get; }

//        bool IsAnnotationVisible { get; }
        bool IsTextCollapsed { get; set; }
        string BackgroundColor { get; set; }
        string ForegroundColor { get; set; }
        
    }
}