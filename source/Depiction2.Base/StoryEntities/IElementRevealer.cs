﻿namespace Depiction2.Base.StoryEntities
{
    public interface IElementRevealer : IElementDisplayer
    {
        DisplayerViewType RevealerShape { get; set; }
        int PropertyCount { get; }
        void SetRevealerPropertyValue(string name, object value);
        void CreateNewProperty(string name,object value);
        void AddNewProperty(IRevealerProperty prop);
        object GetPropertyValue(string name);
    }

    public enum DisplayerViewType
    {
        Rectangle, Circle, Minimized, Global,Region
    }
}