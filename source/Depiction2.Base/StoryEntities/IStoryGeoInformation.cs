﻿using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.StoryEntities
{
    public interface IStoryGeoInformation
    {
        ICartRect RegionBounds { get; }
        ICartRect MapViewBounds { get; }
        Point MapViewCenter { get; }
    }
}