﻿namespace Depiction2.Base.StoryEntities
{
    public interface IDepictionFileFormat
    {
        string FileVersion { get; }
        string AppVersion { get; }
    }
}