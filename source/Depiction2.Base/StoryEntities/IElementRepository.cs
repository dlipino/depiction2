﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using Depiction2.Base.Events;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Base.StoryEntities
{
    public interface IElementRepository
    {
        event NotifyCollectionChangedEventHandler ElementCollectionChanged;
        event EventHandler<PropertyChangeEventArg> ElementPropertyChanged;
        event EventHandler<GeoLocationEventArgs> ElementGeoLocationChanged;
        //IEnumerable maybe?
        ReadOnlyCollection<IElement> AllElements { get; }

        void UpdateRepo(List<Dictionary<string, object>> updateValues, string idPropertyName, IElementTemplate newType);
        HashSet<string> GetElementIdsInRect(ICartRect enclosingGeometry);
        IEnumerable<IElement> GetElementsInGeometry(IDepictionGeometry enclosingGeometry);

        void AddElements(List<IElement> elementList); 
        void AddElements(List<IElement> elementList, bool triggerBehaviors);
        void AddElement(IElement element, bool displayInMain);
        void RemoveElements(List<IElement> elementList);
        void RemoveElement(IElement element);
        void RemoveElementsById(List<string> elementIds);

        void ClearElements();

    }
}