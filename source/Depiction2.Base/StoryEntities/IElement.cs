﻿using System.Collections.Generic;
using System.ComponentModel;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.Base.StoryEntities
{
    public interface IElement : IElementBase, IElementPropertyTrain, IElementGeo, IElementVisual, INotifyPropertyChanged
    {
        //Temp things from 1.4, actually lots of theses are left overs.

        IPermaText PermaText { get; }
        int NumberIncomingEdgesFired { get; set; }
        bool IsDraggable { get; set; }
        bool HasRestorableProperty { get; }
        void RestoreProperties(bool triggerEventChange);
        void TriggerPropertyChange(string propName);

        IList<string> AssociatedElementIds { get; }
        IEnumerable<IElement> AssociatedElements { get; }
        bool HasAssociatedElements { get; }
        bool DisableAssociatedElementListener { get; set; }//Helper, possibly even a hack
        void AttachElements(List<IElement> elements);
        void AttachElementAfter(IElement newElement, IElement afterElement);
        void DettachElement(IElement element);
        void DetachAllAssociates();//happens on delete

        Dictionary<string, string[]> PostAddActions { get; }//were onCreateActions
        Dictionary<string, string[]> OnClickActions { get; }
    }
    public class PropNames
    {
        public const string GeoLocation = "GeoLocation";
        //The string values need to match the clr properties that have 
        //map display properties
        public const string IsGeometryEditable = "IsGeometryEditable";
        public const string IsDraggable = "IsDraggable";
        public const string UsePropertyNamesInInformationText = "UsePropertyNamesInInformationText";
        public const string DisplayInformationText = "DisplayInformationText";
        public const string UseEnhancedInformationText = "UseEnhancedInformationText";

        //information properties
        public const string DisplayName = "DisplayName";
        public const string Description = "Description";
        public const string Author = "Author";

        //build in visual properties
        public const string VisualState = "VisualState";
        public const string ZOIFillColor = "ZOIFillColor";
        public const string ZOIBorderColor = "ZOIBorderColor";
        public const string ZOILineThickness = "ZOILineThickness";
        public const string ZOIShapeType = "ZOIShapeType";
        public const string IconBorderShape = "IconBorderShape";
        public const string IconBorderColor = "IconBorderColor";
        public const string IconSize = "IconSize";
        public const string IconResourceName = "IconResourceName";
        public const string ImageResourceName = "ImageResourceName";
        public const string ImageRotation = "ImageRotation";

        //Properties that are for templates
        public const string HideFromLibrary = "HideFromLibrary";
        public const string DefinitionSource = "DefinitionSource";
        public const string IsMouseAddable = "IsMouseAddable";
        public const string ShowPropertiesOnCreate = "ShowPropertiesOnCreate";
    }
}