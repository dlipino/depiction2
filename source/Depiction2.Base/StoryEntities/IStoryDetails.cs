﻿namespace Depiction2.Base.StoryEntities
{
    public interface IStoryDetails
    {
        string Version { get; }
        string Author { get; }
        string Description { get; }
        string Title { get; }
    }
}