﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.Service.Tiling
{
    public enum TileImageTypes
    {
        Unknown,
        None,
        Street,
        Satellite,
        Topographic
    }

    public interface IUTMTiler : ITiler
    {
    }

    public interface ITiler
    {
        int GetZoomLevel(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel);
        IList<TileModel> GetTiles(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel);
//        TileImageTypes TileImageType { get; }
//        string DisplayName { get; }
        string CacheLocation { get; }
//        string LegacyImporterName { get; }
        int PixelWidth { get; }
        bool DoesOwnCaching { get; }
        int LongitudeToColAtZoom(IDepictionLatitudeLongitude latLong, int zoom);
        int LatitudeToRowAtZoom(IDepictionLatitudeLongitude latLong, int zoom);
        double TileColToTopLeftLong(int col, int zoomLevel);
        double TileRowToTopLeftLat(int row, int zoom);
        TileModel GetTileModel(TileXY tileToGet, int zoomLevel);
    }

    public struct TileXY
    {
        public int Column { get; set; }
        public int Row { get; set; }
        public int UTMZoneNumber { get; set; }//sadface
    }
}
