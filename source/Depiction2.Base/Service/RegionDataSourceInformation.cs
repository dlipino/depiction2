﻿using System;
using System.Collections.Generic;

namespace Depiction2.Base.Service
{
    public class RegionDataSourceInformation
    {
        private string _genericName = string.Empty;
        public string DisplayName { get; set; }

        public string Name { get; set; }
        public string GenericName
        {
            get
            {
                if (string.IsNullOrEmpty(_genericName))
                {
                    return Name;
                }
                return _genericName;
            }
            set { _genericName = value; }
        }
        public string Description { get; set; }

        public Dictionary<string, string> Parameters { get; set; }

        public string ElementType { get; set; }
        public bool ParameterDependent { get; set; }
        public override bool Equals(object obj)
        {
            var other = obj as RegionDataSourceInformation;
            if (other == null) return false;
            return other.Name.Equals(Name, StringComparison.InvariantCultureIgnoreCase) &&
                   other.DisplayName.Equals(DisplayName, StringComparison.InvariantCultureIgnoreCase);
        }

    }
}