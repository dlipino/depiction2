using System;

namespace Depiction2.Base.Service
{
    public interface IDepictionMessage
    {
        string MessageId { get; }
        DepictionMessageType MessageType { get; }
        double MessageDisplayTime { get; }
        string Message { get; }
        DateTime MessageDateTime { get; }
    }
}