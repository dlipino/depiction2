﻿namespace Depiction2.Base.Service
{
    //The locations for this class are mostly all wrong, but time has not been spent to fix it.
    public interface IDepictionPathService
    {
        string AppDataDirectoryPath { get; }
        string DepictionCacheHomeDirectory { get; }
        string CurrentStoryDataDirectory { get; }
        string TempFolderRootPath { get; }

        string CreateTempSubFolder();
        string RetrieveCachedFilePathIfCached(string fileName);

        void ClearLoadFileDirectory();
//        void CleanAllPaths();
    }
}