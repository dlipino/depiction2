﻿using System;
using System.Collections.Generic;

namespace Depiction2.Base.Service
{
    public interface ISimpleNameTypeService
    {
        Dictionary<string, Type> NameTypeDictonary { get; }

        Type GetTypeFromName(string name);
        void LoadCompleteDictionary();
        void UpdateSimpleNameTypeDictionary(Dictionary<string, Type> updateDictionary);
        void ClearTypeService();
        bool AddTypeToDictionary(string simpleName, Type type);
    }
}