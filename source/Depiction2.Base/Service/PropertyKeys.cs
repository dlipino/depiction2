﻿namespace Depiction2.Base.Service
{
    public class PropertyKeys
    {
        //
        public const string geometryKey = "ElementGeometry";
        public const string geometryWktKey = "ElementWktGeometry";
        public const string geoPositionKey = "GeoPosition";
        public const string elementTypeKey = "ElementType";
        public const string propertyIdName = "PropertyIdName";

        //Element property
        #region property name constants

        public const string autherKey = "Author";
        public const string descriptionKey = "Description";
        public const string displayNameKey = "DisplayName";
        public const string isGeometryEditableKey = "IsGeometryEditable";

        public const string isDraggableKey = "IsDraggable";

        public const string zoiKey = "ZOIFillColor";
        public const string zoiBorderKey = "ZOIBorderColor";
        public const string zoiLineThicknessKey = "ZOILineThickness";
        public const string zoiShapeKey = "ZOIShapeType";
        public const string iconNameKey = "IconResourceName";
        public const string iconSizeKey = "IconSize";
        public const string iconBorderColorKey = "IconBorderColor";
        public const string iconBorderShapeKey = "IconBorderShape";
        public const string imageResourceKey = "ImageResourceName";
        public const string imageRotationKey = "ImageRotation";
        public const string propertyNameInHoverTextKey = "UsePropertyNamesInInformationText";
        public const string displayHoverTextKey = "DisplayInformationText";
        public const string useEnhancedHoverTextKey = "UseEnhancedInformationText";

        #endregion
    }
}