﻿using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.Service
{
    //get things to work then clean up
    public interface IDepictionStoryProjectionService
    {
        string DepictionGCSWkt { get; }
        bool IsDepictionProjected { get; }

        Point DepictionPCSToGCS(double x, double y);
        void GCSToDepictionPCSGeom(IDepictionGeometry geometry);
        Point GCSToDepictionPCSPoint(Point point);

        void SetOtherSpatialReference(string wktString);
        Point TransformPoint(double x, double y, bool toDepiction);

        //Move these
        void RemoveProjection();
        void SetToMercator();
    }
}