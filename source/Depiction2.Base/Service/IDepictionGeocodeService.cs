﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Geo;

namespace Depiction2.Base.Service
{
    public interface IDepictionGeocodeService
    {
        List<string> RecentGeocodes { get; }
        string CurrentGeocoderDisplayName { get; }

        IEnumerable<string> AvailableGeocoderNames { get; }

        bool SetCurrentGeocoderFromExtensions(string geocoderName);
        void SetGeocoder(IDepictionGeocoder newGeocoder);
        Point? GeocodeInput(GeocodeInput input, bool recordResults);

    }

    public class GeocodeInput
    {
        public string CompleteAddress = string.Empty;
        public string StreetAddress = string.Empty;
        public string City = string.Empty;
        public string State = string.Empty;
        public string ZipCode = string.Empty;
        public string Country = string.Empty;

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(CompleteAddress)) return CompleteAddress;
            var combinedAddress = string.Empty;
            combinedAddress = CombineAddress(combinedAddress, StreetAddress);
            combinedAddress = CombineAddress(combinedAddress, City);
            combinedAddress = CombineAddress(combinedAddress, State);
            combinedAddress = CombineAddress(combinedAddress, ZipCode);
            combinedAddress = CombineAddress(combinedAddress, Country);

            return combinedAddress;
        }
        private string CombineAddress(string result, string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (!string.IsNullOrEmpty(result))
                {
                    result += "$";
                }
                result += input;
            }
            return result;
        }

    }
}