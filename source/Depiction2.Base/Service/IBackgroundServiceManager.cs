using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Depiction2.Base.Service
{
    public interface IBackgroundServiceManager : IDisposable
    {
        ReadOnlyCollection<IDepictionBackgroundService> CurrentServices { get; }
        event NotifyCollectionChangedEventHandler BackgroundServicesModified;
        void AddBackgroundService(IDepictionBackgroundService service);
        void CancelAllServices();
        void ResetServiceManager();
    }
}