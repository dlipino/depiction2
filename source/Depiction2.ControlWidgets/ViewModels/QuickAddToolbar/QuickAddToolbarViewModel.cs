﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.ControlWidgets.Models.QuickAddToolbar;

namespace Depiction2.ControlWidgets.ViewModels.QuickAddToolbar
{

    //Not currently used because there is no link to the main view model.
    public class QuickAddToolbarViewModel : ViewModelBase
    {
        #region model stuff

        private List<QuickAddList> templateQuickAddList { get; set; }
        private QuickAddList currentList;

        #endregion

        public ObservableCollection<QuickAddElemTemplateViewModel> QuickAddElements { get; set; }
        public bool IsListVisible { get; set; }

        #region Constructor destructor

        public QuickAddToolbarViewModel()
        {
            QuickAddElements = new ObservableCollection<QuickAddElemTemplateViewModel>();
            var defaultTemplates = QuickAddList.GetDefaultDepictionTemplates();

            //first attempt to load all the available lists

            //Find the first one, if that one does not exist then create a default one
            if (currentList == null)
            {
                currentList = new QuickAddList("Default List");
                foreach (var elemTemplate in defaultTemplates)
                {
                    QuickAddElements.Add(new QuickAddElemTemplateViewModel(elemTemplate));
                    currentList.AddElementTemplate(elemTemplate);
                }
            }

        }

        protected override void OnDispose()
        {
            QuickAddElements.Clear();
            base.OnDispose();
        }

        #endregion

        #region helpers

        public void AddElement(IElementTemplate elementTemplate)
        {
            if (currentList.AddElementTemplate(elementTemplate))
            {
                QuickAddElements.Add(new QuickAddElemTemplateViewModel(elementTemplate));
            }
        }

        public void RemoveElement(IElementTemplate elementTemplate)
        {
            if (currentList.RemoveElementTemplate(elementTemplate))
            {
                var viewModelMatch = QuickAddElements.First(t => t.ModelBase.ElementType.Equals(elementTemplate.ElementType));
                if (viewModelMatch != null)
                {
                    QuickAddElements.Remove(viewModelMatch);
                }
            }
        }

        public void StartElementMapAdd(IElementTemplate template)
        {
            var story = DepictionAccess.DStoryActions;
            if (story == null) return;
            story.TriggerStoryRequestMouseAdd(template, false);
        }
        #endregion

        #region things that shoudl go into the model


        #endregion
    }
}