﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.ControlWidgets.ViewModels.QuickAddToolbar
{
    public class QuickAddElemTemplateViewModel : ViewModelBase
    {
        public IElementTemplate ModelBase { private set;  get; }
        public string IconString { get; set; }
        public string TemplateElementType { get; set; }
        public string TemplateDisplayName { get; set; }

        #region constructor/destructor

        public QuickAddElemTemplateViewModel(){}

        public QuickAddElemTemplateViewModel(IElementTemplate baseTemplate)
        {
            if(baseTemplate == null) return;
            ModelBase = baseTemplate;
            IconString = baseTemplate.IconSource;
            TemplateDisplayName = baseTemplate.DisplayName;
            TemplateElementType = baseTemplate.ElementType;
        }

        internal QuickAddElemTemplateViewModel(string names)
        {
            TemplateDisplayName = names;
            TemplateElementType = "element." + names;
        }

        #endregion
    }
}