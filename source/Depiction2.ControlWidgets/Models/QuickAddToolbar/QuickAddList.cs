﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Depiction2.API;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.ControlWidgets.Models.QuickAddToolbar
{
    [DataContract]
    public class QuickAddList
    {
        [DataMember]
        public string QuickAddListName { private set; get; }

        [DataMember]
        private HashSet<string> QuickAddElementNames { get; set; }

        private List<IElementTemplate> CompleteTemplateList { get; set; } 
        
        public IEnumerable<string> AllQuickAddElements { get { return QuickAddElementNames; } } 

        public QuickAddList(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                name = "Default_Empty";
            }
            QuickAddListName = name;

            QuickAddElementNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            CompleteTemplateList = new List<IElementTemplate>();

        }

        public bool AddElementTemplate(IElementTemplate elementTemplate)
        {
            var name = elementTemplate.ElementType;
            return QuickAddElementNames.Add(name);
        }

        public bool RemoveElementTemplate(IElementTemplate elementTemplate)
        {
            var name = elementTemplate.ElementType;
            return QuickAddElementNames.Remove(name);
        }
      
        static public List<IElementTemplate> GetDefaultDepictionTemplates()
        {

            var defaultTemplates = new List<IElementTemplate>();
            var templateLibrary = DepictionAccess.TemplateLibrary;
            if (templateLibrary == null) return defaultTemplates;
            var template = templateLibrary.FindElementTemplate("Depiction.Plugin.UserDrawnLine");
            if(template != null)
            {

                defaultTemplates.Add(template);
            }

            template = templateLibrary.FindElementTemplate("Depiction.Plugin.UserDrawnPolygon");
            if(template != null)
            {
                defaultTemplates.Add(template);
            }
            return defaultTemplates;
        }
    }
}