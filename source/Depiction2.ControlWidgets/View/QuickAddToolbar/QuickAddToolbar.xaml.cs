﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.ControlWidgets.ViewModels.QuickAddToolbar;

namespace Depiction2.ControlWidgets.View.QuickAddToolbar
{
    /// <summary>
    /// Interaction logic for QuickAddToolbar.xaml
    /// 
    /// This is the combination of the view model and view. This will get changed once the MEF gets put in 
    /// </summary>
    public partial class QuickAddToolbar
    {
        public static readonly RoutedCommand QuickAddElementClickedCommand = new RoutedCommand("QuickAddElementClickedCommand", typeof(QuickAddToolbar));

        #region stuff that should be in the viewmodel

        private QuickAddToolbarViewModel QuickAddViewModel { get; set; }
        #endregion
        public QuickAddToolbar()
        {
            InitializeComponent();
            var binding = new CommandBinding(QuickAddElementClickedCommand, QuickAddElementClick);
            CommandBindings.Add(binding);
            IsEnabled = false;
            DataContextChanged += QuickAddToolbar_DataContextChanged;

            //Fake view model control
            QuickAddViewModel = new QuickAddToolbarViewModel();
            DataContext = QuickAddViewModel;
        }

        private void QuickAddElementClick(object sender, ExecutedRoutedEventArgs e)
        {
            var content = e.Parameter as QuickAddElemTemplateViewModel;
            if (content == null) return;

            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {

                QuickAddViewModel.RemoveElement(content.ModelBase);
            }
            else
            {
              QuickAddViewModel.StartElementMapAdd(content.ModelBase);
            }
        }

        void QuickAddToolbar_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                IsEnabled = true;
            }
            else
            {
                IsEnabled = false;
            }
        }

        private void LoadMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SaveMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ClearMenu_Click(object sender, RoutedEventArgs e)
        {
            QuickAddViewModel.QuickAddElements.Clear();
        }


        private void ElementsToolbar_Drop(object sender, DragEventArgs e)
        {
            var data = e.Data;

            var dataItem = data.GetData(typeof(IElementTemplate));
            if (dataItem is IElementTemplate)
            {
                var template = dataItem as IElementTemplate;
                QuickAddViewModel.AddElement(template);
                //                var templateVM = new QuickAddElemTemplateViewModel(template);
                //                QuickAddViewModel.QuickAddElements.Add(templateVM);
            }
        }
    }
}
