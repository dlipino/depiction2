using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

[assembly: InternalsVisibleTo("Depiction.UnitTests")]
namespace Depiction2.CsvExtension.Models.Exporters
{
    //    [DepictionElementExporterMetadata("DepictionCSVWriter", new[] { ".csv" }, 
    //        FileTypeInformation = "CSV file", DisplayName = "Depiction CSV writer")]
    public class DepictionCSVExporter
    {
        #region Implementation of IDepictionElementFileWriter

        public void WriteElements(string fileName, IEnumerable<IElement> elementList, bool useBackgroundThread)
        {
            var csvWriter = new DepictionCSVWriterService();
            if (useBackgroundThread)
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("FileName", fileName);
                parameters.Add("ElementList", elementList);
                var name = string.Format("Writing CSV file : {0}", Path.GetFileName(fileName));

                //Export with zoi's?
                var askForZOI = MessageBoxResult.No;
                foreach (var element in elementList)
                {
                    if (!element.ElementGeometry.GeometryType.Equals(DepictionGeometryType.Point))
                    {
                        //                        askForZOI = DepictionAccess.NotificationService.DisplayInquiryDialog("Include Zone of Influence in export?\n" +
                        //                        "(Note: Complex ZOIs in a CSV file can cause problems when loaded into spreadsheet software.)",
                        //                                                                        "Export with ZOI", MessageBoxButton.YesNo);
                        break;
                    }
                }

                if (askForZOI.Equals(MessageBoxResult.No))
                {
                    parameters.Add("ExportZOI", false);
                }
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(csvWriter);

                csvWriter.UpdateStatusReport(name);
                csvWriter.StartBackgroundService(parameters);

            }
            else
            {
                csvWriter.DoWriteElements(fileName, elementList, true);
            }
        }

        #endregion

        public object AddonMainView
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }

        public string AddonConfigViewText { get { return null; } }

        public object AddonActivationView
        {
            get { return null; }
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }

        public void ExportElements(object location, IEnumerable<IElement> elements)
        {
            if (location == null) return;
            var fileName = location.ToString();
            WriteElements(fileName, elements, true);
        }

    }

    public class DepictionCSVWriterService : BaseDepictionBackgroundThreadOperation
    {
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var fileName = parameters["FileName"].ToString();
            var elementList = parameters["ElementList"] as IEnumerable<IElement>;
            var exportZOIString = "ExportZOI";
            var exportZOI = true;
            if (parameters.ContainsKey(exportZOIString))
            {
                exportZOI = (bool)parameters["ExportZOI"];
            }
            DoWriteElements(fileName, elementList, exportZOI);
            return null;
        }

        protected override void ServiceComplete(object args)
        {

        }

        #endregion

        #region stuff that actually does the writing
        public void DoWriteElements(string fileName, IEnumerable<IElement> elementList, bool exportZOI)
        {
            DoWriteElements(fileName, elementList, exportZOI, null);
        }
        //Used for tests
        internal void DoWriteElements(string fileName, IEnumerable<IElement> elementList, bool exportZOI, string modCharForTests)
        {
            if (elementList == null) return;
            if (GetExportableElementCount(elementList) <= 0) return;
            //for now automatically give elements eid if they do not exist
            foreach (var element in elementList)
            {
                CSVElementSynchronizationService.PrepareElementForSynchronization(element);
            }
            try
            {
                using (var writer = File.CreateText(fileName))
                {
                    var builder = new StringBuilder();
                    var properties = AllProperties(elementList);

                    //Prop names to ignore
                    //Author Description DataCategory VisualType IconPath
                    var propToNotExport = new List<string> { "DataCategory", "VisualType", "IconPath" };
                    //write the header
                    foreach (var property in properties)
                    {
                        if (!propToNotExport.Contains(property.ProperName))
                        {
                            builder.Append(property.ProperName);
                            builder.Append(",");
                        }
                    }
                    builder.Append("ElementType");
                    if (exportZOI) builder.Append(",ZOI");
                    builder.AppendLine();

//                    var comparer = new PropertyComparers.InternalPropertyNameComparer<IElementProperty>();
                    //write the elements
                    int count = 0;
                    var total = elementList.Count();
                    foreach (var element in elementList)
                    {
                        count++;
                        if (ServiceStopRequested) break;
                        UpdateStatusReport(string.Format("Writing element {0} of {1}", count, total));
                        foreach (var property in properties)
                        {
                            if (!propToNotExport.Contains(property.ProperName))
                            {
//                                if (element.OrderedCustomProperties.Contains(property, comparer))
//                                {
//                                    object objectVal;
//                                    if (element.GetPropertyValue(property.InternalName, out objectVal))
//                                    {
//                                        var val = DepictionTypeConverter.ChangeType(objectVal, typeof(string)).ToString();
//                                        if (objectVal is string && !string.IsNullOrEmpty(modCharForTests) &&
//                                            !property.InternalName.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
//                                        {
//                                            val = (string)objectVal + modCharForTests;
//                                        }
//                                        //                                        var val = objectVal.ToString();
//                                        //                                        //hack fix to problem with special chars in certain types of lat/long string formats
//                                        //                                        if(objectVal is LatitudeLongitude)
//                                        //                                        {
//                                        //                                            val = ((LatitudeLongitude) objectVal).ToXmlSaveString();
//                                        //                                        }
//                                        if (val.Contains(","))
//                                            val = "\"" + val + "\"";
//                                        builder.Append(val);
//                                    }
//                                }
                                builder.Append(",");
                            }
                        }
                        builder.Append(element.ElementType);
                        if (exportZOI) builder.Append(",\"" + element.ElementGeometry + "\"");
                        builder.AppendLine();
                    }
                    if (!ServiceStopRequested)
                    {
                        writer.Write(builder.ToString());
                        writer.Flush();
                    }
                    else
                    {
                        writer.Close();//Does this close automatically?
                    }
                }
                if (ServiceStopRequested)
                {
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (IOException e)
            {
//                var message = new DepictionMessage(string.Format("Could not export CSV elements to {0} : {1}", fileName, e));
//                DepictionAccess.NotificationService.DisplayMessage(message);
            }
        }
        #endregion

        #region private helpers, also older stuff

        private static bool CanExportType(DepictionGeometryType geomType)
        {
            switch (geomType)
            {
                case DepictionGeometryType.Point:
                case DepictionGeometryType.MultiPoint:
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    return true;
            }
            return false;
        }
        private static IList<IElementProperty> AllProperties(IEnumerable<IElement> elements)
        {
            //            var propComparer = new PropertyComparers.PropertyNameAndTypeComparer<IElementProperty>();
            Func<IElementProperty, bool> notNullValue = p => p.Value != null;
            List<IElementProperty> properties = null;
            foreach (var element in elements)
            {
                var elementProperties = element.GetMatchingProps(notNullValue);

                List<IElementProperty> properties1 = properties;
                if (properties == null)
                    properties = elementProperties;
                else
                    properties = properties1.Union(elementProperties).ToList();
            }
            return properties;
        }

        private static int GetExportableElementCount(IEnumerable<IElement> elementList)
        {
            var cnt = 0;
            foreach (var elem in elementList)
            {
                if (elem.ElementGeometry != null && CanExportType(elem.ElementGeometry.GeometryType))
                    cnt++;
            }
            return cnt;
        }
        #endregion
    }
}