﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction.CSVExtension.Exceptions;
using Depiction.Externals.Csv;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.CsvExtension.Externals;
using Depiction2.CsvExtension.Externals.Exceptions;
using Depiction2.CsvExtension.ViewModels;
using Depiction2.CsvExtension.Views;

namespace Depiction2.CsvExtension.Models
{
   [DepictionFileLoaderMetadata(Name = "CSVImporter", Description = "CSV File reader", DisplayName = "CSV File Importer", Author = "Depiction Inc.", SupportedExtensions = new[] { ".csv" })]
    public class CSVFileElementImporter : IFileLoaderExtension
    {//This works as longs a the reader add in is "recreatd" every time it gets called. If it uses the same one
        ImportFromCsvView view = new ImportFromCsvView();
        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName, ICartRect depictionRegion)
        {
            var fileName = sourceLocation;
            var defaultTypeString = defaultTemplate.ElementType;
            var importFromCsvViewModel = new ImportFromCsvViewModel(defaultTypeString);
            //importFromCsvViewModel.Properties = FindHeaders(csvFile);

            string[] headers;
            var rows = FindFirstFiveRowsAndHeaders(fileName, out headers);
            importFromCsvViewModel.SetFirstFiveRows(headers, rows);
            importFromCsvViewModel.FileInfo = new FileInfo(fileName);
            //hack 
            if (depictionRegion == null)
            {
                importFromCsvViewModel.CropDataToRegion = false;
            }
            else
            {
                importFromCsvViewModel.CropDataToRegion = true;
            }

            if (headers.Contains("Position"))
            {
                importFromCsvViewModel.LocationStringMode = CSVLocationModeType.LatitudeLongitudeSingleField;
            }
            else if (headers.Contains("Latitude") && headers.Contains("Longitude"))
            {
                importFromCsvViewModel.LocationStringMode = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            }

            // var prototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(defaultElementType, "Point");

            var prototype = DepictionAccess.TemplateLibrary.FindElementTemplate(defaultTypeString);
            importFromCsvViewModel.Prototype = prototype;
            if (defaultTypeString.Equals(DepictionStringService.AutoDetectElementString))
            {
                importFromCsvViewModel.UserTypeSelection = CSVElementTypeSelection.CSVDefined;
            }
            else
            {
                importFromCsvViewModel.UserTypeSelection = CSVElementTypeSelection.UserDefined;
            }

            view = new ImportFromCsvView();

            view.DataContext = importFromCsvViewModel;
            importFromCsvViewModel.RequestAndAddElements += importFromCsvViewModel_RequestAndAddElements;
            var result = view.ShowDialog();
            //until the conversion from customcontrolframe is complete
            //            importFromCsvViewModel_RequestAndAddElements(importFromCsvViewModel);
            //            var window = (Application.Current.MainWindow as IDepictionMainWindow);
            //            if (window == null) return;
            //            window.AddContentControlToMapCanvas(view);
            //            importFromCsvViewModel.IsDialogVisible = true;
            //            view.CenterWidthAndGivenTop(100);
        }


        public void Dispose()
        {
//            view.Close();
//            view = null;
        }

        
        void importFromCsvViewModel_RequestAndAddElements(ImportFromCsvViewModel csvImporterViewModel)
        {
            var name = string.Format("CSV file reader. File : {0}", csvImporterViewModel.FileInfo.Name);
            var csvReadingService = new CSVFileReadingService();

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(csvReadingService);
            csvReadingService.UpdateStatusReport(name);
            var parameters = new Dictionary<string, object>();
            if (DepictionAccess.DStory != null)
            {
                var firstRegion = DepictionAccess.DStory.AllRegions.FirstOrDefault();
                if(firstRegion != null)
                {
                    parameters.Add("Area", firstRegion);
                }
            }
            parameters.Add("ViewModel", csvImporterViewModel);
            csvReadingService.StartBackgroundService(parameters);
            view.Close();
            view = null;
        }

        public List<string[]> FindFirstFiveRowsAndHeaders(string csvFile, out string[] headers)
        {
            var firstFiveRows = new List<string[]>();
           
            try
            {
                using (var streamReader = new StreamReader(new FileStream(csvFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    using (var reader = new CsvReader(streamReader, true){MissingFieldAction = MissingFieldAction.ReplaceByEmpty})
                    {
                        headers = reader.GetFieldHeaders();
                        if (headers == null || headers.Length == 0)
                            throw new CSVMissingHeaderException(csvFile);

                        MissingFieldCsvException missingFieldCsvException = null;
                        int missingFieldsCount = 0;
                        int i = 0;
                        while (reader.ReadNextRecord() && i < 5)
                        {
                            try
                            {
                                string[] row = new string[headers.Length];
                                for (int j = 0; j < headers.Length; j++)
                                {
                                    row[j] = reader[j].Trim();
                                }
                                firstFiveRows.Add(row);
                            }
                            catch (MissingFieldCsvException ex)
                            {
                                missingFieldCsvException = ex;
                                missingFieldsCount++;
                            }
                            catch (MalformedCsvException ex)
                            {
//                                DepictionAccess.NotificationService.DisplayMessageString(
//                                    string.Format("Problem reading a line from csv file \"{0}\":\n{1}", csvFile,
//                                                  ex.Message));
                            }
                            i++;
                        }
                        if (missingFieldCsvException != null)
                        {
//                            DepictionAccess.NotificationService.DisplayMessageString(
//                                string.Format("{0} records in the file \"{1}\" are missing fields:\n{2}",
//                                              missingFieldsCount, csvFile, missingFieldCsvException.Message));
                        }
                    }
                }
            }catch(Exception ex)
            {
                headers = null;
//                DepictionAccess.NotificationService.DisplayMessageString(
//                                string.Format("File error : {0}", ex.Message));
                return null;
            }

            return firstFiveRows;
        }
    }
}