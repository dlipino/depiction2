using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using Depiction.CSVExtension.Exceptions;
using Depiction.Externals.Csv;
using Depiction2.API;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.CsvExtension.Externals;
using Depiction2.CsvExtension.Externals.Exceptions;
using Depiction2.CsvExtension.ViewModels;

namespace Depiction2.CsvExtension.Models
{
    public class CSVFileReadingService : BaseDepictionBackgroundThreadOperation
    {
        private const string positionKey = PropertyKeys.geoPositionKey;
        private IElementTemplate elementScaffold;
        private string propIdName;
        #region public methods

        public IEnumerable<Dictionary<string, object>> ReadCSVFileAndCreatePrototypes(string fileName, GeoLocationInfoToPropertyHolder locationPropertyHolder, CSVLocationModeType locationMode,
                                                                             bool cropToRegion, ICartRect depictionRegion, IDepictionGeocodeService geocoder)
        {
            //The read happens so fast the user doesn't get to see visual updates from the file read
            Thread.Sleep(1000);
            var rawCsvValues = GetValuesFromCsvFile(fileName,  0);
            if (rawCsvValues.Count == 0)
                throw new CSVMissingDataException(fileName);
            if (ServiceStopRequested) return null;
            var allValues = new List<Dictionary<string, object>>();
            foreach(var csvValue in rawCsvValues)
            {
                allValues.Add(ConvertStringDictionaryToCorrectTypes(csvValue));
            }
            GeocodeCSVPrototypes(locationMode, locationPropertyHolder, allValues, geocoder);

            if (ServiceStopRequested) return null;

            List<Dictionary<string, object>> finalPrototypes;
            var notInRegionCount = 0;
            var notGeoCoded = 0;
            if (cropToRegion && depictionRegion != null)
            {
                var inRegion = new List<Dictionary<string, object>>();
                //Something is funky, this doesn't work very well with richards lummi example
                foreach (var prototype in allValues)
                {
                    if (prototype.ContainsKey(positionKey))
                    {
                        var posObj = prototype[positionKey];
                        if (posObj != null)
                        {
                            var posPoint = (Point)posObj;
                            if (depictionRegion.ContainsPoint(posPoint))
                            {
                                inRegion.Add(prototype);
                            }
                        }
                        else
                        {
                            notInRegionCount++;
                            notGeoCoded++;
                        }
                    }
                }

                finalPrototypes = inRegion;
                //                var message =
                //                    new DepictionMessage(string.Format("{0} elements were not contained in depiction region.",
                //                                                       notInRegionCount), 4);
                //                DepictionAccess.NotificationService.DisplayMessage(message);
            }
            else
            {
                finalPrototypes = allValues;
            }
            if (finalPrototypes.Count != 0)
            {
                UpdateStatusReport(string.Format("Loading {0} geo-located elements and {1} elements with no location into the depiction", finalPrototypes.Count, notGeoCoded));
            }
            //The read happens so fast the user doesn't get any input
            Thread.Sleep(1000);
            return finalPrototypes;
        }

        #endregion

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }
        //hmmm i guess you don't really need the args since the needed values can come from the class itself.
        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var csvImporterViewModel = parameters["ViewModel"] as ImportFromCsvViewModel;
            if (csvImporterViewModel == null) return null;
            var area = parameters["Area"] as ICartRect;
            propIdName = csvImporterViewModel.PropEid;
            var propId = PropertyKeys.elementTypeKey;
            if(parameters.ContainsKey(propId))
            {
                propIdName = parameters[propId].ToString();
            }
            elementScaffold = csvImporterViewModel.Prototype;
//            var elemType = TemplatePropertyKeys.elementTypeKey;
//            if (parameters.ContainsKey(elemType))
//            {
//                elementScaffold = parameters[elemType] as IElementTemplate;
//            }
            return ReadCSVFileAndCreatePrototypes(csvImporterViewModel.FileInfo.FullName, csvImporterViewModel.LocationPropertyHolder,
                                                  csvImporterViewModel.LocationStringMode, csvImporterViewModel.CropDataToRegion,  area,
                                                   DepictionAccess.GeocoderService);
        }

        protected override void ServiceComplete(object args)
        {
            Console.WriteLine("Starting the update");
            var startTime = DateTime.Now;
//            var scaffold = DepictionAccess.TemplateLibrary.FindElementTemplate(elementScaffold);
            var geomAndProps = args as List<Dictionary<string, object>>;
            if (geomAndProps == null) return;
            var story = DepictionAccess.DStory;
            if (story == null) return;
            story.UpdateRepo(geomAndProps, propIdName, elementScaffold);

            var endtime = DateTime.Now;
            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);

        }

        #endregion

        #region element getting methods
        public List<Dictionary<string, string>> GetValuesFromCsvFile(string csvFile, int maxRecordsToGet)
        {
            var allRecords = new List<Dictionary<string, string>>();
            double totalCount = BruteForceCount(csvFile);
            if (maxRecordsToGet != 0) totalCount = maxRecordsToGet;
            try
            {
                using (var streamReader = new StreamReader(new FileStream(csvFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    using (var reader = new CsvReader(streamReader, true) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
                    {
                        string[] headers = reader.GetFieldHeaders();
                        if (headers == null || headers.Length == 0)
                            throw new CSVMissingHeaderException(csvFile);

                        MissingFieldCsvException missingFieldCsvException = null;
                        int missingFieldsCount = 0;
                        int recordCount = 0;
                        while (reader.ReadNextRecord())
                        {
                            if (ServiceStopRequested) break;
                            recordCount++;
                            UpdateStatusReport(string.Format("Reading element {0} of {1}", recordCount, totalCount));
                            if (recordCount >= maxRecordsToGet && maxRecordsToGet > 0) break;
                            var elementBase = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
                            try
                            {
                                
                                for (int i = 0; i < headers.Length; i++)
                                {
                                    elementBase.Add(headers[i], reader[i].Trim());

                                }
                                if (elementBase.Count !=0)
                                {
                                    //                                    elementBase.Tags.Add(Tags.DefaultFileTag + ":" + Path.GetFileName(csvFile));
                                    allRecords.Add(elementBase);
                                }
                            }
                            catch (MissingFieldCsvException ex)
                            {
                                missingFieldCsvException = ex;
                                missingFieldsCount++;
                            }
                            catch (MalformedCsvException ex)
                            {
                                //                                DepictionAccess.NotificationService.DisplayMessageString(
                                //                                    string.Format("Problem reading a line from csv file \"{0}\":\n{1}",
                                //                                                  csvFile, ex.Message));
                            }

                        }
                        if (missingFieldCsvException != null)
                        {
                            //                            DepictionAccess.NotificationService.DisplayMessageString(
                            //                                string.Format("{0} records in the file \"{1}\" are missing fields:\n{2}",
                            //                                              missingFieldsCount, csvFile, missingFieldCsvException.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //                DepictionAccess.NotificationService.DisplayMessageString(
                //                                string.Format("File error : {0}", ex.Message));
                return null;
            }
            if (allRecords.Count == 0)
                throw new CSVMissingDataException(csvFile);

            return allRecords;
        }

        public Dictionary<string,object> ConvertStringDictionaryToCorrectTypes(Dictionary<string, string> inputValues)
        {
            var convertedDict = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            //Normall this would convert things, but for now just turn evertying to objects
            foreach(var keypair in inputValues)
            {
                convertedDict.Add(keypair.Key,keypair.Value);
            }
            return convertedDict;
        } 
        public string[] FindHeaders(string csvFile)
        {
            string[] headers;

            using (var streamReader = new StreamReader(File.OpenRead(csvFile)))
            {
                using (var reader = new CsvReader(streamReader, true) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
                {
                    headers = reader.GetFieldHeaders();
                }
            }
            return headers;
        }

        #endregion

        #region random helpers


        private void GeocodeCSVPrototypes(CSVLocationModeType locationStringMode, GeoLocationInfoToPropertyHolder propertyHolder,
                                          IEnumerable<Dictionary<string, object>> prototypes, IDepictionGeocodeService geoCodingService)
        {
            //Things don't go so well if geocoder is null
            var propList = prototypes.ToList();
            double totalCount = propList.Count();
            int count = 0;
            var locatGeocodeService = geoCodingService;

            if (locatGeocodeService == null)
            {
                locatGeocodeService = DepictionAccess.GeocoderService;
            }
            //            var availableGeocoders = locatGeocodeService.FindAGeocodersForRegion(string.Empty);
            var geocodedCount = 0;
            var geocodeLimit = 1000;

            var useWebForGeocoding = true;
            foreach (var propDictionary in propList)
            {

                if (geocodedCount >= geocodeLimit) useWebForGeocoding = false;
                if (ServiceStopRequested) break;
                count++;
                UpdateStatusReport(string.Format("Geocoding element {0} of {1}", count, totalCount));
                var inputFields = new GeocodeInput(); ;
                try
                {
                    dynamic geocodeResults = null;
                    if (locationStringMode.Equals(CSVLocationModeType.LatitudeLongitudeSingleField))
                    {
                        if (propDictionary.ContainsKey(propertyHolder.LatitudeLongitude))
                        {
                            inputFields.CompleteAddress = propDictionary[propertyHolder.LatitudeLongitude].ToString();
                        }
                    }
                    else if (locationStringMode.Equals(CSVLocationModeType.LatitudeLongitudeSeparateFields))
                    {
                        if (propDictionary.ContainsKey(propertyHolder.Latitude) && propDictionary.ContainsKey(propertyHolder.Longitude))
                        {
                            inputFields.CompleteAddress = string.Format("{0}, {1}", propDictionary[propertyHolder.Latitude], propDictionary[propertyHolder.Longitude]);
                        }
                    }
                    else if ((geocodeResults == null || !geocodeResults.IsValid) &&
                             locationStringMode.Equals(CSVLocationModeType.AddressSingleField))
                    {
                        string address = string.Empty;
                        if (propDictionary.ContainsKey(propertyHolder.AddressSingleField))
                        {
                            inputFields.CompleteAddress = propDictionary[propertyHolder.AddressSingleField].ToString();
                        }
                    }
                    else if ((geocodeResults == null || !geocodeResults.IsValid) &&
                             (locationStringMode.Equals(CSVLocationModeType.AddressMultipleFields)))
                    {
                        object streetAddress = string.Empty,
                               city = string.Empty,
                               state = string.Empty,
                               zipCode = string.Empty,
                               country = string.Empty;
                        propDictionary.TryGetValue(propertyHolder.StreetAddress, out streetAddress);
                        propDictionary.TryGetValue(propertyHolder.City, out city);
                        propDictionary.TryGetValue(propertyHolder.State, out state);
                        propDictionary.TryGetValue(propertyHolder.ZipCode, out zipCode);
                        propDictionary.TryGetValue(propertyHolder.Country, out country);
                        inputFields.StreetAddress = streetAddress.ToString();
                        inputFields.City = city.ToString();
                        inputFields.State = state.ToString();
                        inputFields.ZipCode = zipCode.ToString();
                        inputFields.Country = country.ToString();

                    }
                    var result = geoCodingService.GeocodeInput(inputFields, true);
                    if (result != null)
                    {
                        geocodedCount++;

                        if (!propDictionary.ContainsKey(positionKey))
                        {
                            propDictionary.Add(positionKey, result);
                        }
                    }

                    //                    if (geocodeResults != null && geocodeResults.Position != null && geocodeResults.Position.IsValid)
                    //                    {
                    //                        propDictionary.SetInitialPositionAndZOI(geocodeResults.Position, null);
                    //                        if (!geocodeResults.Accurate)
                    //                        {
                    //                            propDictionary.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("IconBorderColor", "IconBorderColor", Colors.Red));
                    //                            propDictionary.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("IconBorderShape", "IconBorderShape", DepictionIconBorderShape.Circle));
                    //                        }
                    //                    }
                }

                catch (Exception e)
                {
                    //                    var message = string.Format("Geocoding error has for {0} has occurred: {1}", propDictionary.ElementType, e.Message);
                    //                    DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                }
            }

            if (geocodedCount > geocodeLimit)
            {
                //                var message = string.Format("The geocoding limit of {0} was reached. A possible {1} elements were not geocoded", geocodeLimit, count - geocodeLimit);
                //                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
            }
            //The save is normally done when the app closes, but for this, why not just write it out a bit early
            try
            {
                //                locatGeocodeService.SaveGeneralGeoCodeCache();
            }
            catch { }
        }

        private int BruteForceCount(string csvFileName)
        {
            int count = 0;
            using (var streamReader = new StreamReader(new FileStream(csvFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var reader = new CsvReader(streamReader, true) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
                {
                    while (reader.ReadNextRecord())
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        #endregion
    }
}