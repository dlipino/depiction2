﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Depiction2.CsvExtension.ViewModels;

namespace Depiction2.CsvExtension.Views
{
    /// <summary>
    /// Interaction logic for ImportFromCsvView.xaml
    /// </summary>
    public partial class ImportFromCsvView
    {
        private List<GridViewColumn> gridViewColumns = new List<GridViewColumn>();
        public ImportFromCsvView()
        {
            InitializeComponent();
            DataContextChanged += ImportFromCsvView_DataContextChanged;
            Loaded += ImportFromCsvView_Loaded;
        }

        void ImportFromCsvView_Loaded(object sender, RoutedEventArgs e)
        {
            var gridView = PART_SampleGridView;
            if (gridView != null)
            {
                foreach (var column in gridViewColumns)
                {
                    gridView.Columns.Add(column);
                }
            }
            Loaded -= ImportFromCsvView_Loaded;
        }

        void ImportFromCsvView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var importFromCsvViewModelOld = e.OldValue as ImportFromCsvViewModel;//new and not used?

            var importFromCsvViewModel = e.NewValue as ImportFromCsvViewModel;
            if (importFromCsvViewModel == null) return;
//            importFromCsvViewModel.RequestClose += importFromCsvViewModel_RequestClose;

            //DataTable table = new DataTable();
            //add columns to grid and table
            var elemName = "Auto";
            if (importFromCsvViewModel.Prototype != null)
            {
                elemName = importFromCsvViewModel.Prototype.ElementType;

            }
            foreach (var elem in importFromCsvViewModel.AvailableElements)
            {
                if (elem == null)
                {
                    
                }else if ( elem.ElementType.Contains(elemName))
                {
                    importFromCsvViewModel.Prototype = elem;
                    //                    elementTypeCombobox.SelectedItem = elem;
                    //                    if (!elemName.Equals("Depiction.Plugin.AutoDetect"))
                    //                    {
                    //                        //UserTypeSelection = CSVElementTypeSelection.CSVDefined;
                    //                        //importFromCsvViewModel.UserSelectedType = true;
                    //                        //                        defaultTypRadioButton.IsChecked = false;
                    //                        //                        userTypeRadioButton.IsChecked = true;
                    //                    }
                    break;
                }
            }

            foreach (var columnName in importFromCsvViewModel.Properties)
            {
                //var dataColumn = new DataColumn(columnName);
                //dataColumn.DataType = typeof (string);
                //DataTable.Columns.Add(dataColumn);
                var column = new GridViewColumn();
                column.Header = columnName;
                column.DisplayMemberBinding = new Binding(columnName);
                //Hack so that a blank space can be added
                if (!string.IsNullOrEmpty(columnName))
                {
                    gridViewColumns.Add(column);
                }
                //                        gridView.Columns.Add(column);
            }
        }

        //        void importFromCsvViewModel_RequestClose()
        //        {
        //            IsShown = false;
        //        }
    }
}