﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Service.Tiling;
using Depiction2.Tilers.Base;

namespace Depiction2.Tilers.OpenStreetMap
{
  [TilerExtensionMetadata(Name = "OpenStreetMapTileImporter", Description = "Street Map (OpenStreetMap) tile importer", DisplayName = "Street Map (OpenStreetMap)", TilerType = TileImageTypes.Street)]
    public class OpenStreetMapTiler : OpenStreetMapTilerBase
    {
        public override string URL { get { return "http://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png"; } }
//        public override string DisplayName { get { return "Street Map (OpenStreetMap)"; } }
        public override string CacheLocation { get { return "OpenStreetMapService"; } }
//        public override string LegacyImporterName { get { return "OpenStreetMapTileImporter"; } }


//        public override TileImageTypes TileImageType { get { return TileImageTypes.Street; } }
        protected override bool UseLetters { get { return true; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(CacheLocation);
                return cacheService;
            }
        }
    }




  
}
