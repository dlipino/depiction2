﻿using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Service.Tiling;
using Depiction2.Tilers.Base;

namespace Depiction2.Tilers.MapQuest
{
    [TilerExtensionMetadata(Name = "MapQuestOpenStreetTileImporter", Description = "Street map (MapQuest) tile importer. For more go to http://open.mapquest.com",
        DisplayName = "Street Map (MapQuest)", TilerType = TileImageTypes.Street)]
    public class MapQuestOpenStreetMapTiler : OpenStreetMapTilerBase
    {

        public override string URL { get { return "http://otile{0}.mqcdn.com/tiles/1.0.0/osm/{1}/{2}/{3}.jpg"; } }
        //        public override string DisplayName { get { return "Street Map (MapQuest)"; } }
        public override string CacheLocation { get { return "MapQuestOpenStreetService"; } }
        //        public override string LegacyImporterName { get { return "MapQuestOpenStreetTileImporter"; } }
        //        public override TileImageTypes TileImageType { get { return TileImageTypes.Street; } }
        protected override bool UseLetters { get { return false; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(CacheLocation);
                return cacheService;
            }
        }
        //public static string Name { get { return "Street Map (OpenStreetMap)"; } }
    }
    [TilerExtensionMetadata(Name = "MapQuestOpenAerialTileImporter", Description = "Imagery (MapQuest) tile importer. For more go to http://open.mapquest.com",
         DisplayName = "Imagery (MapQuest)", TilerType = TileImageTypes.Satellite)]
    public sealed class MapQuestTiler : OpenStreetMapTilerBase
    {
        private const string MapquestURL = "http://oatile{0}.mqcdn.com/tiles/1.0.0/sat/{1}/{2}/{3}.png";
        public override string URL { get { return MapquestURL; } }
        //      public override TileImageTypes TileImageType { get { return TileImageTypes.Satellite; } }
        //      public override string DisplayName { get { return "Imagery (MapQuest)"; } }
        public override string CacheLocation { get { return "MapQuestOpenAerialService"; } }
        //      public override string LegacyImporterName { get { return "MapQuestOpenAerialTileImporter"; } }
        protected override bool UseLetters { get { return false; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(CacheLocation);
                return cacheService;
            }
        }
    }
}