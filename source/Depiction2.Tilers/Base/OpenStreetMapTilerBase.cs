﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Geo;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.Tilers.Base
{
    public abstract class OpenStreetMapTilerBase : ITilerExtension
    {
        public abstract string URL { get; }
//        public abstract string DisplayName { get; }
        public abstract string CacheLocation { get; }
//        public abstract TileImageTypes TileImageType { get; }
//        public abstract string LegacyImporterName { get; }
        protected abstract bool UseLetters { get; }
        public int PixelWidth { get { return 256; } }
        public bool DoesOwnCaching { get { return false; } }
        protected abstract TileCacheService CacheService { get; }

        public int LongitudeToColAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            int col = (int)(Math.Floor((latLong.Longitude + 180.0) / 360.0 * Math.Pow(2.0, zoom)));
            if (col >= (int)Math.Pow(2, zoom))
                col = (int)Math.Pow(2, zoom) - 1;
            if (col <= 0)
                col = 0;
            return col;
        }

        public int LatitudeToRowAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            int row = (int)(Math.Floor((1.0 - Math.Log(Math.Tan(latLong.Latitude * Math.PI / 180.0) + 1.0 / Math.Cos(latLong.Latitude * Math.PI / 180.0)) / Math.PI) / 2.0 * Math.Pow(2.0, zoom)));
            if (row >= (int)Math.Pow(2, zoom))
                row = (int)Math.Pow(2, zoom) - 1;
            if (row <= 0)
                row = 0;
            return row;
        }

        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            return col / Math.Pow(2.0, zoomLevel) * 360.0 - 180;
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            double n = Math.PI - 2.0 * Math.PI * row / Math.Pow(2.0, zoom);
            return 180.0 / Math.PI * Math.Atan(0.5 * (Math.Exp(n) - Math.Exp(-n)));
        }

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            double widthInDegrees = TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel) - TileColToTopLeftLong(tileToGet.Column, zoomLevel);
            double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);
            //
            var uri = new Uri(GetTileUrl(tileToGet.Row, tileToGet.Column, zoomLevel));
            var tile = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                            uri,
                            new LatitudeLongitudeSimple(TileRowToTopLeftLat(tileToGet.Row, zoomLevel), TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                            new LatitudeLongitudeSimple(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
            return tile;
        }

        private readonly Random random = new Random();

        public string GetTileUrl(int row, int column, int zoom)
        {
            int next = random.Next(1, 4);
            object nextObj;
            if (UseLetters)
                nextObj = Convert.ToChar(next + 96);
            else
                nextObj = next;

            return string.Format(CultureInfo.InvariantCulture, URL, nextObj, zoom, column, row);
        }

        public int GetZoomLevel(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
//            var cart = new CartRect(boundingBox, false);
            var boxXMin = boundingBox.Left;
            var boxXMax = boundingBox.Right;
            int i;
            for (i = 1; i < maxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boxXMin - boxXMax) * (1 << i) / 360D;
                if (tilesAcross > minTilesAcross) break;
            }

            return i;
        }

        #region methods

        public IList<TileModel> GetTiles(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
//            var cart = new CartRect(boundingBox,true);

            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.Top, boundingBox.Right), zoomLevel);
            int lastRow = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.Bottom, boundingBox.Left), zoomLevel);
            int col = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.Top, boundingBox.Left), zoomLevel);
            int lastCol = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.Bottom, boundingBox.Right), zoomLevel);
//            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
//            int row = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.BottomRight.Y, boundingBox.BottomRight.X), zoomLevel);
//            int col = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.TopLeft.Y, boundingBox.TopLeft.X), zoomLevel);
//            int lastCol = LongitudeToColAtZoom(new LatitudeLongitudeSimple(boundingBox.BottomRight.Y, boundingBox.BottomRight.X), zoomLevel);
//            int lastRow = LatitudeToRowAtZoom(new LatitudeLongitudeSimple(boundingBox.TopLeft.Y, boundingBox.TopLeft.X), zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);


            foreach (var tileToGet in tilesToGet)
            {
                var tile = GetTileModel(tileToGet, zoomLevel);

                tiles.Add(tile);
            }

            return tiles;
        }


        //<summary>
        //Returns the list of tiles to get ordered in increasing distance from the center
        //of the area being tiled.
        //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles =
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }

        #endregion

        public void Dispose()
        {

        }
    }
}