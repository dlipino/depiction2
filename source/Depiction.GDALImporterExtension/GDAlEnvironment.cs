﻿using System;
using System.IO;
using System.Reflection;
using OSGeo.GDAL;

namespace Depiction.GDALImporterExtension
{
    //GDAL is used for reading images http://www.gdal.org/formats_list.html
    public class GDALEnvironment
    {
        private static string origPath = string.Empty;
        public static void SetGDALEnvironment()
        {
            SetGDALEnvironmentVariables();
            Gdal.AllRegister();
        }
        //Option to unregister? for testing?
        private static bool SetGDALEnvironmentVariables()
        {
            //                string path = AppDomain.CurrentDomain.BaseDirectory;
            var location = Assembly.GetExecutingAssembly().Location;
            string path = Path.GetDirectoryName(location);
            if (string.IsNullOrEmpty(path)) return false;

            //This makes it so the proj.dll is found
            origPath = Environment.GetEnvironmentVariable("PATH");
            //String updatedPath = gdalPathLocation + @"\bin;" + gdalPath + @"\bin\gdal\python\osgeo;" + gdalPath + @"\bin\proj\apps;" + gdalPath + @"\bin\gdal\plugins;" + gdalPath + @"\bin\gdal\plugins-optional;" + gdalPath + @"\bin\gdal\apps;" + gdalPath + @"\bin\ms\apps;" + gdalPath + @"\bin\gdal\csharp;" + gdalPath + @"\bin\ms\csharp;" + gdalPath + @"\bin\curl";
            String updatedPath = path;

            if (origPath != null && !origPath.Contains(updatedPath))
            {
                updatedPath = origPath + ";" + updatedPath;
                Environment.SetEnvironmentVariable("PATH", updatedPath);
            }

            Gdal.SetConfigOption("GDAL_DATA", Path.Combine(path, @"gdal-data"));
            Gdal.SetConfigOption("GDAL_DRIVER_PATH", Path.Combine(path, @"gdal-plugins"));
            Gdal.SetConfigOption("PROJ_LIB", Path.Combine(path, @"proj\SHARE"));
            Gdal.SetConfigOption("GEOTIFF_CSV", Path.Combine(path, @"gdal-data"));

            SetValueNewVariable("GEOTIFF_CSV", Path.Combine(path, @"gdal-data"));
            SetValueNewVariable("GDAL_DRIVER_PATH", Path.Combine(path, @"gdal-plugins"));
            //Is this one even used by GDAL, it was not in the build for a while and things still seemed to work
            //I kind of wonder how these files are used
            SetValueNewVariable("PROJ_LIB", Path.Combine(path, @"proj\SHARE"));
            return true;
        }

        private static void SetValueNewVariable(string name, string value)
        {
            //if (Environment.GetEnvironmentVariable(name) == null)//What was this removed?
            Environment.SetEnvironmentVariable(name, value);
        }
        public static void UnsetGdalEnvironment()
        {
            //Not sure if the is even needed
            if (!string.IsNullOrEmpty(origPath))
            {
                Environment.SetEnvironmentVariable("PATH", origPath);
            }
            //TODO it would be really nice to be able to do this for tests
        }
    }


}