﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction.ProjectionSystem.DepictionProjection;
using Depiction2.API;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Depiction2.GeoWrapper;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using Depiction2.Utilities.Drawing;
using Depiction2.Utilities.Memory;
using OSGeo.OGR;

namespace Depiction.GDALImporterExtension
{
    [DepictionImporterMetadata(Author = "Depiction Inc.", AddonPackage="Default", 
        Description = "Extension for adding OGR compliant files.",
        DisplayName = "OGR Generic Importer", Name="OGRGenericImporter")]
    public class OGRImporterExtension : IDepictionImporterExtension
    {
        private GDALAndOGRImporterWindow settingsWindow;
        private const string all = "All";
        private const string esriShape = "ESRI Shapefile";

        private Dictionary<string, List<string>> CodeExtensionDictionary = new Dictionary<string, List<string>>
                                                                               {
                                                                                   {all,new List<string>{".*"}},
                                                                                   {esriShape, new List<string> {".shp"}}
                                                                               };

        public void InitiateImporter()
        {
            OGREnvironment.SetOGREnvironment();

        }
        #region destruction, although i think

        public void Dispose()
        {
            if (settingsWindow != null) settingsWindow.Close();
        }

        ~OGRImporterExtension()
        {
            settingsWindow = null;
        }

        #endregion

        #region helper methods that are testable

        #region to string

        private static List<string> GetStringRepresentationFromGeom(Geometry mainGeom)
        {
            var type = mainGeom.GetGeometryType();
            List<string> stringVal = new List<string>();
            string[] things = new string[0];
            //http://www.gdal.org/ogr/classOGRGeometry.html
            if (type.Equals(wkbGeometryType.wkbPoint))//i guess multipoint can be thrown in too
            {
                stringVal.Add(mainGeom.ExportToJson(things));
            }
            else if (type.Equals(wkbGeometryType.wkbPolygon) || type.Equals(wkbGeometryType.wkbMultiPolygon))
            {
                var pointCount = mainGeom.GetPointCount();
                var geomCount = mainGeom.GetGeometryCount();
                //assume that geomCount and pointCount are mutually exclusive on zero
                if (pointCount != 0)
                {
                    stringVal.Add(mainGeom.ExportToJson(things));
                }
                else
                {
                    //no individual points so this geometry is made of of lots of other geometries, hopefully
                    for (int i = 0; i < geomCount; i++)
                    {
                        stringVal.AddRange(GetStringRepresentationFromGeom(mainGeom.GetGeometryRef(i)));
                    }
                }
            }
            else if (type.Equals(wkbGeometryType.wkbLineString) || type.Equals(wkbGeometryType.wkbMultiLineString))
            {
                var pointCount = mainGeom.GetPointCount();
                var geomCount = mainGeom.GetGeometryCount();
                //assume that geomCount and pointCount are mutually exclusive on zero
                if (pointCount != 0)
                {
                    stringVal.Add(mainGeom.ExportToJson(things));
                }
                else
                {
                    //no individual points so this geometry is made of of lots of other geometries, hopefully
                    for (int i = 0; i < geomCount; i++)
                    {
                        stringVal.AddRange(GetStringRepresentationFromGeom(mainGeom.GetGeometryRef(i)));
                    }
                }
            }
            return stringVal;
        }

        #endregion
        
        public static List<GeometryGdalWrap> CreateGeometryWrapsFromFile(string fileName, out string projSystem)
        {
            DataSource dataSource = null;
            var geometries = new List<GeometryGdalWrap>();
            try
            {
                dataSource = Ogr.Open(fileName, 0);
            }catch
            {
                //log an error
                projSystem = null;
                return null;
            }
            var layerCount = dataSource.GetLayerCount();
            projSystem = string.Empty;
            for (int i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                //                DepictionProjectionSystem.SetProjectionSystem(layer.GetSpatialRef().__str__());
                projSystem = layer.GetSpatialRef().__str__();
                //More than one layers with geometries?
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var mainGeom = nextFeature.GetGeometryRef();
                    geometries.Add(new GeometryGdalWrap(mainGeom));

                }
            }
            return geometries;
        }
       
        public static List<EnhancedPointListWithChildren> FillPointsFromFile(string fileName, bool returnPoints, bool printDetails)
        {
            var start = MemUtil.GetProcessMem();

            var dataSource = Ogr.Open(fileName, 0);

            var returnList = new List<EnhancedPointListWithChildren>();
            if (dataSource == null) return returnList;
            int geometryCounter = 0;
            var featureCounter = 0;
            var layerCount = dataSource.GetLayerCount();
            for (int i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                DepictionProjectionSystem.SetProjectionSystem(layer.GetSpatialRef().__str__());

                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var mainGeom = nextFeature.GetGeometryRef();

                    GdalExtensionHelpers.GetPointListFromGeom(mainGeom, ref returnList, null);
                    featureCounter++;
                }
            }

            var end = MemUtil.GetProcessMem();
            Console.WriteLine(string.Format("Mem used {0}", end - start));
            var results = string.Format("Feature count {0}, geometry count {1}", featureCounter, geometryCounter);
            Console.WriteLine(results);
            if (printDetails)
            {
                var eCount = 0;
                foreach (var t in returnList)
                {
                    if (t.IsEmpty()) eCount++;
                }
                var pointRes = string.Format("point count {0}, empty count {1}", returnList.Count, eCount);
                Console.WriteLine(pointRes);
            }
            dataSource.Dispose();
            return returnList;
        }

        #endregion

        public void ImportElements(string fileName, string elementType)
        {
            var start = MemUtil.GetProcessMem();
            var startTime = DateTime.Now;
            var endtime = DateTime.Now;
            var elementList = CreateElementsFromFile(fileName);

            endtime = DateTime.Now;
            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);
            if (elementList == null)
            {
                //Give some error
                return;
            }
            startTime = DateTime.Now;
            //hackity hack, still don't know how thigns should/are getting connected, because to many things were put together piece wise
            if (DepictionAccess.DStory == null)
                return;
            DepictionAccess.DStory.AddElements(elementList);
//            DepictionAccess.DStory.MainDisplayer.SetLiveElements(DepictionAccess.DStory.AllElements);
            DepictionAccess.DStory.MainDisplayer.AddDisplayElementsWithMatchingIds(elementList.Select(t => t.ElementId).ToList());
            //            allPointLists = null;


            endtime = DateTime.Now;
            elapsedTime = string.Format("It took {0} to create elements", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);
            Console.WriteLine("Mem after element create");
            MemUtil.GetProcessMem();
            startTime = DateTime.Now;
            //the draw thing
            endtime = DateTime.Now;
            elapsedTime = string.Format("It took {0} to send elements to binding ", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);
            Console.WriteLine("Mem after element to bindings");
            var end = MemUtil.GetProcessMem();
            Console.WriteLine(string.Format("Mem used full set {0}", end - start));
        }

        public List<IElement> CreateElementsFromFile(string elementFile)
        {
            //Not sure what is up with this
            if (!File.Exists(elementFile))
            {
                elementFile = elementFile.Remove(0, 2);
            }
            if (!File.Exists(elementFile)) return null;

            string projSystem;
            var geometries = CreateGeometryWrapsFromFile(elementFile, out projSystem);
            if (geometries == null) return null;
            var elementList = new List<IElement>();
            foreach (var geometry in geometries)
            {
                elementList.Add(new DepictionElement { ElementGeometry = geometry });
            }
            return elementList;
        }

        public Window GetSettingsWindow()
        {
            //            var fileName = @"D:\DepictionData\WorldOutLine\countries.shp";
            //            var fileName = @"D:\DepictionData\\LummiTaxParcels\WC_AR_TaxParcel.shp";
            //                        var fileName = @"D:\DepictionData\us_national_borders\nationalp010g.shp";
            //                        var fileName = @"D:\DepictionData\us_state_bounds\state_bounds.shp";
            //            ImportElements(fileName);
            //            return null;

            settingsWindow = new GDALAndOGRImporterWindow();//This used to cause the application to not terminate adding the dispose fixed the issue
            settingsWindow.ConnectedImporter = this;
            //Vector formats http://www.gdal.org/ogr/ogr_formats.html
            //var ogrDriverCount = Ogr.GetDriverCount();
            //for (int i = 0; i < ogrDriverCount; i++)
            //{
            //    Console.WriteLine(Ogr.GetDriver(i).GetName());
            //}
            return settingsWindow;
        }
    }
}
//        public static List<string> CreateStringFromFile(string fileName, bool printDetails)
//        {
//            var dataSource = Ogr.Open(fileName, 0);
//
//            var returnList = new List<string>();
//
//            int geometryCounter = 0;
//            var featureCounter = 0;
//            var layerCount = dataSource.GetLayerCount();
//            for (int i = 0; i < layerCount; i++)
//            {
//                var extent = new Envelope();
//                var layer = dataSource.GetLayerByIndex(i);
//                Feature nextFeature;
//                var extentString = string.Empty;
//                //                if (printDetails)
//                //                {
//                layer.GetExtent(extent, 0);
//                Console.WriteLine("Extent layer");
//                extentString = string.Format("Max x {0}, Min x {1}, Max y {2}, Min y {3}", extent.MaxX,
//                                                     extent.MinY, extent.MaxY, extent.MinY);
//                Console.WriteLine(extentString);
//                //                }
//
//                while ((nextFeature = layer.GetNextFeature()) != null)
//                {
//                    var mainGeom = nextFeature.GetGeometryRef();
//
//                    var srGeom = mainGeom.GetSpatialReference();
//                    //TODO deal with no spatial reference system better
//                    if (srGeom != null)
//                    {
//                        if (printDetails)
//                        {
//                            Console.WriteLine("Base SpatialReference " + srGeom.__str__());
//                        }
//                        //can be done before or after
//                        //                        ModifySpatialReference(mainGeom);
//                    }
//
//                    returnList.AddRange(GetStringRepresentationFromGeom(mainGeom));
//                    featureCounter++;
//                }
//            }
//            return returnList;
//        }