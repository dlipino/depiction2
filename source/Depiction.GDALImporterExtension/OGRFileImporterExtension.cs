﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Abstract;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using Depiction2.Utilities.Memory;
using OSGeo.OGR;
using OSGeo.OSR;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction.GDALImporterExtension
{

    [DepictionFileImporterMetadata(new[] { ".shp" }, DisplayName = "OGR File Importer", Name = "OGRFileImporter",
         Description = "OGR Vector File reader.")]
    public class OGRFileImporterExtension : IDepictionImporterExtension
    {
        private GDALAndOGRImporterWindow settingsWindow;

        #region testing to make sure this thing is allows depiction to exit

        public void Dispose()
        {
            if (settingsWindow != null) settingsWindow.Close();
        }

        ~OGRFileImporterExtension()
        {
            settingsWindow = null;
        }

        #endregion

        public void InitiateImporter()
        {
            OGREnvironment.SetOGREnvironment();
        }
        public Window GetSettingsWindow()
        {
            settingsWindow = new GDALAndOGRImporterWindow();//This used to cause the application to not terminate adding the dispose fixed the issue
            settingsWindow.ConnectedImporter = this;
            //            var gdalDriverCount = Gdal.GetDriverCount();
            //            //GDAL is used for reading images http://www.gdal.org/formats_list.html
            //            for (int i = 0; i < gdalDriverCount; i++)
            //            {
            //                Console.WriteLine(Gdal.GetDriver(i).ShortName);
            //                Console.WriteLine(Gdal.GetDriver(i).GetDescription());
            //            }
            return settingsWindow;
        }

//        internal void ImportElementsForegroundTest(string fileName, string elementType, string idPropertyName)
//        {
//          
//        }
//        private void DoServiceImport(string fileName, string elementType, string idPropertyName, bool foreground)
//        {
//            var parameters = new Dictionary<string, object>();
//            parameters.Add("FileName", fileName);
//            parameters.Add("ElementType", elementType);
//            //            parameters.Add("RegionBounds", depictionRegion);
//            parameters.Add("PropertyIdName", idPropertyName);
//            bool findProperties = !string.IsNullOrEmpty(idPropertyName);
//            parameters.Add("CreateProperties", findProperties);
//            if(!foreground)
//            {
//                
//            }else
//            {
//                if (DepictionAccess.DStory == null)
//                {
//                    //Log the error that cant have things without the story
//                    return;
//                }
//                var operationThread = new OgrFileReadingService();
//                var name = string.Format("Reading : {0}", Path.GetFileName(fileName));
//
//                DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
//                operationThread.UpdateStatusReport(name);
//                operationThread.StartBackgroundService(parameters);
//            }
//        }
        public void ImportElements(string fileName, string elementType, string idPropertyName)
        {
            var start = MemUtil.GetProcessMem();
            if (DepictionAccess.DStory == null)
            {
                //Log the error that cant have things without the story
                return;
            }


            var parameters = new Dictionary<string, object>();
            parameters.Add("FileName", fileName);
            parameters.Add("ElementType", elementType);
            //            parameters.Add("RegionBounds", depictionRegion);
            parameters.Add("PropertyIdName", idPropertyName);
            bool findProperties = !string.IsNullOrEmpty(idPropertyName);
            parameters.Add("CreateProperties", findProperties);


            var operationThread = new OgrFileReadingService();
            var name = string.Format("Reading : {0}", Path.GetFileName(fileName));

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);

            //            var startTime = DateTime.Now;
            //            UpdateDepictionWithFile(fileName, scaffold, DepictionAccess.DefaultGCS, true, idPropertyName);

            //            var endtime = DateTime.Now;
            //            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(string.Format("{0} : Time to create elements for {1}", elapsedTime, fileName));
            //            //            if (elementList == null)//Give some sot o
            //            //            {
            //            //                //Give some error, although im pretty sure something along the way should have thown something
            //            //                return;
            //            //            }
            //            //
            //            //            startTime = DateTime.Now;
            //            //            DepictionAccess.DStory.AddElements(elementList);
            //            //            DepictionAccess.DStory.MainDisplayer.AddDisplayElementsWithMatchingIds(elementList.Select(t => t.ElementId).ToList());
            //
            //            endtime = DateTime.Now;
            //            elapsedTime = string.Format("It took {0} to create elements", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(elapsedTime);
            //            Debug.WriteLine("Mem after element create");
            //            MemUtil.GetProcessMem();
            //            startTime = DateTime.Now;
            //            //the draw thing
            //            endtime = DateTime.Now;
            //            elapsedTime = string.Format("It took {0} to send elements to binding ", (endtime - startTime).TotalSeconds);
            //            Debug.WriteLine(elapsedTime);
            //            Debug.WriteLine("Mem after element to bindings");
            //            var end = MemUtil.GetProcessMem();
            //            Debug.WriteLine(string.Format("Mem used full set {0}", end - start));
        }

        //generally used for testing



        #region element creation helpers
        static public List<IPropertyScaffold> GetOgrFeatureProperties(Feature feature, FeatureDefn layerFieldDef)
        {
            var propScaffoldList = new List<IPropertyScaffold>();
            for (int iField = 0; iField < feature.GetFieldCount(); iField++)
            {
                FieldDefn fdef = layerFieldDef.GetFieldDefn(iField);
                //if a field is not set, do not import it
                if (feature.IsFieldSet(iField))
                {
                    var type = feature.GetFieldType(iField);

                    var propScaffold = new PropertyScaffold();
                    propScaffold.ValueString = feature.GetFieldAsString(iField).Trim();
                    propScaffold.ProperName = propScaffold.DisplayName = fdef.GetNameRef();

                    switch (type)
                    {
                        case FieldType.OFTReal:
                            propScaffold.TypeString = typeof(double).Name;
                            // propScaffoldList.Add(fdef.GetNameRef(), feature.GetFieldAsDouble(iField));
                            break;
                        case FieldType.OFTInteger:
                            propScaffold.TypeString = typeof(int).Name;
                            // inPropHolder.OgrProperties.Add(fdef.GetNameRef(), feature.GetFieldAsInteger(iField));
                            break;
                        case FieldType.OFTDate:
                            propScaffold.TypeString = typeof(DateTime).Name;
                            break;
                        default:
                            // inPropHolder.OgrProperties.Add(fdef.GetNameRef(), feature.GetFieldAsString(iField).Trim());
                            break;
                    }
                    propScaffoldList.Add(propScaffold);
                }
            }
            return propScaffoldList;
        }

        public HashSet<string> FindPropertyIntersections(string fileName)
        {

            DataSource dataSource = null;
            try
            {
                dataSource = Ogr.Open(fileName, 0);
            }
            catch
            {
                //log an error
                return null;
            }
            HashSet<string> propList = null;
            var layerCount = dataSource.GetLayerCount();
            for (var i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                FeatureDefn layerFieldDef = layer.GetLayerDefn();
                //More than one layers with geometries?
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var props = GetOgrFeatureProperties(nextFeature, layerFieldDef).Select(p => p.ProperName);
                    if (propList == null)
                    {
                        propList = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
                        propList.UnionWith(props);
                    }
                    else
                    {
                        propList.IntersectWith(props);
                    }
                }
            }
            return propList;
        }

        #endregion
    }

    public class OgrFileReadingService : BaseDepictionBackgroundThreadOperation
    {
        private string elementFile;
        private string elementScaffold;
        private bool createProperties;
        private string propIdName;
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            elementFile = parameters["FileName"].ToString();
            elementScaffold = parameters["ElementType"].ToString();
            createProperties = (bool)parameters["CreateProperties"];
            propIdName = parameters["PropertyIdName"].ToString();
            //            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (!File.Exists(elementFile)) return null;

            if (string.IsNullOrEmpty(elementScaffold)) return null;

            string projSystem;
            return CreateGeometryWrapsFromFile(elementFile, DepictionAccess.DefaultGCS, out projSystem, createProperties, propIdName);

            //                var ogrParser = new OsGeoFileTypeReaderToDepictionElements();
            //                var prototypes = ogrParser.GetElementPrototypesFromShapeFile(fileName, elementScaffold, regionBounds,
            //                                                                             "Shape file",
            //                                                                             "File", false, this);
            //                var tag = Tags.DefaultFileTag + Path.GetFileName(fileName);
            //                if (parameters.ContainsKey("Tag"))
            //                {
            //                    tag = parameters["Tag"].ToString();
            //                }
            //                foreach (var thing in prototypes)
            //                {
            //                    thing.Tags.Add(tag);
            //                }
            //                return prototypes;

            return null;
        }

        protected override void ServiceComplete(object args)
        {
            Console.WriteLine("Starting the update");
            var startTime = DateTime.Now;
            var scaffold = DepictionAccess.ScaffoldLibrary.FindScaffold(elementScaffold);
            var geomAndProps = args as List<GeomAndProps>;
            if (geomAndProps == null) return;
            var story = DepictionAccess.DStory;
            if (story == null) return;
            story.UpdateRepo(geomAndProps, propIdName, scaffold);

            var endtime = DateTime.Now;
            var elapsedTime = string.Format("It took {0} to load the file into a list", (endtime - startTime).TotalSeconds);
            Console.WriteLine(elapsedTime);
        }


        //        public void UpdateDepictionWithFile(string elementFile, IElementScaffold elementScaffold, string targetProjSystem, bool createProperties, string propIdName)
        //        {
        //            //Not sure what is up with this
        //            if (!File.Exists(elementFile))
        //            {
        //                elementFile = elementFile.Remove(0, 2);
        //            }
        //            if (!File.Exists(elementFile)) return;
        //            if (elementScaffold == null) return;
        //            string projSystem;
        //            var geomAndProps = CreateGeometryWrapsFromFile(elementFile, targetProjSystem, out projSystem, createProperties, propIdName);
        //            if (geomAndProps == null) return;
        //            var story = DepictionAccess.DStory;
        //            if (story == null) return;
        //            story.UpdateRepo(geomAndProps, propIdName, elementScaffold);
        //        }

        public List<IElement> CreateElementsFromFile(string elementFile, IElementScaffold elementScaffold, string targetProjSystem, bool createProperties)
        {
            //Not sure what is up with this
            if (!File.Exists(elementFile))
            {
                elementFile = elementFile.Remove(0, 2);
            }
            if (!File.Exists(elementFile)) return null;
            if (elementScaffold == null) return null;
            string projSystem;
            string idProp = string.Empty;
            var geomAndProps = CreateGeometryWrapsFromFile(elementFile, targetProjSystem, out projSystem, createProperties, idProp);
            if (geomAndProps == null) return null;

            var elementList = new List<IElement>();
            foreach (var geomAndProp in geomAndProps)
            {
                var element = ElementAndScaffoldService.CreateElementFromScaffoldAndGeometry(elementScaffold, geomAndProp.geometry);
                ElementAndScaffoldService.UpdateElementWithProperties(element, geomAndProp.properties, null);
                elementList.Add(element);
            }
            return elementList;
        }

        internal List<GeomAndProps> CreateGeometryWrapsFromFile(string fileName, string targetProjSystem, out string fileProjSystem, bool createProperties, string idName)
        {
            DataSource dataSource = null;
            fileProjSystem = string.Empty;
            var geometries = new List<GeomAndProps>();
            try
            {
                dataSource = Ogr.Open(fileName, 0);
            }
            catch
            {
                //log an error
                return null;
            }

            var layerCount = dataSource.GetLayerCount();
            SpatialReference depictionSR = null;
            if (!string.IsNullOrEmpty(targetProjSystem))
            {
                depictionSR = new SpatialReference(targetProjSystem);
            }
            fileProjSystem = string.Empty;
            for (var i = 0; i < layerCount; i++)
            {
                var layer = dataSource.GetLayerByIndex(i);
                Feature nextFeature;

                fileProjSystem = layer.GetSpatialRef().__str__();
                FeatureDefn layerFieldDef = layer.GetLayerDefn();
                //More than one layers with geometries?
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var geomAndProp = new GeomAndProps();
                    if (createProperties)
                    {
                        geomAndProp.properties = OGRFileImporterExtension.GetOgrFeatureProperties(nextFeature, layerFieldDef);
                        if (!string.IsNullOrEmpty(idName))
                        {
                            var prop =
                                geomAndProp.properties.FirstOrDefault(
                                    t => t.ProperName.Equals(idName, StringComparison.InvariantCultureIgnoreCase));
                            if (prop != null)
                            {
                                geomAndProp.Id = prop.ValueString;
                            }
                        }
                    }
                    var mainGeom = nextFeature.GetGeometryRef();
                    try
                    {

                        if (depictionSR != null)
                        {
                            mainGeom.TransformTo(depictionSR);
                        }
                        throw new NotImplementedException();
//                        geomAndProp.geometry = new GeometryGdalWrap(mainGeom);
                        geometries.Add(geomAndProp);
                    }
                    catch (Exception ex)
                    {
                        //TODO log the error
                    }

                }
            }
            return geometries;
        }


    }
}
