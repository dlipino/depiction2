﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Base.Utilities;
using Depiction2.Core.Service;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using Depiction2.Utilities;
using OSGeo.GDAL;

namespace Depiction.GDALImporterExtension
{

    /// <summary>
    /// TODO make image work with property ids
    /// </summary>
    [DepictionFileImporterMetadata(new[] { ".tiff",".tif" }, DisplayName = "Gdal File Importer", Name = "GDALFileImporter",
        Description = "Gdal Image file reader.")]
    public class GdalFileImporterExtension : IDepictionImporterExtension
    {

        #region constructor/destructor
        public void Dispose()
        {

        }

        #endregion

        #region intiation and settings window
        public void InitiateImporter()
        {
            GDALEnvironment.SetGDALEnvironment();
        }
        public Window GetSettingsWindow()
        {
            return null;
        }
        #endregion

        #region the import process
        public void ImportElements(string elementLocations, string fullElementType, string idPropertyName)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;

            var scaffold = DepictionAccess.ScaffoldLibrary.FindScaffold(fullElementType);
            var project = DepictionAccess.DStory.StoryProjector;

            var imageElement = GetImageFromFile(elementLocations, scaffold, project);
            story.AddElement(imageElement,true);
        }
        public HashSet<string> FindPropertyIntersections(string fileName)
        {

            var propList = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            return propList;
        }
        //    adfGeoTransform[0] /* top left x */
        //    adfGeoTransform[1] /* w-e pixel resolution */
        //    adfGeoTransform[2] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[3] /* top left y */
        //    adfGeoTransform[4] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[5] /* n-s pixel resolution */
        public IElement GetImageFromFile(string fileName, IElementScaffold elementType, IDepictionStoryProjectionService projector)
        {
            var dsOriginal = Gdal.Open(fileName, Access.GA_ReadOnly);
            var geoCoords = new double[6];
            dsOriginal.GetGeoTransform(geoCoords);
            var pixWidht = dsOriginal.RasterXSize;
            var pixHeight = dsOriginal.RasterYSize;
            var geoPointList = new List<Point>();
            var rotation = 0d;
            if (!(geoCoords[0].Equals(0) && geoCoords[2].Equals(0) && geoCoords[3].Equals(0) && geoCoords[4].Equals(0)))
            {
                var topLeft = new Point(geoCoords[0], geoCoords[3]);
                var botRight = new Point(topLeft.X + (pixWidht * geoCoords[1]), topLeft.Y + (pixHeight * geoCoords[5]));

                var origProj = dsOriginal.GetProjection();
                projector.SetOtherSpatialReference(origProj);
                var tlConv = projector.TransformPoint(topLeft.X, topLeft.Y, true);
                var brConv = projector.TransformPoint(botRight.X, botRight.Y, true);
                //            var geoString = string.Format("Polygon (({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", tlConv.X, tlConv.Y, brConv.X, brConv.Y);
                geoPointList = new List<Point>
                                   {
                                       tlConv,
                                       new Point(brConv.X, tlConv.Y),
                                       brConv,
                                       new Point(tlConv.X, brConv.Y),
                                       tlConv
                                   };
                rotation = geoCoords[4];
                
            }


            var element = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(elementType, geoPointList);
            element.ImageRotation = rotation;
            //Well i guess we need to write out the file to read it into the dictionary
            using(var tf = new DepictionFolderService(false))
            {

                var image = DepictionImageResourceDictionary.GetBitmapFromFile(fileName);
                var simpleFName = Path.GetFileName(fileName);
                if(DepictionAccess.DStory == null)
                {
                    //TODO throw some sort of error
                }else
                {
                    DepictionAccess.DStory.AllImages.AddImage(simpleFName, image, true);
                }
                element.ImageResourceName = simpleFName;
            }
            element.VisualState = ElementVisualSetting.Image;
            return element;
        }
        //         var imageBounds = imageMetadata.Bounds;
        //                if (!imageBounds.IsEmpty && imageBounds.IsValid)
        //                {
        //                    var geoString = string.Format("Polygon (({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", imageBounds.Left, imageBounds.Top, imageBounds.Right, imageBounds.Bottom);
        //                    element.SetGeometryFromString(geoString);
        //                }
        //                element.ImageResourceName = imageMetadata.ImageName;
        //                element.ImageRotation = imageMetadata.Rotation;
        public void ReadImageFile(string fileName)
        {

        }

        #endregion


    }
}