﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.StoryEntities;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction.GDALImporterExtension
{
       [DepictionImporterMetadata(Author = "Depiction Inc.", AddonPackage = "Default",
        Description = "Extension for adding GDAL compliant files.",
        DisplayName = "Gdal generic importer", Name = "GDALGenericImporter")]
    public class GDALImporterExtension : IDepictionImporterExtension
    {
        private GDALAndOGRImporterWindow settingsWindow;

        #region testing to make sure this thing is allows depiction to exit

        public void Dispose()
        {
            if(settingsWindow != null) settingsWindow.Close();
        }

        ~GDALImporterExtension()
        {
            settingsWindow = null;
        }

        #endregion

        public  void InitiateImporter()
        {
            GDALEnvironment.SetGDALEnvironment();
        }

        public  void ImportElements(string fileName, string elementType)
        {
           
        }

        public List<IElement> CreateElementsFromFile(string elementFile)
        {
            return new List<IElement>();
        }

        public  Window GetSettingsWindow()
        {
            settingsWindow = new GDALAndOGRImporterWindow();//This used to cause the application to not terminate adding the dispose fixed the issue
            settingsWindow.ConnectedImporter = this;
//            var gdalDriverCount = Gdal.GetDriverCount();
//            //GDAL is used for reading images http://www.gdal.org/formats_list.html
//            for (int i = 0; i < gdalDriverCount; i++)
//            {
//                Console.WriteLine(Gdal.GetDriver(i).ShortName);
//                Console.WriteLine(Gdal.GetDriver(i).GetDescription());
//            }
            return settingsWindow;
        }
    }
}
