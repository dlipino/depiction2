﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction.GDALImporterExtension
{
    // this can probably be diveded into two classes
    [DepictionProjectionChangerMetadata(Description = "Depiction's spatial reference converter", Name = "DepictionTransform",
        DisplayName = "Depiction Coordinate Tranformer")]
    public class DepictionGdalProjectionConverter : IDepictionProjectionChanger
    {
        //Should be WGS84 for now 6/28/2013
        private string depictionGCSWkt;

        private SpatialReference currentDepictionSpatialReference;
        private CoordinateTransformation toGCS;
        private CoordinateTransformation toPCS;
        private SpatialReference otherSR;

        private CoordinateTransformation fromDepictionTransform;
        private CoordinateTransformation toDepictionTransform;

        #region properties
        public bool IsDepictionProjected
        {
            get
            {
                return currentDepictionSpatialReference.IsProjected() != 0;
            }
        }
        public string DepictionGCSWkt
        {
            get { return depictionGCSWkt; }
        }

        public string CurrentCSWkt
        {
            get
            {
                string wkt;
                currentDepictionSpatialReference.ExportToPrettyWkt(out wkt, 1);
                return wkt;
            }
        }

        public string OtherCSWkt
        {
            get
            {
                if (otherSR == null) return string.Empty;
                string wkt;
                otherSR.ExportToPrettyWkt(out wkt, 1);
                return wkt;
            }
        }
        #endregion

        #region constructor/destruction

        public DepictionGdalProjectionConverter()
        {
            Osr.GetWellKnownGeogCSAsWKT("WGS84", out depictionGCSWkt);
            currentDepictionSpatialReference = new SpatialReference(depictionGCSWkt);
            //            Osr.DontUseExceptions();//Might be handy to know this
        }
        public void Dispose()
        {
            if (toDepictionTransform != null) toDepictionTransform.Dispose();
            if (fromDepictionTransform != null) fromDepictionTransform.Dispose();
            if (otherSR != null) otherSR.Dispose();
            if (currentDepictionSpatialReference != null) currentDepictionSpatialReference.Dispose();
            if (toGCS != null) toGCS.Dispose();

        }
        #endregion
        #region helpers for dealing with the projection system that depiction currently has set
        public void RemoveProjection()
        {
            currentDepictionSpatialReference = currentDepictionSpatialReference.CloneGeogCS();
            toGCS = null;// new CoordinateTransformation(currentDepictionSpatialReference, currentDepictionSpatialReference);
            toPCS = null;// new CoordinateTransformation(currentDepictionSpatialReference, currentDepictionSpatialReference);
        }
        public void SetToMercator()
        {
            currentDepictionSpatialReference.SetMercator(0, 0, 1, 0, 0);
            var gcs = currentDepictionSpatialReference.CloneGeogCS();
            toGCS = new CoordinateTransformation(currentDepictionSpatialReference, gcs);
            toPCS = new CoordinateTransformation(gcs, currentDepictionSpatialReference);
        }
        public Point DepictionPCSToGCS(double x, double y)
        {
            if (toGCS == null)
            {
                return new Point(x, y);
            }
            var transPoints = new[] { x, y };
            toGCS.TransformPoint(transPoints);
            return new Point(transPoints[0], transPoints[1]);
        }

        //So sketchy to do this. One doens'st know if the original is getting modified or
        //if it is a new clone.
        public IDepictionGeometry GCSToDepictionProjectionClone(IDepictionGeometry geometry)
        {
            if (toGCS == null)
            {
                return geometry.Clone();
            }
            var cloned = geometry.Clone();
            var clonedGeom = cloned.GeometryObject as Geometry;
            if (clonedGeom == null) return null;
            clonedGeom.Transform(toGCS);//TransformTo??
            return cloned;
        }

        public void GCSToDepictionPCSGeom(IDepictionGeometry geometry)
        {
            if (toGCS == null)
            {
                return;
            }
            var clonedGeom = geometry.GeometryObject as Geometry;
            if (clonedGeom == null) return;
            try
            {
                clonedGeom.Transform(toPCS);//TransformTo??
            }catch
            {
                clonedGeom.Empty();
            }
        }
        public Point GCSToDepictionPCSPoint(Point geoPoint)
        {
            if (toGCS == null)
            {
                return geoPoint;
            }
            var transPoints = new[] { geoPoint.X, geoPoint.Y };
            try
            {
                toPCS.TransformPoint(transPoints);
            }catch
            {
                transPoints[0] = double.NaN;
                transPoints[1] = double.NaN;
            }
            return new Point(transPoints[0], transPoints[1]);
        }
        #endregion
        #region for converting from other systems
        public void SetOtherSpatialReference(string wktString)
        {
            otherSR = new SpatialReference(wktString);
            fromDepictionTransform = new CoordinateTransformation(currentDepictionSpatialReference, otherSR);
            toDepictionTransform = new CoordinateTransformation(otherSR, currentDepictionSpatialReference);
        }

        public Point TransformPoint(double x, double y, bool toDepiction)
        {

            var transPoints = new[] { x, y };
            if (toDepiction)
            {
                toDepictionTransform.TransformPoint(transPoints);
            }
            else
            {
                fromDepictionTransform.TransformPoint(transPoints);
            }

            return new Point(transPoints[0], transPoints[1]);
        }

        public List<Point> TransformPoints(IEnumerable<Point> transPoints, bool toDepiction)
        {
            var pList = transPoints.ToList();
            var count = pList.Count;
            var xs = new double[count];
            var ys = new double[count];
            var zs = new double[count];

            for (int i = 0; i < count; i++)
            {
                xs[i] = pList[i].X;
                ys[i] = pList[i].Y;
                zs[i] = 0;
            }
            if (toDepiction)
            {
                toDepictionTransform.TransformPoints(count, xs, ys, zs);
            }
            else
            {
                fromDepictionTransform.TransformPoints(count, xs, ys, zs);
            }

            var outList = new List<Point>();
            for (int i = 0; i < count; i++)
            {
                outList.Add(new Point(xs[i], ys[i]));
            }
            return outList;
        }

        public void TransformGeometry(IDepictionGeometry geometry, bool toDepiction)
        {
            var geom = geometry.GeometryObject as Geometry;
            if (geom != null)
            {
                if (toDepiction)
                {
                    var retVal = geom.Transform(toDepictionTransform);
                }
                else
                {
                    var retVal = geom.Transform(fromDepictionTransform);
                }
            }
        }

        //        public IDepictionGeometry CloneAndTransformGeometry(IDepictionGeometry geometry)
        //        {
        //            var geom = geometry.GeometryObject as Geometry;
        //            
        //            if (geom != null)
        //            {
        //                var clone = geom.Clone();
        //                var retVal = clone.Transform(ctTrans);
        //            }
        //            return null;
        //        }
        #endregion


    }
}