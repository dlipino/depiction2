﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction.GDALImporterExtension
{
    [DepictionProjectionChangerMetadata]
    public class GdalProjectionConverter : IDepictionProjectionChanger
    {

        #region Variables

        private SpatialReference srcProj;
        private string srcPrjString;
        private SpatialReference trgProj;
        private string trgPrjString;
        
        private CoordinateTransformation ctTrans;

        #endregion

        #region properties

        public string SourceProj
        {
            get { return srcPrjString; }
        }
        public string TargetProj
        {
            get { return trgPrjString; }
        }

        #endregion

        public void SetTargetAndSourceProj(string src, string trg)
        {
            srcPrjString = src;
            trgPrjString = trg;
            srcProj = new SpatialReference(srcPrjString);
          
            trgProj = new SpatialReference(trgPrjString);
            
            ctTrans = new CoordinateTransformation(srcProj, trgProj);
        }

        public Point TransformPoint(double x, double y)
        {

            double[] transPoints = new double[3];
            ctTrans.TransformPoint(transPoints, x, y, 0);

            return new Point(transPoints[0], transPoints[1]);
        }

        public List<Point> TransformPoints(IEnumerable<Point> transPoints)
        {
            var pList = transPoints.ToList();
            var count = pList.Count;
            var xs = new double[count];
            var ys = new double[count];
            var zs = new double[count];

            for (int i = 0; i < count; i++)
            {
                xs[i] = pList[i].X;
                ys[i] = pList[i].Y;
                zs[i] = 0;
            }
            ctTrans.TransformPoints(count, xs, ys, zs);

            var outList = new List<Point>();
            for (int i = 0; i < count; i++)
            {
                outList.Add(new Point(xs[i], ys[i]));
            }
            return outList;
        }

        public void TransformGeometry(IDepictionGeometry geometry)
        {
            var geom = geometry.GeometryObject as Geometry;
            if(geom != null)
            {
                var retVal = geom.Transform(ctTrans);
            }
        }

//        public IDepictionGeometry CloneAndTransformGeometry(IDepictionGeometry geometry)
//        {
//            var geom = geometry.GeometryObject as Geometry;
//            
//            if (geom != null)
//            {
//                var clone = geom.Clone();
//                var retVal = clone.Transform(ctTrans);
//            }
//            return null;
//        }

        public void Dispose()
        {
            
        }
    }
}