﻿using System.IO;
using System.Reflection;
using System.Windows;
using Depiction2.TempExtensionCore.Interfaces;
using Microsoft.Win32;

namespace Depiction.GDALImporterExtension
{
    /// <summary>
    /// Interaction logic for GDALAndOGRImporterWindow.xaml
    /// </summary>
    public partial class GDALAndOGRImporterWindow
    {
        #region Temp variables

        //        private string fileName = string.Empty;
        #endregion
        #region Properties

        public IDepictionImporterExtension ConnectedImporter { get; set; }

        #endregion
        #region Constructor

        public GDALAndOGRImporterWindow()
        {
            InitializeComponent();
        }

        #endregion

        private void BrowseBttn_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            var location = @"D:\DepictionData";
            if (!Directory.Exists(location))
            {
                location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (!string.IsNullOrEmpty(location))
                {
                    string path = Path.Combine(location, "Datasets");

                    if (!Directory.Exists(path))
                    {
                        location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    }
                    else
                    {
                        location = path;
                    }
                }
            }

            dialog.InitialDirectory = location;

            if (dialog.ShowDialog(this) == true)
            {
                FileNameTextBox.Text = dialog.FileName;
            }
        }

        private void OpenRawBttn_Click(object sender, RoutedEventArgs e)
        {
//            var fileName = FileNameTextBox.Text;
//            if (ConnectedImporter == null) return;
//            if (string.IsNullOrEmpty(fileName)) return;
//            ConnectedImporter.ImportElements(fileName, null);
//            this.Close();
        }

        private void OpenStrBttn_Click(object sender, RoutedEventArgs e)
        {
//            var fileName = FileNameTextBox.Text;
//            if (ConnectedImporter == null) return;
//            if (string.IsNullOrEmpty(fileName)) return;
//            ConnectedImporter.ImportElements("st" + fileName, null);
        }
    }
}
