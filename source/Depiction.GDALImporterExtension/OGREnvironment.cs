﻿using System;
using System.IO;
using System.Reflection;
using OSGeo.GDAL;
using OSGeo.OGR;

namespace Depiction.GDALImporterExtension
{
    public class OGREnvironment
    {
        private static string origPath = string.Empty;
        //Vector formats http://www.gdal.org/ogr/ogr_formats.html
        public static void SetOGREnvironment()
        {
            SetOGREnvironmentVariables();
            Ogr.RegisterAll();
        }

        private static bool SetOGREnvironmentVariables()
        {
            var location = Assembly.GetExecutingAssembly().Location;
            string gdalPathLocation = Path.GetDirectoryName(location);
            if (string.IsNullOrEmpty(gdalPathLocation)) return false;
            //makes proj.dll findable
            origPath = Environment.GetEnvironmentVariable("PATH");
            //String updatedPath = gdalPathLocation + @"\bin;" + gdalPath + @"\bin\gdal\python\osgeo;" + gdalPath + @"\bin\proj\apps;" + gdalPath + @"\bin\gdal\plugins;" + gdalPath + @"\bin\gdal\plugins-optional;" + gdalPath + @"\bin\gdal\apps;" + gdalPath + @"\bin\ms\apps;" + gdalPath + @"\bin\gdal\csharp;" + gdalPath + @"\bin\ms\csharp;" + gdalPath + @"\bin\curl";
            String updatedPath = gdalPathLocation;

            if (origPath != null && !origPath.Contains(updatedPath))
            {
                updatedPath = origPath + ";" + updatedPath;
                Environment.SetEnvironmentVariable("PATH", updatedPath);
            }

            //Is this one even used by GDAL, it was not in the build for a while and things still seemed to work
            //I kind of wonder how these files are used
            Gdal.SetConfigOption("PROJ_LIB", Path.Combine(gdalPathLocation, @"proj\SHARE"));
            SetValueNewVariable("PROJ_LIB", Path.Combine(gdalPathLocation, @"proj\SHARE"));
            return true;
        }
        private static void SetValueNewVariable(string name, string value)
        {
            //if (Environment.GetEnvironmentVariable(name) == null)//What was this removed?
            Environment.SetEnvironmentVariable(name, value);
        }

        public static void UnsetOGREnvironment()
        {
            //Not sure if the is even needed
            if (!string.IsNullOrEmpty(origPath))
            {
                Environment.SetEnvironmentVariable("PATH", origPath);
            }
            //TODO it would be really nice to be able to do this for tests
        }
    }
    //    public static bool SetupGDAL()
    // {
    // bool result = false;
    // //We're adding the libs\GDAL-1.6.3 folder to our path
    // string path = Environment.GetEnvironmentVariable("PATH");
    // string gdalPath = GDAL_EXTRACT_FOLDER_PATH_GOES_HERE + "\\GDAL-1.8.0";
    // 
    // FileInfo info = new FileInfo(gdalPath);
    // if ((info.Attributes & FileAttributes.Directory) > 0)
    // {
    // String oldPath = Environment.GetEnvironmentVariable("PATH");
    // String PATH = gdalPath + @"\bin;" + gdalPath + @"\bin\gdal\python\osgeo;" + gdalPath + @"\bin\proj\apps;" + gdalPath + @"\bin\gdal\plugins;" + gdalPath + @"\bin\gdal\plugins-optional;" + gdalPath + @"\bin\gdal\apps;" + gdalPath + @"\bin\ms\apps;" + gdalPath + @"\bin\gdal\csharp;" + gdalPath + @"\bin\ms\csharp;" + gdalPath + @"\bin\curl";
    // if (!path.Contains(PATH))
    // {
    // path = PATH + ";" + path;
    // Environment.SetEnvironmentVariable("PATH", path);
    // }
    // 
    // String GDAL_DATA = gdalPath + @"\bin\gdal-data";
    // Environment.SetEnvironmentVariable("GDAL_DATA", GDAL_DATA);
    // OSGeo.GDAL.Gdal.SetConfigOption("GDAL_DATA", GDAL_DATA);
    // 
    // String GDAL_DRIVER_PATH = gdalPath + @"\bin\gdal\plugins";
    // Environment.SetEnvironmentVariable("GDAL_DRIVER_PATH", GDAL_DRIVER_PATH);
    // OSGeo.GDAL.Gdal.SetConfigOption("GDAL_DRIVER_PATH", GDAL_DRIVER_PATH);
    // 
    // String PYTHONPATH = gdalPath + @"\bin\gdal\python\osgeo";
    // Environment.SetEnvironmentVariable("PYTHONPATH", PYTHONPATH);
    // OSGeo.GDAL.Gdal.SetConfigOption("PYTHONPATH", PYTHONPATH);
    // 
    // String PROJ_LIB = gdalPath + @"\bin\proj\SHARE";
    // Environment.SetEnvironmentVariable("PROJ_LIB", PROJ_LIB);
    // OSGeo.GDAL.Gdal.SetConfigOption("PROJ_LIB", PROJ_LIB);
    // 
    // OSGeo.GDAL.Gdal.SetConfigOption("GDAL_CACHEMAX", "100000");
    // OSGeo.GDAL.Gdal.SetConfigOption("CPL_TMPDIR", "c:\\");
    // 
    ////Test to see if EPSG spatial reference was registered
    //new OSGeo.OSR.SpatialReference("EPSG:4326");
    // 
    ////This line throws an exception if the the wrong version of GDal was found in the path somewhere OR the path didn't point to GDal correctly.
    // OSGeo.OGR.Ogr.RegisterAll();
    // 
    // result = true;
    // }
    // return result;
    // }
}