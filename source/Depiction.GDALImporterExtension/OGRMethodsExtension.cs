﻿using System.Collections.Generic;
using Depiction.ProjectionSystem.DepictionProjection;
using Depiction2.Base.Utilities.Drawing;
using Depiction2.TempExtensionCore.Extensions.Metadata;
using Depiction2.TempExtensionCore.Interfaces;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction.GDALImporterExtension
{
    [DepictionGdalExtensionMetadata]
    public class OGRMethodsExtension : IDepictionGdalExtension
    {
        public List<EnhancedPointListWithChildren> GetPointsFromWKTString(string wktString)
        {
            var returnList = new List<EnhancedPointListWithChildren>();
            if (string.IsNullOrEmpty(wktString)) return returnList;
            var geom = Geometry.CreateFromWkt(wktString);

            GdalExtensionHelpers.GetPointListFromGeom(geom, ref returnList, null);
            return returnList;
        }
        public List<EnhancedPointListWithChildren> GetTransformedPointsFromWKTString(string wktString, string sourceProj, string targetProj)
        {
            var geom = Geometry.CreateFromWkt(wktString);
            var srGeom = geom.GetSpatialReference();
            var noSrc = srGeom == null && string.IsNullOrEmpty(sourceProj);
            var noTgt = string.IsNullOrEmpty(targetProj);
            if (!noSrc && !noTgt)
            {
                var trgt = DepictionProjectionSystem.CreateSpatialReference(targetProj);
                var srs = DepictionProjectionSystem.CreateSpatialReference(sourceProj);
                var ct = new CoordinateTransformation(srs, trgt);
                geom.Transform(ct);
            }
            var returnList = new List<EnhancedPointListWithChildren>();

            GdalExtensionHelpers.GetPointListFromGeom(geom, ref returnList, null);
            return returnList;
        }

        public void Dispose()
        {

        }

    }
}