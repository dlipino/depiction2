﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Utilities.Drawing;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction.GDALImporterExtension
{
    public class GdalExtensionHelpers
    {
//        internal static bool ModifySpatialReference(Geometry geomToChange, SpatialReference sourceSpatialReference)
//        {
//            bool print = false;
//            Envelope extent = new Envelope();
//            string extentString;
//            if (print)
//            {
//                geomToChange.GetEnvelope(extent);
//                Console.WriteLine(String.Format("Post Transform Extent for geom "));
//                extentString = String.Format("Max x {0}, Min x {1}, Max y {2}, Min y {3}", extent.MaxX,
//                                             extent.MinY, extent.MaxY, extent.MinY);
//                Console.WriteLine(extentString);
//            }
//
//            //                                        var sr = DepictionProjectionSystem.mercatorProjectedCoordinateSystem;
//            //                            srDst.ImportFromWkt(ref sr);
//
//            try
//            {
//                //////                var srGeom = geomToChange.GetSpatialReference();
//                var srDst = new SpatialReference("");
//                ////                srDst = sourceSpatialReference.CloneGeogCS();
//                ////                srDst.SetMercator(0, 0, 1, 0, 0);
//                //                srDst.SetWellKnownGeogCS("WGS84");
//                //                var ct = new CoordinateTransformation(sourceSpatialReference, srDst);
//                //                geomToChange.Transform(ct);
//            }
//            catch (Exception ex)
//            {
//                return false;
//            }
//            if (print)
//            {
//                geomToChange.GetEnvelope(extent);
//                Console.WriteLine(String.Format("Pretransform Extent for geom"));
//                extentString = String.Format("Max x {0}, Min x {1}, Max y {2}, Min y {3}", extent.MaxX,
//                                             extent.MinY, extent.MaxY, extent.MinY);
//                Console.WriteLine(extentString);
//            }
//            return true;
//        }

        internal static void GetPointListFromGeom(Geometry mainGeom, ref List<EnhancedPointListWithChildren> allGeometries, EnhancedPointListWithChildren parent)
        {
            var type = mainGeom.GetGeometryType();
            //http://www.gdal.org/ogr/classOGRGeometry.html

            var pointCount = mainGeom.GetPointCount();
            var geomCount = mainGeom.GetGeometryCount();
            EnhancedPointListWithChildren pointListWithChildren = null;
            
            var geomType = mainGeom.GetGeometryType();
            if (pointCount != 0)
            {
                pointListWithChildren = new EnhancedPointListWithChildren();

                if (pointCount == 1)
                {
                    pointListWithChildren.TypeOfPointList = PointListType.Points;
                }
                else
                {
                    if (mainGeom.GetX(0).Equals(mainGeom.GetX(pointCount - 1)) &&
                        mainGeom.GetY(0).Equals(mainGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        pointListWithChildren.TypeOfPointList = PointListType.Shell;
                        pointListWithChildren.IsClosed = true;
                        pointListWithChildren.IsFilled = true;
                    }
                    else
                    {
                        pointListWithChildren.TypeOfPointList = PointListType.Line;
                        pointListWithChildren.IsClosed = false;
                        pointListWithChildren.IsFilled = false;
                    }
                }
                for (int j = 0; j < pointCount; j++)
                {
                    pointListWithChildren.Add(new Point(mainGeom.GetX(j), mainGeom.GetY(j)));
                }
            }

            if (geomType == wkbGeometryType.wkbMultiPolygon || geomType == wkbGeometryType.wkbMultiLineString)
            {
                GetMultiGeom(mainGeom,ref pointListWithChildren);
            }else
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GetPointListFromGeom(mainGeom.GetGeometryRef(i), ref allGeometries, parent);
                    }
                }
            }
            if(pointListWithChildren != null) allGeometries.Add(pointListWithChildren);
            
        }

        internal static void GetMultiGeom(Geometry multiGeom, ref EnhancedPointListWithChildren parent)
        {
            var pointCount = multiGeom.GetPointCount();
            var geomCount = multiGeom.GetGeometryCount();
            EnhancedPointList child = null;
            var isClosed = false;
            var isFilled = false;
            var listType = PointListType.Points;

            var geomType = multiGeom.GetGeometryType();

            if (pointCount != 0)
            {
                if(parent.MainCount != 0)
                {
                    child = new EnhancedPointList();
                }
                if (pointCount == 1)
                {
                    listType = PointListType.Points;
                }
                else
                {
                    if (multiGeom.GetX(0).Equals(multiGeom.GetX(pointCount - 1)) &&
                        multiGeom.GetY(0).Equals(multiGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        listType = PointListType.Shell;
                        isClosed = true;
                        isFilled = true;
                    }
                    else
                    {
                        listType = PointListType.Line;
                    }
                }
                for (int j = 0; j < pointCount; j++)
                {
                    if(child != null)
                    {
                        child.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));
                    }else
                    {
                        parent.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));   
                    }
                }

                if (child != null)
                {
                    child.IsClosed = isClosed;
                    child.IsFilled = isFilled;
                    child.TypeOfPointList = listType;
                }
                else
                {
                    parent.IsClosed = isClosed;
                    parent.IsFilled = isFilled;
                    parent.TypeOfPointList = listType;
                }

            }

            if (parent == null)
            {
                parent = new EnhancedPointListWithChildren();
            }

            if (geomCount != 0)
            {
                for (int i = 0; i < geomCount; i++)
                {
                    GetMultiGeom(multiGeom.GetGeometryRef(i), ref parent);
                }
            }
            if(child != null) parent.AddChild(child);
        }
    }
}