﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Webservice14.DepictionWebServiceNoSSL;

namespace Depiction2.Webservice14
{
    public static class WebServiceFacade
    {
        private static string fakeVersion = "1.4.1";
        static public string QuickstartServiceName = "ReleaseQuickstartService";
        #region web service constructors

        private static DepictionWebServiceClient WebService
        {
            get
            {
                var service = new DepictionWebServiceClient(QuickstartServiceName);
#if DEBUG
                // Fake the certificate validation for debug since the debug SSL certificate ins't legit ...
                //ServicePointManager.ServerCertificateValidationCallback += RemoteCertValidateCallback;
#endif
                SetServiceCredentials(service.ClientCredentials);
                return service;
            }
        }

        private static void SetServiceCredentials(ClientCredentials credentials)
        {
            credentials.UserName.UserName = "DepiCtion*f54KJfdi@090";
            credentials.UserName.Password = "87$389FdjkJfio234{}fdsfdshGjew4*&Fd";
        }

        //        private static bool RemoteCertValidateCallback(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        //        {
        //            return true;
        //        }

        #endregion

        /// <summary>
        /// Gets the list of Quickstart sources for countries with these codes.
        /// If cannot access the web service, return null.
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        public static List<RegionDataSourceInformation> GetPortalQuickstartSourcesByRegionCodes(string[] codes)
        {
            try
            {
                if (!DepictionInternetConnectivityService.IsInternetAvailable) return null;
                var quickstartParameters = new[] {new DepictionWebServiceParameter {
                    Key = "RegionCodes",
                    Value = codes
                }};
                var version = fakeVersion;// VersionInfo.AppVersionForQS;
                //This looks at the portal
                var quickstartSources = WebService.GetQuickstartSourcesByRegionCodes(version, quickstartParameters);

                //                        qs => new QuickstartItem
                //                                  {
                //                                      AddinName = qs.DataFacade,
                //                                      ElementType = qs.ElementType,
                //                                      Name = qs.Name,
                //                                      Parameters = qs.Parameters,
                //                                      Description = qs.Description,
                //                                  });

                var sourceList = new List<RegionDataSourceInformation>();
                var legacyToCurrentDictionary = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase)
                {
                    {"WMSElementImporter","g"},{"WfsElementGatherer","g"},{"WcsElementImporter","t"}
                };
                var importerKey = "ImporterName";
                var elementTypeKey = "elementType";
                foreach (var qs in quickstartSources)
                {
                    //qs element type actually describes the type of extension (geocoder, importer etc)

                    if (!qs.ElementType.Equals("ElementGatherer", StringComparison.InvariantCultureIgnoreCase))
                        continue;
                    if (qs.Parameters == null || !qs.Parameters.Any()) continue;
                    //i hate legacy stuff. the qs does always have the correct importer name
                    var regionSource = new RegionDataSourceInformation
                                           {
                                               DisplayName = qs.Name,
                                               Description = qs.Description,
                                               Parameters = qs.Parameters
                                           };
                    if (qs.Parameters.ContainsKey(importerKey))
                    {
                        regionSource.Name = regionSource.GenericName = qs.Parameters[importerKey];
                    }
                    if (qs.Parameters.ContainsKey(elementTypeKey))
                    {
                        regionSource.ElementType = qs.Parameters[elementTypeKey];
                    }
                    sourceList.Add(regionSource);
                }
                return sourceList;
            }
            catch (Exception ex)
            {
                //               DepictionExceptionHandler.HandleException(ex,false,true);
                return null;
            }
        }

        /// <summary>
        /// Return the set of region codes for this bounding box.
        /// Return null if could not connect to the web service.
        /// </summary>
        /// <param name="boundingBox"></param>
        /// <returns></returns>
        public static string[] GetRegionCodesForRegion(ICartRect boundingBox)
        {
            try
            {
                var version = fakeVersion;// VersionInfo.AppVersionForQS;
                return DepictionInternetConnectivityService.IsInternetAvailable ? WebService.GetRegionCodesForRegion(version, new[] {new DepictionWebServiceParameter {
                    Key = "Top",
                    Value = boundingBox.Top
                }, new DepictionWebServiceParameter {
                    Key = "Right",
                    Value = boundingBox.Right
                }, new DepictionWebServiceParameter {
                    Key = "Bottom",
                    Value = boundingBox.Bottom
                }, new DepictionWebServiceParameter {
                    Key = "Left",
                    Value = boundingBox.Left
                }}) : null;
            }
            catch (Exception ex)
            {
                //                DepictionExceptionHandler.HandleException(ex, false, true);
                return null;
            }
        }

        public class PublishResult
        {
            public bool Succeeded { get; set; }
            public int ID { get; set; }
        }

        public static PublishResult DoPublishToWeb(string userName, string password, string depictionTitle, string description, string filename, string[] tags, string dpnPath, string webZipPath)
        {
            try
            {

                //Expects the following parameters: String userName, String password, String depictionTitle, String description, string[] tags
                var webDepictionID = WebService.BeginPublishToWeb(fakeVersion, new[] {new DepictionWebServiceParameter {
                    Key = "UserName", Value = userName
                }, new DepictionWebServiceParameter {
                    Key = "Password", Value = password
                }, new DepictionWebServiceParameter {
                    Key = "depictionTitle", Value = depictionTitle
                }, new DepictionWebServiceParameter {
                    Key = "Description", Value = description
                }, new DepictionWebServiceParameter {
                    Key = "Filename", Value = filename
                }, new DepictionWebServiceParameter {
                    Key = "Tags", Value = new[]{"depiction"}
                }});

                using (var dpnStream = new FileStream(dpnPath, FileMode.Open, FileAccess.Read))
                {
                    var position = 0;

                    while (dpnStream.Length > position)
                    {
                        long bufferSize = 64000;

                        if (dpnStream.Length < position + 64000)
                            bufferSize = dpnStream.Length - position;

                        var buffer = new byte[bufferSize];
                        dpnStream.Read(buffer, 0, (Int32)bufferSize);

                        WebService.PublishDPNChunk(webDepictionID, buffer);

                        position += 64000;
                    }
                }

                using (var xmlStream = new FileStream(webZipPath, FileMode.Open))
                {
                    var position = 0;

                    while (xmlStream.Length > position)
                    {
                        long bufferSize = 64000;

                        if (xmlStream.Length < position + 64000)
                            bufferSize = xmlStream.Length - position;

                        var buffer = new byte[64000];
                        xmlStream.Read(buffer, 0, (Int32)bufferSize);

                        WebService.PublishWebDepictionChunk(webDepictionID, buffer);

                        position += 64000;
                    }
                }
                var result = new PublishResult();
                result.Succeeded = WebService.FinalizePublishToWeb(webDepictionID);
                result.ID = webDepictionID;
                return result;
            }
            catch (MessageSecurityException)
            {
                return new PublishResult { Succeeded = false };
            }
        }

        //        public static bool IsProductSNValid(string sn, int id)
        //        {
        //            try
        //            {
        //                return WebService.IsProductSNValid(sn, id);
        //            }
        //            catch (Exception e)
        //            {
        //                throw new Exception(string.Format("Could not validate serial number. Please contact {0}.", DepictionAccess.ProductInformation.SupportEmail), e);
        //            }
        //        }
    }
}
