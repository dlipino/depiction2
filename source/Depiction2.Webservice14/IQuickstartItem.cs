﻿using System.Collections.Generic;

namespace Depiction2.Webservice14
{
    public interface IQuickstartItem
    {
        string Name { get; set; }
        string AddinName { get; set; }
        string Description { get; set; }
        Dictionary<string, string> Parameters { get; set; }
        string ElementType { get; }
    }
}
