﻿using System;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using Depiction2.Base.Geo;

namespace DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork
{
    ///<summary>
    /// Class to represent nodes in a road network
    ///</summary>
    public class RoadNode
    {
        //        private Point pValue = new Point(double.NaN, double.NaN);
        //        private IDepictionLatitudeLongitude llValue = null;
        #region constructors

        public RoadNode()
        {

        }
        ///<summary>
        /// Class to represent nodes in a road network
        ///</summary>
        ///<param name="v1">Latlon of the vertex</param>
        public RoadNode(IDepictionLatitudeLongitude v1)
        {

            if (v1 != null)
            {
                Vertex = new Point(v1.Longitude, v1.Latitude);
                //                VertexLatLong = v1;
                //                var p = (Point) v1;
                //                var lat = p.Y;
                //                var lon = p.X;
                //                Vertex = new Point(lat, lon);
            }
            else
            {
                Vertex = new Point(Double.NaN, double.NaN);
            }
            NodeID = 0;
        }

        #endregion

        public Point Vertex { get; set; }
        //        public IDepictionLatitudeLongitude VertexLatLong
        //        {
        //            get { return llValue; }
        //            set
        //            {
        //                llValue = value;
        //                if (value != null)
        //                {
        //                    pValue = new Point(VertexLatLong.Longitude, VertexLatLong.Latitude);
        //                }
        //                else
        //                {
        //                    pValue = new Point(double.NaN, double.NaN);
        //                }
        //            }
        //        }

        public long NodeID { get; set; }

        #region Methods
        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            // These two lines handle null or wrong type
            var other = obj as RoadNode;
            if (other == null) return false;
            return Vertex.Equals(other.Vertex);
//            return (Vertex.X.Equals(other.Vertex.X) && Vertex.Y.Equals(other.Vertex.Y));
            //            if (other.VertexLatLong == null && VertexLatLong == null) return true;
            //            if (other.VertexLatLong != null && VertexLatLong == null) return false;
            //            if (VertexLatLong != null && other.VertexLatLong == null) return false;
            //            if (VertexLatLong != null && other.VertexLatLong != null)
            //            {
            //                var oP = (other.VertexLatLong);
            //                var p = (VertexLatLong);
            //                return (p.Latitude.Equals(oP.Latitude) && p.Longitude.Equals(oP.Longitude));
            //            }
            return false;

        }
        ///<summary>
        /// Equality operator for RoadNode
        ///</summary>
        ///<param name="a"></param>
        ///<param name="b"></param>
        ///<returns></returns>
        public static bool operator ==(RoadNode a, RoadNode b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }
            return a.Vertex.Equals(b.Vertex);
//            return (a.Vertex.X.Equals(b.Vertex.X) && a.Vertex.Y.Equals(b.Vertex.Y));
            //            if (a.VertexLatLong != null && b.VertexLatLong != null)
            //            {
            //                var oP = (a.VertexLatLong);
            //                var p = (b.VertexLatLong);
            //                return (p.Latitude.Equals(oP.Latitude) && p.Longitude.Equals(oP.Longitude));
            //            }
            //            return false;
            // Return true if the fields match:
            //            return a.Vertex.Latitude == b.Vertex.Latitude && a.Vertex.Longitude == b.Vertex.Longitude;
        }

        ///<summary>
        /// Not equals operator that compares two road nodes
        ///</summary>
        ///<param name="a"></param>
        ///<param name="b"></param>
        ///<returns></returns>
        public static bool operator !=(RoadNode a, RoadNode b)
        {
            return !(a == b);
        }
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            if (Vertex == null) return 0;
            var p = Vertex;
            return p.GetHashCode();
            //            int hashCode = p.Latitude.GetHashCode() >> 3;
            //            hashCode ^= Vertex.Longitude.GetHashCode();
            //            return hashCode;
        }
        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var locString = reader.GetAttribute("Location");
            //comes in latitude, longitude
            if (string.IsNullOrEmpty(locString))
            {
                Vertex = new Point(double.NaN,double.NaN);// LatLong = new LatitudeLongitudeSimple(double.NaN, double.NaN);
            }
            else
            {
                var p = Point.Parse(locString);
                Vertex = new Point(p.Y,p.X);
                //VertexLatLong = new LatitudeLongitudeSimple(p.X, p.Y);
            }

            //            Vertex = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(reader.GetAttribute("Location"));

            var nodeString = reader.GetAttribute("NodeID");
            if (string.IsNullOrEmpty(nodeString))
            {
                NodeID = -1;
            }
            else
            {
                NodeID = int.Parse(nodeString);
            }
            reader.ReadStartElement("RoadNode");
        }

        public void WriteXml(XmlWriter writer)
        {
            //            writer.WriteStartElement("RoadNode");
            //            writer.WriteAttributeString("Location", Vertex.ToXmlSaveString());
            //            writer.WriteAttributeString("NodeID",NodeID.ToString());
            //            writer.WriteEndElement();
        }
    }
}
