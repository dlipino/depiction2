﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.Base.Utilities;
using Depiction2.Base.Utilities.Drawing;
using DepictionLegacy.TerrainAndRoadNetwork.Interface;
using QuickGraph;

namespace DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork
{
    public class RoadGraph : IRoadGraph, IRestorable, IXmlSerializable
    {
        private readonly AdjacencyGraph<RoadNode, RoadSegment> graph;
        private List<RoadSegment> disabledEdges;
//        private static readonly Speed UNKNOWN_SPEED_LIMIT = new Speed(MeasurementSystem.Imperial, MeasurementScale.Large, 30.0);
        private const double UNUSABLE_ROAD_WEIGHT = 1000000;

        #region Properties

        public int EdgeCount
        {
            get
            {
                return graph.EdgeCount / 2;
            }
        }

        public int VertexCount
        {
            get
            {
                return graph.VertexCount;
            }
        }

        public AdjacencyGraph<RoadNode, RoadSegment> Graph
        {
            get { return graph; }
        }

        #endregion

        #region constructor
        public RoadGraph()
        {
            graph = new AdjacencyGraph<RoadNode, RoadSegment>();
            disabledEdges = new List<RoadSegment>();
        }

        #endregion
        public EnhancedPointListWithChildren ConvertToPointList()
        {
            if (graph != null)
            {
                var lineSegments = graph.Edges;
                var idList = new HashSet<string>();//Will be used to check for backwards and forwards
                //ensures no doubling of line segments
                var roadList = new EnhancedPointListWithChildren();
                roadList.TypeOfPointList = PointListType.MultiLine;
                roadList.IsClosed = false;
                roadList.IsFilled = false;
                foreach (var rs in lineSegments)
                {
                    var forward = rs.Source.NodeID + "|" + rs.Target.NodeID;
                    var backward = rs.Target.NodeID + "|" + rs.Source.NodeID;

                    if(!(idList.Contains(forward) || idList.Contains(backward)))
                    {
                        idList.Add(forward);
                        var line = new EnhancedPointList();
                        line.TypeOfPointList = PointListType.Line;
                        line.IsClosed = false;
                        line.IsFilled = false;
                        line.Points.Add(rs.Source.Vertex);
                        line.Points.Add(rs.Target.Vertex);
                        roadList.AddChild(line);
                    }
                }

                return roadList;
            }
            return null;
        }

        public string ConvertToMultilinewkt()
        {
//            MULTILINESTRING ((10 10, 20 20, 10 40),(40 40, 30 30, 40 20, 30 10))
            if (graph != null)
            {
                var lineSegments = graph.Edges;
                var idList = new HashSet<string>();//Will be used to check for backwards and forwards
                //ensures no doubling of line segments
                var lineList = new List<string>();
                foreach (var rs in lineSegments)
                {
                    var forward = rs.Source.NodeID + "|" + rs.Target.NodeID;
                    var backward = rs.Target.NodeID + "|" + rs.Source.NodeID;

                    if (!(idList.Contains(forward) || idList.Contains(backward)))
                    {
                        idList.Add(forward);
                        var lineString = string.Format("({0} {1}, {2} {3})", rs.Source.Vertex.X, rs.Source.Vertex.Y,
                                                       rs.Target.Vertex.X, rs.Target.Vertex.Y);
                        lineList.Add(lineString);
                    }
                }

                return DepictionWktGeometryUtilities.StringLineListToMultilineWkt(lineList);
            }
            return null;
        }
//        public IDepictionGeometry ConvertToGeometry()
//        {
//            if (graph != null)
//            {
//                var lineSegments = graph.Edges;
//                var geomFactory = new GeometryFactory();
//                var lsList = new List<LineString>();
//                //ensures no doubling of line segments
//                var singleLineSegmenst = new HashSet<string>();
//                foreach (var rs in lineSegments)
//                {
//                    var coords = new List<Coordinate>();
//                    var p = rs.Source.NodeID + "|" + rs.Target.NodeID;
//                    var rp = rs.Target.NodeID + "|" + rs.Source.NodeID;
//                    if (rs.Source.NodeID == rs.Target.NodeID)//This is needed for older rn
//                    {
//                        p = rs.Source.Vertex.ToXmlSaveString() + "|" + rs.Target.Vertex.ToXmlSaveString();
//                        rp = rs.Target.Vertex.ToXmlSaveString() + "|" + rs.Source.Vertex.ToXmlSaveString();
//                    }
//
//                    if (!singleLineSegmenst.Add(p)) continue;
//                    if (!singleLineSegmenst.Add(rp)) continue;
//
//                    coords.Add(new Coordinate(rs.Source.Vertex.Longitude, rs.Source.Vertex.Latitude));
//                    coords.Add(new Coordinate(rs.Target.Vertex.Longitude, rs.Target.Vertex.Latitude));
//                    lsList.Add((LineString)geomFactory.CreateLineString(coords.ToArray()));
//                }
//
//                return new DepictionGeometry(geomFactory.CreateMultiLineString(lsList.ToArray()));
//            }
//            return null;
//        }

        public void AddVertex(RoadNode vertex)
        {
            graph.AddVertex(vertex);
        }

        public bool ContainsVertex(RoadNode vertex)
        {
            return graph.ContainsVertex(vertex);
        }

        public void AddEdge(RoadSegment edge)
        {
            RoadSegment otherEdge;
            if (graph.TryGetEdge(edge.Source, edge.Target, out otherEdge))
                return;
            if (!graph.ContainsVertex(edge.Source))
                graph.AddVertex(edge.Source);
            if (!graph.ContainsVertex(edge.Target))
                graph.AddVertex(edge.Target);

            graph.AddEdge(edge);
        }
        //This should probably get moved to extensions or something of the sort
        static public double PointDistance(Point a, Point b)
        {
           return Math.Sqrt(((Vector)a - (Vector)b).LengthSquared);
        }
        public double GetEdgeWeight(RoadSegment edge)
        {
            var s = edge.Source.Vertex;
            var t = edge.Target.Vertex;
            var dist = PointDistance(s, t);
            return disabledEdges.Contains(edge) ? UNUSABLE_ROAD_WEIGHT : dist;// edge.Source.Vertex.Distance(edge.Target.Vertex);
        }

//        public Func<RoadSegment, double> GetDriveTimeFunc(ITempElement route)
//        {
//            return edge =>
//                       {
//                           double value;
//                           if (disabledEdges.Contains(edge))
//                               value = UNUSABLE_ROAD_WEIGHT;
//                           else
//                           {
//                               var kilometers = edge.Source.Vertex.DistanceTo(edge.Target.Vertex, MeasurementSystem.Metric, MeasurementScale.Large);
//                               value = kilometers / SpeedLimit(edge, route).GetValue(MeasurementSystem.Metric, MeasurementScale.Normal) * 60;
//                           }
//                           return value;
//                       };
//        }

        public ReadOnlyCollection<RoadSegment> DisabledEdges
        {
            get { return new ReadOnlyCollection<RoadSegment>(disabledEdges); }
        }

//        private static Speed SpeedLimit(RoadSegment way, IDepictionElement element)
//        {
//            if (way.MaxSpeed != null)
//            {
//                var converter = new SpeedConverter();
//                var speed = converter.ConvertFrom(way.MaxSpeed) as Speed;
//                if (speed != null)
//                {
//                    return speed;
//                }
//            }
//            if (way.Highway != null)
//            {
//                var value = way.Highway;
//                if (element.HasPropertyByInternalName(value))
//                {
//                    Speed speed;
//                    element.GetPropertyValue(value, out speed);
//
//                    if (speed != null)
//                        return speed;
//                }
//            }
//            //unknown speed limit
//            return UNKNOWN_SPEED_LIMIT;
//        }

        public void DisableEdge(RoadSegment edge)
        {
            RoadSegment graphEdge;
            if (graph.TryGetEdge(edge.Source, edge.Target, out graphEdge))
            {
                disabledEdges.Add(graphEdge);
            }
            if (graph.TryGetEdge(edge.Target, edge.Source, out graphEdge))
            {
                disabledEdges.Add(graphEdge);
            }
        }

        /// <summary>
        /// Given any LatitudeLongitude, find the nearest vertex in the graph to that LatitudeLongitude.
        /// </summary>
        /// <param name="inCoord">The LatitudeLongitude.</param>
        /// <returns>If there is one or more vertices, the nearest vertex.  Otherwise null.</returns>
        public RoadNode FindNearestNode(RoadNode inCoord)
        {
            double minDistance = Double.PositiveInfinity;
            RoadNode nearestCoord = null;
            foreach (var node in graph.Vertices)
            {
                var distance = PointDistance(inCoord.Vertex, node.Vertex);//inCoord.Vertex.DistanceTo(node.Vertex);
                if (distance >= minDistance) continue;
                minDistance = distance;
                nearestCoord = node;
            }
            return nearestCoord;
        }

        public RoadNode FindNearestNode(Point inCoord)
        {
            double minDistance = Double.PositiveInfinity;
            RoadNode nearestCoord = null;
            foreach (var node in graph.Vertices)
            {
                var distance = PointDistance(inCoord, node.Vertex);// inCoord.DistanceTo(node.Vertex);
                if (distance >= minDistance) continue;
                minDistance = distance;
                nearestCoord = node;
            }
            return nearestCoord;
        }

        public IRouteFinder RouteFinder
        {
            get
            {
                return new RouteFinderService(this);
            }
        }

        public void Restore(bool notifyChange)
        {
            disabledEdges = new List<RoadSegment>();
        }

        #region Serializtion section

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
        #region Old stuff for reading 1.2.2 roadgraphs, which is useless now because 1.3.2 roadgraphs have more information
//        protected RoadNode ReadeLegacy122RoadNode(string startName, XmlReader reader)
//        {
//            var roadNode = new RoadNode();
//            if (!reader.Name.Equals(startName)) return roadNode;
//            reader.ReadStartElement(startName);
//            if (!reader.Name.Equals("RoadNode")) return roadNode;
//            reader.ReadStartElement("RoadNode");
//            if (reader.Name.Equals("Name"))
//            {
//                reader.ReadElementContentAsString();
//                //roadNode.Name = string.IsNullOrEmpty(name) ? "" : name;
//            }
//            var latitude = double.NaN;
//            var longitude = double.NaN;
//            if (reader.Name.Equals("Latitude"))
//            {
//                latitude = reader.ReadElementContentAsDouble();
//            }
//            if (reader.Name.Equals("Longitude"))
//            {
//                longitude = reader.ReadElementContentAsDouble();
//            }
//            roadNode.Vertex = new Point(longitude,latitude);//new LatitudeLongitude(latitude, longitude));
//            if (reader.Name.Equals("NodeID"))
//            {
//                var id = reader.ReadElementContentAsInt();
//                roadNode.NodeID = id;
//            }
//            reader.ReadEndElement();
//            reader.ReadEndElement();
//            return roadNode;
//        }
//        
//        protected void ReadLegacy122RoadGraph(XmlReader reader)
//        {
//            if (!reader.Name.Equals("RoadGraph")) return;
//            reader.ReadStartElement();
//            while (reader.IsStartElement("Edge"))
//            {
//                reader.ReadStartElement("Edge");
//                RoadSegment edge;
//
//                var node1 = ReadeLegacy122RoadNode("edgeStart", reader);
//                var node2 = ReadeLegacy122RoadNode("edgeEnd", reader);
//
//                var tag = "";
//                if (reader.Name.Equals("tag"))
//                {
//                    reader.ReadStartElement();
//                    tag = reader.ReadElementContentAsString();
//                    reader.ReadEndElement();
//                }
//                graph.AddVertex(node1);
//                graph.AddVertex(node2);
//                edge = new RoadSegment(node1, node2);
//                edge.Name = tag;
//                AddEdge(edge);
//
//                double edgeWeight = Double.NaN;
//                while (reader.IsStartElement("EdgeWeight"))
//                {
//                    reader.ReadStartElement("EdgeWeight");
//                    reader.ReadStartElement("edgeWeight");
//                    edgeWeight = reader.ReadElementContentAsDouble();
//                    //                    edgeWeight = SerializationService.Deserialize<double>("edgeWeight", reader);
//                    reader.ReadEndElement();
//                    reader.ReadEndElement();
//                }
//                if (edgeWeight.Equals(UNUSABLE_ROAD_WEIGHT))
//                {
//                    DisableEdge(edge);
//                }
//                reader.ReadEndElement();
//            }
//            reader.ReadEndElement();
//            return;
//        }
        #endregion
        //Version 1.3.2 and up cannot use road graphs from previous versions

        public const string ns = "http://depiction.com";
        public void ReadXml(XmlReader reader)
        {
            var roadGraphVersion = reader.GetAttribute("Version");
            //bool ignoreRoadGraph = false;
            //if (!string.IsNullOrEmpty(roadGraphVersion) && roadGraphVersion.Equals(CurrentRoadGraphVersion) )
            //{
            //}
            //else
            //{
            //    ignoreRoadGraph = true;
            //}
            var startName = reader.Name;
            //Roadgraph is for 1.3
            //RoadGraph is for 1.2
            //if (ignoreRoadGraph && (reader.Name.Equals("Roadgraph")||reader.Name.Equals("RoadGraph")) )
            //{
            //    DepictionAccess.NotificationService.DisplayMessageString(
            //        "Unable to read road graph, please reload the RoadNetwork", 7);
            //    while (!(reader.NodeType.Equals(XmlNodeType.EndElement) && reader.Name.Equals(startName)))
            //    {
            //        reader.Read();
            //    }
            //    if (reader.NodeType.Equals(XmlNodeType.EndElement) && reader.Name.Equals(startName))
            //    {
            //        reader.ReadEndElement();
            //    }
            //    return;
            //}

            string version;
            if (reader.Name.Equals("Roadgraph"))
            {
                version = reader.GetAttribute("Version");  //must read the attribute before calling ReadStartElement
                reader.ReadStartElement();
            }
            else if (reader.Name.Equals("RoadGraph"))//RoadGraph with capital "G" comes from 1.2
            {
                //TODO give error
//                ReadLegacy122RoadGraph(reader);
                return;
            }
            else
            {
                //Hpefully a nice way to exit if an error occurs
                while (!reader.NodeType.Equals(XmlNodeType.EndElement) && reader.Name.Equals(startName))
                {
                    reader.Read();
                }
                return;
            }
            #region modern reading
            while (reader.Name.Equals("Edge"))//reader.IsStartElement("Edge", ns))
            {
                RoadSegment edge;


                var disabledString = reader.GetAttribute("disabled");
                var tag = reader.GetAttribute("tag");//case dependent
                reader.ReadStartElement("Edge", ns);
                
                if (version == "1.3")
                {

                    var node1 = new RoadNode();// SerializationService.DeserializeObject(typeof(RoadNode), reader) as RoadNode;
                    node1.ReadXml(reader);
                    var node2 = new RoadNode();//SerializationService.DeserializeObject(typeof(RoadNode), reader) as RoadNode);
                    node2.ReadXml(reader);
                    edge = new RoadSegment(node1, node2);
                    edge.Name = tag;
                }
                else if (version == "1.3.2")
                {
                    edge =new RoadSegment();//SerializationService.DeserializeObject(typeof(RoadSegment), reader) as RoadSegment;
                    edge.ReadXml(reader);
                }
                else
                {
                    //der....
                    return;
                }

                AddEdge(edge);//If it was two way it was added twice durign the save

                if (disabledString != null && disabledString.Equals("true"))
                    DisableEdge(edge);
                reader.ReadEndElement();
            }
            reader.ReadEndElement();
            #endregion
        }

        public void WriteXml(XmlWriter writer)
        {
//            var ns = SerializationConstants.DepictionXmlNameSpace;
//            writer.WriteStartElement("Roadgraph", ns);
//
//            writer.WriteAttributeString("Version", "1.3.2");
//            foreach (var edge in Graph.Edges)
//            {
//                WriteEdgeAsXml(writer, edge);
//            }
//            writer.WriteEndElement();
        }

        private void WriteEdgeAsXml(XmlWriter writer, RoadSegment edge)
        {
//            var ns = SerializationConstants.DepictionXmlNameSpace;
//            writer.WriteStartElement("Edge", ns);
//            if (disabledEdges.Contains(edge))
//            {
//                writer.WriteAttributeString("disabled", "true");
//            }
//            SerializationService.SerializeObject(edge, edge.GetType(), writer);
//            writer.WriteEndElement();
        }
        #endregion
    }
}
