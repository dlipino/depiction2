﻿using System.Windows;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface IDepictionImageMetadata//: IDeepCloneable<IDepictionImageMetadata>
    {
        string DepictionImageName { get; }
        string ImageFilename { get; set; }//should this be a depiction specific name?
        double TopLeftLatitude { get; set; }

        double TopLeftLongitude { get; set; }
        double BottomRightLatitude { get; set; }
        double BottomRightLongitude { get; set; }
        double RotationDegrees { get; set; }
        bool IsGeoReferenced { get; }
        bool CanBeManuallyGeoAligned { get; }
        Rect GeoBounds { get; }
    }
}