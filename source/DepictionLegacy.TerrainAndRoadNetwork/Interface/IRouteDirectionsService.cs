﻿using System.Collections.Generic;
using DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface IRouteDirectionsService
    {
        string GetRouteDirections(IList<IList<RoadSegment>> list, IList<double> completeRouteTimes);
    }
}
