﻿using System.Collections.ObjectModel;
using System.Windows;
using DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork;
using QuickGraph;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface IRoadGraph
    {
        AdjacencyGraph<RoadNode, RoadSegment> Graph { get; }
        void DisableEdge(RoadSegment edge);
        RoadNode FindNearestNode(RoadNode inCoord);
        RoadNode FindNearestNode(Point inCoord);
        IRouteFinder RouteFinder { get; }
        bool ContainsVertex(RoadNode vertex);
        double GetEdgeWeight(RoadSegment edge);
//        Func<RoadSegment, double> GetDriveTimeFunc(ITempElement route);
        ReadOnlyCollection<RoadSegment> DisabledEdges { get; }
    }
}
