using System.Windows;
using System.Windows.Media.Imaging;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface IGridSpatialData : IDepictionSpatialData
    {
        Point TopLeft { get; }
        Point BottomRight { get; }
        int PixelWidth { get; }
        int PixelHeight { get; }
        BitmapSource GenerateBitmap();
        byte GetValue(double longitude, double latitude);
//        byte GetValue(Point pos);
        byte GetValueAtColumnRow(int column, int row);
        float y(int yVal);
        float x(int xVal);
    }
}