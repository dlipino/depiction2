﻿using Depiction2.Base.Geo;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    /// <summary>
    /// The interface used to describe a piece of spatial data are used by Depiction.
    /// man this feels so horrible to do, so legacy its leaving a mark
    /// </summary>
    public interface IDepictionSpatialData
    {
        /// <summary>
        /// Does this piece of spatial data intersect with this other piece of spatial data?
        /// </summary>
        /// <param name="g">The other piece of spatial data to compare with this.</param>
        /// <returns></returns>
        bool Intersects(IDepictionSpatialData g);

        /// <summary>
        /// The smallest rectangle aligned north and south that contains this piece of spatial data.
        /// </summary>
        ICartRect BoundingBox { get; }

        /// <summary>
        /// Is there no spatial data for this object?
        /// </summary>
        bool IsEmpty { get; }

        /// <summary>
        /// Is this a valid piece of spatial data?
        /// Some polygons, for instance, may be invalid because their vertices overlap each other.
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// The DepictionGeometryType as defined by IGeometry, or "Coverage" for GridSpatialData.
        /// </summary>
        string GeometryType { get; }
    }
}