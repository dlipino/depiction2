﻿using System.Windows;

namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface ICoverage
    {
        int GetGridWidthInPixels();
        int GetGridHeightInPixels();
        int GetRow(double lat);
        int GetColumn(double lon);
        float GetValueAtGridCoordinate(int col, int row);
        void SetValueAtGridCoordinate(int col, int row, float value);
        float GetInterpolatedElevationValue(Point latitudeLongitude);
        float GetConvolvedValue(int col, int row, int kernel);
        Point GetTopLeftPosition();
        Point GetBottomRightPosition();

        string GetProjectionInWkt();
        bool IsFloatMode();
        double GetGridResolution();
        double GetLatitude(int row);
        double GetLongitude(int col);
        void ClearVisual();
        string GetValueForDisplay(Point position);//TODO pass more information to make the results setting independent
    }
}