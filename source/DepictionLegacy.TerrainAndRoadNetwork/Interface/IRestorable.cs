﻿namespace DepictionLegacy.TerrainAndRoadNetwork.Interface
{
    public interface IRestorable
    {
        //void Restore();
        void Restore(bool notifyPropertyChange);
    }
}
