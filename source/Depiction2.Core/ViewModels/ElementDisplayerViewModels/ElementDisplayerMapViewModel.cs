﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.Utilities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels.ElementDisplayerViewModels
{
    public class ElementDisplayerMapViewModel : ViewModelBase
    {
        public event Action<string> RequestDisplayDialog;
        private IElementDisplayer displayerModel = null;
        #region View model stuff

        private Rect _drawBounds = Rect.Empty;
        protected double displayerOpacity = 1;
        protected int displayerZIndex = DrawZIndexes.MainDisplayerBaseZ;

        #region properties
        virtual public DisplayerViewType ShapeType
        {
            get
            {
                return DisplayerViewType.Global;
            }
        }
        virtual public double DisplayerOpacity { get { return displayerOpacity; } set { displayerOpacity = value; } }
        public string DisplayerName { get { return displayerModel.DisplayerName; } set { displayerModel.DisplayerName = value; } }
        public string DisplayerId { get { return displayerModel.DisplayerId; } }
        public int DisplayerZIndex
        {
            get { return displayerZIndex; }
            set
            {
                displayerZIndex = value;
                OnPropertyChanged("DisplayerZIndex");
            }
        }
        public ReadOnlyObservableSpatialItemSource<MapElementViewModel> LiveElements { get; private set; }

        #region zindex properties


        #endregion

        //These distances are in the current measurement units distance
        //the values for the distances should differ depending on the coordinate space that the map is in. 
        //In order to actually complete things, one value will be used (top and left side).
        public double GeoDisplayWidth
        {
            get
            {
                if (displayerModel == null) return DrawWidth;
                return Math.Round(DepictionPositionService.Instance.DistanceBetween(displayerModel.GeoRectBounds.LeftTop,
                                                                         displayerModel.GeoRectBounds.RightTop,
                                                                         Settings.Default.MeasurementSystem,
                                                                         MeasurementScale.Large), 3);// Settings.Default.MeasurementScale);
            }
            set
            {
                var inputDistance = value;
                if (displayerModel == null)
                {
                    //Do something clever, but don't change the value that is displayed, even better make the value non editable
                    return;
                }
                var leftTop = displayerModel.GeoRectBounds.LeftTop;
                var leftBottom = displayerModel.GeoRectBounds.LeftBottom;
                var newRightBottom = DepictionPositionService.Instance.TranslatePoint(leftBottom,
                                                                                  90, inputDistance,
                                                                                  Settings.Default.MeasurementSystem,
                                                                                  MeasurementScale.Large);
                displayerModel.SetBounds(leftTop, newRightBottom);
            }
        }
        public double GeoDisplayHeight
        {
            get
            {
                if (displayerModel == null) return DrawWidth;
                return Math.Round(DepictionPositionService.Instance.DistanceBetween(displayerModel.GeoRectBounds.LeftTop, displayerModel.GeoRectBounds.LeftBottom,
                                                                         Settings.Default.MeasurementSystem,
                                                                        MeasurementScale.Large), 3);// Settings.Default.MeasurementScale);
            }
            set
            {
                var inputDistance = value;
                if (displayerModel == null)
                {
                    //Do something clever, but don't change the value that is displayed, even better make the value non editable
                    return;
                }
                var leftTop = displayerModel.GeoRectBounds.LeftTop;
                var rightTop = displayerModel.GeoRectBounds.RightTop;
                var newRightBottom = DepictionPositionService.Instance.TranslatePoint(rightTop,
                                                                                  180, inputDistance,
                                                                                  Settings.Default.MeasurementSystem,
                                                                                  MeasurementScale.Large);
                displayerModel.SetBounds(leftTop, newRightBottom);
            }
        }

        #region bound properties

        public double DrawTop
        {
            get
            {
                return _drawBounds.Top;
            }
        }

        public double DrawLeft
        {
            get
            {
                return _drawBounds.Left;
            }
        }

        public double DrawWidth
        {
            get
            {
                return _drawBounds.Width;
            }
        }

        public double DrawHeight
        {
            get
            {
                return _drawBounds.Height;
            }
        }

        public Rect DrawBounds
        {
            get
            {
                return _drawBounds;
            }
        }
        #endregion

        #endregion
        #region constructor/destructor


        //There is an oddity i don't understand, why does the view model have the observable quad tree orginal source, shouldnt that be in the model?
        //ie shouldn't the model know what elements are in the current display area, or is it up to the view model to know this info?
        public ElementDisplayerMapViewModel(IElementDisplayer displayer, ObservableCollection<MapElementViewModel> elementSource)
        {
            displayerModel = displayer;

            LiveElements = new ReadOnlyObservableSpatialItemSource<MapElementViewModel>(elementSource);
            LiveElements.AddIds(displayer.ElementIds);
            UpdateDrawBounds();
            displayerModel.PropertyChanged += model_PropertyChanged;
            displayerModel.DisplayedElementsChangedIdList += ModelDisplayedElementsChangedIdList;
        }

        protected override void OnDispose()
        {
            displayerModel.PropertyChanged -= model_PropertyChanged;
            displayerModel.DisplayedElementsChangedIdList -= ModelDisplayedElementsChangedIdList;
            base.OnDispose();
        }
        #endregion

        #region Constructor/dispose events
        void ModelDisplayedElementsChangedIdList(object sender, NotifyCollectionChangedEventArgs e)
        {

            if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                var oldIdList = e.OldItems.Cast<string>();
                LiveElements.RemoveIds(oldIdList);
            }
            if (e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                var newIdList = e.NewItems.Cast<string>();
                LiveElements.AddIds(newIdList);
            }
        }

        protected virtual void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("GeoRectBounds"))
            {
                UpdateDrawBounds();
            }
            else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }
        #endregion

        public void UpdateDrawBounds()
        {

            var story = DepictionAccess.DStory;
            if (story == null)
            {//hopefully in test
                _drawBounds = LocationConverter.ConvertCartesianToDrawRect(displayerModel.GeoRectBounds);
            }
            else
            {
                var t = LocationConverter.ConvertCartesianToDrawRect(displayerModel.GeoRectBounds);
                _drawBounds = GeoToDrawLocationService.ConvertGCSRectToDrawRect(displayerModel.GeoRectBounds);//, story.StoryProjector);
            }

            OnPropertyChanged(() => DrawTop);
            OnPropertyChanged(() => DrawLeft);
            OnPropertyChanged(() => DrawHeight);
            OnPropertyChanged(() => DrawWidth);
            //the distance width/height
            OnPropertyChanged(() => GeoDisplayHeight);
            OnPropertyChanged(() => GeoDisplayWidth);
            //This one does not exist, but it is used in the view to change clip size
            OnPropertyChanged("RevealerClip");
        }

        public void UpdateGeoBoundsFromDrawRect(Rect drawBounds)
        {
            ICartRect cartBounds;
            var story = DepictionAccess.DStory;

            if (story == null)
            {
                cartBounds = LocationConverter.ConvertPanelToCartisianRect(drawBounds);
            }
            else
            {
                cartBounds = GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(drawBounds);
            }
            var tl = new Point(cartBounds.Left, cartBounds.Top);
            var br = new Point(cartBounds.Right, cartBounds.Bottom);
            displayerModel.SetBounds(tl, br);
        }

        protected void RequestShowDisplayerInformation()
        {
            if (RequestDisplayDialog != null)
            {
                RequestDisplayDialog(DisplayerId);
            }
        }
        #endregion
    }
}