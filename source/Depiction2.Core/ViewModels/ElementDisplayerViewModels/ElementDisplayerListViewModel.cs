﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.ElementDisplayerViewModels
{
    public class ElementDisplayerListViewModel : ViewModelBase
    {
        #region variables
        public IElementDisplayer displayerModel = null;//hmmmm not so sure about this
        #endregion

        #region properties

        public string DisplayerName { get { return displayerModel.DisplayerName; } set { displayerModel.DisplayerName = value; } }
        public string DisplayerId { get { return displayerModel.DisplayerId; } }
        #endregion

        #region constructor and dispose

        public ElementDisplayerListViewModel(IElementDisplayer model)
        {
            displayerModel = model;
        }

        #endregion

        #region constructor event handlers

        #endregion
    }
}