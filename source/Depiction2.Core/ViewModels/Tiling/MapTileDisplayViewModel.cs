﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service.Tiling;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels.Tiling
{
    public class MapTileDisplayViewModel : ViewModelBase
    {
        private IStory _mainStory;
        private TilingService _mainTiler;
        private const int MaximumTilesOnCanvas = 100;
        private ObservableCollection<TileViewModel> tiles = new ObservableCollection<TileViewModel>();
        private int _lastZoomLevel = -1;

        #region props

        public IList<TileViewModel> Tiles { get { return tiles; } }
        #endregion

        #region constructor
        public MapTileDisplayViewModel(TilingService mainTiler, IStory activeStory)
        {
            _mainTiler = mainTiler;
            _mainStory = activeStory;
            _mainStory.PropertyChanged += _mainStory_PropertyChanged;
            mainTiler.DataChanged += FetcherOnDataChanged;
            mainTiler.PropertyChanged += mainTiler_PropertyChanged;
        }
        protected override void OnDispose()
        {
            _mainStory.PropertyChanged -= _mainStory_PropertyChanged;
            _mainTiler.DataChanged -= FetcherOnDataChanged;
            _mainTiler.PropertyChanged -= mainTiler_PropertyChanged;
            base.OnDispose();
        }

        void mainTiler_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Tiler"))
            {
                tiles.Clear();
                GetTileVisualsForStoryRegion();
            }
        }
        #endregion
        #region even methods
        void _mainStory_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StoryFocalPoint") || e.PropertyName.Equals("StoryZoom"))
            {
                GetTileVisualsForStoryRegion();
            }
        }
        #endregion

        public void GetTileVisualsForStoryRegion()
        {
            var bounds = GeoToDrawLocationService.GetStoryGeoExtent();
            if (bounds.Size.Equals(new Size(0,0))) return;
            var viewPaneWidth = GeoToDrawLocationService.ScreenPixelSize.Width;
            var tilesAcross = Convert.ToInt32((viewPaneWidth / (_mainTiler.PixelWidth * 2)));
            if (tilesAcross == -1)
            {
                return;
            }
            RefreshVisibleArea(_mainTiler.GetZoomLevel(bounds, tilesAcross));
            _mainTiler.DoSingleTileEvent(bounds, tilesAcross);
        }

        private void FetcherOnDataChanged(TileModel tileModel)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action<TileModel>(CreateTileViewModel), tileModel);
            }
        }

        private void CreateTileViewModel(TileModel tileModel)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(tileModel.Image);
            bitmapImage.EndInit();
            bitmapImage.Freeze();

            var topLeft = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(
                new Point(tileModel.TopLeft.Longitude, tileModel.TopLeft.Latitude));//, _projectionService);
            var bottomRight = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(
               new Point(tileModel.BottomRight.Longitude, tileModel.BottomRight.Latitude));//, _projectionService);
            var tileViewModel = new TileViewModel(tileModel, topLeft, bottomRight);
            tileViewModel.TileSource = bitmapImage;
            if (!tiles.Contains(tileViewModel))
            {
                tiles.Add(tileViewModel);
            }
        }
        public void RefreshVisibleArea(int zoomLevel)
        {
            if (Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<int>(RefreshVisibleArea), zoomLevel);
                return;
            }
            if (zoomLevel == -1)
            {
                //do something useful?
            }
            if (_lastZoomLevel != zoomLevel)
            {
                ZoomLevelChange(zoomLevel);
                _lastZoomLevel = zoomLevel;
            }
            EnforceMaximumTiles();
        }

        private void EnforceMaximumTiles()
        {
            if (tiles.Count > MaximumTilesOnCanvas)
                for (int i = 0; i < tiles.Count - MaximumTilesOnCanvas; i++)
                    tiles.RemoveAt(0);
        }
        private void ZoomLevelChange(int currentZoomLevel)
        {
            List<TileViewModel> tilesToRefresh = new List<TileViewModel>();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].ImageOpened && tiles[i].ZoomLevel == currentZoomLevel)
                {
                    tilesToRefresh.Add(tiles[i]);
                    tiles.RemoveAt(i);
                    i--;
                }
            }
            foreach (var tile in tilesToRefresh)
            {
                tiles.Add(tile);
            }
        }

    }
}