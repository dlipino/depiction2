﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.Core.ViewModels.Tiling
{
    public class TileViewModel : IEquatable<TileViewModel>
    {
        private readonly TileModel tileModel;

        public TileViewModel(TileModel tileModel, Point topLeftDraw, Point bottomRightDraw)
        {
            this.tileModel = tileModel;
            Point topLeftPos = topLeftDraw;
            Point bottomRightPos = bottomRightDraw;
            Top = topLeftPos.Y;
            Left = topLeftPos.X;
            Width = Math.Abs(bottomRightPos.X - topLeftPos.X);
            Height = Math.Abs(topLeftPos.Y - bottomRightPos.Y);
        }


        public TileModel TileModel { get { return tileModel; } }

        public double Width { get; set; }
        public double Height { get; set; }
        public double Top { get; set; }
        public double Left { get; set; }

        public BitmapSource TileSource { get; set; }

        public bool ImageOpened { get; set; }

        public int ZoomLevel { get { return tileModel.TileZoomLevel; } }
        public bool Equals(TileViewModel other)
        {
            return Top.Equals(other.Top) && Left.Equals(other.Left) && tileModel.TileZoomLevel.Equals(other.tileModel.TileZoomLevel);
        }
    }
}
