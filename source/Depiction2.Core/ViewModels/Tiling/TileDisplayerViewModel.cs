﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Depiction2.API;
using Depiction2.API.Tools;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.Service.Tiling;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ExtensionFramework.Tiling;
using Depiction2.Core.Properties;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels.Tiling
{
    public class TileDisplayerViewModel : ViewModelBase
    {
        #region Variables
        private BackgroundWorker eventFilterWorker = null;
        private ObservableCollection<TileViewModel> tiles = new ObservableCollection<TileViewModel>();
        private const int MaximumTilesOnCanvas = 100;
        private readonly List<ITiler> allTileProvidersStorage = new List<ITiler>();
        //        private TileImageTypes selectableTileProviderType = TileImageTypes.Unknown;
        private int lastZoomLevel;
        private TileFetcher fetcher;
        private ITiler tiler;
        //        private ObservableCollection<ITiler> selectableTileProviders = new ObservableCollection<ITiler>();
        private TileImageTypes tileProviderType;
        private ITiler selectedTileProvider;
        private HashSet<TileImageTypes> availableTilerTypes = new HashSet<TileImageTypes>();
        private IDepictionStoryProjectionService _projectionService = null;
        #endregion

        #region properties
        public IList<TileViewModel> Tiles { get { return tiles; } }
        public HashSet<TileImageTypes> AvailableTilerTypes { get { return availableTilerTypes; } }
        //The tiler that is getting selected, can probably be reduced to just a name
        //Not sure why these overlap,i think to keep from starting a tiler unnecessarily 
        public ITiler SelectedTileProvider
        {
            get { return selectedTileProvider; }
            set
            {
                OnPropertyChanged("SelectedTileProvider");//hack for ui change
                if (value == null) return;
                if (selectedTileProvider == null)
                {
                    selectedTileProvider = value;
                    OnPropertyChanged("SelectedTileProvider");
                    Tiler = value;
                    return;
                }
                if (!selectedTileProvider.Equals(value))
                {
                    selectedTileProvider = value;
                    OnPropertyChanged("SelectedTileProvider");
                    Tiler = value;
                }
            }
        }
        //The active tiler
        public ITiler Tiler
        {
            get { return tiler; }
            set
            {
                //                if (tiler.Equals(value)) return;
                tiler = value;
                tiles.Clear();
                if (fetcher != null)
                {
                    fetcher.AbortFetch();
                    fetcher.DataChanged -= FetcherOnDataChanged;
                    fetcher = null;

                }
                if (tiler == null)
                    return;
                if (!(tiler is EmptyTiler))
                {
                    fetcher = new TileFetcher(tiler);

                    fetcher.DataChanged += FetcherOnDataChanged;
                    bounds = GeoToDrawLocationService.GetStoryGeoExtent();//_mainStory.StoryProjector);
                    DoSingleTileEvent();
//                    if (DepictionAccess.DStory != null)
//                        TileVisibleArea(GeoToDrawLocationService.GetStoryGeoExtent(_mainStory.StoryProjector));
                }
                Settings.Default.TilingSourceName = tiler.DisplayName;
                Settings.Default.Save();
                OnPropertyChanged("Tiler");
            }
        }
        public TileImageTypes TileProviderType
        {
            get { return tileProviderType; }
            set
            {
                if (tileProviderType == value) return;
                tileProviderType = value;
                OnPropertyChanged("SelectableTileProviders");
                OnPropertyChanged("SelectedTileProvider");
                OnPropertyChanged("TileProviderType");
            }
        }

        public List<ITiler> SelectableTileProviders
        {
            get { return allTileProvidersStorage.Where(t => t.TileImageType.Equals(tileProviderType)).ToList(); }
        }

        //        public TileImageTypes SelectableTileProviderType
        //        {
        //            get { return selectableTileProviderType; }
        //            set
        //            {
        //                if (selectableTileProviderType == value) return;
        //
        //                selectableTileProviderType = value;
        //
        //                UpdateSelectableTileProviders(selectableTileProviderType);
        //
        //                NotifyPropertyChanged("SelectableTileProviderType");
        //                if (Tiler != null && Tiler.TileImageType.Equals(selectableTileProviderType)) return;
        //
        //                if (SelectableTileProviders != null && SelectableTileProviders.Any())
        //                {
        //                    Tiler = SelectableTileProviders.First();
        //                }
        //            }
        //        }
        //        public ObservableCollection<ITiler> SelectableTileProviders { get { return selectableTileProviders; } }
        #endregion
        #region Constructor

        //        private DepictionMapGridVm mapVM;
        private IStory _mainStory;

        public TileDisplayerViewModel(IStory storyModel)//IDepictionStoryProjectionService projectionService)
        {
            //            this.mapVM = mapVM;
            _mainStory = storyModel;
            _mainStory.PropertyChanged += _mainStory_PropertyChanged;
            //            mapVM.PropertyChanged += mapVM_PropertyChanged;

            var empty = new EmptyTiler();
            allTileProvidersStorage.Add(empty);
            availableTilerTypes.Add(TileImageTypes.None);
//            throw new NotImplementedException();
//            _projectionService = storyModel.StoryProjector;
//            var tilers = DepictionAccess.ExtensionLibrary.

//            AddToAvailableTilers(new[] { new OpenStreetMapTiler() });
            TileProviderType = TileImageTypes.None;// TileImageTypes.Street;
            Tiler = SelectableTileProviders[0];
        }

        protected override void OnDispose()
        {
            _mainStory.PropertyChanged -= _mainStory_PropertyChanged;
            tiles.Clear();
            if (fetcher != null)
            {
                fetcher.AbortFetch();
                fetcher.DataChanged -= FetcherOnDataChanged;
                fetcher = null;

            }
            base.OnDispose();
        }
        Rect bounds;
        void _mainStory_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StoryFocalPoint") || e.PropertyName.Equals("StoryZoom"))
            {
                Debug.WriteLine("prop change");
//                var extent = GeoToDrawLocationService.GetGeoExtent(_mainStory.StoryProjector);
//                var topLeft = mapVM.TopLeftLatLon());
//                var bottomRight =  mapVM.BottomRightLatLon();

                bounds = GeoToDrawLocationService.GetStoryGeoExtent();//_mainStory.StoryProjector); //new Rect(topLeft, bottomRight);

                Debug.WriteLine(bounds.TopLeft + " " + bounds.BottomRight + " for tiling");
                DoSingleTileEvent();
                //TileVisibleArea(bounds);
            }
        }

        void DoSingleTileEvent()
        {
            if (eventFilterWorker == null)
            {
                eventFilterWorker = new BackgroundWorker();
                eventFilterWorker.DoWork += eventFilterWorker_DoWork;
                eventFilterWorker.RunWorkerAsync();
            }
        }
        void eventFilterWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime now = DateTime.Now;
            while (DateTime.Now < now + new TimeSpan(0, 0, 0, 0, 500))
            {
                Thread.Sleep(100);
            }
            Debug.WriteLine("tiling");
            TileVisibleArea(bounds);
            eventFilterWorker = null;
        }

        #endregion

        public void AddToAvailableTilers(ITiler[] tilers)
        {

            if (tilers != null)
            {
                foreach (var tiler in tilers)
                {
                    SelectableTileProviders.Add(tiler);
                    allTileProvidersStorage.Add(tiler);
                    AvailableTilerTypes.Add(tiler.TileImageType);
                }
            }
            SelectLastOrDefaultTiler();
        }

        protected void SelectLastOrDefaultTiler()
        {
            //Overall semi hackish because of legacy code and settings
            var tilerToLoadName = Settings.Default.TilingSourceName;
            ITiler selectedTiler = null;
            //Ugly stuff, really ugly stuff, and have not intention of making pretty yet
            if (string.IsNullOrEmpty(tilerToLoadName))
            {
                selectedTiler = SelectFirstAvailableTilerAndUpdateTileService();
            }
            else
            {
                var matchingTilers = allTileProvidersStorage.Where(t => t.DisplayName.Equals(tilerToLoadName)).ToList();
                if (!matchingTilers.Any())
                {
                    selectedTiler = SelectFirstAvailableTilerAndUpdateTileService();
                }
                else
                {
                    var firstMatch = matchingTilers.First();

                    var tilerType = firstMatch.TileImageType;
                    TileProviderType = tilerType;
                    Tiler = firstMatch;
                    selectedTiler = firstMatch;
                }
            }
            if (selectedTiler != null)
            {
                UpdateSelectableTileProviders(selectedTiler.TileImageType);
                Settings.Default.TilingSourceName = selectedTiler.DisplayName;
            }

            //            NotifyPropertyChanged("SelectableTileProviders"); NotifyPropertyChanged("SelectedTileProvider");
        }

        private ITiler SelectFirstAvailableTilerAndUpdateTileService()
        {
            //There should always be something in thislist.
            var firstTiler = allTileProvidersStorage.First();

            var tilerType = firstTiler.TileImageType;
            TileProviderType = tilerType;
            Tiler = firstTiler;
            return firstTiler;
        }

        private void UpdateSelectableTileProviders(TileImageTypes tilerImageType)
        {
            //If type does not match, select the first one that does.
            //kind of a hack
            var matchingTilers =
                allTileProvidersStorage.Where(t => t.TileImageType.Equals(tilerImageType));
            //Hopefully this doesnt cause a race condition, or something bad like that
            SelectableTileProviders.Clear();
            foreach (var matchingTiler in matchingTilers)
            {
                SelectableTileProviders.Add(matchingTiler);
            }
        }

        private void FetcherOnDataChanged(TileModel tileModel)
        {
            if (Application.Current != null)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action<TileModel>(OnDataChanged), tileModel);
            }
        }

        private void OnDataChanged(TileModel tileModel)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = new MemoryStream(tileModel.Image);
            bitmapImage.EndInit();
            bitmapImage.Freeze();
            //bitmapImage.SetSource(new MemoryStream(tileModel.Image));
            //            var topLeft = mapVM.GetProjectedPoint(tileModel.TopLeft.Longitude, tileModel.TopLeft.Latitude);
            //            var bottomRight = mapVM.GetProjectedPoint(tileModel.BottomRight.Longitude, tileModel.BottomRight.Latitude);

            var topLeft = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(
                new Point(tileModel.TopLeft.Longitude, tileModel.TopLeft.Latitude));//, _projectionService);
            var bottomRight = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(
               new Point(tileModel.BottomRight.Longitude, tileModel.BottomRight.Latitude));//, _projectionService);
            var tileViewModel = new TileViewModel(tileModel, topLeft, bottomRight);
            tileViewModel.TileSource = bitmapImage;
            if (!tiles.Contains(tileViewModel))
            {
                tiles.Add(tileViewModel);
            }
        }

        public void TileVisibleArea(Rect currentExtent)
        {
            if (Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<Rect>(TileVisibleArea), currentExtent);
                return;
            }
            if (tiler == null) return;
            if (tiler.PixelWidth == 0) return;
            var viewPaneWidth = GeoToDrawLocationService.ScreenPixelSize.Width; 
            var tilesAcross = Convert.ToInt32((int)(viewPaneWidth / (tiler.PixelWidth * 2)));
            var currentZoomLevel = Tiler.GetZoomLevel(currentExtent,
                                       tilesAcross, 18);
            if (lastZoomLevel != currentZoomLevel)
            {
                ZoomLevelChange(currentZoomLevel);
                lastZoomLevel = currentZoomLevel;
            }
            EnforceMaximumTiles();
            if (fetcher != null)
                fetcher.ViewChanged(currentExtent, tilesAcross);
        }

        private void EnforceMaximumTiles()
        {
            if (tiles.Count > MaximumTilesOnCanvas)
                for (int i = 0; i < tiles.Count - MaximumTilesOnCanvas; i++)
                    tiles.RemoveAt(0);
        }

        private void ZoomLevelChange(int currentZoomLevel)
        {
            List<TileViewModel> tilesToRefresh = new List<TileViewModel>();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].ImageOpened && tiles[i].ZoomLevel == currentZoomLevel)
                {
                    tilesToRefresh.Add(tiles[i]);
                    tiles.RemoveAt(i);
                    i--;
                }
            }
            foreach (var tile in tilesToRefresh)
            {
                tiles.Add(tile);
            }
        }
    }
}
