﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.Core.ViewModels.Tiling
{
    public class TilerSelectionViewModel : ViewModelBase
    {
        private TilingService _controllingService;
        private TileImageTypes tileProviderType = TileImageTypes.Unknown;

        public TileImageTypes TileProviderType
        {
            get { return tileProviderType; }
            set
            {
                tileProviderType = value;
                OnPropertyChanged("TileProviderType");
                OnPropertyChanged("SelectableTileProviders");
            }
        }
        public IEnumerable<TileImageTypes> AvailableTilerTypes
        {
            get { return _controllingService.AvailableTilerTypes; }
        }
        public List<ITilerExtensionMetadata> SelectableTileProviders
        {
            get { return _controllingService.AllTilerInfo.Where(t => t.TilerType.Equals(tileProviderType)).ToList(); }
        }

        public ITilerExtensionMetadata SelectedTilerInfo
        {
            get { return _controllingService.CurrentTilerInfo; }
            set
            {
                if (value == null) return;
                _controllingService.SetTiler(value.Name);
                Settings.Default.TilingSourceName = value.Name;
                Settings.Default.Save();

                OnPropertyChanged("SelectedTilerInfo");
            }
        }


        public TilerSelectionViewModel(TilingService tileService)
        {
            _controllingService = tileService;
            _controllingService.PropertyChanged += _controllingService_PropertyChanged;
        }

        void _controllingService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("CurrentTilerInfo"))
           {
               OnPropertyChanged("SelectedTilerInfo");
           }
        }
    }
}