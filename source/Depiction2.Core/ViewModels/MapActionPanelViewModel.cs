﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction.ProjectionSystem;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.Scaffold;
using Depiction2.Core.Service;
using Depiction2.Core.ViewModels.MapActionViewModels;

namespace Depiction2.Core.ViewModels
{
    public class MapActionPanelViewModel : ViewModelBase
    {
        private DepictionMapGridVm _mainMapVm;
        private IElementScaffold _scaffoldToAdd;
        private ImageRegistrationViewModel _imageRegistrationVm;

        public IElementScaffold ScaffoldToAdd
        {
            get { return _scaffoldToAdd; }
            set { _scaffoldToAdd = value; OnPropertyChanged(() => ScaffoldToAdd); }
        }
        //mass confusion between the mapvm and this thing
        public DepictionMapMode MapActionMode
        {
            get { return _mainMapVm.MapMode; }
            set
            {
                _mainMapVm.MapMode = value;
                OnPropertyChanged(() => MapActionMode);
            }
        }
        #region constructor
        public MapActionPanelViewModel(DepictionMapGridVm mainMapVm)
        {
            _mainMapVm = mainMapVm;
        }
        #endregion

        #region image registration
        public void StartImageRegistration(IElement imageElement)
        {
            _imageRegistrationVm = new ImageRegistrationViewModel(imageElement);
        }
        public void StopImageRegistration()
        {
            _imageRegistrationVm = null;
        }
        #endregion
        public void StartElementAdd(IElementScaffold scaffold)
        {
            MapActionMode = DepictionMapMode.ElementAdd;
            ScaffoldToAdd = scaffold;
        }
        public void StopelementAdd()
        {
            
        }

        public void StartRegionSelection()
        {

        }

        public void StartArbitraryRegionSelection()
        {

        }
        public void StopRegionSelection()
        {
            
        }

        public void ActionComplete(List<Point> actionPoints)
        {
            var geoPoints = ConvertPointListToGeoList(actionPoints);
            switch (MapActionMode)
            {
                case DepictionMapMode.ElementAdd:
                    var fullElement = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(ScaffoldToAdd, geoPoints);
                    if (fullElement.PostAddActions != null)
                    {
                        foreach (var action in fullElement.PostAddActions)
                        {
                            var key = action.Key;
                            var behavior = DepictionAccess.ExtensionLibrary.RetrieveBehavior(key);
                            if (behavior != null)
                            {
                                behavior.DoBehavior(fullElement, action.Value);
                            }
                        }
                    }
                    DepictionAccess.DStory.AddElement(fullElement, true);
                    OnPropertyChanged(() => MapActionMode);
                    break;
                case DepictionMapMode.AreaSelection:
                    break;
                case DepictionMapMode.ArbitraryAreaSelection:
                    break;
            }
            MapActionMode = DepictionMapMode.Normal;
        }
        #region helpers
        private List<Point> ConvertPointListToGeoList(List<Point> viewPortPointList)
        {
            var geoPointList = new List<Point>();
            if (!viewPortPointList.Any() || DepictionAccess.DStory == null) return geoPointList;
            var offset = DepictionAccess.DStory.StoryTopLeft;
            var scale = DepictionAccess.DStory.StoryZoom;
            foreach (var p in viewPortPointList)
            {
                var geoPoint = LocationConverter.ConvertCanvasPointToMapDrawPoint(p, scale, offset);
                geoPointList.Add(geoPoint);
            }
            return geoPointList;
        }
        #endregion
    }
}