﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Utilities;

namespace Depiction2.Core.ViewModels.EditingViewModels
{
    public class GeometryEditCanvasViewModel : ViewModelBase
    {
        public event Action<List<IElement>> ShowPropertyRequest;
        private GeometryEditMode _editCanvasMode = GeometryEditMode.None;
        private IStory _mainStory;
        private IStoryActions _mainStoryActions;
        private IDialogService _dialogService;

        public GeometryEditMode EditCanvasMode { get { return _editCanvasMode; } set { _editCanvasMode = value; OnPropertyChanged("EditCanvasMode"); } }
        public GeometryType GeometryType { get; set; }
        public IElementTemplate ElementAddType { get; private set; }
        public IElement EditElement { get; private set; }
        public IDepictionRegion EditRegion { get; private set; }

        public IEnumerable<Point> EditPoints { get; set; }
        #region commands

        private DelegateCommand cancelEditCommand;
        private DelegateCommand completeEditCommand;
        public ICommand CompleteEditCommand
        {
            get
            {
                if (completeEditCommand == null)
                {
                    completeEditCommand = new DelegateCommand(RequestCompleteGeometryEdit);
                }
                return completeEditCommand;
            }
        }
        public ICommand CancelEditCommand
        {
            get
            {
                if (cancelEditCommand == null)
                {
                    cancelEditCommand = new DelegateCommand(CancelEdit);
                }
                return cancelEditCommand;
            }
        }

        private DelegateCommand cancelRegionCommand;
        private DelegateCommand completeRegionCommand;
        public ICommand CompleteRegionCommand
        {
            get
            {
                if (completeRegionCommand == null)
                {
                    completeRegionCommand = new DelegateCommand(CompleteRegion);
                }
                return completeRegionCommand;
            }
        }
        public ICommand CancelRegionCommand
        {
            get
            {
                if (cancelRegionCommand == null)
                {
                    cancelRegionCommand = new DelegateCommand(CancelRegion);
                }
                return cancelRegionCommand;
            }
        }
        #endregion
        #region constructor
        public GeometryEditCanvasViewModel(IStory story, IStoryActions storyActions, IDialogService dialogService)
        {
            _mainStory = story;
            _dialogService = dialogService;
            _mainStoryActions = storyActions;
        }
        protected override void OnDispose()
        {
            base.OnDispose();
        }

        #endregion
        public void RequestCompleteGeometryEdit()
        {
            OnPropertyChanged("RequestCompleteGeometryEdit");//this action is heard by the view
            //which then sends the correct info to the finishing method

        }
        public void CompleteRegion()
        {
            OnPropertyChanged("CompleteRegion");
        }
        public void CancelRegion()
        {
            CancelEdit();
        }
        public void CancelEdit()
        {
            ElementAddType = null;
            EditElement = null;
            EditCanvasMode = GeometryEditMode.None;
            //            OnPropertyChanged("CancleAction");
        }
        public void SetEditMode(IElement editElement)//IEnumerable<Point> editPointList)
        {
            EditElement = editElement;
            //it is also possible to do a direct geometry conversion.
            var geoPoints = editElement.ElementGeometry.GeometryPoints.ToList();
            if (geoPoints.Count < 2) return;
            var pcsPoints = new List<Point>();
            foreach (var geo in geoPoints)
            {
                var pc = CoordinateSystemService.Instance.PointStoryToDisplay(geo);
                pcsPoints.Add(pc);
            }
            EditPoints = pcsPoints;
            //because of poor coding the edit points must be set first
            EditCanvasMode = GeometryEditMode.Edit;
        }

        public void CompleteMultipleElementGeometryAdd(List<List<Point>> multiPcsPoints )
        {
            foreach(var pList in multiPcsPoints)
            {
                CompleteSingleElementGeometryAdd(pList);
            }
        }
        /***
         * takes in coordinates that are raw map canvas coordinates. For now they equal the Projection System that is currently in use.
         */
        public void CompleteSingleElementGeometryAdd(List<Point> pcsPoints )
        {
            //TODO the poinst really need to come in as gcs points or at least get converted
            //maybe write test to ensure useful input too?, although based on how hacky the code leading
            //up to this is, it might be difficult.
            var gcsPoints = new List<Point>();
            foreach (var p in pcsPoints)
            {
                gcsPoints.Add(CoordinateSystemService.Instance.PointDisplayToStory(p));
            }
            if (ElementAddType != null)
            {
                //Convert the points to geo
                _mainStoryActions.TriggerElementPlacement(ElementAddType, gcsPoints);
                var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(ElementAddType, gcsPoints);
                //                var story = DepictionAccess.DStory;
                //                if (story == null) return;
                //                story.AddElement(element, true);
                _mainStory.AddElement(element, true);
                //might also be a chance the user wants to control if this pops up
                if (ElementAddType.ShowPropertiesOnCreate)
                {
                    var propdialog = new SingleElementPropertiesDialogVm(element);
                    _dialogService.ShowWindow(null, propdialog);
                }
                EditCanvasMode = GeometryEditMode.None;
            }
        }
        public void CompleteEditGeometryOrRegionElementSelect(List<Point> pcsPoints)
        {
            //TODO the poinst really need to come in as gcs points or at least get converted
            //maybe write test to ensure useful input too?, although based on how hacky the code leading
            //up to this is, it might be difficult.
            var gcsPoints = new List<Point>();
            foreach (var p in pcsPoints)
            {
                gcsPoints.Add(CoordinateSystemService.Instance.PointDisplayToStory(p));
            }
  
            if (EditElement != null)
            {
                //im sure there is a better way of doing this, but this way requires very little thinking, and its almost bar time
                var geom = DepictionGeometryService.CreateGeometry(gcsPoints);//new GeometryGdalWrap(pcsPoints);
                EditElement.UpdatePrimaryPointAndGeometry(null, geom);
            }
            if (_editCanvasMode.Equals(GeometryEditMode.AreaElementSelection))
            {
                var geom = DepictionGeometryService.CreateGeometry(gcsPoints);//new GeometryGdalWrap(pcsPoints);
                var selectedIds = _mainStory.GetElementsInGeometry(geom);
                if (ShowPropertyRequest != null)
                {
                    ShowPropertyRequest(selectedIds.ToList());
                }
            }
            ElementAddType = null;
            EditElement = null;

        }

        public void StartAdd(IElementTemplate template, bool multi)
        {
            ElementAddType = template;
            var shape = template.ShapeType;
            //hmm elemetns that get added via point have polygon, 
            //TODO figure out a consistant way to add elements via mouse and hints from scaffold
            if (shape.ToLower().Contains("userpolygon"))
            {
                GeometryType = GeometryType.Polygon;
            }
            else if (shape.ToLower().Contains("userline"))
            {
                GeometryType = GeometryType.Polyline;
            }
            else
            {
                GeometryType = GeometryType.Point;
            }
            if (multi)
            {
                EditCanvasMode = GeometryEditMode.MultipleAdd;
            }
            else
            {
                EditCanvasMode = GeometryEditMode.SingleAdd;
            }
        }

        public void StartAreaElementSelection(GeometryType selectRegionType)
        {
            //order is important. set the geometry type before changing the canvasemode
            GeometryType = selectRegionType;
            EditCanvasMode = GeometryEditMode.AreaElementSelection;
        }
        public void CompleteAreaElementSelection(List<Point> pcsRegionPoints)
        {
            if (pcsRegionPoints == null) return;
            var gcsPoints = new List<Point>();
            foreach (var p in pcsRegionPoints)
            {
                gcsPoints.Add(CoordinateSystemService.Instance.PointDisplayToStory(p));
            }
            var areaGeometry = DepictionGeometryService.CreateGeometry(gcsPoints);
            if (areaGeometry == null) return;
            var selectedIds = _mainStory.GetElementsInGeometry(areaGeometry);
            if (ShowPropertyRequest != null)
            {
                ShowPropertyRequest(selectedIds.ToList());
            }
        }

        public void StartRegionCreation(IDepictionRegion regionOfInterest)
        {
            if (regionOfInterest == null)
            {
                var insideRect = RandomUtilities.GetRectInteriorToRect(new Rect(new Point(0, 0), GeoToDrawLocationService.ScreenPixelSize));
                EditPoints = RandomUtilities.ConvertRectToClosedPointList(insideRect, null, false);
            }
            else
            {
                EditRegion = regionOfInterest;
                //                var geoRect = regionOfInterest.GeoRectBounds;
                //                var tl = GeoToDrawLocationService.CartCanvasToScreen(geoRect.TopLeft);
                //                var br = GeoToDrawLocationService.CartCanvasToScreen(geoRect.BottomRight);
                //                var screenRect = new Rect(tl, br);
                var screenRect = GeoToDrawLocationService.ConvertGCSRectToDrawRect(regionOfInterest.GeoRectBounds);
                EditPoints = RandomUtilities.ConvertRectToClosedPointList(screenRect, null, false);
            }

            EditCanvasMode = GeometryEditMode.RegionCreate;
        }
        public void CompleteRegionCreation(Rect screenPixelRect)
        {
            if (_editCanvasMode.Equals(GeometryEditMode.RegionCreate))
            {
                var tl = GeoToDrawLocationService.ScreenToCartCanvas(screenPixelRect.TopLeft);
                var br = GeoToDrawLocationService.ScreenToCartCanvas(screenPixelRect.BottomRight);
                if (EditRegion == null)
                {
                    var r = new DepictionRegion(tl, br);
                    _mainStory.AddRegion(r);
                }
                else
                {
                    EditRegion.SetBounds(tl, br);
                    EditRegion.UpdateSourceData();
                }

                OnPropertyChanged("RegionComplete");
            }
            ElementAddType = null;
            EditElement = null;

            EditCanvasMode = GeometryEditMode.None;
        }

        #region helpers

        public Rect ClosedPointListToRect(IEnumerable<Point> inPoints)
        {
            var ps = inPoints.ToList();
            if (ps.Count != 5 && !ps.First().Equals(ps.Last())) return Rect.Empty;
            return new Rect(ps[0], ps[2]);
        }

        #endregion

    }
    public enum GeometryEditMode
    {
        None,
        SingleAdd,
        MultipleAdd,
        Edit,
        AreaElementSelection,
        RegionCreate
    }
    public enum GeometryType
    {
        None,
        Point,
        Polyline,
        Polygon,
        Rectangle
    }
}