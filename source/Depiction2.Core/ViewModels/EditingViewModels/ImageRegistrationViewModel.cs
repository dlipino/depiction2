﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.Core.ViewModels.EditingViewModels
{
    public class ImageRegistrationViewModel : ViewModelBase
    {
        public IElement _elementModel;
        private Rect screenImagePixelBounds = new Rect(0,0,0,0);
        private DelegateCommand restoreAspectRatioCommand;
        private DelegateCommand cancelRegistrationCommand;
        private DelegateCommand completeRegistrationCommand;
        private double imageOpacity = 50;
        private bool preserveAspectRatio = true;

        #region properties

        public double AspectRatio { get; set; }
        public double ImageSourceAspectRatio { get; set; }
        public double DisplayedAspectRatio { get; set; }
        public bool PreserveAspectRatio
        {
            get { return preserveAspectRatio; }
            set { preserveAspectRatio = value; OnPropertyChanged("PreserveAspectRatio"); }
        }
        public double ImageOpacity
        {
            get { return imageOpacity; }
            set { imageOpacity = value; OnPropertyChanged("ImageOpacity"); }
        }
        public Visibility HackVisibility
        {
            get
            {
                return _elementModel == null ? Visibility.Collapsed : Visibility.Visible;
            }
        }
        public double Width { get { return ScreenImagePixelBounds.Width; } }
        public double Height { get { return ScreenImagePixelBounds.Height; } }
        public double Left { get { return ScreenImagePixelBounds.Left; } }
        public double Top { get { return ScreenImagePixelBounds.Top; } }
        public RotateTransform Rotation { get; private set; }
        public Rect ScreenImagePixelBounds
        {
            get { return screenImagePixelBounds; }
            set
            {
                screenImagePixelBounds = value;
                OnPropertyChanged("Width");
                OnPropertyChanged("Height");
                OnPropertyChanged("Left");
                OnPropertyChanged("Top");
                OnPropertyChanged("ScreenImagePixelBounds");
            }
        }

        public List<Point> GeoCanvasImageBounds { get; set; } 
        public ImageSource ImageSource { get; set; }
        #region commands
        public ICommand RestoreAspectRatioCommand
        {
            get
            {
                if (restoreAspectRatioCommand == null)
                {
                    restoreAspectRatioCommand = new DelegateCommand(RestoreAspectRatio);
                }
                return restoreAspectRatioCommand;
            }
        }
        public ICommand CompleteImageRegistrationCommand
        {
            get
            {
                if(completeRegistrationCommand == null)
                {
                    completeRegistrationCommand = new DelegateCommand(CompleteRegistration);
                }
                return completeRegistrationCommand;
            }
        }
        public ICommand CancelImageRegistrationCommand
        {
            get
            {
                if(cancelRegistrationCommand == null)
                {
                    cancelRegistrationCommand = new DelegateCommand(CancelRegistration);
                }
                return cancelRegistrationCommand;
            }
        }
        #endregion

        #endregion

        #region action helpers

        public void RequestImageRegistration(IElement element)
        {
            _elementModel = element;
            _elementModel.VisualState = ElementVisualSetting.None;
            //very strange that this is done, but i guess there are some things
            //going on with the property dialog that need to be resolved
            //hack
            _elementModel.TriggerPropertyChange("VisualState");
            //the element geometry from the image is not rotated, that is done via the image
            OnPropertyChanged("HackVisibility");
            OnPropertyChanged("ImageElement");
        }
        public void UpdateRegistrationPixelBounds(Rect bounds)
        {
            ScreenImagePixelBounds = bounds;
            if (DepictionAccess.DStory == null || string.IsNullOrEmpty(_elementModel.ImageResourceName))
            {
                return;
            }
            var images = DepictionAccess.DStory.AllImages;
            if (images == null) return;
            ImageSource = images.GetImage(_elementModel.ImageResourceName);
            Rotation = new RotateTransform(_elementModel.ImageRotation);
            CalculateOriginalAspectRatio();
            OnPropertyChanged("ImageSource");
            OnPropertyChanged("Rotation");
        }
        public void CompleteRegistration()
        {
            if (GeoCanvasImageBounds == null) return;
            var geom = DepictionGeometryService.CreateGeometry(GeoCanvasImageBounds);//new GeometryGdalWrap(pcsPoints);
            var id = _elementModel.ElementId;
            //Bounds aren't the exact match, but should be close enough
            if(!Equals(geom.Bounds,_elementModel.ElementGeometry.Bounds) ||
                !Equals(Rotation.Angle,_elementModel.ImageRotation))
            {
                _elementModel.UpdatePrimaryPointAndGeometry(null, geom);
                _elementModel.ImageRotation = Rotation.Angle;   
            }
            
            _elementModel.VisualState = ElementVisualSetting.Image;
            _elementModel.TriggerPropertyChange("VisualState");
            _elementModel = null;
            //I consider this a hack, maybe some sort of an even might be nice
            //used to put the newly registred element onto the main viewer.
            if(DepictionAccess.DStory != null)
            {
                DepictionAccess.DStory.MainDisplayer.AddDisplayElementsWithMatchingIds(new List<string>{id});
            }
            OnPropertyChanged("HackVisibility");
            OnPropertyChanged("ImageElement");
        }
        public void CancelRegistration()
        {
            if(_elementModel.GeoLocation != null)
            {
                _elementModel.VisualState = ElementVisualSetting.Image;
                _elementModel.TriggerPropertyChange("VisualState");
            }
            _elementModel = null;
            OnPropertyChanged("HackVisibility");
            OnPropertyChanged("ImageElement");
        }
        #endregion
        protected void CalculateOriginalAspectRatio()
        {
            var w = ScreenImagePixelBounds.Width;
            var h = ScreenImagePixelBounds.Height;
            var wO = 0d;
            var hO = 0d;

            if (ImageSource == null)
            {
                ImageSourceAspectRatio = 1;
            }
            else
            {
                wO = ImageSource.Width;
                hO = ImageSource.Height;
                ImageSourceAspectRatio = wO / hO;
            }

            if (h.Equals(double.NaN) || h == 0 || w == 0 || w.Equals(double.NaN))
            {
                AspectRatio = ImageSourceAspectRatio;
            }
            else
            {
                if (_elementModel.GeoLocation == null)
                {
                    //What does this do?
                    if (wO != 0 && hO != 0)
                    {
                        var centerX = ScreenImagePixelBounds.Left + w / 2;
                        var centerY = ScreenImagePixelBounds.Top + h / 2;
                        var newW = w;
                        var newH = h;
                        if (hO > wO)
                        {
                            newW = h * wO / hO;
                        }
                        else
                        {
                            newH = w * hO / wO;
                        }

                        ScreenImagePixelBounds = new Rect(new Point(centerX - (newW / 2), centerY - (newH / 2)), new Size(newW, newH));
                        AspectRatio = newW / newH;
                    }
                }
                else
                {
                    AspectRatio = w / h;
                }
            }
        }
        private void RestoreAspectRatio()
        {
            double hDif = ScreenImagePixelBounds.Height - ScreenImagePixelBounds.Width / ImageSourceAspectRatio;
            var startArea = ScreenImagePixelBounds;
            double radAngle = DegreesToRadians(Rotation.Angle);

            var outSize = AspectAndRotationShifter(0, -hDif, 50, "bottomright", startArea, false, radAngle, new Point(.5, .5));

            var finalH = outSize.Height;
            var finalW = outSize.Width;
            var top = outSize.Top;
            var left = outSize.Left;
            ScreenImagePixelBounds = new Rect(left, top, finalW, finalH);
        }
        public Rect AspectAndRotationShifter(double dx, double dy, double minSize, string thumbType,
                                             Rect inArea, bool preserveAspect, double radAngle, Point renderTOrigin)
        {
            var w = inArea.Width;
            var h = inArea.Height;
            var top = inArea.Top;
            var left = inArea.Left;
            var finalW = w;
            var finalH = h;
            double deltaVertical;
            double deltaHorizontal;
            if (thumbType.Contains("right"))
            {
                deltaHorizontal = Math.Min(-dx, w - minSize);
                top -= renderTOrigin.Y * deltaHorizontal * Math.Sin(radAngle);
                left += (deltaHorizontal * renderTOrigin.X * (1 - Math.Cos(radAngle)));
                w -= deltaHorizontal;
                finalW = w;
            }
            else if (thumbType.Contains("left"))
            {
                deltaHorizontal = Math.Min(dx, w - minSize);
                top += deltaHorizontal * Math.Sin(radAngle) - renderTOrigin.X * deltaHorizontal * Math.Sin(radAngle);
                left += deltaHorizontal * Math.Cos(radAngle) + (renderTOrigin.X * deltaHorizontal * (1 - Math.Cos(radAngle)));
                w -= deltaHorizontal;
                finalW = w;
            }

            if (thumbType.Contains("top"))
            {
                deltaVertical = Math.Min(dy, h - minSize);
                top += deltaVertical * Math.Cos(-radAngle) + (renderTOrigin.Y * deltaVertical * (1 - Math.Cos(-radAngle)));
                left += deltaVertical * Math.Sin(-radAngle) - (renderTOrigin.Y * deltaVertical * Math.Sin(-radAngle));
                h -= deltaVertical;
                finalH = h;
            }
            else if (thumbType.Contains("bottom"))
            {
                deltaVertical = Math.Min(-dy, h - minSize);
                top += (renderTOrigin.Y * deltaVertical * (1 - Math.Cos(-radAngle)));
                left -= deltaVertical * renderTOrigin.Y * Math.Sin(-radAngle);
                h -= deltaVertical;
                finalH = h;
            }
            return new Rect(left, top, finalW, finalH);
        }

        public static double DegreesToRadians(double Degrees)
        {
            return (Math.PI / 180) * Degrees;
        }
    }
}