﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.EditingViewModels
{
    public class ElementGeometryEditCanvasVm : ViewModelBase
    {
        private GeometryEditMode _editCanvasMode = GeometryEditMode.None;
        private GeometryType _activeShapeType = GeometryType.None;
        private List<List<Point>> allGeometryPoints;
        private List<Point> activeGeometryPoints;


        public void CompleteActiveShape()
        {
            
        }

        public void StartNewShape()
        {
            
        }

       
    }
}