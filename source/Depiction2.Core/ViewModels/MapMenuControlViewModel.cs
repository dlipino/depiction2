﻿using System.ComponentModel;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;
using Depiction2.Core.ViewModels.DialogViewModels.Map;

namespace Depiction2.Core.ViewModels
{
    public class MapMenuControlViewModel : ViewModelBase
    {
        private IDialogService _dialogService;
        private IStory _mainStory;

        #region Constructor

        public MapMenuControlViewModel(IStory mainStory, IDialogService dialogService)
        {
            _dialogService = dialogService;
            _mainStory = mainStory;
        }
        
        #endregion

        #region Map main menu dialog controls

        private bool _mainMenuVisible = true;
        public bool MainMenuVisible
        {
            get { return _mainMenuVisible; }
            set { _mainMenuVisible = value; OnPropertyChanged(() => MainMenuVisible); }
        }

        private FileMenuDialogVM _fileMenuDialogVm;
        public bool FileDialogVisible
        {
            get
            {
                if (_fileMenuDialogVm == null) return false;
                return !_fileMenuDialogVm.IsHidden;
            }
            set
            {
                if (_fileMenuDialogVm == null)
                {
                    _fileMenuDialogVm = new FileMenuDialogVM(_mainStory,_dialogService);
                    _fileMenuDialogVm.PropertyChanged += FileMenuDialogVmPropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, _fileMenuDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_fileMenuDialogVm);
                }

                OnPropertyChanged(() => FileDialogVisible);
            }
        }

        void FileMenuDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => FileDialogVisible);
            }
        }

        private AddElementDialogVM addElementDialogVM;
        public bool AddElementDialogVisible
        {
            get
            {
                if (addElementDialogVM == null) return false;
                return !addElementDialogVM.IsHidden;
            }
            set
            {
                if (addElementDialogVM == null)
                {
                    addElementDialogVM = new AddElementDialogVM(DepictionAccess.ScaffoldLibrary);//, _mapActionViewModel);
                    addElementDialogVM.PropertyChanged += addElementDialogVM_PropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, addElementDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(addElementDialogVM);
                }

                OnPropertyChanged(() => AddElementDialogVisible);
            }
        }

        void addElementDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => AddElementDialogVisible);
            }else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        private MapHelpDialogVM _mapHelpDialogVm;
        public bool MapHelpDialogVisible
        {
            get
            {
                if (_mapHelpDialogVm == null) return false;
                return !_mapHelpDialogVm.IsHidden;
            }
            set
            {
                if (_mapHelpDialogVm == null)
                {
                    _mapHelpDialogVm = new MapHelpDialogVM();
                    _mapHelpDialogVm.PropertyChanged += MapHelpDialogVmPropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, _mapHelpDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_mapHelpDialogVm);
                }

                OnPropertyChanged(() => MapHelpDialogVisible);
            }
        }

        void MapHelpDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => MapHelpDialogVisible);
            }
        }

        private ManageElementsDialogVM manageElementsDialogVM;
        public bool ManageElementsDialogVisible
        {
            get
            {
                if (manageElementsDialogVM == null) return false;
                return !manageElementsDialogVM.IsHidden;
            }
            set
            {
                if (manageElementsDialogVM == null)
                {
                    manageElementsDialogVM = new ManageElementsDialogVM(_mainStory);
                    manageElementsDialogVM.PropertyChanged += manageElementsDialogVM_PropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, manageElementsDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(manageElementsDialogVM);
                }

                OnPropertyChanged(() => ManageElementsDialogVisible);
            }
        }

        void manageElementsDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => ManageElementsDialogVisible);
            }
        }

        private ElementDisplayControlDialogVM _elementDisplayControlDialogVm;
        public bool MapDisplayDialogVisible
        {
            get
            {
                if (_elementDisplayControlDialogVm == null) return false;
                return !_elementDisplayControlDialogVm.IsHidden;
            }
            set
            {
                if (_elementDisplayControlDialogVm == null)
                {
                    _elementDisplayControlDialogVm = new ElementDisplayControlDialogVM(_mainStory);
                    _elementDisplayControlDialogVm.PropertyChanged += ElementDisplayControlDialogVmPropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, _elementDisplayControlDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_elementDisplayControlDialogVm);
                }

                OnPropertyChanged(() => MapDisplayDialogVisible);
            }
        }

        void ElementDisplayControlDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => MapDisplayDialogVisible);
            }
        }

        private ToolsDialogVM toolsDialogVM;
        public bool ToolsDialogVisible
        {
            get
            {
                if (toolsDialogVM == null) return false;
                return !toolsDialogVM.IsHidden;
            }
            set
            {
                if (toolsDialogVM == null)
                {
                    toolsDialogVM = new ToolsDialogVM();
                    toolsDialogVM.PropertyChanged += toolsDialogVM_PropertyChanged;
                }
                if (value)
                {
                    _dialogService.ShowWindow(this, toolsDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(toolsDialogVM);
                }

                OnPropertyChanged(() => ToolsDialogVisible);
            }
        }

        void toolsDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => ToolsDialogVisible);
            }
        }
        #endregion  
    }
}