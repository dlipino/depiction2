﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction2.Core.ViewModels.Converters
{
    public class AddMethodToBoolConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var param = parameter.ToString();
            if (value.ToString().Equals(param,StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selected = (bool)value;
            if (selected)
            {
                return parameter.ToString();
            }
            return "Mouse";
        }

    }
}