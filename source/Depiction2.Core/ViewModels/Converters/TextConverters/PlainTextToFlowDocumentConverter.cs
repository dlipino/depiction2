﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using Depiction2.Utilities.FrameworkLegacy.Helpers;

namespace Depiction2.Core.ViewModels.Converters.TextConverters
{
    /// <summary>
    /// Turns a plain text into a flow docuemnt.
    /// </summary>
    [ValueConversion(typeof(String), typeof(String))]//I have no clue what this bit is for, it is left over from copy paste
    public class PlainTextToFlowDocumentConverter : IValueConverter
    {
        //I feel reall bad about doing this too, i should actually learn how to properly use IValueConverters
        static public FlowDocument PlainTextToFlowDocument(string plainText)
        {
            var lines = new string[] { string.Empty };
            if (plainText != null)
            {
                lines = Regex.Split(plainText, Environment.NewLine);
            }

            FlowDocument myFlowDocument = new FlowDocument();
            myFlowDocument.PagePadding = new Thickness(0);
            myFlowDocument.LineHeight = 12;
            myFlowDocument.FontSize = 12;
//            myFlowDocument.FontFamily = new FontFamily("Arial");
            myFlowDocument.LineStackingStrategy = LineStackingStrategy.BlockLineHeight;
            
            foreach (var line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    var myParagraph = new Paragraph();
                    myParagraph.Inlines.Add(line);
                    myParagraph.Margin = new Thickness(0);
                    myFlowDocument.Blocks.Add(myParagraph);
                }
            }
            FrameworkElementHelpers.IdentifyHyperlinks(myFlowDocument);
            return myFlowDocument;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strVal = null;
            if (value != null) strVal = value.ToString();//would an as String work better?
            return PlainTextToFlowDocument(strVal);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var flow = value as FlowDocument;
            var outString = "Bad input";
            if (flow == null)
            {
                return outString;
            }
            outString = GetText(flow);
            return outString;
        }
        private string GetText(FlowDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            var runs = GetRunsAndParagraphs(doc);
            var list = new List<TextElement>();
            foreach (var el in runs)
            {
                list.Add(el);
            }
            var count = 0;
            foreach (TextElement el in GetRunsAndParagraphs(doc))
            {
                Run run = el as Run;
                if (run != null)
                {
                    sb.Append(run.Text);
                }
                else
                {
                    if (count != list.Count - 1)
                    {
                        sb.Append(Environment.NewLine);
                    }
                }
                //sb.Append(run == null ? Environment.NewLine : run.Text);
                count++;
            }
            return sb.ToString();
        }
        private IEnumerable<TextElement> GetRunsAndParagraphs(FlowDocument doc)
        {
            // use the GetNextContextPosition method to iterate through the   
            // FlowDocument   

            for (TextPointer position = doc.ContentStart;
                 position != null && position.CompareTo(doc.ContentEnd) <= 0;
                 position = position.GetNextContextPosition(LogicalDirection.Forward))
            {
                if (position.GetPointerContext(LogicalDirection.Forward) ==
                    TextPointerContext.ElementEnd)
                {
                    // return solely the Runs and Paragraphs. all other elements are    
                    // ignored since they aren't supported by FormattedText.   

                    Run run = position.Parent as Run;

                    if (run != null)
                    {
                        yield return run;
                    }
                    else
                    {
                        Paragraph para = position.Parent as Paragraph;

                        if (para != null)
                        {
                            yield return para;
                        }
                    }
                }
            }
        }
    }
}