﻿using Depiction2.Core.ViewModels.Converters.TextConverters;

namespace Depiction2.Core.ViewModels.Converters
{
    public class LegacyTextConverters
    {
        public static readonly PlainTextToFlowDocumentConverter PlainTextToFlowDocument = new PlainTextToFlowDocumentConverter();
        public static readonly InputStringToFlowDocumentConverter InputStringToFlowDocument = new InputStringToFlowDocumentConverter();
        public static readonly HtmlToFlowDocumentConverter HtmlToFlowDocument = new HtmlToFlowDocumentConverter();
    }
}
