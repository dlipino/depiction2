﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.ViewModels.ElementTemplateViewModels
{
    public class ElementTemplateFilterAndQuickAddControllerViewModel : ViewModelBase
    {
        private IElementTemplateLibrary _templateLibraryBase;
        private IElementTemplateLibraryFilter _selectedFilter;

        private FilterAndQuickAddViewModel _editFilter;
        private ObservableCollection<IElementTemplateLibraryFilter> _observableFilters = new ObservableCollection<IElementTemplateLibraryFilter>();
        #region properties
        public IElementTemplateLibraryFilter SelectedFilter
        {
            get { return _selectedFilter; }
            set
            {
                _selectedFilter = value;
                OnPropertyChanged("SelectedFilter");
                EditedFilter = new FilterAndQuickAddViewModel(_selectedFilter);
            }
        }
        public FilterAndQuickAddViewModel EditedFilter { get { return _editFilter; } set { _editFilter = value; OnPropertyChanged("EditedFilter"); } }

        #endregion

        #region commands

        private DelegateCommand _createFilterCommand;
        public ICommand CreateFilterCommand
        {
            get
            {
                if (_createFilterCommand == null)
                {
                    _createFilterCommand = new DelegateCommand(CreateEmptyFilter);
                }
                return _createFilterCommand;
            }
        }


        private DelegateCommand<IElementTemplateLibraryFilter> _deleteFilterCommand;
        public ICommand DeleteFilterCommand
        {
            get
            {
                if (_deleteFilterCommand == null)
                {
                    _deleteFilterCommand = new DelegateCommand<IElementTemplateLibraryFilter>(DeleteFilter);
                }
                return _deleteFilterCommand;
            }
        }



        private DelegateCommand<IElementTemplateLibraryFilter> _editSelectedFilterCommand;
        public ICommand EditSelectedFilterCommand
        {
            get
            {
                if (_editSelectedFilterCommand == null)
                {
                    _editSelectedFilterCommand = new DelegateCommand<IElementTemplateLibraryFilter>(EditFilter);
                }
                return _editSelectedFilterCommand;
            }
        }


        private DelegateCommand<FilterAndQuickAddViewModel> _saveFilterCommand;
        public ICommand SaveFilterCommand
        {
            get
            {
                if (_saveFilterCommand == null)
                {
                    _saveFilterCommand = new DelegateCommand<FilterAndQuickAddViewModel>(SaveFilter);
                }
                return _saveFilterCommand;
            }
        }


        #endregion

        #region constructor/destructor
        public ElementTemplateFilterAndQuickAddControllerViewModel(IElementTemplateLibrary templateLibrary)
        {
            _templateLibraryBase = templateLibrary;
            foreach (var library in templateLibrary.LibraryFilters)
            {
                _observableFilters.Add(library);
            }
            SelectedFilter = _observableFilters.First();
        }

        #endregion

        #region methods that are used

        private void CreateEmptyFilter()
        {
            throw new System.NotImplementedException();
        }
        private void DeleteFilter(IElementTemplateLibraryFilter filter)
        {
            throw new System.NotImplementedException();
        }
        private void EditFilter(IElementTemplateLibraryFilter filter)
        {
            throw new System.NotImplementedException();
        }
        private void SaveFilter(FilterAndQuickAddViewModel filter)
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}