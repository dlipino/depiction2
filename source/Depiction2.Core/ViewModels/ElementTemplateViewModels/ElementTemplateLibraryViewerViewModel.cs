﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate.ViewModel;

namespace Depiction2.Core.ViewModels.ElementTemplateViewModels
{
    public class ElementTemplateLibraryViewerViewModel : ViewModelBase
    {
        private IElementTemplateLibrary _templateLibraryBase;
        private List<ElementTemplateListViewModel> scaffoldVms;
        private ElementTemplateListViewModel _selectedElementTemplate;
        private IElementTemplateLibraryFilter _selectedFilter;
        private TemplateGroups _grouping;
        private ObservableCollection<IElementTemplateLibraryFilter> _observableFilters = new ObservableCollection<IElementTemplateLibraryFilter>();
        #region properties
        public CollectionViewSource TemplatesViewSource { get; set; }
        public ObservableCollection<IElementTemplateLibraryFilter> LibraryFilters { get { return _observableFilters; } }
        public IElementTemplateLibraryFilter SelectedFilter { get { return _selectedFilter; } set { _selectedFilter = value; OnPropertyChanged("SelectedFilter"); } }
        public IElementTemplate SelectedTemplate
        {
            get
            {
                if (_selectedElementTemplate == null) return null;
                return _selectedElementTemplate.TemplateModel;
            }
        }
        //This controls how elements are grouped
        public TemplateGroups Grouping
        {
            get { return _grouping; }
            set
            {
                _grouping = value;
                OnPropertyChanged("Grouping");
                SetScaffoldSourceThreadSafe(scaffoldVms);
            }
        }
        public ElementTemplateListViewModel SelectedElementTemplateViewModel
        {
            get { return _selectedElementTemplate; }
            set
            {
                _selectedElementTemplate = value;
                OnPropertyChanged("SelectedElementTemplateViewModel");
                OnPropertyChanged("SelectedTemplate");
            }
        }
        #endregion

        #region constructor/destruction

        public ElementTemplateLibraryViewerViewModel(IElementTemplateLibrary templateLibrary)
        {
            _templateLibraryBase = templateLibrary;
            TemplatesViewSource = new CollectionViewSource();
            var defaultScaffolds = templateLibrary.ProductElementTemplates;
            scaffoldVms = new List<ElementTemplateListViewModel>();
            foreach (var scaffold in defaultScaffolds)
            {
                scaffoldVms.Add(new ElementTemplateListViewModel(scaffold));
            }
            foreach (var scaffold in templateLibrary.LoadedStoryElementTemplates)
            {
                scaffoldVms.Add(new ElementTemplateListViewModel(scaffold) { DefinitionSource = "Loaded Story" });
            }
           foreach(var library in templateLibrary.LibraryFilters)
           {
               _observableFilters.Add(library);
           }
            SelectedFilter = _observableFilters.First();
            _templateLibraryBase.PropertyChanged += templateLibrary_PropertyChanged;
            _grouping = TemplateGroups.DefinitionSource;
            //Lies, not thread safe.
            SetScaffoldSourceThreadSafe(scaffoldVms);
        }

        void templateLibrary_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
       
        }

        protected override void OnDispose()
        {
            _templateLibraryBase.PropertyChanged -= templateLibrary_PropertyChanged;
            foreach (var scaffold in scaffoldVms)
            {
                scaffold.Dispose();
            }
            base.OnDispose();
        }
        #endregion

        #region private helpers
        protected void SetScaffoldSourceThreadSafe(List<ElementTemplateListViewModel> scaffoldViewModels)
        {
            //            if (DepictionAccess.DispatchAvailableAndNeeded())
            //            {
            //                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<ElementPrototypeViewModel>>(SetPrototypeSourceThreadSafe), prototypes);
            //                return;
            //            }
            TemplatesViewSource.Source = null;
            TemplatesViewSource.SortDescriptions.Clear();
            TemplatesViewSource.SortDescriptions.Clear();
            TemplatesViewSource.GroupDescriptions.Clear();
            //Only changes the grouping (based on a property value)doesnt not do any kind of filtering
            SetElementPrototypeSorting();
            //Probably need to do the filtering here, maybe
            TemplatesViewSource.Source = scaffoldViewModels;
            OnPropertyChanged("TemplatesViewSource");

        }

        public void SelectScaffoldType(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                var res = scaffoldVms.FirstOrDefault(t => t.TemplateModel.ElementType.Equals(DepictionAccess.AutoDetectElement));
                if (res == null) return;
                SelectedElementTemplateViewModel = res;
            }
            else
            {
                var res = scaffoldVms.FirstOrDefault(t => t.TemplateModel.ElementType.Equals(type));
                if (res == null) return;
                SelectedElementTemplateViewModel = res;
            }
        }

        public void SetElementPrototypeSorting()
        {
            //Is the new call needed, kind of messes with the filter
            if(TemplatesViewSource != null)
            {
                TemplatesViewSource.Filter -= ScaffoldsViewSource_Filter;
            }
            TemplatesViewSource = new CollectionViewSource();
            TemplatesViewSource.Filter += ScaffoldsViewSource_Filter;
            var depictionSort = new SortDescription();
            depictionSort.Direction = ListSortDirection.Descending;
            depictionSort.PropertyName = "SortPriority";

            TemplatesViewSource.SortDescriptions.Add(depictionSort);

            var sortDescription = new SortDescription();
            sortDescription.Direction = ListSortDirection.Ascending;
            sortDescription.PropertyName = "DisplayName";
            TemplatesViewSource.SortDescriptions.Add(sortDescription);
            PropertyGroupDescription groupDescription = null;
            var groupingName = Grouping.ToString();
            if (!Grouping.Equals(TemplateGroups.None))
            {
                //            groupDescription = new PropertyGroupDescription("DefinitionSource");
                groupDescription = new PropertyGroupDescription(groupingName);
                TemplatesViewSource.GroupDescriptions.Add(groupDescription);
            }
        }

        void ScaffoldsViewSource_Filter(object sender, FilterEventArgs e)
        {
            e.Accepted = true;
            if (SelectedFilter == null)
            {
                return;
            }
            if (!SelectedFilter.ElementTemplateNames.Any())
            {
                return;
            }
            var scafVm = e.Item as ElementTemplateListViewModel;
            if (scafVm == null) return;
            if (!SelectedFilter.ElementTemplateNames.Contains(scafVm.TemplateModel.ElementType))
            {
                e.Accepted = false;
            }
        }
        #endregion
    }
    //I think there is a way to change to give each enum a better name
    public enum TemplateGroups
    {
        None,
        DefinitionSource,
        Tags
    }
}