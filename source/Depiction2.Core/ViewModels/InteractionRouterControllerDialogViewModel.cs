﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.Interactions;

namespace Depiction2.Core.ViewModels
{
    public class InteractionRouterControllerDialogViewModel : DialogViewModel
    {
        private IStory _mainStory;

        public InteractionRouterControllerViewModel RouterController { get; set; }
        public InteractionRouterControllerDialogViewModel(IStory storyToControl)
        {
            _mainStory = storyToControl;
            RouterController = new InteractionRouterControllerViewModel(storyToControl);
            IsModal = false;
            HideOnClose = true;
        }
    }
}