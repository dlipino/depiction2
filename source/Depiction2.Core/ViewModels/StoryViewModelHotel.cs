﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.ViewModels.BackgroundServiceViewModel;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.Core.ViewModels.DialogViewModels.Map;
using Depiction2.Core.ViewModels.EditingViewModels;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Core.ViewModels.PanAndZoom;
using Depiction2.Core.ViewModels.Tiling;

namespace Depiction2.Core.ViewModels
{
    public class StoryViewModelHotel : ViewModelBase
    {
        private IStory _storyModel;
        private IStoryActions _storyModelActions;
        private IDialogService _dialogService;
        #region properties
        public TopMenuViewModel TopMenuVm { get; set; }

        public TilerSelectionViewModel TilerSelectorVm { get; set; }
        public MapTileDisplayViewModel TileDisplayerVm { get; set; }

        public AnnotationVmHotel AnnotationVm { get; set; }
        public ElementAndDisplayerVmHotel DisplayerVm { get; set; }
        public ElementDisplayerMapViewModel TempElementDisplayer { get; set; }
        public PanAndZoomControlViewModel PanZoomVm { get; set; }
        public BackgroundServiceManageViewModel BackgroundServiceVm { get; set; }
        public GeometryEditCanvasViewModel EditCanvasVm { get; set; }
        public ImageRegistrationViewModel ImageRegistration { get; set; }
        public InteractionRouterControllerDialogViewModel RouterControllerDialog { get; set; }

        #endregion

        #region constuctor/destructor

        public StoryViewModelHotel()
        {
            SetContextMenuCommands();
            StoryZoom = 1d;
        }

        protected override void OnDispose()
        {
            _storyModel.PropertyChanged -= _storyLocation_PropertyChanged;
            _storyModelActions.StoryRequestMouseAdd -= _storyModelActions_StoryRequestMouseAdd;

            //Connect up items that change the canvas mode
            //eventually there one be a general dispose call on everthing.
//            TopMenuVm.AddElementDialog.MouseAddRequest -= EditCanvasVm.StartAdd;
            TopMenuVm.ManageElementDialog.GeolocateElementRequest -= RequestImageRegistration;
            TopMenuVm.ManageElementDialog.ShowPropertyRequest -= RequestPropertyShowForElementList;
            TopMenuVm.DisplayerControlDialog.RequestRegionAction -= AdjustDepictionRegion;
            EditCanvasVm.ShowPropertyRequest -= RequestPropertyShowForElementList;
            DisplayerVm.RequestDisplayDialog -= DisplayerVm_RequestDisplayDialog;
            TopMenuVm.Dispose();
            AnnotationVm.Dispose();
            DisplayerVm.Dispose(); 
            PanZoomVm.Dispose();
            TileDisplayerVm.Dispose();
            EditCanvasVm.Dispose();
            ImageRegistration.Dispose();
            BackgroundServiceVm.Dispose();
            DepictionAccess.BackgroundServiceManager = null;

            base.OnDispose();
        }

       
        #endregion

        #region Construction/destruction helper methods
        //I now have an issue with how the story is passed in an stored. I also wish i remembered how to 
        //disconnected all attached events from the source
        public void FillHotel(IStory story, IStoryActions storyActions, IDialogService dialogService)
        {
            TopMenuVm = new TopMenuViewModel(story, dialogService);
            AnnotationVm = new AnnotationVmHotel(story);
            DisplayerVm = new ElementAndDisplayerVmHotel(story);
            PanZoomVm = new PanAndZoomControlViewModel(story);
            EditCanvasVm = new GeometryEditCanvasViewModel(story, storyActions, dialogService);
            ImageRegistration = new ImageRegistrationViewModel();
            RouterControllerDialog = new InteractionRouterControllerDialogViewModel(story);
            var storyTilingService = new TilingService();
            TileDisplayerVm = new MapTileDisplayViewModel(storyTilingService, story);
            TilerSelectorVm = new TilerSelectionViewModel(storyTilingService);
            storyTilingService.SetTiler(Settings.Default.TilingSourceName);
            var storyBackGroundService = new BackgroundServiceManager();
            BackgroundServiceVm = new BackgroundServiceManageViewModel(storyBackGroundService);
            DepictionAccess.BackgroundServiceManager = storyBackGroundService;

            OnPropertyChanged("TopMenuVm");
            OnPropertyChanged("TileDisplayerVm");
            OnPropertyChanged("TilerSelectorVm");
            OnPropertyChanged("AnnotationVm");
            OnPropertyChanged("DisplayerVm");
            OnPropertyChanged("PanZoomVm");
            OnPropertyChanged("ImageRegistration");
            _dialogService = dialogService;
            _storyModel = story;
            _storyModel.PropertyChanged += _storyLocation_PropertyChanged;
            _storyModelActions = storyActions;
            _storyModelActions.StoryRequestMouseAdd += _storyModelActions_StoryRequestMouseAdd;
            

            //Connect up items that change the canvas mode
//            TopMenuVm.AddElementDialog.MouseAddRequest += EditCanvasVm.StartAdd;//TDOD  this should get refactored out
            TopMenuVm.ManageElementDialog.GeolocateElementRequest += RequestImageRegistration;
            TopMenuVm.ManageElementDialog.ShowPropertyRequest += RequestPropertyShowForElementList;
            TopMenuVm.DisplayerControlDialog.RequestRegionAction += AdjustDepictionRegion;
            EditCanvasVm.ShowPropertyRequest += RequestPropertyShowForElementList;
            DisplayerVm.RequestDisplayDialog += DisplayerVm_RequestDisplayDialog;
        }

        void _storyModelActions_StoryRequestMouseAdd(IElementTemplate templateToAdd, bool multipleAdd)
        {
            EditCanvasVm.StartAdd(templateToAdd, multipleAdd);
        }


       #endregion

        void DisplayerVm_RequestDisplayDialog(string revealerId)
        {
            TopMenuVm.ShowDisplayerDetails(revealerId);
        }

        #region menu control

        #endregion

        #region land of canvas mode control
        #region commands that deal with quick start, and region
        private DelegateCommand createMapRegionCommand;
        public ICommand CreateMapRegionCommand
        {
            get
            {
                if (createMapRegionCommand == null)
                {
                    createMapRegionCommand = new DelegateCommand(StartRegionCreation);
                    createMapRegionCommand.Text = "Create a region";
                }
                return createMapRegionCommand;
            }
        }
        protected void StartRegionCreation()
        {
            EditCanvasVm.StartRegionCreation(null);
        }
       
        public void AdjustDepictionRegion(IDepictionRegion regionToEdit)
        {
            if(regionToEdit == null)
            {
                EditCanvasVm.StartRegionCreation(null);
            }else
            {
                EditCanvasVm.StartRegionCreation(regionToEdit);
            }
        }
        public void CreateInitialRectRegion()
        {
            EditCanvasVm.PropertyChanged += EditCanvasVm_PropertyChanged;
            EditCanvasVm.StartRegionCreation(null);
        }

        void EditCanvasVm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("RegionComplete"))
           {
               TopMenuVm.AddElementDialog.AddElementMode = "QuickStart";
               TopMenuVm.AddElementDialogVisible = true;
               EditCanvasVm.PropertyChanged -= EditCanvasVm_PropertyChanged;
           }
        }
        #endregion

        #region Interaction router controller commands

        private DelegateCommand openRouterControllerCommand;
        public ICommand OpenRouterControllerCommand
        {
            get
            {
                if(openRouterControllerCommand == null)
                {
                    openRouterControllerCommand = new DelegateCommand(OpenRouterControllerDialog);
                    openRouterControllerCommand.Text = "Show story interactions";
                }
                return openRouterControllerCommand;
            }
        }

        private void OpenRouterControllerDialog()
        {
            _dialogService.ShowWindow(this, RouterControllerDialog);
        }

        #endregion

        private DelegateCommand selectElementsWithRectangle;
        private DelegateCommand selectElementsWithPolygon;

        public ICommand SelectElementsWithRectangleCommand
        {
            get
            {
                if(selectElementsWithRectangle == null)
                {
                    selectElementsWithRectangle = new DelegateCommand(StartRectangleSelection);
                    selectElementsWithRectangle.Text = "Select elements with a rectangle";
                }
                return selectElementsWithRectangle;
            }
        }
        public ICommand SelectElementsWithPolygonCommand
        {
            get
            {
                if(selectElementsWithPolygon == null)
                {
                    selectElementsWithPolygon = new DelegateCommand(StartPolygonSelection);
                    selectElementsWithPolygon.Text = "Select elements with a polygon";
                }
                return selectElementsWithPolygon;
            }
        }
        public void StartRectangleSelection()
        {
            EditCanvasVm.StartAreaElementSelection(GeometryType.Rectangle);
        }
        public void StartPolygonSelection()
        {

            EditCanvasVm.StartAreaElementSelection(GeometryType.Polygon);
        }

        public void RequestGeometryEditForElement(IElement element)
        {
            var geoPoints = element.ElementGeometry.GeometryPoints.ToList();
            if (geoPoints.Count < 2) return;
            EditCanvasVm.SetEditMode(element);
        }

        public void RequestPropertyShowForElementList(List<IElement> elements)
        {
            if (!elements.Any()) return;
            if (elements.Count == 1)
            {
                ShowElementPropertiesDialog(elements.First());
            }
            else
            {
                if (_dialogService == null) return;
                var multiElementProperties = new MultipleElementPropertiesDialogViewModel(elements);
                _dialogService.ShowWindow(this, multiElementProperties);
            }
        }
        public void RequestImageRegistration(IElement element)
        {
            if (string.IsNullOrEmpty(element.ImageResourceName)) return;
            ImageRegistration.RequestImageRegistration(element);
        }
        #endregion

        #region magical land of coordinate systems
        //TODO D2 breaks when going into mercator and then zooming way out.
        public void ShiftOffsetWithPanelPixelVector(Vector panelPixelShiftVector)
        {
            var zoom = _storyModel.StoryZoom;
            //Convert to unscaled shift distance
            var virtualPanelVector = new Vector(panelPixelShiftVector.X / zoom, -panelPixelShiftVector.Y / zoom);
            _storyModel.ShiftStoryExtent(virtualPanelVector);
        }

        public void AdjustStoryModelZoom(double multiplier, Point? zoomProjPoint)
        {
            if (zoomProjPoint != null)
            {
                var p = (Point)zoomProjPoint;
                var cartPoint = GeoToDrawLocationService.ConvertDrawPointToGeoPoint(p);//, _storyModel.StoryProjector);
                _storyModel.AdjustZoom(multiplier, cartPoint);
            }
            else
            {
                //Use the current story center
                _storyModel.AdjustZoom(multiplier, null);
            }
        }

        private double gcsToProjectionScaleModifierMagical = 1;
        public double PixelZoomFromStory
        {
            get { return _storyModel.StoryZoom / gcsToProjectionScaleModifierMagical; }
        }
        //This is actually the topleft point
        public Point TopLeftPanelFromStory
        {
            get
            {
                var wpfPanelLoc = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(_storyModel.StoryFocalPoint);//, _storyModel.StoryProjector);
                //now find the location given the current pixel scale value
                var tScale = _storyModel.StoryZoom / gcsToProjectionScaleModifierMagical;
                var vpanelOffset = tScale * ((Vector)(wpfPanelLoc));
                return (Point)vpanelOffset;
            }
        }

        public Point VirtualPanelOffset { get; set; }
        public double StoryZoom { get; set; }

        void _storyLocation_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StoryFocalPoint") || e.PropertyName.Equals("StoryZoom"))
            {
                UpdateViewFromStory();
            }
            else if (e.PropertyName.Equals("Projection"))
            {
                UpdateViewModelGeometriesWithProjection();//_storyModel.StoryProjector);
            }
            else if (e.PropertyName.Equals("PrimaryRegion"))
            {
                
            }else if(e.PropertyName.Equals("ShowManageNonGeo"))
            {
                TopMenuVm.ManageElementsDialogVisible = true;
                TopMenuVm.ManageElementDialog.ManageModality = ManageContentModality.NonGeoAligned;
            }
        }
        public void UpdateViewFromStory()
        {
            VirtualPanelOffset = TopLeftPanelFromStory;
            OnPropertyChanged("VirtualPanelOffset");
            StoryZoom = PixelZoomFromStory;
            OnPropertyChanged("StoryZoom");
        }
        protected void UpdateViewModelGeometriesWithProjection()
        {
            foreach (var elementViewModel in DisplayerVm.ElementViewModels)
            {
                var loc = elementViewModel._elementModel.GeoLocation;
                if (loc != null)
                {
                    var p = (Point)loc;
                    elementViewModel.SetDrawLocationFromGCSPoint(p);
                }

                elementViewModel.UpdateStreamGeometryVisual();
            }
            //now the annotations
            foreach (var annotationVm in AnnotationVm.AllAnnotationVms)
            {
                annotationVm.SetAnnotationPixelLocationFromModelGeolocation();
            }

            //And revealers
            foreach (var revealer in DisplayerVm.RevealerVms)
            {
                revealer.UpdateDrawBounds();
            }

            var newCenter = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(_storyModel.StoryFocalPoint);//, _storyModel.StoryProjector);
            gcsToProjectionScaleModifierMagical = GeoToDrawLocationService.GetGCSToDrawScaleMult(_storyModel.StoryFocalPoint, newCenter);
            UpdateViewFromStory();
        }

        #endregion

        #region magical land of context menu commands
        public ObservableCollection<ICommand> StoryCommands { get; set; }
        public ObservableCollection<ICommand> MapLocationCommands { get; set; }
        public ObservableCollection<ICommand> GenericElementCommands { get; set; }
        public ObservableCollection<ICommand> GenericAnnotationCommands { get; set; }

        protected void SetContextMenuCommands()
        {
            StoryCommands = new ObservableCollection<ICommand>();
            StoryCommands.Add(CreateMapRegionCommand);

            MapLocationCommands = new ObservableCollection<ICommand>();
            MapLocationCommands.Add(CopyRightClickToClipboardCommand);
            MapLocationCommands.Add(AddNewAnnotationCommand);

            GenericAnnotationCommands = new ObservableCollection<ICommand>();
            GenericAnnotationCommands.Add(ShowAnnotationPropertiesDialogCommand);
            GenericAnnotationCommands.Add(RemoveAnnotationCommand);

            GenericElementCommands = new ObservableCollection<ICommand>();
            GenericElementCommands.Add(ShowSingleElementPropertiesCommand);
            GenericElementCommands.Add(DeleteElementCommand);
        }

        #region Story actions that are controlled via the context menu


        #region general map commands center, copy etc

        private DelegateCommand<Point> copyRightClickLocationToClipboard;
        public ICommand CopyRightClickToClipboardCommand
        {
            get
            {
                if (copyRightClickLocationToClipboard == null)
                {
                    copyRightClickLocationToClipboard = new DelegateCommand<Point>(CopyLocation);
                    copyRightClickLocationToClipboard.Text = "Copy location to clipboard";
                }
                return copyRightClickLocationToClipboard;
            }
        }
        protected void CopyLocation(Point geoLocationDecimal)
        {
            Clipboard.SetText(geoLocationDecimal.ToString());
        }

        #endregion

        #region element map commands

        private DelegateCommand<IElement> showElementPropertiesDialog;
        public ICommand ShowSingleElementPropertiesCommand
        {
            get
            {
                if (showElementPropertiesDialog == null)
                {
                    showElementPropertiesDialog = new DelegateCommand<IElement>(ShowElementPropertiesDialog);
                    showElementPropertiesDialog.Text = "Properties";
                }
                return showElementPropertiesDialog;
            }
        }
        protected void ShowElementPropertiesDialog(IElement elementModel)
        {
            if (_dialogService == null) return;
            var elementPropertyDialog = new SingleElementPropertiesDialogVm(elementModel);
            _dialogService.ShowWindow(this, elementPropertyDialog);
        }

        private DelegateCommand<IElement> deleteElement;
        public ICommand DeleteElementCommand
        {
            get
            {
                if (deleteElement == null)
                {
                    deleteElement = new DelegateCommand<IElement>(DeleteElement);
                    deleteElement.Text = "Delete Element";
                }
                return deleteElement;
            }
        }
        public void DeleteElement(IElement elementModel)
        {
            _storyModel.RemoveElement(elementModel);
        }

        #endregion

        #region map annotation commands, add,remove, and edit

        private DelegateCommand<Point> addNewAnnotation;
        public ICommand AddNewAnnotationCommand
        {
            get
            {
                if (addNewAnnotation == null)
                {
                    addNewAnnotation = new DelegateCommand<Point>(AddNewAnnotation);
                    addNewAnnotation.Text = "Add Annotation";
                }
                return addNewAnnotation;
            }
        }
        public void AddNewAnnotation(Point annotationLatLongLocation)
        {
            _storyModel.AddAnnotationAtLocation(annotationLatLongLocation.X, annotationLatLongLocation.Y);
        }

        //Duplicated in annotationeditdialogVM
        private DelegateCommand<IDepictionAnnotation> removeAnnotation;
        public ICommand RemoveAnnotationCommand
        {
            get
            {
                if (removeAnnotation == null)
                {
                    removeAnnotation = new DelegateCommand<IDepictionAnnotation>(RemoveAnnotation);
                    removeAnnotation.Text = "Remove Annotation";
                }
                return removeAnnotation;
            }
        }
        public void RemoveAnnotation(IDepictionAnnotation annotationModel)
        {
            _storyModel.DeleteAnnotation(annotationModel);
        }
        //Maybe use an editing boolean property? that would move the show dialog to the annotation viewmodel
        //and change the way the context menu works in general
        private DelegateCommand<IDepictionAnnotation> showAnnotationPropertiesDialog;
        public ICommand ShowAnnotationPropertiesDialogCommand
        {
            get
            {
                if (showAnnotationPropertiesDialog == null)
                {
                    showAnnotationPropertiesDialog = new DelegateCommand<IDepictionAnnotation>(ShowAnnotationPropertiesDialog);
                    showAnnotationPropertiesDialog.Text = "Properties";
                }
                return showAnnotationPropertiesDialog;
            }
        }
        public void ShowAnnotationPropertiesDialog(IDepictionAnnotation annotationModel)
        {
            if (_dialogService == null) return;
            var annotationEditDialogVm = new AnnotationEditDialogVM(annotationModel);
            _dialogService.ShowDialog(this, annotationEditDialogVm);
        }
        #endregion

        #endregion
        #endregion
    }

}