﻿using System.ComponentModel;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.DialogViewModels.Map;

namespace Depiction2.Core.ViewModels
{
    /// <summary>
    /// Controls the upper menu. Currently gets recreated for every new story
    /// </summary>
    public class TopMenuViewModel : ViewModelBase
    {
        private IDialogService _dialogService;
        private IStory _mainStory;

        private bool _mainMenuVisible = true;
        private FileMenuDialogVM _fileMenuDialogVm;
        private AddElementDialogVM addElementDialogVM;
        private MapHelpDialogVM _mapHelpDialogVm;
        private ManageElementsDialogVM manageElementsDialogVM;
        private ElementDisplayControlDialogVM _elementDisplayControlDialogVm;
        private ToolsDialogVM toolsDialogVM;

        #region properties
        public AddElementDialogVM AddElementDialog
        {
            get { return addElementDialogVM; }
        }
        public ManageElementsDialogVM ManageElementDialog
        {
            get { return manageElementsDialogVM; }
        }
        public ElementDisplayControlDialogVM DisplayerControlDialog
        {
            get { return _elementDisplayControlDialogVm; }
        }
        public bool MainMenuVisible
        {
            get { return _mainMenuVisible; }
            set { _mainMenuVisible = value; OnPropertyChanged(() => MainMenuVisible); }
        }

        public bool AddElementDialogVisible
        {
            get
            {
                if (addElementDialogVM == null) return false;
                return !addElementDialogVM.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, addElementDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(addElementDialogVM);
                }

                OnPropertyChanged(() => AddElementDialogVisible);
            }
        }
        public bool FileDialogVisible
        {
            get
            {
                if (_fileMenuDialogVm == null) return false;
                return !_fileMenuDialogVm.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, _fileMenuDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_fileMenuDialogVm);
                }

                OnPropertyChanged(() => FileDialogVisible);
            }
        }
        public bool MapHelpDialogVisible
        {
            get
            {
                if (_mapHelpDialogVm == null) return false;
                return !_mapHelpDialogVm.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, _mapHelpDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_mapHelpDialogVm);
                }

                OnPropertyChanged(() => MapHelpDialogVisible);
            }
        }
        public bool ManageElementsDialogVisible
        {
            get
            {
                if (manageElementsDialogVM == null) return false;
                return !manageElementsDialogVM.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, manageElementsDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(manageElementsDialogVM);
                }

                OnPropertyChanged(() => ManageElementsDialogVisible);
            }
        }

        public bool MapDisplayDialogVisible
        {
            get
            {
                if (_elementDisplayControlDialogVm == null) return false;
                return !_elementDisplayControlDialogVm.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, _elementDisplayControlDialogVm);
                }
                else
                {
                    _dialogService.HideWindow(_elementDisplayControlDialogVm);
                }

                OnPropertyChanged(() => MapDisplayDialogVisible);
            }
        }

        public bool ToolsDialogVisible
        {
            get
            {
                if (toolsDialogVM == null) return false;
                return !toolsDialogVM.IsHidden;
            }
            set
            {
                if (value)
                {
                    _dialogService.ShowWindow(this, toolsDialogVM);
                }
                else
                {
                    _dialogService.HideWindow(toolsDialogVM);
                }

                OnPropertyChanged(() => ToolsDialogVisible);
            }
        }
        #endregion

        #region Constructor

        public TopMenuViewModel(IStory mainStory, IDialogService dialogService)
        {
            _dialogService = dialogService;
            _mainStory = mainStory;
            addElementDialogVM = new AddElementDialogVM(DepictionAccess.TemplateLibrary, mainStory);
            addElementDialogVM.PropertyChanged += addElementDialogVM_PropertyChanged;

            _fileMenuDialogVm = new FileMenuDialogVM(_mainStory, _dialogService);
            _fileMenuDialogVm.PropertyChanged += FileMenuDialogVmPropertyChanged;

            _mapHelpDialogVm = new MapHelpDialogVM();
            _mapHelpDialogVm.PropertyChanged += MapHelpDialogVmPropertyChanged;

            manageElementsDialogVM = new ManageElementsDialogVM(_mainStory);
            manageElementsDialogVM.PropertyChanged += manageElementsDialogVM_PropertyChanged;

            _elementDisplayControlDialogVm = new ElementDisplayControlDialogVM(_mainStory);
            _elementDisplayControlDialogVm.PropertyChanged += ElementDisplayControlDialogVmPropertyChanged;

//            toolsDialogVM = new ToolsDialogVM();
            var mainWindow = Application.Current.MainWindow;
            toolsDialogVM = ((DepictionAppViewModel)mainWindow.DataContext).ToggleToolsDialog(false);
            toolsDialogVM.PropertyChanged += toolsDialogVM_PropertyChanged;
        }
        protected override void OnDispose()
        {
            addElementDialogVM.PropertyChanged -= addElementDialogVM_PropertyChanged;
            addElementDialogVM.Dispose();

            _fileMenuDialogVm.PropertyChanged -= FileMenuDialogVmPropertyChanged;
            _fileMenuDialogVm.Dispose();

            _mapHelpDialogVm.PropertyChanged -= MapHelpDialogVmPropertyChanged;
            _mapHelpDialogVm.Dispose();

            manageElementsDialogVM.PropertyChanged -= manageElementsDialogVM_PropertyChanged;
            manageElementsDialogVM.Dispose();

            _elementDisplayControlDialogVm.PropertyChanged -= ElementDisplayControlDialogVmPropertyChanged;
            _elementDisplayControlDialogVm.Dispose();

            toolsDialogVM.PropertyChanged -= toolsDialogVM_PropertyChanged;
//            toolsDialogVM.Dispose();//cant do this since this comes from the main source
            base.OnDispose();
        }
        #endregion

        #region Map main menu dialog controls

        void FileMenuDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => FileDialogVisible);
            }
        }

        void addElementDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => AddElementDialogVisible);
            }
            else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        void MapHelpDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => MapHelpDialogVisible);
            }
        }

        void manageElementsDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => ManageElementsDialogVisible);
            }
        }

        void ElementDisplayControlDialogVmPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => MapDisplayDialogVisible);
            }
        }

        void toolsDialogVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsHidden"))
            {
                OnPropertyChanged(() => ToolsDialogVisible);
            }
        }
        #endregion

        #region dialog displayer helpers

        public void ShowDisplayerDetails(string revealerId)
        {
            if (!string.IsNullOrEmpty(revealerId))
            {
                _mainStory.TopRevealerId = revealerId;
                _elementDisplayControlDialogVm.DisplayType = DisplayerType.Revealer;
                //Maybe make a more direct connection in the dialog service
                MapDisplayDialogVisible = true;
            }
        }

        #endregion
    }
}