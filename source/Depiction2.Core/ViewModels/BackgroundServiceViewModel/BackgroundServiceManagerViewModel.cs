using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;

namespace Depiction2.Core.ViewModels.BackgroundServiceViewModel
{
    public class BackgroundServiceManageViewModel : ViewModelBase
    {
        private IBackgroundServiceManager model;

        private ObservableCollection<DepictionBackgroundServiceViewModel> serviceViewModels;

        #region PRoperties

        public ObservableCollection<DepictionBackgroundServiceViewModel> ActiveServices
        {
            get { return serviceViewModels; }
        }

        #endregion

        #region constructor



        public BackgroundServiceManageViewModel(IBackgroundServiceManager managerModel)
        {
            model = managerModel;
            serviceViewModels = new ObservableCollection<DepictionBackgroundServiceViewModel>();
            foreach(var activeService in managerModel.CurrentServices)
            {
                var vm = new DepictionBackgroundServiceViewModel(activeService);
                serviceViewModels.Add(vm);
            }
            model.BackgroundServicesModified += model_BackgroundServicesModified;
        }

        #endregion


        void model_BackgroundServicesModified(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                foreach (var item in e.NewItems)
                {
                    var serviceModel = item as IDepictionBackgroundService;
                    if (serviceModel != null)
                    {
                        var vm = new DepictionBackgroundServiceViewModel(serviceModel);
                        serviceViewModels.Add(vm);
//                        vm.StartService();//hmmm
                    }
                }
            }
            else if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                foreach (var item in e.OldItems)
                {
                    var serviceModel = item as IDepictionBackgroundService;
                    if (serviceModel != null)
                    {
                        var match = serviceViewModels.Where(t => t.ServiceID.Equals(serviceModel.ServiceID)).First();
                        if (match != null)
                        {
                            serviceViewModels.Remove(match);
                        }
                    }
                }

            }
        }
    }
}