﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Base.Helpers;
using Depiction2.Core.ViewModels.ElementViewModels;
using Depiction2.Utilities.MVVM;

namespace Depiction2.Core.ViewModels
{
    public class ImageRegistrationViewModel : ViewModelBase
    {
        public MapElementViewModel parentVM;

        private Rect screenImagePixelBounds = Rect.Empty;
        private DelegateCommand restoreAspectRatioCommand;
        private double imageOpacity = 50;
        private bool preserveAspectRatio = true;

        public double AspectRatio { get; set; }
        public double OriginalImageAspectRatio { get; set; }
        public bool PreserveAspectRatio
        {
            get { return preserveAspectRatio; }
            set { preserveAspectRatio = value; OnPropertyChanged("PreserveAspectRatio"); }
        }
        public double ImageOpacity
        {
            get { return imageOpacity; }
            set { imageOpacity = value; OnPropertyChanged("ImageOpacity"); }
        }
        public double Width { get { return ScreenImagePixelBounds.Width; } }
        public double Height { get { return ScreenImagePixelBounds.Height; } }
        public double Left { get { return ScreenImagePixelBounds.Left; } }
        public double Top { get { return ScreenImagePixelBounds.Top; } }
        public RotateTransform Rotation { get; private set; }
        public Rect ScreenImagePixelBounds
        {
            get { return screenImagePixelBounds; }
            set
            {
                screenImagePixelBounds = value;
                OnPropertyChanged("Width");
                OnPropertyChanged("Height");
                OnPropertyChanged("Left");
                OnPropertyChanged("Top");
                OnPropertyChanged("ScreenImagePixelBounds");
            }
        }
        public ImageSource ImageSource { get; set; }
        public ICommand RestoreAspectRatioCommand
        {
            get
            {
                if (restoreAspectRatioCommand == null)
                {
                    restoreAspectRatioCommand = new DelegateCommand(RestoreAspectRatio);
                }
                return restoreAspectRatioCommand;
            }
        }
        #region Constructor
        public ImageRegistrationViewModel(Rect startRect, MapElementViewModel element)
        {
            parentVM = element;
            ScreenImagePixelBounds = startRect;
            ImageSource = element.GeoImage;
            Rotation = new RotateTransform(element.GeoImageTransform.Angle);
            CalculateOriginalAspectRatio();
        }
        #endregion
        protected void CalculateOriginalAspectRatio()
        {
            var w = ScreenImagePixelBounds.Width;
            var h = ScreenImagePixelBounds.Height;
            var wO = 0d;
            var hO = 0d;

            if (ImageSource == null)
            {
                OriginalImageAspectRatio = 1;
            }
            else
            {
                wO = ImageSource.Width;
                hO = ImageSource.Height;
                OriginalImageAspectRatio = wO / hO;
            }

            if (h.Equals(double.NaN) || h == 0 || w == 0 || w.Equals(double.NaN))
            {
                AspectRatio = OriginalImageAspectRatio;
            }
            else
            {
                if (parentVM._elementModel.GeoLocation == null)
                {                    
                    if (wO != 0 && hO != 0)
                    {
                        var centerX = ScreenImagePixelBounds.Left + w / 2;
                        var centerY = ScreenImagePixelBounds.Top + h / 2;
                        var newW = w;
                        var newH = h;
                        if (hO > wO)
                        {
                             newW = h * wO / hO;
                        }
                        else
                        {
                            newH = w * hO / wO;
                        }

                        ScreenImagePixelBounds = new Rect(new Point(centerX-(newW/2),centerY -(newH/2)), new Size(newW, newH));
                        AspectRatio = newW / newH;
                    }
                }
                else
                {
                    AspectRatio = w / h;
                }
            }
        }
        private void RestoreAspectRatio()
        {
            double hDif = ScreenImagePixelBounds.Height - ScreenImagePixelBounds.Width / OriginalImageAspectRatio;
            var startArea = ScreenImagePixelBounds;//new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), Width, Height);
            double radAngle = DegreesToRadians(Rotation.Angle);

            var outSize = AspectAndRotationShifter(0, -hDif, 50, "bottomright", startArea, false, radAngle, new Point(.5, .5));

            var finalH = outSize.Height;
            var finalW = outSize.Width;
            var top = outSize.Top;
            var left = outSize.Left;
            ScreenImagePixelBounds = new Rect(left, top, finalW, finalH);
        }
        public Rect AspectAndRotationShifter(double dx, double dy, double minSize, string thumbType,
                                             Rect inArea, bool preserveAspect, double radAngle, Point renderTOrigin)
        {
            var w = inArea.Width;
            var h = inArea.Height;
            var top = inArea.Top;
            var left = inArea.Left;
            var finalW = w;
            var finalH = h;
            double deltaVertical;
            double deltaHorizontal;
            if (thumbType.Contains("right"))
            {
                deltaHorizontal = Math.Min(-dx, w - minSize);
                top -= renderTOrigin.Y * deltaHorizontal * Math.Sin(radAngle);
                left += (deltaHorizontal * renderTOrigin.X * (1 - Math.Cos(radAngle)));
                w -= deltaHorizontal;
                finalW = w;
            }
            else if (thumbType.Contains("left"))
            {
                deltaHorizontal = Math.Min(dx, w - minSize);
                top += deltaHorizontal * Math.Sin(radAngle) - renderTOrigin.X * deltaHorizontal * Math.Sin(radAngle);
                left += deltaHorizontal * Math.Cos(radAngle) + (renderTOrigin.X * deltaHorizontal * (1 - Math.Cos(radAngle)));
                w -= deltaHorizontal;
                finalW = w;
            }

            if (thumbType.Contains("top"))
            {
                deltaVertical = Math.Min(dy, h - minSize);
                top += deltaVertical * Math.Cos(-radAngle) + (renderTOrigin.Y * deltaVertical * (1 - Math.Cos(-radAngle)));
                left += deltaVertical * Math.Sin(-radAngle) - (renderTOrigin.Y * deltaVertical * Math.Sin(-radAngle));
                h -= deltaVertical;
                finalH = h;
            }
            else if (thumbType.Contains("bottom"))
            {
                deltaVertical = Math.Min(-dy, h - minSize);
                top += (renderTOrigin.Y * deltaVertical * (1 - Math.Cos(-radAngle)));
                left -= deltaVertical * renderTOrigin.Y * Math.Sin(-radAngle);
                h -= deltaVertical;
                finalH = h;
            }
            return new Rect(left, top, finalW, finalH);
        }

        public static double DegreesToRadians(double Degrees)
        {
            return (Math.PI / 180) * Degrees;
        }
    }
}