﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;
using Depiction2.Core.ViewModels.InteractionViewModels;

namespace Depiction2.Core.ViewModels.Interactions
{
    public class InteractionRepositoryViewModel : ViewModelBase
    {
        #region variables

        private IInteractionRuleRepository _allInteractions;
        private InteractionInformationViewModel _selectedListInteraction;
        private InteractionEditorViewModel _selectedEditInteraction;

        #endregion

        public InteractionInformationViewModel SelectedListInteraction
        {
            get { return _selectedListInteraction; }
            set
            {
                _selectedListInteraction = value;
                if (_selectedEditInteraction != null) _selectedEditInteraction.Dispose();
                SelectedEditInteraction = new InteractionEditorViewModel(_selectedListInteraction.RuleModel);

                OnPropertyChanged("SelectedListInteraction");
            }
        }
        public InteractionEditorViewModel SelectedEditInteraction
        {
            get { return _selectedEditInteraction; }
            set
            {
                _selectedEditInteraction = value;
                OnPropertyChanged("SelectedEditInteraction");
            }
        }
        public ObservableCollection<InteractionInformationViewModel> AllInteractions { get; set; }
        public ObservableCollection<InteractionInformationViewModel> ActiveStoryInteractions { get; set; }

        #region commands

        private DelegateCommand _refreshInteractionsCommand;
        public ICommand RefreshInteractionsComand
        {
            get
            {
                if (_refreshInteractionsCommand == null)
                {
                    _refreshInteractionsCommand = new DelegateCommand(UpdateInteractionViews);
                }
                return _refreshInteractionsCommand;
            }
        }
        #endregion
        #region constructor
        public InteractionRepositoryViewModel(IInteractionRuleRepository interactionRules)
        {
            _allInteractions = interactionRules;
            AllInteractions = new ObservableCollection<InteractionInformationViewModel>();
            ActiveStoryInteractions = new ObservableCollection<InteractionInformationViewModel>();
            UpdateInteractionViews();
        }
        #endregion

        #region setup helpers
        private void UpdateInteractionViews()
        {
            AllInteractions.Clear();
            ActiveStoryInteractions.Clear();
            foreach (var interaction in _allInteractions.ActiveStoryInteractionRules)
            {
                ActiveStoryInteractions.Add(new InteractionInformationViewModel(interaction));
            }
            foreach (var interaction in _allInteractions.DefaultInteractionRules)
            {
                AllInteractions.Add(new InteractionInformationViewModel(interaction));
            }
        }
        #endregion
    }
}