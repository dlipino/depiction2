﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.InteractionViewModels;

namespace Depiction2.Core.ViewModels.Interactions
{
    public class InteractionRouterControllerViewModel : ViewModelBase
    {
        private IStory _mainStory;
        private IInteractionRouter _interactionRouter;

        #region properties and commands
        #region properties

        public ObservableCollection<InteractionControllerViewModel> InactiveInteractions { get; set; }
        public ObservableCollection<InteractionControllerViewModel> ActiveInteractions { get; set; } 
        #endregion
        #region commands and associatedc methods
        private DelegateCommand stopInteractionsCommand;
        public ICommand StopInteractionsCommand
        {
            get
            {
                if (stopInteractionsCommand == null)
                {
                    stopInteractionsCommand = new DelegateCommand(StopInteractions);
                }
                return stopInteractionsCommand;
            }
        }

        private void StopInteractions()
        {
            _interactionRouter.EndAsync();
        }

        private DelegateCommand startInteractionsCommand;
        public ICommand StartInteractionsCommand
        {
            get
            {
                if (startInteractionsCommand == null)
                {
                    startInteractionsCommand = new DelegateCommand(StartInteractions);
                }
                return startInteractionsCommand;
            }
        }

        private void StartInteractions()
        {
            _interactionRouter.BeginAsync();
        }

        private DelegateCommand disableSelectedInteractionsCommand;
        public ICommand DisableSelectedInteractionsCommand
        {
            get
            {
                if(disableSelectedInteractionsCommand == null)
                {
                    disableSelectedInteractionsCommand = new DelegateCommand(DisableSelectedActiveInteractions);
                }
                return disableSelectedInteractionsCommand;
            }
        }

        private void DisableSelectedActiveInteractions()
        {
            foreach (var interaction in ActiveInteractions)
            {
                if (interaction.Selected)
                {
                    interaction.IsDisabled = true;
                }
            }
            UpdateInteractionLists();
        }

        private DelegateCommand enableSelectedInteractionsCommand;
        public ICommand EnableSelectedInteractionsCommand
        {
            get
            {
                if (enableSelectedInteractionsCommand == null)
                {
                    enableSelectedInteractionsCommand = new DelegateCommand(EnableSelectedActiveInteractions);
                }
                return enableSelectedInteractionsCommand;
            }
        }

        private void EnableSelectedActiveInteractions()
        {
            foreach(var interaction in InactiveInteractions)
            {
                if(interaction.Selected)
                {
                    interaction.IsDisabled = false;
                }
            }
           UpdateInteractionLists();
        }

        #endregion

       
        #endregion

        public InteractionRouterControllerViewModel(IStory storyToControl)
        {
            _mainStory = storyToControl;
            _interactionRouter = storyToControl.InteractionsRunner;
            InactiveInteractions = new ObservableCollection<InteractionControllerViewModel>();
            ActiveInteractions = new ObservableCollection<InteractionControllerViewModel>();
            UpdateInteractionLists();


        }

        protected void UpdateInteractionLists()
        {
            InactiveInteractions.Clear();
            ActiveInteractions.Clear();
            foreach(var interaction in _interactionRouter.AvailableInteractions.AllInteractionRules)
            {
                if(interaction.IsDisabled)
                {
                    InactiveInteractions.Add(new InteractionControllerViewModel(interaction));
                }else
                {
                    ActiveInteractions.Add(new InteractionControllerViewModel(interaction));
                }
            }
        }



    }
}