﻿using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.ViewModels.Interactions
{
    public class InteractionLibraryDialogViewModel :DialogViewModel
    {
        public InteractionRepositoryViewModel InteractionRepository { get; set; }

        #region constructor
        public InteractionLibraryDialogViewModel(IInteractionRuleRepository interactionRules )
        {
            InteractionRepository = new InteractionRepositoryViewModel(interactionRules);
            IsModal = false;
            HideOnClose = true;
        }
        #endregion
    }
}