﻿using System;
using System.Windows;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels
{
    public class DepictionAnnotationVM : ViewModelBase, ISpatialElement
    {
        public event EventHandler SimpleBoundsChangedEvent;
        public IDepictionAnnotation annotationModel;

        private const double BaseFontSize = 14;
        private const double BaseBorderThickness = 1;
        private const double BasePadding = 4;
        private const double BaseIconSize = 20;

        #region Variables

        private double scaleWhenCreated;

        private Visibility collapsed = Visibility.Visible;
        #endregion

        #region stuff for zoomable, ick

        public string ElemId { get { return AnnotationID; } }
        //The index is in here because it was/is used by the microsoft created quadtree/zoomable canvas RealizeOverride
        public int Index { get; set; }//Hopeuflly this is temp, because it really messes things up when adding deleting and redrawing
        //index must match the location of the item in the collection that contains it
        public Rect SimpleBounds { get{return new Rect(annotationModel.GeoLocation,new Size(0,0));} }
        public double SimpleBoundsArea { get { return 0; } }

        #endregion

        #region Properties
        public string AnnotationID { get { return annotationModel.AnnotationID; } }

        public string BackgroundColor
        {
            get { return annotationModel.BackgroundColor; }
        }

        public string ForegroundColor
        {
            get { return annotationModel.ForegroundColor; }
        }

        public double ScaleWhenCreated
        {
            get { return annotationModel.ScaleWhenCreated; }
        }

        public Visibility AnnotationVisibility { get; private set; }

        public Visibility Collapsed
        {
            get { return collapsed; }
            set
            {
                collapsed = value;
                if (annotationModel != null) annotationModel.IsTextCollapsed = (collapsed == Visibility.Collapsed);
                OnPropertyChanged("Collapsed");
            }
        }

        public string AnnotationText
        {
            get { return annotationModel.AnnotationText; }
            set
            {
                if (annotationModel != null) annotationModel.AnnotationText = value;
            }
        }

        public double FontSize
        {
            get { return BaseFontSize; }
        }

        public double Padding
        {
            get { return BasePadding; }
        }

        public double IconSize
        {
            get { return BaseIconSize; }
        }

        public double BorderThickness
        {
            get { return BaseBorderThickness; }
        }

        public double Width
        {
            get { return annotationModel.PixelWidth; }
            set
            {
                var oldWidth = annotationModel.PixelWidth;
                var width = value;
                if (width <= 15)
                {
                    width = oldWidth;
                }
                if (annotationModel != null)
                {
                    annotationModel.PixelWidth = width;
                }
            }
        }

        public double Height
        {
            get { return annotationModel.PixelHeight; }
            set
            {
                var oldHeight = annotationModel.PixelHeight;
                var height = value;
                if (height <= 15) height = oldHeight;
                if (height >= 300) height = oldHeight;
                if (annotationModel != null)
                {
                    annotationModel.PixelHeight = height;
                }
            }
        }

        public double ContentLocationX
        {
            get { return annotationModel.ContentLocationX; }
            set
            {
                if (annotationModel != null)
                {
                    annotationModel.ContentLocationX = value;
                }
            }
        }

        public double ContentLocationY
        {
            get { return annotationModel.ContentLocationY; }
            set
            {
                if (annotationModel != null)
                {
                    annotationModel.ContentLocationY = value;
                }
            }
        }

        public bool IsElementIconDraggable { get { return true; } }

        public bool IsElementZOIDraggable { get { return true; } }

        public TranslateTransform IconCenterTransform { get; private set; }

        public bool EditingProperties { get; set; }//Indicates if there is a dialog that is editing the properties

        public double DrawLocationX
        {
            get;
            private set;
        }

        public double DrawLocationY
        {
            get;
            private set;
        }

        #endregion

        #region COnstructor

        public DepictionAnnotationVM(IDepictionAnnotation annotation, double currentScale)
        {
            AnnotationVisibility = Visibility.Visible;
            scaleWhenCreated = currentScale;
            IconCenterTransform = new TranslateTransform(-IconSize / 2, -IconSize / 2);
            if (annotation != null)
            {
                annotationModel = annotation;
                annotationModel.PropertyChanged += annotationModel_PropertyChanged;
                if (annotation.ScaleWhenCreated.Equals(double.NaN))
                {
                    annotation.ScaleWhenCreated = currentScale;
                }
                scaleWhenCreated = annotation.ScaleWhenCreated;
                AdjustVisibilityBasedOnVisibleScale(currentScale);
                collapsed = annotationModel.IsTextCollapsed ? Visibility.Collapsed : Visibility.Visible;

                SetAnnotationPixelLocationFromModelGeolocation();
            }
        }

        protected override void OnDispose()
        {
            annotationModel.PropertyChanged -= annotationModel_PropertyChanged;
            base.OnDispose();
        }

        void annotationModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("GeoLocation"))
            {
                SetAnnotationPixelLocationFromModelGeolocation();
            }
            else if (e.PropertyName.Equals("PixelWidth"))
            {
                OnPropertyChanged("Width");
            }
            else if (e.PropertyName.Equals("PixelHeight"))
            {
                OnPropertyChanged("Height");
            }
            else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }
        #endregion

        #region Helper methods
        public void SetModelGeoLocationFromProjectedPoint(double lat, double lon)
        {
            annotationModel.GeoLocation = GeoToDrawLocationService.ConvertDrawPointToGeoPoint(new Point(lon, lat));
        }
        public void SetAnnotationPixelLocationFromModelGeolocation()
        {
            var projectedLocation = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(annotationModel.GeoLocation);
            DrawLocationX = projectedLocation.X;
            DrawLocationY = projectedLocation.Y;
            OnPropertyChanged("DrawLocationX");
            OnPropertyChanged("DrawLocationY");
            if(SimpleBoundsChangedEvent != null)
            {
                SimpleBoundsChangedEvent(this,new EventArgs());
            }
        }
        private const int zoomInMax = 3;
        private const int zoomOutMax = 2;
        public void AdjustVisibilityBasedOnVisibleScale(double scale)
        {
            var previousState = AnnotationVisibility;
            if (scale > ScaleWhenCreated * 3//* zoomInMax
                || scale < (ScaleWhenCreated / 2.3))/// zoomOutMax))
            {
                
                AnnotationVisibility = Visibility.Hidden;
            }
            else
            {
                AnnotationVisibility = Visibility.Visible;
            }
            if(!previousState.Equals(AnnotationVisibility))
            {
                OnPropertyChanged("AnnotationVisibility");
            }
        }
        public void UpdateModelValuesFromViewModel()
        {

        }
        #endregion
    }
}