﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class PropertyTemplateViewModel : ViewModelBase, IDataErrorInfo
    {
        private string _properName;
        private string _displayName;
        private Type _propertyType;
        private object _propertyValue;
        private object _origValue;
        internal bool? OrigIsHoverText;
        private bool? _isHoverText;
        public bool ReadOnly { get; set; }

        //        public IValidationRule[] LegacyValidationRules { get; set; }
        //        public Dictionary<string, string[]> LegacyPostSetActions { get; set; }
        public string ProperName { get { return _properName; } set { _properName = value; OnPropertyChanged("ProperName"); } }
        public string DisplayName { get { return _displayName; } set { _displayName = value; OnPropertyChanged("DisplayName"); } }

        public bool? UseAsHoverText { get { return _isHoverText; } set { _isHoverText = value; OnPropertyChanged("UseAsHoverText"); } }
        public Type PropertyType { get { return _propertyType; } set { _propertyType = value; OnPropertyChanged("PropertyType"); } }
        public object PropertyValue
        {
            get { return _propertyValue; }
            set
            {
                _propertyValue = value;
                OnPropertyChanged("PropertyValue");
                OnPropertyChanged("ValueChanged");
            }
        }

        #region commands
        #region value change/restor

        public bool IsPreview { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsReadOnlyDisplayName { get; set; }
        public bool ValueChanged
        {
            get { return !Equals(PropertyValue, _origValue); }
        }
        private DelegateCommand restoryPropertyValueCommand;
        public ICommand RestorePropertyValueCommand
        {
            get
            {
                if (restoryPropertyValueCommand == null)
                {
                    restoryPropertyValueCommand = new DelegateCommand(RestorePropertyValue, IsDifferent);
                }
                return restoryPropertyValueCommand;
            }
        }
        private bool IsDifferent()
        {
            return ValueChanged;
        }

        virtual protected void RestorePropertyValue()
        {
            if (!ValueChanged) return;
            PropertyValue = DepictionAccess.GetDeepCopyOfPropertyValue(_origValue);
        }
        #endregion

        #region deleting the property
        private bool _deleteProp;
        public bool DeleteProp
        {
            get { return _deleteProp; }
            set
            {
                _deleteProp = value;
                OnPropertyChanged("DeleteProp");
            }
        }
        #endregion
        #endregion
        public PropertyTemplateViewModel(string properName)
            : this(properName, properName, typeof(string), string.Empty)
        {
        }
        public PropertyTemplateViewModel(string properName, string displayName, object value)
            : this(properName, displayName, value.GetType(), value)
        {
        }
        public PropertyTemplateViewModel(string properName, string displayName, object value, bool? isHoverText)
            : this(properName, displayName, value.GetType(), value)
        {
            OrigIsHoverText = isHoverText;
            UseAsHoverText = isHoverText;
        }
        public PropertyTemplateViewModel(string properName, string displayName, Type propertyType)
        {
            _properName = properName;
            _displayName = displayName;
            IsReadOnly = false;
            IsReadOnlyDisplayName = false;
            UseAsHoverText = false;
            if (propertyType == null)
            {
                _propertyType = typeof(string);
                _propertyValue = string.Empty;
            }
            else
            {
                if (propertyType.IsValueType)
                {
                    _propertyValue = Activator.CreateInstance(propertyType);
                }
                else
                {
                    if (propertyType == typeof(string))
                    {
                        _propertyValue = string.Empty;
                    }
                }
            }
        }
        public PropertyTemplateViewModel(string properName, string displayName, Type propertyType, object value)
        {
            _properName = properName;
            _displayName = displayName;

            //make sure the default value/type match
            if (propertyType == null && value == null)
            {
                propertyType = typeof(string);
                value = string.Empty;
            }
            if (propertyType != null && value == null)
            {
                if (propertyType.IsValueType)
                {
                    value = Activator.CreateInstance(propertyType);
                }
                else
                {
                    if (propertyType == typeof(string))
                    {
                        value = string.Empty;
                    }
                }
            }
            _propertyType = propertyType;
            _propertyValue = value;
            _origValue = DepictionAccess.GetDeepCopyOfPropertyValue(value);
            IsReadOnly = false;
            IsReadOnlyDisplayName = true;
            UseAsHoverText = false;
        }

        public bool ApplyViewModelChangeToOrig()
        {
            if (PropertyValue.Equals(_origValue)) return false;
            _origValue = DepictionAccess.GetDeepCopyOfPropertyValue(PropertyValue);
            OnPropertyChanged("ValueChanged");
            OrigIsHoverText = UseAsHoverText;
            return true;
        }
        public void ApplyOrigValueChange(object origValue)
        {
            _origValue = origValue;
            PropertyValue = DepictionAccess.GetDeepCopyOfPropertyValue(_origValue);
        }
        public IPropertyTemplate CreatePropertyScaffold()
        {
            var propertyScaffold = new PropertyTemplate();
            propertyScaffold.DisplayName = _displayName;
            propertyScaffold.ProperName = _properName;
            propertyScaffold.TypeString = _propertyType.Name;
            propertyScaffold.ValueString = _propertyValue.ToString();

            return propertyScaffold;
        }
        public IElementProperty CreateProperty()
        {
            var property = new ElementProperty(_properName, _displayName, _propertyValue);
            return property;

        }

        public void UpdateElementProperty(IElementProperty propertyToUpdate)
        {
            propertyToUpdate.DisplayName = _displayName;
            //triggers an event change
            propertyToUpdate.SetValue(_propertyValue);
        }
        public bool UpdateHoverTextStateForElement(IElement element)
        {
            if (element == null) return false;

            if (OrigIsHoverText != UseAsHoverText)
            {
                if (UseAsHoverText == true)
                {
                    return element.AddHoverTextProperty(ProperName, false);
                }
                if (UseAsHoverText == false)
                {
                    return element.RemoveHoverTextProperty(ProperName, false);
                }
            }
            return false;
        }
        #region Implementation of IDataErrorInfo

        private string errorString = string.Empty;
        public string this[string columnName]
        {
            get
            {
                errorString = string.Empty;
                if (PropertyValue == null)
                {
                    errorString = "Could not convert to type.";
                    return errorString;
                }

                return null;
            }
        }

        public string Error
        {
            get { return errorString; }
        }

        #endregion
    }

    public class PropertyTemplateVmNameComparer<T> : IEqualityComparer<T> where T : PropertyTemplateViewModel
    {

        public bool Equals(T x, T y)
        {
            if (x.ProperName.Equals(y.ProperName, StringComparison.InvariantCultureIgnoreCase)) return true;
            return false;
        }

        public int GetHashCode(T obj)
        {
            return obj.ProperName.ToLower().GetHashCode();
        }
    }
}