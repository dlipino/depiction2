﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Measurement;
using Depiction2.Base;
using Depiction2.Base.Helpers;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class GenericPropertyViewModel : PropertyViewModelBase, IDataErrorInfo
    {
        private object _modelValue;
        private bool _deleteProp;
        private bool _isPreview;
        private IValidationRule[] legacyValidationRules = null;
        #region properties

        virtual public bool ValueChanged
        {
            get { return !Equals(PropertyValue, _modelValue); }
        }
        public bool HoverTextChange
        {
            get { return UseAsHoverText != _origHoverText; }
        }
        public bool DeleteProp
        {
            get { return _deleteProp; }
            set { _deleteProp = value; OnPropertyChanged("DeleteProp"); OnPropertyChanged("ValueChanged"); }
        }
        public bool IsPreview
        {
            get { return _isPreview; }
            set { _isPreview = value; OnPropertyChanged("IsPreview"); }
        }
        override public object PropertyValue
        {
            get { return _propValue; }
            set
            {
                //Hacks from 1.4, possibly unavoidable
                //TODO does not work with IDataErrorInfo because the conversion to
                //IMeasurement fails on non numerical values, this needs to be fixed.
                if (PropertyType.GetInterface("IMeasurement") != null && value is double)
                {
                    var measure = _propValue as IMeasurement;
                    if (measure != null)
                    {
                        var numVal = (double)value;
                        //                                                ValueChanged = (measure.NumericValue != numVal);

                        measure.NumericValue = numVal;
                    }

                }
                else if (PropertyType.Equals(typeof(Angle)) && value is double)
                {
                    var angle = _propValue as Angle;
                    if (angle != null)
                    {
                        var angleVal = (double)value;
                        //                        ValueChanged = (angle.Value != angleVal);
                        angle.Value = angleVal;
                    }
                }
                else
                {
                    //                    ValueChanged = !Equals(PropertyModel.Value, value);
                    _propValue = value;
                }

                OnPropertyChanged(() => PropertyValue); OnPropertyChanged("ValueChanged");
            }
        }
        #endregion
        #region commands

        private DelegateCommand restoryPropertyValueCommand;
        public ICommand RestorePropertyValueCommand
        {
            get
            {
                if (restoryPropertyValueCommand == null)
                {
                    restoryPropertyValueCommand = new DelegateCommand(RestorePropertyValue, IsDifferent);
                }
                return restoryPropertyValueCommand;
            }
        }
        private bool IsDifferent()
        {
            return ValueChanged;
        }

        virtual protected void RestorePropertyValue()
        {
            if (!ValueChanged) return;
            PropertyValue = DepictionAccess.GetDeepCopyOfPropertyValue(_modelValue);
            OnPropertyChanged(() => PropertyValue);
        }


        #endregion

        #region constructor
        public GenericPropertyViewModel(IElementProperty propertyModel, bool? isHoverText) :
            this(propertyModel.ProperName, propertyModel.DisplayName, propertyModel.Value, propertyModel.ValueType, propertyModel.IsReadOnly, isHoverText)
        {
            legacyValidationRules = propertyModel.LegacyValidationRules;

        }
        public GenericPropertyViewModel(string properName, string displayName, object originalValue, Type valueType, bool isReadonly, bool? isHoverText)
        {
            IsReadOnly = isReadonly;
            _modelValue = originalValue;
            PropertyType = valueType;
            _propValue = DepictionAccess.GetDeepCopyOfPropertyValue(originalValue);
            PropertyName = properName;
            DisplayName = displayName;

            _useAsHoverText = _origHoverText = isHoverText;
        }

        #endregion

        public void SetValueAndUpdateCurrent(object newValueFromProperty)
        {
            _modelValue = newValueFromProperty;
            _propValue = DepictionAccess.GetDeepCopyOfPropertyValue(newValueFromProperty);
            OnPropertyChanged("PropertyValue"); OnPropertyChanged("ValueChanged");
        }

        public bool ApplyPropertyValueChange()
        {
            if (PropertyValue == null) return false;
            if (!ValueChanged) return false;
            SetValueAndUpdateCurrent(PropertyValue);
            return true;
        }
        override public void ApplyPropertyValueToElement(IElement element, bool checkForEquality)
        {
            if (PropertyValue == null) return;
            if (checkForEquality && !ValueChanged) return;
            if (element == null) return;

            var elemProp = element.GetDepictionProperty(PropertyName);
            if (elemProp != null)
            {
                elemProp.SetValue(DepictionAccess.GetDeepCopyOfPropertyValue(PropertyValue));
            }
        }

        public override string ToString()
        {
            return PropertyName;
        }

        #region Implementation of IDataErrorInfo

        private string errorString = string.Empty;
        public string this[string columnName]
        {
            get
            {
                errorString = string.Empty;
                if (PropertyValue == null)
                {
                    errorString = "Could not convert to type.";
                    return errorString;
                }
                if (legacyValidationRules == null) return null;
                foreach (var rule in legacyValidationRules)
                {
                    var result = rule.Validate(PropertyValue);
                    if (result.ValidationOutput == ValidationResultValues.InValid)
                    {
                        errorString = result.Message;
                        return errorString;
                    }
                }
                return null;
            }
        }

        public string Error
        {
            get { return errorString; }
        }

        #endregion
    }
}