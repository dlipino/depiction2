﻿using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class ExtraPropertyViewModel : GenericPropertyViewModel
    {
       
        private IElementProperty _propModel;
        #region properties
        public IElementProperty PropertyModel { get { return _propModel; } }


        #endregion

        #region constructors
        public ExtraPropertyViewModel(IElementProperty propertyModel, bool isHoverText)
            : base(propertyModel, isHoverText)
        {
            _propModel = propertyModel;
        }

        #endregion

//        #region overrides
//
//        override protected void RestorePropertyValue()
//        {
//            DeleteProp = false;
//            base.RestorePropertyValue();
//        }
//        #endregion
    }
}