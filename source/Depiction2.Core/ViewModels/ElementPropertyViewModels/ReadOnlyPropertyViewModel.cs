﻿namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class ReadOnlyPropertyViewModel : PropertyViewModelBase
    {
        internal ReadOnlyPropertyViewModel(string properName,  object value,bool isHoverText)
            : base(properName, properName, value, isHoverText)
        {
            IsReadOnly = true;
        }
        public ReadOnlyPropertyViewModel(string properName,string displayName, object value,bool isHoverText)
            : base(properName, displayName, value,isHoverText)
        {
            IsReadOnly = true;
        }

    }
}