﻿using System;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class PropertyViewModelBase : ViewModelBase
    {
        protected object _propValue;//the sample value when used in multipelement property
        protected bool? _origHoverText;
        protected bool? _useAsHoverText;
        #region properties

        public Type PropertyType { get; protected set; }

        virtual public object PropertyValue
        {
            get { return _propValue; }
            set
            {
                _propValue = value;
                OnPropertyChanged(() => PropertyValue);
            }
        }
        public string PropertyName { get; protected set; }
        public string DisplayName { get; protected set; }
        public bool IsReadOnly { get; protected set; }
        public bool HoverChanged { get { return UseAsHoverText != _origHoverText; } }
        virtual public bool? UseAsHoverText
        {
            get { return _useAsHoverText; }
            set { _useAsHoverText = value;OnPropertyChanged("UseAsHoverText"); }
        }

        #endregion

        #region constructor
        //TODO hmm what happens if thevalue is changed outside of the view, like position
        //these need to know that the value has changed
        protected PropertyViewModelBase(string properName, string displayName, object value, bool isHoverText)
        {
            PropertyName = properName;
            DisplayName = displayName;
            if (value != null)
            {
                PropertyType = value.GetType();
                _propValue = value;
            }
            IsReadOnly = false;
            _useAsHoverText = _origHoverText= isHoverText;
        }

        protected PropertyViewModelBase()
        {

        }

        #endregion

        public bool UpdateCurrentHoverText()
        {
            if(HoverChanged)
            {
                _origHoverText = UseAsHoverText;
                return true;
            }
            return false;
        }
        public bool UpdateHoverTextStateForElement(IElement element)
        {
            if (element == null) return false;

            if (HoverChanged)
            {
                if (UseAsHoverText == true)
                {
                    return element.AddHoverTextProperty(PropertyName,false);
                }
                if (UseAsHoverText == false)
                {
                    return element.RemoveHoverTextProperty(PropertyName,false);
                }
            }
            return false;
        }
        virtual public void ApplyPropertyValueToElement(IElement element,bool checkForEquality)
        {

           

        }
    }
}