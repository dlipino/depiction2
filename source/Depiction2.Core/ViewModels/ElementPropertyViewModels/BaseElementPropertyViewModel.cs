﻿using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    /**
     * these are the build in visual properties
     * */
    public class BaseElementPropertyViewModel : GenericPropertyViewModel
    {
        #region constructor

        public BaseElementPropertyViewModel(string properName, string displayName, object value, bool isReadonly)
            : base(properName, displayName, value, value.GetType(), isReadonly, false)
        { }
        public BaseElementPropertyViewModel(string properName, string displayName, object value)
            : this(properName, displayName, value, false)
        { }

        #endregion

        override public void ApplyPropertyValueToElement(IElement element, bool checkForEquality)
        {
            if (PropertyValue == null) return;
            if (checkForEquality && !ValueChanged) return;
            if (element == null) return;
            bool matchingProp = true;
            //Change the orginal value here
            if (PropertyName.Equals("ZOIBorderColor"))
            {
                element.ZOIBorderColor = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("ZOIFillColor"))
            {
                element.ZOIFillColor = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("ZOILineThickness"))
            {
                element.ZOILineThickness = double.Parse(PropertyValue.ToString());
            }
            else if (PropertyName.Equals("ZOIShapeType"))
            {
                element.ZOIShapeType = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("IconBorderShape"))
            {
                element.IconBorderShape = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("IconBorderColor"))
            {
                element.IconBorderColor = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("IconSize"))
            {
                element.IconSize = double.Parse(PropertyValue.ToString());
            }
            else if (PropertyName.Equals("IconResourceName"))
            {
                element.IconResourceName = PropertyValue.ToString();
            }
            else if (PropertyName.Equals("VisualState"))
            {
                element.VisualState = (ElementVisualSetting)PropertyValue;
            }
            else if (PropertyName.Equals("IsDraggable"))
            {
                element.IsDraggable = (bool)PropertyValue;
            }
//            else if (PropertyName.Equals("UseEnhancedInformationText"))
//            {
//                element.UseEnhancedInformationText = (bool)PropertyValue;
//            }
//            else if (PropertyName.Equals("UsePropertyNamesInInformationText"))
//            {
//                element.UsePropertyNamesInInformationText = (bool)PropertyValue;
//            }
            else
            {
                matchingProp = false;
            }
            //Why don't the props trigger their own change?
            if (matchingProp) element.TriggerPropertyChange(PropertyName);
        }
    }
}