﻿using System;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.ElementPropertyViewModels
{
    public class MultiElementPropertyViewModel : PropertyTemplateViewModel
    {
        public event Action<string, object> RequestMultiElementPropertyChange;
        
        public object MinValue { get; set; }
        public object MaxValue { get; set; }
        public string ValueRange { get { return string.Format("Min {0} - Max{1}", MinValue, MaxValue); } }
        public string TypeString { get { return PropertyType.Name; } }
        

        #region constructor
        public MultiElementPropertyViewModel(string properName, string displayName, object sampleValue, bool? isHoverText)
            : base(properName, displayName, sampleValue, isHoverText)
        {
            
        }
        public MultiElementPropertyViewModel(string properName, string displayName, object sampleValue)
            : base(properName, displayName, sampleValue)
        {

        }
        #endregion


    }
}