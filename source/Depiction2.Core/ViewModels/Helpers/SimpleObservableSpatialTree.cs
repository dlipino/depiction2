﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Depiction2.API;

namespace Depiction2.Core.ViewModels.Helpers
{
    //mostly copied from the one in zoomable canvas
    public class SimpleObservableSpatialTree<T> : ObservableCollection<T>, ZoomableCanvas.ISpatialItemsSource where T : ISpatialElement
    {
        /// <summary>
        /// We use a PriorityQuadTree to implement our spatial index.
        /// </summary>
        private readonly PriorityQuadTree<T> _tree = new PriorityQuadTree<T>();

        /// <summary>
        /// Holds the accurate extent of all item geoBounds in the index.  This may be different from _tree.Extent.
        /// </summary>
        private Rect _extent = Rect.Empty;

        /// <summary>
        /// Holds the last query used in order to know when to raise the <see cref="QueryInvalidated"/> event.
        /// </summary>
        private Rect _lastQuery = Rect.Empty;

        /// <summary>
        /// Occurs when the value of the <see cref="Extent"/> property has changed.
        /// </summary>
        public event EventHandler ExtentChanged;

        /// <summary>
        /// Occurs when the results of the last query are no longer valid and should be re-queried.
        /// </summary>
        public event EventHandler QueryInvalidated;

        /// <summary>
        /// Get a list of the items that intersect the given geoBounds.
        /// </summary>
        /// <param name="geoBounds">The geoBounds to test.</param>
        /// <returns>
        /// List of zero or more items that intersect the given geoBounds, returned in the order given by the priority assigned during Insert.
        /// </returns>

        public IEnumerable<int> Query(Rect rectangle)
        {
            var story = DepictionAccess.DStory;
            if (story == null)
            {
                return _tree.GetItemsIntersecting(rectangle).Select(i => i.Index);
            }
            //rect that comes in is pixel viewport, turn that into something
            //depiction can use
            var geoBounds = GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(rectangle);
            return _tree.GetItemsIntersecting(geoBounds.WpfRect).Select(i => i.Index); ;
        }

        /// <summary>
        /// Gets the computed minimum required rectangle to contain all of the items in the index.  This property is also settable for efficiency the future extent of the items is known.
        /// </summary>
        public Rect Extent
        {
            get
            {
                if (_extent.IsEmpty)
                {
                    foreach (var item in _tree)
                    {
                        _extent.Union(item.SimpleBounds);
                    }
                }
                return _extent;
            }
        }
        //Duplicates/double add? do events double up?
        public void AddSpatial(T toAdd)
        {
            _tree.Insert(toAdd, toAdd.SimpleBounds, Double.PositiveInfinity);
            toAdd.Index = Count;
            toAdd.SimpleBoundsChangedEvent += item_SimpleBoundsChangedEvent;
            Add(toAdd);
            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
        }

        void item_SimpleBoundsChangedEvent(object sender, EventArgs e)
        {
            if (!(sender is T)) return;
            var spatialItem = (T)sender;
            _tree.Remove(spatialItem);
            _tree.Insert(spatialItem, spatialItem.SimpleBounds, Double.PositiveInfinity);
        }

        public void RemoveSpatial(T toRemove)
        {
            Remove(toRemove);
            _tree.Remove(toRemove, toRemove.SimpleBounds);
            for (int i = 0; i < Count; i++)
            {
                this[i].Index = i;
            }
            toRemove.SimpleBoundsChangedEvent -= item_SimpleBoundsChangedEvent;
            if (ExtentChanged != null)
            {
                ExtentChanged(this, EventArgs.Empty);
            }

            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Optimizes the spatial index based on the current extent if optimization is warranted.
        /// </summary>
        public void Optimize()
        {
            var treeExtent = _tree.Extent;
            var realExtent = Extent;
            if (treeExtent.Top - realExtent.Top > treeExtent.Height ||
                treeExtent.Left - realExtent.Left > treeExtent.Width ||
                realExtent.Right - treeExtent.Right > treeExtent.Width ||
                realExtent.Bottom - treeExtent.Bottom > treeExtent.Height)
            {
                _tree.Extent = realExtent;

                if (QueryInvalidated != null)
                {
                    QueryInvalidated(this, EventArgs.Empty);
                }
            }
        }

    }
}