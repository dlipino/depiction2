﻿using System;
using System.Windows;

namespace Depiction2.Core.ViewModels.Helpers
{
    public interface ISpatialElement
    {
        //Used for adding/removing/updating the element if it is in a quad tree.
        event EventHandler SimpleBoundsChangedEvent;//Can be improved by greating BoundsChangeEventArgs that holds old bounds
        string ElemId { get; }
        //The index is in here because it was/is used by the microsoft created quadtree/zoomable canvas RealizeOverride
        int Index { get; set; }//Hopeuflly this is temp, because it really messes things up when adding deleting and redrawing
        //index must match the location of the item in the collection that contains it
        Rect SimpleBounds { get; }
        double SimpleBoundsArea { get; }
    }
}