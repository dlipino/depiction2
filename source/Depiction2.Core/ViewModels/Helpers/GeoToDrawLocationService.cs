﻿using System;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.Utilities;

namespace Depiction2.Core.ViewModels.Helpers
{
    public class GeoToDrawLocationService
    {
        //Assumes the map covers the entire window, 
        public static Size ScreenPixelSize { get; set; }
        public static Rect CanvasPixelExtent { get; set; }
        public static Func<Point, Point> CartCanvasToScreen { get; set; }
        public static Func<Point, Point> ScreenToCartCanvas { get; set; }


        //Find the geo width of the visible story
        static public ICartRect GetStoryGeoExtent()
        {
            return ConvertDrawRectToStoryGeoRect(CanvasPixelExtent);
        }
        
        static public ICartRect ConvertDrawRectToStoryGeoRect(Rect drawRect)
        {
            //Giving it a negative sign could cause issues if in UTM projection system
            //but keeping it just to see if ti really does.
            //should probably make a test.
            var topLeft = new Point(drawRect.TopLeft.X, -drawRect.TopLeft.Y);
            var botRight = new Point(drawRect.BottomRight.X, -drawRect.BottomRight.Y);
            var tlC = CoordinateSystemService.Instance.PointDisplayToStory(topLeft);
            var brC = CoordinateSystemService.Instance.PointDisplayToStory(botRight); 
            return new CartRect(tlC, brC);
        }

        static public Rect ConvertGCSRectToDrawRect(ICartRect gcsRect)
        {
            var topLeft = new Point(gcsRect.Left,gcsRect.Top);
            var botRight = new Point(gcsRect.Right,gcsRect.Bottom);// gcsRect.BottomRight;
            var tlC = ConvertGCSPointToDrawPoint(topLeft);
            var brC = ConvertGCSPointToDrawPoint(botRight);
            return new Rect(tlC, brC);
        }

        static public double GetGCSToDrawScaleMult(Point oldCenter, Point newCenter)
        {
            var xMult = newCenter.X / oldCenter.X;
            var yMult = newCenter.Y / oldCenter.Y;
            return xMult;
        }
        static public Point ConvertGCSPointToDrawPoint(Point geoPoint)
        {
            Point drawPoint;
            var projPoint = CoordinateSystemService.Instance.PointStoryToDisplay(geoPoint);
            drawPoint = LocationConverter.ConvertCartisianToPanelPoint(projPoint);

            return drawPoint;
        }

        static public Point ConvertDrawPointToGeoPoint(Point drawPoint)
        {
            return CoordinateSystemService.Instance.PointDisplayToStory(drawPoint);
        }

        //        public Point ConvertViewPointToDrawPoint(Point viewPoint)
        //        {
        //            var scalePos = (Vector)viewPoint / StoryZoom;
        //            var mapPoint = (Point)((Vector)VirtualPanelOffset / StoryZoom + scalePos);
        //            return mapPoint;
        //        }
        //        public Point TopLeftLatLon()
        //        {
        //            var topLeftDraw = ConvertViewPointToDrawPoint(new Point(0, 0));
        //            var topLeftCartisian = LocationConverter.ConvertPanelToCartisianPoint(topLeftDraw);
        //            var topLeftLatLon = _storyModel.GetLatLon(topLeftCartisian.X, topLeftCartisian.Y);
        //            return topLeftLatLon;
        //        }
        //
        //        public Point BottomRightLatLon()
        //        {
        //            var bottomRightDraw = ConvertViewPointToDrawPoint(new Point(MapPixelSize.Width, MapPixelSize.Height));
        //            var bottomRightCartesian = LocationConverter.ConvertPanelToCartisianPoint(bottomRightDraw);
        //            var bottomRightLatLon = _storyModel.GetLatLon(bottomRightCartesian.X, bottomRightCartesian.Y);
        //            return bottomRightLatLon;
        //        }
    }
}