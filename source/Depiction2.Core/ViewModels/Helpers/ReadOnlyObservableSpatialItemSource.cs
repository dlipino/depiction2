﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Depiction2.API;
using Depiction2.Core.Entities.Element.ViewModel;

namespace Depiction2.Core.ViewModels.Helpers
{
    public class ReadOnlyObservableSpatialItemSource<T> : ReadOnlyObservableCollection<T>, ZoomableCanvas.ISpatialItemsSource where T : MapElementViewModel
    {
        public event EventHandler ExtentChanged;
        public event EventHandler QueryInvalidated;

        #region Properties

        protected HashSet<string> displayedElementIds;
        public Rect Extent
        {
            get { return Rect.Empty; }
        }
        #endregion

        #region constructor

        public ReadOnlyObservableSpatialItemSource(ObservableCollection<T> list)
            : base(list)
        {
            displayedElementIds = new HashSet<string>();
        }
        #endregion

        #region methods
        public IEnumerable<int> Query(Rect rectangle)
        {
           
            var story = DepictionAccess.DStory;
            if(story == null)
            {
                var matchId = this.Where(x => displayedElementIds.Contains(x.ElemId)).ToList();
                var idexes = matchId.OrderByDescending(i => i.SimpleBoundsArea).Select(i => i.Index);
                return idexes;
            }else
            {
                //rect that comes in is pixel viewport, turn that into something
                //depiction can use
                var geoBounds = GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(rectangle);//,story.StoryProjector);
                var allContainedIds = story.GetElementIdsInRect(geoBounds);
                allContainedIds.IntersectWith(displayedElementIds);
                var matchId = this.Where(x => allContainedIds.Contains(x.ElemId)).ToList();
                var idexes = matchId.OrderByDescending(i => i.SimpleBoundsArea).Select(i => i.Index);
                return idexes;
            }
        }

        public void AddIds(IEnumerable<string> ids)
        {
            if (System.Windows.Application.Current != null)
            {
                if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                                                              new Action<IEnumerable<string>>(
                                                                                  AddIds), ids);
                    return;
                }
            }
            displayedElementIds.UnionWith(ids);
            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
        }

        public void RemoveIds(IEnumerable<string> ids)
        {
            if (System.Windows.Application.Current != null)
            {
                if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                                                              new Action<IEnumerable<string>>(
                                                                                  RemoveIds), ids);
                    return;
                }
            }
            foreach (var id in ids)
            {
                displayedElementIds.Remove(id);
            }
            if (QueryInvalidated != null)
            {
                QueryInvalidated(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}