﻿using System.Collections.ObjectModel;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.ExtensionViewModels
{
    public class MefExtensionViewModel : ViewModelBase
    {
        public ObservableCollection<MefExtensionViewModel> ExtensionViewModels { get; private set; }
        public string DisplayName { get; set; }
        public string Company { get; set; }
        public string Description { get; set; }

        #region constructor

        public MefExtensionViewModel(IDepictionExtensionMetadataBase metadata)
        {
            Company = metadata.Author;
            DisplayName = metadata.DisplayName;
            Description = metadata.Description;
        }

        #endregion
    }
}