﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Threading;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;

namespace Depiction2.Core.ViewModels.DialogViewModels
{
    //Manage content view model,or maybe just half of it. Just used to select items
    //this class does nothing with the selected items, its upto something else to do things.
    //This class is not in charge of changing what gets put into the view (ie the elements in the selection list). The
    //owner of this class is in charge of doing that. This is because the items that are displayed are not guaranteed to be
    //part of a complete collection.
    public class ElementSelectListViewModel : ViewModelBase
    {
        #region Variables
        private ListByMode groupingMode = ListByMode.NotSet;
        private string selectionFilter = string.Empty;
        //Seems like an un
        protected Dictionary<string, ListElementViewModel> originalElementDictionary = new Dictionary<string, ListElementViewModel>();
        protected ObservableCollection<ListElementViewModel> originalElementList = new ObservableCollection<ListElementViewModel>();
        private CollectionViewSource ManageElementsCollection { get; set; }
        #endregion

        #region Properties

        public object ElementBindList { get; private set; }//This changes depending of the sort type, hence object

        public Dictionary<ListByMode, string> GroupingSourceList { get; set; }
        public int SelectedCount { get { return SelectedElementIDs.Count; } }
        public int TotalElementCount { get { return originalElementList.Count; } }

        public bool ClearSelectionsOnFilter { get; set; }

        public string SelectionFilter
        {
            get { return selectionFilter; }
            set
            {
                if(selectionFilter == value) return;
                selectionFilter = value;
                ManageElementsCollection.Filter -= CollectionViewSource_Filter;
                if (!string.IsNullOrEmpty(selectionFilter))
                {
                    ManageElementsCollection.Filter += CollectionViewSource_Filter;
                }
                if(ClearSelectionsOnFilter)
                {
                    foreach (var element in originalElementList)
                    {
                        element.IsSelected = false;
                    } 
                }
                OnPropertyChanged("SelectionFilter");
            }
        }

        public ListByMode GroupingMode
        {
            get { return groupingMode; }
            set
            {

                SetElementGrouping(groupingMode, value);
                groupingMode = value;
                OnPropertyChanged("GroupingMode");
            }
        }

        public List<IElement> SelectedElementModels
        {
            get
            {
                if (originalElementList == null) return new List<IElement>();
                var result = originalElementList.Where(t => t.IsSelected).Select(t => t.modelBase).ToList();
                return result;
            }
        } 
        public List<string> SelectedElementIDs
        {
            get
            {
                if(originalElementList==null) return new List<string>();
                var result = originalElementList.Where(t => t.IsSelected).Select(t => t.ElementId).ToList();
                return result;
            }
        }

        #endregion

        #region Constructor/destructor
        
        public ElementSelectListViewModel(IEnumerable<IElement> initialElements)
        {
            var vmList = new List<ListElementViewModel>();
            foreach(var element in initialElements)
            {
                var lem = new ListElementViewModel(element);
                lem.PropertyChanged += ListElement_PropertyChanged;
                
                vmList.Add(lem);
            }
            originalElementDictionary = vmList.ToDictionary(t => t.ElementId);
            originalElementList = new ObservableCollection<ListElementViewModel>(originalElementDictionary.Values);
            GroupingSourceList = new Dictionary<ListByMode, string>
                             {
                                 {ListByMode.All, "Show all"},
                                 {ListByMode.Types, "List by types"},
                                 {ListByMode.Tags, "List by tags"}
                             };
            ManageElementsCollection = new CollectionViewSource();
            ManageElementsCollection.SortDescriptions.Add(new SortDescription("DisplayName", ListSortDirection.Ascending));
            ManageElementsCollection.Source = originalElementList;
            GroupingMode = ListByMode.Types;
        }

        
        
        protected override void OnDispose()
        {
            if(ManageElementsCollection != null)
            {
                ManageElementsCollection.Filter -= CollectionViewSource_Filter;
            }
            
            if (originalElementDictionary != null)
            {
                foreach (var elem in originalElementDictionary.Values)
                {
                    elem.PropertyChanged -= ListElement_PropertyChanged;
                    elem.Dispose();
                }
          
                originalElementDictionary.Clear();
            }
            if(originalElementList != null)
            {  //this looks pretty redundent
                foreach (var elem in originalElementList)
                {
                    elem.PropertyChanged -= ListElement_PropertyChanged;
                    elem.Dispose();
                }
                originalElementList.Clear();
            }

            ManageElementsCollection = null;
            originalElementList = null;
            originalElementDictionary = null;
            base.OnDispose();
        }
        #endregion

        #region protected list element event handlers

        protected virtual void ListElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //does nothing in this for this class since we can get the total selected eventually.
        }

        #endregion

        #region public helpers


        public void UpdateElementTagList()//Hoepuflly
        {
            if (GroupingMode.Equals(ListByMode.Tags))
            {
                SetElementGrouping(ListByMode.NotSet, ListByMode.Tags);
            }
        }
        //may 16 2013 pretty sure all the range stuff no longer works
        public void AddElementVMList(IEnumerable<IElement> elementsToAdd)
        {
            if (System.Windows.Application.Current != null)
            {
                if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                                                              new Action<List<IElement>>(
                                                                                  AddElementVMList), elementsToAdd);
                    return;
                }
            }

            if (elementsToAdd == null || !elementsToAdd.Any()) return;

            var elemList = elementsToAdd.ToList();

            if (elemList.Count() < 40)
            {
                foreach (var elem in elemList)
                {
                    var elemvm = new ListElementViewModel(elem);
                    if (!originalElementDictionary.ContainsKey(elem.ElementId))
                    {
                        elemvm.PropertyChanged += ListElement_PropertyChanged;
                        originalElementList.Add(elemvm);
                        originalElementDictionary.Add(elemvm.ElementId, elemvm);
                    }
                }
            }
            else
            {
                var vmList = new List<ListElementViewModel>();
                foreach (var elem in elemList)
                {
                    var lem = new ListElementViewModel(elem);
                    lem.PropertyChanged += ListElement_PropertyChanged;
                    vmList.Add(lem);
                }
                //Dispose call?
                originalElementDictionary = originalElementList.Concat(vmList).ToDictionary(t => t.ElementId);
                originalElementList = new ObservableCollection<ListElementViewModel>(originalElementDictionary.Values);
                ManageElementsCollection.Source = originalElementList;
                SetElementGrouping(ListByMode.NotSet, GroupingMode);
            }
//            OnPropertyChanged("TotalElementCount");
//            UpdateSelectedCount();
        }
        //This should remove by element key only
        public void RemoveElementByKeyVMList(List<string> elementVMKeys)
        {
//            if (DepictionAccess.DispatchAvailableAndNeeded())
//            {
//                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<List<string>>(RemoveElementByKeyVMList),
//                    elementVMKeys);
//                return;
//            }
            if (elementVMKeys == null || !elementVMKeys.Any()) return;
            if (elementVMKeys.Count < 40)
            {
                foreach (var elemVMKey in elementVMKeys)
                {
                    
                    if (originalElementDictionary.ContainsKey(elemVMKey))//Is try get value faster?
                    {
                        var toRemove = originalElementDictionary[elemVMKey];
                        toRemove.PropertyChanged -= ListElement_PropertyChanged;
                        originalElementList.Remove(toRemove);
                        originalElementDictionary.Remove(elemVMKey);
                    }
                }
            }
            else
            {
                //Dispose call?
                foreach (var key in elementVMKeys)
                {
                    if(originalElementDictionary.ContainsKey(key))
                    {
                        originalElementDictionary[key].PropertyChanged -= ListElement_PropertyChanged;
                        originalElementDictionary.Remove(key);
                    }
                }
                //A hack attempt to make sure not to many collection change events occur
                originalElementList = new ObservableCollection<ListElementViewModel>(originalElementDictionary.Values);
                ManageElementsCollection.Source = originalElementList;
                SetElementGrouping(ListByMode.NotSet, GroupingMode);
            }

//            OnPropertyChanged("TotalElementCount");
//            UpdateSelectedCount();
        }

        public void UpdateSelectedCount()
        {
            OnPropertyChanged("SelectedCount");
            OnPropertyChanged("SelectedElementIDs");
            OnPropertyChanged("ElementBindList");
        }

        #endregion

        #region selected element modifiers
        //No idea what the addToExisting does may 7 2013, oh wait, i think it helps control the view
        //when the view selects the ids the isSelected is done via the UI but if it needs to be done
        //from area select i think addToExisting needs to be set to true may 8 2013
        public void SelectElementsById(IEnumerable<string> elementIdList, bool addToExisting)
        {
            var statusChanged = false;
            if (originalElementList == null)
            {
                // UpdateSelectedCount();
                return;
            }
            if (!addToExisting)
            {
                foreach (var element in originalElementList)
                {
                    element.IsSelected = false;
                }
                statusChanged = true;
            }
            foreach (var id in elementIdList)
            {
                if (originalElementDictionary.ContainsKey(id))
                {
                    if (originalElementDictionary[id].IsSelected != true)
                    {
                        if (!statusChanged) statusChanged = true;
                        originalElementDictionary[id].IsSelected = true;
                    }
                }
            }
            //This is actually kind of strange, the count gets updated automatically, but the selected change does
            //not trigger which causes issues with other commands that use it as a paramter
            if (statusChanged)
            {
                UpdateSelectedCount();
            }
        }

        public void DeselectElementsById(IEnumerable<string> elementIdList)
        {

            foreach (var id in elementIdList)
            {
                if (originalElementDictionary.ContainsKey(id))
                {
                    originalElementDictionary[id].IsSelected = false;
                }
            }
            //This is actually kind of strange, the count gets updated automatically, but the selected change does
            //not trigger which causes issues with other commands that use it as a paramter
            UpdateSelectedCount();
        }
        #endregion

        #region private helper methods

        void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            var elementViewModel = e.Item as ListElementViewModel;
            if (elementViewModel == null) return;
            //TODO bring back filters
//            e.Accepted = Regex.IsMatch(elementViewModel.DisplayName, selectionFilter, RegexOptions.IgnoreCase) && !elementViewModel.TypeDisplayName.Equals("Road network");
        }

        private void SetElementGrouping(ListByMode oldMode, ListByMode groupMode)
        {
            if (Equals(oldMode, groupMode)) return;
            ManageElementsCollection.GroupDescriptions.Clear();
            switch (groupMode)
            {
                case ListByMode.All:
                    ElementBindList = ManageElementsCollection.View;
                    break;
                case ListByMode.Types:
                    ManageElementsCollection.GroupDescriptions.Add(new PropertyGroupDescription("TypeDisplayName"));
                    ElementBindList = ManageElementsCollection.View.Groups;
                    break;
                case ListByMode.Tags:
                    ManageElementsCollection.GroupDescriptions.Add(new PropertyGroupDescription("Tags"));
                    ElementBindList = ManageElementsCollection.View.Groups;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            OnPropertyChanged("ElementBindList");
        }
        #endregion
    }
    public class ElementViewModelKeyEqualityComparer<T> : IEqualityComparer<T> where T : ListElementViewModel
    {
        #region Implementation of IEqualityComparer<MapElementViewModel>

        public bool Equals(T x, T y)
        {
            return Equals(x.ElementId, y.ElementId);
        }

        public int GetHashCode(T obj)
        {
            return obj.ElementId.GetHashCode();
        }
        #endregion
    }
    public enum ListByMode
    {
        All,
        Types,
        Tags,
        NotSet
    }
}
