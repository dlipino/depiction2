﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Properties;
using Depiction2.Core.ViewModels.ElementTemplateViewModels;
using Depiction2.Core.ViewModels.QuickStart;
using DepictionLegacy.FileBrowser;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class AddElementDialogVM : DialogViewModel
    {
        //TODO refactor this out using the story add request action
//        public event Action<IElementTemplate, bool> MouseAddRequest;

        private string addElementMode = "Mouse";
        public ElementTemplateLibraryViewerViewModel ElementTemplateLibraryViewModel { get; private set; }
        public RegionSourcesViewerViewModel QuickStartSourcesViewModel { get; set; }

        public string AddElementMode
        {
            get { return addElementMode; }
            set
            {
                addElementMode = value;
                if (addElementMode.Equals("File") && SelectedTemplate == null && DepictionAccess.TemplateLibrary != null)
                {
                    ElementTemplateLibraryViewModel.SelectScaffoldType(string.Empty);
                }
                OnPropertyChanged("AddElementMode");
            }
        }
        public IElementTemplate SelectedTemplate
        {
            get
            {
                if (ElementTemplateLibraryViewModel == null) return null;
                return ElementTemplateLibraryViewModel.SelectedTemplate;
            }
        }

        #region constructor/destructor
        public AddElementDialogVM(IElementTemplateLibrary templateLibrary, IStory addTarget)
        {
            if (templateLibrary != null)
            {
                ElementTemplateLibraryViewModel = new ElementTemplateLibraryViewerViewModel(templateLibrary);
            }
            QuickStartSourcesViewModel = new RegionSourcesViewerViewModel(addTarget);

            HideOnClose = true;
            IsModal = false;
            AvailablePropertIds = new ObservableCollection<string>();
            FileImporterMetadatas = ExtensionService.Instance.FileLoaders.Select(t => t.Metadata);
        }
        protected override void OnDispose()
        {
            ElementTemplateLibraryViewModel.Dispose();
            AvailablePropertIds.Clear();
            base.OnDispose();
        }

        #endregion

        #region file add properties and variables

        private string fileName = string.Empty;
        private string importerInformation = string.Empty;
        private string _selectedIdProperty = string.Empty;

        public string FileNameWithElementsToAdd
        {
            get { return fileName; }
            set
            {
                fileName = value;
                if (File.Exists(fileName))
                {
                    AvailablePropertIds.Clear();

                    //                                        var props = selectedImporter.Value.FindPropertyIntersections(fileName);
                    //                                        foreach (var prop in props)
                    //                                        {
                    //                                            AvailablePropertIds.Add(prop);
                    //                                        }
                    if (AvailablePropertIds.Count == 0) AvailablePropertIds.Add(string.Empty);
                    SelectedIdProperty = AvailablePropertIds.First();
                }
                OnPropertyChanged("FileNameWithElementsToAdd");
            }
        }
        public string FileLoadInformationText
        {
            get { return importerInformation; }
            set { importerInformation = value; OnPropertyChanged(() => FileLoadInformationText); }
        }
        public IEnumerable<IDepictionFileLoaderMetadata> FileImporterMetadatas { get; private set; }
        public IFileLoaderExtension SelectedFileLoader { get; private set; }
        public IDepictionFileLoaderMetadata SelectedFileLoaderInfo { get; private set; }


        public ObservableCollection<string> AvailablePropertIds { get; private set; }

        public string SelectedIdProperty
        {
            get { return _selectedIdProperty; }
            set
            {
                _selectedIdProperty = value;
                OnPropertyChanged("SelectedIdProperty");
            }
        }

        private DelegateCommand openFileBrowserCommand;
        public ICommand OpenFileBrowserCommand
        {
            get
            {
                if (openFileBrowserCommand == null)
                {
                    openFileBrowserCommand = new DelegateCommand(DisplayFileBrowser);
                }
                return openFileBrowserCommand;
            }
        }
        private void DisplayFileBrowser()
        {
            var fileOpenLocation = Settings.Default.FileOpenLocation;
            if (string.IsNullOrEmpty(fileOpenLocation))
            {
                fileOpenLocation = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            var extensionsInfo = new Dictionary<string, KeyValuePair<string, IEnumerable<string>>>();
            string selected = string.Empty;
            foreach (var meta in FileImporterMetadatas)
            {
                var kvp = new KeyValuePair<string, IEnumerable<string>>(meta.DisplayName, meta.SupportedExtensions);
                if (!extensionsInfo.ContainsKey(meta.Name))
                {
                    extensionsInfo.Add(meta.Name, kvp);
                }
            }
            FileNameWithElementsToAdd = DepictionFileBrowser.OpenFileDialog("Open file", fileOpenLocation, extensionsInfo, out selected);
            if (string.IsNullOrEmpty(FileNameWithElementsToAdd) || !File.Exists(FileNameWithElementsToAdd)) return;
            var source = ExtensionService.Instance.FileLoaders.FirstOrDefault(t => t.Metadata.Name.Equals(selected));
            if (source != null)
            {
                SelectedFileLoaderInfo = source.Metadata;
                OnPropertyChanged("SelectedFileLoaderInfo");
                SelectedFileLoader = source.Value;

            }
            Settings.Default.FileOpenLocation = Path.GetDirectoryName(FileNameWithElementsToAdd);
            Settings.Default.Save();
        }

        private DelegateCommand addElementFileCommand;
        public ICommand AddElementFileCommand
        {
            get
            {
                if (addElementFileCommand == null)
                {
                    addElementFileCommand = new DelegateCommand(AddElementFile, CanAddFiles);
                    addElementFileCommand.Text = "Add Element";
                }
                return addElementFileCommand;
            }
        }

        private bool CanAddFiles()
        {
            if (string.IsNullOrEmpty(FileNameWithElementsToAdd) || !File.Exists(FileNameWithElementsToAdd))
                return false;
            if (ElementTemplateLibraryViewModel.SelectedTemplate == null) return false;
            return true;
        }

        private void AddElementFile()
        {
            var scaffold = ElementTemplateLibraryViewModel.SelectedTemplate;
            if (SelectedFileLoader == null) return;

            SelectedFileLoader.ImportElements(FileNameWithElementsToAdd, scaffold, SelectedIdProperty, null);
        }

        #endregion

        #region Mouse adding

        private bool doMultipleMouseAdd;
        public bool DoMouseMultipleAdd { get { return doMultipleMouseAdd; } set { doMultipleMouseAdd = value; OnPropertyChanged(() => DoMouseMultipleAdd); } }
        private DelegateCommand<IElementTemplate> addElementMouseCommand;
        public ICommand AddElementMouseCommand
        {
            get
            {
                if (addElementMouseCommand == null)
                {
                    addElementMouseCommand = new DelegateCommand<IElementTemplate>(AddMouseElement, IsSelectedScaffoldMouseAddable);
                    addElementMouseCommand.Text = "Add Element";
                }
                return addElementMouseCommand;
            }
        }

        private bool IsSelectedScaffoldMouseAddable(IElementTemplate arg)
        {
            if (arg == null) return false;
            return arg.IsMouseAddable;
        }

        protected void AddMouseElement(IElementTemplate elementTemplate)
        {

            var story = DepictionAccess.DStoryActions;
            if (story == null) return;
            story.TriggerStoryRequestMouseAdd(elementTemplate, DoMouseMultipleAdd);

//            if (MouseAddRequest != null)
//            {
//                MouseAddRequest(elementTemplate, false);
//            }
        }

        #endregion
    }
}