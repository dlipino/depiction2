﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Core.ViewModels.RevealerViewModels;
using Depiction2.Utilities;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class ElementDisplayControlDialogVM : DialogViewModel
    {

        public event Action<IDepictionRegion> RequestRegionAction;
        #region varialbes

        private IStory storyModel;
        DisplayerType displayType = DisplayerType.MainMap;
        private ElementRevealerListViewModel topRevealer;
        private RegionListViewModel _selectedRegion;

        #endregion
        #region properties

        public ElementViewSelectionListViewModel ElementsForMapPlacementVM { get; set; }
        public ObservableCollection<ElementRevealerListViewModel> Revealers { get; private set; }
        public ObservableCollection<RegionListViewModel> Regions { get; private set; }
        public DisplayerType DisplayType
        {
            get { return displayType; }
            set
            {
                displayType = value;
                OnPropertyChanged(() => DisplayType);
                if (displayType.Equals(DisplayerType.MainMap))
                {
                    ElementsForMapPlacementVM.SetDisplayerToControl(storyModel.MainDisplayer);
                }
                else
                {
                    if (ElementsForMapPlacementVM != null && CurrentActiveRevealer != null)
                    {
                        ElementsForMapPlacementVM.SetDisplayerToControl(CurrentActiveRevealer.displayerModel);

                        OnPropertyChanged("CurrentActiveRevealer");
                    }
                }
            }
        }
        //This is connected to the list so it is kind of special
        public ElementRevealerListViewModel CurrentActiveRevealer
        {
            get { return topRevealer; }
            set
            {
                if (value != null)
                {
                    storyModel.TopRevealerId = value.DisplayerId;
                }
                else
                {
                    storyModel.TopRevealerId = string.Empty;
                }
            }
        }
        public RegionListViewModel SelectedRegion
        {
            get { return _selectedRegion; }
            set { _selectedRegion = value; OnPropertyChanged("SelectedRegion"); }
        }
        #endregion
        #region constructor and dispose
        public ElementDisplayControlDialogVM(IStory story)
        {
            HideOnClose = true;
            IsModal = false;
            storyModel = story;
            var geoLocatedElements = story.AllElements.Where(t => t.GeoLocation != null);
            ElementsForMapPlacementVM = new ElementViewSelectionListViewModel(geoLocatedElements, story.MainDisplayer);
            //set up the revealers
            var revealerVmList = new List<ElementRevealerListViewModel>();
            foreach (var rev in story.RevealerDisplayers)
            {
                revealerVmList.Add(new ElementRevealerListViewModel(rev));
            }
            Revealers = new ObservableCollection<ElementRevealerListViewModel>(revealerVmList);
            var regionVmList = new List<RegionListViewModel>();
            foreach (var rev in story.AllRegions)
            {
                regionVmList.Add(new RegionListViewModel(rev));
            }
            Regions = new ObservableCollection<RegionListViewModel>(regionVmList);
            storyModel.RevealerCollectionChanged += RevealerDisplayers_CollectionChanged;
            storyModel.RegionCollectionChanged += storyModel_RegionCollectionChanged;
            storyModel.PropertyChanged += storyModel_PropertyChanged;
            storyModel.ElementCollectionChanged += AllElements_CollectionChanged;
            storyModel.ElementGeoLocationChanged += storyModel_ElementGeoLocationChanged;
        }

        protected override void OnDispose()
        {
            storyModel.RevealerCollectionChanged -= RevealerDisplayers_CollectionChanged;
            storyModel.RegionCollectionChanged -= storyModel_RegionCollectionChanged;
            storyModel.PropertyChanged -= storyModel_PropertyChanged;
            storyModel.ElementCollectionChanged -= AllElements_CollectionChanged;
            storyModel.ElementGeoLocationChanged -= storyModel_ElementGeoLocationChanged;
            base.OnDispose();
        }

        #endregion

        #region constructor event handlers
        #region helpers for updating, specifically when an element changes geolocated status
        void storyModel_ElementGeoLocationChanged(object sender, Base.Events.GeoLocationEventArgs e)
        {
            var elem = sender as IElement;
            if (elem == null) return;
            var idList = new List<string> { elem.ElementId };
            var elemList = new List<IElement> { elem };
            if (e.PreviousLocation == null && elem.GeoLocation != null)
            {
                ElementsForMapPlacementVM.AddElementVMList(elemList);
            }
            if (e.PreviousLocation != null && e.NewLocation == null)
            {
                ElementsForMapPlacementVM.RemoveElementByKeyVMList(idList);
            }
        }

        #endregion
        void storyModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("TopRevealerId"))
            {
                var topRev = Revealers.FirstOrDefault(t => t.DisplayerId.Equals(storyModel.TopRevealerId));

                topRevealer = topRev;
                if (DisplayType.Equals(DisplayerType.Revealer) && topRevealer != null)
                {
                    ElementsForMapPlacementVM.SetDisplayerToControl(CurrentActiveRevealer.displayerModel);
                }
                OnPropertyChanged("CurrentActiveRevealer");
            }
        }
        void storyModel_RegionCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                foreach (var item in e.NewItems)
                {
                    var rev = item as IDepictionRegion;
                    Regions.Add(new RegionListViewModel(rev));
                }
            }
            else if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                foreach (var item in e.OldItems)
                {
                    var rev = item as IDepictionRegion;
                    if (rev != null)
                    {
                        var matching = Regions.Where(t => t.RegionId.Equals(rev.DisplayerId)).ToList();
                        foreach (var matRev in matching)
                        {
                            Regions.Remove(matRev);
                        }
                    }
                }
            }
        }
        void RevealerDisplayers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add))
            {
                foreach (var item in e.NewItems)
                {
                    var rev = item as IElementRevealer;
                    Revealers.Add(new ElementRevealerListViewModel(rev));
                }
            }
            else if (e.Action.Equals(NotifyCollectionChangedAction.Remove))
            {
                foreach (var item in e.OldItems)
                {
                    var rev = item as IElementRevealer;
                    if (rev != null)
                    {
                        var matching = Revealers.Where(t => t.DisplayerId.Equals(rev.DisplayerId)).ToList();
                        foreach (var matRev in matching)
                        {
                            Revealers.Remove(matRev);
                        }
                    }
                }
            }
        }
        void AllElements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                AddElements(e.NewItems);
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                RemoveElementsWithIds(e.OldItems);
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {
                //Really hope it never comes to a reset
            }
        }
        protected void RemoveElementsWithIds(IList rawList)
        {
            if (rawList == null) return;
            var oldElements = rawList.Cast<IElement>().Select(t => t.ElementId).ToList();
            ElementsForMapPlacementVM.RemoveElementByKeyVMList(oldElements);

        }
        protected void AddElements(IList rawList)
        {
            if (rawList == null) return;
            var oldElements = rawList.Cast<IElement>().ToList();
            ElementsForMapPlacementVM.AddElementVMList(oldElements);
        }
        #endregion

        #region display control helpers

        //This currently doesn't work because the dialog service does not connected directly to the
        //dialogviewmodels. Not getting deleted as a reminder of how it should probably work
        //        public void ShowDisplayerDetails(string revealerId)
        //        {
        //            if(!string.IsNullOrEmpty(revealerId))
        //            {
        //                storyModel.TopRevealerId = revealerId;
        //                DisplayType = DisplayerType.Revealer;
        //                IsHidden = false;
        //            }
        //
        //        }

        #endregion
        #region revealer commmands

        private DelegateCommand addRevealer;
        public ICommand AddRevealerCommand
        {
            get
            {
                if (addRevealer == null)
                {
                    addRevealer = new DelegateCommand(AddRevealer);
                    addRevealer.Text = "New Revealer";
                }
                return addRevealer;
            }
        }
        protected void AddRevealer()
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var insideRect = RandomUtilities.GetRectInteriorToRect(GeoToDrawLocationService.CanvasPixelExtent);
            var insideGcs = GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(insideRect);
            //            var tl = GeoToDrawLocationService.ScreenToCartCanvas(insideRect.BottomLeft);
            //            var br = GeoToDrawLocationService.ScreenToCartCanvas(insideRect.TopRight);

            story.AddNewRevealer(insideGcs);
        }

        #endregion

        #region region comands
        private DelegateCommand addRegionCommand;
        public ICommand AddRegionCommand
        {
            get
            {
                if (addRegionCommand == null)
                {
                    addRegionCommand = new DelegateCommand(AddRegion);
                    addRegionCommand.Text = "New Region";
                }
                return addRegionCommand;
            }
        }
        protected void AddRegion()
        {
            if (RequestRegionAction != null)
            {
                RequestRegionAction(null);
            }
        }

        private DelegateCommand deleteRegionCommand;
        public ICommand DeleteRegionCommand
        {
            get
            {
                if (deleteRegionCommand == null)
                {
                    deleteRegionCommand = new DelegateCommand(DeleteRegion,HaveSelection);
                    deleteRegionCommand.Text = "Delete Region";
                }
                return deleteRegionCommand;
            }
        }

        protected void DeleteRegion()
        {
            DepictionAccess.DStory.RemoveRegion(SelectedRegion._regionBase);
        }

        private DelegateCommand adjustRegionCommand;
        public ICommand AdjustRegionCommand
        {
            get
            {
                if (adjustRegionCommand == null)
                {
                    adjustRegionCommand = new DelegateCommand(AdjustRegion, HaveSelection);
                    adjustRegionCommand.Text = "Adjust Region";
                }
                return adjustRegionCommand;
            }
        }

        protected void AdjustRegion()
        {
            if (RequestRegionAction != null)
            {
                RequestRegionAction(SelectedRegion._regionBase);
            }
        }
        private bool HaveSelection()
        {
            if (SelectedRegion == null) return false;
            return true;
        }


        #endregion
    }
    public enum DisplayerType
    {
        MainMap, Revealer, Region
    }
    public enum RegionActions
    {
        Create
    }
}