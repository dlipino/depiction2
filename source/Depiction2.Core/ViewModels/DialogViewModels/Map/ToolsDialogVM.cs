﻿using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class ToolsDialogVM : DialogViewModel
    {
        public ToolsDialogVM()
         {
             HideOnClose = true;
             IsModal = false;
         }
    }
}