﻿using System.Windows.Input;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;
using Depiction2.Core.ViewModels.DialogViewModels.Story;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class FileMenuDialogVM : DialogViewModel
    {
        private IDialogService _dialogService;
        private IStory _mainStory;
        public FileMenuDialogVM(IStory mainStory, IDialogService dialogService)
         {
             HideOnClose = true;
             IsModal = false;

             _dialogService = dialogService;
             _mainStory = mainStory;
         }

        #region story info

        private StoryInformationDialogViewModel storyInfoDialogVm;
        private DelegateCommand launchStoryInfoDialogCommand;

        public ICommand LaunchStoryInfoDialogCommand
        {
            get
            {

                if (launchStoryInfoDialogCommand == null)
                {
                    launchStoryInfoDialogCommand = new DelegateCommand(ShowStoryInfoDialog);
                }
                return launchStoryInfoDialogCommand;
            }
        }

        private void ShowStoryInfoDialog()
        {
            if (storyInfoDialogVm == null)
            {
                storyInfoDialogVm = new StoryInformationDialogViewModel(_mainStory);
            }
            storyInfoDialogVm.UpdateViewModel(_mainStory);
            _dialogService.ShowWindow(this, storyInfoDialogVm);
        }
        #endregion

    }
}