﻿using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class MapHelpDialogVM : DialogViewModel
    {
        public MapHelpDialogVM()
         {
             HideOnClose = true;
             IsModal = false;
         }
    }
}