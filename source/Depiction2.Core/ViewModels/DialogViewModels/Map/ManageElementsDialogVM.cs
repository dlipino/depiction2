﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.DialogViewModels.Map
{
    public class ManageElementsDialogVM : DialogViewModel
    {
        public event Action<IElement> GeolocateElementRequest;
        public event Action<List<IElement>> ShowPropertyRequest;

        #region variables

        private IStory storyModel;
        private ManageContentModality manageModality = ManageContentModality.GeoAligned;
        #endregion

        #region properties
        public ElementSelectListViewModel GeolocatedElements { get; private set; }
        public int GeoLocatedElementCount
        {
            get
            {
                if (GeolocatedElements == null) return 0;
                return GeolocatedElements.TotalElementCount;
            }
        }

        public ElementSelectListViewModel NonGeolocatedElements { get; private set; }
        public int NonGeoLocatedElementCount
        {
            get
            {
                if (NonGeolocatedElements == null) return 0;
                return NonGeolocatedElements.TotalElementCount;
            }
        }
        public ManageContentModality ManageModality
        {
            get { return manageModality; }
            set
            {
                if (!manageModality.Equals(value))
                {
                    manageModality = value;
                    OnPropertyChanged("ManageModality");
                }
            }
        }
        #endregion

        #region constructor/dispose

        protected ManageElementsDialogVM()
        {
            HideOnClose = true;
            IsModal = false;
        }

        public ManageElementsDialogVM(IStory story)
            : this()
        {
            var geoLocatedElements = story.AllElements.Where(t => t.GeoLocation != null);
            GeolocatedElements = new ElementSelectListViewModel(geoLocatedElements);
            var nonGeoLocatedElements = story.AllElements.Where(t => t.GeoLocation == null);
            NonGeolocatedElements = new ElementSelectListViewModel(nonGeoLocatedElements);
            storyModel = story;
            storyModel.ElementCollectionChanged += AllElements_CollectionChanged;
            storyModel.ElementGeoLocationChanged += storyModel_ElementGeoLocationChanged;
        }

        protected override void OnDispose()
        {
            GeolocatedElements.Dispose();
            NonGeolocatedElements.Dispose();
            storyModel.ElementCollectionChanged -= AllElements_CollectionChanged;
            storyModel.ElementGeoLocationChanged -= storyModel_ElementGeoLocationChanged;
            base.OnDispose();
        }
        #endregion
        #region constructor event handlers

        void AllElements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                AddElements(e.NewItems);
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                RemoveElementsWithIds(e.OldItems);
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {
                //Really hope it never comes to a reset
            }

            OnPropertyChanged(() => GeoLocatedElementCount);
            OnPropertyChanged(() => NonGeoLocatedElementCount);
        }
        protected void RemoveElementsWithIds(IList rawList)
        {
            if (rawList == null) return;
            var oldElements = rawList.Cast<IElement>().Select(t => t.ElementId).ToList();
            GeolocatedElements.RemoveElementByKeyVMList(oldElements);
            NonGeolocatedElements.RemoveElementByKeyVMList(oldElements);

        }
        protected void AddElements(IList rawList)
        {
            if (rawList == null) return;
            var newElements = rawList.Cast<IElement>().ToList();
            var geoLocatedElements = newElements.Where(t => t.GeoLocation != null).ToList();
            GeolocatedElements.AddElementVMList(geoLocatedElements);
            var nonGeoLocatedElements = newElements.Where(t => t.GeoLocation == null).ToList();
            NonGeolocatedElements.AddElementVMList(nonGeoLocatedElements);
        }
        #endregion
        #region helpers for updating, specifically when an element changes geolocated status
        void storyModel_ElementGeoLocationChanged(object sender, Base.Events.GeoLocationEventArgs e)
        {
            var elem = sender as IElement;
            if (elem == null) return;
            var idList = new List<string> { elem.ElementId };
            var elemList = new List<IElement> { elem };
            if(e.PreviousLocation == null && elem.GeoLocation != null)
            {
                NonGeolocatedElements.RemoveElementByKeyVMList(idList);
                GeolocatedElements.AddElementVMList(elemList);
            }
            if(e.PreviousLocation != null && e.NewLocation == null)
            {
                GeolocatedElements.RemoveElementByKeyVMList(idList);
                NonGeolocatedElements.AddElementVMList(elemList);
            }
            OnPropertyChanged("NonGeoLocatedElementCount");
            OnPropertyChanged("GeoLocatedElementCount");
        }

        #endregion
        #region commands
        bool SingleElement(ElementSelectListViewModel elements)
        {
            return elements.SelectedCount == 1;
        }
        bool AnyElements(ElementSelectListViewModel elements)
        {
            return elements.SelectedCount != 0;
        }
        private DelegateCommand<ElementSelectListViewModel> deleteSelectedElements;
        public ICommand DeleteSelectedElementsCommand
        {
            get
            {
                if (deleteSelectedElements == null)
                {
                    deleteSelectedElements = new DelegateCommand<ElementSelectListViewModel>(DeleteSelectedElements, AnyElements);
                    deleteSelectedElements.Text = "Delete Selected";
                }
                return deleteSelectedElements;
            }
        }
        protected void DeleteSelectedElements(ElementSelectListViewModel selectedList)
        {
            storyModel.RemoveElements(selectedList.SelectedElementModels);
        }
        private DelegateCommand<ElementSelectListViewModel> displaySelectedInMain;
        public ICommand DisplaySelectedElementsCommand
        {
            get
            {
                if (displaySelectedInMain == null)
                {
                    displaySelectedInMain = new DelegateCommand<ElementSelectListViewModel>(ShowSelectedElements, AnyElements);
                    displaySelectedInMain.Text = "Show Selected";
                }
                return displaySelectedInMain;
            }
        }
        protected void ShowSelectedElements(ElementSelectListViewModel selectedList)
        {
            var elementIds = selectedList.SelectedElementIDs;
            storyModel.MainDisplayer.AddDisplayElementsWithMatchingIds(elementIds);
        }
        private DelegateCommand<ElementSelectListViewModel> hideSelectedInMain;
        public ICommand HideSelectedElementsCommand
        {
            get
            {
                if (hideSelectedInMain == null)
                {
                    hideSelectedInMain = new DelegateCommand<ElementSelectListViewModel>(HideSelectedElements, AnyElements);
                    hideSelectedInMain.Text = "Hide Selected";
                }
                return hideSelectedInMain;
            }
        }
        protected void HideSelectedElements(ElementSelectListViewModel selectedList)
        {
            var elementIds = selectedList.SelectedElementIDs;
            storyModel.MainDisplayer.RemoveElementsWithMatchingIds(elementIds);
            
        }
        private DelegateCommand<ElementSelectListViewModel> centerOnElementCommand;
        public ICommand CenterOnElementCommand
        {
            get
            {
                if (centerOnElementCommand == null)
                {
                    centerOnElementCommand = new DelegateCommand<ElementSelectListViewModel>(CenterOnElement, SingleElement);
                    centerOnElementCommand.Text = "Center on Element";
                }
                return centerOnElementCommand;
            }
        }
        protected void CenterOnElement(ElementSelectListViewModel selectedList)
        {
            var elem = selectedList.SelectedElementModels.FirstOrDefault();
            if (elem == null) return;
            var loc = elem.GeoLocation;
            if (loc == null) return;
            storyModel.SetCenter((Point)loc);
        }

        private DelegateCommand<ElementSelectListViewModel> showElementInfoCommand;
        public ICommand ShowElementInfoCommand
        {
            get
            {
                if (showElementInfoCommand == null)
                {
                    showElementInfoCommand = new DelegateCommand<ElementSelectListViewModel>(ShowElementProperties, AnyElements);
                    showElementInfoCommand.Text = "Show properties";
                }
                return showElementInfoCommand;
            }
        }
        protected void ShowElementProperties(ElementSelectListViewModel selectedList)
        {
            if (selectedList == null) return;//This should throw some sort of error
            var elements = selectedList.SelectedElementModels;
           
            if(ShowPropertyRequest != null)
            {
                ShowPropertyRequest(elements);
            }
        }

        private DelegateCommand<ElementSelectListViewModel> geoLocateElementCommand;
        public ICommand GeoLocateElementCommand
        {
            get
            {
                if (geoLocateElementCommand == null)
                {
                    geoLocateElementCommand = new DelegateCommand<ElementSelectListViewModel>(GeolocateElement, SingleElement);
                    geoLocateElementCommand.Text = "Geolocate element";
                }
                return geoLocateElementCommand;
            }
        }
        protected void GeolocateElement(ElementSelectListViewModel selectedList)
        {
            var elem = selectedList.SelectedElementModels.FirstOrDefault();
            if (elem == null) return;
            if(GeolocateElementRequest != null)
            {
                GeolocateElementRequest(elem);
            }
        }

        #endregion
    }
    public enum ManageContentModality
    {
        GeoAligned,
        NonGeoAligned
    }
}