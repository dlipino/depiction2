﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Element;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels
{
    public class MultipleElementPropertiesDialogViewModel : DialogViewModel
    {
        private MultiElementPropertiesDialogTab _activeTab;
        private IList<IElement> _allElements;
        private MultiElementPropertyViewModel _propertyForMapping;
        private Color _startColor;
        private Color _endColor;
        private ObservableCollection<MultiElementPropertyViewModel> allCommonProperties;
        private List<MultiElementPropertyViewModel> defaultProperties;
        private List<MultiElementPropertyViewModel> baseCommonProperties;
        private List<MultiElementPropertyViewModel> extraCommonProperties;
        private bool? _origDisplayInformationText;
        private bool? _origUseEnhancedInformationText;
        private bool? _origUsePropertyNamesInInformationText;

        #region properties
        public MultiElementPropertiesDialogTab ActiveTab
        {
            get { return _activeTab; }
            set { _activeTab = value; OnPropertyChanged(() => ActiveTab); }
        }

        public ObservableCollection<MultiElementPropertyViewModel> AllCommonProperties { get { return allCommonProperties; } }
        public List<MultiElementPropertyViewModel> DefaultProperties { get { return defaultProperties; } }
        public List<MultiElementPropertyViewModel> BaseCommonProperties { get { return baseCommonProperties; } }
        public List<MultiElementPropertyViewModel> ExtraCommonProperties { get { return extraCommonProperties; } }

        public List<MultiElementPropertyViewModel> VisualProperties { get; private set; }
        public List<MultiElementPropertyViewModel> MapControlProperties { get; private set; }
        public Type SelectedType { get; set; }
        public string NewPropName { get; set; }
        public Dictionary<string, Type> PropertyTypes
        {
            get
            {
                return DepictionAccess.NameTypeService.NameTypeDictonary;
            }
        }
        #region visual properties

        public bool UsePropertyToControlColor { get; set; }
        public MultiElementPropertyViewModel PropertyForMapping
        {
            get { return _propertyForMapping; }
            set { _propertyForMapping = value; OnPropertyChanged("PropertyForMapping"); }
        }

        public bool ManuallySelectThematicMapRange { get; set; }
        public double ThematicMapStartValue { get; set; }
        public double ThematicMapEndValue { get; set; }
        //kind of nasty to use Color, but it comes from 1.4, and i currently dont feel like changing it
        public Color ThematicMapStartColor { get { return _startColor; } set { _startColor = value; OnPropertyChanged("ThematicMapStartColor"); } }
        public Color ThematicMapEndColor { get { return _endColor; } set { _endColor = value; OnPropertyChanged("ThematicMapEndColor"); } }

        public bool SelectedPropertyIsNumber
        {
            get
            {
                if (PropertyForMapping == null) return false;
                if (PropertyForMapping.PropertyType == typeof(int) || PropertyForMapping.PropertyType == typeof(double))
                    return true;
                return false;
            }
        }

        public bool? UsePropertyNamesInInformationText { get; set; }
        public bool? DisplayInformationText { get; set; }
        public bool? UseEnhancedInformationText { get; set; }

        #endregion

        #endregion
        #region commands

        #region add command
        private DelegateCommand addPropertyCommand;
        public ICommand AddPropertyCommand
        {
            get
            {
                if (addPropertyCommand == null)
                {
                    addPropertyCommand = new DelegateCommand(AddPropertyToElement);
                }
                return addPropertyCommand;
            }
        }

        public void AddPropertyToElement()
        {
            if (string.IsNullOrEmpty(NewPropName)) return;
            if (AllCommonProperties.Any(t => t.ProperName.Equals(NewPropName, StringComparison.InvariantCultureIgnoreCase)))
                return;
            var prop = new ElementProperty(NewPropName, SelectedType);
            var mProp = new MultiElementPropertyViewModel(NewPropName, NewPropName, prop.Value, false);
            mProp.IsPreview = true;
            AllCommonProperties.Add(mProp);
        }
        #endregion

        #region apply changes
        private DelegateCommand applyChangesCommand;
        public ICommand ApplyChangesCommand
        {
            get
            {
                if (applyChangesCommand == null)
                {
                    applyChangesCommand = new DelegateCommand(ApplyPropertyValueChanges);
                }
                return applyChangesCommand;
            }
        }

        private void ApplyPropertyValueChanges()
        {
            #region element changing

            foreach (var element in _allElements)
            {
                var hoverChange = false;
                if (DisplayInformationText != _origDisplayInformationText && DisplayInformationText != null)
                {
                    element.DisplayInformationText = (bool)DisplayInformationText;
                    hoverChange = true;
                }
                if (UseEnhancedInformationText != _origUseEnhancedInformationText && UseEnhancedInformationText != null)
                {
                    element.UseEnhancedInformationText = (bool)UseEnhancedInformationText;
                    hoverChange = true;
                }
                if (UsePropertyNamesInInformationText != _origUsePropertyNamesInInformationText && UsePropertyNamesInInformationText != null)
                {
                    element.UsePropertyNamesInInformationText = (bool)UsePropertyNamesInInformationText;
                    hoverChange = true;
                }
                if (ActiveTab.Equals(MultiElementPropertiesDialogTab.Properties))
                {
                    foreach (var prop in AllCommonProperties)
                    {
                        //Order is important, delete first so that new properties that 
                        //get deleted don't get sent through the system.
                        if (prop.DeleteProp)
                        {
                            element.DeleteUserProperty(prop.ProperName);
                        }
                        else if (prop.IsPreview)
                        {

                            element.AddUserProperty(prop.ProperName, prop.DisplayName, prop.PropertyValue);
                            if (prop.UseAsHoverText == true) hoverChange = true;
                        }
                        else
                        {
                            if (prop.UpdateHoverTextStateForElement(element))//prop.HoverChanged)//
                            {
                                hoverChange = true;
                            }
                            if (prop.ValueChanged)//prop.ApplyPropertyValueChange())//
                            {
                                if (prop.UseAsHoverText == true)
                                {
                                    hoverChange = true;
                                }
                                element.UpdatePropertyValue(prop.ProperName, prop.PropertyValue);
//                                prop.ApplyPropertyValueToElement(element, false);
                            }
                        }
                    }
                }
                else if (ActiveTab.Equals(MultiElementPropertiesDialogTab.MapControl))
                {
                    foreach (var mapControlProp in MapControlProperties)
                    {
                        if (mapControlProp.ValueChanged)
                        {
//                            mapControlProp.ApplyPropertyValueToElement(element, true);
                        }
                    }
                }
                else if (ActiveTab.Equals(MultiElementPropertiesDialogTab.Visuals))
                {

                    if (UsePropertyToControlColor)
                    {
                        if (PropertyForMapping == null) return;
                        double min;
                        double max;
                        if (ManuallySelectThematicMapRange)
                        {
                            if (ThematicMapStartValue >= ThematicMapEndValue) return;
                            min = ThematicMapStartValue;
                            max = ThematicMapEndValue;
                        }
                        else
                        {

                            ColorInterpolator.TryConvertToDouble(PropertyForMapping.MinValue, out min);
                            ColorInterpolator.TryConvertToDouble(PropertyForMapping.MaxValue, out max);
                        }
                        var colorCreator = new ColorInterpolator(new List<object> { min, max },
                                                                    new List<Color> { ThematicMapStartColor, ThematicMapEndColor });
                        if (string.IsNullOrEmpty(element.ImageResourceName))
                        {
                            if (string.IsNullOrEmpty(PropertyForMapping.ProperName))//????? when would this happen
                            {
                                var value = 0;
                                MapColorForValue(element, colorCreator, value);
                            }
                            else
                            {
                                var prop = element.GetDepictionProperty(PropertyForMapping.ProperName);
                                if (prop != null && prop.Value != null)
                                {
                                    MapColorForValue(element, colorCreator, prop.Value);
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (var visProp in VisualProperties)
                        {
                            if (visProp.ValueChanged)
                            {
//                                visProp.ApplyPropertyValueToElement(element, true);
                            }
                        }
                    }
                }

                if (hoverChange)
                {
                    element.TriggerPropertyChange("HoverText");
                }
            }
            #endregion

            //Now apply the changes to this view model
            if (ActiveTab.Equals(MultiElementPropertiesDialogTab.Properties))
            {
                var delProps = new List<MultiElementPropertyViewModel>();
                foreach (var prop in AllCommonProperties)
                {
                    //Order is important, delete first so that new properties that 
                    //get deleted don't get sent through the system.
                    if (prop.DeleteProp)
                    {
                        delProps.Add(prop);
                    }
                    else if (prop.IsPreview)
                    {
                        prop.IsPreview = false;
//                        prop.ApplyPropertyValueChange();
                    }
                    else
                    {
//                        prop.UpdateCurrentHoverText();
                        prop.ApplyViewModelChangeToOrig();
                    }
                }

                foreach (var delProp in delProps)
                {
                    AllCommonProperties.Remove(delProp);
                }
            }
            else if (ActiveTab.Equals(MultiElementPropertiesDialogTab.Visuals))
            {
                if (UsePropertyToControlColor)
                {

                }
                else
                {
                    foreach (var visProp in VisualProperties)
                    {
//                        visProp.ApplyPropertyValueChange();
                    }
                }
            }
            else if (ActiveTab.Equals(MultiElementPropertiesDialogTab.MapControl))
            {
                foreach (var visProp in MapControlProperties)
                {
//                    visProp.ApplyPropertyValueChange();
                }
            }

            _origDisplayInformationText = DisplayInformationText;
            _origUseEnhancedInformationText = UseEnhancedInformationText;
            _origUsePropertyNamesInInformationText = UsePropertyNamesInInformationText;
        }
        #endregion
        #endregion
        #region Constructor/Destructor

        public MultipleElementPropertiesDialogViewModel(List<IElement> elementsToDisplay)
        {
            HideOnClose = false;
            IsModal = false;
            Title = "Multiple Element Property Dialog";
            _allElements = elementsToDisplay;
            _activeTab = MultiElementPropertiesDialogTab.Properties;
            var firstElem = true;
            var baseCommonNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            var extraCommonNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var element in elementsToDisplay)
            {
                //Hacky
                var dElement = element as DepictionElement;
                if (dElement == null) continue;
                //get the non added properties
                if (firstElem)
                {
                    firstElem = false;
                    _origDisplayInformationText = element.DisplayInformationText;
                    _origUseEnhancedInformationText = element.UseEnhancedInformationText;
                    _origUsePropertyNamesInInformationText = element.UsePropertyNamesInInformationText;
                    CreateVisualList(dElement);
                    CreateMapControlList(dElement);
                }
                else
                {
                    if (_origUseEnhancedInformationText != element.UseEnhancedInformationText)
                    {
                        _origUseEnhancedInformationText = null;
                    }
                    if (_origDisplayInformationText != element.DisplayInformationText)
                    {
                        _origDisplayInformationText = null;
                    }
                    if (_origUsePropertyNamesInInformationText != element.UsePropertyNamesInInformationText)
                    {
                        _origUsePropertyNamesInInformationText = null;
                    }
                }
                SetPropertiesInCommon(baseCommonNames, dElement.DefaultProperties);
                SetPropertiesInCommon(extraCommonNames, dElement.AdditionalProperties);
            }
            UseEnhancedInformationText = _origUseEnhancedInformationText;
            DisplayInformationText = _origDisplayInformationText;
            UsePropertyNamesInInformationText = _origUsePropertyNamesInInformationText;

            baseCommonProperties = CreatePropertiesFromLists(baseCommonNames);
            var viewModels = new List<MultiElementPropertyViewModel>();
            var defaultProps = new Dictionary<string, string> { { "DisplayName", "Display Name" }, { "Description", "Description" } };

            foreach (var prop in defaultProps)
            {
                var mProp = CreateMultiDefaultProperty(prop.Key, prop.Value);
                viewModels.Add(mProp);
            }
            baseCommonProperties.InsertRange(0, viewModels);
            extraCommonProperties = CreatePropertiesFromLists(extraCommonNames);
            allCommonProperties = new ObservableCollection<MultiElementPropertyViewModel>(baseCommonProperties.Union(extraCommonProperties).ToList());
            PropertyForMapping = allCommonProperties.FirstOrDefault();

            //set some default color things
            ManuallySelectThematicMapRange = false;
            ThematicMapStartColor = Colors.White;
            ThematicMapStartValue = 0;
            ThematicMapEndColor = Colors.Red;
            ThematicMapEndValue = 100;
        }

        protected override void OnDispose()
        {
            base.OnDispose();
        }
        #endregion

        #region private construction helpers

        private void SetPropertiesInCommon(HashSet<string> names, IEnumerable<IElementProperty> properties)
        {
            if (names.Count == 0)
            {
                names.UnionWith(properties.Select(t => t.ProperName));
            }
            else
            {
                names.IntersectWith(properties.Select(t => t.ProperName));
            }
        }

        private List<MultiElementPropertyViewModel> CreatePropertiesFromLists(HashSet<string> propNames)
        {
            var viewModels = new List<MultiElementPropertyViewModel>();

            var valueList = new List<object>();
            bool? isHoverText = false;
            foreach (var name in propNames)
            {
                valueList.Clear();
                MultiElementPropertyViewModel mProp = null;// new MultiElementPropertyViewModel(name);
                IElementProperty first = null;
                var deletable = false;
                foreach (var element in _allElements)
                {
                    var prop = element.GetDepictionProperty(name);
                    if (prop == null) continue;
                    if (first == null)
                    {
                        first = prop;
                        isHoverText = element.HoverTextProperties.Contains(prop.ProperName);
                    }
                    else
                    {
                        if (isHoverText != element.HoverTextProperties.Contains(prop.ProperName))
                        {
                            isHoverText = null;
                        }
                    }

                    if (element.AdditionalPropertyNames.Contains(new Tuple<string, string>(prop.ProperName, prop.DisplayName)))
                    {
                        deletable = true;
                    }
                    valueList.Add(prop.Value);
                }

                if (first == null) continue;
                mProp = new MultiElementPropertyViewModel(first.ProperName,first.DisplayName, first.Value,isHoverText);
                mProp.MaxValue = valueList.Max();
                mProp.MinValue = valueList.Min();
                mProp.PropertyValue = valueList.First();
                mProp.RequestMultiElementPropertyChange += mProp_RequestMultiElementPropertyChange;//is the used?
                viewModels.Add(mProp);
            }
            return viewModels;
        }

        void mProp_RequestMultiElementPropertyChange(string propName, object value)
        {
            foreach (var element in _allElements)
            {
                var prop = element.GetDepictionProperty(propName);
                if (prop == null) continue;
                if (value is IDeepCloneable)
                {
                    var cloned = ((IDeepCloneable)value).DeepClone();
                    prop.SetValue(cloned);
                }
                else
                {
                    prop.SetValue(value);
                }
            }
        }


        private MultiElementPropertyViewModel CreateMultiDefaultProperty(string propName, string displayName)
        {
            bool? isHoverText = false;
            var first = true;
            object sampleValue = string.Empty;
            foreach (var element in _allElements)
            {
                var hoverText = element.HoverTextProperties.Contains(propName);
                if (first)
                {
                    isHoverText = hoverText;
                    sampleValue = element.GetType().GetProperty(propName).GetValue(element, null);
                    first = false;
                }
                else
                {
                    if (isHoverText != hoverText)
                    {
                        isHoverText = null;
                    }
                }
            }

            return new MultiElementPropertyViewModel(propName, displayName, sampleValue, isHoverText);
        }
        #endregion

//        private void CreateVisualList(IElement sampleElement)
//        {
//            VisualProperties = new List<BaseElementPropertyViewModel>();
//            VisualProperties.Add(new BaseElementPropertyViewModel("ZOIBorderColor", "ZOI Border Color", sampleElement.ZOIBorderColor));
//            VisualProperties.Add(new BaseElementPropertyViewModel("ZOIFillColor", "ZOI Fill Color", sampleElement.ZOIFillColor));
//            VisualProperties.Add(new BaseElementPropertyViewModel("ZOILineThickness", "ZOI Line Size", sampleElement.ZOILineThickness));
//            VisualProperties.Add(new BaseElementPropertyViewModel("ZOIShapeType", "ZOI Shape Type", sampleElement.ZOIShapeType));
//            VisualProperties.Add(new BaseElementPropertyViewModel("IconBorderShape", "Icon Border Shape", sampleElement.IconBorderShape));
//            VisualProperties.Add(new BaseElementPropertyViewModel("IconBorderColor", "Icon Border Color", sampleElement.IconBorderColor));
//            VisualProperties.Add(new BaseElementPropertyViewModel("IconSize", "Icon Size", sampleElement.IconSize));
//            VisualProperties.Add(new BaseElementPropertyViewModel("IconResourceName", "Icon Name", sampleElement.IconResourceName));
//            VisualProperties.Add(new BaseElementPropertyViewModel("VisualState", "Visual State", sampleElement.VisualState));
//        }
//        private void CreateMapControlListList(IElement sampleElement)
//        {
//            MapControlProperties = new List<BaseElementPropertyViewModel>();
//            MapControlProperties.Add(new BaseElementPropertyViewModel("IsDraggable", "Are elements movable", sampleElement.IsDraggable));
//            MapControlProperties.Add(new BaseElementPropertyViewModel("IsGeometryEditable", "Element geometry editable", sampleElement.IsGeometryEditable));
//        }
        private void CreateMapControlList(IElement sampleElement)
        {
            MapControlProperties = new List<MultiElementPropertyViewModel>();
            MapControlProperties.Add(new MultiElementPropertyViewModel("IsDraggable", "Element movable", sampleElement.IsDraggable));
            MapControlProperties.Add(new MultiElementPropertyViewModel("IsGeometryEditable", "Editable geometry", sampleElement.IsGeometryEditable));
        }
        private void CreateVisualList(IElement sampleElement)
        {
            var converter = new BrushConverter();
            VisualProperties = new List<MultiElementPropertyViewModel>();
            VisualProperties.Add(new MultiElementPropertyViewModel("ZOIBorderColor", "ZOI Border Color", converter.ConvertFromString(sampleElement.ZOIBorderColor)));
            VisualProperties.Add(new MultiElementPropertyViewModel("ZOIFillColor", "ZOI Fill Color", converter.ConvertFromString(sampleElement.ZOIFillColor)));
            VisualProperties.Add(new MultiElementPropertyViewModel("ZOILineThickness", "ZOI Line Size", sampleElement.ZOILineThickness));
            var zoiShape = ZOIShapeType.Point;
            Enum.TryParse(sampleElement.ZOIShapeType, true, out zoiShape);
            VisualProperties.Add(new MultiElementPropertyViewModel("ZOIShapeType", "ZOI Shape Type", zoiShape));
            var borderShape = DepictionIconBorderShape.None;
            Enum.TryParse(sampleElement.IconBorderShape, true, out borderShape);
            VisualProperties.Add(new MultiElementPropertyViewModel("IconBorderShape", "Icon Border Shape", borderShape));
            VisualProperties.Add(new MultiElementPropertyViewModel("IconBorderColor", "Icon Border Color", converter.ConvertFromString(sampleElement.IconBorderColor)));
            VisualProperties.Add(new MultiElementPropertyViewModel("IconSize", "Icon Size", sampleElement.IconSize));
            VisualProperties.Add(new MultiElementPropertyViewModel("IconResourceName", "Icon Name",
                StringToDepictionIconConverter.Default.Convert(sampleElement.IconResourceName, typeof(ImageSource), null, Thread.CurrentThread.CurrentCulture)));
            var visualState = ElementVisualSetting.Icon;
            Enum.TryParse(sampleElement.IconBorderShape, true, out visualState);
            VisualProperties.Add(new MultiElementPropertyViewModel("VisualState", "Visual State", visualState));
        }
        private static void MapColorForValue(IElement element, ColorInterpolator map, object value)
        {
            //            Color.FromName(colorString).ToArgb().ToString("X8").Substring(2,6);
            var color = map.GetColorForValue(value);
            var colorHex = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
            switch (element.ElementGeometry.GeometryType)
            {
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    element.ZOIFillColor = colorHex;
                    element.TriggerPropertyChange("ZOIFillColor");

                    break;
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                    element.ZOIBorderColor = colorHex;
                    element.TriggerPropertyChange("ZOIBorderColor");
                    break;
                case DepictionGeometryType.Point:
                    element.IconBorderColor = colorHex;
                    element.TriggerPropertyChange("IconBorderColor");
                    //                    DepictionIconBorderShape border;
                    //                    if (element.GetPropertyValue("IconBorderShape", out border))
                    //                    {
                    //                        if (border.Equals(DepictionIconBorderShape.None))
                    //                        {
                    //                            element.UpdatePropertyValue("IconBorderShape", DepictionIconBorderShape.Square);
                    //                        }
                    //                    }
                    break;

            }
        }
    }
    public enum MultiElementPropertiesDialogTab
    {
        MapControl,
        Properties,
        Visuals
    }
}