﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.Core.ViewModels.DialogViewModels
{
    public class SingleElementPropertiesDialogVm : DialogViewModel
    {
        private IElement _elementModel;
        private ElementPropertiesDialogTab _activeTab;

        #region properties

        public ElementPropertiesDialogTab ActiveTab
        {
            get { return _activeTab; }
            set { _activeTab = value; OnPropertyChanged(() => ActiveTab); }
        }
        public string IconPath { get { return string.IsNullOrEmpty(_elementModel.ImageResourceName) ? _elementModel.IconResourceName : _elementModel.ImageResourceName; } }
        public EditElementViewModel ElementViewModel { get; set; }
        public List<PropertyTemplateViewModel> InformationProperties { get { return ElementViewModel.InformationProperties; } }
        public List<PropertyTemplateViewModel> VisualProperties { get { return ElementViewModel.VisualProperties; } }
        public List<PropertyTemplateViewModel> MapControlProperties { get { return ElementViewModel.MapControlProperties; } }
        public List<PropertyTemplateViewModel> DefaultProperties { get { return ElementViewModel.DefaultProperties; } }
        public ObservableCollection<PropertyTemplateViewModel> UserProperties { get { return ElementViewModel.UserProperties; } }
        public bool UsePropertyNamesInInformationText { get; set; }
        public bool DisplayInformationText { get; set; }
        public bool UseEnhancedInformationText { get; set; }
        //for property creation
        public Type SelectedType { get; set; }
        public string NewPropName { get; set; }
        public Dictionary<string, Type> PropertyTypes
        {
            get
            {
                return DepictionAccess.NameTypeService.NameTypeDictonary;
            }
        }
        #endregion

        #region button commands

        private DelegateCommand addPropertyCommand;
        public ICommand AddPropertyCommand
        {
            get
            {
                if (addPropertyCommand == null)
                {
                    addPropertyCommand = new DelegateCommand(AddPropertyToElement);
                }
                return addPropertyCommand;
            }
        }

        public void AddPropertyToElement()
        {
            if (string.IsNullOrEmpty(NewPropName)) return;
            var prop = new ElementProperty(NewPropName, SelectedType);
            _elementModel.AddUserProperty(prop);
        }

        private DelegateCommand acceptChangesCommand;
        public ICommand AcceptChangesCommand
        {
            get
            {
                if (acceptChangesCommand == null)
                {
                    acceptChangesCommand = new DelegateCommand(AcceptChangesToElement);
                }
                return acceptChangesCommand;
            }
        }
        protected void AcceptChangesToElement()
        {
            ApplyPropertiesChangesToElement();
            RequestDialogClose();
        }
        private DelegateCommand applyChangesCommand;
        public ICommand ApplyChangesCommand
        {
            get
            {
                if (applyChangesCommand == null)
                {
                    applyChangesCommand = new DelegateCommand(ApplyPropertiesChangesToElement);
                }
                return applyChangesCommand;
            }
        }

        protected void ApplyPropertiesChangesToElement()
        {
            if (_elementModel == null) return;
            switch (ActiveTab)
            {
                case ElementPropertiesDialogTab.InformationProperties:
                    ElementViewModel.ApplyInformationChanges();
                    break;

                case ElementPropertiesDialogTab.ZOIProperties:
                    ElementViewModel.ApplyVisualPropertyChanges();
                    break;

                case ElementPropertiesDialogTab.MapControlProperties:
                    ElementViewModel.ApplyMapControlPropertyChanges();
                    break;

                case ElementPropertiesDialogTab.DefaultProperties:
                    ElementViewModel.ApplyDefaultPropertyChanges();
                    break;
                case ElementPropertiesDialogTab.UserProperties:

                    ElementViewModel.ApplyUserPropertyChanges();
                    break;
            }
            if(!DisplayInformationText.Equals(_elementModel.DisplayInformationText))
            {
                _elementModel.DisplayInformationText = DisplayInformationText;
            }
            if (!UseEnhancedInformationText.Equals(_elementModel.UseEnhancedInformationText))
            {
                _elementModel.UseEnhancedInformationText = UseEnhancedInformationText;
            }
            if (!UsePropertyNamesInInformationText.Equals(_elementModel.UsePropertyNamesInInformationText))
            {
                _elementModel.UsePropertyNamesInInformationText = UsePropertyNamesInInformationText;
            }
        }

        #endregion

        #region Constructor/Destructor

        public SingleElementPropertiesDialogVm(IElement elementToEdit)
            : base(elementToEdit.ElementId)
        {
            _elementModel = elementToEdit;
            HideOnClose = false;
            IsModal = false;
            Title = _elementModel.ElementType;
            SelectedType = typeof(string);
            ElementViewModel = new EditElementViewModel(elementToEdit);
            if(ElementViewModel == null)
            {
                throw new NullReferenceException("Cannot have a null element viewmodel in dialog.");
            }
            DisplayInformationText = elementToEdit.DisplayInformationText;
            UseEnhancedInformationText = elementToEdit.UseEnhancedInformationText;
            UsePropertyNamesInInformationText = elementToEdit.UsePropertyNamesInInformationText;
        }

       

        protected override void OnDispose()
        {
            base.OnDispose();
        }
        #endregion


    }

    public enum ElementPropertiesDialogTab
    {
        InformationProperties,
        DefaultProperties,
        UserProperties,
        MapControlProperties,
        ZOIProperties,
        GroupProperties,
        Hovertext,
        Tags
    }
}