﻿using System.Windows;
using System.Windows.Input;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels.App
{
    public class MessageBoxDialogViewModel : DialogViewModel
    {
        #region variables

        private MessageBoxResult result = MessageBoxResult.None;
        #endregion

        #region properties
        //        public Style DepictionMessageBoxStyle
        //        {
        //            get
        //            {
        //                Style frameStyle = null;
        //                string styleName = "DepictionMessageBoxStyle";
        //                if (Application.Current != null && Application.Current.Resources != null)
        //                {
        //                    if (Application.Current.Resources.Contains(styleName))
        //                    {
        //                        frameStyle = Application.Current.Resources[styleName] as Style;
        //                    }
        //                }
        //                return frameStyle;
        //            }
        //        }
        public MessageBoxResult MessageDialogBoxResult
        {
            get { return result; }
            private set
            {
                result = value;
                OnPropertyChanged("MessageDialogBoxResult");
                //                if (!isModal) return;
                //                if (MessageBoxResult.Cancel == result)
                //                {
                //                    DialogResult = false;
                //                }
                //                else
                //                {
                //                    DialogResult = true;
                //                }
            }
        }

        public MessageBoxButton MessageBoxType { get; set; }
        public string Message { get; set; }
        #endregion
        #region Commands

        public ICommand ButtonCommand
        {
            get
            {
                var buttonCommand = new DelegateCommand<string>(ButtonAction);//, CanExecuteEndingButton);
                return buttonCommand;
            }
        }

        private void ButtonAction(string pressedButton)
        {
            var buttonAction = pressedButton.ToLowerInvariant();

            switch (buttonAction)
            {
                case "ok":
                    MessageDialogBoxResult = MessageBoxResult.OK;
                    break;
                case "no":
                    MessageDialogBoxResult = MessageBoxResult.No;
                    break;
                case "yes":
                    MessageDialogBoxResult = MessageBoxResult.Yes;
                    break;
                case "cancel":
                    MessageDialogBoxResult = MessageBoxResult.Cancel;
                    break;
            }
        }

        #endregion

        public MessageBoxDialogViewModel()
        {
            IsModal = true;
            IsHidden = false;
            MessageBoxType = MessageBoxButton.OK;
//            MessageDialogBoxResult = MessageBoxResult.Cancel;
        }
    }
}