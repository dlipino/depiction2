﻿using System.Windows.Documents;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels.App
{
    public class EulaDialogVM : DialogViewModel
    {
        private FixedDocumentSequence aboutDocument;

        public FixedDocumentSequence EulaDocument
        {
            get { return aboutDocument; }
            set
            {
                aboutDocument = value;
                OnPropertyChanged("EulaDocument");
            }
        }

        public EulaDialogVM()
        {
//            var eulafileName = DepictionAccess.ProductInformation.EulaFileAssemblyLocation;
//            var assembly = DepictionAccess.ProductInformation.ResourceAssemblyName;
//
//            var xpsDoc = AboutDialogVM.LoadDocFromAssemblyResource(assembly, eulafileName);
//            if (xpsDoc == null) { EulaDocument = new FixedDocumentSequence(); }
//            else
//            {
//                EulaDocument = xpsDoc.GetFixedDocumentSequence();
//            }
//            DialogDisplayName = "EULA";
        }
    }
}