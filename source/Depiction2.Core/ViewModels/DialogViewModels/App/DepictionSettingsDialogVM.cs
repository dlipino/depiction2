﻿

using System.Collections.Generic;
using System.Net;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Properties;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.Measurement;

namespace Depiction2.Core.ViewModels.DialogViewModels.App
{
    public class DepictionSettingsDialogVM : DialogViewModel
    {
        #region Properties

        public Dictionary<MeasurementSystem, string> MeasurementSystemsList { get; set; }
        public Dictionary<MeasurementScale, string> MeasurementScalesList { get; set; }
        public Dictionary<LatitudeLongitudeFormat, string> LatitudeLongitudeFormatList { get; set; }
        public LatitudeLongitudeFormat SelectedLatitudeLongitudeFormat { get; set; }

        public IList<int> PrecisionList { get; set; }

        public int SelectedPrecision { get; set; }
        public MeasurementScale SelectedScale { get; set; }
        public MeasurementSystem SelectedSystem { get; set; }
        public LatitudeLongitudeFormat SelectedLatLongFormat { get; set; }
        public bool OverwriteCache { get; set; }
        public bool DisplayWorldOutline { get; set; }

        public string ProxyUserName { get; set; }
        public string ProxyPassword { get; set; }

        public string UserDirectory
        {
            get
            {
                if (DepictionAccess.PathService != null)
                {
                    return DepictionAccess.PathService.AppDataDirectoryPath;
                }
                return string.Empty;
            }
        }

        #endregion

        #region constructor
        public DepictionSettingsDialogVM()
        {
            IsModal = false;
            HideOnClose = true;
            SetLists();
            SelectedLatLongFormat = Settings.Default.LatitudeLongitudeFormat;
            SelectedScale = Settings.Default.MeasurementScale;
            SelectedSystem = Settings.Default.MeasurementSystem;
            SelectedPrecision = Settings.Default.Precision;
//            OverwriteCache = Settings.Default.ReplaceCachedFiles;
            ProxyPassword = Settings.Default.ProxyPassword;
            ProxyUserName = Settings.Default.ProxyUserName;
        }

        #endregion

        #region private helpers

        private void SetLists()
        {
            MeasurementSystemsList = new Dictionary<MeasurementSystem, string>
                                         {
                                             {MeasurementSystem.Imperial, "Imperial"},
                                             {MeasurementSystem.Metric, "Metric"}
                                         };
            MeasurementScalesList = new Dictionary<MeasurementScale, string>
                                         {
                                             {MeasurementScale.Small, "Small"},
                                             {MeasurementScale.Normal, "Normal"},
                                             {MeasurementScale.Large, "Large"},
                                         };
            LatitudeLongitudeFormatList = new Dictionary<LatitudeLongitudeFormat, string>
                                              {
                                                  {LatitudeLongitudeFormat.SignedDecimal, "40.44620, -79.94886"},
                                                  {LatitudeLongitudeFormat.Decimal, "40.44620N, 79.94886W"},
                                                  
                                                  {LatitudeLongitudeFormat.SignedDegreesFractionalMinutes, "40° 26.77170, -79° 56.93172"},
                                                   {LatitudeLongitudeFormat.DegreesFractionalMinutes, "40° 26.77170N, 79° 56.93172W"},
                                                  {LatitudeLongitudeFormat.DegreesMinutesSeconds, "40°26'46\"N, 79°56'55\"W"},
                                                  {LatitudeLongitudeFormat.ColonFractionalSeconds, "40:26:46.302N, 79:56:55.903W"},
                                                  {LatitudeLongitudeFormat.ColonIntegerSeconds, "40:26:46N, 79:56:55W"},
                                                  {LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds, "40d26'46\"N, 79d56'55\" W"},
                                                   {LatitudeLongitudeFormat.UTM,"17T 630084 4833438"},
                                              };
            PrecisionList = new List<int> { 0, 1, 2, 3, 4, 5 };
        }
        #endregion

        #region Command

        public ICommand ApplyOptionsChangesCommand
        {
            get
            {
                var applyOptionsChangesCommand = new DelegateCommand(ApplyOptions, OptionsHaveChanged);
                return applyOptionsChangesCommand;
            }
        }
        public ICommand AcceptOptionsChangesCommand
        {
            get
            {
                var applyOptionsChangesCommand = new DelegateCommand(AcceptOptions);
                return applyOptionsChangesCommand;
            }
        }

        private bool OptionsHaveChanged()
        {
            if (!Settings.Default.LatitudeLongitudeFormat.Equals(SelectedLatLongFormat)) return true;
            if (!Settings.Default.MeasurementScale.Equals(SelectedScale)) return true;
            if (!Settings.Default.MeasurementSystem.Equals(SelectedSystem)) return true;
            if (!Settings.Default.Precision.Equals(SelectedPrecision)) return true;
            if (!Settings.Default.ProxyUserName.Equals(ProxyUserName)) return true;
            if (!Settings.Default.ProxyPassword.Equals(ProxyPassword)) return true;
            return false;
        }

        private void ApplyOptions()
        {
            Settings.Default.Precision = SelectedPrecision;//This has to be before selectedScale because
            //there is no event change assocatiated with this.
            Settings.Default.LatitudeLongitudeFormat = SelectedLatLongFormat;
            Settings.Default.MeasurementScale = SelectedScale;
            Settings.Default.MeasurementSystem = SelectedSystem;

            //A different way, the view is using direct access to the settings properties
            ProxyUserName = ProxyUserName ?? string.Empty;
            ProxyPassword = ProxyPassword ?? string.Empty;

            Settings.Default.ProxyUserName = ProxyUserName;
            Settings.Default.ProxyPassword = ProxyPassword;
            if (WebRequest.DefaultWebProxy != null)
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(ProxyUserName, ProxyPassword);
            Settings.Default.Save();
        }

        private void AcceptOptions()
        {
            if (OptionsHaveChanged()) ApplyOptions();
            RequestDialogClose();
        }

        #endregion
    }
}