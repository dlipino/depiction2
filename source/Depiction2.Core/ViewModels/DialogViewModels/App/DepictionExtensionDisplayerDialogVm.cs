﻿using System.Collections.Generic;
using System.Windows.Input;
using Depiction2.Base.Helpers;
using Depiction2.Core.ViewModels.ExtensionViewModels;

namespace Depiction2.Core.ViewModels.DialogViewModels.App
{
    public class DepictionExtensionDisplayerDialogVm : DialogViewModel
    {
//        private DepictionExtensionLibrary extensionLibrary;
        #region properties

        public IEnumerable<MefExtensionViewModel> ExtensionViewModels { get; private set; }
        public string ExtensionSearchPath
        {
            get
            {
                return string.Empty;// return extensionLibrary == null ? "No search path." : extensionLibrary.BaseExtensionDir;
            }
        }

        #endregion
        #region constructor

        public DepictionExtensionDisplayerDialogVm()
        {
            IsModal = false;
            HideOnClose = true;
//            extensionLibrary = DepictionAccess.ExtensionLibrary;
            var extensionViewModelList= new List<MefExtensionViewModel>(); 
//            if (extensionLibrary != null)
//            {
//                foreach (var depictionExtensionMetadataBase in extensionLibrary.GetAllMetadata())
//                {
//                    extensionViewModelList.Add(new MefExtensionViewModel(depictionExtensionMetadataBase));
//                }
//            }
            SetExtensions(extensionViewModelList);

        }
        #endregion
        #region commands

        private DelegateCommand refreshExtensionsCommand;
        public ICommand RefreshExtensionsCommand
        {
            get
            {
                if (refreshExtensionsCommand == null)
                {
                    refreshExtensionsCommand = new DelegateCommand(RefreshExtensions,CanRefresh);
                }
                return refreshExtensionsCommand;
            }
        }

        private bool CanRefresh()
        {
            return false;
//            if (extensionLibrary == null) return false;
//            return true;
        }

        private void RefreshExtensions()
        {
//            extensionLibrary = DepictionAccess.ExtensionLibrary;
//            if (extensionLibrary == null) return;
//            extensionLibrary.Decompose(); 
//            var extensionViewModelList = new List<MefExtensionViewModel>();
//            if (extensionLibrary != null)
//            {
//                foreach (var depictionExtensionMetadataBase in extensionLibrary.GetAllMetadata())
//                {
//                    extensionViewModelList.Add(new MefExtensionViewModel(depictionExtensionMetadataBase));
//                }
//            }
//            SetExtensions(extensionViewModelList);
//            extensionLibrary.Compose();
        }

        #endregion

        #region private helpers
        private void SetExtensions(IEnumerable<MefExtensionViewModel> extensionMetadata)
        {
            ExtensionViewModels = extensionMetadata;
            OnPropertyChanged("ExtensionViewModels");
        }
        #endregion
    }
}