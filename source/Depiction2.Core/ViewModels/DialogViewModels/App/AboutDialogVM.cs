﻿using System;
using System.IO;
using System.IO.Packaging;
using System.Reflection;
using System.Windows.Documents;
using System.Windows.Xps.Packaging;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.ViewModels.DialogViewModels.App
{
    public class AboutDialogVM : DialogViewModel
    {
        private FixedDocumentSequence aboutDocument;

        public FixedDocumentSequence AboutDocument
        {
            get { return aboutDocument; }
            set { aboutDocument = value;
                OnPropertyChanged("AboutDocument"); }
        }

        public AboutDialogVM()
        {
//            var aboutfileName = DepictionAccess.ProductInformation.AboutFileAssemblyLocation;
//            var assembly = DepictionAccess.ProductInformation.ResourceAssemblyName;
//
//            var xpsDoc = LoadDocFromAssemblyResource(assembly, aboutfileName);
//            if (xpsDoc == null) { AboutDocument = new FixedDocumentSequence(); }
//            else
//            {
//                AboutDocument = xpsDoc.GetFixedDocumentSequence();
//            }
//
////            var xpsDoc = LoadDocFromProductResourcesAssembly("About.xps");
////            if(xpsDoc == null)
////            {
////                AboutDocument = new FixedDocumentSequence();
////            }else
////            {
////                AboutDocument = xpsDoc.GetFixedDocumentSequence();
////            }
//            DialogDisplayName = "About";
        }
//        public static XpsDocument LoadDocFromProductResourcesAssembly(string filename)
//        {
//            return LoadDocFromAssemblyResource(DepictionAccess.ProductInformation.ResourceAssemblyName, filename);
//        }
        public static XpsDocument LoadDocFromAssemblyResource(string assemblyName,string filename)
        {
            if(string.IsNullOrEmpty(assemblyName))
            {//TODO find some nice defaults, maybe. or just say no good data no output
                assemblyName = "Resources.Product.Depiction";
            }
            // Getting a Stream out of the Resource file, strDocument
            var fullName = string.Format("{0}.{1}", assemblyName,filename);
            var itemAssembly = Assembly.Load(new AssemblyName(assemblyName));
            Stream stream = itemAssembly.GetManifestResourceStream(fullName);
            if (stream == null) return null;
            // Getting the length of the Stream we just obtained
            int length = (int)stream.Length;

            // Setting up a new MemoryStream and Byte Array
            MemoryStream ms = new MemoryStream();
            ms.Capacity = length;
            byte[] buffer = new byte[length];

            // Copying the Stream to the Byte Array (Buffer)
            stream.Read(buffer, 0, length);

            // Copying the Byte Array (Buffer) to the MemoryStream
            ms.Write(buffer, 0, length);

            // Setting up a new Package based on the MemoryStream
            Package pkg = Package.Open(ms);

            // Putting together a Uri for the Package using the document name (strDocument)
            string strMemoryPackageName = string.Format("memorystream://{0}", filename);

            Uri packageUri = new Uri(strMemoryPackageName);

            // Adding the Package to PackageStore using the Uri
            if (PackageStore.GetPackage(packageUri) == null)
                PackageStore.AddPackage(packageUri, pkg);

            // Finally, putting together the XpsDocument
            return new XpsDocument(pkg, CompressionOption.Maximum, strMemoryPackageName);
        }
    }
}