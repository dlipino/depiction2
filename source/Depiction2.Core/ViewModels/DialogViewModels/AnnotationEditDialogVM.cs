﻿using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.DialogViewModels
{
    public class AnnotationEditDialogVM : DialogViewModel
    {
        private IDepictionAnnotation _annotationModel;
        private string _annotText = string.Empty;
        #region properties

        public string AnnotationText { get { return _annotText; } set { _annotText = value; } }
        public string AnnotationTextColor { get; set; }
        public string AnnotationBackgroundColor { get; set; }

        #endregion
        #region Constructor

        public AnnotationEditDialogVM(IDepictionAnnotation annotationToEdit)
        {
            _annotationModel = annotationToEdit;
            _id = _annotationModel.AnnotationID;
            _annotText = _annotationModel.AnnotationText;
            AnnotationBackgroundColor = annotationToEdit.BackgroundColor;
            AnnotationTextColor = annotationToEdit.ForegroundColor;
            HideOnClose = false;
        }

        #endregion

        #region commands

        private DelegateCommand deleteAnnotation;
        public ICommand DeleteAnnotationCommand
        {
            get
            {
                if (deleteAnnotation == null)
                {
                    deleteAnnotation = new DelegateCommand(DeleteAnnotation);
                    deleteAnnotation.Text = "Remove Annotation";
                }
                return deleteAnnotation;
            }
        }

        protected void DeleteAnnotation()
        {
            if (DepictionAccess.DStory == null) return;
            DepictionAccess.DStory.DeleteAnnotation(_annotationModel);
            DialogCloseResult = true;
            RequestDialogClose();
        }

        private DelegateCommand applyChanges;
        public ICommand ApplyChangesCommand
        {
            get
            {
                if(applyChanges == null)
                {
                    applyChanges = new DelegateCommand(ApplyChanges);
                }
                return applyChanges;
            }
        }
        protected void ApplyChanges()
        {
            _annotationModel.ForegroundColor = AnnotationTextColor;
            _annotationModel.BackgroundColor = AnnotationBackgroundColor;
            _annotationModel.AnnotationText = AnnotationText;
            DialogCloseResult = true;
            RequestDialogClose();

        }

        #endregion
    }
}