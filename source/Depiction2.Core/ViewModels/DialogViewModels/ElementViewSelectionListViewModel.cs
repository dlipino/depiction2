﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;

namespace Depiction2.Core.ViewModels.DialogViewModels
{
    public class ElementViewSelectionListViewModel : ElementSelectListViewModel
    {
        public IElementDisplayer DisplayerToControl { get; private set; }
        private bool disableConnection = false;
        #region constructor/destructor

        public ElementViewSelectionListViewModel(IEnumerable<IElement> allElements, IElementDisplayer displayerToControl)
            : base(allElements)
        {
            SetDisplayerToControl(displayerToControl);
        }

        protected override void OnDispose()
        {
            if (DisplayerToControl != null)
            {
                DisplayerToControl.DisplayedElementsChangedIdList -= DisplayerToControl_DisplayedElementsChangedIdList;
            }
            DisplayerToControl = null;
            base.OnDispose();
        }
        #endregion
        #region helper methods
        public void SetDisplayerToControl(IElementDisplayer displayer)
        {
            //when changing the displayer make sure that modifying IsSelected due to visual change does not change the actual displayer
            disableConnection = true;
            if (originalElementList != null)
            {
                foreach (var element in originalElementList)
                {
                    element.IsSelected = false;
                }
            }
            if (DisplayerToControl != null)
            {
                DisplayerToControl.DisplayedElementsChangedIdList -= DisplayerToControl_DisplayedElementsChangedIdList;
            }
            DisplayerToControl = displayer;

            if (displayer != null)
            {
                DisplayerToControl.DisplayedElementsChangedIdList += DisplayerToControl_DisplayedElementsChangedIdList;
                var matching = originalElementList.ToDictionary(t => t.ElementId);
                foreach (var key in displayer.ElementIds)
                {
                    if (matching.ContainsKey(key))
                    {
                        matching[key].IsSelected = true;
                    }
                }
            }
            OnPropertyChanged(() => DisplayerToControl);
            disableConnection = false;
        }
        #endregion
        #region protected list element event handlers

        override protected void ListElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsSelected", StringComparison.InvariantCultureIgnoreCase) && !disableConnection)
            {
                var levm = sender as ListElementViewModel;
                if (levm != null)
                {
                    if (levm.IsSelected)
                    {
                        DisplayerToControl.AddDisplayElementsWithMatchingIds(new[] { levm.ElementId });
                    }
                    else
                    {
                        DisplayerToControl.RemoveElementsWithMatchingIds(new[] { levm.ElementId });
                    }
                }
            }
        }
        void DisplayerToControl_DisplayedElementsChangedIdList(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Add) && DisplayerToControl != null)
            {
                foreach (var item in e.NewItems)
                {
                    if (item is string)
                    {
                        var key = (string)item;
                        if (originalElementDictionary.ContainsKey(key))
                        {
                            originalElementDictionary[key].IsSelected = true;
                        }
                    }
                }
            }
        }
        #endregion




        //        void DisplayerToControl_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //        {
        //            if (e.Action.Equals(NotifyCollectionChangedAction.Add) && DisplayerToControl != null)
        //            {
        //                foreach (var item in e.NewItems)
        //                {
        //                    if (item is MapElementViewModel)
        //                    {
        //                        var key = ((MapElementViewModel)item).ElementKey;
        //                        if (originalElementDictionary.ContainsKey(key))
        //                        {
        //                            originalElementDictionary[key].IsSelected = true;
        //                        }
        //                    }
        //                }
        //            }
        //        }
    }
}