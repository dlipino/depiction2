﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.DialogViewModels.Story
{
    public class StoryInformationDialogViewModel : DialogViewModel
    {
        #region properties

        public string StoryTitle { get;  set; }
        public string Author { get;  set; }
        public string Description { get;  set; }
        public string ProjSystemInfo { get; private set; }
        #endregion

        #region
        public StoryInformationDialogViewModel(IStory story)
        {
            HideOnClose = true;
            IsModal = false;
            ProjSystemInfo = string.Empty;
            var details = story.StoryDetails;
            StoryTitle = details.Title;
            Author = details.Author;
            Description = details.Description;
            UpdateViewModel(story);
        }
        #endregion

        #region private helpers
        public void UpdateViewModel(IStory newStory)
        {
            ProjSystemInfo = newStory.DisplayCoordinateSystem;
            OnPropertyChanged(() => ProjSystemInfo);
        }
        #endregion


    }
}