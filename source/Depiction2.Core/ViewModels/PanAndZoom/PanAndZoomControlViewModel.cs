﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.PanAndZoom
{
    public class PanAndZoomControlViewModel : ViewModelBase
    {
        #region variables

        private IStory _mainStory;

        #endregion

        #region properties
        
        public string LocationInfo
        {
            get { return (string)GetValue(LocationInfoProperty); }
            set { SetValue(LocationInfoProperty, value); }
        }
        
        public static readonly DependencyProperty LocationInfoProperty =
            DependencyProperty.Register("LocationInfo", typeof(string), typeof(PanAndZoomControlViewModel), new UIPropertyMetadata(String.Empty));

        public string ProjectedLocation
        {
            get { return (string)GetValue(ProjectedLocationProperty); }
            set { SetValue(ProjectedLocationProperty, value); }
        }

        public static readonly DependencyProperty ProjectedLocationProperty =
            DependencyProperty.Register("ProjectedLocation", typeof(string), typeof(PanAndZoomControlViewModel), new UIPropertyMetadata(String.Empty));
        #endregion
        #region commands

        private DelegateCommand<ZoomDirection> zoomCommand;

        public ICommand ZoomCommand
        {
            get
            {
                if (zoomCommand == null)
                {
                    zoomCommand = new DelegateCommand<ZoomDirection>(Zoom);
                }
                return zoomCommand;
            }
        }

        private DelegateCommand<PanDirection> panCommand;

        public ICommand PanCommand
        {
            get
            {
                if (panCommand == null)
                {
                    panCommand = new DelegateCommand<PanDirection>(Pan);
                }
                return panCommand;
            }
        }

        private DelegateCommand centerOnRegionCommand;

        public ICommand CenterOnRegionCommand
        {
            get
            {
                if (centerOnRegionCommand == null)
                {
                    centerOnRegionCommand = new DelegateCommand(CenterOnStoryRegion);
                }
                return centerOnRegionCommand;
            }
        }

        #endregion
        #region Constructor/Destructor

        public PanAndZoomControlViewModel(IStory mainStory)
        {
            _mainStory = mainStory;
            Settings.Default.PropertyChanged += Default_PropertyChanged;
            _mainStory.PropertyChanged += _mainStory_PropertyChanged;
        }

        void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
          if(e.PropertyName.Equals("MeasurementSystem"))
          {
              OnPropertyChanged("ScaleChange");
          }
        }

        void _mainStory_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StoryZoom"))
            {
                OnPropertyChanged("ScaleChange");
            }
        }

        protected override void OnDispose()
        {
            Settings.Default.PropertyChanged -= Default_PropertyChanged;
            _mainStory.PropertyChanged -= _mainStory_PropertyChanged;
            base.OnDispose();
        }
        #endregion

        #region methods

        protected void Zoom(ZoomDirection zoomDirection)
        {
            _mainStory.Zoom(zoomDirection, null);
        }

        protected void Pan(PanDirection panDirection)
        {
            _mainStory.ShiftStoryExtent(panDirection);
        }

        protected void CenterOnStoryRegion()
        {
            var extent = _mainStory.ElementExtent;
            if (!extent.IsEmpty)
            {
                var center = extent.Center;
                _mainStory.SetCenter(center);
            }
        }
        #endregion

        public void DisplayStoryInformationForProjLocation(double y, double x)
        {
            ProjectedLocation = string.Format("{0:0.0000} , {1:0.0000}", x, y);
            var geoLocation = CoordinateSystemService.Instance.PointDisplayToStory(new Point(x, y));
            LocationInfo = _mainStory.InformationForGeographicLocation(geoLocation.Y, geoLocation.X);
        }
    }
}