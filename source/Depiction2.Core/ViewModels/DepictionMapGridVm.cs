﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction.ProjectionSystem;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ExtensionFramework.Tiling;
using Depiction2.Core.Service;
using Depiction2.Core.Tiling;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.ElementViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Core.ViewModels.PanAndZoom;
using Depiction2.Core.ViewModels.RevealerViewModels;
using Depiction2.Core.ViewModels.Tiling;
using Depiction2.Utilities.MVVM;

namespace Depiction2.Core.ViewModels
{
    public class DepictionMapGridVm : ViewModelBase
    {
        #region variables
        private IStory _storyModel = null;
        private bool isStoryAttached = false;
        private DepictionMapMode _mapMode = DepictionMapMode.Normal;

        private IDialogService _dialogService = null;
//        private TopMenuViewModel _storyDialogMenuViewModel;
        private double gcsToProjectionScaleModifier = 1;

        private Point pixelTopLeft;
        private double pixelScale = 1;
        #endregion

        #region properties that connect to map

//        public TopMenuViewModel StoryDialogMenuViewModel
//        {
//            get { return _storyDialogMenuViewModel; }
//        }
//        
//        public TileDisplayerViewModel TileDisplayerVM { get; set; }
//        public PanAndZoomControlViewModel PanZoomVm { get; set; }
        public DepictionMapMode MapMode { get { return _mapMode; } set { _mapMode = value; OnPropertyChanged(() => MapMode); } }

        public double StoryZoom
        {
            get { return _storyModel.StoryZoom / gcsToProjectionScaleModifier; }
        }
        //This is actually the topleft point
        public Point VirtualPanelOffset
        {
            get
            {
                var wpfPanelLoc = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(_storyModel.StoryTopLeft, _storyModel.StoryProjector);
                //now find the location given the current pixel scale value
                var tScale = _storyModel.StoryZoom / gcsToProjectionScaleModifier;
                var vpanelOffset = tScale * ((Vector)(wpfPanelLoc));
                return (Point)vpanelOffset;
            }
        }

        #endregion

        #region command properties, these are legacy

//        public ObservableCollection<ICommand> MapLocationCommands { get; set; }
//        public ObservableCollection<ICommand> GenericElementCommands { get; set; }
//        public ObservableCollection<ICommand> GenericAnnotationCommands { get; set; }

        #endregion

        #region Dependency properties

        public Point PanelLocation
        {
            get { return (Point)GetValue(PanelLocationProperty); }
            set { SetValue(PanelLocationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PanelLocation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PanelLocationProperty =
            DependencyProperty.Register("PanelLocation", typeof(Point), typeof(DepictionMapGridVm), new UIPropertyMetadata(new Point()));

        public Point VirtualLocation
        {
            get { return (Point)GetValue(VirtualLocationProperty); }
            set { SetValue(VirtualLocationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for VirtualLocation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VirtualLocationProperty =
            DependencyProperty.Register("VirtualLocation", typeof(Point), typeof(DepictionMapGridVm), new UIPropertyMetadata(new Point()));

        #endregion

        #region Constructor/destructor

        public DepictionMapGridVm(IStory story, IDialogService dialogService)
        {
            AttachStory(story);
//            _storyDialogMenuViewModel = new TopMenuViewModel(story, dialogService);
//            _storyDialogMenuViewModel.PropertyChanged += _storyDialogMenuViewModel_PropertyChanged;
            _dialogService = dialogService;
            MapLocationCommands = new ObservableCollection<ICommand>();
            MapLocationCommands.Add(CopyRightClickToClipboardCommand);
            MapLocationCommands.Add(AddNewAnnotationCommand);

            GenericAnnotationCommands = new ObservableCollection<ICommand>();
            GenericAnnotationCommands.Add(ShowAnnotationPropertiesDialogCommand);
            GenericAnnotationCommands.Add(RemoveAnnotationCommand);

            GenericElementCommands = new ObservableCollection<ICommand>();
            GenericElementCommands.Add(ShowElementPropertiesDialogCommand);
            GenericElementCommands.Add(DeleteElementCommand);
        }

        void _storyDialogMenuViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("MouseAdd"))
           {
               MapMode = DepictionMapMode.ElementAdd;
           }
        }

        //Destroy the ViewModels, make sure to detach all view models from the models
        protected override void OnDispose()
        {
            DettachStory();
            base.OnDispose();
        }
        #endregion

        #region construction/destruction helpers

        public void AttachStory(IStory story)
        {
            DettachStory();
            _storyModel = story;
            //Create the viewmodel stuff
         



            //Tiler
//            TileDisplayerVM = new TileDisplayerViewModel(this, _storyModel);
//            TileDisplayerVM.AddToAvailableTilers(new[] { new OpenStreetMapTiler() });
//            TileDisplayerVM.TileProviderType = TileImageTypes.Street;
//            TileDisplayerVM.Tiler = TileDisplayerVM.SelectableTileProviders[0];

            //Pan zoom control
//            PanZoomVm = new PanAndZoomControlViewModel(story);

            //end vm stuff, connect things to the story
            _storyModel.PropertyChanged += story_PropertyChanged;
            isStoryAttached = true;
        }

        public void DettachStory()
        {
            if (isStoryAttached)
            {
                _storyModel.PropertyChanged -= story_PropertyChanged;
//                _storyModel.ElementCollectionChanged -= AllElements_CollectionChanged;
//                _storyModel.RevealerCollectionChanged -= RevealerDisplayers_CollectionChanged;
                isStoryAttached = false;
                _storyModel = null;
            }
        }
        #endregion

        #region story and element property changes
        void story_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Projection"))
            {
                var story = sender as IStory;
                if (story != null)
                {
//                    UpdateViewModelGeometriesWithProjection(story.StoryProjector);
                }
            }
            else if (e.PropertyName.Equals("TopRevealerId"))
            {
//                var newTopRevealerId = _storyModel.TopRevealerId;
//                var newTopRevealer = revealersVM.FirstOrDefault(t => t.RevealerId.Equals(newTopRevealerId));
//                if (newTopRevealer == null) return;
//                if (TopRevealer == null)
//                {
//                    TopRevealer = newTopRevealer;
//                    newTopRevealer.SetTopRevealerAttributes();
//                }
//                else
//                {
//                    if (!TopRevealer.RevealerId.Equals(newTopRevealerId))
//                    {
//                        //Change current top revealer display attributes
//                        TopRevealer.SetToNormalRevealerAttributes();
//                        //Now set a new top revealer;
//                        TopRevealer = newTopRevealer;
//                        newTopRevealer.SetTopRevealerAttributes();
//                    }
//                }
            }
            else if (e.PropertyName.Equals("StoryTopLeft"))
            {
                OnPropertyChanged(() => VirtualPanelOffset);
            }
            else if (e.PropertyName.Equals("StoryZoom"))
            {
                OnPropertyChanged(() => StoryZoom);
            }
            else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }

        #endregion

        #region collection changes
        

        #endregion

        #region story pass throughs for adding removing etc

        public Size ViewPaneSize { get; set; }

        public void ShiftOffsetWithPanelPixelVector(Vector panelPixelShiftVector)
        {
            var zoom = _storyModel.StoryZoom;
            //Convert to unscaled shift distance
            var virtualPanelVector = new Vector(panelPixelShiftVector.X / zoom, -panelPixelShiftVector.Y / zoom);
            _storyModel.ShiftStoryExtent(virtualPanelVector);
        }

        public void AdjustStoryModelZoom(double multiplier, Point? zoomProjPoint)
        {
            if (zoomProjPoint != null)
            {
                var p = (Point)zoomProjPoint;
                var cartPoint = GeoToDrawLocationService.ConvertDrawPointToGeoPoint(p, _storyModel.StoryProjector);
                _storyModel.AdjustZoom(multiplier, cartPoint);
            }
            else
            {
                //Use the current story center
                _storyModel.AdjustZoom(multiplier, null);
            }
        }

        public Point ConvertViewPointToDrawPoint(Point viewPoint)
        {
            var scalePos = (Vector)viewPoint / StoryZoom;
            var mapPoint = (Point)((Vector)VirtualPanelOffset / StoryZoom + scalePos);
            return mapPoint;
        }

        public Point TopLeftLatLon()
        {
            var topLeftDraw = ConvertViewPointToDrawPoint(new Point(0, 0));
            var topLeftCartisian = LocationConverter.ConvertPanelToCartisianPoint(topLeftDraw);
            var topLeftLatLon = _storyModel.GetLatLon(topLeftCartisian.X, topLeftCartisian.Y);
            return topLeftLatLon;
        }

        public Point BottomRightLatLon()
        {
            var bottomRightDraw = ConvertViewPointToDrawPoint(new Point(ViewPaneSize.Width, ViewPaneSize.Height));
            var bottomRightCartesian = LocationConverter.ConvertPanelToCartisianPoint(bottomRightDraw);
            var bottomRightLatLon = _storyModel.GetLatLon(bottomRightCartesian.X, bottomRightCartesian.Y);
            return bottomRightLatLon;
        }
        
        #endregion story pass throughs

        #region Story actions that are controlled via the context menu

        #region general map commands center, copy etc

        private DelegateCommand<Point> copyRightClickLocationToClipboard;
        public ICommand CopyRightClickToClipboardCommand
        {
            get
            {
                if (copyRightClickLocationToClipboard == null)
                {
                    copyRightClickLocationToClipboard = new DelegateCommand<Point>(CopyLocation);
                    copyRightClickLocationToClipboard.Text = "Copy location to clipboard";
                }
                return copyRightClickLocationToClipboard;
            }
        }
        protected void CopyLocation(Point geoLocationDecimal)
        {
            Clipboard.SetText(geoLocationDecimal.ToString());
        }

        #endregion

        #region element map commands

        private DelegateCommand<IElement> showElementPropertiesDialog;
        public ICommand ShowElementPropertiesDialogCommand
        {
            get
            {
                if (showElementPropertiesDialog == null)
                {
                    showElementPropertiesDialog = new DelegateCommand<IElement>(ShowElementPropertiesDialog);
                    showElementPropertiesDialog.Text = "Properties";
                }
                return showElementPropertiesDialog;
            }
        }
        protected void ShowElementPropertiesDialog(IElement elementModel)
        {
            if (_dialogService == null) return;
            var annotationEditDialogVm = new SingleElementPropertiesDialogVm(elementModel);
            _dialogService.ShowWindow(this, annotationEditDialogVm);
        }

        private DelegateCommand<IElement> deleteElement;
        public ICommand DeleteElementCommand
        {
            get
            {
                if (deleteElement == null)
                {
                    deleteElement = new DelegateCommand<IElement>(DeleteElement);
                    deleteElement.Text = "Delete Element";
                }
                return deleteElement;
            }
        }
        protected void DeleteElement(IElement elementModel)
        {
            _storyModel.RemoveElement(elementModel);
        }

        #endregion

        #region map annotation commands, add,remove, and edit

        private DelegateCommand<Point> addNewAnnotation;
        public ICommand AddNewAnnotationCommand
        {
            get
            {
                if (addNewAnnotation == null)
                {
                    addNewAnnotation = new DelegateCommand<Point>(AddNewAnnotation);
                    addNewAnnotation.Text = "Add Annotation";
                }
                return addNewAnnotation;
            }
        }
        protected void AddNewAnnotation(Point annotationLatLongLocation)
        {
            _storyModel.AddAnnotationAtLocation(annotationLatLongLocation.X, annotationLatLongLocation.Y);
        }

        //Duplicated in annotationeditdialogVM
        private DelegateCommand<IDepictionAnnotation> removeAnnotation;
        public ICommand RemoveAnnotationCommand
        {
            get
            {
                if (removeAnnotation == null)
                {
                    removeAnnotation = new DelegateCommand<IDepictionAnnotation>(RemoveAnnotation);
                    removeAnnotation.Text = "Remove Annotation";
                }
                return removeAnnotation;
            }
        }
        protected void RemoveAnnotation(IDepictionAnnotation annotationModel)
        {
            _storyModel.DeleteAnnotation(annotationModel);
        }
        //Maybe use an editing boolean property? that would move the show dialog to the annotation viewmodel
        //and change the way the context menu works in general
        private DelegateCommand<IDepictionAnnotation> showAnnotationPropertiesDialog;
        public ICommand ShowAnnotationPropertiesDialogCommand
        {
            get
            {
                if (showAnnotationPropertiesDialog == null)
                {
                    showAnnotationPropertiesDialog = new DelegateCommand<IDepictionAnnotation>(ShowAnnotationPropertiesDialog);
                    showAnnotationPropertiesDialog.Text = "Properties";
                }
                return showAnnotationPropertiesDialog;
            }
        }
        protected void ShowAnnotationPropertiesDialog(IDepictionAnnotation annotationModel)
        {
            if (_dialogService == null) return;
            var annotationEditDialogVm = new AnnotationEditDialogVM(annotationModel);
            _dialogService.ShowDialog(this, annotationEditDialogVm);
        }
        #endregion

        #endregion
    }

    public enum DepictionMapMode
    {
        Uninitialized,
        RegionSelection,
        Normal,
        ElementAdd,
        ZOIEdit,
        ZOIAdd,
        ImageRegistration,
        AreaSelection,
        ArbitraryAreaSelection
    }
}