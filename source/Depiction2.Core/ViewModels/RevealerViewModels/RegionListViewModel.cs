﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.ViewModels.RevealerViewModels
{
    public class RegionListViewModel : ViewModelBase
    {
        public IDepictionRegion _regionBase;
        private string _regionName = string.Empty;
        #region properties
        public string RegionId { get { return _regionBase.DisplayerId; } }
        public string RegionName
        {
            get
            {
                
                return _regionName;
            }
            set
            {
                _regionName = value;
                _regionBase.DisplayerName = value;
                OnPropertyChanged("RegionName");
            }
        }

        #endregion
        #region constructor

        public RegionListViewModel(IDepictionRegion baseRegion)
        {
            _regionBase = baseRegion;
            _regionName = baseRegion.DisplayerName;
        }
        #endregion
    }
}