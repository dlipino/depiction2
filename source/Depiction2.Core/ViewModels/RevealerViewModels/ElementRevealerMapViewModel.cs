﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.Base.Helpers;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels.RevealerViewModels
{
    public class ElementRevealerMapViewModel : ElementDisplayerMapViewModel
    {
        public const double MINCONTENT_X = -25;
        public const double MINCONTENT_Y = -25;
        private const double OPACITY_MIN = .0;
        private const double OPACITY_MAX = 1;

        private const double DEFAULTOPACITY = .6;

        public const string RevealerTopColor = "Red";
        public const string RevealerNormalColor = "Maroon";// Brushes.
        public const string RevealerAnchoredColor = "White";
        private IElementRevealer revealerModel = null;

        #region variables

        private double _minContentLocationX = MINCONTENT_X;
        private double _minContentLocationY = MINCONTENT_Y;

        private bool _mouseByPassEnabled;

        private bool isRevealerMaximized;
        private bool isRevealerBorderVisible;
        private bool isRevealerAnchored;
        private bool revealerBottomVisible = true;
        private bool revealerSideVisible;
        private bool revealerTopVisible = true;
        private bool permaTextVisible;
        private double revealerOpacity = DEFAULTOPACITY;
        private bool isRevealerRect = true;
        private DisplayerViewType _shapeType = DisplayerViewType.Rectangle;

        private string revealerColor = RevealerNormalColor;

        #endregion
        #region properties
        public string RevealerId { get { return revealerModel.DisplayerId; } }
        public string RevealerColor { get { return revealerColor; } set { revealerColor = value; OnPropertyChanged("RevealerColor"); } }

        override public DisplayerViewType ShapeType
        {
            get
            {
                if (!isRevealerMaximized) return DisplayerViewType.Minimized;

                return _shapeType;
            }
        }

        public int ElementCount { get; private set; }
        public string Units
        {
            get
            {
                if (Settings.Default.MeasurementSystem == MeasurementSystem.Metric)
                {
                    return Distance.kmString;
                }
                else
                {
                    return Distance.mileString;
                }
            }
        }
        public bool MouseByPassEnabled
        {
            get { return _mouseByPassEnabled; }
            set
            {
                _mouseByPassEnabled = value;
            }
        }

        public double MinContentLocationX
        {
            get { return _minContentLocationX; }
            set
            {
                revealerModel.SetRevealerPropertyValue("PermaTextX", value);
            }
        }

        public double MinContentLocationY
        {
            get { return _minContentLocationY; }
            set
            {
                revealerModel.SetRevealerPropertyValue("PermaTextY", value);
            }
        }

        public bool RevealerMaximized
        {
            get { return isRevealerMaximized; }
            set
            {
                revealerModel.SetRevealerPropertyValue("Maximized", value);
            }
        }
        public bool RevealerBorderVisible
        {
            get { return isRevealerBorderVisible; }
            set
            {
                revealerModel.SetRevealerPropertyValue("BorderVisible", value);
            }
        }
        public bool RevealerAnchored
        {
            get { return isRevealerAnchored; }
            set
            {
                if (value)
                {
                    RevealerColor = RevealerAnchoredColor;
                }
                else
                {
                    RevealerColor = RevealerNormalColor;
                }
                revealerModel.SetRevealerPropertyValue("Anchored", value);
            }
        }
        public bool IsRectRevealer
        {
            get { return isRevealerRect; }
            set
            {
                isRevealerRect = value;
                if (isRevealerRect)
                {
                    revealerModel.RevealerShape = DisplayerViewType.Rectangle;//SetRevealerPropertyValue("RevealerShape", DisplayerViewType.Rectangle);
                }
                else
                {
                    revealerModel.RevealerShape = DisplayerViewType.Circle;//SetRevealerPropertyValue("RevealerShape", DisplayerViewType.Circle);
                }

                OnPropertyChanged("IsRectRevealer");
            }
        }
        override public double DisplayerOpacity
        {
            get { return revealerOpacity; }
            set
            {
                var tempOpacity = value;
                if (tempOpacity > OPACITY_MAX) tempOpacity = OPACITY_MAX;
                if (tempOpacity < OPACITY_MIN) tempOpacity = OPACITY_MIN;
                revealerModel.SetRevealerPropertyValue("Opacity", tempOpacity);
            }
        }
        #region menu visibility control, the property values do not match due to legacy conflicts and general lazyness
        public bool RevealerBottomVisible
        {
            get { return revealerBottomVisible; }
            set
            {
                revealerModel.SetRevealerPropertyValue("BottomMenu", value);
            }
        }
        public bool RevealerTopVisible
        {
            get { return revealerTopVisible; }
            set
            {
                revealerModel.SetRevealerPropertyValue("TopMenu", value);
            }
        }
        public bool RevealerSideVisible
        {
            get { return revealerSideVisible; }
            set
            {
                revealerModel.SetRevealerPropertyValue("SideMenu", value);
            }
        }
        #endregion


        #endregion

        #region Constructor

        public ElementRevealerMapViewModel(IElementRevealer displayer, ObservableCollection<MapElementViewModel> elementSource)
            : base(displayer, elementSource)
        {
            revealerModel = displayer;
            //What to do if the properties are not present?
            isRevealerMaximized = (bool)revealerModel.GetPropertyValue("Maximized");
            isRevealerBorderVisible = (bool)revealerModel.GetPropertyValue("BorderVisible");
            RevealerAnchored = (bool)revealerModel.GetPropertyValue("Anchored");
            revealerOpacity = (double)revealerModel.GetPropertyValue("Opacity");
            _shapeType = revealerModel.RevealerShape;//(DisplayerViewType)(revealerModel.GetPropertyValue("RevealerShape"));
            revealerTopVisible = (bool)revealerModel.GetPropertyValue("TopMenu");
            revealerBottomVisible = (bool)revealerModel.GetPropertyValue("BottomMenu");
            revealerSideVisible = (bool)revealerModel.GetPropertyValue("SideMenu");
            _minContentLocationX = (double)revealerModel.GetPropertyValue("PermaTextX");
            _minContentLocationY = (double)revealerModel.GetPropertyValue("PermaTextY");
            displayerOpacity = revealerOpacity;
            displayerZIndex = DrawZIndexes.RevealerDisplayerBaseZ;
            Settings.Default.PropertyChanged += Default_PropertyChanged;
        }

        private void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("MeasurementSystem"))
            {
                OnPropertyChanged("Units");
                OnPropertyChanged("GeoDisplayWidth");
                OnPropertyChanged("GeoDisplayHeight");
            }
        }

        protected override void OnDispose()
        {
            Settings.Default.PropertyChanged -= Default_PropertyChanged;
            base.OnDispose();
        }

        protected override void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Maximized", StringComparison.InvariantCultureIgnoreCase))
            {
                isRevealerMaximized = (bool)revealerModel.GetPropertyValue("Maximized");
                OnPropertyChanged("ShapeType");
                OnPropertyChanged("RevealerClip");
            }
            else if (e.PropertyName.Equals("BorderVisible", StringComparison.InvariantCultureIgnoreCase))
            {
                isRevealerBorderVisible = (bool)revealerModel.GetPropertyValue("BorderVisible");
                OnPropertyChanged("RevealerBorderVisible");
            }
            else if (e.PropertyName.Equals("Anchored", StringComparison.InvariantCultureIgnoreCase))
            {
                isRevealerAnchored = (bool)revealerModel.GetPropertyValue("Anchored");
                OnPropertyChanged("RevealerAnchored");
            }
            else if (e.PropertyName.Equals("Opacity", StringComparison.InvariantCultureIgnoreCase))
            {
                revealerOpacity = (double)revealerModel.GetPropertyValue("Opacity");
                OnPropertyChanged("DisplayerOpacity");
            }
            else if (e.PropertyName.Equals("RevealerShape", StringComparison.InvariantCultureIgnoreCase))
            {
                _shapeType = revealerModel.RevealerShape;// (DisplayerViewType)(revealerModel.GetPropertyValue("RevealerShape"));
                OnPropertyChanged("ShapeType");
                OnPropertyChanged("RevealerClip");
            }
            else if (e.PropertyName.Equals("TopMenu", StringComparison.InvariantCultureIgnoreCase))
            {

                revealerTopVisible = (bool)revealerModel.GetPropertyValue("TopMenu");
                OnPropertyChanged("RevealerTopVisible");
            }
            else if (e.PropertyName.Equals("BottomMenu", StringComparison.InvariantCultureIgnoreCase))
            {
                revealerBottomVisible = (bool)revealerModel.GetPropertyValue("BottomMenu");
                OnPropertyChanged("RevealerBottomVisible");
            }
            else if (e.PropertyName.Equals("SideMenu", StringComparison.InvariantCultureIgnoreCase))
            {
                revealerSideVisible = (bool)revealerModel.GetPropertyValue("SideMenu");
                OnPropertyChanged("RevealerSideVisible");
            }
            else if (e.PropertyName.Equals("PermaTextX", StringComparison.InvariantCultureIgnoreCase))
            {
                _minContentLocationX = (double)revealerModel.GetPropertyValue("PermaTextX");
                OnPropertyChanged("MinContentLocationX");
            }
            else if (e.PropertyName.Equals("PermaTextY", StringComparison.InvariantCultureIgnoreCase))
            {
                _minContentLocationY = (double)revealerModel.GetPropertyValue("PermaTextY");
                OnPropertyChanged("MinContentLocationY");
            }
            else
            {
                base.model_PropertyChanged(sender, e);
            }
        }
        #endregion

        #region attribute change helper methods

        public void SetTopRevealerAttributes()
        {
            RevealerColor = RevealerTopColor;
            DisplayerZIndex = DrawZIndexes.TopRevealerDisplayerBaseZ;

        }
        public void SetToNormalRevealerAttributes()
        {
            if (RevealerAnchored)
            {
                RevealerColor = RevealerAnchoredColor;
            }
            else
            {
                RevealerColor = RevealerNormalColor;
            }
            DisplayerZIndex = DrawZIndexes.RevealerDisplayerBaseZ;
        }

        #endregion
        #region commands. these might be temporary as they might get global

        private DelegateCommand deleteRevealer;
        public ICommand DeleteRevealerCommand
        {
            get
            {
                if (deleteRevealer == null)
                {
                    deleteRevealer = new DelegateCommand(DeleteRevealer);
                    deleteRevealer.Text = "Delete";
                }
                return deleteRevealer;
            }
        }
        protected void DeleteRevealer()
        {
            DepictionAccess.DStory.DeleteRevealer(revealerModel);
        }

        private DelegateCommand countElements;
        public ICommand CountVisibleElementsCommand
        {
            get
            {
                if (countElements == null)
                {
                    countElements = new DelegateCommand(CountElementsInBounds);
                    countElements.Text = "Count";
                }
                return countElements;
            }
        }
        protected void CountElementsInBounds()
        {
            var story = DepictionAccess.DStory;
            if (story == null)
            {
                ElementCount = int.MinValue;
                return;
            }

            ElementCount = story.GetElementIdsInRect(revealerModel.GeoRectBounds).Count;
            OnPropertyChanged("ElementCount");
        }


        private DelegateCommand showRevealerDetailsCommand;

        public ICommand ShowRevealerDetailsCommand
        {
            get
            {
                if (showRevealerDetailsCommand == null)
                {
                    showRevealerDetailsCommand = new DelegateCommand(RequestShowDisplayerInformation);
                }
                return showRevealerDetailsCommand;
            }
        }

        #endregion
    }
}