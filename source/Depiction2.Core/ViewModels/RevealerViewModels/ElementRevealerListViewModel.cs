﻿using System;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;

namespace Depiction2.Core.ViewModels.RevealerViewModels
{
    public class ElementRevealerListViewModel : ElementDisplayerListViewModel
    {
        private IElementRevealer revealerModel = null;
        private bool isRevealerMaximized = true;

        #region Properties

        public bool RevealerMaximized
        {
            get { return isRevealerMaximized; }
            set
            {
                revealerModel.SetRevealerPropertyValue("Maximized", value);
            }
        }
        #endregion

        #region Constructor and destructor stuff

        public ElementRevealerListViewModel(IElementRevealer model):base(model)
        {
            revealerModel = model;
            isRevealerMaximized = (bool)revealerModel.GetPropertyValue("Maximized");
            revealerModel.PropertyChanged += revealerModel_PropertyChanged;
        }

        protected override void OnDispose()
        {
            revealerModel.PropertyChanged -= revealerModel_PropertyChanged;
            base.OnDispose();
        }
        #endregion
        #region constructor event handlers
        void revealerModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Maximized", StringComparison.InvariantCultureIgnoreCase))
            {
                isRevealerMaximized = (bool)revealerModel.GetPropertyValue("Maximized");
            }
            else
            {
                OnPropertyChanged(e.PropertyName);
            }
        }
        #endregion

        #region commands

        private DelegateCommand deleteRevealer;
        public ICommand DeleteRevealerCommand
        {
            get
            {
                if (deleteRevealer == null)
                {
                    deleteRevealer = new DelegateCommand(DeleteRevealer);
                    deleteRevealer.Text = "Delete";
                }
                return deleteRevealer;
            }
        }
        protected void DeleteRevealer()
        {
            DepictionAccess.DStory.DeleteRevealer(revealerModel);
        }

        #endregion
    }
}