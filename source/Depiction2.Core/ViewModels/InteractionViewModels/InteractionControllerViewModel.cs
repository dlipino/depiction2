﻿using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.ViewModels.InteractionViewModels
{
    public class InteractionControllerViewModel : ViewModelBase
    {
        private IInteractionRule _baseRule;
        private bool _selected;
        public bool Selected { get { return _selected; } set { _selected = value; OnPropertyChanged("Selected"); } }
        public bool IsDisabled
        {
            get { return _baseRule.IsDisabled; }
            set
            {
                _baseRule.IsDisabled = value;
                OnPropertyChanged("IsDisabled");
            }

        }

        public string Name { get { return _baseRule.Name; } set { _baseRule.Name = value; OnPropertyChanged("Name"); } }

        public InteractionControllerViewModel(IInteractionRule interaction)
        {
            _baseRule = interaction;
        }
    }
}