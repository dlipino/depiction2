﻿using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.ViewModels.InteractionViewModels
{
    public class InteractionInformationViewModel : ViewModelBase
    {
        private IInteractionRule _baseRule = null;
        private bool _selected;
        private string _name;
        public bool Selected { get { return _selected; } set { _selected = value; OnPropertyChanged("Selected"); } }
        public string Name { get { return _name; } set { _name = value; OnPropertyChanged("Name"); } }
        public string RuleSource { get { return _baseRule.RuleSource; } }
        public IInteractionRule RuleModel { get { return _baseRule; } }
        public InteractionInformationViewModel(IInteractionRule interaction)
        {
            _baseRule = interaction;
            _name = interaction.Name;
        }
    }
}