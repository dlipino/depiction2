﻿using System.Collections.Generic;
using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.ViewModels.InteractionViewModels
{
    public class InteractionEditorViewModel : ViewModelBase
    {
        private IInteractionRule _baseRule;

        public bool IsRuleEditable { get; set; }
        public string InteractionName { get; set; }
        public string InteractionAuthor { get; set; }
        public string InteractionDescription { get; set; }
        public string InteractionSource { get; set; }

        public string Publisher { get { return _baseRule.Publisher; } }

        public IEnumerable<string> Subscribers
        {
            get
            {
                return _baseRule.Subscribers;
            }
        }

        public InteractionEditorViewModel(IInteractionRule mainRule)
        {
            _baseRule = mainRule;
            InteractionName = _baseRule.Name;
            InteractionAuthor = _baseRule.Author;
            InteractionDescription = _baseRule.Description;
            InteractionSource = _baseRule.RuleSource;

        }



    }
}
