﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate.ViewModel;
using Depiction2.Core.ViewModels.ElementTemplateViewModels;

namespace Depiction2.Core.ViewModels.ElementTemplates
{
    public class ElementTemplateLibraryExplorerViewModel : ViewModelBase
    {
        #region variables


        #endregion

        #region properties
        public ElementTemplateLibraryViewerViewModel ElementTemplateLibraryViewModel { get; private set; }
        public ElementTemplateViewModel SelectedTemplate { get; private set; }
        #endregion

        #region command structure

        #endregion
        #region constructor
        public ElementTemplateLibraryExplorerViewModel(IElementTemplateLibrary templateLibrary)
        {
            if (templateLibrary == null)
            {
                //not sure what should really be done in this case.
                return;
            }

            ElementTemplateLibraryViewModel = new ElementTemplateLibraryViewerViewModel(templateLibrary);
            ElementTemplateLibraryViewModel.PropertyChanged += ScaffoldLibraryViewModel_PropertyChanged;

        }

        void ScaffoldLibraryViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals("SelectedTemplate"))
            {
                DisplaySelectedScaffold(ElementTemplateLibraryViewModel.SelectedTemplate);
            }
        }
        #endregion

        #region helpers
        private void DisplaySelectedScaffold(IElementTemplate selectedElementTemplate)
        {
           SelectedTemplate = new ElementTemplateViewModel(selectedElementTemplate);
            OnPropertyChanged("SelectedTemplate");
        }
        #endregion
    }
}