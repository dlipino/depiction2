﻿using System;
using System.Collections.ObjectModel;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.ViewModels.ElementTemplates
{
    public class ElementTemplateFilterAndQuickAddControlViewModel : ViewModelBase
    {
        private IElementTemplateLibrary _libraryModel;
        private DelegateCommand addFilterCommand;
        private DelegateCommand deleteFilterCommand;


        public IElementTemplateLibraryFilter SelectedFilter { get; set; }

        public ObservableCollection<IElementTemplateLibraryFilter> AllFilters { get; set; }

        #region constructor/destruction

        public ElementTemplateFilterAndQuickAddControlViewModel(IElementTemplateLibrary libraryBase)
        {
            _libraryModel = libraryBase;
            AllFilters = new ObservableCollection<IElementTemplateLibraryFilter>(libraryBase.LibraryFilters);
           
            _libraryModel.PropertyChanged += _libraryModel_PropertyChanged;
        }
        protected override void OnDispose()
        {
            _libraryModel.PropertyChanged -= _libraryModel_PropertyChanged;
            base.OnDispose();
        }
      

        #endregion
        #region event hanlders
        void _libraryModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName.Equals("LibraryFilters",StringComparison.InvariantCultureIgnoreCase))
            {
                LoadFiltersFromModel();
            }
        }
        #endregion
        protected void LoadFiltersFromModel()
        {
//            if (!File.Exists(defaultFullFileName)) return;
//            var newFilters = ElementTemplateLibraryFilter.LoadFiltersFromDir(defaultFullFileName);
            AllFilters.Clear();
            foreach (var filter in _libraryModel.LibraryFilters)
            {
                AllFilters.Add(filter);
            }
        }
    }
}