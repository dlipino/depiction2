﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.ViewModels.ElementTemplates
{
    public class ElementTemplateExplorerDialogViewModel :DialogViewModel
    {
        public ElementTemplateLibraryExplorerViewModel ControllerViewModel { get; set; }

        #region constructor

        public ElementTemplateExplorerDialogViewModel(IElementTemplateLibrary templateLibrary)
        {
            IsModal = false;
            HideOnClose = true;
            ControllerViewModel = new ElementTemplateLibraryExplorerViewModel(templateLibrary);
        }

        #endregion
    }
}