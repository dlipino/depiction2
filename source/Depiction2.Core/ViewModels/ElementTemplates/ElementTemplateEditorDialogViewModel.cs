﻿using System.Collections.Generic;
using System.Windows.Input;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Entities.ElementTemplate.ViewModel;
using Depiction2.Core.StoryEntities.ElementTemplates;
using DepictionLegacy.FileBrowser;

namespace Depiction2.Core.ViewModels.ElementTemplates
{
    public class ElementTemplateEditorDialogViewModel : DialogViewModel
    {
        private ElementTemplateViewModel _editingTemplate;
        private ElementTemplate _originalTemplate;
        #region properties
   
        public ElementTemplateViewModel EditingTemplate
        {
            get { return _editingTemplate; }
            set { _editingTemplate = value; OnPropertyChanged("EditingTemplate"); }
        }
        #endregion
        #region dialog commands
        private DelegateCommand createNewScaffoldCommand;
        public ICommand CreateNewScaffoldCommand
        {
            get
            {
                if (createNewScaffoldCommand == null)
                {
                    createNewScaffoldCommand = new DelegateCommand(CreateNewScaffold);
                }
                return createNewScaffoldCommand;
            }
        }

        private DelegateCommand loadScaffoldFileCommand;
        public ICommand LoadScaffoldFileCommand
        {
            get
            {
                if (loadScaffoldFileCommand == null)
                {
                    loadScaffoldFileCommand = new DelegateCommand(LoadScaffoldFile);
                }
                return loadScaffoldFileCommand;
            }
        }
        #region save stuff
        private DelegateCommand saveScaffoldCommand;
        public ICommand SaveScaffoldToFileCommand
        {
            get
            {
                if (saveScaffoldCommand == null)
                {
                    saveScaffoldCommand = new DelegateCommand(SaveScaffoldToFile);
                }
                return saveScaffoldCommand;
            }
        }

     
        #endregion

        #region apply changes
        private DelegateCommand applyScaffoldChangesCommand;
        public ICommand ApplyScaffoldChangesCommand
        {
            get
            {
                if (applyScaffoldChangesCommand == null)
                {
                    applyScaffoldChangesCommand = new DelegateCommand(ApplyScaffoldChanges);
                }
                return applyScaffoldChangesCommand;
            }
        }

    
        #endregion

        #endregion

        #region constructor
        public ElementTemplateEditorDialogViewModel(ElementTemplate templateBase)
        {
            _originalTemplate = templateBase;
            EditingTemplate = new ElementTemplateViewModel(templateBase);
        }
        #endregion

        #region controlling methods

        public void CreateNewScaffold()
        {

            if (!WarnOfCurrentScaffoldDestruction()) return;
            EditingTemplate = new ElementTemplateViewModel(_originalTemplate);
        }
        public void SaveScaffoldToFile()
        {
            string selectedSaver;
            var availableSavers = StorySerializationService.GetStorySavers();
            var filterList = new List<FileFilter>();
            foreach (var loader in availableSavers)
            {
                var meta = loader.Value;
                filterList.Add(FileFilter.CreateStoryTypeFilter(meta.Name, meta.ScaffoldExtension, meta.DisplayName));
            }

            var newFileName = DepictionFileBrowser.GetScaffoldSaveFileName(filterList, out selectedSaver);
            if (string.IsNullOrEmpty(newFileName)) return;

            var scaffold = EditingTemplate.GetElementScaffold();
            StorySerializationService.SaveScaffoldToFile(selectedSaver,newFileName,scaffold);
        }

        public void LoadScaffoldFile()
        {

//            if (!WarnOfCurrentScaffoldDestruction()) return;
//            string selectedSaver;
//            var availableSavers = StoryService.GetStoryLoaders();
//            var filterList = new List<FileFilter>();
//            foreach (var loader in availableSavers)
//            {
//                var meta = loader.Value;
//                filterList.Add(FileFilter.CreateStoryTypeFilter(meta.Name, meta.ScaffoldExtension, meta.DisplayName));
//            }
//
//            var newFileName = DepictionFileBrowser.GetScaffoldSaveFileName(filterList, out selectedSaver);
//            if (string.IsNullOrEmpty(newFileName)) return;
//
//            var scaffold = EditingTemplate.GetElementScaffold();
//            StoryService.SaveScaffoldToFile(selectedSaver, newFileName, scaffold);
        }
        public void ApplyScaffoldChanges()
        {
            _editingTemplate.ApplyAllPropertyChanges();
        }
        #endregion

        private bool WarnOfCurrentScaffoldDestruction()
        {
            return true;
        }
    }
}