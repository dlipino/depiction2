﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Depiction2.Base.Helpers;
using Depiction2.Core.Entities.Element.ViewModel;

namespace Depiction2.Core.ViewModels
{
    public class SimpeElementDisplayerVm :ViewModelBase
    {
        private ObservableCollection<MapElementViewModel> allElementVms;
        public IEnumerable<MapElementViewModel> TempElementViewModels
        {
            get { return allElementVms; }
        }
    }
}