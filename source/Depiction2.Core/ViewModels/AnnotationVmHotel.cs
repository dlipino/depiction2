﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;

namespace Depiction2.Core.ViewModels
{
    public class AnnotationVmHotel: ViewModelBase
    {
        private IStory _storyModel;
        private SimpleObservableSpatialTree<DepictionAnnotationVM> allAnnotationVms;
        public ObservableCollection<DepictionAnnotationVM> AllAnnotationVms
        {
            get { return allAnnotationVms; }
        }
        #region construction/destruction
        public AnnotationVmHotel(IStory story)
        {
            _storyModel = story;
            _storyModel.AnnotationCollectionChanged += AllAnnotations_CollectionChanged;
            _storyModel.PropertyChanged += _storyModel_PropertyChanged;
            allAnnotationVms = new SimpleObservableSpatialTree<DepictionAnnotationVM>();
            foreach (var annot in story.AllAnnotations)
            {
                var annotVm = new DepictionAnnotationVM(annot, story.StoryZoom);
                allAnnotationVms.AddSpatial(annotVm);
            }
        }

        void _storyModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("StoryZoom"))
           {
               foreach(var annotVm in AllAnnotationVms)
               {
                   annotVm.AdjustVisibilityBasedOnVisibleScale(_storyModel.StoryZoom);
               }
           }
        }
        protected override void OnDispose()
        {
            allAnnotationVms.Clear();
            _storyModel.AnnotationCollectionChanged -= AllAnnotations_CollectionChanged;
            _storyModel.PropertyChanged -= _storyModel_PropertyChanged;
            base.OnDispose();
        }

        #endregion
        void AllAnnotations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;

            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                var newElements = e.NewItems;
                foreach (var newAnnot in newElements)
                {
                    if (newAnnot is IDepictionAnnotation)
                    {
                        allAnnotationVms.AddSpatial(new DepictionAnnotationVM(newAnnot as IDepictionAnnotation, _storyModel.StoryZoom));
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                //TODO optimize, especially if this is how elements are removed.
                var oldAnnots = e.OldItems;
                foreach (var oldAnnot in oldAnnots)
                {
                    if (oldAnnot is IDepictionAnnotation)
                    {
                        var annot = oldAnnot as IDepictionAnnotation;
                        var match = allAnnotationVms.FirstOrDefault(a => a.AnnotationID.Equals(annot.AnnotationID));
                        if (match != null)
                        {
                            allAnnotationVms.RemoveSpatial(match);
                        }
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {
                allAnnotationVms.Clear();
                foreach (var annot in _storyModel.AllAnnotations)
                {
                    allAnnotationVms.AddSpatial(new DepictionAnnotationVM(annot, _storyModel.StoryZoom));
                }
                //That does not work because there is no binding involved
                //AllAnnotationVms = new ObservableCollection<DepictionAnnotationVM>(annotList);
            }
        }
    }
}