﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Extension.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Messaging;
using Depiction2.Core.Interactions;
using Depiction2.Core.Product;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.DialogViewModels.App;
using Depiction2.Core.ViewModels.DialogViewModels.Map;
using Depiction2.Core.ViewModels.ElementTemplates;
using Depiction2.Core.ViewModels.Interactions;
using DepictionLegacy.FileBrowser;

namespace Depiction2.Core.ViewModels
{
    public class DepictionAppViewModel : ViewModelBaseMS
    {
        #region event
        /// <summary>
        /// Raised when this workspace is shutting down.
        /// </summary>
        public event EventHandler RequestClose;
        #endregion event

        private StoryActions _storyActions;//this is only temp for now
        private bool mShutDownInProgress;
        private bool? mDialogCloseResult;

        private bool mIsReadyToClose = true;
        private CurrentAppDepictionScreen _appScreen = CurrentAppDepictionScreen.Main;
        private StoryViewModelHotel _mapVM = null;
        private MessageServiceViewModel _messengerVm = null;
        private IDialogService _dialogService = null;

        private bool storyChange;
        private string _currentStoryFileName = string.Empty;
        private string _appTitle = VersionInfo.ProductTitle;

        #region properties

        public string AppTitle
        {
            get { return _appTitle; }
            set { _appTitle = value; OnPropertyChanged("AppTitle"); }
        }
        public CurrentAppDepictionScreen AppScreen
        {
            get { return _appScreen; }
            private set
            {
                _appScreen = value;
                NotifyPropertyChanged(() => AppScreen);
            }
        }

        public StoryViewModelHotel StoryVM
        {
            get { return _mapVM; }
            set { _mapVM = value; NotifyPropertyChanged(() => StoryVM); }
        }
        public MessageServiceViewModel MessengerVm
        {
            get { return _messengerVm; }
            set { _messengerVm = value; NotifyPropertyChanged(() => MessengerVm); }
        }
        /// <summary>
        /// This can be used to close the attached view via ViewModel
        /// 
        /// Source: http://stackoverflow.com/questions/501886/wpf-mvvm-newbie-how-should-the-viewmodel-close-the-form
        /// </summary>
        public bool? WindowCloseResult
        {
            get
            {
                return mDialogCloseResult;
            }

            set
            {
                if (mDialogCloseResult != value)
                {
                    mDialogCloseResult = value;
                    NotifyPropertyChanged(() => WindowCloseResult);
                }
            }
        }

        /// <summary>
        /// Get/set property to determine whether application is ready to close
        /// (the setter is public here to bind it to a checkbox - in a normal
        /// application that setter would more likely be private and be set via
        /// a corresponding method call that manages/overrides the properties' value).
        /// </summary>
        public bool IsReadyToClose
        {
            get
            {
                return mIsReadyToClose;
            }

            set
            {
                if (mIsReadyToClose != value)
                {
                    mIsReadyToClose = value;
                    NotifyPropertyChanged(() => IsReadyToClose);
                }
            }
        }

        /// <summary>
        /// Get/set delegate method to evaluate whether application can shut down or not.
        /// </summary>
        public Func<bool> OnSessionEnding { get; set; }
        #endregion properties

        #region Dialog View Models
        //Not all dialogVM need to be always around, especially the ones that only display text and are not moved
        //around, i guess this will evolve as progress gets made
        private AboutDialogVM aboutDialogVm;
        private EulaDialogVM eulaDialogVm;
        private DepictionSettingsDialogVM settingsDialogVm;
        private DepictionExtensionDisplayerDialogVm extensionDialogVm;
        private InteractionLibraryDialogViewModel interactionLibryaryVm;
        private InteractionEditorDialogViewModel interactionEditorVm;
        private ElementTemplateExplorerDialogViewModel _elementTemplateExplorerVm;
        private ElementTemplateEditorDialogViewModel _elementTemplateEditorVm;
        private ToolsDialogVM toolsDialogVM;

        #endregion

        #region Constructor

        public DepictionAppViewModel()
            : this(ServiceLocator.Resolve<IDialogService>())
        {

        }
        public DepictionAppViewModel(IDialogService mainDialogService)
        {
            _dialogService = mainDialogService;
            _messengerVm = new MessageServiceViewModel(DepictionAccess.MessageService);

        }

        protected override void OnDispose()
        {
            //This should be the end of the app, so hopefully nothing is needed.
            base.OnDispose();
        }
        #endregion

        #region methods
        private void UpdateStoryName(string newName)
        {
            _currentStoryFileName = newName;

            var simpleName = Path.GetFileName(newName);
            if (string.IsNullOrEmpty(simpleName))
            {
                AppTitle = VersionInfo.ProductTitle;
                return;
            }
            AppTitle = string.Format("{0} : {1}", VersionInfo.ProductTitle, simpleName);

        }
        /// <summary>
        /// Method to be executed when user closes the application.
        /// This method calls the <seealso cref="OnSessionEnding"/> delegate methode (if set)
        /// to evaluate whether session can actually end or not.
        /// 
        /// The method fires the  <seealso cref="RequestClose"/> event to signal outside objects
        /// that close application should occur now.
        /// </summary>
        public void ExitExecuted()
        {
            try
            {
                if (mShutDownInProgress == false)
                {
                    mShutDownInProgress = true;

                    if (OnSessionEnding != null)
                    {
                        if (OnSessionEnding())
                        {
                            mShutDownInProgress = false;
                            return;
                        }
                    }

                    WindowCloseResult = true;              // Close the MainWindow and tell outside world
                    // that we are closed down.
                    EventHandler handler = this.RequestClose;

                    if (handler != null)
                        handler(this, EventArgs.Empty);
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine("Exception occurred in OnRequestClose\n{0}", exp.ToString());
                mShutDownInProgress = false;
            }
        }

        public List<string> GeocodeHistory
        {
            get
            {
                if (DepictionAccess.GeocoderService == null) return new List<string>();
                var h = DepictionAccess.GeocoderService.RecentGeocodes;
                if (string.IsNullOrEmpty(LocationToGeocode))
                {

                    LocationToGeocode = h.FirstOrDefault();
                    OnPropertyChanged("LocationToGeocode");
                }
                return h;
            }
        }
        protected bool ConfirmSaveStory(IStory storyToSave)
        {
            var md = new MessageBoxDialogViewModel();
            md.MessageBoxType = MessageBoxButton.YesNoCancel;
            md.Title = "Save depiction before leaving?";
            md.Message = "Would you like to save the current depiction?";
            var result = _dialogService.ShowDialog(null, md);
            switch (md.MessageDialogBoxResult)
            {
                case (MessageBoxResult.Yes):
                case (MessageBoxResult.OK):
                    //Save
                    var saveResult = SaveDepictionStory(storyToSave, _currentStoryFileName);
                    if (saveResult) return true;
                    return ConfirmSaveStory(storyToSave);

                case (MessageBoxResult.No):
                    //do nothing
                    break;
                case (MessageBoxResult.None):
                case (MessageBoxResult.Cancel):
                    return false;

            }
            return true;
        }

        public bool SaveCurrentStoryIfChanged()
        {
            if (storyChange)
            {
                var confirmSuccess = ConfirmSaveStory(DepictionAccess.DStory);
                if (!confirmSuccess) return false;
            }
            return true;
        }
        public bool CloseCurrentDepiction(bool checkForChange)
        {
            if (DepictionAccess.DStory == null) return true;
            if (StoryVM == null) return true;
            if (checkForChange)
            {
                if (!SaveCurrentStoryIfChanged()) return false;
            }

            //Check for changes
            if (StoryVM != null)
            {
                StoryVM.Dispose();
            }
            _dialogService.CloseAllWindowsTestMethod();
            StoryVM = null;
            var s = DepictionAccess.DStory as Story;
            if (s == null) return false;
            s.StoryChanged -= DStory_StoryChanged;

            s.Dispose();
            DepictionAccess.DStory = null;
            ClearStoryActions();

            //TODO make sure to clean up the events.
            DepictionAccess.DStoryActions = null;
            _currentStoryFileName = string.Empty;
            if (DepictionAccess.PathService != null)
            {
                DepictionAccess.PathService.ClearLoadFileDirectory();
            }
            return true;
        }
        /// <summary>
        /// Return true if the current depiction is closed, also return true if there is no active depiction story
        /// </summary>
        /// <returns></returns>
        public bool CloseCurrentDepiction()
        {
            return CloseCurrentDepiction(true);
        }
        protected void StartStoryInteractionsAndCreateViewModel(IStory loadedStory)
        {
            var story = loadedStory;
            if (loadedStory == null) return;

            if (DepictionAccess.InteractionLibrary != null)
            {
                story.InteractionRules.LoadDefaultRulesIntoRepository(DepictionAccess.InteractionLibrary.AllInteractionRules);
            }
            if (story.InteractionsRunner != null)
            {
                story.InteractionsRunner.BeginAsync();
            }
            DepictionAccess.DStory = story;
            DepictionAccess.DStoryActions = new StoryActions();
            DepictionAccess.DStory.StoryChanged += DStory_StoryChanged;
            CoordinateSystemService.Instance.SetStoryCoordinateAndDisplaySystem(story.ElementCoordinateSystem,
                                                                                story.DisplayCoordinateSystem);
            RecreateStoryActions();
            var vmHotel = new StoryViewModelHotel();
            //order of next two is important
            vmHotel.FillHotel(DepictionAccess.DStory, _storyActions, _dialogService);
            StoryVM = vmHotel;
            //needs to be done after setting the storyvm because the 
            //region size is set at that point.
            vmHotel.TileDisplayerVm.GetTileVisualsForStoryRegion();

            AppScreen = CurrentAppDepictionScreen.Map;
            storyChange = false;
        }

        void DStory_StoryChanged(object sender, EventArgs e)
        {
            storyChange = true;
        }
        public string LocationToGeocode { get; set; }

        protected IStory CreateANewDepictionAtLocation(object mapLocation)
        {
            if (DepictionAccess.GeocoderService == null) return null;
            var geocoder = DepictionAccess.GeocoderService;
            var input = new GeocodeInput();
            input.CompleteAddress = mapLocation.ToString();
            var result = geocoder.GeocodeInput(input, true);
            if (result == null) return null;
            OnPropertyChanged("GeocodeHistory");
            //from 1.4, the arbitrary zoom is .1, but i think that was for mercator, so i have no idea what that means for city level etc.
            var s = new Story(DepictionAccess.InteractionLibrary);

            s.InitializeInteractionRouter();
            //            s.InteractionRules.AddInteractionsFromDirectory();
            //totally arbitrary number, hopefully find some way to set the scale that is required
            s.AdjustZoom(8257.01859259104, result);
            return s;
        }

        public void NewDepiction(object mapLocation)
        {
            var s = CreateANewDepictionAtLocation(mapLocation);
            if (s == null) return;
            StartStoryInteractionsAndCreateViewModel(s);
            StoryVM.CreateInitialRectRegion();
        }
        public void ToDepictionMenu()
        {
            if (!CloseCurrentDepiction()) return;
            AppScreen = CurrentAppDepictionScreen.Main;
            _dialogService.CloseAllWindowsTestMethod();
        }

        public void ToSelectDepictionLocationScreen()
        {
            if (!CloseCurrentDepiction()) return;
            AppScreen = CurrentAppDepictionScreen.GeoLocate;
            _dialogService.CloseAllWindowsTestMethod();
        }

        public void LoadExtensionFromDir()
        {
            var dllDir = DepictionFileBrowser.OpenFileDialog(AppDomain.CurrentDomain.BaseDirectory);
            if (string.IsNullOrEmpty(dllDir)) return;
            ExtensionService.Instance.LoadExtensionFromDir(Path.GetDirectoryName(dllDir));
        }

        #endregion methods

        #region helpers that are temp, maybe

        private void ClearStoryActions()
        {
            if (_storyActions != null)
            {
                _storyActions.Dispose();
                _storyActions = null;
            }
            DepictionAccess.DStoryActions = null;
        }

        private void CreateStoryActions()
        {
            _storyActions = new StoryActions();
            DepictionAccess.DStoryActions = _storyActions;
        }

        private void RecreateStoryActions()
        {
            ClearStoryActions();
            CreateStoryActions();
        }

        #endregion

        #region load story methods

        public bool CanLoadDepiction()
        {
            if (!DepictionGeometryService.Activated) return false;
            return StorySerializationService.GetStoryLoaders().Count != 0;
        }
        public void LoadADepiction()
        {
            var contineuWithLoad = SaveCurrentStoryIfChanged();
            //fals only appears with a cancel.
            if (!contineuWithLoad) return;
            string selectedLoader;
            var availableLoaders = StorySerializationService.GetStoryLoaders();
            var filterList = new List<FileFilter>();
            //Location for getting hte initial filter is not set in stone
            var initialFilterName = Settings.Default.LastStoryImporter;
            foreach (var loader in availableLoaders)
            {
                var meta = loader.Value;
                var createdFilter = FileFilter.CreateStoryTypeFilter(meta.Name, meta.FileExtension, meta.DisplayName);
                if (meta.Name.Equals(initialFilterName))
                {
                    createdFilter.Selected = true;
                }
                filterList.Add(createdFilter);
            }

            var fileToOpen = DepictionFileBrowser.GetDepictionToLoad(filterList, out selectedLoader);
            if (!string.IsNullOrEmpty(fileToOpen))
            {
                Settings.Default.LastStoryImporter = selectedLoader;
                Settings.Default.Save();
            }
            var loadedStory = StorySerializationService.Instance.LoadDepictionWithLoader(selectedLoader, fileToOpen);
            if (loadedStory == null) return;
            //Already did a change check
            CloseCurrentDepiction(false);
            UpdateStoryName(fileToOpen);

            StartStoryInteractionsAndCreateViewModel(loadedStory);
            //copy over the files used in the load to the current story directories
            var serializeFolder = StorySerializationService.Instance.DataSerializationFolder;
            var storyFolder = DepictionAccess.PathService.CurrentStoryDataDirectory;
            var filesInFolder = Directory.GetFiles(serializeFolder);
            foreach (var file in filesInFolder)
            {
                var fileName = Path.GetFileName(file);
                if (string.IsNullOrEmpty(fileName)) continue;
                File.Copy(file, Path.Combine(storyFolder, fileName), true);
            }
            StorySerializationService.Instance.CleanSerializationFolders();
        }

        #endregion

        #region story save methods

        public void SaveDepictionAs()
        {
            SaveDepictionStory(DepictionAccess.DStory, null);
        }
        //Overwrite the story with the same name
        public void SaveDepiction()
        {
            SaveDepictionStory(DepictionAccess.DStory, _currentStoryFileName);
        }

        protected bool SaveDepictionStory(IStory storyToSave, string storyFileName)
        {
            string selectedSaver = Settings.Default.LastStoryExporter;
            var availableSavers = StorySerializationService.GetStorySavers();
            var filterList = new List<FileFilter>();
            foreach (var loader in availableSavers)
            {
                var meta = loader.Value;
                filterList.Add(FileFilter.CreateStoryTypeFilter(meta.Name, meta.StoryExtension, meta.DisplayName));
            }
            var fileName = storyFileName;
            if (string.IsNullOrEmpty(storyFileName) || string.IsNullOrEmpty(selectedSaver))
            {
                fileName = DepictionFileBrowser.GetDepictionSaveFileName(filterList, out selectedSaver);
            }
            if (string.IsNullOrEmpty(fileName)) return false;
            var saveSuccess = StorySerializationService.Instance.SaveDepictionWithSaver(selectedSaver, fileName, storyToSave);
            if (saveSuccess)
            {
                Settings.Default.LastStoryExporter = selectedSaver;
                Settings.Default.Save();
                UpdateStoryName(fileName);
                storyChange = false;
            }
            return saveSuccess;
        }
        #endregion

        #region global dialog control
        public ToolsDialogVM ToggleToolsDialog(bool? toggleChoice)
        {
            if (toolsDialogVM == null)
            {
                toolsDialogVM = new ToolsDialogVM();
            }
            if (toggleChoice == true || toggleChoice == null)
            {
                _dialogService.ShowWindow(this, toolsDialogVM);
            }
            else
            {
                _dialogService.HideWindow(toolsDialogVM);
            }
            return toolsDialogVM;
        }
        public void ToggleSettingsDialog()
        {
            if (settingsDialogVm == null)
            {
                settingsDialogVm = new DepictionSettingsDialogVM();
            }
            _dialogService.ShowWindow(this, settingsDialogVm);
        }

        public void ToggleEulaDialog()
        {
            if (eulaDialogVm == null)
            {
                eulaDialogVm = new EulaDialogVM();
            }
            _dialogService.ShowDialog(this, eulaDialogVm);
        }
        public void ToggleAboutDialog()
        {
            if (aboutDialogVm == null)
            {
                aboutDialogVm = new AboutDialogVM();
            }
            _dialogService.ShowDialog(this, aboutDialogVm);
        }

        public void ToggleExtensionsDialog()
        {
            if (extensionDialogVm == null)
            {
                extensionDialogVm = new DepictionExtensionDisplayerDialogVm();
            }
            _dialogService.ShowWindow(this, extensionDialogVm);
        }
        public void ToggleInteractionLibraryDialog()
        {
            if (interactionLibryaryVm == null)
            {
                interactionLibryaryVm = new InteractionLibraryDialogViewModel(DepictionAccess.InteractionLibrary);
            }
            _dialogService.ShowWindow(this, interactionLibryaryVm);
        }

        public void ToggleInteractionEditorDialog()
        {
            if (interactionEditorVm == null)
            {
                interactionEditorVm = new InteractionEditorDialogViewModel();
            }
            _dialogService.ShowWindow(this, interactionEditorVm);
        }

        public void ToggleScaffoldExplorerDialog()
        {
            if (_elementTemplateExplorerVm == null)
            {
                _elementTemplateExplorerVm = new ElementTemplateExplorerDialogViewModel(DepictionAccess.TemplateLibrary);
            }
            _dialogService.ShowWindow(this, _elementTemplateExplorerVm);
        }

        public void ToggleScaffoldEditorDialog()
        {
            if (_elementTemplateEditorVm == null)
            {
                _elementTemplateEditorVm = new ElementTemplateEditorDialogViewModel(null);
            }
            _dialogService.ShowDialog(this, _elementTemplateEditorVm);
        }
        #endregion
    }

    #region screen enums
    public enum CurrentAppDepictionScreen
    {
        Main, GeoLocate, Map
    }
    #endregion
}