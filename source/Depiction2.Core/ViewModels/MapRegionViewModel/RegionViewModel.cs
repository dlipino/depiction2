﻿using System.Collections.ObjectModel;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;

namespace Depiction2.Core.ViewModels.MapRegionViewModel
{
    public class RegionViewModel : ElementDisplayerMapViewModel
    {
        public override DisplayerViewType ShapeType
        {
            get
            {
                return DisplayerViewType.Region;
            }
        }
        public RegionViewModel(IDepictionRegion displayer, ObservableCollection<MapElementViewModel> elementSource)
            : base(displayer, elementSource)
        {
        }
    }
}