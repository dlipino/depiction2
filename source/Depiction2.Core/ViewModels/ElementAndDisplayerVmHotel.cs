﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Threading;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Core.ViewModels.DialogViewModels.Map;
using Depiction2.Core.ViewModels.ElementDisplayerViewModels;
using Depiction2.Core.ViewModels.MapRegionViewModel;
using Depiction2.Core.ViewModels.RevealerViewModels;

namespace Depiction2.Core.ViewModels
{
    public class ElementAndDisplayerVmHotel : ViewModelBase
    {//kind of hacky. just trying to get the displayerdialog to appear
        //might be better to move this to the model? maybe?
        public event Action<string> RequestDisplayDialog;

        private IStory _storyModel;//Why do i keep this around?
        internal ElementRevealerMapViewModel topRevealer;
        //These are displayed in the revealer displayer
        private ObservableCollection<ElementDisplayerMapViewModel> revealersVM;
        //        private ObservableCollection<RegionViewModel> regionVms;
        private ElementDisplayerMapViewModel mainviewVM;

        private ObservableCollection<MapElementViewModel> allElementVms;

        #region properties
        public ObservableCollection<ElementDisplayerMapViewModel> RevealerVms
        {
            get { return revealersVM; }
        }
        public ElementDisplayerMapViewModel MainDisplayerVm
        {
            get { return mainviewVM; }
        }

        public IEnumerable<MapElementViewModel> ElementViewModels
        {
            get { return allElementVms; }
        }
        #endregion

        #region constructor/destrictpr

        public ElementAndDisplayerVmHotel(IStory story)
        {
            _storyModel = story;
            var vmList = new List<MapElementViewModel>();
            foreach (var element in story.AllElements)
            {
                var elemVM = new MapElementViewModel(element);
                elemVM.Index = vmList.Count;//give them numbers
                vmList.Add(elemVM);
            }
            allElementVms = new ObservableCollection<MapElementViewModel>(vmList);

            revealersVM = new ObservableCollection<ElementDisplayerMapViewModel>();

            foreach (var revealer in story.RevealerDisplayers)
            {
                var revealerVm = new ElementRevealerMapViewModel(revealer, allElementVms);
                revealerVm.RequestDisplayDialog += revealerVm_RequestDisplayDialog;
                revealersVM.Add(revealerVm);
            }
            foreach (var region in story.AllRegions)
            {
                var regionViewModel = new RegionViewModel(region, allElementVms);
                revealersVM.Add(regionViewModel);
            }

            mainviewVM = new ElementDisplayerMapViewModel(story.MainDisplayer, allElementVms);

            _storyModel.RevealerCollectionChanged += RevealerDisplayers_CollectionChanged;
            _storyModel.ElementCollectionChanged += AllElements_CollectionChanged;
            _storyModel.RegionCollectionChanged += RegionDisplayers_CollectionChanged;
            _storyModel.PropertyChanged += TopRevealerHack_PropertyChanged;
        }



        protected override void OnDispose()
        {
            foreach (var elem in allElementVms)
            {
                elem.Dispose();
            }
            allElementVms.Clear();
            foreach (var rev in revealersVM)
            {
                rev.RequestDisplayDialog -= revealerVm_RequestDisplayDialog;
                rev.Dispose();
            }
            revealersVM.Clear();

            mainviewVM.Dispose();
            _storyModel.RevealerCollectionChanged -= RevealerDisplayers_CollectionChanged;
            _storyModel.ElementCollectionChanged -= AllElements_CollectionChanged;

            _storyModel.RegionCollectionChanged -= RegionDisplayers_CollectionChanged;
            _storyModel.PropertyChanged -= TopRevealerHack_PropertyChanged;
            base.OnDispose();
        }

        void TopRevealerHack_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("TopRevealerId"))
            {
                var story = sender as IStory;
                if (story == null) return;
                if (topRevealer != null)
                {
                    if (topRevealer.DisplayerId.Equals(story.TopRevealerId))
                    {
                        return;
                    }
                    topRevealer.SetToNormalRevealerAttributes();
                }
                var newTop = revealersVM.FirstOrDefault(t => t.DisplayerId.Equals(story.TopRevealerId)) as ElementRevealerMapViewModel;
                if (newTop == null) return;
                topRevealer = newTop;

                topRevealer.SetTopRevealerAttributes();
            }
            //            else if (e.PropertyName.Equals("PrimaryRegion"))
            //            {
            //                var story = sender as IStory;
            //                if (story == null) return;
            //                //TODO hack, for now there is only one region so remove all previous regions
            //                var oldRegionVms = revealersVM.Where(t => t.GetType() == typeof (RegionViewModel)).ToList();
            //                foreach(var oldR in oldRegionVms)
            //                {
            //                    revealersVM.Remove(oldR);
            //                }
            //                var revVm = new RegionViewModel(story.PrimaryRegion,allElementVms);
            //                revealersVM.Add(revVm);
            //                //Order is important, the revealer has to be drawn first then the clip region must call an update
            //                revVm.UpdateDrawBounds();
            //            }
        }

        void revealerVm_RequestDisplayDialog(string revealerId)
        {
            if (RequestDisplayDialog != null)
            {
                RequestDisplayDialog(revealerId);
            }
        }

        #endregion
        private void RegionDisplayers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                var newRevealers = e.NewItems;
                foreach (var newRevealer in newRevealers)
                {
                    if (newRevealer is IDepictionRegion)
                    {
                        var revVm = new RegionViewModel((IDepictionRegion)newRevealer, allElementVms);
                        revealersVM.Add(revVm);
                        //Order is important, the revealer has to be drawn first then the clip region must call an update
                        revVm.UpdateDrawBounds();
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                //TODO optimize, especially if this is how elements are removed.
                var oldRevealers = e.OldItems;
                foreach (var oldRevealer in oldRevealers)
                {
                    if (oldRevealer is IDepictionRegion)
                    {
                        var revealer = oldRevealer as IDepictionRegion;
                        var match = revealersVM.FirstOrDefault(a => a.DisplayerId.Equals(revealer.DisplayerId));
                        if (match != null)
                        {
                            revealersVM.Remove(match);
                            match.Dispose();
                        }
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {

            }
        }
        void RevealerDisplayers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                var newRevealers = e.NewItems;
                foreach (var newRevealer in newRevealers)
                {
                    if (newRevealer is IElementRevealer)
                    {
                        var revVm = new ElementRevealerMapViewModel((IElementRevealer)newRevealer, allElementVms);
                        revVm.RequestDisplayDialog += revealerVm_RequestDisplayDialog;
                        revealersVM.Add(revVm);
                        //Order is important, the revealer has to be drawn first then the clip region must call an update
                        revVm.UpdateDrawBounds();
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                //TODO optimize, especially if this is how elements are removed.
                var oldRevealers = e.OldItems;
                foreach (var oldRevealer in oldRevealers)
                {
                    if (oldRevealer is IElementRevealer)
                    {
                        var revealer = oldRevealer as IElementRevealer;
                        var match = revealersVM.FirstOrDefault(a => a.DisplayerId.Equals(revealer.DisplayerId));
                        if (match != null)
                        {
                            match.RequestDisplayDialog -= revealerVm_RequestDisplayDialog;
                            revealersVM.Remove(match);
                            match.Dispose();
                        }
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {

            }
        }


        void AllElements_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (System.Windows.Application.Current != null)
            {
                if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                                                              new Action<object, NotifyCollectionChangedEventArgs>(
                                                                                  AllElements_CollectionChanged), sender, e);
                    return;
                }
            }

            var eventAction = e.Action;
            if (eventAction.Equals(NotifyCollectionChangedAction.Add))
            {
                var newElements = e.NewItems;
                foreach (var newElem in newElements)
                {
                    if (newElem is IElement)
                    {
                        var elemVM = new MapElementViewModel(newElem as IElement);
                        //TODO clean up on aisle 4
                        elemVM.Index = allElementVms.Count;//set the index to their current array location (for zoomable canvase)
                        allElementVms.Add(elemVM);
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Remove))
            {
                var oldElements = e.OldItems;
                var idsToRemove = new List<string>();
                foreach (var oldElem in oldElements)
                {
                    if (oldElem is IElement)
                    {
                        var elem = oldElem as IElement;
                        idsToRemove.Add(elem.ElementId);
                        var match = allElementVms.FirstOrDefault(a => a.ElemId.Equals(elem.ElementId));
                        var matchIndex = allElementVms.IndexOf(match);
                        if (match != null && matchIndex >= 0)
                        {
                            allElementVms.RemoveAt(matchIndex);
                            //brute force reindex
                            for (int i = 0; i < allElementVms.Count; i++)
                            {
                                allElementVms[i].Index = i;
                            }
                        }
                    }
                }
            }
            else if (eventAction.Equals(NotifyCollectionChangedAction.Reset))
            {

            }
        }
    }
}