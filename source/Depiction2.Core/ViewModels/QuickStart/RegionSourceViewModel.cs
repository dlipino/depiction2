﻿using System.Windows.Input;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;

namespace Depiction2.Core.ViewModels.QuickStart
{
    public class RegionSourceViewModel : ViewModelBase
    {
        private DelegateCommand quickstartInfoCommand;
        private bool isSelected;
        public RegionDataSourceInformation QuickstartModel { get; private set; }
        public string Name { get { return QuickstartModel.DisplayName; } }
        public bool IsSelected { get { return isSelected; } set { isSelected = value; OnPropertyChanged("IsSelected"); } }

        public ICommand QuickstartInfoCommand
        {
            get
            {
                if (quickstartInfoCommand == null)
                {
                    quickstartInfoCommand = new DelegateCommand(DisplayQuickStartInfo);
                }
                return quickstartInfoCommand;
            }
        }
        #region constructor

        public RegionSourceViewModel(RegionDataSourceInformation sourceInfo)
        {
            QuickstartModel = sourceInfo;
        }


        #endregion
        private void DisplayQuickStartInfo()
        {
            var dialogMessage = QuickstartModel.Description;
            var title = string.Format("{0} information",QuickstartModel.DisplayName);
//            DepictionAccess.NotificationService.SendNotificationDialog(null, dialogMessage, title);
        }
    }
}