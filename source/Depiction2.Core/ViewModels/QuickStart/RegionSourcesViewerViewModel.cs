﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Webservice14;

namespace Depiction2.Core.ViewModels.QuickStart
{
    //AKA quickstart viewer view model
    public class RegionSourcesViewerViewModel : ViewModelBase
    {
        private int quickstartCount = 0;
        private ICartRect sourceRegion = null;
        private IDepictionRegion _selectedRegion = null;
        private IStory _storySource = null;
        private DelegateCommand acceptQuickstartData;
        private List<RegionSourceViewModel> sourceViewModels;

        public int QuickStartCount { get { return quickstartCount; } }
        public ObservableCollection<IDepictionRegion> DepictionRegions { get; set; }
        public IDepictionRegion SelectedRegion
        {
            get { return _selectedRegion; }
            set
            {
                _selectedRegion = value;
                if (value != null)
                {
                    UpdateQSStuffInBackground(_selectedRegion.GeoRectBounds);
                }
                OnPropertyChanged("SelectedRegion");
            }
        }
        public List<IRegionSourceExtension> QuickStartItems { get; set; }
        public List<IRegionSourceMetadata> ImageByAreaImporters { get; set; }
        public CollectionViewSource QuickstartViewSource { get; private set; }

        #region Commands
        //Used in add content dialog, kind of a hack
        public ICommand AcceptQuickstartDataCommand
        {
            get
            {
                if (acceptQuickstartData == null)
                {
                    acceptQuickstartData = new DelegateCommand(LoadSelectedQuickstartSources);
                }
                return acceptQuickstartData;
            }
        }

        #endregion

        #region constructor

        public RegionSourcesViewerViewModel(IStory resultSource)
        {
            if (resultSource == null) return;

            _storySource = resultSource;
            DepictionRegions = new ObservableCollection<IDepictionRegion>(resultSource.AllRegions);
            var region = resultSource.AllRegions.ToList();
            if (region.Any())
            {
                SelectedRegion = DepictionRegions.First();
            }
            _storySource.RegionCollectionChanged += StorySourceRegionCollectionChanged;


        }

        void StorySourceRegionCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            DepictionRegions.Clear();
            foreach (var region in _storySource.AllRegions)
            {
                DepictionRegions.Add(region);
            }
            if (SelectedRegion == null && DepictionRegions.Any())
            {
                SelectedRegion = DepictionRegions.First();
            }
        }

        protected override void OnDispose()
        {
            if (_storySource != null)
            {
                _storySource.RegionCollectionChanged -= StorySourceRegionCollectionChanged;
            }
            base.OnDispose();
        }
        #endregion

        #region usage methods
        public void LoadSelectedQuickstartSources()
        {
            if (sourceViewModels == null) return;
            if (SelectedRegion == null) return;
            foreach (var sourceVm in sourceViewModels)
            {
                if (!sourceVm.IsSelected) continue;
                var model = sourceVm.QuickstartModel;
                //the generic name is a legacy thing
                var importerExtension =
                    ExtensionService.Instance.RegionDataLoaders.FirstOrDefault(t => t.Metadata.GenericName.Equals(model.Name, StringComparison.CurrentCultureIgnoreCase));
                if (importerExtension == null) continue;
                SelectedRegion.AddDataSource(model);
                importerExtension.Value.ImportElements(SelectedRegion, model);
                sourceVm.IsSelected = false;
            }
        }

        public void SetSourceListView(List<RegionSourceViewModel> quickstartItems)
        {
            sourceViewModels = quickstartItems;
            QuickstartViewSource = new CollectionViewSource();
            QuickstartViewSource.Source = quickstartItems;
            QuickstartViewSource.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            QuickstartViewSource.View.Refresh();

            quickstartCount = quickstartItems.Count;
            OnPropertyChanged("QuickstartViewSource");
            OnPropertyChanged("QuickStartCount");
        }
        public IEnumerable<RegionDataSourceInformation> GetAvailableQuickstartItemsForRegion(ICartRect regionBounds)
        {
            var currentRegionCodes = WebServiceFacade.GetRegionCodesForRegion(regionBounds);
            var webServiceRegionServices = WebServiceFacade.GetPortalQuickstartSourcesByRegionCodes(currentRegionCodes);
            var loadedImporters = new List<RegionDataSourceInformation>();
            foreach (var meta in ExtensionService.Instance.RegionDataLoaders.Select(t => t.Metadata))
            {
                var sourceInfo = new RegionDataSourceInformation();
                sourceInfo.Name = meta.Name;
                if (!string.IsNullOrEmpty(meta.GenericName))
                {
                    sourceInfo.GenericName = meta.GenericName;
                }
                sourceInfo.Description = meta.Description;
                sourceInfo.ElementType = string.Empty;
                sourceInfo.DisplayName = meta.DisplayName;
                sourceInfo.ParameterDependent = meta.RequiresParameters;
                loadedImporters.Add(sourceInfo);
            }
            if (webServiceRegionServices == null)
            {
                return new List<RegionDataSourceInformation>();
            }
            var qsImporterNames = webServiceRegionServices.Select(ws => ws.GenericName).Distinct();
            var availableImporters = new HashSet<string>(loadedImporters.Select(li => li.GenericName), StringComparer.CurrentCultureIgnoreCase);
            //            var importerTypesqs14 = new List<string>
            //                                        {
            //                                            "NASAJPLImporter",
            //                                            "OpenStreetMapTileImporter",
            //                                            "OpenStreetMapRoadNetworkImporter",
            //                                            "NAIPSeamlessImporter",
            //                                            "WMSElementImporter",
            //                                            "WfsElementGatherer",
            //                                            "HistoricalTornadoGatherer1950",
            //                                            "HistoricalWildfires2002_2009",
            //                                            "WcsElementImporter",
            //                                            "HistoricalHurricaneTrackImporter"
            //                                        };
            var independentImporters = loadedImporters.Where(t => t.ParameterDependent == false);
            var usableRegionLoaders = new List<RegionDataSourceInformation>(independentImporters);
            //match the old qs sources to what is currently in depiction
            foreach (var service in webServiceRegionServices)
            {
                if (availableImporters.Contains(service.GenericName))
                {
                    usableRegionLoaders.Add(service);
                }
            }

            return usableRegionLoaders;
        }

        #endregion

        #region helpers for setting qs data in the background

        public void UpdateQSStuffInBackground(ICartRect regionOfInterest)
        {
            BackgroundWorker quickstartWorker = new BackgroundWorker();
            quickstartWorker.RunWorkerCompleted += quickstartWorker_RunWorkerCompleted;
            quickstartWorker.DoWork += quickstartWorker_DoWork;
            quickstartWorker.RunWorkerAsync(regionOfInterest);
        }

        private void quickstartWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var regionOfInterest = e.Argument as ICartRect;
            if (regionOfInterest == null) return;

            e.Result = GetAvailableQuickstartItemsForRegion(regionOfInterest);
        }

        private void quickstartWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var sourceMetas = e.Result as IEnumerable<RegionDataSourceInformation>;
            if (sourceMetas == null) return;
            var sourceVm = new List<RegionSourceViewModel>();
            var metaList = sourceMetas.ToList();
            DepictionAccess.HackRegionSourceList = metaList;
            foreach (var meta in metaList)
            {
                sourceVm.Add(new RegionSourceViewModel(meta));
            }
            SetSourceListView(sourceVm);

        }

        #endregion
    }
}