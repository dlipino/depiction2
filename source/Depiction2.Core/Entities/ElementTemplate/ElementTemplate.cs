﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.Entities.ElementTemplate
{
    [DataContract]
    public class ElementTemplate : IElementTemplate
    {
        //the sourceid is generally sent when loading the element from a dpn file.
        [DataMember]
        public string ElementType { get; private set; }
        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public bool HideFromLibrary { get; set; }
        [DataMember]
        public bool IsMouseAddable { get; set; }
        [DataMember]
        public bool ShowPropertiesOnCreate { get; set; }
        [DataMember]
        public string DefinitionSource { get; set; }
        [DataMember]
        public HashSet<string> HoverTextProps { get; set; }
        [DataMember]
        public HashSet<string> Tags { get; set; }

        [DataMember]
        public List<DefaultTemplateProperty> InfoProperties { get; set; }
        //for now leave this a mutable. 
        [DataMember]
        public List<IPropertyTemplate> Properties { get; set; }


        public string IconSource
        {
            get
            {
                return GetStringValueForProp("IconResourceName");
            }
        }

        public string ShapeType
        {
            get
            {
                var shapeProp = GetStringValueForProp("ZOIShapeType");
                if (shapeProp == null)
                {
                    return "Unknown";
                }
                return shapeProp;
            }
        }

        [DataMember]
        public Dictionary<string, string[]> OnCreateActions { get; set; }
        //hmmm legacy, not sure if it should stay
        [DataMember]
        public Dictionary<string, string[]> OnClickActions { get; set; }
        #region constructor

        protected ElementTemplate()
            : this("Generic")
        {

        }
        public ElementTemplate(string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                ElementType = "Generic";
            }
            else
            {

                ElementType = type;
            }
            IsMouseAddable = true;
            HideFromLibrary = false;
            ShowPropertiesOnCreate = false;
            Properties = new List<IPropertyTemplate>();
            InfoProperties = new List<DefaultTemplateProperty>();
            OnCreateActions = new Dictionary<string, string[]>();
            OnClickActions = new Dictionary<string, string[]>();
            Tags = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);//StringComparer.OrdinalIgnoreCase);
            HoverTextProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);//StringComparer.OrdinalIgnoreCase);
        }
        #endregion

        #region helpes
        public string GetStringValueForProp(string propName)
        {
            var prop =
                InfoProperties.FirstOrDefault(
                    t => t.ProperName.Equals(propName, StringComparison.InvariantCultureIgnoreCase));
            if (prop == null)
            {
                return "Uknown";
            }
            return prop.ValueString;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("{0}, {1}", ElementType, DisplayName);
        }
    }


}