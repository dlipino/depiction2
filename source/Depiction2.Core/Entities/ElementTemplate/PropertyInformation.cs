﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.StoryEntities.Element;

namespace Depiction2.Core.Entities.ElementTemplate
{
    public class PropertyInformation
    {
        private static Dictionary<string, string> visualPropertyNames = null;
        private static Dictionary<string, string> informationPropertyNames = null;
        private static Dictionary<string, string> mapControlPropertyNames = null;
        private static Dictionary<string, string> templatePropertyNames = null;

        private static Dictionary<string, string> propertyDefaultValues = null;
        private static HashSet<string> clrProps = null;
        private static HashSet<string> boolProps = null;
        static private bool propsSet = false;

        #region properties

        public static Dictionary<string, string> VisualProps
        {
            get
            {
                if (!propsSet) SetPropertyLists();
                return visualPropertyNames;
            }
        }
        public static Dictionary<string, string> InformationProps
        {
            get
            {
                if (!propsSet) SetPropertyLists();
                return informationPropertyNames;
            }
        }
        public static Dictionary<string, string> MapControlProps
        {
            get
            {
                if (!propsSet) SetPropertyLists();
                return mapControlPropertyNames;
            }
        }
        public static Dictionary<string, string> TemplateProps
        {
            get
            {
                if (!propsSet) SetPropertyLists();
                return templatePropertyNames;
            }
        }
        public static Dictionary<string, string> DefaultPropStringValues
        {
            get
            {
                if (propertyDefaultValues == null) CreateCLRPropertyDefaults();
                return propertyDefaultValues;
            }
        }
        public static HashSet<string> BoolProps
        {
            get
            {
                if (!propsSet) CreateCLRPropertyDefaults();
                return boolProps;
            }
        }
        #endregion
        public PropertyInformation()
        {
            SetPropertyLists();
            CreateCLRPropertyDefaults();
        }
        private static void SetPropertyLists()
        {
            informationPropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            informationPropertyNames.Add(PropNames.DisplayName, "Display Name");
            informationPropertyNames.Add(PropNames.Author, "Author");
            informationPropertyNames.Add(PropNames.Description, "Description");

            mapControlPropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            mapControlPropertyNames.Add(PropNames.IsDraggable, "Draggable");
            mapControlPropertyNames.Add(PropNames.IsGeometryEditable, "Editable geometry");
            mapControlPropertyNames.Add(PropNames.GeoLocation, "GeoLocation");

            visualPropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            visualPropertyNames.Add(PropNames.ZOIFillColor, "ZOI Fill Color");
            visualPropertyNames.Add(PropNames.ZOIBorderColor, "ZOI Border Color");
            visualPropertyNames.Add(PropNames.ZOILineThickness, "ZOI Line Size");
            visualPropertyNames.Add(PropNames.ZOIShapeType, "ZOI Shape Type");
            visualPropertyNames.Add(PropNames.IconResourceName, "Icon resource name");
            visualPropertyNames.Add(PropNames.IconSize, "Icon Size");
            visualPropertyNames.Add(PropNames.IconBorderColor, "Icon Border Color");
            visualPropertyNames.Add(PropNames.IconBorderShape, "Icon Border Shape");
            visualPropertyNames.Add(PropNames.VisualState, "Visual State");
            
            templatePropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            templatePropertyNames.Add(PropNames.IsMouseAddable, "Mouse addable");
            templatePropertyNames.Add(PropNames.DefinitionSource, "Definition Source");
            templatePropertyNames.Add(PropNames.HideFromLibrary, "Hide from add library");
            templatePropertyNames.Add(PropNames.ShowPropertiesOnCreate, "Show properties on create");

            boolProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            boolProps.Add(PropNames.IsMouseAddable);
            boolProps.Add(PropNames.HideFromLibrary);
            boolProps.Add(PropNames.ShowPropertiesOnCreate);
            boolProps.Add(PropNames.IsDraggable);
            boolProps.Add(PropNames.IsGeometryEditable);

            clrProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var key in visualPropertyNames.Values)
            {
                clrProps.Add(key);
            }
            foreach (var key in informationPropertyNames.Values)
            {
                clrProps.Add(key);
            }
            foreach (var key in mapControlPropertyNames.Values)
            {
                clrProps.Add(key);
            }
            foreach (var key in templatePropertyNames.Values)
            {
                clrProps.Add(key);
            }

            propsSet = true;
        }

        private static void CreateCLRPropertyDefaults()
        {
            propertyDefaultValues = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            propertyDefaultValues.Add(PropNames.ZOIBorderColor, Brushes.Black.ToString());
            propertyDefaultValues.Add(PropNames.ZOIFillColor, Brushes.Red.ToString());
            propertyDefaultValues.Add(PropNames.ZOILineThickness, 1.ToString());
            propertyDefaultValues.Add(PropNames.ZOIShapeType, ZOIShapeType.Point.ToString());
            propertyDefaultValues.Add(PropNames.IconBorderShape, DepictionIconBorderShape.None.ToString());
            propertyDefaultValues.Add(PropNames.IconBorderColor, Brushes.Orange.ToString());
            propertyDefaultValues.Add(PropNames.IconResourceName, "Unknown");
            propertyDefaultValues.Add(PropNames.IconSize, ((double)DepictionIconSize.Medium).ToString());
            propertyDefaultValues.Add(PropNames.VisualState, ElementVisualSetting.Icon.ToString());
            propertyDefaultValues.Add(PropNames.DisplayName, string.Empty);
            propertyDefaultValues.Add(PropNames.Author, string.Empty);
            propertyDefaultValues.Add(PropNames.Description, string.Empty);
            propertyDefaultValues.Add(PropNames.IsMouseAddable, true.ToString());
            propertyDefaultValues.Add(PropNames.DefinitionSource, "Default");
            propertyDefaultValues.Add(PropNames.HideFromLibrary, false.ToString());
            propertyDefaultValues.Add(PropNames.ShowPropertiesOnCreate, false.ToString());
            propertyDefaultValues.Add(PropNames.IsDraggable, true.ToString());
            propertyDefaultValues.Add(PropNames.IsGeometryEditable, true.ToString());
        }
    }
}