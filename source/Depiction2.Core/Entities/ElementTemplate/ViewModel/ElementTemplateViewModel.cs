﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.Core.Entities.ElementTemplate.ViewModel
{
    public class ElementTemplateViewModel : ViewModelBase
    {
        private IElementTemplate _templateSource;
        private string _elementTemplateName;
        private string _iconResourceName;
        public string ElementTemplateName
        {
            get { return _elementTemplateName; }
            set { _elementTemplateName = value; OnPropertyChanged("ElementTemplateName"); }
        }

        public string IconResourceName
        {
            get { return _iconResourceName; }
            set { _iconResourceName = value; OnPropertyChanged("IconResourceName"); }
        }
        public ObservableCollection<PropertyTemplateViewModel> InformationProperties { get; set; }
        public ObservableCollection<PropertyTemplateViewModel> DisplayProperties { get; set; }
        public ObservableCollection<PropertyTemplateViewModel> VisualProperties { get; set; }

        public ObservableCollection<PropertyTemplateViewModel> ElementUserProperties { get; set; }

        #region constuctor
        public ElementTemplateViewModel(IElementTemplate templateSource)
        {
            if (templateSource == null)
            {
                _templateSource = new ElementTemplate("Generic");
            }
            else
            {
                _templateSource = templateSource;
                ElementTemplateName = templateSource.ElementType;
            }
            CreateInformationPropertyTemplates(templateSource);
            CreateVisualPropertyTemplates(templateSource);
            CreateDefaultPropertyTemplates(templateSource);

            ElementUserProperties = new ObservableCollection<PropertyTemplateViewModel>();
            if (templateSource != null)
            {
                foreach (var inProp in templateSource.Properties)
                {
                    var propertyType = DepictionAccess.GetTypeFromName(inProp.TypeString);
                    var hoverText = templateSource.HoverTextProps.Contains(inProp.ProperName);
                    var valueString = inProp.ValueString;
                    if (propertyType == null)
                    {
                        var prop = new PropertyTemplateViewModel(inProp.ProperName, inProp.DisplayName, valueString){UseAsHoverText = hoverText};
                        ElementUserProperties.Add(prop);
                    }
                    else
                    {
                        var convValue = DepictionTypeConverter.ChangeType(valueString, propertyType);
                        var prop = new PropertyTemplateViewModel(inProp.ProperName, inProp.DisplayName, convValue) { UseAsHoverText = hoverText };
                        ElementUserProperties.Add(prop);
                    }
                }
            }
        }
        #endregion
        #region helpers
        private void CreateInformationPropertyTemplates(IElementTemplate baseTemplate)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in PropertyInformation.TemplateProps)
            {
                var sval = PropertyInformation.DefaultPropStringValues[item.Key];
                var hoverText = baseTemplate.HoverTextProps.Contains(item.Key);
                var defaultProp = baseTemplate.InfoProperties.FirstOrDefault(t => t.ProperName.Equals(item.Key, StringComparison.CurrentCultureIgnoreCase));
                if (defaultProp != null)
                {
                    sval = defaultProp.ValueString;
                }
                propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, sval) { UseAsHoverText = hoverText });
            }
            foreach (var item in PropertyInformation.InformationProps)
            {
                var sval = PropertyInformation.DefaultPropStringValues[item.Key];
                var hoverText = baseTemplate.HoverTextProps.Contains(item.Key);
                var defaultProp = baseTemplate.InfoProperties.FirstOrDefault(t => t.ProperName.Equals(item.Key, StringComparison.CurrentCultureIgnoreCase));
                if (item.Key.Equals(PropNames.DisplayName, StringComparison.InvariantCultureIgnoreCase))
                {
                    sval = baseTemplate.DisplayName;
                }
                if (defaultProp != null)
                {
                    sval = defaultProp.ValueString;
                }
                propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, sval) { UseAsHoverText = hoverText });

            }

            InformationProperties = new ObservableCollection<PropertyTemplateViewModel>(propHash);
        }
        private void CreateVisualPropertyTemplates(IElementTemplate baseTemplate)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in PropertyInformation.VisualProps)
            {
                var sval = PropertyInformation.DefaultPropStringValues[item.Key];
                var hoverText = baseTemplate.HoverTextProps.Contains(item.Key);
                var defaultProp = baseTemplate.InfoProperties.FirstOrDefault(t => t.ProperName.Equals(item.Key, StringComparison.CurrentCultureIgnoreCase));
                if (defaultProp != null)
                {
                    sval = defaultProp.ValueString;
                }
                propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, sval) { UseAsHoverText = hoverText });

            }
            VisualProperties = new ObservableCollection<PropertyTemplateViewModel>(propHash);

        }

        private void CreateDefaultPropertyTemplates(IElementTemplate baseTemplate)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());

            foreach (var item in PropertyInformation.MapControlProps)
            {
                if (PropertyInformation.DefaultPropStringValues.ContainsKey(item.Key))
                {
                    var sval = PropertyInformation.DefaultPropStringValues[item.Key];
                    var hoverText = baseTemplate.HoverTextProps.Contains(item.Key);
                    var defaultProp = baseTemplate.InfoProperties.FirstOrDefault(t => t.ProperName.Equals(item.Key, StringComparison.CurrentCultureIgnoreCase));
                    if (defaultProp != null)
                    {
                        sval = defaultProp.ValueString;
                    }
                    propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, sval) { UseAsHoverText = hoverText });
                }
            }

            DisplayProperties = new ObservableCollection<PropertyTemplateViewModel>(propHash);
        }
        #endregion
        #region add property stuff
        public string NewPropName { get; set; }
        public Type SelectedType { get; set; }
        public Dictionary<string, Type> PropertyTypes
        {
            get
            {
                return DepictionAccess.NameTypeService.NameTypeDictonary;
            }
        }
        private DelegateCommand addPropertyCommand;
        public ICommand AddPropertyCommand
        {
            get
            {
                if (addPropertyCommand == null)
                {
                    addPropertyCommand = new DelegateCommand(AddPropertyToElement);
                }
                return addPropertyCommand;
            }
        }

        public void AddPropertyToElement()
        {
            var propScaffold = new PropertyTemplateViewModel(NewPropName, NewPropName, SelectedType);
            if (ElementUserProperties.Any(t => t.ProperName.Equals(NewPropName))) return;
            ElementUserProperties.Add(propScaffold);
        }
        #endregion

        #region helpers

        public void ApplyAllPropertyChanges()
        {
            foreach (var prop in InformationProperties)
            {
                prop.ApplyViewModelChangeToOrig();
            }
            foreach (var prop in ElementUserProperties)
            {
                prop.ApplyViewModelChangeToOrig();
            }
            foreach (var prop in VisualProperties)
            {
                prop.ApplyViewModelChangeToOrig();
            }
        }

        public IElementTemplate GetElementScaffold()
        {
            var scaffold = new ElementTemplate(ElementTemplateName);
            foreach (var propVm in InformationProperties)
            {
                scaffold.Properties.Add(propVm.CreatePropertyScaffold());
            }
            foreach (var propVm in ElementUserProperties)
            {
                scaffold.Properties.Add(propVm.CreatePropertyScaffold());
            }
            foreach (var propVm in VisualProperties)
            {
                scaffold.Properties.Add(propVm.CreatePropertyScaffold());
            }
            return scaffold;
        }

        #endregion
    }
}