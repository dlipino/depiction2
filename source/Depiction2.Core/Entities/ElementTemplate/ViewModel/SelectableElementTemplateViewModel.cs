﻿using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.Entities.ElementTemplate.ViewModel
{
    public class SelectableElementTemplateViewModel : ViewModelBase
    {
        private IElementTemplate _templateModel;
        private bool _selected;
        public bool Selected { get { return _selected; } set { _selected = value; OnPropertyChanged("Selected"); } }


        public string IconReferenceName { get { return _templateModel.IconSource; } }
        public string DisplayName { get { return _templateModel.DisplayName; } }


        public SelectableElementTemplateViewModel(IElementTemplate model)
        {
            _templateModel = model;
        }
    }
}