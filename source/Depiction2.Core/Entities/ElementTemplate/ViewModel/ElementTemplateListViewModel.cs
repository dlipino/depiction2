﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.Entities.ElementTemplate.ViewModel
{
    public class ElementTemplateListViewModel : ViewModelBase
    {
        #region variables
        private IElementTemplate _templateModel;
        private bool _selected;
        #endregion

        #region property
        public bool Selected { get { return _selected; } set { _selected = value; OnPropertyChanged("Selected"); } }

        public string DisplayName { get { return _templateModel.DisplayName; } }
        public int SortPriority { get; private set; }
        public string DefinitionSource { get; set; }
        public string Description { get; private set; }
        public string IconSource { get { return _templateModel.IconSource; } }
        public IElementTemplate TemplateModel { get { return _templateModel; } }
        public IEnumerable<string> Tags { get
        {
            if(_templateModel.Tags.Any())
            {
                return _templateModel.Tags.ToList();
            }
            return new List<string> {"No Tag"};
        } }
        #endregion

        #region constructor
        public ElementTemplateListViewModel(IElementTemplate template)
        {
            _templateModel = template;
            SortPriority = 0;
            if(string.IsNullOrEmpty(template.DefinitionSource))
            {
                DefinitionSource = "General";
            }else
            {
                DefinitionSource = _templateModel.DefinitionSource;
            }

            var descriptionProp = _templateModel.InfoProperties.FirstOrDefault(t => t.ProperName.Equals("Description"));
            if(descriptionProp !=null)
            {
                Description = descriptionProp.ValueString;
            }

        }

        protected override void OnDispose()
        {
            base.OnDispose();
        }
        #endregion
    }
}