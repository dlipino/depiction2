﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Events;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.Base.Utilities;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Element;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Core.Entities.Element
{
    [DataContract]
    public class DepictionElement : ModelBase, IElement
    {
        public event EventHandler<GeoLocationEventArgs> GeoLocationChange;
        public event Action<IElementProperty> PropertyRemoved;
        public event Action<IElementProperty> PropertyAdded;

        static private PropertyNameComparerIgnoreCase<IElementProperty> propComparer = new PropertyNameComparerIgnoreCase<IElementProperty>();
        #region variables

        private IDepictionGeometry _elementGeometry;

        private bool _usePropertyNamesInInformationText;
        private bool _displayInformationText;
        private bool _useEnhancedInformationText;
        LinkedList<IElement> associatedElementList = new LinkedList<IElement>();
        //this point is only important when there are actions associated to the location
        //and a geometry gets created because of those actions.
        private Point? primaryGeoLocationXY = null;
        private Point? boundsGeoCentroid = null;
        private ICartRect _geoGeoBounds = new CartRect();//Rect.Empty;
        //        private HashSet<string> tags = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
        private HashSet<string> hoverTextPropNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);

        private string _imageResourceName = string.Empty;
        private double geoImageRotation = 0;

        private Dictionary<string, string[]> postAddActions = new Dictionary<string, string[]>();
        private Dictionary<string, string[]> onClickActions = new Dictionary<string, string[]>();
        #endregion
        #region properties

        public int NumberIncomingEdgesFired { get; set; }
        #region Element base properties

        [DataMember]
        public string ElementId { get; private set; }

        [DataMember, DefaultValue("DefaultElement")]
        public string ElementType { get; private set; }

        [DataMember, DefaultValue("")]
        public string DisplayName { get; set; }
        [DataMember, DefaultValue("")]
        public string Author { get; set; }
        [DataMember, DefaultValue("")]
        public string Description { get; set; }

        #endregion

        #region Image
        [DataMember, DefaultValue("")]
        public string ImageResourceName
        {
            get { return _imageResourceName; }
            set
            {
                _imageResourceName = value;
                OnPropertyChanged("ImageResourceName");
            }
        }
        [DataMember, DefaultValue(0d)]
        public double ImageRotation
        {
            get { return geoImageRotation; }
            set
            {
                geoImageRotation = value;
                OnPropertyChanged("GeoImageRotation");
            }
        }
        #endregion

        #region geo element properties

        public double SimpleBoundsArea { get; private set; }
        public ICartRect SimpleBounds
        {
            get { return _geoGeoBounds; }
            private set
            {
                _geoGeoBounds = value;
                if(_geoGeoBounds != null)
                {
                    SimpleBoundsArea = _geoGeoBounds.Area;
                    boundsGeoCentroid = new Point(_geoGeoBounds.Width / 2 + _geoGeoBounds.Left, _geoGeoBounds.Height / 2 + _geoGeoBounds.Bottom);
                }else
                {
                    boundsGeoCentroid = null;
                }
            }
        }

        [DataMember, DefaultValue(true)]
        public bool IsDraggable { get; set; }
        [DataMember]
        public Point? GeoLocation
        {
            get
            {
                if (primaryGeoLocationXY != null) return primaryGeoLocationXY;
                return GeometryCentroid;
            }
            private set { primaryGeoLocationXY = value; }
        }

        public Point? GeometryCentroid
        {
            get
            {
                if (boundsGeoCentroid != null && !SimpleBounds.IsEmpty)
                {
                    boundsGeoCentroid = new Point(SimpleBounds.Width / 2 + SimpleBounds.Left, SimpleBounds.Height / 2 + SimpleBounds.Bottom);
                }
                return boundsGeoCentroid;
            }
        }
        [DataMember, DefaultValue(false)]
        public bool IsGeometryEditable { get; set; }
        //There are issues with geometry
        [DataMember]
        private string StringGeometry
        {
            get
            {
                return ElementGeometry == null ? string.Empty : ElementGeometry.WktGeometry;
            }
            set { SetGeometryFromWtkString(value); }
        }
        public IDepictionGeometry ElementGeometry
        {
            get { return _elementGeometry; }
            set
            {
                _elementGeometry = value;
                if (value == null)
                {
                    SimpleBounds = null;
                    return;
                }

                SimpleBounds = value.RectBounds;
                OnPropertyChanged("ElementGeometry");
            }
        }

        #endregion

        #region ZOi things

        [DataMember, DefaultValue(typeof(ElementVisualSetting), "None")]
        public ElementVisualSetting VisualState { get; set; }

        [DataMember, DefaultValue("Black")]
        public string ZOIFillColor { get; set; }

        [DataMember, DefaultValue("Blue")]
        public string ZOIBorderColor { get; set; }

        [DataMember, DefaultValue(1d)]
        public double ZOILineThickness { get; set; }

        [DataMember, DefaultValue("")]
        public string ZOIShapeType { get; set; }

        [DataMember, DefaultValue("None")]
        public string IconBorderShape { get; set; }

        [DataMember, DefaultValue("Green")]
        public string IconBorderColor { get; set; }

        [DataMember, DefaultValue(24d)]
        public double IconSize { get; set; }

        [DataMember, DefaultValue("Depiction.Default")]
        public string IconResourceName { get; set; }

        #endregion

        #region information/permatext region
        [DataMember]
        public bool DisplayInformationText
        {
            get { return _displayInformationText; }
            set
            {
                _displayInformationText = value;
                if (_displayInformationText)
                {
                    if (PermaText == null)
                    {
                        PermaText = new DepictionPermaText { IsEnhancedPermaText = UseEnhancedInformationText };
                    }
                    else
                    {
                        PermaText.IsVisible = true;
                    }
                }
                OnPropertyChanged("DisplayInformationText");
            }
        }
        [DataMember]
        public bool UsePropertyNamesInInformationText
        {
            get { return _usePropertyNamesInInformationText; }
            set { _usePropertyNamesInInformationText = value; OnPropertyChanged("UsePropertyNamesInInformationText"); }
        }

        [DataMember]
        public bool UseEnhancedInformationText
        {
            get { return _useEnhancedInformationText; }
            set
            {
                _useEnhancedInformationText = value;
                if (PermaText != null)
                {
                    PermaText.IsEnhancedPermaText = true;
                }
                OnPropertyChanged("UseEnhancedInformationText");
            }
        }

        public IPermaText PermaText { get; set; }
        #endregion
        #region user element properties

        public List<RestoreProperty> RestoreProps { get; set; }
        [DataMember]
        internal HashSet<IElementProperty> DefaultProperties { get; set; }
        public ReadOnlyCollection<Tuple<string, string>> DefaultElementPropertyNames
        {
            get
            {
                var productInfos =
                    from p in DefaultProperties
                    select new Tuple<string, string>(p.ProperName, p.DisplayName);
                var list = productInfos.ToList();
                return new ReadOnlyCollection<Tuple<string, string>>(list);
            }
        }
        [DataMember]
        internal HashSet<IElementProperty> AdditionalProperties { get; set; }
        public ReadOnlyCollection<Tuple<string, string>> AdditionalPropertyNames
        {
            get
            {
                var productInfos =
                    from p in AdditionalProperties
                    select new Tuple<string, string>(p.ProperName, p.DisplayName);
                var list = productInfos.ToList();
                return new ReadOnlyCollection<Tuple<string, string>>(list);
            }
        }
        #endregion
        #region element extras
        [DataMember]
        public Dictionary<string, string[]> PostAddActions
        {
            get { return postAddActions; }
            set { postAddActions = value; }
        }
        [DataMember]
        public Dictionary<string, string[]> OnClickActions
        {
            get { return onClickActions; }
            set { onClickActions = value; }
        }
        [DataMember]
        private HashSet<string> HoverTextPropNames { get { return hoverTextPropNames; } set { hoverTextPropNames = value; } }

        public IEnumerable<string> HoverTextProperties { get { return HoverTextPropNames; } }
        //TODO: why not us an enumerable
        public ReadOnlyCollection<string> Tags { get { return new ReadOnlyCollection<string>(TagSet.ToList()); } }
        [DataMember]
        private HashSet<string> TagSet { get; set; }
        [DataMember]
        public IList<string> AssociatedElementIds { get; private set; }//for saving
        public bool HasAssociatedElements { get { return AssociatedElementIds.Any(); } }
        public IEnumerable<IElement> AssociatedElements { get { return associatedElementList; } }

        #endregion
        #endregion
        #region Constructor

        public DepictionElement()
            : this("", "Generic")
        {
        }

        public DepictionElement(string type)
            : this("", type)
        {
        }

        public DepictionElement(string id, string type)
        {
            ElementId = !string.IsNullOrEmpty(id) ? id : Guid.NewGuid().ToString();
            if (!string.IsNullOrEmpty(type))
            {
                DisplayName = ElementType = type;
            }
            else
            {
                DisplayName = ElementType = "DefaultElement";
            }
            DefaultProperties = new HashSet<IElementProperty>(propComparer);
            AdditionalProperties = new HashSet<IElementProperty>(propComparer);
            HoverTextPropNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            TagSet = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            AssociatedElementIds = new List<string>();
            RestoreProps = new List<RestoreProperty>();
            VisualState = ElementVisualSetting.None;
            Author = "";
            Description = "";
            IsGeometryEditable = false;
            IsDraggable = true;
            ZOIFillColor = "Black";
            ZOIBorderColor = "Blue";
            ZOILineThickness = 1d;
            ZOIShapeType = "";
            IconBorderShape = "None";
            IconBorderColor = "Green";
            IconSize = 24d;
            IconResourceName = "Depiction.Default";
        }

        protected override void OnDispose()
        {
            _elementGeometry.Dispose();
            base.OnDispose();
        }
        #endregion

        #region location and geometry setters/helpers. they are really messy

        public bool SetGeometryFromWtkString(string geometryString)
        {
            if (!DepictionGeometryService.Activated) return false;
            try
            {
                ElementGeometry = DepictionGeometryService.CreateGeomtryFromWkt(geometryString);
            }
            catch (Exception ex) { }
            return ElementGeometry != null;
        }

        public void SetGeoLocationFromString(string positionStringLatLong)
        {
            //the goe location string comes in latlong order, YX
            double[] latlong = new double[] { double.NaN, double.NaN };
            try
            {
                var outPoint = Point.Parse(positionStringLatLong);
                latlong[0] = outPoint.X;
                latlong[1] = outPoint.Y;
            }
            catch
            {
                if (latlong[0].Equals(double.NaN) || latlong[1].Equals(double.NaN))
                {  //wow dot spatial fails on big numbers
                    //try the dot spatial, i suppose we could try the depiction one too
                    string message;
                    LatLongParserUtility.ParseForLatLong(positionStringLatLong, out latlong[0], out latlong[1], out message);
                    //                    DotNetLatLongParser.ParseLatLongString(positionStringLatLong, out latlong[0], out latlong[1]);
                }
            }

            primaryGeoLocationXY = null;
            if (!latlong[0].Equals(double.NaN) && !latlong[1].Equals(double.NaN))
            {
                primaryGeoLocationXY = new Point(latlong[1], latlong[0]);
            }
            else { primaryGeoLocationXY = null; }
        }

        public void MatchLocationAndGeometry()
        {
            if (!DepictionGeometryService.Activated) return;
            if (ElementGeometry == null)
            {
                ElementGeometry = DepictionGeometryService.CreateGeometry();//new GeometryGdalWrap();
            }
            if (primaryGeoLocationXY != null && ElementGeometry.Bounds.Equals(Rect.Empty))
            {
                ElementGeometry = DepictionGeometryService.CreatePointGeometry((Point)primaryGeoLocationXY);//new GeometryGdalWrap((Point)primaryGeoLocationXY);
            }
        }
        internal void SetInitialLocationAndGeometry(Point? initialLocation, IDepictionGeometry initialGeometry)
        {
            primaryGeoLocationXY = initialLocation;
            ElementGeometry = initialGeometry;
            if (initialLocation == null && initialGeometry != null)
            {
                primaryGeoLocationXY = initialGeometry.Bounds.GetCenter();
            }
            if (initialGeometry == null)
            {
                ElementGeometry = DepictionGeometryService.CreateGeometry();
            }
        }
        //why would this ever be used? seems like it is just used to confuse
        public void UpdatePrimaryPointAndGeometry(Point? newPrimaryLocation, IDepictionGeometry updateGeometry)
        {
            var old = primaryGeoLocationXY;
            primaryGeoLocationXY = newPrimaryLocation;
            if (updateGeometry == null) { MatchLocationAndGeometry(); }
            else
            {
                //if the geometry type changes, adjust what is getting visuals get drawn.
                //specifically if it goes from point to polygon
                if (ElementGeometry == null || !updateGeometry.GeometryType.Equals(ElementGeometry.GeometryType))
                {
                    switch (updateGeometry.GeometryType)
                    {
                        case DepictionGeometryType.Polygon:
                        case DepictionGeometryType.MultiPolygon:
                        case DepictionGeometryType.LineString:
                        case DepictionGeometryType.MultiLineString:
                            VisualState = VisualState | ElementVisualSetting.Geometry;
                            break;
                    }
                }
                ElementGeometry = updateGeometry;
            }

            NotifyGeoChange(old, primaryGeoLocationXY, true);
        }
        public void GeoShiftPrimaryGeoLocationAndShiftGeometry(Vector geoShift)
        {
            //odd things will happen when there are post set actions involved
            //but for now ignore the bad man
            var old = primaryGeoLocationXY;
            if (primaryGeoLocationXY != null)
            {
                primaryGeoLocationXY = primaryGeoLocationXY - geoShift;
            }
            ElementGeometry.ShiftGeometry((Point)geoShift);
            ElementGeometry = ElementGeometry;
            NotifyGeoChange(old, primaryGeoLocationXY, true);
        }

        private void UpdatePrimaryGeoLocationAndShiftGeometry(Point? newPrimaryLocation, bool notifyAssociates)
        {
            if (GeoLocation == null && _elementGeometry == null)
            {
                //Does something clever which might involve doing some post set actions
                //because this puppy has no center or geometry
            }
            else if (GeoLocation == null && _elementGeometry != null)
            {

            }
            else if (GeoLocation != null && _elementGeometry != null)
            {
                if (newPrimaryLocation != null)
                {
                    //this is the easy case since the zoi exists and depends on the
                    //point inputted by the user
                    var pN = (Point)newPrimaryLocation;
                    var p0 = (Point)GeoLocation;
                    var old = GeoLocation;
                    var shift = (Point)(p0 - pN);
                    ElementGeometry.ShiftGeometry(shift);
                    ElementGeometry = ElementGeometry;

                    primaryGeoLocationXY = newPrimaryLocation;

                    NotifyGeoChange(old, primaryGeoLocationXY, notifyAssociates);
                }
            }
        }

        public void UpdatePrimaryGeoLocationAndShiftGeometry(Point? newPrimaryLocation)
        {
            UpdatePrimaryGeoLocationAndShiftGeometry(newPrimaryLocation, true);
        }
        public void UpdatePrimaryPointWithoutAssociatedEvents(Point newPrimaryLocation)
        {
            //This could go horrible wrong if the associated element happens to be an element
            //that changes shape based on location, is a route waypoint, and affects the roadnetwork
            //with its area.
            UpdatePrimaryGeoLocationAndShiftGeometry(newPrimaryLocation, false);
        }
        #endregion
        #region property control methods
        internal void AddElementDefaultProperty(IElementProperty baseProp)
        {
            if (baseProp == null) return;
            if (DefaultProperties.Contains(baseProp)) return;
            DefaultProperties.Add(baseProp);
            baseProp.PropertyChanged += newProp_PropertyChanged;
        }


        public void AddUserProperty(IElementProperty newProp)
        {
            if (newProp == null) return;
            if (AdditionalProperties.Contains(newProp)) return;

            AdditionalProperties.Add(newProp);
            newProp.PropertyChanged += newProp_PropertyChanged;
            if (PropertyAdded != null)
            {
                PropertyAdded(newProp);
            }
        }
        // must clone the value, in case the value gets used multiple times
        public void AddUserProperty(string properName, string displayName, object value)
        {
            if (value == null) return;
            if (string.IsNullOrEmpty(properName)) return;
            if (string.IsNullOrEmpty(displayName))
            {
                displayName = properName;
            }
            var prop = new ElementProperty(properName, displayName,
                                               DepictionAccess.GetDeepCopyOfPropertyValue(value));
            AddUserProperty(prop);
        }
        public void DeleteUserProperty(string propertyName)
        {
            var prop =
               AdditionalProperties.FirstOrDefault(
                   t => t.ProperName.Equals(propertyName, StringComparison.CurrentCultureIgnoreCase));
            if (prop == null) return;
            AdditionalProperties.Remove(prop);
            RemoveHoverTextProperty(prop.ProperName, true);
            prop.PropertyChanged -= newProp_PropertyChanged;
            if (PropertyRemoved != null)
            {
                PropertyRemoved(prop);
            }
        }
        public void DeleteUserProperty(IElementProperty propToDelete)
        {
            if (propToDelete == null) return;
            DeleteUserProperty(propToDelete.ProperName);

        }
        //        public bool UpdatePropertyValue(string propertyName, object value, bool triggerChange)
        //        {
        //            var target = GetDepictionProperty(propertyName);
        //            if(target != null)
        //            {
        //                target.SetValue(value);
        //                return true;
        //            }
        //            return false;
        //        }
        public void AdjustElementWithPropertyHack(IElementProperty updateProp)
        {
            if (updateProp == null) return;
            if (DefaultProperties.Contains(updateProp) || AdditionalProperties.Contains(updateProp))
            {
                var target = GetDepictionProperty(updateProp.ProperName);
                target.SetValue(updateProp.Value);

            }
            else
            {
                AdditionalProperties.Add(updateProp);
                updateProp.PropertyChanged += newProp_PropertyChanged;
            }
        }

        public void TriggerPropertyChange(string propName)
        {
            OnPropertyChanged(propName);
        }
        void newProp_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var prop = sender as IElementProperty;
            if (prop == null) return;
            if (prop.LegacyPostSetActions != null)
            {
                foreach (var action in prop.LegacyPostSetActions)
                {
                    var key = action.Key;
                    var behaviours =
                        ExtensionService.Instance.DepictionBehaviors.Where(t => t.Metadata.Name.Equals(key)).ToList();
                    if (behaviours.Any())
                    {
                        foreach (var behavior in behaviours)
                        {
                            if (behavior != null)
                            {
                                var behaviorParams = LegacyHelpers.BuildParametersForPostSetActions(action.Value);
                                behavior.Value.DoBehavior(this, behaviorParams);
                            }
                        }
                    }
                }
            }
            //not sure if order is important
            OnPropertyChanged(e.PropertyName);
        }

        public IElementProperty GetDepictionProperty(string properName)
        {
            var prop = AdditionalProperties.FirstOrDefault(t => t.ProperName.Equals(properName, StringComparison.InvariantCultureIgnoreCase));
            if (prop == null) prop = DefaultProperties.FirstOrDefault(t => t.ProperName.Equals(properName, StringComparison.InvariantCultureIgnoreCase));
            if (prop != null) return prop;
            return null;
        }

        public bool HasProperty(string propName)
        {
            return true;
        }

        public List<IElementProperty> GetMatchingProps(Func<IElementProperty, bool> funct)
        {
            var props = from p in DefaultProperties where funct(p) select p;// select p.Value;
            var additional = from p in AdditionalProperties where funct(p) select p;
            return props.Concat(additional).ToList();
        }


        public bool AddHoverTextProperty(string name, bool triggerEvent)
        {
            if (hoverTextPropNames.Add(name))
            {
                if (triggerEvent) OnPropertyChanged("HoverText");
                return true;
            }
            return false;
        }
        public bool RemoveHoverTextProperty(string name, bool triggerEvent)
        {
            if (hoverTextPropNames.Remove(name))
            {
                if (triggerEvent) OnPropertyChanged("HoverText");
                return true;
            }
            return false;
        }

        #endregion
        #region event helpers
        private void NotifyGeoChange(Point? oldLoc, Point? newLoc, bool notifyAssociates)//GeoLocationEventArgs eventArgs)
        {
            LatitudeLongitudeSimple oldGeo = null;
            LatitudeLongitudeSimple newGeo = null;
            if (oldLoc != null)
            {
                oldGeo = new LatitudeLongitudeSimple(((Point)oldLoc).Y, ((Point)oldLoc).X);
            }
            if (newLoc != null)
            {
                newGeo = new LatitudeLongitudeSimple(((Point)newLoc).Y, ((Point)newLoc).X);
            }
            var eventArgs = new GeoLocationEventArgs(oldGeo, newGeo);
            //            eventArgs.TriggerAssociatedChanges = notifyAssociates;
            if (GeoLocationChange != null)
            {
                GeoLocationChange(this, eventArgs);
            }
        }
        #endregion
        #region Tag methods
        public bool AddTag(string tag)
        {
            return TagSet.Add(tag);
        }
        public bool RemoveTag(string tag)
        {
            return TagSet.Remove(tag);
        }
        #endregion

        #region restore hack
        public bool HasRestorableProperty
        {
            get
            {
                lock (AdditionalProperties)
                {
                    if (AdditionalProperties.Any(prop => prop.Value is IRestorable))
                    {
                        return true;
                    }
                }
                lock (DefaultProperties)
                {
                    if (DefaultProperties.Any(prop => prop.Value is IRestorable))
                    {
                        return true;
                    }
                }
                return RestoreProps.Any();
            }
        }

        public void RestoreProperties(bool notifyPropertyChange)
        {
            lock (DefaultProperties)
            {
                foreach (var prop in DefaultProperties)
                {
                    if (prop.Value is IRestorable)
                    {
                        ((IRestorable)prop.Value).RestoreProperty(notifyPropertyChange, this);
                    }
                }
            }
            lock (AdditionalProperties)
            {
                foreach (var prop in AdditionalProperties)
                {
                    if (prop.Value is IRestorable)
                    {
                        ((IRestorable)prop.Value).RestoreProperty(notifyPropertyChange, this);
                    }
                }
            }
            foreach (var prop in RestoreProps)
            {
                DepictionElementExtensions.UpdatePropertyValue(this, prop.Name, prop.Value, notifyPropertyChange);
            }
            RestoreProps.Clear();
        }

        #endregion

        #region Associated elemtns

        public bool DisableAssociatedElementListener { get; set; }//Hack property used by interactions that do internal changes
        public void AttachElements(List<IElement> elements)
        {
            var associatAdded = false;
            foreach (var element in elements)
            {
                if (!associatedElementList.Any(t => t.ElementId.Equals(element.ElementId)) &&
                    element.GeoLocation != null && !element.AssociatedElements.Any())
                {
                    associatAdded = true;
                    associatedElementList.AddLast(element);
                    element.GeoLocationChange += associatedElement_GeoLocationChange;
                    element.PropertyChanged += associatedElement_PropertyChanged;
                }
            }
            if (associatAdded)
            {
                TriggerAssociatedElementChange("Added");
            }
            //not sure if order is important, for now just recreate the whole thing
            AssociatedElementIds = associatedElementList.Select(t => t.ElementId).ToList();
        }

        public void AttachElementAfter(IElement newElement, IElement addAfter)
        {
            if (!associatedElementList.Any(t => t.ElementId.Equals(newElement.ElementId)) && newElement.GeoLocation != null
                && !newElement.AssociatedElements.Any())
            {
                var node = associatedElementList.Find(addAfter);
                if (node != null)
                {
                    associatedElementList.AddAfter(node, newElement);
                    newElement.GeoLocationChange += associatedElement_GeoLocationChange;
                    newElement.PropertyChanged += associatedElement_PropertyChanged;
                    TriggerAssociatedElementChange("Added");

                    //not sure if order is important, for now just recreate the whole thing
                    AssociatedElementIds = associatedElementList.Select(t => t.ElementId).ToList();
                }
            }
        }

        public void DettachElement(IElement element)
        {
            if (associatedElementList.Remove(element))
            {
                element.GeoLocationChange -= associatedElement_GeoLocationChange;
                element.PropertyChanged -= associatedElement_PropertyChanged;

                //not sure if order is important, for now just recreate the whole thing
                AssociatedElementIds = associatedElementList.Select(t => t.ElementId).ToList();
                TriggerAssociatedElementChange("Removed");
            }
        }
        public void DetachAllAssociates()
        {
            foreach (var associate in associatedElementList)
            {
                associate.GeoLocationChange -= associatedElement_GeoLocationChange;
                associate.PropertyChanged -= associatedElement_PropertyChanged;
            }
            associatedElementList.Clear();

            //not sure if order is important, for now just recreate the whole thing
            AssociatedElementIds = associatedElementList.Select(t => t.ElementId).ToList();
        }
        private void TriggerAssociatedElementChange(string propertyName)
        {
            if (!DisableAssociatedElementListener)
            {
                OnPropertyChanged(string.Format("Associate.{0}", propertyName));
            }
        }

        void associatedElement_GeoLocationChange(object sender, GeoLocationEventArgs e)
        {
            if (GeoLocationChange != null)
            {
                TriggerAssociatedElementChange("GeoLocation");
            }
        }

        private void associatedElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Deleted"))
            {
                var delElem = sender as IElement;
                if (delElem == null) return;
                DettachElement(delElem);
            }
            else
            {
                TriggerAssociatedElementChange(e.PropertyName);
            }
        }

        #endregion

        #region equals

        public override bool Equals(object obj)
        {
            var other = obj as DepictionElement;
            if (other == null) return false;
            if (String.Compare(other.ElementId, ElementId, StringComparison.Ordinal) != 0) return false;

            if (!Equals(other.ElementType, ElementType)) return false;
            if (!Equals(other.DisplayName, DisplayName)) return false;
            if (!Equals(other.ElementGeometry, ElementGeometry)) return false;
            return true;
        }
        #endregion
        public override string ToString()
        {
            return ElementType;
        }
    }
}