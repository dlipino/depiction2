﻿using System.Collections.Generic;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Extensions;

namespace Depiction2.Core.Entities.Element.ViewModel
{
    public class ListElementViewModel : ViewModelBase
    {
        #region varialbes

        public IElement modelBase;
        private bool isSelected;
        private string normalDisplayName = string.Empty;

        #endregion

        #region properties

        public string ElementId { get { return modelBase.ElementId; } }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value; OnPropertyChanged("IsSelected");
            }
        }

        public IEnumerable<string> Tags { get
        {
//            return new List<string> {"Simple Tag"};
            return modelBase.Tags;
        } }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(normalDisplayName))
                {
                    normalDisplayName = modelBase.ConstructToolTipText(", ", modelBase.UsePropertyNamesInInformationText, true);
                }
                return normalDisplayName;
            }
        }

        public string TypeDisplayName
        {
            get
            {
                //                var allNames = modelBase.ElementType.Split('.');
                //                return allNames.Length > 0 ? allNames[allNames.Length - 1] : modelBase.ElementType;
                return modelBase.DisplayName;
            }
        }

        public string IconSource
        {
            get
            {
                return string.IsNullOrEmpty(modelBase.ImageResourceName) ? modelBase.IconResourceName : modelBase.ImageResourceName;
            }
        }
        #endregion

        #region constructor

        public ListElementViewModel(IElement elementModel)
        {
            modelBase = elementModel;
            modelBase.PropertyChanged += modelBase_PropertyChanged;
        }

        protected override void OnDispose()
        {
            modelBase.PropertyChanged -= modelBase_PropertyChanged;
            base.OnDispose();
        }

        #endregion

        #region property change listener

        void modelBase_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var name = e.PropertyName;
            if(name.Equals("HoverText") || name.Equals("UsePropertyNameInHoverText") )
            {
                normalDisplayName = string.Empty;
                OnPropertyChanged("DisplayName");
            }
        }

        #endregion
    }
}