﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.Core.Entities.Element.ViewModel
{
    public class EditElementViewModel : ViewModelBase
    {
        public IElement _modelBase;
        #region properties

        public List<PropertyTemplateViewModel> InformationProperties { get; private set; }
        public List<PropertyTemplateViewModel> VisualProperties { get; private set; }
        public List<PropertyTemplateViewModel> MapControlProperties { get; private set; }
        public List<PropertyTemplateViewModel> DefaultProperties { get; private set; }
        public ObservableCollection<PropertyTemplateViewModel> UserProperties { get; private set; }

        #endregion
        #region constructor

        public EditElementViewModel(IElement owningElement)
        {
            if (owningElement == null)
            {
                throw new ArgumentNullException("owningElement", "Owning element cannot be null for EditViewModel");
            }
            _modelBase = owningElement;
            _modelBase.PropertyChanged += _elementModel_PropertyChanged;
            _modelBase.GeoLocationChange += _elementModel_GeoLocationChange;
            _modelBase.PropertyAdded += _elementModel_PropertyAdded;
            _modelBase.PropertyRemoved += _elementModel_PropertyRemoved;


            CreateInformationPropertyTemplates(_modelBase);
            CreateVisualPropertyTemplates(_modelBase);
            CreateMapControlPropertyTemplates(_modelBase);
            CreateDefaultPropertyTemplates(_modelBase);
            CreateUserPropertyTemplates(_modelBase);
        }
        protected override void OnDispose()
        {
            _modelBase.PropertyChanged -= _elementModel_PropertyChanged;
            _modelBase.GeoLocationChange -= _elementModel_GeoLocationChange;
            _modelBase.PropertyAdded -= _elementModel_PropertyAdded;
            _modelBase.PropertyRemoved -= _elementModel_PropertyRemoved;
            base.OnDispose();
        }
        #endregion

        #region property list creation helpers

        private void CreateInformationPropertyTemplates(IElement baseElement)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in PropertyInformation.InformationProps)
            {
                var isHoverText = baseElement.HoverTextProperties.Contains(item.Key);
                object value = null;
                switch (item.Key)
                {
                    case PropNames.DisplayName:
                        value = baseElement.DisplayName;
                        break;
                    case PropNames.Author:
                        value = baseElement.Author;
                        break;
                    case PropNames.Description:
                        value = baseElement.Description;
                        break;
                }
                if (value != null)
                {
                    propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, value, isHoverText));
                }
            }
            InformationProperties = new List<PropertyTemplateViewModel>(propHash);
        }
        private void CreateVisualPropertyTemplates(IElement baseElement)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in PropertyInformation.VisualProps)
            {
                object value = null;
                switch (item.Key)
                {
                    case PropNames.ZOIFillColor:
                        value = baseElement.ZOIFillColor;
                        break;
                    case PropNames.ZOIBorderColor:
                        value = baseElement.ZOIBorderColor;
                        break;
                    case PropNames.ZOILineThickness:
                        value = baseElement.ZOILineThickness;
                        break;
                    case PropNames.ZOIShapeType:
                        value = baseElement.ZOIShapeType;
                        break;
                    case PropNames.IconResourceName:
                        value = baseElement.IconResourceName;
                        break;
                    case PropNames.IconSize:
                        value = baseElement.IconSize;
                        break;
                    case PropNames.IconBorderColor:
                        value = baseElement.IconBorderColor;
                        break;
                    case PropNames.IconBorderShape:
                        value = baseElement.IconBorderShape;
                        break;
                    case PropNames.VisualState:
                        value = baseElement.VisualState;
                        break;
                }
                if (value != null)
                {
                    propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, value));
                }
            }
            VisualProperties = new List<PropertyTemplateViewModel>(propHash);
        }
        private void CreateMapControlPropertyTemplates(IElement baseElement)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in PropertyInformation.MapControlProps)
            {
                var isHoverText = baseElement.HoverTextProperties.Contains(item.Key);
                object value = null;
                switch (item.Key)
                {
                    case PropNames.IsDraggable:
                        value = baseElement.IsDraggable;
                        break;
                    case PropNames.IsGeometryEditable:
                        value = baseElement.IsGeometryEditable;
                        break;
                    case PropNames.GeoLocation:
                        value = baseElement.GeoLocation;
                        break;
                }
                if (value != null)
                {
                    propHash.Add(new PropertyTemplateViewModel(item.Key, item.Value, value, isHoverText));
                }
            }
            MapControlProperties = new List<PropertyTemplateViewModel>(propHash);
        }
        private void CreateDefaultPropertyTemplates(IElement baseElement)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in baseElement.DefaultElementPropertyNames)
            {
                var isHoverText = baseElement.HoverTextProperties.Contains(item.Item1);
                var prop = baseElement.GetDepictionProperty(item.Item1);
                if (prop == null) continue;

                if (prop.Value != null)
                {
                    propHash.Add(new PropertyTemplateViewModel(item.Item1, item.Item2, prop.Value, isHoverText));
                }
            }
            DefaultProperties = new List<PropertyTemplateViewModel>(propHash);
        }
        private void CreateUserPropertyTemplates(IElement baseElement)
        {
            var propHash = new HashSet<PropertyTemplateViewModel>(new PropertyTemplateVmNameComparer<PropertyTemplateViewModel>());
            foreach (var item in baseElement.AdditionalPropertyNames)
            {
                var isHoverText = baseElement.HoverTextProperties.Contains(item.Item1);
                var prop = baseElement.GetDepictionProperty(item.Item1);
                if (prop == null) continue;

                if (prop.Value != null)
                {
                    propHash.Add(new PropertyTemplateViewModel(item.Item1, item.Item2, prop.Value, isHoverText));
                }
            }
            UserProperties = new ObservableCollection<PropertyTemplateViewModel>(propHash);
        }
        #endregion
        #region event handlers
        void _elementModel_PropertyRemoved(IElementProperty obj)
        {
            var prop =
                UserProperties.FirstOrDefault(
                    t => t.ProperName.Equals(obj.ProperName, StringComparison.CurrentCultureIgnoreCase));
            if (prop == null) return;
            UserProperties.Remove(prop);
        }

        void _elementModel_PropertyAdded(IElementProperty obj)
        {
            var newExtra = new PropertyTemplateViewModel(obj.ProperName, obj.DisplayName, obj.Value);
            newExtra.IsPreview = true;

            UserProperties.Add(newExtra);
        }
        void _elementModel_GeoLocationChange(object sender, Base.Events.GeoLocationEventArgs e)
        {
            var location = InformationProperties.Find(t => t.ProperName.Equals(PropNames.GeoLocation));
            if (location == null) return;
            location.PropertyValue = _modelBase.GeoLocation.ToString();
        }

        void _elementModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //            PropertyTemplateViewModel propToChange = null;
            //            var propName = e.PropertyName;
            //            propToChange = VisualProperties.Find(t => t.ProperName.Equals(propName));
            //            if (propToChange == null)
            //            {
            //                propToChange = DefaultProperties.Find(t => t.ProperName.Equals(propName));
            //            }
            //            if (propToChange == null)
            //            {
            //                propToChange = UserProperties.FirstOrDefault(t => t.ProperName.Equals(propName));
            //            }
            //            if (propToChange == null) return;
            //            //Brute force property setting, all the normal properties of the element
            //            if (propName.Equals("IsDraggable"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IsDraggable);
            //            }
            //            else if (propName.Equals("IsGeometryEditable"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IsGeometryEditable);
            //            }
            //            else if (propName.Equals("ZOIBorderColor"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.ZOIBorderColor);
            //            }
            //            else if (propName.Equals("ZOIFillColor"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.ZOIFillColor);
            //            }
            //            else if (propName.Equals("ZOILineThickness"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.ZOILineThickness);
            //            }
            //            else if (propName.Equals("ZOIShapeType"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.ZOIShapeType);
            //            }
            //            else if (propName.Equals("IconBorderShape"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IconBorderShape);
            //            }
            //            else if (propName.Equals("IconBorderColor"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IconBorderColor);
            //            }
            //            else if (propName.Equals("IconSize"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IconSize);
            //            }
            //            else if (propName.Equals("IconResourceName"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.IconResourceName);
            //            }
            //            else if (propName.Equals("VisualState"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.VisualState);
            //            }
            //            else if (propName.Equals("DisplayInformationText"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.DisplayInformationText);
            //            }
            //            else if (propName.Equals("UseEnhancedInformationText"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.UseEnhancedInformationText);
            //            }
            //            else if (propName.Equals("UsePropertyNamesInInformationText"))
            //            {
            //                propToChange.ApplyOrigValueChange(_elementModel.UsePropertyNamesInInformationText);
            //            }
            //            else
            //            {
            //                var elementProp = _elementModel.GetDepictionProperty(propName);
            //                if (elementProp != null)
            //                {
            //                    propToChange.ApplyOrigValueChange(elementProp.Value);
            //                }
            //            }
        }
        #endregion

        #region viewmodel to model methods

        public void ApplyInformationChanges()
        {
            var hoverChange = false;
            foreach (var infoProp in InformationProperties)
            {
                if (infoProp.UpdateHoverTextStateForElement(_modelBase))
                {
                    hoverChange = true;
                }
                if (infoProp.ApplyViewModelChangeToOrig())
                {
                    _modelBase.UpdatePropertyValue(infoProp.ProperName, infoProp.PropertyValue);
                }
            }
            if (hoverChange) _modelBase.TriggerPropertyChange("HoverText");
        }

        public void ApplyVisualPropertyChanges()
        {
            foreach (var visualPropertyViewModel in VisualProperties)
            {
                if (visualPropertyViewModel.ApplyViewModelChangeToOrig())
                {
                    _modelBase.UpdatePropertyValue(visualPropertyViewModel.ProperName, visualPropertyViewModel.PropertyValue);
                }
            }
        }

        public void ApplyMapControlPropertyChanges()
        {
            foreach (var mapPropViewMOdel in MapControlProperties)
            {
                if (mapPropViewMOdel.ApplyViewModelChangeToOrig())
                {
                    _modelBase.UpdatePropertyValue(mapPropViewMOdel.ProperName, mapPropViewMOdel.PropertyValue);
                }
            }
        }

        public void ApplyDefaultPropertyChanges()
        {
            var hoverChange = false;
            foreach (var defaultElementProperty in DefaultProperties)
            {
                if (defaultElementProperty.UpdateHoverTextStateForElement(_modelBase))
                {
                    hoverChange = true;
                }
                if (defaultElementProperty.ApplyViewModelChangeToOrig())
                {
                    if (defaultElementProperty.UseAsHoverText == true)
                    {
                        hoverChange = true;
                    }
                    _modelBase.UpdatePropertyValue(defaultElementProperty.ProperName,
                                                      defaultElementProperty.PropertyValue, false);
                }
            }
            if (hoverChange) _modelBase.TriggerPropertyChange("HoverText");
        }

        public void ApplyUserPropertyChanges()
        {
            var hoverChange = false;
            var delProps = new List<PropertyTemplateViewModel>();
            foreach (var useproperty in UserProperties)
            {
                //Order is important, delete first so that new properties that 
                //get deleted don't get sent through the system.
                if (useproperty.DeleteProp)
                {
                    delProps.Add(useproperty);
                }
                else if (useproperty.IsPreview)
                {
                    _modelBase.AddUserProperty(new ElementProperty(useproperty.ProperName, useproperty.DisplayName, useproperty.PropertyValue));
                    useproperty.IsPreview = false;
                    useproperty.ApplyViewModelChangeToOrig();
                    if (useproperty.UseAsHoverText == true) hoverChange = true;
                }
                else
                {
                    if (useproperty.UpdateHoverTextStateForElement(_modelBase))
                    {
                        hoverChange = true;
                    }
                    if (useproperty.ApplyViewModelChangeToOrig())
                    {
                        if (useproperty.UseAsHoverText == true)
                        {
                            hoverChange = true;
                        }
                        _modelBase.UpdatePropertyValue(useproperty.ProperName,
                                                     useproperty.PropertyValue, false);
                    }
                }
            }
            //TODO deal with properties that are deleted 
            foreach (var delProp in delProps)
            {
                _modelBase.DeleteUserProperty(delProp.ProperName);
            }
            if (hoverChange)
            {
                _modelBase.TriggerPropertyChange("HoverText");
            }
        }
        #endregion

    }
}