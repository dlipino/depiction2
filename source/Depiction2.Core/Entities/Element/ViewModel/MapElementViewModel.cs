﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.Utilities;
using Depiction2.Core.Extensions;
using Depiction2.Core.ViewModels.Helpers;

//Is it better to make lots of smaller viewmodels (image, point, zoi) instead of one large one?
//one bad/annoying thing would be that more observable collections would be needed so things could get bulky.
namespace Depiction2.Core.Entities.Element.ViewModel
{
    //There might be some clean up work for the spatial, the main story uses quad source but not spatial
    //items
    public class MapElementViewModel : ViewModelBase, ISpatialElement
    {
        public event EventHandler SimpleBoundsChangedEvent;//not used for this
        public IElement _elementModel = null;
        private RotateTransform imageRotTrans = new RotateTransform(0);
        private string _hoverText = string.Empty;
        #region constructor/destructor

        public MapElementViewModel(IElement elementModel)
        {
            _elementModel = elementModel;
            Index = -1;
            
            DrawShapeGeometry = DepictionGeometryService.CreateDrawStreamGeometryFromGeometryWrapList(CoordinateSystemService.Instance.GeometryStoryToDisplay(elementModel.ElementGeometry));
            imageRotTrans.Angle = elementModel.ImageRotation;
            if (elementModel.GeoLocation != null)
            {
                var pcs = CoordinateSystemService.Instance.PointStoryToDisplay((Point) elementModel.GeoLocation);
                var drawPoint = LocationConverter.ConvertCartisianToPanelPoint(pcs);
                DrawLocationX = drawPoint.X;
                DrawLocationY = drawPoint.Y;
            }
            elementModel.PropertyChanged += ElementModelPropertyChanged;
            elementModel.GeoLocationChange += elementModel_GeoLocationChange;
        }

        protected override void OnDispose()
        {
            _elementModel.PropertyChanged -= ElementModelPropertyChanged;
            _elementModel.GeoLocationChange -= elementModel_GeoLocationChange;
        }
        #endregion

        #region constructor event halders
        void elementModel_GeoLocationChange(object sender, Base.Events.GeoLocationEventArgs e)
        {
            if (_elementModel.GeoLocation != null)
            {
//                IDepictionStoryProjectionService projector = null;
//                if (DepictionAccess.DStory != null && DepictionAccess.DStory.StoryProjector != null)
//                {
//                    projector = DepictionAccess.DStory.StoryProjector;
//                }
                SetDrawLocationFromGCSPoint((Point)_elementModel.GeoLocation);//, projector);
            }
        }
        //TODO create a dictionary to match the model property to the viewmodel property
        void ElementModelPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (_elementModel == null)
            {
                //Throw some sort of error
                return;
            }
            if (propName.Equals("ImageResourceName"))
            {
                OnPropertyChanged(() => GeoImage);
            }
            else if (propName.Equals("GeoImageRotation"))
            {
                imageRotTrans.Angle = _elementModel.ImageRotation;
                OnPropertyChanged(() => GeoImageTransform);
            }
            else if (propName.Equals("ZOIFillColor"))
            {
                OnPropertyChanged("ZOIFill");
            }
            else if (propName.Equals("ZOILineThickness"))
            {
                OnPropertyChanged("ZOILineThickness");
            }
            else if (propName.Equals("ZOIBorderColor"))
            {
                OnPropertyChanged("ZOIStroke");
            }
            else if (propName.Equals("IconResourceName"))
            {
                OnPropertyChanged("IconImage");
            }
            else if (propName.Equals("ElementVisibilityFlag"))
            {
                OnPropertyChanged("ElementVisibility");
            }
            else if (propName.Equals("ElementGeometry"))
            {
                UpdateStreamGeometryVisual();
                //                SetGeometryWithDispatcher(GeometryWrapHelpers.CreateDrawStreamGeometryFromGeometryWrapList(_elementModel.ElementGeometry));
            }
            else if (propName.Equals("HoverText") || propName.Equals("UsePropertyNamesInInformationText"))
            {
                _hoverText = string.Empty;
                OnPropertyChanged("HoverText");
                OnPropertyChanged("PermaTextString");
            }
            else if (propName.Equals("DisplayInformationText", StringComparison.InvariantCultureIgnoreCase) )
            {
                OnPropertyChanged("UsePermaText");
            }else if (propName.Equals("UseEnhancedInformationText", StringComparison.InvariantCultureIgnoreCase) )
            {
                OnPropertyChanged("UseEnhancedPermaText");
            }
            else
            {
                OnPropertyChanged(propName);
            }
        }

        #endregion

        #region Properties
        #region properties that have no home
        public string HoverText { get
        {
            if(string.IsNullOrEmpty(_hoverText))
            {
                _hoverText = _elementModel.ConstructToolTipText(Environment.NewLine,
                                                                _elementModel.UsePropertyNamesInInformationText, false);
            }
            return _hoverText;
        } }
        public bool IsDraggable { get { return _elementModel.IsDraggable; } }
        public string ElemId
        {
            get { return _elementModel.ElementId; }
        }

        public ElementVisualSetting VisualState { get { return _elementModel.VisualState; } }
        #endregion
        #region Permatext properties

        public IPermaText PermaText { get { return _elementModel.PermaText; } }

        public bool UsePermaText
        {
            get
            {
                return _elementModel.DisplayInformationText;
            }
        }

        public bool UseEnhancedPermaText
        {
            get
            {
                return _elementModel.UseEnhancedInformationText;
            }
        }
        public string PermaTextString
        {
            get
            {
                if (PermaText == null) return "";
                return _elementModel.ConstructToolTipText(Environment.NewLine, _elementModel.UsePropertyNamesInInformationText, !UseEnhancedPermaText);
            }
        }

        public double PermaTextX
        {
            get
            {
                if (PermaText == null) return 0;
                return PermaText.PermaTextX;
            }
            set
            {
                if (PermaText == null) return;
                PermaText.PermaTextX = value; OnPropertyChanged("PermaTextX");
            }
        }
        public double PermaTextY
        {
            get
            {
                if (PermaText == null) return 0; return PermaText.PermaTextY;
            }
            set
            {
                if (PermaText == null) return;
                PermaText.PermaTextY = value; OnPropertyChanged("PermaTextY");
            }
        }
        public double PermaTextWidth
        {
            get
            {
                if (PermaText == null) return 0; return PermaText.PixelWidth;
            }
            set
            {
                if (PermaText == null) return;
                PermaText.PixelWidth = value; OnPropertyChanged("PermaTextWidth");
            }
        }
        public double PermaTextHeight
        {
            get
            {
                if (PermaText == null) return 0; return PermaText.PixelHeight;
            }
            set
            {
                if (PermaText == null) return;
                PermaText.PixelHeight = value; OnPropertyChanged("PermaTextHeight");
            }
        }

        private double permaTextCenterX = 0;
        private double permaTextCenterY = 0;
        public double PermaTextCenterX { get { return permaTextCenterX; } set { permaTextCenterX = value; OnPropertyChanged("PermaTextCenterX"); } }
        public double PermaTextCenterY { get { return permaTextCenterY; } set { permaTextCenterY = value; OnPropertyChanged("PermaTextCenterY"); } }
        #endregion
        #region Icon properties

        public Point IconPosition { get; set; }
        public ImageSource IconImage
        {
            get
            {
                return (ImageSource)StringToDepictionIconConverter.Default.Convert(_elementModel.IconResourceName,
                                                                      typeof (ImageSource), null,
                                                                      CultureInfo.CurrentCulture);
                //Copied from StringTODepictionIconConverter, not sure what is better, using
                //the converter of using the iconimage
//                var name = _elementModel.IconResourceName;
//                //Try to find the image in the application images
//                var defaultIcons = DepictionAccess.DefaultIconDictionary;
//                if (defaultIcons.ContainsKey(name))
//                {
//                    return (ImageSource)defaultIcons[name];
//                }
//
//                var imageResources = DepictionAccess.DStory.AllImages;
//                if (imageResources == null) return null;
//                if (!imageResources.Contains(name)) return null;
//
//                return DepictionAccess.DStory.AllImages.GetImage(name);
            }
        }

        public double IconSize { get { return _elementModel.IconSize; } }
        public TranslateTransform CenterIcon { get { return new TranslateTransform(-_elementModel.IconSize / 2, -_elementModel.IconSize / 2); } }
        #endregion
        #region ZOI properties
        public string ZOIFill { get { return _elementModel.ZOIFillColor; } }
        public string ZOIStroke { get { return _elementModel.ZOIBorderColor; } }
        public double ZOILineThickness { get { return _elementModel.ZOILineThickness; } }
        #endregion
        #region Image stuff
        public RotateTransform GeoImageTransform { get { return imageRotTrans; } }
        public ImageSource GeoImage
        {
            get
            {
                if (DepictionAccess.DStory == null || string.IsNullOrEmpty(_elementModel.ImageResourceName))
                {
                    return null;
                }
                var images = DepictionAccess.DStory.AllImages;
                if (images == null) return null;
                var image = images.GetImage(_elementModel.ImageResourceName);
                return image;
            }
        }
        #endregion

        #region ISpatial item properties that are used in the zoomable canvas /quad tree
        public int Index { get; set; }//used for controlling their placement in the visual true and in the observablecollection that holds this

        public Rect SimpleBounds
        {
            get { return _elementModel.SimpleBounds.WpfRect; }
        }
        public double SimpleBoundsArea
        {
            get { return _elementModel.SimpleBoundsArea; }
        }

        #endregion
        #region icon display properties
        public double DrawLocationX { get; set; }
        public double DrawLocationY { get; set; }

        public double DrawOffsetX
        {
            get
            {
                if (DrawShapeGeometry == null) return 0;
                return DrawShapeGeometry.Bounds.Left;
            }
        }
        public double DrawOffsetY
        {
            get
            {
                if (DrawShapeGeometry == null) return 0;
                return DrawShapeGeometry.Bounds.Top;
            }
        }
        public double DrawWidth
        {
            get
            {
                if (DrawShapeGeometry == null) return 0;
                return DrawShapeGeometry.Bounds.Width;
            }
        }

        public double DrawHeight
        {
            get
            {
                if (DrawShapeGeometry == null) return 0;
                return DrawShapeGeometry.Bounds.Height;
            }
        }
        #endregion
        #endregion
        #region dep props
        public StreamGeometry DrawShapeGeometry
        {
            get { return (StreamGeometry)GetValue(DrawShapeGeometryProperty); }
            set { SetValue(DrawShapeGeometryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DrawShapeGeometryProperty =
            DependencyProperty.Register("DrawShapeGeometry", typeof(StreamGeometry), typeof(MapElementViewModel),
             new PropertyMetadata(DrawShapeGeometryChanged));


        private static void DrawShapeGeometryChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var svm = d as MapElementViewModel;
            if (svm == null) return;
            svm.OnPropertyChanged("DrawOffsetX");
            svm.OnPropertyChanged("DrawOffsetY");
            svm.OnPropertyChanged("DrawWidth");
            svm.OnPropertyChanged("DrawHeight");
            svm.OnPropertyChanged("DrawLocationY");
            svm.OnPropertyChanged("DrawLocationX");
        }

        #endregion

        #region set model things
        //the book keeping for Coordinate System is a pain
        public void UpdateStreamGeometryVisual()//IDepictionStoryProjectionService projectionChanger)
        {
            var geomClone = CoordinateSystemService.Instance.GeometryStoryToDisplay(_elementModel.ElementGeometry);

            SetGeometryWithDispatcher(DepictionGeometryService.CreateDrawStreamGeometryFromGeometryWrapList(geomClone));
        }
        public void SetDrawLocationFromGCSPoint(Point gcsLocation)//, IDepictionStoryProjectionService projector)
        {
            var drawPoint = GeoToDrawLocationService.ConvertGCSPointToDrawPoint(gcsLocation); //, projector);
            DrawLocationX = drawPoint.X;
            DrawLocationY = drawPoint.Y;

            OnPropertyChanged("DrawLocationY");
            OnPropertyChanged("DrawLocationX");
            OnPropertyChanged("GeoLocation");
        }
        //assume it comes in as projected and not GCS
        //And convertered to cartisian
        public void SetModelGeoLocationFromProjectedLocation(Point displayLocation)
        {
            var gcsPoint = CoordinateSystemService.Instance.PointDisplayToStory(displayLocation);//GeoToDrawLocationService.ConvertDrawPointToGeoPoint(displayLocation);//, projector);
            _elementModel.UpdatePrimaryGeoLocationAndShiftGeometry(gcsPoint);
        }
        public void ShiftModelGeoLocation(Point startPixel, Point endPixel)
        {

            var startGeo = CoordinateSystemService.Instance.PointDisplayToStory(startPixel);
            var endGeo = CoordinateSystemService.Instance.PointDisplayToStory(endPixel);

            _elementModel.GeoShiftPrimaryGeoLocationAndShiftGeometry(startGeo-endGeo);
        }

        #endregion

        private void SetGeometryWithDispatcher(StreamGeometry drawGeometry)
        {
            //Not sure if this will work all the time, bue cause might be getting set from background . . .
            if (Application.Current != null && !Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<StreamGeometry>(SetGeometryWithDispatcher), drawGeometry);
                return;
            }
            DrawShapeGeometry = drawGeometry;
        }

        public void DoOnClickActions(Point viewportCanvasPoint)
        {
            var actions = _elementModel.OnClickActions;
            var cartPoint = GeoToDrawLocationService.ScreenToCartCanvas(viewportCanvasPoint);
            var geoPoint = GeoToDrawLocationService.ConvertDrawPointToGeoPoint(cartPoint);
            if (actions == null) return;
            foreach (var action in actions)
            {
                var key = action.Key;
                var behaviours =
                    ExtensionService.Instance.DepictionBehaviors.Where(t => t.Metadata.Name.Equals(key)).ToList();
                if (behaviours.Any())
                {
                    foreach (var behavior in behaviours)
                    {
                        if (behavior != null)
                        {
                            //hack, we just assume the behavior takes a point
                            behavior.Value.DoBehavior(_elementModel, new object[] { geoPoint });
//                            var behaviorParams = LegacyHelpers.BuildParametersForPostSetActions(action.Value);
//                            behavior.Value.DoBehavior(_elementModel, behaviorParams);
                        }
                    }
                }
            }
        }
    }
}