﻿using System;

namespace Depiction2.Core.Entities.Messaging
{
    public class MessageViewModel
    {
        public DateTime MessageTime { get; private set; }
        public Guid Id { get; private set; }
        public string Message { get; set; }
        public MessageViewModel(string message)
        {
            Message = message;
            Id = Guid.NewGuid();
            MessageTime = DateTime.Now;

        }
    }
}