﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction2.API.Service;
using Depiction2.Base.Helpers;

namespace Depiction2.Core.Entities.Messaging
{
    public class MessageServiceViewModel : ViewModelBase
    {
        private DepictionMessageService _model;
        //messages should come in pretty slowly
        private Dispatcher dispatcher;
        public ObservableCollection<MessageViewModel> MessageCollection { get; set; }

        #region commands
        private DelegateCommand<Guid> removeMessageCommand;
        public ICommand RemoveMessageCommand
        {
            get
            {
                if (removeMessageCommand == null)
                {
                    removeMessageCommand = new DelegateCommand<Guid>(RemoveMessage);

                }
                return removeMessageCommand;
            }
        }

        #endregion

        #region constructo/destruction
        public MessageServiceViewModel(DepictionMessageService messageService)
        {
            _model = messageService;
            dispatcher = Dispatcher.CurrentDispatcher;

            MessageCollection = new ObservableCollection<MessageViewModel>();
            _model.DisplayMessageAdded += _model_DisplayMessageAdded;

        }



        protected override void OnDispose()
        {

            _model.DisplayMessageAdded -= _model_DisplayMessageAdded;
            base.OnDispose();
        }
        #endregion
        #region events
        void _model_DisplayMessageAdded(string message, int fadeTime)
        {
            if (dispatcher.CheckAccess())
            {
                AddMessage(message, fadeTime);
            }
            else
            {
                dispatcher.BeginInvoke(DispatcherPriority.Render,
                    new Action(() => AddMessage(message, fadeTime)));
            }

        }
        #endregion
        private void AddMessage(string message, int fadeTime)
        {
            var vm = new MessageViewModel(message);
            if (fadeTime > 0)
            {
                var timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(fadeTime);
                timer.Tick += delegate(object sender, EventArgs args)
                                  {
                                      RemoveMessage(vm.Id);
                                      timer.Stop();
                                  };
                MessageCollection.Add(vm);
                timer.Start();
            }
            else
            {
                MessageCollection.Add(vm);
            }
        }
        private void RemoveMessage(Guid messageId)
        {
            var rMessage = MessageCollection.FirstOrDefault(t => t.Id.Equals(messageId));
            if (rMessage == null) return;
            MessageCollection.Remove(rMessage);
        }

    }
}