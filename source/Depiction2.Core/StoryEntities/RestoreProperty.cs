﻿namespace Depiction2.Core.StoryEntities
{
    public class RestoreProperty
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}