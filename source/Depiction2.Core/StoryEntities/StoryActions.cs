﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.Core.StoryEntities
{
    public class StoryActions : ModelBase, IStoryActions
    {

        public event Action<IElementTemplate, bool> StoryRequestMouseAdd;
        public event Action<IElementTemplate, bool> PlaceTempElement;
        public event Action<IEnumerable<IElement>> RequestElementView;


        protected override void OnDispose()
        {
            base.OnDispose();
        }

        public void TriggerStoryRequestMouseAdd(IElementTemplate elementTemplate, bool multiAdd)
        {
            if(StoryRequestMouseAdd != null)
            {
                StoryRequestMouseAdd(elementTemplate, multiAdd);
            }

        }
        public void TriggerElementPlacement(IElementTemplate element, IEnumerable<Point> geometryPoints)
        {


            if (PlaceTempElement != null)
            {
                PlaceTempElement(element, false);
            }
        }

        public void TriggerRequestElementView(IEnumerable<IElement> elements)
        {
            if (RequestElementView != null)
            {
                RequestElementView(elements);
            }
        }
    }
}