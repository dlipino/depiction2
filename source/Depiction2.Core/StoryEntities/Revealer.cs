﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class Revealer : ElementDisplayer, IElementRevealer
    {
        private DisplayerViewType _revealerShape = DisplayerViewType.Rectangle;
        [DataMember]
        private Dictionary<string, IRevealerProperty> properties { get; set; }

        //Put in because the json serializer doesnt handle enums as objects very well
        public DisplayerViewType RevealerShape
        {
            get { return _revealerShape; }
            set
            {
                _revealerShape = value;
                OnPropertyChanged("RevealerShape");
            }
        }

        public int PropertyCount { get { return properties.Count; } }

        #region constructor

        public Revealer(string revealerName, string id)
            : base(id)
        {
            DisplayerName = revealerName;
            properties = new Dictionary<string, IRevealerProperty>();
            CreateAndAddProperty("Maximized", true);
            CreateAndAddProperty("BorderVisible", true);
            CreateAndAddProperty("Anchored", false);
            CreateAndAddProperty("Opacity", .7);
            //            CreateAndAddProperty("RevealerShape", DisplayerViewType.Rectangle);//Json serializtion does not handle enum as object well, must add it to clr props
            CreateAndAddProperty("TopMenu", true);
            CreateAndAddProperty("BottomMenu", true);
            CreateAndAddProperty("SideMenu", false);
            CreateAndAddProperty("PermaTextX", 20d);
            CreateAndAddProperty("PermaTextY", 20d);
        }
        public Revealer(string revealerName)
            : this(revealerName, Guid.NewGuid().ToString())
        {

        }
        public Revealer()
            : this("None", Guid.NewGuid().ToString())
        {

        }
        //Reference
        //                    isRevealerMaximized = (bool)revealer_model.GetPropertyValue("Maximized");
        //            isRevealerBorderVisible = (bool)revealer_model.GetPropertyValue("BorderVisible");
        //            isRevealerAnchored = (bool)revealer_model.GetPropertyValue("Anchored");
        //            revealerOpacity = (double)revealer_model.GetPropertyValue("Opacity");
        //            shapeType = (RevealerShapeType)(revealer_model.GetPropertyValue("RevealerShape"));
        //            revealerTopVisible = (bool)revealer_model.GetPropertyValue("TopMenu");
        //            revealerBottomVisible = (bool)revealer_model.GetPropertyValue("BottomMenu");
        //            revealerSideVisible = (bool)revealer_model.GetPropertyValue("SideMenu");
        //            _minContentLocationX = (double)revealer_model.GetPropertyValue("PermaTextX");
        //            _minContentLocationY = (double)revealer_model.GetPropertyValue("PermaTextY");

        //        var type = DepictionLegacyTypeInformationSerialization.GetFullTypeFromSimpleTypeString(attValType);
        //        var prop = new RevealerProperty(attName, attVal, type);

        protected override void OnDispose()
        {
            base.OnDispose();
        }
        #endregion
        #region private constructor helpers

        private void CreateAndAddProperty(string name, object value)
        {
            var newProp = new RevealerProperty(name, value);
            properties.Add(name.ToLower(), newProp);
        }
        #endregion
        #region interface methods
        public void SetRevealerPropertyValue(string name, object value)
        {
            var lName = name.ToLower();
            if (properties.ContainsKey(lName))
            {
                properties[lName].SetValue(value, true);
                OnPropertyChanged(lName);
            }
        }
        //A special method that is used during the load process
        public void AddOrReplaceProperty(IRevealerProperty prop)
        {
            var name = prop.PropertyName.ToLower();
            if (properties.ContainsKey(name))
            {
                properties.Remove(name);
            }
            properties.Add(name, prop);
        }
        public void AddNewProperty(IRevealerProperty prop)
        {
            var name = prop.PropertyName.ToLower();
            if (!properties.ContainsKey(name)) properties.Add(name, prop);
        }

        public object GetPropertyValue(string name)
        {
            var lName = name.ToLower();
            if (properties.ContainsKey(lName))
            {
                return properties[lName].Value;
            }
            return null;
        }

        public void CreateNewProperty(string name, object value)
        {
            var lName = name.ToLower();
            var prop = new RevealerProperty(lName, value);
            properties.Add(lName, prop);
        }
        #endregion

        #region equals
        public override bool Equals(IElementDisplayer displayer)
        {
            if (!base.Equals(displayer)) return false;
            var rev = displayer as IElementRevealer;
            if (rev == null) return false;

            if (!properties.Count.Equals(rev.PropertyCount)) return false;
            foreach (var key in properties.Keys)
            {
                var expected = properties[key];
                var res = rev.GetPropertyValue(key);
                var exp = expected.Value;
                if (!Equals(exp, res))
                {
                    return false;
                }
            }
            if (!Equals(rev.RevealerShape, RevealerShape)) return false;

            return true;
        }
        #endregion
    }
}