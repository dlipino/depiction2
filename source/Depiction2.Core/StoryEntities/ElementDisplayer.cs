﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class ElementDisplayer : ModelBase, IElementDisplayer
    {
        public event NotifyCollectionChangedEventHandler DisplayedElementsChangedIdList;
        #region variables

        private IStory _mainStory;
        private CartRect _rectGeoBounds = null;
        private string _displayerName;
        #endregion

        #region properties
        [DataMember]
        public string DisplayerName { get { return _displayerName; } set { _displayerName = value; OnPropertyChanged(() => DisplayerName); } }
        [DataMember]
        public string DisplayerId { get; private set; }

        [DataMember]
        protected HashSet<string> _elementIds { get; set; }
        public ReadOnlyCollection<string> ElementIds
        {
            get { return new ReadOnlyCollection<string>(_elementIds.ToList()); }
        }

        public double GeoTop { get { return GeoRectBounds.Top; } }
        public double GeoBottom { get { return GeoRectBounds.Bottom; } }
        public double GeoLeft { get { return GeoRectBounds.Left; } }
        public double GeoRight { get { return GeoRectBounds.Right; } }
        public double GeoWidth
        {
            get
            {
                return GeoRectBounds.Width;
            }
        }
        public double GeoHeight
        {
            get
            {
                return GeoRectBounds.Height;
            }
        }

        [DataMember]
        public ICartRect GeoRectBounds
        {
            get { return _rectGeoBounds; }
            set
            {
                _rectGeoBounds = new CartRect(value);
                OnPropertyChanged(() => GeoRectBounds);
            }
        }

        #endregion

        #region Constructor/destruction

        public ElementDisplayer()
            : this(Guid.NewGuid().ToString())
        {
        }

        public ElementDisplayer(string id)
        {
            GeoRectBounds = null;

            if (!string.IsNullOrEmpty(id))
            {
                DisplayerId = id;
            }
            else
            {
                DisplayerId = Guid.NewGuid().ToString();
            }
            _elementIds = new HashSet<string>();
        }
        
        protected override void OnDispose()
        {
            if (_mainStory == null) return;
            _mainStory.ElementCollectionChanged -= elementSource_CollectionChanged;
            base.OnDispose();
        }
        #endregion

        #region element contorl

        //maybe put this with constructor?
        public void ConnectToStory(IStory story)
        {
            if (story == null) return;
            _mainStory = story;
            var elementSource = story.AllElements;
            if (elementSource == null)
            {
                throw new NullReferenceException("Cannot havea new element soure in an element displayer.");
            }
            story.ElementCollectionChanged += elementSource_CollectionChanged;
        }

        void elementSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action.Equals(NotifyCollectionChangedAction.Remove) || e.Action.Equals(NotifyCollectionChangedAction.Reset))
            {
                //Dont really know how the reset works
                var oldItems = e.OldItems;
                if(oldItems != null)
                {
                    var idListToRemove = new List<string>();
                    foreach (var oldItem in oldItems)
                    {
                        var elem = oldItem as IElement;
                        if (elem != null)
                        {
                            idListToRemove.Add(elem.ElementId);
                        }
                    }
                    RemoveElementsWithMatchingIds(idListToRemove);
                }
            }
        }

        //Does not check to see if the element is present in the story
        public void AddDisplayElementsWithMatchingIds(IEnumerable<string> elementIdsToShow)
        {
            var elemList = elementIdsToShow.ToList();
            _elementIds.UnionWith(elemList);
            if(DisplayedElementsChangedIdList != null)
            {
                var e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, elemList);
                DisplayedElementsChangedIdList(this, e);
            }
        }

        public void RemoveElementsWithMatchingIds(IEnumerable<string> elementIdsToRemove)
        {
            var elemList = elementIdsToRemove.ToList();
            _elementIds.ExceptWith(elemList);
            if (DisplayedElementsChangedIdList != null)
            {
                var e = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, elemList);
                DisplayedElementsChangedIdList(this, e);
            }
        }

        #endregion

        #region Bound control

        virtual public void SetBounds(IDepictionGeometry drawBounds)
        {
            GeoRectBounds = drawBounds.RectBounds;
        }

        virtual public void SetBounds(Point tl, Point br)
        {
            GeoRectBounds = new CartRect(tl, br);
        }

        #endregion

        #region equals
        public override bool Equals(object obj)
        {
            return Equals(obj as IElementDisplayer);
        }
        virtual public bool Equals(IElementDisplayer displayer)
        {
            if (displayer == null) return false;
            if (!Equals(DisplayerId, displayer.DisplayerId)) return false;
            if (!Equals(DisplayerName, displayer.DisplayerName)) return false;
            if (!_elementIds.SetEquals(displayer.ElementIds)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}