﻿using System;
using System.Runtime.Serialization;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
   [DataContract]
    public class RevealerProperty : IRevealerProperty
    {
        private object propValue;
        [DataMember]
        public string PropertyName { get; set; }
        [DataMember]
        public object Value { get { return propValue; } private set { propValue = value; } }
        public Type PropertyType { get { return Value.GetType(); } }

        #region Constructor

        public RevealerProperty()
        {
            PropertyName = "Default";
            Value = 0;
        }

        public RevealerProperty(string name, object value)
        {
            PropertyName = name.ToLower();
            Value = value;
        }
        public RevealerProperty(string name, string valString, Type valType)
        {
            PropertyName = name.ToLower();
            SetValue(valString, valType);
        }

        #endregion

        #region Methods

        public bool SetValue(string stringValue, Type valueType)
        {
            if (valueType.IsEnum)
            {
                try
                {
                    return SetValue(Enum.Parse(valueType, stringValue), true);
                }
                catch
                {
                    return false;
                }
            }
            return SetValue(Convert.ChangeType(stringValue, valueType), true);
        }

        public bool SetValue(object value, bool changeType)
        {
            var type = value.GetType();
            if (!changeType)
            {
                if (!Value.GetType().Equals(type)) return false;
            }
            Value = value;
            return true;
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (RevealerProperty)obj;
            if (!Equals(PropertyName, other.PropertyName)) return false;
            if (!Equals(PropertyType, other.PropertyType)) return false;
            if (!Equals(Value, other.Value)) return false;

            return true;
        }

        #endregion
    }
}