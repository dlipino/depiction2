﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class DepictionRegion : ElementDisplayer, IDepictionRegion
    {
//        private HashSet<RegionDataSourceInformation> _dataSourceList = new HashSet<RegionDataSourceInformation>();
        [DataMember]
        private HashSet<string> DataSourceList { get; set; }
        private IDepictionGeometry _regionGeometry;
        #region properties
        public IEnumerable<string> ConnectedDataSources
        {
            get { return DataSourceList; }
        }
        [DataMember]
        public IDepictionGeometry RegionGeometry
        {
            get { return _regionGeometry; }
            private set { _regionGeometry = value; }

        }

        public ICartRect RegionRect
        {
            get { return _regionGeometry.RectBounds; }
        }

        #endregion

        #region serialization props

        #endregion
        #region constructor
        public DepictionRegion(Point tl,Point br)
        {
            DisplayerName = "Data Region";
            DataSourceList = new HashSet<string>();
            base.SetBounds(tl,br);
            _regionGeometry = DepictionGeometryService.CreateGeometry(new CartRect(tl, br));
        }
        #endregion

        public void SetNewGeometry(IDepictionGeometry newGeometry)
        {
            _regionGeometry = newGeometry;//clone?
            GeoRectBounds = newGeometry.RectBounds;
        }

      public void AddManagedElementId(string elementId)
        {
            _elementIds.Add(elementId);
        }

        public bool AddDataSource(RegionDataSourceInformation dataSource)
        {
            return DataSourceList.Add(dataSource.Name);
        }
        public bool RemoveDataSource(RegionDataSourceInformation dataSource)
        {
            return DataSourceList.Remove(dataSource.Name);
        }

        public void UpdateSourceData()
        {
            //This happens when the region size changes, should eventually give multiple options
            //for now clear things out
            //Stop interactions
            var mainStory = DepictionAccess.DStory;
            if(mainStory != null)
            {
                
                mainStory.RemoveElementsById(_elementIds.ToList());
            }

            foreach(var sourceName in DataSourceList)
            {
                var importerExtension =
                    ExtensionService.Instance.RegionDataLoaders.FirstOrDefault(t => t.Metadata.GenericName.Equals(sourceName, StringComparison.CurrentCultureIgnoreCase));
                if (importerExtension == null) continue;
                if(DepictionAccess.HackRegionSourceList == null) continue;

                var sourceInfo = DepictionAccess.HackRegionSourceList.FirstOrDefault(t => t.Name.Equals(sourceName));
                if (sourceInfo == null) continue;

                importerExtension.Value.ImportElements(this, sourceInfo);
            }
        }
        public override void SetBounds(Point tl, Point br)
        {
            _regionGeometry = DepictionGeometryService.CreateGeometry(new CartRect(tl, br));
            base.SetBounds(tl, br);
        }
        public override void SetBounds(IDepictionGeometry drawBounds)
        {
            _regionGeometry = drawBounds;
            base.SetBounds(drawBounds);
        }

//        #region serialization help
//        [OnSerializing]
//        internal void OnSerializingMethod(StreamingContext context)
//        {
//            TopLeft = _wpfRectBase.TopLeft;
//            BottomRight = _wpfRectBase.BottomRight;
//        }
//        [OnDeserialized]
//        internal void OnDeserializedMethod(StreamingContext context)
//        {
//            _wpfRectBase = new Rect(TopLeft, BottomRight);
//        }
//        #endregion
    }
}