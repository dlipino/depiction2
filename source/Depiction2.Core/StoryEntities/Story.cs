﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Events;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.Base.Utilities;
using Depiction2.Core.Collections;
using Depiction2.Core.Interactions;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.StoryInformation;

namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class Story : ModelBase, IStory
    {
        public event NotifyCollectionChangedEventHandler ElementCollectionChanged;
        public event NotifyCollectionChangedEventHandler AnnotationCollectionChanged;
        public event NotifyCollectionChangedEventHandler RevealerCollectionChanged;
        public event NotifyCollectionChangedEventHandler RegionCollectionChanged;
        public event EventHandler<PropertyChangeEventArg> ElementPropertyChanged;
        public event EventHandler<GeoLocationEventArgs> ElementGeoLocationChanged;
        public event EventHandler StoryChanged;//general helper to keep track of story changes

        #region variables
        private ModQuadTree<IElement> allElements;
        private DepictionImageResourceDictionary allImages;
        private List<IDepictionAnnotation> allAnnotations;
        private List<IElementRevealer> revealerDisplayers;
        private List<IDepictionRegion> _allRegions;
        private IElementDisplayer mainDisplayer;
        private string topRevealerId = string.Empty;

        private IInteractionRuleRepository interactionRuleRepository = InteractionRuleRepository.Instance;
        private IInteractionRouter router;

        private string _displayCoordinateSystem = CoordinateSystemService.DepictionGeographicCoordinateSystem;

        private StoryGeoInformation geoInformation;
        #endregion
        #region properties
        [DataMember]
        public IStoryDetails StoryDetails { get; set; }
        public IInteractionRuleRepository InteractionRules
        {
            get { return interactionRuleRepository; }
        }

        public IInteractionRouter InteractionsRunner
        {
            get { return router; }
        }
        [DataMember]
        public string ElementCoordinateSystem
        {
            get { return CoordinateSystemService.DepictionGeographicCoordinateSystem; }
            private set { }
        }

        [DataMember]
        public string DisplayCoordinateSystem
        {
            get
            {
                return string.IsNullOrEmpty(_displayCoordinateSystem) ? ElementCoordinateSystem : _displayCoordinateSystem;
            }
            private set { _displayCoordinateSystem = value; }
        }

        //Readonly?
        public ICartRect ElementExtent
        {
            get
            {
                var finalExtent = new CartRect();
                foreach (var item in allElements)
                {
                    finalExtent.Union(item.SimpleBounds);
                }
                return finalExtent;
            }
        }

        public ReadOnlyCollection<IElement> AllElements
        {
            get { return new ReadOnlyCollection<IElement>(allElements.ToList()); }
        }
        //Readonly?
        public IEnumerable<IDepictionAnnotation> AllAnnotations
        {
            get { return allAnnotations; }
        }

        public DepictionImageResourceDictionary AllImages
        {
            get { return allImages; }
        }

        public IElementDisplayer MainDisplayer
        {
            get { return mainDisplayer; }
            set { mainDisplayer = value; }
        }
        public ReadOnlyCollection<IElementRevealer> RevealerDisplayers
        {
            get { return new ReadOnlyCollection<IElementRevealer>(revealerDisplayers); }
        }

        //Feels like this should be in the viewmodel stuff and not in the model

        public string TopRevealerId
        {
            get { return topRevealerId; }
            set
            {
                if (value.Equals(topRevealerId, StringComparison.InvariantCultureIgnoreCase)) return;
                topRevealerId = value; OnPropertyChanged(() => TopRevealerId);
            }
        }

        #endregion
        #region constructor/ disposal

        protected Story()
        {
            geoInformation = new StoryGeoInformation();
            allImages = new DepictionImageResourceDictionary();
            allElements = new ModQuadTree<IElement>();
            allAnnotations = new List<IDepictionAnnotation>();
            mainDisplayer = new ElementDisplayer { DisplayerName = "MainDisplayer" };
            revealerDisplayers = new List<IElementRevealer>();
            _allRegions = new List<IDepictionRegion>();
            StoryDetails = new StoryDetails();
        }

        public Story(IInteractionRuleRepository interactionRulesForStory)
            : this()
        {
            if (interactionRulesForStory == null)
            {
                //Using the interaction rule instance
                //DepictionAccess.MessageService.AddStoryMessage("No interactions are loaded for story.", 3);
                return;
            }
            interactionRuleRepository = interactionRulesForStory;
            router = new Router(this, interactionRuleRepository);
        }

        protected override void OnDispose()
        {
            if (router != null) router.Dispose();
            allImages.Clear();
            allElements.Clear();
            allAnnotations.Clear();
            mainDisplayer.Dispose();
            foreach (var rev in revealerDisplayers)
            {
                rev.DisplayedElementsChangedIdList -= revealer_DisplayedElementsChangedIdList;
                rev.PropertyChanged -= revealer_PropertyChanged;
                rev.Dispose();
            }
            revealerDisplayers.Clear();

            base.OnDispose();
        }
        #endregion
        #region map region controls
        public void AddRegion(IDepictionRegion newRegion)
        {
            _allRegions.Add(newRegion);
            TriggerRegionCollectionChange(NotifyCollectionChangedAction.Add, new[] { newRegion });
        }
        public void RemoveRegion(IDepictionRegion regionToRemove)
        {
            if (_allRegions.Remove(regionToRemove))
            {

                TriggerRegionCollectionChange(NotifyCollectionChangedAction.Remove, new[] { regionToRemove });
            }
        }
        private void TriggerRegionCollectionChange(NotifyCollectionChangedAction action, IList items)
        {
            if (RegionCollectionChanged != null)
            {
                RegionCollectionChanged(this, new NotifyCollectionChangedEventArgs(action, items));
            }
        }
        #endregion
        #region story change trigger
        void TriggerStoryChange()
        {
            if (StoryChanged != null)
            {
                StoryChanged(this, null);
            }
        }
        #endregion
        #region story image stuff
        //not null check in the method input
        public void SetStoryImages(DepictionImageResourceDictionary imageDictionary)
        {
            allImages = imageDictionary;
        }

        #endregion
        #region interaction manipulation
        
        public void InitializeInteractionRouter()
        {
            router = new Router(this, interactionRuleRepository);
        }

        public void LoadNonDefaultInteractions(IList<IInteractionRule> nonDefaultRules)
        {
            interactionRuleRepository.LoadNonDefaultRulesIntoRepository(nonDefaultRules);
        }

        #endregion
        
        #region spatial reference and location info methods
        Func<IElementProperty, bool> isCoverage = delegate(IElementProperty p) { return p.Value is ICoverage; };

        //        public Point ConvertProjectedToGeo(double projX, double projY)
        //        {
        //            return StoryProjector.DepictionPCSToGCS(projX, projY);
        //        }
        //
        //        public Point GetProjectedPoint(double geoX, double geoY)
        //        {
        //            if (StoryProjector == null) return new Point(Double.NaN, Double.NaN);
        //            var p = StoryProjector.DepictionPCSToGCS(geoX, geoY);
        //            return p;
        //        }

        public string InformationForGeographicLocation(double latitude, double longitude)
        {
            //            if (StoryProjector == null)
            //            {
            //                //i guess this would mean everything is the default geographic coordinate, hopefully
            //                throw new Exception("There is no projection extension!");
            //            }
            //            var geoLoc = StoryProjector.DepictionPCSToGCS(longitude, latitude);
            var dataString = string.Empty;
            dataString += DepictionToStringService.LatLongToString(latitude, longitude);
            var elems = AllElements;
            foreach (var elem in elems)
            {
                var props = elem.GetMatchingProps(isCoverage);
                foreach (var prop in props)
                {
                    var coverage = prop.Value as ICoverage;
                    if (coverage != null) dataString += coverage.GetValueForDisplay(new Point(longitude, latitude), Settings.Default.MeasurementSystem, Settings.Default.Precision);
                }
            }
            return dataString;
        }

        public void ChangeDisplayCoordinateSystem(string newDisplayCoordinateSystem)
        {
            if (_displayCoordinateSystem.Equals(newDisplayCoordinateSystem, StringComparison.CurrentCultureIgnoreCase)) return;
            router.EndAsync();

            _displayCoordinateSystem = newDisplayCoordinateSystem;
            CoordinateSystemService.Instance.SetStoryCoordinateAndDisplaySystem(ElementCoordinateSystem, DisplayCoordinateSystem);

            OnPropertyChanged("Projection");
            router.BeginAsync();
        }

        #endregion

        #region Displayer and revealer

        /// <summary>
        /// Creates a revealer with the rect bounds with are converted to cartesian points. Although it is unclear as to
        /// when the convserion is done.
        /// </summary>
        /// <param name="bounds"></param>
        public void AddNewRevealer(ICartRect bounds)
        {
            var newRevealer = new Revealer("New Revealer");
            newRevealer.GeoRectBounds = bounds;
            LoadRevealer(newRevealer);
        }

        public void LoadRevealer(IElementRevealer revealer)
        {
            revealerDisplayers.Add(revealer);
            revealer.DisplayedElementsChangedIdList += revealer_DisplayedElementsChangedIdList;
            revealer.PropertyChanged += revealer_PropertyChanged;
            TriggerRevealerCollectionChange(NotifyCollectionChangedAction.Add, new[] { revealer });
        }

        void revealer_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            TriggerStoryChange();
        }

        void revealer_DisplayedElementsChangedIdList(object sender, NotifyCollectionChangedEventArgs e)
        {
            TriggerStoryChange();
        }

        public void DeleteRevealer(IElementRevealer revealer)
        {
            if (revealerDisplayers.Remove(revealer))
            {
                revealer.DisplayedElementsChangedIdList -= revealer_DisplayedElementsChangedIdList;
                revealer.PropertyChanged -= revealer_PropertyChanged;
                TriggerRevealerCollectionChange(NotifyCollectionChangedAction.Remove, new[] { revealer });
            }
        }

        public void DeleteRevealerWithId(string revealerId)
        {

        }

        private void TriggerRevealerCollectionChange(NotifyCollectionChangedAction action, IList items)
        {
            if (RevealerCollectionChanged != null)
            {
                RevealerCollectionChanged(this, new NotifyCollectionChangedEventArgs(action, items));
            }
        }

        #endregion

        #region element manipulation
        //To color things, but that should be done beforehand.
        //        if (geom != null && (geom.GeometryType.Equals(DepictionGeometryType.Polygon) || geom.GeometryType.Equals(DepictionGeometryType.MultiPolygon)))
        //                    {
        //                        matching.ZOIFillColor = ShapeCreationService.SevenColors[count % 7];
        //                    }
        //        System.Windows.Application.Current.Dispatcher.Invoke(
        //    System.Windows.Threading.DispatcherPriority.Normal,
        //    (Action)delegate() 
        //    {
        //         // Your Action Code
        //    });
        public void UpdateRepo(List<Dictionary<string, object>> updateValues, string idPropertyName, IElementTemplate newType)
        {
            List<IElement> createdElements;
            var updateAbleElements = new Dictionary<string, IElement>();
            if (!string.IsNullOrEmpty(idPropertyName))
            {
                //brute force non linq way of getting elements that are updatable
                foreach (var element in AllElements)
                {
                    var prop = element.GetDepictionProperty(idPropertyName);
                    if (prop == null) continue;
                    var propValue = prop.Value.ToString();
                    if (string.IsNullOrEmpty(propValue)) continue;
                    updateAbleElements.Add(propValue, element);
                }
            }

            createdElements = new List<IElement>();
            foreach (var gp in updateValues)
            {
                if (string.IsNullOrEmpty(idPropertyName))
                {
                    var newElem = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(gp);
                    if (newElem == null) continue;
                    createdElements.Add(newElem);
                }
                else
                {
                    var matching = updateAbleElements[idPropertyName];
                    if (matching == null) continue;
                    //Maybe turn off interactions
                    ElementAndElemTemplateService.AddOrUpdateElementWithPropertyDictionary(matching, gp);
                }
            }
            AddElements(createdElements);
            MainDisplayer.AddDisplayElementsWithMatchingIds(createdElements.Select(t => t.ElementId).ToList());
        }

        public HashSet<string> GetElementIdsInRect(ICartRect bounds)
        {
            //So annoying that a windows Rect needs to be used
            var wpfRect = LocationConverter.ConvertCartesianToDrawRect(bounds);
            return new HashSet<string>(allElements.GetItemsIntersecting(wpfRect).Select(t => t.ElementId));
        }

        public IEnumerable<IElement> GetElementsInGeometry(IDepictionGeometry enclosingGeometry)
        {
            //because of the stupid coordinate system inversion, the rect has to be converted
            //pretty that is a serious problem, but for now need to make things work
            var rectBounds = enclosingGeometry.RectBounds;
            var usableWPFBounds = rectBounds.WpfRect;
            //a quick pass
            var firsPass = allElements.GetItemsIntersecting(usableWPFBounds);
            //now brute force
            foreach (var element in firsPass)
            {
                if (enclosingGeometry.Contains(element.ElementGeometry))
                {
                    yield return element;
                }
            }
        }

        public void AddElement(IElement element)
        {
            AddElement(element, false);
        }

        public void AddElement(IElement element, bool displayInMain)
        {
            AddElements(new List<IElement> { element });
            if (displayInMain && mainDisplayer != null)
            {
                if (element.GeoLocation != null) mainDisplayer.AddDisplayElementsWithMatchingIds(new[] { element.ElementId });
                else OnPropertyChanged("ShowManageNonGeo");//kind of hacky way to open the manage content
            }
        }
        //Used when loading a story, for now at least. move to an extension method?
        public void LoadElements(List<IElement> elementList)
        {
            if (allElements == null)
            {
                allElements = new ModQuadTree<IElement>();
            }
            var added = allElements.AddElements(elementList);
            foreach (var element in added)
            {
                ElementEventConnector(element, true);
            }
        }
        public void AddElements(List<IElement> elementList, bool triggerBehaviors)
        {
            if (allElements == null)
            {
                allElements = new ModQuadTree<IElement>();
            }
            var added = allElements.AddElements(elementList);
            foreach (var element in added)
            {
                ElementEventConnector(element, true);
                if (triggerBehaviors)
                {
                    if (element.PostAddActions == null) continue;
                    foreach (var action in element.PostAddActions)
                    {
                        var key = action.Key;
                        var behaviours =
                            ExtensionService.Instance.DepictionBehaviors.Where(t => t.Metadata.Name.Equals(key)).ToList();
                        if (behaviours.Any())
                        {
                            foreach (var behavior in behaviours)
                            {
                                if (behavior != null)
                                {
                                    var behaviorParams = LegacyHelpers.BuildParametersForPostSetActions(action.Value);
                                    behavior.Value.DoBehavior(element, behaviorParams);
                                }
                            }
                        }
                    }
                }

            }

            TriggerElementCollectionChange(NotifyCollectionChangedAction.Add, elementList);
        }
        /// <summary>
        /// Add elements and trigger behaviors
        /// </summary>
        /// <param name="elementList"></param>
        public void AddElements(List<IElement> elementList)
        {
            AddElements(elementList, true);
        }

        public void RemoveElement(IElement element)
        {
            RemoveElements(new List<IElement> { element });
        }

        public void RemoveElementsById(List<string> elementIds)
        {
            var toDelete = from id in elementIds
                           from e in allElements
                           where e.ElementId.Equals(id)
                           select e;
            RemoveElements(toDelete.ToList());
        }
        //For now associate elements are dealt with in the story remove, although this could be
        //really time consuming once the number of elements goes up
        public void RemoveElements(List<IElement> elementList)
        {
            if (elementList == null) return;
            //Find the connection between associated etc, what a mess hack central, i take that back, race condition central
            //this could get very very bad because elements will get deleted and interactions will trigger in the middle of 
            //all the deleting and disconnecting
            var toDeleteElementsWithAssociates = elementList.Where(t => t.AssociatedElements.Any());//Remove the associated element too?

            if (allElements == null) return;
            var removed = allElements.RemoveElements(elementList).ToList();
            if (!removed.Any()) return;

            foreach (var element in removed)
            {
                element.TriggerPropertyChange("Deleted");//Hack to simplify? associate element connection removal
                element.DetachAllAssociates();
                ElementEventConnector(element, false);
            }
            TriggerElementCollectionChange(NotifyCollectionChangedAction.Remove, removed);
        }

        public void ClearElements()
        {
            if (allElements == null) return;
            foreach (var element in allElements)
            {
                ElementEventConnector(element, false);
            }
            allElements.Clear();
            TriggerElementCollectionChange(NotifyCollectionChangedAction.Reset, null);
        }

        private void TriggerElementCollectionChange(NotifyCollectionChangedAction action, IList items)
        {
            if (ElementCollectionChanged != null)
            {
                ElementCollectionChanged(this, new NotifyCollectionChangedEventArgs(action, items));
            }
            TriggerStoryChange();
        }

        private void ElementEventConnector(IElement element, bool connect)
        {
            if (connect)
            {
                element.PropertyChanged += element_PropertyChanged;
                element.GeoLocationChange += element_GeoLocationChange;
            }
            else
            {
                element.PropertyChanged -= element_PropertyChanged;
                element.GeoLocationChange -= element_GeoLocationChange;
            }
        }

        private void AddAndRemoveElementFromQuadTree(IElement changed)
        {
            if (changed == null) return;
            allElements.RemoveElements(new[] { changed });
            allElements.AddElements(new[] { changed });
        }

        void element_GeoLocationChange(object sender, GeoLocationEventArgs e)
        {
            //Remove then add into the quad tree.
            //            AddAndRemoveElementFromQuadTree(sender as IElement);//i think only geometry listening is needed, actually that is a total lie
            //moving the element does affect the quad tree stuff, but going to wait until things break before doing antyhing
            GenericPropertyNotifier(sender as IElement, "GeoLocation");
            if (ElementGeoLocationChanged != null)
            {
                ElementGeoLocationChanged(sender, e);
            }
        }

        void element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("ElementGeometry"))
            {
                AddAndRemoveElementFromQuadTree(sender as IElement);
            }
            GenericPropertyNotifier(sender as IElement, e.PropertyName);
        }

        void GenericPropertyNotifier(IElement element, string propName)
        {
            if (ElementPropertyChanged != null)
            {
                ElementPropertyChanged(this, new PropertyChangeEventArg(element, propName));
            }
            TriggerStoryChange();
        }
        #endregion

        #region annotation stuff

        public void AddAnnotations(List<IDepictionAnnotation> annotations)
        {
            if (allAnnotations == null) allAnnotations = new List<IDepictionAnnotation>();
            allAnnotations.AddRange(annotations);
            TriggerAnnotChange(NotifyCollectionChangedAction.Add, annotations);
        }

        public void AddAnnotationAtLocation(double x, double y)
        {
            var annot = new DepictionAnnotation { GeoLocation = new Point(x, y) };
            allAnnotations.Add(annot);
            TriggerAnnotChange(NotifyCollectionChangedAction.Add, new[] { annot });
        }

        public void DeleteAnnotation(IDepictionAnnotation annotation)
        {
            if (allAnnotations.Remove(annotation))
            {
                TriggerAnnotChange(NotifyCollectionChangedAction.Remove, new[] { annotation });
            }
        }

        private void TriggerAnnotChange(NotifyCollectionChangedAction action, IList items)
        {
            if (AnnotationCollectionChanged != null)
            {
                AnnotationCollectionChanged(this, new NotifyCollectionChangedEventArgs(action, items));
            }
        }

        private void ResetAnnotations(List<IDepictionAnnotation> newAnnotations)
        {
            allAnnotations = newAnnotations;
            TriggerAnnotChange(NotifyCollectionChangedAction.Reset, null);
        }

        #endregion

        #region Location stuff

        private Point _focalPointLocation;
        private double _zoom = 1d;

        [DataMember]
        public Point StoryFocalPoint
        {
            get { return _focalPointLocation; }
            set { _focalPointLocation = value; OnPropertyChanged(() => StoryFocalPoint); TriggerStoryChange(); }
        }

        public IEnumerable<IDepictionRegion> AllRegions
        {
            get { return _allRegions; }
        }

        [DataMember]
        public double StoryZoom
        {
            get { return _zoom; }
            set { _zoom = value; OnPropertyChanged(() => StoryZoom); TriggerStoryChange(); }
        }
        public void ShiftStoryExtent(Vector direction)
        {
            StoryFocalPoint -= direction;
        }
        public void SetCenter(Point newCenter)
        {
            StoryFocalPoint = newCenter;
        }

        public void AdjustZoom(double multiplier, Point? zoomCenter)
        {
            StoryZoom *= multiplier;
            if (zoomCenter != null)
            {
                var point = (Vector)zoomCenter;
                StoryFocalPoint = (Point)point;
            }
        }

        public void ShiftStoryExtent(PanDirection direction)
        {
            var shiftVector = new Vector(1, 1);
            switch (direction)
            {
                case PanDirection.Down:
                    shiftVector = new Vector(0, -10);
                    break;
                case PanDirection.DownFine:
                    shiftVector = new Vector(0, -1);
                    break;
                case PanDirection.Up:
                    shiftVector = new Vector(0, 10);
                    break;
                case PanDirection.UpFine:
                    shiftVector = new Vector(0, 1);
                    break;
                case PanDirection.Left:
                    shiftVector = new Vector(10, 0);
                    break;
                case PanDirection.LeftFine:
                    shiftVector = new Vector(1, 0);
                    break;
                case PanDirection.Right:
                    shiftVector = new Vector(-10, 0);
                    break;
                case PanDirection.RightFine:
                    shiftVector = new Vector(-1, 0);
                    break;
            }
            shiftVector = new Vector(shiftVector.X / _zoom, shiftVector.Y / _zoom);
            ShiftStoryExtent(shiftVector);
        }

        public void Zoom(ZoomDirection direction, Point? zoomCenter)
        {
            var oneLineDelta = 120;// Mouse.MouseWheelDeltaForOneLine;
            var delta = 120;
            switch (direction)
            {
                case ZoomDirection.Out:
                    delta = -120;
                    break;
                case ZoomDirection.In:
                    delta = 120;
                    break;
                case ZoomDirection.OutFine:
                    delta = -120;
                    oneLineDelta = 60;
                    break;
                case ZoomDirection.InFine:
                    delta = 120;
                    oneLineDelta = 60;
                    break;
            }
            var x = Math.Pow(2, delta / 3.0 / oneLineDelta);
            AdjustZoom(x, null);
        }

        #endregion

        #region geo information

        ICartRect _visExtent;
        //Does not trigger a story change, since this depends on the size of thew main window.
        public ICartRect VisibleExtent
        {
            get { return _visExtent; }
            set
            {
                _visExtent = value;
                geoInformation.MapViewBounds = value;
                Debug.WriteLine(StoryFocalPoint + " the top left");
                Debug.WriteLine(_visExtent + " the extent");
            }
        }

        //        private IDepictionRegion _primaryRegion;
        //        public IDepictionRegion PrimaryRegion
        //        {
        //            get { return _primaryRegion; }
        //            set
        //            {
        //                if (Equals(_primaryRegion, value)) return;
        //                _primaryRegion = value;
        //                geoInformation.RegionBounds = new CartRect(value.GeoRectBounds, true);
        //                OnPropertyChanged("PrimaryRegion");
        //                TriggerStoryChange();
        //            }
        //        }

        #endregion
    }
}