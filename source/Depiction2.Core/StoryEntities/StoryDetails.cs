﻿using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
    public class StoryDetails : IStoryDetails
    {
        private string _version = "0.0";

        private string _author = string.Empty;

        private string _description = "None";

        private string _title = "Generic Story";

        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #region constructor

        #endregion
    }
}