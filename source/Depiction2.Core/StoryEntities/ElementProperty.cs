﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Depiction2.API.Converters;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.ValidationRules;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class ElementProperty : ModelBase, IElementProperty
    {
        #region variables
        private object propVal = null;
        private Type propType = null;

        #endregion

        public bool IsReadOnly { get { return false; } }
        [DataMember]
        public string ProperName { get; private set; }
        [DataMember]
        public string DisplayName { get; set; }
        //If it gets returned by reference, bad thigns can happen
        [DataMember]
        public object Value
        {
            get { return propVal; }
            private set
            {
                propVal = value;
                if (value != null)
                {
                    propType = value.GetType();
                }
            }
        }

        public Type ValueType
        {
            get { return propType; }
        } 
       
        public bool TrySetValueFromString(string stringValue)
        {
            var convVal = DepictionTypeConverter.ChangeType(stringValue, ValueType);
            if (convVal == null) return false;
            SetValue(convVal);
            return true;
        }

        public bool SetValue(object newValue, bool triggerEvent)
        {
            if (newValue == null)
            {
                return false;
                //Do something because this would be bad
            }
            if (newValue.Equals(propVal)) return false;

            propVal = newValue;
            if (propType == null || typeof(string) == propType)
            {
                propType = propVal.GetType();
            }
            if (triggerEvent) OnPropertyChanged("Value");
            return true;
        }
        public bool SetValue(object newValue)
        {
           return SetValue(newValue, true);
        }

        #region legacy
        private Dictionary<string, string[]> postSetActions = new Dictionary<string, string[]>();
        private IValidationRule[] _legacyValidationRules = new IValidationRule[0];
        [DataMember]
        public IValidationRule[] LegacyValidationRules
        {
            get { return _legacyValidationRules; }
            set { _legacyValidationRules = value; }
        }
        [DataMember]
        public Dictionary<string, string[]> LegacyPostSetActions
        {
            get { return postSetActions; }
            set { postSetActions = value; }
        }

        #endregion

        #region constructor
        protected ElementProperty()
        {
            
        }
        public ElementProperty(string properName,string displayName)
        {
            ProperName = properName;
            DisplayName = displayName;
        }
        public ElementProperty(string properName, string displayName, object value)
        {
            ProperName = properName;
            DisplayName = displayName;
            propVal = value;
            if (value != null)
            {
                propType = value.GetType();
            }
        }
        internal ElementProperty(string properName) : this(properName, typeof(string)) { }
        public ElementProperty(string properName, Type type)
        {
            ProperName = properName;
            DisplayName = properName;
            if (type == null)
            {
                //throw some sort of exception
                throw new NullReferenceException("Cannot create an element property with null type");
            }
            propType = type;
            try
            {
                if (type == typeof(string))
                {
                    propVal = string.Empty;
                }
                else
                {
                    propVal = Activator.CreateInstance(type);
                }
            }
            catch
            {
                throw new NullReferenceException(string.Format("Cannot have a null property value for type {0}", type));
            }
        }
        #endregion


        public override string ToString()
        {
            return string.Format("{0}, {1} : {2}", ProperName, DisplayName, propVal);
        }
    }


}