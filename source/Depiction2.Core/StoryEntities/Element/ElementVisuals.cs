﻿namespace Depiction2.Core.StoryEntities.Element
{
    public enum DepictionIconSize
    {
        Lilliputian=12,
        Tiny=16,
        Small=20,
        Medium=24,
        Large=32,
        ExtraLarge=38
    }
    public enum DepictionIconBorderShape
    {
        None, Circle, Triangle, Square
    }
    //Simplified zoi types used to determine how to add a polygon, default is Point
    public enum ZOIShapeType
    {
        Unknown, Point, Line, UserLine, Polygon, UserPolygon
    }
}