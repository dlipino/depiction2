﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities
{
    [DataContract]
    public class DepictionAnnotation : ModelBase, IDepictionAnnotation
    {
        private double pixelWidth = 200;
        private double pixelHeight = 50;
        private double contentLocationX = 20;
        private double contentLocationY = 25;
        private double iconSize = 20;
        private bool isTextCollapsed;
        private string backgroundColor = "White";
        private string foregroundColor = "Black";
        private string annotationText = "Enter annotation text here.";
        private Point _geoLocation;
        #region Properties
        [DataMember]
        public Point GeoLocation
        {
            get { return _geoLocation; }
            set
            {
                _geoLocation = value;
                OnPropertyChanged(() => GeoLocation);
            }
        }

        [DataMember]
        public string AnnotationID { get; private set; }

        [DataMember]
        public string AnnotationText
        {
            get { return annotationText; }
            set { annotationText = value; OnPropertyChanged(() => AnnotationText); }
        }

        [DataMember]
        public double ContentLocationX
        {
            get { return contentLocationX; }
            set { contentLocationX = value; OnPropertyChanged(() => ContentLocationX); }
        }

        [DataMember]
        public double ContentLocationY
        {
            get { return contentLocationY; }
            set { contentLocationY = value; OnPropertyChanged(() => ContentLocationY); }
        }

        [DataMember]
        public double PixelWidth
        {
            get { return pixelWidth; }
            set { pixelWidth = value; OnPropertyChanged(() => PixelWidth); }
        }

        [DataMember]
        public double PixelHeight
        {
            get { return pixelHeight; }
            set { pixelHeight = value; OnPropertyChanged(() => PixelHeight); }
        }

        public double IconSize
        {
            get { return iconSize; }
        }

//        [DataMember]
//        public bool IsAnnotationVisible { get; set; }
        [DataMember, DefaultValue(double.NaN)]
        public double ScaleWhenCreated { get; set; }
        [DataMember]
        public bool IsTextCollapsed
        {
            get { return isTextCollapsed; }
            set { isTextCollapsed = value; OnPropertyChanged(() => IsTextCollapsed); }
        }

        [DataMember]
        public string BackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; OnPropertyChanged(() => BackgroundColor); }
        }

        [DataMember]
        public string ForegroundColor
        {
            get { return foregroundColor; }
            set { foregroundColor = value; OnPropertyChanged(() => ForegroundColor); }
        }
        #endregion
        #region constructor
        public DepictionAnnotation()
        {
            AnnotationID = Guid.NewGuid().ToString();
            if(DepictionAccess.DStory != null)
            {
                ScaleWhenCreated = DepictionAccess.DStory.StoryZoom;
            }
        }
        //Current zoom level doesnt mean anything yet
        public DepictionAnnotation(double currentZoomLevel)
        {
            AnnotationID = Guid.NewGuid().ToString();
            ScaleWhenCreated = currentZoomLevel;
        }
        #endregion

        public override bool Equals(Object obj)
        {
            return Equals(obj as IDepictionAnnotation);
        }

        public bool Equals(IDepictionAnnotation p)
        {
            // If parameter is null return false:
            if (p == null)
            {
                return false;
            }
            if (!Equals(ForegroundColor, p.ForegroundColor)) return false;
            if (!Equals(BackgroundColor, p.BackgroundColor)) return false;
            if (!Equals(GeoLocation, p.GeoLocation)) return false;
            if (!Equals(AnnotationText, p.AnnotationText)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32(ForegroundColor) ^ Convert.ToInt32(BackgroundColor) ^ Convert.ToInt32(AnnotationText);
        }
    }
}