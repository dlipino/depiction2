﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.ValidationRules;

namespace Depiction2.Core.StoryEntities.ElementTemplates
{
    [DataContract]
    public class PropertyTemplate : IPropertyTemplate
    {
        [DataMember,DefaultValue("")]
        public string ProperName { get; set; }
        [DataMember, DefaultValue("")]
        public string DisplayName { get; set; }
        [DataMember, DefaultValue("")]
        public string TypeString { get; set; }
        [DataMember, DefaultValue("")]
        public string ValueString { get; set; }

        [DataMember]
        public IValidationRule[] LegacyValidationRules { get; set; }
        [DataMember]
        public Dictionary<string, string[]> LegacyPostSetActions { get; set; }

        #region constructor

        public PropertyTemplate()
        {
            ProperName = "";
            DisplayName = "";
            TypeString = "";
            ValueString = "";
            LegacyPostSetActions = new Dictionary<string, string[]>();
            LegacyValidationRules = new IValidationRule[0];
        }
        #endregion

        public override string ToString()
        {
            return string.Format("{0},{1}", ProperName,DisplayName);
        }
    }
}