﻿using Depiction2.Base.Geo;

namespace Depiction2.Core.StoryEntities.ElementTemplates
{
    public class GeoImageTemplate
    {
        public string ImageName { get; set; }
        public ICartRect Bounds { get; set; }
        public double Rotation { get; set; }
    }
}