﻿using System.Collections.Generic;
using System.IO;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Newtonsoft.Json;

namespace Depiction2.Core.StoryEntities.ElementTemplates
{
    public class ElementTemplateLibraryFilter : IElementTemplateLibraryFilter
    {
        public const string LibraryFilterExtension = ".etf";
        private HashSet<string> _templateNames = new HashSet<string>();
        public string FilterName { get; set; }

        public IEnumerable<string> ElementTemplateNames
        {
            get { return _templateNames; }
        }

        public bool AddElementType(string name)
        {
            return _templateNames.Add(name);
        }

        public bool RemoveElementType(string name)
        {
            return _templateNames.Remove(name);
        }

        public void ClearElements()
        {
            _templateNames.Clear();
        }

        public bool SaveFilter()
        {
            //TODO set the correct default save dir
            var saveDir = string.Empty;
            if(!Directory.Exists(saveDir)) return false;
            var jsonS = JsonConvert.SerializeObject(this);
            var cleanName = FilterName.Trim();
            var filterFileName = Path.Combine(saveDir, cleanName + LibraryFilterExtension);
            File.WriteAllText(filterFileName, jsonS);
            return true;
        }
    }
}