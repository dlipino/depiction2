﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction2.API;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Newtonsoft.Json;

namespace Depiction2.Core.StoryEntities.ElementTemplates
{
    public class ElementTemplateLibrary : ModelBase, IElementTemplateLibrary
    {

//        public void LoadElementDefinitions(string productResourceAssembly)
//        {
//            prototypeLibrary = DepictionAccess.ElementLibrary;//Not sure why this is here, probably so things don't get overwritten.
//            //Load autodetect prototype
//            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory("Depiction.CoreModel", "dml", true, PathService.DepictionElementPath);
//            //Load the elements for the product type
//            if (string.IsNullOrEmpty(productResourceAssembly))
//            {
//                productResourceAssembly = "Resources.Product.Depiction";
//            }
//            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(productResourceAssembly, "dml", true, PathService.DepictionElementPath);
//
//            prototypeLibrary.SetDefaultPrototypesFromPath(PathService.DepictionElementPath, false);
//            prototypeLibrary.SetUserPrototypesFromPath(PathService.UserElementsDirectoryPath, false);
//            prototypeLibrary.SetAddinPrototypesAndIcons(false);
//
//        }

        #region hack, some helper constants

        public const string StorySource = "Loaded Story";
        public const string ProductSource = "Default";

        #endregion

        List<IElementTemplate> _productElementTemplates = new List<IElementTemplate>();
        List<IElementTemplate> _storyElementTemplates = new List<IElementTemplate>();
        List<IElementTemplateLibraryFilter> _libraryFilters = new List<IElementTemplateLibraryFilter>();

        public IEnumerable<IElementTemplateLibraryFilter> LibraryFilters
        {
            get { return _libraryFilters; }
        }

        public IEnumerable<IElementTemplate> ProductElementTemplates
        {
            get { return _productElementTemplates; }
        }
        public IEnumerable<IElementTemplate> LoadedStoryElementTemplates
        {
            get { return _storyElementTemplates; }
        }

        public IElementTemplate FindElementTemplate(string scaffoldType)
        {
            var scaffold =
                _productElementTemplates.FirstOrDefault(
                    t => t.ElementType.Equals(scaffoldType, StringComparison.InvariantCultureIgnoreCase));
            if (scaffold == null)
            {
                scaffold = _storyElementTemplates.FirstOrDefault(
                     t => t.ElementType.Equals(scaffoldType, StringComparison.InvariantCultureIgnoreCase));
            }

            return scaffold;
        }

        public void AddStoryElementTemplates(List<IElementTemplate> loadedStoryElementTemplates)
        {
            var nonDefaultElemnts = loadedStoryElementTemplates.Except(_productElementTemplates, new ElementTemplateTypeComparer<IElementTemplate>());
            _storyElementTemplates.AddRange(nonDefaultElemnts);
        }

        #region constructor

        public ElementTemplateLibrary()
        {
            ResetLibraryWithDefaults();
            UpdateFiltersFromDefaultLocation();
        }

        #endregion

        #region protected helpers

        protected void ResetLibraryWithDefaults()
        {
            _libraryFilters.Clear();
            var defaultFilter = new ElementTemplateLibraryFilter();
            defaultFilter.FilterName = "All Elements";
            _libraryFilters.Add(defaultFilter);
        }

        #endregion

        public void UpdateFiltersFromDefaultLocation()
        {
            if (DepictionAccess.PathService == null) return;
            var baseDir = DepictionAccess.PathService.AppDataDirectoryPath;
            _libraryFilters.AddRange(LoadFiltersFromDir(baseDir));
            OnPropertyChanged("LibraryFilters");
        }
        public void AddElementTemplatesToDefault(IEnumerable<IElementTemplate> templates)
        {
            var nonDefaultElemnts = templates.Except(_productElementTemplates, new ElementTemplateTypeComparer<IElementTemplate>());
            _productElementTemplates.AddRange(nonDefaultElemnts);
        }

        protected override void OnDispose()
        {
            _productElementTemplates = new List<IElementTemplate>();
        }
        #region static load helpers
        static public IEnumerable<IElementTemplateLibraryFilter> LoadFiltersFromDir(string searchDirectory)
        {
            //Todo: Get the correct search query, i assume it is a regex
            var filters = new List<IElementTemplateLibraryFilter>();
            if (!Directory.Exists(searchDirectory)) return filters;
            var filesWithCorrectExtension = Directory.GetFiles(searchDirectory,
                string.Format("*.{0}", ElementTemplateLibraryFilter.LibraryFilterExtension),
                                         SearchOption.TopDirectoryOnly);

            foreach (var filterFile in filesWithCorrectExtension)
            {
                var filter = JsonConvert.DeserializeObject<ElementTemplateLibraryFilter>(filterFile);
                if (filter == null) continue;
                filters.Add(filter);
            }

            return filters;
        }
        #endregion
    }
}