﻿using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities.StoryInformation
{
    public class StoryGeoInformation : ModelBase,IStoryGeoInformation
    {
        #region variables

        private ICartRect _regionBounds = new CartRect();
        private ICartRect _mapViewBounds= new CartRect();
        private Point _mapViewCenter;

        #endregion

        #region properties
        public ICartRect RegionBounds
        {
            get { return _regionBounds; }
            set { _regionBounds = value; }
        }

        //From 1.4 loads/saves
        public ICartRect MapViewBounds
        {
            get { return _mapViewBounds; }
            set { _mapViewBounds = value; }
        }

        public Point MapViewCenter
        {
            get { return _mapViewCenter; }
            set { _mapViewCenter = value; }
        }
        #endregion

    }
}