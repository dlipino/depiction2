﻿using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.StoryEntities.StoryInformation
{
    public class StoryFileInformation: IDepictionFileFormat
    {
        public string FileVersion { get; set; }
        public string AppVersion { get; set; }
    }
}