﻿using Depiction2.Base.Product;

namespace Depiction2.Core.Product
{
    public class Depiction2Information : ProductInformationBase
    {
        public override string ProductType { get { return Release14; } }

        public override string ProductAppDirectoryName { get { return "Depiction2"; } }
        public override string ProductName { get { return "Depiction"; } }
        public override string FullAppVersion { get { return VersionInfo.FullAppVersion; } }

        public override string ResourceAssemblyName { get { return string.Empty; } }
        public override string SplashScreenPath { get { return string.Empty; } }
        public override string ThemeLocation { get { return string.Empty; } }

    }
}