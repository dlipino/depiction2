﻿using Depiction2.Base.Product;

namespace Depiction2.Core.Product
{
    public class Depiction2UnitTest : ProductInformationBase
    {
        public override string ProductType { get { return Test; } }

        public override string ProductAppDirectoryName { get { return "Depiction2_UnitTest"; } }
        public override string ProductName { get { return "DepictionUT"; } }
        public override string FullAppVersion { get { return VersionInfo.FullAppVersion; } }

        public override string ResourceAssemblyName { get { return string.Empty; } }
        public override string SplashScreenPath { get { return string.Empty; } }
        public override string ThemeLocation { get { return string.Empty; } }
//        public override string DefaultIcons { get { return "DefaultIconDictionary.xaml"; } }//Not needed since it comes with the theme stuff

    }
}