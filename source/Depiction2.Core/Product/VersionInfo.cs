using System.Reflection;

namespace Depiction2.Core.Product
{
    /// <summary>
    /// Version information for this instance of Depiction.
    /// </summary>
    public static class VersionInfo
    {
        // DO NOT EDIT THESE LINES. They are modified by nant. 
        private const string VERSION_NUMBER = "@DEBUG_VERSION@"; //  = "DEBUG_VERSION";
        private const string BUILD_NUMBER = "@DEBUG_NUMBER@"; //e  = "DEBUG_NUMBER";
        private const string BUILD_TYPE = "@DEV@";// = "Dev";
        // END OF NANT STUFF

        public const string ProductTitle = "Depiction 2 Alpha";

        public static string FullAppVersion
        {
            get
            {
                var version = VERSION_NUMBER + "." + BUILD_NUMBER;
                if (BUILD_TYPE.Equals("Dev"))
                {
                    version += "_" + BUILD_TYPE;
                }
                return version;
            }
        }
        public static string SimpleAppVersion
        {
            get
            {
                var version = VERSION_NUMBER;
                if (BUILD_TYPE.Equals("Dev"))
                {
                    version += "_" + BUILD_TYPE;
                }
                return version;
            }
        }
//        public static string AppVersionForQS
//        {
//            get
//            {
//                switch (DepictionAccess.ProductInformation.ProductType)
//                {
//                    case ProductInformationBase.Prep:
//                        return "Prep";
//                    default:
//                        return VERSION_NUMBER;
//                }
//            }
//        }
        /// <summary>
        /// The version number presented to the user.
        /// </summary>
        public static string AppVersion
        {
            get
            {
                return VERSION_NUMBER;
            }
        }

        /// <summary>
        /// The version number presented to the user, with " Trial" appended if isTrialVersion = true.
        /// </summary>
        public static string FullVersion(bool isTrialVersion)
        {
            if (isTrialVersion)
                return string.Format("{0} Trial", AppVersion);
            return AppVersion;
        }

        /// <summary>
        /// The internal version number as major.minor.build.
        /// </summary>
        public static string MajorMinorBuild
        {
            get
            {
                return string.Format("{0}.{1}.{2}", MajorNumber, MinorNumber, BuildNumber);
            }
        }

        /// <summary>
        /// The version's major number.
        /// </summary>
        public static int MajorNumber
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Major;
            }
        }

        /// <summary>
        /// The version's minor number.
        /// </summary>
        public static int MinorNumber
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.Minor;
            }
        }

        /// <summary>
        /// The number of the build that produced this instance of Depiction.
        /// </summary>
        public static string BuildNumber
        {
            get
            {
                return BUILD_NUMBER;
            }
        }
        public static string BuildType
        {
            get
            {
                return BUILD_TYPE;
            }
        }
    }
}