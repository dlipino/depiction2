﻿using System;
using Depiction2.API.Properties;
using Depiction2.Base.Geo;
using Depiction2.Utilities;

namespace Depiction2.Core.Service
{
    public class DepictionToStringService
    {
        static LatLongToStringUtility latlonToString = new LatLongToStringUtility();

        static public string LatLongToString(double lat, double lon)
        {
            var separator = ", ";
            switch (Settings.Default.LatitudeLongitudeFormat)
            {
                case LatitudeLongitudeFormat.ColonFractionalSeconds:
                    return latlonToString.DoColonFractionalSeconds(lat, lon, separator);
                case LatitudeLongitudeFormat.ColonIntegerSeconds:
                    return latlonToString.DoColonIntegerSeconds(lat, lon, separator);
                case LatitudeLongitudeFormat.Decimal:
                    return latlonToString.DoDecimal(lat, lon, separator);
                case LatitudeLongitudeFormat.DegreesFractionalMinutes:
                    return latlonToString.DoDegreesFractionalMinutes(lat, lon, separator);
                case LatitudeLongitudeFormat.DegreesMinutesSeconds:
                    return latlonToString.DoDegreesMinutesSeconds(lat, lon, separator, "°");
                case LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds:
                    return latlonToString.DoDegreesMinutesSeconds(lat, lon, separator, "d");
                case LatitudeLongitudeFormat.SignedDecimal:
                    return latlonToString.DoSignedDecimal(lat, lon, separator);
                case LatitudeLongitudeFormat.SignedDegreesFractionalMinutes:
                    return latlonToString.DoSignedDegreesFractionalMinutes(lat, lon, separator);
                    case LatitudeLongitudeFormat.UTM:
                    return "UTM not implemented. Please change format in settings.";
                    //check for a projection
                default:
                    return "Format error, check settings.";
            }
        }
    }
}