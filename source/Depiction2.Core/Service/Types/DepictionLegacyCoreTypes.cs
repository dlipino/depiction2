using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Xml;
using Depiction2.API.Measurement;
using Depiction2.API.ValidationRules;
using Depiction2.Core.Geo;

namespace Depiction2.Core.Service.Types
{
    //Ok this is a hassle since all the legal types have to manully inputted
    public class DepictionLegacyCoreTypes
    {
        //TODO Competes with DepictionTypeConverter, sort of
        private static Dictionary<string, Type> allowedElementPropertyTypeDictionary;
        private static Dictionary<string, Type> stringToTypeForConversion;
        private static List<Type> depictionCoreTypes = new List<Type>();
        static DepictionLegacyCoreTypes()
        {
            allowedElementPropertyTypeDictionary = new Dictionary<string, Type>();
            allowedElementPropertyTypeDictionary.Add("MinValueValidationRule", typeof(MinValueValidationRule));
            allowedElementPropertyTypeDictionary.Add("DataTypeValidationRule", typeof(DataTypeValidationRule));
            allowedElementPropertyTypeDictionary.Add("RangeValidationRule", typeof(RangeValidationRule));
            allowedElementPropertyTypeDictionary.Add("TerrainExistsValidationRule", typeof(TerrainExistsValidationRule));//i hate this one

            allowedElementPropertyTypeDictionary.Add("Area", typeof(Area));
            allowedElementPropertyTypeDictionary.Add("Distance", typeof(Distance));
            allowedElementPropertyTypeDictionary.Add("Speed", typeof(Speed));
            allowedElementPropertyTypeDictionary.Add("Temperature", typeof(Temperature));
            allowedElementPropertyTypeDictionary.Add("Volume", typeof(Volume));
            allowedElementPropertyTypeDictionary.Add("Weight", typeof(Weight));
            allowedElementPropertyTypeDictionary.Add("Color", typeof(Color));
            allowedElementPropertyTypeDictionary.Add("Bool", typeof(bool));
            allowedElementPropertyTypeDictionary.Add("Boolean", typeof(bool));
            allowedElementPropertyTypeDictionary.Add("Number", typeof(double));
            allowedElementPropertyTypeDictionary.Add("Integer", typeof(int));
            allowedElementPropertyTypeDictionary.Add("Double", typeof(double));
            allowedElementPropertyTypeDictionary.Add("Float", typeof(double));
            allowedElementPropertyTypeDictionary.Add("String", typeof(string));
            allowedElementPropertyTypeDictionary.Add("Text", typeof(string));
            allowedElementPropertyTypeDictionary.Add("Angle", typeof(Angle));
//            allowedElementPropertyTypeDictionary.Add("RoadGraph", typeof (RoadGraph));
            //            allowedElementPropertyTypeDictionary.Add("Terrain", typeof(Terrain));//is this even needed, kind of just hacked in
            #region the base depiction types, hopefully
            depictionCoreTypes.Add(typeof(Area));
            depictionCoreTypes.Add(typeof(Distance));
            depictionCoreTypes.Add(typeof(Temperature));
            depictionCoreTypes.Add(typeof(Volume));
            depictionCoreTypes.Add(typeof(Weight));
            depictionCoreTypes.Add(typeof(Speed));

            depictionCoreTypes.Add(typeof(int));
            depictionCoreTypes.Add(typeof(double));
            depictionCoreTypes.Add(typeof(string));
            depictionCoreTypes.Add(typeof(bool));
            depictionCoreTypes.Add(typeof(Color));

//            depictionCoreTypes.Add(typeof(RoadGraph));
//            depictionCoreTypes.Add(typeof(MapCoordinateBounds));
            depictionCoreTypes.Add(typeof(LatitudeLongitudeDepiction));
//            depictionCoreTypes.Add(typeof(ZOIShapeType));
//            depictionCoreTypes.Add(typeof(DepictionIconPath));
//            depictionCoreTypes.Add(typeof(DepictionIconBorderShape));

            depictionCoreTypes.Add(typeof(DataTypeValidationRule));
            depictionCoreTypes.Add(typeof(TerrainExistsValidationRule));
            depictionCoreTypes.Add(typeof(MinValueValidationRule));
            depictionCoreTypes.Add(typeof(RangeValidationRule));
            #endregion

            CreateConversionTypes();

        }
        static private void CreateConversionTypes()
        {
            stringToTypeForConversion = new Dictionary<string, Type>();
            //Longer names first to hack use contains

            stringToTypeForConversion.Add("MinValueValidationRule", typeof(MinValueValidationRule));
            stringToTypeForConversion.Add("DataTypeValidationRule", typeof(DataTypeValidationRule));
            stringToTypeForConversion.Add("RangeValidationRule", typeof(RangeValidationRule));
            stringToTypeForConversion.Add("TerrainExistsValidationRule", typeof(TerrainExistsValidationRule));//i hate this one


            stringToTypeForConversion.Add("Color", typeof(Color));
            stringToTypeForConversion.Add("Bool", typeof(bool));
            stringToTypeForConversion.Add("Boolean", typeof(bool));
            stringToTypeForConversion.Add("Number", typeof(double));
            stringToTypeForConversion.Add("Integer", typeof(int));
            stringToTypeForConversion.Add("Double", typeof(double));
            stringToTypeForConversion.Add("Float", typeof(double));
            stringToTypeForConversion.Add("String", typeof(string));
            stringToTypeForConversion.Add("Text", typeof(string));
            stringToTypeForConversion.Add("Angle", typeof(Angle));
            //Very questionable that theses are here, because they (in theory) should
            //come from an addin/extension
//            stringToTypeForConversion.Add("Terrain", typeof(TerrainCoverage));
//            stringToTypeForConversion.Add("RoadGraph", typeof(RoadGraph));
        }

        static public void ClearTypeDictionary()
        {
            allowedElementPropertyTypeDictionary.Clear();
            stringToTypeForConversion.Clear();
        }
        static public void DoFullTypeUpdateFromFileAndDefaults(string fileName)
        {
            DepictionLegacyTypeInformationSerialization.UpdateSerializationServiceTypeDictionaryWithFile(fileName);
            UpdateTypeSerializationDictionaryWithDefaultTypes();
        }
        static public void DoFullTypeUpdateFromStreamAndDefaults(XmlReader xmlReader)
        {
            DepictionLegacyTypeInformationSerialization.UpdateSerializationServiceTypeDictionaryWithStream(xmlReader);
            UpdateTypeSerializationDictionaryWithDefaultTypes();
        }

        static public void UpdateTypeSerializationDictionaryWithDefaultTypes()
        {
//            if (DepictionTypeInformationSerialization.TypeFileExists) return;
//            var existingDict = DepictionTypeInformationSerialization.SimpleTypeNameToFullNameDictionary;
//            //it should never be null
//            if(existingDict == null)
//            {
//                existingDict = DepictionTypeInformationSerialization.SimpleTypeNameToFullNameDictionary =
//                               new SerializableDictionary<string, string>();
//            }
            foreach (var type in depictionCoreTypes)
            {
//                var simpleName = type.Name;
                DepictionLegacyTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(type);
//                if(existingDict.ContainsKey(simpleName))
//                {
//                    continue;
//                }
//                var fullName = type.FullName;
//                existingDict.Add(simpleName,fullName);
            }
        }


        public static Type FindType(string friendlyName)
        {
            if (allowedElementPropertyTypeDictionary.ContainsKey(friendlyName))
            {
                return allowedElementPropertyTypeDictionary[friendlyName];
            }
            return null;
        }

        public static string FriendlyName(Type requestedType)
        {
            foreach(var keyValuePair in allowedElementPropertyTypeDictionary)
            {
                if (requestedType.Equals(keyValuePair.Value))
                {
                    return keyValuePair.Key;
                }
            }
            return requestedType.Name;
        }
    }
   
}