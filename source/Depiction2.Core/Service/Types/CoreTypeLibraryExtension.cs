﻿using System;
using System.Collections.Generic;
using Depiction2.API.Extension.Tools.Types;
using Depiction2.API.Measurement;
using Depiction2.API.ValidationRules;

namespace Depiction2.Core.Service.Types
{
    [TypeImporterMetadata(Name = "CoreType", DisplayName = "Depiction types", Author = "Depiction Inc.")]
    public class CoreTypeLibraryExtension: ITypeImporter
    {
        private Dictionary<string, Type> defaultDepictionNameAndTypeDictionary;
        private List<Type> depictionCoreTypes = new List<Type>();

        public CoreTypeLibraryExtension()
        {
            defaultDepictionNameAndTypeDictionary = new Dictionary<string, Type>();
            depictionCoreTypes.Add(typeof(Area));
            depictionCoreTypes.Add(typeof(Distance));
            depictionCoreTypes.Add(typeof(Temperature));
            depictionCoreTypes.Add(typeof(Volume));
            depictionCoreTypes.Add(typeof(Weight));
            depictionCoreTypes.Add(typeof(Speed));
            depictionCoreTypes.Add(typeof(DataTypeValidationRule));
            depictionCoreTypes.Add(typeof(TerrainExistsValidationRule));
            depictionCoreTypes.Add(typeof(MinValueValidationRule));
            depictionCoreTypes.Add(typeof(RangeValidationRule));
            foreach (var type in depictionCoreTypes)
            {
                defaultDepictionNameAndTypeDictionary.Add(type.Name, type);
            }
        }
        public void Dispose()
        {
            
        }

        public Dictionary<string, Type> TypeDictionary()
        {
            return defaultDepictionNameAndTypeDictionary;
        }
    }
}