﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Core.Service
{
    public class ElementAndElemTemplateService
    {
        public static IElement CreateElementFromScaffoldAndPoints(IElementTemplate template, IEnumerable<Point> geometryPoints)
        {
            var geom = DepictionGeometryService.CreateGeometry(geometryPoints);
            return CreateElementFromScaffoldAndGeometry(template, geom);
        }
        public static IElement CreateElementFromScaffoldAndGeometry(IElementTemplate template, IDepictionGeometry geometry)
        {
            var createdElement = CreateElementFromTemplate(template);
            if (geometry != null)
            {
                switch (geometry.GeometryType)
                {
                    case DepictionGeometryType.Point:
                        createdElement.VisualState = ElementVisualSetting.Icon;
                        break;
                    case DepictionGeometryType.LineString:
                    case DepictionGeometryType.MultiLineString:
                        createdElement.VisualState = ElementVisualSetting.Geometry;
                        break;
                    case DepictionGeometryType.Polygon:
                        createdElement.VisualState = ElementVisualSetting.Geometry;
                        break;
                    case DepictionGeometryType.MultiPolygon:
                        createdElement.VisualState = ElementVisualSetting.Geometry;
                        break;
                    case DepictionGeometryType.MultiPoint:
                        break;
                }
            }
            //Hack, special case for circle and rectangle type objects
            if (template.OnCreateActions.Count != 0)
            {
                //Assume it will create a geometry
                createdElement.VisualState = createdElement.VisualState | ElementVisualSetting.Geometry;
            }

            createdElement.UpdatePrimaryPointAndGeometry(createdElement.GeoLocation, geometry);
            return createdElement;
        }

        public static void AddOrUpdateElementWithPropertyDictionary(IElement updateTarget, Dictionary<string, object> updateProperties)
        {
            //And now that those are out of the way, put together the other ones.
            var keys = updateProperties.Keys.ToList();
            foreach (var key in keys)
            {
                if (!updateTarget.UpdatePropertyValue(key, updateProperties[key], false))
                {
                    updateTarget.AddUserProperty(key, key, updateProperties[key]);
                }
                updateProperties.Remove(key);
            }
        }

        public static IElement CreateElementUsingPropertyDictionary(Dictionary<string, object> elementProperties)
        {
            var elementTypeKey = PropertyKeys.elementTypeKey;
            var typeString = string.Empty;
            if (elementProperties.ContainsKey(elementTypeKey))
            {
                typeString = elementProperties[elementTypeKey].ToString();
                elementProperties.Remove(elementTypeKey);
            }
            return CreateElementFromTemplateAndDictionary(typeString, elementProperties);
        }
        private static IElement CreateElementFromTemplate(IElementTemplate template)
        {
            var element = new DepictionElement(template.ElementType) { DisplayName = template.DisplayName };
            element.OnClickActions = template.OnClickActions.ToDictionary(x => x.Key, x => (string[])(x.Value.Clone()));//x.Value.ToArray() also works
            element.PostAddActions = template.OnCreateActions.ToDictionary(x => x.Key, x => x.Value.ToArray());

            foreach (var clrProp in template.InfoProperties)
            {
                UpdateElementClrProperty(element, clrProp);
            }
            for (int i = 0; i < template.Properties.Count; i++)
            {
                var templateProp = template.Properties[i];
                if (!templateProp.LegacyValidationRules.Any() && !templateProp.LegacyPostSetActions.Any())
                {
                    var objType = DepictionAccess.NameTypeService.GetTypeFromName(templateProp.TypeString);
                    var objValue = DepictionTypeConverter.ChangeType(templateProp.ValueString, objType);
                    if(!element.UpdatePropertyValue(templateProp.ProperName, objValue, false))
                    {
                        element.AddElementDefaultProperty(new ElementProperty(templateProp.ProperName, templateProp.ProperName, objValue));
                    }
                }
                else
                {
                    var prop = CreatePropertyFromScaffold(templateProp);
                    if (prop == null) continue;
                    element.AddElementDefaultProperty(prop);
                }
            }
            return element;
        }
        private static bool UpdateElementClrProperty<T>(IElement targetElement, T propSource)
        {
            var defaultPropTemplate = propSource as DefaultTemplateProperty;
            var userPropTemplate = propSource as IPropertyTemplate;
            var elemProp = propSource as IElementProperty;
            var propertyName = string.Empty;
            dynamic val = string.Empty;
            if(userPropTemplate != null)
            {
                val = userPropTemplate.ValueString;
                propertyName = userPropTemplate.ProperName;
            }else if(elemProp != null)
            {
                val = elemProp.Value;
                propertyName = elemProp.ProperName;
            }
            else if (defaultPropTemplate != null)
            {
                val = defaultPropTemplate.ValueString;
                propertyName = defaultPropTemplate.ProperName;
            }
            else
            {
                return false;
            }
            switch (propertyName)
            {
                #region clr property cases
                
                case PropNames.Author:
                    targetElement.Author = val;
                    break;
                case PropNames.Description:
                    targetElement.Description = val;
                    break;
                case PropNames.IsGeometryEditable:
                    targetElement.IsGeometryEditable = bool.Parse(val);
                    break;
                case PropNames.IsDraggable:
                    targetElement.IsDraggable = bool.Parse(val);
                    break;
                case PropNames.UsePropertyNamesInInformationText:
                    targetElement.UsePropertyNamesInInformationText = bool.Parse(val);
                    break;
                case PropNames.DisplayInformationText:
                    targetElement.DisplayInformationText = bool.Parse(val);
                    break;
                case PropNames.UseEnhancedInformationText:
                    targetElement.UseEnhancedInformationText = bool.Parse(val);
                    break;
                case PropNames.VisualState:
                    targetElement.VisualState = (ElementVisualSetting)(Enum.Parse(typeof(ElementVisualSetting), val, true));
                    break;
                case PropNames.ZOIFillColor:
                    targetElement.ZOIFillColor = val;
                    break;
                case PropNames.ZOIBorderColor:
                    targetElement.ZOIBorderColor = val;
                    break;
                case PropNames.ZOILineThickness:
                    targetElement.ZOILineThickness = double.Parse(val);
                    break;
                case PropNames.ZOIShapeType:
                    targetElement.ZOIShapeType = val;
                    break;
                case PropNames.IconBorderShape:
                    targetElement.IconBorderShape = val;
                    break;
                case PropNames.IconBorderColor:
                    targetElement.IconBorderColor = val;
                    break;
                case PropNames.IconSize:
                    targetElement.IconSize = double.Parse(val);
                    break;
                case PropNames.IconResourceName:
                    targetElement.IconResourceName = val;
                    break;
                case PropNames.ImageResourceName:
                    targetElement.ImageResourceName = val;
                    break;
                case PropNames.ImageRotation:
                    targetElement.ImageRotation = double.Parse(val);
                    break;
                default:
                    return false;
                    #endregion
            }
            return true;
        }
        
        public static IElement CreateElementFromTemplateAndDictionary(string targetTemplateName, Dictionary<string, object> requestedPropertyValues)
        {
            IElement createdElement = null;
            var defaultProperties = new List<string>();
            //TODO Some sort of system has to be put in place to ensure that complete elements don't overlap with elements that are not complete.
            if (DepictionAccess.TemplateLibrary != null && !string.IsNullOrEmpty(targetTemplateName))
            {
                var template = DepictionAccess.TemplateLibrary.FindElementTemplate(targetTemplateName);
                if (template == null)
                {
                    createdElement = new DepictionElement(targetTemplateName);
                }
                else
                {
                    createdElement = CreateElementFromTemplate(template);
                }
            }
            if (createdElement == null)
            {
                createdElement = new DepictionElement();
            }

            //Geometry/position setting
            var geometryKey = PropertyKeys.geometryKey;
            var geometryWktKey = PropertyKeys.geometryWktKey;
            var geoPositionKey = PropertyKeys.geoPositionKey;
            IDepictionGeometry geom = null;
            Point? p = null;
            string geomString = string.Empty;
            if (requestedPropertyValues.ContainsKey(geometryKey))
            {
                var geomObject = requestedPropertyValues[geometryKey];
                if (geomObject is IDepictionGeometry)
                {
                    geom = geomObject as IDepictionGeometry;
                }
                requestedPropertyValues.Remove(geometryKey);
            }
            if (requestedPropertyValues.ContainsKey(geoPositionKey))
            {
                p = requestedPropertyValues[geoPositionKey] as Point?;
                requestedPropertyValues.Remove(geoPositionKey);
            }
            if (requestedPropertyValues.ContainsKey(geometryWktKey))
            {
                geomString = requestedPropertyValues[geometryWktKey] as string;
                if (geom == null && !string.IsNullOrEmpty(geomString))
                {
                    geom = DepictionGeometryService.CreateGeomtryFromWkt(geomString);
                }
                requestedPropertyValues.Remove(geometryWktKey);
            }
            createdElement.VisualState = ElementVisualSetting.Icon;
            createdElement.UpdatePrimaryPointAndGeometry(p, geom);
            //Tags
            if (requestedPropertyValues.ContainsKey("TagList"))
            {
                var ty = requestedPropertyValues["TagList"].GetType();

                var rawTags = requestedPropertyValues["TagList"] as List<string>;
                if (rawTags != null)
                {
                    foreach (var t in rawTags)
                    {
                        createdElement.AddTag(t);
                    }
                }
                requestedPropertyValues.Remove("TagList");
            }

            //And now that those are out of the way, put together the other ones.
            AddOrUpdateElementWithPropertyDictionary(createdElement, requestedPropertyValues);
            return createdElement;

        }
        //The IElementScaffold is currently used when reading in 1.4 elements from the repository. The elements come in with their
        //properties in one location so they need to be split up for 2.0
        public static void UpdateElementWithProperties<T>(IElement element, IEnumerable<T> propertyObjects, IElementTemplate elementBase)
        {
            var coreElement = element as DepictionElement;
            if (propertyObjects == null) return;
            foreach (var prop in propertyObjects)
            {
                if (!UpdateElementClrProperty(coreElement,prop))
                {
                    //must create IElementProperty
                    IElementProperty fullProp = null;
                    if (prop is IPropertyTemplate)
                    {
                        fullProp = CreatePropertyFromScaffold(prop as IPropertyTemplate);
                    }
                    else if (prop is IElementProperty)
                    {
                        fullProp = prop as IElementProperty;
                    }
                    else { continue; }

                    if (coreElement != null && elementBase != null)
                    {
                        if (elementBase.Properties.Any(
                            t => t.ProperName.Equals(fullProp.ProperName, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            coreElement.AddElementDefaultProperty(fullProp);
                        }
                        else
                        {
                            element.AdjustElementWithPropertyHack(fullProp);
                        }
                    }
                    else
                    {
                        element.AdjustElementWithPropertyHack(fullProp);
                    }
                }
            }
        }

        internal static IElementProperty CreatePropertyFromScaffold(IPropertyTemplate prop)
        {
            var type = DepictionAccess.GetTypeFromName(prop.TypeString) ?? typeof(string);
            var specialProp = new ElementProperty(prop.ProperName, type) { DisplayName = prop.DisplayName };
            specialProp.LegacyPostSetActions = prop.LegacyPostSetActions.ToDictionary(x => x.Key, x => x.Value.ToArray());
            specialProp.LegacyValidationRules = prop.LegacyValidationRules.ToArray();
            //            var validationRules = new List<IValidationRule>();
            //            foreach(var ruleScaffold in prop.LegacyValidationRuleBase)
            //            {
            //                validationRules.Add(ValidationRuleTemplate.ConvertToValidationRule(ruleScaffold,DepictionAccess.NameTypeService));
            //            }
            //            specialProp.LegacyValidationRules = validationRules.ToArray();
            object realValue = null;


            if (type == null)
            {
                //log some sort of error
                specialProp.SetValue(prop.ValueString);
                return specialProp;
            }
            //            if (prop.IsValueInXml)
            //            {
            //                using (var reader = DepictionXmlFileUtilities.GetStringReader(prop.ValueString))
            //                {
            //                    reader.Read();
            //                    object ret = DepictionXmlFileUtilities.DeserializeObject(type, reader);
            //                    if (ret != null)
            //                    {
            //                        realValue = ret;
            //                    }
            //                }
            //            }
            //            else
            //            {
            realValue = DepictionTypeConverter.ChangeType(prop.ValueString, type);
            //            }

            specialProp.SetValue(realValue);
            return specialProp;
        }


    }
}