﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service;
using Depiction2.Core.Service.WindowViewModelMapping;

namespace Depiction2.Core.Service
{
    //This was totally taken from the web.
    //// using DialogResult = System.Windows.Forms.DialogResult;
    // Summary:
    //     Specifies identifiers to indicate the return value of a dialog box.
    ////[ComVisible(true)]
    public enum DialogResult
    {
        // Summary:
        //     Nothing is returned from the dialog box. This means that the modal dialog
        //     continues running.
        None = 0,
        //
        // Summary:
        //     The dialog box return value is OK (usually sent from a button labeled OK).
        OK = 1,
        //
        // Summary:
        //     The dialog box return value is Cancel (usually sent from a button labeled
        //     Cancel).
        Cancel = 2,
        //
        // Summary:
        //     The dialog box return value is Abort (usually sent from a button labeled
        //     Abort).
        Abort = 3,
        //
        // Summary:
        //     The dialog box return value is Retry (usually sent from a button labeled
        //     Retry).
        Retry = 4,
        //
        // Summary:
        //     The dialog box return value is Ignore (usually sent from a button labeled
        //     Ignore).
        Ignore = 5,
        //
        // Summary:
        //     The dialog box return value is Yes (usually sent from a button labeled Yes).
        Yes = 6,
        //
        // Summary:
        //     The dialog box return value is No (usually sent from a button labeled No).
        No = 7,
    }

    /// <summary>
    /// Class responsible for abstracting ViewModels from Views.
    /// </summary>
    public class DialogService : IDialogService
    {
        private readonly HashSet<FrameworkElement> views;
        private readonly Dictionary<string, Window> hiddenWindowsAndIds; 
        private readonly IWindowViewModelMappings windowViewModelMappings;


        /// <summary>
        /// Initializes a new instance of the <see cref="DialogService"/> class.
        /// </summary>
        /// <param name="windowViewModelMappings">
        /// The window ViewModel mappings. Default value is null.
        /// </param>
        public DialogService(IWindowViewModelMappings windowViewModelMappings = null)
        {
            this.windowViewModelMappings = windowViewModelMappings;

            views = new HashSet<FrameworkElement>();
            hiddenWindowsAndIds = new Dictionary<string, Window>();
        }


        #region IDialogService Members

        /// <summary>
        /// Gets the registered views.
        /// </summary>
        public ReadOnlyCollection<FrameworkElement> Views
        {
            get { return new ReadOnlyCollection<FrameworkElement>(views.ToList()); }
        }


        /// <summary>
        /// Registers a View.
        /// </summary>
        /// <param name="view">The registered View.</param>
        public void Register(FrameworkElement view)
        {
            // Get owner window
            Window owner = GetOwner(view);
            if (owner == null)
            {
                // Perform a late register when the View hasn't been loaded yet.
                // This will happen if e.g. the View is contained in a Frame.
                view.Loaded += LateRegister;
                return;
            }

            // Register for owner window closing, since we then should unregister View reference,
            // preventing memory leaks
            owner.Closed += OwnerClosed;
            views.Add(view);
        }


        /// <summary>
        /// Unregisters a View.
        /// </summary>
        /// <param name="view">The unregistered View.</param>
        public void Unregister(FrameworkElement view)
        {
            views.Remove(view);
        }

        #region show dialog methods
        /// <summary>
        /// Shows a dialog.
        /// </summary>
        /// <remarks>
        /// The dialog used to represent the ViewModel is retrieved from the registered mappings.
        /// </remarks>
        /// <param name="ownerViewModel">
        /// A ViewModel that represents the owner window of the dialog.
        /// </param>
        /// <param name="viewModel">The ViewModel of the new dialog.</param>
        /// <returns>
        /// A nullable value of type bool that signifies how a window was closed by the user.
        /// </returns>
        public bool? ShowDialog(object ownerViewModel, object viewModel)
        {
            Type dialogType = windowViewModelMappings.GetWindowTypeFromViewModelType(viewModel.GetType());
            return ShowDialog(ownerViewModel, viewModel, dialogType);
        }

        /// <summary>
        /// Shows a dialog.
        /// </summary>
        /// <param name="ownerViewModel">
        /// A ViewModel that represents the owner window of the dialog.
        /// </param>
        /// <param name="viewModel">The ViewModel of the new dialog.</param>
        /// <typeparam name="T">The type of the dialog to show.</typeparam>
        /// <returns>
        /// A nullable value of type bool that signifies how a window was closed by the user.
        /// </returns>
        public bool? ShowDialog<T>(object ownerViewModel, object viewModel) where T : Window
        {
            return ShowDialog(ownerViewModel, viewModel, typeof(T));
        }
        //Need to do something about creating the same window over and over. ie the difference between hiding a 
        //window that is active all program as opposed to window that has a very short life span
        public void ShowWindow(object ownerViewModel, DialogViewModel dialogViewModel)
        {
            Type dialogType = windowViewModelMappings.GetWindowTypeFromViewModelType(dialogViewModel.GetType());
            if(!dialogViewModel.IsModal)
            {
                ShowWindow(ownerViewModel, dialogViewModel, dialogType);
            }
            //well i suppose something should happen if you try to open a modal one ina non modal mode
        }
        #endregion
        #region close window methods
        public void HideWindow(DialogViewModel dialogViewModel)
        {
            var id = dialogViewModel.Id;
            if (hiddenWindowsAndIds.ContainsKey(id))
            {
                //this view model is active and does not need a new window
                hiddenWindowsAndIds[id].Close();
            }
        }

        //Used when going back to the main screen or loading a new depiction
        public void CloseAllWindowsTestMethod()
        {
            var windowKeys = hiddenWindowsAndIds.Keys.ToList();
            foreach(var key in windowKeys)
            {
                var window = hiddenWindowsAndIds[key];
                window.Close();
            }
        }
        #endregion
       
        #endregion

        #region Attached properties

        /// <summary>
        /// Attached property describing whether a FrameworkElement is acting as a View in MVVM.
        /// </summary>
        public static readonly DependencyProperty IsRegisteredViewProperty =
            DependencyProperty.RegisterAttached(
            "IsRegisteredView",
            typeof(bool),
            typeof(DialogService),
            new UIPropertyMetadata(IsRegisteredViewPropertyChanged));


        /// <summary>
        /// Gets value describing whether FrameworkElement is acting as View in MVVM.
        /// </summary>
        public static bool GetIsRegisteredView(FrameworkElement target)
        {
            return (bool)target.GetValue(IsRegisteredViewProperty);
        }


        /// <summary>
        /// Sets value describing whether FrameworkElement is acting as View in MVVM.
        /// </summary>
        public static void SetIsRegisteredView(FrameworkElement target, bool value)
        {
            target.SetValue(IsRegisteredViewProperty, value);
        }


        /// <summary>
        /// Is responsible for handling IsRegisteredViewProperty changes, i.e. whether
        /// FrameworkElement is acting as View in MVVM or not.
        /// </summary>
        private static void IsRegisteredViewPropertyChanged(DependencyObject target,DependencyPropertyChangedEventArgs e)
        {
            // The Visual Studio Designer or Blend will run this code when setting the attached
            // property, however at that point there is no IDialogService registered
            // in the ServiceLocator which will cause the Resolve method to throw a ArgumentException.
            if (DesignerProperties.GetIsInDesignMode(target)) return;

            FrameworkElement view = target as FrameworkElement;
            if (view != null)
            {
                // Cast values
                bool newValue = (bool)e.NewValue;
                bool oldValue = (bool)e.OldValue;

                if (newValue)
                {
                    ServiceLocator.Resolve<IDialogService>().Register(view);
                }
                else
                {
                    ServiceLocator.Resolve<IDialogService>().Unregister(view);
                }
            }
        }

        #endregion

        private void ShowWindow(object ownerViewModel, DialogViewModel dialogViewModel, Type dialogType)
        {
            var id = dialogViewModel.Id;
            if (hiddenWindowsAndIds.ContainsKey(id))
            {
                //this view model is active and does not need a new window
                hiddenWindowsAndIds[id].Show();
                dialogViewModel.IsHidden = false;
            }else
            {
                //First time trying to create a window with this viewmodel so we need to create the window
                // Create dialog and set properties
                var dialog = (Window)Activator.CreateInstance(dialogType);
                dialog.Owner = FindOwnerWindow(ownerViewModel);
                dialog.DataContext = dialogViewModel;
                //interesting, why isnt the datacontext connected right here. so that calling hide/show automatically
                //triggers the service hide/show
                dialogViewModel.IsHidden = false;
                dialog.Show();
               
                //Make sure to trap the closed event
                dialog.Closed += dialog_Closed;
                dialog.Closing += dialog_Closing;
                dialogViewModel.RequestClose += requestClose;
                hiddenWindowsAndIds.Add(dialogViewModel.Id,dialog);
            }
        }

        void dialog_Closing(object sender, CancelEventArgs e)
        {
            var win = sender as Window;
            if (win == null)
            {
                //this would be really bad and should actually never happen.
                return;
            }
            var dc = win.DataContext as DialogViewModel;
            if (dc == null)
            {
                //this would be bad, but not program breakings.
                return;
            }
            if(dc.HideOnClose)
            {
                win.Hide();
                dc.IsHidden = true;
                e.Cancel = true;
               
            }else
            {
                //A double dispose might happen, need to know if the datacontext has already
                //been disposed, or has already been attempted.
                dc.Dispose();
            }
            if(win.Owner != null)
            {

                win.Owner.Activate();
            }else
            {
                Application.Current.MainWindow.Activate();
            }
        }

        void dialog_Closed(object sender, EventArgs e)
        {
            var win = sender as Window;
            if(win == null)
            {
                //this would be really bad and should actually never happen.
                return;
            }
            var dc = win.DataContext as DialogViewModel;
            if(dc == null)
            {
                //this would be bad, but not program breakings.
                return;
            }
            dc.RequestClose -= requestClose;
            win.Closed -= dialog_Closed;
            win.Closing -= dialog_Closing;
            hiddenWindowsAndIds.Remove(dc.Id);
        }

        private void requestClose(object sender, EventArgs e)
        {
            var dialogVm = sender as DialogViewModel;
            if(dialogVm == null) return;
            HideWindow(dialogVm);
        }

        /// <summary>
        /// Shows a dialog.
        /// </summary>
        /// <param name="ownerViewModel">
        /// A ViewModel that represents the owner window of the dialog.
        /// </param>
        /// <param name="viewModel">The ViewModel of the new dialog.</param>
        /// <param name="dialogType">The type of the dialog.</param>
        /// <returns>
        /// A nullable value of type bool that signifies how a window was closed by the user.
        /// </returns>
        private bool? ShowDialog(object ownerViewModel, object viewModel, Type dialogType)
        {
            // Create dialog and set properties
            Window dialog = (Window)Activator.CreateInstance(dialogType);
            dialog.Owner = FindOwnerWindow(ownerViewModel);
            dialog.DataContext = viewModel;
            if(viewModel is DialogViewModel)
            {
                var dialogVm = ((DialogViewModel) viewModel);
                dialogVm.RequestClose += requestClose;//Make sure to trap the closed event
                dialog.Closed += dialog_Closed;
                dialog.Closing += dialog_Closing;
                hiddenWindowsAndIds.Add(dialogVm.Id, dialog);
            }

            // Show dialog
            return dialog.ShowDialog();
        }

        /// <summary>
        /// Finds window corresponding to specified ViewModel.
        /// </summary>
        private Window FindOwnerWindow(object viewModel)
        {
            FrameworkElement view = views.SingleOrDefault(v => ReferenceEquals(v.DataContext, viewModel));
            if (view == null)
            {
                //TODO make this more useful
                //For now if the window is not registered just grab the main window.
                view = Application.Current.MainWindow;
//                throw new ArgumentException("Viewmodel is not referenced by any registered View.");
            }

            // Get owner window
            Window owner = view as Window;
            if (owner == null)
            {
                owner = Window.GetWindow(view);
            }

            // Make sure owner window was found
            if (owner == null)
            {
                throw new InvalidOperationException("View is not contained within a Window.");
            }

            return owner;
        }


        /// <summary>
        /// Callback for late View register. It wasn't possible to do a instant register since the
        /// View wasn't at that point part of the logical nor visual tree.
        /// </summary>
        private void LateRegister(object sender, RoutedEventArgs e)
        {
            FrameworkElement view = sender as FrameworkElement;
            if (view != null)
            {
                // Unregister loaded event
                view.Loaded -= LateRegister;

                // Register the view
                Register(view);
            }
        }


        /// <summary>
        /// Handles owner window closed, View service should then unregister all Views acting
        /// within the closed window.
        /// </summary>
        private void OwnerClosed(object sender, EventArgs e)
        {
            Window owner = sender as Window;
            if (owner != null)
            {
                // Find Views acting within closed window
                IEnumerable<FrameworkElement> windowViews =
                    from view in views
                    where Window.GetWindow(view) == owner
                    select view;

                // Unregister Views in window
                foreach (FrameworkElement view in windowViews.ToArray())
                {
                    Unregister(view);
                }
            }
        }


        /// <summary>
        /// Gets the owning Window of a view.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns>The owning Window if found; otherwise null.</returns>
        private Window GetOwner(FrameworkElement view)
        {
            return view as Window ?? Window.GetWindow(view);
        }
    }
}
