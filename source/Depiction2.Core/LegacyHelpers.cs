﻿using System;
using System.Collections.Generic;
using System.Linq;
using Depiction2.Base.Extensions;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core
{
    public class LegacyHelpers
    {
        public static object[] BuildParametersForPostSetActions(IEnumerable<string> inParameters)
        {
            var outParameters = new List<object>();
            if (inParameters == null) return outParameters.ToArray();
            foreach (var param in inParameters)
            {
                var split = param.Split(':');
                if (split.Length == 2)
                {
                    outParameters.Add(split[0]);
                    outParameters.Add(split[1]);
                }
                else
                {
                    outParameters.Add(param);
                }
            }

            return outParameters.ToArray();
        }
        public static object[] BuildParameters(IEnumerable<Type> parameterTypes, IEnumerable<IElementFileParameter> parameters,
            IElement publisher, IElement subscriber, IElementRepository affectedElements)
        {
            var outParameters = new List<object>();

            if (parameters != null)
                for (int i = 0; i < parameters.Count(); i++)
                {
                    IElementFileParameter property = parameters.ElementAt(i);

                    var query = property.ElementQuery;

                    switch (property.Type)
                    {
                        //SO much hacking
                        case ElementFileParameterType.Publisher:
                            if (parameterTypes.ElementAt(i).Equals(typeof(string)))
                            {
                                outParameters.Add(query);
                            }
                            else
                            {
                                outParameters.Add(publisher.Query(query));
                            }
                            break;
                        case ElementFileParameterType.Subscriber:
                            if (parameterTypes.ElementAt(i).Equals(typeof(string)))
                            {
                                outParameters.Add(query);
                            }
                            else
                            {
                                outParameters.Add(subscriber.Query(query));
                            }

                            break;
                        case ElementFileParameterType.Value:
                            try
                            {
                                outParameters.Add(query);
                            }
                            catch
                            {
                                throw new Exception("Cannot convert behavior value to appropriate type!");
                            }
                            break;
                        case ElementFileParameterType.NotThere:
                            if (affectedElements == null) break;
                            var behaviorSource =
                                affectedElements.AllElements.FirstOrDefault(t => t.ElementType.Equals(query));
                            if(behaviorSource==null)
                            {

                                outParameters.Add(null);
                            }

                            break;
                    }
                }

            return outParameters.ToArray();
        }

    }
}