﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.Utilities;

namespace Depiction2.Core.Extensions
{
    static public class StoryExtensions
    {
        static public void AddRevealerWithElements(this IStory story, string revealerTitle, List<IElement> elementsInRevealer)
        {
            var insideRect = RandomUtilities.GetRectInteriorToRect(GeoToDrawLocationService.CanvasPixelExtent);
            var insideGcs = GeoToDrawLocationService.ConvertDrawRectToStoryGeoRect(insideRect);
            var newRevealer = new Revealer(revealerTitle);
            newRevealer.GeoRectBounds = insideGcs;
            if(elementsInRevealer != null && elementsInRevealer.Any())
            {
                newRevealer.AddDisplayElementsWithMatchingIds(elementsInRevealer.Select(t=>t.ElementId));
            }

            story.LoadRevealer(newRevealer);
        } 
    }
}