﻿using System;
using System.Text;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.Core.Extensions
{
    static public class DepictionElementExtensions
    {
        #region property setting
        public static bool UpdatePropertyValue(this IElement tempElement, string propName, object value)
        {
            return tempElement.UpdatePropertyValue(propName, value, true);
        }
        public static bool UpdatePropertyValue(this IElement tempElement, string propName, object objectValue, bool triggerEvent)
        {
            if (SetDefaultProperty(tempElement, propName, objectValue, triggerEvent))
            {
                return true;
            }
            if (SetGeometryProperty(tempElement, propName, objectValue, triggerEvent))
            {
                return true;
            }
            if (SetVisualProperty(tempElement, propName, objectValue, triggerEvent))
            {
                return true;
            }
            var prop = tempElement.GetDepictionProperty(propName);
            if (prop == null) return false;
            if (objectValue is string)
            {
                return prop.TrySetValueFromString((string)objectValue);
            }
            prop.SetValue(objectValue);
            return true;
        }
        public static bool SetDefaultProperty(IElement element, string propName,object propValue,bool triggerEvent)
        {
            var stringCompareSettings = StringComparison.InvariantCultureIgnoreCase;
            if (propName.Equals("Author", stringCompareSettings))
            {
                element.Author = propValue.ToString();
            }
            else if (propName.Equals("Description", stringCompareSettings))
            {
                element.Description = propValue.ToString();

            }
            else if (propName.Equals("DisplayName", stringCompareSettings))
            {
                element.DisplayName = propValue.ToString();
                
            }else
            {
                return false;
            }
            if (triggerEvent) element.TriggerPropertyChange(propName);
            return true;
        }
        public static bool SetGeometryProperty(IElement element,string propName, object propValue, bool triggerEvent)
        {
            var stringCompareSettings = StringComparison.InvariantCultureIgnoreCase;
            if (propName.Equals("Draggable", stringCompareSettings) || propName.Equals("IsDraggable", stringCompareSettings))
            {
                element.IsDraggable = bool.Parse(propValue.ToString());
            }
            else if (propName.Equals("IsGeometryEditable", stringCompareSettings))
            {
                element.IsDraggable = bool.Parse(propValue.ToString());
            }else if (propName.Equals("GeoPosition"))
            {
                var pos = propValue as Point?;
                element.UpdatePrimaryPointAndGeometry(pos, null);
            }
            else
            {
                return false;
            }
            return true;
        }
        public static bool SetVisualProperty(IElement element, string propertyName, object propertyValue, bool triggerEvent)
        {
            var stringCompareSettings = StringComparison.InvariantCultureIgnoreCase;
            var matchingProp = true;
            var stringVal = string.Empty;
            if (propertyValue != null)
            {
                stringVal = propertyValue.ToString();
            }

            if (propertyName.Equals("ZOIBorderColor", stringCompareSettings))
            {
                element.ZOIBorderColor = stringVal;
            }
            else if (propertyName.Equals("ZOIFillColor", stringCompareSettings))
            {
                element.ZOIFillColor = stringVal;
            }
            else if (propertyName.Equals("ZOILineThickness", stringCompareSettings))
            {
                element.ZOILineThickness = double.Parse(stringVal);
            }
            else if (propertyName.Equals("ZOIShapeType", stringCompareSettings))
            {
                element.ZOIShapeType = stringVal;
            }
            else if (propertyName.Equals("IconBorderShape", stringCompareSettings))
            {
                element.IconBorderShape = stringVal;
            }
            else if (propertyName.Equals("IconBorderColor", stringCompareSettings))
            {
                element.IconBorderColor = stringVal;
            }
            else if (propertyName.Equals("IconSize", stringCompareSettings))
            {
                element.IconSize = double.Parse(stringVal);
            }
            else if (propertyName.Equals("IconResourceName", stringCompareSettings))
            {
                element.IconResourceName = stringVal;
            }
            else if (propertyName.Equals("ImageResourceName", stringCompareSettings))
            {
                element.ImageResourceName = stringVal;
            }
            else if (propertyName.Equals("ImageRotation", stringCompareSettings))
            {
                element.ImageRotation = double.Parse(stringVal);
            }
            else if (propertyName.Equals("VisualState", stringCompareSettings))
            {
                ElementVisualSetting state;
                Enum.TryParse(stringVal, true, out state);
                element.VisualState = state;
            }
            else if (propertyName.Equals("DisplayInformationText", stringCompareSettings))
            {
                element.DisplayInformationText = bool.Parse(stringVal);
            }
            else if (propertyName.Equals("UseEnhancedInformationText", stringCompareSettings))
            {
                element.UseEnhancedInformationText = bool.Parse(stringVal);
            }
            else if (propertyName.Equals("UsePropertyNamesInInformationText", stringCompareSettings))
            {
                element.UsePropertyNamesInInformationText = bool.Parse(stringVal);
            }
            else
            {
                matchingProp = false;
            }
            //Why don't the props trigger their own change?
            if (matchingProp && triggerEvent) element.TriggerPropertyChange(propertyName);
            return matchingProp;

        }
        #endregion
        #region property value getting
        public static bool GetPropertyValue(this IElement element, string propName, out object value)
        {
            var propValue = GetVisualPropertyValue(element, propName);
            if (propValue != null)
            {
                value = propValue;
                return true;
            }

            var prop = element.GetDepictionProperty(propName);
            if (prop == null)
            {
                value = null;
                return false;
            }
            value = prop.Value;
            return true;
        }
        internal static bool GetGeoPropertyValue(IElement element, string propertyName, out object value)
        {
            if (propertyName.Equals("GeoLocation"))
            {
                value = element.GeoLocation;
            }
            else if (propertyName.Equals("ElementGeometry"))
            {
                value = element.ElementGeometry;
            }
            else
            {
                value = null;
                return false;
            }
            return true;
        }
        internal static bool GetBasePropertyValue(IElement element, string propertyName, out object value)
        {
            if (propertyName.Equals("ElementId"))
            {
                value = element.ElementId;
            }
            else if (propertyName.Equals("ElementType"))
            {
                value = element.ElementType;
            }
            else if (propertyName.Equals("DisplayName"))
            {
                value = element.DisplayName;
            }
            else if (propertyName.Equals("Author"))
            {
                value = element.Author;
            }
            else if (propertyName.Equals("Description"))
            {
                value = element.Description;
            }
            else
            {
                value = null;
                return false;
            }
            return true;
        }
        internal static object GetVisualPropertyValue(IElement element, string propertyName)
        {
            if (propertyName.Equals("ZOIBorderColor"))
            {
                return element.ZOIBorderColor;
            }
            if (propertyName.Equals("ZOIFillColor"))
            {
                return element.ZOIFillColor;
            }
            if (propertyName.Equals("ZOILineThickness"))
            {
                return element.ZOILineThickness;
            }
            if (propertyName.Equals("ZOIShapeType"))
            {
                return element.ZOIShapeType;
            }
            if (propertyName.Equals("IconBorderShape"))
            {
                return element.IconBorderShape;
            }
            if (propertyName.Equals("IconBorderColor"))
            {
                return element.IconBorderColor;
            }
            if (propertyName.Equals("IconSize"))
            {
                return element.IconSize;
            }
            if (propertyName.Equals("IconResourceName"))
            {
                return element.IconResourceName;
            }
            if (propertyName.Equals("VisualState"))
            {
                return element.VisualState;
            }
            if (propertyName.Equals("DisplayInformationText"))
            {
                return element.DisplayInformationText;
            }
            if (propertyName.Equals("UseEnhancedInformationText"))
            {
                return element.UseEnhancedInformationText;
            }
            if (propertyName.Equals("UsePropertyNamesInInformationText"))
            {
                return element.UsePropertyNamesInInformationText;
            }
            return null;

        }
        #endregion

        public static IDepictionLatitudeLongitude GeoLocationToLatLong(this IElement element)
        {
            if (element.GeoLocation == null) return null;
            var p = (Point)element.GeoLocation;
            return new LatitudeLongitudeSimple(p.Y, p.X);
        }

        public static string ConstructToolTipText(this IElement element, string separator, bool usePropNames, bool useSimple)
        {
            var builder = new StringBuilder();
            #region the true constructor

            bool isFirst = true;
            foreach (var hoverTextProperty in element.HoverTextProperties)
            {
                var stringValue = "";
                bool isHtml = false;
                var prop = element.GetDepictionProperty(hoverTextProperty);
                var propString = string.Empty;
                var propDisplayName = string.Empty;
                Type propType = null;
                if (prop == null)
                {
                    if (hoverTextProperty.Equals("Type"))
                    {
                        propDisplayName = "Type";
                        propString = element.ElementType;
                    }
                    else if (hoverTextProperty.Equals("DisplayName"))
                    {
                        propDisplayName = "Display Name";
                        propString = element.DisplayName;
                    }
                    else if (hoverTextProperty.Equals("Author"))
                    {
                        propDisplayName = "Author";
                        propString = element.Author;
                    }
                    else if (hoverTextProperty.Equals("Description"))
                    {
                        propDisplayName = "Description";
                        propString = element.Description;
                    }
                    if (string.IsNullOrEmpty(propString)) continue;
                }
                else if (prop.Value != null && !string.IsNullOrEmpty(prop.Value.ToString()))
                {
                    propType = prop.Value.GetType();
                    propString = prop.Value.ToString();
                    propDisplayName = prop.DisplayName;
                }
                //Alas this doesn't actually fix the html in tooltip problem, at least not all of it
                //this will remove the html if normal permatext is used.
                if (!string.IsNullOrEmpty(propString))
                {

                    if (propString.StartsWith("<") && propString.EndsWith(">"))
                    {
                        isHtml = true;
                    }

                    if (!useSimple)
                    {
                        stringValue = propString;
                    }
                    else
                    {
                        if (!isHtml)
                        {
                            stringValue = propString;
                        }
                    }
                }

                if (!isFirst)
                {
                    if (separator.Equals(Environment.NewLine))
                    {
                        builder.AppendLine();
                    }
                    else
                    {
                        builder.Append(separator);
                    }
                }
                //boolean properties will always get displayname attached.
                if ((usePropNames || typeof(bool) == propType) && !isHtml)
                {
                    builder.Append(propDisplayName);
                    builder.Append(": ");
                }
                builder.Append(stringValue);

                isFirst = false;

            }

            #endregion
            if (builder.Length != 0) return builder.ToString(); ;

            return element.DisplayName;
        }


    }
}