﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Resources;
using Depiction2.API.Measurement;
using Depiction2.API.ValidationRules;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Interactions;
using Depiction2.Core.Interactions.Helpers;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;

namespace Depiction2.Core
{
    [ResourceExtensionMetadata(DisplayName = "Core Resource", Name = "CoreResources", 
        Author = "Depiction Inc.", ExtensionPackage = "Core", ResourceName = "CoreResources")]
    public class CoreResources: IDepictionResourceExtension
    {
        private Dictionary<string, Type> allCoreTypeDictionary = new Dictionary<string, Type>();
        //Lists are only around to make things cleaner?hopefully
        private List<Type> depictionCorePropertyTypes = new List<Type>();
        private List<Type> depictionCorePropertyValidationTypes = new List<Type>();
        private List<Type> depictionCoreStoryElementTypes = new List<Type>();

        public CoreResources()
        {
            depictionCorePropertyTypes.Add(typeof(Area));
            depictionCorePropertyTypes.Add(typeof(Distance));
            depictionCorePropertyTypes.Add(typeof(Temperature));
            depictionCorePropertyTypes.Add(typeof(Volume));
            depictionCorePropertyTypes.Add(typeof(Weight));
            depictionCorePropertyTypes.Add(typeof(Speed));

            depictionCorePropertyValidationTypes.Add(typeof(DataTypeValidationRule));
            depictionCorePropertyValidationTypes.Add(typeof(TerrainExistsValidationRule));
            depictionCorePropertyValidationTypes.Add(typeof(MinValueValidationRule));
            depictionCorePropertyValidationTypes.Add(typeof(RangeValidationRule));

            depictionCoreStoryElementTypes.Add(typeof(CartRect));

            depictionCoreStoryElementTypes.Add(typeof(Story));
            depictionCoreStoryElementTypes.Add(typeof(StoryDetails));
            depictionCoreStoryElementTypes.Add(typeof(ElementDisplayer));
            depictionCoreStoryElementTypes.Add(typeof(DepictionRegion));
            depictionCoreStoryElementTypes.Add(typeof(Revealer));
            depictionCoreStoryElementTypes.Add(typeof(RevealerProperty));
            depictionCoreStoryElementTypes.Add(typeof(ElementTemplate));
            depictionCoreStoryElementTypes.Add(typeof(DefaultTemplateProperty));
            depictionCoreStoryElementTypes.Add(typeof(PropertyTemplate));
            depictionCoreStoryElementTypes.Add(typeof(DepictionElement));
            depictionCoreStoryElementTypes.Add(typeof(ElementProperty));
            depictionCoreStoryElementTypes.Add(typeof(DepictionAnnotation));
            depictionCoreStoryElementTypes.Add(typeof(InteractionRule));
            depictionCoreStoryElementTypes.Add(typeof(ElementFileParameter));

            foreach (var type in depictionCorePropertyTypes)
            {
                allCoreTypeDictionary.Add(type.Name, type);
            }
            foreach (var type in depictionCorePropertyValidationTypes)
            {
                allCoreTypeDictionary.Add(type.Name, type);
            }
            foreach (var type in depictionCoreStoryElementTypes)
            {
                allCoreTypeDictionary.Add(type.Name, type);
            }
        }
        public void Dispose()
        {
            
        }

        public ResourceDictionary ExtensionResources
        {
            get { return null; }
        }

        public Dictionary<string, Type> ExtensionTypes
        {
            get
            {
                return allCoreTypeDictionary;
            }
        }
    }
}