﻿using System.Collections.Generic;
using System.IO;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.Utilities;
using Depiction2.Core.Service;

namespace Depiction2.Core.ExtensionFramework
{
    [DepictionFileLoaderMetadata(Name = "DefaultImageLoader", Description = "A simple non geolocated image loader",
        DisplayName = "Depiction Image", Author = "Depiction Inc.",
        SupportedExtensions = new[] { ".jpg", ".jpeg", ".bmp", ".png", ".tif", ".tiff", ".gif", ".giff" })]
    public class DefaultImageImporter : IFileLoaderExtension
    {
        public void Dispose()
        {

        }

        public void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName,ICartRect sourceBounds)
        {
            if (DepictionAccess.TemplateLibrary == null && defaultTemplate == null)
            {
                //This would be bad
                return;
            }
            var finalScaffold = defaultTemplate;
            if (defaultTemplate == null || defaultTemplate.ElementType.Equals(DepictionAccess.AutoDetectElement))
            {
                if (DepictionAccess.TemplateLibrary == null) return;
                finalScaffold = DepictionAccess.TemplateLibrary.FindElementTemplate(DepictionAccess.DefaultImageElement);
            }
            if (finalScaffold == null) return;//or maybe create one
            var dict = new Dictionary<string, object>();
            dict.Add("FileName", sourceLocation);
            dict.Add("ScaffoldType", finalScaffold);
            var service = new DefaultImageReadingService();
            var shortName = Path.GetFileName(sourceLocation);
            var name = string.Format("Loading image file: {0}", shortName);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }
    }

    public class DefaultImageReadingService : BaseDepictionBackgroundThreadOperation
    {
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var info = args as Dictionary<string, object>;
            if (info == null) return null;
            var fileName = info["FileName"] as string;
            var type = info["ScaffoldType"] as IElementTemplate;
            if (!File.Exists(fileName)) return null;
            var image = DepictionImageResourceDictionary.GetBitmapFromFile(fileName);
            var element = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(type, null);


            var simpleFName = Path.GetFileName(fileName);
            if (DepictionAccess.DStory == null)
            {
                //TODO throw some sort of error
            }
            else
            {
                DepictionAccess.DStory.AllImages.AddImage(simpleFName, image, true);
            }
            element.ImageResourceName = simpleFName;

            element.VisualState = ElementVisualSetting.Image;
            return element;
        }

        protected override void ServiceComplete(object args)
        {
            var story = DepictionAccess.DStory;
            if (story == null) return;
            var elem = args as IElement;
            if (elem == null) return;
            story.AddElement(elem, true);
        }
    }
}