﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.Base.Geo;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.Core.ExtensionFramework.Tiling
{
    [TilerExtensionMetadata(Name = "EmptyTiler", DisplayName = "Empty Tiler", TilerType = TileImageTypes.None)]
    public class EmptyTiler : ITiler
    {
        public int GetZoomLevel(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            return 1;
        }

        public IList<TileModel> GetTiles(ICartRect boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            throw new NotImplementedException();
        }

        public TileImageTypes TileImageType
        {
            get { return TileImageTypes.None; }
        }

        public string DisplayName
        {
            get { return "None"; }
        }

        public string CacheLocation
        {
            get { return "None"; }
        }

        public string LegacyImporterName
        {
            get { throw new NotImplementedException(); }
        }

        public int PixelWidth
        {
            get
            {
                return 1;
            }
        }

        public bool DoesOwnCaching
        {
            get { throw new NotImplementedException(); }
        }

        public int LongitudeToColAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            throw new NotImplementedException();
        }

        public int LatitudeToRowAtZoom(IDepictionLatitudeLongitude latLong, int zoom)
        {
            throw new NotImplementedException();
        }

        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            throw new NotImplementedException();
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            throw new NotImplementedException();
        }

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            throw new NotImplementedException();
        }
    }
}
