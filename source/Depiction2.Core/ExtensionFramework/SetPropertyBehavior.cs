using System.Collections.Generic;
using Depiction2.API.Converters;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;

namespace Depiction2.Core.ExtensionFramework
{
    [BehaviorMetadata("SetProperty", "Set element property", "Set element property")]
    public class SetPropertyBehavior : BaseBehavior
    {
        public override DepictionParameterInfo[] DepictionParameters
        {
            get
            {
                return new[] {new DepictionParameterInfo("PropertyName", typeof (string))
                                  {
                                      ParameterName = "Property To Set", ParameterDescription = "Indicates the property whos value will be changed"
                                  }, 
                              new DepictionParameterInfo("Value", typeof (object))
                                  {
                                      ParameterName = "New Value", ParameterDescription = "The property to be changed will have its value set to this value"
                                  }};
            }
        }

        public string FriendlyName
        {
            get { return "Set property value"; }
        }

        public string Description
        {
            get { return "Set a property value of a chosen element property"; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (!parameterBag.ContainsKey("PropertyName")) return new BehaviorResult();
            var propName = parameterBag["PropertyName"].ToString();
            var val = parameterBag["Value"];
            object orig = null;
            RestoreProperty restore = null;

            subscriber.GetPropertyValue(propName, out orig);
            if (orig != null)
            {
                restore = new RestoreProperty { Name = propName, Value = orig };
            }
            else
            {
                //prop does not exist create one
                var propValue = DepictionTypeConverter.ChangeTypeByGuessing(val);
                var prop = new ElementProperty(propName, propValue.GetType());
                restore = new RestoreProperty { Name = propName, Value = prop.Value };
                subscriber.AddUserProperty(prop);
                subscriber.UpdatePropertyValue(propName, propValue, true);
            }

            var el = subscriber as DepictionElement;
            if (el != null)
            {
                el.RestoreProps.Add(restore);
            }
            return new BehaviorResult();
        }
    }
}