﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.Collections
{
    public class ModQuadTree<T> : PriorityQuadTree<T> where T : IElement
    {
        HashSet<string> containedElements = new HashSet<string>();
        public IEnumerable<T> AddElements(IEnumerable<T> elements)
        {
            var addList = new List<T>();
            foreach (var element in elements)
            {
                if (!containedElements.Contains(element.ElementId))
                {
                    Insert(element, element.SimpleBounds.WpfRect, double.NaN);
                    containedElements.Add(element.ElementId);
                    addList.Add(element);
                }
            }
            return addList;
        }

        public IEnumerable<T> RemoveElements(IEnumerable<T> elements)
        {
            var removeList = new List<T>();
            foreach (var element in elements)
            {
                if (containedElements.Contains(element.ElementId) &&
                    Remove(element, element.SimpleBounds.WpfRect))
                {
                    removeList.Add(element);
                    containedElements.Remove(element.ElementId);
                }
            }
            return removeList;
        }

        //        public void UpdateGeoArea()
        //        {
        //            var finalExtent = Rect.Empty;
        //            foreach (var item in this)
        //            {
        //                finalExtent.Union(item.SimpleBounds);
        //            }
        //            Extent = finalExtent;
        //        }

    }
}