﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Threading;
using Depiction2.Base.Helpers;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using QuickGraph;

namespace Depiction2.Core.Interactions
{
    public class InteractionGraph : ModelBase
    {
        #region fields

        public event Action InteractionsDone;

        private readonly HashSet<IElement> dirtyElements = new HashSet<IElement>();
        public ElementTupleList ElementRulePairs { get; set; }
        private readonly Dictionary<object, IInteractionRule> excludedEdges = new Dictionary<object, IInteractionRule>();
        private readonly Hashtable _excludedVertices = new Hashtable();
        private readonly IEnumerable<IInteractionRule> _interactionRules;
        private readonly MessageQueue messageQueue;

        private readonly Hashtable objectsNotToBeRestored = new Hashtable();
        private BidirectionalGraph<IElement, TaggedEdge<IElement, IInteractionRule>> graph;
        private string currentInteraction = "";

        #endregion
        #region Properties
        public string CurrentInteraction
        {
            get { return currentInteraction; }
            set
            {
                currentInteraction = value;
                OnPropertyChanged("CurrentInteraction");
            }
        }

        public bool ProcessingMessageQueue { get; set; }

        #endregion
        #region constructor

        public InteractionGraph(IEnumerable<IElement> elementRepository, IEnumerable<IInteractionRule> interactionRules)
        {
            //what happens when the repository changes?
            ElementRulePairs = new ElementTupleList();
            _interactionRules = interactionRules;
            messageQueue = new MessageQueue();
//            BuildElementRulePairs(elementRepository, _interactionRules);
        }

        internal void Terminate()
        {
            messageQueue.CancelWait();
        }

        public void AddChangeEvent(IElement element)
        {
            messageQueue.AddMessage(element);
        }
        public void AddChangeEvents(List<IElement> elements)
        {
            messageQueue.AddMessages(elements);
        }

        #endregion

        #region This stuff is only used for testing ... I'd like to get rid of it to reduce overhead

        public int VertexCount
        {
            get { return graph.VertexCount; }
        }

        public int EdgeCount
        {
            get { return graph.EdgeCount; }
        }

        public IEnumerable<IElement> Vertices
        {
            get { return graph.Vertices; }
        }

        public void RebuildInterElementInteractionGraph(IEnumerable<IElement> elements)
        {
            //BuildInterElementInteractionGraph(elementRepository.ElementsGeoLocated.Where(o => !excludedVertices.Contains(o)));//TODO is this right? what about ungeolocated (nevermind i just changed)
            BuildInterElementInteractionGraph(elements.Where(o => !_excludedVertices.Contains(o)));
        }

        public void ExecuteCurrentInteractionGraph()
        {
            foreach (var vertex in graph.Vertices.Where(graph.IsInEdgesEmpty).ToArray())
            {
                Walk(vertex);
            }
        }

        private void Walk(IElement vertex)
        {
            vertex.NumberIncomingEdgesFired++;

            foreach (var edge in graph.OutEdges(vertex))
            {
                Walk(edge.Target);
            }
        }

        #endregion

        #region Graph building methods

        private void BuildInterElementInteractionGraph(IEnumerable<IElement> elements)
        {
            var start = DateTime.Now;
            graph = new BidirectionalGraph<IElement, TaggedEdge<IElement, IInteractionRule>>();
            var elementList = elements.ToList();
            CurrentInteraction = "Setting up interactions";
            //Traversing through the interaction rules
            foreach (var rule in _interactionRules)
            {
                if (string.IsNullOrEmpty(rule.Publisher))
                    continue;
                if (rule.IsDisabled)
                    continue;

                var rule1 = rule;

                //Get all objects of publisher type for rule1
                foreach (var triggerElement in elementList.Where(o => o.ElementType == rule1.Publisher))
                {
                    //Get all objects subscribing to rule1
                    foreach (var affectedElement in elementList.Where(o => rule1.Subscribers.Any(s => s == o.ElementType)))
                    {
                        if (ReferenceEquals(triggerElement, affectedElement)) continue;
                        if (IsEdgeExcluded(triggerElement, affectedElement, rule)) continue;
                        if (!dirtyElements.Contains(triggerElement) && !dirtyElements.Contains(affectedElement)) continue;
                        if (rule.EvaluateConditions(triggerElement, affectedElement))
                            AddGraphEdge(triggerElement, affectedElement, rule);
                    }
                }
            }
            Debug.WriteLine(string.Format("BuildInterElementInteractionGraph (includes evaluation conditions) took {0} milliseconds", (DateTime.Now - start).TotalMilliseconds));
        }

        private void AddGraphEdge(IElement vertexA, IElement vertexB, IInteractionRule rule)
        {
            if (!graph.ContainsVertex(vertexA))
            {
                vertexA.NumberIncomingEdgesFired = 0;
                graph.AddVertex(vertexA);
            }

            if (!graph.ContainsVertex(vertexB))
            {
                vertexB.NumberIncomingEdgesFired = 0;
                graph.AddVertex(vertexB);
            }

            graph.AddEdge(new TaggedEdge<IElement, IInteractionRule>(vertexA, vertexB, rule));
        }

        #endregion
        #region Element tuple list non save hack fix region
//        public void UpdateElementRulePairs(IEnumerable<IElement> elementList)
//        {
//            //not sure how important this really is since it only seems to be used in the constructor.
//            ElementRulePairs.Clear();
//            BuildElementRulePairs(elementList, _interactionRules);
//        }
        //Hackity hack hack hack, so that the elementtuplelist doesnt have to be saved
//        private void BuildElementRulePairs(IEnumerable<IElement> affectedElements, IEnumerable<IInteractionRule> interactionRulesToUse)
//        {
//            var start = DateTime.Now;
//            var elements = affectedElements.ToList();
//            //Traversing through the interaction rules
//            foreach (var rule in interactionRulesToUse)
//            {
//                if (string.IsNullOrEmpty(rule.Publisher))
//                    continue;
//                if (rule.IsDisabled)
//                    continue;
//
//                var rule1 = rule;
//                //Get all objects of publisher type for rule1
//                foreach (var triggerElement in elements.Where(o => o.ElementType == rule1.Publisher))
//                {
//                    //Get all objects subscribing to rule1
//                    foreach (var affectedElement in elements.Where(o => rule1.Subscribers.Any(s => s == o.ElementType)))
//                    {
//                        // if (!dirtyElements.Contains(triggerElement) && !dirtyElements.Contains(affectedElement)) continue;
//                        if (rule1.EvaluateConditions(triggerElement, affectedElement))
//                            ElementRulePairs.Add(triggerElement, affectedElement);
//                    }
//                }
//            }
//            Debug.WriteLine(string.Format("BuildingElementRule pairs (includes evaluation conditions) took {0} milliseconds", (DateTime.Now - start).TotalMilliseconds));
//        }
        #endregion
        #region Graph executing methods; this is where interaction rules are applied

        private void WalkAndExecute(IEnumerable<IElement> allElements, IElement vertex)
        {
            foreach (var edge in graph.OutEdges(vertex))
            {
                CurrentInteraction = edge.Tag.Name;
                try
                {
                    edge.Tag.ExecuteRule(vertex, edge.Target);
                }
                catch (Exception ex)
                {
                    //DepictionAccess.NotificationService.SendNotification(string.Format("Error executing interaction rule: {0}: {1}", edge.Tag.Name, ex.Message));
                }
                //as of 1.4 the whole restore thing is totally messed up,rather i dont know how it works so to make my self feel better i say it is 
                //messed up
                SetDoNotRestore(edge.Target); // If rule has been executed on an object once, it should not be restored.  Duh!
                ElementRulePairs.Add(edge.Source, edge.Target);

                edge.Target.NumberIncomingEdgesFired++;
                ExcludeEdge(vertex, edge.Target, edge.Tag);
            }

            // InDegree indicates the number of incoming edges
            // to a vertex. If the NumberIncomingEdgesFired(number of times the
            // vertex has been acted upon) is greater than the
            // number of degrees, the rules have been applied at
            // least once
            if (graph.InDegree(vertex) <= vertex.NumberIncomingEdgesFired)
            {
                ExcludeVertex(vertex);
            }
            foreach (DepictionInteractionMessage msg in messageQueue.PopAll())
            {
                AddToDirtyList(msg.Publisher);
            }
            RebuildInterElementInteractionGraph(allElements);
        }

        /// <summary>
        /// This is the main method of the InteractionRule graph.
        /// This walks the graph of elements and interactions, executing the interactions as necessary.
        /// When this is done, no more interactions need to be applied (in theory).
        ///
        /// Current Algorithm:
        /// <ol>
        /// <li>  Build Graph, Setting NumberIncomingEdgesFired for all vertices to 0. 
        ///      Ignore TriggerObjects vertices with IsDirty == false; (As rules are applied in cascading fashion, the isDirty flag is set on downstream objects, 
        ///          so subsequent graph rebuilding will take that into consideration.</li>
        /// <li>  Find Most Likely Vertex in which to start.
        ///       <ol>
        ///         <li>  Vertex has the lowest number of incident edges</li>
        ///         <li> In the case where there are a number with the minimum incident edges, select the vertex with the highest NumberIncomingEdgesFired.</li>
        ///         <li> If no candidate scores higher, just pick one, it doesn't matter.</li>
        ///       </ol>
        /// </li>
        /// <li>  Fire all outgoing rules for selected vertex, incrementing the NumberIncomingEdgesFired for the target/affected object every time a rule is applied to it.</li>
        /// <li>  Exclude all rules/edges just fired.</li>
        /// <li>  If for selected vertex (IncidentEdges &lt;= NumberIncomingEdgesFired), exclude the vertex.</li>
        /// <li>  Recalculate Graph, excluding the excluded Vertices and Edges.</li>
        /// <li>  Repeat steps 1-5 until graph is empty (no more edges).</li>
        /// </ol>
        /// </summary>
        public void ExecuteInteractions(ReadOnlyCollection<IElement> elementList, IEnumerable<DepictionInteractionMessage> changes)
        {
            var start = DateTime.Now;
            messageQueue.Clear();   // make sure we start with a clean slate, no leftover messages.
            ProcessingMessageQueue = true;
            BuildChangeGraph(elementList, changes);
            IElement vertex;
            while (TryFindNextRootVertex(out vertex))
            {
                WalkAndExecute(elementList, vertex);
            }

            Debug.WriteLine(string.Format("ExecuteInteractions (includes an entire pass of interactions) took {0} milliseconds", (DateTime.Now - start).TotalMilliseconds));
            CurrentInteraction = "";
            TriggerInteractionsEnd();
            ProcessingMessageQueue = false;
        }
        protected void TriggerInteractionsEnd()
        {
            if (InteractionsDone == null) return;
            if (System.Windows.Application.Current != null)
                if (!System.Windows.Application.Current.Dispatcher.CheckAccess())
                {
                    System.Windows.Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(TriggerInteractionsEnd));
                    return;
                }
            if (InteractionsDone != null)
            {
                InteractionsDone();
            }
        }

        private bool NeedsRestore(IElement element)
        {
            return !objectsNotToBeRestored.Contains(element);
        }

        private void SetDoNotRestore(IElement element)
        {
            if (!objectsNotToBeRestored.Contains(element))
                objectsNotToBeRestored.Add(element, null);
        }

        //soooo hacky
        private void AddToDirtyList(IElement dirtyElement)
        {
            if (dirtyElements.Contains(dirtyElement)) return;
            dirtyElements.Add(dirtyElement);
            if (NeedsRestore(dirtyElement))
            {
                SetDoNotRestore(dirtyElement);
                //TODO this is a hack, because i don't know how the dirty list actually works. hack
                if (dirtyElement.HasRestorableProperty)
                {
                    dirtyElement.RestoreProperties(true);
                }
            }
        }

        public void BuildChangeGraph(IEnumerable<IElement> elementList, IEnumerable<DepictionInteractionMessage> changes)
        {
            var start = DateTime.Now;
            _excludedVertices.Clear();
            excludedEdges.Clear();
            dirtyElements.Clear();

            objectsNotToBeRestored.Clear();
            foreach (var msg in changes)
            {
                AddToDirtyList(msg.Publisher);
                var objectsToRemove = new List<IElement>();
                foreach (var element in ElementRulePairs.GetAffectedElementsRecursive(msg.Publisher))
                {
                    AddToDirtyList(element);
                    objectsToRemove.Add(element);
                }
                foreach (var obj in objectsToRemove)
                    ElementRulePairs.Remove(msg.Publisher, obj);

            }
            Debug.WriteLine(string.Format("BuildChangeGraph (includes restoration) took {0} milliseconds", (DateTime.Now - start).TotalMilliseconds));
            RebuildInterElementInteractionGraph(elementList);
        }

        #endregion

        #region Graph state management during execution

        /// <summary>
        /// We exclude an edge to prevent looping
        /// </summary>
        public void ExcludeEdge(IElement trigger, IElement affected, IInteractionRule rule)
        {
            var key = new { source = trigger, target = affected };
            if (!excludedEdges.ContainsKey(key)) excludedEdges.Add(key, rule);
        }

        /// <summary>
        /// We exclude edges if the interaction rule associated with it has already been applied this round.
        /// This prevents recursion. IsEdgeExluded checks if a rule has already been applied.
        /// </summary>
        private bool IsEdgeExcluded(IElement trigger, IElement affected, IInteractionRule rule)
        {
            IInteractionRule excludeRule;

            return excludedEdges != null && excludedEdges.TryGetValue(new { source = trigger, target = affected }, out excludeRule) && excludeRule == rule;
        }

        /// <summary>
        /// We exclude vertexes to make sure each interaction rule is only applied once
        /// </summary>
        /// <param name="vertex"></param>
        public void ExcludeVertex(IElement vertex)
        {
            _excludedVertices.Add(vertex, null);
            dirtyElements.Remove(vertex);
        }

        /// <summary>
        /// Find the vertex to execute on next.
        /// The rule here is take the vertex with the minimum incident edges;
        /// if there is a tie, the vertex with the most ticks wins
        /// </summary>
        private bool TryFindNextRootVertex(out IElement vertex)
        {
            int minInEdgeElementCount = 0;
            IElement minInEdgeElement = null;
            vertex = null;
            if (graph.VertexCount <= 0) return false;
            int minInEdgeCount = graph.Vertices.Min(obj => graph.InDegree(obj));
            foreach (var obj in graph.Vertices)
            {
                if (minInEdgeCount != graph.InDegree(obj)) continue;
                minInEdgeElementCount++;
                minInEdgeElement = obj;
            }

            if (minInEdgeElementCount == 1)
            {
                vertex = minInEdgeElement;
                return true;
            }

            int maxTix = -1;
            IElement elementWithMostTicks = null;

            foreach (var element in graph.Vertices)
            {
                int inEdgeCount = graph.InDegree(element);
                if (minInEdgeCount != inEdgeCount || maxTix >= element.NumberIncomingEdgesFired) continue;
                maxTix = element.NumberIncomingEdgesFired;
                elementWithMostTicks = element;
            }

            if (elementWithMostTicks != null)
            {
                vertex = elementWithMostTicks;
                return true;
            }

            return false;
        }

        #endregion
    }
}
