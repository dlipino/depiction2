﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.Interactions
{
    public class ElementTupleList : IXmlSerializable
    {

        private readonly Dictionary<IElement, List<IElement>> triggerToAffectedDictionary = new Dictionary<IElement, List<IElement>>();
        private readonly Dictionary<IElement, List<IElement>> affectedToTriggersDictionary = new Dictionary<IElement, List<IElement>>();

        public void Add(IElement element1, IElement element2)
        {
//Faild attempt to understand how associate element events works with interactions
//            if (ReferenceEquals(element1, element2) && !element1.AssociatedElements.Any())
//            {
//                Debug.Assert(!ReferenceEquals(element1, element2));
//            }
            Debug.Assert(!ReferenceEquals(element1, element2));
            addToDictionary(element1, element2, triggerToAffectedDictionary);
            addToDictionary(element2, element1, affectedToTriggersDictionary);
        }

        private static void addToDictionary(IElement element1, IElement element2, Dictionary<IElement, List<IElement>> dictionary)
        {
            if (!dictionary.ContainsKey(element1))
                dictionary.Add(element1, new List<IElement> { element2 });
            else
            {
                List<IElement> affectedElements;
                dictionary.TryGetValue(element1, out affectedElements);
                affectedElements.Add(element2);
            }
        }

        public int Count
        {
            get { return triggerToAffectedDictionary.Count; }
        }

        public void Remove(IElement trigger, IElement affected)
        {
            removeFromDictionary(trigger, affected, triggerToAffectedDictionary);
            removeFromDictionary(affected, trigger, affectedToTriggersDictionary);
        }

        private static void removeFromDictionary(IElement element1, IElement element2, Dictionary<IElement, List<IElement>> dictionary)
        {
            List<IElement> elements;
            dictionary.TryGetValue(element1, out elements);
            if (elements != null)
            {
                elements.Remove(element2);
                if (elements.Count == 0)
                    dictionary.Remove(element1);
            }
        }

        public bool Exists(IElement element1, IElement element2)
        {
            List<IElement> elements;
            triggerToAffectedDictionary.TryGetValue(element1, out elements);
            return (elements != null) && elements.Contains(element2);
        }

        public void Clear()
        {
            triggerToAffectedDictionary.Clear();
            affectedToTriggersDictionary.Clear();
        }

        public ICollection<IElement> GetAffectedElementsRecursive(IElement trigger)
        {
            var elements = new HashSet<IElement>();
            AddAffectedElementToListRecursive(elements, trigger);
            elements.Remove(trigger);
            return elements;
        }

        private void AddAffectedElementToListRecursive(HashSet<IElement> elementList, IElement trigger)
        {
            List<IElement> elements;
            elementList.Add(trigger);
            if (triggerToAffectedDictionary.TryGetValue(trigger, out elements))
            {
                foreach (var element in elements)
                {
                    if (!elementList.Contains(element))
                        AddAffectedElementToListRecursive(elementList, element);
                }
            }

        }

        public IList<IElement> GetAffectedElements(IElement trigger)
        {
            List<IElement> elements;
            triggerToAffectedDictionary.TryGetValue(trigger, out elements);
            return elements;
        }

        public IList<IElement> GetTriggerElements(IElement affected)
        {
            List<IElement> elements;
            affectedToTriggersDictionary.TryGetValue(affected, out elements);
            return elements;
        }

        private List<KeyValuePair<string, string>> restoredElementKeys;

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
//            restoredElementKeys = SerializationService.Deserialize<List<KeyValuePair<string, string>>>("restoredElementKeys", reader);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var elementKeys = new List<KeyValuePair<string, string>>();
            foreach (var kvp in triggerToAffectedDictionary)
            {
                foreach (var value in kvp.Value)
                    elementKeys.Add(new KeyValuePair<string, string>(kvp.Key.ElementId, value.ElementId));
            }
//            SerializationService.Serialize("restoredElementKeys", elementKeys, writer);
        }

        public void RestoreElements(IEnumerable<IElement> elements)
        {
            foreach (var elementKey in restoredElementKeys)
            {
                IElement element1 = null, element2 = null;
                foreach (var element in elements)
                {
                    if (element.ElementId.Equals(elementKey.Key))
                        element1 = element;
                    if (element.ElementId.Equals(elementKey.Value))
                        element2 = element;
                }
                if (element1 != null && element2 != null)
                    Add(element1, element2);
            }
            restoredElementKeys = null;
        }

    }

}
