﻿using Depiction2.Base.StoryEntities;

namespace Depiction2.Core.Interactions
{
    public class DepictionInteractionMessage
    {
        private readonly IElement publisher;
        public DepictionInteractionMessage(IElement publisher)
        {
            this.publisher = publisher;
        }

        public IElement Publisher
        {
            get { return publisher; }
        }
    }
}
