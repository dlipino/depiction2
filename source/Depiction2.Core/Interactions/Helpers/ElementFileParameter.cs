﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.Interactions.Helpers
{
    [Serializable]
    public class ElementFileParameter : IElementFileParameter, IXmlSerializable
    {
        /// <summary>
        /// Gets or sets the sim object query.
        /// </summary>
        /// <value>The sim object query.</value>
        public string ElementQuery { get; set; }

        /// <summary>
        /// Gets or sets the type of the query.
        /// </summary>
        /// <value>The type of the query.</value>
        public ElementFileParameterType Type { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            return obj is ElementFileParameter && ElementQuery == ((ElementFileParameter)obj).ElementQuery && Type == ((ElementFileParameter)obj).Type;
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public XmlSchema GetSchema()
        {
           throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            bool readStart = false;
            if(reader.Name.Equals("ElementFileParameter"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            if (reader.Name.Equals("ElementQuery"))
            {
                ElementQuery = reader.ReadElementContentAsString();
//                ElementQuery = reader.ReadElementContentAsString("ElementQuery", ns);
            }
            if (reader.Name.Equals("Type"))
            {
                Type =
                    (ElementFileParameterType)
                    Enum.Parse(typeof (ElementFileParameterType), reader.ReadElementContentAsString());
                //            Type = (ElementFileParameterType)Enum.Parse(typeof(ElementFileParameterType), reader.ReadElementContentAsString("Type", ns));
            }
            if (readStart && reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
//            writer.WriteElementString("ElementQuery", ns, ElementQuery);
//            writer.WriteElementString("Type", ns, Type.ToString());
            writer.WriteElementString("ElementQuery", ElementQuery);
            writer.WriteElementString("Type", Type.ToString());

        }
    }
}
