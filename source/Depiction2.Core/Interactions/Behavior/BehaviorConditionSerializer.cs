﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.API.Service;
using Depiction2.API.Utilities;
using Depiction2.Base.Interactions;
using Depiction2.Core.Interactions.Helpers;

namespace Depiction2.Core.Interactions.Behavior
{
    public class BehaviorConditionSerializer : IXmlSerializable
    {
        public BehaviorConditionSerializer()
        {

        }

        public BehaviorConditionSerializer(string name, IEnumerable<IElementFileParameter> parameters)
        {
            Name = name;
            Parameters = parameters;
        }

        public string Name { get; set; }

        public IEnumerable<IElementFileParameter> Parameters { get; set; }

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            bool readStart = false;
            if(reader.Name.Equals("Behavior"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            if (reader.Name.Equals("Name"))
            {
                Name = reader.ReadElementContentAsString();
//                Name = reader.ReadElementContentAsString("Name", ns);
            }
            Parameters = new List<IElementFileParameter>(DepictionXmlFileUtilities.DeserializeItemList<IElementFileParameter>("Parameters", typeof(ElementFileParameter), reader));
            if(readStart && reader.NodeType.Equals(XmlNodeType.EndElement)) reader.ReadEndElement();

        }

        public void WriteXml(XmlWriter writer)
        {
//            writer.WriteElementString("Name", Name);
//            SerializationService.SerializeItemList("Parameters", typeof(ElementFileParameter), Parameters, writer,false);
        }
    }

}
