﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Depiction2.Base.Interactions;

namespace Depiction2.Core.Interactions.Behavior
{
    [XmlRoot(ElementName = "Behavior")]
    public class BehaviorSerializer : BehaviorConditionSerializer
    {//the condition and behaviors have divereged
        public BehaviorSerializer(string name, IEnumerable<IElementFileParameter> parameters)
            : base(name, parameters)
        { }
        public BehaviorSerializer()
        { }
    }
}
