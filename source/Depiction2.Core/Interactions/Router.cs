﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Depiction2.Base.Events;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.Core.Interactions
{
    public class Router : IInteractionRouter
    {
        private BackgroundWorker bgWorkerThread;
        //        private Timer waitTimer;
        private readonly MessageQueue messageQueue = new MessageQueue();
        private bool connectedToElementRepository = false;
        private bool routerRunning;
        private bool disposing;

        private IElementRepository _elementRepository = null;
        private IInteractionRuleRepository interactionsRepository;
        #region properties
        public int RulePairs
        {
            get
            {
                if (InteractionGraph == null) return 0;
                return InteractionGraph.ElementRulePairs.Count;
            }
        }
        public InteractionGraph InteractionGraph { get; private set; }
        public IInteractionRuleRepository AvailableInteractions { get { return interactionsRepository; } }
        #endregion
        #region Constructor/destructor

        public Router(IElementRepository elementRepository, IInteractionRuleRepository interactionsRepository)
        {
            this.interactionsRepository = interactionsRepository;
            _elementRepository = elementRepository;
        }

        public void Dispose()
        {
            disposing = true;
            DisconnectRouteFromElementRepositoryChanges();
            EndAsync();
        }
        #endregion

        #region control methods
        public bool BeginAsync()
        {
            if (bgWorkerThread != null || routerRunning)
            {
                //TODO log some sort of error
                return false;
            }
            SetupRouterInteractionGraphAndEventListeners();
            bgWorkerThread = CreateAndRunAsyncInteractionBackgroundWorker();
            return true;
        }

        public void EndAsync()
        {
            DisconnectRouterInteractionGraphAndEventListeners();
            if (bgWorkerThread != null)
            {
                bgWorkerThread.CancelAsync();
                messageQueue.AddMessage(null);
            }
        }

        #endregion

        #region testing methods

        internal void SetupRouterInteractionGraphAndEventListeners()
        {
            InteractionGraph = new InteractionGraph(_elementRepository.AllElements, interactionsRepository.AllInteractionRules);
            ConnectRouterToElementRepositoryChanges();
        }

        internal void DisconnectRouterInteractionGraphAndEventListeners()
        {
            if (InteractionGraph != null) InteractionGraph.Terminate();
            InteractionGraph = null;
            DisconnectRouteFromElementRepositoryChanges();
        }

        internal void DoSingleInteractionPassNoMessages()
        {
            IList<DepictionInteractionMessage> messages;
            while ((messages = messageQueue.PopAll()) != null && messages.Count > 0)
            {
                InteractionGraph.ExecuteInteractions(_elementRepository.AllElements, messages);
            }
        }
        #endregion
        #region repo connectors

        protected void ConnectRouterToElementRepositoryChanges()
        {
            if (_elementRepository != null && !connectedToElementRepository)
            {
                _elementRepository.ElementCollectionChanged += ElementRepositoryElementListChanged;
                _elementRepository.ElementPropertyChanged += _elementRepository_ElementPropertyChanged;
            }
        }

        protected void DisconnectRouteFromElementRepositoryChanges()
        {
            if (_elementRepository.AllElements == null) return;
            connectedToElementRepository = false;
            _elementRepository.ElementCollectionChanged -= ElementRepositoryElementListChanged;
            _elementRepository.ElementPropertyChanged -= _elementRepository_ElementPropertyChanged;
        }

        void _elementRepository_ElementPropertyChanged(object sender, PropertyChangeEventArg e)
        {
            TriggerRouterForElementPropertyChange(e.Element, e.PropertyName);
        }

        void ElementRepositoryElementListChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    var newElements = e.NewItems.Cast<IElement>().ToList();
                    TriggerRouterForMultipleElements(newElements);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    var oldElements = e.OldItems.Cast<IElement>().ToList();
                    //This part isnt needed, the graph just has to get rerun because it sets thing back to normal state (or at least it should)
                    TriggerRouterForMultipleElements(oldElements);

                    break;
            }
        }

        #endregion
        #region backgroun worker creationg

        private BackgroundWorker CreateAndRunAsyncInteractionBackgroundWorker()
        {
            var bgWorker = new BackgroundWorker();
            bgWorker.DoWork += bgWorkerThread_DoWork;
            //Timing issues make this unsafe to use, so all the disconnect work is done in the do work
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.RunWorkerAsync(null);

            routerRunning = true;
            return bgWorker;
        }

        /// <summary>
        /// Design notes for Router and InteractionGraph, MessageQueue,
        /// (brian and jason) 4/4/2008
        /// 
        /// Our design is concerned with correctness of results at this juncture rather than performance.  That the current design yields correct results in 
        /// every case is an open question at this point, but it works, given the test cases that we have fed it.  We would like to build up
        /// a far more extensive unit test suite before we have a high confidence level in the results.
        /// 
        /// With the current Interaction Graph, we have the concept of passing the graph a set of changeMessages, and based on those changes, 
        /// excecuting the graph to completion.  At that point, our contention is (subject of course to counter examples) that:
        /// 1) All interaction rules that need to be fired for the given changeset are fired, none are missed.
        /// 2) No rule is fired more times than it needs to be.
        /// 
        /// Executing the graph to completion means to process the current changeset and all subsequent changes that are generated during as a result
        /// of the firing of interaction rules.
        /// 
        /// To make this happen we needed to distinguish between change messages that come as a result of user actions, ie, moving a building, etc, and change messages
        /// that come as a result of graph execution.
        /// 
        /// Since the graph execution is done on a background thread, we differentiate between messages coming from that thread and all others.  
        /// If a message comes in as a result of graph execution, that message is routed back to the executing graph so that the graph can update itself based
        /// on the updated changes.  All other change messages get routed to the changeMessageQueue to be consumed the next time the graph is executed.
        /// 
        /// This background threaded consuming of the messages and executing a changeset based on those change messages allows users to 
        /// continue to interact with the story elements, changing, moving, setting properties, etc, and the change execution background 
        /// thread will continue to process those changes until it is caught up and no more change messages are on the queue.
        /// 
        /// InteractionsDisabled property allows temporarily disabling interactions (throwing away changes messages that occur).
        /// It is used now in object creation where the ZOI creation of the object generates a change message, triggering a recalc, 
        /// but as the object has not  yet been added to the repository, the recalc is not only superfluous, but it does nothing 
        /// because the graph only acts on objects that are part of the repository. 
        /// 
        /// If we want to use this same mechanism (InteractionsDisabled) to perform bulk updates we should make so that the messages are not lost, but the 
        /// background thread just suspends processing of them.  That's not how it works now.
        /// 
        /// davidl  Jan 8 2011
        /// 
        /// A desired augmentation is to connect the post set actions to the router so that all inter and intra element changes are run by the interaction graph.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        private void bgWorkerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            if (worker == null) return;

            while (!worker.CancellationPending)
            {
                try
                {
                    if (worker.CancellationPending)
                        break;
                    var messages = messageQueue.WaitAndPopAll();
                    if (worker.CancellationPending)
                        break;
                    if (messages != null)
                    {
                        InteractionGraph.ExecuteInteractions(_elementRepository.AllElements, messages);
                    }
                }
                catch (Exception ex)
                {
                    //                    DepictionExceptionHandler.HandleException(ex, false, true);
                }
            }

            messageQueue.CancelWait();
            routerRunning = false;
            bgWorkerThread = null;
            //turning off the router is harder than it seems
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //            DisconnectRouterInteractionGraphAndEventListeners();
            //                        messageQueue.CancelWait();
            //            routerRunning = false;
            //            bgWorkerThread = null;
            var worker = sender as BackgroundWorker;
            if (worker == null) return;
            worker = null;
            if (disposing)
            {
                messageQueue.StopWait();
            }
        }
        #endregion
        public void TriggerRouterForElementPropertyChange(IElement element, string propName)
        {
            if (element == null) return;
            if (propName == null) propName = "";

            if (!interactionsRepository.DoesRuleWithElementTypeAndTriggerNameExist(element.ElementType, propName))
            {
                return;
            }
            //The catch for associated elements changes
            //Something new for 2.0, as of 10/30/2013 associated elements triggers will fire the from the 
            //parent element but the trigger property will have Associate.{propertyName} on it.
            //hacky like i can't even understand
            foreach (var rule in interactionsRepository.AllInteractionRules)
            {
                if (rule.Publisher.Equals(element.ElementType) && rule.Subscribers.Contains(element.ElementType))
                {
                    foreach (var trigger in rule.PublisherTriggerPropertyNames)
                    {
                        if (trigger.Contains("Associate."))
                        {
                            if (trigger.Equals(propName))
                            {
                                rule.ExecuteAssociatedBehavior(element, _elementRepository);
                            }
                        }
                    }
                }
            }

            Debug.WriteLine(propName + " property that changed for " + element.ElementType);
            TriggerRouterForMultipleElements(new[] { element });
        }
        internal void TriggerRouterForMultipleElements(IEnumerable<IElement> elements)
        {
            var triggerList = new List<IElement>();
            foreach (var element in elements)
            {
                if (element == null) continue;
                triggerList.Add(element);
            }
            if (InteractionGraph != null && InteractionGraph.ProcessingMessageQueue)
            {
                //#if DEBUG
                //                foreach (var element in triggerList)
                //                {
                //                    Debug.WriteLine(string.Format("GraphQueue:Queued for {0}", element.ElementType));
                //                }
                //#endif
                InteractionGraph.AddChangeEvents(triggerList);
            }
            else
            {
                //#if DEBUG
                //                foreach (var element in triggerList)
                //                {
                //                    Debug.WriteLine(string.Format("MainQueue: Queued for {0}", element.ElementType));
                //                }
                //#endif
                messageQueue.AddMessages(triggerList);
            }
        }
    }
}
