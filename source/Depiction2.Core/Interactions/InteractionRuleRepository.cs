﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.API.Utilities;
using Depiction2.Base.Interactions;

[assembly: InternalsVisibleTo("Depiction2.Story2IO.UnitTests")]
namespace Depiction2.Core.Interactions
{
    [DataContract]
    public class InteractionRuleRepository : IInteractionRuleRepository, IXmlSerializable
    {
        private HashSet<IInteractionRule> _activeStoryRules = new HashSet<IInteractionRule>();
        private HashSet<IInteractionRule> defaultRules = new HashSet<IInteractionRule>();

        [DataMember]
        public int Count
        {
            get { return InteractionRules.Count; }
        }
        [DataMember]
        private List<IInteractionRule> InteractionRules { 
            get
            {
                var combined = new HashSet<IInteractionRule>();
                
                combined.UnionWith(defaultRules);
                combined.UnionWith(_activeStoryRules);
                return combined.ToList();
            }
        }
      
        public ReadOnlyCollection<IInteractionRule> AllInteractionRules
        {
            get { return InteractionRules.AsReadOnly(); }
        }

        public IEnumerable<IInteractionRule> DefaultInteractionRules
        {
            get { return defaultRules; }
        }

        public IEnumerable<IInteractionRule> ActiveStoryInteractionRules
        {
            get { return _activeStoryRules; }
        }
        #region Instance things
        //this kind of makes generalized tests hellish
        //what is the difference between instance and making this a static class
        private static InteractionRuleRepository instance;
        internal static InteractionRuleRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InteractionRuleRepository();
                }
                return instance;
            }
        }

        #endregion
        #region constructor/destructor

        protected InteractionRuleRepository()
        {
            //InteractionRules = new List<IInteractionRule>();

        }
        #endregion
        public void LoadDefaultRulesIntoRepository(IEnumerable<IInteractionRule> interactionRulesToAdd)
        {
            if (interactionRulesToAdd != null)
            {
                foreach (var interactionRule in interactionRulesToAdd)
                {
                    defaultRules.Add(interactionRule);
                }
            }
        }
        public void LoadNonDefaultRulesIntoRepository(IEnumerable<IInteractionRule> interactionRulesToAdd)
        {
            if (interactionRulesToAdd != null)
            {
                foreach (var interactionRule in interactionRulesToAdd)
                {
                    _activeStoryRules.Add(interactionRule);
                }
            }
        }
        public bool DoesRuleWithElementTypeAndTriggerNameExist(string elementType, string triggerProperty)
        {
            //For current debugging purposes don't use linq, once stability is reached start using linq
            foreach (var iRule in InteractionRules)
            {
                var rule = iRule as InteractionRule;
                if (rule == null) continue;
                var elemToTriggerDictionary = rule.ElementAndPropertyTriggerDictionary;
                if (elemToTriggerDictionary.ContainsKey(elementType))
                {
                    if (string.IsNullOrEmpty(triggerProperty)) return true;
                    var propTriggers = elemToTriggerDictionary[elementType];
                    if (propTriggers.Contains(triggerProperty)) return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Adds the specified interaction rule to the repository if it does not already exist.
        /// </summary>
        /// <param name="interactionRule">The interaction rule to add.</param>
        /// <returns>Whether the rule was added.</returns>
        public bool AddInteractionRule(IInteractionRule interactionRule)
        {
            return AddInteractionRule(interactionRule, true);
        }

        public int AddInteractionsFromDirectory(string dirName)
        {
            //            var storyLoaders = DepictionAccess.ExtensionLibrary.DepictionVersionFileReaders;
            var count = 0;
            //            foreach(var loader in storyLoaders)
            //            {
            //                var interactions = loader.Value.GetInteractionRulesFromDirectory(dirName);
            //                count += interactions.Count;
            //                
            //                interactionRules.AddRange(interactions);
            //            }
            return count;
        }

        public bool AddInteractionRule(IInteractionRule interactionRule, bool runInteractions)
        {
            return _activeStoryRules.Add(interactionRule);
//            if (InteractionRules.Contains(interactionRule))
//                return false;
//            InteractionRules.Add(interactionRule);

            //            if (DepictionAccess.CurrentDepiction != null && DepictionAccess.CurrentDepiction.CompleteElementRepository != null && runInteractions)
            //            {
            //                foreach (var element in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements.Where(e => interactionRule.Publisher.Equals(e.ElementType)))
            //                {
            //                    //element.UpdateInteractions();
            //                }
            //            }

//            return true;
        }

        /// <summary>
        /// Remove the subscription to the specified rule
        /// </summary>
        public void RemoveInteractionRule(IInteractionRule rule)
        {
            _activeStoryRules.Remove(rule);
//            if (!InteractionRules.Contains(rule)) return;
//            InteractionRules.Remove(rule);

            //            if (DepictionAccess.CurrentDepiction != null &&
            //                DepictionAccess.CurrentDepiction.CompleteElementRepository != null)
            //            {
            //                foreach (var element in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements)
            //                {
            //                   // element.UpdateInteractions();
            //                }
            //            }
        }

        /// <summary>
        /// Gets all interaction rules for which the specified element type is either the 
        /// publisher or a subscriber.
        /// </summary>
        /// <param name="elementType">Type of element to search for.</param>
        /// <returns></returns>
        public IInteractionRule[] GetInteractionRules(string elementType)
        {
            IEnumerable<IInteractionRule> selectedRules =
                from r in InteractionRules
                where r.Subscribers.Any(s => s == elementType) || r.Publisher == elementType
                select r;

            return selectedRules.ToArray();
        }

        public void CopyInteractionRules(string oldElementType, string newElementType)
        {
            var rulesToAdd = new List<IInteractionRule>();
            foreach (var rule in InteractionRules)
            {
                if (rule.Publisher.Equals(oldElementType))
                {
                    var newRule = rule.DeepClone();
                    newRule.Publisher = newElementType;
                    rulesToAdd.Add(newRule);
                }

                if (rule.Subscribers.Any(s => s == oldElementType))
                {
                    rule.Subscribers.Add(newElementType);
                }
            }

            InteractionRules.AddRange(rulesToAdd);
        }

        public void Clear()
        {
            InteractionRules.Clear();
        }

        #region legacy 1.4 xml save/load
        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool readStart = false;
            //            if (reader.Name.Equals("InteractionRuleRepository"))
            if (reader.Name.Equals("DepictionInteractions"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            var loadedRules =
                new List<IInteractionRule>(DepictionXmlFileUtilities.DeserializeItemList<IInteractionRule>("Rules", typeof(InteractionRule), reader));
            _activeStoryRules = new HashSet<IInteractionRule>(loadedRules);
            if (readStart && reader.NodeType.Equals(XmlNodeType.EndElement)) reader.ReadEndElement();
        }
        static public List<IInteractionRule> GetRulesFromXmlStream(XmlReader reader)
        {
            bool readStart = false;
            //            if (reader.Name.Equals("InteractionRuleRepository"))
            if (reader.Name.Equals("DepictionInteractions"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            var loadedRules =
                new List<IInteractionRule>(DepictionXmlFileUtilities.DeserializeItemList<IInteractionRule>("Rules", typeof(InteractionRule), reader));
          
            if (readStart && reader.NodeType.Equals(XmlNodeType.EndElement)) reader.ReadEndElement();
            return loadedRules;
        } 
        public void WriteXml(XmlWriter writer)
        {
            //            var activeRules = new List<IInteractionRule>();
            //            foreach(var rule in interactionRules)
            //            {
            //                if(!rule.IsDisabled)
            //                {
            //                    activeRules.Add(rule);
            //                }
            //            }
            //            SerializationService.SerializeItemList("Rules", typeof(InteractionRule), activeRules, writer);
        }
        #endregion
    }

}
