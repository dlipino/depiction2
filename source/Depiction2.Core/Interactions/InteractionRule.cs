﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.API.Service;
using Depiction2.API.Utilities;
using Depiction2.Base;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Interactions.Behavior;
using Depiction2.Core.Interactions.Condition;

namespace Depiction2.Core.Interactions
{
    [DataContract]
    public class InteractionRule : IInteractionRule, IEqualityComparer<IInteractionRule>, IXmlSerializable
    {
        #region fields

        string sessionIdentifier = Guid.NewGuid().ToString();

        #endregion

        #region properties

        public string UniqueTempSessionId
        {
            get { return sessionIdentifier; }
            private set { sessionIdentifier = value; }//Used for cloning
        }

        public bool IsDefaultRule { get; set; }
        public string RuleSource { get; set; }
        [DataMember, DefaultValue(false)]
        public bool IsDisabled { get; set; }

        [DataMember, DefaultValue("")]
        public string Name { get; set; }
        [DataMember, DefaultValue("")]
        public string Description { get; set; }
        [DataMember, DefaultValue("")]
        public string Author { get; set; }

        [DataMember, DefaultValue("")]
        public string Publisher { get; set; }
        [DataMember]
        public HashSet<string> PublisherTriggerPropertyNames { get; private set; }
        [DataMember]
        public Dictionary<string, HashSet<string>> Conditions { get; private set; }

        [DataMember]
        public List<string> Subscribers { get; private set; }
        [DataMember]
        public HashSet<string> SubscriberTriggerPropertyNames { get; private set; }
        [DataMember]
        public Dictionary<string, IElementFileParameter[]> Behaviors { get; private set; }


        //Combination of publisher and subscriber plus the trigger properties
        public Dictionary<string, HashSet<string>> ElementAndPropertyTriggerDictionary { get; private set; }

        #endregion
        #region constructors

        public InteractionRule()
        {
            Subscribers = new List<string>();
            PublisherTriggerPropertyNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            SubscriberTriggerPropertyNames = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            Behaviors = new Dictionary<string, IElementFileParameter[]>();
            Conditions = new Dictionary<string, HashSet<string>>();
            ElementAndPropertyTriggerDictionary = new Dictionary<string, HashSet<string>>();
            IsDisabled = false;
            Author = "";
            Description = "";
            Publisher = "";
            Name = "";
        }

        #endregion

        #region methods
        [OnDeserializing]
        internal void SetupElementTriggerDictionary(StreamingContext context)
        {
            SetupElementAndTriggerDictionary();
        }

        protected void SetupElementAndTriggerDictionary()
        {
            ElementAndPropertyTriggerDictionary.Clear();
            if (Publisher == null) return;
            ElementAndPropertyTriggerDictionary.Add(Publisher, PublisherTriggerPropertyNames);
            foreach (var subscriber in Subscribers)
            {
                var allProps = new HashSet<string>();
                foreach (var prop in SubscriberTriggerPropertyNames)
                {
                    allProps.Add(prop);
                }
                foreach (var list in Conditions.Values)
                {//COmbine all the condition triggers

                    foreach (var prop in list)
                    {
                        allProps.Add(prop);
                    }
                }
                if (ElementAndPropertyTriggerDictionary.ContainsKey(subscriber))
                {
                    foreach (var prop in allProps) ElementAndPropertyTriggerDictionary[subscriber].Add(prop);
                }
                else
                {
                    ElementAndPropertyTriggerDictionary.Add(subscriber, allProps);
                }
            }
        }

        public IInteractionRule MakeFunctionalCopyOfInteractionRule()
        {
            return null;
            //            var stream = new MemoryStream();
            //            SerializationService.SerializeObject("InteractionRule", this, typeof(InteractionRule), stream);
            //            stream.Position = 0;
            //            return (InteractionRule)SerializationService.DeserializeObject("InteractionRule", typeof(InteractionRule), stream);
        }

        //Happens when the publisher and subscriber are the same, ie when an abitrary associated element triggers
        //a change in the parent
        public void ExecuteAssociatedBehavior(IElement subscriber, IElementRepository parameterElements)
        {
            if (Behaviors != null)
                foreach (var behavior in Behaviors)
                {

                    IBehavior behaviorToDo = null;
                    try
                    {
                        var lazy =
                            ExtensionService.Instance.DepictionBehaviors.FirstOrDefault(
                                t => t.Metadata.Name.Equals(behavior.Key));
                        if (lazy != null)
                        {
                            behaviorToDo = lazy.Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Silently swallow deprecated behaviors
                        continue;
                    }

                    if (behaviorToDo == null)
                    {
                        //Debug.Assert(behaviorToDo != null, string.Format("Could not find expected behavior: {0}", behavior.Key));
                        continue;
                    }
                    BehaviorResult result;
                    var behaviorParameters = LegacyHelpers.BuildParameters((behaviorToDo.DepictionParameters != null) ? behaviorToDo.DepictionParameters.Select(p => p.ParameterType) :
                       null, behavior.Value, subscriber, subscriber, parameterElements);
                    try
                    {
                        result = behaviorToDo.DoBehavior(subscriber, behaviorParameters);
                    }
                    catch (Exception ex)
                    {
                        //                        DepictionAccess.NotificationService.DisplayMessageString(
                        //                            string.Format("The \"{0}\" interaction could not run, because: {1}.", Name, ex.Message));
                        break;
                    }
                    //                    if (result.SubscriberHasChanged)//TODO may have bit of more than i can chew, (not sure what this was supposed to do 10/2013)
                    //                        subscriber.UpdateInteractions();
                }
        }

        //This is used 
        public void ExecuteRule(IElement publisher, IElement subscriber)
        {
            if (Behaviors != null)
                foreach (var behavior in Behaviors)
                {

                    IBehavior behaviorToDo = null;
                    try
                    {
                        var lazy =
                            ExtensionService.Instance.DepictionBehaviors.FirstOrDefault(
                                t => t.Metadata.Name.Equals(behavior.Key));
                        if (lazy != null)
                        {
                            behaviorToDo = lazy.Value;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Silently swallow deprecated behaviors
                        continue;
                    }


                    if (behaviorToDo == null)
                    {
                        //Debug.Assert(behaviorToDo != null, string.Format("Could not find expected behavior: {0}", behavior.Key));
                        continue;
                    }
                    var parameters = LegacyHelpers.BuildParameters((behaviorToDo.DepictionParameters != null) ? behaviorToDo.DepictionParameters.Select(p => p.ParameterType) :
                        null, behavior.Value, publisher, subscriber, null);
                    BehaviorResult result;
                    try
                    {
                        result = behaviorToDo.DoBehavior(subscriber, parameters);
                    }
                    catch (Exception ex)
                    {
                        //                        DepictionAccess.NotificationService.DisplayMessageString(
                        //                            string.Format("The \"{0}\" interaction could not run, because: {1}.", Name, ex.Message));
                        break;
                    }
                    //                    if (result.SubscriberHasChanged)//TODO may have bit of more than i can chew, (not sure what this was supposed to do 10/2013)
                    //                        subscriber.UpdateInteractions();
                }
        }

        public bool EvaluateConditions(IElement publisher, IElement subscriber)
        {
            //throw new NotImplementedException();//throwing this exception would make the whole interaction system fail, would trigger a message change
            if (Conditions != null)
                foreach (var conditionPair in Conditions)
                {
                    var lazy =
                        ExtensionService.Instance.DepictionConditions.FirstOrDefault(
                     t => t.Metadata.Name.Equals(conditionPair.Key));
                    if (lazy == null) continue;
                    var condition = lazy.Value;
#if DEBUG
                    Debug.Assert(condition != null, string.Format("Could not find expected condition: {0}", conditionPair.Key));
#endif
                    //var parameters =  BuildParameters(condition.Parameters.Select(c => c.ParameterType), conditionPair.Value, publisher, subscriber);
                    if (!condition.Evaluate(publisher, subscriber, null))
                        return false;
                }
            return true;
        }

        #endregion

        #region deep clone
        public IInteractionRule DeepClone()
        {
            var cloneWithMatchingId = (InteractionRule)MakeFunctionalCopyOfInteractionRule();
            cloneWithMatchingId.UniqueTempSessionId = UniqueTempSessionId;
            cloneWithMatchingId.IsDefaultRule = IsDefaultRule;
            return cloneWithMatchingId;
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        #endregion
        #region A bunch of equals and equals helpers
        bool IEqualityComparer<IInteractionRule>.Equals(IInteractionRule x, IInteractionRule y)
        {
            return Equals(x, y);
        }

        public static bool KeyEquals(IInteractionRule one, IInteractionRule other)
        {
            string oneId = null;
            string otherId = null;
            if (one != null) oneId = one.UniqueTempSessionId;
            if (other != null) otherId = other.UniqueTempSessionId;
            if (string.IsNullOrEmpty(oneId) || string.IsNullOrEmpty(otherId)) return false;
            return Equals(oneId, otherId);
        }

        public bool KeyEquals(IInteractionRule other)
        {
            return KeyEquals(this, other);
        }
        public bool RuleEquals(IInteractionRule other)
        {
            if (!(other != null
                   && Equals(Name, other.Name)
                   && Equals(Description, other.Description)
                   && Equals(Author, other.Author)))
            {
                return false;
            }
            if (!FunctionallyEquals(other)) return false;
            return true;
        }
        public bool FunctionallyEquals(IInteractionRule other)
        {
            if (other == null || !Equals(Publisher, other.Publisher))
                return false;

            //                || !Subscribers.Equals(other.Subscribers)
            if (Subscribers.Count != other.Subscribers.Count) return false;
            for (int i = 0; i < Subscribers.Count; i++)
            {
                if (!Subscribers[i].Equals(other.Subscribers[i])) return false;
            }
            if (!IsParamDictionaryEqual(Behaviors, other.Behaviors)) return false;

            if (Conditions.Count != other.Conditions.Count) return false;
            foreach (var key in Conditions.Keys)
            {
                if (!other.Conditions.ContainsKey(key)) return false;
                var val1 = Conditions[key];
                var val2 = other.Conditions[key];
                if (!val1.SetEquals(val2)) return false;
            }
            return true;
        }

        private static bool IsParamDictionaryEqual(Dictionary<string, IElementFileParameter[]> paramDictionary, IDictionary<string, IElementFileParameter[]> paramDictionary2)
        {
            if (paramDictionary.Count != paramDictionary2.Count) return false;
            foreach (var key in paramDictionary.Keys)
            {
                if (!paramDictionary2.ContainsKey(key)) return false;
                var val1 = paramDictionary[key];
                var val2 = paramDictionary2[key];
                if (val1.Length != val2.Length) return false;
                for (int i = 0; i < val1.Length; i++)
                {
                    if (!val1[i].Equals(val2[i])) return false;
                }
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return RuleEquals(obj as InteractionRule);
            //            return KeyEquals(this, obj as InteractionRule);
        }

        public static bool Equals(IInteractionRule one, IInteractionRule other)
        {
            return one.RuleEquals(other as InteractionRule);
            //return KeyEquals(one, other);
        }

        public int GetHashCode(IInteractionRule obj)
        {
            var hc = obj.Publisher.GetHashCode();
            foreach (var pTrigger in obj.PublisherTriggerPropertyNames)
            {
                hc = (hc << 1) ^ (pTrigger.GetHashCode());
            }
            foreach (var condition in obj.Conditions)
            {
                hc = (hc << 1) ^ condition.Key.GetHashCode();
                foreach (var val in condition.Value)
                {
                    hc = (hc << 1) ^ val.GetHashCode();
                }
            }
            foreach (var behavior in obj.Behaviors)
            {
                hc = (hc << 1) ^ behavior.Key.GetHashCode();
            }
            foreach (var subscriber in Subscribers)
            {
                hc = (hc << 1) ^ subscriber.GetHashCode();
            }
            foreach (var pTrigger in obj.SubscriberTriggerPropertyNames)
            {
                hc = (hc << 1) ^ (pTrigger.GetHashCode());
            }

            return hc;
        }

        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        #endregion
        #region to string

        public override string ToString()
        {
            return Description;
        }
        #endregion
        #region xml serialization

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public static void SaveRulesToXmlFile(IEnumerable<IInteractionRule> rules, string path)
        {
            using (var stream = new FileStream(path, FileMode.Create))
            {
                SaveRulesToXmlStream(stream, rules);
            }
        }
        //TODO re-enable these things
        public static void SaveRulesToXmlStream(Stream stream, IEnumerable<IInteractionRule> rules)
        {
            //            // Use invariant format for persisting numbers and datetimes.
            //            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            //            try
            //            {
            //                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            //                using (var writer = SerializationService.GetXmlWriter(stream))
            //                //                using (var writer = XmlDictionaryWriter.CreateTextWriter(stream, Encoding.UTF8, false))
            //                {
            //                    SerializationService.SerializeItemList("InteractionRules", typeof(InteractionRule), rules, writer, true);
            //                }
            //            }
            //            finally
            //            {
            //                Thread.CurrentThread.CurrentCulture = culture;
            //            }
        }

        public static IList<IInteractionRule> LoadRulesFromXmlFile(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                return LoadRulesFromXmlStream(stream);
            }
        }

        public static IList<IInteractionRule> LoadRulesFromDirectory(string path)
        {
            var rules = new List<IInteractionRule>();
            var files = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var interactionRules = LoadRulesFromXmlFile(file);
                if (interactionRules != null)
                    rules.AddRange(interactionRules);
            }

            return rules;
        }

        public static IList<IInteractionRule> LoadRulesFromXmlStream(Stream stream)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var reader = DepictionXmlFileUtilities.GetXmlReader(stream))
                {

                    return DepictionXmlFileUtilities.DeserializeItemList<IInteractionRule>("InteractionRules", typeof(InteractionRule), reader);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }


        public void ReadXml(XmlReader reader)
        {
            if (!reader.Name.Equals("InteractionRule")) return;
            var xmlDictionary = new Dictionary<string, string>();

            reader.ReadStartElement();
            //Hack
            var publisherType = string.Empty;
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("InteractionRule", StringComparison.InvariantCultureIgnoreCase))
            {
                var name = reader.Name;
                if (name.Equals("Publisher"))
                {
                    publisherType = reader.GetAttribute("Type");
                }
                xmlDictionary.Add(name, reader.ReadInnerXml());
            }

            if (xmlDictionary.ContainsKey("Name")) { Name = xmlDictionary["Name"]; }
            if (xmlDictionary.ContainsKey("Description")) { Description = xmlDictionary["Description"]; }
            if (xmlDictionary.ContainsKey("Author")) { Author = xmlDictionary["Author"]; }

            if (xmlDictionary.ContainsKey("Publisher"))
            {
                var stringReader = new StringReader(xmlDictionary["Publisher"]);
                var pubReader = XmlReader.Create(stringReader);
                if (publisherType == null)
                {
                    Publisher = xmlDictionary["Publisher"];
                }
                else
                {
                    Publisher = publisherType;
                    pubReader.Read();
                    PublisherTriggerPropertyNames.Clear();
                    if (pubReader.Name.Equals("PublisherTriggerProperties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        pubReader.Read();
                        while (pubReader.Name.Equals("Property"))
                        {
                            //TODO a conversion from 1.4 property trigger names to 2.0 property trigger names needs to occur
                            //these will be the properties that once user properties and got turned into standard element properties
                            var attrName = pubReader.GetAttribute("name");
                            if (!string.IsNullOrEmpty(attrName))
                            {
                                //convert to 2.0 name
                                if (attrName.Equals("Position"))
                                {
                                    attrName = "GeoLocation";
                                }
                                else if (attrName.Equals("ZoneOfInfluence"))
                                {
                                    attrName = "ElementGeometry";
                                }
                                PublisherTriggerPropertyNames.Add(attrName);
                            }

                            pubReader.Read();
                        }
                        pubReader.Read();
                    }
                    pubReader.Read();
                }
            }
            if (xmlDictionary.ContainsKey("Conditions"))
            {
                var rawString = xmlDictionary["Conditions"];

                var finalXML = "<Conditions>" + rawString + "</Conditions>";
                var stringReader = new StringReader(finalXML);
                var pubReader = XmlReader.Create(stringReader);
                var conditionList = DepictionXmlFileUtilities.DeserializeItemList<ConditionSerializer>("Conditions", typeof(ConditionSerializer), pubReader);
                Conditions.Clear();
                foreach (var condition in conditionList)
                    Conditions.Add(condition.Name, condition.ConditionTriggerProperties);
            }
            if (xmlDictionary.ContainsKey("Subscribers"))
            {
                var rawString = xmlDictionary["Subscribers"];
                var finalXML = "<Subscribers>" + rawString + "</Subscribers>";
                var stringReader = new StringReader(finalXML);
                var subscriberReader = XmlReader.Create(stringReader);

                subscriberReader.Read();
                subscriberReader.Read();
                while (subscriberReader.Name.Equals("String"))
                {
                    Subscribers.Add(subscriberReader.ReadElementString("String"));
                }
                if (subscriberReader.Name.Equals("SubscriberTriggerProperties", StringComparison.InvariantCultureIgnoreCase))
                {
                    subscriberReader.Read();
                    while (subscriberReader.Name.Equals("Property"))
                    {
                        //TODO a conversion from 1.4 property trigger names to 2.0 property trigger names needs to occur
                        //these will be the properties that once user properties and got turned into standard element properties
                        var attrName = subscriberReader.GetAttribute("name");
                        if (!string.IsNullOrEmpty(attrName))
                        {
                            //convert to 2.0 name
                            if (attrName.Equals("Position"))
                            {
                                attrName = "GeoLocation";
                            }
                            else if (attrName.Equals("ZoneOfInfluence"))
                            {
                                attrName = "ElementGeometry";
                            }
                            SubscriberTriggerPropertyNames.Add(attrName);
                        }
                        subscriberReader.Read();
                    }
                    subscriberReader.Read();
                }
                subscriberReader.Read();
            }
            //Subscribers = new List<string>(SerializationService.DeserializeItemList<string>("Subscribers", typeof(string), reader));
            if (xmlDictionary.ContainsKey("Behaviors"))
            {
                var rawString = xmlDictionary["Behaviors"];
                var finalXML = "<Behaviors>" + rawString + "</Behaviors>";
                var stringReader = new StringReader(finalXML);
                var behaviorReader = XmlReader.Create(stringReader);
                var behaviorsList = DepictionXmlFileUtilities.DeserializeItemList<BehaviorSerializer>("Behaviors", typeof(BehaviorSerializer), behaviorReader);
                Behaviors.Clear();
                foreach (var behavior in behaviorsList)
                    Behaviors.Add(behavior.Name, behavior.Parameters.ToArray());
            }

            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("InteractionRule", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra for interaction rule");
                reader.Read();
            }
            reader.ReadEndElement();
            SetupElementAndTriggerDictionary();
        }

        public void WriteXml(XmlWriter writer)
        {
            //            writer.WriteStartElement("InteractionRule");
            //            writer.WriteElementString("Name", Name);
            //            writer.WriteElementString("Description", Description);
            //            writer.WriteElementString("Author", Author);
            //
            //            writer.WriteStartElement("Publisher");
            //            writer.WriteAttributeString("Type", Publisher);
            //
            //            if (publisherTriggerPropertyNames.Count != 0)
            //            {
            //                writer.WriteStartElement("PublisherTriggerProperties");
            //                foreach (var propName in publisherTriggerPropertyNames)
            //                {
            //                    writer.WriteStartElement("Property");
            //                    writer.WriteAttributeString("name", propName);
            //                    writer.WriteEndElement();
            //                }
            //                writer.WriteEndElement();
            //            }
            //            writer.WriteEndElement();
            //
            //            var conditionList = new List<ConditionSerializer>();
            //            foreach (var key in conditions.Keys)
            //                conditionList.Add(new ConditionSerializer(key, conditions[key]));
            //            SerializationService.SerializeItemList("Conditions", typeof(ConditionSerializer), conditionList, writer);
            //
            //            writer.WriteStartElement("Subscribers");
            //            foreach (var subscriber in Subscribers)
            //            {
            //                writer.WriteElementString("String", subscriber);
            //            }
            //            if (subscriberTriggerPropertyNames.Count != 0)
            //            {
            //                writer.WriteStartElement("SubscriberTriggerProperties");
            //                foreach (var propName in subscriberTriggerPropertyNames)
            //                {
            //                    writer.WriteStartElement("Property");
            //                    writer.WriteAttributeString("name", propName);
            //                    writer.WriteEndElement();
            //                }
            //                writer.WriteEndElement();
            //            }
            //            writer.WriteEndElement();
            //            //            SerializationService.SerializeItemList("Subscribers", typeof(string), Subscribers, writer);
            //            var behaviorList = new List<BehaviorSerializer>();
            //            foreach (var key in behaviors.Keys)
            //                behaviorList.Add(new BehaviorSerializer(key, behaviors[key]));
            //
            //            SerializationService.SerializeItemList("Behaviors", typeof(BehaviorSerializer), behaviorList, writer);
            //            writer.WriteEndElement();
        }

        #endregion

    }
}
