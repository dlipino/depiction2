﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities;
using Depiction2.Utilities;

namespace Depiction2.Core.Geo
{
    /// <summary>
    /// Domain Representation of Latitude and Longitude
    /// </summary>
    [TypeConverter("Depiction.API.ValueTypeConverters.LatitudeLongitudeTypeConverter")]//As of 2.0 not sure how this really deals with things
    public class LatitudeLongitudeDepiction : LatitudeLongitudeDepictionBase, IXmlSerializable
    {

        #region Constructor
        /// <summary>
        /// Parameterless construction for XML serialization. Creates a Lat/long with invalid values
        /// </summary>
        public LatitudeLongitudeDepiction(){}

        /// <summary>
        /// Initializes a new instance of the <see cref="LatitudeLongitudeDepiction"/> class.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public LatitudeLongitudeDepiction(double latitude, double longitude):this(latitude,longitude,true)
        {
//            string message = "";
//            if (LatLongParserUtility.ValidateLatLong(latitude, longitude, out message))
//            {
//                Latitude = latitude;
//                Longitude = longitude;
//            }
//            else
//            {
//                Latitude = INVALID_VALUE;
//                Longitude = INVALID_VALUE;
//            }
        }
        public LatitudeLongitudeDepiction(double latitude, double longitude,bool boundsCheck)
        {
            string message;
            Latitude = latitude;
            Longitude = longitude;
            if (!LatLongParserUtility.ValidateLatLong(latitude, longitude, out message) && boundsCheck)
            {
                Latitude = LatLongParserUtility.INVALID_VALUE;
                Longitude = LatLongParserUtility.INVALID_VALUE;
            }
        }
        public LatitudeLongitudeDepiction(string latLongCombo)
        {
            SetLatLongFromString(latLongCombo);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="LatitudeLongitudeDepiction"/> class.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        public LatitudeLongitudeDepiction(string latitude, string longitude)
        {
            var combined = latitude + " " + longitude;
            SetLatLongFromString(combined);
        }
        protected void SetLatLongFromString(string latLongString)
        {
            double lat;
            double lon;
            string message;
            var goodLatLong = LatLongParserUtility.ParseForLatLong(latLongString, out lat, out lon,
                                                              out message);
            if (goodLatLong == true)
            {
                if (LatLongParserUtility.ValidateLatLong(lat, lon, out message))
                {
                    Latitude = lat;
                    Longitude = lon;
                }
                else
                {
                    Latitude = LatLongParserUtility.INVALID_VALUE;
                    Longitude = LatLongParserUtility.INVALID_VALUE;
                }
            }
            else
            {
                Latitude = LatLongParserUtility.INVALID_VALUE;
                Longitude = LatLongParserUtility.INVALID_VALUE;

            }
        }
        #endregion

        #region Methods
         
        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns>The result of the operator.</returns>
        public static LatitudeLongitudeDepiction operator -(LatitudeLongitudeDepiction a, IDepictionLatitudeLongitude b)
        {
            return new LatitudeLongitudeDepiction(a.Latitude - b.Latitude, a.Longitude - b.Longitude);
        }
        public static LatitudeLongitudeDepiction operator -(IDepictionLatitudeLongitude a, LatitudeLongitudeDepiction b)
        {
            return new LatitudeLongitudeDepiction(a.Latitude - b.Latitude, a.Longitude - b.Longitude);
        }
        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns>The result of the operator.</returns>
        public static LatitudeLongitudeDepiction operator +(LatitudeLongitudeDepiction a, IDepictionLatitudeLongitude b)
        {
            return new LatitudeLongitudeDepiction(a.Latitude + b.Latitude, a.Longitude + b.Longitude);
        }
        public static LatitudeLongitudeDepiction operator +(LatitudeLongitudeDepiction a, LatitudeLongitudeDepiction b)
        {
            return new LatitudeLongitudeDepiction(a.Latitude + b.Latitude, a.Longitude + b.Longitude);
        }


        #region ToString
        private static string LatitudeLongitudeSeparatorString()
        {
            return "|";
//            switch (Settings.Default.LatitudeLongitudeSeparator)
//            {
//                case LatitudeLongitudeSeparator.CommaAndSpace:
//                    return ", ";
//                case LatitudeLongitudeSeparator.Comma:
//                    return ",";
//                case LatitudeLongitudeSeparator.Space:
//                    return " ";
//                default:
//                    throw new Exception("Unexpected value for LatitudeLongitudeSeparator");
//            }
        }


        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            var separator = LatitudeLongitudeSeparatorString();
            if (!IsValid) return string.Format("{0}{1}{2}", LatLongParserUtility.INVALID_VALUE, LatitudeLongitudeSeparatorString(), LatLongParserUtility.INVALID_VALUE);
//            switch (Settings.Default.LatitudeLongitudeFormat)
//            {
////                case LatitudeLongitudeFormat.UTM:
////                    return DoUTMString();
//                case LatitudeLongitudeFormat.ColonFractionalSeconds:
//                    return DoColonFractionalSeconds(separator);
//                case LatitudeLongitudeFormat.ColonIntegerSeconds:
//                    return DoColonIntegerSeconds(separator);
//                case LatitudeLongitudeFormat.Decimal:
//                    return DoDecimal(separator);
//                case LatitudeLongitudeFormat.DegreesFractionalMinutes:
//                    return DoDegreesFractionalMinutes(separator);
//                case LatitudeLongitudeFormat.DegreesMinutesSeconds:
//                    return DoDegreesMinutesSeconds(separator, "°");
//                case LatitudeLongitudeFormat.DegreesWithDCharMinutesSeconds:
//                    return DoDegreesMinutesSeconds(separator, "d");
//                case LatitudeLongitudeFormat.SignedDecimal:
//                    return DoSignedDecimal(separator);
//                case LatitudeLongitudeFormat.SignedDegreesFractionalMinutes:
//                    return DoSignedDegreesFractionalMinutes(separator);
//                default:
//                    throw new Exception("Unexpected value for LatitudeLongitudeFormat");
//            } 
            return "";
        }

        private string DoSignedDegreesFractionalMinutes(string separator)
        {
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(Latitude, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(Longitude, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3:00}° {4:00.00000}",
                                 latDeg, latMin, separator, longDeg, longMin);
        }

        private string DoDegreesMinutesSeconds(string separator, string degreeSign)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}{1}{2:00}'{3:00}\"{4}{5}{6:00}{7}{8:00}'{9:00}\"{10}",
                                 latDeg, degreeSign, latMin, latSec, NS, separator, longDeg, degreeSign, longMin, longSec, EW);
        }

        private string DoDegreesFractionalMinutes(string separator)
        {
            //40° 26.77170N, 79° 55.93172W
            int latDeg;
            double latMin;
            int longDeg;
            double longMin;
            ExtractDegreesMinutes(Latitude, out latDeg, out latMin, 5);
            ExtractDegreesMinutes(Longitude, out longDeg, out longMin, 5);
            return string.Format("{0:00}° {1:00.00000}{2}{3}{4:00}° {5:00.00000}{6}",
                                 Math.Abs(latDeg), latMin, NS, separator, Math.Abs(longDeg), longMin, EW);
        }
        private string DoColonIntegerSeconds(string separator)
        {
            int latDeg;
            int latMin;
            int latSec;
            int longDeg;
            int longMin;
            int longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec);
            return string.Format("{0:00}:{1:00}:{2:00}{3}{4}{5:00}:{6:00}:{7:00}{8}",
                                 latDeg, latMin, latSec, NS, separator, longDeg, longMin, longSec, EW);
        }
//        private string DoUTMString()
//        {
//            return this.ConvertToUTM().ToString();
//        }
        private string DoColonFractionalSeconds(string separator)
        {
            int latDeg;
            int latMin;
            double latSec;
            int longDeg;
            int longMin;
            double longSec;
            ExtractDegreesMinutesSeconds(Latitude, out latDeg, out latMin, out latSec, 3);
            ExtractDegreesMinutesSeconds(Longitude, out longDeg, out longMin, out longSec, 3);
            return string.Format("{0:00}:{1:00}:{2:00.000}{3}{4}{5:00}:{6:00}:{7:00.000}{8}",
                                 latDeg, latMin, latSec, NS, separator, longDeg, longMin, longSec, EW);
        }

        private string DoSignedDecimal(string separator)
        {
            return string.Format("{0:F5}{1}{2:F5}", Latitude, separator, Longitude);
        }

        private string DoDecimal(string separator)
        {
            var ns = (Latitude < 0) ? "S" : "N";
            var ew = (Longitude < 0) ? "W" : "E";
            return string.Format("{0:00.00000}{1}{2}{3:00.00000}{4}", Math.Abs(Latitude), ns, separator, Math.Abs(Longitude), ew);
        }

        private string EW
        {
            get { return (Longitude < 0) ? "W" : "E"; }
        }

        private string NS
        {
            get { return (Latitude < 0) ? "S" : "N"; }
        }

        /// <summary>
        /// Degrees will have same sign as value.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutes(double value, out int deg, out double min, int decimals)
        {
            deg = (int)Math.Truncate(value);
            var fraction = Math.Abs(value) - Math.Floor(Math.Abs(value));
            min = fraction * 60;
            if (Math.Round(min, decimals) >= 60)
            {
                min = 0;
                deg += 1 * Math.Sign(deg);
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out int sec)
        {
            double dSec;
            ExtractDegreesMinutesSeconds(value, out deg, out min, out dSec, 0);
            sec = (int)Math.Round(dSec);
            if (sec == 60)
            {
                min += 1;
                sec = 0;
            }
            if (min == 60)
            {
                deg += 1;
                min = 0;
            }
        }

        /// <summary>
        /// Degrees is always positive.
        /// Round the results to decimals number of digits.        /// </summary>
        /// <param name="value"></param>
        /// <param name="deg"></param>
        /// <param name="min"></param>
        /// <param name="sec"></param>
        /// <param name="decimals"></param>
        private static void ExtractDegreesMinutesSeconds(double value, out int deg, out int min, out double sec, int decimals)
        {
            deg = (int)Math.Floor(Math.Abs(value));
            double temp;
            if (deg != 0)
                temp = 60 * (Math.Abs(value) % deg);
            else
                temp = 60 * Math.Abs(value);
            min = (int)Math.Floor(temp);
            sec = (temp - min) * 60;
            if (Math.Round(sec, decimals) >= 60)
            {
                sec = 0;
                min += 1;
            }
            if (min >= 60)
            {
                min = 0;
                deg += 1;
            }
        }
        #endregion

//        public bool IsWithin(IMapCoordinateBounds boundingBox)
//        {
//            double top = boundingBox.TopLeft.Latitude;
//            double bottom = boundingBox.BottomRight.Latitude;
//            double right = boundingBox.BottomRight.Longitude;
//            double left = boundingBox.TopLeft.Longitude;
//            double y = Latitude;
//            double x = Longitude;
//            return (x <= right && x >= left && y <= top && y >= bottom);
//        }


        #endregion
        #region Deep cloning

        override public IDepictionLatitudeLongitude DeepClone()
        {
            return new LatitudeLongitudeDepiction(Latitude, Longitude);
        }
        #endregion


        #region serialization region
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (!reader.Name.Equals("LatitudeLongitude")) return;
            //Might have to use a converter, hopefully all lat long get saved in decimal format(us style)
            var latString = reader.GetAttribute("latitude");
            var longString = reader.GetAttribute("longitude");
            if (string.IsNullOrEmpty(latString) || string.IsNullOrEmpty(longString))
            {

            }
            else
            {
                Latitude = double.Parse(latString);
                Longitude = double.Parse(longString);
            }
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
//            writer.WriteStartElement("LatitudeLongitude");
//            var typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(GetType());
//            writer.WriteAttributeString("type", typeName);
//            writer.WriteAttributeString("latitude", Latitude.ToString("R",CultureInfo.InvariantCulture));
//            writer.WriteAttributeString("longitude", Longitude.ToString("R", CultureInfo.InvariantCulture));
//            writer.WriteEndElement();
        }
        #endregion
    }
}