﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction2.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities;
using Depiction2.Utilities;

namespace Depiction2.Core.Geo
{
//    [TypeConverter("Depiction.API.ValueTypeConverters.LatitudeLongitudeTypeConverter")]
//    public interface ILatitudeLongitude : IDepictionLatitudeLongitude,IDeepCloneable<ILatitudeLongitude>
//    {
//
//        bool IsValid { get; }
//        string ToXmlSaveString();
//        double this[Ordinate longaxis] { get; }
//        ILatitudeLongitude Subtract(ILatitudeLongitude summand);
//        ILatitudeLongitude Add(ILatitudeLongitude summand);
//    }
    [TypeConverter("Depiction.API.ValueTypeConverters.LatitudeLongitudeTypeConverter")]
    public class LatitudeLongitudeDepictionBase : IDepictionLatitudeLongitude, IDeepCloneable<IDepictionLatitudeLongitude>
    {
        #region Implementation of ILatitudeLongitude

        public double Latitude { get; protected set; }
        public double Longitude { get; protected set; }

        public bool IsValid { get { return (!Latitude.Equals(LatLongParserUtility.INVALID_VALUE) && !Longitude.Equals(LatLongParserUtility.INVALID_VALUE)); } }

        #endregion

        protected LatitudeLongitudeDepictionBase()
        {
            Latitude = LatLongParserUtility.INVALID_VALUE;
            Longitude = LatLongParserUtility.INVALID_VALUE;
        }
        public LatitudeLongitudeDepictionBase(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
        public string ToXmlSaveString()
        {
            return Latitude.ToString("R", CultureInfo.InvariantCulture) + "," +
                   Longitude.ToString("R", CultureInfo.InvariantCulture);
        }

        public double this[Ordinate longaxis]
        {
            get
            {
                if (longaxis.Equals(Ordinate.X))
                    return Longitude;
                if (longaxis.Equals(Ordinate.Y))
                    return Latitude;
                return double.NaN;
            }
        }

        public IDepictionLatitudeLongitude Subtract(IDepictionLatitudeLongitude summand)
        {
            return summand == null
                ? this
                : this.Add(new LatitudeLongitudeDepiction(-summand.Latitude, -summand.Longitude));
        }

        public IDepictionLatitudeLongitude Add(IDepictionLatitudeLongitude summand)
        {
            //if (self == null && summand == null)
            //    return null;
            if (summand == null)
                return this;
            //if (self == null)
            //    return summand;

            // for now we only care for 2D
            return new LatitudeLongitudeDepiction(Latitude + summand.Latitude, Longitude + summand.Longitude);
        }
        protected bool ToleranceEquals(double start, double other)
        {
            if (double.IsNaN(start) && double.IsNaN(other)) return true;
            if (double.IsNaN(start) && !double.IsNaN(other)) return false;
            if (!double.IsNaN(start) && double.IsNaN(other)) return false;
            double difference = Math.Abs(start * tolerance);
            if (Math.Abs(start - other) <= difference) return true;
            return false;
        }

        private const double tolerance = .0000001;//Can't remeber how many decimal places until tolerances is about 1 inch

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as LatitudeLongitudeDepictionBase;
            if (other == null) return false;
            if (!ToleranceEquals(Latitude, other.Latitude)) return false;
            if (!ToleranceEquals(Longitude, other.Longitude)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = Latitude.GetHashCode() >> 3;
            hashCode ^= Longitude.GetHashCode();
            return hashCode;
        }
        #region deepcloning, which isn'treally used?
        virtual public IDepictionLatitudeLongitude DeepClone()
        {
            return new LatitudeLongitudeDepictionBase(Latitude, Longitude);
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        #endregion

    }
}