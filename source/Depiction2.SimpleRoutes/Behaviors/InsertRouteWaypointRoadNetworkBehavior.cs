﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;

namespace Depiction2.SimpleRoutes.Behaviors
{
    [BehaviorMetadata("InsertRouteRoadNetworkWaypoint", "Insert a route waypoint on road network", "Set initial route waypoints on road network")]
    public class InsertRouteWaypointRoadNetworkBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Location", typeof(Point))
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var locObject = parameterBag["Location"];
            if (!(locObject is Point)) return new BehaviorResult();
            var point = (Point)locObject;
            var waypoints = subscriber.AssociatedElements.ToArray();
            var zoiCoords = subscriber.ElementGeometry.GeometryPoints.ToArray();
            var zoiCoordLocation = 0;
            var minDistance = double.PositiveInfinity;
            IElement previousWaypoint = null;
            //find the lines that come out of each way point
            for (int i = 0; i < (waypoints.Length - 1); i++)
            {
                var end = waypoints[i + 1].GeoLocation;
                for (; zoiCoordLocation < zoiCoords.Length; zoiCoordLocation++)
                {
                    var vect = point - zoiCoords[zoiCoordLocation];
                    var distance = Math.Abs(vect.Length);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        previousWaypoint = waypoints[i];
                    }
                    if (zoiCoords[zoiCoordLocation].Equals(end))
                    {
                        break;
                    }
                }
            }
            var waypointScaffold = DepictionAccess.TemplateLibrary.FindElementTemplate("Depiction.Plugin.RouteWaypoint");
            var associatedElement = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(waypointScaffold, new List<Point> { point });
            if (associatedElement != null)
            {
                DepictionAccess.DStory.AddElement(associatedElement, true);
                subscriber.AttachElementAfter(associatedElement, previousWaypoint);
            }
            //Not sure if this is needed, and i do know this is kind of hacky, but i want to trigger the adjustroute behavior
            return new BehaviorResult();
        }
    }
}