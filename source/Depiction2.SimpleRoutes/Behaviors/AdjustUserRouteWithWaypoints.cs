﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.SimpleRoutes.Behaviors
{
//    [Export(typeof(BaseBehavior))]
//    [Behavior("FindRoute", "Find a route", "Find a route")]

    [BehaviorMetadata("AdjustUserRoute", "Adjust user drawn route according to way points", "Adjust user drawn route way points")]
//    public class FindRouteBehavior : BaseBehavior//1.4 name
    public class AdjustUserRouteWithWaypoints : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("RoadNetwork", typeof (IElement))
                        {
                            ParameterName = "Road Network", 
                            ParameterDescription = "The road network to use when calculating the route"
                        }
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }


        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
           
            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                SetRouteToNodes(subscriber);
                return new BehaviorResult();
            }
           
            return new BehaviorResult();
        }
        internal static void SetRouteToNodes(IElement route)
        {
            if (route.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
            {
                var completeRouteCoords = new List<Point>();
                foreach (var node in route.AssociatedElements)
                {
                    var location = node.GeoLocation;
                    if(location != null) completeRouteCoords.Add((Point)location);
                }
                if(completeRouteCoords.Count >0)
                {
                    var geom = DepictionGeometryService.CreateGeometry(completeRouteCoords);
                    lock (route.ElementGeometry)
                    {
                        route.UpdatePrimaryPointAndGeometry(completeRouteCoords[0], geom);
                    }
                }else
                {
                    //Hack, not sure how to deal with all the waypoints getting removed
                    route.VisualState = ElementVisualSetting.Icon;
                }
            }
        }
    }
}