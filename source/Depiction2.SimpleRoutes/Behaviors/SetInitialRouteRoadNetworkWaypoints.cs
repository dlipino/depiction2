﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;

namespace Depiction2.SimpleRoutes.Behaviors
{
    [BehaviorMetadata("SetRouteRoadNetworkWaypoints", "Set initial route waypoints", "Set initial route waypoints")]
    public class SetInitialRouteRoadNetworkWaypoints : BaseBehavior
    {
        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return null; }
        }
        
        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            //            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteUserDrawn") ||
            //                (subscriber.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") ||
            //                subscriber.ElementType.Equals("Depiction.Plugin.DriveTimeRoute") ||
            //                subscriber.ElementType.Equals("Depiction.Plugin.RouteWaypoint")))
            //            {
            var zoi = subscriber.ElementGeometry;
            var zoiCoords = zoi.GeometryPoints.ToArray();

            var coordCount = zoiCoords.Length;
            if (coordCount <= 1) return new BehaviorResult();
            var waypointScaffold = DepictionAccess.TemplateLibrary.FindElementTemplate("Depiction.Plugin.RouteWaypoint");
            //                var startScaffold = DepictionAccess.ScaffoldLibrary.FindElementTemplate("start");
            //                var endScaffold = DepictionAccess.ScaffoldLibrary.FindElementTemplate("end");
            //                var associatedElemetns = new List<IElement>();
            var associatedElementList = new List<IElement>();
            for (int i = 0; i < coordCount; i++)
            {
                IElement associatedElement = null;
                var pointList = new List<Point> { zoiCoords[i] };
                if (i == 0)
                {
                    associatedElement = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(waypointScaffold, pointList);
                }
                else if (i == coordCount - 1)
                {
                    associatedElement = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(waypointScaffold, pointList);
                }
                else
                {
                    associatedElement = ElementAndElemTemplateService.CreateElementFromScaffoldAndPoints(waypointScaffold, pointList);
                }
                if (associatedElement != null)
                {
                    associatedElement.UpdatePrimaryPointAndGeometry(zoiCoords[i], null);
                    associatedElementList.Add(associatedElement);
                    DepictionAccess.DStory.AddElement(associatedElement, true);
                }
            }
            //            }

            subscriber.AttachElements(associatedElementList);
//            foreach(var assocElem in associatedElementList)
//            {
//                subscriber.AttachElement(assocElem);
//            }
//            DepictionAccess.DStory.AddElements(associatedElementList);
            return new BehaviorResult();
        }
    }
}