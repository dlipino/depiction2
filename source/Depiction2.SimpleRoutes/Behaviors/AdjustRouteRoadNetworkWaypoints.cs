﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Extensions;
using Depiction2.Core.StoryEntities;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;
using Depiction2.SimpleRoutes.Utilities;

namespace Depiction2.SimpleRoutes.Behaviors
{
    [BehaviorMetadata("AdjustRoadNetworkRoute", "Roadnetwork and waypoint route", "Adjust route according to waypoints and road network")]
    public class AdjustRouteRoadNetworkWaypoints : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("RoadNetwork", typeof (IElement))
                        {
                            ParameterName = "Road Network", 
                            ParameterDescription = "The road network to use when calculating the route"
                        }
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }


        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (!parameterBag.ContainsKey("RoadNetwork"))
            {
                return new BehaviorResult { SubscriberHasChanged = false };
            }
            var roadNetwork = parameterBag["RoadNetwork"] as IElement;
            //            var roadGraphProp = roadNetwork.GetDepictionProperty("RoadGraph");
            RoadGraph roadGraph = null;
            //            if(roadGraphProp == null)
            roadGraph = roadNetwork != null ? roadNetwork.GetDepictionProperty("RoadGraph").Value as RoadGraph : null;

            if (roadGraph == null)
            {
                SetRouteToNodes(subscriber);
                return new BehaviorResult();
            }
            if (roadGraph.Graph.EdgeCount == 0)
            {
                return new BehaviorResult();
            }
            //SetRouteToNodes(subscriber);
            FindNewRoute(roadGraph, subscriber);
            //TempHack for updating visible route length
            // subscriber.UpdateToolTip();//HAck
            return new BehaviorResult();
        }
        internal static void SetRouteToNodes(IElement route)
        {
            if (route.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork"))
            {
                var completeRouteCoords = new List<Point>();
                foreach (var node in route.AssociatedElements)
                {
                    var location = node.GeoLocation;
                    if (location != null) completeRouteCoords.Add((Point)location);
                }
                if (completeRouteCoords.Count > 0)
                {
                    var geom = DepictionGeometryService.CreateGeometry(completeRouteCoords);
                    lock (route.ElementGeometry)
                    {
                        route.UpdatePrimaryPointAndGeometry(completeRouteCoords[0], geom);
                    }
                }
                else
                {
                    //Hack, not sure how to deal with all the waypoints getting removed
                    route.VisualState = ElementVisualSetting.Icon;
                }
            }
        }
        private static void FindNewRoute(RoadGraph roadNetwork, IElement route)
        {
            if (roadNetwork == null) return;

            var roadNodes = new List<RoadNode>();//in roadnewtork coord system
            var elementWaypoints = route.AssociatedElements.ToArray();//depiction coord system
            if (elementWaypoints.Length <= 1) return;
            bool attachToRoadNetworkNodes = true;
            IElementProperty snapProp;
            if ((snapProp = route.GetDepictionProperty("SnapToRoadNetworkNodes")) != null)
            {
                if (snapProp.Value is bool)
                {
                    attachToRoadNetworkNodes = (bool)snapProp.Value;
                }
            }
            route.DisableAssociatedElementListener = true;
            for (int i = 0; i < elementWaypoints.Length; i++)
            {
                //expects input from roadnetwork coord system
                var nearestNode = roadNetwork.FindNearestNode((Point)elementWaypoints[i].GeoLocation);
                if (attachToRoadNetworkNodes)
                {
                    //comes out roadnetwork coord system, need to set to depiction coord system
                    //Dont want to trigger another route change
                    Debug.WriteLine(string.Format("Updating associate {0} location", i));
                    elementWaypoints[i].UpdatePrimaryPointWithoutAssociatedEvents(nearestNode.Vertex);
                }
                roadNodes.Add(nearestNode);
            }


            lock (roadNetwork)
            {
                route.UpdatePropertyValue("Blocked", false);
                bool hasFreeformSegments;
                var completeRouteCoords = new List<Point>();
                var completeRouteSegments = new List<IList<RoadSegment>>();
                var completeRouteTimes = new List<double>();
                //var completeDirections = "";
                //var completeRouteDistance = 0d;
                var completeRouteTime = 0d;
                for (int i = 0; i < elementWaypoints.Length - 1; i++)
                {
                    var startNode = roadNodes[i];//roadnetwork coord
                    var endNode = roadNodes[i + 1];//roadnetwork coord
                    if (!startNode.Equals(endNode))
                    {
                        double? estimatedTime;
                        //In the roadnetwork coordinate system
                        var partialRoute = RouteFinderService.FindRouteOnGraph(roadNetwork, new[] { startNode, endNode }, route.ElementType,
                                                                             out hasFreeformSegments, out estimatedTime);
                        if (partialRoute != null) completeRouteSegments.Add(partialRoute);
                        if (estimatedTime != null)
                        {
                            completeRouteTime += (double)estimatedTime;
                            completeRouteTimes.Add((double)estimatedTime);
                        }
                        var coords = new List<Point>();//roadnetwork coords
                        //double partialDistance = 0;
                        if (partialRoute != null && partialRoute.Count >= 1)
                        {
                            bool firstVertex = true;
                            foreach (var edge in partialRoute)
                            {
                                if (firstVertex)
                                {
                                    coords.Add(edge.Source.Vertex);//new Point(edge.Source.Vertex.Longitude, edge.Source.Vertex.Latitude));
                                    firstVertex = false;
                                }
                                coords.Add((edge.Target.Vertex));//new Point(edge.Target.Vertex.Longitude, edge.Target.Vertex.Latitude));
                            }
                            if (!attachToRoadNetworkNodes)
                            {
                                var startPosition = elementWaypoints[i].GeoLocation;//depiction coords
                                var endPosition = elementWaypoints[i + 1].GeoLocation;//depiction coods

                                coords.Insert(0, (Point)startPosition);
                                coords.Add((Point)endPosition);
                            }
                        }
                        else
                        {//Every so often routes will just die
                            var startPosition = elementWaypoints[i].GeoLocation;//depiction coords
                            var endPosition = elementWaypoints[i + 1].GeoLocation;//depiction coords

                            coords.Insert(0, (Point)startPosition);
                            coords.Add((Point)endPosition);
                            //partialDistance += startPosition.DistanceTo(endPosition, measureSystem,measureScale);
                        }
                        completeRouteCoords.AddRange(coords);

                        //if (partialRoute != null)
                        //{
                        //    completeDirections += directionsService.GetRawDirections(partialRoute, out partialDistance);
                        //}
                        //completeRouteDistance += partialDistance;
                    }
                }
                if (completeRouteCoords.Count == 0 && route.GeoLocation != null) completeRouteCoords.Add((Point)route.GeoLocation);

                var geom = DepictionGeometryService.CreateGeometry(completeRouteCoords);//in 

                var directionsService = new RouteDirectionsService();
                string completeDirections = directionsService.GetRouteDirections(completeRouteSegments, completeRouteTimes);
                //                IElementProperty directionProperty;
                //                if ((directionProperty = route.GetProperty(directionPropertyName)) != null)
                //                {
                //                    directionProperty.SetValue(completeDirections);
                //                }
                //                else
                //                {
                //                    var directionsProperty = new ElementProperty("Directions", "Directions", completeDirections);
                //                    //                    directionsProperty.IsHoverText = false;
                //                    route.AddUserProperty(directionsProperty);
                //                    //                    route.UseEnhancedPermaText = false;
                //                }
                route.DisableAssociatedElementListener = false;
                lock (route.ElementGeometry)
                {
                    Debug.WriteLine("Updateing route geometry");
                    route.UpdatePrimaryPointAndGeometry(completeRouteCoords[0], geom);
                }
            }
        }
    }
}