using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;

namespace Depiction2.SimpleRoutes.Behaviors
{
    [BehaviorMetadata("DestroyRoadGraph", "Destroy a road network", "Destroy a road network",
        Description = "Makes all road segments within the Destroyer elements zone of influence unusable")]
    public class DestroyRoadNetworkZoneBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Destroyer", typeof(IElement))
                        {
                            ParameterDescription = "Specify which element destroys the road network (this is usually the publisher)", 
                            ParameterName = "Choose the Publisher"
                        }
                };

        public override DepictionParameterInfo[] DepictionParameters
        {
            get { return parameters; }
        }


        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var triggerElement = parameterBag["Destroyer"] as IElement;

            var roadGraphProp = subscriber.GetDepictionProperty("RoadGraph");
            if (roadGraphProp == null)
            {
                throw new Exception(string.Format("The '{0}' behavior expects its Subscriber to be a Road Network", "DestroyZone"));
            }
            var roadGraph = roadGraphProp.Value as RoadGraph;
            if (roadGraph == null) return new BehaviorResult();
            bool notifyChange = DisableIntersectingEdges(triggerElement, roadGraph);
            subscriber.TriggerPropertyChange("RoadGraph");
           
            return new BehaviorResult { SubscriberHasChanged = true };
        }


        private static bool DisableIntersectingEdges(IElement triggerElement, RoadGraph roadGraph)
        {
            if (triggerElement.ElementGeometry == null) return false;
            var affectedEdges = GetIntersectingEdges(triggerElement.ElementGeometry, roadGraph);
            foreach (var edge in affectedEdges)
            {
                roadGraph.DisableEdge(edge);
            }
            return affectedEdges.Count > 0;
        }

        private static IList<RoadSegment> GetIntersectingEdges(IDepictionGeometry zoneOfInfluence, RoadGraph roadGraph)
        {
            //            var precisionModel = new PrecisionModel(); //FLOATING POINT precision
            //            var geometryFactory = new GeometryFactory(precisionModel, 32767);
            var edgesAffected = new List<RoadSegment>();
            var graph = roadGraph.Graph;

            foreach (var edge in graph.Edges)
            {
                var coordinates = new List<Point>();
                coordinates.Add(edge.Source.Vertex);
                coordinates.Add(edge.Target.Vertex);

                var objLineString = DepictionGeometryService.CreateGeometry(coordinates);
                lock (zoneOfInfluence)
                {
                    if (objLineString.Intersects(zoneOfInfluence))
                    {
                        edgesAffected.Add(edge);
                    }
                }
            }

            return edgesAffected;
        }
    }
}