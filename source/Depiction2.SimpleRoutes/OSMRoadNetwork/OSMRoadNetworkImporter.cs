﻿using System;
using System.Collections.Generic;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork
{
    [RegionSourceMetadata(Name = "OSMRoadNetworkLoader", DisplayName = "OSM Road Network Creator", Author = "Depiction Inc.",
       ShortName = "OSM RoadNetwork", GenericName = "OpenStreetMapRoadNetworkImporter")]
    public class OSMRoadNetworkImporter : IRegionSourceExtension
    {
        public void Dispose()
        {
            
        }
        
        public void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo)
        {
            var service = new OSMWebImportService();
            service.TargetRegion = regionOfInterest.RegionRect;
            var dict = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            dict.Add("Region", regionOfInterest);
            var parameters = sourceInfo.Parameters;
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    dict.Add(item.Key, item.Value);
                }
            }
            var name = string.Format("RoadNetwork from: {0}", service.HomeUrl);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(dict);
        }

        public void SimpleElementImport(ICartRect region, Dictionary<string, string> inParameters)
        {
            var service = new OSMWebImportService();
            service.TargetRegion = region;
            var name = string.Format("RoadNetwork from: {0}", service.HomeUrl);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(service);
            service.UpdateStatusReport(name);
            service.StartBackgroundService(null);
        }
    }
}