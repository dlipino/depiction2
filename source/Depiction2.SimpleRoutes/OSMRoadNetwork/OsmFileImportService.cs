﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Depiction2.API.Tools;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;

[assembly: InternalsVisibleTo("Depiction2.SimpleRoutes.UnitTests")]
namespace Depiction2.SimpleRoutes.OSMRoadNetwork
{
    public class OsmFileImportService : BaseDepictionBackgroundThreadOperation
    {
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Open Street Map Road network");
            return true; ;
        }

        protected override object ServiceAction(object args)
        {
            var argDictionary = args as Dictionary<string, object>;
            var fileList = new List<string>();
            ICartRect areaRestriction = null;
            var cacheLocation = string.Empty;
            if (argDictionary == null) return null;

            if (!argDictionary.ContainsKey("Files")) return null;
            fileList = argDictionary["Files"] as List<string>;
            if (argDictionary.ContainsKey("Area"))
            {
                areaRestriction = argDictionary["Area"] as ICartRect;
            }
            if (argDictionary.ContainsKey("CacheLocation"))
            {
                cacheLocation = argDictionary["CacheLocation"] as string;
            }
            return CreateRoadSegmentsFromFileList(fileList, areaRestriction, cacheLocation);
            
        }

        protected override void ServiceComplete(object args)
        {
           
        }


        public IEnumerable<RoadSegment> CreateRoadSegmentsFromFileList(List<string> roadNetworkFiles, ICartRect areaRestriction,string cached)
        {
            var nodes = new Dictionary<long, OSMNode>();
            var ways = new Dictionary<long, OSMWay>();
            TileCacheService cacheService = null;
            if(!string.IsNullOrEmpty(cached))
            {
                cacheService = new TileCacheService(cached);
            }

            foreach (var rnFile in roadNetworkFiles)
            {
                var fullName = rnFile;
                if (!File.Exists(fullName))
                {
                    if(cacheService!=null)
                    {
                        fullName = cacheService.GetCacheFullStoragePath(rnFile);

                    }
                    if (!File.Exists(fullName)) continue;
                }
                OSMNode[] nodesInFile;
                OSMWay[] waysInFile;
                OSMGatherUtilities.GetNodesAndWaysFromFile(fullName, out nodesInFile, out waysInFile, this);
                OSMWebImportService.CombineNodesAndWays(nodesInFile, waysInFile, nodes, ways, ServiceStopRequested);
            }

            if (ServiceStopRequested) return null;
            var roadSegments = OSMGatherUtilities.GetTaggedEdgeListFromOSMData(nodes, ways, areaRestriction, this);
            var validHighwayValues = new[] { "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "tertiary", "unclassified", "road", "residential", "living_street", "service", "track" };
            double totalWays = ways.Count(w => w.Value.Tags.ContainsKey("highway") && validHighwayValues.Contains(w.Value.Tags["highway"]));

            var message = string.Format("Generating RoadNetwork: {0} roads", totalWays);
            UpdateStatusReport(message);
            return roadSegments;;
        }
    }
}