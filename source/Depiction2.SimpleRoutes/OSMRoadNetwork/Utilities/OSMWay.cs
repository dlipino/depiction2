using System;
using System.Collections.Generic;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities
{
    public class OSMWay
    {
        public long WayID { get; set; }
        public DateTime TimeStamp { get; set; }
        public string User { get; set; }
        public long[] NodeIDs { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}