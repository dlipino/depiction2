using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using QuickGraph;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities
{
    public class RoadSegment: IEdge<RoadNode>
    {
        public RoadNode Source { get; private set; }
        public RoadNode Target { get; private set; }

        public string SegmentIdST { get { return Source.NodeID + "|" + Target.NodeID; } }

        public string Name { get; set; }
        public string MaxSpeed { get; set; }
        public string Highway { get; set; }
//        public bool Disabled { get; set; }
        #region constructor
//        internal RoadSegment()
//        {
//            
//        }
        public RoadSegment(RoadNode source, RoadNode target)
        {
            Source = source;
            Target = target;
        }
        #endregion

        public override bool Equals(object obj)
        {
            var other = obj as RoadSegment;
            if (other == null) return false;

            if (Source != other.Source)
                return false;
            if (Target != other.Target)
                return false;

            return true;
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            //if (!reader.Name.Equals("way")) return; //Still not sure how to make sure things to go bad, will suck when i figure it out and have to redo everything
            Name = reader.GetAttribute("name");
            MaxSpeed = reader.GetAttribute("maxspeed");
            Highway = reader.GetAttribute("highway");
            reader.ReadStartElement();
            Source = new RoadNode();
            Source.ReadXml(reader);
            Target = new RoadNode();
            Target.ReadXml(reader);
//            Source = SerializationService.DeserializeObject(typeof(RoadNode), reader) as RoadNode;
//            Target = SerializationService.DeserializeObject(typeof(RoadNode), reader) as RoadNode;
            reader.ReadEndElement();
        }
//
//        public void WriteXml(XmlWriter writer)
//        {
////            writer.WriteStartElement("RoadSegment");
////            writer.WriteAttributeString("name", Name);
////            writer.WriteAttributeString("maxspeed", MaxSpeed);
////            writer.WriteAttributeString("highway", Highway);
////            SerializationService.SerializeObject(Source, Source.GetType(), writer);
////            SerializationService.SerializeObject(Target, Target.GetType(), writer);
////            writer.WriteEndElement();
//        }

        
    }
}