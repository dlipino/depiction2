using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities
{
    public class OSMGatherUtilities
    {
        static public List<RoadSegment> GetTaggedEdgeListFromOSMData(Dictionary<long, OSMNode> nodes, Dictionary<long, OSMWay> ways, ICartRect area, BaseDepictionBackgroundThreadOperation threadConnection)
        {
            var roadSegments = new List<RoadSegment>();

            var validHighwayValues = new[] { "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "tertiary", "unclassified", "road", "residential", "living_street", "service", "track" };

            double totalWays = ways.Count(w => w.Value.Tags.ContainsKey("highway") && validHighwayValues.Contains(w.Value.Tags["highway"]));
            var message = string.Format("Processed {0}  roads", totalWays);
            if (threadConnection != null)
            {
                threadConnection.UpdateStatusReport(message);
            }
            foreach (var way in Enumerable.Where(Enumerable.Select(ways, w => w.Value), w => w.Tags.ContainsKey("highway") && validHighwayValues.Contains(w.Tags["highway"])))
            {
                if (threadConnection != null && threadConnection.ServiceStopRequested) return null;

                for (int i = 0; i < (way.NodeIDs.Length - 1); i++)
                {
                    var v1 = new Point(nodes[way.NodeIDs[i]].Position.X,
                                                   nodes[way.NodeIDs[i]].Position.Y);
                    var v2 = new Point(nodes[way.NodeIDs[i + 1]].Position.X,
                                                   nodes[way.NodeIDs[i + 1]].Position.Y);
                    if(area != null)
                    {
                        if (!area.ContainsPoint(v1) && !area.ContainsPoint(v2))
                            continue;
                    }
                  
                    var wayName = way.Tags.ContainsKey("name") ? way.Tags["name"] : "";
                    var wayHighway = way.Tags.ContainsKey("highway") ? way.Tags["highway"] : null;
                    var wayMaxSpeed = way.Tags.ContainsKey("maxspeed") ? way.Tags["maxspeed"] : null;
                    var newsegment = new RoadSegment(new RoadNode(v1) { NodeID = way.NodeIDs[i] },
                                                                      new RoadNode(v2) { NodeID = way.NodeIDs[i + 1] });
                    newsegment.Name = wayName;
                    newsegment.Highway = wayHighway;
                    newsegment.MaxSpeed = wayMaxSpeed;
                    roadSegments.Add(newsegment);
                    if (!(way.Tags.ContainsKey("oneway") && way.Tags["oneway"].Equals("yes")))
                    {
                        newsegment = new RoadSegment(new RoadNode(v2) { NodeID = way.NodeIDs[i + 1] },
                                                                      new RoadNode(v1) { NodeID = way.NodeIDs[i] });
                        newsegment.Name = wayName;
                        newsegment.Highway = wayHighway;
                        newsegment.MaxSpeed = wayMaxSpeed;
                        roadSegments.Add(newsegment);
                    }
                }
            }
            return roadSegments;
        }

        static public void GetNodesAndWaysFromFile(string xmlPath, out OSMNode[] nodesInFile, out OSMWay[] waysInFile, BaseDepictionBackgroundThreadOperation threadConnection)
        {
            var nodes = new List<OSMNode>();
            var ways = new List<OSMWay>();

            using (var reader = new XmlTextReader(xmlPath) { XmlResolver = null })
            {
                while (reader.Read())
                {
                    if (threadConnection != null && threadConnection.ServiceStopRequested)
                    {
                        nodesInFile = new OSMNode[0];
                        waysInFile = new OSMWay[0];
                        reader.Close();
                        return;
                    }
                    if (reader.NodeType != XmlNodeType.Element) continue;

                    if (reader.Name == "node")
                    {
                        OSMNode nodeExtracted = ExtractNode(reader);
                        nodes.Add(nodeExtracted);
                    }
                    if (reader.Name == "way")
                    {
                        OSMWay wayExtracted = ExtractWay(reader);
                        ways.Add(wayExtracted);
                    }
                }
            }
            nodesInFile = nodes.ToArray();
            waysInFile = ways.ToArray();
        }
        private static OSMNode ExtractNode(XmlTextReader reader)
        {
            var nodeId = reader.GetAttribute("id");
            var latitude = reader.GetAttribute("lat");
            var longitude = reader.GetAttribute("lon");
            var tags = new Dictionary<string, string>();
            //var timestamp = reader.GetAttribute("timestamp");
            //var user = reader.GetAttribute("user");

            string name = null;
            if (!reader.IsEmptyElement)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "node") break;
                    if (reader.Name == "tag")
                    {
                        var k = reader.GetAttribute("k");
                        var v = reader.GetAttribute("v");
                        tags.Add(k, v);
                    }
                }
            }
//            return new OSMNode { LatLong = new LatitudeLongitude(latitude, longitude), NodeID = Convert.ToInt64(nodeId), Tags = tags };
            double x, y;
            double.TryParse(longitude, out x);
            double.TryParse(latitude, out y);
            return new OSMNode { Position = new Point(x, y), NodeID = Convert.ToInt64(nodeId), Tags = tags };
        }

        private static OSMWay ExtractWay(XmlTextReader reader)
        {
            var wayId = reader.GetAttribute("id");
            var timestamp = reader.GetAttribute("timestamp");
            var user = reader.GetAttribute("user");
            var nodeIds = new List<long>();
            var tags = new Dictionary<string, string>();

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    return new OSMWay { NodeIDs = nodeIds.ToArray(), Tags = tags, TimeStamp = Convert.ToDateTime(timestamp), User = user, WayID = Convert.ToInt64(wayId) };
                }

                if (reader.NodeType != XmlNodeType.Element) continue;

                if (reader.Name == "nd")
                    nodeIds.Add(Convert.ToInt64(reader.GetAttribute("ref")));

                if (reader.Name == "tag")
                {
                    var key = reader.GetAttribute("k");
                    var value = reader.GetAttribute("v");
                    if (!tags.ContainsKey(key))
                        tags.Add(key, value);
                    else
                    {
                        if (!tags[key].Equals(value))
                            throw new Exception(
                                string.Format("Could not add ({0}, {1}), since ({0},{2}) already exists in tags.",
                                              key, value, tags[key]));
                    }
                }
            }

            return new OSMWay { NodeIDs = nodeIds.ToArray(), Tags = tags, TimeStamp = Convert.ToDateTime(timestamp), User = user, WayID = Convert.ToInt64(wayId) };
        }

    }
}