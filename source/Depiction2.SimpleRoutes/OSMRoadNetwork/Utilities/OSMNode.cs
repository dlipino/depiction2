using System.Collections.Generic;
using System.Windows;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities
{
    public class OSMNode
    {
        public long NodeID { get; set; }
        public Point Position { get; set; }
        public Dictionary<string, string> Tags { get; set; }
        //public string User { get; set; }
        //public DateTime TimeStamp { get; set; }
    }
}