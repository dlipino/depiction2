﻿using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using Depiction2.Base.Geo;

namespace Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities
{
    [DataContract]
    public class RoadNode
    {
        [DataMember]
        public Point Vertex { get; set; }
        [DataMember]
        public long NodeID { get; set; }

        #region constructors
        internal RoadNode()
        {
            
        }
        public RoadNode(IDepictionLatitudeLongitude v1)
        {
            Vertex = v1 != null ? new Point(v1.Longitude, v1.Latitude) : new Point(Double.NaN, double.NaN);
            NodeID = 0;
        }
        public RoadNode(Point v1)
        {
            Vertex = v1;
            NodeID = 0;
        }
        #endregion


        #region Equality methods

        public override bool Equals(object obj)
        {
            var other = obj as RoadNode;
            if (other == null) return false;
            return Vertex.Equals(other.Vertex);
        }

        public static bool operator ==(RoadNode a, RoadNode b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }
            return a.Vertex.Equals(b.Vertex);
        }
        public static bool operator !=(RoadNode a, RoadNode b)
        {
            return !(a == b);
        }
        public override int GetHashCode()
        {
            var p = Vertex;
            return p.GetHashCode();
            //            int hashCode = p.Latitude.GetHashCode() >> 3;
            //            hashCode ^= Vertex.Longitude.GetHashCode();
            //            return hashCode;
        }
        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var locString = reader.GetAttribute("Location");
            //comes in latitude, longitude
            if (string.IsNullOrEmpty(locString))
            {
                Vertex = new Point(double.NaN,double.NaN);// LatLong = new LatitudeLongitudeSimple(double.NaN, double.NaN);
            }
            else
            {
                var p = Point.Parse(locString);
                Vertex = new Point(p.Y,p.X);
                //VertexLatLong = new LatitudeLongitudeSimple(p.X, p.Y);
            }

            //            Vertex = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(reader.GetAttribute("Location"));

            var nodeString = reader.GetAttribute("NodeID");
            if (string.IsNullOrEmpty(nodeString))
            {
                NodeID = -1;
            }
            else
            {
                NodeID = long.Parse(nodeString);
            }
            reader.ReadStartElement("RoadNode");
        }
//
//        public void WriteXml(XmlWriter writer)
//        {
//            //            writer.WriteStartElement("RoadNode");
//            //            writer.WriteAttributeString("Location", Vertex.ToXmlSaveString());
//            //            writer.WriteAttributeString("NodeID",NodeID.ToString());
//            //            writer.WriteEndElement();
//        }
    }
}
