using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.API.Tools;
using Depiction2.Base.Abstract;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;

[assembly: InternalsVisibleTo("Depiction2.SimpleRoutes.UnitTests")]
namespace Depiction2.SimpleRoutes.OSMRoadNetwork
{
    public class OSMWebImportService : BaseDepictionBackgroundThreadOperation
    {
        private const int maxNumberOfThreads = 12;
        private const double cachedTileDiameterInDegrees = .015;//was .03, but there are issues with the amount of info which stops the transfer in certain areas
        //TODO send this as a parameter from webservice
        private string older14OsmURL = "http://open.mapquestapi.com/xapi/api/0.6/way[highway=motorway|motorway_link|trunk|trunk_link|primary|primary_link|secondary|tertiary|unclassified|road|residential|living_street|service|track][bbox={0},{1},{2},{3}]";
        internal const string current14OsmUrl = "http://overpass-api.de/api/xapi?way[highway=motorway|motorway_link|trunk|trunk_link|primary|primary_link|secondary|tertiary|unclassified|road|residential|living_street|service|track][bbox={0},{1},{2},{3}]";

        readonly Dictionary<long, OSMNode> nodes = new Dictionary<long, OSMNode>();
        readonly Dictionary<long, OSMWay> ways = new Dictionary<long, OSMWay>();
        private int numberOfWorkers;
        private readonly object numberOfWorkersLock = new object();
        private readonly object tilesRetrievedLock = new object();
        private double totalTiles;
        private double tilesRetrieved;
        private List<string> tileFileNames; 
        private TileCacheService cacheService;
        private string cacheDir = "OSMRoadNetwork";

        private IDepictionRegion sourceOwner = null;
        //        public bool ReplaceCachedInformation { get { return Settings.Default.ReplaceCachedFiles; } }

        #region properties
        public string HomeUrl { get { return "http://overpass-api.de/"; } }
        public ICartRect TargetRegion { get; set; }
        public string RawURL { get; set; }

        #endregion

        #region Constructor

        public OSMWebImportService()
        {
            cacheService = new TileCacheService(cacheDir);
            RawURL = current14OsmUrl;
        }

        #endregion

        #region Overrides of BaseDepictionBackgroundThreadOperation

        override protected bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Open Street Map Road network");
            if (args == null) return true;
            if (args.ContainsKey("Region"))
            {
                sourceOwner = args["Region"] as IDepictionRegion;
            }
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var argDictionary = args as Dictionary<string, object>;
            var area = TargetRegion;
            if (argDictionary != null)
            {
//                if (!argDictionary.ContainsKey("Area")) return null;
//                area = argDictionary["Area"] as ICartRect;//dealt with via a property

                if (argDictionary.ContainsKey("url"))
                {
                    RawURL = argDictionary["url"].ToString();
                }
            }

            var roadGraph = CreateRoadGraphForArea(area, RawURL);
            var roadGeometryWkt = roadGraph.ConvertToMultilinewkt();

            IElement roadNetworkElement = null;
            if (DepictionAccess.TemplateLibrary != null)
            {
                var roadNetworkScaffold = DepictionAccess.TemplateLibrary.FindElementTemplate("Depiction.Plugin.RoadNetwork");
                var roadGeometry = DepictionGeometryService.CreateGeomtryFromWkt(roadGeometryWkt);
                if (roadNetworkScaffold != null)
                {
                    roadNetworkElement = ElementAndElemTemplateService.CreateElementFromScaffoldAndGeometry(roadNetworkScaffold, roadGeometry);
                }
            }

            if (roadNetworkElement == null)
            {
                var holderElement = new DepictionElement("Generic.RoadNetwork");
                holderElement.SetGeometryFromWtkString(roadGeometryWkt);
                roadNetworkElement = holderElement;
            }
            roadNetworkElement.AddUserProperty(new ElementProperty("RoadGraph","Road graph",roadGraph));

            var story = DepictionAccess.DStory;
            if(story != null)
            {
                story.AddElement(roadNetworkElement,true);
            }
            if (sourceOwner != null)
            {
                sourceOwner.AddManagedElementId(roadNetworkElement.ElementId);
            }
            return roadNetworkElement;

        }
        protected override void ServiceComplete(object args)
        {
            //            var argElement = args as IDepictionElement;
            //            if (argElement != null)
            //            {
            //                UpdateStatusReport("Road network downloaded, adding to depiction");
            //                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new[] { argElement }, false);
            //            }
        }
        #endregion

        #region protecteed methdos

        internal int[] BreakupAreaIntoTiles(ICartRect area)
        {

            var firstCol = getColumn(area.Left);
            var lastCol = getColumn(area.Right);
            var firstRow = getRow(area.Top);
            var lastRow = getRow(area.Bottom);

            var tileCount = (lastCol - firstCol + 1) * (lastRow - firstRow + 1);

            var results = new[] { firstCol, lastCol, firstRow, lastRow, tileCount };
            return results;
        }

        protected RoadGraph CreateRoadGraphForArea(ICartRect area, string newOsmUrl)
        {
            if (!string.IsNullOrEmpty(newOsmUrl))
            {
                RawURL = newOsmUrl;
            }
            var gridResults = BreakupAreaIntoTiles(area);
            var firstCol = gridResults[0];
            var lastCol = gridResults[1];
            var firstRow = gridResults[2];
            var lastRow = gridResults[3];

            totalTiles = gridResults[4];
            tileFileNames = new List<string>();
            for (int col = firstCol; col <= lastCol; col++)
            {
                for (int row = firstRow; row <= lastRow; row++)
                {
                    while (numberOfWorkers >= maxNumberOfThreads)
                    {
                        Thread.Sleep(100);
                    }
                    if (!ServiceStopRequested)
                    {
                        BackgroundWorker osmRoadNetworkTileGetter = new BackgroundWorker();
                        osmRoadNetworkTileGetter.DoWork += osmRoadNetworkTileGetter_DoWork;
                        osmRoadNetworkTileGetter.RunWorkerCompleted += osmRoadNetworkTileGetter_RunWorkerCompleted;
                        Thread.Sleep(100);
                        osmRoadNetworkTileGetter.RunWorkerAsync(new Point(col, row));

                        lock (numberOfWorkersLock)
                        {
                            numberOfWorkers++;
                        }
                    }
                }
            }

            while (numberOfWorkers > 0) Thread.Sleep(100);
            if (ServiceStopRequested) return null;
            var roadSegments = OSMGatherUtilities.GetTaggedEdgeListFromOSMData(nodes, ways, area, this);
            var validHighwayValues = new[] { "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "tertiary", "unclassified", "road", "residential", "living_street", "service", "track" };
            double totalWays = ways.Count(w => w.Value.Tags.ContainsKey("highway") && validHighwayValues.Contains(w.Value.Tags["highway"]));

            var message = string.Format("Generating RoadNetwork: {0} roads", totalWays);
            UpdateStatusReport(message);
            var completedRoadGraph = BuildRoadGraph(roadSegments);
            completedRoadGraph.DataCacheName = cacheDir;
            completedRoadGraph.DataSourceFiles = tileFileNames;
            return completedRoadGraph;
        }

        #endregion
        #region tile thread methods

        void osmRoadNetworkTileGetter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lock (tilesRetrievedLock)
            {
                tilesRetrieved++;
                tileFileNames.Add(e.Result.ToString());
                var message = string.Format("Retrieved {0} of {1} tiles", tilesRetrieved, totalTiles);
                UpdateStatusReport(message);
            }
            lock (numberOfWorkersLock)
            {
                numberOfWorkers--;
            }
        }

        void osmRoadNetworkTileGetter_DoWork(object sender, DoWorkEventArgs e)
        {
            Point p = (Point)e.Argument;

            int col = (int)p.X;
            int row = (int)p.Y;
            if (ServiceStopRequested) return;
            var fileName = "OSMRoadNetwork_row_" + row + "_col_" + col + ".xml";
            string fullxmlFileName;
            if (cacheService == null)
            {
                fullxmlFileName = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName);
            }
            else
            {
                fullxmlFileName = cacheService.GetCacheFullStoragePath(fileName);
            }

            if (!File.Exists(fullxmlFileName))// || ReplaceCachedInformation)
            {
                var box = GetBoundingBox(col, row);
                fullxmlFileName = SaveRoadNetworkFileFromUrl(string.Format(CultureInfo.InvariantCulture, RawURL, box.Left, box.Bottom, box.Right, box.Top), fullxmlFileName, 0);
            }

            if (string.IsNullOrEmpty(fullxmlFileName)) return;
            //Copy the file to the current story file location
            if(DepictionAccess.PathService != null)
            {
                var storyFileLocation = DepictionAccess.PathService.CurrentStoryDataDirectory;
                var fileDataName = Path.Combine(storyFileLocation, fileName);
                File.Copy(fullxmlFileName,fileDataName,true);
            }

            try
            {
                OSMNode[] nodesInFile;
                OSMWay[] waysInFile;
                OSMGatherUtilities.GetNodesAndWaysFromFile(fullxmlFileName, out nodesInFile, out waysInFile, this);
                CombineNodesAndWays(nodesInFile,waysInFile,nodes,ways,ServiceStopRequested);
//                lock (nodes)
//                {
//                    foreach (var node in nodesInFile)
//                    {
//                        if (ServiceStopRequested) break;
//                        if (!nodes.ContainsKey(node.NodeID))
//                            nodes.Add(node.NodeID, node);
//                    }
//                }
//
//                lock (ways)
//                {
//                    foreach (var way in waysInFile)
//                    {
//                        if (ServiceStopRequested) break;
//                        if (!ways.ContainsKey(way.WayID))
//                            ways.Add(way.WayID, way);
//                    }
//                }
            }
            catch (Exception)
            {
                // Don't keep a cached file that failed to process.

                if (File.Exists(fullxmlFileName))
                    File.Delete(fullxmlFileName);
                // DepictionAccess.PathService.DeleteCachedFile(fileName);
            }
            e.Result = fileName;
        }

        static internal void CombineNodesAndWays(OSMNode[] nodesInFile, OSMWay[] waysInFile, Dictionary<long, OSMNode> nodeDict, Dictionary<long, OSMWay> wayDict,bool serviceState)
        {
            lock (nodeDict)
            {
                foreach (var node in nodesInFile)
                {
                    if (serviceState) break;
                    if (!nodeDict.ContainsKey(node.NodeID))
                        nodeDict.Add(node.NodeID, node);
                }
            }

            lock (wayDict)
            {
                foreach (var way in waysInFile)
                {
                    if (serviceState) break;
                    if (!wayDict.ContainsKey(way.WayID))
                        wayDict.Add(way.WayID, way);
                }
            }
        }
        
        #endregion

        #region public methods
       
        static public RoadGraph BuildRoadGraph(IEnumerable<RoadSegment> roadSegments)
        {
            var roadGraph = new RoadGraph();

            //construct the graph from road segments
            foreach (var ls in roadSegments)
            {
                roadGraph.AddVertex(ls.Source);
                roadGraph.AddVertex(ls.Target);
                roadGraph.AddEdge(ls);
            }
            return roadGraph;
        }

        #endregion

        #region private helpers

        #region static methods


        private static int getColumn(double Longitude)
        {
            return Convert.ToInt32(Math.Floor((Longitude + 180) / cachedTileDiameterInDegrees));
        }

        private static int getRow(double Latitude)
        {
            return Convert.ToInt32(Math.Floor(Math.Abs(Latitude - 80) / cachedTileDiameterInDegrees));
        }

        private static ICartRect GetBoundingBox(int column, int row)
        {
            var topLeft = new Point((column * cachedTileDiameterInDegrees) - 180, 80 - (row * cachedTileDiameterInDegrees));
            var botRight = new Point(topLeft.X + cachedTileDiameterInDegrees, topLeft.Y - cachedTileDiameterInDegrees);
            return new CartRect(topLeft, botRight);
            //            var topLeft = new LatitudeLongitude(80 - (row * cachedTileDiameterInDegrees), (column * cachedTileDiameterInDegrees) - 180);
            //            return new MapCoordinateBounds(topLeft, new LatitudeLongitude(topLeft.Latitude - cachedTileDiameterInDegrees, topLeft.Longitude + cachedTileDiameterInDegrees));
        }
        #endregion

        internal string SaveRoadNetworkFileFromUrl(string url, string fileName, int exceptionCount)
        {
            //var webClient = new WebClient();
            //var buffer = new byte[0];
            if (ServiceStopRequested) return null;

            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.AutomaticDecompression = DecompressionMethods.GZip;
            HttpWebResponse webResponse = null;
            if (ServiceStopRequested) return null;
            try
            {
                webResponse = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException ex)
            {
                // Retry 3 times if there is a web exception (time-out)
                if (ServiceStopRequested)
                {
                    if (webResponse != null) webResponse.Close();
                    return null;
                }

                if (exceptionCount < 3)
                {
                    return SaveRoadNetworkFileFromUrl(url, fileName, exceptionCount + 1);
                }
                //                DepictionAccess.NotificationService.DisplayMessageString(ex.Message, 5);
            }
            if (ServiceStopRequested || webResponse == null)
            {
                if (webResponse != null) webResponse.Close();
                return null;
            }
            var buffer = webResponse.GetResponseStream();
            if (buffer == null) return string.Empty;
            if (!string.IsNullOrEmpty(fileName))
            {
                using (var fileStream = new FileStream(fileName, FileMode.Create))
                {
                    buffer.CopyTo(fileStream);
                }
            }

            // if this file was created while we were waiting, let's not redo it, why would this occur
            //            var valuetoReturn = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName) ??
            //                                DepictionAccess.PathService.CacheFile(webResponse.GetResponseStream(), fileName);
            webResponse.Close();
            return fileName;
        }



        #endregion
    }
}