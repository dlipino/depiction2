﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Resources;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;

namespace Depiction2.SimpleRoutes
{
    [ResourceExtensionMetadata(DisplayName = "RoadNetwork Resource", Name = "ResourcesRoadNetwork", Author = "Depiction Inc.", ExtensionPackage = "Default", ResourceName = "RoadNetworkResources")]
    public class RoadNetworkResources : IDepictionResourceExtension
    {

        public ResourceDictionary ExtensionResources
        {
            get
            {
                var uri = new Uri("/Depiction2.SimpleRoutes;Component/Resources/Images/RouteResources.xaml",UriKind.RelativeOrAbsolute);
                var rd = new ResourceDictionary {Source = uri};
                return rd;
            }
        }

        public Dictionary<string, Type> ExtensionTypes
        {
            get { return new Dictionary<string, Type> {{typeof (RoadGraph).Name, typeof (RoadGraph)}}; }
        }

        public void Dispose()
        {

        }
    }
}