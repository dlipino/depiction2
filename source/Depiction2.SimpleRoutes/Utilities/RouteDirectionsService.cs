﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Depiction2.API.Measurement;
using Depiction2.API.Properties;
using Depiction2.API.Service;
using Depiction2.Base.Measurement;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;

namespace Depiction2.SimpleRoutes.Utilities
{
    public class RouteDirectionsService
    {
        public string GetRouteDirections(IList<IList<RoadSegment>> list, IList<double> completeRouteTimes)
        {
            var stringBuilder = new StringBuilder();
            var measureSystem = Settings.Default.MeasurementSystem;
            const MeasurementScale measureScale = MeasurementScale.Large;
            var completeRouteDistance = 0d;
            //var completeRouteTime = driveTimes.Sum();


            for (int i = 0; i < list.Count; i++)
            {
                var roadSegments = list[i];
                double partialDistance;
                var partialDirections = GetRawDirections(roadSegments, completeRouteTimes.Count > i ? completeRouteTimes[i] : 0, out partialDistance);
                stringBuilder.Append(partialDirections);
                completeRouteDistance += partialDistance;
            }

            //Header
            var directionHeader = "<h5>Directions: </h5>";
            if (list.Count > 1)
            {
                var distString = new Distance().GetUnits(measureSystem, measureScale);
                directionHeader += String.Format("<p><b>Total route distance: {0}{1}</b></p>",
                                                 completeRouteDistance.ToString("F2"), distString);
                if (completeRouteTimes.Count > 0)
                    directionHeader += String.Format("<p><b>Estimated route time: {0} minutes</b></p>",
                                                     completeRouteTimes.Sum().ToString("F0"));
            }

            stringBuilder.Insert(0, directionHeader);

            return stringBuilder.ToString();
        }

        private static string GetRawDirections(IList<RoadSegment> route, double time, out double distance)
        {
            MeasurementSystem measureSystem = Settings.Default.MeasurementSystem;
            const MeasurementScale measureScale = MeasurementScale.Large;
            var distString = new Distance().GetUnits(measureSystem, measureScale);

            StringBuilder directions = new StringBuilder(); ;// "<h5>Directions: </h5>";
            directions.Append("<ul>");
            distance = 0;
            if (route.Count == 1)
            {//just start and end Nodes in the route
                var streetName = String.IsNullOrEmpty(route[0].Name) ? "[unknown streetname]" : route[0].Name;
                distance = DepictionPositionService.Instance.DistanceBetween(route[0].Source.Vertex, route[0].Target.Vertex, measureSystem, measureScale);
                directions.Append(String.Format("<li>Start on {0}</li>", streetName));
                directions.Append(String.Format("<li>End on {0}</li>", streetName));
            }
            else if (route.Count > 1)
            {
                //directions += "<ul>";
                string distanceMessage;
                var startIndex = 0;
                double segmentLength;
                int nextIndex = FindNextJunction(route, startIndex, out distanceMessage, out segmentLength);
                distance += segmentLength;
                if (startIndex == nextIndex || nextIndex == route.Count - 1)
                {
                    var currentStreet = String.IsNullOrEmpty(route[startIndex].Name)
                                            ? "[unknown street name]"
                                            : route[startIndex].Name;
                    directions.Append(String.Format("<li>Proceed on {0} for {1}</li>", currentStreet, distanceMessage));
                    var nextStreetName = String.IsNullOrEmpty(route[nextIndex].Name)
                                             ? "[unknown street name]"
                                             : route[nextIndex].Name;
                    if (!currentStreet.Equals(route[nextIndex].Name))
                    {
                        //might need a turn here                        
                        var finalturn = GetTurn(route[nextIndex - 1].Source.Vertex, route[nextIndex].Source.Vertex,
                                                route[nextIndex].Target.Vertex);
                        if (!finalturn.Equals(""))
                        {

                            directions.Append(String.Format("<li>{0} onto {1}</li>", finalturn, nextStreetName));
                        }
                        //add the last segment's length to totalDistance
                        distance += DepictionPositionService.Instance.DistanceBetween(route[nextIndex].Source.Vertex,route[nextIndex].Target.Vertex, measureSystem, measureScale);
                    }
                    directions.Append(String.Format("<li><b>End on {0}</b>", nextStreetName));
                    // directions += String.Format("<p><b>Total route distance: {0}mi</b>", totalDistance.ToString("F2"));
                }
                else
                {

                    var towardNodeName = route[nextIndex].Name;
                    if (towardNodeName.Equals("")) towardNodeName = "[unknown street name]";
                    var currentStreetName = String.IsNullOrEmpty(route[nextIndex - 1].Name)
                                                ? "[unknown street name]"
                                                : route[nextIndex - 1].Name;
                    directions.Append(ProceedOnMessage(currentStreetName, towardNodeName, distanceMessage));
                    var turn = GetTurn(route[nextIndex - 1].Source.Vertex, route[nextIndex].Source.Vertex,
                                       route[nextIndex].Target.Vertex);
                    if (!turn.Equals(""))
                    {
                        var nextStreetName = String.IsNullOrEmpty(route[nextIndex].Name)
                                                 ? "[unknown street name]"
                                                 : route[nextIndex].Name;
                        directions.Append(String.Format("<li>{0} onto {1}</li>", turn, nextStreetName));
                    }
                    while (nextIndex < route.Count - 1)
                    {
                        startIndex = nextIndex;
                        nextIndex = FindNextJunction(route, startIndex, out distanceMessage, out segmentLength);
                        distance += segmentLength;
                        currentStreetName = String.IsNullOrEmpty(route[startIndex].Name)
                                                ? "[unknown street name]"
                                                : route[startIndex].Name;
                        var nextStreetName = String.IsNullOrEmpty(route[nextIndex].Name)
                                                 ? "[unknown street name]"
                                                 : route[nextIndex].Name;

                        if (startIndex != nextIndex && nextIndex < route.Count - 1)
                        {
                            if (segmentLength > 0.25) //street segment length is at least 0.25 miles
                                directions.Append(String.Format("<li>{0} on {1}</li>", distanceMessage, currentStreetName));
                            turn = GetTurn(route[nextIndex - 1].Source.Vertex, route[nextIndex].Source.Vertex,
                                           route[nextIndex].Target.Vertex);
                            if (!turn.Equals(""))
                            {
                                directions.Append(String.Format("<li>{0} onto {1}</li>", turn, nextStreetName));
                            }
                        }
                        else
                        {
                            if (!currentStreetName.Equals(route[nextIndex].Name))
                            {
                                //might need a turn here                        
                                var finalturn = GetTurn(route[nextIndex - 1].Source.Vertex,
                                                        route[nextIndex].Source.Vertex,
                                                        route[nextIndex].Target.Vertex);
                                if (!finalturn.Equals(""))
                                {
                                    directions.Append(String.Format("<li>{0} onto {1}</li>", finalturn, nextStreetName));
                                }
                                //add the last segment's length to totalDistance
                                distance +=
                                    DepictionPositionService.Instance.DistanceBetween(route[route.Count - 1].Source.Vertex,
                                        route[route.Count - 1].Target.Vertex, measureSystem, measureScale);

                            }
                            directions.Append(String.Format("<li><b>End on {0}</b>", nextStreetName));
                        }

                    }
                }

                //directions += String.Format("<p><b>Total route distance: {0}mi</b>", totalDistance.ToString("F2"));
            }
            directions.Append("</ul>");

            if (time > 0)
                directions.Insert(0, String.Format("<p><b>Estimated route time: {0} minutes</b></p>", time.ToString("F0")));
            directions.Insert(0, String.Format("<p><b>Route distance: {0}{1}</b></p>", distance.ToString("F2"), distString));

            return directions.ToString();

        }
        private static string ProceedOnMessage(string currentStreet, string nextStreet, string distanceMsg)
        {
            return String.Format("<li>Proceed on {0} toward {1} for {2}</li>", currentStreet, nextStreet, distanceMsg);

        }

        /// <summary>
        /// Starting from startIndex, find the next junction in the route that is on a different street segment
        /// </summary>
        /// <param name="route"></param>
        /// <param name="startIndex"></param>
        /// <param name="message"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
        private static int FindNextJunction(IList<RoadSegment> route, int startIndex, out string message, out double distance)
        {
            MeasurementSystem measureSystem = Settings.Default.MeasurementSystem;
            const MeasurementScale measureScale = MeasurementScale.Large;

            var distString = new Distance().GetUnits(measureSystem, measureScale);
            var endIndex = -1;
            var roadLength = 0d;
            distance = 0d;
            string startNodeName = route[startIndex].Name;
            for (int i = startIndex; i < route.Count; i++)
            {
                if (startNodeName.Equals(route[i].Name))
                {
                    roadLength += DepictionPositionService.Instance.DistanceBetween(route[i].Source.Vertex,route[i].Target.Vertex, measureSystem, measureScale);
                }
                else
                {
                    endIndex = i;
                    break;
                }
            }
            if (endIndex == -1)
            {
                endIndex = route.Count - 1;
            }
            if (startIndex == endIndex)
            {
                endIndex++;
                roadLength += DepictionPositionService.Instance.DistanceBetween(route[startIndex].Source.Vertex,route[endIndex].Target.Vertex, measureSystem, measureScale);
            }
            message = String.Format("({0} {1}) ", roadLength.ToString("F2"), distString);
            distance += roadLength;
            return endIndex;
        }

        /// <summary>
        /// Given waypoints 1, 2, and 3, find the turn involved in moving from 1 to 2 to 3
        /// </summary>
        /// <param name="prevNode"></param>
        /// <param name="currentNode"></param>
        /// <param name="nextNode"></param>
        /// <returns></returns>
        private static string GetTurn(Point prevNode, Point currentNode, Point nextNode)
        {
            if (prevNode.X.Equals(double.NaN) || prevNode.Y.Equals(double.NaN) ||
                currentNode.X.Equals(double.NaN) || currentNode.Y.Equals(double.NaN) ||
                 nextNode.X.Equals(double.NaN) || nextNode.Y.Equals(double.NaN)) return "";

            double a = 1;
            double c = (currentNode.X) - prevNode.X; //(currentNode.Longitude) - prevNode.Longitude;
            double d = (currentNode.Y) - prevNode.Y;//(currentNode.Latitude) - prevNode.Latitude;

            double mag = Math.Sqrt(c * c + d * d);
            if (mag <= 0.000000001) return "";
            double z = (a * c) / mag;
            double angle = Math.Acos(z); //this is the angle subtended by the X axis and the line joining
            //the first node with the second node
            //the angle is in RADIANS
            angle *= 180 / Math.PI;
            if (d < 0)
            {
                angle = -angle;
            }


            //find second angle
            c = (nextNode.X) - currentNode.X;//(nextNode.Longitude) - currentNode.Longitude;
            d = (nextNode.Y) - currentNode.Y;//(nextNode.Latitude) - currentNode.Latitude;

            mag = Math.Sqrt(c * c + d * d);
            if (mag <= 0.000000001) return "";
            z = (a * c) / mag;
            double angle2 = Math.Acos(z); //this is the angle subtended by the X axis and the line joining
            //the second node with the third node
            //the angle is in RADIANS
            angle2 *= 180 / Math.PI;
            if (d < 0)
            {
                angle2 = -angle2;
            }

            //Determine TURN direction
            if (angle < 0 && angle2 > 0)
            {
                if (angle2 < angle + 180) return "Left";
                return "Right";
            }
            if ((angle > 0 && angle2 < 0))
            {
                if (angle2 < angle - 180) return "Left";
                return "Right";
            }
            //if both angles are positive or both negative
            if (angle > angle2) return "Right";
            return "Left";
        }


    }
}
