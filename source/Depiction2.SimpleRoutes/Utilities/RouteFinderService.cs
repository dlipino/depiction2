﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Depiction2.SimpleRoutes.OSMRoadNetwork.Utilities;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.ShortestPath;

namespace Depiction2.SimpleRoutes.Utilities
{
    public class RouteFinderService 
    {
        private readonly RoadGraph _roadGraph;
        private RoadNode _start, _end;
        public RouteFinderService(RoadGraph roadGraph)
        {
            _roadGraph = roadGraph;
        }


        static public IList<RoadSegment> FindRouteOnGraph(RoadGraph roadGraph, IEnumerable<RoadNode> waypointList, string routeType, out bool hasFreeformSegments, out double? estimatedTime)
        {
            IEnumerable<RoadSegment> path = null;
            estimatedTime = null;
            hasFreeformSegments = false;
            RoadNode start = null;
            RoadNode end = null;
            foreach (var wayPoint in waypointList)
            {
                if (start == null)
                {
                    start = wayPoint;
                    continue;
                }
                end = wayPoint;
                DijkstraShortestPathAlgorithm<RoadNode, RoadSegment> dijkstra = null;

                if (routeType.Equals("Depiction.Plugin.RouteRoadNetwork"))
                {
                    dijkstra = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(roadGraph.Graph, roadGraph.GetEdgeWeight);
                }
//                else if (route.ElementType.Equals("Depiction.Plugin.DriveTimeRoute"))
//                {
//                    dijkstra = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(roadGraph.Graph, roadGraph.GetDriveTimeFunc(route));
//                }

                if (dijkstra == null)
                    return null;

                var startTime = DateTime.Now;
                // Attach a Vertex Predecessor Recorder Observer to give us the paths
                var predecessors = new VertexPredecessorRecorderObserver<RoadNode, RoadSegment>();
                using (predecessors.Attach(dijkstra))
                {
                    dijkstra.Compute(start);
                    predecessors.TryGetPath(end, out path);
                }

                Debug.WriteLine(string.Format("Finding route took {0} milliseconds", (DateTime.Now - startTime).TotalMilliseconds));
                if (dijkstra.State.Equals(ComputationState.Finished))
                {
                    double cost = dijkstra.Distances[end];
                    if (routeType.Equals("Depiction.Plugin.DriveTimeRoute"))
                        estimatedTime = cost;
                    if (cost > 1000000)
                    {
                        hasFreeformSegments = true;
                    }
                }

                start = end;
            }
            
            return path != null ? path.ToList() : null;
        }

    }
}
