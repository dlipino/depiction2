﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Depiction2.Base.StoryEntities;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

[assembly: InternalsVisibleTo("Depiction2.Terrain.UnitTests")]
namespace Depiction2.Story14IO.UnitTests.Dpn14
{
    public class Dpn14LoadUtilities
    {
        static public IElement FindElementByFullType(List<IElement> list, string fullType)
        {
            return list.FirstOrDefault(t => t.ElementType.Equals(fullType, StringComparison.CurrentCultureIgnoreCase));
        }
        static public IEnumerable<IElement> FindAllElementsByFullType(List<IElement> list, string fullType)
        {
            return list.Where(t => t.ElementType.Equals(fullType, StringComparison.CurrentCultureIgnoreCase));
        }
        public static string GetFullFileName(string holdingFolder,string dpnFile)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("Dpn14", holdingFolder);
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, dpnFile);
            Assert.IsTrue(File.Exists(fullFileName));

            return fullFileName;
        }

        public static string GetFullFileNameAndUpdateTypes(string dpnFile)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("Dpn14", "TestStoryFiles");
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, dpnFile);
            Assert.IsTrue(File.Exists(fullFileName));

            Depiction14DpnInformationLoader.UpdateDepictionAccessTypeList(fullFileName);

            return fullFileName;
        }
       public static string GetFullTestFileName(string dmlFile)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("Dpn14", "TestElementFiles");
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, dmlFile);
            Assert.IsTrue(File.Exists(fullFileName), "Could not find the expected dml file");
            return fullFileName;
        }
    }
}