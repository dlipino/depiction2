﻿using System.IO;
using System.Reflection;
using Depiction2.API;
using Depiction2.API.Measurement;
using Depiction2.API.Measurement.Converters;
using Depiction2.Base.StoryEntities;
using System.Linq;
using Depiction2.Story14IO.Story14.DmlLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.DmlLoaders
{
    [TestFixture]
    public class DmlToElementTemplateTest
    {
        static internal string GetFullDmlFileNameForTest(string dmlFile)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("Dpn14", "TestElementFiles");
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, dmlFile);
            Assert.IsTrue(File.Exists(fullFileName), "Could not find the expected dml file");
            return fullFileName;
        }

        [SetUp]
        public void Setup()
        {

            //            DepictionAccess.NameTypeService = new DepictionSimpleTypeService();
        }

        [TearDown]
        public void TeadDown()
        {

            //            DepictionAccess.NameTypeService = null;
        }

        [Test]
        public void CompleteDmlLoadTest()
        {
            var fullDmlFileName = GetFullDmlFileNameForTest("RectangleShape.dml");
            var scaffold = Dml14ToTemplate.GetScaffoldFrom14DmlFile(fullDmlFileName);


            Assert.IsNotNull(scaffold);
            Assert.AreEqual(3, scaffold.Properties.Count);
            Assert.AreEqual(1, scaffold.OnCreateActions.Count);
            Assert.AreEqual(10, scaffold.InfoProperties.Count);
        }
        [Test]
        public void CompleteDml2LoadTest()
        {
            var fullDmlFileName = GetFullDmlFileNameForTest("Depiction.Plugin.Sensor.dml");
            var scaffold = Dml14ToTemplate.GetScaffoldFrom14DmlFile(fullDmlFileName);
            Assert.IsNotNull(scaffold);
            Assert.AreEqual(5, scaffold.Properties.Count);
            Assert.AreEqual(1, scaffold.OnCreateActions.Count);
            Assert.AreEqual(7, scaffold.InfoProperties.Count);
            Assert.IsTrue(scaffold.ShowPropertiesOnCreate);
            Assert.IsTrue(scaffold.IsMouseAddable);
            var angleProps = scaffold.Properties.Where(t => t.ProperName.Equals("Orientation")).ToList();
            Assert.IsTrue(angleProps.Count() == 1);
            var angleProp = angleProps[0];
            Assert.AreEqual(typeof(Angle).Name,angleProp.TypeString);
            var angleConverter = new AngleConverter();
            var angleValue = angleConverter.ConvertFromString(angleProp.ValueString) as Angle;
            Assert.IsNotNull(angleValue);
            Assert.AreEqual(30,angleValue.Value);

        }
        [Test]
        public void CompleteSimpleDmlLoadTest()
        {
            var fullDmlFileName = GetFullDmlFileNameForTest("SimpleExample.dml");
            var scaffold = Dml14ToTemplate.GetScaffoldFrom14DmlFile(fullDmlFileName);
            Assert.AreEqual("Depiction.AdminBoundary", scaffold.IconSource);
            Assert.AreEqual(3, scaffold.Properties.Count);
            Assert.IsNotNull(scaffold);
            Assert.AreEqual(1, scaffold.HoverTextProps.Count);
            Assert.IsTrue(scaffold.HoverTextProps.Contains("Author"));

            Assert.IsFalse(scaffold.IsMouseAddable);
            Assert.IsTrue(scaffold.HideFromLibrary);
            Assert.IsTrue(scaffold.ShowPropertiesOnCreate);
            //Check color properties
            var props = scaffold.InfoProperties.Where(t => t.ProperName.Equals(PropNames.ZOIFillColor)).ToList();
            Assert.AreEqual(1, props.Count());
            var prop = props[0];
            Assert.AreEqual("Green", prop.ValueString);

            props = scaffold.InfoProperties.Where(t => t.ProperName.Equals(PropNames.ZOIBorderColor)).ToList();
            Assert.AreEqual(1, props.Count());
            prop = props[0];
            Assert.AreEqual("Yellow", prop.ValueString);
        }
        [Test]
        public void DoesElementLoadProperties()
        {
            var fileName = GetFullDmlFileNameForTest("TestElement.dml");
            var scaffold = Dml14ToTemplate.GetScaffoldFrom14DmlFile(fileName);
            Assert.IsNotNull(scaffold);
            Assert.AreEqual(1, scaffold.Properties.Count);

            Assert.AreEqual(1, scaffold.OnCreateActions.Count);

            var property = scaffold.Properties[0];
            Assert.AreEqual(1, property.LegacyPostSetActions.Count);
            Assert.AreEqual(2, property.LegacyValidationRules.Count());


            //for the string validation rule reader.
            //            Assert.AreEqual(2,property.LegacyValidationRuleBase.Count());
            //
            //            var rules = property.LegacyValidationRuleBase.ToArray();
            //
            //            Assert.AreEqual("DataTypeValidationRule",rules[0].ValidationRuleName);
            //            var ruleParams = rules[0].Parameters;
            //            Assert.AreEqual(2,ruleParams.Length);
            //            foreach(var param in ruleParams)
            //            {
            //                Assert.IsFalse(string.IsNullOrEmpty(param.Key));
            //                Assert.IsFalse(string.IsNullOrEmpty(param.Value));
            //            }
            //
            //            Assert.AreEqual("MinValueValidationRule", rules[1].ValidationRuleName);
            //            ruleParams = rules[1].Parameters;
            //            Assert.AreEqual(1, ruleParams.Length);
            //            foreach (var param in ruleParams)
            //            {
            //                Assert.IsFalse(string.IsNullOrEmpty(param.Key));
            //                Assert.IsFalse(string.IsNullOrEmpty(param.Value));
            //            }

        }
    }
}