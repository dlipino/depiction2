﻿using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Story14IO.Story14;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14
{
    [TestFixture]
    public class Legacy14StoryLoaderTest
    {
        private string fullFileName = string.Empty;
        [SetUp]
        public void Setup()
        {

            //            DepictionAccess.NameTypeService = new DepictionSimpleTypeService();
        }

        [TearDown]
        public void TeadDown()
        {

            //            DepictionAccess.NameTypeService = null;
        }
        [Test]
        public void DoesLegacyLoaderLoad()
        {
            var loader = new Depiction14StoryLoader();
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14Elements3Revealers.dpn");
            var fullStory = loader.GetStoryFromFile(fullName);

            Assert.IsNotNull(fullStory);
            Assert.IsNotNull(fullStory.StoryDetails);
            Assert.AreNotEqual("None", fullStory.StoryDetails.Description);
        }
        [Test]
        public void DoesMissingFileTriggerMessage()
        {
            var messenger = DepictionAccess.MessageService;
            var loader = new Depiction14StoryLoader();
            var fullName = Dpn14LoadUtilities.GetFullTestFileName("ElementNoPosition.xml");
            var fullStory = loader.GetStoryFromFile(fullName);
            Assert.AreEqual(1, messenger.StoryCount);
            Assert.IsNull(fullStory);
            messenger.ClearAllMessages();

            fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("FileFormatTooLow.dpn");
            fullStory = loader.GetStoryFromFile(fullName);
            Assert.AreEqual(1, messenger.StoryCount);//checking what message is better
            Assert.IsNull(fullStory);
            messenger.ClearAllMessages();

            fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("MissingFileFormat.dpn");
            fullStory = loader.GetStoryFromFile(fullName);
            Assert.AreEqual(1, messenger.StoryCount);
            Assert.IsNull(fullStory);
            messenger.ClearAllMessages();
        }

    }
}