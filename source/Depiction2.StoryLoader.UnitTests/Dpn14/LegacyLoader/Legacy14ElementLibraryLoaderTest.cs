﻿using System.Linq;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14ElementLibraryLoaderTest: LegacyFileLoaderBase
    {
       
        [Test]
        public void DoesSimpleElementLibraryLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SingleHouse.dpn");
            var dpnScaffolds = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(fullName);

            Assert.AreEqual(1, dpnScaffolds.Count);
            var scaffoldLib = new ElementTemplateLibrary();
            scaffoldLib.AddStoryElementTemplates(dpnScaffolds);

            var entryLocation = Depiction14FileOpenHelpers.DepictionDataDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryIconBinDir;
            var dpnScaffoldImages = Depiction14ImageLoader.GetImagesFromZipFileDirectory(fullName, entryLocation);
            Assert.AreEqual(1, dpnScaffoldImages.Count);
            //            var element = newElements.First();
            //            Assert.IsNotNull(element.ElementGeometry);
            //            Assert.AreNotEqual(Rect.Empty, element.ElementGeometry.Bounds);
            //            //The propertis that have validation rules
            //            var propertiesWithValidation = new List<string> { "NumberOfFloors", "Height", "Value", "FloorSpace", "MaxOccupants" };


        }

        [Test]
        public void DoRouteAndRoadNetworkElementsLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14RouteAndRoadNetwork.dpn");
            var dpnScaffolds = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(fullName);

            Assert.IsNotNull(dpnScaffolds.FirstOrDefault(t => t.ElementType.Equals("start")));
            Assert.IsNotNull(dpnScaffolds.FirstOrDefault(t => t.ElementType.Equals("end")));
            Assert.IsNotNull(dpnScaffolds.FirstOrDefault(t => t.ElementType.Equals("WayPoint")));

            var scaffoldLib = new ElementTemplateLibrary();
            scaffoldLib.AddStoryElementTemplates(dpnScaffolds);
            var waypoint = scaffoldLib.FindElementTemplate("Waypoint");
            Assert.IsNotNull(waypoint);
            var start = scaffoldLib.FindElementTemplate("start");
            Assert.IsNotNull(start);
            var end = scaffoldLib.FindElementTemplate("end");
            Assert.IsNotNull(end);

        }
    }
}