﻿using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14ImageLoaderTest : LegacyFileLoaderBase
    {
        [Test]
        public void DoesGetImageObjectsFromZipFile()
        {
            var fileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved2Images.dpn");
            var imageDictionary = Depiction14ImageLoader.GetDepictionImageNamesFromDPNFile(fileName);
            //OpenStreetMapService_47.63_-122.4_to_47.57_-122.26.jpg
            //Hydrangeas.jpg
            //There are 3 images in that test file
            Assert.AreEqual(3, imageDictionary.Count);
            foreach(string key in imageDictionary.Keys)
            {
                var image = imageDictionary.GetImage(key);
                Assert.IsNotNull(image);
                if(key.Equals("Hydrangeas.jpg"))
                {
                    Assert.AreEqual(1024, image.Width);
                    Assert.AreEqual(768, image.Height);
                }else if(key.Equals("OpenStreetMapService_47.63_-122.4_to_47.57_-122.26.jpg"))
                {
                    Assert.AreEqual(1024,image.Width);
                    Assert.AreEqual(768,image.Height);
                }
            }
        }
    }
}