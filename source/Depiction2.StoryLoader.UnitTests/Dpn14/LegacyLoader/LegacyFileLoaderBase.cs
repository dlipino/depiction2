﻿using Depiction2.API.Service;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class LegacyFileLoaderBase
    {
        [SetUp]
        virtual protected void BaseSetup()
        {
            StorySerializationService.Instance.ResetSerializationFolder();
        }
        [TearDown]
        virtual protected void BaseTearDown()
        {

            StorySerializationService.Instance.CleanSerializationFolders();
        }


    }
}