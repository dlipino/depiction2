﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story14IO.Story14.DmlLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14ElementLoaderTest: LegacyFileLoaderBase
    {
        [Test]
        public void SingleElementInFileTest()
        {
            var fileName = Dpn14LoadUtilities.GetFullTestFileName("SingleElementFile.xml");
            var elements = Dml14ToElement.GetElementsFromXml(fileName, "", null);
            Assert.AreEqual(1,elements.Count);
            var elem = elements.FirstOrDefault();
            Assert.IsNotNull(elem);
            Assert.AreEqual("RedCrossChapterOffice.png", elem.IconResourceName);
            var posString = "30.92249,-81.63638";
            var pos = Point.Parse(posString);
            var geoPos = new Point(pos.Y, pos.X);
            Assert.AreEqual(geoPos, elem.GeoLocation);
            Assert.AreEqual(15, elem.AdditionalPropertyNames.Count);
            Assert.AreEqual("RedCrossChapterOffice.png",elem.IconResourceName);
            Assert.AreEqual("Orange",elem.ZOIBorderColor);
            Assert.AreEqual("Pink",elem.ZOIFillColor);
            Assert.IsFalse(elem.IsGeometryEditable);
        }
        [Test]
        public void SingleElementBadPositionTest()
        {
            var fileName = Dpn14LoadUtilities.GetFullTestFileName("ElementNoPosition.xml");
            var elements = Dml14ToElement.GetElementsFromXml(fileName, "", null);
            Assert.AreEqual(1, elements.Count);
            var elem = elements.FirstOrDefault();
            Assert.IsNotNull(elem);
            Assert.IsNull(elem.GeoLocation);
        }
        [Test]
        public void DoAllUserPropertiesLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SingleHouse.dpn");
            //Setup the library to esure the elements get their properties get placed into
            //the right property list
            var storyLibrary = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(fullName);
            var libraryToUse = new ElementTemplateLibrary();
            libraryToUse.AddStoryElementTemplates(storyLibrary);

            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName, libraryToUse);
            Assert.AreEqual(1, newElements.Count);
            var element = newElements.First();
            //            Assert.IsNotNull(element.ElementGeometry);
            //            Assert.AreNotEqual(Rect.Empty, element.ElementGeometry.Bounds);
            Assert.IsTrue(newElements.Any(t => t.GeoLocation != null));
            //The propertis that have validation rules
            var propertiesWithValidation = new List<string> { "NumberOfFloors", "Height", "Value", "FloorSpace", "MaxOccupants" };
            var propertiesThatShouldNotExist = new List<string> { "Draggable", "DisplayName", "IconSize", "Author" };
            foreach (var propName in propertiesThatShouldNotExist)
            {
                Assert.IsFalse(element.DefaultElementPropertyNames.Any(t => t.Item1.Equals(propName)));
            }
            foreach (var propName in propertiesWithValidation)
            {
                Assert.IsTrue(element.DefaultElementPropertyNames.Any(t => t.Item1.Equals(propName)));
            }
            var expectedHovertextCount = 5;
            var hovertextProps = element.HoverTextProperties;
            Assert.AreEqual(expectedHovertextCount, hovertextProps.Count());
            Assert.AreEqual(1, element.AdditionalPropertyNames.Count);
            Assert.AreEqual(14, element.DefaultElementPropertyNames.Count);
        }
        [Test]
        [Ignore("Doesnt test for anything")]
        public void DoesOldAprsLiveFileLoad()
        {

            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("seattleaprs.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
        }
        [Test]
        [Ignore("Saved file has a roadnetwork, which does not load by default")]
        public void DoesGetGeoLocatedObjectsFromZipFile()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //There are 19 geolocate and 1 non geolocated in that test file
            Assert.AreEqual(20, newElements.Count);
            //one of the ids

            var ids = new List<string> { "80c0d93f-aa92-4b5b-9dc9-c197edb9ccc1", "b85e1f50-70c3-45ae-8870-baabbe531d70" };
            var matching = newElements.Where(x => ids.Contains(x.ElementId));
            Assert.AreEqual(2, matching.Count());
        }

        [Test]
        [Ignore("Must load a geometry extension")]
        public void DoesStoryWithOneNonGeolocatedElementLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142OneNonGeoImage.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //There are 19 geolocate and 1 non geolocated in that test file
            Assert.AreEqual(1, newElements.Count);

            //check things on the non geolocated element
            var nongeo = newElements.FirstOrDefault(t => t.GeoLocation == null);
            Assert.IsNotNull(nongeo.ElementGeometry);
            Assert.AreEqual(false, nongeo.ElementGeometry.IsValid);
        }

        [Test]
        public void DoesStoryWithElementsAndPermatextLoad()
        {
            //Not the greatest of tests since the permatext must first be read by the
            //scaffold reader then sent over to the element. ie this test two thigns at once
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143ElementsWithPermaText.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(6, newElements.Count);
            var buildingId = "2f367d43-1fb5-45b9-9ea4-7827bd01588d";//enhanced
            var building = newElements.FirstOrDefault(t => t.ElementId.Equals(buildingId));
            Assert.IsNotNull(building);
            Assert.IsNotNull(building.PermaText);
            var busId = "18973133-fbd0-429b-82ae-77681944ba08";//simple 
            var bus = newElements.FirstOrDefault(t => t.ElementId.Equals(busId));
            Assert.IsNotNull(bus);
            Assert.IsNotNull(bus.PermaText);
            var carId = "8950bfab-c0a2-4521-b7b9-0ffcaa0a3b59";//no permatext
            var car = newElements.FirstOrDefault(t => t.ElementId.Equals(carId));
            Assert.IsNotNull(car);
            Assert.IsNull(car.PermaText);
        }
        //To aviod lambda restrictions in tests while debugging

        [Test]
        [Ignore("Needs a geometry extension loaded before this works")]
        public void EnsureThatElementVisualVisibilityIsCorrect()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143ElementsWithPermaText.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(6, newElements.Count);

            var v = Enum.GetValues(typeof(ElementVisualSetting));
            var imageElement = Dpn14LoadUtilities.FindElementByFullType(newElements, "Depiction.Plugin.Image");
            Assert.IsNotNull(imageElement, "Couldnt find image");
            var expected = ElementVisualSetting.Image;
            Assert.AreEqual(expected, imageElement.VisualState);

            var circleElement = Dpn14LoadUtilities.FindElementByFullType(newElements, "Depiction.Plugin.CircleShape");
            Assert.IsNotNull(circleElement, "Couldnt find circle");
            expected = ElementVisualSetting.Geometry | ElementVisualSetting.Icon;
            Assert.AreEqual(expected, circleElement.VisualState);

            var shapeElement = Dpn14LoadUtilities.FindElementByFullType(newElements, "Depiction.Plugin.UserDrawnPolygon");
            Assert.IsNotNull(shapeElement, "Couldnt find polygon");
            expected = ElementVisualSetting.Geometry;
            Assert.AreEqual(expected, shapeElement.VisualState);

            var carElement = Dpn14LoadUtilities.FindElementByFullType(newElements, "Depiction.Plugin.Car");
            Assert.IsNotNull(carElement, "Couldnt find car");
            expected = ElementVisualSetting.Icon;
            Assert.AreEqual(expected, carElement.VisualState);
        }
        [Test]
        [Ignore("Needs a geometry extension to be loaded")]
        public void NonGeoAlignedElementDpnLoadTest()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142OneNonGeoImage.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //There are 19 geolocate and 1 non geolocated in that test file
            Assert.AreEqual(1, newElements.Count);

            //check things on the non geolocated element
            var nongeoList = newElements.Where(t => t.GeoLocation == null).ToList();
            Assert.AreEqual(1, nongeoList.Count);

            var nongeo = nongeoList.First();
            Assert.IsNotNull(nongeo.ElementGeometry);
            Assert.AreEqual(false, nongeo.ElementGeometry.IsValid);
            var props = nongeo.AdditionalPropertyNames;

        }

        [Test]
        public void AreUserPropertiesPlacedCorrectly()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("MiniLummi143.dpn");
            //Setup the library to esure the elements get their properties get placed into
            //the right property list
            var storyLibrary = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(fullName);
            var libraryToUse = new ElementTemplateLibrary();
            libraryToUse.AddStoryElementTemplates(storyLibrary);

            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName, libraryToUse);
            Assert.AreEqual(5, newElements.Count);

            var userPolygons = Dpn14LoadUtilities.FindAllElementsByFullType(newElements, "Depiction.Plugin.UserDrawnPolygon").ToList();
            Assert.AreEqual(2, userPolygons.Count());
            var userPoly = userPolygons.First();
            var baseProps = userPoly.DefaultElementPropertyNames;
            var userProps = userPoly.AdditionalPropertyNames;
            Assert.AreNotEqual(0, userProps.Count);
            var loadedPolygons = Dpn14LoadUtilities.FindAllElementsByFullType(newElements, "Depiction.Plugin.LoadedPolygon").ToList();
            Assert.AreEqual(3, loadedPolygons.Count());
            baseProps = userPoly.DefaultElementPropertyNames;
            userProps = userPoly.AdditionalPropertyNames;
            Assert.AreNotEqual(0, userProps.Count);

        }



        [Test]
        public void DpnWithSingleHouse()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SingleHouse.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(1, newElements.Count);
            var element = newElements.First();
            Assert.IsNotNull(element.GeoLocation);
//            Assert.IsNotNull(element.ElementGeometry);
//            Assert.AreNotEqual(Rect.Empty, element.ElementGeometry.Bounds);
        }

        [Test]
        [Ignore("Needs a loaded geometry extension")]
        public void DpnWithShapeTest()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("DepictionThreeShapeElements.dpn");
            var elements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            var userLineElementKey = "93a0ef49-9c95-44a3-9658-c6438bad058d";
            var userLineElement = elements.FirstOrDefault(t => t.ElementId.Equals(userLineElementKey));
            Assert.IsNotNull(userLineElement);
            foreach(var elem in elements)
            {
                Assert.IsNotNull(elem.ElementGeometry);
            }
        }
        [Test]
        [Ignore("Test uses interaction rules, which are not loaded yet")]
        public void DoPropertiesFromLoadedElementsTriggerPropertyChangeEvents()
        {
//            var dir = AppDomain.CurrentDomain.BaseDirectory;
//            DepictionExtensionLibrary.Instance.ComposeFromDir(dir);
//
//            var fullName = GetFullFileNameAndUpdateTypes("DepictionThreeShapeElements.dpn");
//            var elementList = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
//            var circleElement = elementList.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.CircleShape"));
//            Assert.IsNotNull(circleElement);
//            var radiusProp = circleElement.GetProperty("Radius");
//            Assert.IsNotNull(radiusProp);
//            var propTriggered = false;
//            var origArea = circleElement.ElementGeometry.Bounds;
//            Assert.AreNotEqual(Rect.Empty, origArea);
//            circleElement.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e)
//                                                 {
//                                                     if (Equals("Radius", e.PropertyName))
//                                                     {
//                                                         propTriggered = true;
//                                                     }
//                                                 };
//            radiusProp.SetValue(new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 12));
//            Assert.IsTrue(propTriggered);
//            var newArea = circleElement.ElementGeometry.Bounds;
//            Assert.AreNotEqual(Rect.Empty, newArea);
//            Assert.AreNotEqual(origArea, newArea);
//
//            DepictionExtensionLibrary.Instance.Decompose();
        }



        [Test]
        public void DoesFileWithOneElementLoadScaffold()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SingleHouse.dpn");
            var elementScaffolds = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(1, elementScaffolds.Count);
        }
        [Test]
        public void DoImageElementLoadWithScaffold()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143ThreeImages.dpn");
            var elementScaffolds = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(3, elementScaffolds.Count);
            //            var elementList = new List<IElement>();
            //            foreach (var scaffold in elementScaffolds)
            //            {
            //                var element = ElementAndElemTemplateService.ScaffoldToElement(scaffold);
            //                Assert.IsNotNull(element);
            //                elementList.Add(element);
            //            }
        }
       
        [Test]
        public void DoesStoryWithCircleElementAndPropertyActionsLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142SingleCircle.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(1, newElements.Count);
            var elem = newElements[0];

            var radiusProp = elem.GetDepictionProperty("Radius");
            Assert.IsNotNull(radiusProp);
        }
    }
}