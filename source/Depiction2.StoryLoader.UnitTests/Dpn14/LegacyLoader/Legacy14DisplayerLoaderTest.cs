﻿using System.Linq;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14DisplayerLoaderTest
    {

        [Test]
        public void DoesGetDisplayerObjectsFromZipFile()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved.dpn");
            Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(fullFileName);
            var displayers = Depiction14DisplayerLoader.GetDepictionDisplayersFromDPNFile(fullFileName);
            //There are 3 displayers in that test file; one backdrop and two revealers
            var dList = displayers.ToList();
            Assert.AreEqual(3, dList.Count);

            var count = 0;
            foreach (var displayer in dList)
            {
                var name = displayer.DisplayerName;
                var elemCount = displayer.ElementIds.Count;
                var bounds = displayer.GeoRectBounds;
                var id = displayer.DisplayerId;
                if (count == 0)
                {
                    Assert.AreEqual(16, elemCount);
                    Assert.IsTrue(bounds.IsEmpty);
                    Assert.AreEqual("Backdrop", name);
                    Assert.AreEqual("482be388-341f-43f0-b1d8-025ec742014b", id);
                }
                else
                {
                    var revealer = displayer as IElementRevealer;
                    Assert.IsNotNull(revealer);

                    if (name.Equals("Street Map (OpenStreetMap)"))
                    {
                        //Cartesian top="47.629383090624117" left="-122.33364404000001" bottom="47.606854108451586" right="-122.27811764"
                        Assert.AreEqual(1, elemCount);

                        Assert.AreEqual(47.629383090624117, revealer.GeoTop,.00001);
                        Assert.AreEqual(-122.33364404000001, revealer.GeoLeft, .00001);
                        Assert.AreEqual(47.592071813634561, revealer.GeoBottom, .00001);
                        Assert.AreEqual(-122.27811764, revealer.GeoRight, .00001);

                        Assert.AreEqual("3eea09ea-2df9-4b1f-9559-dc890d5fb7ca", id);
                        Assert.AreEqual(DisplayerViewType.Circle, revealer.RevealerShape);
                    }
                    else if (name.Equals("Elevation - Seamless"))
                    {
                        //Cartesian top="47.624993351380517" left="-122.38448539999999" bottom="47.602463691054616" right="-122.328959"
                        Assert.AreEqual(1, elemCount);
                        Assert.AreEqual(47.624993351380517, revealer.GeoTop, .00001);
                        Assert.AreEqual(-122.38448539999999, revealer.GeoLeft, .00001);
                        Assert.AreEqual(47.602463691054616, revealer.GeoBottom, .00001);
                        Assert.AreEqual(-122.328959, revealer.GeoRight, .00001);

                        Assert.AreEqual("d431005c-fbbd-49cc-87fa-a83e154f72fe", id);

                        Assert.AreEqual(DisplayerViewType.Rectangle, revealer.RevealerShape);
                    }
                    else
                    {
                        Assert.IsTrue(false);
                    }
                }
                count++;
            }
        }

        [Test]
        public void DoesDPNWithOneRevealerWithManyElementsLoad()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142SavedOneRevealerManyElements.dpn");
            //hmmm
            Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(fullFileName);
            var displayers = Depiction14DisplayerLoader.GetDepictionDisplayersFromDPNFile(fullFileName);
            //There are 3 displayers in that test file; one backdrop and two revealers
            var dList = displayers.ToList();
            Assert.AreEqual(2, dList.Count);

            var count = 0;
            foreach (var displayer in dList)
            {
                var name = displayer.DisplayerName;
                var elemCount = displayer.ElementIds.Count;
                var bounds = displayer.GeoRectBounds;
                var id = displayer.DisplayerId;
                if (count == 0)
                {
                    Assert.AreEqual(0, elemCount);
                    Assert.IsTrue(bounds.IsEmpty);
                    Assert.AreEqual("Backdrop", name);
                }
                else
                {
                }
                count++;
            }
        }

        [Test]
        public void DoRevealersLoadWithCorrectSettings()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14Elements3Revealers.dpn");
            //hmmm
            Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(fullFileName);
            var displayers = Depiction14DisplayerLoader.GetDepictionDisplayersFromDPNFile(fullFileName);
            //There are 4 displayers in that test file; one backdrop and 3 revealers
            var dList = displayers.ToList();
            Assert.AreEqual(4, dList.Count);

            var count = 0;
            foreach (var displayer in dList)
            {
                var name = displayer.DisplayerName;
                var elemCount = displayer.ElementIds.Count;
                var bounds = displayer.GeoRectBounds;
                var id = displayer.DisplayerId;
                if (count == 0)
                {
//                    Assert.AreEqual(0, elemCount);
//                    Assert.AreEqual(Rect.Empty, bounds);
//                    Assert.AreEqual("Backdrop", name);
                }
                else
                {
                    var rev = displayer as IElementRevealer;
                    Assert.IsNotNull(rev);
                    if (name.Equals("Revealer 1"))
                    {
                        Assert.IsTrue((bool)rev.GetPropertyValue("Maximized"));
                        Assert.IsTrue((bool)rev.GetPropertyValue("BorderVisible"));
                        Assert.IsTrue((bool)rev.GetPropertyValue("Anchored"));
//   <property name="Maximized" value="True" valueType="Boolean" />
//      <property name="BorderVisible" value="True" valueType="Boolean" />
//      <property name="Anchored" value="True" valueType="Boolean" />
//      <property name="Opacity" value="0.5" valueType="Double" />
//      <property name="RevealerShape" value="Rectangle" valueType="RevealerShapeType" />
//      <property name="TopMenu" value="True" valueType="Boolean" />
//      <property name="BottomMenu" value="True" valueType="Boolean" />
//      <property name="SideMenu" value="False" valueType="Boolean" />
//      <property name="PermaTextVisible" value="True" valueType="Boolean" />
//      <property name="PermaTextX" value="-25" valueType="Double" />
//      <property name="PermaTextY" value="-25" valueType="Double" />
                    }
                    else if (name.Equals("Revealer 2"))
                    {
                        Assert.IsTrue((bool)rev.GetPropertyValue("Maximized"));
                        Assert.IsFalse((bool)rev.GetPropertyValue("BorderVisible"));
                        Assert.IsTrue((bool)rev.GetPropertyValue("Anchored"));
//      <property name="Maximized" value="True" valueType="Boolean" />
//      <property name="BorderVisible" value="False" valueType="Boolean" />
//      <property name="Anchored" value="True" valueType="Boolean" />
//      <property name="Opacity" value="0.5" valueType="Double" />
//      <property name="RevealerShape" value="Rectangle" valueType="RevealerShapeType" />
//      <property name="TopMenu" value="True" valueType="Boolean" />
//      <property name="BottomMenu" value="True" valueType="Boolean" />
//      <property name="SideMenu" value="False" valueType="Boolean" />
//      <property name="PermaTextVisible" value="True" valueType="Boolean" />
//      <property name="PermaTextX" value="-25" valueType="Double" />
//      <property name="PermaTextY" value="-25" valueType="Double" />
                    }
                    else if (name.Equals("Empty"))
                    {
                        Assert.IsFalse((bool)rev.GetPropertyValue("Maximized"));
                        Assert.IsFalse((bool)rev.GetPropertyValue("BorderVisible"));
                        Assert.IsFalse((bool)rev.GetPropertyValue("Anchored"));
//    <property name="Maximized" value="False" valueType="Boolean" />
//      <property name="BorderVisible" value="False" valueType="Boolean" />
//      <property name="Anchored" value="False" valueType="Boolean" />
//      <property name="Opacity" value="0.5" valueType="Double" />
//      <property name="RevealerShape" value="Rectangle" valueType="RevealerShapeType" />
//      <property name="TopMenu" value="True" valueType="Boolean" />
//      <property name="BottomMenu" value="True" valueType="Boolean" />
//      <property name="SideMenu" value="False" valueType="Boolean" />
//      <property name="PermaTextVisible" value="True" valueType="Boolean" />
//      <property name="PermaTextX" value="-25" valueType="Double" />
//      <property name="PermaTextY" value="-25" valueType="Double" />
                    }
                }
                count++;
            }
        }
    }
}