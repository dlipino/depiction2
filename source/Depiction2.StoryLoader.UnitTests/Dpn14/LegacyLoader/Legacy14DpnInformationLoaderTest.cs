﻿using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14DpnInformationLoaderTest : LegacyFileLoaderBase
    {
       
        [Test]
        public void DoesGetMetadataFromZipFileWork()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14Elements3Revealers.dpn");
            var details = Depiction14DpnInformationLoader.GetMetadataInformationFromDPNFile(fullFileName);
            var expectedTitle = "Elements and 3 revealers";
            var expectedAuthor = "Depiction testing";
            var expectedDescritpino =
                "A simple depiction 1.4 file that will be used for testing depiction file information and revealer save/load abilities.";

            Assert.AreEqual(expectedTitle, details.Title);
            Assert.AreEqual(expectedAuthor, details.Author);
            Assert.AreEqual(expectedDescritpino, details.Description);

        }
        [Test]
        public void DoesGetFileFormatFromZipFileWork()
        {

            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SingleHouse.dpn");
            var storyInfo = Depiction14DpnInformationLoader.GetFileInformationFromDPNFile(fullFileName);
            Assert.AreEqual("1",storyInfo.FileVersion);
            Assert.AreEqual("1.4.3.12525", storyInfo.AppVersion);
        }
        [Test]
        public void DoesGetGeoInformationFromZipFileWork()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved2Images.dpn");
            var storyGeoInfo = Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(fullFileName);
            Assert.IsFalse(double.IsInfinity(storyGeoInfo.MapViewBounds.Width));
        }
    }
}