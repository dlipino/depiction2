﻿using System.IO;
using System.Xml;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.GdalExtension.Terrain;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story14IO.Story14.DmlLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class TerrainDpn14LoadTests : LegacyFileLoaderBase
    {
        [SetUp]
        public void Setup()
        {
            DepictionAccess.PathService = new DepictionPathService("Test");
            DepictionAccess.PathService.ClearLoadFileDirectory();
        }

        [TearDown]
        public void TearDown()
        {
        }

        [Test]
        public void SimpexmlTerrainLoadTest()
        {
            //Test for null
            var elevFile = Dpn14LoadUtilities.GetFullFileName("TestData", "testelev.tiff");
            Assert.IsTrue(File.Exists(elevFile));
            var helperFileDirectory = StorySerializationService.Instance.DataSerializationFolder;
            File.Copy(elevFile, Path.Combine(helperFileDirectory, "testelev.tiff"));
            var xmlString = "<value>" +
                            "<terrainData>" +
                            "<hasData>true</hasData>" +
                            "<elevationFileName>testelev.tiff</elevationFileName>" +
                            "</terrainData>" +
                            "<TerrainDifferences>" +
                            "<terrainDifference>" +
                            "<Point3D xmlns:d1p1=\"http://schemas.datacontract.org/2004/07/System.Windows.Media.Media3D\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                            "<d1p1:_x>337</d1p1:_x>" +
                            "<d1p1:_y>242</d1p1:_y>" +
                            "<d1p1:_z>-62.874164581298828</d1p1:_z>" +
                            "</Point3D>" +
                            "</terrainDifference>" +
                            "</TerrainDifferences>" +
                            "</value>";
            using (var reader = XmlReader.Create(new StringReader(xmlString)))
            {
                reader.Read();
                Assert.AreEqual("value", reader.Name);
                var result = Dml14ToElement.LoadXmlPropertyValue(reader, typeof(RestorableTerrainData));
                Assert.IsTrue(reader.Name.Equals("value") && reader.NodeType.Equals(XmlNodeType.EndElement));
            }
            StorySerializationService.Instance.CleanSerializationFolders();
        }

        [Test]
        public void DoesSmall14ElevationLoad()
        {
            Assert.IsTrue(ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.GdalExtension.dll"), "Could not load gdal extension");
            ExtensionService.Instance.LoadExtensionResourcesDictionariesIntoDepiction();
            var usedTerrainType = typeof(RestorableTerrainData);
            Assert.IsNotNull(DepictionAccess.NameTypeService.GetTypeFromName(usedTerrainType.Name));
            var paths = new DepictionPathService("Test");
            DepictionAccess.PathService = paths;
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction143SeattleElevation.dpn");
            //Copies the needed file
            var images = Depiction14ImageLoader.GetDepictionImageNamesFromDPNFile(fullName);
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(1, newElements.Count);
            var terrainElem = newElements[0];
            Assert.AreEqual(ElementVisualSetting.Image, terrainElem.VisualState);

            var prop = terrainElem.GetDepictionProperty("Terrain");
            Assert.IsNotNull(prop);
            Assert.AreEqual(typeof(RestorableTerrainData), prop.ValueType);
            ExtensionService.Instance.Decompose();
            DepictionAccess.NameTypeService.ClearTypeService();
            paths.CleanAllPaths();
            StorySerializationService.Instance.CleanSerializationFolders();
        }
    }
}