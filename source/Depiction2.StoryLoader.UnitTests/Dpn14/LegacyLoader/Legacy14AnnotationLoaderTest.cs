﻿using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14AnnotationLoaderTest : LegacyFileLoaderBase
    {
        [Test]
        public void DoesGetAnnotationObjectsFromZipFile()
        {
            var fileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved2Annotations.dpn");
            var newElements = Depiction14AnnotationLoader.GetAnnotationsFromDPNFile(fileName);
            //There are 2 annotations in that test file
            Assert.AreEqual(2, newElements.Count);
        }
    }
}