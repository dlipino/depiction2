﻿using System.Linq;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class Legacy14InteractionRuleLoaderTest : LegacyFileLoaderBase
    {

        [Test]
        public void DoesGetInteractionRuleObjectsFromZipFile()
        {
            var fullFileName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142Saved2Images.dpn");
            var newInteractionRules = Depiction14InteractionRuleLoader.GetInteractionsFromDPNFile(fullFileName);
            //There are 2 annotations in that test file
            Assert.AreEqual(14, newInteractionRules.Count());
        }
    }
}