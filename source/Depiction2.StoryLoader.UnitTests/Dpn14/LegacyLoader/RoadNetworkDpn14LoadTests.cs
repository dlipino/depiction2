﻿using System.Collections.Generic;
using System.Linq;
using Depiction2.API;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Entities.Element.ViewModel;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using NUnit.Framework;

namespace Depiction2.Story14IO.UnitTests.Dpn14.LegacyLoader
{
    [TestFixture]
    public class RoadNetworkDpn14LoadTests : LegacyFileLoaderBase
    {

        [Test]
        public void DoesSmall14RoadNetworkNotLoadAndGiveError()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14SmallRoadNetwork.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(0, newElements.Count);
        }
        [Test]
        [Ignore("Geometries are tested, but a geometry extension is not getting loaded.")]
        public void DoRouteAndRoadNetworkElementsLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14RouteAndRoadNetwork.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //route, roadnetwork, 4 waypoints,route start, route end, car == 9 - roadnetwork
            //roadnetwork is not read anymore.
            Assert.AreEqual(8, newElements.Count);
            var pCount = 0;
            var vms = new List<MapElementViewModel>();
            foreach (var element in newElements)
            {
                if (element.ElementGeometry.GeometryType.Equals(DepictionGeometryType.Point))
                {
                    Assert.IsFalse(string.IsNullOrEmpty(element.IconResourceName));
                    vms.Add(new MapElementViewModel(element));
                    pCount++;
                }
            }
            Assert.AreEqual(7, pCount);

            var idForRoute = "c3408fd1-4168-43e5-bac9-26febe061f89";
            var routeElements = newElements.Where(t => t.ElementId.Equals(idForRoute)).ToList();
            Assert.AreEqual(1, routeElements.Count());
            var routeElement = routeElements.First();
            Assert.AreEqual(6, routeElement.AssociatedElements.Count());
        }

        [Test]
        public void DoRouteWayPointsLoadCorrectlyFrom14Dpn()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14RouteAndRoadNetwork.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //route, roadnetwork, 4 waypoints,route start, route end, car == 9
            Assert.AreEqual(8, newElements.Count);
            var wayPoints = Dpn14LoadUtilities.FindAllElementsByFullType(newElements, "waypoint").ToList();

            Assert.AreEqual(4, wayPoints.Count);
            foreach (var waypoint in wayPoints)
            {
                Assert.AreEqual(ElementVisualSetting.Icon, waypoint.VisualState);
                Assert.IsFalse(string.IsNullOrEmpty(waypoint.ElementId));
                Assert.IsTrue(waypoint.IsDraggable, "Draggable not set correctly");
                Assert.AreEqual("Depiction.RouteWayPoint", waypoint.IconResourceName, "Icon name for waypoint is incorrect");
            }

            var endPoint = Dpn14LoadUtilities.FindAllElementsByFullType(newElements, "end").ToList();
            Assert.AreEqual(1, endPoint.Count);
            Assert.AreEqual("Depiction.RouteEnd", endPoint[0].IconResourceName, "Incorrect icon for route end");


            var startPoint = Dpn14LoadUtilities.FindAllElementsByFullType(newElements, "start").ToList();
            Assert.AreEqual(1, startPoint.Count);
            Assert.AreEqual("Depiction.RouteStart", startPoint[0].IconResourceName, "Incorrect icon for route start");

            var idForRoute = "c3408fd1-4168-43e5-bac9-26febe061f89";
            var routeElements = newElements.Where(t => t.ElementId.Equals(idForRoute)).ToList();
            Assert.AreEqual(1, routeElements.Count());
            var routeElement = routeElements.First();
            Assert.AreEqual(6, routeElement.AssociatedElements.Count());
        }


        [Test]
        [Ignore]
        public void DoesStoryWithLargeRoadNetworkLoad()
        {
            //            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14SeattleRoadNetwork.dpn");
            //            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //            Assert.AreEqual(1, newElements.Count);
            //
            //            var roadGraphElement = newElements[0];
            //            Assert.IsNotNull(roadGraphElement);
            //            //            Assert.AreNotEqual(0, roadGraphElement.DefaultElementPropertyNames.Count);//TODO make it work
            //
            //            var roadGraphProp = roadGraphElement.GetProperty("RoadGraph");
            //            Assert.IsNotNull(roadGraphProp);
            //
            //            var roadGraphData = roadGraphProp.Value as RoadGraph;
            //            Assert.IsNotNull(roadGraphData);
            //
            //            var geom = roadGraphElement.ElementGeometry;
            //            Assert.IsNotNull(geom);
            //            Assert.AreEqual(DepictionGeometryType.MultiLineString, geom.GeometryType);

        }
        [Test]
        [Ignore]
        public void DoesDpnWithRoadNetworkRouteAndExplosionLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14RouteExplosion.dpn");
            var elementScaffolds = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //route, start,end 4 way points, explosion,roadnetwork
            Assert.AreEqual(9, elementScaffolds.Count);
            var scaffold = elementScaffolds.First();
        }
        [Test]
        [Ignore]
        public void DoesStoryWithSmallRoadNetworkLoad()
        {
            //            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction14SmallRoadNetwork.dpn");
            //            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            //            Assert.AreEqual(1, newElements.Count);
            //
            //            var roadGraphElement = newElements[0];
            //            Assert.IsNotNull(roadGraphElement);
            //            Assert.IsNotNull(roadGraphElement.ElementGeometry);
            //            Assert.AreNotEqual(Rect.Empty, roadGraphElement.ElementGeometry.Bounds);
            //            //            Assert.AreNotEqual(0, roadGraphElement.DefaultElementPropertyNames.Count);//TODO put it back
            //
            //            var roadGraphProp = roadGraphElement.GetProperty("RoadGraph");
            //            Assert.NotNull(roadGraphProp);
            //
            //            var roadGraphData = roadGraphProp.Value as RoadGraph;
            //            Assert.IsNotNull(roadGraphData);
            //            var edges = roadGraphData.Graph.Edges.Where(n => n.Name.Equals("16th Avenue")).ToList();
            //
            //            Assert.AreEqual(2, edges.Count());//Well i guess it has two, for bidirectional status
            //            //             <RoadSegment name="16th Avenue" maxspeed="" highway="residential">
            //            //                  <RoadNode Location="47.6016905,-122.3115555" NodeID="53079292" />
            //            //                  <RoadNode Location="47.6026938,-122.3115471" NodeID="53079293" />
            //            //                </RoadSegment>
            //            var edge = edges.First();
            //            var expSource = new Point(-122.3115555, 47.6016905);
            //            var expSId = 53079292;
            //            var source = edge.Source;
            //
            //            Assert.AreEqual(expSId, source.NodeID);
            //            Assert.AreEqual(expSource, source.Vertex);
            //
            //            var expTarget = new Point(-122.3115471, 47.6026938);
            //            var expTId = 53079293;
            //            var target = edge.Target;
            //            Assert.AreEqual(expTId, target.NodeID);
            //            Assert.AreEqual(expTarget, target.Vertex);
        }
    }
}