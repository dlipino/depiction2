﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Depiction2.TempCore.ShapeModels;
using Depiction2.TempCore.ShapeView;
using Depiction2.Utilities.Drawing;

namespace ZoomableControlTake2
{
    public class WorldOutlineQuad : FrameworkElement
    {
        static Pen drawPen = new Pen(Brushes.Black, 1);

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource",
                typeof(ShapeDataGridItemsSource),
                typeof(WorldOutlineQuad),
                new PropertyMetadata(OnItemsSourceChanged));

        public static readonly DependencyProperty BackgroundProperty =
            Panel.BackgroundProperty.AddOwner(typeof(WorldOutlineQuad));

        public ShapeDataGridItemsSource ItemsSource
        {
            set { SetValue(ItemsSourceProperty, value); }
            get { return (ShapeDataGridItemsSource)GetValue(ItemsSourceProperty); }
        }

        static void OnItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            (obj as WorldOutlineQuad).OnItemsSourceChanged(args);
        }

        void OnItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue != null)
            {
                var oldVal = args.OldValue;
                if (oldVal is ShapeDataGridItemsSource)
                {
                    ((ShapeDataGridItemsSource)oldVal).CollectionChanged -= OnCollectionChanged;
                }
            }

            if (args.NewValue != null)
            {
                var oldVal = args.NewValue;
                if (oldVal is ShapeDataGridItemsSource)
                {
                    ((ShapeDataGridItemsSource)oldVal).CollectionChanged += OnCollectionChanged;
                }
            }

            InvalidateVisual();
        }



        #region constructor
        public WorldOutlineQuad()
        {
            drawPen.Freeze();
            this.CacheMode = new BitmapCache();
        }
        #endregion

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(Brushes.White, null, new Rect(RenderSize));
            if (ItemsSource == null)
            {
                dc.DrawRectangle(Brushes.Beige, drawPen, new Rect(10, 10, 100, 300));

                return;
            }
            //            drawPen = new Pen(Brushes.Black, 1000);
            //            drawPen.Freeze();

//            foreach (var dataPoint in ItemsSource)
//            {
//                dc.DrawGeometry(Brushes.Black, drawPen, dataPoint);
//            }
        }
    }
}
