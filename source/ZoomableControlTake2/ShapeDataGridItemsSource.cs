﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempCore.ShapeView;
using ZoomableControlTake2.SpatialIndexing;

namespace ZoomableControlTake2
{
    public class ShapeDataGridItemsSource : RangeObservableCollection<ShapeDataViewModel>, ZoomableCanvas.ISpatialItemsSource
    {
        private QuadTree quadTree; 
        public event EventHandler ExtentChanged;
        public event EventHandler QueryInvalidated;
        private void UpdateQuad()
        {
            ExtentChanged(this, EventArgs.Empty);
            QueryInvalidated(this, EventArgs.Empty);
        }
        public Rect Extent
        {
            get
            {
                var result = new Rect();
                var rect = new Rect();
                foreach (var item in this)
                {
                    rect.X = item.OffsetX;
                    rect.Y = item.OffsetY;
                    rect.Size = item.ShapeGeometry.Bounds.Size;
                    result.Union(rect);
                }
                return result;
            }
        }

        public IEnumerable<int> Query(Rect rectangle)
        {
            if(quadTree == null) return new List<int>();

            var val = quadTree.Search(rectangle);
            //sorted works, val does not
            var sorted = from a in val orderby a select a;
            return sorted;


//            //The bruteforce way, no quads involved, help?
//            var list = new List<int>();
//            var rect = new Rect();
//            for(int i = 0;i<Count;i++)
//            {
//                rect.X = this[i].OffsetX;
//                rect.Y = this[i].OffsetY;
//                rect.Size = this[i].ShapeGeometry.Bounds.Size;
//                if (rectangle.Intersects(rect))
//                {
//                    list.Add(i);
//                    // yield return i;
//                }
//                   
//            }
//            return list;
        }

        public void UpdateList(List<ShapeDataViewModel> geoms)
        {
           
            AddRange(geoms);
            if(quadTree != null) quadTree.Dispose();//Just for fun
            quadTree = SpatialIndexingSpecials.CreateSpatialIndexRecursive(this);
            UpdateQuad();
        }

        public void ClearList()
        {
            Clear();
            UpdateQuad();
        }

    }
}
