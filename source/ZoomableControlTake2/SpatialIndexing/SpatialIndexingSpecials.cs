﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using Depiction2.TempCore.ShapeView;

namespace ZoomableControlTake2.SpatialIndexing
{
    public static class SpatialIndexingSpecials
    {

        /// <summary>
        /// Generates a spatial index for a specified shape file.
        /// </summary>
        /// <param name="filename">The filename</param>
        public static QuadTree CreateSpatialIndexRecursive(IList<ShapeDataViewModel> elements)
        {
            var sw = new Stopwatch();
            sw.Start();

            var objList = new List<QuadTree.BoxObjects>();
            //Convert all the geometries to boundingboxes 
            var i = 0;
            foreach (var box in GetAllFeatureBoundingBoxes(elements))
            {

                var g = new QuadTree.BoxObjects { Box = box, ID = i };//ID = elements[i].ID
                objList.Add(g);
                i++;
            }

            Heuristic heur;
            heur.maxdepth = (int)Math.Ceiling(Math.Log(elements.Count, 2));
            heur.minerror = 10;
            heur.tartricnt = 5;
            heur.mintricnt = 2;
            var root = new QuadTree(objList, 0, heur);

            sw.Stop();
            Debug.WriteLine("Linear creation of QuadTree took {0}ms", sw.ElapsedMilliseconds);

            //if (_fileBasedIndexWanted && !String.IsNullOrEmpty(filename))
            //    root.SaveIndex(filename + ".sidx");

            return root;
        }

        private static IEnumerable<Rect> GetAllFeatureBoundingBoxes(IList<ShapeDataViewModel> elements)
        {
            foreach (var element in elements)
            {
                yield return element.ShapeGeometry.Bounds;

            }


        }
    }
}

//if (_shapeType == ShapeType.Point)
//{
//    for (int a = 0; a < _featureCount; ++a)
//    {
//        //if (recDel((uint)a)) continue;

//        _fsShapeFile.Seek(_offsetOfRecord[a] + 8, 0); //skip record number and content length
//        if ((ShapeType)_brShapeFile.ReadInt32() != ShapeType.Null)
//        {
//            yield return new Envelope(new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()));
//        }
//    }
//}
//else
//{
//    for (int a = 0; a < _featureCount; ++a)
//    {
//        //if (recDel((uint)a)) continue;
//        _fsShapeFile.Seek(_offsetOfRecord[a] + 8, 0); //skip record number and content length
//        if ((ShapeType)_brShapeFile.ReadInt32() != ShapeType.Null)
//            yield return new Envelope(new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()),
//                                      new Coordinate(_brShapeFile.ReadDouble(), _brShapeFile.ReadDouble()));
//        //boxes.Add(new BoundingBox(brShapeFile.ReadDouble(), brShapeFile.ReadDouble(),
//        //                          brShapeFile.ReadDouble(), brShapeFile.ReadDouble()));
//    }
//}   