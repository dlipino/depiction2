﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
//using Depiction2.TempCore.ShapeView;
using ZoomableControlTake2.SpatialIndexing;

namespace ZoomableControlTake2
{
    /// <summary>
    /// The fast is a misnomer, since i don't really know how to control the draw. Ideally 
    /// the draw would not use any of the templates but just draw using the onrender. But that doesnt
    /// seem to be happening.
    /// </summary>
    public partial class FastDrawItemsControl
    {
        public Point MousePosition
        {
            get { return (Point)GetValue(MousePositionProperty); }
            set { SetValue(MousePositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MousePosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MousePositionProperty =
            DependencyProperty.Register("MousePosition", typeof(Point), typeof(FastDrawItemsControl), new UIPropertyMetadata(new Point()));

        

        public ZoomableCanvas ZoomableCanvas;
//        private ShapeDataGridItemsSource DataItems;
//        private TempSpatialItemsSource<ShapeDataViewModel> DataItems;

        static Pen drawPen = new Pen(Brushes.Black, 1);
        #region

        public FastDrawItemsControl()
        {
            InitializeComponent();
//            CacheMode = new BitmapCache();
            ClipToBounds = true;
//            DataItems = new TempSpatialItemsSource<ShapeDataViewModel>();
//            ItemsSource = DataItems;
        }

        #endregion

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
        #region mouse wheel overrides



        Point LastMousePosition;
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            var scalePos = (Vector) position/ZoomableCanvas.Scale;
            MousePosition = (Point)((Vector)ZoomableCanvas.Offset/ZoomableCanvas.Scale + scalePos);
//            MousePosition = (Point)((Vector)(ZoomableCanvas.Offset + (Vector)position) / ZoomableCanvas.Scale);
            if (e.LeftButton == MouseButtonState.Pressed
                && !(e.OriginalSource is Thumb)) // Don't block the scrollbars.
            {
                CaptureMouse();
                ZoomableCanvas.Offset -= position - LastMousePosition;
                e.Handled = true;
            }
            else
            {
                ReleaseMouseCapture();
            }
            LastMousePosition = position;
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            var x = Math.Pow(2, e.Delta / 3.0 / Mouse.MouseWheelDeltaForOneLine);
            ZoomableCanvas.Scale *= x;

            // Adjust the offset to make the point under the mouse stay still.
            var position = (Vector)e.GetPosition(this);
            ZoomableCanvas.Offset = (Point)((Vector)
                (ZoomableCanvas.Offset + position) * x - position);

            e.Handled = true;
        }
        #endregion
        #region public helpers
        
//        public void AddGeometries(List<ShapeDataViewModel> geometries)
//        {
//            DataItems.AddRange(geometries);
//            var extent = DataItems.Extent;
//            var center = extent.GetCenter();
//            ZoomableCanvas.Offset = center;
//
////            DataItems.UpdateList(geometries);
//        }
//
//        public void ClearGeometries()
//        {
//            DataItems.Reset();
//        }
        #endregion

//        protected override void OnRender(DrawingContext drawingContext)
//        {
//            if(ItemsSource is ShapeDataGridItemsSource )
//            {
//                foreach (var dataPoint in ItemsSource as ShapeDataGridItemsSource)
//                {
//                    drawingContext.DrawGeometry(Brushes.Black, drawPen, dataPoint.ShapeGeometry);
//                }
//            }else
//            {
//
//                 base.OnRender(drawingContext);
//                
//            }
//        }
    }
}
