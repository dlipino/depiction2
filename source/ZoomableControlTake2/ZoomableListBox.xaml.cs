﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.TempCore.ShapeView;
using ZoomableControlTake2.SpatialIndexing;

namespace ZoomableControlTake2
{
    /// <summary>
    /// Interaction logic for ZoomableListBox.xaml
    /// </summary>
    public partial class ZoomableListBox
    {
        public ZoomableCanvas ZoomableCanvas;
        private TempSpatialItemsSource<ShapeDataViewModel> DataItems;
//        private ShapeDataGridItemsSource DataItems;
//        private GridLikeItemsSource DataItems;

        #region
        
        public ZoomableListBox()
        {
            InitializeComponent();
            DataItems = new TempSpatialItemsSource<ShapeDataViewModel>();
//            DataItems = new GridLikeItemsSource();
            ItemsSource = DataItems;
        }

        #endregion

        private void ZoomableCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            // Store the canvas in a local variable since x:Name doesn't work.
            //but ItemsPanel grabbing should work 
            ZoomableCanvas = (ZoomableCanvas)sender;

            // Set the canvas as the DataContext so our overlays can bind to it.
            DataContext = ZoomableCanvas;
        }
        #region mouse wheel overrides



        Point LastMousePosition;
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            if (e.LeftButton == MouseButtonState.Pressed
                && !(e.OriginalSource is Thumb)) // Don't block the scrollbars.
            {
                CaptureMouse();
                ZoomableCanvas.Offset -= position - LastMousePosition;
                e.Handled = true;
            }
            else
            {
                ReleaseMouseCapture();
            }
            LastMousePosition = position;
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            var x = Math.Pow(2, e.Delta / 3.0 / Mouse.MouseWheelDeltaForOneLine);
            ZoomableCanvas.Scale *= x;

            // Adjust the offset to make the point under the mouse stay still.
            var position = (Vector)e.GetPosition(this);
            ZoomableCanvas.Offset = (Point)((Vector)
                (ZoomableCanvas.Offset + position) * x - position);

            e.Handled = true;
        }
        #endregion
        #region public helpers

//        public void SetCount(int count)
//        {
//            DataItems.Count = count;
//        }

        public void AddGeometries(List<ShapeDataViewModel> geometries)
        {
//            DataItems.UpdateList(geometries);
        }
        #endregion
    }
}
