﻿using System.Linq;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Interactions;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story2IO.FileLoad;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D2FileLoadTests : FileLoaderBase
    {
        private Story2Loader storyLoader;
        private DepictionPathService internalPathService = null;
       
        [SetUp]
        public void Setup()
        {
            storyLoader = new Story2Loader();

            DepictionAccess.TemplateLibrary = new ElementTemplateLibrary();
            DepictionAccess.InteractionLibrary = InteractionRuleRepository.Instance; //Speechless at how bad this is
            internalPathService = new DepictionPathService("D2StoryLoadTests");//Bad
            DepictionAccess.PathService = internalPathService; 

            ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.SimpleRoutes.dll");
            ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.GdalExtension.dll");

            NotificationService.Instance.ErrorLogCount = 0;

            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(ElementTemplate)), "SerializationBinding not set correctly.");
            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DefaultTemplateProperty)), "SerializationBinding not set correctly.");
            
        }

        [TearDown]
        public void TearDown()
        {
            storyLoader.Dispose();
            DepictionAccess.TemplateLibrary = null;
            DepictionAccess.InteractionLibrary = null;
            internalPathService.CleanAllPaths();
            DepictionAccess.PathService = null;
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
            
        }
        [Test]
        [Ignore]
        public void EmptyFileTest()
        {
            var savedStoryFile = StorySaveLoadTestTools.GetFullTestFileName("CleanUserRoute.dn2");
            var story = storyLoader.GetStoryFromFile(savedStoryFile);
        }
        [Test]
        [Ignore("The loading is not complete yet")]
        public void UserRouteLoadAndInteractionSetupTest()
        {
            var savedStoryFile = StorySaveLoadTestTools.GetFullTestFileName("CleanUserRoute.dn2");
            var story = storyLoader.GetStoryFromFile(savedStoryFile);
            Assert.IsNotNull(story);
            Assert.AreEqual(0,NotificationService.Instance.ErrorLogCount);
            Assert.IsNotNull(story.InteractionsRunner);
            var elements = story.AllElements.Where(t => t.GeoLocation != null).ToList();
            Assert.AreEqual(8, elements.Count);
            var routeElement = elements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"));
            Assert.IsNotNull(routeElement);
            Assert.AreEqual(7,routeElement.AssociatedElements.Count());

            //Something funky about this, not sure why the build stuff was there, rather, it was there for 1.4, but 
            //somebody didn't explain the full reason.
//            if (DepictionAccess.InteractionLibrary != null)
//            {
//                story.InteractionRules.LoadDefaultRulesIntoRepository(DepictionAccess.InteractionLibrary.AllInteractionRules);
//            }
//            if (story.InteractionsRunner != null)
//            {
//                story.InteractionsRunner.BeginAsync();
//            }
//
//            Assert.AreNotEqual(0, story.InteractionsRunner.RulePairs);
        }
    }
}