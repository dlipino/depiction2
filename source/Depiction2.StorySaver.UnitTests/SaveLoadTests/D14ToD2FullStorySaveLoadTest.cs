﻿using System.IO;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2FullStorySaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        private string D2TestPath = "D2Test";
        private DepictionPathService internalPathService = null;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();
            DepictionAccess.TemplateLibrary = new ElementTemplateLibrary();
            internalPathService = new DepictionPathService(D2TestPath);
            DepictionAccess.PathService = internalPathService;

            //Done in the story load process
//            TypeService.Instance.LoadCompleteDictionary();
//            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);
//            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DepictionElement)), "SerializationBinding not set correctly.");
//            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(ElementProperty)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            internalPathService.CleanAllPaths();
            DepictionAccess.TemplateLibrary = null;
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
        }
        [Test]
        public void WrongStoryTypeLoadTest()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("Depiction14Elements3Revealers.dpn");
            Assert.IsNull(storyLoader.GetStoryFromFile(test14Dpn));
        }
        [Test]
        public void D2StorySave()
        {
            var story = new Story(null);
            story.StoryDetails = new StoryDetails{Author = "Something",Title = "tester"};
            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            storySaver.SaveStoryToTempFolderFileAndZip(story, pathService.FolderName, zipFile);
           
            var loadedStory = storyLoader.GetStoryFromFile(zipFile);
            Assert.IsNotNull(loadedStory);

        }
        [Test]
        public void FullStoryRevealersSaveTest()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("Depiction14Elements3Revealers.dpn");
            var fullLoader = new Depiction14StoryLoader();
            var loaded14Story = fullLoader.GetStoryFromFile(test14Dpn);
            Assert.IsNotNull(loaded14Story);
            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            storySaver.SaveStoryToTempFolderFileAndZip(loaded14Story, pathService.FolderName, zipFile);
            var loadedStory = storyLoader.GetStoryFromFile(zipFile);
            Assert.IsNotNull(loadedStory);
        }

        [Test]
        public void FullStoryAnnotationsSaveTest()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("2Annotations.dpn");
            var fullLoader = new Depiction14StoryLoader();
            var loaded14Story = fullLoader.GetStoryFromFile(test14Dpn);
            Assert.IsNotNull(loaded14Story);
            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            storySaver.SaveStoryToTempFolderFileAndZip(loaded14Story, pathService.FolderName,zipFile);
            var loadedStory = storyLoader.GetStoryFromFile(zipFile);
            Assert.IsNotNull(loadedStory);
        }

        [Test]
        public void FullStoryImagesSaveTest()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("Depiction143ThreeImagesdpn.dpn");
            var fullLoader = new Depiction14StoryLoader();
            var loaded14Story = fullLoader.GetStoryFromFile(test14Dpn);
            Assert.IsNotNull(loaded14Story);
            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            storySaver.SaveStoryToTempFolderFileAndZip(loaded14Story,pathService.FolderName, zipFile);
            var loadedStory = storyLoader.GetStoryFromFile(zipFile);
            Assert.IsNotNull(loadedStory);
        }
    }
}