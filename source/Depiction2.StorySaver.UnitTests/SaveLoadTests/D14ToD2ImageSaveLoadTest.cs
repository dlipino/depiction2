﻿using System;
using System.Collections.Generic;
using System.IO;
using Depiction2.API.Service;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2ImageSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();
            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DepictionElement)), "SerializationBinding not set correctly.");
            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(ElementProperty)), "SerializationBinding not set correctly.");

            StorySerializationService.Instance.ResetSerializationFolder();
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();

            StorySerializationService.Instance.CleanSerializationFolders();
        }

        [Test]
        public void ImageResourceFrom14DpnTest()
        {
            var storyFile = StorySaveLoadTestTools.GetFullTestFileName("Depiction143ThreeImagesdpn.dpn");
            var images = Depiction14ImageLoader.GetDepictionImageNamesFromDPNFile(storyFile);
            var zipName = Path.Combine(pathService.FolderName, "Stored.zip");
            storySaver.ImageDictionarySave(pathService.FolderName, images, null, zipName);

            var loadedImages = storyLoader.GetImagesFromZip(zipName);
            Assert.AreEqual(images.Count, loadedImages.Count);
            foreach(var entry in images.Keys)
            {
                Assert.IsTrue(loadedImages.Contains(entry));
            }
        }
    }
}