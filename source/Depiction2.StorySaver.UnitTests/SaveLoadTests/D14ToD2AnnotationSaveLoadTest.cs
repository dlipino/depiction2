﻿using System.IO;
using System.Linq;
using Depiction2.API.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Story14IO.Story14;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2AnnotationSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        private Depiction14StoryLoader oldLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();
            oldLoader = new Depiction14StoryLoader();
            ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.Core.dll");

            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DepictionAnnotation)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            oldLoader.Dispose();
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
        }

        [Test]
        public void LoadAnnotationTest()
        {
            var annotationDpnFile = StorySaveLoadTestTools.GetFullTestFileName("2Annotations.dpn");
            var oldStory = oldLoader.GetStoryFromFile(annotationDpnFile);
            Assert.IsNotNull(oldStory);
            var zName = Path.Combine(pathService.FolderName, "Zip.zip");

            storySaver.AnnotationSave(pathService.FolderName, oldStory, zName);
            var loadedAnnots = storyLoader.GetAnnotationsFromZipFile(zName);
            Assert.IsNotNull(loadedAnnots);
            var origAnnotList = oldStory.AllAnnotations.ToList();
            Assert.AreEqual(origAnnotList.Count, loadedAnnots.Count);
            for (int i = 0; i < origAnnotList.Count; i++)
            {
                var exp = origAnnotList[i];
                var res = loadedAnnots[i];
                Assert.AreEqual(exp, res);
            }
        }
    }
}