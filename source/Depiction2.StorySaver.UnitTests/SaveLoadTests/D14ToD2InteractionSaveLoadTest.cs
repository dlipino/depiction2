﻿using System.Collections.Generic;
using System.IO;
using Depiction2.API.Service;
using Depiction2.Base.Interactions;
using Depiction2.Core.Interactions;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2InteractionSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();
            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(InteractionRule)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
        }

        [Test]
        public void FullInteractionRepositorySaveLoadTest()
        {
            var interactionFileName = StorySaveLoadTestTools.GetFullTestFileName("DepictionInteractionRules.xml");
            var interRules = Depiction14InteractionRuleLoader.GetInteractionRepoFromFile(interactionFileName);
            Assert.IsNotNull(interRules);
            
            var interactionRuleFile = storySaver.InteractionRepositorySave(pathService.FolderName, interRules);
            List<IInteractionRule> rules = null;
            using (var s = new FileStream(interactionRuleFile, FileMode.Open))
            {
                rules = storyLoader.GetRepositoryFromStream(s);
            }
            Assert.IsNotNull(rules);

            Assert.AreEqual(interRules.Count,rules.Count);
            for(int i =0;i<interRules.Count;i++)
            {
                var exp = interRules[i];
                var res = rules[i];
                exp.RuleEquals(res);
//                Assert.AreEqual(exp.Publisher,res.Publisher);
//                Assert.AreEqual(exp.SubscriberTriggerPropertyNames, res.SubscriberTriggerPropertyNames);
//                Assert.AreEqual(exp.PublisherTriggerPropertyNames,res.PublisherTriggerPropertyNames);
            }
        }
    }
}