﻿using Depiction2.API.Service;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class FileLoaderBase
    {
        [SetUp]
        virtual protected void BaseSetup()
        {
            StorySerializationService.Instance.ResetSerializationFolder();
        }

        [TearDown]
        virtual protected void BaseTearDown()
        {
            StorySerializationService.Instance.CleanSerializationFolders();
        }


    }
}