﻿using System.IO;
using System.Linq;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2TemplateSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();

            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(ElementTemplate)), "SerializationBinding not set correctly.");
            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DefaultTemplateProperty)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
        }

        [Test]
        public void ScaffoldLibrarySaveTest()
        {
            var annotationDpnFile = StorySaveLoadTestTools.GetFullTestFileName("Depiction14Elements3Revealers.dpn");
            var loadedScaffolds = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(annotationDpnFile);
            Assert.IsNotNull(loadedScaffolds);
            var zipName = Path.Combine(pathService.FolderName, "Stored.zip");
            storySaver.StoryScaffoldSave(pathService.FolderName, loadedScaffolds, null, zipName);

            var scaffolds = storyLoader.ScaffoldsFromStoryZip(zipName);

        }
        [Test]
        public void SingleScaffoldSaveTest()
        {
            var annotationDpnFile = StorySaveLoadTestTools.GetFullTestFileName("Depiction14Elements3Revealers.dpn");
            var loadedScaffolds = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(annotationDpnFile);
            Assert.IsNotNull(loadedScaffolds);
            Assert.IsTrue(loadedScaffolds.Any());
            var first = loadedScaffolds[0];
            foreach(var prop in first.Properties)
            {
                var propString = JsonUtilities.Instance.GetJsonObjectString(prop,null,null);
                var rprop = JsonUtilities.Instance.GetTypeJsonString<PropertyTemplate>(propString,null,null);
            }
            var scaffoldString = JsonUtilities.Instance.GetJsonObjectString(first, null, null);
            var rscaf = JsonUtilities.Instance.GetTypeJsonString<ElementTemplate>(scaffoldString, null, null);

        }


    }
}