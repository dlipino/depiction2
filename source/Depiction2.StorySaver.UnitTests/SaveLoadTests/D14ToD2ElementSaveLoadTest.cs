﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2ElementSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();

            StorySerializationService.Instance.ResetSerializationFolder();
            DepictionAccess.TemplateLibrary = new ElementTemplateLibrary();

            ExtensionService.Instance.LoadSpecificExtensionsFromBaseDirectory("Depiction2.Core.dll");
            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DepictionElement)), "SerializationBinding not set correctly.");
            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(ElementProperty)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            DepictionAccess.TemplateLibrary = null;
            
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
            StorySerializationService.Instance.CleanSerializationFolders();
        }
        [Test]
        public void PartialLoadSaveTestFrom14File()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("Depiction143ThreeImagesdpn.dpn");
            var fullLoader = new Depiction14StoryLoader();
            var loaded14Story = fullLoader.GetStoryFromFile(test14Dpn);
            Assert.IsNotNull(loaded14Story);
            //            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            //            storySaver.SaveStoryToZipFile(loaded14Story, zipFile);

            foreach (var elem in loaded14Story.AllElements)
            {
                var es = JsonUtilities.Instance.GetJsonObjectString(elem, null, null);
                var res = JsonUtilities.Instance.GetTypeJsonString<DepictionElement>(es, null,null);
            }

            //            var results = storyLoader.StoryElementLoadFromZip(zipFile, Path.Combine(SaveLoadUtilities.ElementsDir, SaveLoadUtilities.GeolocatedElements), binder);
            //            Assert.IsNotNull(results);
        }
        [Test]
        public void FullLoadSaveTestFrom14File()
        {
            var test14Dpn = StorySaveLoadTestTools.GetFullTestFileName("Depiction143ThreeImagesdpn.dpn");
            var fullLoader = new Depiction14StoryLoader();
            var loaded14Story = fullLoader.GetStoryFromFile(test14Dpn);
            Assert.IsNotNull(loaded14Story);
            var zipFile = Path.Combine(pathService.FolderName, "Zipity.zip");
            storySaver.SaveStoryToTempFolderFileAndZip(loaded14Story,pathService.FolderName, zipFile);
            var results = storyLoader.StoryElementLoadFromZip(zipFile, Path.Combine(JsonUtilities.ElementsDir, JsonUtilities.GeolocatedElements));
            Assert.IsNotNull(results);
        }
        [Test]
        public void SaveSimpleSingleElement()
        {
            IElement elem = new DepictionElement("Generic");
            elem.UpdatePrimaryPointAndGeometry(new Point(2, 3), null);

            var l = new List<IElement> { elem };
            string json = JsonUtilities.Instance.GetJsonObjectString(l, null, null);

            var obj = JsonUtilities.Instance.GetTypeJsonString<List<IElement>>(json, null, null);
            var first = obj.First();

            Assert.AreEqual(elem.ElementType, first.ElementType);
            Assert.AreEqual(elem.ElementId, first.ElementId);
            Assert.AreEqual(elem.GeoLocation, first.GeoLocation);
        }
        [Test]
        public void RoundTripSimpleElementSaveTest()
        {
            var origElements = new List<IElement>();

            var story = new Story(null);
            var elem = new DepictionElement("Generic");
            elem.SetGeoLocationFromString("2,3");
            origElements.Add(elem);
            story.AddElements(origElements);
            var sFolder = pathService.FolderName;
            storySaver.StoryElementsSave(sFolder, story);
            var zipName = "Zipper.zip";
            var fullZipName = Path.Combine(sFolder, zipName);
            storySaver.ZipFolder(sFolder, fullZipName);

            Assert.IsTrue(File.Exists(fullZipName));

            var results = storyLoader.StoryElementLoadFromZip(fullZipName, Path.Combine(JsonUtilities.ElementsDir, JsonUtilities.GeolocatedElements));

            Assert.IsNotNull(results);
            Assert.AreEqual(origElements.Count, results.Count);
            for (var i = 0; i < origElements.Count; i++)
            {
                //                Assert.AreEqual(origElements[i], results[i]);
                Assert.AreEqual(origElements[i].ElementType, results[i].ElementType);
                Assert.AreEqual(origElements[i].ElementId, results[i].ElementId);
                Assert.AreEqual(origElements[i].GeoLocation, results[i].GeoLocation);
            }
        }
    }
}