﻿using System.IO;
using System.Linq;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.Story14IO.Story14;
using Depiction2.Story2IO.Converters;
using Depiction2.Story2IO.FileLoad;
using Depiction2.Story2IO.FileSave;
using NUnit.Framework;
using Newtonsoft.Json;

namespace Depiction2.Story2IO.UnitTests.SaveLoadTests
{
    [TestFixture]
    public class D14ToD2RegionsSaveLoadTest : FileLoaderBase
    {
        private DepictionFolderService pathService;
        private Story2Saver storySaver;
        private Story2Loader storyLoader;
        private Depiction14StoryLoader oldLoader;
        [SetUp]
        public void Setup()
        {
            pathService = new DepictionFolderService(true);
            storySaver = new Story2Saver();
            storyLoader = new Story2Loader();
            oldLoader = new Depiction14StoryLoader();

            StorySerializationService.Instance.ResetSerializationFolder();
            TypeService.Instance.LoadCompleteDictionary();
            JsonUtilities.Instance.UpdateTypeBinder(TypeService.Instance.NameTypeDictonary);

            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DepictionElement)), "SerializationBinding not set correctly.");
            Assert.IsTrue(JsonUtilities.Instance.HasType(typeof(DisplayerViewType)), "SerializationBinding not set correctly.");
        }

        [TearDown]
        public void TearDown()
        {
            pathService.Dispose();
            storySaver.Dispose();
            storyLoader.Dispose();
            oldLoader.Dispose();
            JsonUtilities.Instance.ResetInstance();
            TypeService.Instance.ClearTypeService();
            ExtensionService.Instance.Decompose();
            StorySerializationService.Instance.CleanSerializationFolders();
        }

        [Test]
        public void SaveLoadRegionsTest()
        {
            var annotationDpnFile = StorySaveLoadTestTools.GetFullTestFileName("Depiction14Elements3Revealers.dpn");
            var oldStory = oldLoader.GetStoryFromFile(annotationDpnFile);
            Assert.IsNotNull(oldStory);
            var zName = Path.Combine(pathService.FolderName, "Zip.zip");

            storySaver.SaveStoryRegions(pathService.FolderName, oldStory, zName);

            var main = storyLoader.LoadMainDisplayer(zName);
            Assert.AreEqual(oldStory.MainDisplayer, main);
            var revealers = storyLoader.LoadRevealers(zName);
            Assert.AreEqual(oldStory.RevealerDisplayers.Count, revealers.Count);
            for (int i = 0; i < oldStory.RevealerDisplayers.Count; i++)
            {
                Assert.AreEqual(oldStory.RevealerDisplayers[i], revealers[i]);
            }
            var regions = storyLoader.LoadRegions(zName);
            Assert.AreEqual(1, regions.Count);
            Assert.AreEqual(oldStory.AllRegions.First(), regions[0]);


        }
        [Test]
        public void RevealerPropertySerializationTest()
        {
            //            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            //            var enumConverter = new Newtonsoft.Json.Converters.StringEnumConverter();
            //            jsonFormatter.SerializerSettings.Converters.Add(enumConverter);
            //            JsonSerializerSettings.DefaultEnumSerializationHandling = EnumSerializationHandling.Value;
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            };
            settings.Converters.Add(new RevealerPropertyConverter());
            var prop = new RevealerProperty("dav", 34);
            var json = JsonConvert.SerializeObject(prop, settings);
            var rej = JsonConvert.DeserializeObject<RevealerProperty>(json, settings);
            Assert.IsNotNull(rej);
            Assert.AreEqual(prop.Value, rej.Value);

            var propEnum = new RevealerProperty("dav", SaveEnums.Loaded);
            json = JsonConvert.SerializeObject(propEnum, settings);
            rej = JsonConvert.DeserializeObject<RevealerProperty>(json, settings);
            Assert.IsNotNull(rej);
            Assert.AreEqual(propEnum.Value, rej.Value);

            //            var lProp = JsonConvert.DeserializeObject(json, settings);

            //            var thing = new List<dynamic> { SaveEnums.Loaded };
            //            var j = JsonConvert.SerializeObject(thing, settings);
            //            var rj = JsonConvert.DeserializeObject<List<dynamic>>(j, settings);
            //            Assert.AreEqual(thing,rj);
            //            dynamic test = new ExpandoObject();
            //            test.x = "xvalue";
            //            test.y = DateTime.Now;
            //            test.z = SaveEnums.Loaded;
            //            var j = JsonConvert.SerializeObject(test,settings);
            //            var jo = JsonConvert.DeserializeObject<ExpandoObject>(j, settings);
        }

    }
}