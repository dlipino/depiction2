﻿using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.JsonSaveLoadTests
{
    [TestFixture]
    public class DisplayerSaveLoadTests
    {
        private TypeNameDictionarySerializationBinder binder;
        [SetUp]
        public void Setup()
        {
            binder = new TypeNameDictionarySerializationBinder();
            binder.TypeDictionary.Add(typeof(CartRect).Name, typeof(CartRect));
        }
        [TearDown]
        public void TearDown()
        {
            binder.TypeDictionary.Clear();
            binder = null;
        }

        [Test]
        public void MainDisplayerSaveLoadTest()
        {
            var tl = new Point(-2, 10);
            var br = new Point(6, -5);
            var displayer = new ElementDisplayer();
            displayer.SetBounds(tl,br);
            var json = JsonUtilities.Instance.GetJsonObjectString(displayer, binder, null);
            var jsonVal = JsonUtilities.Instance.GetTypeJsonString<ElementDisplayer>(json, binder, null);
            Assert.AreEqual(displayer.GeoRectBounds,jsonVal.GeoRectBounds);
        }

        [Test]
        public void DepictionRegionSaveLoadTest()
        {
            var tl = new Point(-2, 10);
            var br = new Point(6, -5);
            var displayer = new DepictionRegion(tl, br);
            var json = JsonUtilities.Instance.GetJsonObjectString(displayer, binder, null);
            var jsonVal = JsonUtilities.Instance.GetTypeJsonString<DepictionRegion>(json, binder, null);
            Assert.AreEqual(displayer.GeoRectBounds, jsonVal.GeoRectBounds);
        }
    }
}