﻿using Depiction2.API.Service;
using Depiction2.Core.Entities.ElementTemplate;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.JsonSaveLoadTests
{
    [TestFixture]
    public class ElementTemplateSaveLoadTests
    {
        private TypeNameDictionarySerializationBinder serialBinder;
        
        [SetUp]
        public void Setup()
        {
            TearDown();//just in case i kill a test early
            serialBinder = new TypeNameDictionarySerializationBinder(TypeService.Instance.NameTypeDictonary);
        }

        [TearDown]
        public void TearDown()
        {
            TypeService.Instance.ClearTypeService();
            serialBinder = null;
        }

        [Test]
        public void ElementTemplateSaveLoad()
        {
            var element = new ElementTemplate("Gemeroc");
            var jsonString = JsonUtilities.Instance.GetJsonObjectString(element, serialBinder, null);
            var jsonRes = JsonUtilities.Instance.GetTypeJsonString<ElementTemplate>(jsonString, serialBinder, null);
            Assert.IsFalse(ReferenceEquals(element, jsonRes));
            Assert.AreEqual(element.HideFromLibrary, jsonRes.HideFromLibrary);
            Assert.AreEqual(element.IsMouseAddable, jsonRes.IsMouseAddable);

        }
    }
}