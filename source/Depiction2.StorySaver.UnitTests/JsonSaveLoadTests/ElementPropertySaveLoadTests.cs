﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.StoryEntities;
using System.Data;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.JsonSaveLoadTests
{
    [TestFixture]
    public class ElementPropertySaveLoadTests
    {
        private TypeNameDictionarySerializationBinder serialBinder;
        [SetUp]
        public void Setup()
        {
            TearDown();//just in case i kill a test early
            serialBinder = new TypeNameDictionarySerializationBinder(TypeService.Instance.NameTypeDictonary);
        }
        [TearDown]
        public void TearDown()
        {
            TypeService.Instance.ClearTypeService();
            serialBinder = null;
        }
        internal class MockValue
        {
            protected List<string> Names { get; set; }
            protected string Val { get; set; }
            public MockValue()
            {
                Names = new List<string>{"the","end","is","near"};
                Val = "now";
            }
            public override bool Equals(object obj)
            {
                var other = obj as MockValue;
                if (other == null) return false;
                if (other.Val != Val) return false;
                if (other.Names.Count != Names.Count) return false;
                for (int i = 0; i < Names.Count;i++)
                {
                    if (other.Names[i] != Names[i]) return false;
                }

                return true;
            }
        }
        internal List<IElementProperty> CreateTestProperties(TypeNameDictionarySerializationBinder typeBinder)
        {
            IElementProperty first = new ElementProperty("First", "a");
            IElementProperty second = new ElementProperty("Second", "b", 2d);
            IElementProperty third = new ElementProperty("Third", "c", "happey");
            var classValue = new MockValue();
            serialBinder.TypeDictionary.Add(typeof(MockValue).Name, typeof(MockValue));
            IElementProperty fourth = new ElementProperty("Fourth", "d", classValue);
//            IElementProperty fifth = new ElementProperty("Fourth", "d", ElementVisualSetting.Image);//doesnt work so well
            var list = new List<IElementProperty> {first, second, third, fourth};
            return list;
        }
        [Test]
        public void SingleElementPropertySaveTest()
        {
            var propList = CreateTestProperties(serialBinder);
            foreach(var prop in propList)
            {
                var jsonString = JsonUtilities.Instance.GetJsonObjectString(prop, serialBinder, null);
                var jsonRes = JsonUtilities.Instance.GetTypeJsonString<ElementProperty>(jsonString, serialBinder, null);
                Assert.IsFalse(ReferenceEquals(prop, jsonRes));
                Assert.AreEqual(prop.Value, jsonRes.Value);
                Assert.AreEqual(prop.ValueType, jsonRes.ValueType);
            }
        }
    }
}