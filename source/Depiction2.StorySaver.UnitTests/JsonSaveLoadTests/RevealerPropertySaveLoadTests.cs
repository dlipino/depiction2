﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.StoryEntities;
using System.Data;
using NUnit.Framework;

namespace Depiction2.Story2IO.UnitTests.JsonSaveLoadTests
{
    [TestFixture]
    public class RevealerPropertySaveLoadTests
    {
        private TypeNameDictionarySerializationBinder serialBinder;
        [SetUp]
        public void Setup()
        {
            TearDown();//just in case i kill a test early
            serialBinder = new TypeNameDictionarySerializationBinder(TypeService.Instance.NameTypeDictonary);
        }
        [TearDown]
        public void TearDown()
        {
            serialBinder = null;
        }
        internal class MockValue
        {
            protected List<string> Names { get; set; }
            protected string Val { get; set; }
            public MockValue()
            {
                Names = new List<string>{"the","end","is","near"};
                Val = "now";
            }
            public override bool Equals(object obj)
            {
                var other = obj as MockValue;
                if (other == null) return false;
                if (other.Val != Val) return false;
                if (other.Names.Count != Names.Count) return false;
                for (int i = 0; i < Names.Count;i++)
                {
                    if (other.Names[i] != Names[i]) return false;
                }

                return true;
            }
        }
        internal List<IRevealerProperty> CreateTestProperties(TypeNameDictionarySerializationBinder typeBinder)
        {
            IRevealerProperty first = new RevealerProperty("First", "a");
            IRevealerProperty second = new RevealerProperty("Second", 2d);
            IRevealerProperty third = new RevealerProperty("Third", DisplayerViewType.Circle);
            var classValue = new MockValue();
            serialBinder.TypeDictionary.Add(typeof(MockValue).Name, typeof(MockValue));
            IRevealerProperty fourth = new RevealerProperty("Fourth", classValue);
//            IElementProperty fifth = new ElementProperty("Fourth", "d", ElementVisualSetting.Image);//doesnt work so well
            var list = new List<IRevealerProperty> { first, second, third, fourth };
            return list;
        }
        [Test]
        [Ignore("This is a known fail, Json.net serializer doesn't like enums as objects")]
        public void SingleElementPropertySaveTest()
        {
            var propList = CreateTestProperties(serialBinder);
            foreach(var prop in propList)
            {
                var jsonString = JsonUtilities.Instance.GetJsonObjectString(prop, serialBinder, null);
                var jsonRes = JsonUtilities.Instance.GetTypeJsonString<RevealerProperty>(jsonString, serialBinder, null);
                Assert.IsFalse(ReferenceEquals(prop, jsonRes));
                Assert.AreEqual(prop.Value, jsonRes.Value);
                Assert.AreEqual(prop.PropertyType, jsonRes.PropertyType);
            }
        }
    }
}