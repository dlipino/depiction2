﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Measurement;
using Depiction2.API.Service;
using Depiction2.API.ValidationRules;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story2IO.Converters;
using NUnit.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Depiction2.Story2IO.UnitTests
{
    [TestFixture]
    public class RandomSaveTests
    {
        private SerializationBinder serialBinder;

        [SetUp]
        public void Setup()
        {
            serialBinder = new TypeNameDictionarySerializationBinder(TypeService.Instance.NameTypeDictonary);
        }

        [TearDown]
        public void TearDown()
        {
            TypeService.Instance.ClearTypeService();
        }

        public string JsonSave(object toSave)
        {
            string json =
                JsonConvert.SerializeObject(
                    toSave,
                    Formatting.Indented,
                    new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), TypeNameHandling = TypeNameHandling.Objects });
            return json;
        }
        private interface IHolder
        {
            double Id { get; }
        }
        private class Holder:IHolder
        {
            public double Id { get; set; }
            public ElementVisualSetting First { get; set; }
            public Holder()
            {
                First = ElementVisualSetting.None;
            }

           
        }
        private class SimpleHolder:IHolder
        {
            public double Id { get; private set; }
            public SimpleHolder()
            {
                Id = 4;
            }
        }
        [Test]
        [Ignore]
        public void OjbectSaveLoadTest()
        {
            SerializationBinder tb = serialBinder;
            var tdsb = tb as TypeNameDictionarySerializationBinder;
            if(tdsb != null)
            {
                tdsb.TypeDictionary.Add(typeof(Holder).Name,typeof(Holder));
            }
            var item = new Holder();
            var jsonString = JsonUtilities.Instance.GetJsonObjectString(item, tb, null);
            var jsonRes = JsonUtilities.Instance.GetObjectJsonString(jsonString, tb, null);
            Assert.IsFalse(ReferenceEquals(item,jsonRes));
            Assert.AreEqual(typeof(Holder), jsonRes.GetType()); 
            var newItem = jsonRes as Holder;
            Assert.AreEqual(ElementVisualSetting.None, newItem.First);
        }
        [Test]
        [Ignore]
        public void OjbectListSaveLoadTest()
        {
            SerializationBinder tb = serialBinder;
            var tdsb = tb as TypeNameDictionarySerializationBinder;
            if (tdsb != null)
            {
                tdsb.TypeDictionary.Add(typeof(Holder).Name, typeof(Holder));
                tdsb.TypeDictionary.Add(typeof(SimpleHolder).Name, typeof(SimpleHolder));
            }
            var item = new List<IHolder> {new Holder(), new SimpleHolder()};
            var jsonString = JsonUtilities.Instance.GetJsonObjectString(item, tb, null);
            var jsonRes = JsonUtilities.Instance.GetTypeJsonString<List<IHolder>>(jsonString, tb, null);
            Assert.IsFalse(ReferenceEquals(item, jsonRes));
            
            Assert.AreEqual(typeof(List<IHolder>), jsonRes.GetType());
     //       var newItem = jsonRes as IHolder;
//            Assert.AreEqual(ElementVisualSetting.None, newItem.First);
        }
        [Test]
        [Ignore]
        public void SingeTypeSaveLoadTest()
        {
            
            SerializationBinder tb = serialBinder;

            dynamic first = 2d;
            var  jsonString = JsonUtilities.GetJsonObjectString(first, tb, null);
            var jsonRes = JsonUtilities.GetObjectJsonString(jsonString, tb, null);
            Assert.AreEqual(typeof(double),jsonRes.GetType());
            object second = "2.0";
            jsonString = JsonUtilities.Instance.GetJsonObjectString(second, tb, null);
            jsonRes = JsonUtilities.GetObjectJsonString(jsonString, tb, null);
            Assert.AreEqual(typeof(string), jsonRes.GetType());
            object third = ElementVisualSetting.Image;
//            Console.WriteLine(third.GetType());
            jsonString = JsonUtilities.Instance.GetJsonObjectString(third, tb, null);
            jsonRes = JsonUtilities.GetObjectJsonString(jsonString, tb, null);
            Assert.AreEqual(typeof(ElementVisualSetting), jsonRes.GetType());
        }

        [Test]
        [Ignore]
        public void StorySaveTest()
        {
            IStory s = new Story(null);
            s.AdjustZoom(1.3, new Point(4, 5));
            var saved = JsonConvert.SerializeObject(s);

            var l = JsonConvert.DeserializeObject<Story>(saved);
        }
        [Test]
        [Ignore]
        public void MeasurementSerialization()
        {
            var angle = new Angle { Value = 57.3 };
            var area = new Area(MeasurementSystem.Imperial, MeasurementScale.Large, 47);
            var distance = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, 3.5);
            var speed = new Speed(MeasurementSystem.Imperial, MeasurementScale.Small, 31.5);
            var temp = new Temperature(MeasurementSystem.Imperial, MeasurementScale.Large, .4);
            var volume = new Volume(MeasurementSystem.Imperial, MeasurementScale.Small, 3.5);
            var weight = new Weight(MeasurementSystem.Imperial, MeasurementScale.Normal, 3.5);
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            var saved = JsonConvert.SerializeObject(area, settings);
            var ra = JsonConvert.DeserializeObject<Area>(saved);
            Assert.AreEqual(area.InitialScale, ra.InitialScale);
            Assert.AreEqual(area.InitialSystem, ra.InitialSystem);

            saved = JsonConvert.SerializeObject(distance);
            var rd = JsonConvert.DeserializeObject<Distance>(saved);
            Assert.AreEqual(distance.InitialScale, rd.InitialScale);
            Assert.AreEqual(distance.InitialSystem, rd.InitialSystem);

            var l = new List<object> { angle, area, distance, speed, temp, volume, weight };

            saved = JsonConvert.SerializeObject(l, settings);

            var rl = JsonConvert.DeserializeObject<List<object>>(saved, settings);

        }
        [Test]
        public void ElementPropertyValueSerializationTest()
        {
            var area = new Area(MeasurementSystem.Imperial, MeasurementScale.Large, 12);
            var pA = new ElementProperty("A", "A", area);

            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            settings.Converters.Add(new ElementPropertyConverter());
            var saved = JsonConvert.SerializeObject(pA, Formatting.Indented, settings);
            var ra = JsonConvert.DeserializeObject<ElementProperty>(saved, settings);
            Assert.AreEqual(area, ra.Value);

            var pO = new ElementProperty("A", "A", "thing");
            saved = JsonConvert.SerializeObject(pO, Formatting.Indented, settings);
            var rO = JsonConvert.DeserializeObject<ElementProperty>(saved, settings);
            Assert.AreEqual("thing", rO.Value);

            var pE = new ElementProperty("A", "A", SaveEnums.Toast);
            saved = JsonConvert.SerializeObject(pE, Formatting.Indented, settings);
            var rE = JsonConvert.DeserializeObject<ElementProperty>(saved, settings);
            Assert.AreEqual(SaveEnums.Toast, rE.Value);

            var pI = new ElementProperty("A", "A", 2.6);
            saved = JsonConvert.SerializeObject(pI, Formatting.Indented, settings);
            var rI = JsonConvert.DeserializeObject<ElementProperty>(saved, settings);
            Assert.AreEqual(2.6, rI.Value);
            //            var distance = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, 7.2);
            //            var pD = new ElementProperty("B", "B", distance);
        }
        [Test]
        [Ignore]
        public void AnonymousTypeSaveLoadTest()
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            var things = new { num = 1, st = "name", en = SaveEnums.Saved };
            var json = JsonConvert.SerializeObject(things, settings);
            dynamic lThings = JsonConvert.DeserializeObject(json);
            var myObjects = JsonConvert.DeserializeAnonymousType(json, things);

            var enu = lThings.en;
            dynamic stuff = JsonConvert.DeserializeObject("{ 'Name': 'Jon Smith', 'Address': { 'City': 'New York', 'State': 'NY' }, 'Age': 42 }");

            string name = stuff.Name;
            string address = stuff.Address.City;
        }

        [Test]
        [Ignore]
        public void MinValidationRuleSerializationTest()
        {
            var speed = new Speed(MeasurementSystem.Imperial, MeasurementScale.Small, 31.5);
            var rule = new MinValueValidationRule(speed);
            var area = new Area(MeasurementSystem.Imperial, MeasurementScale.Large, 31.5);
            var rule1 = new MinValueValidationRule(area);
            var all = new List<IValidationRule> { rule, rule1 };
            var binder = new TypeNameDictionarySerializationBinder(new Dictionary<string, Type>());
            binder.TypeDictionary.Add(typeof(MinValueValidationRule).Name, typeof(MinValueValidationRule));
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                Binder = binder
            };
            var s = JsonConvert.SerializeObject(all, settings);
            var r = JsonConvert.DeserializeObject<List<IValidationRule>>(s, settings);
        }
        [Test]
        [Ignore]
        public void InterfaceSaveTest()
        {
            var saved = new SimpleSave { Name = "one" };
            var other = new OtherSave { Name = "two" };
            var saveList = new List<ISimpleSave> { saved, other };
            //            var s = JsonSave(saved);
            //            var o = JsonSave(other);
            //            var so = JsonSave(saveList);
            //var loaded = JsonConvert.DeserializeObject<ISimpleSave>(s);//fails
            //            var loaded = JsonConvert.DeserializeObject(s);
            //            var conv = new SaveConverter();
            //            var sos = JsonConvert.DeserializeObject<List<ISimpleSave>>(so);
            //            var rs1 = JsonConvert.DeserializeObject(s);
            //            var rs = JsonConvert.DeserializeObject(s, saved.GetType(), conv);
            //            var ro = JsonConvert.DeserializeObject(o, other.GetType(), conv);
            var binder = new TypeNameSerializationBinder(string.Empty);//"ConsoleApplication.{0}, ConsoleApplication");

            string json = JsonConvert.SerializeObject(saveList, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Binder = binder
            });
            var obj = JsonConvert.DeserializeObject<List<ISimpleSave>>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Binder = binder
            });

        }
        [Test]
        [Ignore]
        public void DictTest()
        {
            var actions = new Dictionary<string, string[]>();
            var a = JsonConvert.SerializeObject(actions);

            var d = new Dictionary<string, List<string>>();
            Point? p = new Point(1, 3);
            var s = JsonConvert.SerializeObject(p);
            var so = JsonConvert.DeserializeObject<Point?>(s);
        }
    }


    //    string json =
    //  JsonConvert.SerializeObject(
    //    product,
    //    Formatting.Indented,
    //    new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }
    //  );
}