﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using NUnit.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Depiction2.Story2IO.UnitTests
{
    public class StorySaveLoadTestTools
    {
        static internal string GetFullTestFileName(string fileName)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = "TestFiles";
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            Assert.IsTrue(File.Exists(fullFileName),string.Format("Cannot find specified file {0}",fileName));
            return fullFileName;
        }

        public IStory CreateEmptyStory()
        {
            return new Story(null);
        }
    }

    public interface ISimpleSave
    {
        string Name { get; set; }
    }
    public class SimpleSave : ISimpleSave
    {
        public string Name { get; set; }
        public string NonInterface { get; set; }
    }
    public class OtherSave : ISimpleSave
    {
        public string Name { get; set; }
        public string Friend { get; set; }
    }
    public class TypeNameSerializationBinder : SerializationBinder
    {
        public string TypeFormat { get; private set; }

        public TypeNameSerializationBinder(string typeFormat)
        {
            TypeFormat = typeFormat;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }

        public override Type BindToType(string assemblyName, string typeName)
        {
            string resolvedTypeName = string.Format(TypeFormat, typeName);
            if (typeName.Equals("SimpleSave")) return typeof (SimpleSave);
            if (typeName.Equals("OtherSave")) return typeof (OtherSave);
            return null;
//            return Type.GetType(resolvedTypeName, true);
        }
    }
    public enum SaveEnums
    {
        Saved,Loaded,Toast
    }
    public class SaveConverter : CustomCreationConverter<ISimpleSave>
    {
        public override ISimpleSave Create(Type objectType)
        {
            if (objectType == typeof(SimpleSave))
            {
                return new SimpleSave();
            }
            if (objectType == typeof(OtherSave))
            {
                return new OtherSave();
            }
            return null;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            base.WriteJson(writer, value, serializer);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return base.ReadJson(reader, objectType, existingValue, serializer);
        }

//                public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
//        {
//            var mappedObj = new Mapped();
//            //get an array of the object's props so I can check if the JSON prop s/b mapped to it
//            var objProps = objectType.GetProperties().Select(p => p.Name.ToLower()).ToArray();
// 
//            //loop through my JSON string
//            while (reader.Read())
//            {
//                //if I'm at a property...
//                if (reader.TokenType == JsonToken.PropertyName)
//                {
//                    //convert the property to lower case
//                    string readerValue = reader.Value.ToString().ToLower();
//                    if (reader.Read())  //read in the prop value
//                    {
//                        //is this a mapped prop?
//                        if (objProps.Contains(readerValue))
//                        {
//                            //get the property info and set the Mapped object's property value
//                            PropertyInfo pi = mappedObj.GetType().GetProperty(readerValue, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
//                            var convertedValue = Convert.ChangeType(reader.Value, pi.PropertyType);
//                            pi.SetValue(mappedObj, convertedValue, null);
//                        }
//                        else
//                        {
//                            //otherwise, stuff it into the Dictionary
//                            mappedObj.TheRest.Add(readerValue, reader.Value);
//                        }
//                    }
//                }
//            }
//            return mappedObj;
//        }
//    }
    }


}