﻿using System.Windows;
using System.Windows.Media;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities.Drawing;
using OSGeo.OGR;
using Geometry = OSGeo.OGR.Geometry;

namespace Depiction2.GeoWrapper
{
    public class GeometryWrapHelpers
    {
        #region stream geometry help
        static public StreamGeometry CreateDrawStreamGeometryFromGeometryWrapList(IDepictionGeometry geometryWrapper)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (geometryWrapper == null || geometryWrapper.GeometryObject == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            //Hack only works for gdal stuff
            GeometryOntoDrawStreamGeometry((Geometry)(geometryWrapper.GeometryObject), context);

            context.Close();
            streamGeometry.Freeze();

            return streamGeometry;
        }

        internal static void GeometryOntoDrawStreamGeometry(Geometry mainGeom, StreamGeometryContext streamContext)
        {
            //http://www.gdal.org/ogr/classOGRGeometry.html

            var pointCount = mainGeom.GetPointCount();
            var geomCount = mainGeom.GetGeometryCount();

            var geomType = mainGeom.GetGeometryType();
            var isClosed = false;
            var isFilled = false;//this will be more interesting because of shapes that my or may not be inside
            if (pointCount != 0)
            {
                if (pointCount == 1)
                {

                }
                else
                {
                    if (mainGeom.GetX(0).Equals(mainGeom.GetX(pointCount - 1)) &&
                        mainGeom.GetY(0).Equals(mainGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        isClosed = true;
                        isFilled = true;
                    }
                    else
                    {
                    }
                }
                for (int j = 0; j < pointCount; j++)
                {
                    var p = new Point(mainGeom.GetX(j), -mainGeom.GetY(j));
                    if (j == 0)
                    {
                        streamContext.BeginFigure(p, isFilled, isClosed);
                    }
                    else
                    {
                        streamContext.LineTo(p, true, true);
                    }
                }
            }

            if (geomType == wkbGeometryType.wkbMultiPolygon || geomType == wkbGeometryType.wkbMultiLineString)
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GeometryOntoDrawStreamGeometry(mainGeom.GetGeometryRef(i), streamContext);
                    }
                }
            }
            else
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GeometryOntoDrawStreamGeometry(mainGeom.GetGeometryRef(i), streamContext);
                    }
                }
            }
        }

        #endregion

        #region point list help

        static public EnhancedPointListWithChildren CreateEnhancedListWithChildrenFromGeometryWrap(IDepictionGeometry geometryWrapper)
        {
            var pointList = new EnhancedPointListWithChildren();
            if (geometryWrapper == null || geometryWrapper.GeometryObject == null)
            {
                return pointList;
            }

            //Hack only works for gdal stuff
            GeometryOntoEnhancedPointList((Geometry)(geometryWrapper.GeometryObject), pointList);

            return pointList;
        }

        static private void GeometryOntoEnhancedPointList(Geometry mainGeom, EnhancedPointListWithChildren pointList)
        {
            var pointCount = mainGeom.GetPointCount();
            var geomCount = mainGeom.GetGeometryCount();

            var geomType = mainGeom.GetGeometryType();
            var isClosed = false;
            var isFilled = false;//this will be more interesting because of shapes that my or may not be inside
            if (pointCount != 0)
            {
                if (pointCount == 1)
                {

                }
                else
                {
                    if (mainGeom.GetX(0).Equals(mainGeom.GetX(pointCount - 1)) &&
                        mainGeom.GetY(0).Equals(mainGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        isClosed = true;
                        isFilled = true;
                    }
                    else
                    {
                    }
                }
                if(pointList.MainCount==0)
                {
                    pointList.IsClosed = isClosed;pointList.IsFilled = isFilled;
                    for (int j = 0; j < pointCount; j++)
                    {
                        var p = new Point(mainGeom.GetX(j), mainGeom.GetY(j));
                        pointList.Add(p);
                    }
                }else
                {
                    var simpleList = new EnhancedPointList() { IsClosed = isClosed, IsFilled = isFilled };
                    for (int j = 0; j < pointCount; j++)
                    {
                        var p = new Point(mainGeom.GetX(j), mainGeom.GetY(j));
                        simpleList.Add(p);
                    }
                    pointList.AddChild(simpleList);
                }
             
            }

            if (geomType == wkbGeometryType.wkbMultiPolygon || geomType == wkbGeometryType.wkbMultiLineString)
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GeometryOntoEnhancedPointList(mainGeom.GetGeometryRef(i), pointList);
                    }
                }
            }
            else
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GeometryOntoEnhancedPointList(mainGeom.GetGeometryRef(i), pointList);
                    }
                }
            }
        }

        #endregion



        //                internal static void GetMultiGeom(Geometry multiGeom)
        //                {
        //                    var pointCount = multiGeom.GetPointCount();
        //                    var geomCount = multiGeom.GetGeometryCount();
        //        
        //                    var geomType = multiGeom.GetGeometryType();
        //        
        //                    if (pointCount != 0)
        //                    {
        //                        if (parent.MainCount != 0)
        //                        {
        //                            child = new EnhancedPointList();
        //                        }
        //                        if (pointCount == 1)
        //                        {
        //                            listType = PointListType.Points;
        //                        }
        //                        else
        //                        {
        //                            if (multiGeom.GetX(0).Equals(multiGeom.GetX(pointCount - 1)) &&
        //                                multiGeom.GetY(0).Equals(multiGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
        //                            {
        //                            }
        //                            else
        //                            {
        //                              
        //                            }
        //                        }
        //                        for (int j = 0; j < pointCount; j++)
        //                        {
        //                            if (child != null)
        //                            {
        //                                child.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));
        //                            }
        //                            else
        //                            {
        //                                parent.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));
        //                            }
        //                        }
        //        
        //                        if (child != null)
        //                        {
        //                            child.IsClosed = isClosed;
        //                            child.IsFilled = isFilled;
        //                            child.TypeOfPointList = listType;
        //                        }
        //                        else
        //                        {
        //                            parent.IsClosed = isClosed;
        //                            parent.IsFilled = isFilled;
        //                            parent.TypeOfPointList = listType;
        //                        }
        //        
        //                    }
        //        
        //                    if (parent == null)
        //                    {
        //                        parent = new EnhancedPointListWithChildren();
        //                    }
        //        
        //                    if (geomCount != 0)
        //                    {
        //                        for (int i = 0; i < geomCount; i++)
        //                        {
        //                            GetMultiGeom(multiGeom.GetGeometryRef(i), ref parent);
        //                        }
        //                    }
        //                    if (child != null) parent.AddChild(child);
        //                }

        /*
         * 
         * 
        
         * 
         * 
         
        static private void DrawGeometryWrapOntoStreamGeometry(StreamGeometryContext streamContext, GeometryWrap enhancedPoints)
        {
            if (enhancedPoints == null) return;
            bool start = false;
            var isClosed = enhancedPoints.IsClosed;
            var isFilled = enhancedPoints.IsFilled;

            foreach (var p in enhancedPoints.Points)
            {
                if (!start)
                {
                    streamContext.BeginFigure(p, isFilled, isClosed);
                    start = true;
                }
                else
                {
                    streamContext.LineTo(p, true, true);
                }
            }
            var childPoints = enhancedPoints.ChildLists;
            if (childPoints == null) return;
            foreach (var child in childPoints)
            {
                bool drawStarted = false;
                var isHoleClosed = child.IsClosed;
                var isHoleFilled = child.IsFilled;

                foreach (var p in child.Points)
                {
                    if (!drawStarted)
                    {
                        streamContext.BeginFigure(p, isHoleFilled, isHoleClosed);
                        drawStarted = true;
                    }
                    else
                    {
                        streamContext.LineTo(p, true, true);
                    }
                }
            }
        }
         * 
         * internal static void GetPointListFromGeom(Geometry mainGeom, ref List<EnhancedPointListWithChildren> allGeometries, EnhancedPointListWithChildren parent)
        {
            var type = mainGeom.GetGeometryType();
            //http://www.gdal.org/ogr/classOGRGeometry.html

            var pointCount = mainGeom.GetPointCount();
            var geomCount = mainGeom.GetGeometryCount();
            EnhancedPointListWithChildren pointListWithChildren = null;

            var geomType = mainGeom.GetGeometryType();
            if (pointCount != 0)
            {
                pointListWithChildren = new EnhancedPointListWithChildren();

                if (pointCount == 1)
                {
                    pointListWithChildren.TypeOfPointList = PointListType.Points;
                }
                else
                {
                    if (mainGeom.GetX(0).Equals(mainGeom.GetX(pointCount - 1)) &&
                        mainGeom.GetY(0).Equals(mainGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        pointListWithChildren.TypeOfPointList = PointListType.Shell;
                        pointListWithChildren.IsClosed = true;
                        pointListWithChildren.IsFilled = true;
                    }
                    else
                    {
                        pointListWithChildren.TypeOfPointList = PointListType.Line;
                        pointListWithChildren.IsClosed = false;
                        pointListWithChildren.IsFilled = false;
                    }
                }
                for (int j = 0; j < pointCount; j++)
                {
                    pointListWithChildren.Add(new Point(mainGeom.GetX(j), mainGeom.GetY(j)));
                }
            }

            if (geomType == wkbGeometryType.wkbMultiPolygon || geomType == wkbGeometryType.wkbMultiLineString)
            {
                GetMultiGeom(mainGeom, ref pointListWithChildren);
            }
            else
            {
                if (geomCount != 0)
                {
                    for (int i = 0; i < geomCount; i++)
                    {
                        GetPointListFromGeom(mainGeom.GetGeometryRef(i), ref allGeometries, parent);
                    }
                }
            }
            if (pointListWithChildren != null) allGeometries.Add(pointListWithChildren);

        }

        internal static void GetMultiGeom(Geometry multiGeom, ref EnhancedPointListWithChildren parent)
        {
            var pointCount = multiGeom.GetPointCount();
            var geomCount = multiGeom.GetGeometryCount();
            EnhancedPointList child = null;
            var isClosed = false;
            var isFilled = false;
            var listType = PointListType.Points;

            var geomType = multiGeom.GetGeometryType();

            if (pointCount != 0)
            {
                if (parent.MainCount != 0)
                {
                    child = new EnhancedPointList();
                }
                if (pointCount == 1)
                {
                    listType = PointListType.Points;
                }
                else
                {
                    if (multiGeom.GetX(0).Equals(multiGeom.GetX(pointCount - 1)) &&
                        multiGeom.GetY(0).Equals(multiGeom.GetY(pointCount - 1)) || geomType == wkbGeometryType.wkbPolygon)
                    {
                        listType = PointListType.Shell;
                        isClosed = true;
                        isFilled = true;
                    }
                    else
                    {
                        listType = PointListType.Line;
                    }
                }
                for (int j = 0; j < pointCount; j++)
                {
                    if (child != null)
                    {
                        child.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));
                    }
                    else
                    {
                        parent.Add(new Point(multiGeom.GetX(j), multiGeom.GetY(j)));
                    }
                }

                if (child != null)
                {
                    child.IsClosed = isClosed;
                    child.IsFilled = isFilled;
                    child.TypeOfPointList = listType;
                }
                else
                {
                    parent.IsClosed = isClosed;
                    parent.IsFilled = isFilled;
                    parent.TypeOfPointList = listType;
                }

            }

            if (parent == null)
            {
                parent = new EnhancedPointListWithChildren();
            }

            if (geomCount != 0)
            {
                for (int i = 0; i < geomCount; i++)
                {
                    GetMultiGeom(multiGeom.GetGeometryRef(i), ref parent);
                }
            }
            if (child != null) parent.AddChild(child);
        }


        static public StreamGeometry CreateStreamGeometryFromEnhancedPointListWithChildren(GeometryWrap enhancedPoints)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (enhancedPoints == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            DrawPointListOntoStreamGeometry(context, enhancedPoints);

            context.Close();
            streamGeometry.Freeze();
            return streamGeometry;
        }

        static public StreamGeometry CreateStreamGeometryFromGeometryWrapList(List<GeometryWrap> zoiPixelPointCollectionList)
        {
            var streamGeometry = new StreamGeometry();
            StreamGeometryContext context = streamGeometry.Open();
            if (zoiPixelPointCollectionList == null)
            {
                context.Close();
                streamGeometry.Freeze();
                return streamGeometry;
            }

            foreach (var pl in zoiPixelPointCollectionList)
            {
                DrawPointListOntoStreamGeometry(context, pl);
            }

            context.Close();
            streamGeometry.Freeze();

            return streamGeometry;
        }
        static private void DrawPointListOntoStreamGeometry(StreamGeometryContext streamContext, GeometryWrap enhancedPoints)
        {
            if (enhancedPoints == null) return;
            bool start = false;
            var isClosed = enhancedPoints.IsClosed;
            var isFilled = enhancedPoints.IsFilled;

            foreach (var p in enhancedPoints.Points)
            {
                if (!start)
                {
                    streamContext.BeginFigure(p, isFilled, isClosed);
                    start = true;
                }
                else
                {
                    streamContext.LineTo(p, true, true);
                }
            }
            var childPoints = enhancedPoints.ChildLists;
            if (childPoints == null) return;
            foreach (var child in childPoints)
            {
                bool drawStarted = false;
                var isHoleClosed = child.IsClosed;
                var isHoleFilled = child.IsFilled;

                foreach (var p in child.Points)
                {
                    if (!drawStarted)
                    {
                        streamContext.BeginFigure(p, isHoleFilled, isHoleClosed);
                        drawStarted = true;
                    }
                    else
                    {
                        streamContext.LineTo(p, true, true);
                    }
                }
            }
        }
        */
    }
}