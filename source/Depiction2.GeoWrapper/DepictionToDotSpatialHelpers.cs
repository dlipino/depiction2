﻿using Depiction2.Base.Measurement;
using DotSpatial.Positioning;

namespace Depiction2.GeoWrapper
{
    public class DepictionToDotSpatialHelpers
    {
        static public DistanceUnit ConvertDepictionSystemAndScaleToGeoFramworkDistanceUnit(MeasurementSystem system, MeasurementScale scale)
        {
            var systemAndScale = (int)system + (int)scale;
            DistanceUnit geoDistanceUnit;
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    geoDistanceUnit = DistanceUnit.Inches;
                    break;
                case MeasurementConstants.ImperialNormal:
                    geoDistanceUnit = DistanceUnit.Feet;
                    break;
                case MeasurementConstants.ImperialLarge:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case MeasurementConstants.MetricSmall:
                    geoDistanceUnit = DistanceUnit.Centimeters;
                    break;
                case MeasurementConstants.MetricNormal:
                    geoDistanceUnit = DistanceUnit.Meters;
                    break;
                case MeasurementConstants.MetricLarge:
                    geoDistanceUnit = DistanceUnit.Kilometers;
                    break;
                default:
                    geoDistanceUnit = DistanceUnit.StatuteMiles;
                    break;
            }
            return geoDistanceUnit;
        }
    }
}