﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities;
using OSGeo.OGR;

namespace Depiction2.GeoWrapper
{
    public class GeometryGdalWrap : IDepictionGeometry
    {
        private Geometry geometryWrap = null;
        private Rect bounds = Rect.Empty;
        public object GeometryObject { get { return geometryWrap; } }
        public IEnumerable<Point> GeometryPoints
        {
            get
            {
                if(geometryWrap == null) return new List<Point>();
                var points = GeometryWrapHelpers.CreateEnhancedListWithChildrenFromGeometryWrap(this);
                return points.Points;
            }
        } 
        public DepictionGeometryType GeometryType
        {
            get
            {
                var type = DepictionGeometryType.Unknown;
                if (geometryWrap == null) return type;
                switch (geometryWrap.GetGeometryType())
                {
                    case wkbGeometryType.wkbPolygon:
                        type = DepictionGeometryType.Polygon;
                        break;
                    case wkbGeometryType.wkbMultiPolygon:
                        type = DepictionGeometryType.MultiPolygon;
                        break;
                    case wkbGeometryType.wkbLineString:
                        type = DepictionGeometryType.LineString;
                        break;
                    case wkbGeometryType.wkbMultiLineString:
                        type = DepictionGeometryType.MultiLineString;
                        break;
                    case wkbGeometryType.wkbPoint:
                    case wkbGeometryType.wkbPoint25D:
                        type = DepictionGeometryType.Point;
                        break;
                    case wkbGeometryType.wkbMultiPoint:
                        type = DepictionGeometryType.MultiPoint;
                        break;
                }
                return type;
            }
        }

        public Rect Bounds
        {
            get
            {
                UpdateBounds();//Hackish
                return bounds;
            }
        }

        public bool IsSimple
        {
            get
            {
                if (geometryWrap == null) return false;
                return geometryWrap.IsSimple();
            }
        }
        public bool IsValid
        {
            get
            {
                if (geometryWrap == null) return false;
                return geometryWrap.IsValid();
            }
        }

        #region constructor/destructor
        public GeometryGdalWrap()
        {
        }
        public GeometryGdalWrap(Point singlePoint)
        {
            geometryWrap = new Geometry(wkbGeometryType.wkbPoint);
            //TODO make a test
            //This makes the type point25d instead of point
            geometryWrap.AddPoint(singlePoint.X, singlePoint.Y, 0);
            UpdateBounds();
        }
        public GeometryGdalWrap(Geometry geo)
        {
            geometryWrap = geo;
            UpdateBounds();
        }

        public GeometryGdalWrap(string wktGeometry)
        {
            SetGeometryFromWkt(wktGeometry);
        }

        public GeometryGdalWrap(IEnumerable<Point> geometryPoints) :this(DepictionWktGeometryUtilities.PointListToSingleWkt(geometryPoints.ToList()))
        {
           
        }

        public void Dispose()
        {
            if (geometryWrap == null) return;
            geometryWrap.Dispose();
        }
        
        #endregion

        private void UpdateBounds()
        {
            if (geometryWrap == null)
            {
                bounds = Rect.Empty;
                return;
            }
            var env = new Envelope();
            geometryWrap.GetEnvelope(env);
            bounds = new Rect(new Point(env.MinX, env.MinY), new Point(env.MaxX, env.MaxY));
        }

        public void SetGeometry(List<Point> geometryPoints)
        {
            var wktString = DepictionWktGeometryUtilities.PointListToSingleWkt(geometryPoints);
            SetGeometryFromWkt(wktString);
        }

        private void SetGeometryFromWkt(string wktGeometry)
        {
            if (string.IsNullOrEmpty(wktGeometry))
            {
                geometryWrap = null;
            }
            else
            {
                geometryWrap = Geometry.CreateFromWkt(wktGeometry);
                //TODO assign spatial reference
//                DepictionAccess.DefaultDepictionGCS
//                geometryWrap.AssignSpatialReference(new SpatialReference());
            }
            UpdateBounds();
        }

        public bool Intersects(IDepictionGeometry otherGeometry)
        {
            var other = otherGeometry.GeometryObject as Geometry;
            if (other == null)
            {
                //well this is not totally true all the time
                return false;
            }

            return geometryWrap.Intersects(other);
        }

        public bool Within(IDepictionGeometry otherGeometry)
        {
            var other = otherGeometry.GeometryObject as Geometry;
            if (other == null)
            {
                //well this is not totally true all the time
                return false;
            }

            return geometryWrap.Within(other);
        }

        public bool Contains(IDepictionGeometry otherGeometry)
        {
            var other = otherGeometry.GeometryObject as Geometry;
            if (other == null)
            {
                //well this is not totally true all the time
                return false;
            }
            return geometryWrap.Contains(other);
        }

        public void ShiftGeometry(Point shiftDistance)
        {
            //for now the dumbway
            ShiftGeometry(geometryWrap, shiftDistance);
        }

        protected void ShiftGeometry(Geometry geom, Point shiftDistance)
        {
            if (geom == null) return;
            var pointCount = geom.GetPointCount();
            var geomCount = geom.GetGeometryCount();

            var geomType = geom.GetGeometryType();
            if (pointCount != 0)
            {

                for (int j = 0; j < pointCount; j++)
                {
                    var x = geom.GetX(j);
                    var y = geom.GetY(j);
                    geom.SetPoint_2D(j, x - shiftDistance.X, y - shiftDistance.Y);
                }
            }
            if (geomCount != 0)
            {
                for (int i = 0; i < geomCount; i++)
                {
                    ShiftGeometry(geom.GetGeometryRef(i), shiftDistance);
                }
            }
        }

        public IDepictionGeometry Clone()
        {
            if (geometryWrap == null) return new GeometryGdalWrap();
            return new GeometryGdalWrap(geometryWrap.Clone());
        }

    }
}