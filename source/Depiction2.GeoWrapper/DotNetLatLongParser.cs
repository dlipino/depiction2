﻿using DotSpatial.Positioning;

namespace Depiction2.GeoWrapper
{
    public class DotNetLatLongParser
    {
         static public bool ParseLatLongString(string inlatlong, out double lat, out double lon)
         {
             try
             {
                 var position = new Position(inlatlong);
                 lat = position.Latitude.DecimalDegrees;
                 lon = position.Longitude.DecimalDegrees;
             }catch
             {
                 lat = double.NaN;
                 lon = double.NaN;
                 return false;
             }

             return true;
         }
    }
}