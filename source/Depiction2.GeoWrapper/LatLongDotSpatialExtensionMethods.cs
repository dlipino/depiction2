﻿using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using DotSpatial.Positioning;

namespace Depiction2.GeoWrapper
{
    static public class LatLongDotSpatialExtensionMethods
    {
        #region translation

        public static Point TranslateTo(this Point originPoint, double degreeDirction, double distance, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            var distanceUnit = DistanceUnit.StatuteMiles;
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    distanceUnit = DistanceUnit.Inches;
                    break;
                case MeasurementConstants.ImperialNormal:
                    distanceUnit = DistanceUnit.Feet;
                    break;
                case MeasurementConstants.ImperialLarge:
                    distanceUnit = DistanceUnit.StatuteMiles;
                    break;
                case MeasurementConstants.MetricSmall:
                    distanceUnit = DistanceUnit.Centimeters;
                    break;
                case MeasurementConstants.MetricNormal:
                    distanceUnit = DistanceUnit.Meters;
                    break;
                case MeasurementConstants.MetricLarge:
                    distanceUnit = DistanceUnit.Kilometers;
                    break;
            }
            return TranslateTo(originPoint, new Angle(degreeDirction), distance, distanceUnit);
        }

        public static Point TranslateTo(Point originPoint, Angle bearing, double distance, DistanceUnit measurementSystem)
        {
            var origPosition = new Position(new Latitude(originPoint.Y), new Longitude(originPoint.X));
            Distance gfDistance = new Distance(distance, measurementSystem);
            var newPos = origPosition.TranslateTo(bearing, gfDistance);
            return new Point(newPos.Longitude.DecimalDegrees, newPos.Latitude.DecimalDegrees);
        }

        #endregion


        public static double DistanceTo(this Point originPoint, Point destPoint, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var origPosition = new Position(new Latitude(originPoint.Y), new Longitude(originPoint.X));
            var destPosition = new Position(new Latitude(destPoint.Y), new Longitude(destPoint.X));
            var distance = origPosition.DistanceTo(destPosition);
            return DistanceConversion(distance, specificMeasurement, specificScale);
        }
        public static Position ConvertToDotSpatialPosition(this LatitudeLongitudeDotSpatial latlon)
        {
            return new Position(new Latitude(latlon.Latitude), new Longitude(latlon.Longitude));
        }

//        public static double DistanceTo(this IDepictionLatitudeLongitude originLatLon, IDepictionLatitudeLongitude destinationLatLon,
//                                   MeasurementSystem specificMeasurement, MeasurementScale specificScale)
//        {
//            var origPosition = new Position(new Latitude(originLatLon.Latitude), new Longitude(originLatLon.Longitude));
//            var destPosition = new Position(new Latitude(destinationLatLon.Latitude), new Longitude(destinationLatLon.Longitude));
//            var distance = origPosition.DistanceTo(destPosition);
//
//            return DistanceConversion(distance, specificMeasurement, specificScale);
//        }
        static private double DistanceConversion(Distance distance, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            var systemAndScale = (int)specificMeasurement + (int)specificScale;
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    return distance.ToInches().Value;
                case MeasurementConstants.ImperialNormal:
                    return distance.ToFeet().Value;
                case MeasurementConstants.ImperialLarge:
                    return distance.ToStatuteMiles().Value;
                case MeasurementConstants.MetricSmall:
                    return distance.ToCentimeters().Value;
                case MeasurementConstants.MetricNormal:
                    return distance.ToMeters().Value;
                case MeasurementConstants.MetricLarge:
                    return distance.ToKilometers().Value;
            }
            return distance.Value;
        }
    }
}