using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.API.Measurement;
using Depiction2.API.Service;
using Depiction2.Base.Measurement;
using Depiction2.Base.Service;
using Depiction2.Base.Utilities;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.CsvExtension.Models;
using Depiction2.CsvExtension.UnitTests.TestHelpers;
using Depiction2.CsvExtension.ViewModels;
using NUnit.Framework;

namespace Depiction2.CsvExtension.UnitTests
{
    [TestFixture]
    public class CSVReadingTests
    {
        private DepictionFolderService tempFolder;
        private string tempFileName;
        private CSVFileReadingService csvReader;
        [SetUp]
        protected void Setup()
        {
            tempFolder = new DepictionFolderService(true);
            DepictionAccess.GeocoderService = new GeocoderService();
            var geoCoder = new TestingGeocoder();
            csvReader = new CSVFileReadingService();
            DepictionAccess.GeocoderService.SetGeocoder(geoCoder);
            tempFileName = tempFolder.GetATempFilenameInDir() + ".csv"; ;
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.GeocoderService = null;
            DepictionAccess.TemplateLibrary = null;
            csvReader = null;
            tempFolder.Close();
        }


        #region helpers

        private IEnumerable<Dictionary<string, object>> WriteAndLoadCSV(string[] lines, string fileName, GeoLocationInfoToPropertyHolder propHolder, CSVLocationModeType locationPropMode)
        {
            File.WriteAllLines(fileName, lines);
            return csvReader.ReadCSVFileAndCreatePrototypes(fileName, propHolder, locationPropMode, false, null, DepictionAccess.GeocoderService).ToList();
        }
        private void CheckPositionData(Dictionary<string, object> dictionaryOfProps, Point? expectedPosition)
        {
            var posKey = PropertyKeys.geoPositionKey;
            Assert.IsTrue(dictionaryOfProps.ContainsKey(posKey));
            var pos = dictionaryOfProps[posKey] as Point?;
            Assert.AreEqual(expectedPosition, pos);
        }
        #endregion
        [Test]
        public void CSVWithalidLatLongInSeperateFieldsGeocodes()
        {
            double latString = 40;
            double longString = 1;
            var expectedLatLong = new Point(longString, latString);

            var lines = new[] { "latitude,longitude", latString + "," + longString };

            var locationPropHolder = new GeoLocationInfoToPropertyHolder();

            locationPropHolder.Latitude = "latitude";
            locationPropHolder.Longitude = "longitude";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;

            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType).ToList();

            Assert.AreEqual(1, prototypes.Count);

            foreach (var prototype in prototypes)
            {
                CheckPositionData(prototype, expectedLatLong);
            }
        }
        [Test]
        public void ReadTestCSVFileWithValidLatLongInSeperateFields()
        {
            var fullFile = TestUtilities.GetFullFileName("Default11CsvTest.csv", "TestFiles", null);
            Assert.IsTrue(File.Exists(fullFile));
            double latString = 1;
            double longString = 1;
            var expectedLatLong = new Point(longString, latString);

            var locationPropHolder = new GeoLocationInfoToPropertyHolder();

            locationPropHolder.Latitude = "latitude";
            locationPropHolder.Longitude = "longitude";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(fullFile, locationPropHolder, locationModeType, false, null, DepictionAccess.GeocoderService).ToList();

            Assert.AreEqual(1, prototypes.Count);
            foreach (var prototype in prototypes)
            {
                CheckPositionData(prototype, expectedLatLong);
            }
        }

        [Test]
        public void DoCreatedTemplateFieldsCreateCorrectElement()
        {
            var fullFile = TestUtilities.GetFullFileName("Default11CsvTest.csv", "TestFiles", null);
            Assert.IsTrue(File.Exists(fullFile));
            double latString = 1;
            double longString = 1;
            var expectedLatLong = new Point(longString, latString);

            var locationPropHolder = new GeoLocationInfoToPropertyHolder();

            locationPropHolder.Latitude = "latitude";
            locationPropHolder.Longitude = "longitude";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(fullFile, locationPropHolder, locationModeType, false, null, DepictionAccess.GeocoderService).ToList();

            Assert.AreEqual(1, prototypes.Count);
            foreach (var prototype in prototypes)
            {
                CheckPositionData(prototype, expectedLatLong);
            }

            var story = new Story(null);
            story.UpdateRepo(prototypes, string.Empty, null);

            Assert.AreEqual(1, story.AllElements.Count(e => e.GeoLocation != null));
        }
        [Test]
        [Ignore]
        public void DoCSVWithOnlyEIDUpdateTheCorrectElement()
        {

            //            var eidValue = 123;
            //            var changePropName = "ChangeProp";
            //            var initChangeVal = "One thing";
            //            var modChangeVal = "Two thing";
            //
            //            var story = new DepictionStory();
            //
            //            var propDict = new Dictionary<string, object> { { "Eid", eidValue }, { changePropName, initChangeVal }, { "Position", new LatitudeLongitude(1, 2) } };
            //
            //            var prototypeWithEid = UnitTestHelperMethods.CreateRawPrototypeWithProps("Circle", propDict);
            //
            //            //            propDict = new Dictionary<string, object> { { "Eid", eidValue }, { changePropName, initChangeVal } };
            //            //            var otherEIDPrototype = UnitTestHelperMethods.CreateRawPrototypeWithProps("PointOfInterest", propDict);
            //            story.CreateOrUpdateDepictionElementListFromPrototypeList(new[] { prototypeWithEid }, false, false);
            //
            //            Assert.AreEqual(1, story.CompleteElementRepository.AllElements.Count);
            //            var circleElement = story.CompleteElementRepository.AllElements[0];
            //
            //            var radiusProp = circleElement.GetPropertyByInternalName("Radius");
            //            Assert.IsNotNull(radiusProp);
            //            var expectedDistance = new Distance(MeasurementSystem.Imperial, MeasurementScale.Normal, 500);
            //            Assert.AreEqual(expectedDistance, radiusProp.Value);
            //
            //            var lines = new[] { "Radius, EID, Name", "1500 feet, 123, Test Circle", "NA, 20110502145801.206.1, EID Test 4" };
            //
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //            var locationModeType = CSVLocationModeType.None;
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, string.Empty);
            //
            //            var csvReader = new CSVFileReadingService();
            //            csvReader.AddReadeCSVPrototypeToDepictionStory(story, prototypes);
            //            Assert.AreEqual(2, story.CompleteElementRepository.AllElements.Count);
            //
            //            var radiusPropMod = circleElement.GetPropertyByInternalName("Radius");
            //            Assert.IsNotNull(radiusPropMod);
            //            var expectedDistance2 = new Distance(MeasurementSystem.Imperial, MeasurementScale.Normal, 1500);
            //            Assert.AreEqual(expectedDistance2, radiusPropMod.Value);
            //            AddinRepository.Decompose();
            //            DepictionAccess.PathService.RemoveInstanceTempFolder();
            //
            //            DepictionAccess.PathService = null;
        }

        [Test]
        [Ignore]
        public void CSVWNoTypeUsesDefaultType()
        {
            //            var dmlsToLoad = new List<string> { "Person.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //            string latString = "40";
            //            string longString = "1";
            //            var expectedLatLong = new LatitudeLongitude(latString, longString);
            //
            //            var lines = new[] { "latitude,longitude", latString + "," + longString };
            //
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //
            //            locationPropHolder.Latitude = "latitude";
            //            locationPropHolder.Longitude = "longitude";
            //            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            //
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, "Person");
            //            Assert.AreEqual(1, prototypes.Count);
            //            foreach (var prototype in prototypes)
            //            {
            //                var latLong = new LatitudeLongitude();
            //                var hasPosition = prototype.GetPropertyValue("Position", out latLong);
            //                Assert.IsTrue(hasPosition);
            //                Assert.IsTrue(latLong.IsValid);
            //                Assert.AreEqual(expectedLatLong, latLong);
            //                Assert.AreEqual("Depiction.Plugin.Person", prototype.ElementType);
            //            }
        }

        [Test]
        [Ignore]
        public void ElementTypeIsUsed()
        {//What the heck is this test supposed to do?!
            //            var dmlsToLoad = new List<string> { "Person.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //            var elementType = "Depiction.Plugin.Person";
            //            var lines = new[] { "ElementType,xy", elementType + ",yy" };
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //
            //            locationPropHolder.Latitude = "latitude";
            //            locationPropHolder.Longitude = "longitude";
            //            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            //
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, string.Empty);
            //            Assert.AreEqual(1, prototypes.Count);
            //            foreach (var prototype in prototypes)
            //            {
            //                // var latLong = new LatitudeLongitude();
            //                // var hasPosition = element.GetPropertyValue("Position", out latLong);
            //                // Assert.IsTrue(hasPosition);
            //                // Assert.IsFalse(latLong.IsValid);
            //                Assert.AreEqual(elementType, prototype.ElementType);
            //
            //            }
        }

        [Test]
        [Ignore]
        public void DoesCircleElementCallZOICreateAction()
        {
            //            var dmlsToLoad = new List<string> { "CircleShape.dml", "PointOfInterest.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //            //            try
            //            //            {
            //            //                Ogr.RegisterAll();
            //            //            }
            //            //            catch (Exception)
            //            //            {
            //            //                // DepictionAccess.NotificationService.SendNotification(string.Format("Failed at \"Ogr.RegisterAll();\". Please try to restart {0}.", UIAccess._productInformation.ProductName));
            //            //            }
            //            var testProduct = new TestProductInformation();
            //            DepictionAccess.ProductInformation = testProduct;//Maybe this should be undone at end of test
            //            DepictionAccess.PathService = new ApplicationPathService(testProduct.DirectoryNameForCompany);//DepictionAccess._productInformation);
            //            AddinRepository.Compose();//It would be cool to have a smaller version of this for tests
            //
            //            var depictionStory = new DepictionStory();
            //            string latString = "40";
            //            string longString = "1";
            //            var expectedLatLong = new LatitudeLongitude(latString, longString);
            //
            //            //            var lines = new[] { "latitude,longitude", latString + "," + longString };
            //            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //            //            locationPropHolder.Latitude = "latitude";
            //            //            locationPropHolder.Longitude = "longitude";
            //            //            var locationModeType = CSVLocationModeType.LatitudeLongitudeSeparateFields;
            //
            //            var lines = new[] { "elementType,position", "circle, \" " + expectedLatLong.ToString() + "\"" };
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //            locationPropHolder.LatitudeLongitude = "position";
            //            var locationModeType = CSVLocationModeType.LatitudeLongitudeSingleField;
            //
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, "Circle");
            //            Assert.AreEqual(1, prototypes.Count);
            //            var csvReader = new CSVFileReadingService();
            //            csvReader.AddReadeCSVPrototypeToDepictionStory(depictionStory, prototypes);
            //            Assert.AreEqual(1, depictionStory.CompleteElementRepository.ElementsGeoLocated.Count);
            //            var circleElement = depictionStory.CompleteElementRepository.ElementsGeoLocated[0];
            //
            //            Assert.AreEqual(DepictionGeometryType.Polygon, circleElement.ZoneOfInfluence.DepictionGeometryType);
            //
            //            AddinRepository.Decompose();
            //            DepictionAccess.PathService.RemoveInstanceTempFolder();
            //
            //            DepictionAccess.PathService = null;
        }

        //Sort of duplicated in 
        [Test]
        [Ignore]
        public void DoPropertyAttributesStayThenSameWhenElementIsUpdatedByCSV()
        {
            //            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //            var locationModeType = CSVLocationModeType.None;
            //
            //            var element = ElementFactory.CreateElementFromTypeString("PointOfInterest");
            //            Assert.IsNotNull(element);
            //            //Remove all hovertext props
            //            foreach(var oprop in element.OrderedCustomProperties)
            //            {
            //                oprop.IsHoverText = false;
            //            }
            //            Assert.AreEqual(0, CountHoverTextProps(element));
            //            var propName = "Random";
            //            var isHoverTextValue = true;
            //            var eid = "123";
            //            var idProp = new DepictionElementProperty("eid", eid);
            //            element.AddPropertyIfItDoesNotExist(idProp, true, false);
            //            var prop = new DepictionElementProperty(propName, "something");
            //            prop.IsHoverText = isHoverTextValue;
            //            element.AddPropertyIfItDoesNotExist(prop, true, false);
            //            element.UseEnhancedPermaText = true;
            //            var propLine = "eid," + propName;
            //            var modValue = "Somethin else";
            //            var propValues = "123," + modValue;
            //            var lines = new[] { propLine, propValues };
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, string.Empty);
            //            var proto = prototypes.First();
            //            Assert.IsNotNull(proto);
            //            ElementFactory.UpdateElementWithPrototypeProperties(element, proto, false, true);
            //
            //            var modProp = element.GetPropertyByInternalName(propName);
            //            Assert.IsNotNull(modProp);
            //            Assert.AreEqual(modValue,modProp.Value);
            //            Assert.IsTrue(modProp.IsHoverText);
        }

        //        static internal int CountHoverTextProps(IDepictionElement element)
        //        {
        //            var count = 0;
        //            foreach(var prop in element.OrderedCustomProperties)
        //            {
        //                if (prop.IsHoverText) count++;
        //            }
        //            return count;
        //        }
        [Test]
        [Ignore]
        public void DoesCSVWithMissingTrailingCommasLoadCorrectly()
        {
            //            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            //            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            //            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            //            var locationModeType = CSVLocationModeType.None;
            //            //            //Sanity check
            //            var lines = new[] { "Info1,Info2,Info3,Info4", "a1,a2,,a4", "b1,b2,b3,b4", "c1,c2,c3,c4" };
            //            var prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, string.Empty);
            //            Assert.AreEqual(3, prototypes.Count);
            //
            //            //Now find the bug
            //
            //            lines = new[] { "Info1,Info2,Info3,Info4", "a1,a2,,a4", "b1,b2,b3", "c1,c2,c3,c4" };
            //            prototypes = WriteAndLoadCSV(lines, tempFileName, locationPropHolder, locationModeType, string.Empty);
            //            Assert.AreEqual(3, prototypes.Count);
            //            foreach (var proto in prototypes)
            //            {
            //                Assert.IsTrue(proto.HasPropertyByInternalName("Info1"));
            //                Assert.IsTrue(proto.HasPropertyByInternalName("Info2"));
            //                Assert.IsTrue(proto.HasPropertyByInternalName("Info3"));
            //                Assert.IsTrue(proto.HasPropertyByInternalName("Info4"));
            //            }
        }

    }
}