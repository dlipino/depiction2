﻿using System.Windows;
using Depiction2.API.Extension.Geocoder;
using Depiction2.Base.Service;

namespace Depiction2.CsvExtension.UnitTests.TestHelpers
{
    public class TestingGeocoder : IGeocoderExtension
    {
        public void Dispose()
        {
         
        }

        public Point? GeocodeInput(GeocodeInput input)
        {
            return new Point(1,1);
        }
    }
}