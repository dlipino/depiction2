﻿using System.Collections.Generic;
using System.Diagnostics;
using Depiction2.API.Extension.Interaction.Condition;
using Depiction2.Base.Interactions.Interactions;
using Depiction2.Base.StoryEntities;

namespace Depiction2.Interaction.Intersection
{
    [ConditionMetadata("IntersectionCondition", "Intersection Condition", "Evaluates spatial intersection of two ZOIs")]
    public class IntersectionCondition : ICondition
    {
        public string Name
        {
            get { return "IntersectionCondition"; }
        }
        //hmmm, doubles what is in the xml
        public HashSet<string> ConditionTriggerProperties
        {
            get { return new HashSet<string> { "ElementGeometry", "ZoneOfInfluence" }; }
        }

        public bool Evaluate(IElement triggerElement, IElement affectedElement, object[] inputParameters)
        {
            //should be something less death dealing
            Debug.Assert(affectedElement.ElementGeometry != null);
            // If there isn't a zone of influence, we default to true
            if (affectedElement.ElementGeometry == null)
            {
                return false;
            }

            bool returnValue;

            lock (affectedElement.ElementGeometry)
            {
                if (affectedElement.ElementGeometry == null) return true;
                var geom = affectedElement.ElementGeometry;
                returnValue = geom.Intersects(triggerElement.ElementGeometry);
            }

            return returnValue;
        }
    }
}