﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Resources;

namespace Depiction2.Interaction.Intersection.Resources
{
    [ResourceExtensionMetadata(DisplayName = "Intersection Resource", Name = "ResourcesIntersection", Author = "Depiction Inc.", ExtensionPackage = "Default", ResourceName = "IntersectionResources")]
    public class IntersectionResources : IDepictionResourceExtension
    {

        public ResourceDictionary ExtensionResources
        {
            get
            {
                var uri = new Uri("/Depiction2.Interaction.Intersection;Component/Resources/Images/IntersectionResources.xaml",UriKind.RelativeOrAbsolute);
                var rd = new ResourceDictionary {Source = uri};
                return rd;
            }
        }
        public Dictionary<string, Type> ExtensionTypes
        {
            get { return new Dictionary<string, Type>(); }
        }
        public void Dispose()
        {

        }
    }
}