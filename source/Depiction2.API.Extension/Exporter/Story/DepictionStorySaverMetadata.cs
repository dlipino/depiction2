﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.API.Extension.Importer.Story;

namespace Depiction2.API.Extension.Exporter.Story
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionStorySaverMetadata : DepictionExtensionMetadataBase, IDepictionStorySaverMetadata
    {
        public string ExpectedVersion { get; set; }

        public string StoryExtension { get; set; }
        public string ScaffoldExtension { get; set; }
        public string ElementExtension { get; set; }
        public string InteractionExtension { get; set; }
        #region constructor, the typeof is very important

        public DepictionStorySaverMetadata()
            : base(typeof(IDepictionStorySaver))
        {
            Name = "StoryLoader";
            DisplayName = "Nonworking story loader";
            Description = "Does nothing";
            ExtensionPackage = "Default";
            ExpectedVersion = string.Empty;

            StoryExtension = string.Empty;
            ElementExtension = string.Empty;
            StoryExtension = string.Empty;
            InteractionExtension = string.Empty;
        }
        #endregion
    }
}