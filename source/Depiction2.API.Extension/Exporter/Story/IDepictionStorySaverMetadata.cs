﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Exporter.Story
{
    public interface IDepictionStorySaverMetadata : IDepictionExtensionMetadataBase
    {
        string ExpectedVersion { get; }
        string StoryExtension { get; }
        string ScaffoldExtension { get; }
    }
}