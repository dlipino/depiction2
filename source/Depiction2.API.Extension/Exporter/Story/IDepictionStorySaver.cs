﻿using Depiction2.API.Extension.Base;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.API.Extension.Exporter.Story
{
    public interface IDepictionStorySaver : IDepictionExtensionBase
    {
        bool SaveStoryToTempFolderFileAndZip(IStory storyToSave, string tempFolder, string zipFileName);
        bool SaveScaffoldToFile(IElementTemplate templateToSave, string fullFileName);
    }
}