﻿using System.Windows;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Extension.Tools.Positioning
{
    public interface IPositionMeasureExtension : IDepictionExtensionBase
    {
        Point TranslatePoint(Point originPoint, double degreeDirction, double distance,
                             MeasurementSystem specificMeasurement, MeasurementScale specificScale);

        double DistanceBetween(Point start, Point end, MeasurementSystem specificMeasurement,
                               MeasurementScale specificScale);
    }
}