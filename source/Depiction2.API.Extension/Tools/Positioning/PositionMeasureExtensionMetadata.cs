﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Tools.Positioning
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PositionMeasureExtensionMetadata : DepictionExtensionMetadataBase, IPositionMeasureExtensionMetadata
    {

        public PositionMeasureExtensionMetadata()
            : base(typeof(IPositionMeasureExtension))
        {
            Name = DisplayName = "Geometry";
            Author = "Default";
            ExtensionPackage = "Default";

        }
    }
}