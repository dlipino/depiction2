﻿using System.Windows;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;

namespace Depiction2.API.Extension.Tools.Projection
{
    public interface IProjectionExtension : IDepictionExtensionBase
    {
        string SourceCoordinateSystem { get;  }
        string TargetCoordinateSystem { get;  }

        void UpdateCoordinateSystems(string sourceSystem, string targetSystem);
        Point PointFromSourceToTarget(double x, double y);
        Point PointFromTargetToSource(double x, double y);

        IDepictionGeometry GeometryCloneFromSourceToTarget(IDepictionGeometry geometry);
        bool GeometryFromSourceToTarget(IDepictionGeometry geometry);
    }
}