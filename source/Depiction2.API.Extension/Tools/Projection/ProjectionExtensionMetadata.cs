﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Tools.Projection
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ProjectionExtensionMetadata : DepictionExtensionMetadataBase, IProjectionExtensionMetadata
    {
        //The type of is very important
        public ProjectionExtensionMetadata()
            : base(typeof(IProjectionExtension))
        {
            Name = DisplayName = "BaseProjector";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
            Description = "Nonset projector";
        }

    }
}