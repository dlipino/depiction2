﻿using System.Windows.Media;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;

namespace Depiction2.API.Extension.Tools.Geometry
{
    public interface IGeometryExtension : IDepictionExtensionBase 
    {
        IDepictionGeometry CreateGeometry(object geometrySource);
        StreamGeometry CreateStreamGeometryFromDepictionGeometry(IDepictionGeometry geometry);
    }
}