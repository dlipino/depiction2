﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Tools.Geometry
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class GeometryExtensionMetadata : DepictionExtensionMetadataBase, IGeometryExtensionMetadata
    {

        public GeometryExtensionMetadata()
            : base(typeof(IGeometryExtension))
        {
            Name = DisplayName = "Geometry";
            Author = "Default";
            ExtensionPackage = "Default";

        }
    }
}