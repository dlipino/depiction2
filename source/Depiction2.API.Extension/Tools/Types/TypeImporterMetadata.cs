﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.API.Extension.Geocoder;

namespace Depiction2.API.Extension.Tools.Types
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TypeImporterMetadata : DepictionExtensionMetadataBase, ITypeImporterMetadata
    {
        #region constructor, this part is very important

        public TypeImporterMetadata()
            : base(typeof(ITypeImporter))
        {
            Name = DisplayName = "BaseTypeImporter";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
            Description = "Type importerr";
        }
        #endregion

    }
}