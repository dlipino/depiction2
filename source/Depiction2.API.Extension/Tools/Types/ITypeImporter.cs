﻿using System;
using System.Collections.Generic;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Tools.Types
{
    public interface ITypeImporter : IDepictionExtensionBase
    {
        Dictionary<string, Type> TypeDictionary();
    }
}