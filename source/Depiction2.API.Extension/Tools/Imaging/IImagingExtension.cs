﻿using System.Windows;

namespace Depiction2.API.Extension.Tools.Imaging
{
    public interface IImageWarpExtension
    {
        bool WarpImageGeoToMercator(string sourceImage, Point sourceTopLeft, double metersPerPixelX,
                                    double metersPerPixelY,
                                    string warpedImage, out Point warpedTopLeft, out Point warpedBottomRight,
                                    double dfErrorThreshold);
    }
}