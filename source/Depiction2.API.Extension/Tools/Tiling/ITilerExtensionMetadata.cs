﻿using Depiction2.API.Extension.Base;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.API.Extension.Tools.Tiling
{
    public interface ITilerExtensionMetadata : IDepictionExtensionMetadataBase
    {
        TileImageTypes TilerType { get; }
    }
}