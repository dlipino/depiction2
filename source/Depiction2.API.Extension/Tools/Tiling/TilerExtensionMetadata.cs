﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.API.Extension.Tools.Tiling
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class TilerExtensionMetadata : DepictionExtensionMetadataBase, ITilerExtensionMetadata
    {
        public TileImageTypes TilerType { get; set; }
        //The type of is very important
        public TilerExtensionMetadata()
            : base(typeof(ITilerExtension))
        {
            Name = DisplayName = "TilerExtension";
            Author = "";
            ExtensionPackage = "Default";
            Description = "A tiler";
            TilerType = TileImageTypes.Unknown;
        }
    }
}