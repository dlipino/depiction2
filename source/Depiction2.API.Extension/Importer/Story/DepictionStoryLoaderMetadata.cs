﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Story
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class DepictionStoryLoaderMetadata : DepictionExtensionMetadataBase, IDepictionStoryLoaderMetadata
    {
        public string ExpectedVersion { get; set; }
        public string FileExtension { get; set; }
        #region constructor, the typeof is very important

        public DepictionStoryLoaderMetadata()
            : base(typeof(IDepictionStoryLoader))
        {
            Name = "StoryLoader";
            DisplayName = "Nonworking story loader";
            Description = "Does nothing";
            ExtensionPackage = "Default";
            ExpectedVersion = string.Empty;
            FileExtension = ".*";
        }
        #endregion
    }
}