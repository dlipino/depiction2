﻿using System.Collections.Generic;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.API.Extension.Importer.Story
{
    public interface IDepictionStoryLoader : IDepictionExtensionBase
    {
        IStory GetStoryFromFile(string zippedStoryFileName);
        List<IElementTemplate> GetElementTemplatesFromDir(string scaffoldDirectory);
        List<IElementTemplate> GetEmbeddedElementTemplatesFromResource(string resoureName);
        IList<IInteractionRule> GetInteractionRulesFromFile(string fileName);
        IList<IInteractionRule> GetInteractionRulesFromDirectory(string rulesDirectory);
    }
}