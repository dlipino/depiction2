﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Story
{
    public interface IDepictionStoryLoaderMetadata : IDepictionExtensionMetadataBase
    {
        string ExpectedVersion { get; }
        string FileExtension { get; }
//        string ElementFileExtension { get; }
//        string InteractionFileExtension { get; }
    }
}