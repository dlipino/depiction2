﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class SourceLoaderMetadata : DepictionExtensionMetadataBase, ISourceLoaderMetadata
    {
        public string ShortName { get; set; }
        public bool DisplayConfigInAddDialog { get; set; }
        public bool UsePredifinedType { get; set; }
        public SourceLoaderMetadata()
            : base(typeof(ISourceLoaderExtension))
        {
            ShortName = "";
            Name = DisplayName = "SourceLoadExtension";
            Author = "";
            ExtensionPackage = "Default";
            Description = "Arbitrary source importer";
            DisplayConfigInAddDialog = false;
            UsePredifinedType = false;
        }
    }
}