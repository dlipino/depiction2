﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface ISourceLoaderMetadata : IDepictionExtensionMetadataBase
    {
        bool DisplayConfigInAddDialog { get; }
        bool UsePredifinedType { get; }
        string ShortName { get;  }
    }
}