﻿using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface IFileLoaderExtension : IDepictionExtensionBase
    {
        void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName, ICartRect resultBounds);
    }
}