﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface IRegionSourceMetadata : IDepictionExtensionMetadataBase
    {
        string ShortName { get;  }
        string GenericName { get; }
        bool RequiresParameters { get; }
    }
}