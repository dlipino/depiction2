﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface IDepictionFileLoaderMetadata : IDepictionExtensionMetadataBase
    {
        string[] SupportedExtensions { get; }
    }
}