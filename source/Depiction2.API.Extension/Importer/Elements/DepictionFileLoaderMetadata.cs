﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionFileLoaderMetadata : DepictionExtensionMetadataBase,IDepictionFileLoaderMetadata
    {
        public string[] SupportedExtensions { get; set; }

        public DepictionFileLoaderMetadata()
            : base(typeof(IFileLoaderExtension))
        {
            Name = DisplayName = "FileLoadExtension";
            Author = "";
            ExtensionPackage = "Default";
            Description = "A file importer";
            SupportedExtensions = new string[0];
        }

    }
}