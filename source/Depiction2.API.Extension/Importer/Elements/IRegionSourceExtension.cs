﻿using System.Collections.Generic;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface IRegionSourceExtension : IDepictionExtensionBase
    {
        void ImportElements(IDepictionRegion regionOfInterest, RegionDataSourceInformation sourceInfo);
        void SimpleElementImport(ICartRect regionOfInterest, Dictionary<string, string> parameters);
    }
}