﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Importer.Elements
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class RegionSourceMetadata : DepictionExtensionMetadataBase, IRegionSourceMetadata
    {
        private string genericLegacyName = string.Empty;
        public string ShortName { get; set; }
        //Also semi legacy, so i really don't know how much this should get used
        public string GenericName
        {
            get
            {
                if (string.IsNullOrEmpty(genericLegacyName))
                {
                    return Name;
                }
                return genericLegacyName;
            }
            set { genericLegacyName = value; }
        } 
        public bool RequiresParameters { get; set; }

        public RegionSourceMetadata()
            : base(typeof(IRegionSourceExtension))
        {
            RequiresParameters = true;
            ShortName = "";
            Name = DisplayName = "RegionSourceExtension";
            Author = "";
            GenericName = "";
            ExtensionPackage = "Default";
            Description = "Source for regionimporter";
        }
    }
}