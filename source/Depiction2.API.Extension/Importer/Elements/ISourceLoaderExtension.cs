﻿using Depiction2.API.Extension.Base;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.API.Extension.Importer.Elements
{
    public interface ISourceLoaderExtension : IDepictionExtensionBase
    {
        object ElementAddConfigView { get; }
        void ImportElements(string sourceLocation, IElementTemplate defaultTemplate, string idPropertyName);
    }
}