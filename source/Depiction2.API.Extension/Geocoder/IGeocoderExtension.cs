﻿using System.Windows;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;

namespace Depiction2.API.Extension.Geocoder
{
    public interface IGeocoderExtension : IDepictionExtensionBase, IDepictionGeocoder
    {
//        Point? GeocodeInput(GeocodeInput input);
    }
}