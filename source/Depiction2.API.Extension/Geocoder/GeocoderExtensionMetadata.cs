﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Geocoder
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class GeocoderExtensionMetadata : DepictionExtensionMetadataBase, IGeocoderExtensionMetadata
    {
        protected string[] validRegions = new string[0];


        public string[] ValidRegions
        {
            get { return validRegions; }
        }
        #region constructor, this part is very important

        public GeocoderExtensionMetadata()
            : base(typeof(IGeocoderExtension))
        {
            Name = DisplayName = "BaseGeocoder";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
            Description = "Nonset geocoder";
        }
        public GeocoderExtensionMetadata(string[] extensions)
            : this()
        {
            if (extensions != null) validRegions = extensions;
        }
        #endregion

    }
}