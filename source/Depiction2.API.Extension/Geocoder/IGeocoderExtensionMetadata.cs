﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Geocoder
{
    public interface IGeocoderExtensionMetadata : IDepictionExtensionMetadataBase
    {
        string[] ValidRegions { get; }
    }
}