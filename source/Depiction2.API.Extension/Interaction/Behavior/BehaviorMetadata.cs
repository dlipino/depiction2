﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Interactions.Behaviors;

namespace Depiction2.API.Extension.Interaction.Behavior
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class BehaviorMetadata : DepictionExtensionMetadataBase, IBehaviorMetadata
    {
        public BehaviorMetadata(string behaviorName, string behaviorDisplayName, string behaviorDescription)
            : base(typeof(IBehavior))
        {
            Name = behaviorName;

            DisplayName = behaviorDisplayName;
            Description = behaviorDescription;
        }

        public bool Activated
        {
            get { return true; }
        }
    }
}