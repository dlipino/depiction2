﻿using System.Collections.Generic;
using System.Diagnostics;
using Depiction2.Base.Extensions;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;

namespace Depiction2.API.Extension.Interaction.Behavior
{
    /// <summary>
    /// BaseBehavior is the base class for Behaviors.
    /// A behavior is a way to encapsulate a set of logic to modify an element.
    /// Behaviors are invoked either by an interaction, 
    /// or by a postSetActions after an element's property changes value.
    /// </summary>
    public abstract class BaseBehavior : IBehavior
    {
        public abstract DepictionParameterInfo[] DepictionParameters { get; }
        protected abstract BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag);

        public virtual BehaviorResult DoBehavior(IElement subscriber, object[] inputParameters)
        {
            if (DepictionParameters == null && inputParameters.Length != 0)
            {
                return new BehaviorResult();
            }
            if (subscriber == null) return new BehaviorResult();
            if (DepictionParameters != null && DepictionParameters.Length != 0 && inputParameters.Length != DepictionParameters.Length)
            {
                return new BehaviorResult();
            }

            var parameterBag = new Dictionary<string, object>();

            for (int i = 0; i < inputParameters.Length; i++)
            {

                var inputParam = inputParameters[i];
                //Two different methods that this gets called, thrue interactions and from a post set actions
                if (inputParam is string && inputParam.ToString().StartsWith("."))
                {//Coming from post set action, dml file
                    var propName = inputParam.ToString().Remove(0, 1);
                    parameterBag.Add(propName, subscriber.Query(inputParam.ToString()));
                }
                else
                {//Coming from an interaction
                    DepictionParameterInfo userParameter = DepictionParameters[i];
                    var userKey = userParameter.ParameterKey;
                    parameterBag.Add(userKey, inputParameters[i]);
                }

            }

            return InternalDoBehavior(subscriber, parameterBag);
        }

        virtual public void Dispose()
        {
            Debug.WriteLine(string.Format("Disposing of behaviour: {0}", ToString()));
        }
    }
}