﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.Base.Interactions.Interactions;

namespace Depiction2.API.Extension.Interaction.Condition
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class ConditionMetadata : DepictionExtensionMetadataBase, IConditionMetadata
    {
        public ConditionMetadata(string conditionName, string conditionDisplayName, string conditionDescription)
            : base(typeof(ICondition))
        {
            Name = conditionName;
             DisplayName = conditionDisplayName;

           Description = conditionDescription;
        }

//        public bool Activated
//        {
//            get { return true; }
//        }
    }
}