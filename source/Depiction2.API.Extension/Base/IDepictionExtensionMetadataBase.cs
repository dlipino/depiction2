﻿namespace Depiction2.API.Extension.Base
{
    public interface IDepictionExtensionMetadataBase
    {
        string Name { get; }
        string DisplayName { get; }
        string Author { get; }
        string Description { get; }
        string ExtensionPackage { get; }
    }
}