﻿using System;
using System.ComponentModel.Composition;

namespace Depiction2.API.Extension.Base
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    abstract public class DepictionExtensionMetadataBase : ExportAttribute, IDepictionExtensionMetadataBase
    {
        #region Properties
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Author { get; set; }
        public string Description { get;  set; }
        public string ExtensionPackage { get;  set; }
        #endregion

        #region constructor, this part is very important

        protected DepictionExtensionMetadataBase(Type metadataForType) : base(metadataForType) { }

        #endregion
//        #region Methods
//        public bool Activated
//        {
//            get { return CheckActivation(); }
//        }
//
//        public bool CheckActivation()
//        {
//            return true;
//        }
//        #endregion
    }
}