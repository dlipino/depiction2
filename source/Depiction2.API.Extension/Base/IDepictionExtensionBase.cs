﻿using System;

namespace Depiction2.API.Extension.Base
{
    //Seems like a pain to do this, but the dispose was the only way i could see around the window
    //in extension issue. I think i read that the IDisposable was obsolete though, but if extensions with visuals
    //do not use the IDisposable teh application will not exit correctly.
    public interface IDepictionExtensionBase : IDisposable
    {
         
    }
}