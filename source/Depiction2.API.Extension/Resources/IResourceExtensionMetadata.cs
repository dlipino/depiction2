﻿using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Resources
{
    public interface IResourceExtensionMetadata : IDepictionExtensionMetadataBase
    {
        string ResourceName { get; }
    }
}