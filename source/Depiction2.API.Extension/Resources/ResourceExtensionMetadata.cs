﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Resources
{
    /// <summary>
    /// The resource extension is in testing phase. It seems possible to pass
    /// the resourcedictionary without having to create it in the extension.
    /// </summary>

    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ResourceExtensionMetadata : DepictionExtensionMetadataBase, IResourceExtensionMetadata
    {
        public string ResourceName { get; set; }
        //getting the base type is very important
        public ResourceExtensionMetadata()
            : base(typeof(IDepictionResourceExtension))
        {
            Name = DisplayName = "ResourceExtension";
            Author = "Depiction Inc.";
            ExtensionPackage = "Default";
            Description = "More resources";
        }
    }
}