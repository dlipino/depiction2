﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Resources
{
    public interface IDepictionResourceExtension : IDepictionExtensionBase
    {
        ResourceDictionary ExtensionResources { get; }
        Dictionary<string, Type> ExtensionTypes { get; } 
    }
}