﻿using System;
using Depiction2.API.Extension.Base;

namespace Depiction2.API.Extension.Logging
{
    public interface ILogExtension : IDepictionExtensionBase
    {
        void LogException(Exception ex);
        void LogException(string additionalInfo, Exception ex);
        void LogEvent(string eventInfo);
    }
}