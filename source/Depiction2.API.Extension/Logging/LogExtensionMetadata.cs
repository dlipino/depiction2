﻿using System;
using System.ComponentModel.Composition;
using Depiction2.API.Extension.Base;
using Depiction2.API.Extension.Geocoder;

namespace Depiction2.API.Extension.Logging
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class LogExtensionMetadata : DepictionExtensionMetadataBase, ILogExtensionMetadata
    {
        #region constructor, this part is very important

        public LogExtensionMetadata()
            : base(typeof(ILogExtension))
        {
            Name = DisplayName = "BaseLogger";
            Author = "Default";
            ExtensionPackage = "Default";
            Description = "Default Logger name";
        }
        #endregion

    }
}