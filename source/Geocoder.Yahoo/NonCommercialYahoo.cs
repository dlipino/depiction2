﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Xml;
using Depiction2.API.Extension.Geocoder;
using Depiction2.Base.Service;

namespace Geocoder.Yahoo
{
    //Hopefully this doesn't break any laws
    [GeocoderExtensionMetadata(DisplayName = "Noncommercial YAHOO", Name = "NonCommercialYahoo", Author = "David", ExtensionPackage = "YahooGeocoder")]
    public class NonCommercialYahoo : IGeocoderExtension
    {
        //             URI =   @"http://gws2.maps.yahoo.com/findlocation?pf=1&locale=en_US&flags=&offset=15&gflags=&start=0&count=100";
        //                url = string.Format("{0}&q={1}", URI, Uri.EscapeDataString(addressString));
        public void Dispose()
        {
        }

        public Point? GeocodeInput(GeocodeInput input)
        {
            string addressString = input.CompleteAddress;
            if (string.IsNullOrEmpty(addressString)) return null;
            var URI = @"http://gws2.maps.yahoo.com/findlocation?pf=1&locale=en_US&flags=&offset=15&gflags=&start=0&count=100";
            var url = string.Format("{0}&q={1}", URI, Uri.EscapeDataString(addressString));
            WebRequest geocoderRequest = WebRequest.Create(url);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return ParseResponse(responseStream);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return null;//new GeocodeResults(string.Format(InvalidGeoLocationText, addressString));
                throw;
            }
        }

        #region private helpers
        private static Point? ParseResponse(Stream responseStream)
        {
            Point? position;

            if (responseStream != null)
            {
                var latitude = double.NaN;// string.Empty;
                var longitude = double.NaN;// string.Empty;
                var reader = new XmlTextReader(responseStream);
                //using?
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name != "Succeeded" && reader.Name != "ResultSet")
                            {
                                if (reader.Name.Equals("Latitude", StringComparison.InvariantCultureIgnoreCase)
                                    || reader.Name.Equals("Longitude", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string text = reader.ReadString();
                                    if (reader.Name.Equals("Latitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (!double.TryParse(text, out latitude))
                                        {
                                            throw new Exception("Could not parse latitude");
                                        }
                                    }
                                    if (reader.Name.Equals("Longitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        if (!double.TryParse(text, out longitude))
                                        {
                                            throw new Exception("Could not parse latitude");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (XmlException ex)
                {
                    throw new Exception("Failed to parse rtun", ex);
                }
                finally
                {
                    reader.Close();
                }

                position = new Point(longitude, latitude);
            }
            else
            {
                throw new Exception("No response");
            }

            return position;
        }
        #endregion


    }
}