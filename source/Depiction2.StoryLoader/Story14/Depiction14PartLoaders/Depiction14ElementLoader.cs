﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.DmlLoaders;
using Ionic.Zip;

namespace Depiction2.Story14IO.Story14.Depiction14PartLoaders
{
    public class Depiction14ElementLoader
    {
        public const string DepictionXmlNameSpace = "http://depiction.com";
        static public List<IElement> GetElementsFrom14DPNFile(string fileName)
        {
            return GetElementsFrom14DPNFile(fileName, null);
        }
        static public List<IElement> GetElementsFrom14DPNFile(string fileName, IElementTemplateLibrary baseElementLibrary)
        {
            //The hack for reading terrain when the element is getting created and not when the scaffold is getting created
            Depiction14FileOpenHelpers.LastDpnFileLocation = fileName;
            //end the hack
            if (string.IsNullOrEmpty(fileName)) return new List<IElement>();
            var readElements = new List<IElement>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                //Get geolocated elements
                var fullElemFileName = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.ElementDir + "/" + Depiction14FileOpenHelpers.ElemGeoFile;

                var geoElementsEntry = zipFile[fullElemFileName];
                //get non geolocated files
                fullElemFileName = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.ElementDir + "/" + Depiction14FileOpenHelpers.ElemeNonGeoFile;
                var nongeoElementsEntry = zipFile[fullElemFileName];
                if (geoElementsEntry != null && nongeoElementsEntry != null)
                {
                    readElements.AddRange(Dml14ToElement.GetElementsFrom14Stream(geoElementsEntry.OpenReader(), fileName, baseElementLibrary));
                    readElements.AddRange(Dml14ToElement.GetElementsFrom14Stream(nongeoElementsEntry.OpenReader(), fileName, baseElementLibrary));
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;

            return readElements;
        }

//        private TerrainCoverage ReadFullElevation(XmlReader reader, string origZipFile)
//        {
//            var terrain = new TerrainCoverage();
//            terrain.ReadXml(reader, origZipFile);
//            return terrain;
//        }

    }
}