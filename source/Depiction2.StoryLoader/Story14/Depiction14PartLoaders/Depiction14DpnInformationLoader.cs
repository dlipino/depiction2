﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Xml;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.StoryInformation;
using Ionic.Zip;

namespace Depiction2.Story14IO.Story14.Depiction14PartLoaders
{
    public class Depiction14DpnInformationLoader
    {
        const string notunpack = "Could not unpack file {0}";
        private const string readError = "Unexpected error reading {0}";
        #region Depiction metadata, dpn file format, and depiction geo location info section
        //this does not give the scale at which things were displayed, but it does give the lat/long extent
        static public IStoryGeoInformation GetStoryGeoInformationFromDPNFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return null;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            var storyGeoInfo = new StoryGeoInformation();
            using (var zipFile = ZipFile.Read(fileName))
            {
                var geoinfoFile = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.GeoInformationFile;

                var formatZipEntry = zipFile[geoinfoFile];
                if (formatZipEntry != null)
                {
                    using (var reader = Depiction14FileOpenHelpers.GetXmlReader(formatZipEntry.OpenReader()))
                    {
                        reader.Read(); //Start the read process

                        if (reader.Name.Equals("DepictionGeoInformation"))
                        {
                            reader.ReadToFollowing("DepictionLocationInformation");
                            reader.ReadStartElement();
                        }
                        while (!(reader.Name.Equals("DepictionLocationInformation") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                        {
                            if (reader.Name.Equals("WorldLatLongBoundingBox"))
                            {
                                reader.Skip();
                            }
                            else if (reader.Name.Equals("DepictionStartLocation"))
                            {
                                reader.ReadStartElement();
                                if (reader.Name.Equals("LatitudeLongitude"))
                                {
                                    double x, y;

                                    double.TryParse(reader.GetAttribute("longitude"), out x);
                                    double.TryParse(reader.GetAttribute("latitude"), out y);
                                    storyGeoInfo.MapViewCenter = new Point(x, y);
                                    reader.Skip();
                                }
                                reader.ReadEndElement();

                            }
                            else if (reader.Name.Equals("DepictionMapWindow"))
                            {
                                reader.ReadStartElement();
                                if (reader.Name.Equals("MapCoordinateBounds"))
                                {
                                    double top, bot, left, right;

                                    double.TryParse(reader.GetAttribute("top"), out top);
                                    double.TryParse(reader.GetAttribute("bottom"), out bot);
                                    double.TryParse(reader.GetAttribute("left"), out left);
                                    double.TryParse(reader.GetAttribute("right"), out right);
                                    storyGeoInfo.MapViewBounds = new CartRect(new Point(left, top),
                                                                              new Point(right, bot));
                                    reader.Skip();
                                }
                                reader.ReadEndElement();
                            }
                            else if (reader.Name.Equals("DepictionRegionBounds"))
                            {
                                reader.ReadStartElement();
                                if (reader.Name.Equals("MapCoordinateBounds"))
                                {
                                    double top, bot, left, right;

                                    double.TryParse(reader.GetAttribute("top"), out top);
                                    double.TryParse(reader.GetAttribute("bottom"), out bot);
                                    double.TryParse(reader.GetAttribute("left"), out left);
                                    double.TryParse(reader.GetAttribute("right"), out right);
                                    storyGeoInfo.RegionBounds = new CartRect(new Point(left, top),
                                                                              new Point(right, bot));
                                    reader.Skip();
                                }
                                reader.ReadEndElement();
                            }
                            else
                            {
                                reader.Skip();
                            }
                        }
                        reader.ReadEndElement();
                    }
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return storyGeoInfo;
        }

        static public void UpdateDepictionAccessTypeList(string fileName)
        {
            var dict = GetTypeListFromeFile(fileName);
            if (DepictionAccess.NameTypeService != null)
            {
                DepictionAccess.NameTypeService.UpdateSimpleNameTypeDictionary(dict);
            }
        }

        static public Dictionary<string, Type> GetTypeListFromeFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return null;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            var storyGeoInfo = new Dictionary<string, Type>();
            using (var zipFile = ZipFile.Read(fileName))
            {
                var formatZipEntry = zipFile[Depiction14FileOpenHelpers.DepictionDataTypeFile];
                if (formatZipEntry != null)
                {
                    storyGeoInfo = Depiction14SimpleTypeService.GetTypeDictionaryFromStream(formatZipEntry.OpenReader());
                }
            }
            Thread.CurrentThread.CurrentCulture = culture;
            return storyGeoInfo;
        }

        static public IDepictionFileFormat GetFileInformationFromDPNFile(string fileName)
        {
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            var storyInfo = new StoryFileInformation();
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var zipFile = ZipFile.Read(fileName))
                {
                    var formatFile = Depiction14FileOpenHelpers.DepictionFileFormatFile;

                    var formatZipEntry = zipFile[formatFile];
                    if (formatZipEntry == null)
                    {
                        throw new ZipException(string.Format("Could not find requested file entry {0}", formatFile));
                    }

                    using (var reader = Depiction14FileOpenHelpers.GetXmlReader(formatZipEntry.OpenReader()))
                    {
                        reader.Read(); //Start the read process

                        if (reader.Name.Equals("DepictionFormat"))
                        {
                            reader.ReadStartElement();
                        }
                        if (reader.Name.Equals("DepictionFileFormatInformation"))
                        {
                            reader.ReadStartElement();
                            while (!(reader.Name.Equals("DepictionFileFormatInformation")))
                            {
                                if (reader.Name.Equals("DepictionFileVersion"))
                                {
                                    storyInfo.FileVersion = reader.ReadElementContentAsString();
                                }
                                else if (reader.Name.Equals("AppVersion"))
                                {
                                    storyInfo.AppVersion = reader.ReadElementContentAsString();
                                }
                                else if (reader.Name.Equals("MinimumAppVersionForDepiction"))
                                {
                                    reader.Skip();
                                }
                                else
                                {
                                    reader.Skip();
                                }
                            }
                            reader.ReadEndElement();
                        }

                        reader.ReadEndElement();
                    }
                }
            }
            catch (ZipException zex)
            {
                var mes = string.Format(notunpack, fileName);
                DepictionAccess.MessageService.AddStoryMessage(mes);
                return null;
            }
            catch (Exception ex)
            {
                var mes = string.Format(readError, fileName);
                DepictionAccess.MessageService.AddStoryMessage(mes);
                NotificationService.Instance.LogException(ex);
                return null;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }

            return storyInfo;
        }

        static public IStoryDetails GetMetadataInformationFromDPNFile(string fileName)
        {
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            var details = new StoryDetails();
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var zipFile = ZipFile.Read(fileName))
                {
                    var formatFile = Depiction14FileOpenHelpers.DepictionMetaDataFile;

                    var formatZipEntry = zipFile[formatFile];
                    if (formatZipEntry != null)
                    {
                        using (var reader = Depiction14FileOpenHelpers.GetXmlReader(formatZipEntry.OpenReader()))
                        {
                            reader.Read(); //Start the read process

                            if (reader.Name.Equals("DepictionInformation"))
                            {
                                reader.ReadToFollowing("DepictionMetadata");
                                reader.ReadStartElement();
                            }
                            while (!(reader.Name.Equals("DepictionMetadata") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                            {
                                if (reader.Name.Equals("Title"))
                                {
                                    details.Title = reader.ReadElementString().Trim();
                                }
                                else if (reader.Name.Equals("Author"))
                                {
                                    details.Author = reader.ReadElementString().Trim();
                                }
                                else if (reader.Name.Equals("Description"))
                                {
                                    details.Description = reader.ReadElementString().Trim();
                                }
                                else
                                {
                                    //the other one that is visible is the tags, but that is not currently used.
                                    reader.Skip();
                                }
                            }
                            reader.ReadEndElement();
                        }
                    }
                }
            }
            catch (ZipException zex)
            {
                var mes = string.Format(notunpack, fileName);
                DepictionAccess.MessageService.AddStoryMessage(mes);
                return null;
            }
            catch (Exception ex)
            {
                var mes = string.Format(readError, fileName);
                DepictionAccess.MessageService.AddStoryMessage(mes);
                NotificationService.Instance.LogException(ex);
                return null;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
            return details;
        }

        #endregion
    }
}