﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.DmlLoaders;
using Ionic.Zip;

namespace Depiction2.Story14IO.Story14.Depiction14PartLoaders
{
    public class Depiction14ElementLibraryLoader
    {
        static public List<IElementTemplate> GetLibraryElementsFrom14Dpn(string fileName)
        {
            var scaffolds = new List<IElementTemplate>();
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var elementLibraryDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.LibraryDir + "/";
                var elementIconDir = elementLibraryDir + "/" + Depiction14FileOpenHelpers.LibraryIconBinDir;
                var dmlNames = zipFile.EntryFileNames.Where(t => t.StartsWith(elementLibraryDir) && t.EndsWith(".dml"));

                foreach (var dmlName in dmlNames)
                {
                    var formatZipEntry = zipFile[dmlName];
                    if (formatZipEntry != null)
                    {
                        var scaffold = Dml14ToTemplate.GetScaffoldFrom14Stream(formatZipEntry.OpenReader());
                        if(scaffold != null) scaffolds.Add(scaffold);
                    }
                }
            }

            //1.4 dpn do not story the waypoint, end, and start
            if (scaffolds.Any(t => t.ElementType.Contains("Route")))
            {
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("start")) == null)
                {
                    scaffolds.Add(CreateScaffold("start", "Route start", "Depiction.RouteStart"));
                }
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("end")) == null)
                {
                    scaffolds.Add(CreateScaffold("end", "Route end", "Depiction.RouteEnd"));
                }
                if (scaffolds.FirstOrDefault(t => t.ElementType.Equals("WayPoint")) == null)
                {
                    scaffolds.Add(CreateScaffold("WayPoint", "Route way point", "Depiction.RouteWayPoint"));
                }
            }
            //Label them
            foreach(ElementTemplate scaffold in scaffolds)
            {
                scaffold.DefinitionSource = ElementTemplateLibrary.StorySource;
            }

            Thread.CurrentThread.CurrentCulture = culture;
            return scaffolds;
        }

        static public Dictionary<string, ImageSource> GetImagesFrom14DpnLibraryFile(string fileName)
        {
            var entryLocation = Depiction14FileOpenHelpers.DepictionDataDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryDir + "/" +
                                             Depiction14FileOpenHelpers.LibraryIconBinDir;
            return Depiction14ImageLoader.GetImagesFromZipFileDirectory(fileName, entryLocation);
        }

        static ElementTemplate CreateScaffold(string type, string displayName, string resourceName)
        {
            var elementType = type;
            var wayScaffold = new ElementTemplate(elementType)
            {
                DisplayName = displayName
            };

            var iconResourceName = resourceName;
            var iconNameProp = new PropertyTemplate()
            {
                ProperName = "IconPath",
                DisplayName = "Icon Path",
                ValueString = iconResourceName
            };
            wayScaffold.Properties.Add(iconNameProp);
            var iconSizeProp = new PropertyTemplate()
            {
                ProperName = "IconSize",
                DisplayName = "IconSize",
                TypeString = "Double",
                ValueString = "20"
            };
            wayScaffold.Properties.Add(iconSizeProp);

            var dragProp = new PropertyTemplate()
            {
                ProperName = "Draggable",
                DisplayName = "Draggable",
                ValueString = "True"
            };
            wayScaffold.Properties.Add(dragProp);

            return wayScaffold;
        }
    }
}