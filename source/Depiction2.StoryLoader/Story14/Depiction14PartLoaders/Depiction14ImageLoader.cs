﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using System.Xml;
using Depiction2.API;
using Depiction2.API.Service;
using Depiction2.Base.Utilities;
using Ionic.Zip;

namespace Depiction2.Story14IO.Story14.Depiction14PartLoaders
{
    public class Depiction14ImageLoader
    {
        static public Dictionary<string, ImageSource> GetImagesFromZipFileDirectory(string zipFileName, string pathForIcons)
        {
            var images = new Dictionary<string, ImageSource>();
            var tempFolderService = new DepictionFolderService(false);
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            //Fixup the path for icons
            if (!pathForIcons.EndsWith("/"))
            {
                pathForIcons += "/";
            }
            using (var zipFile = ZipFile.Read(zipFileName))
            {
                if (zipFile.ContainsEntry(pathForIcons))
                {
                    var imageNames = zipFile.EntryFileNames.Where(t => t.StartsWith(pathForIcons));
                    foreach (var imageName in imageNames)
                    {
                        var zipEntry = zipFile[imageName];
                        if (zipEntry != null && !zipEntry.IsDirectory)
                        {
                            zipEntry.Extract(tempFolderService.FolderName);
                            var fullFileName = Path.Combine(tempFolderService.FolderName, imageName);
                            var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullFileName);
                            var simpleName = Path.GetFileName(fullFileName);
                            if (image != null && !string.IsNullOrEmpty(simpleName))
                            {
                                images.Add(simpleName, image);
                            }
                        }
                    }
                }
            }
            tempFolderService.Close();
            Thread.CurrentThread.CurrentCulture = culture;
            return images;
        }
        #region Depiction image file reading

        static public DepictionImageResourceDictionary GetDepictionImageNamesFromDPNFile(string fileName)
        {
            var imageNames = new List<string>();
            var imageResourceDictionary = new DepictionImageResourceDictionary();
            var holdingFolder = StorySerializationService.Instance.DataSerializationFolder;
            if(string.IsNullOrEmpty(holdingFolder))
            {
                throw new NullReferenceException("Cannot find the temp holding path");
            }
           
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            using (var zipFile = ZipFile.Read(fileName))
            {
                var imageDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.ImagesDir;
                var imageBinaryDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" +
                                     Depiction14FileOpenHelpers.ImagesDir + "/" +
                                     Depiction14FileOpenHelpers.ImagesBinaryDir;
                var imageResourceFileName = imageDir + "/" + Depiction14FileOpenHelpers.ImagesFile;
                zipFile.FlattenFoldersOnExtract = true;
                zipFile.ExtractSelectedEntries("name = *.*", imageBinaryDir,holdingFolder);// "document\", outputDirectory, ExtractExistingFileAction.OverwriteSilently);
               
                var formatZipEntry = zipFile[imageResourceFileName];
                if (formatZipEntry != null)
                {
                    imageNames.AddRange(GetImageNamesFromStream(formatZipEntry.OpenReader()));
                }

                foreach (var name in imageNames)
                {
                    var fullFileName = Path.Combine(holdingFolder, name);
                    var image = DepictionImageResourceDictionary.GetBitmapFromFile(fullFileName);
                    imageResourceDictionary.AddImage(name, image);
                }

            }
            Thread.CurrentThread.CurrentCulture = culture;
            return imageResourceDictionary;
        }
        static private List<string> GetImageNamesFromStream(Stream interactionFileStream)
        {
            var filenames = new List<string>();
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(interactionFileStream))
            {
                reader.Read(); //Start the read process

                if (reader.Name.Equals("DepictionImages"))
                {
                    reader.ReadToFollowing("RasterImageDictionary");
                    reader.ReadStartElement();
                }
                var rasterFileNames = new List<string>();
                while (!(reader.Name.Equals("RasterImageDictionary") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                {
                    if (reader.Name.Equals("RasterFilenames"))
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.Skip();
                        }
                        else
                        {
                            reader.ReadStartElement();
                            while (!(reader.Name.Equals("RasterFilenames") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                            {
                                rasterFileNames.Add(reader.ReadElementString());
                            }
                            filenames.AddRange(rasterFileNames);
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("RasterKeys"))
                    {
                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();

            }
            return filenames;
        }

        #endregion
    }
}