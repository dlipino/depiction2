﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using Depiction2.API;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.API.Service;
using Depiction2.API.Tools;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Interactions;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story14IO.Story14.DmlLoaders;

namespace Depiction2.Story14IO.Story14
{
    [DepictionStoryLoaderMetadata(Author = "Depiction Inc.", Description = "Loads .dpn files from Depiction 1.4.", DisplayName = "Story loader 1.4.x",
        ExpectedVersion = "1.4", ExtensionPackage = "Default", Name = "Dpn14Loader", FileExtension = ".dpn")]
    public class Depiction14StoryLoader : IDepictionStoryLoader
    {
        private bool IsVersionReadable(string versionString)
        {
            if (string.IsNullOrEmpty(versionString)) return false;
            if (versionString.Contains("1.4"))
            {
                return true;
            }
            if (versionString.Contains("@"))
            {
                var problemMessage =
                      string.Format("Reading from a debug save file");
                DepictionAccess.MessageService.AddStoryMessage(problemMessage, 2);
                return true;
            }
            return false;
        }
        public IStory GetStoryFromFile(string zippedStoryFileName)
        {
            if (string.IsNullOrEmpty(zippedStoryFileName))
            {
                var problemMessage =
                      string.Format("Invalid file name {0}", zippedStoryFileName);
                DepictionAccess.MessageService.AddStoryMessage(problemMessage);
                return null;
            }
            if (TypeService.Instance.NameTypeDictonary.Count == 0)
            {
                TypeService.Instance.LoadCompleteDictionary();
            }
            //Exception handling is down in methods
            var fileInfo = Depiction14DpnInformationLoader.GetFileInformationFromDPNFile(zippedStoryFileName);
            if (fileInfo == null) return null;
            if (!IsVersionReadable(fileInfo.AppVersion))
            {
                var problemMessage =
                    string.Format("Cannot load file {0} with version {1}. Only files saved from Depiction 1.4 can be loaded.", Path.GetFileName(zippedStoryFileName), fileInfo.AppVersion);
                DepictionAccess.MessageService.AddStoryMessage(problemMessage);
                return null;
            }
            var storyDetails = Depiction14DpnInformationLoader.GetMetadataInformationFromDPNFile(zippedStoryFileName);
            if (storyDetails == null) return null;


            if (DepictionAccess.PathService != null)
            {
                DepictionAccess.PathService.ClearLoadFileDirectory();
            }

            var scaffoldsFromDpn = Depiction14ElementLibraryLoader.GetLibraryElementsFrom14Dpn(zippedStoryFileName);
            IElementTemplateLibrary libraryToUse = null;
            //Should only happen in tests, but it is still bad form to have this occur
            //maybe there should be another method to grab the library or add it to the
            //global one.
            if (DepictionAccess.TemplateLibrary != null)
            {
                DepictionAccess.TemplateLibrary.AddStoryElementTemplates(scaffoldsFromDpn);
                libraryToUse = DepictionAccess.TemplateLibrary;
            }
            else
            {
                libraryToUse = new ElementTemplateLibrary();
                libraryToUse.AddStoryElementTemplates(scaffoldsFromDpn);
            }
            //order is important, image resources must be loaded before the elemetns because the image resources has the 
            //elevation tiff image.
            var imageResourceDictionary = Depiction14ImageLoader.GetDepictionImageNamesFromDPNFile(zippedStoryFileName);
            var elements = Depiction14ElementLoader.GetElementsFrom14DPNFile(zippedStoryFileName, libraryToUse);
            var geoInformation = Depiction14DpnInformationLoader.GetStoryGeoInformationFromDPNFile(zippedStoryFileName);
            var annotations = Depiction14AnnotationLoader.GetAnnotationsFromDPNFile(zippedStoryFileName);
            var displayers = Depiction14DisplayerLoader.GetDepictionDisplayersFromDPNFile(zippedStoryFileName).ToList();
            var loadedInteractionRules = Depiction14InteractionRuleLoader.GetInteractionsFromDPNFile(zippedStoryFileName);

            var story = new Story(null);
            story.LoadElements(elements);
            story.AddAnnotations(annotations);
            //SetGeoInformation is something from legacy and is what is used
            //to determin the zoom, i don't think it is ever updated.
            //            story.SetGeoInformation(geoInformation);
            story.VisibleExtent = geoInformation.MapViewBounds;
            var b = geoInformation.RegionBounds;
            var tl = new Point(b.Left, b.Top);
            var br = new Point(b.Right, b.Bottom);
            //            story.PrimaryRegion = new DepictionRegion(tl, br);
            story.AddRegion(new DepictionRegion(tl, br));
            story.LoadNonDefaultInteractions(loadedInteractionRules);
            story.InitializeInteractionRouter();

            //The elements to display are set duing the save process
            if (displayers.Count > 0)
            {
                story.MainDisplayer = displayers[0];
                story.MainDisplayer.ConnectToStory(story);
                var revList = displayers.GetRange(1, displayers.Count - 1);

                foreach (var dis in revList)
                {
                    var rev = dis as IElementRevealer;
                    if (rev != null)
                    {
                        story.LoadRevealer(rev);
                        rev.ConnectToStory(story);
                    }
                }
            }

            var savedElementTypeIcons = Depiction14ElementLibraryLoader.GetImagesFrom14DpnLibraryFile(zippedStoryFileName);

            var missingIcons = FindNondefaultImages(savedElementTypeIcons);
            story.SetStoryImages(imageResourceDictionary);
            story.AllImages.AppendImages(missingIcons);

            //Hack a way to get the center and zoom, when loaded from a 1.4 depiction
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                var viewWidth = Application.Current.MainWindow.ActualWidth;
                var storyWidth = story.VisibleExtent.Width;
                var scale = viewWidth / storyWidth;
                var center = story.VisibleExtent.Center;
                //   cheat a bit since we know the scale is 1
                story.AdjustZoom(scale, center);
            }
            story.StoryDetails = storyDetails;
            return story;
        }

        public List<IElementTemplate> GetElementTemplatesFromDir(string scaffoldDirectory)
        {
            return Dml14ToTemplate.GetAllScaffoldsFromDirectory14(scaffoldDirectory);
        }

        public List<IElementTemplate> GetEmbeddedElementTemplatesFromResource(string resoureName)
        {
            var folderService = new DepictionFolderService(false);
            var tempFolder = folderService.FolderName;

            ResourceReaderService.CopyAssemblyEmbeddedResourcesToDirectory(resoureName, "dml", true, tempFolder);
            var readElements = GetElementTemplatesFromDir(tempFolder);
            folderService.Close();
            return readElements;
        }
        #region interaction rules loading for 1.4

        public IList<IInteractionRule> GetInteractionRulesFromFile(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                return LoadRulesFromXmlStream(stream);
            }
        }
        public IList<IInteractionRule> GetInteractionRulesFromDirectory(string rulesDirectory)
        {
            var rules = new List<IInteractionRule>();
            var files = Directory.GetFiles(rulesDirectory, "*.xml", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var interactionRules = GetInteractionRulesFromFile(file);
                if (interactionRules != null)
                    rules.AddRange(interactionRules);
            }

            return rules;
        }

        private IList<IInteractionRule> LoadRulesFromXmlStream(Stream stream)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var reader = Depiction14FileOpenHelpers.GetXmlReader(stream))
                {

                    return Depiction14FileOpenHelpers.DeserializeItemList<IInteractionRule>("InteractionRules", typeof(InteractionRule), reader);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }
        #endregion
        private Dictionary<string, ImageSource> FindNondefaultImages(Dictionary<string, ImageSource> loadedImages)
        {
            var defaultKeys = DepictionAccess.DefaultIconDictionary.Keys.ToList();
            var loadedKeys = loadedImages.Keys.ToList();
            var nonDefaultIcons = loadedKeys.Except(defaultKeys);
            var leftOver = new Dictionary<string, ImageSource>();
            foreach (var iconName in nonDefaultIcons)
            {
                leftOver.Add(iconName, loadedImages[iconName]);
            }
            return leftOver;
        }

        public void Dispose()
        {
            Debug.WriteLine("Done with the story loader");
        }
    }
}

//        public const string mercatorProjectedCoordinateSystem14 = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
//                                                         "GEOGCS[\"Popular Visualisation CRS\",  " +
//                                                             "DATUM[\"WGS84\",    " +
//                                                                 "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
//                                                             "AUTHORITY[\"EPSG\",\"6055\"]], " +
//                                                        "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
//                                                        "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
//                                                        "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
//                                                    "PROJECTION[\"Mercator\"]," +
//                                                    "PARAMETER[\"semi_minor\",6378137]," +
//                                                    "PARAMETER[\"False_Easting\", 0]," +
//                                                    "PARAMETER[\"False_Northing\", 0]," +
//                                                    "PARAMETER[\"Central_Meridian\", 0]," +
//                                                    "PARAMETER[\"Latitude_of_origin\", 0]," +
//                                                    "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
//                                                    "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
//                                                    "AUTHORITY[\"EPSG\",\"3785\"]]";
