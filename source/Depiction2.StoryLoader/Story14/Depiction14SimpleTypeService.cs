﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Xml;
using Depiction2.API.Utilities;

namespace Depiction2.Story14IO.Story14
{
    public class Depiction14SimpleTypeService 
    {


        static public Dictionary<string, Type> GetTypeDictionaryFromStream(Stream stream)
        {
            var output = new Dictionary<string, string>();
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                using (var xmlReader = DepictionXmlFileUtilities.GetXmlReader(stream))
                {
                    xmlReader.ReadStartElement("elementTypes");
                    while (!xmlReader.NodeType.Equals(XmlNodeType.EndElement))
                    {
                        var key = xmlReader.GetAttribute("key");
                        var value = xmlReader.GetAttribute("value");
                        xmlReader.Read();
                        if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
                        {
                            output.Add(key, value);
                        }
                    }
                    xmlReader.ReadEndElement();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From DeserializeObject:" + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }

            //Attempt to create the elements based on the values,
            //only keeps those that do get created
            var finalOutput = new Dictionary<string, Type>();
            var keys = output.Keys;
            foreach (var key in keys)
            {
                var type = Type.GetType(output[key], false);
                if (type != null)
                {
                    finalOutput.Add(key, type);
                }
            }
            return finalOutput;
        }
//        private Dictionary<string, Type> defaultDepictionNameAndTypeDictionary;
//        private List<Type> depictionCoreTypes = new List<Type>();
//        #region properties
//
//        public Dictionary<string, Type> NameTypeDictonary
//        {
//            get { return defaultDepictionNameAndTypeDictionary; }
//        }
//
//        #endregion
//        
//        #region constructor
//
//        public Depiction14SimpleTypeService()
//        {
//            CreateDefaultDepictionNameTypeDictionary();
//        }
//        #region consturction helpers
//        private void SetDepictionCoreTypes()
//        {
//            #region the base depiction types, hopefully
//            depictionCoreTypes.Add(typeof(Area));
//            depictionCoreTypes.Add(typeof(Distance));
//            depictionCoreTypes.Add(typeof(Temperature));
//            depictionCoreTypes.Add(typeof(Volume));
//            depictionCoreTypes.Add(typeof(Weight));
//            depictionCoreTypes.Add(typeof(Speed));
//
//            depictionCoreTypes.Add(typeof(int));
//            depictionCoreTypes.Add(typeof(double));
//            depictionCoreTypes.Add(typeof(string));
//            depictionCoreTypes.Add(typeof(bool));
//            depictionCoreTypes.Add(typeof(Color));
//            depictionCoreTypes.Add(typeof(SolidColorBrush));
//            depictionCoreTypes.Add(typeof(DateTime));
//
//            //These are sketchy since they might come from an extension
//            depictionCoreTypes.Add(typeof(DataTypeValidationRule));
//            depictionCoreTypes.Add(typeof(TerrainExistsValidationRule));
//            depictionCoreTypes.Add(typeof(MinValueValidationRule));
//            depictionCoreTypes.Add(typeof(RangeValidationRule));
//            //            depictionCoreTypes.Add(typeof(TerrainCoverage));
//            //            depictionCoreTypes.Add(typeof(RoadGraph));
//
//            #endregion
//
//        }
//        private void CreateDefaultDepictionNameTypeDictionary()
//        {
//            SetDepictionCoreTypes();
//            //Reset everytning
//            defaultDepictionNameAndTypeDictionary = new Dictionary<string, Type>();
//            foreach (var type in depictionCoreTypes)
//            {
//                //The the common wpf types that are used in depiction
//                defaultDepictionNameAndTypeDictionary.Add(type.Name, type);
//            }
//            //            //Some legacy names/types
//            //            defaultDepictionNameAndTypeDictionary.Add("Terrain", typeof(TerrainCoverage));
//        }
//        #endregion
//        #endregion
//        #region generic helpers
//
//        public Type GetTypeFromName(string name)
//        {
//            var simpleName = name;
//            //first try to get it from the complete name
//            if (!defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
//            {
//                //simplify the name
//                var index = name.LastIndexOf(',');
//                simpleName = name;
//                if (index > 0)
//                {
//                    var sub = name.Substring(0, index).Trim();
//                    simpleName = sub.Substring(sub.LastIndexOf('.') + 1);
//                }
//            }
//            //Now try again
//            if (defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
//            {
//                return defaultDepictionNameAndTypeDictionary[simpleName];
//            }
//            return Type.GetType(name);
//        }
//
//        public void UpdateSimpleNameTypeDictionary(Dictionary<string, Type> updateDictionary)
//        {
//            foreach (var keyValue in updateDictionary)
//            {
//                if (!defaultDepictionNameAndTypeDictionary.ContainsKey(keyValue.Key))
//                {
//                    defaultDepictionNameAndTypeDictionary.Add(keyValue.Key, keyValue.Value);
//                }
//            }
//        }
//
//        public void ResetToDefaultDictionary()
//        {
//            CreateDefaultDepictionNameTypeDictionary();
//        }
//
//        public bool AddTypeToDictionary(string simpleName, Type type)
//        {
//            if (defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
//            {
//                return false;
//            }
//            defaultDepictionNameAndTypeDictionary.Add(simpleName, type);
//            return true;
//        }
//
//        #endregion



    }
}