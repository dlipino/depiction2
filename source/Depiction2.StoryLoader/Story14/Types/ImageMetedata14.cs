﻿using Depiction2.Base.Geo;

namespace Depiction2.Story14IO.Story14.Types
{
    public class ImageMetedata14
    {
        public string ImageName { get; set; }
        public ICartRect Bounds { get; set; }
        public double Rotation { get; set; }
    }
}