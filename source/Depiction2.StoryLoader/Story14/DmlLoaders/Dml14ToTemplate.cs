﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Depiction2.API;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;

namespace Depiction2.Story14IO.Story14.DmlLoaders
{
    public class Dml14ToTemplate
    {
        #region public read methods
        static public List<IElementTemplate> GetAllScaffoldsFromDirectory14(string directory)
        {
            var prototypes = new List<IElementTemplate>();
            if (!Directory.Exists(directory)) return prototypes;
            var fileList = Directory.GetFiles(directory, "*.dml");
            foreach (var fileName in fileList)
            {
                var scaffold = GetScaffoldFrom14DmlFile(fileName);

                if (scaffold != null)
                {
                    prototypes.Add(scaffold);
                }
            }
            return prototypes;
        }

        static public IElementTemplate GetScaffoldFrom14DmlFile(string fileName)
        {
            IElementTemplate template = null;
            try
            {
                using (var dmlFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    template = GetScaffoldFrom14Stream(dmlFileStream);
                }
            }
            catch (Exception ex)
            {
                var errorMessage = string.Format("{0} is not a valid dml file", Path.GetFileName(fileName));
                //                 DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 10);
                return null;
            }
            return template;
        }
        static public IElementTemplate GetScaffoldFrom14Stream(Stream sintleElementStream)
        {
            IElementTemplate template = null;
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(sintleElementStream))
            {
                reader.MoveToContent(); //Not sure why this has to be here
                template = GetScaffoldFrom14XmlReader(reader);
                reader.Close();
            }
            return template;
        }
        #endregion
        static private IElementTemplate GetScaffoldFrom14XmlReader(XmlReader reader)
        {
            var nodeName = "depictionElement";
            ElementTemplate elemTemplate = null;
            if (!conversionDictSet)
            {
                SetConversionDictionary();
            }
            while (reader.Name.Equals(nodeName, StringComparison.InvariantCultureIgnoreCase) && reader.NodeType.Equals(XmlNodeType.Element))
            {
                var parentId = reader.GetAttribute("elementKey");
                if (!string.IsNullOrEmpty(parentId))
                {
                    //Do something because scaffold are not supposed to have ids
                }
                elemTemplate = new ElementTemplate(reader.GetAttribute("elementType")) { DefinitionSource = "" };
                var sourceAttr = reader.GetAttribute("definitionSource");
                if (!string.IsNullOrEmpty(sourceAttr))
                {
                    elemTemplate.DefinitionSource = sourceAttr;
                }
                elemTemplate.DisplayName = reader.GetAttribute("displayName");
                var startDepth = reader.Depth;
                bool usingPermaText;
                bool.TryParse(reader.GetAttribute("usingPermaText"), out usingPermaText);
                reader.ReadStartElement();
                //Get the properties
                while (!reader.Name.Equals(nodeName, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        FillElementTemplatePropertiesFromXml(reader, elemTemplate);
                    }
                    else if (reader.Name.Equals("ImageMetadata"))
                    {
                        reader.Skip();//As far as i know there should be any image metadata
                    }
                    else if (reader.Name.Equals("ZoneOfInfluence"))
                    {
                        reader.Skip();//tehre should also be no geometry, as far as i know
                    }
                    else if (reader.Name.Equals("Tags"))
                    {
                        while (!(reader.Name.Equals("Tags") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                        {
                            var tag = reader.ReadElementString();
                            elemTemplate.Tags.Add(tag);
                        }
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("generateZoi"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "generateZoi");
                        elemTemplate.OnCreateActions = zoiGenerateActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onCreate"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "onCreate");
                        elemTemplate.OnCreateActions = zoiGenerateActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onClick"))
                    {
                        var clickActions = GenericReaders.ReadElementActions(reader, "onClick");
                        elemTemplate.OnClickActions = clickActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("PermaText"))
                    {
                        reader.Skip();
                    }
                    else if (reader.Name.Equals("elementWaypoints"))
                    {
                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();
            }
            //something should happen it null
            return elemTemplate;
        }


        private static Dictionary<string, string> visual14PropertyNames = null;
        private static Dictionary<string, string> information14PropertyNames = null;
        private static Dictionary<string, string> display14PropertyNames = null;
        private static Dictionary<string, string> template14PropertyNames = null;
        private static HashSet<string> ignoreTemplateProps = null;
        private static bool conversionDictSet = false;
        private static void SetConversionDictionary()
        {
            conversionDictSet = true;
            visual14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            visual14PropertyNames.Add("ZOIFill", PropNames.ZOIFillColor);
            visual14PropertyNames.Add("ZOIBorder", PropNames.ZOIBorderColor);
            visual14PropertyNames.Add("ZOILineThickness", PropNames.ZOILineThickness);
            visual14PropertyNames.Add("ZOIShapeType", PropNames.ZOIShapeType);
            visual14PropertyNames.Add("IconPath", PropNames.IconResourceName);
            visual14PropertyNames.Add("IconSize", PropNames.IconSize);
            visual14PropertyNames.Add("IconBorderColor", PropNames.IconBorderColor);
            visual14PropertyNames.Add("IconBorderShape", PropNames.IconBorderShape);
            visual14PropertyNames.Add("ShowIcon", PropNames.VisualState);

            information14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            information14PropertyNames.Add("Author", PropNames.Author);
            information14PropertyNames.Add("Description", PropNames.Description);
            information14PropertyNames.Add("DisplayName", PropNames.DisplayName);

            display14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            display14PropertyNames.Add("Draggable", PropNames.IsDraggable);
            display14PropertyNames.Add("CanEditZoi", PropNames.IsGeometryEditable);
            display14PropertyNames.Add(PropNames.IsGeometryEditable, PropNames.IsGeometryEditable);

            template14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            template14PropertyNames.Add("PlaceableWithMouse", PropNames.IsMouseAddable);
            template14PropertyNames.Add("DefinitionSource", PropNames.DefinitionSource);
            template14PropertyNames.Add("DisplayInAddContent", PropNames.HideFromLibrary);
            template14PropertyNames.Add("ShowElementPropertiesOnCreate", PropNames.ShowPropertiesOnCreate);

            ignoreTemplateProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            ignoreTemplateProps.Add("Position");
        }

        static private void FillElementTemplatePropertiesFromXml(XmlReader reader, ElementTemplate targetTemplate)
        {
            if (!reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase)) return;
            //for now, this should always be the case
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }
            #region read in all properties
            //Expects the begining of a property xml piece.
            while (reader.Name.Equals("property"))
            {
                var emptyProp = reader.IsEmptyElement;
                var propName = reader.GetAttribute("name");
                if (string.IsNullOrEmpty(propName)) propName = "Generic";
                var origPropName = propName;
                var valueString = reader.GetAttribute("value");
                var isHoverTextString = reader.GetAttribute("isHoverText");
                var isHoverText = false;
                if(!string.IsNullOrEmpty(isHoverTextString))
                {
                    isHoverText = bool.Parse(isHoverTextString);
                }

                var convName = string.Empty;
                //find the converted name
                if (visual14PropertyNames.ContainsKey(propName))
                {
                    convName = visual14PropertyNames[propName];
                }
                else if (information14PropertyNames.ContainsKey(propName))
                {
                    convName = information14PropertyNames[propName];
                }
                else if (display14PropertyNames.ContainsKey(propName))
                {
                    convName = display14PropertyNames[propName];
                }
                else if (template14PropertyNames.ContainsKey(propName))
                {
                    convName = template14PropertyNames[propName];
                }
                //Create the template properties
                if (ignoreTemplateProps.Contains(origPropName))
                {
                    reader.Skip();
                }
                else if (!string.IsNullOrEmpty(convName))
                {
                    //minor fixes for 1.4 dpns
                    if (origPropName.Equals("IconPath"))
                    {
                        var names = valueString.Split(':');
                        if (names.Length > 1)
                        {
                            //For 1.4 icon resource names
                            valueString = names[1];
                        }
                    }
                    else if (origPropName.Equals("ShowIcon", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //this is only rally vaild for 1.4
                        if (bool.Parse(valueString))
                        {
                            valueString = (ElementVisualSetting.Icon).ToString();
                        }
                        else
                        {
                            valueString = (ElementVisualSetting.Geometry).ToString();
                        }
                    }
                    //Set special template props or create a generic prop
                    if (convName.Equals(PropNames.IsMouseAddable))
                    {
                        targetTemplate.IsMouseAddable = bool.Parse(valueString);
                    }
                    else if (convName.Equals(PropNames.HideFromLibrary))
                    {
                        targetTemplate.HideFromLibrary = !bool.Parse(valueString);
                    }
                    else if (convName.Equals(PropNames.ShowPropertiesOnCreate))
                    {
                        targetTemplate.ShowPropertiesOnCreate = true;
                    }else
                    {
                        targetTemplate.InfoProperties.Add(new DefaultTemplateProperty(convName, valueString));
                        if (isHoverText) targetTemplate.HoverTextProps.Add(convName); 
                    }
                    
                    //Skip the inner part and go back to reading properties
                    if (!emptyProp)
                    {
                        reader.Skip();
                    }
                    else
                    {
                        reader.ReadStartElement();
                    }

                    var d = reader.GetAttribute("name");
                }
                else//it is a property that the user wants for this element type
                {
                    var propDisplayName = reader.GetAttribute("displayName");
                    var propertyTypeString = reader.GetAttribute("typeName");
                    var propScaffold = new PropertyTemplate
                    {
                        ProperName = propName,
                        DisplayName = propDisplayName,
                        TypeString = propertyTypeString,
                        ValueString = valueString
                    };
                    if (isHoverText) targetTemplate.HoverTextProps.Add(propName);
                    #region special properties
                    if (!emptyProp)
                    {
                        var postSetActions = new Dictionary<string, string[]>();
                        reader.ReadStartElement();
                        while (!reader.Name.Equals("property") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            if (reader.Name.Equals("value"))
                            {
                                if (propName != null)
                                {
                                    propScaffold.ValueString = reader.ReadInnerXml();
                                }
                                //Odd case wehre the save value was an acutal type value to xml (angle)
                                if(reader.NodeType.Equals(XmlNodeType.EndElement))
                                {
                                    reader.ReadEndElement();
                                }
                            }
                            else
                            {
                                propScaffold.ValueString = valueString;
                            }
                            //1.4
                            if (reader.Name.Equals("restoreValue"))//possibly not needed
                            { }
                            if (reader.Name.Equals("postSetActions"))
                            {
                                postSetActions = GenericReaders.ReadElementActions(reader, "postSetActions");
                                propScaffold.LegacyPostSetActions = postSetActions;
                            }
                            if (reader.Name.Equals("validationRules"))
                            {
                                propScaffold.LegacyValidationRules = GenericReaders.ReadValidationRules(reader,
                                                                                                       DepictionAccess.NameTypeService,
                                                                                                        propertyTypeString).ToArray();
                            }
                        }
                    }
                    else
                    {
                        reader.ReadStartElement();
                    }
                    //I think the inner reads leave at the endelement node
                    if (reader.Name.Equals("property") && reader.NodeType.Equals(XmlNodeType.EndElement))
                    {
                        reader.ReadEndElement();
                    }
                    #endregion
                    targetTemplate.Properties.Add(propScaffold);
                }
            }
            #endregion
            //Should be at the end of properties
            if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase) 
                && reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.ReadEndElement();
            }
        }
    }
}