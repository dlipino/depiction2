﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Xml;
using Depiction2.API;
using Depiction2.API.Converters;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Base.StoryEntities.PropertyTypes;
using Depiction2.Base.Utilities;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities;
using Depiction2.Core.StoryEntities.Element;
using Depiction2.Story14IO.Story14.Types;

[assembly: InternalsVisibleTo("Depiction2.Story14IO.UnitTests")]
namespace Depiction2.Story14IO.Story14.DmlLoaders
{
    public class Dml14ToElement
    {
        private static Dictionary<string, string> visual14PropertyNames = null;
        private static Dictionary<string, string> information14PropertyNames = null;
        private static Dictionary<string, string> display14PropertyNames = null;
        private static Dictionary<string, string> template14PropertyNames = null;
        private static HashSet<string> ignoreElementProps = null;
        private static HashSet<string> clrProps = null;
        private static bool conversionDictSet = false;
        private static void SetElementConversionDictionary()
        {
            conversionDictSet = true;
            visual14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            visual14PropertyNames.Add("ZOIFill", PropNames.ZOIFillColor);
            visual14PropertyNames.Add("ZOIBorder", PropNames.ZOIBorderColor);
            visual14PropertyNames.Add("ZOILineThickness", PropNames.ZOILineThickness);
            visual14PropertyNames.Add("ZOIShapeType", PropNames.ZOIShapeType);
            visual14PropertyNames.Add("IconPath", PropNames.IconResourceName);
            visual14PropertyNames.Add("IconSize", PropNames.IconSize);
            visual14PropertyNames.Add("IconBorderColor", PropNames.IconBorderColor);
            visual14PropertyNames.Add("IconBorderShape", PropNames.IconBorderShape);
            visual14PropertyNames.Add("ShowIcon", PropNames.VisualState);

            information14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            information14PropertyNames.Add("Author", PropNames.Author);
            information14PropertyNames.Add("Description", PropNames.Description);
            information14PropertyNames.Add("DisplayName", PropNames.DisplayName);

            display14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            display14PropertyNames.Add("Draggable", PropNames.IsDraggable);
            display14PropertyNames.Add("CanEditZoi", PropNames.IsGeometryEditable);
            display14PropertyNames.Add(PropNames.IsGeometryEditable, PropNames.IsGeometryEditable);
            display14PropertyNames.Add("Position", PropNames.GeoLocation);

            template14PropertyNames = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            template14PropertyNames.Add("PlaceableWithMouse", PropNames.IsMouseAddable);
            template14PropertyNames.Add("DefinitionSource", PropNames.DefinitionSource);
            template14PropertyNames.Add("DisplayInAddContent", PropNames.HideFromLibrary);
            template14PropertyNames.Add("ShowElementPropertiesOnCreate", PropNames.ShowPropertiesOnCreate);

            ignoreElementProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            ignoreElementProps.Add("PlaceableWithMouse");
            ignoreElementProps.Add("DefinitionSource");
            ignoreElementProps.Add("DisplayInAddContent");
            ignoreElementProps.Add("ShowElementPropertiesOnCreate");
            clrProps = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var key in visual14PropertyNames.Values)
            {
                clrProps.Add(key);
            }
            foreach (var key in information14PropertyNames.Values)
            {
                clrProps.Add(key);
            }
            foreach (var key in display14PropertyNames.Values)
            {
                clrProps.Add(key);
            }
        }

        #region publci read methods
        static public List<IElement> GetElementsFromXml(string fileName, string dataFile, IElementTemplateLibrary library)
        {
            List<IElement> readElements = null;
            if (!File.Exists(fileName)) return new List<IElement>();
            using (var stream = File.OpenRead(fileName))
            {
                readElements = GetElementsFrom14Stream(stream, dataFile, library);
                stream.Close();
            }
            return readElements;
        }
        //used to read the saved active element file 
        static public List<IElement> GetElementsFrom14Stream(Stream elementFileStream, string dataFile, IElementTemplateLibrary library)
        {
            var readElements = new List<IElement>();
            int expectedElemCount = 0;
            var internalCount = 0;
            using (var reader = Depiction14FileOpenHelpers.GetXmlReader(elementFileStream))
            {
                reader.Read(); //Start the read process
                if (reader.Name.Equals("ElementList"))
                {
                    var elemCount = reader.GetAttribute(0);
                    //Should only be one value, the elementCount
                    if (!int.TryParse(reader.GetAttribute(0), out expectedElemCount))
                    {
                        return readElements;
                    }
                    reader.ReadToFollowing("DepictionElement");
                }
                var elements = ReadSingleElement(reader, dataFile, library);
                readElements.AddRange(elements);

                reader.Close();
            }
            return readElements;
        }
        #endregion
        static HashSet<string> ignored14ElementTypes = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase)
                                                         {
                                                             "Depiction.Plugin.RoadNetwork"
                                                         }; 
        //This actually does just read one element, but, if the element happens to be a route, it creates
        // the start,end and waypoints.
        static public List<IElement> ReadSingleElement(XmlReader reader, string dataFile, IElementTemplateLibrary library)
        {
            if (!conversionDictSet)
            {
                SetElementConversionDictionary();
            }
            var nodeName = "DepictionElement";
            var readElements = new List<IElement>();
            while (reader.Name.Equals(nodeName) && reader.NodeType.Equals(XmlNodeType.Element))
            {
                var parentId = reader.GetAttribute("elementKey");
                var elementType = reader.GetAttribute("elementType");
                if(ignored14ElementTypes.Contains(elementType))
                {
                    var message = string.Format("Could not load {0} from file, please reload from a different source",elementType);
                    DepictionAccess.MessageService.AddStoryMessage(message);
                    reader.Skip();
                    continue;
                }
                var singleElement = new DepictionElement(parentId, elementType);
                var nonClrProps = new List<IElementProperty>();
                ImageMetedata14 elementImageData = null;
                var elementZOIString = string.Empty;
                singleElement.DisplayName = reader.GetAttribute("displayName");
                var startDepth = reader.Depth;
                bool usingPermaText;
                if (bool.TryParse(reader.GetAttribute("usingPermaText"), out usingPermaText))
                {
                    singleElement.DisplayInformationText = usingPermaText;
                }
                reader.ReadStartElement();
                //Get the properties
                while (!reader.Name.Equals(nodeName))
                {
                    if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        nonClrProps = ReadPropertyList(reader, dataFile, singleElement);
                    }
                    else if (reader.Name.Equals("ImageMetadata"))
                    {
                        elementImageData = GetImageMetadata14(reader, startDepth);
                    }
                    else if (reader.Name.Equals("ZoneOfInfluence"))
                    {
                        reader.ReadToDescendant("geometryWkt");

                        elementZOIString = reader.ReadElementString();

                        //counting to the tree level would be clever way of doing this
                        //cant remember why this works, but it is designed to read until the ZoneOfInfluence
                        //end element, actually it is supposed to read past, untill the next element on the
                        //same level as the zoneofinfluence.
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("Tags"))
                    {
                        while (!(reader.Name.Equals("Tags") && reader.NodeType.Equals(XmlNodeType.EndElement)))
                        {
                            var tag = reader.ReadElementString();
                            singleElement.AddTag(tag);
                        }
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("generateZoi"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "generateZoi");
                        singleElement.PostAddActions = zoiGenerateActions;//maybe append?
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onCreate"))
                    {
                        var zoiGenerateActions = GenericReaders.ReadElementActions(reader, "onCreate");
                        singleElement.PostAddActions = zoiGenerateActions;//maybe appened?
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("onClick"))
                    {
                        var clickActions = GenericReaders.ReadElementActions(reader, "onClick");
                        singleElement.OnClickActions = clickActions;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                    }
                    else if (reader.Name.Equals("PermaText"))
                    {
                        var permaText = new DepictionPermaText();
                        reader.Read();

                        permaText.ReadXml(reader);
                        permaText.IsVisible = usingPermaText;
                        while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
                        {
                            reader.ReadEndElement();
                        }
                        singleElement.PermaText = permaText;
                    }
                    #region Depiction 2 stuff for reading waypoitns as elements
                    else if (reader.Name.Equals("elementWaypoints"))
                    {
                        //Create the waypoint
                        while (reader.Read() && reader.Name.Equals("elementWaypoint"))
                        {
                            elementType = reader.GetAttribute("name");
                            var waypointId = string.Empty;
                            if (!string.IsNullOrEmpty(parentId))
                            {
                                waypointId = Guid.NewGuid().ToString();
                            }
                            var wayScaffold = new DepictionElement(waypointId, elementType);

                            var iconResourceName = reader.GetAttribute("IconPath");
                            var names = iconResourceName.Split(':');
                            if (names.Length > 1)
                            {
                                //For 1.4 icon resource names
                                iconResourceName = names[1];
                            }
                            wayScaffold.IconResourceName = iconResourceName;
                            var iconSizeString = reader.GetAttribute("IconSize");
                            double iconSize;
                            if (iconSizeString == null) iconSize = 20;
                            else
                            {
                                double.TryParse(iconSizeString, out iconSize);
                            }
                            wayScaffold.IconSize = iconSize;

                            var position = reader.GetAttribute("Location");
                            if (!string.IsNullOrEmpty(position))
                            {
                                double lat, lon;
                                string message;
                                LatLongParserUtility.ParseForLatLong(position, out lat, out lon, out message);
                                var p = new Point(lon, lat);
                                wayScaffold.UpdatePrimaryPointAndGeometry(p, null);
                            }
                            wayScaffold.IsDraggable = true;
                            wayScaffold.VisualState = ElementVisualSetting.Icon;
                            readElements.Add(wayScaffold);
                            singleElement.AttachElements(new List<IElement> { wayScaffold });
                        }
                        reader.ReadEndElement();
                    }
                    #endregion
                    else
                    {
                        reader.Skip();
                    }
                }
                reader.ReadEndElement();
                //We got all the info we need about the element
                //Update the geometry
                SetGeometry(singleElement, elementImageData, elementZOIString, nonClrProps);
                //Not sure what this is used for, possibly the resetting up of properties since 1.4 throws
                //all the properties into the save container
                IElementTemplate template = null;
                if (library != null)
                {
                    template = library.FindElementTemplate(singleElement.ElementType);
                }
                ElementAndElemTemplateService.UpdateElementWithProperties(singleElement, nonClrProps, template);
                //
                //this will generally happen in 1.4 loads, the icon state should be set by
                //the visual property holder thing.
                if (!string.IsNullOrEmpty(singleElement.ImageResourceName))
                {
                    singleElement.VisualState = ElementVisualSetting.Image;//maybe with icon
                }
                else
                {
                    if (singleElement.ElementGeometry == null)
                    {
                        singleElement.VisualState = ElementVisualSetting.Icon;
                    }
                    else if (!singleElement.ElementGeometry.GeometryType.Equals(DepictionGeometryType.Point))
                    {
                        singleElement.VisualState = singleElement.VisualState | ElementVisualSetting.Geometry;
                    }
                    else
                    {
                        if (singleElement.VisualState == ElementVisualSetting.None)
                        {
                            singleElement.VisualState = ElementVisualSetting.Icon;
                        }
                    }
                }
                //insert because, well, no real reason i guess, just to make the new element come before
                //waypoints
                readElements.Insert(0, singleElement);
            }
            return readElements;
        }

        #region property reading helper
        internal static dynamic LoadXmlPropertyValue(XmlReader reader, Type valueType)
        {
            dynamic convertedValue = null;
            if (typeof(IXmlLoadable).IsAssignableFrom(valueType))
            {
                convertedValue = Activator.CreateInstance(valueType);
                var instance = convertedValue as IXmlLoadable;
                if (instance != null)
                {
                    instance.ReadFromXml(reader);
                }
            }
            return convertedValue;
        }
        static public List<IElementProperty> ReadPropertyList(XmlReader reader, string fileSourceName, IElement owningElement)
        {
            var nonclrProps = new List<IElementProperty>();
            if (!reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase)) return nonclrProps;
            //for now, this should always be the case
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }

            while (reader.Name.Equals("property"))
            {
                var emptyProp = reader.IsEmptyElement;
                //Figure out property type
                var nameAttr = reader.GetAttribute("name");
                if (string.IsNullOrEmpty(nameAttr)) { nameAttr = "Generic"; }
                var propName = nameAttr;
                //check for ignored props first
                if (ignoreElementProps.Contains(nameAttr))
                {
                    reader.Skip();
                    continue;
                }
                #region Proper name getting
                //find the converted name
                if (visual14PropertyNames.ContainsKey(nameAttr))
                {
                    propName = visual14PropertyNames[nameAttr];
                }
                else if (information14PropertyNames.ContainsKey(nameAttr))
                {
                    propName = information14PropertyNames[nameAttr];
                }
                else if (display14PropertyNames.ContainsKey(nameAttr))
                {
                    propName = display14PropertyNames[nameAttr];
                }

                #endregion
                //Set the hover text
                var hoverAttr = reader.GetAttribute("isHoverText");
                if (!string.IsNullOrEmpty(hoverAttr))
                {
                    if (hoverAttr.Contains("T"))
                    {
                        owningElement.AddHoverTextProperty(propName, false);
                    }
                }
                //get the value
                var valueString = reader.GetAttribute("value");
                if (valueString == null) valueString = "";
                if (clrProps.Contains(propName))
                {
                    #region set the proper clr value
                    if (propName.Equals(PropNames.IsGeometryEditable))
                    {
                        owningElement.IsGeometryEditable = bool.Parse(valueString);
                    }
                    else if (propName.Equals(PropNames.IsDraggable))
                    {
                        owningElement.IsDraggable = bool.Parse(valueString);
                    }
                    else if (propName.Equals(PropNames.UsePropertyNamesInInformationText))
                    {
                        owningElement.IsDraggable = bool.Parse(valueString);
                    }
                    else if (propName.Equals(PropNames.DisplayName))
                    {
                        owningElement.DisplayName = valueString;
                    }
                    else if (propName.Equals(PropNames.Description))
                    {
                        owningElement.Description = valueString;
                    }
                    else if (propName.Equals(PropNames.Author))
                    {
                        owningElement.Author = valueString;
                    }
                    else if (propName.Equals(PropNames.VisualState))
                    {
                        if (bool.Parse(valueString))
                        {
                            owningElement.VisualState = (ElementVisualSetting.Icon);
                        }
                        else
                        {
                            owningElement.VisualState = (ElementVisualSetting.Geometry);
                        }
                    }
                    else if (propName.Equals(PropNames.ZOIFillColor))
                    {
                        owningElement.ZOIFillColor = valueString;
                    }
                    else if (propName.Equals(PropNames.ZOIBorderColor))
                    {
                        owningElement.ZOIBorderColor = valueString;
                    }
                    else if (propName.Equals(PropNames.ZOILineThickness))
                    {
                        owningElement.ZOILineThickness = double.Parse(valueString);
                    }
                    else if (propName.Equals(PropNames.ZOIShapeType))
                    {
                        owningElement.ZOIShapeType = valueString;
                    }
                    else if (propName.Equals(PropNames.IconBorderShape))
                    {
                        owningElement.IconBorderShape = valueString;
                    }
                    else if (propName.Equals(PropNames.IconBorderColor))
                    {
                        owningElement.IconBorderColor = valueString;
                    }
                    else if (propName.Equals(PropNames.IconSize))
                    {
                        owningElement.IconSize = double.Parse(valueString);
                    }
                    else if (propName.Equals(PropNames.IconResourceName))
                    {
                        var names = valueString.Split(':');
                        if (names.Length > 1)
                        {
                            if (names[0].Equals("file") && names.Length > 2)
                            {
                                //For 1.4 file resource names so it is a path
                                var name = Path.GetFileName(names[2]);
                                owningElement.IconResourceName = name;
                            }
                            else
                            {
                                //For 1.4 icon resource names
                                owningElement.IconResourceName = names[1];
                            }
                        }
                        else
                        {
                            owningElement.IconResourceName = valueString;
                        }
                    }
                    else if (propName.Equals(PropNames.ImageResourceName))
                    {

                    }
                    else if (propName.Equals(PropNames.ImageRotation))
                    {

                    }
                    else if (propName.Equals(PropNames.DisplayInformationText))
                    {

                    }
                    else if (propName.Equals(PropNames.UseEnhancedInformationText))
                    {

                    }
                    else if (propName.Equals(PropNames.GeoLocation))
                    {
                        Point? geoPos = new Point();
                        //order if this might cause slow loading

                        if (!Regex.IsMatch(valueString, "[WwnNeEsS]"))//not north because of NaN
                        {
                            var pos = Point.Parse(valueString);
                            geoPos = new Point(pos.Y, pos.X);

                        }
                        else
                        {//this should only happen on older depictions, hopefully all new ones save in decimal
                            double lat, lon;
                            string message;
                            LatLongParserUtility.ParseForLatLong(valueString, out lat, out lon, out message);
                            if (lon.Equals(double.NaN) || lat.Equals(double.NaN))
                            {
                                geoPos = null;
                            }
                            else
                            {
                                geoPos = new Point(lon, lat);
                            }
                        }

                        owningElement.UpdatePrimaryPointAndGeometry(geoPos, null);
                    }
                    #endregion
                    if (!emptyProp)
                    {
                        reader.Skip();
                    }
                    else
                    {
                        reader.ReadStartElement();
                    }
                    continue;
                }
                //not a clr prop
                var disNameAttr = reader.GetAttribute("displayName");
                var dispName = disNameAttr;
                if (string.IsNullOrEmpty(disNameAttr))
                {
                    dispName = propName;
                }

                var elementProperty = new ElementProperty(propName, dispName);

                var propertyTypeString = reader.GetAttribute("typeName");
                var propertyType = DepictionAccess.GetTypeFromName(propertyTypeString);

                var postSetActions = new Dictionary<string, string[]>();
                var validationRules = new IValidationRule[0];
                object convertedValue = null;//dynamic vs object
                //The non-empty property should happen when a property has post set actions
                //the value should only exists for roadgraph and terrain
                #region special properties
                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    while (!reader.Name.Equals("property") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                    {
                        if (reader.Name.Equals("value"))
                        {
                            convertedValue = LoadXmlPropertyValue(reader, propertyType);
                            if (reader.Name.Equals("value") && !reader.NodeType.Equals(XmlNodeType.EndElement))
                            {
                                reader.Skip();
                            }
                            else
                            {
                                reader.ReadEndElement();
                            }
                        }
                        if (reader.Name.Equals("restoreValue")) { }//INcomplete
                        if (reader.Name.Equals("postSetActions"))
                        {
                            postSetActions = GenericReaders.ReadElementActions(reader, "postSetActions");
                            elementProperty.LegacyPostSetActions = postSetActions;
                        }
                        if (reader.Name.Equals("validationRules"))
                        {
                            validationRules = GenericReaders.ReadValidationRules(reader, DepictionAccess.NameTypeService, propertyTypeString).ToArray();
                            elementProperty.LegacyValidationRules = validationRules;
                        }
                    }
                    reader.ReadEndElement();
                }
                else
                {
                    reader.ReadStartElement();
                }
                #endregion

                if (propertyType == null)
                {
                    elementProperty.SetValue(valueString, false);
                }
                else
                {
                    if (convertedValue == null)
                    {
                        convertedValue = DepictionTypeConverter.ChangeType(valueString, propertyType);
                    }

                    elementProperty.SetValue(convertedValue, false);
                }
                nonclrProps.Add(elementProperty);
            }
            //Should be at the end of properties
            if (reader.Name.Equals("Properties", StringComparison.InvariantCultureIgnoreCase)
                && reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.ReadEndElement();
            }
            return nonclrProps;
        }
        #endregion

        #region imagedata from 14
        private static void SetGeometry(DepictionElement element, ImageMetedata14 imageMetadata, string geometryString, IEnumerable<IElementProperty> properties)
        {
            if (imageMetadata != null)
            {
                var imageBounds = imageMetadata.Bounds;
                if (!imageBounds.IsEmpty && imageBounds.IsValid)
                {
                    var geoString = string.Format("Polygon (({0} {1},{2} {1},{2} {3},{0} {3},{0} {1}))", imageBounds.Left, imageBounds.Top, imageBounds.Right, imageBounds.Bottom);
                    element.SetGeometryFromWtkString(geoString);
                }
                element.ImageResourceName = imageMetadata.ImageName;
                element.ImageRotation = imageMetadata.Rotation;
            }
            else
            {
                //Hack for roadnetwork from 1.4 saves
                if (string.IsNullOrEmpty(geometryString))
                {
                    if (element.GeoLocation != null)
                    {
                        var geoString = string.Format("Point ({0} {1})", element.GeoLocation.Value.X, element.GeoLocation.Value.Y);
                        element.SetGeometryFromWtkString(geoString);
                    }

                    //                    //                    throw new NotImplementedException("Don't know how to load elements with no geometry string");
                    //                    var roadGraph = properties.FirstOrDefault(t => t.ProperName.Equals("RoadGraph"));
                    //                    if (roadGraph != null && DepictionGeometryService.Activated)
                    //                    {
                    //
                    //                        var valType = roadGraph.ValueType;
                    //                        //make a complex geometry interface in base.
                    //                        //                        element.ElementGeometry = new GeometryGdalWrap(((RoadGraph)(roadGraph.Value)).ConvertToMultilinewkt());
                    //                    }
                }
                else
                {
                    element.SetGeometryFromWtkString(geometryString);
                }
            }
        }

        static private ImageMetedata14 GetImageMetadata14(XmlReader reader, int startDepth)
        {
            if (!reader.Name.Equals("ImageMetadata")) return null;
            var geoImage = new ImageMetedata14();
            reader.ReadToDescendant("DepictionImageMetadata");
            reader.ReadStartElement();
            double latTL = double.NaN, longTL = double.NaN, latBR = double.NaN, longBR = double.NaN;

            while (
                !(reader.Name.Equals("DepictionImageMetadata") &&
                  reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
                if (reader.Name.Equals("ImageFilename"))
                {
                    geoImage.ImageName = reader.ReadString();
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("TopLeftLatitude"))
                {
                    double.TryParse(reader.ReadString(), out latTL);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("TopLeftLongitude"))
                {
                    double.TryParse(reader.ReadString(), out longTL);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("BottomRightLatitude"))
                {
                    double.TryParse(reader.ReadString(), out latBR);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("BottomRightLongitude"))
                {
                    double.TryParse(reader.ReadString(), out longBR);
                    reader.ReadEndElement();
                }
                else if (reader.Name.Equals("Rotation"))
                {
                    double rotation;
                    double.TryParse(reader.ReadString(), out rotation);
                    geoImage.Rotation = rotation;
                    reader.ReadEndElement();
                }
                else
                {
                    reader.Skip();
                }
            }
            geoImage.Bounds = new CartRect(new Point(longTL, latTL), new Point(longBR, latBR));
            while (reader.Depth != startDepth && reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.ReadEndElement();
            }
            return geoImage;
        }
        #endregion
    }
}