﻿using System;
using System.ComponentModel;
using System.Windows;
using Depiction2.API;
using DepictionLegacy.FileBrowser;

namespace Depiction2.ViewDialogs.IconAndColorDialogs
{
    public partial class IconSelectorDialog : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string selectedDefaultIconPath;
        private string selectedUserIconPath;
        public string SelectedDefaultIconPath
        {
            get { return selectedDefaultIconPath; }
            set { selectedDefaultIconPath = value; NotifyPropertyChanged("SelectedDefaultIconPath"); }
        }
        public string SelectedUserIconPath
        {
            get { return selectedUserIconPath; }
            set { selectedUserIconPath = value; NotifyPropertyChanged("SelectedUserIconPath"); }
        }
        public IconSelectionType IconSelectionTypeValue { get; set; }
      
        public int StoryImageCount
        {
            get { return DepictionAccess.StoryImageDictionary.Count; }
        }
        #region dep props

        public string IconFileName
        {
            get { return (string)GetValue(IconFileNameProperty); }
            set { SetValue(IconFileNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconFileName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFileNameProperty =
            DependencyProperty.Register("IconFileName", typeof(string), typeof(IconSelectorDialog), new UIPropertyMetadata(string.Empty));

        #endregion

        #region COnstructor

        public IconSelectorDialog()
        {
            InitializeComponent();
            UserResources.Checked += ResourceButton_Checked;
            AppResources.Checked += ResourceButton_Checked;
            LoadResources.Checked += ResourceButton_Checked;

            cancelButton.Click += cancelButton_Click;
            okButton.Click += okButton_Click;
            Focusable = true;
            Topmost = true;
        }

        #endregion
        #region evetn methods

        void okButton_Click(object sender, RoutedEventArgs e)
        {
            //            //Copy the file to the user icons path, this might always be accurate, but it will work everytime for 
            //            if (IconSelectionTypeValue == IconSelectionType.File && DepictionAccess.PathService != null)
            //            {
            //                if(File.Exists(IconFileName))
            //                {
            //                    var fileName = Path.GetFileName(IconFileName);
            //                    File.Copy(IconFileName, Path.Combine(DepictionAccess.PathService.UserElementIconDirectoryPath, fileName));
            //                }
            //            }

            DialogResult = true;
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        void browseButton_Click(object sender, RoutedEventArgs e)
        {
            var fileBrowser = new DepictionFileBrowser();
            var file = fileBrowser.LoadImageFile();
            IconFileName = file;
        }

        void ResourceButton_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)AppResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.Default;

            }
            else if ((bool)UserResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.User;

            }
            else if ((bool)LoadResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.File;
            }
        }

        #endregion
        #region static creator

        static public string SelectIconSource(string originalPath)
        {
            try
            {
                var mainWindow = Application.Current.MainWindow;

                var iconSelector = new IconSelectorDialog { Owner = mainWindow };
                iconSelector.SelectedDefaultIconPath = originalPath;
                iconSelector.SelectedUserIconPath = originalPath;
                iconSelector.SetToFileSelection(originalPath);

                bool? dialogResult = iconSelector.ShowDialog();

                if ((bool)dialogResult)
                {
                    var result = iconSelector.SelectedDefaultIconPath;

                    var selectionType = iconSelector.IconSelectionTypeValue;
                    if (Equals(selectionType, IconSelectionType.User))
                    {
                        result = iconSelector.SelectedUserIconPath;
                    }
                    else if (Equals(selectionType, IconSelectionType.File))
                    {
                        var fileName = iconSelector.IconFileName;
                        result = fileName;// new DepictionIconPath(IconPathSource.File, fileName);
                    }
                    return result;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        #endregion
        public void SetToFileSelection(string filePath)
        {
//            if (filePath.Source.Equals(IconPathSource.File))
//            {
//                if (UserImageDictionary.ContainsKey(filePath))
//                {
//                    UserResources.IsChecked = true;
//                }
//                else
//                {
//                    LoadResources.IsChecked = true;
//                    IconFileName = filePath.Path;
//                }
//            }
        }

        #region Implementation of INotifyPropertyChanged

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public enum IconSelectionType
        {
            Default,
            User,
            File
        }
    }

}
