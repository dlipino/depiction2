﻿using System.Windows;
using System.Windows.Input;
using Depiction2.Core.ViewModels;
using Depiction2.ViewControls.Tools;

namespace Depiction2.ViewDialogs
{
    public class CommandConnector
    {
        #region command connection

        static public void InitMainWindowCommandBinding(Window commandHoldingWindow)
        {
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null) return;
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.Exit,
            (s, e) =>
            {
                e.Handled = true;
                ((DepictionAppViewModel)mainWindow.DataContext).ExitExecuted();
            }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(ApplicationCommands.Open,
            (s, e) =>
            {
                e.Handled = true;
                ((DepictionAppViewModel)mainWindow.DataContext).LoadADepiction();
            },
            (s, e) =>
            {
                e.CanExecute = ((DepictionAppViewModel)mainWindow.DataContext).CanLoadDepiction();
            }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(ApplicationCommands.Save,
            (s, e) =>
            {
                e.Handled = true;
                ((DepictionAppViewModel)mainWindow.DataContext).SaveDepiction();
            }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(ApplicationCommands.SaveAs,
          (s, e) =>
          {
              e.Handled = true;
              ((DepictionAppViewModel)mainWindow.DataContext).SaveDepictionAs();
          }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(ApplicationCommands.New,
            (s, e) =>
            {
                e.Handled = true;
                ((DepictionAppViewModel)mainWindow.DataContext).ToSelectDepictionLocationScreen();
            }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.StartNewDepiction,
              (s, e) =>
              {
                  e.Handled = true;
                  ((DepictionAppViewModel)mainWindow.DataContext).NewDepiction(e.Parameter);
              }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToMainMenu,
           (s, e) =>
           {
               e.Handled = true;
               ((DepictionAppViewModel)mainWindow.DataContext).ToDepictionMenu();
           }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.OpenFile,
             (s, e) =>
             {
                 e.Handled = true;
                 ((DepictionAppViewModel)mainWindow.DataContext).LoadExtensionFromDir();
             }));

            //            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.NavigateBrowserTo,
            //            (s, e) =>
            //            {
            //                if (e != null)
            //                    AppCommand.NavigateTo(e.Parameter);
            //
            //                e.Handled = true;
            //            }));

            //            InitMainAppDialogToggleCommandBindings(commandHoldingWindow);
        }


        static public void InitMainAppDialogToggleCommandBindings(Window commandHoldingWindow)
        {
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null) return;
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleSettingDialog,
              (s, e) =>
              {
                  e.Handled = true;
                  ((DepictionAppViewModel)mainWindow.DataContext).ToggleSettingsDialog();
              }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleAboutDialog,
              (s, e) =>
              {
                  e.Handled = true;
                  ((DepictionAppViewModel)mainWindow.DataContext).ToggleAboutDialog();
              }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleEulaDialog,
              (s, e) =>
              {
                  e.Handled = true;
                  ((DepictionAppViewModel)mainWindow.DataContext).ToggleEulaDialog();
              }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleToolsDialog,
              (s, e) =>
              {
                  e.Handled = true;
                  ((DepictionAppViewModel)mainWindow.DataContext).ToggleToolsDialog(null);
              }));

            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleExtensionDialog,
          (s, e) =>
          {
              e.Handled = true;
              ((DepictionAppViewModel)mainWindow.DataContext).ToggleExtensionsDialog();
          }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleInteractionLibraryDialog,
           (s, e) =>
           {
               e.Handled = true;
               ((DepictionAppViewModel)mainWindow.DataContext).ToggleInteractionLibraryDialog();
           }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleInteractionEditorDialog,
           (s, e) =>
           {
               e.Handled = true;
               ((DepictionAppViewModel)mainWindow.DataContext).ToggleInteractionEditorDialog();
           }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleScaffoldExplorerDialog,
           (s, e) =>
           {
               e.Handled = true;
               ((DepictionAppViewModel)mainWindow.DataContext).ToggleScaffoldExplorerDialog();
           }));
            commandHoldingWindow.CommandBindings.Add(new CommandBinding(AppCommand.ToggleScaffoldEditorDialog,
           (s, e) =>
           {
               e.Handled = true;
               ((DepictionAppViewModel)mainWindow.DataContext).ToggleScaffoldEditorDialog();
           }));
        }
        #endregion
    }
}