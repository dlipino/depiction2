﻿using System.Diagnostics;
using System.Windows.Navigation;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for TipControlView.xaml
    /// </summary>
    public partial class TipControlView
    {
        public TipControlView()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                case ProductInformationBase.DepictionRW:
                    MainPanel.Children.Remove(DefaultTextPanel);
                    break;
                default:
                    MainPanel.Children.Remove(PrepTextPanel);
                    break;
            }
//#if PREP
//            MainPanel.Children.Remove(DefaultTextPanel);
//#else
//            MainPanel.Children.Remove(PrepTextPanel);
//#endif
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            var url = e.Uri.ToString();
            Process.Start(url);
        }
    }
}