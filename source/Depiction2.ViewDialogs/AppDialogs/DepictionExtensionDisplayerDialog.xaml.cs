﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace Depiction2.ViewDialogs.AppDialogs
{
    /// <summary>
    /// Interaction logic for AddinDisplayerDialog.xaml
    /// </summary>
    public partial class DepictionExtensionDisplayerDialog
    {
        public DepictionExtensionDisplayerDialog()
        {
            InitializeComponent();
            DataContextChanged += AddinDisplayerDialog_DataContextChanged;
        }

        private void AddinDisplayerDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
//            switch (DepictionAccess.ProductInformation.ProductType)
//            {
//                case ProductInformationBase.Debug:
//                    break;
//                default:
//                    var tabControl = AddonTabControl;
//                    var tabControlItem = DefaultAddonTab;
//                    if (tabControl != null && tabControlItem != null)
//                    {
//                        tabControl.Items.Remove(tabControlItem);
//                    }
//                    break;
//            }
//#if !DEBUG
//            //This only seems to work here. It does not work on the applyTemplateCall
//            var tabControl =AddonTabControl;
//            var tabControlItem = DefaultAddonTab;
//            if(tabControl != null && tabControlItem != null)
//            {
//                tabControl.Items.Remove(tabControlItem);
//            }
//#endif
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)//Resharper LIES!!!!
        {
            Process.Start(e.Uri.ToString());

        }
    }
}
