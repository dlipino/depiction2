using Depiction2.Core.ViewModels.DialogViewModels.App;

namespace Depiction2.ViewDialogs.AppDialogs
{
    public partial class DepictionSettingsDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DepictionSettingsDialog"/> class.
        /// </summary>
        public DepictionSettingsDialog()
        {
            InitializeComponent();
            DataContextChanged += DepictionSettingsDialog_DataContextChanged;
        }

        void DepictionSettingsDialog_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            var newDC = e.NewValue as DepictionSettingsDialogVM;
            if (newDC == null) return;
            proxyPassword.Password = newDC.ProxyPassword;
        }

        void proxyPassword_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            var dc = DataContext as DepictionSettingsDialogVM;
            if (dc == null) return;
            dc.ProxyPassword = proxyPassword.Password;
        }
    }
}