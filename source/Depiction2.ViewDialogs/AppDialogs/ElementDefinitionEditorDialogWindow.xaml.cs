﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.View.Dialogs.HelperClasses;
using Depiction.View.Dialogs.HelperClasses.ElementPropertyStyles;
using Depiction.View.Dialogs.Properties;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.DialogViewModels;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ElementDefinitionEditorDialogWindow.xaml
    /// </summary>
    public partial class ElementDefinitionEditorDialogWindow
    {
        private GridViewColumnHeader _CurSortCol = null;
        private SortAdorner _CurAdorner = null;

        #region constructor

        public ElementDefinitionEditorDialogWindow()
        {
            InitializeComponent();
            var commandBinding = new CommandBinding(ElementPropertiesControlResourceLibrary.ChangeButtonBackgroundCommand, ElementPropertiesControlResourceLibrary.ChangeButtonBackground);
            CommandBindings.Add(commandBinding);
            commandBinding = new CommandBinding(ElementPropertiesControlResourceLibrary.SelectElementIconCommand, ElementPropertiesControlResourceLibrary.SelectElementIcon);
            CommandBindings.Add(commandBinding);
            AllowDrop = true;
            Drop += PrototypeEditorDialogWindow_Drop;
            GiveFeedback += PrototypeEditorDialogWindow_GiveFeedback;
            DragOver += PrototypeEditorDialogWindow_DragOver;
            SetLocationToStoredValue();
        }
        #endregion

        #region size and layout things

        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.DefinitionEditorDialogRestoreBounds = RestoreBounds;
            }
        }
        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.DefinitionEditorDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.DefinitionEditorDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                Width = bounds.Width;
                Height = bounds.Height;
            }
            base.SetLocationToStoredValue();
        }
        #endregion

        void PrototypeEditorDialogWindow_DragOver(object sender, DragEventArgs e)
        {
            AllowDrop = false;//Not sure how to not let the thing accept drops without turning drop totally off
            var data = e.Data;
            if (data.GetDataPresent(DataFormats.FileDrop))
            {
                var fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (fileNames == null) return;
                if (fileNames.Length != 1) return;
                var extension = Path.GetExtension(fileNames[0]);
                if (extension.Equals(".dml", StringComparison.InvariantCultureIgnoreCase))
                {
                    AllowDrop = true;
                    return;
                }
            }
            var elemVM = data.GetData(typeof(MapElementViewModel)) as MapElementViewModel;
            if (elemVM != null)
            {
                AllowDrop = true;
                //var elem = elemVM.ElementModel as DepictionElementBase;
            }
        }

        void PrototypeEditorDialogWindow_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            e.Handled = false;
        }

        void PrototypeEditorDialogWindow_Drop(object sender, DragEventArgs e)
        {
            var dc = DataContext as ElementDefinitionEditorDialogViewModel;
            if (dc == null) return;
            var data = e.Data;
            if (data.GetDataPresent(DataFormats.FileDrop))
            {
                var fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (fileNames == null) return;
                if (fileNames.Length != 1) return;
                var extension = Path.GetExtension(fileNames[0]);
                if (extension.Equals(".dml", StringComparison.InvariantCultureIgnoreCase))
                {
                    dc.LoadEditPrototypeFromFile(fileNames[0]);
                    return;
                }
            }
            var elemVM = data.GetData(typeof(MapElementViewModel)) as MapElementViewModel;
            if (elemVM != null)
            {
                var elem = elemVM.ElementModel as DepictionElementBase;
                dc.LoadPrototypeFromPreExisting(elem);
            }
        }
        //Duplicate of elementpropertidsdialog
        private void SortClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            if (column == null) return;
            String field = column.Tag as String;

            if (_CurSortCol != null)
            {
                AdornerLayer.GetAdornerLayer(_CurSortCol).Remove(_CurAdorner);
                propertySelectorListView.Items.SortDescriptions.Clear();
            }

            ListSortDirection newDir = ListSortDirection.Ascending;
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Descending))
            {
                _CurAdorner = null;
                return;
            }
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Ascending))
            {
                newDir = ListSortDirection.Descending;
            }
            _CurSortCol = column;
            _CurAdorner = new SortAdorner(_CurSortCol, newDir);
            AdornerLayer.GetAdornerLayer(_CurSortCol).Add(_CurAdorner);
            propertySelectorListView.Items.SortDescriptions.Add(new SortDescription(field, newDir));
        }
    }
}
