﻿using System.Windows;
using System.Windows.Controls;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ManageContentControlFrame.xaml
    /// </summary>
    public partial class ManageContentDialog
    {
        public ManageContentDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            AssociatedHelpFile = "manageContent.html";
            SetLocationToStoredValue();

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    btnExportCsv.Visibility = Visibility.Collapsed;
                    btnExportCsv.IsEnabled = false;
                    btnSendToEmail.Visibility = Visibility.Collapsed;
                    btnSendToEmail.IsEnabled = false;
                    btnHide.Visibility = Visibility.Collapsed;
                    btnHide.IsEnabled = false;
                    btnShow.Visibility = Visibility.Collapsed;
                    btnShow.IsEnabled = false;
                    selectHelpText.Visibility = Visibility.Visible;
                    triangleHelpText.Visibility = Visibility.Visible;
                    DockPanel.SetDock(ActionPanel, Dock.Left);

                    DockPanel.SetDock(unregisteredActionPanel, Dock.Left);
                    btnEditProperties_UnReg.Visibility = Visibility.Collapsed;
                    btnEditProperties_UnReg.IsEnabled = false;

                    selectHelpText_unReg.Visibility = Visibility.Visible;
                    triangleHelpText_unReg.Visibility = Visibility.Visible;
                    break;
                case ProductInformationBase.DepictionRW:

                    btnExportCsv.Visibility = Visibility.Collapsed;
                    btnExportCsv.IsEnabled = false;
                    btnSendToEmail.Visibility = Visibility.Collapsed;
                    btnSendToEmail.IsEnabled = false;
                    btnHide.Visibility = Visibility.Collapsed;
                    btnHide.IsEnabled = false;
                    btnShow.Visibility = Visibility.Collapsed;
                    btnShow.IsEnabled = false;
                    selectHelpText.Visibility = Visibility.Visible;
                    triangleHelpText.Visibility = Visibility.Visible;
                    DockPanel.SetDock(ActionPanel, Dock.Left);

                    DockPanel.SetDock(unregisteredActionPanel, Dock.Left);
                    btnEditProperties_UnReg.Visibility = Visibility.Collapsed;
                    btnEditProperties_UnReg.IsEnabled = false;

                    btnCenterOnElement.Visibility = Visibility.Collapsed;
                    btnCenterOnElement.IsEnabled = false;

                    selectHelpText_unReg.Visibility = Visibility.Visible;
                    triangleHelpText_unReg.Visibility = Visibility.Visible;
                    btnExportGeneric.Visibility=Visibility.Visible;
                    break;
            }
        }

        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.ManageContentDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.ManageContentDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                Width = bounds.Width;
                Height = bounds.Height;
            }
            base.SetLocationToStoredValue();
        }

        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.ManageContentDialogRestoreBounds = RestoreBounds;
            }
            base.LocationOrSizeChanged_Updated();
        }
    }
}