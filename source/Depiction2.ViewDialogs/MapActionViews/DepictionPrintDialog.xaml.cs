﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Depiction2.ViewDialogs.MapActionViews
{
    /// <summary>
    /// Interaction logic for DepictionPrintDialog.xaml
    /// </summary>
    public partial class DepictionPrintDialog
    {
        public DepictionPrintDialog()
        {
            InitializeComponent();
        }

        public bool IncludeMenus
        {
            get { return (bool)includeMenusCheckBox.IsChecked; }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
//            var printed = DoPrint(IncludeMenus);
//            if(printed)
//            {
//                var vm = DataContext as DialogViewModelBase;
//                if(vm != null) vm.IsDialogVisible = false;
//            }

        }

        protected bool DoPrint(bool printWithMenus)
        {
            // the following procedure has been derived from a forum post at
            // http://social.msdn.microsoft.com/Forums/en-US/wpf/thread/5e745b6e-5043-4b8d-86d5-f0a4a3639fd5/
            // I can't claim to quite understand why all of it's necessary.
            try
            {
                var dialog = new PrintDialog();
                if ((bool)dialog.ShowDialog())
                {
                    
                    var elementToPrint = FindPanelToPrint(printWithMenus);
                    
                    // Calculate the scale factor to fit the content to the page
                    const double margin = 48d;  // half an inch at 96 dots/inch
                    var paperSize = new Size(dialog.PrintableAreaWidth, dialog.PrintableAreaHeight);
                    var elementSize = new Size(elementToPrint.ActualWidth, elementToPrint.ActualHeight);
                    double useScale = ScaleToFit(paperSize, elementSize, margin);

                    // Create a transform to scale the visual
                    var transform = new TransformGroup();
                    transform.Children.Add(new ScaleTransform(useScale, useScale));
                    transform.Children.Add(new TranslateTransform(margin, margin));

                    // Temporarily hide the element's parent, so that the scaling doesn't show on the screen. Is this even needed?
                 //   var hideElement = elementToPrint.Parent as FrameworkElement;
                  //  if (hideElement != null) hideElement.Visibility = Visibility.Hidden;

                    // Add a border
//                    var border = new Border
//                                     {
//                                         BorderBrush = new SolidColorBrush(Colors.Black),
//                                         BorderThickness = new Thickness(1d / useScale),
//                                         SnapsToDevicePixels = true
//                                     };
//
//                    elementToPrint.Children.Add(border);
                    Panel depictionMap = null;
                    Brush mapBackgound = null;
                    if(elementToPrint is Canvas)
                    {
                        depictionMap = elementToPrint.Children[0] as Panel;
                    }
                    if(depictionMap != null)
                    {
                        mapBackgound = depictionMap.Background;
                        depictionMap.Background = Brushes.Transparent;
                    }
                   
                   
                    // Oddly, even though we're not using LayoutTransform, we still need to 
                    // call this.  If we don't, the RenderTransform doesn't get applied. 
                    // There was evidence to suggest this is a race condition with PrintVisual.
                    elementToPrint.UpdateLayout();
                    var vis = CreateDrawingVisualForPrinting(elementToPrint, transform);

                    try
                    {
                        dialog.PrintVisual(vis, "Depiction");//DepictionAccess.ProductInformation.ProductName);
                    }
                    catch (ArgumentNullException)
                    {
                        //eat exception that is thrown if user cancels printing to an .xps document
                    }
                    // Restore the scale and visibility

                    if (depictionMap != null)
                    {
                        depictionMap.Background = mapBackgound;
                    }
                   // elementToPrint.Background = background;
                    //elementToPrint.Children.Remove(border);

                    //if (hideElement != null) hideElement.Visibility = Visibility.Visible;
                    return true;
                }

            }
            catch (Exception ex)
            {
                //                    DepictionAccess.NotificationService.SendNotification(
                //                        String.Format("Could not print\nbecause {0}", ex.Message));
               
                return false;
            }
            return false;
        }
        private DrawingVisual CreateDrawingVisualForPrinting(FrameworkElement elmentToDraw, TransformGroup transform)
        {
            var elementSize = new Size(elmentToDraw.ActualWidth, elmentToDraw.ActualHeight);
            RenderTargetBitmap bmp = new RenderTargetBitmap((int)elmentToDraw.ActualWidth, (int)elmentToDraw.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(elmentToDraw);
            //create new visual which would be initialized by image
            DrawingVisual vis = new DrawingVisual();
            vis.Transform = transform;
            //create a drawing context so that image can be rendered to print
            DrawingContext dc = vis.RenderOpen();
            dc.DrawImage(bmp, new Rect(elementSize));
            dc.Close();
            
//            BitmapSaveLoadService.SaveBitmap(bmp, @"C:\DepictionSaveTests\testImage.jpg");

            return vis;
        }
        private Panel FindPanelToPrint(bool withMenu)
        {
            if (Owner == null) return null;
            if (!Owner.HasContent) return null;
            var content = Owner.Content as Panel;
            if (content == null) return null;
            var children = content.Children;
            foreach(UIElement child in children)
            {
                //Because of circualer dependecies the name will have to be used
                if(child.GetType().ToString().Contains("DepictionMapScreen"))
                {
                    var parentPanel = child as Panel;
                    if(withMenu) return parentPanel;
                    if(parentPanel == null) return null;
                    foreach(UIElement panel in parentPanel.Children)
                    {
                        if(panel.GetType().ToString().Contains("DepictionMapWithHelperDialogsView"))
                        {
                            var interiorPanel = panel as Panel;
                            if(interiorPanel == null) return panel as Panel;
                            foreach (UIElement other in interiorPanel.Children)
                            {
                                if(other is AdornerDecorator)
                                {
                                    var finalPanel = ((AdornerDecorator)other).Child as Panel;
                                    return finalPanel;
                                }
                            }
                        }
                    }
                }
            }


            return null;
        }
        /// <summary>
        /// Calculates the scale factor needed to shrink or expand an element to fit the paper.
        /// </summary>
        /// <param name="paperSize">Printable area of the paper</param>
        /// <param name="elementSize">Actual size of the element as rendered on the display</param>
        /// <param name="margin">Desired margin between printed content and paper edge</param>
        /// <returns>Factor by which to scale the element's width and height.</returns>
        private static double ScaleToFit(Size paperSize, Size elementSize, double margin)
        {
            double usablePaperWidth = paperSize.Width - margin * 2;
            double usablePaperHeight = paperSize.Height - margin * 2;
            double scaleX = usablePaperWidth / elementSize.Width;
            double scaleY = usablePaperHeight / elementSize.Height;
            return Math.Min(scaleX, scaleY);
        }
        
    }
}