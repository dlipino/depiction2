using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction2.ViewDialogs.Converters
{
    //Might be combinable with double converter
    public class DepictionIntToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int)) return value.ToString();
            int intValue = 0;
            if (value != null) intValue = (int)value;
            return string.Format("{0:f0}", intValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int result;
            if (int.TryParse((string)value, NumberStyles.Number, culture, out result))
                return result;
            return value;
        }
    }
}