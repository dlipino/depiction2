﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.ViewDialogs.Converters
{
    public class IntToVisualStateConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {  
            if (value == null) return value;
            return (int)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return value;
            return (ElementVisualSetting)value;
        }
    }
}