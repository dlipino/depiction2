﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Depiction2.ViewDialogs.Converters
{
    public class StringToSolidColorBrushConverter : IValueConverter
    {
        private BrushConverter _brushConverter = new BrushConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bc = _brushConverter.ConvertFrom(value);
            return bc;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var s = _brushConverter.ConvertToString(value);
            return s;
        }
    }
}