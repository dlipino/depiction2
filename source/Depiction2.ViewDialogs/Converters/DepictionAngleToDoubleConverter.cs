using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.API.Measurement;

namespace Depiction2.ViewDialogs.Converters
{
    public class DepictionAngleToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var angle = value as Angle;
            if (angle == null) return string.Empty;
            var doubleValue = angle.Value;
            return doubleValue - 90;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value + 90;
        }
    }
}