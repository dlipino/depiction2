﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction2.Core.ViewModels;

namespace Depiction2.ViewDialogs.Converters
{//This converter should eventually get killed off. Not needed because datatriggers can what this does (and do it better)
    public class StartScreenTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CurrentAppDepictionScreen)
            {
                var type = parameter.ToString();
                var screen = ((CurrentAppDepictionScreen)value).ToString().ToLower();
                if (type.ToLower().Contains(screen))
                {
                    return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CurrentAppDepictionScreen.Main;
        }
    }
}
