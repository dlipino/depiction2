using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.API.Properties;
using Depiction2.Base.Measurement;

namespace Depiction2.ViewDialogs.Converters
{
    public class DepictionIMeasurementToDoubleStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var measure = value as IMeasurement;
            if (measure == null) return string.Empty;
            var doubleValue = measure.NumericValue;
            return string.Format(string.Format("{{0:N{0}}}", Settings.Default.Precision), doubleValue);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double result;

            if (double.TryParse((string)value, NumberStyles.Any, culture, out result))
                return result;
            return double.NaN;
        }
    }
}