﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.Core.StoryEntities.Element;

namespace Depiction2.ViewDialogs.Converters
{
    public class StringToShapeTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bShape = ZOIShapeType.Unknown;
            Enum.TryParse(value.ToString(), true, out bShape); //TryParse(value.ToString(), true, out en);// 
            return bShape;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? string.Empty : value.ToString();
        }
    }
}