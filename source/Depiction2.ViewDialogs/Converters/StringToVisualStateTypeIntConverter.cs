﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.ViewDialogs.Converters
{
    public class StringToVisualStateTypeIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bShape = ElementVisualSetting.None;
            Enum.TryParse(value.ToString(), true, out bShape); //TryParse(value.ToString(), true, out en);// 
            return (int)bShape;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? string.Empty : value.ToString();
        }
    }
}