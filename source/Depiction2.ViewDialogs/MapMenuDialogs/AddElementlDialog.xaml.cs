﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Depiction2.API.Service;
using Depiction2.Core.ViewModels.Converters;

namespace Depiction2.ViewDialogs.MapMenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenuDialog.xaml
    /// </summary>
    public partial class AddElementlDialog
    {
        private List<TabItem> extensionTabs;
        public AddElementlDialog()
        {
            InitializeComponent();
            selectModalityTabControl.SelectionChanged += selectModalityTabControl_SelectionChanged;
            tabFile.Tag = true;
            tabMouse.Tag = true;
            tabQuickstart.Tag = false;
            extensionTabs = new List<TabItem>();
            var converter = new AddMethodToBoolConverter();
            //find the views for loaded extensions
            foreach (var extInfo in ExtensionService.Instance.SourceLoaders)
            {
                if (extInfo.Metadata.DisplayConfigInAddDialog)
                {
                    //danger, the view can only have one parent so if it is used again there will be issues.
                    var t = extInfo.Value.ElementAddConfigView as FrameworkElement;
                    if (t != null)
                    {
                        var meta = extInfo.Metadata;
                        var tabItem = new TabItem { Header = meta.ShortName, Content = t, Tag = meta.UsePredifinedType };
                        var itemsbinding = new Binding("AddElementMode")
                        {
                            Converter = converter,
                            ConverterParameter = meta.Name
                        };

                        tabItem.SetBinding(TabItem.IsSelectedProperty, itemsbinding);
                        selectModalityTabControl.Items.Add(tabItem);
                        extensionTabs.Add(tabItem);
                    }
                }
            }
            Closed += AddElementlDialog_Closed;
        }

        void selectModalityTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var addedItems = e.AddedItems as IList<object>;
            if (addedItems == null) return;
            if (addedItems.Count != 1) return;
            var selectedTab = addedItems[0] as TabItem;
            if (selectedTab == null) return;
            var tag = selectedTab.Tag;
            if (!(tag is bool)) return;

            var showElementsTag = (bool) tag;
            if(showElementsTag)
            {
                elementListPanel.Visibility = Visibility.Visible;
            }else
            {
                elementListPanel.Visibility = Visibility.Collapsed;
            }
        }


        void AddElementlDialog_Closed(object sender, System.EventArgs e)
        {
            //Semi hack. will stay in until the addelementdialog viewmodel does not get destroyed every new story.
            foreach (var extTab in extensionTabs)
            {
                extTab.Content = null;
                selectModalityTabControl.Items.Remove(extTab);
            }
            extensionTabs.Clear();

        }
    }
    //        private ICommand DeleteElementCommand
    //        {
    //            get
    //            {
    //                var deleteElementCommand = new DelegateCommand<IElementPrototype>(DeleteElementPrototype);
    //                return deleteElementCommand;
    //            }
    //        }
    //
    //        private void DeleteElementPrototype(IElementPrototype elementPrototype)
    //        {
    //            if (elementPrototype == null) return;
    //            MessageBoxResult res =
    //                DepictionMessageBox.ShowDialog(Application.Current.MainWindow, string.Format("Do you want to delete {0} from the element library?", elementPrototype.ElementType),
    //                                               "Confirm element definition deletion", MessageBoxButton.YesNo);
    //            if (res.Equals(MessageBoxResult.No))
    //            {
    //                return;
    //            }
    //            if (DepictionAccess.ElementLibrary != null)
    //            {
    //                DepictionAccess.ElementLibrary.DeleteElementPrototypeFromLibrary(elementPrototype);
    //                DepictionAccess.ElementLibrary.NotifyPrototypeLibraryChange();
    //            }
    //        }
    //        private ICommand MoveElementFromLoadedToUserDefinitionCommand
    //        {
    //            get
    //            {
    //                var moveElementCommand = new DelegateCommand<IElementPrototype>(MoveElementPrototypeFromLoadedToUser);
    //                return moveElementCommand;
    //            }
    //        }
    //
    //        private void MoveElementPrototypeFromLoadedToUser(IElementPrototype elementPrototype)
    //        {
    //            if (elementPrototype == null) return;
    //
    //            MessageBoxResult res =
    //                DepictionMessageBox.ShowDialog(Application.Current.MainWindow, string.Format("Do you want to move {0} to user definitions?", elementPrototype.ElementType),
    //                                               "Confirm element definition move", MessageBoxButton.YesNo);
    //            if (res.Equals(MessageBoxResult.No))
    //            {
    //                return;
    //            }
    //            if (DepictionAccess.ElementLibrary != null)
    //            {
    //                ElementDefinitionEditorDialogViewModel.SaveDefinitionToFile(elementPrototype, true);
    //                DepictionAccess.ElementLibrary.NotifyPrototypeLibraryChange();
    //            }
    //        }
    //        #endregion
    //
    //        #region Constructor
    //        public AddContentDialog()
    //        {
    //
    //            InitializeComponent();
    //            ShowInfoButton = true;
    //            AssociatedHelpFile = "addContent.html#top";
    //            DataContextChanged += AddContentDialog_DataContextChanged;
    //            SetLocationToStoredValue();
    //
    //            switch (DepictionAccess.ProductInformation.ProductType)
    //            {
    //                case ProductInformationBase.Prep:
    //                    selectModalityTabControl.Items.Remove(tabLiveReports);
    //                    selectModalityTabControl.Items.Remove(tabWebService);
    //                    FileStackPanel.Children.Remove(cropCheckBox);
    //                    break;
    //                case ProductInformationBase.DepictionRW:
    //                    selectModalityTabControl.Items.Remove(tabLiveReports);
    //                    selectModalityTabControl.Items.Remove(tabAddress);
    //                    selectModalityTabControl.Items.Remove(tabMouse);
    //                    FileStackPanel.Children.Remove(cropCheckBox);
    //                    break;
    //            }
    //        }
    //
    //        protected override void LocationOrSizeChanged_Updated()
    //        {
    //            if (!RestoreBounds.IsEmpty)
    //            {
    //                Settings.Default.AddContentDialogRestoreBounds = RestoreBounds;
    //            }
    //            base.LocationOrSizeChanged_Updated();
    //        }
    //
    //        public override void SetLocationToStoredValue()
    //        {
    //            if (!Settings.Default.AddContentDialogRestoreBounds.IsEmpty)
    //            {
    //                WindowStartupLocation = WindowStartupLocation.Manual;
    //                var bounds = Settings.Default.AddContentDialogRestoreBounds;
    //                Top = bounds.Top;
    //                Left = bounds.Left;
    //                Width = bounds.Width;
    //                Height = bounds.Height;
    //            }
    //            base.SetLocationToStoredValue();
    //        }
    //        #endregion
    //
    //        override protected void MainThreadShow(bool show)
    //        {
    //            var treeView = Part_ElementTypeTreeView;// ContentTemplate.FindName("Part_ElementTypeTreeView", contentPresenter) as TreeView;
    //            if (treeView != null)
    //            {
    //                treeView.SelectedItem = null;
    //            }
    //            if (quickStartDataList != null) quickStartDataList.SelectedItem = null;
    //            base.MainThreadShow(show);
    //        }
    //
    //        void AddContentDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    //        {
    //            var oldValue = e.OldValue as AddContentDialogVM;
    //            var newValue = e.NewValue as AddContentDialogVM;
    //            if (oldValue != null)
    //            {
    //                oldValue.PropertyChanged -= AddContentDialogVM_PropertyChanged;
    //            }
    //            if (newValue != null)
    //            {
    //                newValue.PropertyChanged += AddContentDialogVM_PropertyChanged;
    //            }
    //        }
    //
    //        void AddContentDialogVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    //        {
    //            if (e.PropertyName.Equals("AddMode"))
    //            {
    //                var addContent = sender as AddContentDialogVM;
    //                if (addContent == null) return;
    //                if (addContent.AddMode.Equals(AddContentMode.File) || addContent.AddMode.Equals(AddContentMode.LiveReport))
    //                {//TODO the find is not longer needed
    //                    var elementListBox = Part_ElementTypeTreeView;// ContentTemplate.FindName("Part_ElementTypeTreeView", contentPresenter) as TreeView;
    //                    if (elementListBox != null)
    //                    {
    //                        if (elementListBox.SelectedValue == null || elementListBox.SelectedValue.Equals(addContent.AutoDetectPrototypeViewModel.ElementType))
    //                        {
    //                            elementListBox.SelectedItem = addContent.AutoDetectPrototypeViewModel;
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //
    //        private void elementTypeListItem_MouseLeftClick(object sender, MouseButtonEventArgs e)
    //        {
    //            if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0)
    //            {
    //                var box = sender as ListBoxItem;
    //                if (box == null) return;
    //                var evm = box.DataContext as ElementPrototypeViewModel;
    //                if (evm == null) return;
    //                if (evm.IsPlaceableByMouse)
    //                {
    //                    if (API.Properties.Settings.Default.QuickAddElementList == null)
    //                    {
    //                        API.Properties.Settings.Default.QuickAddElementList = new StringCollection();
    //                        //Settings.Default.Save();
    //                    }
    //
    //                    var list = API.Properties.Settings.Default.QuickAddElementList;
    //
    //                    var name = evm.Model.ElementType;
    //                    var simpleNameList = name.Split('.');
    //                    var simpleName = simpleNameList[simpleNameList.Length - 1];
    //                    var change = false;
    //                    if (!list.Contains(name) && !list.Contains(simpleName))
    //                    {
    //                        list.Add(name);
    //                        change = true;
    //                    }
    //                    if (change)
    //                    {
    //                        API.Properties.Settings.Default.QuickAddElementList = list;
    //                        API.Properties.Settings.Default.Save();
    //                    }
    //                }
    //            }
    //        }
    //
    //        //This is a lie, it is used in the xaml as part of a style
    //        void elementTypeListItem_DoubleClick(object sender, MouseButtonEventArgs e)
    //        {
    //            var listBox = sender as ListBoxItem;
    //            if (listBox == null) return;
    //            var selectedItem = listBox.DataContext as ElementPrototypeViewModel;
    //            if (selectedItem == null) return;
    //            var dc = DataContext as AddContentDialogVM;
    //            if (dc == null) return;
    //            if (e.ChangedButton.Equals(MouseButton.Left))
    //            {
    //                if (dc.AddElementTypeBySelectedMethod.CanExecute(selectedItem))
    //                {
    //                    dc.AddElementTypeBySelectedMethod.Execute(selectedItem);
    //                    if (Settings.Default.CloseAddContentAfterAdd)
    //                    {
    //                        dc.IsDialogVisible = false;
    //                    }
    //                }
    //            }
    //        }
    //
    //        private void ElementPrototypeView_ContextMenuOpening(object sender, ContextMenuEventArgs e)
    //        {
    //            switch (DepictionAccess.ProductInformation.ProductType)
    //            {
    //                case ProductInformationBase.Prep:
    //                    return;
    //            }
    ////#if PREP
    ////            return;
    ////#endif
    //            var fe = sender as FrameworkElement;
    //            if (fe == null) return;
    //            var dc = fe.DataContext as ElementPrototypeViewModel;
    //            if (dc == null) return;
    //            var menu = new ContextMenu();
    //
    //            if (Owner != null)
    //            {
    //                var mainWindowDC = Owner.DataContext as DepictionAppViewModel;
    //                if (mainWindowDC != null)
    //                {
    //                    var clonable = dc.Model as IDeepCloneable;
    //                    if (clonable != null)
    //                    {
    //                        var exportElementItem = new MenuItem
    //                                                    {
    //                                                        Header = string.Format("Copy {0}", dc.DisplayName),
    //                                                        Command =
    //                                                            mainWindowDC.ElementDefinitionEditorVm.
    //                                                            LoadPrototypeFromLibraryCommand,
    //                                                        CommandParameter = clonable.DeepClone()
    //                                                    };
    //
    //                        menu.Items.Add(exportElementItem);
    //                        if (dc.DefinitionSource.Equals(ElementPrototypeLibrary.LoadedPrototypeDescription))
    //                        {
    //                            var moveElementItem = new MenuItem
    //                            {
    //                                Header = string.Format("Move {0} to user library", dc.DisplayName),
    //                                Command = MoveElementFromLoadedToUserDefinitionCommand,
    //                                CommandParameter = dc.Model
    //                            };
    //                            menu.Items.Add(moveElementItem);
    //                        }
    //                    }
    //                }
    //            }
    //            if (dc.DefinitionSource.Equals(ElementPrototypeLibrary.UserPrototypeDescription))
    //            {
    //                var deleteElementItem = new MenuItem { Header = string.Format("Delete {0}", dc.DisplayName), Command = DeleteElementCommand, CommandParameter = dc.Model };
    //                menu.Items.Add(deleteElementItem);
    //            }
    //
    //            fe.ContextMenu = menu;
    //            fe.ContextMenu.IsOpen = true;
    //        }
    //
    //        private void btnAdd_Click(object sender, RoutedEventArgs e)
    //        {
    //            var dc = DataContext as AddContentDialogVM;
    //            if (dc == null) return;
    //            if (Settings.Default.CloseAddContentAfterAdd)
    //            {
    //                //Hack because making the dialog invisibile clears the command from the button
    //                //so it cant get run after the button click
    //                var button = sender as Button;
    //                if (button != null)
    //                {
    //                    if (button.Name.Equals("btnElementAdd"))
    //                    {
    //                        button.Command.Execute(button.CommandParameter);
    //                    }else if(button.Name.EndsWith("btnAddQuickStart"))
    //                    {
    //                        button.Command.Execute(button.CommandParameter);
    //                    }
    //                }
    //                dc.IsDialogVisible = false;
    //            }
    //            e.Handled = false;
    //        }
    //    }
}
