﻿namespace Depiction2.ViewDialogs.MapMenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenuDialog.xaml
    /// </summary>
    public partial class FileMenuDialog 
    {
        public FileMenuDialog()
        {
            InitializeComponent();
            CommandConnector.InitMainWindowCommandBinding(this);  
        }
    }
}
