﻿namespace Depiction2.ViewDialogs.MapMenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenuDialog.xaml
    /// </summary>
    public partial class MapHelpDialog 
    {
        public MapHelpDialog()
        {
            InitializeComponent();
            CommandConnector.InitMainAppDialogToggleCommandBindings(this);
        }
    }
}
