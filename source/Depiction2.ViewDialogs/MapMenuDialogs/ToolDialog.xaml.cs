﻿namespace Depiction2.ViewDialogs.MapMenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenuDialog.xaml
    /// </summary>
    public partial class ToolDialog 
    {
        public ToolDialog()
        {
            InitializeComponent();
            CommandConnector.InitMainAppDialogToggleCommandBindings(this); 
        }
    }
}
