﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Depiction2.Core.ViewModels.DialogViewModels.App;

namespace Depiction2.ViewDialogs.Helpers
{
    /// <summary>
    /// So old that i don't even remember how this works anymore, from 1.4 and blindly ported over to 2.0
    /// </summary>
    public partial class DepictionMessageBox
    {
        private MessageBoxResult result = MessageBoxResult.None;
        private Button close;
        private FrameworkElement title;
        private bool isModal;


        #region Constructor

        public DepictionMessageBox()
        {
            InitializeComponent();
            Topmost = true;
//            Closing += DepictionMessageBox_Closing;
//            Closed += DepictionMessageBox_Closed;
            DataContextChanged += DepictionMessageBox_DataContextChanged;
        }

        void DepictionMessageBox_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDc = e.OldValue as MessageBoxDialogViewModel;
            if (oldDc != null)
            {
                oldDc.PropertyChanged -= DataContext_PropertyChanged;
            }
            var newDc = e.NewValue as MessageBoxDialogViewModel;
            if (newDc != null)
            {
                newDc.PropertyChanged += DataContext_PropertyChanged;
                MessageBoxButton = newDc.MessageBoxType;
                Title = newDc.Title;
            }


        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("MessageDialogBoxResult"))
            {
                var dc = DataContext as MessageBoxDialogViewModel;
                if (dc == null) return;
                var dialogBoxResult = dc.MessageDialogBoxResult;
                switch (dialogBoxResult)
                {
                    case MessageBoxResult.Cancel:
                        DialogResult = null;
                        Close();
                        break;
                    case MessageBoxResult.OK:
                    case MessageBoxResult.Yes:
                        DialogResult = true;
                        break;
                    case MessageBoxResult.No:
                    case MessageBoxResult.None:
                        DialogResult = false;
                        break;
                }
            }
        }

        #endregion

        #region Dependency Properties

        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            set { SetValue(MessageProperty, value); }
        }

        public static readonly DependencyProperty MessageProperty =
            DependencyProperty.Register("Message", typeof(string), typeof(DepictionMessageBox), new UIPropertyMetadata(string.Empty));


        public MessageBoxButton MessageBoxButton
        {
            get { return (MessageBoxButton)GetValue(MessageBoxButtonProperty); }
            set { SetValue(MessageBoxButtonProperty, value); }
        }

        public static readonly DependencyProperty MessageBoxButtonProperty =
            DependencyProperty.Register("MessageBoxButton", typeof(MessageBoxButton), typeof(DepictionMessageBox), new UIPropertyMetadata(MessageBoxButton.OK,MessageBoxButtonChange));

        private static void MessageBoxButtonChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var box = d as DepictionMessageBox;
            if (box == null) return;
            
            var boxType = (MessageBoxButton)e.NewValue;
            box._ok.IsEnabled = false;
            box._cancel.IsEnabled = false;
            box._no.IsEnabled = false;
            box._yes.IsEnabled = false;
            switch (boxType)
            {
                case MessageBoxButton.OK:
                    box._ok.IsEnabled = true;
                    box._ok.IsDefault = true;
                    break;
                case MessageBoxButton.OKCancel:
                    box._ok.IsEnabled = true;
                    box._ok.IsDefault = true;
                    box._cancel.IsEnabled = true;
                    box._cancel.IsCancel = true;
                    break;
                case MessageBoxButton.YesNo:
                    box._no.IsEnabled = true;

                    box._yes.IsEnabled = true;
                    box._yes.IsDefault = true;
                    break;
                case MessageBoxButton.YesNoCancel:
                    box._cancel.IsEnabled = true;
                    box._cancel.IsCancel = true;

                    box._no.IsEnabled = true;

                    box._yes.IsEnabled = true;
                    box._yes.IsDefault = true;
                    break;
            }
        }

        public MessageBoxImage MessageBoxImage
        {
            get { return (MessageBoxImage)GetValue(MessageBoxImageProperty); }
            set { SetValue(MessageBoxImageProperty, value); }
        }

        public static readonly DependencyProperty MessageBoxImageProperty =
            DependencyProperty.Register("MessageBoxImage", typeof(MessageBoxImage), typeof(DepictionMessageBox), new UIPropertyMetadata(MessageBoxImage.None));

        public SolidColorBrush TitleBackground
        {
            get { return (SolidColorBrush)GetValue(TitleBackgroundProperty); }
            set { SetValue(TitleBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TitleBackgroundProperty =
            DependencyProperty.Register("TitleBackground", typeof(SolidColorBrush), typeof(DepictionMessageBox), new UIPropertyMetadata(Brushes.Black));

        public SolidColorBrush TitleForeground
        {
            get { return (SolidColorBrush)GetValue(TitleForegroundProperty); }
            set { SetValue(TitleForegroundProperty, value); }
        }

        public static readonly DependencyProperty TitleForegroundProperty =
            DependencyProperty.Register("TitleForeground", typeof(SolidColorBrush), typeof(DepictionMessageBox), new UIPropertyMetadata(Brushes.White));


        #endregion

        #region Event Handlers

//        void DepictionMessageBox_Closing(object sender, System.ComponentModel.CancelEventArgs e)
//        {
//            if (DepictionMessageBoxResult.Equals(MessageBoxResult.None))
//            {
//                DepictionMessageBoxResult = DefaultResult;
//            }
//        }
//        void DepictionMessageBox_Closed(object sender, EventArgs e)
//        {
//            Mouse.OverrideCursor = null;
//        }

        private void DepictionMessageBox_Loaded(object sender, RoutedEventArgs e)
        {
            close = (Button)Template.FindName("PART_Close", this);
            if (null != close)
            {
                close.IsCancel = true;
                close.IsDefault = true;
                if(!isModal)
                {
                    close.Click += delegate
                                        {
                                            Close();
                                        };
                }
                //                if(removeClose)
                //                {
                //                    RemoveTopCloseButton();
                //                }

                //Why would anybody want to do this? more importantly what is it always false for me (davidl)
                //                if (false == _cancel.IsVisible)
                //                {
                //                    _close.IsCancel = false;
                //                }
            }

            title = (FrameworkElement)Template.FindName("PART_Title", this);
            if (null != title)
            {
                title.MouseLeftButtonDown += title_MouseLeftButtonDown;
            }
            Cursor = Cursors.Arrow;//not enough
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        #endregion

        #region Methods that are more Depiction specific

//        static protected MessageBoxResult GetCorrectDefault(MessageBoxButton messageBoxType)
//        {
//            switch (messageBoxType)
//            {
//                case MessageBoxButton.YesNoCancel:
//                case MessageBoxButton.OKCancel:
//                    return MessageBoxResult.Cancel;
//                case MessageBoxButton.YesNo:
//                    return MessageBoxResult.No;
//                case MessageBoxButton.OK:
//                    return MessageBoxResult.OK;
//            }
//            return MessageBoxResult.None;
//        }
        public static void Show(Window owner, string messageBoxText,
                                            string caption, MessageBoxButton button, MessageBoxImage icon)
        {
            var messageBox = new DepictionMessageBox();
            if (owner == null && Application.Current != null && Application.Current.MainWindow != null)
            {
                if (Application.Current.MainWindow.IsVisible) owner = Application.Current.MainWindow;
            }
//            messageBox.DefaultResult = GetCorrectDefault(button);
            messageBox.Title = caption;
            messageBox.Owner = owner;
            messageBox.Message = messageBoxText;
            messageBox.MessageBoxButton = button;
            messageBox.MessageBoxImage = icon;

            messageBox.Show();
        }

//        public static MessageBoxResult ShowDialog(Window owner, string messageBoxText,
//                                            string caption, MessageBoxButton button, MessageBoxImage icon,
//                                            MessageBoxResult defaultResult)
//        {
//            var messageBox = new DepictionMessageBox();
//            messageBox.isModal = true;
//            messageBox.Title = caption;
//            messageBox.DefaultResult = defaultResult;
//            if (defaultResult.Equals(MessageBoxResult.None))
//            {
//                messageBox.DefaultResult = GetCorrectDefault(button);
//            }
//            messageBox.Owner = owner;
//            if(owner == null)
//            {
//                messageBox.WindowStartupLocation = WindowStartupLocation.CenterScreen;
//            }
//            messageBox.Message = messageBoxText;
//            messageBox.MessageBoxButton = button;
//            messageBox.MessageBoxImage = icon;
//            messageBox.ShowDialog();
//
//            return messageBox.DepictionMessageBoxResult;
//        }
        #endregion

        #region static methods for showinga dialog


        public static void ShowOk(string messageBoxText, string caption)
        {
            Show(null, messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.None);
        }
        public static void ShowOk(Window owner, string messageBoxText, string caption)
        {
            Show(null, messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.None);
        }

        #endregion

        #region Static methods for showing a modal dialog
//        public static MessageBoxResult ShowDialog(string messageBoxText)
//        {
//            return ShowDialog(null, messageBoxText, string.Empty,
//                        MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(string messageBoxText, string caption)
//        {
//            return ShowDialog(null, messageBoxText, caption, MessageBoxButton.OK,
//                        MessageBoxImage.None, MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(Window owner, string messageBoxText)
//        {
//            return ShowDialog(owner, messageBoxText, string.Empty, MessageBoxButton.OK,
//                        MessageBoxImage.None, MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(string messageBoxText, string caption, MessageBoxButton button)
//        {
//            return ShowDialog(null, messageBoxText, caption, button, MessageBoxImage.None,
//                        MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(Window owner, string messageBoxText, string caption)
//        {
//            return ShowDialog(owner, messageBoxText, caption, MessageBoxButton.OK,
//                        MessageBoxImage.None, MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(string messageBoxText, string caption,
//                                            MessageBoxButton button, MessageBoxImage icon)
//        {
//            return ShowDialog(null, messageBoxText, caption, button, icon,
//                        MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(Window owner, string messageBoxText, string caption,
//                                            MessageBoxButton button)
//        {
//            return ShowDialog(owner, messageBoxText, caption, button,
//                        MessageBoxImage.None, MessageBoxResult.None);
//        }
//
//        public static MessageBoxResult ShowDialog(string messageBoxText, string caption,
//                                            MessageBoxButton button, MessageBoxImage image, MessageBoxResult defaultResult)
//        {
//            return ShowDialog(null, messageBoxText, caption, button, image, defaultResult);
//        }
//
//        public static MessageBoxResult ShowDialog(Window owner, string messageBoxText, string caption,
//                                            MessageBoxButton button, MessageBoxImage icon)
//        {
//            return ShowDialog(owner, messageBoxText, caption, button, icon, MessageBoxResult.None);
//        }
        #endregion
        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)//Resharper LIES!!!!
        {
            Process.Start(e.Uri.ToString());

        }
    }
}