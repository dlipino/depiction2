﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate.ViewModel;

namespace Depiction2.ViewDialogs.Helpers
{
    public partial class ElementTemplateLibraryViewer
    {
        public ElementTemplateLibraryViewer()
        {
            InitializeComponent();
        }

        private void ListBoxItem_LeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is ListBoxItem)
            {
                var draggedItem = sender as ListBoxItem;
                draggedItem.IsSelected = true;
                var dc = draggedItem.DataContext as ElementTemplateListViewModel;
                if (dc == null) return;
                if (!dc.TemplateModel.IsMouseAddable) return;
                var dragData = new DataObject(typeof (IElementTemplate), dc.TemplateModel);
                DragDrop.DoDragDrop(draggedItem, dragData, DragDropEffects.Move);
            }
        }
    }
}
