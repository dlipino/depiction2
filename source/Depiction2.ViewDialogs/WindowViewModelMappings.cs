﻿using System;
using System.Collections.Generic;
using Depiction2.Core.Service.WindowViewModelMapping;
using Depiction2.Core.ViewModels;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.Core.ViewModels.DialogViewModels.App;
using Depiction2.Core.ViewModels.DialogViewModels.Map;
using Depiction2.Core.ViewModels.DialogViewModels.Story;
using Depiction2.Core.ViewModels.ElementTemplates;
using Depiction2.Core.ViewModels.Interactions;
using Depiction2.ViewDialogs.AppDialogs;
using Depiction2.ViewDialogs.Helpers;
using Depiction2.ViewDialogs.LibraryDialogs;
using Depiction2.ViewDialogs.MapMenuDialogs;
using Depiction2.ViewDialogs.PropertyDialogs;
using Depiction2.ViewDialogs.StoryDialogs;

namespace Depiction2.ViewDialogs
{
    /// <summary>
    /// Class describing the Window-ViewModel mappings used by the dialog service.
    /// </summary>
    public class WindowViewModelMappings : IWindowViewModelMappings
    {
        private IDictionary<Type, Type> mappings;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowViewModelMappings"/> class.
        /// </summary>
        public WindowViewModelMappings()
        {
            mappings = new Dictionary<Type, Type>
			{
        ////{ typeof(AppViewModel), typeof(string) } ////,
//        { typeof(UsernameViewModel), typeof(UsernameDialog)}
                {typeof(EulaDialogVM),typeof(EulaDialog)},
                {typeof(AboutDialogVM),typeof(AboutDialog)},
                {typeof(DepictionSettingsDialogVM),typeof(DepictionSettingsDialog)},
                {typeof(DepictionExtensionDisplayerDialogVm),typeof(DepictionExtensionDisplayerDialog)},
                {typeof(AddElementDialogVM),typeof(AddElementlDialog)},
                {typeof(ToolsDialogVM),typeof(ToolDialog)},
                {typeof(StoryInformationDialogViewModel),typeof(StoryInformationDialog)},
                {typeof(ElementDisplayControlDialogVM),typeof(ElementDisplayControlDialog)},
                {typeof(ManageElementsDialogVM),typeof(ManageElementslDialog)},
                {typeof(MapHelpDialogVM),typeof(MapHelpDialog)},
                {typeof(AnnotationEditDialogVM),typeof(AnnotationPropertiesDialog)},
                {typeof(SingleElementPropertiesDialogVm),typeof(SingleElementPropertiesDialog)},
                {typeof(MultipleElementPropertiesDialogViewModel),typeof(MultipleElementPropertiesDialog)},
                {typeof(FileMenuDialogVM),typeof(FileMenuDialog)},
                {typeof(MessageBoxDialogViewModel),typeof(DepictionMessageBox)},
                {typeof(ElementTemplateExplorerDialogViewModel),typeof(ElementTemplateLibraryExplorerDialog)},
                {typeof(ElementTemplateEditorDialogViewModel),typeof(ElementTemplateEditorDialog)},
                {typeof(InteractionLibraryDialogViewModel),typeof(InteractionLibraryDialog)},
                {typeof(InteractionEditorDialogViewModel),typeof(InteractionEditorDialog)},
                {typeof(InteractionRouterControllerDialogViewModel),typeof(InteractionRouterControllerDialog)}
			};
        }

        /// <summary>
        /// Gets the window type based on registered ViewModel type.
        /// </summary>
        /// <param name="viewModelType">The type of the ViewModel.</param>
        /// <returns>The window type based on registered ViewModel type.</returns>
        public Type GetWindowTypeFromViewModelType(Type viewModelType)
        {
            return mappings[viewModelType];
        }
    }
}