﻿using System.IO;
using Depiction2.API;
using Depiction2.TempExtensionCore.Interfaces;

namespace Depiction.Views
{
    /// <summary>
    /// Interaction logic for ImporterExtensionWindow.xaml
    /// </summary>
    public partial class SomeWindowWhichDoesSomethingMaybe
    {
        static private int clickCount = 0;
        static string[] fileNames = new[]
                                {
                                    @"D:\DepictionData\WorldOutLine\countries.shp",
                                    @"D:\DepictionData\us_state_bounds\state_bounds.shp"
                                };
        public SomeWindowWhichDoesSomethingMaybe()
        {
            InitializeComponent();
            //For now 
            DataContext = this;
//            ImporterExtensionList.ItemsSource = DepictionAccess.ExtensionLibrary.ImporterExtensionArray;
        }

        private void DisplayImporterViewBttn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var selectedExtension = ImporterExtensionList.SelectedValue as IDepictionImporterExtension;
            if (selectedExtension == null) return;
//
            var windowToDisplay = selectedExtension.GetSettingsWindow();
            if (windowToDisplay != null) windowToDisplay.ShowDialog();
//            LoadFiles(selectedExtension);

        }
        static public void LoadFiles(IDepictionImporterExtension selectedExtension)
        {
            var fileName = fileNames[clickCount % 2];
            if (File.Exists(fileName))
            {
                selectedExtension.ImportElements(fileName);
            }
            else
            {
                var windowToDisplay = selectedExtension.GetSettingsWindow();
                if (windowToDisplay != null) windowToDisplay.ShowDialog();
            }
            clickCount++;
        }
    }
}
