﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources;

namespace Depiction2.ViewDialogs.PropertyDialogs
{
    /// <summary>
    /// Interaction logic for ElementPropertiesDialog.xaml
    /// </summary>
    public partial class SingleElementPropertiesDialog
    {
        static public RoutedUICommand DeleteElementsCommand = new RoutedUICommand("Delete all", "DeleteElementsCommand", typeof(SingleElementPropertiesDialog));

        public SingleElementPropertiesDialog()
        {
            InitializeComponent();
            // This transform group is required for the storyboard to work right
            var transformGroup = new TransformGroup();
            transformGroup.Children.Add(new ScaleTransform { ScaleX = 1, ScaleY = 1, CenterX = .5, CenterY = .5 });
            transformGroup.Children.Add(new SkewTransform { AngleX = 0, AngleY = 0 });
            transformGroup.Children.Add(new RotateTransform { Angle = 0 });
            transformGroup.Children.Add(new TranslateTransform { X = 0, Y = 0 });
            RenderTransform = transformGroup;
            //End transformgroup
            ShowInfoButton = true;
            AssociatedHelpFile = "elementInformation.html";
            var commandBinding = new CommandBinding(VisualPropTemplateDataTemplates.SelectColorCommand, VisualPropTemplateDataTemplates.SelectColor);
            CommandBindings.Add(commandBinding);
            commandBinding = new CommandBinding(VisualPropTemplateDataTemplates.SelectElementIconCommand, VisualPropTemplateDataTemplates.SelectElementIcon);
            CommandBindings.Add(commandBinding);

            DataContextChanged += ElementPropertiesControl_DataContextChanged;

        }

        //Doesn't work well enough yet
        //        private Storyboard burpStoryboard;
        //        public void DoFocusVisualStoryboard()
        //        {
        //            if (burpStoryboard == null)
        //            {
        //                if(string.IsNullOrEmpty(Name))
        //                {
        //                    Name = "elementPropertyDialog";
        //                }
        //                RegisterName(Name, this);
        //                var duration = new Duration(TimeSpan.FromMilliseconds(75));
        //                var myDoubleAnimationX = new DoubleAnimation
        //                {
        //                    From = 1.0,
        //                    To = 1.05,
        //                    Duration = duration,
        //                    AutoReverse = true
        //                };
        //                var myDoubleAnimationY = new DoubleAnimation
        //                {
        //                    From = 1.0,
        //                    To = 1.05,
        //                    Duration = duration,
        //                    AutoReverse = true
        //                };
        //
        //                // Create the storyboard.
        //                burpStoryboard = new Storyboard();
        //                burpStoryboard.Children.Add(myDoubleAnimationX);
        //                burpStoryboard.Children.Add(myDoubleAnimationY);
        //                Storyboard.SetTargetName(myDoubleAnimationX, Name);
        //                Storyboard.SetTargetName(myDoubleAnimationY, Name);
        //
        //                var myPropertyPathX =
        //                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)");
        //                Storyboard.SetTargetProperty(myDoubleAnimationX, myPropertyPathX);
        //                var myPropertyPathY =
        //                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)");
        //                Storyboard.SetTargetProperty(myDoubleAnimationY, myPropertyPathY);
        //            }
        //            burpStoryboard.Begin(this, false);
        //        }

        void ElementPropertiesControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //            var dcOld = e.OldValue as ElementPropertyDialogContentVM;
            //
            //            var dc = e.NewValue as ElementPropertyDialogContentVM;
            //            if (dcOld != null)
            //            {
            //                dcOld.Dispose();
            //            }
            //            if (dc == null) return;
            //            if (dc.OriginalElementIds.Count == 1)
            //            {
            //                DeleteElementsCommand.Text = "Delete";
            //                multipeElementText.Visibility = Visibility.Collapsed;
            //            }
            //            else
            //            {
            //                DeleteElementsCommand.Text = "Delete all";
            //            }
        }
    }
}
