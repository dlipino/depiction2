﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API.Measurement;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.Element;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    public partial class ReadOnlyPropertyTemplateResourceLibrary
    {
        private void angleThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = (FrameworkElement)sender;
            //            var dataContext = (GenericPropertyViewModel)thumb.DataContext;
            var canvas = (FrameworkElement)thumb.Parent;
            var parentCanvas = (FrameworkElement)canvas.Parent;

            Point currentLocation = Mouse.GetPosition(parentCanvas);
            //Mouse.GetPosition(this)

            // We want to rotate around the center of the knob, not the top corner 
            Point knobCenter = new Point(parentCanvas.ActualHeight / 2, parentCanvas.ActualWidth / 2);

            // Calculate an angle 
            double radians = Math.Atan((currentLocation.Y - knobCenter.Y) /
                                       (currentLocation.X - knobCenter.X));
            var angle = radians * 180 / Math.PI;

            // Apply a 180 degree shift when X is negative so that we can rotate 
            // all of the way around 
            if (currentLocation.X - knobCenter.X < 0)
            {
                angle += 180;
            }
            ((RotateTransform)canvas.RenderTransform).Angle = angle;
            //            dataContext.ValueChanged = true;
        }
    }
    public class ReadonlyPropertyTemplateSelector:DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var containerElement = (FrameworkElement)container;
            DataTemplate template = null;

            dynamic prop = item as PropertyTemplateViewModel;


            if (prop == null) return base.SelectTemplate(item, container);
            var propName = ((PropertyTemplateViewModel)item).ProperName;

            //By name 
            if (PropertyInformation.BoolProps.Contains(propName))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyBooleanDataTemplateConverter");
            }
            else if (propName.Equals("IconSize", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyIconSizeDataTemplateConverter");
            }
            else if (propName.Equals("VisualState", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyVisualStateDataTemplateConverter");
            }
            else if (propName.Equals("IconResourceName", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyDepictionIconPathDataTemplateConverter");
            }
            else if (propName.Equals("IconBorderShape", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyIconShapeDataTemplateConverter");
            }
            else if (propName.Equals("IconBorderColor", StringComparison.InvariantCultureIgnoreCase) ||
                propName.Equals("ZOIFillColor", StringComparison.InvariantCultureIgnoreCase) ||
                propName.Equals("ZOIBorderColor", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyColorDataTemplateConverter");
            }
            else if (propName.Equals("ZOILineThickness", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyDoubleDataTemplateConverter");
            }
            else if (propName.Equals("ZOIShapeType", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyZOIShapeDataTemplateConverter");
            }
            else
            {
                template = (DataTemplate)containerElement.FindResource("ReadOnlyStringDataTemplate");
            }
            if (template != null) return template;
            return base.SelectTemplate(item, container);
        }
    }
}
