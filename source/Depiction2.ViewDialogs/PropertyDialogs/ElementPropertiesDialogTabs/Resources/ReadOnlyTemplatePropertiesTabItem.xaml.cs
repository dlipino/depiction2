﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Depiction2.ViewDialogs.Helpers;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    /// <summary>
    /// Interaction logic for ListViewEnhanced.xaml
    /// </summary>
    public partial class ReadOnlyTemplatePropertiesTabItem
    {
        private GridViewColumnHeader _CurSortCol = null;
        private SortAdorner _CurAdorner = null;

        public bool CanControlHovertext
        {
            get { return (bool)GetValue(CanControlHovertextProperty); }
            set { SetValue(CanControlHovertextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanControlHovertext.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanControlHovertextProperty =
            DependencyProperty.Register("CanControlHovertext", typeof(bool), typeof(ReadOnlyTemplatePropertiesTabItem), new UIPropertyMetadata(true));

        public IEnumerable PropertiesSource
        {
            get { return (IEnumerable)GetValue(PropertiesSourceProperty); }
            set { SetValue(PropertiesSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PropertiesSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PropertiesSourceProperty =
            DependencyProperty.Register("PropertiesSource", typeof(IEnumerable), typeof(ReadOnlyTemplatePropertiesTabItem), new UIPropertyMetadata(null));

        public ReadOnlyTemplatePropertiesTabItem()
        {
            InitializeComponent();


            
        }

       

        //Duplicate of prototypeeditordialogwindow
        private void PropertySettingSortClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            if (column == null) return;
            String field = column.Tag.ToString();// as String;
            var propertySelectorListView = elementPropertiesListView;

            if (propertySelectorListView == null) return;
            if (_CurSortCol != null)
            {
                if (_CurAdorner != null)
                {
                    AdornerLayer.GetAdornerLayer(_CurSortCol).Remove(_CurAdorner);
                }
                propertySelectorListView.Items.SortDescriptions.Clear();
            }

            if (field.Equals("Reset"))
            {
                return;
            }
            ListSortDirection newDir = ListSortDirection.Ascending;
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Descending))
            {
                _CurAdorner = null;
                return;
            }
            if (_CurSortCol == column && _CurAdorner != null && _CurAdorner.Direction.Equals(ListSortDirection.Ascending))
            {
                newDir = ListSortDirection.Descending;
            }

            _CurSortCol = column;
            _CurAdorner = new SortAdorner(_CurSortCol, newDir);
            AdornerLayer.GetAdornerLayer(_CurSortCol).Add(_CurAdorner);
            propertySelectorListView.Items.SortDescriptions.Add(new SortDescription(field, newDir));
        }
    }
}
