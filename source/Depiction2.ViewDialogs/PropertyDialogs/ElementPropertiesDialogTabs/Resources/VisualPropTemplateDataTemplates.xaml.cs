﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;
using Depiction2.ViewDialogs.IconAndColorDialogs;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    public partial class VisualPropTemplateDataTemplates
    {
        public static RoutedUICommand SelectColorCommand = new RoutedUICommand("Change backgound", "ChangeBackgroundCommand", typeof(Button));
        public static RoutedUICommand SelectButtonColorCommand = new RoutedUICommand("Change button background", "ButtonBackgroundChange", typeof(Button));
        public static RoutedUICommand SelectElementIconCommand = new RoutedUICommand("Select icon", "SelectElementIconCommand", typeof(Button));

        static public void SelectColor(object sender, ExecutedRoutedEventArgs e)
        {
            var visualVm = e.Parameter as PropertyTemplateViewModel;
            if (visualVm == null) return;
            var color = ColorConverter.ConvertFromString(visualVm.PropertyValue.ToString());
            if (color == null) return;
            var result = ColorPickerDialog.PickColorModal(new SolidColorBrush((Color)color));
            if (result == null) return;
            var stringResult = result.ToString();
            visualVm.PropertyValue = stringResult;
        }


        static public void SelectElementIcon(object sender, ExecutedRoutedEventArgs e)
        {
            var visualVm = e.Parameter as PropertyTemplateViewModel;
            if (visualVm == null) return;

            var result = IconSelectorDialog.SelectIconSource(visualVm.PropertyValue.ToString());
            if (string.IsNullOrEmpty(result)) return;
            visualVm.PropertyValue = result;
        }
    }
}
