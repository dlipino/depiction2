﻿using System.Collections.Generic;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    public partial class ConverterDictionary
    {
        static public Dictionary<int, string> GetVisualSettings()
        {
            return new Dictionary<int, string>
                       {
                           {(int)ElementVisualSetting.Icon, "Icon"},
                           {(int)ElementVisualSetting.Geometry, "Geometry"},
                           {(int)ElementVisualSetting.Image, "Image"},
                           {(int)(ElementVisualSetting.Icon|ElementVisualSetting.Geometry),"Icon and Geometry"},
                           {(int)(ElementVisualSetting.Icon|ElementVisualSetting.Image),"Icon and Image"}
                       };
        }

        static public Dictionary<double, string> GetIconSizeNames()
        {
            return new Dictionary<double, string>
                       {
                           { 12d,"Lilliputian" }, 
                           { 16d,"Tiny"} , 
                           { 20d,"Small" }, 
                           { 24d,"Medium" }, 
                           { 32d, "Large"}, 
                           { 38d,"Extra-Large"}
                       };
        }
    }

}
