﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API.Measurement;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    /// <summary>
    /// Interaction logic for ElementPropertiesControlResourceLibrary.xaml
    /// </summary>
    public partial class ElementPropertiesControlResourceLibrary
    {
        #region Static helpers

//        static public void SelectElementIcon(object sender, ExecutedRoutedEventArgs e)
//        {
//            var button = e.OriginalSource as Button;
//            if (button == null) return;
//            var bDC = button.DataContext as MenuElementPropertyViewModel;
//            if (bDC == null) return;
//            var fe = sender as FrameworkElement;
//            if (fe == null) return;
//            var dc = fe.DataContext as ElementPropertyDialogContentVM;
//            if (dc != null)
//            {
//                var result = IconSelectorDialog.SelectIconSource(e.Parameter as DepictionIconPath);
//                if (result != null)
//                {
//                    dc.UnifiedIconPath = result;
//                    bDC.PropertyValue = result;
//                }
//                return;
//            }
//            var otherDc = fe.DataContext as ElementDefinitionEditorDialogViewModel;
//            if (otherDc != null)
//            {
//                var result = IconSelectorDialog.SelectIconSource(e.Parameter as DepictionIconPath);
//                if (result != null)
//                {
//                    bDC.PropertyValue = result;
//                }
//            }
//        }

        //        static public void ChangeButtonBackground(object sender, ExecutedRoutedEventArgs e)
        //        {
        //            var button = e.OriginalSource as Button;
        //            if (button == null) return;
        //
        //            Color finalColor;
        //            if (e.Parameter != null && e.Parameter is Color)
        //            {
        //                var fe = sender as FrameworkElement;
        //                if (fe == null) return;
        //                var dc = fe.DataContext as ElementPropertyDialogContentVM;
        //                if (dc == null) return;
        //                var color = (Color)e.Parameter;
        //                var name = button.Name;
        //                var result = ColorPickerDialog.PickColorModal(new SolidColorBrush(color));
        //                if (result == null) return;
        //                finalColor = result.Color;
        //                if (name.Contains("startColor"))
        //                {
        //                    dc.ThematicMapStartColor = finalColor;
        //                }
        //                else if (name.Contains("endColor"))
        //                {
        //                    dc.ThematicMapEndColor = finalColor;
        //                }
        //            }
        //            else
        //            {
        //                var currentColor = button.Background as SolidColorBrush;
        //                var result = ColorPickerDialog.PickColorModal(currentColor);
        //                if (result == null) return;
        //                var dc = button.DataContext as MenuElementPropertyViewModel;
        //                if (dc == null) return;
        //                dc.PropertyValue = result.Color;
        //            }
        //        }

        #endregion

//        public object IconShapeEnums
//        {
//            get
//            {
//                return Enum.GetValues(typeof(DepictionIconBorderShape));
//            }
//        }

        private void angleThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = (FrameworkElement)sender;
//            var dataContext = (GenericPropertyViewModel)thumb.DataContext;
            var canvas = (FrameworkElement)thumb.Parent;
            var parentCanvas = (FrameworkElement)canvas.Parent;

            Point currentLocation = Mouse.GetPosition(parentCanvas);
            //Mouse.GetPosition(this)

            // We want to rotate around the center of the knob, not the top corner 
            Point knobCenter = new Point(parentCanvas.ActualHeight / 2, parentCanvas.ActualWidth / 2);

            // Calculate an angle 
            double radians = Math.Atan((currentLocation.Y - knobCenter.Y) /
                                       (currentLocation.X - knobCenter.X));
            var angle = radians * 180 / Math.PI;

            // Apply a 180 degree shift when X is negative so that we can rotate 
            // all of the way around 
            if (currentLocation.X - knobCenter.X < 0)
            {
                angle += 180;
            }
            ((RotateTransform)canvas.RenderTransform).Angle = angle;
//            dataContext.ValueChanged = true;
        }
    }

    public class GenericPropertyTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var containerElement = (FrameworkElement) container;
            DataTemplate template;
            //            //The other alternative is to place these things in the app

//            if (!(item is GenericPropertyViewModel) || item==null) return base.SelectTemplate(item, container);
//            item = ((GenericPropertyViewModel)item).PropertyValue;
            if (!(item is PropertyTemplateViewModel) || item == null) return base.SelectTemplate(item, container);
            item = ((PropertyTemplateViewModel)item).PropertyValue;
            if (item is IMeasurement)
            {
                template = (DataTemplate)containerElement.FindResource("MeasurementDataTemplate");
            }
            else
                if (item is int)
                {
                    template = (DataTemplate)containerElement.FindResource("IntegerDataTemplate");
                }
                else if (item is Angle)
                {
                    template = (DataTemplate)containerElement.FindResource("AngleDataTemplate");
                }
                else if (item is double)
                {
                    template = (DataTemplate)containerElement.FindResource("DoubleDataTemplate");
                }
                else if (item is string)
                {
                    template = (DataTemplate)containerElement.FindResource("StringDataTemplate");
                }
                else if (item is bool)
                {
                    template = (DataTemplate)containerElement.FindResource("BooleanDataTemplate");
                }
                else if (item is IDepictionLatitudeLongitude)
                {
                    template = (DataTemplate)containerElement.FindResource("LatLongDataTemplate");
                }
                else
                {
                    if (item is Image)
                    {
                        template = base.SelectTemplate(item, container);
                    }
                    else
                    {
                        template = (DataTemplate)containerElement.FindResource("StringTemplate");
                    }
                }
            return template;
        }
    }


    //    public class HoverTextSelectingTemplateSelector : DataTemplateSelector
    //    {
    //        public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //        {
    //            if (item is CollectionViewGroup)
    //                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("HoverTextSelectingTreeTemplate");
    //            if (item is MenuElementPropertyViewModel)
    //            {
    //                return (DataTemplate)((FrameworkElement)container).FindResource("HoverTextSelectingDataTemplate");
    //            }
    //            return base.SelectTemplate(item, container);
    //        }
    //    }

    //    public class PropertyViewTemplateSelector : DataTemplateSelector
    //    {
    //        public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //        {
    //            if (item is CollectionViewGroup)
    //                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("PropertyChangingTreeTemplate");
    //            if (item is MenuElementPropertyViewModel)
    //            {
    //                return (DataTemplate)((FrameworkElement)container).FindResource("PropertyDataTemplate");
    //            }
    //            return base.SelectTemplate(item, container);
    //        }
    //    }

       

}
