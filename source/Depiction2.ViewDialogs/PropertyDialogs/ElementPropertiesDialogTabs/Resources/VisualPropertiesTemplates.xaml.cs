﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;
using Depiction2.ViewDialogs.IconAndColorDialogs;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    /// <summary>
    /// Interaction logic for ElementSelectionAndSortingControl.xaml
    /// </summary>
    public partial class VisualPropertiesTemplates
    {
        public static RoutedUICommand SelectColorCommand = new RoutedUICommand("Change backgound", "ChangeBackgroundCommand", typeof(Button));
        public static RoutedUICommand SelectButtonColorCommand = new RoutedUICommand("Change button background", "ButtonBackgroundChange", typeof(Button));
        public static RoutedUICommand SelectElementIconCommand = new RoutedUICommand("Select icon", "SelectElementIconCommand", typeof(Button));



        //        public static readonly Dictionary<DepictionIconSize, double> AvailableIconSizes = new Dictionary<DepictionIconSize, double>
        //                                                                                              {
        //                                                                   { DepictionIconSize.Lilliputian,12d }, 
        //                                                                   { DepictionIconSize.Tiny, 16d} , 
        //                                                                   {DepictionIconSize.Small, 20d }, 
        //                                                                   { DepictionIconSize.Medium, 24d }, 
        //                                                                   {DepictionIconSize.Large, 32d }, 
        //                                                                   { DepictionIconSize.ExtraLarge, 38d }
        //                                                               };

        static public Dictionary<int, string> GetVisualSettings()
        {
            return new Dictionary<int, string>
                                        {
                                            {(int)ElementVisualSetting.Icon, "Icon"},
                                            {(int)ElementVisualSetting.Geometry, "Geometry"},
                                            {(int)ElementVisualSetting.Image, "Image"},
                                            {(int)(ElementVisualSetting.Icon|ElementVisualSetting.Geometry),"Icon and Geometry"},
                                            {(int)(ElementVisualSetting.Icon|ElementVisualSetting.Image),"Icon and Image"}
                                        };
        }
        static public Dictionary<double, string> GetIconSizeNames()
        {
            return new Dictionary<double, string>
                       {
                            { 12d,"Lilliputian" }, 
                            { 16d,"Tiny"} , 
                            { 20d,"Small" }, 
                            { 24d,"Medium" }, 
                            { 32d, "Large"}, 
                            { 38d,"Extra-Large"}
                        };
        }

        static public void SelectColor(object sender, ExecutedRoutedEventArgs e)
        {
            var visualVm = e.Parameter as PropertyTemplateViewModel;
            if (visualVm == null) return;
            var color = ColorConverter.ConvertFromString(visualVm.PropertyValue.ToString());
            if (color == null) return;
            var result = ColorPickerDialog.PickColorModal(new SolidColorBrush((Color)color));
            if (result == null) return;
            var stringResult = result.ToString();
            visualVm.PropertyValue = stringResult;
        }


        static public void SelectElementIcon(object sender, ExecutedRoutedEventArgs e)
        {
            var visualVm = e.Parameter as PropertyTemplateViewModel;
            if (visualVm == null) return;

            var result = IconSelectorDialog.SelectIconSource(visualVm.PropertyValue.ToString());
            if (string.IsNullOrEmpty(result)) return;
            visualVm.PropertyValue = result;
        }

    }
    public class VisualPropertyTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (!(item is PropertyTemplateViewModel)) return base.SelectTemplate(item, container);
            var containerElement = (FrameworkElement)container;
            DataTemplate template = null;

            var propName = ((PropertyTemplateViewModel)item).ProperName;

            //By name 
            if (propName.Equals("IsDraggable", StringComparison.InvariantCultureIgnoreCase) ||
                propName.Equals("IsGeometryEditable", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("BooleanDataTemplate");
            }
            else if (propName.Equals("DisplayInformationText", StringComparison.InvariantCultureIgnoreCase) ||
                     propName.Equals("UseEnhancedInformationText", StringComparison.InvariantCultureIgnoreCase) ||
                     propName.Equals("UsePropertyNamesInInformationText", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("BooleanDataTemplate");
            }
            else if (propName.Equals("IconSize", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("IconSizeDataTemplate");
            }
            else if (propName.Equals("IconResourceName", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("DepictionIconPathDataTemplate");
            }
            else if (propName.Equals("IconBorderShape", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("IconShapeDataTemplate");
            }
            else if (propName.Equals("IconBorderColor", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ColorDataTemplate");
            }
            else if (propName.Equals("ZOIFillColor", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ColorDataTemplate");
            }
            else if (propName.Equals("ZOIBorderColor", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ColorDataTemplate");
            }
            else if (propName.Equals("ZOILineThickness", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("DoubleDataTemplate");
            }
            else if (propName.Equals("ZOIShapeType", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ZOIShapeDataTemplate");
            }
            else if (propName.Equals("VisualState", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("VisualStateDataTemplate");
            }


            if (template == null)
            {
                return base.SelectTemplate(item, container);
            }
            return template;
        }
    }
}
