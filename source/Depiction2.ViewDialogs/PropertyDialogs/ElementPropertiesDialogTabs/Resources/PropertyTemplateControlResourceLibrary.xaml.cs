﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.API.Measurement;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities.ElementParts;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.Element;
using Depiction2.Core.ViewModels.ElementPropertyViewModels;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources
{
    /// <summary>
    /// Interaction logic for ElementPropertiesControlResourceLibrary.xaml
    /// </summary>
    public partial class PropertyTemplateControlResourceLibrary
    {
        private void angleThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = (FrameworkElement)sender;
            //            var dataContext = (GenericPropertyViewModel)thumb.DataContext;
            var canvas = (FrameworkElement)thumb.Parent;
            var parentCanvas = (FrameworkElement)canvas.Parent;

            Point currentLocation = Mouse.GetPosition(parentCanvas);
            //Mouse.GetPosition(this)

            // We want to rotate around the center of the knob, not the top corner 
            Point knobCenter = new Point(parentCanvas.ActualHeight / 2, parentCanvas.ActualWidth / 2);

            // Calculate an angle 
            double radians = Math.Atan((currentLocation.Y - knobCenter.Y) /
                                       (currentLocation.X - knobCenter.X));
            var angle = radians * 180 / Math.PI;

            // Apply a 180 degree shift when X is negative so that we can rotate 
            // all of the way around 
            if (currentLocation.X - knobCenter.X < 0)
            {
                angle += 180;
            }
            ((RotateTransform)canvas.RenderTransform).Angle = angle;
            //            dataContext.ValueChanged = true;
        }
    }
    public class PropTemplateTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var containerElement = (FrameworkElement)container;
            DataTemplate template = null;
            
            dynamic prop = item as PropertyTemplateViewModel;
        
              
            if (prop == null) return base.SelectTemplate(item, container);
            #region by name
            var propName = ((PropertyTemplateViewModel)item).ProperName;

            //By name 
            if (PropertyInformation.BoolProps.Contains(propName))
            {
                template = (DataTemplate)containerElement.FindResource("BooleanDataTemplateConverter");
            }
            else if (propName.Equals("IconSize", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("IconSizeDataTemplateConverter");
            }
            else if (propName.Equals("VisualState", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("VisualStateDataTemplateConverter");
            }
            else if (propName.Equals("IconResourceName", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("DepictionIconPathDataTemplateConverter");
            }
            else if (propName.Equals("IconBorderShape", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("IconShapeDataTemplateConverter");
            }
            else if (propName.Equals("IconBorderColor", StringComparison.InvariantCultureIgnoreCase) ||
                propName.Equals("ZOIFillColor", StringComparison.InvariantCultureIgnoreCase)||
                propName.Equals("ZOIBorderColor", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ColorDataTemplateConverter");
            }
            else if (propName.Equals("ZOILineThickness", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("DoubleDataTemplateConverter");
            }
            else if (propName.Equals("ZOIShapeType", StringComparison.InvariantCultureIgnoreCase))
            {
                template = (DataTemplate)containerElement.FindResource("ZOIShapeDataTemplateConverter");
            }
            if (template != null) return template;
            #endregion
            #region by type
            var propType = prop.PropertyType;
            if (propType == typeof(IMeasurement) || typeof(IMeasurement).IsAssignableFrom(propType))
            {
                template = (DataTemplate)containerElement.FindResource("MeasurementDataTemplate");
            }

            else if (propType == typeof(int))
            {
                template = (DataTemplate)containerElement.FindResource("IntegerDataTemplate");
            }
            else if (propType == typeof(Angle))
            {
                template = (DataTemplate)containerElement.FindResource("AngleDataTemplate");
            }
            else if (propType == typeof(double))
            {
                template = (DataTemplate)containerElement.FindResource("DoubleDataTemplate");
            }
            else if (propType == typeof(string))
            {
                template = (DataTemplate)containerElement.FindResource("StringDataTemplate");
            }
            else if (propType == typeof(bool))
            {
                template = (DataTemplate)containerElement.FindResource("BooleanDataTemplate");
            }
            else if (propType == typeof(IDepictionLatitudeLongitude))
            {
                template = (DataTemplate)containerElement.FindResource("LatLongDataTemplate");
            }
            else if (propType == typeof(Image))
            {
                template = base.SelectTemplate(item, container);
            }
            else if (propType == typeof(SolidColorBrush))
            {
                template = (DataTemplate)containerElement.FindResource("ColorDataTemplate");
            }
            else if (propType == typeof(ElementVisualSetting))
            {
                template = (DataTemplate)containerElement.FindResource("VisualStateDataTemplate");
            }
            else if (propType == typeof(ZOIShapeType))
            {
                template = (DataTemplate)containerElement.FindResource("ZOIShapeDataTemplate");
            }
            else if (propType == typeof(DepictionIconSize))
            {
                template = (DataTemplate)containerElement.FindResource("IconSizeDataTemplate");
            }
            else if (propType == typeof(DepictionIconBorderShape))
            {
                template = (DataTemplate)containerElement.FindResource("IconShapeDataTemplate");
            }
            else
            {
                template = (DataTemplate)containerElement.FindResource("StringTemplate");
            }
            #endregion
            return template;
        }
    }
    //    public class GenericPropertyTemplateSelector : DataTemplateSelector
    //    {
    //        public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //        {
    //            var containerElement = (FrameworkElement) container;
    //            DataTemplate template;
    //            //            //The other alternative is to place these things in the app
    //
    //            if (!(item is GenericPropertyViewModel) || item==null) return base.SelectTemplate(item, container);
    //            item = ((GenericPropertyViewModel)item).PropertyValue;
    //
    //            if (item is IMeasurement)
    //            {
    //                template = (DataTemplate)containerElement.FindResource("MeasurementDataTemplate");
    //            }
    //            else
    //                if (item is int)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("IntegerDataTemplate");
    //                }
    //                else if (item is Angle)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("AngleDataTemplate");
    //                }
    //                else if (item is double)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("DoubleDataTemplate");
    //                }
    //                else if (item is string)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("StringDataTemplate");
    //                }
    //                else if (item is bool)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("BooleanDataTemplate");
    //                }
    //                else if (item is IDepictionLatitudeLongitude)
    //                {
    //                    template = (DataTemplate)containerElement.FindResource("LatLongDataTemplate");
    //                }
    //                else
    //                {
    //                    if (item is Image)
    //                    {
    //                        template = base.SelectTemplate(item, container);
    //                    }
    //                    else
    //                    {
    //                        template = (DataTemplate)containerElement.FindResource("StringTemplate");
    //                    }
    //                }
    //            return template;
    //        }
    //    }


    //    public class HoverTextSelectingTemplateSelector : DataTemplateSelector
    //    {
    //        public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //        {
    //            if (item is CollectionViewGroup)
    //                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("HoverTextSelectingTreeTemplate");
    //            if (item is MenuElementPropertyViewModel)
    //            {
    //                return (DataTemplate)((FrameworkElement)container).FindResource("HoverTextSelectingDataTemplate");
    //            }
    //            return base.SelectTemplate(item, container);
    //        }
    //    }

    //    public class PropertyViewTemplateSelector : DataTemplateSelector
    //    {
    //        public override DataTemplate SelectTemplate(object item, DependencyObject container)
    //        {
    //            if (item is CollectionViewGroup)
    //                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("PropertyChangingTreeTemplate");
    //            if (item is MenuElementPropertyViewModel)
    //            {
    //                return (DataTemplate)((FrameworkElement)container).FindResource("PropertyDataTemplate");
    //            }
    //            return base.SelectTemplate(item, container);
    //        }
    //    }



}
