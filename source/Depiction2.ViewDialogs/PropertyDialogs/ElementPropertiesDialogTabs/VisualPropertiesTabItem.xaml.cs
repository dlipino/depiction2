﻿using System.Collections;
using System.Windows;

namespace Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs
{
    /// <summary>
    /// Interaction logic for ListViewEnhanced.xaml
    /// </summary>
    public partial class VisualPropertiesTabItem 
    {
        public IEnumerable PropertiesSource
        {
            get { return (IEnumerable)GetValue(PropertiesSourceProperty); }
            set { SetValue(PropertiesSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PropertiesSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PropertiesSourceProperty =
            DependencyProperty.Register("PropertiesSource", typeof(IEnumerable), typeof(VisualPropertiesTabItem), new UIPropertyMetadata(null));

        public VisualPropertiesTabItem()
        {
            InitializeComponent();
        }
    }
}
