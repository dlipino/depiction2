﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.ViewDialogs.IconAndColorDialogs;
using Depiction2.ViewDialogs.PropertyDialogs.ElementPropertiesDialogTabs.Resources;

namespace Depiction2.ViewDialogs.PropertyDialogs
{
    public partial class MultipleElementPropertiesDialog 
    {
        public MultipleElementPropertiesDialog()
        {
            InitializeComponent();
            var commandBinding = new CommandBinding(VisualPropTemplateDataTemplates.SelectColorCommand, VisualPropTemplateDataTemplates.SelectColor);
            CommandBindings.Add(commandBinding);
            commandBinding = new CommandBinding(VisualPropTemplateDataTemplates.SelectElementIconCommand, VisualPropTemplateDataTemplates.SelectElementIcon);
            CommandBindings.Add(commandBinding);
            commandBinding = new CommandBinding(VisualPropTemplateDataTemplates.SelectButtonColorCommand, SelectButtonColor);
            CommandBindings.Add(commandBinding);
        }

        private void SelectButtonColor(object sender, ExecutedRoutedEventArgs e)
        {
            var dc = DataContext as MultipleElementPropertiesDialogViewModel;
            if (dc == null) return;
            var b = e.OriginalSource as Button;
            if (b == null) return;
            var result = ColorPickerDialog.PickColorModal(b.Background as SolidColorBrush);
            if (result == null) return;
            if (b.Equals(startColorButton))
            {
                dc.ThematicMapStartColor = result.Color;
            }else if(b.Equals(endColorButton))
            {

                dc.ThematicMapEndColor = result.Color;
            }
            b.Background = result;
        }
    }
}
