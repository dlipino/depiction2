﻿using System.Windows;
using System.Windows.Media;
using Depiction2.Core.ViewModels.DialogViewModels;
using Depiction2.ViewDialogs.IconAndColorDialogs;

namespace Depiction2.ViewDialogs.PropertyDialogs
{
    /// <summary>
    /// Interaction logic for AnnotationEditingDialog.xaml
    /// </summary>
    public partial class AnnotationPropertiesDialog
    {
        public AnnotationPropertiesDialog()
        {
            InitializeComponent();
        }

        private void BtnForegroundClick(object sender, RoutedEventArgs e)
        {
            var currentColor = btnForeground.Background as SolidColorBrush;
            var newColor = ColorPickerDialog.PickColorModal(currentColor);
            if (newColor != null)
            {
                btnForeground.Background = newColor;
                var dc = DataContext as AnnotationEditDialogVM;
                if(dc != null)
                {
                    dc.AnnotationTextColor = newColor.ToString();
                }
            }
        }

        private void BtnBackgroundClick(object sender, RoutedEventArgs e)
        {
            var currentColor = btnBackground.Background as SolidColorBrush;
            var newColor = ColorPickerDialog.PickColorModal(currentColor);
            if (newColor != null)
            {
                btnBackground.Background = newColor; 
                var dc = DataContext as AnnotationEditDialogVM;
                if (dc != null)
                {
                    dc.AnnotationBackgroundColor = newColor.ToString();
                }
            }
        }
    }
}