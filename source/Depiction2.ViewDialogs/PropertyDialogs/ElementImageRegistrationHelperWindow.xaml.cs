﻿using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.ElementImageRegistration
{
    /// <summary>
    /// Interaction logic for ElementImageRegistrationHelperWindow.xaml
    /// </summary>
    public partial class ElementImageRegistrationHelperWindow
    {
        public ElementImageRegistrationHelperWindow()
        {
            InitializeComponent();
        }
        protected override void SoftCloseWindow()
        { 
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc!= null) dc.EndGeoLocateElementCommand.Execute(null);
       
            base.SoftCloseWindow();
        }
    }
}
