﻿
using System.Windows;
using Depiction2.ViewDialogs.LibraryDialogs.Content;
using NUnit.Framework;

namespace Depiction2.ViewDialogs.UnitTests
{   
    [TestFixture]
    public class ViewDialogTestSuite
    {
        [Test]
        [Ignore("Theses cannot be run by a unit test. Also it is not complete.")]
        public void ViewTest()
        {
            var window = new Window();
            
            InteractionLibraryView view = new InteractionLibraryView();
            window.Show();
        }
    }
}
