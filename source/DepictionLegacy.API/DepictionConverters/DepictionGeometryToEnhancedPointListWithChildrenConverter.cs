﻿using System.Collections.Generic;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;


namespace Depiction.CoreModel.TypeConverter
{
    public class DepictionGeometryToEnhancedPointListWithChildrenConverter
    {
         public static List<EnhancedPointListWithChildren> IGeometryToEnhancedPointListWithChildren(IGeometry geometry, IGeoConverter pixelConverter)
         {
             if (geometry == null || geometry.IsEmpty)
                 return null;
             switch (geometry.GeometryType)
             {
                 case "Polygon":
                     return PolygonGeometryToPointCollectionlist(geometry, pixelConverter);
                 case "MultiPolygon":
                     return MultiPolygonGeometryToPointCollectionList(geometry, pixelConverter);
                 case "LineString":
                     return LineStringGeometryPointCollectionList(geometry, pixelConverter);
                 case "MultiLineString":
                     return MultiLineStringGeometryToPointCollectionList(geometry, pixelConverter);
                 case "Point":
                     var outPointCollections = new List<EnhancedPointListWithChildren>();
                     var points = new EnhancedPointListWithChildren();
                     var point = pixelConverter.WorldToGeoCanvas(new LatitudeLongitude(((IPoint)geometry).Y, ((IPoint)geometry).X));
                     points.Outline.Add(point);
                     outPointCollections.Add(points);
                     return outPointCollections;
                 case "MultiPoint":
                     return null;
                 default:
                     return null;
             }
         }
        public static List<EnhancedPointListWithChildren> IGeometryToEnhancedPointListWithChildren(DepictionGeometry depictionGeometry, IGeoConverter pixelConverter)
        {
            if (depictionGeometry == null || depictionGeometry.IsEmpty)
                return null;
            var geometry = depictionGeometry.Geometry;
            return IGeometryToEnhancedPointListWithChildren(geometry, pixelConverter);
        }
        protected static string GuessGeometryTypeFromEnhancedPointListWithChildren(List<EnhancedPointListWithChildren> drawnPoints)
        {
            //Multipoints fails
            bool multiplePolygons = false;
            bool linePolygons = false;
            if (drawnPoints.Count == 1 && drawnPoints[0].Children.Count == 0 && drawnPoints[0].Outline.Count == 1)
                return DepictionGeometryType.Point.ToString();
            if (drawnPoints.Count > 1) multiplePolygons = true;

            foreach(var pointCollection in drawnPoints)
            {
                if (!pointCollection.IsClosed) linePolygons = true;
                if (pointCollection.Children.Count >= 1) multiplePolygons = true;
            }
            if (multiplePolygons && linePolygons) return DepictionGeometryType.MultiLineString.ToString();
            if (multiplePolygons && !linePolygons) return DepictionGeometryType.MultiPolygon.ToString();
            if (!multiplePolygons && linePolygons) return DepictionGeometryType.LineString.ToString();
            if (!multiplePolygons && !linePolygons) return DepictionGeometryType.Polygon.ToString();
            return DepictionGeometryType.Point.ToString();

        }
        public static DepictionGeometry EnhancedPointListWithChildrenListToIGeometry(List<EnhancedPointListWithChildren> drawnPoints, string geomType, IGeoConverter geoToPixelConverter)
        {//Why don't we use DepictionGeometryType?
            //            DepictionGeometryType.
            //Ok this is kind of bad, because the geomtype is set, there is now way to add points and create a relevant geometry.

            var guessedGeomType = GuessGeometryTypeFromEnhancedPointListWithChildren(drawnPoints);

            switch (guessedGeomType)
            {
                case "Polygon":
                    return PointCollectionListToPolygonGeometry(drawnPoints, geoToPixelConverter);
                case "MultiPolygon":
                    return PointCollectionListToMultiPolygonGeometry(drawnPoints, geoToPixelConverter);
                case "LineString":
                    return PointCollectionListToLineStringGeometry(drawnPoints, geoToPixelConverter);
                case "MultiLineString":
                    return PointCollectionListToMultiLineStringGeometry(drawnPoints, geoToPixelConverter);
                case "Point":
                    foreach (var collection in drawnPoints)
                    {
                        var coordList = PointCollectionToICoordinatesList(collection, geoToPixelConverter);
                        foreach(var coord in coordList)
                        {
                            return new DepictionGeometry(new LatitudeLongitude(coord.Y,coord.X));
                        }
                    }
                    return null;
                case "MultiPoint":
                    return null;
                default:
                    return null;
            }
        }

        #region for creating List PointCollection That are used for drawing closed editable polygons
        private static List<EnhancedPointListWithChildren> PolygonGeometryToPointCollectionlist(IGeometry geometry, IGeoConverter converter)
        {
            var polygon = (IPolygon)geometry;
            var outPointCollections = new List<EnhancedPointListWithChildren>();
            outPointCollections.Add(ConvertIPolygonToEnhancedPointListWithChildren(polygon, converter));
            return outPointCollections;

        }
        private static List<EnhancedPointListWithChildren> MultiPolygonGeometryToPointCollectionList(IGeometry geometry, IGeoConverter converter)
        {
            var outPointCollections = new List<EnhancedPointListWithChildren>();
            var polygons = ((IMultiPolygon)geometry).Geometries;

            foreach (IPolygon polygon in polygons)
            {
                var listForPoly = ConvertIPolygonToEnhancedPointListWithChildren(polygon, converter);
                outPointCollections.Add(listForPoly);
            }
            return outPointCollections;
        }
        private static List<EnhancedPointListWithChildren> MultiLineStringGeometryToPointCollectionList(IGeometry geometry, IGeoConverter converter)
        {
            var lineStrings = ((MultiLineString)geometry).Geometries;

            var outPointCollections = new List<EnhancedPointListWithChildren>();
            foreach (var lineString in lineStrings)
            {
                var coordinates = lineString.Coordinates;
                var lineCollection = ICoordinatesToEnhancedPointListWithChildren(coordinates, converter);
                outPointCollections.Add(lineCollection);
            }

            return outPointCollections;
        }

        private static List<EnhancedPointListWithChildren> LineStringGeometryPointCollectionList(IGeometry geometry, IGeoConverter converter)
        {
            var coordinates = geometry.Coordinates;
            var outPointCollections = new List<EnhancedPointListWithChildren>();
            outPointCollections.Add(ICoordinatesToEnhancedPointListWithChildren(coordinates, converter));
            return outPointCollections;
        }

        #region Helper methods

        private static EnhancedPointListWithChildren ICoordinatesToEnhancedPointListWithChildren(IEnumerable<ICoordinate> coordinates, IGeoConverter converter)
        {
            var pointCollection = new EnhancedPointListWithChildren();

            foreach (var coordinate in coordinates)
            {
                var point = converter.WorldToGeoCanvas(new LatitudeLongitude(coordinate.Y, coordinate.X));
                pointCollection.Outline.Add(point);
                pointCollection.IsClosed = false;
                pointCollection.TypeOfPointList = PointListType.Line;
            }
            return pointCollection;
        }

        private static EnhancedPointListWithChildren ConvertIPolygonToEnhancedPointListWithChildren(IPolygon inpoly, IGeoConverter converter)
        {
            //Notice the removal of the last point, this occurs so that the firat and last point are not equal
            var shellCoords = inpoly.Shell.Coordinates;
            var wpfPolygon = new EnhancedPointListWithChildren();
            foreach (var coordinate in shellCoords)
            {
                var point = converter.WorldToGeoCanvas(new LatitudeLongitude(coordinate.Y, coordinate.X));
                wpfPolygon.Outline.Add(point);
            }
            //EnhancedPointlistWithChildren does not double up on the last point
            if (wpfPolygon.Outline[0] == wpfPolygon.Outline[wpfPolygon.Outline.Count-1] )
            {
                wpfPolygon.Outline.RemoveAt(wpfPolygon.Outline.Count - 1);
            }
            wpfPolygon.IsClosed = true;
            wpfPolygon.IsFilled = true;
            wpfPolygon.TypeOfPointList = PointListType.Shell;

            if (inpoly.Holes != null)
            {
                foreach (var hole in inpoly.Holes)
                {
                    var holePointList = new EnhancedPointList();
                    foreach (var coordinate in hole.Coordinates)
                    {
                        holePointList.Outline.Add(converter.WorldToGeoCanvas(new LatitudeLongitude(coordinate.Y, coordinate.X)));
                    }
                    holePointList.IsFilled = true;
                    holePointList.IsClosed = hole.IsClosed;//false was the original befre change to use the hole value
                    holePointList.TypeOfPointList = PointListType.Hole;
                    if (holePointList.Outline[0] == holePointList.Outline[holePointList.Outline.Count - 1])
                    {
                        holePointList.Outline.RemoveAt(holePointList.Outline.Count - 1);
                    }
                    wpfPolygon.Children.Add(holePointList);
                }
            }
            return wpfPolygon;
        }
        #endregion

        #endregion

        #region For creating the DepictionGeometry from EnhancedPointListWithChildrenList

        private static DepictionGeometry PointCollectionListToMultiLineStringGeometry(List<EnhancedPointListWithChildren> points, IGeoConverter converter)
        {
            var geometryFactory = new GeometryFactory();
            var lineStrings = new List<ILineString>();
            foreach (var collection in points)
            {
                var coordList = PointCollectionToICoordinatesList(collection, converter);
                lineStrings.Add(geometryFactory.CreateLineString(coordList.ToArray()));
            }
            return new DepictionGeometry(geometryFactory.CreateMultiLineString(lineStrings.ToArray()));
        }

        private static DepictionGeometry PointCollectionListToLineStringGeometry(List<EnhancedPointListWithChildren> points, IGeoConverter converter)
        {
            var geometryFactory = new GeometryFactory();
            DepictionGeometry depictGeom = null;
            for (int i = 0; i < points.Count && i < 1; i++)
            {
                var coordList = PointCollectionToICoordinatesList(points[i], converter);
                
                depictGeom = new DepictionGeometry(geometryFactory.CreateLineString(coordList.ToArray()));
            }
            return depictGeom;
        }

        private static DepictionGeometry PointCollectionListToMultiPolygonGeometry(List<EnhancedPointListWithChildren> points, IGeoConverter converter)
        {
            var geometryFactory = new GeometryFactory();
            var allPolys = new List<IPolygon>();
            foreach (var collection in points)
            {
                allPolys.Add(PointCollectionListToConvertIPolygon(collection, converter));

            }
            return new DepictionGeometry(geometryFactory.CreateMultiPolygon(allPolys.ToArray()));
        }

        private static DepictionGeometry PointCollectionListToPolygonGeometry(List<EnhancedPointListWithChildren> points, IGeoConverter converter)
        {
            //There should only be one point collection for a Polygon geometry
            DepictionGeometry depictGeom = null;
            for (int i = 0; i < points.Count && i < 1; i++)
            {
                depictGeom = new DepictionGeometry(PointCollectionListToConvertIPolygon(points[i], converter));
            }
            return depictGeom;
        }
        #region Helper methods

        private static List<ICoordinate> PointCollectionToICoordinatesList(EnhancedPointListWithChildren pixelPoints, IGeoConverter converter)
        {
            var coordList = new List<ICoordinate>();
            foreach (var pixelPoint in pixelPoints.Outline)
            {
                var worldLoc = converter.GeoCanvasToWorld(pixelPoint);
                var coord = new Coordinate(worldLoc.Longitude, worldLoc.Latitude);
                coordList.Add(coord);
            }

            return coordList;
        }

        private static IPolygon PointCollectionListToConvertIPolygon(EnhancedPointListWithChildren pixelPoints, IGeoConverter converter)
        {
            var geometryFactory = new GeometryFactory();
            var shellCoords = new List<ICoordinate>();
            foreach (var shellPixPoint in pixelPoints.Outline)
            {
                var worldLoc = converter.GeoCanvasToWorld(shellPixPoint);
                var coord = new Coordinate(worldLoc.Longitude, worldLoc.Latitude);
                shellCoords.Add(coord);
            }
            if (shellCoords[0] != shellCoords[shellCoords.Count - 1])
            {
                //            //Assumes polygons are closed so add the first point as the last point to make IPolygon work right
                shellCoords.Add(shellCoords[0]); //Hackish
            }

            //Is this actually who holes are created in an IPolygon?
            var linearRing = geometryFactory.CreateLinearRing(shellCoords.ToArray());

            var holeLinearRingList = new List<ILinearRing>();
            foreach (var hole in pixelPoints.Children)
            {
                var holePointList = new List<Coordinate>();
                foreach (var holePixPoint in hole.Outline)
                {
                    var worldLoc = converter.GeoCanvasToWorld(holePixPoint);
                    var coord = new Coordinate(worldLoc.Longitude, worldLoc.Latitude);
                    holePointList.Add(coord);

                }
                if (holePointList[0] != holePointList[holePointList.Count - 1])
                {
                    //            //Assumes polygons are closed so add the first point as the last point to make IPolygon work right
                    holePointList.Add(holePointList[0]); //Hackish
                }
                holeLinearRingList.Add(geometryFactory.CreateLinearRing(holePointList.ToArray()));
            }
            return geometryFactory.CreatePolygon(linearRing, holeLinearRingList.ToArray());
        }
        #endregion
        #endregion
    }
}