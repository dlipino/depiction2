﻿using System;
using System.Windows;
using OSGeo.OSR;

namespace Depiction.ProjectionSystem.DepictionProjection
{
    public class DepictionProjectionSystem
    {
        #region Constants for projections
        public const string LummiUTM = "PROJCS[\"NAD_1983_StatePlane_Washington_North_FIPS_4601_Feet\"," +
                                       "GEOGCS[\"GCS_North_American_1983\"," +
                                       "DATUM[\"North_American_Datum_1983\"," +
                                       "SPHEROID[\"GRS_1980\",6378137.0,298.257222101]]," +
                                       "PRIMEM[\"Greenwich\",0.0]," +
                                       "UNIT[\"Degree\",0.0174532925199433]]," +
                                       "PROJECTION[\"Lambert_Conformal_Conic_2SP\"]," +
                                       "PARAMETER[\"False_Easting\",1640416.666666667]," +
                                       "PARAMETER[\"False_Northing\",0.0]," +
                                       "PARAMETER[\"Central_Meridian\",-120.8333333333333]," +
                                       "PARAMETER[\"Standard_Parallel_1\",47.5]," +
                                       "PARAMETER[\"Standard_Parallel_2\",48.73333333333333]," +
                                       "PARAMETER[\"Latitude_Of_Origin\",47.0]," +
                                       "UNIT[\"Foot_US\",0.3048006096012192]]";

//        public const string mercatorProjectedCoordinateSystem14 = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
//                                                          "GEOGCS[\"Popular Visualisation CRS\",  " +
//                                                              "DATUM[\"WGS84\",    " +
//                                                                  "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
//                                                              "AUTHORITY[\"EPSG\",\"6055\"]], " +
//                                                         "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
//                                                         "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
//                                                         "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
//                                                     "PROJECTION[\"Mercator\"]," +
//                                                     "PARAMETER[\"semi_minor\",6378137]," +
//                                                     "PARAMETER[\"False_Easting\", 0]," +
//                                                     "PARAMETER[\"False_Northing\", 0]," +
//                                                     "PARAMETER[\"Central_Meridian\", 0]," +
//                                                     "PARAMETER[\"Latitude_of_origin\", 0]," +
//                                                     "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
//                                                     "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
//                                                     "AUTHORITY[\"EPSG\",\"3785\"]]";

        public const string geographicCoordinateSystem14 =
             "GEOGCS[\"GCS_WGS_1984\"," +
                 "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                 "PRIMEM[\"Greenwich\",0]," +
                 "UNIT[\"Degree\",0.0174532925199433]" +
             "]";

//        public const string mercatorProjectedCoordinateSystemproj4 = "PROJCS[\"Mercator_1SP\",GEOGCS[\"GCS_Geographic Coordinate System\"," +
//                                                                "DATUM[\"D_WGS84\",SPHEROID[\"WGS84\",6378137,298.257223560493]]," +
//                                                                "PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]]," +
//                                                                "PROJECTION[\"Mercator\"]," +
//                                                                "PARAMETER[\"central_meridian\",0]," +
//                                                                "PARAMETER[\"standard_parallel_1\",0]," +
//                                                                "PARAMETER[\"false_easting\",0]," +
//                                                                "PARAMETER[\"false_northing\",0],UNIT[\"Meter\",1]]";

        public const string mercatorProjectedCoordinateSystem = "PROJCS[\"WGS 84 / World Mercator\",GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
            "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]]," +
            "PROJECTION[\"Mercator_1SP\"],PARAMETER[\"central_meridian\",0],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",0],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"3395\"]," +
            "AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH]]";
        #endregion

        #region stuff used by app
        public static string CurrentCoordinateSystemString = geographicCoordinateSystem14;
        public static SpatialReference currentSpatialSystem = new SpatialReference(CurrentCoordinateSystemString);
        public static SpatialReference mercatorSystem = new SpatialReference(geographicCoordinateSystem14);
        #endregion
        public static void SetProjectionSystem(string projString)
        {
            try
            {
                var newSrs = new SpatialReference(projString);
                CurrentCoordinateSystemString = projString;
                currentSpatialSystem = newSrs;
            }catch
            {
                
            }
        }

        public static Point UTMToMercator(Point utmPoint)
        {
            var newPoint = new Point();
//            if(currentSpatialSystem.GetUTMZone() >0)
//            {
                //var ct = new CoordinateTransformation(currentSpatialSystem, mercatorSystem);

//                double[] transPoints = new double[3];
//                ct.TransformPoint(transPoints, utmPoint.X, utmPoint.Y, 0);
//                newPoint.X = transPoints[0];
//                newPoint.Y = transPoints[1];
//            }
            return newPoint;
        }

        public static SpatialReference CreateSpatialReference(string wktProjString)
        {
            try
            {
                return new SpatialReference(wktProjString);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
//        public static SpatialReference Create14Projection()
//        {
//            var srDst = new SpatialReference(mercatorProjectedCoordinateSystem14);
//
//            return srDst;
//        }
//        public static SpatialReference CreateLummiReference()
//        {
//            var srDst = new SpatialReference(LummiUTM);
//            
//            return srDst;
//        }

//        static public void TransformPoints(int nCount, double[] x, double[] y,double[] z)
//        {
//            var lummi = CreateLummiReference();
//            var wgs = CreateSpatialReference();
//            var ct = new CoordinateTransformation(wgs, lummi);
//            ct.TransformPoints(nCount,x,y,z);
//        }

//        static public double[] TranformPoint(double x, double y, double z)
//        {
//            var lummi = CreateLummiReference();
//            var wgs = CreateSpatialReference(mercatorProjectedCoordinateSystem14);
//            var ct = new CoordinateTransformation(wgs, lummi);
//            double[] transPoints = new double[3];
//            ct.TransformPoint(transPoints, x, y, z);
//            return transPoints;
//        }
    }
}