﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using Depiction2.API.Service;
using Depiction2.Base;
using Depiction2.Base.Interactions;
using Depiction2.Base.Product;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;


[assembly: InternalsVisibleTo("Depiction2.Main")]
[assembly: InternalsVisibleTo("Depiction2.Core")]
[assembly: InternalsVisibleTo("Depiction2.ViewService")]
[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
[assembly: InternalsVisibleTo("Depiction2.Story2IO.UnitTests")]
[assembly: InternalsVisibleTo("Depiction2.Story14IO.UnitTests")]
[assembly: InternalsVisibleTo("Depiction2.Terrain.UnitTests")]
[assembly: InternalsVisibleTo("Depiction2.CsvExtension.UnitTests")]

namespace Depiction2.API
{
    static public class DepictionAccess
    {
        public const string DefaultPointElement = "Depiction.Default.Point";
        public const string DefaultImageElement = "Depiction.Default.Image";
        public const string AutoDetectElement = "Depiction.Default.AutoDetect";

        private static IBackgroundServiceManager backgroundService;
        public static IStory DStory { get; set; }
        public static IStoryActions DStoryActions { get; set; }
        public static IElementTemplateLibrary TemplateLibrary { get; internal set; }
        public static IInteractionRuleRepository InteractionLibrary { get; internal set; }
        public static ProductInformationBase ProductInformation { get; internal set; }
        public static IDepictionPathService PathService { get; internal set; }
        public static ISimpleNameTypeService NameTypeService { get { return TypeService.Instance; } }
        public static DepictionMessageService MessageService { get { return DepictionMessageService.Instance; } }
        public static IDepictionGeocodeService GeocoderService { get; set; }
        public static List<RegionDataSourceInformation> HackRegionSourceList { get; set; }

        static public IBackgroundServiceManager BackgroundServiceManager
        {
            get
            {
                if (backgroundService == null)
                {
                    backgroundService = new BackgroundServiceManager();
                }
                return backgroundService;
            }
            internal set
            {
                if (backgroundService != null)
                {
                    backgroundService.CancelAllServices();
                    backgroundService.Dispose();
                }
                backgroundService = value;
            }
        }
        static public object GetDeepCopyOfPropertyValue(object value)
        {
            if (value is IDeepCloneable)
            {
                return ((IDeepCloneable)(value)).DeepClone();
            }
            return value;
        }
        public static Type GetTypeFromName(string name)
        {
            //attempt to make it simple 

            if (NameTypeService == null)
            {
                Debug.WriteLine("The conversion service is not set, returning string type");
                //                return typeof(string);
                throw new NullReferenceException("Simple name to type service is not instantiated");
            }
            var type = NameTypeService.GetTypeFromName(name);
            if (type == null)
            {
                //Maybe just log an error, because some 1.4 types were not added into 2.0
                //                throw new Exception(string.Format("Unable to locate type:{0}", name));
            }
            return type;
        }
        static public ImageSource FindElementIconByName(string iconResourceName)
        {
            if (Application.Current == null || Application.Current.Resources == null)
            {
                return new BitmapImage();
            }
            if (!Application.Current.Resources.Contains(iconResourceName))
            {
                iconResourceName = "UnkownIcon";
            }

            var image = Application.Current.Resources[iconResourceName] as ImageSource;

            if (image == null) return new BitmapImage();
            return image;
        }
        static public Dictionary<string, ImageSource> DefaultIconDictionary
        {
            get
            {
                var outDict = new Dictionary<string, ImageSource>();
                if (Application.Current != null && Application.Current.Resources != null)
                {
                    var merged = Application.Current.Resources.MergedDictionaries;
                    foreach (var dict in merged)
                    {
                        if (dict["DefaultIcons"] is ResourceDictionary)
                        {
                            var rd = dict["ProductIcons"] as ResourceDictionary;
                            foreach (DictionaryEntry entry in rd)
                            {
                                var bitmapSource = (ImageSource)entry.Value;
                                var newKey = (string)(entry.Key);
                                outDict.Add(newKey, bitmapSource);
                            }
                            break;
                        }

                        if (dict.Source != null)
                        {
                            if (dict.Source.Equals("DefaultResources\\AppIcons.xaml"))
                            {
                                AddImageSourcesToDict(outDict, dict);
                            }
                            foreach (var exRes in ExtensionService.Instance.DepictionResourceExtensions)
                            {
                                //TODO make this cleaner
                                var resourceDict = exRes.Value.ExtensionResources;
                                if (resourceDict != null && dict.Source.Equals(resourceDict.Source))
                                {
                                    AddImageSourcesToDict(outDict, dict);
                                    break;
                                }
                            }
                        }
                    }
                }
                return outDict;
            }
        }
        private static void AddImageSourcesToDict(Dictionary<string, ImageSource> outDict, ResourceDictionary source)
        {
            foreach (DictionaryEntry entry in source)
            {
                var bitmapSource = (ImageSource)entry.Value;
                if (bitmapSource == null) continue;
                var newKey = (string)(entry.Key);
                if (!outDict.ContainsKey(newKey))
                {
                    outDict.Add(newKey, bitmapSource);
                }
            }
        }
        static public Dictionary<string, ImageSource> StoryImageDictionary
        {
            get
            {
                var outDict = new Dictionary<string, ImageSource>();
                if (DStory != null)
                {
                    var images = DStory.AllImages;
                    if (images != null)
                    {
                        foreach (var key in images.Keys)
                        {
                            var bitmapSource = (ImageSource)images[key];
                            var newKey = key.ToString();
                            outDict.Add(newKey, bitmapSource);
                        }
                    }
                }
                return outDict;
            }
        }
    }
}