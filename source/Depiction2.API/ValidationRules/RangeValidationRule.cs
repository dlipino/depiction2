using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using Depiction2.API.Converters;
using Depiction2.Base;
using Depiction2.Base.Measurement;
using Depiction2.Base.ValidationRules;

namespace Depiction2.API.ValidationRules
{
    [DataContract]
    public class RangeValidationRule : IValidationRule
    {
        #region Properties
        public bool IsRuleValid
        {
            get
            {
                if (RangeMin == null || RangeMax == null) return false;
                if (!RangeMin.GetType().Equals(RangeMax.GetType())) return false;
                double calcMin = 0, calcMax = 1;
                if (RangeMin.GetType().Equals(typeof(IMeasurement)))
                {
                    var useSystem = ((IMeasurement)RangeMin).InitialSystem;
                    var useScale = ((IMeasurement)RangeMin).InitialScale;
                    calcMin = ((IMeasurement)RangeMin).GetValue(useSystem, useScale);
                    calcMax = ((IMeasurement)RangeMax).GetValue(useSystem, useScale);
                }
                else
                {
                    if (!double.TryParse(RangeMin.ToString(), out calcMin)) return false;
                    if (!double.TryParse(RangeMax.ToString(), out calcMax)) return false;

                }
                if (calcMin > calcMax) return false;

                return true;
            }
        }
        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public ValidationRuleParameter[] Parameters
        {
            get { return new[] { new ValidationRuleParameter(RangeMin.ToString(), RangeMin.GetType()), new ValidationRuleParameter(RangeMax.ToString(), RangeMax.GetType()) }; }
        }
        [DataMember]
        public object RangeMax { get; set; }
        [DataMember]
        public object RangeMin { get; set; }

        #endregion
        #region Constructor

        ///<summary>
        /// Create a new instance of this class.
        ///</summary>
        public RangeValidationRule()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RangeValidationRule"/> class.
        /// </summary>
        /// <param name="minValue">The min value.</param>
        /// <param name="maxValue">The max value.</param>
        public RangeValidationRule(object minValue, object maxValue)
        {
            RangeMin = minValue;
            RangeMax = maxValue;
        }

        #endregion


        #region Methods

        /// <summary>
        /// Validates the specified value to validate.
        /// If one of the parameters is an IMeasurement, then all must be.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult();
            result.ValidationOutput = ValidationResultValues.InValid;
            double minVal, maxVal, valVal;
            if (RangeMin is IMeasurement || RangeMax is IMeasurement || valueToValidate is IMeasurement)
            {
                if (!(RangeMin is IMeasurement))
                {
                    result.Message = string.Format(
                        "The minimum value {0} cannot be validated because its units have not been specified.", RangeMin);
                    return result;
                }
                if (!(RangeMax is IMeasurement))
                {
                    result.Message = string.Format(
                        "The maximum value {0} cannot be validated because its units have not been specified.", RangeMax);
                    return result;
                }
                if (!(valueToValidate is IMeasurement))
                {
                    result.Message = string.Format(
                        "The value \"{0}\" cannot be validated because its units have not been specified.", valueToValidate);
                    return result;
                }
                var useSystem = ((IMeasurement)RangeMin).InitialSystem;
                var useScale = ((IMeasurement)RangeMin).InitialScale;
                valVal = ((IMeasurement)valueToValidate).GetValue(useSystem, useScale);
                minVal = ((IMeasurement)RangeMin).GetValue(useSystem, useScale);
                maxVal = ((IMeasurement)RangeMax).GetValue(useSystem, useScale);
            }
            else
            {
                try
                {
                    minVal = Convert.ToDouble(RangeMin);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert minimum value \"{0}\" to numeric for validation.", RangeMin);
                    return result;
                }
                try
                {
                    maxVal = Convert.ToDouble(RangeMax);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert maximum value \"{0}\" to numeric for validation.", RangeMax);
                    return result;
                }
                try
                {
                    valVal = Convert.ToDouble(valueToValidate);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert \"{0}\" to numeric for validation.", valueToValidate);
                    return result;
                }
            }

            var isWithBounds = (valVal >= minVal && valVal <= maxVal);
            if (isWithBounds) result.ValidationOutput = ValidationResultValues.Valid;
            result.Message = string.Format("Value must be between {0} and {1}", RangeMin, RangeMax);
            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            //For the sake of speediness, im adding a hack to read 1.2.2 range validation rules
            if (reader.Name.Equals("min"))
            {//1.2.2 load call
                RangeMin = reader.ReadElementContentAsDouble("min", ns);
                RangeMax = reader.ReadElementContentAsDouble("max", ns);
            }
            else
            {

                var minTypeString = reader.ReadElementContentAsString("minType", ns);
                var minValue = reader.ReadElementContentAsString("minValue", ns);

                var minType = Type.GetType(minTypeString);
                RangeMin = DepictionTypeConverter.ChangeType(minValue, minType);

                var maxTypeString = reader.ReadElementContentAsString("maxType", ns);
                var maxValue = reader.ReadElementContentAsString("maxValue", ns);

                var maxType = Type.GetType(maxTypeString);
                RangeMax = DepictionTypeConverter.ChangeType(maxValue, maxType);
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            //Why did I add type? oh nm, they are to do conversions, stil its odd
            writer.WriteElementString("minType", ns, RangeMin.GetType().FullName);
            writer.WriteElementString("minValue", ns, RangeMin.ToString());
            writer.WriteElementString("maxType", ns, RangeMax.GetType().FullName);
            writer.WriteElementString("maxValue", ns, RangeMax.ToString());
        }
    }
}