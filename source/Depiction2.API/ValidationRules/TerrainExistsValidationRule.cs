using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using Depiction2.Base.ValidationRules;

namespace Depiction2.API.ValidationRules
{
    ///<summary>
    /// A validation rule to check for the existence of elevation in this depiction.
    /// Ick this one should get killed off, but it is needed for dpn porting
    ///</summary>
    [DataContract]
    public class TerrainExistsValidationRule : IValidationRule
    {
        #region IValidationRule Members

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
        }

        public DepictionValidationResult Validate(object valueToValidate)
        {
            var requiredElement = "Elevation";
            var terrainExists = false;// DepictionAccess.CurrentDepiction != null && DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag(requiredElement) != null;
            if (terrainExists)
                return new DepictionValidationResult { ValidationOutput = ValidationResultValues.Valid };

            var message = string.Format("Must add \"{0}\" data before changing flood height. (see \"Elevation data\" in help).",
                                        requiredElement);
            return new DepictionValidationResult { ValidationOutput = ValidationResultValues.InValid, Message = message };
        }

        public bool IsRuleValid
        {
            get { return true; }
        }

        public ValidationRuleParameter[] Parameters
        {
            get { return new ValidationRuleParameter[0]; }
        }

        #endregion
    }
}