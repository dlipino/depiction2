using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using Depiction2.API.Converters;
using Depiction2.Base;
using Depiction2.Base.Measurement;
using Depiction2.Base.ValidationRules;

namespace Depiction2.API.ValidationRules
{
    [DataContract]
    public class DataTypeValidationRule : IValidationRule
    {
        #region Properties
        public bool IsRuleValid
        {
            get
            {
                return TypeToValidate != null;
            }
        }

        public ValidationRuleParameter[] Parameters
        {
            get
            {
                return new[] { 
                    new ValidationRuleParameter(GetSerializableTypeName(TypeToValidate), 
                    TypeToValidate.GetType()), 
                    new ValidationRuleParameter(ErrorMessage, ErrorMessage.GetType()) };
            }
        }

        [DataMember]
        private Type TypeToValidate { get; set; }
        [DataMember, DefaultValue("")]
        private string ErrorMessage { get; set; }
        #endregion
        #region Constructor

        ///<summary>
        /// Create a new instance of this class. Needed for saving
        ///</summary>
        public DataTypeValidationRule()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataTypeValidationRule"/> class.
        /// </summary>
        /// <param name="typeToValidate">The type to validate.</param>
        /// <param name="failErrorMessage">The fail error message.</param>
        public DataTypeValidationRule(Type typeToValidate, string failErrorMessage)
        {
            TypeToValidate = typeToValidate;
            ErrorMessage = failErrorMessage;
        }

        #endregion
        
        #region Methods

        /// <summary>
        /// Validates the specified value to validate.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult();

            try
            {
                object convertedValue = DepictionTypeConverter.ChangeType(valueToValidate, TypeToValidate);
                if (convertedValue is IMeasurement)
                    convertedValue = ((IMeasurement)convertedValue).GetCurrentSystemDefaultScaleValue();
                if (convertedValue.Equals(double.NaN) || convertedValue.Equals(double.PositiveInfinity) || convertedValue.Equals(double.NegativeInfinity) || convertedValue.Equals(float.NaN) || convertedValue.Equals(float.PositiveInfinity) || convertedValue.Equals(float.NegativeInfinity))
                    throw new Exception(String.Format("{0} is not a valid numeric input", convertedValue));
                result.ValidationOutput = ValidationResultValues.Valid;
            }
            catch
            {
                result.ValidationOutput = ValidationResultValues.InValid;
                result.Message = ErrorMessage;
            }

            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }


        public void ReadXml(XmlReader reader)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();

            var typeName = reader.ReadElementContentAsString("validTypeName", ns);
            TypeToValidate = Type.GetType(typeName);
            ErrorMessage = reader.ReadElementContentAsString("errorMessage", ns);

            reader.ReadEndElement();
        }

        private static string GetSerializableTypeName(Type type)
        {
            // A copy of code in SerializationService
            var assemblyName = type.Assembly.FullName.Split(new[] { ',' });
            var nonSpecificTypeName = string.Format("{0},{1}", type.FullName, assemblyName[0]);

            if (Type.GetType(nonSpecificTypeName) != null)
                return nonSpecificTypeName;
            return type.AssemblyQualifiedName;
        }


        public void WriteXml(XmlWriter writer)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            writer.WriteElementString("validTypeName", ns, GetSerializableTypeName(TypeToValidate));
            writer.WriteElementString("errorMessage", ns, ErrorMessage);
        }
    }
}