﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using Depiction2.API.Converters;
using Depiction2.Base;
using Depiction2.Base.Measurement;
using Depiction2.Base.ValidationRules;

namespace Depiction2.API.ValidationRules
{
    [DataContract]
    public class MinValueValidationRule : IValidationRule
    {
        #region properties
        public bool IsRuleValid { get { return (MinValue != null); } }
        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public ValidationRuleParameter[] Parameters
        {
            get { return new[] { new ValidationRuleParameter(MinValue.ToString(), MinValue.GetType()) }; }
        }
        [DataMember]
        public object MinValue { get; set; }

        [DataMember]
        private Type ValueType { get; set; }
        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MinValueValidationRule"/> class.
        /// </summary>
        /// <param name="minVal">The min val.</param>
        public MinValueValidationRule(object minVal)
        {
            MinValue = minVal;
            ValueType = minVal.GetType();
        }

        ///<summary>
        /// Create a new instance of this class.
        ///</summary>
        public MinValueValidationRule()
        {}

        #endregion


        #region methods
        [OnDeserialized]
        private void SetCorrectMinValueType(StreamingContext streamingContext)
        {
//            var valType = Type.GetType(ValueTypeString);
            var conv = TypeDescriptor.GetConverter(ValueType);
           if(conv.CanConvertFrom(MinValue.GetType()))
           {
               MinValue = conv.ConvertFrom(MinValue);
           }

        }
        /// <summary>
        /// Validates the specified value to validate.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult { ValidationOutput = ValidationResultValues.InValid, Message = null };

            double minVal;
            double valVal;
            if (MinValue is IMeasurement || valueToValidate is IMeasurement)
            {
                if (!(MinValue is IMeasurement))
                {
                    result.Message = string.Format(
                        "The minimum value {0} cannot be validated because its units have not been specified.", MinValue);
                    return result;
                }
                if (!(valueToValidate is IMeasurement))
                {
                    result.Message = string.Format(
                        "The value {0} cannot be validated because its units have not been specified.", valueToValidate);
                    return result;
                }

                var useSystem = ((IMeasurement)MinValue).InitialSystem;
                var useScale = ((IMeasurement)MinValue).InitialScale;
                minVal = ((IMeasurement)MinValue).GetValue(useSystem,useScale);
                valVal = ((IMeasurement)valueToValidate).GetValue(useSystem, useScale);
            }
            else
            {
                try
                {
                    minVal = Convert.ToDouble(MinValue);
                }
                catch
                {
                    result.Message = string.Format("Unable to convert minimum value {0} to numeric for validation.", MinValue);
                    return result;
                }
                
                try
                {
                    valVal = Convert.ToDouble(valueToValidate);
                }
                catch
                {
                    result.Message = string.Format("Unable to convert value {0} to numeric for validation.", valueToValidate);
                    return result;
                }
                
            }
            if(valVal >= minVal)
            {
                result.ValidationOutput = ValidationResultValues.Valid; 
            }


            if (result.ValidationOutput.Equals(ValidationResultValues.InValid))
            {
                result.Message = string.Format("Value must be at least {0}", MinValue);
            }

            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            var minTypeString = reader.ReadElementContentAsString("minType", ns);
            var minValue = reader.ReadElementContentAsString("minValue", ns);

            var minType = Type.GetType(minTypeString);
            MinValue = DepictionTypeConverter.ChangeType(minValue, minType);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = DepictionConstants.DepictionXmlNameSpace;
            writer.WriteElementString("minType", ns, MinValue.GetType().FullName);
            writer.WriteElementString("minValue", ns, MinValue.ToString());
        }
    }
}