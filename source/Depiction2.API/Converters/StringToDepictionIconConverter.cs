﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Depiction2.API.Converters
{
    public class StringToDepictionIconConverter : IValueConverter
    {
        static public StringToDepictionIconConverter Default = new StringToDepictionIconConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            var name = value.ToString();
            if(DepictionAccess.DStory != null)
            {
                var imageResources = DepictionAccess.DStory.AllImages;
                if (imageResources != null && imageResources.Contains(name))
                {
                    return DepictionAccess.DStory.AllImages.GetImage(name);
                }
            }

            return DepictionAccess.FindElementIconByName(name);

//            //Try to find the image in the application images
//            var defaultIcons = DepictionAccess.DefaultIconDictionary;
//            if(defaultIcons.ContainsKey(name))
//            {
//                return defaultIcons[name];
//            }
//            if (defaultIcons.ContainsKey("UnkownIcon"))
//            {
//                return defaultIcons["UnkownIcon"];
//            }
//            return new BitmapImage();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }


    }
}