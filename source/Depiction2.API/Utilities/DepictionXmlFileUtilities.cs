﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Depiction2.API.Utilities
{
    public class DepictionXmlFileUtilities
    {
        public const string DepictionXmlNameSpace = "http://depiction.com";
        //        public const string DepictionDataTypeFile = "DepictionDatatypes.xml";
        //        public const string DepictionFileFormatFile = "DepictionFormat.xml";
        //        public const string DepictionMetaDataFile = "DepictionMetadata.xml";
        //        public const string DepictionDataDir = "DepictionData";
        //        public const string GeoInformationFile = "DepictionGeoInformation.xml";
        //        public const string ElementDir = "AllDepictionElements";
        //        public const string ElemGeoFile = "ElementsGeolocated.xml";
        //        public const string ElemeNonGeoFile = "ElementsNonGeolocated.xml";
        //        public const string AnnotDir = "Annotations";
        //        public const string AnnotFile = "DepictionAnnotations.xml";
        //        public const string DisplayersDir = "Displayers";
        //        public const string BackdropFile = "DepictionElementBackdrop.xml";
        //        public const string RevealersFile = "DepictionRevealers.xml";
        //        public const string LibraryDir = "ElementLibrary";
        //        public const string LibraryIconBinDir = "icons";
        //        public const string ImagesDir = "Images";
        //        public const string ImagesFile = "DepictionImages.xml";
        //        public const string ImagesBinaryDir = "binaryFiles";
        //        public const string InteractionDir = "InteractionRules";
        //        public const string InteractionFile = "DepictionInteractionRules.xml";

        /**
         *The file format to open
         *DepictionDatatypes.xml
         *DepictionFormat.xml
         *DepictioMetadata.xml
         *  /DepictionData
         *   DepictionGeoInformation.xml
         *   /AllDepictionElements
         *    ElementsGeolocated.xml
         *    ElementsNonGeolocated.xml
         *   /Annotations
         *    DepictionAnnotations.xml
         *   /Displayers
         *    DepictionElementBackdrop.xml
         *    DepictionRevealers.xml
         *   /ElementLibrary
         *    *.dml
         *    /icons
         *     Icons files, with names that dont give image type.
         *   /Images
         *    DepictImages.xml
         *    /binaryfiles
         *     all images used, include icons. They usually don't have useful names or extensions
         *   /InteractionRules
         *    DepicitionInteractionRules.xml
         
         */
        public static XmlReader GetXmlReader(Stream stream)
        {
            var settings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true, CheckCharacters = false };
            return XmlReader.Create(stream, settings);
        }
        public static XmlReader GetStringReader(string stringValue)
        {
            var reader = new StringReader(stringValue);

            var settings = new XmlReaderSettings
            {
                IgnoreWhitespace = true,
                IgnoreComments = true,
                CheckCharacters = false,
                ConformanceLevel = ConformanceLevel.Fragment
            };
            //conformancelevel makes it so multiple parents can exist in a xml file, in particular this
            //happens when reading depiction property data (terrain) from a xmal reader constructed
            //from the innerxml string.
            return XmlReader.Create(reader, settings);
        }
        public static XmlWriter GetXmlWriter(Stream stream)
        {
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, CheckCharacters = false };
            return XmlWriter.Create(stream, settings);
        }
        private static readonly Dictionary<Type, DataContractSerializer> serializers = new Dictionary<Type, DataContractSerializer>();

        public static DataContractSerializer GetSerializer(Type type)
        {
            DataContractSerializer serializer;

            if (!serializers.TryGetValue(type, out serializer))
            {
                var xmlRootAttribute = type.GetCustomAttributes(typeof(XmlRootAttribute), false);
                if (xmlRootAttribute.Length > 0)
                    serializer = new DataContractSerializer(type, ((XmlRootAttribute)xmlRootAttribute[0]).ElementName, DepictionXmlNameSpace);
                else if (type.IsGenericType || type.IsArray)
                    serializer = new DataContractSerializer(type);
                else
                    serializer = new DataContractSerializer(type, type.Name, DepictionXmlNameSpace);

                serializers.Add(type, serializer);
            }

            return serializer;
        }


        public static IList<T> DeserializeItemList<T>(string localName, Type type, XmlReader reader)
        {
            var elementList = new List<T>();

            //Return empty list if the node type is empty (davidl). would it be better to return a null?
            if (reader.NodeType.Equals(XmlNodeType.EndElement)) return elementList;
            try
            {
                bool listIsEmpty = reader.IsEmptyElement;
                reader.ReadStartElement(localName);//, ns);
                if (!listIsEmpty)
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        var interType = type;
                        if (interType == null)
                        {
                            var typeName = reader.Name;
                            interType = Type.GetType(typeName);
                        }
                        try
                        {
                            //If a single object gives a problem keep going
                            var element = DeserializeObject(interType, reader);
                            elementList.Add((T)element);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                    reader.ReadEndElement();
                }
            }
            catch (Exception ex) { return new List<T>(); }

            return elementList;
        }
        public static object DeserializeObject(Type type, XmlReader reader)
        {
            object ret;
            if (type.GetInterface(typeof(IXmlSerializable).Name) != null)
            {
                ret = Activator.CreateInstance(type);
                ((IXmlSerializable)ret).ReadXml(reader);
            }
            else
            {
                ret = GetSerializer(type).ReadObject(reader);
            }

            return ret;
        }
        public static T DeserializeObject<T>(string localName, XmlReader reader)
        {
            return (T)DeserializeObject<T>(localName, typeof(T), reader);
        }
        public static object DeserializeObject<T>(string localName, Type type, XmlReader reader)
        {
            //var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement(localName);//, ns);
            object ret;
            if (type.GetInterface(typeof(IXmlSerializable).Name) != null)
            {
                //                var construct = type.GetConstructor(Type.EmptyTypes);//Future reference
                ret = Activator.CreateInstance(type);
                ((IXmlSerializable)ret).ReadXml(reader);
            }
            else
            {
                ret = null;
                //                if (type.Equals(typeof(object)) && reader.GetAttribute("type") != null)
                //                {
                //                    var inType = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(reader.GetAttribute("type"));
                //                    ret = Activator.CreateInstance(inType);
                //                    ((IXmlSerializable)ret).ReadXml(reader);
                //                }
                //                else
                //                {
                //                    ret = GetSerializer(type).ReadObject(reader);
                //                }
            }
            reader.ReadEndElement();
            return ret;
        }

    }
}