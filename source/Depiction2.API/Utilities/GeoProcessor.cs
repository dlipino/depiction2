﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Utilities
{
    /// <summary>
    /// This is a set of geo utility methods for general purpose use.
    /// </summary>
    public static class GeoProcessor
    {
        public static IDepictionGeometry ConvertContoursToPolygonWithHoles(IList<List<Point>> cList)
        {
            List<Point> contourList;
            var polygons = new List<IDepictionGeometry>();
            var allPolygons = new List<IDepictionGeometry>();


            for (int i = 0; i < cList.Count; i++)
            {
                contourList = cList[i];
                if (contourList.Count <= 3)
                    continue;
                bool addLast = false;
                var geoType = DepictionGeometryType.LineString;
                if (!(contourList[0].X.Equals(contourList[contourList.Count - 1].X) && (contourList[0].Y.Equals(contourList[contourList.Count - 1].Y))))
                {
                    addLast = true;
                    geoType = DepictionGeometryType.Polygon;
                }
                var pList = new List<Point>();
                foreach (var p in contourList)
                {
                    pList.Add(new Point(p.X, p.Y));
                }
                if (addLast) pList.Add(new Point(contourList[0].X, contourList[0].Y));
         
                var poly = DepictionGeometryService.CreateGeometry();
                poly.SetGeometry(pList);
                if (!poly.IsSimple) continue;
                allPolygons.Add(poly);
                //see if this polygon contains the seed point

                polygons.Add(poly);
            }
            var shellPoly = polygons[0];
            if (polygons.Count > 0)
            {
                for (int i = 0; i < allPolygons.Count; i++)
                {
                    if (!(allPolygons[i]).Equals(polygons[0]))
                    {
                        if (allPolygons[i].Within(polygons[0]))
                        {
                            polygons.Add(allPolygons[i]);
                        }
                    }
                }
            }
            return shellPoly;
        }

        public static IDepictionGeometry ConvertToPolygon(IList<List<IDepictionLatitudeLongitude>> list)
        {
            return ConvertContoursToPolygonWithHoles(list);
        }

        /// <summary>
        /// Convert a set of polygons to a single polygon surrounding a seed point
        /// This single polygon can contain holes
        /// If so, the first poly in the returned list is the shell and the rest are holes
        /// </summary>
        public static IDepictionGeometry ConvertContoursToPolygonWithHoles(IList<List<IDepictionLatitudeLongitude>> cList)
        {
            //Nov 7 2013 so many old comments if have not idea what is useful anymore. (davidl)
            //            var precModel = new PrecisionModel(); //FLOATING POINT precision
            //            var geomFactory = new GeometryFactory(precModel, 32767);

            //construct a box around the seed point
            //to account for accuracy shortcomings
            //cList contains all the contours found by an iso-flood algorithm
            //
            //discard the ones that don't contain the seed point
            List<IDepictionLatitudeLongitude> contourList;
            var polygons = new List<IDepictionGeometry>();
            var allPolygons = new List<IDepictionGeometry>();


            for (int i = 0; i < cList.Count; i++)
            {
                contourList = cList[i];
                if (contourList.Count <= 3)
                    continue;
                bool addLast = false;
                var geoType = DepictionGeometryType.LineString;
                if (!(contourList[0].Longitude.Equals(contourList[contourList.Count - 1].Longitude) && (contourList[0].Latitude.Equals(contourList[contourList.Count - 1].Latitude))))
                {
                    //                    contourList.Add(new LatitudeLongitude(contourList[0].Latitude, contourList[0].Longitude));
                    addLast = true;
                    geoType = DepictionGeometryType.Polygon;
                }
                var pList = new List<Point>();
                foreach (var p in contourList)
                {
                    pList.Add(new Point(p.Longitude, p.Latitude));
                }
                if (addLast) pList.Add(new Point(contourList[0].Longitude, contourList[0].Latitude));
                //                var carr = new Coordinate[contourList.Count];
                //                for (int j = 0; j < contourList.Count; j++)
                //                {
                //                    var p = contourList[j];
                //                    carr[j] = new Coordinate(p.Longitude, p.Latitude);
                //                }


                //                var lRing = geomFactory.CreateLinearRing(carr);
                //                var poly = new DepictionGeometry(geomFactory.CreatePolygon(lRing, null));
                var poly = DepictionGeometryService.CreateGeometry();//new GeometryGdalWrap();
                poly.SetGeometry(pList);
                if (!poly.IsSimple) continue;
                allPolygons.Add(poly);
                //see if this polygon contains the seed point

                polygons.Add(poly);
            }
            var shellPoly = polygons[0];
            if (polygons.Count > 0)
            {
                for (int i = 0; i < allPolygons.Count; i++)
                {
                    if (!(allPolygons[i]).Equals(polygons[0]))
                    {
                        if (allPolygons[i].Within(polygons[0]))
                        {
                            polygons.Add(allPolygons[i]);
                        }
                    }
                }
            }
            return shellPoly;
            //            ILinearRing shell = ((IPolygon)shellPoly.Geometry).Shell;
            //            var holes = new List<ILinearRing>();
            //            for (int i = 1; i < polygons.Count; i++)
            //            {
            //                holes.Add(((IPolygon)polygons[i].Geometry).Shell);
            //
            //            }
            //            IGeometry geometry = geomFactory.CreatePolygon(shell, holes.ToArray());
            //            geometry = geometry.Buffer(0.000001);
            //            return geometry as IPolygon;
        }

        ///<summary>
        /// At a given latlon position, convert meters to decimal degrees
        /// </summary>
        public static double MetersToDecimalDegrees(double lon, double lat, double offset)
        {
            throw new NotImplementedException();
//            var north = new LatitudeLongitudeDotSpatial(lat, lon);
//            var offsetPoint = north.TranslateTo(Azimuth.North, offset, DistanceUnit.Meters);
//            return Math.Abs(offsetPoint.Latitude - north.Latitude);
        }

        public static bool IsPolygonSimple(ArrayList vertices)
        {

            //            var precModel = new PrecisionModel(); //FLOATING POINT precision
            //            var geomFactory = new GeometryFactory(precModel, 32767);
            //            var carr = new Coordinate[vertices.Count];
            //            var geom = new Geometry();
            //            for (int j = 0; j < vertices.Count; j++)
            //            {
            //                var p = (Point)vertices[j];
            //                carr[j] = new Coordinate(p.X, p.Y);
            //            }
            //#pragma warning disable 612,618
            //            var cseq = new DefaultCoordinateSequence(carr);
            //#pragma warning restore 612,618
            //            var lRing = new LinearRing(cseq, geomFactory);
            //
            //            var poly = new GeometryGdalWrap(new Polygon(lRing));
            var poly = DepictionGeometryService.CreateGeometry();//new GeometryGdalWrap();
            var pointList = new List<Point>();
            for (int j = 0; j < vertices.Count; j++)
            {
                if (vertices[j] is Point)
                {

                    var p = (Point)vertices[j];
                    pointList.Add(p);
                }
            }
            poly.SetGeometry(pointList);
            return poly.IsSimple;

        }
        /// <summary>
        /// Convert a set of polygons to a single polygon surrounding a seed point.
        /// Will discard any polygons that do not contain tha seed point.
        /// This single polygon can contain holes.
        /// If so, the first poly in the returned list is the shell and the rest are holes.
        /// </summary>
        /// <param name="cList"></param>
        /// <param name="seedLon">The longitude of the seed point.</param>
        /// <param name="seedLat">The latitude of the seed point.</param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static List<List<Point>> ConvertContoursToPolygonWithHoles(List<List<IDepictionLatitudeLongitude>> cList, double seedLon, double seedLat, double tolerance)
        {
            bool containmentTest = true;
            if (seedLon == seedLat && seedLon == -1000)
            {
                //ignore the seed point
                containmentTest = false;
            }

            //            var precModel = new PrecisionModel(); //FLOATING POINT precision
            //            var geomFactory = new GeometryFactory(precModel, 32767);

            //construct a box around the seed point
            //to account for accuracy shortcomings

            IDepictionGeometry gridCellBox = null;
            if (tolerance > 0)
                gridCellBox = CreateGridCellPolygon(seedLon, seedLat, tolerance);

            //cList contains all the contours found by an iso-flood algorithm
            //
            //discard the ones that don't contain the seed point

            var seedPoint = DepictionGeometryService.CreatePointGeometry(new Point(seedLon, seedLat));//new GeometryGdalWrap(new Point(seedLon, seedLat));

            List<IDepictionLatitudeLongitude> contourList;
            var polygons = new List<IDepictionGeometry>();
            var polyPoints = new List<List<Point>>();
            int smallestPolygonLength = 1000000;
            var allPolygons = new List<IDepictionGeometry>();
            var allPolyPoints = new List<List<Point>>();



            int i;
            for (i = 0; i < cList.Count; i++)
            {
                contourList = cList[i];
                if (containmentTest && smallestPolygonLength < contourList.Count)
                    continue;
                if (contourList.Count <= 3)
                    continue;

                if (!(contourList[0].Equals(contourList[contourList.Count - 1])))
                {
                    contourList.Add(contourList[0]);
                }
                var carr = new List<Point>();// new Coordinate[contourList.Count];
                for (int j = 0; j < contourList.Count; j++)
                {
                    var p = contourList[j];
                    carr.Add(new Point(p.Longitude, p.Latitude));
                    //                    carr[j] = new Coordinate(p.Longitude, p.Latitude);
                }
                //#pragma warning disable 612,618
                //                var cseq = new DefaultCoordinateSequence(carr);
                //#pragma warning restore 612,618
                //                var lRing = new LinearRing(cseq, geomFactory);

                var poly = DepictionGeometryService.CreateGeometry(carr);//new GeometryGdalWrap(carr);//new Polygon(lRing));

                if (!poly.IsSimple) continue;
                allPolygons.Add(poly);
                allPolyPoints.Add(carr);
                //see if this polygon contains the seed point
                //So i guess this is checking for holes
                bool contained;
                if (tolerance > 0) contained = poly.Intersects(gridCellBox);
                else contained = poly.Contains(seedPoint);

                if (!containmentTest) contained = true;

                if (!contained) continue;
                smallestPolygonLength = contourList.Count;
                polygons.Add(poly);
                polyPoints.Add(carr);

            }
            //Umm some strange way of checking for holes and such
            //and some hacking to match the 1.4 way of getting valuyes

            if (polygons.Count > 0)
            {
                for (i = 0; i < allPolygons.Count; i++)
                {
                    if (!(allPolygons[i]).Equals(polygons[0]))
                    {
                        if (allPolygons[i].Within(polygons[0]))
                        {
                            polygons.Add(allPolygons[i]);
                            polyPoints.Add(allPolyPoints[i]);
                        }
                    }
                }
            }
            //            var coordList = new List<List<Point>>();
            //            for (int j = 0; j < polygons.Count; j++)
            //            {
            //                var newPoly = (polygons[j]);
            //
            //                var newArray = new List<Point>();
            //                var vertices = newPoly.Coordinates; //SimplifyPolygon(newPoly,5/*meters*/)
            //
            //                if (vertices == null) continue;
            //                if (vertices.Length <= 3) continue;
            //
            //                for (i = 0; i < vertices.Length; i++)
            //                {
            //                    newArray.Add(new Point((float)vertices[i].X, (float)vertices[i].Y));
            //                }
            //                coordList.Add(newArray);
            //            }
            return polyPoints;
        }

        public static List<Point>[] ConvertToPolygon(List<List<IDepictionLatitudeLongitude>> list, double x, double y, double tolerance)
        {
            var relevantCoords = ConvertContoursToPolygonWithHoles(list, x, y, tolerance);

            var latlonlist = new List<Point>[relevantCoords.Count];

            for (int i = 0; i < relevantCoords.Count; i++)
            {
                latlonlist[i] = new List<Point>();
                var contourList = relevantCoords[i];
                for (int j = 0; j < contourList.Count; j++)
                {
                    var pt = contourList[j];
                    latlonlist[i].Add(pt);//new LatitudeLongitude(pt.Y, pt.X));
                }
            }
            return latlonlist;
        }

        public static bool IsCoordinateListClosed(List<Point> list)
        {
            if (list == null) return false;

            if (list[0].X.Equals(list[list.Count - 1].X) && list[0].Y.Equals(list[list.Count - 1].Y))
                return true;

            return false;
        }

        public static IDepictionGeometry CreateGridCellPolygon(double lon, double lat, double spacing)
        {
            var coordList = new List<Point>();
            var center = new Point(lon, lat);
            var distance = DepictionPositionService.Instance;
            var topLeft = distance.TranslatePoint(center,-90d, spacing / 2, MeasurementSystem.Metric, MeasurementScale.Normal);
            topLeft = distance.TranslatePoint(topLeft, 0d, spacing / 2, MeasurementSystem.Metric, MeasurementScale.Normal);
            //top left
            coordList.Add(new Point(topLeft.X, topLeft.Y));
            //top right
            topLeft = distance.TranslatePoint(topLeft, 90d, spacing, MeasurementSystem.Metric, MeasurementScale.Normal);
            coordList.Add(new Point(topLeft.X, topLeft.Y));
            //bottom right
            topLeft = distance.TranslatePoint(topLeft, 180d, spacing, MeasurementSystem.Metric, MeasurementScale.Normal);
            coordList.Add(new Point(topLeft.X, topLeft.Y));

            //bottom left
            topLeft = distance.TranslatePoint(topLeft, -90d, spacing, MeasurementSystem.Metric, MeasurementScale.Normal);
            coordList.Add(new Point(topLeft.X, topLeft.Y));

//            var topLeft = distance.TranslatePoint(center,-90d, spacing / 2, DistanceUnit.Meters));
//            topLeft = topLeft.TranslateTo(0d, spacing / 2, DistanceUnit.Meters);
//            //top left
//            coordList.Add(new Point(topLeft.Longitude, topLeft.Latitude));
//            //top right
//            topLeft = topLeft.TranslateTo(90d, spacing, DistanceUnit.Meters);
//            coordList.Add(new Point(topLeft.Longitude, topLeft.Latitude));
//            //bottom right
//            topLeft = topLeft.TranslateTo(180d, spacing, DistanceUnit.Meters);
//            coordList.Add(new Point(topLeft.Longitude, topLeft.Latitude));
//            //bottom left
//            topLeft = topLeft.TranslateTo(-90d, spacing, DistanceUnit.Meters);
//            coordList.Add(new Point(topLeft.Longitude, topLeft.Latitude));

            coordList.Add(coordList[0]);
            return DepictionGeometryService.CreateGeometry(coordList);
        }
        //
        //given a lon,lat location, create a polygon (square) representing the four corners of the grid cell
        //in Depiction, the terrain/coverage layer has the representative location of a grid cell to be the
        //SOUTHWEST corner of the pixel. So...this is the static method we use to generate a grid cell polygon
        //        public static Polygon CreateGridCellPolygonSW(IGeometryFactory factory, double lon, double lat, double spacing)
        //        {
        //            var coordList = new List<ICoordinate>();
        //            var sw = new LatitudeLongitude(lat, lon);
        //
        //            var topLeft = sw.TranslateTo(0d, spacing, GeoFramework.DistanceUnit.Meters);
        //            //top left
        //            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
        //            //top right
        //            topLeft = topLeft.TranslateTo(90d, spacing, GeoFramework.DistanceUnit.Meters);
        //            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
        //            //bottom right
        //            topLeft = topLeft.TranslateTo(180d, spacing, GeoFramework.DistanceUnit.Meters);
        //            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
        //            //bottom left
        //            topLeft = topLeft.TranslateTo(-90d, spacing, GeoFramework.DistanceUnit.Meters);
        //            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
        //
        //            coordList.Add(coordList[0]);
        //
        //            ILinearRing sqRing = new LinearRing(coordList.ToArray());
        //            return (Polygon)factory.CreatePolygon(sqRing, null);
        //        }
    }
}
