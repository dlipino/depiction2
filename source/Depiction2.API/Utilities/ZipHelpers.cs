﻿using System;
using System.IO;
using Ionic.Zip;

namespace Depiction2.API.Utilities
{
    public class ZipHelpers
    {
        public static bool UnzipFileToDirectory(string fileName, string archiveDir)
        {
            if (Directory.Exists(archiveDir))
                Directory.Delete(archiveDir, true);
            try
            {
                using (var zip = new ZipFile(fileName))
                {
                    zip.ExtractAll(archiveDir);
                }
            }
            catch (Exception)
            {
                //Grrr total bad hacks
                return false;
            }
            return true;
        }
//        static public string UnpackFileFromZip(string zipfileName, string imageFileName, DepictionFolderService holdingFolder)
//        {
//            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
//            var fullFileName = string.Empty;
//            using (var zipFile = ZipFile.Read(zipfileName))
//            {
//                var imageDir = Depiction14FileOpenHelpers.DepictionDataDir + "/" + Depiction14FileOpenHelpers.ImagesDir;
//
//                var imageBinDir = imageDir + "/" + Depiction14FileOpenHelpers.ImagesBinaryDir;
//                var fileEntry = imageBinDir + "/" + imageFileName;
//                fullFileName = Path.Combine(holdingFolder.FolderName, Depiction14FileOpenHelpers.DepictionDataDir, Depiction14FileOpenHelpers.ImagesDir, Depiction14FileOpenHelpers.ImagesBinaryDir, imageFileName);
//
//                if (zipFile.ContainsEntry(fileEntry))
//                {
//                    var entry = zipFile[fileEntry];
//                    entry.Extract(holdingFolder.FolderName);
//                }
//            }
//            return fullFileName;
//        }
    }
}