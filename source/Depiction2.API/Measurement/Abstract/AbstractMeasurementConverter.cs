﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement.Abstract
{
    abstract public class AbstractMeasurementConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                var measurement = (IMeasurement)value;
//                return measurement.IntialUnitsValue();
                //                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
                var val = measurement.GetValue(measurement.InitialSystem, measurement.InitialScale);
//                bool isSingle = ToleranceEquals(val, 1, .0005);
                return string.Format(string.Format("{{0:N{0}}} {{1}}", 6),val, measurement.GetUnits(measurement.InitialSystem, measurement.InitialScale));
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}