﻿using System;
using System.ComponentModel;
using Depiction2.API.Measurement.Abstract;
using Depiction2.API.Measurement.Converters;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement
{
    [TypeConverter(typeof(TemperatureConverter))]
    public class Temperature : AbstractMeasurement
    {
        #region Conversion constants

        private const double fToCMult = 0.55555;
        private const double fToCOffset = -17.77777;
        private const double cToFMult = 1.8;
        private const double cToFOffset = 32d;
        #endregion

        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1}
                                                                  };

        private static readonly double[,] offsetFactors = new[,]
                                                              {
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0}
                                                              };

        public Temperature(MeasurementSystem system, MeasurementScale scale, double value)
        {
            initialSystem = system;
            initialScale = scale;
            numericValue = value;
        }

        public Temperature()
        { }


        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }
        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool singular)
        {
            var systemAndScale = (int)system + (int)scale;
            switch (systemAndScale)
            {
                case MeasurementConstants.MetricSmall:
                case MeasurementConstants.MetricNormal:
                case MeasurementConstants.MetricLarge:
                    return "°C";
                case MeasurementConstants.ImperialSmall:
                case MeasurementConstants.ImperialNormal:
                case MeasurementConstants.ImperialLarge:
                    return "°F";

                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Temperature(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }

        public override int CompareTo(IMeasurement other)
        {
            var areaOther = other as Temperature;
            if (areaOther == null) throw new NotSupportedException("Cannot match type for comparison.");
            var scale = MeasurementScale.Normal;
            var units = MeasurementSystem.Metric;

            double EPSILON = .000001;
            if (Math.Abs(GetValue(units, scale) - areaOther.GetValue(units, scale)) < EPSILON) return 0;
            if (GetValue(units, scale) > areaOther.GetValue(units, scale)) return 1;
            if (GetValue(units, scale) < areaOther.GetValue(units, scale)) return -1;
            return -1;
        }
    }
}