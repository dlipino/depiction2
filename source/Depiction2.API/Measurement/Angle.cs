﻿using System;
using System.ComponentModel;
using Depiction2.API.Measurement.Converters;
using Depiction2.Base;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement
{
    [TypeConverter(typeof(AngleConverter))]
    public class Angle : IDeepCloneable<Angle>, IComparable<Angle>
    {
        public double Value { get; set; }
        public Angle DeepClone()
        {
            var newAngle = new Angle();
            newAngle.Value = Value;
            return newAngle;
        }

        public int CompareTo(Angle other)
        {
            double EPSILON = .000001;
            if (Math.Abs(Value - other.Value) < EPSILON) return 0;
            if (Value > other.Value) return 1;
            if (Value < other.Value) return -1;
            return -1;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var angle = obj as Angle;
            if (angle == null) return false;
            return Value.Equals(((Angle)obj).Value);
        }

        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
    }
}