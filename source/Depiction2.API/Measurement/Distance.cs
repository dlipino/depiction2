using System;
using System.ComponentModel;
using Depiction2.API.Measurement.Abstract;
using Depiction2.API.Measurement.Converters;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement
{
    /// <summary>
    /// A type representing a distance, e.g. meters. For use as the type of an element's property.
    /// </summary>
    [TypeConverter(typeof(DistanceConverter))]
    public class Distance : AbstractMeasurement
    {
        public const string ftString = "ft";
        public const string kmString = "km";
        public const string mileString = "miles";
        #region conversion Constants
        public const double cmToInch = 0.393700787; /*cm to in*/
        public const double inchToCm = 1 / cmToInch;
        public const double cmToFeet = 0.032808399; /*cm to ft*/
        public const double feetToCm = 1 / cmToFeet;
        public const double cmToMile = .00000621371192; /*cm to miles*/
        public const double mileToCm = 1 / cmToMile;

        public const double mToInch = 39.3700787; /*m to in*/
        public const double inchToM = 1 / mToInch;
        public const double mToFeet = 3.2808399; /*m to ft*/
        public const double feetToM = 1 / mToFeet;
        public const double mToMile = .000621371192; /*m to miles*/
        public const double mileToM = 1 / mToMile;

        public const double kmToInch = 39370.0787; /*km to in*/
        public const double inchToKm = 1 / kmToInch;
        public const double kmToFeet = 3280.8399; /*km to ft*/
        public const double feetToKm = 1 / kmToFeet;
        public const double kmToMile = 0.621371192; /*km to miles*/
        public const double mileToKm = 1 / kmToMile;

        public const double cmToM = .01; //CM to M
        public const double cmToKm = cmToM * mToKm;
        public const double mToCm = 100;
        public const double mToKm = .001;//M toKM
        public const double kmToM = 1000;
        public const double kmToCm = kmToM * mToCm;

        public const double feetToInch = 12d;//Feet to Inches
        public const double inchToFeet = 1 / feetToInch;
        public const double mileToFeet = 5280d;//Miles to Feet
        public const double feetToMile = 1 / mileToFeet;
        public const double mileToInch = mileToFeet * feetToInch;
        public const double inchToMile = 1 / mileToInch;
        #endregion

        //For now the order of the conversion factors will be (there should be some sort of and/or operater for this
        //MS =0 (Metric = 0 Small = 0)
        //MN =1(Metric = 0 Medium = 1)
        //ML =2(Metric = 0 Large = 2)
        //IS = 3 (Imperial = 3 Small = 0)
        //IN = 4(Imperial = 3 Medium = 1)
        //IL = 5(Imperial = 3 Large = 2)
        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1d,cmToM,cmToKm,cmToInch,cmToFeet,cmToMile},
                                                                      {mToCm,1d,mToKm,mToInch,mToFeet,mToMile},
                                                                      {kmToCm,kmToM,1d,kmToInch,kmToFeet,kmToMile},
                                                                      {inchToCm,inchToM,inchToKm,1d,inchToFeet,inchToMile},
                                                                      {feetToCm,feetToM,feetToKm,feetToInch,1d,feetToMile},
                                                                      {mileToCm,mileToM,mileToKm,mileToInch,mileToFeet,1d}
                                                                  };

        private static readonly double[,] offsetFactors = new[,] 
                                                              {
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d}
                                                              };
        /// <summary>
        /// Create a new distance with this system, scale and value.
        /// </summary>
        /// <param name="measureSystem"></param>
        /// <param name="measureScale"></param>
        /// <param name="value"></param>
        public Distance(MeasurementSystem measureSystem, MeasurementScale measureScale, double value)
        {
            initialSystem = measureSystem;
            initialScale = measureScale;
            this.numericValue = value;
        }

        /// <summary>
        /// For serialization use, only.
        /// </summary>
        public Distance()
        {}
        
        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale, bool isSingular)
        {
            var systemAndScale = (int) system + (int) scale;
            var nonSingle = "s";
            if (isSingular)
            {
                nonSingle = string.Empty;
            }
            switch (systemAndScale)
            {
                case MeasurementConstants.ImperialSmall:
                    if(!isSingular)
                    {
                        return "inches";
                    }
                    return "inch";
                case MeasurementConstants.ImperialNormal:
                    if (isSingular) return "foot";
                    return "feet";
                case MeasurementConstants.ImperialLarge:
                    return "mile" + nonSingle;
                case MeasurementConstants.MetricSmall:
                    return "centimeter" + nonSingle;
                case MeasurementConstants.MetricNormal:
                    return "meter" + nonSingle;
                case MeasurementConstants.MetricLarge:
                    return "km";
                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Distance(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }

        public override int CompareTo(IMeasurement other)
        {
            var areaOther = other as Distance;
            if (areaOther == null) throw new NotSupportedException("Cannot match type for comparison.");
            var scale = MeasurementScale.Normal;
            var units = MeasurementSystem.Metric;

            double EPSILON = .000001;
            if (Math.Abs(GetValue(units, scale) - areaOther.GetValue(units, scale)) < EPSILON) return 0;
            if (GetValue(units, scale) > areaOther.GetValue(units, scale)) return 1;
            if (GetValue(units, scale) < areaOther.GetValue(units, scale)) return -1;
            return -1;
        }
    }
}