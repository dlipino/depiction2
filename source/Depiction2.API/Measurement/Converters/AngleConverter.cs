﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Xml;
using Depiction2.API.Utilities;

namespace Depiction2.API.Measurement.Converters
{
    public class AngleConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var angle = (Angle)value;
                return string.Format(culture, "{0} degrees", angle.Value);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                var valueString = value as string;
                if ((valueString).EndsWith("degrees"))
                {
                    double angleValue;
                    if (double.TryParse(((string)value).Replace("degrees", ""), NumberStyles.Any, info, out angleValue))
                        return new Angle { Value = angleValue };
                }
                //                var doc = new XmlDocument();
                //                doc.LoadXml(valueString);
                //                doc.
                using (var reader = DepictionXmlFileUtilities.GetStringReader(valueString))
                {
                    if (reader != null)
                    {
                        reader.ReadStartElement();
                        var readAngle = reader.ReadElementContentAsDouble();
                        //                        var angleValue = DepictionXmlFileUtilities.DeserializeObject(typeof (Angle), reader);//Doesnt work.
                        return new Angle { Value = readAngle };
                    }
                }
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}