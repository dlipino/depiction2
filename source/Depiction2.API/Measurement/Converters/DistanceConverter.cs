﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction2.API.Converters;
using Depiction2.API.Measurement.Abstract;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement.Converters
{
    public class DistanceConverter : AbstractMeasurementConverter
    {
//        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
//        {
//            return (sourceType == typeof(string));
//        }
//
//        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
//        {
//            return (destType == typeof(string));
//        }
//
//        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
//        {
//            if (destinationType.Equals(typeof(string)))
//            {
//                var measurement = (IMeasurement)value;
//                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
//            }
//            return base.ConvertTo(context, culture, value, destinationType);
//        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Distance", out systemString, null);
                if (valueString == null) return null;


                var scale = MeasurementScale.Normal;
                var system = MeasurementSystem.Metric;

                switch (systemString.ToLower())
                {
                    case "m":
                    case "meter":
                    case "meters":
                    case "metre":
                    case "metres":
                    case "metric":
                        scale = MeasurementScale.Normal;
                        system = MeasurementSystem.Metric;
                        break;
                    case "ft":
                    case "feet":
                    case "foot":
                    case "'":
                    case "imperial":
                        scale = MeasurementScale.Normal;
                        system = MeasurementSystem.Imperial;
                        break;
                    case "in":
                    case "inch":
                    case "inches":
                    case "\"":
                    case "imperialsmall":
                        scale = MeasurementScale.Small;
                        system = MeasurementSystem.Imperial;
                        break;
                    case "cm":
                    case "centimeter":
                    case "centimeters":
                    case "centimetre":
                    case "centimetres":
                    case "metricsmall":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Small;
                        
                        break;
                    case "mi":
                    case "mile":
                    case "miles":
                    case "imperiallarge":
                        scale = MeasurementScale.Large;
                        system = MeasurementSystem.Imperial;
                        break;
                    case "km":
                    case "kilometer":
                    case "kilometers":
                    case "kilometre":
                    case "kilometres":
                    case "metriclarge":
                        scale = MeasurementScale.Large;
                        system = MeasurementSystem.Metric;
                        break;
                    default:
                        throw new Exception(string.Format("Distance cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Distance(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}