﻿using System;
using System.ComponentModel;
using Depiction2.API.Measurement.Abstract;
using Depiction2.API.Measurement.Converters;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Measurement
{
    [TypeConverter(typeof(VolumeConverter))]
    public class Volume : AbstractMeasurement
    {
        const double galToOz = 128;/*gal to oz*/
        const double ozToGal = 1/galToOz;

        const double galToL = 3.78541178;/*gal to l*/
        private const double lToGal = 1 / galToL;
        private const double galToml = 3785.41178; /*gallons to ml*/ 
        private const double mlToGal = 1 / galToml;

        const double LToml = 1000;/*l to ml*/
        const double mlToL = 1 / LToml;/*l to ml*/
        const double mlToOz = 0.0338140227; /*ml to oz*/
        private const double OzToMl = 1 / mlToOz;
       
        const double LToOz = 33.8140227;/*l to oz*/
        private const double OzToL = 1 / LToOz;


        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1,mlToL,mlToL,mlToOz,mlToGal,mlToGal},
                                                                      {LToml,1,1,LToOz,lToGal,lToGal},
                                                                      {LToml,1,1,LToOz,lToGal,lToGal},
                                                                      {OzToMl,OzToL,OzToL,1,ozToGal,ozToGal},
                                                                      {galToml,galToL,galToL,galToOz,1d,1d},
                                                                      {galToml,galToL,galToL,galToOz,1d,1d}
                                                                  };

        private static readonly double[,] offsetFactors = new[,]  { 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}
                                                                  };
        public Volume()
        { }
        public Volume(MeasurementSystem measurementSystem, MeasurementScale measurementScale, double value)
        {
            initialSystem = measurementSystem;
            initialScale = measurementScale;

            this.numericValue = value;
        }

        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool isSingular)
        {
            int systemAndScale = (int)system + (int)scale;
            var nonSingle = "s";
            if(isSingular)
            {
                nonSingle = string.Empty;
            }
            switch (systemAndScale)
            {
                case MeasurementConstants.MetricSmall:
                    return "milliliter" + nonSingle;
                case MeasurementConstants.MetricNormal:
                case MeasurementConstants.MetricLarge:
                    return "liter" + nonSingle;
                case MeasurementConstants.ImperialSmall:
                    return "ounce" + nonSingle;
                case MeasurementConstants.ImperialNormal:
                case MeasurementConstants.ImperialLarge:
                    return "gallon" + nonSingle;

                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Volume(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }

        public override int CompareTo(IMeasurement other)
        {
            var areaOther = other as Volume;
            if (areaOther == null) throw new NotSupportedException("Cannot match type for comparison.");
            var scale = MeasurementScale.Normal;
            var units = MeasurementSystem.Metric;

            double EPSILON = .000001;
            if (Math.Abs(GetValue(units, scale) - areaOther.GetValue(units, scale)) < EPSILON) return 0;
            if (GetValue(units, scale) > areaOther.GetValue(units, scale)) return 1;
            if (GetValue(units, scale) < areaOther.GetValue(units, scale)) return -1;
            return -1;
        }
    }
}