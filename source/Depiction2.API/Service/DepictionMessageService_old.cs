using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction2.Base.Service;
using Depiction2.Utilities.MVVM;

namespace Depiction2.API.Service
{
    public class DepictionMessageService : IDepictionNotificationService
    {
        public event Action MessageAdded;
        Dictionary<string, IDepictionMessage> allMessages = new Dictionary<string, IDepictionMessage>();
        ObservableCollection<IDepictionMessage> messageDisplayerCollection = new ObservableCollection<IDepictionMessage>();

        private DelegateCommand<IDepictionMessage> removeMessageCommand;

        public ICommand RemoveMessageCommand
        {
            get
            {
                if (removeMessageCommand == null)
                {
                    removeMessageCommand = new DelegateCommand<IDepictionMessage>(RemoveMessage);

                }
                return removeMessageCommand;
            }
        }

        public ReadOnlyObservableCollection<IDepictionMessage> MessageCollection
        {
            get
            {
                return new ReadOnlyObservableCollection<IDepictionMessage>(messageDisplayerCollection);
            }
        }

        #region Notification messages

        public bool DisplayMessage(IDepictionMessage message)
        {
            if (!allMessages.ContainsKey(message.MessageId))
            {
                if (!message.MessageDisplayTime.Equals(double.NaN))
                {
                    DisplayTimedMessage(message, (int)(message.MessageDisplayTime * 1000));
                }
                else
                {
                    allMessages.Add(message.MessageId, message);
                    PrivateAddMessage(message);
                    if (MessageAdded != null) MessageAdded.Invoke();
                    return true;
                }
            }
            return false;
        }

        public bool DisplayMessageString(string simpleMessage)
        {
            return DisplayMessage(new DepictionMessage(simpleMessage));
        }

        public bool DisplayMessageString(string simpleMessage, int timeSeconds)
        {
            return DisplayMessage(new DepictionMessage(simpleMessage, timeSeconds));
        }

        protected void DisplayTimedMessage(IDepictionMessage message, int timeMilliseconds)
        {
            if (!allMessages.ContainsKey(message.Mess+
ageId))
            {
                BackgroundWorker messageRemoveTimer = new BackgroundWorker();
                messageRemoveTimer.DoWork += messageRemover_DoWork;
                messageRemoveTimer.RunWorkerCompleted += messageRemoveTimer_RunWorkerCompleted;
                var parameters = new Dictionary<string, object> { { "message", message }, { "time", timeMilliseconds } };
                allMessages.Add(message.MessageId, message);
                PrivateAddMessage(message);
                messageRemoveTimer.RunWorkerAsync(parameters);
            }
        }

        void messageRemoveTimer_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null) return;
            var message = e.Result as IDepictionMessage;
            RemoveMessage(message);
        }

        void messageRemover_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as Dictionary<string, object>;

            if (args != null)
            {
                var message = args["message"] as IDepictionMessage;
                if (message == null) return;
                var time = (int)args["time"];
                Thread.Sleep(time);
                e.Result = message;
            }
        }
        public void RemoveMessage(IDepictionMessage message)
        {
            if (message != null) PrivateRemoveMessage(message);
        }

        #endregion

        #region dialog modification

//        static public MessageBoxResult FileImportWithUnlimitedAreaWarning()
//        {
//            var message = string.Format("Importing a GIS data file without limiting its region may cause significant performance issues depending on the size of the file. Continue?");
//            return DepictionAccess.NotificationService.DisplayInquiryDialog(message, "No Region Bounds", MessageBoxButton.YesNo);
//        }

        public void SendNotificationDialog(string message)
        {
            PrivateShowNonModalDialogWithNonNullOwner(null, message, "Helper dialog");
        }

        public void SendNotificationDialog(string message, string title)
        {
            PrivateShowNonModalDialogWithNonNullOwner(null, message, title);
        }

        public void SendNotificationDialog(Window owner, string message, string title)
        {
            PrivateShowNonModalDialogWithNonNullOwner(owner, message, title);
        }

        public MessageBoxResult DisplayInquiryDialog(Window owner, string message, string title, MessageBoxButton buttonOptions)
        {
            return PrivateSendInquiryDialogWithNonNullOwner(owner, message, title, buttonOptions);
        }

        public MessageBoxResult DisplayInquiryDialog(string message, string title, MessageBoxButton buttonOptions)
        {
            return PrivateSendInquiryDialogWithNonNullOwner(null, message, title, buttonOptions);
        }
        #endregion
        #region static helpers 
        //This is just a temp method. Eventually we will need to create a more permanant messaging system.
        static public bool? DisplayModalYesNoMessageBox(string messageBoxText, string messageBoxTitle)
        {
            var mainWindow = Application.Current.MainWindow as IDepictionMainWindow;
            if (mainWindow == null) return null;
            var result = mainWindow.MessageDialogs.ShowModalDialog(Application.Current.MainWindow, messageBoxText, messageBoxTitle, MessageBoxButton.YesNoCancel);
            if (result.Equals(MessageBoxResult.No)) return false;
            if (result.Equals(MessageBoxResult.Yes)) return true;
            return null;
        }
        #endregion
        #region Private helper methods
        private IDepictionMainWindow GetMainWindow()
        {
            var current = Application.Current;
            if (current == null) return null;
            var window = current.MainWindow as IDepictionMainWindow;
            if (window == null) return null;
            return window;

        }
        private void PrivateShowNonModalDialogWithNonNullOwner(Window owner, string message, string title)
        {
            var window = GetMainWindow();
            if (window == null) return;
            var ownedWindow = owner;
            if (owner == null) ownedWindow = window as Window;
             window.MessageDialogs.ShowNonModalDialog(ownedWindow, message, title);
        }
        private MessageBoxResult PrivateSendInquiryDialogWithNonNullOwner(Window owner, string message, string title, MessageBoxButton buttonOptions)
        {
            if(DepictionAccess.DispatchAvailableAndNeeded())
            {
                var result = (MessageBoxResult)DepictionAccess.DepictionDispatcher.Invoke(DispatcherPriority.Normal,
                                                                       new Func<Window, string, string, MessageBoxButton,MessageBoxResult>(PrivateSendInquiryDialogWithNonNullOwner),
                                                                       owner, message, title, buttonOptions) ;
                return result;
            }
            var window = GetMainWindow();
            if (window == null) return MessageBoxResult.None;
            var ownedWindow = owner;
            if (owner == null) ownedWindow = window as Window;
            var output = window.MessageDialogs.ShowModalDialog(ownedWindow, message, title, buttonOptions);
            return output;
        }

//        private void PrivateAddMessage(IDepictionMessage message)
//        {
//            if (DepictionAccess.DispatchAvailableAndNeeded())
//            {
//                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<IDepictionMessage>(PrivateAddMessage), message);
//                return;
//            }
//            messageDisplayerCollection.Add(message);
//            if (MessageAdded != null) MessageAdded.Invoke();
//        }
//
//        private void PrivateRemoveMessage(IDepictionMessage message)
//        {
//            if (DepictionAccess.DispatchAvailableAndNeeded())
//            {
//                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<IDepictionMessage>(PrivateRemoveMessage), message);
//                return;
//            }
//
//            if (allMessages.ContainsKey(message.MessageId))
//            {
//                allMessages.Remove(message.MessageId);
//                messageDisplayerCollection.Remove(message);
//            }
//        }
        #endregion
    }
}