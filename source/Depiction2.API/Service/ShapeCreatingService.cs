using System;
using System.Collections.Generic;
using System.Windows;

namespace Depiction2.Interaction.Shape
{
    /// <summary>
    /// Service to render circles and octagons.
    /// </summary>
    public class ShapeCreatingService
    {
       

        ///<summary>
        /// Adds to this element a zone of influence that is a polygon described the list of points.
        ///</summary>
        ///<param name="pointList">The polygon to use for this element's zone of influence.</param>
//        public static List<Point> GeneratePolygonZOIFromPointList(Point? center, List<Point> pointList,DistanceUnit distanceType)
//        {
//            throw new NotImplementedException();
//            if (center == null) return null;
//            var p = (Point) center;
//            if (pointList == null || pointList.Count <= 1) throw new Exception("Cannot generate polygon from empty point list.");
//            var latlong = new LatitudeLongitudeDotSpatial(p.Y, p.X);
//            if (!latlong.IsValid) throw new Exception("Cannot generate polygon with no center point.");
//
////            var geometryFactory = new GeometryFactory();
////
////            var coordinates = new CoordinateList();
////            var latlong = center as LatitudeLongitude;
////            if (latlong == null) return null;
//            var outList = new List<Point>();
//            foreach (var point in pointList)
//            {
//                var newPos = latlong.TranslatePositionToPoint(point.X, -point.Y, distanceType);
//                outList.Add(newPos);
////                coordinates.Add(new Coordinate(newPos.Longitude, newPos.Latitude));
//            }
////            ILinearRing linearRing = geometryFactory.CreateLinearRing(coordinates.ToArray());
//            return outList;
//        }
    }
}