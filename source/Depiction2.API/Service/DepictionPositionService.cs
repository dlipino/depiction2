﻿using System;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Tools.Positioning;
using Depiction2.Base.Measurement;

namespace Depiction2.API.Service
{
    //The position distance was correct, i just didn't realize how skinny chile was and how far zoomed out the map was.

    public class DepictionPositionService
    {
        private IPositionMeasureExtension currentMeasureSystem;
        public static bool IsActive
        {
            get
            {
                return ExtensionService.Instance.PositionMeasureTools.Count != 0;
            }
        }
        #region construciont and access
        private static DepictionPositionService _instance;
        static public DepictionPositionService Instance
        {
            get
            {
                if (_instance == null) _instance = new DepictionPositionService();
                return _instance;
            }
        }
        protected DepictionPositionService()
        {

        }
        #endregion
        private IPositionMeasureExtension GetAndSetFirstTool()
        {
            if (ExtensionService.Instance.PositionMeasureTools.Any())
            {
                currentMeasureSystem = ExtensionService.Instance.PositionMeasureTools.First().Value;
                return currentMeasureSystem;
            }
            return null;
            //            throw new NullReferenceException("GeometryService: No geometry tools have been loaded");
        }
        public  Point TranslatePoint(Point originPoint, double degreeDirction, double distance, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            if (currentMeasureSystem == null)
            {
                GetAndSetFirstTool();
            }
            if (currentMeasureSystem == null) return new Point(double.NaN,double.NaN);
            return currentMeasureSystem.TranslatePoint(originPoint, degreeDirction, distance, specificMeasurement,
                                                       specificScale);
        
        }
//        static public double DistanceTo(this Point start, Point end, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
//        {
//            return 0;
//            //            throw new NotImplementedException();
//        }
        public double DistanceBetween(Point start, Point end, MeasurementSystem specificMeasurement, MeasurementScale specificScale)
        {
            if(currentMeasureSystem == null)
            {
                GetAndSetFirstTool();
            }
            if (currentMeasureSystem == null) return double.NaN;
           return currentMeasureSystem.DistanceBetween(start, end, specificMeasurement, specificScale);
            //            return DistanceToAsGeoDistance(this, destinationLatLon, specificMeasurement, specificScale).Value;
        }
    }
}