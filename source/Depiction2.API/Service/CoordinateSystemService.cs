﻿using System;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Tools.Projection;
using Depiction2.Base.Geo;

namespace Depiction2.API.Service
{
    //Story specific service
    public class CoordinateSystemService
    {
        #region wtk systems
        public const string mercatorProjectedCoordinateSystem = "PROJCS[\"WGS 84 / World Mercator\",GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
               "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]]," +
               "PROJECTION[\"Mercator_1SP\"],PARAMETER[\"central_meridian\",0],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",0],PARAMETER[\"false_northing\",0],AUTHORITY[\"EPSG\",\"3395\"]," +
               "AXIS[\"Easting\",EAST],AXIS[\"Northing\",NORTH]]";

        public const string DepictionGeographicCoordinateSystem =
                 "GEOGCS[\"GCS_WGS_1984\"," +
                     "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                     "PRIMEM[\"Greenwich\",0]," +
                     "UNIT[\"Degree\",0.0174532925199433]" +
                 "]";

        #endregion

        #region construciont and access

        private IProjectionExtension firstTool = null;
        private static CoordinateSystemService instance;
        static public CoordinateSystemService Instance
        {
            get
            {
                if (instance == null) instance = new CoordinateSystemService();
                return instance;
            }
        }
        protected CoordinateSystemService()
        {

        }
        #endregion
        public bool Activated
        {
            get
            {
                return ExtensionService.Instance.ProjectionTools.Count != 0;
            }
        }
        public string StoryCoordinateSystem { get; private set; }
        public string DisplayCoordinateSystem { get; private set; }

        public void SetStoryCoordinateAndDisplaySystem(string storySystem, string displaySystem)
        {
            StoryCoordinateSystem = storySystem;
            DisplayCoordinateSystem = displaySystem;
            var tool = GetFirstTool();
            if (tool == null) return;
            tool.UpdateCoordinateSystems(storySystem,displaySystem);
        }

        public IProjectionExtension GetFirstTool()
        {
            if (firstTool != null) return firstTool;
            if (ExtensionService.Instance.ProjectionTools.Any())
            {
                firstTool = ExtensionService.Instance.ProjectionTools.First().Value;
                return firstTool;
            }
            throw new NullReferenceException("CoordinateSystemService: No coordinate system tools have been loaded");
        }

        public Point PointStoryToDisplay(Point p)
        {
            try
            {

                return GetFirstTool().PointFromSourceToTarget(p.X, p.Y);
            }catch(Exception ex)
            {
                return p;
            }
        }

        public Point PointDisplayToStory(Point p)
        {
            try
            {

                return GetFirstTool().PointFromTargetToSource(p.X, p.Y);
            }
            catch (Exception ex)
            {
                return p;
            }
        }

        public IDepictionGeometry GeometryStoryToDisplay(IDepictionGeometry geometry)
        {
            try
            {
                return GetFirstTool().GeometryCloneFromSourceToTarget(geometry);
            }
            catch (Exception ex)
            {
                return null;
            }
           
        }
    }
}