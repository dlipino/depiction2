﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Tools;
using Depiction2.Base.Geo;
using Depiction2.Base.Helpers;
using Depiction2.Base.Service.Tiling;

namespace Depiction2.API.Service
{
    //Seems like a wrapper for tileFetcher
    public class TilingService : ModelBase
    {

        #region EventHandlers
        List<Action<TileModel>> DataChangeList = new List<Action<TileModel>>();
        //Cool way of attaching events, not sure how practical in the long run
        //this keeps the original event attached to the fetcher alive even
        //when the fetcher gets destroyed and recreated. The even is attached 
        //from the viewmodel which doens't have direct access to the fetcher
        //and doesnt know when the fetcher gets destroyed
        public event Action<TileModel> DataChanged
        {
            add
            {
                if(!DataChangeList.Contains(value))
                {
                    DataChangeList.Add(value);
                }
                if (tileFetcher == null) return;
                tileFetcher.DataChanged += value;
            }
            remove
            {
                if (tileFetcher == null) return;
                tileFetcher.DataChanged -= value;
//                DataChangeList.Remove(value);
            }
        }

        #endregion
        #region variables
        private BackgroundWorker eventFilterWorker = null;
        private HashSet<TileImageTypes> availableTilerTypes = new HashSet<TileImageTypes>();

        private ITilerExtensionMetadata currentTilerInfo = null;
        private TileFetcher tileFetcher;
        #endregion

        #region properties
        public IEnumerable<TileImageTypes> AvailableTilerTypes { get { return availableTilerTypes; } }
        public int PixelWidth
        {
            get { return tileFetcher != null ? tileFetcher.TilerPixelWidth : -1; }
        }

        public IEnumerable<ITilerExtensionMetadata> AllTilerInfo { get; private set; }
        public ITilerExtensionMetadata CurrentTilerInfo
        {
            get { return currentTilerInfo; }
            set { currentTilerInfo = value; OnPropertyChanged("CurrentTilerInfo"); }
        }
        #endregion

        #region constructor
        public TilingService()
        {
            AllTilerInfo = ExtensionService.Instance.Tilers.Select(t => t.Metadata).ToList();
            availableTilerTypes = new HashSet<TileImageTypes>(AllTilerInfo.Select(t => t.TilerType));
        }
        protected override void OnDispose()
        {
            if (tileFetcher != null)
            {
                tileFetcher.AbortFetch();
                tileFetcher = null;

            }
            base.OnDispose();
        }
        #endregion

        public int GetZoomLevel(ICartRect extent, int tilesAcress)
        {
            return tileFetcher == null ? -1 : tileFetcher.CalculateZoomLevel(extent, tilesAcress);
        }

        #region helpers

        public void SetTiler(string newTilerName)
        {
            var tilerExtension = ExtensionService.Instance.Tilers.FirstOrDefault(t => t.Metadata.Name.Equals(newTilerName));
            if (tilerExtension == null) return;
            CurrentTilerInfo = tilerExtension.Metadata;
            if (tileFetcher != null)
            {
                tileFetcher.AbortFetch();
                var l = DataChangeList.ToList();
                foreach (var dc in l)
                {
                    DataChanged -= dc;
                }
                tileFetcher = null;
            }

            if (!(tilerExtension.Metadata.TilerType.Equals(TileImageTypes.None)))
            {
                var ext = tilerExtension.Value;
                tileFetcher = new TileFetcher(ext);
                foreach (var dc in DataChangeList)
                {
                    DataChanged += dc;
                }
            }

            OnPropertyChanged("Tiler");
        }

        #endregion
        #region background work stuff

        public void DoSingleTileEvent(ICartRect bounds, int tilesAcross)
        {
            if (eventFilterWorker == null)
            {
                eventFilterWorker = new BackgroundWorker();
                eventFilterWorker.DoWork += eventFilterWorker_DoWork;
                var dict = new Dictionary<string, object>();
                dict.Add("Rect", bounds);
                dict.Add("TilesAcross", tilesAcross);
                eventFilterWorker.RunWorkerAsync(dict);
            }
        }
        void eventFilterWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime now = DateTime.Now;
            while (DateTime.Now < now + new TimeSpan(0, 0, 0, 0, 500))
            {
                Thread.Sleep(100);
            }
            Debug.WriteLine("tiling");
            var args = e.Argument as Dictionary<string, object>;
            if (args != null && tileFetcher != null)
            {
                var ext = (ICartRect)args["Rect"];
                var across = (int)args["TilesAcross"];
                tileFetcher.ViewChanged(ext, across);
            }
            eventFilterWorker = null;
        }
        #endregion



    }
}