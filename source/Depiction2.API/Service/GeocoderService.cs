﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.API.Extension.Geocoder;
using Depiction2.Base.Geo;
using Depiction2.Base.Service;
using Depiction2.Base.Utilities;
using Newtonsoft.Json;//Maybe move this to an extension

namespace Depiction2.API.Service
{
    //http://json.codeplex.com/license json.net license
    public class GeocoderService : IDepictionGeocodeService
    {
        private string storedGeocode = "GeocodeHistory.json";
        private IDepictionGeocoder currentGeocoder;
//        OrderedDictionary geocodeCache = new OrderedDictionary();
        private List<KeyValuePair<string, Point>> geocodeCache = new List<KeyValuePair<string, Point>>();

        public List<string> RecentGeocodes { get { return geocodeCache.Select(t => t.Key).ToList(); } } 
        //for now this uses the depictionextensionlibrary, maybe in the future it will be in charge of its own geocoders
        public string CurrentGeocoderDisplayName { get; private set; }

        public IEnumerable<string> AvailableGeocoderNames
        {
            get
            {
                return ExtensionService.Instance.Geocoders.Select(t => t.Metadata.DisplayName);
            }
        }

        public bool SetCurrentGeocoderFromExtensions(string name)
        {
            var info = ExtensionService.Instance.Geocoders.First(t => name.Equals(t.Metadata.Name));
            if (info == null) return false;
            CurrentGeocoderDisplayName = info.Metadata.DisplayName;
            currentGeocoder = info.Value;
            return true;
        }

        public void SetGeocoder(IDepictionGeocoder newGeocoder)
        {
            currentGeocoder = newGeocoder;
            CurrentGeocoderDisplayName = "User Added";
        }

        public Point? GeocodeInput(GeocodeInput input, bool recordResults)
        {
            if (string.IsNullOrEmpty(input.ToString())) return null;
            if (geocodeCache.Any(t=>t.Key.Equals(input.CompleteAddress)))
            {
                var val = geocodeCache.First(t => t.Key.Equals(input.CompleteAddress));

                geocodeCache.Remove(val);
                geocodeCache.Insert(0,val);
                return val.Value;
            }
            //see if it is just a normal lat/long
            double lat, lon;
            string message;
            if(LatLongParserUtility.ParseForLatLong(input.CompleteAddress,out lat,out lon,out message) == true)
            {
                return new Point(lon,lat);
            }
            
            if (currentGeocoder == null) return null;
            var result = currentGeocoder.GeocodeInput(input);

            if (recordResults && result != null)
            {
                geocodeCache.Insert(0,new KeyValuePair<string, Point>(input.ToString(), (Point)result));
            }
            return result;
        }

        #region constructor

        public GeocoderService()
        {
            if(ExtensionService.Instance.Geocoders.Any())
            {
                var first = ExtensionService.Instance.Geocoders.First();
                currentGeocoder = first.Value;
                CurrentGeocoderDisplayName = first.Metadata.DisplayName;
            }
           
            ReadCache();
        }

        ~GeocoderService()
        {
            WriteCache();
        }


        #endregion

        #region cache save/load
        private void ReadCache()
        {
            if (DepictionAccess.PathService == null) return;
            var appPath = DepictionAccess.PathService.AppDataDirectoryPath;
            var cacheFile = Path.Combine(appPath, storedGeocode);
            if(File.Exists(cacheFile))
            {
                var cacheString = File.ReadAllText(cacheFile);
                try
                {
                    geocodeCache = JsonConvert.DeserializeObject<List<KeyValuePair<string, Point>>>(cacheString);
                }catch
                {
                    var oldCache  = JsonConvert.DeserializeObject<Dictionary<string, Point>>(cacheString);
                    geocodeCache.Clear();
                    foreach(var kvp in oldCache)
                    {
                        geocodeCache.Add(new KeyValuePair<string, Point>(kvp.Key,kvp.Value));
                    }
                }
            }
        }
        private void WriteCache()
        {
            if (DepictionAccess.PathService == null) return;
            var appPath = DepictionAccess.PathService.AppDataDirectoryPath;
            var cacheFile = Path.Combine(appPath, storedGeocode);

            string json = JsonConvert.SerializeObject(geocodeCache);

            //write string to file
            File.WriteAllText(cacheFile, json);

        }
        #endregion
    }
}