﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction2.API.Extension.Exporter.Story;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.Base.Interactions;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementTemplates;

namespace Depiction2.API.Service
{
    //Convert to instance
    public class StorySerializationService
    {
        private DepictionFolderService tempSerializationFolder = null;
        private string _rootSerializationFolder = string.Empty;
        private string _rootDataFolder = string.Empty;
        public string RootSerializationFolder { get { return _rootSerializationFolder; } }
        public string DataSerializationFolder { get { return _rootDataFolder; } }

        #region constructor and instance things

        private static StorySerializationService _instance;
        static public StorySerializationService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StorySerializationService();
                }
                return _instance;
            }
        }
        protected StorySerializationService()
        {
            ResetSerializationFolder();
        }
        #endregion
        #region helpers

        public void CleanSerializationFolders()
        {
            if (tempSerializationFolder != null)
            {
                tempSerializationFolder.Close();
            }

            _rootSerializationFolder = string.Empty;
            _rootDataFolder = string.Empty;
        }
        public void ResetSerializationFolder()
        {
            if (tempSerializationFolder != null)
            {
                tempSerializationFolder.Close();
            }
            tempSerializationFolder = new DepictionFolderService(false);
            _rootSerializationFolder = tempSerializationFolder.FolderName;
            _rootDataFolder = Path.Combine(_rootSerializationFolder, "Data");
            if (!Directory.Exists(_rootDataFolder))
            {
                Directory.CreateDirectory(_rootDataFolder);
            }
        }
        #endregion

        #region loader services

        static public Dictionary<string, IDepictionStoryLoaderMetadata> GetStoryLoaders()
        {
            var dict = new Dictionary<string, IDepictionStoryLoaderMetadata>();
            foreach (var fr in ExtensionService.Instance.DepictionStoryFileReaders)
            {
                var key = fr.Metadata.Name;
                if (dict.ContainsKey(key))
                {
                    //let somebody know that multiple exist.
                }
                else
                {
                    //name,extension (with .)
                    dict.Add(fr.Metadata.Name, fr.Metadata);
                }
            }
            return dict;
        }

        public IStory LoadDepictionWithLoader(string loaderName, string depictionName)
        {
            if (!File.Exists(depictionName)) return null;
            if (string.IsNullOrEmpty(loaderName)) return null;
            var selected = ExtensionService.Instance.DepictionStoryFileReaders.First(t => t.Metadata.Name.Equals(loaderName));
            if (selected == null)
            {
                var problemMessage =
                 string.Format("Could not load selected depiction story file importer.");
                DepictionAccess.MessageService.AddStoryMessage(problemMessage);
                return null;
            }
            ResetSerializationFolder();
            var loadedStory = selected.Value.GetStoryFromFile(depictionName);
            if (loadedStory == null)
            {
                var problemMessage =
                 string.Format("Could not load {0} into Depiction", Path.GetFileName(depictionName));
                DepictionAccess.MessageService.AddStoryMessage(problemMessage);
                return null;
            }
            return loadedStory;
        }

        #endregion

        #region saver
        static public Dictionary<string, IDepictionStorySaverMetadata> GetStorySavers()
        {
            var dict = new Dictionary<string, IDepictionStorySaverMetadata>();
            foreach (var fr in ExtensionService.Instance.DepictionStoryFileSavers)
            {
                var key = fr.Metadata.Name;
                if (dict.ContainsKey(key))
                {
                    //let somebody know that multiple exist.
                }
                else
                {
                    //name,extension (with .)
                    dict.Add(fr.Metadata.Name, fr.Metadata);
                }
            }
            return dict;
        }

        public bool SaveDepictionWithSaver(string saverName, string depictionName, IStory storyToSave)
        {
            if (string.IsNullOrEmpty(saverName)) return false;
            var selected = ExtensionService.Instance.DepictionStoryFileSavers.First(t => t.Metadata.Name.Equals(saverName));
            if (selected == null) return false;

            ResetSerializationFolder();
            if (!Directory.Exists(DataSerializationFolder))
            {
                Directory.CreateDirectory(DataSerializationFolder);
            }
            var saveSuccess = selected.Value.SaveStoryToTempFolderFileAndZip(storyToSave, _rootSerializationFolder, depictionName);
            CleanSerializationFolders();
            var saveMessage = string.Empty;
            if (saveSuccess)
            {
                saveMessage = string.Format("{0} has been successfully saved.", depictionName);
            }
            else
            {
                saveMessage = string.Format("{0} could not be saved saved.", depictionName);
            }
            DepictionMessageService.Instance.AddStoryMessage(saveMessage, 3);
            return saveSuccess;

        }

        #endregion

        #region element template getting
        static public List<IElementTemplate> GetElementTemplatesFromResourceFile(string resourceFileName)
        {
            var scaffolds = new List<IElementTemplate>();
            var readers = ExtensionService.Instance.DepictionStoryFileReaders;
            foreach (var reader in readers)
            {
                var readTemplates = reader.Value.GetEmbeddedElementTemplatesFromResource(resourceFileName);
                if (readTemplates != null && readTemplates.Any())
                {
                    scaffolds.AddRange(readTemplates);
                }
            }
            return scaffolds;
        }
        static public List<IElementTemplate> GetElementTemplatesFromDirectory(string dirName)
        {
            var scaffolds = new List<IElementTemplate>();
            if (!Directory.Exists(dirName)) return scaffolds;
            var readers = ExtensionService.Instance.DepictionStoryFileReaders;
            foreach (var reader in readers)
            {
                var readTemplates = reader.Value.GetElementTemplatesFromDir(dirName);
                if (readTemplates != null && readTemplates.Any())
                {
                    scaffolds.AddRange(readTemplates);
                }
            }
            return scaffolds;
        }

        static public void SaveScaffoldToFile(string saverExtension, string fileName, IElementTemplate templateToSave)
        {
            if (string.IsNullOrEmpty(saverExtension)) return;
            var selected = ExtensionService.Instance.DepictionStoryFileSavers.First(t => t.Metadata.Name.Equals(saverExtension));
            if (selected != null)
            {
                selected.Value.SaveScaffoldToFile(templateToSave, fileName);
            }

        }

        #endregion

        #region interaction gettings

        static public List<IInteractionRule> GetInteractionsFromDirectory(string dirName)
        {
            var interactions = new List<IInteractionRule>();
            if (!Directory.Exists(dirName)) return interactions;
            var readers = ExtensionService.Instance.DepictionStoryFileReaders;
            foreach (var reader in readers)
            {
                interactions.AddRange(reader.Value.GetInteractionRulesFromDirectory(dirName));
            }
            return interactions;
        }

        #endregion

    }
}