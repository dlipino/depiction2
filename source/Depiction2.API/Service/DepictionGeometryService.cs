﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Depiction2.API.Extension.Tools.Geometry;
using Depiction2.Base.Geo;

namespace Depiction2.API.Service
{
    public class DepictionGeometryService
    {
        public static bool Activated { get { return ExtensionService.Instance.GeometryTools.Count != 0; } }

        static IGeometryExtension GetFirstTool()
        {
            if (ExtensionService.Instance.GeometryTools.Any())
            {
                return ExtensionService.Instance.GeometryTools.First().Value;
            }
            return null;
//            throw new NullReferenceException("GeometryService: No geometry tools have been loaded");
        }
        static private IDepictionGeometry CreateGeomFromTool(object geometrySource)
        {
            if (geometrySource is IDepictionGeometry) return geometrySource as IDepictionGeometry;
            //general used for gdal geometry objects
            var tool = GetFirstTool();
            if(tool == null) return null;
            return GetFirstTool().CreateGeometry(geometrySource);
        }

        static public IDepictionGeometry CreateGeomtryFromUnknownType(object unknownSource)
        {
            return CreateGeomFromTool(unknownSource);
        }

        static public IDepictionGeometry CreateGeometry()
        {
            return CreateGeomFromTool(null);
        }

        static public IDepictionGeometry CreateGeometry(ICartRect rect)
        {
            var t = rect.Top;
            var r = rect.Right;
            var l = rect.Left;
            var b = rect.Bottom;

            return CreateGeometry(new List<Point>
                               {new Point(l, t), new Point(r, t), new Point(r, b), new Point(l, b), new Point(l, t)});
        }

        static public IDepictionGeometry CreateGeometry(IEnumerable<Point> points)
        {
            return CreateGeomFromTool(points);
        }
        static public IDepictionGeometry CreateGeomtryFromWkt(string wtkGeometry)
        {
            return CreateGeomFromTool(wtkGeometry);
        }

        static public IDepictionGeometry CreatePointGeometry(Point p)
        {
            return CreateGeomFromTool(p);
        }

        static public StreamGeometry CreateDrawStreamGeometryFromGeometryWrapList(IDepictionGeometry geometry)
        {
            var tool = GetFirstTool();
            if (tool == null) return null;
            return GetFirstTool().CreateStreamGeometryFromDepictionGeometry(geometry);
        }
    }
}