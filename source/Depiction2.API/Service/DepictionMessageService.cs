﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Depiction2.Story14IO.UnitTests")]
namespace Depiction2.API.Service
{
    public class DepictionMessageService
    {
        public event Action<string, int> DialogMessageAdded;
        public event Action<string, int> DisplayMessageAdded;
        private List<string> _applicationMessages;
        private List<string> _storyMessages;

        static private DepictionMessageService _instance = null;
        static public DepictionMessageService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DepictionMessageService();
                }
                return _instance;
            }
        }
        protected DepictionMessageService()
        {
            _applicationMessages = new List<string>();
            _storyMessages = new List<string>();
        }
        #region for testing
        internal int StoryCount { get { return _storyMessages.Count; } }
        internal int AppCount { get { return _applicationMessages.Count; } }
        internal void ClearAllMessages()
        {
            _storyMessages.Clear();
            _applicationMessages.Clear();
            
        }
        #endregion

        public void ClearStoryMessages()
        {
            _storyMessages.Clear();
        }
        public void AddApplicationMessage(string message)
        {
            AddApplicationMessage(message, 0);
        }
        public void AddApplicationMessage(string message, int fadeTime)
        {
            _storyMessages.Add(message);
            if (DisplayMessageAdded != null) DisplayMessageAdded(message, fadeTime);
        }
        public void AddStoryMessage(string message)
        {
            AddStoryMessage(message, 0);
        }
        public void AddStoryMessage(string message, int fadeTime)
        {
            _storyMessages.Add(message);
            if (DisplayMessageAdded != null) DisplayMessageAdded(message, fadeTime);
        }
    }
}