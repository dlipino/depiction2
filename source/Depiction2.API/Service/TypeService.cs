﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Media;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.StoryEntities.ElementParts;

namespace Depiction2.API.Service
{
    public class TypeService : ISimpleNameTypeService
    {
        private Dictionary<string, Type> defaultDepictionNameAndTypeDictionary = new Dictionary<string, Type>();
        private List<Type> depictionCoreTypes;

        #region Instance things
        //this kind of makes generalized tests hellish
        //what is the difference between instance and making this a static class
        private static TypeService instance;
        public static TypeService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TypeService();
                }
                return instance;
            }
        }

        #endregion

        protected TypeService()
        {
            LoadCompleteDictionary();
        }

        private void SetCoreTypes()
        {
            depictionCoreTypes = new List<Type>();
            depictionCoreTypes.Add(typeof(int));
            depictionCoreTypes.Add(typeof(double));
            depictionCoreTypes.Add(typeof(string));
            depictionCoreTypes.Add(typeof(bool));
            depictionCoreTypes.Add(typeof(Color));
            depictionCoreTypes.Add(typeof(SolidColorBrush));
            depictionCoreTypes.Add(typeof(DateTime));
            depictionCoreTypes.Add(typeof(ElementVisualSetting));
            depictionCoreTypes.Add(typeof(DisplayerViewType));

            defaultDepictionNameAndTypeDictionary.Clear();
            foreach (var type in depictionCoreTypes)
            {
                defaultDepictionNameAndTypeDictionary.Add(type.Name, type);
            }
        }

     
        public Dictionary<string, Type> NameTypeDictonary
        {
            get { return defaultDepictionNameAndTypeDictionary; }
        }

        public Type GetTypeFromName(string name)
        {
            var simpleName = name;
            //first try to get it from the complete name
            if (!defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
            {
                //simplify the name
                var index = name.LastIndexOf(',');
                simpleName = name;
                if (index > 0)
                {
                    var sub = name.Substring(0, index).Trim();
                    simpleName = sub.Substring(sub.LastIndexOf('.') + 1);
                }
            }
            //Now try again
            if (defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
            {
                return defaultDepictionNameAndTypeDictionary[simpleName];
            }
            return Type.GetType(name);
        }

        public void UpdateSimpleNameTypeDictionary(Dictionary<string, Type> updateDictionary)
        {
            foreach (var keyValue in updateDictionary)
            {
                if (!defaultDepictionNameAndTypeDictionary.ContainsKey(keyValue.Key))
                {
                    defaultDepictionNameAndTypeDictionary.Add(keyValue.Key, keyValue.Value);
                }
            }
        }
        /// <summary>
        /// Loads all the types, including those it gets from a extensions
        /// </summary>
        public void LoadCompleteDictionary()
        {
            SetCoreTypes();
            foreach (var typeList in ExtensionService.Instance.DepictionResourceExtensions.Select(t => t.Value))
            {
                UpdateSimpleNameTypeDictionary(typeList.ExtensionTypes);
            }
            
        }
        public void ClearTypeService()
        {
            defaultDepictionNameAndTypeDictionary.Clear();
        }

        public bool AddTypeToDictionary(string simpleName, Type type)
        {
            if (defaultDepictionNameAndTypeDictionary.ContainsKey(simpleName))
            {
                return false;
            }
            defaultDepictionNameAndTypeDictionary.Add(simpleName, type);
            return true;
        }
    }
}