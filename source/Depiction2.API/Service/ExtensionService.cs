﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;
using Depiction2.API.Extension.Exporter.Story;
using Depiction2.API.Extension.Geocoder;
using Depiction2.API.Extension.Importer.Elements;
using Depiction2.API.Extension.Importer.Story;
using Depiction2.API.Extension.Interaction.Behavior;
using Depiction2.API.Extension.Interaction.Condition;
using Depiction2.API.Extension.Logging;
using Depiction2.API.Extension.Resources;
using Depiction2.API.Extension.Tools.Geometry;
using Depiction2.API.Extension.Tools.Positioning;
using Depiction2.API.Extension.Tools.Projection;
using Depiction2.API.Extension.Tools.Tiling;
using Depiction2.API.Extension.Tools.Types;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Interactions.Interactions;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace Depiction2.API.Service
{
    public class ExtensionService
    {
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IDepictionStoryLoader, IDepictionStoryLoaderMetadata>> DepictionStoryFileReaders { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IDepictionStorySaver, IDepictionStorySaverMetadata>> DepictionStoryFileSavers { get; private set; }

        //I think it would be nice if these were not shared.
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IFileLoaderExtension, IDepictionFileLoaderMetadata>> FileLoaders { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<ILogExtension, ILogExtensionMetadata>> LogTools { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<ISourceLoaderExtension, ISourceLoaderMetadata>> SourceLoaders { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IRegionSourceExtension, IRegionSourceMetadata>> RegionDataLoaders { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IGeometryExtension, IGeometryExtensionMetadata>> GeometryTools { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IPositionMeasureExtension, IPositionMeasureExtensionMetadata>> PositionMeasureTools { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IProjectionExtension, IProjectionExtensionMetadata>> ProjectionTools { get; private set; }

        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IGeocoderExtension, IGeocoderExtensionMetadata>> Geocoders { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<ITilerExtension, ITilerExtensionMetadata>> Tilers { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IBehavior, IBehaviorMetadata>> DepictionBehaviors { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<ICondition, IConditionMetadata>> DepictionConditions { get; private set; }
        [ImportMany(AllowRecomposition = true)]
        public IList<Lazy<IDepictionResourceExtension, IResourceExtensionMetadata>> DepictionResourceExtensions { get; private set; }

        private CompositionContainer _extensionContainer;
        private AggregateCatalog _aggCatalog;
        private HashSet<string> _extensionResourceDirs;
        private HashSet<string> _extensionHomeDirs;

        #region Instance things
        //this kind of makes generalized tests hellish
        //what is the difference between instance and making this a static class
        private static ExtensionService instance;
        public static ExtensionService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExtensionService();
                }
                return instance;
            }
        }

        #endregion
        #region Constructor/destructor

        internal ExtensionService()
        {
            _extensionResourceDirs = new HashSet<string>();
            _extensionHomeDirs = new HashSet<string>();

            DepictionStoryFileReaders = new List<Lazy<IDepictionStoryLoader, IDepictionStoryLoaderMetadata>>();
            DepictionStoryFileSavers = new List<Lazy<IDepictionStorySaver, IDepictionStorySaverMetadata>>();
            LogTools = new List<Lazy<ILogExtension, ILogExtensionMetadata>>();
            FileLoaders = new List<Lazy<IFileLoaderExtension, IDepictionFileLoaderMetadata>>();
            SourceLoaders = new List<Lazy<ISourceLoaderExtension, ISourceLoaderMetadata>>();
            RegionDataLoaders = new List<Lazy<IRegionSourceExtension, IRegionSourceMetadata>>();
            GeometryTools = new List<Lazy<IGeometryExtension, IGeometryExtensionMetadata>>();
            PositionMeasureTools = new List<Lazy<IPositionMeasureExtension, IPositionMeasureExtensionMetadata>>();
            ProjectionTools = new List<Lazy<IProjectionExtension, IProjectionExtensionMetadata>>();
            Geocoders = new List<Lazy<IGeocoderExtension, IGeocoderExtensionMetadata>>();
            Tilers = new List<Lazy<ITilerExtension, ITilerExtensionMetadata>>();
            DepictionBehaviors = new List<Lazy<IBehavior, IBehaviorMetadata>>();
            DepictionConditions = new List<Lazy<ICondition, IConditionMetadata>>();
            DepictionResourceExtensions = new List<Lazy<IDepictionResourceExtension, IResourceExtensionMetadata>>();

            _aggCatalog = new AggregateCatalog();
            //Load the built in extensions
            var defaults = AppDomain.CurrentDomain.BaseDirectory;
            var coreFile = Path.Combine(defaults, "Depiction2.Core.dll");
            if (File.Exists(coreFile))
            {
                var assembly = Assembly.LoadFile(coreFile);
                _aggCatalog.Catalogs.Add(new AssemblyCatalog(assembly));
            }

            var extensionHome = Path.Combine(defaults, "Extensions");
            if (Directory.Exists(extensionHome))
            {
                var allSubDirs = Directory.GetDirectories(extensionHome, "*");
                foreach (var folder in allSubDirs)
                {
                    _aggCatalog.Catalogs.Add(new DirectoryCatalog(folder));
                    var elementDir = Path.Combine(folder, "Resources");
                    _extensionHomeDirs.Add(folder);
                    if (Directory.Exists(elementDir))
                    {
                        _extensionResourceDirs.Add(elementDir);
                    }
                }
            }
            else
            {
                var ex = new DirectoryNotFoundException("Could not find base extension directory.");
                //                DepictionLogger.LogException(ex);
            }
            _extensionContainer = new CompositionContainer(_aggCatalog);
            _extensionContainer.ComposeParts(this);
        }

        ~ExtensionService()
        {
            Decompose();
        }
        #endregion

        public void Decompose()
        {
            if (_extensionContainer != null)
            {
                _extensionContainer.Dispose();
            }
            _extensionContainer = null;

            instance = null; //causes the test cycle to crash: SetProperty cant be found (while it did before 4/19/2014
        }

        internal bool LoadSpecificExtensionsFromBaseDirectory(string libraryName)
        {
            var defaults = AppDomain.CurrentDomain.BaseDirectory;
            var coreFile = Path.Combine(defaults, libraryName);
            if (File.Exists(coreFile))
            {
                var assembly = Assembly.LoadFile(coreFile);
                _aggCatalog.Catalogs.Add(new AssemblyCatalog(assembly));
                return true;
            }
            return false;
        }
        public bool LoadExtensionFromDir(string dir)
        {
            if (!Directory.Exists(dir)) return false;
            var catalog = new DirectoryCatalog(dir);

            if (_extensionHomeDirs.Add(dir))
            {
                _aggCatalog.Catalogs.Add(catalog);
            }
            return true;
        }

        public void LoadCompleteExtension()
        {

        }
        public void LoadExtensionResourcesDictionariesIntoDepiction()
        {
            foreach (var gt in DepictionResourceExtensions)
            {
                TypeService.Instance.UpdateSimpleNameTypeDictionary(gt.Value.ExtensionTypes);
                if (Application.Current != null)
                {
                    if (gt.Value.ExtensionResources != null)
                    {
                        var rd = gt.Value.ExtensionResources;
                        //Uri does not like it
                        //                    var name = gt.Metadata.ResourceName;
                        //                    if(!string.IsNullOrEmpty(name))
                        //                    {
                        //                        rd.Source = new Uri(name, UriKind.Relative);
                        //                    }

                        Application.Current.Resources.MergedDictionaries.Add(rd);
                    }
                }
            }
            //ORder is important since the element dictionary doesnt deal well with duplicates (ie takes the initial element and ignores new ones with same).

            //from directories used by the addins
            foreach (var resourceDir in _extensionResourceDirs)
            {
                var elemDir = Path.Combine(resourceDir, "Elements");
                if (Directory.Exists(elemDir) && DepictionAccess.TemplateLibrary != null)
                {
                    var extElements = StorySerializationService.GetElementTemplatesFromDirectory(elemDir);
                    DepictionAccess.TemplateLibrary.AddElementTemplatesToDefault(extElements);
                }
                var interDir = Path.Combine(resourceDir, "Interactions");
                if (Directory.Exists(interDir) && DepictionAccess.InteractionLibrary != null)
                {
                    var inters = StorySerializationService.GetInteractionsFromDirectory(interDir);
                    DepictionAccess.InteractionLibrary.LoadDefaultRulesIntoRepository(inters);
                }
            }
        }
        public void LoadResourcesIntoStory()
        {

            LoadExtensionResourcesDictionariesIntoDepiction();
            //From the main resource file
            var productResourceAssemblyName = DepictionAccess.ProductInformation.ResourceAssemblyName;
            var mainElementTemplates =
                StorySerializationService.GetElementTemplatesFromResourceFile(productResourceAssemblyName);
            var themeAssemblyName = string.Format("pack://application:,,,/{0};component/{1}",
               productResourceAssemblyName, "Resources/ElementIconResource.xaml");

            var defaultResourceDictionary = new ResourceDictionary
            {
                Source = new Uri(themeAssemblyName)
            };
            var appResources = Application.Current.Resources.MergedDictionaries;
            appResources.Add(defaultResourceDictionary);
            if (DepictionAccess.TemplateLibrary != null)
            {
                DepictionAccess.TemplateLibrary.AddElementTemplatesToDefault(mainElementTemplates);
            }
        }
    }
}