﻿using System;
using System.Linq;
using Depiction2.API.Extension.Logging;

namespace Depiction2.API.Service
{
    public class NotificationService
    {
        static private NotificationService _instance;
        private ILogExtension _loggerInUse = null;

        public int ErrorLogCount { get; set; }
        public bool Activated
        {
            get
            {
                return ExtensionService.Instance.LogTools.Count != 0;
            }
        }

        #region construction and static instance

        protected NotificationService()
        {
            ErrorLogCount = 0;
            SetLogger(string.Empty);
        }

        static public NotificationService Instance
        {
            get
            {
                if (_instance == null) _instance = new NotificationService();
                return _instance;
            }
        }
        //mayby have a public constructor for tests and third party stuff
        #endregion
        public bool SetLogger(string loggerName)
        {
            if (!Activated) return false;//putting a break point here does bad things
            if (string.IsNullOrEmpty(loggerName))
            {
                var first = ExtensionService.Instance.LogTools.FirstOrDefault();
                if (first != null)
                {
                    _loggerInUse = first.Value;
                    return true;
                }
                return false;
            }

            var matching = ExtensionService.Instance.LogTools.Where(t => t.Metadata.Name.Equals(loggerName)).ToList();
            if (!matching.Any()) return false;
            _loggerInUse = matching.First().Value;
            return false;
        }
        public void LogMessage(string message)
        {
            if (_loggerInUse == null) return;
            _loggerInUse.LogEvent(message);
        }
        public void LogException(string additionalInfo, Exception ex)
        {
            ErrorLogCount += 1;
            if (_loggerInUse == null) return;
            if (string.IsNullOrEmpty(additionalInfo))
            {
                _loggerInUse.LogException(ex);
            }
            else
            {
                _loggerInUse.LogException(additionalInfo, ex);
            }
        }
        public void LogException(Exception ex)
        {
            LogException(null, ex);
        }
    }
}