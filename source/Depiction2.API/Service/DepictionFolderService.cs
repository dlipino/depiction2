﻿using System;
using System.IO;

namespace Depiction2.API.Service
{
    public class DepictionFolderService : IDisposable
    {
        //Not really sure why this class is in here, but it needed to start somewhere
        public DepictionFolderService(bool isTest)
        {
            if (isTest) FolderName = CreateTempTestDirectory();
            else
            {
                FolderName = CreateTempDepictionDirectory();
            }
        }

        ~DepictionFolderService()
        {
            Close();
            //if (folderName != "") throw new Exception("Ya gotta close me");
        }
        /// <summary>
        ///Calls the close command
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        public string FolderName { get; set; }
        public string GetATempFilenameInDir()
        {
            return GetATempFilenameInDir(null);
        }
        public string GetATempFilenameInDir(string dotLessExtension)
        {
            if (!string.IsNullOrEmpty(dotLessExtension))
                return Path.Combine(FolderName, "TFL_" + Guid.NewGuid() + "." + dotLessExtension);
            return Path.Combine(FolderName, "TFL_" + Guid.NewGuid());
        }

        private string CreateTempTestDirectory()
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "TESTL_" + Guid.NewGuid());
            Directory.CreateDirectory(tempDir);
            return tempDir;
        }
        private static string CreateTempDepictionDirectory()
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "TDL_" + Guid.NewGuid());
            Directory.CreateDirectory(tempDir);
            return tempDir;
        }

        private static void DeleteFilesInDir(string dirName)
        {
            Array.ForEach(Directory.GetFiles(dirName), File.Delete);
        }

        static private void EmptyFolder(DirectoryInfo directoryInfo)
        {
            foreach (var file in directoryInfo.GetFiles())
            {
                file.Delete();
            }
            foreach (var subfolder in directoryInfo.GetDirectories())
            {
                EmptyFolder(subfolder);
                Directory.Delete(subfolder.FullName);
            }
        }

        private static void DeleteTempDirectory(string dirName)
        {
            if (string.IsNullOrEmpty(dirName)) return;
            try
            {
                if (Directory.Exists(dirName))
                {
                    EmptyFolder(new DirectoryInfo(dirName));
                    Directory.Delete(dirName, true);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// This method should delete everything that was created in the temp directory, in addition to removing
        /// the directory itself.
        /// </summary>
        public void Close()
        {
            DeleteTempDirectory(FolderName);
            FolderName = String.Empty;
        }
    }
}