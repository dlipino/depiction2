﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using Depiction2.Base.Service;

[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
[assembly: InternalsVisibleTo("Depiction2.Story2IO.UnitTests")]
namespace Depiction2.API.Service
{
    /// <summary>
    /// Service that gets the directory paths requested by the user
    /// </summary>
    public class DepictionPathService : IDepictionPathService//SHould be renamed to depictionApplicationPathService
    {
        private string folderInTempFolderRootPath;
        private string topLevelAppDirName = "Depiction2_Temp";

        #region COnstructor
        public DepictionPathService(string baseDir)
        {
            if (!string.IsNullOrEmpty(baseDir))
            {
                topLevelAppDirName = baseDir;
            }
        }

        #endregion

        #region Default directory properties

        #region default depiction directories

        //This should stay the same for each product
        public string AppDataDirectoryPath
        {
            get
            {
                return EnsurePath(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), topLevelAppDirName));
            }
        }
        public string TempFolderRootPath
        {
            get
            {
                return EnsurePath(Path.Combine(Path.GetTempPath(), topLevelAppDirName));
            }
        }
        public string DepictionCacheHomeDirectory
        {
            get
            {
                return EnsurePath(Path.Combine(AppDataDirectoryPath, "Cache"));
            }
        }

        public string CurrentStoryDataDirectory
        {
            get
            {
                return EnsurePath(Path.Combine(AppDataDirectoryPath, "FileData"));
            }
        }
        public string RetrieveCachedFilePathIfCached(string fileName)
        {
            var tilePath = Path.Combine(DepictionCacheHomeDirectory, fileName);
            return File.Exists(tilePath) ? tilePath : null;
        }
        #endregion

        #endregion

        #region private helpers
        /// <summary>
        ///  If this directory does not exist, create it.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal string EnsurePath(string path)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {

                }
            }
            return path;
        }
        internal void DoCleanup(string folderToClean)
        {
            try
            {
                if (Directory.Exists(folderToClean))
                {
                    Directory.Delete(folderToClean, true);
                }
            }
            catch (Exception ex)
            {
                //                DepictionExceptionHandler.HandleException("Unable to read temporary folder", ex, false);
            }
        }
        #endregion

        #region public helpers

        public void ClearLoadFileDirectory()
        {
            DoCleanup(CurrentStoryDataDirectory);
        }
        /// <summary>
        /// Cleans up the temporary files created by Depiction.
        /// </summary>
        public void CleanTempFolder()
        {
            DoCleanup(TempFolderRootPath);
        }
        public string CreateTempSubFolder()
        {
            if (String.IsNullOrEmpty(folderInTempFolderRootPath))
            {
                try
                {
                    folderInTempFolderRootPath = Path.Combine(TempFolderRootPath, Guid.NewGuid().ToString());
                }
                catch (Exception ex)
                {
                    //                        DepictionAccess.NotificationService.WriteToLog("Unable to get Temporary folder path", ex);
                    return null;
                }
            }
            // Make sure this directory is there on each request, just in case it was deleted by somebody, somewhere, sometime.
            if (!Directory.Exists(folderInTempFolderRootPath))
            {
                Directory.CreateDirectory(folderInTempFolderRootPath);
            }
            return folderInTempFolderRootPath;
        }
        #endregion

        internal void CleanForTest()
        {
            DoCleanup(TempFolderRootPath);
            DoCleanup(AppDataDirectoryPath);
            ClearLoadFileDirectory();
        }
        //TODO this should not be open for public usage.
        internal void CleanAllPaths()
        {
            CleanForTest();
        }
    }
}