﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Depiction2.API.Tools
{
    public class ResourceReaderService
    {
        public static bool CopyAssemblyEmbeddedResourcesToDirectory(string assemblyName, string fileExtension, bool replace, string directoryPathToCreateIn)
        {
            if (!Directory.Exists(directoryPathToCreateIn))
            {
                return false;
            }

            var existingFiles = new List<string>(Directory.GetFiles(directoryPathToCreateIn, "*." + fileExtension));
            var elementAndIconAssembly = Assembly.Load(new AssemblyName(assemblyName));
            foreach (string manifestResourceName in elementAndIconAssembly.GetManifestResourceNames())
            {
                if (!manifestResourceName.Contains("." + fileExtension)) continue;
                if (!replace && existingFiles.Contains(manifestResourceName)) continue;

                int bytesRead;
                byte[] bits;

                using (var fileResource = elementAndIconAssembly.GetManifestResourceStream(manifestResourceName))
                {
                    if (fileResource == null) continue;
                    bits = new byte[fileResource.Length];
                    bytesRead = fileResource.Read(bits, 0, (int)fileResource.Length);
                }

                using (var stream = new FileStream(Path.Combine(directoryPathToCreateIn, manifestResourceName), FileMode.Create))
                {
                    stream.Write(bits, 0, bytesRead);
                }
            }
            return true;
        }
    }
}