﻿using System.IO;
using System.Windows.Media.Imaging;
using Depiction2.Base.Utilities;

namespace Depiction2.API.Tools
{
    public class TileCacheService
    {
        protected string cacheRoot = @"c:\depictionCache";
        protected string fullCachPath = @"c:\depictionCache";

        public TileCacheService(string cachePathName)
        {
            if (DepictionAccess.PathService != null && Directory.Exists(DepictionAccess.PathService.DepictionCacheHomeDirectory))
            {
                cacheRoot = DepictionAccess.PathService.DepictionCacheHomeDirectory;
            }
            if (!string.IsNullOrEmpty(cachePathName))
            {
                fullCachPath = Path.Combine(cacheRoot, cachePathName);
                if (!Directory.Exists(fullCachPath))
                {
                    Directory.CreateDirectory(fullCachPath);
                }
            }
        }

        public string GetCacheFullStoragePath(string cacheFileName)
        {
            return Path.Combine(fullCachPath, cacheFileName);
        }

        public void SaveDataToCachePath(byte[] image, string imageName, bool overwrite)
        {
            if (image == null)
                return;
            if (!Directory.Exists(fullCachPath))
            {
                Directory.CreateDirectory(fullCachPath);
            }
            var fullName = Path.Combine(fullCachPath, imageName);
            try
            {
                File.WriteAllBytes(fullName, image);
            }
            catch { }
        }

        public bool SaveDataToCachePath(BitmapSource image, string imageName, bool overwrite)
        {
            if (!Directory.Exists(fullCachPath))
            {
                Directory.CreateDirectory(fullCachPath);
            }
            var fullName = Path.Combine(fullCachPath, imageName);
            if (File.Exists(fullName) && !overwrite) return false;
            return DepictionImageResourceDictionary.SaveBitmap(image, fullName);
        }

//        public Rect? RetrieveCachedWarpBounds(string tileKey)
//        {
//            string filePath = GetCacheFullStoragePath(tileKey + ".wrp");
//            if (!File.Exists(filePath)) return null;
//            byte[] fileStream = File.ReadAllBytes(filePath);
//
//            using (var memoryStream = new MemoryStream(fileStream))
//            {
//                var binaryFormatter = new BinaryFormatter();
//                return (Rect)binaryFormatter.Deserialize(memoryStream);
//            }
//        }
//        public void CacheWarpBounds(string tileKey, IDepictionLatitudeLongitude topLeft, IDepictionLatitudeLongitude bottomRight)
//        {
//            throw new NotImplementedException();
//            //if (!Directory.Exists(fullCachPath))
//            //{
//            //    Directory.CreateDirectory(fullCachPath);
//            //}
//
//            //using (FileStream fileStream = File.Create(fullCachPath + "\\" + tileKey + ".wrp"))
//            //{
//            //    var boundingBox = new Rect(topLeft, bottomRight);
//            //    var binaryFormatter = new BinaryFormatter();
//            //    binaryFormatter.Serialize(fileStream, boundingBox);
//            //}
//        }
    }
}
