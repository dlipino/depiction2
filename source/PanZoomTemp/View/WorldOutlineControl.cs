﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempCore.ShapeModels;
using Depiction2.TempCore.ShapeView;
using Depiction2.Utilities.Drawing;

namespace PanZoomTemp.View
{
    public class WorldOutlineControl : FrameworkElement
    {
        static Pen drawPen = new Pen(Brushes.Black, 1);

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource",
                typeof(object),
                typeof(WorldOutlineControl),
                new PropertyMetadata(OnItemsSourceChanged));

        public static readonly DependencyProperty BackgroundProperty =
            Panel.BackgroundProperty.AddOwner(typeof(WorldOutlineControl));

        public object ItemsSource
        {
            set { SetValue(ItemsSourceProperty, value); }
            get { return (object)GetValue(ItemsSourceProperty); }
        }

        static void OnItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            (obj as WorldOutlineControl).OnItemsSourceChanged(args);
        }

        void OnItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue != null)
            {
                var oldVal = args.OldValue;
                if (oldVal is RangeObservableCollection<ShapeData>)
                {
                    ((RangeObservableCollection<ShapeData>)oldVal).CollectionChanged -= OnCollectionChanged;
                }
                else if (oldVal is RangeObservableCollection<ShapeDataViewModel>)
                {

                    ((RangeObservableCollection<ShapeDataViewModel>)oldVal).CollectionChanged -= OnCollectionChanged;
                }
            }

            if (args.NewValue != null)
            {
                var oldVal = args.NewValue;
                if (oldVal is RangeObservableCollection<ShapeData>)
                {
                    ((RangeObservableCollection<ShapeData>)oldVal).CollectionChanged += OnCollectionChanged;
                }
                else if (oldVal is RangeObservableCollection<WorldOutLineModel>)
                {

                    ((RangeObservableCollection<WorldOutLineModel>)oldVal).CollectionChanged += OnCollectionChanged;
                }
            }

            InvalidateVisual();
        }

        

        #region constructor
        public WorldOutlineControl()
        {
            drawPen.Freeze();
            this.CacheMode = new BitmapCache();
        }
        #endregion

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            InvalidateVisual();
        }
        
        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(Brushes.White, null, new Rect(RenderSize));
            if (ItemsSource == null) return;
//            drawPen = new Pen(Brushes.Black, 1000);
//            drawPen.Freeze();

            if (ItemsSource is RangeObservableCollection<ShapeData>)
            {
                var items =  ((RangeObservableCollection<ShapeData>)ItemsSource);
                foreach (var dataPoint in items)
                {
                    dc.DrawGeometry(Brushes.Black, drawPen, DrawingHelpers.CreateGeometryFromEnhancedPointListWithChildren(dataPoint.PointList));
                }
            }
            else if (ItemsSource is RangeObservableCollection<ShapeDataViewModel>)
            {
                var items = ((RangeObservableCollection<ShapeDataViewModel>) ItemsSource);
                foreach (var dataPoint in items)
                {
                    dc.DrawGeometry(Brushes.Black, drawPen, dataPoint.ShapeGeometry);
                }
            }
        }
    }
}
