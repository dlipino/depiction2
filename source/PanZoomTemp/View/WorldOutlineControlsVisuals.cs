﻿//This is not fast enought to draw background stuff, not does no the lummie parcels
//Might be useful for small visible subset.
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.TempAPI.ExtensionClasses;
using Depiction2.TempCore.ShapeModels;
using Depiction2.TempCore.ShapeView;

namespace PanZoomTemp.View
{
    public class WorldOutlineControlsVisuals : FrameworkElement
    {
        static Pen drawPen = new Pen(Brushes.Black, 1);

        VisualCollection visualChildren;

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource",
                typeof(object),
                typeof(WorldOutlineControlsVisuals),
                new PropertyMetadata(OnItemsSourceChanged));

        public object ItemsSource
        {
            set { SetValue(ItemsSourceProperty, value); }
            get { return (object)GetValue(ItemsSourceProperty); }
        }
        #region contructor
        public WorldOutlineControlsVisuals()
        {
            visualChildren = new VisualCollection(this);
           // ToolTip = "";
        }
        #endregion


        static void OnItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            (obj as WorldOutlineControlsVisuals).OnItemsSourceChanged(args);
        }

        void OnItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            visualChildren.Clear();
            if (args.OldValue != null)
            {
                var oldVal = args.OldValue;
                if (oldVal is RangeObservableCollection<ShapeData>)
                {
                    ((RangeObservableCollection<ShapeData>)oldVal).CollectionChanged -= OnCollectionChanged;
                    ((RangeObservableCollection<ShapeData>)oldVal).CollectionCleared -= OnCollectionCleared;
                }
                else if (oldVal is RangeObservableCollection<ShapeDataViewModel>)
                {
                    ((RangeObservableCollection<ShapeDataViewModel>)oldVal).CollectionChanged -= OnCollectionChanged;
                    ((RangeObservableCollection<ShapeDataViewModel>)oldVal).CollectionCleared -= OnCollectionCleared;
                }
            }

            if (args.NewValue != null)
            {
                var newVal = args.NewValue;
                if (newVal is RangeObservableCollection<ShapeData>)
                {
                    ((RangeObservableCollection<ShapeData>)newVal).CollectionChanged += OnCollectionChanged;
                    ((RangeObservableCollection<ShapeData>) newVal).CollectionCleared += OnCollectionCleared;
                }
                else if (newVal is RangeObservableCollection<ShapeDataViewModel>)
                {
                    ((RangeObservableCollection<ShapeDataViewModel>)newVal).CollectionChanged += OnCollectionChanged;
                    ((RangeObservableCollection<ShapeDataViewModel>)newVal).CollectionCleared += OnCollectionCleared;
                }
                CreateVisualChildren(newVal as ICollection);
            }
//            if (args.OldValue != null)
//            {
//                ObservableNotifiableCollection<DataPoint> coll = args.OldValue as ObservableNotifiableCollection<DataPoint>;
//                coll.CollectionCleared -= OnCollectionCleared;
//                coll.CollectionChanged -= OnCollectionChanged;
//                coll.ItemPropertyChanged -= OnItemPropertyChanged;
//            }
//
//            if (args.NewValue != null)
//            {
//                ObservableNotifiableCollection<DataPoint> coll = args.NewValue as ObservableNotifiableCollection<DataPoint>;
//                coll.CollectionCleared += OnCollectionCleared;
//                coll.CollectionChanged += OnCollectionChanged;
//                coll.ItemPropertyChanged += OnItemPropertyChanged;
//
//                CreateVisualChildren(coll);
//            }
        }

        void OnCollectionCleared(object sender, EventArgs args)
        {
            RemoveVisualChildren(visualChildren);
        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.OldItems != null)
                RemoveVisualChildren(args.OldItems);

            if (args.NewItems != null)
                CreateVisualChildren(args.NewItems);
        }

//        void OnItemPropertyChanged(object sender, ItemPropertyChangedEventArgs args)
//        {
//            DataPoint dataPoint = args.Item as DataPoint;
//
//            foreach (Visual child in visualChildren)
//            {
//                DrawingVisualPlus drawingVisual = child as DrawingVisualPlus;
//
//                if (dataPoint == drawingVisual.DataPoint)
//                {
//                    // Assume only VariableX or VariableY are changing
//                    TranslateTransform xform = drawingVisual.Transform as TranslateTransform;
//
//                    if (args.PropertyName == "VariableX")
//                        xform.X = RenderSize.Width * dataPoint.VariableX;
//
//                    else if (args.PropertyName == "VariableY")
//                        xform.Y = RenderSize.Height * dataPoint.VariableY;
//                }
//            }
//        }

        void CreateVisualChildren(ICollection coll)
        {
            foreach (var obj in coll)
            {
                var dataPoint = obj as ShapeDataViewModel;
                if(dataPoint == null) continue;
                var drawingVisual = new OutlineVisual();
                drawingVisual.Shape = dataPoint;
//                drawingVisual.DataPoint = dataPoint;
                DrawingContext dc = drawingVisual.RenderOpen();

                dc.DrawGeometry(Brushes.Black,drawPen,dataPoint.ShapeGeometry);

//                drawingVisual.Transform = new TranslateTransform(RenderSize.Width * dataPoint.VariableX,
//                                                                 RenderSize.Height * dataPoint.VariableY);

                dc.Close();
                visualChildren.Add(drawingVisual);
            }
        }

        void RemoveVisualChildren(ICollection coll)
        {
            foreach (object obj in coll)
            {
                var dataPoint = obj as ShapeDataViewModel;
                var removeList = new List<OutlineVisual>();

                foreach (var child in visualChildren)
                {
                    var drawingVisual = child as OutlineVisual;
                    if (drawingVisual.Shape == dataPoint)
                    {
                        removeList.Add(drawingVisual);
                        break;
                    }
                }
                foreach (var drawingVisual in removeList)
                    visualChildren.Remove(drawingVisual);
            }
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
//            foreach (Visual child in visualChildren)
//            {
//                DrawingVisualPlus drawingVisual = child as DrawingVisualPlus;
//                TranslateTransform xform = drawingVisual.Transform as TranslateTransform;
//
//                if (sizeInfo.WidthChanged)
//                    xform.X = sizeInfo.NewSize.Width * drawingVisual.DataPoint.VariableX;
//
//                if (sizeInfo.HeightChanged)
//                    xform.Y = sizeInfo.NewSize.Height * drawingVisual.DataPoint.VariableY;
//            }
            base.OnRenderSizeChanged(sizeInfo);
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return visualChildren.Count;
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (index < 0 || index >= visualChildren.Count)
                throw new ArgumentOutOfRangeException("index");

            return visualChildren[index];
        }

        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(Brushes.Orange, null, new Rect(RenderSize));
        }

        protected override void OnToolTipOpening(ToolTipEventArgs e)
        {
//            HitTestResult result = VisualTreeHelper.HitTest(this, Mouse.GetPosition(this));
//
//            if (result.VisualHit is DrawingVisualPlus)
//            {
//                DrawingVisualPlus drawingVisual = result.VisualHit as DrawingVisualPlus;
//                DataPoint dataPoint = drawingVisual.DataPoint;
//                ToolTip = String.Format("{0}, X={1}, Y={2}", dataPoint.ID, dataPoint.VariableX, dataPoint.VariableY);
//            }
            base.OnToolTipOpening(e);
        }

        protected override void OnToolTipClosing(ToolTipEventArgs e)
        {
            ToolTip = "";
            base.OnToolTipClosing(e);
        }

        class OutlineVisual : DrawingVisual
        {
            public ShapeDataViewModel Shape { get; set; }
        }
    }
}
