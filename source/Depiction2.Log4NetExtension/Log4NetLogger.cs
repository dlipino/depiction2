﻿using System;
using System.IO;
using System.Reflection;
using Depiction2.API.Extension.Logging;
using log4net;
using log4net.Config;

//http://www.apache.org/licenses/LICENSE-2.0

namespace Depiction2.Log4NetExtension
{
    //This extension should be shared be the application
    [LogExtensionMetadata(Name = "Log4NetLogger", Description = "Logging using log4net", Author = "Depiction Inc.", DisplayName = "Log4Net logger")]
    public class Log4NetLogger : ILogExtension
    {
        private static ILog _log = null;
        public static void InitializeLogger()
        {
            if (_log == null)
            {
                //Here is the once-per-class call to initialize the log object
                var executingAssemglyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var configName = "DepictionLog4NetLogger.config";
                var configLocation = configName;
                if(!string.IsNullOrEmpty(executingAssemglyLocation))
                {
                    configLocation = Path.Combine(executingAssemglyLocation, configName);
                }
                var fileInfo = new FileInfo(configLocation); 
                XmlConfigurator.Configure(fileInfo);
                _log = LogManager.GetLogger(typeof(Log4NetLogger));
            }
        }
        public void LogException(Exception ex)
        {
            LogException(null,ex);
        }
        public void LogException(string additionalInfo, Exception ex)
        {
            if (_log == null)
            {
                InitializeLogger();
            }
            if (_log == null) return;
            if(string.IsNullOrEmpty(additionalInfo))
            {
                _log.Error(ex);
            }else
            {
                _log.Error(additionalInfo,ex);
            }
        }

        public void LogEvent(string eventInfo)
        {
            if (_log == null)
            {
                InitializeLogger();
            }
            if (_log == null) return;
            _log.Info(eventInfo);
        }

        public void Dispose()
        {
            //Dispose of the _log, hopefully, someday
            _log = null;
        }
    }
}