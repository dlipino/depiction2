﻿using System.IO;
using Depiction2.API.Service;
using Depiction2.GdalExtension.EnvironmentSetters;
using NUnit.Framework;
using OSGeo.GDAL;

namespace Depiction2.GdalExtension.UnitTests.GdalControlTests
{
    [TestFixture]
    public class GdalIOTests
    {

        private int gdalReaderDriverCount = 109;
        private DepictionFolderService _depictionFolder;
        #region setup/teardown
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            Assert.AreEqual(gdalReaderDriverCount, Gdal.GetDriverCount());
            _depictionFolder = new DepictionFolderService(true);
        }

        [TearDown]
        protected void TearDown()
        {
            GDALEnvironment.UnsetGdalEnvironment();

            if (_depictionFolder != null) _depictionFolder.Close();
        }
        #endregion

        [Test]
        public void GdalImageWriteTest()
        {
            var fileName = "geolocated.tif";
            var fullFileName = TestUtilities.GetFullFileName(fileName, "Images");
            var data = Gdal.Open(fullFileName, Access.GA_ReadOnly);

            var driver = Gdal.GetDriverByName("GTiff");
            var outName = Path.Combine(_depictionFolder.FolderName, "outfile.tiff");
            var copyRes = driver.CreateCopy(outName, data, 0, new string[0], null, null);
            copyRes.Dispose();
            driver.Dispose();

            var pngdriver = Gdal.GetDriverByName("png");
            outName = Path.Combine(_depictionFolder.FolderName, "outfile.png");
            copyRes = pngdriver.CreateCopy(outName, data, 0, new string[0], null, null);
            pngdriver.Dispose();
            copyRes.Dispose();

            var jpegdriver = Gdal.GetDriverByName("jpeg");
            outName = Path.Combine(_depictionFolder.FolderName, "outfile.jpeg");
            jpegdriver.CreateCopy(outName, data, 0, new string[0], null, null).Dispose();
            jpegdriver.Dispose();

        }
        //        [Ignore("This sucks")]
        //        [Test]
        //        public void GeoTiffImportTest()
        //        {
        //            var tempDir = temp.FolderName;
        //            var baseName = "tfwImage";
        //            var imageName = baseName + ".tif";
        //            var tempImageName = Path.Combine(tempDir, imageName);
        //            string fileStart = "Depiction.UnitTests.TestImages.WorldFile.";
        //            string assemblyName = "Depiction.UnitTests";
        //            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
        //            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
        //            var dsOriginal = Gdal.Open(tempImageName, Access.GA_ReadOnly);
        //            var tempOut = "C:\\dlp\\depTest";
        //            var outName = "outtiffRoundTrip.tif";
        //            var tempOutImage = Path.Combine(tempOut, outName);
        //            //            var saveOptions = new string[] { "TFW=YES" };
        //            var saveOptions = new string[] { };
        //            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
        //
        //            IProjectedCoordinateSystem popularVisualizationCS = CoordinateSystemWktReader.Parse(popularVisualisationWKT) as IProjectedCoordinateSystem;
        //            //Cant get an inverse out of this one
        //            //            const string geowkt = "GEOGCS[\"WGS 84\"," +
        //            //                                  "DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]]," +
        //            //                                  "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
        //            //                                  "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4326\"]]";
        //            //            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geowkt) as IGeographicCoordinateSystem;
        //
        //
        //            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geoWKT) as IGeographicCoordinateSystem;
        //
        //
        //            CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();
        //
        //            //// MORE THAN MEETS THE EYE!!
        //            var transformer = ctfac.CreateFromCoordinateSystems(geoCS, popularVisualizationCS);
        //
        //            var topLeftLatLong = new LatitudeLongitude(47.9320413898329, -122.374351441618);
        //            var botLatLong = new LatitudeLongitude(47.5776691792137, -121.954224837974);
        //
        //            var topleft = WorldToGeoCanvas(topLeftLatLong, transformer);
        //            var bottomRight = WorldToGeoCanvas(botLatLong, transformer);
        //            var imagePixWidth = dsOriginal.RasterXSize;//pixWidth of baseimage
        //            var imagePixHeight = dsOriginal.RasterYSize;//pixheight of baseimage
        //
        //            var widthDis = bottomRight.X - topleft.X;
        //            var heightDis = bottomRight.Y - topleft.Y;
        //            // top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
        //            double pixelWidth = Math.Abs(widthDis / imagePixWidth);
        //            double pixelHeight = Math.Abs(heightDis / imagePixHeight);
        //            //Alright so there are oddities with the saved TFW files and the ones that depiction uses as the truth. I believe the gdal since it seems more consistent
        //            //with what sources say about the tfw files. ie we shouldn't use the  + (pixelWidth / 2) and  - (pixelHeight / 2)
        //            //The removal of half pixel height is because spatial reference is expecting the mid point (real world length) of the topleft coordinate pixel
        //            //                var wgs84Transform = new double[] { tlUTM.EastingMeters + (pixelWidth / 2), pixelWidth, 0, tlUTM.NorthingMeters - (pixelHeight / 2), 0, -pixelHeight };
        //            var wgs84Transform = new double[] { topleft.X, pixelWidth, 0, topleft.Y, 0, -pixelHeight };
        //            geoTiff.SetGeoTransform(wgs84Transform);
        //            geoTiff.SetProjection(popularVisualisationWKT);
        //            geoTiff.FlushCache();
        //            geoTiff.Dispose();
        //
        //            var reloadTiff = Gdal.Open(tempOutImage, Access.GA_ReadOnly);
        //            var outTrans = new double[6];
        //            reloadTiff.GetGeoTransform(outTrans);
        //            var proj = reloadTiff.GetProjection();
        //            var projRef = reloadTiff.GetProjectionRef();
        //            var topLeftPoint = new System.Windows.Point(outTrans[0], outTrans[3]);
        //            var topLeftCart = GeoCanvasToWorld(topLeftPoint, transformer);
        //        }  
    }
}