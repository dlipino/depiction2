﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Depiction2.Base.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.FileLoading;
using NUnit.Framework;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction2.GdalExtension.UnitTests.VectorFileReadingTests
{
    [TestFixture]
    public class OGRFileImporterExtensionTest
    {
        //OGRFileImporterExtension.cs
        private OGRFileImporterExtension ogrImporter;
        private OgrFileReadingService ogrReadingService;
        private IElementTemplate _genericTemplateNoProps;
        #region helpers
        public const string gcs =
                  "GEOGCS[\"GCS_WGS_1984\"," +
                      "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                      "PRIMEM[\"Greenwich\",0]," +
                      "UNIT[\"Degree\",0.0174532925199433]" +
                  "]";

        static internal string GetFullFileName(string fileName, string holdingFolder)
        {
            var currentAssemblyDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(currentAssemblyDirectoryName);
            var fileDir = Path.Combine("TestFiles", holdingFolder);
            var fullFileName = Path.Combine(currentAssemblyDirectoryName, fileDir, fileName);
            Assert.IsTrue(File.Exists(fullFileName), "Could not find file for ogr import test");
            return fullFileName;
        }

        #endregion

        [SetUp]
        protected void Setup()
        {
            ogrImporter = new OGRFileImporterExtension();
            ogrReadingService = new OgrFileReadingService();
            Assert.AreEqual(58, Ogr.GetDriverCount());
            var scaffold = new ElementTemplate("Generic");
            _genericTemplateNoProps = scaffold;
        }

        [TearDown]
        protected void TearDown()
        {
            _genericTemplateNoProps = null;
            ogrImporter.Dispose();
            ogrReadingService = null;
            OGREnvironment.UnsetOGREnvironment();
        }

        [Test]
        public void DoesWorldCountryShapeFileLoad()
        {
            DateTime startTime;
            DateTime endTime;
            string elapsedTime;
            var fileName = GetFullFileName("countries.shp", "countriesShp");

            startTime = DateTime.Now;
            var elements = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, string.Empty);
            endTime = DateTime.Now;
            elapsedTime = string.Format("It took {0} create {1} geometries with info", (endTime - startTime).TotalSeconds, elements.Count);
            Debug.WriteLine(elapsedTime);

            Assert.IsNotNull(elements);
            foreach (var element in elements)
            {
                Assert.IsTrue(element.ContainsKey("ElementGeometry"));
            }

            Assert.AreEqual(248, elements.Count);
            var first = elements.First();
            Assert.AreNotEqual(0, first.Count);
            startTime = DateTime.Now;
            elements = ogrReadingService.CreateElementPropertiesFromFile(fileName, false, string.Empty);
            endTime = DateTime.Now;
            elapsedTime = string.Format("It took {0} create {1} geometries without info", (endTime - startTime).TotalSeconds, elements.Count);
            Debug.WriteLine(elapsedTime);

            //Now test for speed without the property creation

        }

        [Test]
        public void GetLayerPropertiesTest()
        {
            var propsInCommonCount = 11;//value taken after the first run
            var fileName = GetFullFileName("countries.shp", "countriesShp");

            var props = ogrImporter.FindPropertyIntersections(fileName);
            Assert.AreEqual(propsInCommonCount, props.Count);


            var elements = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, string.Empty);
            Assert.AreEqual(248, elements.Count);
            foreach (var elem in elements)
            {
                Assert.IsTrue(props.IsSubsetOf(elem.Keys));
            }
        }
        [Test]
        public void KMZReadingTest()
        {
            var fileName = GetFullFileName("1.0_week_age.kml", "KMZ");
            var url = "http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-age.kmz";
            //        http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week_age.kml
            //f=0
            var data = Ogr.Open(fileName, Convert.ToInt32(true));
            var layerc = data.GetLayerCount();
            var r = data.GetRefCount();
            var templateProperties = new List<Dictionary<string, object>>();
            

            for (int i = 0; i < layerc; i++)
            {
                var layer = data.GetLayerByIndex(i);
                Console.WriteLine(layer.GetName() + " Count: " + layer.GetFeatureCount(i));
                var feature = layer.GetNextFeature();
                var gc = layer.GetGeometryColumn();
                var ld = layer.GetLayerDefn();
                
                while (feature != null)
                {
                    var featureData = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
                    var dr = feature.GetDefnRef();
                    var fc = dr.GetFieldCount();
                    var rc = dr.GetReferenceCount();

                    featureData.Add(PropertyKeys.geometryKey, feature.GetGeometryRef());
                        var s1 = feature.GetStyleString();
                    Console.WriteLine("Field count " + fc + " Style: " + s1);

                    var id = feature.GetFID();
                    for (int j = 0; j < fc; j++)
                    {
                        var fref = feature.GetFieldDefnRef(j);
                        var fType = fref.GetFieldType();
                        var fName = fref.GetName();
                        Console.WriteLine(fType);
                        switch (fType)
                        {
                            case FieldType.OFTInteger:
                                featureData.Add(fName, feature.GetFieldAsInteger(j));
                                break;
                            case FieldType.OFTReal:
                                featureData.Add(fName, feature.GetFieldAsDouble(j));
                                break;
                            case FieldType.OFTString:
                            case FieldType.OFTWideString:
                                featureData.Add(fName, feature.GetFieldAsString(j));
                                break;
                            case FieldType.OFTDate:
                            case FieldType.OFTDateTime:

                                var s = feature.GetFieldAsString(j);
                                if (!string.IsNullOrEmpty(s))
                                {
                                    int year, month, day, hour, min, sec, flag;
                                    feature.GetFieldAsDateTime(j, out year, out month, out day, out hour, out min, out sec, out flag);
                                    featureData.Add(fName, new DateTime(year, month, day, hour, min, sec));
                                }

                                break;
                        }
                    }
                    templateProperties.Add(featureData);
                    feature = layer.GetNextFeature();
                }

            }
            data.Dispose();
            //            Console.WriteLine(layer.GetGeomType());
            //            Console.WriteLine(layer.GetFeatureCount(0));
        }
        [Test]
        [Ignore]
        public void MissplacedTestForElementRepositoryUpdating()
        {
            //From the previous test grabbing 
            //            //[1] = "GMI_CNTRY"
            //            //[0] = "FIPS_CNTRY"
            //            var fileName = GetFullFileName("countries.shp", "countriesShp");
            //
            //            var repo = new Story();
            //
            //            var fileProjSystem = string.Empty;
            //            var idProp = "GMI_CNTRY";
            //            var idProp2 = "FIPS_CNTRY";
            //            var unknownProp = "bla";
            //            var propToChange = "POP_CNTRY";
            //            var newVal = 2;
            //            var propsAndGeo = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, idProp);
            //            var propsAndGeo2 = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, idProp2);
            //            foreach (var geomAndPropse in propsAndGeo2)
            //            {
            //                var popProp = geomAndPropse.properties.FirstOrDefault(t => t.ProperName.Equals(propToChange)) as PropertyScaffold;
            //                if (popProp == null) continue;
            //                popProp.ValueString = newVal.ToString(CultureInfo.InvariantCulture);
            //
            //            }
            //            var countryCount = 248;
            //            Assert.AreEqual(countryCount, propsAndGeo.Count, "Did not create correct number of property/geometry holders");
            //            repo.UpdateRepo(propsAndGeo, idProp, genericScaffoldNoProps);
            //            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not create correct number of elements");
            //
            //            //Try to duplicate the add
            //            repo.UpdateRepo(propsAndGeo, idProp, genericScaffoldNoProps);
            //            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not replace elements that had matching ids");
            //            foreach (DepictionElement elem in repo.AllElements)
            //            {
            //                if (elem == null) continue;
            //                foreach (var prop in elem._additionalProperties)
            //                {
            //                    Assert.IsNotNull(prop.ValueType);
            //                }
            //            }
            //
            //
            //            repo.UpdateRepo(propsAndGeo2, idProp2, genericScaffoldNoProps);
            //            Assert.AreEqual(countryCount, repo.AllElements.Count, "Did not replace elements that had different matching ids");
            //            foreach (var elem in repo.AllElements)
            //            {
            //                var prop = elem.GetProperty(propToChange);
            //                Assert.IsNotNull(prop);
            //                Assert.AreEqual(newVal, prop.Value);
            //            }
            //
            //            repo.UpdateRepo(propsAndGeo, unknownProp, genericScaffoldNoProps);
            //            Assert.AreEqual(countryCount * 2, repo.AllElements.Count, "Did no add elements with nonmatching ids");
            //

        }


        [Test]
        [Ignore("This tests a larger shp file and does really help with simple bugs")]
        public void DoesLummiIslandFileLoad()
        {
            var fileName = GetFullFileName("WC_AR_TaxParcel.shp", "LummiTaxParcelsShp");
            var elements = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, null);
            Assert.IsNotNull(elements);

            Assert.AreEqual(106781, elements.Count);
        }

        [Test]
        [Ignore("Test to help optimize the .shp file reading, not really a functionality test")]
        public void FunWithTime()
        {
            var fileName = GetFullFileName("WC_AR_TaxParcel.shp", "LummiTaxParcelsShp");
            DateTime startTime;
            DateTime endTime;
            string elapsedTime;
            List<Dictionary<string, object>> geomAndPropList;
            //            Base for now properties is 15secs
            startTime = DateTime.Now;
            geomAndPropList = ogrReadingService.CreateElementPropertiesFromFile(fileName, false, string.Empty);

            endTime = DateTime.Now;
            Assert.AreEqual(106781, geomAndPropList.Count);
            elapsedTime = string.Format("It took {0} create {1} geometries without info", (endTime - startTime).TotalSeconds, geomAndPropList.Count);
            Console.WriteLine(elapsedTime);

            startTime = DateTime.Now;
            geomAndPropList = ogrReadingService.CreateElementPropertiesFromFile(fileName, true, string.Empty);

            endTime = DateTime.Now;

            Assert.AreEqual(106781, geomAndPropList.Count);
            elapsedTime = string.Format("It took {0} create {1} geometries with info", (endTime - startTime).TotalSeconds, geomAndPropList.Count);
            Console.WriteLine(elapsedTime);
        }


        [Test]
        [Ignore("Having fun with coordinate systems")]
        public void FunWithJson()
        {
            var fileName = GetFullFileName("countries.shp", "countriesShp");
            var dataSource = Ogr.Open(fileName, 0);
            var layerCount = dataSource.GetLayerCount();
            for (int i = 0; i < layerCount; i++)
            {
                Feature nextFeature;
                var layer = dataSource.GetLayerByIndex(i);
                while ((nextFeature = layer.GetNextFeature()) != null)
                {
                    var mainGeom = nextFeature.GetGeometryRef();
                    string[] stuff = new string[0];
                    var result = mainGeom.ExportToJson(stuff);
                }
            }
        }
        [Test]
        [Ignore("Having fun with coordinate systems")]
        public void FunWithGeographicAndProjectedCoordinateSystems()
        {
            var sr = new SpatialReference("");

            sr.SetProjCS("Mercator");
            sr.SetWellKnownGeogCS("WGS84");
            sr.SetMercator(0, 0, 1, 0, 0);
            //            var s = sr.GetSemiMinor();
            //            sr.SetGeogCS("Popular Visualisation CRS", "WGS84", "WGS84", 6378137.0, 298.257223563, "Greenwich", 0,
            //                         "degree", 0.0174532925199433);
            string res;
            sr.ExportToWkt(out res);
        }
    }
}