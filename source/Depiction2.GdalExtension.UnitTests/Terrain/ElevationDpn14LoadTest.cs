﻿using System.Linq;
using Depiction2.Story14IO.Story14.Depiction14PartLoaders;
using Depiction2.Story14IO.UnitTests.Dpn14;
using NUnit.Framework;

namespace Depiction2.GdalExtension.UnitTests.Terrain
{
    [TestFixture]
    public class ElevationDpn14LoadTest
    {
        [SetUp]
        public void SetupForLegacyLoadTests()
        {
//            DepictionAccess.NameTypeService = null;//inew DepictionSimpleTypeService()n case a previous test didnt end well
//            DepictionAccess.NameTypeService = ;
        }

        [TearDown]
        public void TearDown()
        {
//            DepictionAccess.NameTypeService = null;
        }

        [Test]
        [Ignore]
        public void DoesStoryWithElevationLoad()
        {
//            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142SavedElevation.dpn");
//            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
//            Assert.AreEqual(1, newElements.Count);
//
//            var terrainElement = newElements[0];
//            Assert.IsNotNull(terrainElement);
//            //            Assert.AreNotEqual(0, terrainElement.DefaultElementPropertyNames.Count);//TODO for now
//
//            var terrainProp = terrainElement.GetProperty("Terrain");
//            Assert.IsNotNull(terrainProp);
//
//            var terrainData = terrainProp.Value as TerrainCoverage;
//            Assert.IsNotNull(terrainData);
//
//            //So the elevation that is read in comes in as WSG coordinates
//
//            var lat = 47.61110020524255;
//            var lon = -122.30083756326005;
//            var elev = 92.013d;// 0539f;
//            CheckElevationValue(elev, terrainData, new Point(lon, lat), 3);
//
//            //            var lat = 47.591461147884019;
//            //            var lon = -122.28841005498563;
//            //            var elev = 53.0721f;// m//174.120911ft 
//            //           var val = terrainData.TerrainPlusStructure.GetClosestElevationValue(new Point(lon, lat));
//            //            Assert.AreEqual(elev,val);
        }
        //        private void CheckElevationValue(double expected, TerrainCoverage data, Point location, int precision)
        //        {
        //            //            var lat = 47.591461147884019;
        //            //            var lon = -122.28841005498563;
        //            //            var elev = 53.0721f;// m//174.120911ft 
        //            var val = data.TerrainPlusStructure.GetClosestElevationValue(location);
        //            UnitTestUtilities.AssertDoubleEquality(expected, val, precision);
        //        }

        [Test]
        [Ignore("Elevation will not load from 1.4")]
        public void DoesStoryWithLargeElevationChangeLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142LargeElevationChanged.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(1, newElements.Count);

        }
        [Test]
        [Ignore("Must load geometry extension. Maybe even the elevation extension")]
        public void DoesStoryWithChangedElevationLoad()
        {
            var fullName = Dpn14LoadUtilities.GetFullFileNameAndUpdateTypes("Depiction142SimpleElevationChanged.dpn");
            var newElements = Depiction14ElementLoader.GetElementsFrom14DPNFile(fullName);
            Assert.AreEqual(2, newElements.Count);

            var terrainElement = newElements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.Elevation"));
            Assert.IsNotNull(terrainElement);
            var groundChangeElement = newElements.FirstOrDefault(t => t.ElementType.Equals("Depiction.Plugin.ChangeGroundHeight"));
            Assert.IsNotNull(groundChangeElement);
            var geom = groundChangeElement.ElementGeometry;
            Assert.IsNotNull(geom);
            var points = geom.GeometryPoints;

        }


    }
}