﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction2.GdalExtension.Terrain;
using Depiction2.GdalExtension.Terrain.Utilities;
using NUnit.Framework;

namespace Depiction2.GdalExtension.UnitTests.Terrain
{
    [TestFixture]
    public class CoverageDataConverterTest
    {
        [Test]
        public void GenericElevationFileLoad()
        {
            var fullFile = TestUtilities.GetFullFileName("SeattleElevationNotOneMeter14.tiff", "Terrain");
            Assert.IsTrue(File.Exists(fullFile));
            var converter = new CoverageDataConverter(fullFile, "m",  false, null);
            var elems = converter.ConvertDataToElements();
            Assert.IsTrue(elems.Any());
            var elevation = elems.FirstOrDefault();
            Assert.IsNotNull(elevation,"Elevation was null");
            var elevProp = elevation.GetDepictionProperty("Terrain");
            Assert.IsNotNull(elevProp,"Must have an elevation property");
            var elevVal = elevProp.Value as RestorableTerrainData;
            Assert.IsNotNull(elevVal,"Must have good terrain coverage");
            var elevData = elevVal.TerrainBounds;
            Assert.IsNotNull(elevData, "Needs a geolocation");
        }
        [Test]
        public void TerrainDataCheckTest()
        {
            var fullFile = TestUtilities.GetFullFileName("SeattleElevationNotOneMeter20.tiff", "Terrain");
            Assert.IsTrue(File.Exists(fullFile));
            var terrain = new TerrainData();
            terrain.LoadElevationFile(fullFile);
            var coverage = new RestorableTerrainData(terrain);
            var modPoint = new Point(-122.35940815625,47.617286209375 );
            var cleanPoint = new Point(-122.3805692625, 47.620266646875);
            string origString;
            string modString;
            var dataValue = RestorableTerrainData.GetElevationAndReadoutFromDataGrid(terrain, modPoint, "m", 4, out origString);
            var dataValueMod = RestorableTerrainData.GetElevationAndReadoutFromDataGrid(coverage.ModifiedTerrainData, modPoint, "m", 4, out modString);
            var wktOrig = terrain.GetProjectionInWkt();
            var wktMod = coverage.ModifiedTerrainData.GetProjectionInWkt();
            Assert.AreEqual(dataValue,dataValueMod);
            dataValue = RestorableTerrainData.GetElevationAndReadoutFromDataGrid(terrain, cleanPoint, "m", 4, out origString);
            dataValueMod = RestorableTerrainData.GetElevationAndReadoutFromDataGrid(coverage.ModifiedTerrainData, cleanPoint, "m", 4, out modString);
            Assert.AreEqual(dataValue, dataValueMod);
        }
        [Test]
        [Ignore]
        public void TerrainDataProjectionCheck()
        {
            var files = new List<string>
                            {
                                "SeattleElevationOneMeter14.tiff",
                                "SeattleElevationNotOneMeter14.tiff",
                                "SeattleElevationNotOneMeter20.tiff"
                            };
            foreach(var file in files)
            {
                var fullFile = TestUtilities.GetFullFileName(file, "Terrain");
                Assert.IsTrue(File.Exists(fullFile));
                var terrain = new TerrainData();
                terrain.LoadElevationFile(fullFile);
                var coverage = new RestorableTerrainData(terrain);
                var wktOrig = terrain.GetProjectionInWkt();
                var wktMod = coverage.ModifiedTerrainData.GetProjectionInWkt();
            }
        }
    }
}