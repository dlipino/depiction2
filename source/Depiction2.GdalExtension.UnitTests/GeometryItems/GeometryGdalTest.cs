﻿using System;
using System.Collections.Generic;
using System.Windows;
using Depiction2.Base.Geo;
using Depiction2.Base.Utilities;
using Depiction2.GdalExtension.GeometryItems;
using NUnit.Framework;

namespace Depiction2.GdalExtension.UnitTests.GeometryItems
{
    [TestFixture]
    public class GeometryGdalTest
    {

        #region test geometries
        GeometryGdal empty = new GeometryGdal();
        GeometryGdal empty1 = new GeometryGdal();

        GeometryGdal mainRect = new GeometryGdal(new List<Point> { new Point(2, 2), new Point(2, -2), new Point(-2, -2), new Point(-2, 2), new Point(2, 2) });

        GeometryGdal internalPoint = new GeometryGdal(new Point(1,1));
        GeometryGdal externalPoint = new GeometryGdal(new Point(3,3));
        GeometryGdal intersectingPoint = new GeometryGdal(new Point(2,2));

        GeometryGdal intersectingRect = new GeometryGdal(new List<Point> { new Point(3, 3), new Point(3, -1), new Point(-1, -1), new Point(-1, 3), new Point(3, 3) });
        GeometryGdal internalRect = new GeometryGdal(new List<Point> { new Point(1, 1), new Point(1, -1), new Point(-1, -1), new Point(-1, 1), new Point(1, 1) });
        GeometryGdal externalRect = new GeometryGdal(new List<Point> { new Point(4, 4), new Point(4, 3), new Point(3, 3), new Point(3, 4), new Point(4, 4) });

        GeometryGdal intersectLine = new GeometryGdal(new List<Point>{new Point(1,3), new Point(1,-3)});
        GeometryGdal nonIntersectLine = new GeometryGdal(new List<Point>{new Point(4,3),new Point(4,-3)});
        GeometryGdal withinLine = new GeometryGdal(new List<Point> { new Point(1, 0), new Point(-1, 0) });
        

        #endregion
        [Test]
        public void PoinListToSingleWktTest()
        {
            IDepictionGeometry geom = null;
            var singlePoint = new List<Point> { new Point() };
            geom = new GeometryGdal(DepictionWktGeometryUtilities.PointListToSingleWkt(singlePoint));
            Assert.AreEqual(DepictionGeometryType.Point, geom.GeometryType);


            var twopline = new List<Point> { new Point(0, 0), new Point(1, 1) };
            geom = new GeometryGdal(DepictionWktGeometryUtilities.PointListToSingleWkt(twopline));
            Assert.AreEqual(DepictionGeometryType.LineString, geom.GeometryType);

            var threepline = new List<Point> { new Point(0, 0), new Point(1, 1), new Point(2, 2) };
            geom = new GeometryGdal(DepictionWktGeometryUtilities.PointListToSingleWkt(threepline));
            Assert.AreEqual(DepictionGeometryType.LineString, geom.GeometryType);

            var rect = new List<Point> { new Point(1, 1), new Point(1, -1), new Point(-1, -1), new Point(-1, 1), new Point(1, 1) };
            geom = new GeometryGdal(DepictionWktGeometryUtilities.PointListToSingleWkt(rect));
            Assert.AreEqual(DepictionGeometryType.Polygon, geom.GeometryType);
        }

        [Test]
        public void WtkCreationTest()
        {
            var geomStringWithLineBreak = string.Format("LINESTRING (-122.34345397872301 47.609249385057304,-122.333237638298 47.608486612136801,{0}-122.333237638298 47.605024656755198," +
                 "-122.330268787234 47.603381614699899,-122.316996276596 47.604789939621398,-122.315948446809 47.603146890193202)", Environment.NewLine);
            //            var gome = new GeometryGdal(geomStringWithLineBreak);

            var geomStringNoLineBreak = geomStringWithLineBreak.Replace(Environment.NewLine, "");
            var geom = new GeometryGdal(geomStringNoLineBreak);
            //            wktGeometry =
            //                "LINESTRING (-122.3434539787230147.609249385057304,-122.333237638298 47.608486612136801,-122.33323763829847.605024656755198,-122.330268787234 47.603381614699899,-122.31699627659647.604789939621398,-122.315948446809 47.603146890193202)";
        }

        [Test]
        public void GeometryTypeVerificationTest()
        {
            Assert.AreEqual(DepictionGeometryType.Point, internalPoint.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Point, externalPoint.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Point, intersectingPoint.GeometryType);


            Assert.AreEqual(DepictionGeometryType.Polygon, mainRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, intersectingRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, internalRect.GeometryType);
            Assert.AreEqual(DepictionGeometryType.Polygon, externalRect.GeometryType);

            Assert.AreEqual(DepictionGeometryType.LineString, intersectLine.GeometryType);
            Assert.AreEqual(DepictionGeometryType.LineString, nonIntersectLine.GeometryType);
        }
        [Test]
        public void IntersectionTest()
        {
            //ensure all intesection type give a non exception
            //first test double nulls.
            Assert.IsFalse(empty.Intersects(empty1));
            Assert.IsFalse(empty1.Intersects(empty));

            Assert.IsTrue(mainRect.Intersects(intersectLine));
            Assert.IsTrue(intersectLine.Intersects(mainRect));
            Assert.IsFalse(mainRect.Intersects(nonIntersectLine));

            Assert.IsTrue(mainRect.Intersects(internalPoint));
            Assert.IsTrue(mainRect.Intersects(intersectingPoint));
            Assert.IsFalse(mainRect.Intersects(externalPoint));
            Assert.IsTrue(mainRect.Intersects(intersectLine));

            Assert.IsTrue(intersectLine.Intersects(internalPoint));
        }
        [Test]
        public void WithinTest()
        {
            Assert.False(mainRect.Within(withinLine));
            Assert.True(withinLine.Within(mainRect));

            Assert.False(intersectLine.Within(mainRect));
            Assert.IsTrue(internalPoint.Within(mainRect));
            Assert.IsFalse(externalPoint.Within(mainRect));
            Assert.False(intersectingPoint.Within(mainRect));

            Assert.True(intersectLine.Within(intersectLine));//sanity test
            Assert.IsFalse(nonIntersectLine.Within(mainRect));
            Assert.False(internalPoint.Within(withinLine));
        }
        [Test]
        public void ContainsTest()
        {
            Assert.True(mainRect.Contains(withinLine));
            Assert.False(withinLine.Contains(mainRect));

            Assert.False(intersectLine.Contains(mainRect));
            Assert.False(mainRect.Contains(intersectLine));//really?
            Assert.IsTrue(mainRect.Contains(internalPoint));
            Assert.False(internalPoint.Contains(withinLine));
            Assert.IsFalse(mainRect.Contains(externalPoint));
            Assert.False(mainRect.Contains(intersectingPoint));

            Assert.True(intersectLine.Contains(intersectLine));//sanity test
            Assert.IsFalse(nonIntersectLine.Contains(mainRect));
        }
    }
}