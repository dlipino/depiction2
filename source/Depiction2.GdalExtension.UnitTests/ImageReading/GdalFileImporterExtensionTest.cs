﻿using System.IO;
using System.Reflection;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.StoryEntities.ElementTemplates;
using Depiction2.Core.Entities.ElementTemplate;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.FileLoading;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OSR;

namespace Depiction2.GdalExtension.UnitTests.ImageReading
{
    [TestFixture]
    public class GdalFileImporterExtensionTest
    {
        private GdalImageImporterExtension gdalImporter;
        private GdalImageReadingService readingService;
        private IElementTemplate _genericTemplate;
        private DepictionFolderService _depictionFolder;
        private int gdalReaderDriverCount = 109;
        #region helpers

     
        //           adfGeoTransform[0] /* top left x */
        //    adfGeoTransform[1] /* w-e pixel resolution */
        //    adfGeoTransform[2] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[3] /* top left y */
        //    adfGeoTransform[4] /* rotation, 0 if image is "north up" */
        //    adfGeoTransform[5] /* n-s pixel resolution */
        [Test]
        public void CreateAGeocodedImage()
        {
            //create geo reference image via gdal
            var testImageName = "UnLocatedImage.jpg";
            var fullName = TestUtilities.GetFullFileName(testImageName, "Images");
            var dsOriginal = Gdal.Open(fullName, Access.GA_ReadOnly);
            var origWidth = 1920;
            var origHeight = 1200;
            var newWidth = 1920;
            var newHeight = 1200;
            var outImage = "geolocated.tif";
            var tempOutImage = Path.Combine(_depictionFolder.FolderName, outImage);
            //            var saveOptions = new string[] { "TFW=YES" }; 
            var saveOptions = new string[] { };
            var topLeft = new Point(-122.36085, 47.61819);
            var botRight = new Point(-122.30937, 47.58558);
            var dis = botRight - topLeft;
            var disPoint = new Point(dis.X / origWidth, dis.Y / origHeight);

            var geoTiffDriver = Gdal.GetDriverByName("GTiff");//outputFileName, Access.GA_ReadOnly);
            var geoTiff = geoTiffDriver.CreateCopy(tempOutImage, dsOriginal, 0, saveOptions, null, null);
            var gcsTransform = new double[] { topLeft.X, disPoint.X, 0, topLeft.Y, 0, disPoint.Y };
            geoTiff.SetGeoTransform(gcsTransform);
            var outTrans = new double[6];
            geoTiff.GetGeoTransform(outTrans);
            SpatialReference srs = new SpatialReference("");
            srs.SetWellKnownGeogCS("WGS84");
            string gsWkt;
            srs.ExportToWkt(out gsWkt);
            geoTiff.SetProjection(gsWkt);

            geoTiff.FlushCache();
            geoTiff.Dispose();
        }

        #endregion
        #region setup/teardown
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            gdalImporter = new GdalImageImporterExtension();
            readingService = new GdalImageReadingService();
            Assert.AreEqual(gdalReaderDriverCount, Gdal.GetDriverCount());
            var scaffold = new ElementTemplate("Generic");
            _genericTemplate = scaffold;
            _depictionFolder = new DepictionFolderService(true);
        }

        [TearDown]
        protected void TearDown()
        {
            GDALEnvironment.UnsetGdalEnvironment();
            _genericTemplate = null;
            gdalImporter.Dispose();
            if (_depictionFolder != null) _depictionFolder.Close();
        }
        #endregion
        [Test]
        public void ReadGeoLocatedTiffImageTest()
        {
            var fileName = "geolocated.tif";
            var fullFileName = TestUtilities.GetFullFileName(fileName, "Images");
            var data = readingService.GetInformationFromGdalFile(fullFileName);
            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(data);

            var geom = element.ElementGeometry;
            var tlF = new Point(geom.Bounds.Left, geom.Bounds.Bottom);
            var brF = new Point(geom.Bounds.Right, geom.Bounds.Top);
            var tlExp = new Point(-122.36085, 47.61819);
            var brExp = new Point(-122.30937, 47.58558);
            var rotExp = 0;

            Assert.AreEqual(tlExp, tlF);
            Assert.AreEqual(brExp, brF);
            Assert.AreEqual(rotExp, element.ImageRotation);
            Assert.AreEqual(fileName, element.ImageResourceName);

            //                        Assert.AreEqual(ElementVisualSetting.Image, element.VisualState);

        }

        [Test]
        public void ReadNonGeoLocatedTiffImageTest()
        {
            var fileName = "nongeolocated.tif";
            var fullFileName = TestUtilities.GetFullFileName(fileName, "Images");
            var data = readingService.GetInformationFromGdalFile(fullFileName);
            var element = ElementAndElemTemplateService.CreateElementUsingPropertyDictionary(data);

            var geom = element.ElementGeometry;
            Assert.AreEqual(Rect.Empty, geom.Bounds);
            var rotExp = 0;

            Assert.AreEqual(rotExp, element.ImageRotation);
            Assert.AreEqual(fileName, element.ImageResourceName);

            //            Assert.AreEqual(ElementVisualSetting.Image, element.VisualState);//This is not set in the getinformationfromgdalfile method

        }
       
    }
}