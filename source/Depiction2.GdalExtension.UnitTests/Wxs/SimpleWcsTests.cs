﻿using System.IO;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.WxS;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OGR;

namespace Depiction2.GdalExtension.UnitTests.Wxs
{
    [TestFixture]
    public class SimpleWcsTests
    {
        private DepictionFolderService _depictionFolder;
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            OGREnvironment.SetOGREnvironment();
            var count = Ogr.GetDriverCount();
            _depictionFolder = new DepictionFolderService(true);
            var driverCount = 58;
            Assert.AreEqual(driverCount, Ogr.GetDriverCount());
            //            for (int i = 0; i < driverCount; i++)
            //            {
            //                Console.WriteLine(Ogr.GetDriver(i).GetName());
            //            }
        }

        [TearDown]
        protected void TearDown()
        {
            _depictionFolder.Close();
            OGREnvironment.UnsetOGREnvironment();
            GDALEnvironment.UnsetGdalEnvironment();
        }

        [Test]
        [Ignore]
        public void PredefinedUrlTest()
        {
            //http://depictioninc.com/wcs/?request=DescribeCoverage&version=1.0.0&service=WCS&coverage=depiction:test
            
            var request =
                "http://depictioninc.com/wcs/?STYLES=&VERSION=1.0.0&WIDTH=626&CRS=EPSG:4269&HEIGHT=927" +
                "&REQUEST=GetCoverage&FORMAT=GeoTIFF&COVERAGE=depiction:test" +
                "&EXCEPTIONS=application/vnd.ogc.se_xml&SERVICE=WCS" +
                "&SRS=EPSG:4326&BBOX=-122.4999999999,47.5,-122.25,47.75&TRANSPARENT=TRUE&";


            var data = Gdal.Open(request, Access.GA_ReadOnly);
            //elevation will always come out as geotiff, for now.
            var gtiff = Gdal.GetDriverByName("GTIFF");
            var fullName = Path.Combine(_depictionFolder.FolderName, "out.tiff");
            var outData = gtiff.CreateCopy(fullName, data, 0, new string[0], null, null);
            outData.Dispose();

        }
        [Test]
        [Ignore]
        public void CreateUrlTest()
        {
            var roi = new CartRect(new Point(-122.398367, 47.6306258), new Point(-122.259551, 47.574302));
            var url = "http://depictioninc.com/wcs/?";
            var layer = "depiction:test";

            
            var data = GdalWcsBackgroundService.GetWcsDataSetFromPartialInfo(roi, url, layer, "");
            //elevation will always come out as geotiff, for now.
            var gtiff = Gdal.GetDriverByName("GTIFF");
            var fullName = Path.Combine(_depictionFolder.FolderName, "out.tiff");
            var outData = gtiff.CreateCopy(fullName, data, 0, new string[0], null, null);
            outData.Dispose();
            data.Dispose();

        }
    }
    #region random code
    //            var srcGeoTransform = new double[6];
    //            data.GetGeoTransform(srcGeoTransform);
    //            var srcProjection = data.GetProjection();
    //            var srcBandCount = data.RasterCount;
    //
    //            var srcBandType = data.GetRasterBand(1).DataType;
    //            var srcOverviewCount = data.GetRasterBand(1).GetOverviewCount();
    //            //Turn the data into something that can be written to file correctly
    //            var options = new string[0];
    //            var mem = Gdal.GetDriverByName("MEM");
    //
    //            var xOffset = 0;
    //            var yOffset = 0;
    //            Dataset memData = null;// mem.Create("", dstWidth, dstHeight, 0, srcBandType, options);
    //            var dstGeoTransform = new double[6];
    //            dstGeoTransform[0] = srcGeoTransform[0] + (xOffset * srcGeoTransform[1]) + (yOffset * srcGeoTransform[2]);
    //            dstGeoTransform[1] = srcGeoTransform[1];
    //            dstGeoTransform[2] = srcGeoTransform[2];
    //            dstGeoTransform[3] = srcGeoTransform[3] + (xOffset * srcGeoTransform[4]) + (yOffset * srcGeoTransform[5]);
    //            dstGeoTransform[4] = srcGeoTransform[4];
    //            dstGeoTransform[5] = srcGeoTransform[5];
    //
    //            //Get the top most overview (least detail) for now ie zoom level
    //            for (int i = 1; i < srcBandCount + 1; i++)
    //            {
    //                var srcBand = data.GetRasterBand(i);
    //                var x = srcBand.XSize;
    //                var y = srcBand.YSize;
    //                if(srcOverviewCount>1)
    //                {
    //                    srcBand = srcBand.GetOverview(srcOverviewCount - 1);
    //                    x = srcBand.XSize;
    //                    y = srcBand.YSize;  
    //                }
    //
    //                if (memData == null)
    //                {
    //                    memData = mem.Create("", x, y, 0, srcBandType, options);
    //                }
    //                memData.SetGeoTransform(dstGeoTransform);
    //                memData.SetProjection(srcProjection);
    //                Band memBand = null;
    //                switch (srcBandType)
    //                {
    //                    case DataType.GDT_Byte:
    //                        var byteBuffer = new byte[x * y];
    //                        srcBand.ReadRaster(xOffset, yOffset, x, y, byteBuffer, x, y, 0, 0);
    //                        memData.AddBand(srcBand.DataType, options);
    //                        memBand = memData.GetRasterBand(i);
    //                        memBand.WriteRaster(0, 0, x, y, byteBuffer, x, y, 0, 0);
    //                        break;
    //                    case DataType.GDT_Int16:
    //                        var buffer = new int[x * y];
    //                        srcBand.ReadRaster(xOffset, yOffset, x, y, buffer, x, y, 0, 0);
    //                        memData.AddBand(srcBand.DataType, options);
    //                        memBand = memData.GetRasterBand(i);
    //                        memBand.WriteRaster(0, 0, x, y, buffer, x, y, 0, 0);
    //                        break;
    //                }
    //            }
    //
    //            data.Dispose();
    #endregion
}