﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Xml;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.WxS;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OGR;
using OSGeo.OSR;

namespace Depiction2.GdalExtension.UnitTests.Wxs
{
    [TestFixture]
    public class SimpleWmsTests
    {
        #region wms sources
        //        http://demo.mapserver.org/cgi-bin/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&BBOX=-122.398367,47.5743016156473,-122.259551,47.630625766462&SRS=EPSG:4326&WIDTH=953&HEIGHT=480&LAYERS=bluemarble,cities&STYLES=&FORMAT=image/png&TRANSPARENT=true
        //        http://demo.mapserver.org/cgi-bin/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&BBOX=-90,-90,180,90&SRS=EPSG:4326&WIDTH=953&HEIGHT=480&LAYERS=bluemarble,cities&STYLES=&FORMAT=image/png&TRANSPARENT=true
        //        http://sampleserver1.arcgisonline.com/ArcGIS/services/Specialty/ESRI_StatesCitiesRivers_USA/MapServer/WMSServer?VERSION=1.3.0&REQUEST=GetMap&CRS=CRS:84&BBOX=-178.217598,18.924782,-66.969271,71.406235&WIDTH=765&HEIGHT=360&LAYERS=0,1,2&STYLES=,,Symbolizer&EXCEPTIONS=application/vnd.ogc.se_xml&FORMAT=image/png&BGCOLOR=0xFFFFFF&TRANSPARENT=TRUE

        //         http://geoint.lmic.state.mn.us/cgi-bin/mncomp?request=GetMap&service=WMS&bbox=-102,30,-85,51&version=1.1.1&layers=mncomp&width=1000&height=1000&SRS=EPSG:4326

        //        http://suite.opengeo.org/geoserver/wms?service=WMS&version=1.3.0&request=GetMap&layers=usa:states&srs=EPSG:4326&bbox=24.956,-124.731,49.372,-66.97&format=image/png&width=780&height=330

        //        http://suite.opengeo.org/geoserver/wms?service=WMS&version=1.3.0&request=GetMap&layers=usa:states&srs=EPSG:4326&bbox=47.5743016156473,-122.398367,47.630625766462,-122.259551&format=image/png&width=780&height=330
        //        http://ows.terrestris.de/osm/service?styles=&layer=OSM-WMS&service=WMS&format=image%2Fpng&sld_version=1.1.0&request=GetLegendGraphic&version=1.1.1
        //         http://ows.terrestris.de/osm/service?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetCapabilities
        //        minx,miny,maxx,maxy
        //        &BGCOLOR=0xFF99CC&TRANSPARENT=TRUE
        #endregion
        private string functional =
            "http://ows.terrestris.de/osm/service?service=WMS&version=1.1.1&request=GetMap&layers=OSM-WMS&SRS=EPSG:4326&bbox=-122.398367,47.5743016156473,-122.259551,47.630625766462&STYLES=&FORMAT=image/png&width=512&height=512";

        private double top = 47.630625766462;
        private double left = -122.398367;
        private double bottom = 47.5743016156473;
        private double right = -122.259551;
//        private ICartRect seattleBounds = new CartRect(new Point(-122.398367, 47.630625766462), new Point(-122.259551, 47.5743016156473));
        private DepictionFolderService storageFolder;
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            OGREnvironment.SetOGREnvironment();
            var count = Ogr.GetDriverCount();
            var driverCount = 58;
            Assert.AreEqual(driverCount, Ogr.GetDriverCount());
            storageFolder = new DepictionFolderService(true);
            //            for (int i = 0; i < driverCount; i++)
            //            {
            //                Console.WriteLine(Ogr.GetDriver(i).GetName());
            //            }
        }

        [TearDown]
        protected void TearDown()
        {
            OGREnvironment.UnsetOGREnvironment();
            GDALEnvironment.UnsetGdalEnvironment();
            storageFolder.Dispose();
        }

        [Test]
        [Ignore("Should be done by hand since image verification is required.")]
        public void DoesOsmWMSWork()
        {
            var osmServerAddress = "http://ows.terrestris.de/osm/service?";
            var layerName = "OSM-WMS";
            var seattleBounds = new CartRect(new Point(-122.398367, 47.630625766462), new Point(-122.259551, 47.5743016156473));
           
            //request should look like:
//        http://ows.terrestris.de/osm/service?service=WMS&version=1.1.1&request=GetMap&layers=OSM-WMS&SRS=EPSG:4326&bbox=-122.398367,47.5743016156473,-122.259551,47.630625766462&Style=&FORMAT=image/png&width=256&height=256
            var memData = GdalWmsBackgroundService.GetTopLevelWmsOverView(seattleBounds, osmServerAddress, layerName,null);
            var driver = Gdal.GetDriverByName("jpg");
            var outFile = Path.Combine(storageFolder.FolderName, "final.jpg");
            var pngData = driver.CreateCopy(outFile, memData, 0, new string[0], null, null);
            driver.Dispose();
            pngData.Dispose();
            
        }

        #region how does gdal work tests
        [Test]
        [Ignore]
        public void SeattleOpenStreetMapImage()
        {
            var wmsAddress = "http://ows.terrestris.de/osm/service?";
            var layerName = "OSM-WMS";
            var roi = new CartRect(new Point(-122.398367, 47.630625766462), new Point(-122.259551, 47.5743016156473));
          
            var dstWidth = 512;
            var dstHeight = 512;
            var xOffset = 0;
            var yOffset = 0;

            var serviceType = "service=WMS";
            var version = "&version=1.1.1";
            var requestType = "&request=GetMap";
            var layer = string.Format("&layers={0}", layerName);
            var srs = "&SRS=EPSG:4326";
            var bbox = string.Format("&bbox={0},{1},{2},{3}", roi.Left, roi.Bottom, roi.Right, roi.Top);
            var style = "&Style=";
            var format = "&FORMAT=image/jpeg";
            var widthheight = string.Format("&width={0}&height={1}", dstWidth, dstHeight);
            var request = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}", wmsAddress, serviceType, version, requestType,
                                       layer, srs, bbox, style, format, widthheight);
            var data = Gdal.Open(request, Access.GA_ReadOnly);
            var srcGeoTransform = new double[6];
            data.GetGeoTransform(srcGeoTransform);
            var srcProjection = data.GetProjection();
            var srcBandCount = data.RasterCount;
            var srcBandType = data.GetRasterBand(1).DataType;
            //Turn the data into something that can be written to file correctly
            var options = new string[0];
            var mem = Gdal.GetDriverByName("MEM");
            OSGeo.GDAL.Dataset memData = null;// mem.Create("", dstWidth, dstHeight, 0, srcBandType, options);
            var dstGeoTransform = new double[6];
            dstGeoTransform[0] = srcGeoTransform[0] + (xOffset * srcGeoTransform[1]) + (yOffset * srcGeoTransform[2]);
            dstGeoTransform[1] = srcGeoTransform[1];
            dstGeoTransform[2] = srcGeoTransform[2];
            dstGeoTransform[3] = srcGeoTransform[3] + (xOffset * srcGeoTransform[4]) + (yOffset * srcGeoTransform[5]);
            dstGeoTransform[4] = srcGeoTransform[4];
            dstGeoTransform[5] = srcGeoTransform[5];
            var overCount = data.RasterXSize;
            for (int i = 1; i < srcBandCount + 1; i++)
            {
                var srcBand = data.GetRasterBand(i);
                var buffer = new byte[dstWidth * dstHeight];
                int off = 1;
                double otherf;
                srcBand.GetOffset(out otherf, out off);

                var oc = srcBand.GetOverviewCount();
                var meta = srcBand.GetMetadata("");
                int bx, by;
                srcBand.GetBlockSize(out bx, out by);
                var cn = srcBand.GetCategoryNames();

                for (int j = 0; j < oc; j++)
                {
                    var f = srcBand.GetOverview(20);
                    var ovBand = srcBand.GetOverview(j);
                    ovBand.GetBlockSize(out bx, out by);
                    ovBand.GetOffset(out otherf, out off);
                    var x = ovBand.XSize;
                    var y = ovBand.YSize;

                    if (j == 18)
                    {
                        if (memData == null)
                        {
                            memData = mem.Create("", x, y, 0, srcBandType, options);
                        }
                        memData.SetGeoTransform(dstGeoTransform);
                        memData.SetProjection(srcProjection);
                        buffer = new byte[x * y];
                        ovBand.ReadRaster(xOffset, yOffset, x, y, buffer, x, y, 0, 0);
                        memData.AddBand(ovBand.DataType, options);
                        var memBand = memData.GetRasterBand(i);
                        memBand.WriteRaster(0, 0, x, y, buffer, x, y, 0, 0);

                    }

                }
                //                ovBand.ReadRaster(xOffset, yOffset, dstWidth, dstHeight, buffer, dstWidth, dstHeight, 0, 0);

                //               
                //              
                //                var bType = srcBand.DataType;//Should always be the sames as srcBandType
                //                memData.AddBand(bType, options);
                //                var memBand = memData.GetRasterBand(i);
                //                memBand.WriteRaster(0, 0, dstWidth, dstHeight, buffer, dstWidth, dstHeight, 0, 0);
            }

            data.Dispose();

            var driver = Gdal.GetDriverByName("jpeg");
            var outFile = Path.Combine(storageFolder.FolderName, "final.jpeg");
            var pngData = driver.CreateCopy(outFile, memData, 0, new string[0], null, null);
            pngData.Dispose();
            
        }
        [Test]
        [Ignore]
        public void OtherMapServer()
        {
            var sample =
                "http://demo.mapserver.org/cgi-bin/wms?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&BBOX=-90,-90,180,90&SRS=EPSG:4326&WIDTH=953&HEIGHT=480&LAYERS=bluemarble,cities&STYLES=&FORMAT=image/png&TRANSPARENT=true";
            var data = Gdal.Open(sample, Access.GA_ReadOnly);
            var d = new double[6];
            data.GetGeoTransform(d);
            var saveOptions = new string[0];// { "TFW=YES" };
            var driver = Gdal.GetDriverByName("GTiff");

            //            var fileName = Path.Combine(storageFolder.FolderName, "Test.tiff");
            //            driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
        }

        [Test]
        [Ignore]
        public void XmlReadTest()
        {
            var xmlDoc = TestUtilities.GetFullFileName("AndAnother.xml", "WMSXml");
            var doc = new XmlDocument();
            doc.Load(xmlDoc);
            var stringdoc = doc.InnerXml;
            var data = Gdal.Open(stringdoc, Access.GA_ReadOnly);
            var driver = Gdal.GetDriverByName("png");
            var saveOptions = new string[0];
            var fileName = Path.Combine(storageFolder.FolderName, "Test.png");
            var copyRes = driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
            driver.Dispose();
            copyRes.Dispose();
        }

        [Test]
        [Ignore]
        public void OneEarthWMSTest()
        {
            var xmlDoc = TestUtilities.GetFullFileName("OneEarthJpl.xml", "WMSXml");
            var doc = new XmlDocument();
            doc.Load(xmlDoc);
            var stringdoc = doc.InnerXml;
            var data = Gdal.Open(stringdoc, Access.GA_ReadOnly);
            var d = new double[6];
            data.GetGeoTransform(d);
            var driver = Gdal.GetDriverByName("png");
            //            var saveOptions = new string[0];
            //            var fileName = Path.Combine(storageFolder.FolderName, "Test.png");
            //            var copyRes = driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
            //            driver.Dispose();
            //            copyRes.Dispose();
        }

        [Test]
        [Ignore]
        public void tilechachegeo()
        {
            //            gdal_translate -of PNG -outsize 500 250 metacarta_wmsc.xml metacarta_wmsc.png
            var xmlDoc = TestUtilities.GetFullFileName("tilecachegeo.xml", "WMSXml");
            var doc = new XmlDocument();
            doc.Load(xmlDoc);
            var stringdoc = doc.InnerXml;
            var data = Gdal.Open(stringdoc, Access.GA_ReadOnly);
            var d = new double[6];
            data.GetGeoTransform(d);
            var driver = Gdal.GetDriverByName("png");
            var saveOptions = new string[0];
            var fileName = Path.Combine(storageFolder.FolderName, "Test.png");
            var copyRes = driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
            driver.Dispose();
            copyRes.Dispose();
        }
        [Test]
        [Ignore]
        public void TMSSample()
        {
            var xmlDoc = TestUtilities.GetFullFileName("TMSSample.xml", "WMSXml");
            var doc = new XmlDocument();
            doc.Load(xmlDoc);
            var stringdoc = doc.InnerXml;
            var data = Gdal.Open(stringdoc, Access.GA_ReadOnly);
            var d = new double[6];
            data.GetGeoTransform(d);
            var driver = Gdal.GetDriverByName("png");
            //            var saveOptions = new string[0];
            //            var fileName = Path.Combine(storageFolder.FolderName, "Test.png");
            //            var copyRes = driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
            //            driver.Dispose();
            //            copyRes.Dispose();
        }


        [Test]
        [Ignore]
        public void DepictionxmlSample()
        {
            //                 private double top = 47.630625766462;
            //        private double left = -122.398367;
            //        private double bottom = 47.5743016156473;
            //        private double right = -122.259551;
            //            -projwin ulx uly lrx lry:
            //        http://www.gdal.org/gdal_translate.html
            //             gdal_translate -of PNG -projwin -122.398367 47.630625766462 -122.259551 47.5743016156473 thing.xml depiction.png
            //             gdal_translate -of PNG -outsize 500 250 osmwms.xml depiction.png
            //             gdal_translate -of PNG osmwms.xml depiction.png
            var xmlDoc = TestUtilities.GetFullFileName("osmwms.xml", "WMSXml");
            var doc = new XmlDocument();
            doc.Load(xmlDoc);
//            /osm/service?service=WMS&request=GetMap&version=1.1.1&layers=OSM-WMS&styles=&srs=EPSG:4326&format=image/jpeg&width=512&height=512&bbox=-122.39836700,47.57430162,-122.25955100,47.63062577 HTTP/1.1 
            var stringdoc = doc.InnerXml;
            var data = Gdal.Open(stringdoc, Access.GA_ReadOnly);
            //This creates a data of the size in the xml file
            var bCount = data.RasterCount;
            var bType = data.GetRasterBand(1).DataType;
            var d = new double[6];
            data.GetGeoTransform(d);
            var srcProj = data.GetProjection();
            var xoff = 0;
            var yoff = 0;
            // Adapt the geotransform matrix to the subarea
           
       
            var mem = Gdal.GetDriverByName("MEM");
            //this depends on the 
            var width = 1024;
            var height = 415;
            var memData = mem.Create("", width, height, 0, bType, new string[0]);
//            d[0] += (xoff * d[1]) + (yoff * d[2]);
//            d[3] += (xoff * d[4]) + (yoff * d[5]);
            var dstGeoTransform = new double[6];
            dstGeoTransform[0] = d[0] + (xoff * d[1]) + (yoff * d[2]);
            dstGeoTransform[1] = d[1];
            dstGeoTransform[2] = d[2];
            dstGeoTransform[3] = d[3] + (xoff * d[4]) + (yoff * d[5]);
            dstGeoTransform[4] = d[4];
            dstGeoTransform[5] = d[5];
            memData.SetGeoTransform(d);
            memData.SetProjection(srcProj);
            
            for(int i =1;i<bCount+1;i++)
            {
                var srcBand = data.GetRasterBand(i);
                var buffer = new byte[width*height];
                var o = srcBand.GetOverviewCount();
                srcBand.ReadRaster(xoff, yoff, width, height, buffer, width, height, 0, 0);
                memData.AddBand(bType,new string[0]);
                var vrtBand = memData.GetRasterBand(i);
                vrtBand.WriteRaster(0, 0, width, height, buffer, width, height, 0, 0);
            }
            var driver = Gdal.GetDriverByName("png");
            var outFile = Path.Combine(storageFolder.FolderName, "final.png");
            var pngData = driver.CreateCopy(outFile, memData, 0, new string[0], null, null);
//            var saveOptions = new string[0];
//            var fileName = Path.Combine(storageFolder.FolderName, "osmwms.png");
//            var copyRes = driver.CreateCopy(fileName, data, 0, saveOptions, null, null);
            driver.Dispose();
            memData.Dispose();
            pngData.Dispose();
            

//            copyRes.Dispose();

        }
        #endregion
        //        gdal.AllRegister();
        //Dataset ds = gdal.Open( "usapm.jpg", gdalconst.GA_ReadOnly );
        //
        //SpatialReference t_srs = new SpatialReference("");
        //t_srs.SetFromUserInput("+proj=longlat +ellps=clrk80 +no_defs");
        //string t_srs_wkt;
        //t_srs.ExportToWkt(out t_srs_wkt);
        //SpatialReference s_srs = new SpatialReference("");
        //s_srs.SetFromUserInput("+proj=utm +zone=16 +datum=NAD83 +no_defs");
        //string s_srs_wkt;
        //s_srs.ExportToWkt(out s_srs_wkt);
        //Dataset dswarp = gdal.AutoCreateWarpedVRT( ds, s_srs_wkt, t_srs_wkt, gdalconst.GRA_NearestNeighbour, 0.125 );
        //
        //Driver drv_out = gdal.GetDriverByName("GTiff");
        //
        //// saving the VRT dataset (optional)
        //Dataset dsw1 = dswarp.GetDriver().CreateCopy("usapm_bad.vrt", dswarp, 1, null);
        //// saving the warped image
        //Dataset dsw = drv_out.CreateCopy("usapm_bad.tif", dswarp, 0, new string[] { "INTERLEAVE=PIXEL", null });
    }
}