﻿using System;
using System.Collections.Generic;
using Depiction2.Base.Service;
using Depiction2.Core.StoryEntities.ElementTemplates;
using Depiction2.GdalExtension.EnvironmentSetters;
using Depiction2.GdalExtension.FileLoading;
using NUnit.Framework;
using OSGeo.OGR;

namespace Depiction2.GdalExtension.UnitTests.Wxs
{
    [TestFixture]
    public class SimpleWfsTests
    {
        //          m_dataset = OGRSFDriverRegistrar::Open("http://demo.mapserver.org/cgi-bin/wfs?SERVICE=WFS&VERSION=1.0.0&REQUEST=getFeatures&TYPENAME=cities", FALSE );
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            OGREnvironment.SetOGREnvironment();
            var count = Ogr.GetDriverCount();
            var driverCount = 58;
            Assert.AreEqual(driverCount, Ogr.GetDriverCount());
            //            for (int i = 0; i < driverCount; i++)
            //            {
            //                Console.WriteLine(Ogr.GetDriver(i).GetName());
            //            }
        }

        [TearDown]
        protected void TearDown()
        {

            OGREnvironment.UnsetOGREnvironment();
            GDALEnvironment.UnsetGdalEnvironment();
        }
//        [elementType,Depiction.Plugin.AdministrativeBoundary]
//        [hoverText,NAMELSAD10]
//        [ImporterName,WfsElementGatherer]
//        [url,http://depictioninc.com/wfs]
//        [layerName,depiction:CongressDistricts111]

        [Test]
        [Ignore]
        public void SimpleConnectionTest()
        {
            //        http://depictioninc.com/wfs?REQUEST=GetCapabilities&SERVICE=WFS&VERSION=1.1.1

            //            http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
            //        http://demo.mapserver.org/cgi-bin/wfs?
            var wfs = Ogr.Open("http://depictioninc.com/wfs?&SERVICE=WFS", 0);
            var layerCount = wfs.GetLayerCount();
            Console.WriteLine(wfs.GetName());
            //            for (int i = 0; i < layerCount; i++)
            //            {
            //                var layerData = wfs.GetLayerByIndex(i);
            //                var lname = layerData.GetName();
            //                var fcount = layerData.GetFeatureCount(0);
            //                var sr = layerData.GetSpatialRef();
            //                string wktString = string.Empty;
            //                sr.ExportToWkt(out wktString);
            //                var output = string.Format("Layer {0}, Features: {1}, SR:{2}", lname, fcount, wktString);
            //                Console.WriteLine(output);
            //            }

            //get a test file
            var layer = wfs.GetLayerByName("depiction:CongressDistricts111");
            Console.WriteLine(layer.GetGeomType());
            Console.WriteLine(layer.GetFeatureCount(0));
            Envelope area = new Envelope();
            layer.GetExtent(area, 0);
            var topy = 47.630625766462;
            var leftx = -122.398367;
            var boty = 47.5743016156473;
            var rightx = -122.259551;
            layer.SetSpatialFilterRect(leftx, boty, rightx, topy);
            var rfCount = layer.GetFeatureCount(0);
            var templateData = new List<Dictionary<string, object>>();

            Feature f = layer.GetNextFeature();

            while (f != null)
            {
                var dataHolder = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
                var geom = f.GetGeometryRef();
                dataHolder.Add(PropertyKeys.geometryKey, geom);
                var fieldCount = f.GetFieldCount();
                for (int j = 0; j < fieldCount; j++)
                {
                    var fref = f.GetFieldDefnRef(j);
                    var fType = fref.GetFieldType();
                    var fName = fref.GetName();

                    switch (fType)
                    {
                        case FieldType.OFTInteger:
                            dataHolder.Add(fName, f.GetFieldAsInteger(j));
                            break;
                        case FieldType.OFTReal:
                            dataHolder.Add(fName, f.GetFieldAsDouble(j));
                            break;
                        case FieldType.OFTString:
                        case FieldType.OFTWideString:
                            dataHolder.Add(fName, f.GetFieldAsString(j));
                            break;
                    }
                    
                }
                templateData.Add(dataHolder);
                f = layer.GetNextFeature();
            }

        }
    }
}