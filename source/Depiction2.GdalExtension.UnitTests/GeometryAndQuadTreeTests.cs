﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Collections;
using Depiction2.Core.Entities.Element;
using Depiction2.Core.StoryEntities;
using Depiction2.GdalExtension.GeometryItems;
using NUnit.Framework;

namespace Depiction2.GdalExtension.UnitTests
{
    [TestFixture]
    public class GeometryAndQuadTreeTests
    {
        [Test]
        public void GeometryAndQuadTest()
        {
            var quadTree = new ModQuadTree<IElement>();
            var elem = new DepictionElement("Genenr");
            elem.UpdatePrimaryPointAndGeometry(new Point(1, 1), new GeometryGdal(new Point(1, 1)));
            quadTree.AddElements(new List<IElement> { elem });
            var extent = quadTree.Extent;

            var elem1 = new DepictionElement("Second");
            elem1.UpdatePrimaryPointAndGeometry(new Point(3, 3), new GeometryGdal(new Point(3, 3)));
            quadTree.AddElements(new List<IElement> { elem1 });
            extent = quadTree.Extent;
            //             var elem2 = new DepictionElement("Thirds");
            //             elem2.UpdatePrimaryPointAndGeometry(new Point(0,0),new GeometryGdal(new List<Point>{new Point(1,1),new Point(1,-1),new Point(-1,-1), new Point(-1,1), new Point(1,1)}) );
            //             quadTree.AddElements(new List<IElement> { elem2 });

            extent = quadTree.Extent;
            var allItems = quadTree.GetItemsIntersecting(new Rect(-10, -10, 20, 20));
            Assert.AreEqual(2, allItems.Count());
            var geom =
                new GeometryGdal(new List<Point>
                                      {
                                          new Point(10, 10),
                                          new Point(10, -10),
                                          new Point(-10, -10),
                                          new Point(-10, 10),
                                          new Point(10, 10)
                                      });
            var simpleBounds = geom.Bounds;
            var advancedItems = quadTree.GetItemsIntersecting(simpleBounds);
            Assert.AreEqual(2, advancedItems.Count());
        }
        [Test]
        [Ignore("This is used to track down bad area finds")]
        public void GeometryStorySanityTest()
        {
            var quadTree = new ModQuadTree<IElement>();

            var elem = new DepictionElement("Generic");
            var geoPoint = new Point(-122.328, 47.601);
            elem.UpdatePrimaryPointAndGeometry(geoPoint, new GeometryGdal(geoPoint));
            //            var badPoint = new Point(0, 0);
            //            elem.UpdatePrimaryPointAndGeometry(badPoint,new GeometryGdal(badPoint));
            var areaGeom =
            new GeometryGdal(
                "POLYGON ((-122.33756430896599 47.6087271362845,-122.32436341826001 47.6087271362845,-122.32436341826001 47.599765063695003,-122.33756430896599 47.599765063695003,-122.33756430896599 47.6087271362845))"
                );
            //TODO ok so the quad tree stores items with no geo coords, pretty sure this shoyuldnt[ happen, but for now im
            //ignoring.
            quadTree.AddElements(new List<IElement> { elem });

            var within = areaGeom.Contains(elem.ElementGeometry);
            
            var simpleBounds = areaGeom.Bounds;
            within = simpleBounds.Contains(geoPoint);
            
            var cartBounds = areaGeom.RectBounds;
            within = cartBounds.ContainsPoint(geoPoint);


            var allItems = quadTree.GetItemsInside(cartBounds.WpfRect);
            Assert.AreEqual(1, allItems.Count());
            var fj = quadTree.GetItemsIntersecting(simpleBounds);
            Assert.AreEqual(1,fj.Count());
        }

        [Test]
        public void GeometryAndStoryTests()
        {
            var story = new Story(null);
            var areaGeom =
                new GeometryGdal(
                    "POLYGON ((-122.33756430896599 47.6087271362845,-122.32436341826001 47.6087271362845,-122.32436341826001 47.599765063695003,-122.33756430896599 47.599765063695003,-122.33756430896599 47.6087271362845))"
                    );
            var geoPoint = new Point(-122.328, 47.601);
            var elem = new DepictionElement("Generic");

            elem.UpdatePrimaryPointAndGeometry(geoPoint, new GeometryGdal(geoPoint));
            story.AddElement(elem);
            var overlap = story.GetElementsInGeometry(areaGeom);
            Assert.AreEqual(1, overlap.Count());
        }
    }
}