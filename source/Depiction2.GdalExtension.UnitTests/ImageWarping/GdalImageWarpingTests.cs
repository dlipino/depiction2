﻿using System.IO;
using Depiction2.API.Service;
using Depiction2.GdalExtension.EnvironmentSetters;
using NUnit.Framework;
using OSGeo.GDAL;
using OSGeo.OGR;
using OSGeo.OSR;
namespace Depiction2.GdalExtension.UnitTests.ImageWarping
{
    [TestFixture]
    public class GdalImageWarpingTests
    {
        private DepictionFolderService storageFolder;
        [SetUp]
        protected void Setup()
        {
            GDALEnvironment.SetGDALEnvironment();
            OGREnvironment.SetOGREnvironment();
            storageFolder = new DepictionFolderService(true);
        }

        [TearDown]
        protected void TearDown()
        {
            OGREnvironment.UnsetOGREnvironment();
            GDALEnvironment.UnsetGdalEnvironment();
            storageFolder.Dispose();
        }
//         adfGeoTransform[0] /* top left x */
//    adfGeoTransform[1] /* w-e pixel resolution */
//    adfGeoTransform[2] /* 0 */
//    adfGeoTransform[3] /* top left y */
//    adfGeoTransform[4] /* 0 */
//    adfGeoTransform[5] /* n-s pixel resolution (negative value) */
        [Test]
        [Ignore]
        public void SimpleWarp()
        {
            var origImage = TestUtilities.GetFullFileName("geolocated.tif", "Images");

            var srcData = Gdal.Open(origImage, Access.GA_ReadOnly);
            var srcProj = srcData.GetProjection();
            var srcSrs = new SpatialReference(srcProj);
            var srcCoords = new double[6];
            srcData.GetGeoTransform(srcCoords);
            var srcWidth = srcData.RasterXSize;
            var srcHeight = srcData.RasterYSize;
            var srcWidthPixRes = srcCoords[1];
            var srcHeightPixRes = srcCoords[5];
            var srcXLeft = srcCoords[0];
            var srcYTop = srcCoords[3];
            var srcXRight = srcXLeft + (srcWidth*srcWidthPixRes);
            var srcYBot = srcYTop + (srcHeight*srcHeightPixRes);
//            [0] = -122.36085
//            [1] = 0.000026812499999998944
//            [2] = 0.0
//            [3] = 47.61819
//            [4] = 0.0
//            [5] = -0.000027174999999998543
            var srcType = srcData.GetRasterBand(1).DataType;
            var dstSrs = new SpatialReference("");
            dstSrs.SetUTM(10,1);//0 is southern, 1 is northern
            dstSrs.SetWellKnownGeogCS("WGS84");
            var dstSrsWkt = string.Empty;
            dstSrs.ExportToWkt(out dstSrsWkt);

            var coordTrans = new CoordinateTransformation(srcSrs, dstSrs);
            var dstTransTL = new double[3];
            coordTrans.TransformPoint(dstTransTL, srcXLeft, srcYTop, 0);
             var dstTransBR = new double[3];
            coordTrans.TransformPoint(dstTransBR, srcXRight, srcYBot, 0);
        
            var dstXLeft = dstTransTL[0];
            var dstYTop = dstTransTL[1];
            var dstXRight = dstTransBR[0];
            var dstYBot = dstTransBR[1];
            var dstWidth = srcData.RasterXSize/2;
            var dstHeight = srcData.RasterYSize/2;
            var dstWidthPixRes = (dstXRight-dstXLeft) / dstWidth;
            var dstHeightPixRes = (dstYBot-dstYTop) / dstHeight;
            var dstData = Gdal.GetDriverByName("MEM").Create("", dstWidth, dstHeight, 3, srcType, null);
            dstData.SetProjection(dstSrsWkt);
            dstData.SetGeoTransform(new[] {dstXLeft, dstWidthPixRes, 0, dstYTop, 0, dstHeightPixRes});

            var err = Gdal.ReprojectImage(srcData, dstData, srcProj, dstSrsWkt, ResampleAlg.GRA_NearestNeighbour, .125, .125, null, null);

            var destImageFileName = Path.Combine(storageFolder.FolderName, "out.png");
            var outDriver = Gdal.GetDriverByName("PNG");
            var pngData = outDriver.CreateCopy(destImageFileName, dstData, 0, new string[0], null, null);
            dstData.Dispose();
            pngData.Dispose();
            srcData.Dispose();
        }
    }
}