﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction2.Core.ViewModels.EditingViewModels;

namespace Depiction2.GeometryEditor.RegionCreation
{
    public class RegionCreationControl : Control
    {
        private bool eventsAttached = false;
        static RegionCreationControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RegionCreationControl), new FrameworkPropertyMetadata(typeof(RegionCreationControl)));
        }
        public RegionCreationControl()
        {
            //How do un hook these things?
            IsVisibleChanged += RegionSelectorContentControl_IsVisibleChanged;
        }

        public void AcceptRegionExtents()
        {
            var dc = DataContext as GeometryEditCanvasViewModel;
            if (dc == null) return;
            var rect = new Rect(new Point(Canvas.GetLeft(this), Canvas.GetTop(this)), new Size(Width, Height));
            dc.CompleteRegionCreation(rect);
        }

        private void MainWindow_KeyEvent(object sender, KeyEventArgs e)
        {
            if (IsVisible)
            {
                if (e.Key.Equals(Key.LeftShift) || e.Key.Equals(Key.RightShift))
                {
                    if (e.IsDown)
                    {
                        IsHitTestVisible = false;
                    }
                    if (e.IsUp)
                    {
                        IsHitTestVisible = true;
                    }
                }
            }
            else
            {
                e.Handled = false;
            }
        }

        void RegionSelectorContentControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!e.NewValue.Equals(true))
            {
                DettachMainWindowKeyboardEvents();
                return;
            }
            AttachMainWindowKeyboardEvents();
        }

        protected void AttachMainWindowKeyboardEvents()
        {
            if (Application.Current != null && Application.Current.MainWindow != null && !eventsAttached)
            {
                Application.Current.MainWindow.KeyDown += MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp += MainWindow_KeyEvent;
                eventsAttached = true;
            }
        }

        protected void DettachMainWindowKeyboardEvents()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                Application.Current.MainWindow.KeyDown -= MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp -= MainWindow_KeyEvent;
                eventsAttached = false;
            }
        }
    }
}
