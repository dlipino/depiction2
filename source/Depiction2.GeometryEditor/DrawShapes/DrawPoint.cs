﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Depiction2.GeometryEditor.DrawShapes
{
    public class DrawPoint : IEditableShape, IMovable
    {
        private Point _location;
        private IList<IEditableShape> _partOf;
        public int ZIndex
        {
            get { return 3; }
        }
        public bool IsEnabled { get; set; }

        public SolidColorBrush FillColor { get; set; }
        public SolidColorBrush StrokeColor { get; set; }
        public bool ShowVertex { get; set; }
        public IList<IEditableShape> ShapeParts
        {
             get
            {
                return new List<IEditableShape>{this};
            }
        }

        public IList<DrawPoint> ShapeVertices
        {
            get
            {
                return new List<DrawPoint>{this};
            }
            set
            {
                //Does nothing
            }
        }
        public IList<IEditableShape> PartOf
        {
            get
            {
                if (_partOf == null)
                {
                    _partOf = new List<IEditableShape>();
                }
                return _partOf;
            }
        }
       
        public Point RefCoordinate { get { return _location; } }
        public Point Location
        {
            get { return _location; }
        }

        #region constructor
        public DrawPoint(Point location)
        {
            _location = location;
            FillColor = new SolidColorBrush(Colors.Yellow);
            StrokeColor = new SolidColorBrush(Colors.Black);
            ShowVertex = false;

        }
        #endregion
        public void MoveTo(Point position)
        {
            _location = position;
            UpdateVisual();
        }

        public bool AddVertex(DrawPoint vertex)
        {
            //Does nothing
            return false;
        }
        public bool InsertVertexAfter(DrawPoint after, DrawPoint vertex)
        {
            return false;
        }
        public bool RemoveVertex(DrawPoint vertex)
        {
            return false;
        }
        public bool CompleteShape()
        {
            return true;
        }

        public IEditableShape HitTest(Point point)
        {
            double tolerance = GeometryMath.CursorTolerance + visualSize;
            if (point.X >= _location.X - tolerance
                && point.X <= _location.X + tolerance
                && point.Y >= _location.Y - tolerance
                && point.Y <= _location.Y + tolerance)
            {
                return this;
            }
            return null;
        }

        public void UpdateVisual()
        {
            Canvas.SetLeft(ellipse, _location.X - ellipse.Width / 2);
            Canvas.SetTop(ellipse, _location.Y - ellipse.Height / 2);
        }
        public void UpdateIncompleteVisual(Point location)
        {
            _location = location;
            UpdateVisual();
        }
        private int visualSize = 8;
        private Ellipse ellipse = null;
        public void AddVisual(Canvas parent)
        {
            if (parent == null) return;
            ellipse = new Ellipse
            {
                Width = visualSize,
                Height = visualSize,
                Fill = FillColor,
                Stroke = StrokeColor,
                StrokeThickness = 0.5
            };
            UpdateVisual();
            parent.Children.Add(ellipse);
        }
        public void RemoveVisual(Canvas parent)
        {
            if (parent == null) return;
            parent.Children.Remove(ellipse);
        }
    }
}