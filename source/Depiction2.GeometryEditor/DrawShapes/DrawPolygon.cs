﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Depiction2.GeometryEditor.DrawShapes
{
    public class DrawPolygon : IEditableShape
    {
        private DrawPoint tempPoint = null;
        protected DrawPoint[] vertices;
        protected Point[] vertexCoordinates;
        protected readonly ObservableCollection<DrawPoint> _shapeVertices = new ObservableCollection<DrawPoint>();
        private IList<IEditableShape> _partOf;
        private IList<IEditableShape> _parts = new List<IEditableShape>();

        public IList<IEditableShape> ShapeParts
        {
            get { return _parts; }
        }
        public IList<DrawPoint> ShapeVertices
        {
            get
            {
                return _shapeVertices;
            }
            set
            {
                //                suppressDependencyListChangeNotification = true;
                try
                {
                    _shapeVertices.Clear();
                    if (value == null || value.Count == 0)
                    {
                        return;
                    }
                    foreach (var item in value)
                    {
                        _shapeVertices.Add(item);
                    }
                }
                finally
                {
                    //                    suppressDependencyListChangeNotification = false;
                    //                    OnDependenciesChanged();
                }
            }
        }

        public SolidColorBrush FillColor { get; set; }
        public SolidColorBrush StrokeColor { get; set; }
        public bool ShowVertex { get; set; }

        public IList<IEditableShape> PartOf
        {
            get
            {
                if (_partOf == null)
                {
                    _partOf = new List<IEditableShape>();
                }
                return _partOf;
            }
        }

        public int ZIndex
        {
            get { return 1; }
        }

        public Point RefCoordinate
        {
            get { return _shapeVertices[0].Location; }
        }

        public bool IsEnabled { get; set; }

        #region construcfto

        public DrawPolygon()
        {
            _shapeVertices.CollectionChanged += ShapeVerticesCollectionChanged;
            FillColor = Brushes.Blue;
            StrokeColor = Brushes.Black;
            ShowVertex = true;
        }

        void ShapeVerticesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdatePointCache();//not sure why this is colled
            UpdateVisual();
        }
        #endregion

        public bool AddVertex(DrawPoint vertex)
        {
            _shapeVertices.Add(vertex);
            if (drawCanvas != null) vertex.AddVisual(drawCanvas);
            return true;
        }

        public bool InsertVertexAfter(DrawPoint after, DrawPoint vertex)
        {
            //Clear out old things
            foreach (var vert in _shapeVertices)
            {
                vert.PartOf.Clear();
            }

            var insertIndex = _shapeVertices.IndexOf(after);
            if (insertIndex + 1 == _shapeVertices.Count)
            {
                _shapeVertices.Add(vertex);
            }
            else
            {
                _shapeVertices.Insert(insertIndex + 1, vertex);
            }

            CompleteShape();
            if (drawCanvas != null) vertex.AddVisual(drawCanvas);
            return true;
        }

        public bool RemoveVertex(DrawPoint vertex)
        {
            //need to return all the figures that are no longer used
            //ie the ones that the removed vertex was part of
            //then stuff needs to update
            var taken = _shapeVertices.Remove(vertex);
            if (taken)
            {
                vertex.RemoveVisual(drawCanvas);
                vertex.PartOf.Clear();
            }
            //Because im lazy, instead of being smart and removing etc
            //will just recreate the hit testable parts of the polygon
            //clean house first
            foreach (var editableShape in _parts)
            {
                editableShape.PartOf.Clear();

            }
            CompleteShape();

            return taken;
        }
        public bool CompleteShape()
        {
            _parts = new List<IEditableShape>();
            if (_shapeVertices.Count < 3)
            {
                //do something useful
                return false;
            }
            _parts.Add(this);
            //create parts of this thing and connect them to the correct items
            for (var i = 0; i < _shapeVertices.Count; i++)
            {
                var vert1 = _shapeVertices[i];
                DrawPoint vert2 = null;
                if (i + 1 == _shapeVertices.Count)
                {
                    vert2 = _shapeVertices[0];
                }
                else
                {
                    vert2 = _shapeVertices[i + 1];
                }
                var edge = new DrawPolyline();
                edge.AddVertex(vert1);
                edge.AddVertex(vert2);

                vert1.PartOf.Add(edge);
                vert2.PartOf.Add(edge);
                vert1.PartOf.Add(this);

                _parts.Add(vert1);
                _parts.Add(edge);
            }
            return true;
        }

        public IEditableShape HitTest(Point point)
        {
            var points = ShapeVertices.Select(p => p.Location).ToArray();
            if (points.IsPointInPolygon(point))
            {
                return this;
            }
            return null;
        }

        private Polygon shape;
        private Canvas drawCanvas;

        public void UpdateIncompleteVisual(Point endPoint)
        {
            if (vertices == null)
            {
                UpdatePointCache();
            }
            if (!endPoint.Equals(new Point(double.NaN, double.NaN)))
            {
                if (tempPoint == null)
                {
                    tempPoint = new DrawPoint(endPoint);
                    AddVertex(tempPoint);
                }
                else
                {
                    //move the temp point location to the last position
                    tempPoint.MoveTo(endPoint);
                    _shapeVertices.Remove(tempPoint);
                    _shapeVertices.Add(tempPoint);
                }
            }

            PointCollection drawnCollection = null;
            if (shape != null) drawnCollection = shape.Points;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertexCoordinates[i] = vertices[i].Location;
                if (drawnCollection != null) drawnCollection[i] = vertexCoordinates[i];
            }

        }

        public void UpdateVisual()
        {
            if (vertices == null)
            {
                UpdatePointCache();
            }

            PointCollection drawnCollection = null;
            if (shape != null) drawnCollection = shape.Points;

            for (int i = 0; i < vertices.Length; i++)
            {
                vertexCoordinates[i] = vertices[i].Location;
                if (drawnCollection != null) drawnCollection[i] = vertexCoordinates[i];
            }
        }
        //Set the point collection to the correct size of points
        //but with all the values set to zero, must call update visual 
        //after this
        private void UpdatePointCache()
        {
            vertices = ShapeVertices.ToArray();
            vertexCoordinates = new Point[vertices.Length];
            var cache = new PointCollection();

            if (shape != null) shape.Points = cache;

            for (int i = 0; i < vertices.Length; i++)
            {
                cache.Add(new Point());
            }
        }
        public void AddVisual(Canvas parent)
        {
            if (parent == null) return;
            shape = new Polygon
            {
                Stroke = StrokeColor,
                StrokeThickness = 1,
                FillRule = FillRule.EvenOdd,
                Fill = FillColor,
                Opacity = .5
            };
            UpdateVisual();
            parent.Children.Add(shape);
            drawCanvas = parent;
        }
        public void RemoveVisual(Canvas parent)
        {
            if (parent == null) return;
            parent.Children.Remove(shape);
            //Remove vertex points
            foreach (var p in _shapeVertices)
            {
                p.RemoveVisual(parent);
            }
        }
    }
}