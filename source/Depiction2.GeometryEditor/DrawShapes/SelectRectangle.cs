﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Depiction2.GeometryEditor.DrawShapes
{
    public class SelectRectangle : IEditableShape
    {
        private Path rectangleSelectPath;
        private Rect selectionRect;
        private Point startPoint = new Point(double.NaN,double.NaN);
        private ObservableCollection<DrawPoint> _shapeVertices;
        public IList<IEditableShape> PartOf { get; private set; }
        public IList<IEditableShape> ShapeParts { get; private set; }
        public IList<DrawPoint> ShapeVertices { get { return _shapeVertices; }  }
        public Point RefCoordinate { get; private set; }
        public int ZIndex { get; private set; }
        public bool IsEnabled { get; set; }
        public SolidColorBrush FillColor { get; set; }
        public SolidColorBrush StrokeColor { get; set; }
        public bool ShowVertex { get; set; }

        #region constructor
        public SelectRectangle()
        {
            _shapeVertices = new ObservableCollection<DrawPoint>();
            FillColor = Brushes.AntiqueWhite;
            StrokeColor = Brushes.Orange;
            ShowVertex = false;
        }

        #endregion
        public bool AddVertex(DrawPoint vertex)
        {
            if (startPoint.X.Equals(double.NaN))
            {
                startPoint = vertex.Location;
                selectionRect = new Rect(startPoint, startPoint);
                rectangleSelectPath.Data = new RectangleGeometry(selectionRect);
            }else
            {
                //hacky
                CompleteShape();//use the selectrect to create the _shapevertices
            }
            
            return true;
        }

        public bool InsertVertexAfter(DrawPoint after, DrawPoint vertex)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveVertex(DrawPoint vertex)
        {
            throw new System.NotImplementedException();
        }

        public bool CompleteShape()
        {
            var vertList = new List<Point>
                               {
                                   selectionRect.TopLeft,
                                   selectionRect.TopRight,
                                   selectionRect.BottomRight,
                                   selectionRect.BottomLeft
                               };
            foreach(var point in vertList)
            {
                _shapeVertices.Add(new DrawPoint(point));
            }
            return true;
        }

        public IEditableShape HitTest(Point point)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateVisual()
        {
            throw new System.NotImplementedException();
        }
        public void UpdateIncompleteVisual(Point currentPoint)
        {
            if(_shapeVertices.Count == 4)
            {
                //the shape is complete
                return;
            }
            if (!startPoint.X.Equals(double.NaN))
            {
                var width = currentPoint.X - startPoint.X;
                var height = currentPoint.Y - startPoint.Y;
                if (width < 0)
                {
                    selectionRect.X = startPoint.X + width;
                    selectionRect.Width = -width;
                }
                else
                {
                    selectionRect.X = startPoint.X;
                    selectionRect.Width = width;
                }
                if (height < 0)
                {
                    selectionRect.Y = startPoint.Y + height;
                    selectionRect.Height = -height;
                }
                else
                {
                    selectionRect.Y = startPoint.Y;
                    selectionRect.Height = height;
                }
                var rectGeom = rectangleSelectPath.Data as RectangleGeometry;
                if (rectGeom != null)
                {
                    rectGeom.Rect = selectionRect;
                }
            }
        }
        public void AddVisual(Canvas parent)
        {
            rectangleSelectPath = new Path();
            rectangleSelectPath.Fill = FillColor;
            rectangleSelectPath.StrokeThickness = 1;
            rectangleSelectPath.IsHitTestVisible = false;
            rectangleSelectPath.Opacity = .5;
            rectangleSelectPath.Stroke = StrokeColor;
//            rectangleSelectPath.Data = new RectangleGeometry(new Rect(50, 60, 300, 230));
            parent.Children.Add(rectangleSelectPath);
            //      <Path x:Name="AreaSelectionRectangle" Fill="AntiqueWhite" Stroke="BurlyWood" StrokeThickness="1" Opacity=".5" IsHitTestVisible="False"/>
        }

        public void RemoveVisual(Canvas parent)
        {
            parent.Children.Remove(rectangleSelectPath);
        }
    }
}