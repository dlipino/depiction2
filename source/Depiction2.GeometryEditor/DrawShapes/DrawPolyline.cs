﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Depiction2.GeometryEditor.DrawShapes
{
    public class DrawPolyline : IEditableShape
    {
        private DrawPoint tempPoint = null;
        protected DrawPoint[] vertices;
        protected Point[] vertexCoordinates;
        protected readonly ObservableCollection<DrawPoint> _shapeVertices = new ObservableCollection<DrawPoint>();
        private IList<IEditableShape> _partOf;
        private IList<IEditableShape> _parts = new List<IEditableShape>();

        public SolidColorBrush FillColor { get; set; }
        public SolidColorBrush StrokeColor { get; set; }
        public bool ShowVertex { get; set; }

        public IList<DrawPoint> ShapeVertices
        {
            get
            {
                return _shapeVertices;
            }
            set
            {
                //                suppressDependencyListChangeNotification = true;
                try
                {
                    _shapeVertices.Clear();
                    if (value == null || value.Count == 0)
                    {
                        return;
                    }
                    foreach (var item in value)
                    {
                        _shapeVertices.Add(item);
                    }
                }
                finally
                {
                    //                    suppressDependencyListChangeNotification = false;
                    //                    OnDependenciesChanged();
                }
            }
        }
        public IList<IEditableShape> ShapeParts
        {
            get { return _parts; }
        }
        public IList<IEditableShape> PartOf
        {
            get
            {
                if (_partOf == null)
                {
                    _partOf = new List<IEditableShape>();
                }
                return _partOf;
            }
        }

        public int ZIndex
        {
            get { return 2; }
        }

        public Point RefCoordinate
        {
            get { return _shapeVertices[0].Location; }
        }

        public bool IsEnabled { get; set; }

        #region construcfto

        public DrawPolyline()
        {
            _shapeVertices.CollectionChanged += ShapeVerticesCollectionChanged;
            ShowVertex = false;
            StrokeColor = Brushes.Black;
            FillColor = Brushes.Transparent;

        }

        void ShapeVerticesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UpdatePointCache();
            UpdateVisual();
        }
        #endregion

        public bool AddVertex(DrawPoint vertex)
        {
            _shapeVertices.Add(vertex);
            if (drawCanvas != null) vertex.AddVisual(drawCanvas);
            return true;
        }
        public bool InsertVertexAfter(DrawPoint after, DrawPoint vertex)
        {
            var insertIndex = _shapeVertices.IndexOf(after);
            if (insertIndex + 1 == _shapeVertices.Count)
            {
                _shapeVertices.Add(vertex);
            }
            else
            {
                _shapeVertices.Insert(insertIndex + 1, vertex);
            } 
            foreach (var vert in _shapeVertices)
            {
                vert.PartOf.Clear();
            }
            CompleteShape();
            if (drawCanvas != null) vertex.AddVisual(drawCanvas);
            return true;
        }
        
        public bool RemoveVertex(DrawPoint vertex)
        {
            var taken = _shapeVertices.Remove(vertex);
            if (taken)
            {
                vertex.RemoveVisual(drawCanvas);
            }
            return taken;
        }
        public bool CompleteShape()
        {
            _parts.Clear();
            if (_shapeVertices.Count < 2)
            {
                //do something useful
                return false;
            }
            _parts.Add(this);
            //create parts of this thing and connect them to the correct items
            foreach (var vert in _shapeVertices)
            {
                vert.PartOf.Add(this);
                _parts.Add(vert);
            }
            return true;
        }

        public IEditableShape HitTest(Point point)
        {
            //            double epsilon = ToLogical(Shape.StrokeThickness / 2 + Math.CursorTolerance);
            var epsilon = 1 + GeometryMath.CursorTolerance;
            var points = ShapeVertices.Select(p => p.Location).ToArray();
            if (GeometryMath.IsPointOnPolygonalChain(points, point, epsilon, false))
            {
                return this;
            }
            return null;
        }

        private Polyline shape;
        private Canvas drawCanvas;
        public void UpdateIncompleteVisual(Point endPoint)
        {
            if (vertices == null)
            {
                UpdatePointCache();
            }
            if (tempPoint == null)
            {
                tempPoint = new DrawPoint(endPoint);
                AddVertex(tempPoint);
            }
            else
            {
                //move the temp point location to the last position
                tempPoint.MoveTo(endPoint);
                _shapeVertices.Remove(tempPoint);
                _shapeVertices.Add(tempPoint);
            }

            PointCollection drawnCollection = null;
            if (shape != null) drawnCollection = shape.Points;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertexCoordinates[i] = vertices[i].Location;
                if (drawnCollection != null) drawnCollection[i] = vertexCoordinates[i];
            }
        }
        public void UpdateVisual()
        {
            if (vertices == null)
            {
                UpdatePointCache();
            }

            PointCollection points = null;
            if (shape != null)
            {
                points = shape.Points;
            }
            for (int i = 0; i < vertices.Length; i++)
            {
                vertexCoordinates[i] = vertices[i].Location;
                if (points != null) points[i] = vertexCoordinates[i];
            }
        }
        //Set the point collection to the correct size of points
        //but with all the values set to zero, must call update visual 
        //after this
        private void UpdatePointCache()
        {
            vertices = ShapeVertices.ToArray();
            vertexCoordinates = new Point[vertices.Length];
            var cache = new PointCollection();
            if (shape != null) shape.Points = cache;

            for (int i = 0; i < vertices.Length; i++)
            {
                cache.Add(new Point());
            }
        }
        public void AddVisual(Canvas parent)
        {

            if (parent == null) return;
            shape = new Polyline
            {
                Stroke = StrokeColor,
                StrokeThickness = 1,
            };
            UpdateVisual();
            parent.Children.Add(shape);
            drawCanvas = parent;
        }
        public void RemoveVisual(Canvas parent)
        {
            if (parent == null) return;
            parent.Children.Remove(shape);
            //Remove vertex points
            foreach (var p in _shapeVertices)
            {
                p.RemoveVisual(parent);
            }
        }

        #region static helpers
        static public DrawPoint FindVertBeforePoint(DrawPolyline polyline, Point p)
        {
            DrawPoint vertBefore = null;
            if (polyline.ShapeVertices.Count == 2)
            {
                vertBefore = polyline.ShapeVertices[0];
            }
            else
            {
                var epsilon = 1 + GeometryMath.CursorTolerance;
                for (int i = 0; i < polyline.ShapeVertices.Count - 1; i++)
                {
                    var points = new List<Point> { polyline.ShapeVertices[i].Location, polyline.ShapeVertices[i + 1].Location };
                    if (GeometryMath.IsPointOnPolygonalChain(points, p, epsilon, false))
                    {
                        vertBefore = polyline.ShapeVertices[i];
                        break;
                    }
                }
            }
            return vertBefore;
        }
        #endregion
    }
}