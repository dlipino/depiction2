﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.EditingViewModels;
using Depiction2.Core.ViewModels.Helpers;
using Depiction2.GeometryEditor.DrawShapes;

namespace Depiction2.GeometryEditor
{
    //Single edit, possilby for multiple add
    public class GeometryEditCanvas : Canvas
    {
        GeometryEditMode _canvasMode = GeometryEditMode.SingleAdd;
        GeometryType _geometryType = GeometryType.Polygon;
        private List<IEditableShape> _multShapeList = null; 

        private IEditableShape editShapePart = null;
        private IEditableShape activeShape = null;


        Point oldLocation = new Point();
        List<IMovable> movables = new List<IMovable>();
        private Point mouseDownLocation;

        bool startedMoving = false;

        public GeometryEditCanvas()
        {
            MouseLeftButtonDown += GeometryEditCanvas_MouseLeftButtonDown;
            MouseLeftButtonUp += GeometryEditCanvas_MouseLeftButtonUp;
            MouseMove += GeometryEditCanvas_MouseMove;
            MouseRightButtonUp += GeometryEditCanvas_MouseRightButtonUp;
            _canvasMode = GeometryEditMode.SingleAdd;
            Background = Brushes.Transparent;
        }

        #region helpers

        public void StartElementAreaSelection(GeometryType type)
        {
            _geometryType = type;
            _canvasMode = GeometryEditMode.AreaElementSelection;
        }

        public void CompleteElementAreaSelection()
        {
            var dc = DataContext as GeometryEditCanvasViewModel;
            if (dc == null) return;
            if (activeShape == null) return;

            var mapVerts = ConvertActiveShapeVertsToMapPoints(activeShape.ShapeVertices);
            dc.CompleteAreaElementSelection(mapVerts);
            dc.EditCanvasMode = GeometryEditMode.None;
        }

        public void StartEditMode(List<Point> pcsPoints)
        {
            if (pcsPoints == null || pcsPoints.Count < 2) return;
            if (GeoToDrawLocationService.CartCanvasToScreen == null) return;
            var fp = new Point();
            var fpSet = false;
            var convList = new List<Point>();
            foreach (var p in pcsPoints)
            {
                var conv = GeoToDrawLocationService.CartCanvasToScreen(p);
                if (!fpSet)
                {
                    fp = conv;
                    fpSet = true;
                }
                convList.Add(conv);
            }

            if (Equals(fp, convList[convList.Count - 1]))
            {
                convList.RemoveAt(convList.Count - 1);
                _geometryType = GeometryType.Polygon;
                activeShape = new DrawPolygon();
            }
            else
            {
                _geometryType = GeometryType.Polyline;
                activeShape = new DrawPolyline();
            }
            activeShape.AddVisual(this);
            //TODO unit test this form of create
            foreach (var p in convList)
            {
                activeShape.AddVertex(new DrawPoint(p));
            }
            activeShape.CompleteShape();
            _canvasMode = GeometryEditMode.Edit;
        }

        public void StartSingleAddMode(GeometryType type)
        {
            _canvasMode = GeometryEditMode.SingleAdd;
            _geometryType = type;
        }

        public void StartMultiAddMode(GeometryType type)
        {
            _canvasMode = GeometryEditMode.MultipleAdd;
            _multShapeList = new List<IEditableShape>();
            _geometryType = type;
        }
        internal void SendGeometryEditPointsToViewModelAndEndEditMode()
        {
            var dc = DataContext as GeometryEditCanvasViewModel;
            if (dc == null) return; 
            var rawMapPoints = ConvertActiveShapeVertsToMapPoints(activeShape.ShapeVertices);
            dc.CompleteEditGeometryOrRegionElementSelect(rawMapPoints);
            dc.EditCanvasMode = GeometryEditMode.None;
        }
        //        internal void SendActiveShapeMapPointsToViewModel(List<Point> mapCanvasPoints)
        //        {
        //            var dc = DataContext as GeometryEditCanvasViewModel;
        //            if (dc == null) return;
        //
        ////            if (mapCanvasPoints == null)//not sure if we want to end this
        ////            {
        ////                dc.EditCanvasMode = GeometryEditMode.None;
        ////                return;
        ////            }
        //
        //            dc.CompleteEditGeometryOrRegionElementSelect(mapCanvasPoints);
        ////            dc.EditCanvasMode = GeometryEditMode.None;//doesnt say it ends the draw mode
        //        }
        public void ClearCanvas()
        {
            _canvasMode = GeometryEditMode.None;
            if (activeShape != null)
            {
                activeShape.CompleteShape();//not sure how the temppoint does in this situtation
                activeShape = null;
                Children.Clear();
            }
        }
        public bool IsShiftDown()
        {
            return Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
        }
        public bool IsCtrlDown()
        {
            return Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
        }
        #endregion

        #region mouse event connections
        void GeometryEditCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            editShapePart = null;
            startedMoving = false;
            movables.Clear();
        }
        void GeometryEditCanvas_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var dc = DataContext as GeometryEditCanvasViewModel;
            if (dc == null) return;
            //Deal with the special case that is 
            if (dc.EditCanvasMode.Equals(GeometryEditMode.MultipleAdd))
            {
                if(_multShapeList == null)
                {
                    Children.Clear();
                    e.Handled = true;
                    return;
                }
                var rawMapPointList = new List<List<Point>>(); 
                foreach(var listShape in _multShapeList)
                {
                    var rawPoints = ConvertActiveShapeVertsToMapPoints(listShape.ShapeVertices);
                    rawMapPointList.Add(rawPoints);
                }
                dc.CompleteMultipleElementGeometryAdd(rawMapPointList);
            }
            //Back to the normal stuff
            if (activeShape == null)
            {
                //no active shape means that the user wants to end
                Children.Clear();
                e.Handled = true;
                return;//maybe cancel the editmode?
            }
            var rawMapPoints = ConvertActiveShapeVertsToMapPoints(activeShape.ShapeVertices);

            if (dc.EditCanvasMode.Equals(GeometryEditMode.SingleAdd))
            {
                //pretty sure it should never make it here. single add ends on the left click
                //or at the moment of right click
                dc.CompleteSingleElementGeometryAdd(rawMapPoints);

            }
            else//all that is left is geometry edit or regionelement selection
            {
                dc.CompleteEditGeometryOrRegionElementSelect(rawMapPoints);
                dc.EditCanvasMode = GeometryEditMode.None;
            }
            Children.Clear();
            activeShape = null;
            e.Handled = true;
        }
        void GeometryEditCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            var currentLocation = e.GetPosition(this);

            if (_canvasMode.Equals(GeometryEditMode.Edit))
            {
                if (editShapePart != null)
                {
                    #region editing
                    if (!startedMoving)
                    {
                        if (currentLocation == mouseDownLocation)
                        {
                            return;
                        }
                        startedMoving = true;
                    }
                    //just need the mouse delta
                    var offset = currentLocation - oldLocation;
                    foreach (var move in movables)
                    {
                        move.MoveTo(move.Location + (offset));
                        //find dependents and update
                        var shape = move as IEditableShape;
                        if (shape == null) continue;
                        foreach (var dep in shape.PartOf)
                        {
                            if (!ReferenceEquals(dep, shape))
                            {
                                dep.UpdateVisual();
                            }
                        }
                    }
                    oldLocation = currentLocation;

                    #endregion
                }
            }
            else
            {
                if (activeShape != null)
                {
                    activeShape.UpdateIncompleteVisual(currentLocation);
                }
            }
            e.Handled = true;
        }

        void GeometryEditCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (IsCtrlDown())
            {
                CtrlLeftDownAction(sender, e);
                return;
            }
            if (IsShiftDown())
            {
                ShiftLeftDownAction(sender, e);
                return;
            }
            var p = e.GetPosition(this);
            mouseDownLocation = p;
            startedMoving = false;
            if (_canvasMode.Equals(GeometryEditMode.MultipleAdd) || _canvasMode.Equals(GeometryEditMode.SingleAdd))
            {
                #region goemetry creation
                switch (_geometryType)
                {
                    case GeometryType.Point:
                        
                        var dc = DataContext as GeometryEditCanvasViewModel;
                        if(dc == null)
                        {
                            break;
                        }
                       if(_canvasMode.Equals(GeometryEditMode.SingleAdd))
                       {
                           activeShape = new DrawPoint(p);
                           activeShape.AddVisual(this);
                           var rawMapPoints = ConvertActiveShapeVertsToMapPoints(activeShape.ShapeVertices);
                           dc.CompleteSingleElementGeometryAdd(rawMapPoints);
                       }else if(_canvasMode.Equals(GeometryEditMode.MultipleAdd))
                       {
                           var clickShape = new DrawPoint(p);
                           _multShapeList.Add(clickShape);
//                           activeShape = new DrawPoint(p);
//                           activeShape.AddVisual(this);
                           clickShape.AddVisual(this);
                       }



                        break;
                    case GeometryType.Polyline:
                        if (activeShape == null)
                        {
                            activeShape = new DrawPolyline();
                            activeShape.AddVisual(this);
                        }
                        activeShape.AddVertex(new DrawPoint(p));
                        activeShape.UpdateIncompleteVisual(p);
                        break;
                    case GeometryType.Polygon:
                        if (activeShape == null)
                        {
                            activeShape = new DrawPolygon();
                            activeShape.AddVisual(this);
                        }
                        activeShape.AddVertex(new DrawPoint(p));
                        activeShape.UpdateIncompleteVisual(p);
                        break;

                }
                #endregion
            }
            else if (_canvasMode.Equals(GeometryEditMode.Edit))
            {
                #region geometry edit
                var bestZ = 0;
                if (activeShape == null) return;
                                foreach (var part in activeShape.ShapeParts)
                {
                    var hit = part.HitTest(p);
                    if (hit != null && hit.ZIndex > bestZ)
                    {
                        bestZ = hit.ZIndex;
                        editShapePart = hit;
                    }
                }

                if (editShapePart == null) return;
                var movable = editShapePart as IMovable;

                if (movable == null)//what a drag, find all the movable parts
                {
                    oldLocation = p;
                    movables.AddRange(editShapePart.ShapeVertices);
                }
                else
                {
                    Debug.WriteLine("Made contact");
                    oldLocation = editShapePart.RefCoordinate;
                    //find all the other movalbe parts
                    movables.Add(movable);
                }
                #endregion
            }
            //selecting element using a region rectangle or poly.
            else if (_canvasMode.Equals(GeometryEditMode.AreaElementSelection))
            {
                if (_geometryType.Equals(GeometryType.Rectangle))
                {
                    if (activeShape == null)
                    {
                        activeShape = new SelectRectangle();
                        activeShape.AddVisual(this);
                    }

                    activeShape.AddVertex(new DrawPoint(p));

                    activeShape.UpdateIncompleteVisual(p);
                    if (activeShape.ShapeVertices.Count == 4)
                    {
                        CompleteElementAreaSelection();
                    }
                }
                else if (_geometryType.Equals(GeometryType.Polygon))
                {
                    if (activeShape == null)
                    {
                        activeShape = new DrawPolygon();
                        activeShape.FillColor = Brushes.AntiqueWhite;
                        activeShape.ShowVertex = false;
                        activeShape.AddVisual(this);
                    }
                    activeShape.AddVertex(new DrawPoint(p));

                    activeShape.UpdateIncompleteVisual(p);
                }
            }
        }
        #endregion
        #region action helpers
        private List<Point> ConvertActiveShapeVertsToMapPoints(IList<DrawPoint> geoCanvasPoints)
        {
            if (GeoToDrawLocationService.ScreenToCartCanvas == null) return null;
            //Point size check
            var min = 1;
            switch (_geometryType)
            {
                case GeometryType.Point:
                    min = 1;
                    break;
                case GeometryType.Polyline:
                    min = 2;
                    break;
                case GeometryType.Polygon:
                    min = 3;
                    break;
            }
            if (min > geoCanvasPoints.Count)
            {
                return null;
            }
            var mapVerts = new List<Point>();

            foreach (var p in geoCanvasPoints)
            {
                var cartPoint = GeoToDrawLocationService.ScreenToCartCanvas(p.Location);
                mapVerts.Add(cartPoint);
            }
            if (_geometryType.Equals(GeometryType.Polygon) || _geometryType.Equals(GeometryType.Rectangle))
            {
                mapVerts.Add(mapVerts[0]);
            }
            return mapVerts;
        }

        private void CtrlLeftDownAction(object sender, MouseButtonEventArgs e)
        {
            var p = e.GetPosition(this);
            mouseDownLocation = p;
            startedMoving = false;
            var bestZ = 0;

            foreach (var part in activeShape.ShapeParts)
            {
                var hit = part.HitTest(p);
                if (hit != null && hit.ZIndex > bestZ)
                {
                    bestZ = hit.ZIndex;
                    editShapePart = hit;
                }
            }

            var point = editShapePart as DrawPoint;
            if (point == null || activeShape == null) return;
            //we now that principle shapes do not share vertices, for now.
            activeShape.RemoveVertex(point);

        }

        private void ShiftLeftDownAction(object sender, MouseButtonEventArgs e)
        {
            var p = e.GetPosition(this);
            mouseDownLocation = p;
            var bestZ = 0;

            foreach (var part in activeShape.ShapeParts)
            {
                var hit = part.HitTest(p);
                if (hit != null && hit.ZIndex > bestZ)
                {
                    bestZ = hit.ZIndex;
                    editShapePart = hit;
                }
            }
            if (editShapePart is DrawPoint) return;
            if (editShapePart is DrawPolygon) return;
            if (activeShape == null) return;

            var polyline = editShapePart as DrawPolyline;
            if (polyline == null) return;
            //get the vertex before
            DrawPoint vertBefore = DrawPolyline.FindVertBeforePoint(polyline, p);

            if (vertBefore == null) return;
            activeShape.InsertVertexAfter(vertBefore, new DrawPoint(p));
        }
        #endregion
    }
}