﻿using System.Windows;

namespace Depiction2.GeometryEditor
{
    public interface IMovable
    {
        void MoveTo(Point position);
        Point Location { get; }
    }


}