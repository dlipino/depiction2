﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.Core.ViewModels.EditingViewModels;

namespace Depiction2.GeometryEditor
{
    /// <summary>
    /// Interaction logic for GeometryEditUserControl.xaml
    /// </summary>
    public partial class GeometryEditUserControl
    {
        public GeometryEditUserControl()
        {
            InitializeComponent();
            PreviewMouseWheel += GeometryEditUserControl_PreviewMouseWheel;

            Visibility = Visibility.Collapsed;

            Background = Brushes.Transparent;

            DataContextChanged += GeometryEditCanvas_DataContextChanged;
        }
        void GeometryEditCanvas_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newdc = e.NewValue as GeometryEditCanvasViewModel;
            if (newdc != null)
            {
                newdc.PropertyChanged += genericPropertyChangeHandler;
            }
        }
        //There is an odd semi circular logic because the viewmodel doesn't know what pixel/screen coordinates are getting selected
        void genericPropertyChangeHandler(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (propName.Equals("RequestCompleteGeometryEdit"))
            {
                if (EditCanvas.IsVisible)
                {
                    EditCanvas.SendGeometryEditPointsToViewModelAndEndEditMode();
                }
            }else if(propName.Equals("CompleteRegion"))
            {
                 if (RegionCreateControlCanvas.IsVisible)
                {
                    RegionCreateControl.AcceptRegionExtents();
                }
            }
            else if (propName.Equals("CancelAction"))
            {
                //Same as SetGeometryEditMode.None
            }
            else if (propName.Equals("EditCanvasMode"))
            {
                var dc = DataContext as GeometryEditCanvasViewModel;
                if (dc == null) return;
                if (dc.EditCanvasMode != GeometryEditMode.None)
                {
                    Visibility = Visibility.Visible;
                }
                switch (dc.EditCanvasMode)
                {
                    case GeometryEditMode.Edit:
                        EditCanvas.Visibility = Visibility.Visible;
                        EditCanvas.StartEditMode(dc.EditPoints.ToList());
                        break;
                    case GeometryEditMode.SingleAdd:
                        EditCanvas.Visibility = Visibility.Visible;
                        EditCanvas.StartSingleAddMode(dc.GeometryType);
                        break;
                    case GeometryEditMode.MultipleAdd:
                        EditCanvas.Visibility = Visibility.Visible;
                        EditCanvas.StartMultiAddMode(dc.GeometryType);
                        break;
                    case GeometryEditMode.AreaElementSelection:
                        EditCanvas.Visibility = Visibility.Visible;
                        EditCanvas.StartElementAreaSelection(dc.GeometryType);
                        break;
                    case GeometryEditMode.RegionCreate:
                        RegionCreateControlCanvas.Visibility = Visibility.Visible;
                        var regionBounds = dc.ClosedPointListToRect(dc.EditPoints);
                        Canvas.SetLeft(RegionCreateControl, regionBounds.Left);
                        Canvas.SetTop(RegionCreateControl, regionBounds.Top);
                        RegionCreateControl.Width = regionBounds.Width;
                        RegionCreateControl.Height = regionBounds.Height;

                        break;
                    case GeometryEditMode.None:
                        Visibility = Visibility.Collapsed;
                        EditCanvas.Visibility = Visibility.Collapsed;
                        RegionCreateControlCanvas.Visibility = Visibility.Collapsed;
                        EditCanvas.ClearCanvas();
                        break;
                }
            }
        }
        #region mouse

        private void GeometryEditUserControl_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            //Disables the mouse wheel events so that the map doesnt change under the 
            //geometry that is getting edited
            var dc = DataContext as GeometryEditCanvasViewModel;
            if (dc == null)
            {
                e.Handled = true;
                return;
            }
            if (dc.EditCanvasMode.Equals(GeometryEditMode.RegionCreate) ||
                dc.EditCanvasMode.Equals(GeometryEditMode.SingleAdd)||
                dc.EditCanvasMode.Equals(GeometryEditMode.MultipleAdd))
            {
                e.Handled = false;
                return;
            }

            e.Handled = true;
        }

        #endregion

    }
}
