﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Depiction2.GeometryEditor.DrawShapes;

namespace Depiction2.GeometryEditor
{
    public interface IEditableShape
    {
        SolidColorBrush FillColor { get; set; }
        SolidColorBrush StrokeColor { get; set; }
        bool ShowVertex { get; set; }

        IList<IEditableShape> PartOf { get;  }
        IList<IEditableShape> ShapeParts { get; }
        IList<DrawPoint> ShapeVertices { get;  }

        Point RefCoordinate { get; }
        int ZIndex { get; }
        bool IsEnabled { get; set; }
        
        bool AddVertex(DrawPoint vertex);
        bool InsertVertexAfter(DrawPoint after, DrawPoint vertex);
        bool RemoveVertex(DrawPoint vertex);
        bool CompleteShape();

        IEditableShape HitTest(Point point);//, Predicate<IEditableShape> filter);
        void UpdateIncompleteVisual(Point tempEndPoint);
        void UpdateVisual();
        void AddVisual(Canvas parent);
        void RemoveVisual(Canvas parent);
    }
}