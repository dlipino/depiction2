﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Depiction2.GeometryEditor.DrawShapes;

namespace Depiction2.GeometryEditor
{
    public class GeometryDrawCanvas : Canvas
    {
        private List<IEditableShape> _multShapeList = null;

        #region Constructor

        public GeometryDrawCanvas()
        {
            Background = Brushes.OldLace;
            MouseLeftButtonDown += GeometryEditCanvas_MouseLeftButtonDown;
        }



        #endregion

        private void GeometryEditCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var p = e.GetPosition(this);
            var origin = e.Source;
            var other = e.OriginalSource;
            if(origin is IEditableShape)
            {
                return;
            }
            var pointShape = new DrawPoint(p);
            pointShape.AddVisual(this);
        }

        public void ClearCanvas()
        {
            
        }
    }
}