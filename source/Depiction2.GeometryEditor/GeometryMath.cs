﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Depiction2.GeometryEditor
{//Copy pasted from LiveGeometry
    public class GeometryMath
    {
        static public int CursorTolerance = 8;
        public const double Epsilon = 0.00000001;
        public static Point NaNPoint
        {
            get
            {
                return new Point(double.NaN, double.NaN);
            }
        }
        public struct ProjectionInfo
        {
            public Point Point { get; set; }
            public double Ratio { get; set; }
            public double DistanceToLine { get; set; }

            public bool IsWithinSegment()
            {
                return Ratio >= 0 && Ratio <= 1;
            }
        }
        public static bool IsPointOnPolygonalChain(IList<Point> points, Point point, double epsilon, bool IsClosed)
        {
            var projection = GetProjection(point, points, IsClosed);
            var projectionDistance = projection.DistanceToLine;
            if (projectionDistance < epsilon)
            {
                return true;
            }

            var nearestPoint = GetNearestPoint(point, points);
            var nearestPointDistance = nearestPoint.Distance(point);
            if (nearestPointDistance < epsilon)
            {
                return true;
            }

            return false;
        }
        public static Point GetNearestPoint(Point point, IList<Point> pointList)
        {
            return pointList[GetIndexOfNearestPoint(point, pointList)];
        }
        public static ProjectionInfo GetProjection(Point point, PointPair line)
        {
            Point projectionPoint = GetProjectionPoint(point, line);
            ProjectionInfo result = new ProjectionInfo()
            {
                Point = projectionPoint,
                Ratio = GetProjectionRatio(line, projectionPoint),
                DistanceToLine = projectionPoint.Distance(point)
            };
            return result;
        }
        static double GetProjectionRatio(PointPair line, Point projection)
        {
            var result = 0.0;
            // Need to use epsilon to prevent erroneous result.
            if (!(line.P1.X - line.P2.X).IsWithinEpsilon())
            {
                result = (projection.X - line.P1.X) / (line.P2.X - line.P1.X);
            }
            else if (!(line.P1.Y - line.P2.Y).IsWithinEpsilon())
            {
                result = (projection.Y - line.P1.Y) / (line.P2.Y - line.P1.Y);
            }
            return result;
        }
        public static Point GetProjectionPoint(Point p, PointPair line)
        {
            Point result = new Point();

            if (line.P1.Y.IsWithinEpsilonTo(line.P2.Y))
            {
                if (line.P1.X.IsWithinEpsilonTo(line.P2.X))
                {
                    result = line.P1;
                }
                else
                {
                    result.X = p.X;
                    result.Y = line.P1.Y;
                }
            }
            else if (line.P1.X.IsWithinEpsilonTo(line.P2.X))
            {
                result.X = line.P1.X;
                result.Y = p.Y;
            }
            else
            {
                var a = p.Minus(line.P1).SumOfSquares();
                var b = p.Minus(line.P2).SumOfSquares();
                var c = line.P1.Minus(line.P2).SumOfSquares();

                if (c != 0)
                {
                    var m = (a + c - b) / (2 * c);
                    result = ScalePointBetweenTwo(line.P1, line.P2, m);
                }
                else
                {
                    result = line.P1;
                }
            }

            return result;
        }
        public static Point ScalePointBetweenTwo(Point p1, Point p2, double ratio)
        {
            return new Point(
                p1.X + (p2.X - p1.X) * ratio,
                p1.Y + (p2.Y - p1.Y) * ratio);
        }
        public static ProjectionInfo GetProjection(Point point, IList<Point> polygonalChain, bool IsClosed)
        {
            ProjectionInfo nearestProjection = new ProjectionInfo();
            nearestProjection.DistanceToLine = double.MaxValue;
            var count = polygonalChain.Count;
            var stop = (IsClosed) ? count : count - 1;
            for (int i = 0; i < stop; i++)
            {
                var segment = new PointPair(polygonalChain[i], polygonalChain[i.RotateNext(count)]);
                var projectionInfo = GetProjection(point, segment);
                if ((projectionInfo.DistanceToLine < nearestProjection.DistanceToLine)
                        && projectionInfo.IsWithinSegment())
                {
                    nearestProjection = projectionInfo;
                }
            }

            return nearestProjection;
        }
        public static int GetIndexOfNearestPoint(Point point, IList<Point> pointList)
        {
            double nearest = double.PositiveInfinity;
            Point nearestPoint = new Point();
            int nearestPointIndex = 0;
            for (int i = 0; i < pointList.Count(); i++)
            {
                var currentPoint = pointList[i];
                var currentDistance = point.Distance(currentPoint);
                if (currentDistance < nearest)
                {
                    nearest = currentDistance;
                    nearestPoint = currentPoint;
                    nearestPointIndex = i;
                }
            }

            return nearestPointIndex;
        }
    }

    public static class GeometryHelpExtensions
    {
        /// <summary>
        /// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
        /// </summary>
        public static bool IsPointInPolygon(this IList<Point> polygon, Point start)
        {
            int n = polygon.Count;
            double x = start.X;
            double y = start.Y;
            bool inside = false;
            int i = 0;
            int j = 0;

            for (i = 0, j = n - 1; i < n; j = i++)
            {
                if (((polygon[i].Y > y) != (polygon[j].Y > y)) &&
                 (x < (polygon[j].X - polygon[i].X) * (y - polygon[i].Y)
                    / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
                {
                    inside = !inside;
                }
            }

            return inside;
        }
        public static double SumOfSquares(this Point point)
        {
            var x = point.X;
            var y = point.Y;
            x = x * x;
            y = y * y;
            return x + y;
        }
        public static Point Minus(this Point point, Point other)
        {
            return new Point(point.X - other.X, point.Y - other.Y);
        }
        static double GetProjectionRatio(PointPair line, Point projection)
        {
            var result = 0.0;
            // Need to use epsilon to prevent erroneous result.
            if (!(line.P1.X - line.P2.X).IsWithinEpsilon())
            {
                result = (projection.X - line.P1.X) / (line.P2.X - line.P1.X);
            }
            else if (!(line.P1.Y - line.P2.Y).IsWithinEpsilon())
            {
                result = (projection.Y - line.P1.Y) / (line.P2.Y - line.P1.Y);
            }
            return result;
        }
        public static double Distance(this Point p1, Point p2)
        {
            var x = p1.X - p2.X;
            x = x*x;
            var y = p1.Y - p2.Y;
            y = y*y;
            return System.Math.Sqrt(x + y);
        }
        public static bool IsValidValue(this double value)
        {
            return !double.IsNaN(value)
                && !double.IsInfinity(value);
        }
        public static bool IsWithinEpsilon(this double value)
        {
            return Math.Abs(value) < GeometryMath.Epsilon;
        }
        public static int RotateNext(this int index, int count)
        {
            return (index + 1) % count;
        }
        public static bool IsWithinEpsilonTo(this double value, double center)
        {
            return Math.Abs(value - center) < GeometryMath.Epsilon;
        }
    }

    public struct PointPair
    {
        public PointPair(double x1, double y1, double x2, double y2)
            : this(new Point(x1, y1), new Point(x2, y2))
        {
        }

        public PointPair(Point p1, Point p2)
        {
            P1 = p1;
            P2 = p2;
        }

        public Point P1;
        public Point P2;

        public bool Contains(Point point)
        {
            return point.X >= P1.X
                && point.X <= P2.X
                && point.Y >= P1.Y
                && point.Y <= P2.Y;
        }

        public bool ContainsInner(Point point)
        {
            return point.X > P1.X
                && point.X < P2.X
                && point.Y > P1.Y
                && point.Y < P2.Y;
        }

        public PointPair Reverse
        {
            get
            {
                return new PointPair(P2, P1);
            }
        }

        public PointPair GetBoundingRect()
        {
            PointPair result = this;
            if (result.P1.X > result.P2.X)
            {
                var t = result.P1.X;
                result.P1.X = result.P2.X;
                result.P2.X = t;
            }
            if (result.P1.Y > result.P2.Y)
            {
                var t = result.P1.Y;
                result.P1.Y = result.P2.Y;
                result.P2.Y = t;
            }
            return result;
        }

        public double Length
        {
            get
            {
                return P1.Distance(P2);
            }
        }

        public Point Midpoint
        {
            get
            {
                return new Point((P1.X + P2.X) / 2, (P1.Y + P2.Y) / 2);
            }
        }

        public override string ToString()
        {
            return string.Format("({0};{1})-({2};{3})", P1.X, P1.Y, P2.X, P2.Y);
        }

//        public PointPair Inflate(double size)
//        {
//            return new PointPair(P1.Offset(-size), P2.Offset(size));
//        }

        public bool HasValidValue()
        {
            return ((P1.X.IsValidValue() && P1.Y.IsValidValue()) || (P2.X.IsValidValue() && P2.Y.IsValidValue()));
        }
        public Point FirstValidValue()
        {
            if (P1.X.IsValidValue() && P1.Y.IsValidValue()) return P1;
            if (P2.X.IsValidValue() && P2.Y.IsValidValue()) return P2;
            return GeometryMath.NaNPoint;
        }
    }

}