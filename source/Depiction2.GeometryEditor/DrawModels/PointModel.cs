﻿using System.Collections.Generic;
using System.Windows;

namespace Depiction2.GeometryEditor.DrawModels
{
    public class PointModel : IEditableShapeModel, IMovable
    {
        private PointBase _location;

        public IList<IEditableShapeModel> PartOf { get; private set; }
        public IList<IEditableShapeModel> ShapeParts { get; private set; }
        public IList<PointBase> ShapeVertices { get; private set; }

        public Point Location
        {
            get { return _location.Location; }
        }

        public bool AddVertex(Point vertex)
        {
            return false;
        }

        public bool InsertVertexAfter(PointBase after, PointBase vertex)
        {
            return false;
        }

        public bool RemoveVertex(PointBase vertex)
        {
            return false;
        }

        public void MoveTo(Point position)
        {
            _location = new PointBase { Location = position };
        }

    }
}