﻿using System;
using System.Windows;

namespace Depiction2.GeometryEditor.DrawModels
{
    public class PointBase
    {
        public Guid PointId { get; private set; }
        public Point Location { get; set; }

        public PointBase()
        {
            PointId = Guid.NewGuid();
        }
        public PointBase(Point point):this()
        {
            Location = point;
        }
    }
}