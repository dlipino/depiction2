﻿using System.Collections.Generic;
using System.Windows;

namespace Depiction2.GeometryEditor.DrawModels
{
    public class PolylineModel : IEditableShapeModel
    {
        private List<PointBase> _vertices; 

        public IList<IEditableShapeModel> PartOf
        {
            get { throw new System.NotImplementedException(); }
        }

        public IList<IEditableShapeModel> ShapeParts
        {
            get { throw new System.NotImplementedException(); }
        }

        public IList<PointBase> ShapeVertices
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool AddVertex(Point vertex)
        {
            _vertices.Add(new PointBase(vertex));
            return true;
        }

        public bool InsertVertexAfter(PointBase after, PointBase vertex)
        {
            throw new System.NotImplementedException();
        }

        public bool RemoveVertex(PointBase vertex)
        {
            throw new System.NotImplementedException();
        }

     
    }
}