﻿using System.Collections.Generic;
using System.Windows;
using Depiction2.GeometryEditor.DrawModels;

namespace Depiction2.GeometryEditor
{
    public interface IEditableShapeModel
    {
        IList<IEditableShapeModel> PartOf { get; }
        IList<IEditableShapeModel> ShapeParts { get; }
        IList<PointBase> ShapeVertices { get; }
        bool AddVertex(Point vertex);
        bool InsertVertexAfter(PointBase after, PointBase vertex);
        bool RemoveVertex(PointBase vertex);
    }
}