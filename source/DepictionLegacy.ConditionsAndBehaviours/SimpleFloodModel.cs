﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using DepictionLegacy.TerrainAndRoadNetwork.Interface;
using DepictionLegacy.TerrainAndRoadNetwork.Terrain;
using LGPLPrison;


[assembly: InternalsVisibleTo("Depiction2.UnitTests")]
namespace DepictionLegacy.ConditionsAndBehaviours
{
    public class SimpleFloodModel
    {
        public static IDepictionGeometry GenerateSimpleFloodGeometryFromTerrain(ICoverage terrain, Point seedPoint, double metersAboveSeaLevel)
        {
            var elevationOfFlood = metersAboveSeaLevel;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;


            //            var point = GetXYFromLatLon(terrain.GetTopLeftPosition(), terrain.GetBottomRightPosition(), terrain.GetGridWidthInPixels(), terrain.GetGridHeightInPixels(), seedPoint);
            //            var sGrid = FloodFillService.QuickFill(terrain, point, elevationOfFlood);
            //            var surfGrid = sGrid.ExpandByOnePixel();
            var surfGrid = GenerateExpandedSurfaceFlood(terrain, seedPoint, elevationOfFlood);
            //var bitmap = sGrid.GenerateBitmap();

            //BitmapSaveToFileHelper.SaveBitmap(bitmap, @"c:\temp\testbitmap.png");
            var geometry = GenerateSimpleFloodFromSurfaceGrid(surfGrid);
            if (geometry.IsValid)
                return geometry;//new DepictionGeometry(geometry);

            return null;
        }
        internal static GridSpatialData GenerateExpandedSurfaceFlood(ICoverage terrain, Point seedPoint, double metersAboveSeaLevel)
        {
            var point = GetXYFromLatLon(terrain.GetTopLeftPosition(), terrain.GetBottomRightPosition(), terrain.GetGridWidthInPixels(), terrain.GetGridHeightInPixels(), seedPoint);
            var sGrid = FloodFillService.QuickFill(terrain, point, metersAboveSeaLevel);
            var surfGrid = sGrid.ExpandByOnePixel();
            return surfGrid;
        }
        //WHAT?!?! this method makes no sense why is the seedpoint not used, 
        //This one generates a Bitmap for a flood instead of a Geometry... I think the Geometry is prettier, but I guess this could be useful for really detailed floods.
        public static IGridSpatialData GenerateSimpleFloodGridFromTerrain(ICoverage terrain, double metersAboveSeaLevel)// IDepictionLatitudeLongitude seedPoint,
        {
            var elevationOfFlood = metersAboveSeaLevel;

            if (elevationOfFlood <= 0)
                elevationOfFlood = .0000000001;

            int gridWidth = terrain.GetGridWidthInPixels();
            int gridHeight = terrain.GetGridHeightInPixels();
            var topLeft = terrain.GetTopLeftPosition();
            var bottomRight = terrain.GetBottomRightPosition();

            var surfGrid = new GridSpatialData(gridWidth, gridHeight, topLeft, bottomRight);

            for (int i = 0; i < gridWidth; i++)
            {
                for (int j = 0; j < gridHeight; j++)
                {
                    double elev = terrain.GetValueAtGridCoordinate(i, j);
                    if (elev < elevationOfFlood)
                        surfGrid.SetZ(i, j, 1);
                }
            }
            return surfGrid;
        }


        private static IDepictionGeometry GenerateSimpleFloodFromSurfaceGrid(GridSpatialData surfGrid)
        {
            var contours = ContourClass.Contour(surfGrid, 0);
            var reduced = ReducePoints(contours);
            var worldContours = ConvertImageToWorldCoordinates(surfGrid, reduced, 1);
            return GeoProcessor.ConvertToPolygon(worldContours);
        }

        private static Point GetXYFromLatLon(Point topLeft, Point bottomRight, int pixWidth, int pixHeight, Point latlon)
        {
            double latHeight = topLeft.Y - bottomRight.Y;  //topLeft.Latitude - bottomRight.Latitude;
            double lonWidth = bottomRight.X - topLeft.X;//bottomRight.Longitude - topLeft.Longitude;
            double lon = latlon.X - topLeft.X;
            double lat = latlon.Y - bottomRight.Y;

            var x = lon * pixWidth / lonWidth;
            var y = lat * pixHeight / latHeight;
            return new Point(x, y);
        }

        private static IList<List<Point>> ReducePoints(IList<List<Point>> pointList)
        {
            var newPoints = new List<List<Point>>();
            foreach (var points in pointList)
            {
                newPoints.Add(ReducePointsFrom(points));
            }
            return newPoints;
        }
        private enum direction { none, up, down, left, right };

        public static List<Point> ReducePointsFrom(List<Point> points)
        {
            var newPoints = new List<Point>();
            Point lastPoint = new Point(double.MaxValue, double.MaxValue);
            var firstTime = true;
            direction dir = direction.none;
            foreach (var point in points)
            {

                var lastDir = dir;
                if (firstTime)
                {
                    firstTime = false;
                    lastPoint = point;
                    continue;
                }

                if (point.X == lastPoint.X)
                {
                    if (point.Y > lastPoint.Y)
                        dir = direction.up;
                    else if (point.Y < lastPoint.Y)
                        dir = direction.down;
                }
                else if (point.Y == lastPoint.Y)
                {
                    if (point.X > lastPoint.X)
                        dir = direction.left;
                    else if (point.X < lastPoint.X)
                        dir = direction.right;
                }

                var dirchanged = (dir != lastDir);
                if (dirchanged)
                {
                    newPoints.Add(lastPoint);
                }
                lastPoint = point;
            }
            if (lastPoint.X != double.MaxValue)
                newPoints.Add(lastPoint);
            return newPoints;
        }


        private static IDepictionLatitudeLongitude GetWorldCoordinates(GridSpatialData grid, int col, int row)
        {
            double lon, lat;
            double heightLat = grid.HeightLat;
            double widthLon = grid.WidthLon;
            lon = (double)col / (grid.PixelWidth - 1) * widthLon + grid.TopLeft.X;
            lat = grid.BottomRight.Y + (double)row / (grid.PixelHeight - 1) * heightLat;
            throw new NotImplementedException();
//            return new LatitudeLongitudeDotSpatial(lat, lon);
        }

        internal static IList<List<IDepictionLatitudeLongitude>> ConvertImageToWorldCoordinates(GridSpatialData grid, IList<List<Point>> cList, int offset)
        {
            var worldCoordinates = new List<List<IDepictionLatitudeLongitude>>();
            double x, y;
            double currentX = -10000, currentY = -10000;

            for (int i = 0; i < cList.Count; i++)
            {
                var aList = cList[i];
                var nList = new List<IDepictionLatitudeLongitude>();
                worldCoordinates.Add(nList);
                for (int j = 0; j < aList.Count; j++)
                {
                    x = aList[j].X - offset;
                    y = aList[j].Y - offset;
                    if (x != currentX || y != currentY)
                    {
                        currentX = x;
                        currentY = y;
                        nList.Add(GetWorldCoordinates(grid, (int)x, (int)y));
                    }
                    else
                    {
                        j--;
                    }
                }
            }
            return worldCoordinates;
        }
    }
}
