﻿using System;
using System.Windows;
using DepictionLegacy.TerrainAndRoadNetwork.Interface;
using DepictionLegacy.TerrainAndRoadNetwork.Terrain;
//using System.Drawing;

namespace DepictionLegacy.ConditionsAndBehaviours
{
    public class FloodFillService
    {
        //Queue of floodfill ranges. We use our own class to increase performance.
        //To use .NET Queue class, change this to:
        //<FloodFillRange> ranges = new Queue<FloodFillRange>();
        static FloodFillRangeQueue ranges = new FloodFillRangeQueue();
        static private ICoverage bitmap { get; set; }
        static private int bitmapHeight { get; set; }
        static private int gBitmapWidth { get; set; }
        static private bool[,] gPixelsChecked { get; set; }
        static GridSpatialData surfaceGrid { get; set; }
        //static protected byte[] gByteFillColor;
        static private double startColor;

        public static GridSpatialData QuickFill(ICoverage pBitmap, Point pt, double metersAboveSeaLevel)
        {
            bitmap = pBitmap;
            
            surfaceGrid = new GridSpatialData(pBitmap.GetGridWidthInPixels(),
                                              pBitmap.GetGridHeightInPixels(), pBitmap.GetTopLeftPosition(),
                                              pBitmap.GetBottomRightPosition());

            //***Prepare for fill.
            PrepareForFloodFill(pt);

            ranges = new FloodFillRangeQueue(((gBitmapWidth + bitmapHeight) / 2) * 5);//new Queue<FloodFillRange>();

            //***Get starting color.
            int x = (int)pt.X; 
            int y = (int)pt.Y;
            //int idx = CoordsToByteIndex(ref x, ref y);
            startColor = metersAboveSeaLevel;

            bool[,] pixelsChecked = gPixelsChecked;

            //***Do first call to floodfill.
            LinearFill(ref x, ref y);

            //***Call floodfill routine while floodfill ranges still exist on the queue
            while (ranges.Count > 0)
            {
                //**Get Next Range Off the Queue
                FloodFillRange range = ranges.Dequeue();

                //**Check Above and Below Each Pixel in the Floodfill Range
                //int downPxIdx = (gBitmapWidth * (range.Y + 1)) + range.StartX;//CoordsToPixelIndex(lFillLoc,y+1);
                //int upPxIdx = (gBitmapWidth * (range.Y - 1)) + range.StartX;//CoordsToPixelIndex(lFillLoc, y - 1);
                int upY = range.Y - 1;//so we can pass the y coord by ref
                int downY = range.Y + 1;
                int tempIdx;
                for (int i = range.StartX; i <= range.EndX; i++)
                {
                    //*Start Fill Upwards
                    //if we're not above the top of the bitmap and the pixel above this one is within the color tolerance
                    //tempIdx = CoordsToByteIndex(ref i, ref upY);
                    if (range.Y > 0 && (!pixelsChecked[i, upY]) && CheckPixel(ref i, ref upY))
                        LinearFill(ref i, ref upY);

                    //*Start Fill Downwards
                    //if we're not below the bottom of the bitmap and the pixel below this one is within the color tolerance
                    //tempIdx = CoordsToByteIndex(ref i, ref downY);
                    if (range.Y < (bitmapHeight - 1) && (!pixelsChecked[i, downY]) && CheckPixel(ref i, ref downY))
                        LinearFill(ref i, ref downY);
                    //downPxIdx++;
                    //upPxIdx++;
                }

            }
            return surfaceGrid;
        }

        /// <summary>
        /// Finds the furthermost left and right boundaries of the fill area
        /// on a given y coordinate, starting from a given x coordinate, filling as it goes.
        /// Adds the resulting horizontal range to the queue of floodfill ranges,
        /// to be processed in the main loop.
        /// </summary>
        /// <param name="x">The x coordinate to start from.</param>
        /// <param name="y">The y coordinate to check at.</param>
        private static void LinearFill(ref int x, ref int y)
        {

            //cache some bitmap and fill info in local variables for a little extra speed
            //byte[] bitmapBits = this.bitmapBits;
            bool[,] pixelsChecked = gPixelsChecked;
            //byte[] byteFillColor = gByteFillColor;
            //int bitmapPixelFormatSize = this.bitmapPixelFormatSize;
            int bitmapWidth = gBitmapWidth;

            //***Find Left Edge of Color Area
            int lFillLoc = x; //the location to check/fill on the left
            //int idx = CoordsToByteIndex(ref x, ref y); //the byte index of the current location
            //int pxIdx = (bitmapWidth * y) + x;//CoordsToPixelIndex(x,y);
            while (true)
            {
                //**fill with the color
                surfaceGrid.SetZ(y, lFillLoc, 1);
                //bitmapBits[idx] = byteFillColor[0];
                //bitmapBits[idx + 1] = byteFillColor[1];
                //bitmapBits[idx + 2] = byteFillColor[2];

                //**indicate that this pixel has already been checked and filled
                pixelsChecked[lFillLoc, y] = true;

                //**screen update for 'slow' fill
                //if (slow) UpdateScreen(ref lFillLoc, ref y);
                
                //**de-increment
                lFillLoc--;     //de-increment counter
                //idx--;        //de-increment pixel index
                //idx -= bitmapPixelFormatSize;//de-increment byte index
                //**exit loop if we're at edge of bitmap or color area
                if (lFillLoc <= 0 || (pixelsChecked[lFillLoc, y]) || !CheckPixel(ref lFillLoc, ref y))
                    break;

            }
            lFillLoc++;

            //***Find Right Edge of Color Area
            int rFillLoc = x; //the location to check/fill on the left
            //idx = CoordsToByteIndex(ref x, ref y);
            while (true)
            {
                //**fill with the color
                surfaceGrid.SetZ(y, rFillLoc, 1);
                //bitmapBits[idx] = byteFillColor[0];
                //bitmapBits[idx + 1] = byteFillColor[1];
                //bitmapBits[idx + 2] = byteFillColor[2];
                
                //**indicate that this pixel has already been checked and filled
                pixelsChecked[rFillLoc, y] = true;
                
                //**screen update for 'slow' fill
                //if (slow) UpdateScreen(ref rFillLoc, ref y);
                //**increment
                rFillLoc++;     //increment counter
                //idx++;        //increment pixel index
                //idx += bitmapPixelFormatSize;//increment byte index
                //**exit loop if we're at edge of bitmap or color area
                if (rFillLoc >= bitmapWidth || pixelsChecked[rFillLoc, y] || !CheckPixel(ref rFillLoc, ref y))
                    break;
            }
            rFillLoc--;

            //add range to queue
            FloodFillRange r = new FloodFillRange(lFillLoc, rFillLoc, y);
            ranges.Enqueue(ref r);
        }

        ///<summary>Sees if a pixel is within the color tolerance range.</summary>
        ///<param name="px">The byte index of the pixel to check, passed by reference to increase performance.</param>
        protected static bool CheckPixel(ref int x, ref int y)
        {
            //tried a 'for' loop but it adds an 8% overhead to the floodfill process
            /*bool ret = true;
            for (byte i = 0; i < 3; i++)
            {
                ret &= (bitmap.Bits[px] >= (startColor[i] - tolerance[i])) && bitmap.Bits[px] <= (startColor[i] + tolerance[i]);
                px++;
            }
            return ret;*/

            return bitmap.GetValueAtGridCoordinate(x, y) < startColor;

            //return (bitmapBits[px] >= (startColor[0] - tolerance[0])) && bitmapBits[px] <= (startColor[0] + tolerance[0]) &&
            //    (bitmapBits[px + 1] >= (startColor[1] - tolerance[1])) && bitmapBits[px + 1] <= (startColor[1] + tolerance[1]) &&
            //    (bitmapBits[px + 2] >= (startColor[2] - tolerance[2])) && bitmapBits[px + 2] <= (startColor[2] + tolerance[2]);
        }

        private static void PrepareForFloodFill(Point pt)
        {
            //cache data in member variables to decrease overhead of property calls
            //this is especially important with Width and Height, as they call
            //GdipGetImageWidth() and GdipGetImageHeight() respectively in gdiplus.dll - 
            //which means major overhead.
            //byteFillColor = new byte[] { fillColor.B, fillColor.G, fillColor.R };
            //bitmapStride = bitmap.Stride;
            //bitmapPixelFormatSize = bitmap.PixelFormatSize;
            //bitmapBits = bitmap.Bits;
            gBitmapWidth = bitmap.GetGridWidthInPixels();
            bitmapHeight = bitmap.GetGridHeightInPixels();

            gPixelsChecked = new bool[gBitmapWidth,bitmapHeight];
        }
    }

    internal class FloodFillRangeQueue
    {
        FloodFillRange[] array;
        int size;
        int head;

        /// <summary>
        /// Returns the number of items currently in the queue.
        /// </summary>
        public int Count
        {
            get { return size; }
        }

        public FloodFillRangeQueue()
            : this(10000)
        {

        }

        public FloodFillRangeQueue(int initialSize)
        {
            array = new FloodFillRange[initialSize];
            head = 0;
            size = 0;
        }

        /// <summary>Gets the <see cref="FloodFillRange"/> at the beginning of the queue.</summary>
        public FloodFillRange First
        {
            get { return array[head]; }
        }

        /// <summary>Adds a <see cref="FloodFillRange"/> to the end of the queue.</summary>
        public void Enqueue(ref FloodFillRange r)
        {
            if (size + head == array.Length)
            {
                FloodFillRange[] newArray = new FloodFillRange[2 * array.Length];
                Array.Copy(array, head, newArray, 0, size);
                array = newArray;
                head = 0;
            }
            array[head + (size++)] = r;
        }

        /// <summary>Removes and returns the <see cref="FloodFillRange"/> at the beginning of the queue.</summary>
        public FloodFillRange Dequeue()
        {
            FloodFillRange range = new FloodFillRange();
            if (size > 0)
            {
                range = array[head];
                array[head] = new FloodFillRange();
                head++;//advance head position
                size--;//update size to exclude dequeued item
            }
            return range;
        }

        /// <summary>Remove all FloodFillRanges from the queue.</summary>
        /*public void Clear() 
        {
            if (size > 0)
                Array.Clear(array, 0, size);
            size = 0;
        }*/

    }

    /// <summary>
    /// Represents a linear range to be filled and branched from.
    /// </summary>
    internal struct FloodFillRange
    {
        public int StartX;
        public int EndX;
        public int Y;

        public FloodFillRange(int startX, int endX, int y)
        {
            StartX = startX;
            EndX = endX;
            Y = y;
        }
    }

}