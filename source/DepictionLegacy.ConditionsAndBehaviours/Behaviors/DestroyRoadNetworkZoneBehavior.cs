using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using Depiction2.API.Service;
using Depiction2.Base.Geo;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.TempExtensionCore.Behaviors;
using DepictionLegacy.TerrainAndRoadNetwork.Interface;
using DepictionLegacy.TerrainAndRoadNetwork.RoadNetwork;

namespace DepictionLegacy.ConditionsAndBehaviours.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("DestroyZone", "Destroy a road network", "Destroy a road network")]
    public class DestroyRoadNetworkZoneBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters = 
            new[]
                {
                    new DepictionParameterInfo("Destroyer", typeof(IElement))
                        {
                            ParameterDescription = "Specify which element destroys the road network (this is usually the publisher)", 
                            ParameterName = "Choose the Publisher"
                        }
                };

        public override DepictionParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Destroy road network"; }
        }

        public string Description
        {
            get { return "Makes all road segments within the Destroyer elements zone of influence unusable"; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var triggerElement = parameterBag["Destroyer"] as IElement;

            var roadGraphProp = subscriber.GetProperty("RoadGraph");
            if (roadGraphProp == null)
            {
                throw new Exception(string.Format("The '{0}' behavior expects its Subscriber to be a Road Network", FriendlyName));
            }
//            subscriber.RemovePropertyIfNameAndTypeMatch(roadGraphProp);
           
            var roadGraph = roadGraphProp.Value as IRoadGraph;
            if(roadGraph == null) return new BehaviorResult();
            bool notifyChange = DisableIntersectingEdges(triggerElement, roadGraph);
//            subscriber.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RoadGraph", roadGraph) { VisibleToUser = false }, notifyChange);
            return new BehaviorResult { SubscriberHasChanged = true };
        }


        private static bool DisableIntersectingEdges(IElement triggerElement, IRoadGraph roadGraph)
        {
            if (triggerElement.ElementGeometry == null ) return false;
            var affectedEdges = GetIntersectingEdges(triggerElement.ElementGeometry, roadGraph);
            foreach (var edge in affectedEdges)
            {
                roadGraph.DisableEdge(edge);
            }
            return affectedEdges.Count > 0;
        }

        private static IList<RoadSegment> GetIntersectingEdges(IDepictionGeometry zoneOfInfluence, IRoadGraph roadGraph)
        {
//            var precisionModel = new PrecisionModel(); //FLOATING POINT precision
//            var geometryFactory = new GeometryFactory(precisionModel, 32767);
            var edgesAffected = new List<RoadSegment>();
            var graph = roadGraph.Graph;

            foreach (var edge in graph.Edges)
            {
                var coordinates = new List<Point>();
                coordinates.Add(edge.Source.Vertex);// new Coordinate(edge.Source.Vertex.Longitude, edge.Source.Vertex.Latitude);
                coordinates.Add(edge.Target.Vertex);// new Coordinate(edge.Target.Vertex.Longitude, edge.Target.Vertex.Latitude);

                var objLineString = DepictionGeometryService.CreateGeometry(coordinates);// new GeometryGdalWrap(coordinates); // new DepictionGeometry(geometryFactory.CreateLineString(coordinates));
                lock (zoneOfInfluence)
                {
                    if (objLineString.Intersects(zoneOfInfluence))
                    {
                        edgesAffected.Add(edge);
                    }
                }
            }

            return edgesAffected;
        }
    }
}