﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Windows;
using Depiction2.API.Measurement;
using Depiction2.API.Service;
using Depiction2.API.ValidationRules;
using Depiction2.Base.Geo;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.Measurement;
using Depiction2.Base.StoryEntities;
using Depiction2.Base.ValidationRules;
using Depiction2.Core.Geo;
using Depiction2.TempExtensionCore.Behaviors;
using DepictionCoverage;
using DepictionLegacy.TerrainAndRoadNetwork.Terrain;

namespace DepictionLegacy.ConditionsAndBehaviours.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("LineOfSightArc", "Calculate a simple line of sight arc", "Calculates a simple line of sight arc")]
    public class LineOfSightArc : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Terrain", typeof (TerrainCoverage))
                        {
                            ParameterDescription = "Elevation Data",
                            ParameterName = "The elevation data to use when calculating the flood"
                        },
                    new DepictionParameterInfo("Range", typeof (Distance))
                        {
                            ParameterDescription = "Distance to cover for line of sight",
                            ParameterName = "Range distance"
                        },
                    new DepictionParameterInfo("Height", typeof (Distance))
                        {
                            ParameterDescription = "Height from which to evaluate line of sight",
                            ParameterName = "Height"
                        },
                    new DepictionParameterInfo("Orientation", typeof (Angle))
                        {
                            ParameterName = "Orientation",
                            ParameterDescription = "The new orientation angle for the line of sight arc"
//                            ,ValidationRules =
//                                new IValidationRule[]
//                                    {
//                                        new DataTypeValidationRule(typeof (Angle),
//                                                                   "The new orientation value must be a number")
//                                    }
                        },
                    new DepictionParameterInfo("Width", typeof (double))
                        {
                            ParameterName = "Width",
                            ParameterDescription = "The new angle width for the line of sight arc",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "The new angle width value must be a number")
                                    }
                        },
                    new DepictionParameterInfo("Resolution", typeof (Distance))
                        {
                            ParameterDescription = "Sampling interval at which to evaluate line of sight",
                            ParameterName = "Resolution"
                        },
                    new DepictionParameterInfo("HeightTolerance", typeof (Distance))
                        {
                            ParameterDescription = "Height tolerance that accounts for elevation data accuracy",
                            ParameterName = "Height Tolerance"
                        }
                };

        public override DepictionParameterInfo[] Parameters
        {
            get { return parameters; }

        }

        public string FriendlyName
        {
            get { return "Create line of sight arc polygon"; }
        }

        public string Description
        {
            get { return "Create line of sight arc polygon"; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {

            if (subscriber.GeoLocation == null)
                return new BehaviorResult();
            //WHy are all the parameters element properties?
            var terrainPlusStructure = parameterBag["Terrain"] as TerrainCoverage;
            var radius = parameterBag["Range"] as Distance;
            var height = parameterBag["Height"] as Distance;
            var width = (double)(parameterBag["Width"]);
            var orientation = (double)(parameterBag["Orientation"]); 
//            if(radius==null || height == null || terrainPlusStructure == null)
//            {
//                if(terrainPlusStructure == null)
//                {
////                    DepictionAccess.NotificationService.DisplayMessageString(
////                        "Cannot run line of sight interaction without elevation", 5);
//                    subscriber.SetPropertyValue("Active", false, true);
//                }
//                return new BehaviorResult();
//            }
//            bool active;
//            if(subscriber.GetPropertyValue("Active", out active))
//            {
//                if(!active)
//                {
//                    subscriber.SetPropertyValue("Active", true, true);
//                }
//            }

            var northernOrientation = 90 - orientation;
            double startAngle = northernOrientation - width / 2;
            double stopAngle = northernOrientation + width / 2;
            if (startAngle < 0) startAngle += 360;
            if (stopAngle < 0) stopAngle += 360;
            if (stopAngle > 360) stopAngle = 360 - stopAngle;
            //this makes sure start and stop angles are always between  and 360

            var samplingInterval = parameterBag["Resolution"] as Distance;
            var heightTolerance = parameterBag["HeightTolerance"] as Distance;
            if (heightTolerance == null || samplingInterval == null) return new BehaviorResult();
            //we want everything internally represented in meters
            var radiusMeters = radius.GetValue(MeasurementSystem.Metric,MeasurementScale.Normal);
            var heightMeters = height.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            var sampingIntervalInMeters = samplingInterval.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
            var heightToleranceMeters = heightTolerance.GetValue(MeasurementSystem.Metric, MeasurementScale.Normal);
//            DepictionAccess.NotificationService.DisplayMessageString(string.Format(
//                                                                     "{0} calculations for large ranges may take many minutes...", subscriber.TypeDisplayName),3);
            long memAllocatedBefore = GC.GetTotalMemory(true);
            subscriber.UpdatePrimaryPointAndGeometry(subscriber.GeoLocation, DepictionGeometryService.CreateGeometry());//reset ZOI to a point in case things go poorly
//            subscriber.SetZOIGeometry(new DepictionGeometry(subscriber.Position));//reset ZOI to a point
            var elevationData = CreateCoverage(subscriber, terrainPlusStructure, sampingIntervalInMeters);

            UpdateZoneOfInfluence(subscriber, elevationData,subscriber, radiusMeters, heightMeters, heightToleranceMeters,startAngle,stopAngle);

            //delete elevationData
            elevationData.Dispose(); 
            GC.Collect();
            long memAllocatedAfter = GC.GetTotalMemory(true);
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        private static void UpdateZoneOfInfluence(IElement subscriber, Coverage elevationData, IElement LOSObject, double radius, double height, double heightTolerance, double startAngle, double stopAngle)
        {
//            var geomFactory = new GeometryFactory();
            if (LOSObject.GeoLocation == null) return;
            var losPos = (Point) LOSObject.GeoLocation;
            //
            //Get the Line Of Sight object's latlon
            //
            double y = losPos.Y;//LOSObject.Position.Latitude;
            double x = losPos.X;// LOSObject.Position.Longitude;

            //if the location is outside the Depiction region, LOS runs into problems
            //the elevation outside the region is unknown. The tower sits on top of this elevation
            //what do you do?
            //kick em out
            double elevvalue = elevationData.GetClosestElevationValue(x, y);
            if(elevvalue.Equals(Double.NaN))
            {
                //unknown elevation or outside the Depiction region
                //notify

//                DepictionAccess.NotificationService.DisplayMessageString(string.Format(
//                                                                         "{0} has been placed at a location of unknown elevation value. Please place it where there is elevation and try again.", subscriber.TypeDisplayName));
                return ;
            }

            var cList = new ArrayList();
            radius = GetRadiusInPixels(elevationData, radius);
            //
            //compute line of sight
            //
            elevationData.LineOfSightContours(x, y, height, radius, heightTolerance, cList,startAngle,stopAngle);
            
            //death to arraylists, for now localize them here and keep them from infecting other things
            var actualList = ArrayListToLatLongLists(cList);

            var contourList = GeoProcessor.ConvertToPolygon(actualList, -1000, -1000, 0/*tolerance around seed point*/);
            if (contourList != null)
            {
                lock (LOSObject.ElementGeometry)
                {
//                    LOSObject.ClearZoneOfInfluence();
                    IDepictionGeometry shell = null;
                    var holes = new List<IDepictionGeometry>();
                    for (int i = 0; i < contourList.Length; i++)
                    {
                        if (i == 0)
                        {
                            var shellCoordinates = contourList[i];
//                            var coordinates = new CoordinateList();

//                            foreach (var latLong in shellCoordinates)
//                            {
//                                coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
//                            }
                            shell = DepictionGeometryService.CreateGeometry(shellCoordinates);//geomFactory.CreateLinearRing(coordinates.ToArray());
                        }
                        else
                        {
//                            var coordinates = new CoordinateList();
//                            foreach (LatitudeLongitude latLong in contourList[i])
//                            {
//                                coordinates.Add(new Coordinate(latLong.Longitude, latLong.Latitude));
//                            }
//                            holes.Add(geomFactory.CreateLinearRing(coordinates.ToArray()));
                            holes.Add(DepictionGeometryService.CreateGeometry(contourList[i]));
                        }
                    }

//                    var geometry = geomFactory.CreatePolygon(shell, holes.ToArray());

                    //buffering it to prevent intersection conflicts//alas that causes issues with polygons that are multi polygons
                    //but are not declared as such in the above loop. Basically it will turn the normal polygon into a multipolygon
                    //and then it won't get drawn correctly.
                   //if (!geometry.IsEmpty)
                   //     geometry = geometry.Buffer(0.000001);
//                    LOSObject.SetZOIGeometry(new DepictionGeometry(geometry));
                    LOSObject.UpdatePrimaryPointAndGeometry(losPos,shell);
                }
            }
            //force garbage collection

            for (int j = 0; j < cList.Count; j++)
            {
                var contour = (ArrayList)cList[j];
                for (int k = 0; k < contour.Count; k++)
                {
                    contour[k] = null;
                }
                contour.Clear();
                cList[j] = null;
            }
            cList.Clear();
            GC.Collect();

        }

        private static List<List<Point>> ArrayListToPointLists(ArrayList cList)
        {
            var cListActualList = new List<List<Point>>();
            foreach (var list in cList)
            {
                var actualList = new List<Point>();
                var arrayList = (ArrayList)list;
                foreach (var obj in arrayList)
                {
                    var point = (Point)obj;
                    actualList.Add(point);//new LatitudeLongitude(point.Y, point.X));
                }
                cListActualList.Add(actualList);
            }
            return cListActualList;
        }
        private static List<List<IDepictionLatitudeLongitude>> ArrayListToLatLongLists(ArrayList cList)
        {
            var cListActualList = new List<List<IDepictionLatitudeLongitude>>();
            foreach (var list in cList)
            {
                var actualList = new List<IDepictionLatitudeLongitude>();
                var arrayList = (ArrayList)list;
                foreach (var obj in arrayList)
                {
                    var point = (Point)obj;
                    actualList.Add(new LatitudeLongitudeDepiction(point.Y, point.X));
                }
                cListActualList.Add(actualList);
            }
            return cListActualList;
        }

        private static double GetRadiusInPixels(Coverage data, double radius)
        {

            var widthInPixels = data.GetWidthInPixels();
            var heightInPixels = data.GetHeightInPixels();
            var diagDistance = Math.Sqrt(widthInPixels * widthInPixels + heightInPixels * heightInPixels);
            var cornerPts = new ArrayList();
            data.GetCorners(cornerPts, true);//corners in true geographic coordinates

            throw new NotImplementedException();
//            var topLeft = new LatitudeLongitudeDotSpatial((double)cornerPts[1], (double)cornerPts[0]);
//            var bottomRight = new LatitudeLongitudeDotSpatial((double)cornerPts[3], (double)cornerPts[2]);
//            var diagDistanceInMeters = topLeft.DistanceTo(bottomRight, MeasurementSystem.Metric, MeasurementScale.Normal);

//            double radiusInPixels = radius * diagDistance / diagDistanceInMeters;
//            return radiusInPixels;
        }

        /// <summary>
        /// Returns a Coverage containing the data from this terrain object.
        /// Will resample the elevation data grid to the resolution defined by the samplingInterval parameter.
        /// </summary>
        /// <param name="subscriber">The element to change.</param>
        /// <param name="terrainCoverage">The elevation data for the depiction area.</param>
        /// <param name="samplingInterval">Resample the elevation grid every this number of meters.</param>
        /// <returns></returns>
        private static Coverage CreateCoverage(IElement subscriber, TerrainCoverage terrainCoverage, double samplingInterval)
        {
            var elevationData = new Coverage();
            lock (terrainCoverage)
            {
                int originalNumRows = terrainCoverage.GetGridHeightInPixels();
                int originalNumCols = terrainCoverage.GetGridWidthInPixels();
                var topLeft = terrainCoverage.GetTopLeftPosition();
                var botRight = terrainCoverage.GetBottomRightPosition();
                var south = botRight.Y;
                var north = topLeft.Y;
                var east = botRight.X;
                var west = topLeft.X;
//                double south = botRight.Latitude;
//                double north = topLeft.Latitude;
//                double east = botRight.Longitude;
//                double west = topLeft.Longitude;

                bool success = elevationData.Create(south, north, east, west, terrainCoverage.GetGridWidthInPixels(),
                                                    terrainCoverage.GetGridHeightInPixels(),
                                                    terrainCoverage.IsFloatMode(), -32768, "", 0);
                if (success)
                {
                    //resample the elevation data grid to the resolution defined by the samplingInterval (in meters) parameter.
                    double resampleFactor = elevationData.ResampleElevationGrid(samplingInterval);

                    //
                    //fill in the elevationData grid values from terrain
                    //fill in the grid with values from terrain
                    for (int col = 0; col < elevationData.GetWidthInPixels(); col++)
                        for (int row = 0; row < elevationData.GetHeightInPixels(); row++)
                        {
                            int origCol = (int)(col / resampleFactor);
                            if (origCol >= originalNumCols - 1) origCol = originalNumCols - 1;
                            int origRow = (int)(row / resampleFactor);
                            if (origRow >= originalNumRows - 1) origRow = originalNumRows - 1;
                            float val = terrainCoverage.GetValueAtGridCoordinate(origCol, origRow);
                            elevationData.SetElevationValue(col, row, val);
                        }
                }
                else
                {
//                    DepictionAccess.NotificationService.DisplayMessageString(string.Format(
//                                                                             "{0} failed; system might be low on memory.", subscriber.TypeDisplayName));
                }
            }
            return elevationData;
        }
    

    }
}