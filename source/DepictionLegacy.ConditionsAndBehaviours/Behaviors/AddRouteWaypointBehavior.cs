﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;
using Depiction2.TempExtensionCore.Behaviors;

namespace DepictionLegacy.ConditionsAndBehaviours.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("AddRouteWaypoint", "Add a route waypoint", "Set initial route waypoints")]
    public class AddRouteWaypointBehavior : BaseBehavior
    {
        private static readonly DepictionParameterInfo[] parameters =
            new[]
                {
                    new DepictionParameterInfo("Location", typeof(Point))
                };
        public override DepictionParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Add way point to route"; }
        }

        public string Description
        {
            get { return "Adds a waypoint to a route."; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            var locObject = parameterBag["Location"];
            if(!(locObject is Point) )return new BehaviorResult();
            var point = (Point) locObject;
            var waypoints = subscriber.AssociatedElements.ToArray();
            var zoiCoords = subscriber.ElementGeometry.GeometryPoints.ToArray();
            var zoiCoordLocation = 0;
            var minDistance = double.PositiveInfinity;
            IElement previousWaypoint = null;
            //find the lines that come out of each way point
            for (int i = 0; i < (waypoints.Length - 1); i++)
            {
                var end = waypoints[i + 1].GeoLocation;
                for (; zoiCoordLocation < zoiCoords.Length; zoiCoordLocation++)
                {
                    var vect = point - zoiCoords[zoiCoordLocation];
                    var distance = Math.Abs(vect.Length);
                    if(distance<minDistance)
                    {
                        minDistance = distance;
                        previousWaypoint = waypoints[i];
                    }
                    if (zoiCoords[zoiCoordLocation].Equals(end))
                    {
                        break;
                    }
                }
            }
            var waypointScaffold = DepictionAccess.ScaffoldLibrary.FindScaffold("Waypoint");
            var associatedElement = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(waypointScaffold, new List<Point> { point });
            if (associatedElement != null)
            {
                DepictionAccess.DStory.AddElement(associatedElement, true);
                subscriber.AttachElementAfter(associatedElement, previousWaypoint);
            }

            return new BehaviorResult();


        }
    }
}