using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction2.API.Converters;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.TempExtensionCore.Behaviors;

namespace DepictionLegacy.ConditionsAndBehaviours.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("SetProperty", "Set element property", "Set element property")]
    public class SetPropertyBehavior : BaseBehavior
    {
        public override DepictionParameterInfo[] Parameters
        {
            get
            {
                return new[] {new DepictionParameterInfo("PropertyName", typeof (string))
                                  {
                                      ParameterName = "Property To Set", ParameterDescription = "Indicates the property whos value will be changed"
                                  }, 
                              new DepictionParameterInfo("Value", typeof (object))
                                  {
                                      ParameterName = "New Value", ParameterDescription = "The property to be changed will have its value set to this value"
                                  }};
            }
        }

        public string FriendlyName
        {
            get { return "Set property value"; }
        }

        public string Description
        {
            get { return "Set a property value of a chosen element property"; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (!parameterBag.ContainsKey("PropertyName")) return new BehaviorResult(); 
            var propName = parameterBag["PropertyName"].ToString();
            var val = parameterBag["Value"];
       
            var prop = subscriber.GetProperty(propName);
            if (prop == null) return new BehaviorResult { SubscriberHasChanged = false };
            var convertedVal = DepictionTypeConverter.ChangeType(val, prop.Value.GetType());
            prop.SetValue(convertedVal);
//            if (subscriber.SetPropertyValue(propName, convertedVal) == false) return new BehaviorResult() { SubscriberHasChanged = false };

            return new BehaviorResult();
        }
    }
}