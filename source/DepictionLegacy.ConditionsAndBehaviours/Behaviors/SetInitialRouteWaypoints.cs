﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using Depiction2.API;
using Depiction2.Base.Interactions;
using Depiction2.Base.Interactions.Behaviors;
using Depiction2.Base.StoryEntities;
using Depiction2.Core.Service;
using Depiction2.Core.StoryEntities.Scaffold;
using Depiction2.TempExtensionCore.Behaviors;

namespace DepictionLegacy.ConditionsAndBehaviours.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("SetRouteWaypoints", "Set initial route waypoints", "Set initial route waypoints")]
    public class SetInitialRouteWaypoints : BaseBehavior
    {
        public override DepictionParameterInfo[] Parameters
        {
            get { return null; }
        }

        public string FriendlyName
        {
            get { return "Initiate route waypoints"; }
        }

        public string Description
        {
            get { return "Sets initial route waypoints."; }
        }

        protected override BehaviorResult InternalDoBehavior(IElement subscriber, Dictionary<string, object> parameterBag)
        {
            if (subscriber.ElementType.Equals("Depiction.Plugin.RouteUserDrawn") ||
                (subscriber.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork") ||
                subscriber.ElementType.Equals("Depiction.Plugin.DriveTimeRoute") ||
                subscriber.ElementType.Equals("Depiction.Plugin.RouteWaypoint")))
            {
                var zoi = subscriber.ElementGeometry;
                var zoiCoords = zoi.GeometryPoints.ToArray();

                var coordCount = zoiCoords.Length;
                if (coordCount <= 1) return new BehaviorResult();
                var waypointScaffold = DepictionAccess.ScaffoldLibrary.FindScaffold("Waypoint");
                var startScaffold = DepictionAccess.ScaffoldLibrary.FindScaffold("start");
                var endScaffold = DepictionAccess.ScaffoldLibrary.FindScaffold("end");
//                var associatedElemetns = new List<IElement>();
                for (int i = 0; i < coordCount; i++)
                {
                    IElement associatedElement = null;
                    var pointList = new List<Point> {zoiCoords[i]};
                    if (i == 0)
                    {
                        associatedElement = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(startScaffold, pointList);
                    }
                    else if (i == coordCount - 1)
                    {
                        associatedElement = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(endScaffold, pointList);
                    }
                    else
                    {
                        associatedElement = ElementAndScaffoldService.CreateElementFromScaffoldAndPoints(waypointScaffold, pointList);
                    }
                    if (associatedElement != null)
                    {
                        associatedElement.UpdatePrimaryPointAndGeometry(zoiCoords[i], null);
                        DepictionAccess.DStory.AddElement(associatedElement,true);
                        subscriber.AttachElement(associatedElement);
                    }
                }
            }

            return new BehaviorResult();
        }
    }
}